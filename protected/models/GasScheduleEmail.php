<?php

/**
 * This is the model class for table "{{_gas_schedule_email}}".
 * 12 
 * The followings are the available columns in table '{{_gas_schedule_email}}':
 * @property string $id
 * @property integer $type
 * @property integer $email_template_id
 * @property string $user_id
 * @property string $time_send 
 * @property string $created_date
 */
class GasScheduleEmail extends BaseSpj
{
    const MAX_SEND      = 50; // Feb 26, 2015
    const MAIL_PRIMARY  = "nkhuongminh@gmail.com"; // Sep 17, 2015
    const MAIL_SECOND   = "nkhuongminh1@gmail.com"; // Sep 17, 2015
    const MAIL_THIRD    = "nkhuongminh2@gmail.com"; // Oct 22, 2015

    /* TYPE SEND MAIL */
    const MAIL_RESET_PASS               = 1;
    const MAIL_TEXT_ALERT               = 2;
    const MAIL_SUPPORT_CUSTOMER_CHANGE_STATUS = 3;
    const MAIL_ISSUE_TICKET_CHANGE      = 4;
    const MAIL_LEAVE_APPROVE            = 5;
    const MAIL_NEW_CUSTOMER             = 6; // Apr 12, 2016 mail notify cho điều phối vs kế toán khi sale or CCS tạo KH
    const MAIL_SETTLE_ALERT             = 7; // Jul 10, 2016 mail notify cho user tạo và cấp quản lý về trạng thái của quết toán
    const MAIL_SHELL_RETURN_PLANNING    = 8; // June 23 send email to sale when make new shell return planing
    const MAIL_PHAP_LY_UPDATE           = 9; // Sep 19, 2016 send email to sale when make new shell return planing
    const MAIL_STATUS_CUSTOMER_SPECIAL  = 10; // Oct 07, 2016 send email to sale when make new or change status
    const MAIL_STATUS_PRICE_REQUEST     = 11; // DEC 26, 2016 send email Đề xuất tăng giảm giá KH Bò Mối
    const MAIL_QUOTES                   = 12; // Feb 26, 2017 send email báo giá KH Bò Mối
    const MAIL_BUG_TO_DEV               = 13; // SEND BUG TO DEV
    const MAIL_MEMBER_NEW               = 16; // gửi tài khoản + mật khẩu khi tạo user mới
    const MAIL_MEMBER_LEAVE             = 17; // nhân viên nghỉ việc
    const MAIL_EMPLOYEE_NEW             = 18; // nhân viên mới
    //++ TSK0206-SPJ (NguyenPT 20180117) Handle notify email
    const MAIL_NOTIFY_INTERVIEW             = 19;   // Notify when have new HR Interview plan was created to interviewer
    const MAIL_NOTIFY_INTERVIEW_CANDIDATE   = 20;   // Notify when have new HR Interview plan was created to candidate
    const MAIL_NOTIFY_HR_PLAN_APPROVER      = 21;   // Notify when have new HR plan was created to approver
    //-- TSK0206-SPJ (NguyenPT 20180117) Handle notify email
    
    /* TYPE SEND MAIL */

    // 3: support customer, 4: ticket change, 5 nghi phep
    // DELETE FROM `c1gas35`.`gas_gas_schedule_email` WHERE `gas_gas_schedule_email`.`type` =2
    // Mar 14, 2015 => 1240 rows deleted. (Query took 0.0143 sec)

    // Jan 26,2015 
    public static $UID_NOT_RESET_PASS = array(
        111250, // Bùi Đức Hoàn giam doc kinh doanh
        136808, // Đoàn Ngọc Hải Triều truong phong kinh doanh gas moi
        114943, // Phạm Văn Đức truong phong kinh doanh gas bo
        26676, // Vũ Thái Long giam doc
        303, // Nguyễn Văn Đợt Tổng giám sát
        112677, // Kho Bến Cát User Đại Lý
//        142134, // Anh Dũng NB
        258737, // Nguyễn Ngọc Kiên - TP Pháp Lý
        863038, // Nguyễn Đình Phương - Giám Đốc Chi Nhánh
        GasLeave::UID_VIEW_ALL_LEAVE_1, // Lễ Tân - Nguyễn Thị Hạnh
        GasLeave::PHUC_HV, // Tong Giam Sat - Huỳnh Văn Phúc
        289272,// Thân Văn Hiệp
        GasConst::UID_LUYCH,
        GasConst::UID_NHUNG_TTT,
        GasConst::UID_VINH_VT,
        GasConst::UID_HOAN_NH,
        GasConst::UID_PHUONG_NT,
        GasConst::UID_THIEN_DT,
        GasConst::UID_DOT_NV,
        GasConst::UID_TRUONG_NN,
        1163847, // Đặng Hữu Phúc - NV Kế Toán Khu Vực
        138, // Trần Thị Lệ - NV Kế Toán Khu Vực
        850531, // Trần Thị Vân - NV Kế Toán Khu Vực
        627183, // Phan Thị Lệ Quyên - NV Kế Toán Khu Vực
        516143, // Đào Đức Huy
        1298168, // Nguyễn Tấn Nhật
        952191, // Nguyễn Thị Minh
        GasConst::UID_NHAN_NTT,
        GasLeave::THUC_NH,
        740841, // Nguyễn Văn Đạt
        GasSupportCustomer::UID_THUY_MDX,
        GasConst::UID_DUYEN_NT,
        2186484, // Huỳnh Lê Chung
        GasLeave::UID_CHIEF_ACCOUNTANT, //  Kế Toán Trưởng Nguyễn Thị Ngân
        GasLeave::UID_DUONG_NV, // Duong IT
        GasLeave::UID_NAM_NH, // Nam IT
        GasSupportCustomer::UID_THUY_MDX, // Mai Đào Xuân Thủy - KTVP
    );

    public static function model($className = __CLASS__)
    {

        return parent::model($className);
    }

    public function tableName()
    {
        return '{{_gas_schedule_email}}';
    }

    public function rules()
    {
        return array(
            array('id, type, email_template_id, user_id, time_send, created_date', 'safe'),
        );
    }

    public function relations()
    {
        return [];
    }

    public function attributeLabels()
    {
        return [];
    }

    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('t.type', $this->type);
        $criteria->compare('t.email_template_id', $this->email_template_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    /**
     * @Author: DungNT Nov 20, 2014
     * @Todo: build email reset pass to all user have email
     * then insert to table GasScheduleEmail at 1h AM. about 2h AM will send
     */
    public static function BuildListResetPass()
    {
        /* Lấy toàn bộ những user có email và insert vào bảng chờ gửi (GasScheduleEmail)
         * lúc 1h, sau đó 2h sẽ chạy cron send. Cron send sẽ luôn chạy 1 lần every 10 phút
         * nhưng trong hàm send reset pass mình kiểm tra nếu giờ nhỏ hơn 2 thì return luôn
         * vì lúc 1h mới có list reset paass 
         */
        $aUid = self::$UID_NOT_RESET_PASS;// Jan 26,2015 user không cần reset pass
        $aRoleIdNotIn = array(ROLE_E_MAINTAIN, ROLE_CANDIDATE, ROLE_CRAFT_WAREHOUSE, ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_EMPLOYEE_MAINTAIN, ROLE_CUSTOMER, ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT);// Jan 26,2015 user không cần reset pass
        // get list user có mail và active
        $aModelUserMail = Users::GetListUserMail(array('aUidNotReset' => $aUid, 'aRoleIdNotIn' => $aRoleIdNotIn));
        $aRowInsert = [];
        foreach ($aModelUserMail as $model) {
            self::BuildListResetPassGetArrayInsert($model, $aRowInsert);
        }
        $tableName = self::model()->tableName();
        $sql = "insert into $tableName (type,
                    email_template_id,
                    user_id,
                    email
                    ) values " . implode(',', $aRowInsert);
        if (count($aRowInsert) > 0)
            Yii::app()->db->createCommand($sql)->execute();
    }

    // belong to BuildListResetPass
    public static function BuildListResetPassGetArrayInsert($model, &$aRowInsert)
    {
        $type = self::MAIL_RESET_PASS;
        $email_template_id = SendEmail::MAIL_RESET_PASS;
        $aRowInsert[] = "('$type',
                        '$email_template_id', 
                        '$model->id',
                        '$model->email'
                        )";
    }

    /**
     * @Author: DungNT Nov 20, 2014
     * @Todo: run cron every 10 phut send mail reset pass, moi lan send khoang 50 cai
     * se run vao luc 2h sang
     * @Param: $model
     */
    public static function RunCronSendResetPass()
    {
        if (date("H") < 2) return; // OPEN IT ON LIVE
        $from = time();
        $aModelScheduleEmail = [];// for delete all after send mail
        $aIdScheduleEmail = [];// for delete all after send mail
        $aModelUser = []; // for one query to table user
        $aIdUser = [];// for one query to table user
        self::GetInfoUser($aModelScheduleEmail, $aIdScheduleEmail, $aModelUser, $aIdUser, self::MAIL_RESET_PASS);
        if (count($aIdUser) < 1) {
            return;
        }
        foreach ($aModelUser as $model) {
            SendEmail::ResetPasswordModelAndSendMail($model); // OPEN IT ON LIVE
        }
        self::DeleteAllByArrayId($aIdScheduleEmail);
        $to = time();
        $second = $to - $from;
        $ResultRun = "CRON Mail ResetPassByRole: " . count($aIdUser) . ' done in: ' . ($second) . '  Second  <=> ' . round($second / 60, 2) . ' Minutes';
        Logger::WriteLog($ResultRun);
    }

    /**
     * @Author: DungNT Nov 20, 2014
     * @Todo: get list array model user reset
     */
    public static function GetInfoUser(&$aModelScheduleEmail, &$aIdScheduleEmail, &$aModelUser, &$aIdUser, $type)
    {
        $criteria = new CDbCriteria();
        if(!empty($type)){
            $criteria->addCondition('t.type=' . $type);// Jul 10, 2016 Không thể bỏ điều kiện này được
        }
        /** Jul 10, 2016 hiện tại chỗ này sẽ gửi hết các loại mail trong table Schedule 
         * chưa có limit những mail nào gửi theo hẹn giờ
         * @Change_XuLy_HenGioGui Feb 26, 2017 thêm điều kiệm chỉ gửi những email không hẹn giờ hoặc đã quá giờ gửi
         */
        $criteria->addCondition('t.time_send IS NULL OR t.time_send< NOW()');// Feb 26, 2017
        $criteria->order = "t.id ASC";
        $criteria->limit = GasScheduleEmail::MAX_SEND;
        $models = self::model()->findAll($criteria);
        $aModelScheduleEmail = $models;
        $aIdScheduleEmail = CHtml::listData($models, 'id', 'id');
        $aIdUser = CHtml::listData($models, 'user_id', 'user_id');
        $aModelUser = Users::getOnlyArrayModelByArrayId($aIdUser);
    }

    /**
     * @Author: DungNT Nov 19, 2014
     * @Todo: delete all GasScheduleEmail by list id
     */
    public static function DeleteAllByArrayId($aIdScheduleEmail)
    {
        if(count($aIdScheduleEmail) < 1){
            return ;
        }
        $criteria = new CDbCriteria();
//        $criteria->addInCondition("id", $aIdScheduleEmail);
        $sParamsIn = implode(',', $aIdScheduleEmail);
        $criteria->addCondition("id IN ($sParamsIn)");
        GasScheduleEmail::model()->deleteAll($criteria);
    }

    /**
     * @Author: DungNT Nov 20, 2014
     * @Todo: run cron every 10 phut send mail alert khi có văn bản mới cho user nào có mail
     */
    public static function RunCronSendTextAlert(&$CountSend)
    {
        $from = time();
        $aModelScheduleEmail = [];// for delete all after send mail
        $aIdScheduleEmail = [];// for delete all after send mail
        $aModelUser = []; // for one query to table user
        $aIdUser = [];// for one query to table user
        self::GetInfoUser($aModelScheduleEmail, $aIdScheduleEmail, $aModelUser, $aIdUser, self::MAIL_TEXT_ALERT);
        $CountSend = count($aIdUser);
        if ($CountSend < 1) {
            return;
        }
//        $text_id = $aModelScheduleEmail[0]->obj_id;
        $mText = GasText::model()->findByPk($aModelScheduleEmail[0]->obj_id);
        foreach ($aModelUser as $mUser) {
            SendEmail::TextAlert($mUser, $mText); // OPEN IT ON LIVE
        }
        self::DeleteAllByArrayId($aIdScheduleEmail);
        $to = time();
        $second = $to - $from;
        $ResultRun = "CRON Email văn bản mới - Text Alert: " . count($aIdUser) . ' done in: ' . ($second) . '  Second  <=> ' . round($second / 60, 2) . ' Minutes';
        self::WriteLogEmailSend($ResultRun, $aModelUser);
        Logger::WriteLog($ResultRun);
    }

    /**
     * @Author: DungNT Nov 20, 2014
     * @Todo: run cron every 10 phut send mail alert khi có văn bản mới cho user nào có mail
     * @param: $text_id  pk cua van ban moi
     */
    public static function BuildListTextAlert($text_id)
    {
        if(!GasCheck::isServerLive()){
            return ;// Now2918 AnhDung add
        }
        /* Lấy toàn bộ những user có email và insert vào bảng chờ gửi (GasScheduleEmail)
         * sau đó Cron send sẽ luôn chạy 1 lần every 10 phút
         */
        // get list user có mail và active
        $mRole = new Roles();
        $needMore['aRoleIdNotIn'] = $mRole->getRoleNotSendEmail();
        $aModelUserMail = Users::GetListUserMail($needMore);
        $aRowInsert = [];
        foreach ($aModelUserMail as $model) {
            self::BuildListTextAlertGetArrayInsert($model, $aRowInsert, $text_id);
        }
        self::RunSqlInsert($aRowInsert);
    }

    // belong to BuildListTextAlert
    public static function BuildListTextAlertGetArrayInsert($model, &$aRowInsert, $text_id)
    {
        $type = self::MAIL_TEXT_ALERT;
        $email_template_id = SendEmail::MAIL_TEXT_ALERT;
        $aRowInsert[] = "('$type',
                        '$email_template_id', 
                        '$text_id', 
                        '$model->id',
                        '$model->email'
                        )";
    }

    /**
     * @Author: DungNT Dec 30, 2014
     * @Todo: run cron every 10 phut send mail notify user lien quan đến support customer
     * @param: $support_customer_id  pk cua support customer liên quan
     */
    public static function BuildListNotifySupportCustomer($support_customer_id)
    {
        return; // Oct 22, 2015 tắt send mail, User tự lên check
        /* Lấy toàn bộ những user có email và insert vào bảng chờ gửi (GasScheduleEmail)
         * sau đó Cron send sẽ luôn chạy 1 lần every 10 phút
         */
        // get list user có mail và active        
        $aModelUserMail = GasSupportCustomer::GetListModelUserNotify($support_customer_id);
        $aRowInsert = [];
        foreach ($aModelUserMail as $mUser) {
            self::BuildListNotifySupportCustomerGetArrayInsert($mUser, $aRowInsert, $support_customer_id);
        }
        self::RunSqlInsert($aRowInsert);
    }
    
    /**
     * @Author: DungNT Dec 14, 2016
     * @Todo: run cron every 10 phut send mail notify user lien quan đến support customer
     * @param: $mSupportCustomer model
     */
    public static function BuildListNotifySupportCustomerFix($aUid, $mSupportCustomer)
    {
        // get list user có mail và active        
        $aModelUserMail = Users::getOnlyArrayModelByArrayId($aUid);
        $aRowInsert = [];
        foreach ($aModelUserMail as $mUser) {
            self::BuildListNotifySupportCustomerGetArrayInsert($mUser, $aRowInsert, $mSupportCustomer->id);
        }
        self::RunSqlInsert($aRowInsert);
    }

    // belong to BuildListNotifySupportCustomer
    public static function BuildListNotifySupportCustomerGetArrayInsert($mUser, &$aRowInsert, $support_customer_id)
    {
        $type = self::MAIL_SUPPORT_CUSTOMER_CHANGE_STATUS;
        $email_template_id = SendEmail::MAIL_SUPPORT_CUSTOMER_CHANGE_STATUS;
        if (trim($mUser->email) != "") {
            $aRowInsert[] = "('$type',
                            '$email_template_id',
                            '$support_customer_id',
                            '$mUser->id',
                            '$mUser->email'
                            )";
        }
    }

    /**
     * @Author: DungNT Dec 30, 2014
     * @Todo: buid sql insert and run
     * @Param: $aRowInsert array data
     */
    public static function RunSqlInsert($aRowInsert)
    {
        $tableName = GasScheduleEmail::model()->tableName();
        $sql = "insert into $tableName (type,
                    email_template_id,
                    obj_id,
                    user_id,
                    email
                    ) values " . implode(',', $aRowInsert);
        if (count($aRowInsert) > 0)
            Yii::app()->db->createCommand($sql)->execute();
    }
    
    /**
     * @Author: DungNT Sep 19, 2016
     * @Todo: fix buid sql insert and run
     * @Param: $aRowInsert array data
     */
    public static function runSqlInsertForAll($aRowInsert)
    {
        $tableName = GasScheduleEmail::model()->tableName();
        $sql = "insert into $tableName (type,
                    email_template_id,
                    obj_id,
                    user_id,
                    email,
                    subject,
                    body
                    ) values " . implode(',', $aRowInsert);
        if (count($aRowInsert) > 0)
            Yii::app()->db->createCommand($sql)->execute();
    }
    
    /**
     * @Author: DungNT Mar 01, 2017
     * @Todo: fix buid sql insert and run, thêm  biến json để đưa thêm data vào khi run đỡ phải truy vấn lại
     * @Param: $aRowInsert array data
     */
    public static function runSqlInsertFixJson($aRowInsert)
    {
        $tableName = GasScheduleEmail::model()->tableName();
        $sql = "insert into $tableName (type,
                    email_template_id,
                    obj_id,
                    user_id,
                    email,
                    subject,
                    body,
                    json
                    ) values " . implode(',', $aRowInsert);
        if (count($aRowInsert) > 0)
            Yii::app()->db->createCommand($sql)->execute();
    }
    
    /**
     * @Author: DungNT Sep 04, 2017
     * @Todo: fix buid sql insert and run, thêm  biến json để đưa thêm data vào khi run đỡ phải truy vấn lại
     * @Param: $aRowInsert array data
     */
    public static function runSqlInsertFixJsonTimeSend($aRowInsert)
    {
        $tableName = GasScheduleEmail::model()->tableName();
        $sql = "insert into $tableName (type,
                    email_template_id,
                    obj_id,
                    user_id,
                    email,
                    subject,
                    body,
                    time_send,
                    json
                    ) values " . implode(',', $aRowInsert);
        if (count($aRowInsert) > 0)
            Yii::app()->db->createCommand($sql)->execute();
    }

    /**
     * @Author: DungNT Dec 30, 2014
     * @Todo: run cron every 10 phut send mail alert khi có notify
     * hàm này có thể dùng chung cho về sau nữa, chung cho các notify của table khác
     * xử lý của hàm này là lấy tất cả email của 1 type ra rồi send 1 lần
     */
    public static function RunCronSendNotifySupportCustomer(&$CountSend, $type, $info_show)
    {
        try {
        $from = time();
        $aModelScheduleEmail    = [];// for delete all after send mail
        $aIdScheduleEmail       = [];// for delete all after send mail
        $aModelUser             = []; // for one query to table user
        $aIdUser                = [];// for one query to table user
        GasScheduleEmail::GetInfoUser($aModelScheduleEmail, $aIdScheduleEmail, $aModelUser, $aIdUser, '');
        $CountSend = count($aIdUser);
        if ($CountSend < 1) {
            return;
        }
        $sLog = "";// Jul 10, 2016
        $aIdDelete = [];// Jul 10, 2016 vì hàm này sẽ xử lý gửi cho nhiều loại mail, nên có thể sẽ bị delete nhầm id ở $aIdScheduleEmail

        foreach ($aModelScheduleEmail as $key => $mScheduleEmail) {
            switch ($mScheduleEmail->type) {
                case self::MAIL_SUPPORT_CUSTOMER_CHANGE_STATUS:
                    $aIdDelete[] = $mScheduleEmail->id;
                    self::doSend1($mScheduleEmail);
                    $sLog .= " -- $key --- NotifySupportCustomer hỗ trợ KH {$mScheduleEmail->email}";
                    break;
                case self::MAIL_ISSUE_TICKET_CHANGE:
                    $aIdDelete[] = $mScheduleEmail->id;
                    self::doSend2($mScheduleEmail);
                    $sLog .= " -- $key --- NotifyIssueTicket Phản ánh sự việc {$mScheduleEmail->email}";
                    break;
                case self::MAIL_LEAVE_APPROVE:
                    $aIdDelete[] = $mScheduleEmail->id;
                    self::doSend3($mScheduleEmail);
                    $sLog .= "  -- $key --- Notify Leave Approve Nghỉ Phép {$mScheduleEmail->email}";
                    break;
                case self::MAIL_NEW_CUSTOMER:
                    $aIdDelete[] = $mScheduleEmail->id;
                    self::doSend4($mScheduleEmail);
                    $sLog .= "  -- $key --- Notify New customer {$mScheduleEmail->email}";
                    break;
                case self::MAIL_SETTLE_ALERT:
                    $aIdDelete[] = $mScheduleEmail->id;
                    self::doSend5($mScheduleEmail);
                    $sLog .= "  -- $key --- Notify Settle Alert {$mScheduleEmail->email}";
                    break;
                case self::MAIL_PHAP_LY_UPDATE:
                    $aIdDelete[] = $mScheduleEmail->id;
                    CmsEmail::sendmailReadyContent($mScheduleEmail);
                    $sLog .= "  -- $key --- Notify Pháp Lý Alert {$mScheduleEmail->email}";
                    break;
                case self::MAIL_STATUS_CUSTOMER_SPECIAL:
                case self::MAIL_STATUS_PRICE_REQUEST:
                case self::MAIL_QUOTES:
                case self::MAIL_BUG_TO_DEV:
                case self::MAIL_MEMBER_NEW:
                case self::MAIL_MEMBER_LEAVE:
                case self::MAIL_EMPLOYEE_NEW:
                //++ TSK0206-SPJ (NguyenPT 20180117) Handle notify email
                case self::MAIL_NOTIFY_INTERVIEW:
                case self::MAIL_NOTIFY_INTERVIEW_CANDIDATE:
                case self::MAIL_NOTIFY_HR_PLAN_APPROVER:
                //-- TSK0206-SPJ (NguyenPT 20180117) Handle notify email
                    $aIdDelete[] = $mScheduleEmail->id;
                    CmsEmail::sendmailReadyContent($mScheduleEmail);
                    $sLog .= "  -- $key --- Notify Type: ($mScheduleEmail->type) -- {$mScheduleEmail->email}";
                    break;
                default:
                    break;
            }
        }

        GasScheduleEmail::DeleteAllByArrayId($aIdDelete);
        $to = time();
        $second = $to - $from;
        $ResultRun = "CRON send mail: " . count($aIdUser) . ' done in: ' . ($second) . '  Second  <=> ' . round($second / 60, 2) . ' Minutes '. $sLog;
//        self::WriteLogEmailSend($ResultRun, $aModelUser);// Close on Jul 10, 2016
        Logger::WriteLog($ResultRun);
        } catch (Exception $exc) {
            $ResultRun = "ERROR CRON send mail NEEX FIX gấp: ".$exc->getMessage();
            Logger::WriteLog($ResultRun);
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * @Author: DungNT Apr 12, 2016
     * @Todo: xử lý send mail cho các type
     * @Param: $mScheduleEmail model $mScheduleEmail
     */
    public static function doSend1($mScheduleEmail)
    {
        $mSupportCustomer = GasSupportCustomer::model()->findByPk($mScheduleEmail->obj_id);
        $mUser = Users::model()->findByPk($mScheduleEmail->user_id);
        if ($mSupportCustomer) {
            SendEmail::SupportCustomerChangeStatus($mUser, $mSupportCustomer);
        }
    }

    public static function doSend2($mScheduleEmail)
    {
        $mGasIssueTicketsDetail = GasIssueTicketsDetail::model()->findByPk($mScheduleEmail->obj_id);
        $mUser = Users::model()->findByPk($mScheduleEmail->user_id);
        if ($mGasIssueTicketsDetail) {
            SendEmail::IssueTicketReply($mUser, $mGasIssueTicketsDetail);
        }
    }

    // Feb 26, 2015 Fix không cho send mail ngay khi tạo, phải luôn vào schedule
    public static function doSend3($mScheduleEmail)
    {
        $mGasLeave = GasLeave::model()->findByPk($mScheduleEmail->obj_id);
        $mUser = Users::model()->findByPk($mScheduleEmail->user_id);
        if ($mGasLeave) {
            SendEmail::LeaveAlertSend($mUser, $mGasLeave);
        }
    }

    // Apr 12, 2016 new customer notify dieu phoi
    public static function doSend4($mScheduleEmail)
    {
        $mCustomer = Users::model()->findByPk($mScheduleEmail->obj_id);
        $mUser = Users::model()->findByPk($mScheduleEmail->user_id);
        if ($mCustomer) {
            SendEmail::NewCustomer($mUser, $mCustomer);
        }
    }
    
    // Jul 10, 2016 settle alert user
    public static function doSend5($mScheduleEmail)
    {
        $mGasSettle = GasSettle::model()->findByPk($mScheduleEmail->obj_id);
        $mUser = Users::model()->findByPk($mScheduleEmail->user_id);
        if ($mGasSettle) {
            SendEmail::SettleAlert($mUser, $mGasSettle);
        }
    }

    /*****  Feb 25, 2015 ISSUE TICKET *****/
    /**
     * @Author: DungNT Feb 25, 2015
     * @Todo: run cron every 10 phut send mail notify user lien quan đến ISSUE TICKET
     * @param: $support_customer_id  pk cua support customer liên quan
     */
    public static function BuildListNotifyIssueTicket($issue_tickets_detail_id)
    {
//        return ; // only for debug Jan 27, 2016
        /* Lấy toàn bộ những user có email và insert vào bảng chờ gửi (GasScheduleEmail)
         * sau đó Cron send sẽ luôn chạy 1 lần every 10 phút
         */
        // get list user có mail và active        
        $aModelUserMail = GasIssueTickets::GetListModelUserNotify($issue_tickets_detail_id);
        $aRowInsert = [];
        foreach ($aModelUserMail as $mUser) {
            self::BuildListNotifyIssueTicketGetArrayInsert($mUser, $aRowInsert, $issue_tickets_detail_id);
        }
        GasScheduleEmail::RunSqlInsert($aRowInsert);
    }

    // belong to BuildListNotifySupportCustomer
    public static function BuildListNotifyIssueTicketGetArrayInsert($mUser, &$aRowInsert, $issue_tickets_detail_id)
    {
        $type = self::MAIL_ISSUE_TICKET_CHANGE;
        $email_template_id = SendEmail::MAIL_ISSUE_TICKET_CHANGE;
        if ($mUser && trim($mUser->email) != "") {
            $aRowInsert[] = "('$type',
                            '$email_template_id',
                            '$issue_tickets_detail_id',
                            '$mUser->id',
                            '$mUser->email'
                            )";
        }
    }

    /*****  Feb 25, 2015 ISSUE TICKET *****/

    /*****  Feb 26, 2015 BUG - RESOLVE - ALL EMAIL WILL PUT TO TABLE SCHEDULE *****/
    /*****  Feb 26, 2015 LEAVE - NGHI PHEP *****/
    /**
     * @Author: DungNT Feb 26, 2015
     * @Todo: run cron every 5 phut (20 mail) send mail notify user lien quan đến leave approve
     * Hôm nay bị bug, khi gmail send 1000 mail thì bị limit trong 1 ngày, không send dc nữa
     * nên những email gửi ngay sẽ văng ra lỗi và không gửi được => Solution: sẽ đưa vào scheule hết
     * @param: $model  model GasLeave
     */
    public static function BuildListNotifyLeaveApprove($model, $aUid)
    {
        /* Lấy toàn bộ những user có email và insert vào bảng chờ gửi (GasScheduleEmail)
         * sau đó Cron send sẽ luôn chạy 1 lần every 10 phút
         */
        // get list user có mail và active
        $aRowInsert = [];
        $aModelUserMail = Users::getOnlyArrayModelByArrayId($aUid);
        if (count($aModelUserMail)) {
//       Apr 07, 2015 bỏ đoạn check này vì check nhiều quá
//            if( in_array($mUserApproved->role_id, GasLeave::$ROLE_APPROVE_LEVEL_1) ){
//                MyFormat::AddEmailAnhDung($aModelUserMail);// only for test
            foreach ($aModelUserMail as $mUser) {
                self::BuildListNotifyLeaveApproveGetArrayInsert($mUser, $aRowInsert, $model->id);
            }
            GasScheduleEmail::RunSqlInsert($aRowInsert);
//            }Apr 07, 2015 bỏ đoạn check này vì check nhiều quá
        }
    }

    // belong to BuildListNotifySupportCustomer
    public static function BuildListNotifyLeaveApproveGetArrayInsert($mUser, &$aRowInsert, $leave_id)
    {
        $type = self::MAIL_LEAVE_APPROVE;
        $email_template_id = SendEmail::MAIL_LEAVE_ALERT;
        if (trim($mUser->email) != "") {
            $aRowInsert[] = "('$type',
                            '$email_template_id',
                            '$leave_id',
                            '$mUser->id',
                            '$mUser->email'
                            )";
        }
    }

    /*****  Feb 26, 2015 LEAVE - NGHI PHEP *****/

    /*****  Apr 12, 2016 NOTIFY NEW CUSTOMER -  *****/
    /**
     * @Author: DungNT Apr 12, 2016
     * @Todo: khi sale or ccs tao moi KH thi se notify cho Dieu pHoi + 2 nv ke toan
     * @param: $model  model User
     */
    public static function BuildListNotifyNewCustomer($model, $needMore = [])
    {
//        $aZoneNotHcm = array_merge(GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TAY, GasOrders::$PROVINCE_ZONE_MIEN_TRUNG);
        $aZoneNotHcm = GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TAY;// Apr 03, 2017 có gửi cho các tỉnh miền Trung - Nhị
//        if (in_array($model->province_id, $aZoneNotHcm) || in_array($model->is_maintain, $aTypeHgd)) {
        if (in_array($model->is_maintain, CmsFormatter::$aTypeIdHgd)) {//  Jun 11, 2016 không notify với KH hộ GĐ
            return;
        }// Apr 13, 2016 chương và KT chỉ theo dõi TP HCM + Đồng Nai + BD

        /* Lấy toàn bộ những user có email và insert vào bảng chờ gửi (GasScheduleEmail)
         * sau đó Cron send sẽ luôn chạy 1 lần every 10 phút
         */
        // get list user có mail và active
        $aRowInsert = [];
        $aUid = [];
        // Apr 22, 2016 Khi KH lấy hàng lần đầu thì mới notify cho NV phòng kế toán 
        if (in_array($model->province_id, GasOrders::$PROVINCE_ZONE_MIEN_TRUNG) && isset($needMore['notify_accounting']) ) {
                $aUid[GasConst::UID_NHI_DT]     = GasConst::UID_NHI_DT;
                $aUid[GasLeave::THUC_NH]        = GasLeave::THUC_NH;
        }elseif(in_array($model->province_id, GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TAY) && isset($needMore['notify_accounting'])) {
//            $aUid[GasConst::UID_THUY_HT] = GasConst::UID_THUY_HT;
            $aUid[GasLeave::THUC_NH]            = GasLeave::THUC_NH;
        }elseif(in_array($model->province_id, GasOrders::$TARGET_ZONE_PROVINCE_TAY_NGUYEN) && isset($needMore['notify_accounting'])) {
            $aUid[GasLeave::UID_VIEW_ALL_TAY_NGUYEN_GIA_LAI] = GasLeave::UID_VIEW_ALL_TAY_NGUYEN_GIA_LAI;
            $aUid[GasLeave::THUC_NH]            = GasLeave::THUC_NH;
        }elseif ($model->is_maintain == STORE_CARD_KH_BINH_BO && isset($needMore['notify_accounting'])) {
            $aUid[GasLeave::THUC_NH]            = GasLeave::THUC_NH;
            $aUid[GasSupportCustomer::UID_QUY_PV] = GasSupportCustomer::UID_QUY_PV;
        } elseif ($model->is_maintain == STORE_CARD_KH_MOI && isset($needMore['notify_accounting'])) {
            $aUid[GasLeave::THUC_NH]            = GasLeave::THUC_NH;
            $aUid[GasSupportCustomer::UID_QUY_PV] = GasSupportCustomer::UID_QUY_PV;
        } else {
            if(!empty($model->sale_id)){
                $aUid[$model->sale_id] = $model->sale_id;// notify cho Sale
            }
        }
        $aModelUserMail = Users::getOnlyArrayModelByArrayId($aUid);
        foreach ($aModelUserMail as $mUser) {
            self::BuildListNotifyNewCustomerGetArrayInsert($mUser, $aRowInsert, $model->id);
        }
        GasScheduleEmail::RunSqlInsert($aRowInsert);
    }

    // belong to BuildListNotifySupportCustomer
    public static function BuildListNotifyNewCustomerGetArrayInsert($mUser, &$aRowInsert, $customer_id)
    {
        $type = self::MAIL_NEW_CUSTOMER;
        $email_template_id = SendEmail::MAIL_ID_NEW_CUSTOMER;
        if (trim($mUser->email) != "") {
            $aRowInsert[] = "('$type',
                            '$email_template_id',
                            '$customer_id',
                            '$mUser->id',
                            '$mUser->email'
                            )";
        }
    }


    /*****  Jul 10, 2016 NOTIFY settle alert-  *****/
    /**
     * @Author: DungNT Jul 10, 2016
     * @Todo: khi có thay đổi gì về quyết toán thì mail cho user quản lý hoặc user tạo quyết toán
     * @param: $model  model GasSettle
     * @note: 1: khi tạo mới 
     *  2: khi update status thì gửi cho user cấp trên
     */
    public static function BuildListNotifySettle($model, $aUid, $needMore=[]) {
        /* Lấy toàn bộ những user có email và insert vào bảng chờ gửi (GasScheduleEmail)
         * sau đó Cron send sẽ luôn chạy 1 lần every 10 phút
         */
        // get list user có mail và active
        $aRowInsert=[];
        $aModelUserMail = Users::getOnlyArrayModelByArrayId($aUid);
        foreach($aModelUserMail as $mUser){
            self::BuildListNotifySettleGetArrayInsert($mUser, $aRowInsert, $model->id);
        }
        GasScheduleEmail::RunSqlInsert($aRowInsert);
    }
    
    // belong to BuildListNotifySupportCustomer
    public static function BuildListNotifySettleGetArrayInsert($mUser, &$aRowInsert, $settle_id)
    {
        $type = self::MAIL_SETTLE_ALERT;
        $email_template_id = SendEmail::MAIL_ID_SETTLE_ALERT;
        if (trim($mUser->email) != "") {
            $aRowInsert[] = "('$type',
                            '$email_template_id',
                            '$settle_id',
                            '$mUser->id',
                            '$mUser->email'
                            )";
        }
    }
    

    /**
     * @Author: DungNT Sep 16, 2015
     * @Todo: write email send to log
     * @Param: $model
     */
    public static function WriteLogEmailSend(&$ResultRun, $aModelUser)
    {
        $sEmail = "";
        foreach ($aModelUser as $mUser) {
            $sEmail .= " --- $mUser->email";
        }
        $ResultRun .= "<br> $sEmail";
    }


    /**
     * @Author: DungNT Sep 18, 2015
     * @Todo: writelog khi build list send mail
     * @Param: $info: text xác định đang chạy ở function nào, $aModelUserMail
     */
    public static function WritelogDebugBuildMail($info, $aModelUserMail)
    {
        GasScheduleEmail::WriteLogEmailSend($info, $aModelUserMail);
        Logger::WriteLog($info);
    }

    /**
     * Collect data from shell return model to insert email sending schedule.
     * Only item has sale id will be sent
     * Only sale person will be received the email
     * @Author: TRUNG June 23 2016
     * @param $shellReturnModels GasShellReturn[] list record need to send email
     */
    public static function makeScheduleSendShellReturn($shellReturnModels)
    {
        $aRowInsert = [];
        /** @var GasShellReturn $model each record need to check */
        foreach ($shellReturnModels as $model) {
            // Check exist sale id
            if (!empty($model) && !empty($model->rSale) && !empty($model->rSale->email) && $model->forecast > 0) {
                /** @var string $email email of sale of customer */
                $email = $model->rSale->email;

                $type = self::MAIL_SHELL_RETURN_PLANNING;
                $email_template_id = SendEmail::MAIL_ID_SHELL_RETURN_PLANING;
                $aRowInsert[] = "('$type',
                            '$email_template_id',
                            '$model->id',
                            '$model->rSale->id',
                            '$model->rSale->email'
                            )";
            }
        }

        // Check and insert to schedule email stack
        if (count($aRowInsert) > 0) {
            $tableName = GasScheduleEmail::model()->tableName();
            $sql = "insert into $tableName (type,
                    email_template_id,
                    user_id,
                    email
                    ) values " . implode(',', $aRowInsert);
            Yii::app()->db->createCommand($sql)->execute();
        }
    }
    
    
    /**
     * @Author: DungNT Sep 16, 2016
     * @Todo: khi có hồ sơ pháp lý change hoặc create new thì notify cho DungKT
     * @param: $model  model GasLeave
     */
    public static function BuildListNotifyPhapLy($model)
    {
        // get list user có mail và active
        $aRowInsert = [];
        $aUid       = array(GasLeave::DUNG_NTT, GasLeave::PHUONG_PTK);
//        $aUid       = array(GasConst::UID_DUNG_NT);
        $aModelUserMail = Users::getOnlyArrayModelByArrayId($aUid);
        $type = self::MAIL_PHAP_LY_UPDATE;
        $email_template_id = SendEmail::MAIL_PHAP_LY_UPDATE;
        foreach ($aModelUserMail as $mUser) {
            $sSubject = $sBody = '';
            GasScheduleEmail::phapLyGetContent($mUser, $model, $sSubject, $sBody);
            $sSubject   = MyFormat::escapeValues($sSubject);
            $sBody      = MyFormat::escapeValues($sBody);
            GasScheduleEmail::buildArrayInsertForAll($mUser, $aRowInsert, $type, $email_template_id, $model->id, $sSubject, $sBody);
        }
        // Feb0118 add email send for ktkv
        $mUser = new Users();
        $mUser->id      = 2;
        $mUser->email   = 'ketoan.kv@spj.vn';
        GasScheduleEmail::buildArrayInsertForAll($mUser, $aRowInsert, $type, $email_template_id, $model->id, $sSubject, $sBody);
        // Aug0219 add email send for Ke Toan Van Phong -- need fix this code
        $mUser->email   = 'ketoan.vanphong@spj.vn';
        GasScheduleEmail::buildArrayInsertForAll($mUser, $aRowInsert, $type, $email_template_id, $model->id, $sSubject, $sBody);
        
        GasScheduleEmail::runSqlInsertForAll($aRowInsert);
    }
    
    public static function buildArrayInsertForAll($mUser, &$aRowInsert, 
            $type, $email_template_id, $obj_id, $subject, $body)
    {
        if (trim($mUser->email) != '') {
            $aRowInsert[] = "('$type',
                            '$email_template_id',
                            '$obj_id',
                            '$mUser->id',
                            '$mUser->email',
                            '$subject',
                            '$body'
                            )";
        }
    }
    
    /**
     * @Author: DungNT Mar 01, 2017
     * @Todo: fix sql insert thêm biến json vào
     */
    public static function buildArrayInsertFixJson($user_id, &$aRowInsert, 
            $type, $email_template_id, $obj_id, $subject, $body, $json, $email)
    {
        if (trim($email) != '') {
            $aRowInsert[] = "('$type',
                            '$email_template_id',
                            '$obj_id',
                            '$user_id',
                            '$email',
                            '$subject',
                            '$body',
                            '$json'
                            )";
        }
    }
    
    public static function phapLyGetContent($mUser, $model, &$sSubject, &$sBody) {
        // DuongNV Sep0419 add detail
        $mAgent             = empty($model->rAgent) ? '' : $model->rAgent;
        $province           = '';
        $detail             = '';
        $aCompany           = Borrow::model()->getListdataCompany();
        if($mAgent){
            $province       = empty($mAgent->getProvince()) ? '' : ' - Khu vực: '.$mAgent->getProvince().'<br>';
            $detail        .= empty($aCompany[$mAgent->parent_id]) ? '' : 'Tên Công ty: '.$aCompany[$mAgent->parent_id].'<br>';
            $mBranch        = empty($mAgent->sale_id) ? '' : Users::model()->findByPk($mAgent->sale_id);
            if($mAgent->gender == BranchCompany::IS_BRANCH){
                $detail    .= empty($mBranch) ? '' : 'Tên chi nhánh: '.$mBranch->first_name.'<br>';
                $detail    .= empty($mBranch) ? '' : 'Chi nhánh số: '.$mBranch->code_account.'<br>';
                $mSonBranch = Users::model()->findByPk($mBranch->storehouse_id);
                $detail    .= empty($mSonBranch) ? '' : 'Địa điểm thuộc Chi nhánh: '.$mSonBranch->first_name.'<br>';
            }
            if($mAgent->gender == BranchCompany::IS_ADDRESS){
                $detail    .= empty($mBranch) ? '' : 'Tên địa điểm: '.$mBranch->first_name.'<br>';
                $detail    .= empty($mBranch) ? '' : 'Địa điểm KD số: '.$mBranch->code_account.'<br>';
            }
        }
        
        $TITLE = "{$model->getAgent()} - {$model->getName()}";
        $aBody = array(
//            '{NAME}'        => $mUser->getFullName(),
            '{NAME}'        => 'Bạn',
//            '{AGENT_NAME}'  => $model->getAgent(),
            '{AGENT_NAME}'  => $model->getAgent().$province,
            '{PROFILE_NAME}' => $model->getName(),
//            '{PROFILE_CODE}' => $model->code_no,
            '{PROFILE_CODE}' => $model->code_no.'<br>'.$detail,
            '{PROFILE_NOTE}' => $model->note,
            '{LINK}'        => "<a href='{$model->getUrlView()}' target='_blank'>{$model->getUrlView()}</a>",
        );
        $aSubject = array(
            '{TITLE}' => $TITLE,
        );
        self::getContentEmail(SendEmail::MAIL_PHAP_LY_UPDATE, $aSubject, $aBody, $sSubject, $sBody);
    }
    
    /**
     * @Author: DungNT Sep 19, 2016
     * @Todo: get subject and content of an email
     * @Param: 
     */
    public static function getContentEmail($iEmailTemplateID, $aSubject, $aBody, &$sSubject, &$sBody) {
        $modelEmailTemplate = EmailTemplates::model()->findByPk($iEmailTemplateID);

        $sSubject = $modelEmailTemplate->email_subject;        
        if(is_array($aSubject) && count($aSubject)>0){
            foreach($aSubject as $key => $value)
            {
                $sSubject = str_replace($key, $value, $sSubject);
            }
        }

        $sBody = $modelEmailTemplate->email_body;
        if(is_array($aBody) && count($aBody)>0){
            foreach($aBody as $key => $value)
            {
                $sBody = str_replace($key, $value, $sBody);
            }
        }
    }
    
    /**
     * @Author: DungNT Feb 28, 2017
     * @Todo: Fix get subject and content of an email, khi insert nhiều thì không nên find email template ở đây được
     * có thể có 10 ngàn email mà đưa vào vòng for để find thì không thể được
     */
    public static function getContentEmailFixQuery($modelEmailTemplate, $aSubject, $aBody, &$sSubject, &$sBody) {
        $sSubject = $modelEmailTemplate->email_subject;        
        if(is_array($aSubject) && count($aSubject)>0){
            foreach($aSubject as $key => $value)
            {
                $sSubject = str_replace($key, $value, $sSubject);
            }
        }

        $sBody = $modelEmailTemplate->email_body;
        if(is_array($aBody) && count($aBody)>0){
            foreach($aBody as $key => $value)
            {
                $sBody = str_replace($key, $value, $sBody);
            }
        }
    }
    
    /**
     * @Author: DungNT Oct 07, 2016
     * @Todo: new or change status of customer special
     * @param: $model  model CustomerSpecial
     */
    public static function BuildNotifyCustomerSpecial($model, $needMore = [])
    {
        $sale_id = $model->sale_id;
        if($sale_id == GasLeave::KH_CONGTY_BO){
            $sale_id = GasLeave::UID_HEAD_GAS_BO;
        }elseif($sale_id == GasLeave::KH_CONGTY_MOI){
            $sale_id = GasLeave::UID_HEAD_GAS_MOI;
        }
        // get list user có mail và active
        $aRowInsert = [];
        $aUid       = array($sale_id);
        if(isset($needMore['aUid'])){
            $aUid = $needMore['aUid'];
        }
        if(isset($needMore['aUidMerge'])){
            $aUid = array_merge($aUid, $needMore['aUidMerge']);
        }
//        $aUid [] = GasLeave::UID_DUNG_NT;
        
        $aModelUserMail     = Users::getOnlyArrayModelByArrayId($aUid);
        $type               = self::MAIL_STATUS_CUSTOMER_SPECIAL;
        $email_template_id  = SendEmail::MAIL_STATUS_CUSTOMER_SPECIAL;
        foreach ($aModelUserMail as $mUser) {
            $sSubject = $sBody = '';
            GasScheduleEmail::customerSpecialGetContent($mUser, $model, $sSubject, $sBody);
            $sSubject   = MyFormat::escapeValues($sSubject);
            $sBody      = MyFormat::escapeValues($sBody);
            self::buildArrayInsertForAll($mUser, $aRowInsert, $type, $email_template_id, $model->id, $sSubject, $sBody);
        }
        self::runSqlInsertForAll($aRowInsert);
    }
    public static function customerSpecialGetContent($mUser, $model, &$sSubject, &$sBody) {
        $aBody = array(
            '{NAME}'            => $mUser->getFullName(),
            '{TYPE}'            => $model->getTypeText(),
            '{CUSTOMER_NAME}'   => $model->getCustomer('first_name'),
            '{CUSTOMER_ADDRESS}'=> $model->getCustomer('address'),
            '{SALE_NAME}'       => $model->getSale(),
            '{STATUS}'          => $model->getStatusText(),
            '{MESSAGE}'         => $model->getReason(),
        );
        $aSubject = array(
            '{CUSTOMER_NAME}'   => $model->getCustomer('first_name'),
        );
        self::getContentEmail(SendEmail::MAIL_STATUS_CUSTOMER_SPECIAL, $aSubject, $aBody, $sSubject, $sBody);
    }
    
    /**
     * @Author: DungNT Dec 26, 2016
     * @Todo: new or change status of customer special
     * @param: $model  model UsersPriceRequest
     */
    public static function buildNotifyPriceRequest($model, $needMore = [])
    {
        $model      = UsersPriceRequest::model()->findByPk($model->id);
        $aUid       = array($model->uid_login);
        $aRowInsert = [];
        if(isset($needMore['aUidMerge'])){
            $aUid = array_merge($aUid, $needMore['aUidMerge']);
        }
//        $aUid [] = GasLeave::UID_DUNG_NT;

        $aModelUserMail     = Users::getOnlyArrayModelByArrayId($aUid);
        $type               = self::MAIL_STATUS_PRICE_REQUEST;
        $email_template_id  = SendEmail::MAIL_STATUS_PRICE_REQUEST;
        foreach ($aModelUserMail as $mUser) {
            $sSubject = $sBody = '';
            self::priceRequestGetContent($mUser, $model, $sSubject, $sBody, $type, $email_template_id);
            $sSubject   = MyFormat::escapeValues($sSubject);
            $sBody      = MyFormat::escapeValues($sBody);
            self::buildArrayInsertForAll($mUser, $aRowInsert, $type, $email_template_id, $model->id, $sSubject, $sBody);
        }
        self::runSqlInsertForAll($aRowInsert);
    }
    public static function priceRequestGetContent($mUser, $model, &$sSubject, &$sBody, $type, $email_template_id) {
        $aBody = array(
            '{NAME}'            => $mUser->getFullName(),
            '{INFO}'            => $model->getDetailInfo(),
            '{CREATED_BY}'      => $model->getUidLogin(),
            '{STATUS}'          => $model->getStatusText(),
        );
        $aSubject = array(
            '{NAME}'            => $mUser->getFullName(),
            '{CREATED_BY}'      => $model->getUidLogin(),
            '{STATUS}'          => $model->getStatusText(),
        );
        self::getContentEmail($email_template_id, $aSubject, $aBody, $sSubject, $sBody);
    }
    
    /**
     * @Author: DungNT Feb 26, 2017
     * @Todo: send mail báo giá cho KH bò mối
     * @param: $model  model UsersPriceRequest
     */
    public static function makeQuotes($aCustomer, $needMore = [])
    {
        $type               = self::MAIL_QUOTES;
        $email_template_id  = SendEmail::MAIL_QUOTES;
        $modelEmailTemplate = EmailTemplates::model()->findByPk($email_template_id);
        $aRowInsert = $aErrors = [];
        $aCompanyAllow = [GasConst::COMPANY_DKMN, GasConst::COMPANY_HUONGMINH];
        $mAppCache      = new AppCache();
        $aCompany       = $mAppCache->getUserByRole(ROLE_COMPANY);
        
        foreach ($aCustomer as $mUser) {
//            if(!in_array($mUser->storehouse_id, $aCompanyAllow)){
//                $aErrors[] = $mUser->id;
//                continue ;
//            }// có mail nhưng chưa có công ty thì sẽ đưa hết vào cty DKMN
            if(!in_array($mUser->storehouse_id, $aCompanyAllow)){
                $mUser->storehouse_id = GasConst::COMPANY_DKMN;
            }
            $sSubject = $sBody = '';
            $aInfoCompany = isset($aCompany[$mUser->storehouse_id]) ? $aCompany[$mUser->storehouse_id] : $aCompany[GasConst::COMPANY_DKMN];
            
            self::makeQuotesGetContent($aInfoCompany, $mUser, $sSubject, $sBody, $type, $modelEmailTemplate);
            $sSubject   = MyFormat::escapeValues($sSubject);
            $sBody      = MyFormat::escapeValues($sBody);
            
            $mUser->setQuotesPdfName();
            $json = ['pdf_filename' => $mUser->pdfPathAndName];
            $json = MyFormat::jsonEncode($json);
            $temp = $mUser->getUserRefField('contact_boss_mail');
            $aEmail = explode(';', $temp);
            foreach($aEmail as $email){
                self::buildArrayInsertFixJson($mUser->id, $aRowInsert, $type, $email_template_id, 0, $sSubject, $sBody, $json, $email);
            }
        }
        
        self::runSqlInsertFixJson($aRowInsert);
        SendEmail::bugToDev(count($aRowInsert)." record -- Debug dev makeQuotes Email OK, theo dõi hàm send mail báo giá xem có bị lỗi gì không. ".date('Y-m-d H:i:s'));
        if(count($aErrors)){
            $info = "Lỗi gửi báo giá, những công ty của KH sau chưa được setup để gửi báo giá:  ". implode(', ', $aErrors);
            Logger::WriteLog($info);
            SendEmail::bugToDev($info);
        }
        
    }
    public static function makeQuotesGetContent($aInfoCompany, $mUser, &$sSubject, &$sBody, $type, $modelEmailTemplate) {
        $customerName = $mUser->getFullNameTruSoChinh();
//        $mUser->loadExtPhone2(); dùng cho biến $mUser->phone_first
        
        $aBody = array(
            '{COMPANY_NAME}'        => $aInfoCompany['first_name'],
            '{COMPANY_ADD}'         => $aInfoCompany['address'],
            '{COMPANY_PHONE}'       => $aInfoCompany['phone'],
            '{COMPANY_FAX}'         => $aInfoCompany['code_account'],// set tạm code_account là fax cho loại user là Công ty
            '{COMPANY_SIGN}'        => '',
            
            '{CUSTOMER_NAME}'       => $customerName,
            '{CUSTOMER_TEL}'        => $mUser->phone_first,
            '{CUSTOMER_FAX}'        => $mUser->getUserRefField('contact_boss_fax'),
            '{QUOTES_BEGIN}'        => '01/'.date('m/Y'),
            '{QUOTES_END}'          => date('t/m/Y'),
            '{QUOTES_MONTH}'        => date('m/Y'),
        );
        $aSubject = array(
            '{COMPANY_NAME}'        => $aInfoCompany['first_name'],
            '{QUOTES_MONTH}'        => date('m/Y'),
        );
        self::getContentEmailFixQuery($modelEmailTemplate, $aSubject, $aBody, $sSubject, $sBody);
    }
    
    
    /** @Author: DungNT Aug 16, 2017
     *  @Todo: build mail info or bug to dev
     */
    public static function buildNotifyDev($aMailDev, $aSubject, $aBody, $needMore = [])
    {
        $aRowInsert         = [];
        $type               = self::MAIL_BUG_TO_DEV;
        $email_template_id  = SendEmail::MAIL_DEV_ERRORS;
        $sSubject = $sBody = $json = '';
        self::getContentEmail($email_template_id, $aSubject, $aBody, $sSubject, $sBody);
        $sSubject   = MyFormat::escapeValues($sSubject);
        $sBody      = MyFormat::escapeValues($sBody);
        $time_send  = date('Y-m-d H:i:s');
        if(isset($needMore['time_send'])){
            $time_send  = $needMore['time_send'];
        }
        if(isset($needMore['json_data'])){
            $json = $needMore['json_data'];
        }

        foreach ($aMailDev as $email) {
            $aRowInsert[] = "('$type',
                            '$email_template_id',
                            '0',
                            '0',
                            '$email',
                            '$sSubject',
                            '$sBody',
                            '$time_send',
                            '$json'
                            )";
        }
//        self::runSqlInsertForAll($aRowInsert);
        self::runSqlInsertFixJsonTimeSend($aRowInsert);
    }
 
    /** @Author: DungNT Aug2017
     * @Todo: new account or change pass member
     * @param: $model  model Users
     */
    public static function memberNew($mUser, $needMore = []){
        $type               = self::MAIL_MEMBER_NEW;
        $email_template_id  = SendEmail::MAIL_MEMBER_NEW;
        $sSubject = $sBody = ''; $aRowInsert=[];
        $aBody = array(
            '{NAME}'        => $mUser->getFullName(),
            '{USERNAME}'    => $mUser->username,
            '{PASSWORD}'    => $mUser->temp_password,
        );
        $aSubject = array(
            '{NAME}'        => $mUser->getFullName(),
        );
        self::getContentEmail($email_template_id, $aSubject, $aBody, $sSubject, $sBody);
        $sSubject   = MyFormat::escapeValues($sSubject);
        $sBody      = MyFormat::escapeValues($sBody);
        self::buildArrayInsertForAll($mUser, $aRowInsert, $type, $email_template_id, $mUser->id, $sSubject, $sBody);
        self::runSqlInsertForAll($aRowInsert);
    }
    
    /** @Author: DungNT Aug2017
     * @Todo: member LEAVE - nhân viên nghỉ việc 
     * @param: $model model Users
     */
    public static function memberLeave($mUser, $needMore = []){
        return ; // Jun2918 Tạm close lại sẽ mở lại trong giữa tháng 10 - reOpen On Oct1117
        $aRoleNotSend = [ROLE_EMPLOYEE_MARKET_DEVELOPMENT];
        if(in_array($mUser->role_id, $aRoleNotSend)){
            return ;
        }
        $mUser = Users::model()->findByPk($mUser->id);
        $type               = self::MAIL_MEMBER_LEAVE;
        $email_template_id  = SendEmail::MAIL_MEMBER_LEAVE;
        $sSubject = $sBody = ''; $aRowInsert=[];
        $email = 'everyone@spj.vn';
        $aBody = array(
            '{LEAVE_NAME}'          => $mUser->getFullName(),
            '{LEAVE_POSITION}'      => Roles::GetRoleNameById($mUser->role_id),
            '{LEAVE_DATE}'          => $mUser->rUsersProfile->getLeaveDate(),
            '{AGENT_NAME}'          => $mUser->getParentCacheSession(),
        );
        $aSubject = array(
            '{LEAVE_NAME}'          => $mUser->getFullName(),
        );
        self::getContentEmail($email_template_id, $aSubject, $aBody, $sSubject, $sBody);
        $sSubject   = MyFormat::escapeValues($sSubject);
        $sBody      = MyFormat::escapeValues($sBody);
        self::buildArrayInsertFixJson($mUser->id, $aRowInsert, $type, $email_template_id, $mUser->id, $sSubject, $sBody, '', $email);
        self::runSqlInsertFixJson($aRowInsert);
    }
    
    /** @Author: DungNT Oct292017
     * @Todo: thông báo nhân viên mới
     * @param: $model model Users
     */
    public static function employeeNew($mUser, $needMore = []){
//        return ; // Sep1417 Tạm close lại sẽ mở lại trong giữa tháng 10 - reOpen On Oct1117
        $aRoleNotEmail = [ROLE_CANDIDATE, ROLE_PARTNER];
        if(!GasCheck::isServerLive() || in_array($mUser->role_id, $aRoleNotEmail)){
            return ;
        }
        $mUser = Users::model()->findByPk($mUser->id);
        $type               = self::MAIL_EMPLOYEE_NEW;
        $email_template_id  = SendEmail::MAIL_EMPLOYEE_NEW;
        $sSubject = $sBody = ''; $aRowInsert=[];
        $email = 'everyone@spj.vn';
        $aBody = array(
            '{LEAVE_NAME}'          => $mUser->getFullName(),
            '{LEAVE_POSITION}'      => Roles::GetRoleNameById($mUser->role_id),
            '{LEAVE_DATE}'          => $mUser->rUsersProfile->getDateBeginJob(),
            '{AGENT_NAME}'          => $mUser->getParentCacheSession(),
        );
        $aSubject = array(
            '{LEAVE_NAME}'          => $mUser->getFullName(),
        );
        self::getContentEmail($email_template_id, $aSubject, $aBody, $sSubject, $sBody);
        $sSubject   = MyFormat::escapeValues($sSubject);
        $sBody      = MyFormat::escapeValues($sBody);
        self::buildArrayInsertFixJson($mUser->id, $aRowInsert, $type, $email_template_id, $mUser->id, $sSubject, $sBody, '', $email);
        self::runSqlInsertFixJson($aRowInsert);
    }
    
    /** @Author: DungNT Sep 04, 2017
     *  @Todo: build list alert nhắc nhở điều phối logline
     */
    public static function loglineBoMoi() {
        $needMore               = ['title'=>'Nhắc nhở logline bò mối '.date('d-m-Y'), 'time_send'=> date('Y-m-d'). ' 08:01:00'];
        $needMore['list_mail']  = ['dieuphoi@spj.vn', 'dungnt@spj.vn'];
        SendEmail::bugToDev('Dear Điều Phối, bạn vui lòng gửi email điện thoại logline trực buối tối.', $needMore);
    }
    public static function remindAnything() {
        $needMore               = ['title'=>'Nhắc nhở setup tuần tháng của báo cáo Spancop ', 'time_send'=> '2020-06-06 08:01:00'];
        $needMore['list_mail']  = ['dungnt@spj.vn'];
        SendEmail::bugToDev('Nhắc nhở setup tuần tháng của báo cáo Spancop admin/gasWeekSetup/index  Quản Lý Tuần Của Tháng ', $needMore);
    }
    /** @Author: DungNT Now102017
     *  @Todo: build list alert KH dùng tài khoản trụ sở chính để đặt app, yêu cầu nhân viên KD liên hệ lại
     *  $mAppOrder = GasAppOrder::model()->findByPk(116771);
        $mCustomer = $mAppOrder->rCustomer;
        GasScheduleEmail::alertLockBookApp($mCustomer, $mAppOrder);
        die;
     */
    public static function alertLockBookApp($mCustomer, $mAppOrder) {
        $needMore               = ['title'=>'Lỗi KH đặt app cho tài khoản trụ sở chính'];
        $needMore['list_mail']  = ['kinhdoanh@spj.vn', 'dungnt@spj.vn'];
        $body = "Khách hàng bị lỗi đặt app: $mCustomer->code_bussiness - $mCustomer->first_name<br>";
        $body .= "<br><br>Địa chỉ: $mCustomer->address";
        $body .= "<br><br>Nhân viên kinh doanh vui lòng liên hệ hỗ trợ khách hàng";
        $body .= "<br><br>Số điện thoại liên hệ KH: $mAppOrder->customer_contact";
        SendEmail::bugToDev($body, $needMore);
    }

    //++ TSK0206-SPJ (NguyenPT 20180117) Handle notify email
    /**
     * Notify when have new interview plan was created to Interviewer
     * @param type $mInterviewPlan Interview plan model
     */
    public static function notifyInterviewPlan($mInterviewPlan) {
        // Get data
        $userName       = isset($mInterviewPlan->rUser) ? $mInterviewPlan->rUser->first_name : '';
        $userEmail      = isset($mInterviewPlan->rUser) ? $mInterviewPlan->rUser->email : '';
        $candidateName  = isset($mInterviewPlan->rCandidate) ? $mInterviewPlan->rCandidate->first_name : '';
        $department     = isset($mInterviewPlan->rPlan) ? $mInterviewPlan->rPlan->getDepartment() : '';
        $roleName       = isset($mInterviewPlan->rRole) ? $mInterviewPlan->rRole->role_name : '';
        $interviewDate  = $mInterviewPlan->interview_date;
        
        // Create email info
        $aRowInsert = [];
        $type = self::MAIL_NOTIFY_INTERVIEW;
        $email_template_id = SendEmail::MAIL_NOTIFY_INTERVIEW;
        $sSubject = $sBody = '';
        $email = $userEmail;
        if (empty($email)) {
            // Stop handle if email is empty
            return;
        }
        $aBody = array(
            '{LEAVE_USER_NAME}'         => $userName,
            '{LEAVE_CANDIDATE_NAME}'    => $candidateName,
            '{LEAVE_POSITION}'          => $roleName,
            '{LEAVE_DEPARTMENT}'        => $department,
            '{LEAVE_TIME}'              => $interviewDate,
            '{LEAVE_LINK}'              => Yii::app()->createAbsoluteUrl(
                    'admin/hrInterviewPlan/confirm',
                    array(
                        'id' => $mInterviewPlan->id
                    ))
            
        );
        $aSubject = array(            
            '{LEAVE_DATE}'          => $interviewDate
        );
        self::getContentEmail($email_template_id, $aSubject, $aBody, $sSubject, $sBody);
        $sSubject   = MyFormat::escapeValues($sSubject);
        $sBody      = MyFormat::escapeValues($sBody);
        self::buildArrayInsertFixJson(
                $mInterviewPlan->user_id, $aRowInsert, $type, $email_template_id,
                $mInterviewPlan->user_id, $sSubject, $sBody, '', $email);
        self::runSqlInsertFixJson($aRowInsert);
    }
    
    /**
     * Notify when have new interview plan was created to Candidate
     * @param type $mInterviewPlan Interview plan model
     */
    public static function notifyInterviewPlanCandidate($mInterviewPlan) {
        // Get data
        $candidateName  = isset($mInterviewPlan->rCandidate) ? $mInterviewPlan->rCandidate->first_name : '';
        $candidateEmail = isset($mInterviewPlan->rCandidate) ? $mInterviewPlan->rCandidate->email : '';
        $department     = isset($mInterviewPlan->rPlan) ? $mInterviewPlan->rPlan->getDepartment() : '';
        $roleName       = isset($mInterviewPlan->rRole) ? $mInterviewPlan->rRole->role_name : '';
        $interviewDate  = $mInterviewPlan->interview_date;
        
        // Create email info
        $aRowInsert = [];
        $type = self::MAIL_NOTIFY_INTERVIEW_CANDIDATE;
        $email_template_id = SendEmail::MAIL_NOTIFY_INTERVIEW_CANDIDATE;
        $sSubject = $sBody = '';
        $email = $candidateEmail;
        if (empty($email)) {
            // Stop handle if email is empty
            return;
        }
        $aBody = array(
            '{LEAVE_CANDIDATE_NAME}'    => $candidateName,
            '{LEAVE_POSITION}'          => $roleName,
            '{LEAVE_DEPARTMENT}'        => $department,
            '{LEAVE_TIME}'              => $interviewDate,
            '{LEAVE_ADDRESS}'           => '86 Nguyễn Cửu Vân Phường 17 Quận Bình Thạnh TPHCM',
            
        );
        $aSubject = [];
        self::getContentEmail($email_template_id, $aSubject, $aBody, $sSubject, $sBody);
        $sSubject   = MyFormat::escapeValues($sSubject);
        $sBody      = MyFormat::escapeValues($sBody);
        self::buildArrayInsertFixJson(
                $mInterviewPlan->user_id, $aRowInsert, $type, $email_template_id,
                $mInterviewPlan->user_id, $sSubject, $sBody, '', $email);
        self::runSqlInsertFixJson($aRowInsert);
    }
    
    /**
     * Handle notify when HrPlan was created to approver
     * @param Object $mHrPlan Plan model
     */
    public static function notifyHrPlanApprover($mHrPlan) {
        // Get data
        $userName       = isset($mHrPlan->rApproved) ? $mHrPlan->rApproved->first_name : '';
        $companyName    = $mHrPlan->getCompany();
        $department     = $mHrPlan->getDepartment();
        $quantity       = $mHrPlan->qty;
        $userEmail      = isset($mHrPlan->rApproved) ? $mHrPlan->rApproved->email : '';
        // Create email info
        $aRowInsert = [];
        $type = self::MAIL_NOTIFY_HR_PLAN_APPROVER;
        $email_template_id = SendEmail::MAIL_NOTIFY_HR_PLAN_APPROVER;
        $sSubject = $sBody = '';
        $email = $userEmail;
        if (empty($email)) {
            // Stop handle if email is empty
            return;
        }
        $aBody = array(
            '{LEAVE_USER_NAME}'         => $userName,
            '{LEAVE_COMPANY_NAME}'      => $companyName,
            '{LEAVE_DEPARTMENT}'        => $department,
            '{LEAVE_FROM_DATE}'         => $mHrPlan->plan_from,
            '{LEAVE_TO_DATE}'           => $mHrPlan->plan_to,
            '{LEAVE_QUANTITY}'          => $quantity,
            '{LEAVE_DESCRIPTION}'       => $mHrPlan->description,
            '{LEAVE_LINK}'              => Yii::app()->createAbsoluteUrl(
                    'admin/hrPlan/view',
                    array(
                        'id' => $mHrPlan->id
                    ))
            
        );
        $aSubject = [];
        self::getContentEmail($email_template_id, $aSubject, $aBody, $sSubject, $sBody);
        $sSubject   = MyFormat::escapeValues($sSubject);
        $sBody      = MyFormat::escapeValues($sBody);
        self::buildArrayInsertFixJson(
                $mHrPlan->approved_1, $aRowInsert, $type, $email_template_id,
                $mHrPlan->approved_1, $sSubject, $sBody, '', $email);
        self::runSqlInsertFixJson($aRowInsert);
    }
    //-- TSK0206-SPJ (NguyenPT 20180117) Handle notify email
    
    /** @Author: DungNT Mar 11, 2018
     *  @Todo: mail thông báo KH nhập nhiều lần mã code
     **/
    public static function alertLockPtttCode($mSell) {
        $mCustomer = $mSell->rCustomer;
        $needMore               = ['title'=>'Cảnh báo 1 KH về nhiều mã code PTTT trong thời gian ngắn'];
        $needMore['list_mail']  = ['giamsat@spj.vn', 'kiennn@spj.vn', 'dungnt@spj.vn'];
        $body = "Khách hàng dưới đây về nhiều mã code PTTT trong thời gian ngắn: $mCustomer->first_name<br>";
        $body .= "<br><br>PTTT CODE: $mSell->pttt_code";
        $body .= "<br><br>Địa chỉ: $mCustomer->address";
        $body .= "<br><br>Các bộ phận liên quan kiểm tra lại NV PTTT nhập thông tin có hợp lệ không.";
        $body .= "<br><br>Số điện thoại: $mSell->phone";
        SendEmail::bugToDev($body, $needMore);
    }
    
    /** @Author: HOANG NAM 07/03/2018
     *  @Todo: send mail pass
     *  @Code: NAM008
     *  @Param: 
     **/
    public static function notifyHrPlanPass($mInterviewPlan,$mHrInterviewPlanDetail) {
        $mHrPlan    = $mInterviewPlan->rPlan;
        if(empty($mHrPlan)){
            return;
        }
        $userEmail=[];
        if(!empty($mHrPlan->rApproved->email)):
            $userEmail[] = $mHrPlan->rApproved->email;
        endif;
        if(!empty($mHrPlan->rCreatedBy->email)):
            $userEmail[] = $mHrPlan->rCreatedBy->email;
        endif;
        $bodyMail = GasScheduleEmail::handleBodyHrPlanPass($mInterviewPlan,$mHrInterviewPlanDetail);
        $needMore['title']      =   "Kết quả phỏng vấn ";
        $needMore['title']      .=  isset($mInterviewPlan->code_no) ? $mInterviewPlan->code_no: '';
        $needMore['list_mail']  = $userEmail;
        SendEmail::bugToDev($bodyMail, $needMore);
    }
    /** @Author: HOANG NAM 07/03/2018
     *  @Todo: create body send mail
     *  @Code: NAM008
     *  @Param: 
     **/
    public static function handleBodyHrPlanPass($mInterviewPlan,$mHrInterviewPlanDetail){
        $result     =   '';
        $result     .= isset($mInterviewPlan->rPlan) ? '<b>Kế hoạch tuyển dụng:</b> '.$mInterviewPlan->rPlan->code_no.'-'. $mInterviewPlan->rPlan->name .'<br>' : '';
        $result     .= isset($mInterviewPlan->rUser) ? '<b>Người phỏng vấn:</b> '.$mInterviewPlan->rUser->first_name .'<br>' : '';
        $result     .= isset($mInterviewPlan->rCandidate) ? '<b>Ứng viên:</b> '.$mInterviewPlan->rCandidate->first_name .'<br>' : '';
        $result     .= isset($mInterviewPlan->rRole) ? '<b>Vị trí ứng tuyển:</b> '.$mInterviewPlan->rRole->role_name.'<br>' : '';
        $result     .= isset($mInterviewPlan->status) ? '<b>Trạng thái:</b> '.HrInterviewPlan::getStatus()[$mInterviewPlan->status].'<br>' : '';
        $result     .= isset($mInterviewPlan->score) ? '<b>Điểm đánh giá ứng viên:</b> '.$mInterviewPlan->score.'<br>' : '';
        $result     .= isset($mHrInterviewPlanDetail->note) ? '<b>Nhận xét:</b> '.$mHrInterviewPlanDetail->note.'<br>' : '';
        return $result;
    }
    
    /** @Author: HOANG NAM 04/04/2018
     *  @Todo: sent email to Approved GasLeaveHolidays
     *  @Param: 
     **/
    public static function notifyGasLeaveHolidays($mGasLeaveHolidaysPlan) {
        $userEmail=[];
        if(!empty($mGasLeaveHolidaysPlan->rApproved->email)):
            $userEmail[] = $mGasLeaveHolidaysPlan->rApproved->email;
        endif;
        $bodyMail = GasScheduleEmail::handleBodyGasLeaveHolidays($mGasLeaveHolidaysPlan);
        $needMore['title']      =   "Kế hoạch nghỉ lễ ".$mGasLeaveHolidaysPlan->id;
        $needMore['list_mail']  = $userEmail;
        SendEmail::bugToDev($bodyMail, $needMore);
    }
    
    public static function handleBodyGasLeaveHolidays($mGasLeaveHolidaysPlan) {
        $userName   = isset($mGasLeaveHolidaysPlan->rCreatedBy) ? $mGasLeaveHolidaysPlan->rCreatedBy->first_name : '';
        $result     =   '';
        $result     .=   'Có kế hoạch nghỉ lễ gửi từ '.$userName.' cần phê duyệt';
        $result     .=  '<br>';
        $result     .=   '<b>Xin vui lòng nhấn vào link bên dưới để Phê duyệt kế hoạch hoặc cập nhật:</b>';
        $result     .=  '<br>';
        $result     .=  Yii::app()->createAbsoluteUrl('admin/GasLeaveHolidays/index',array('year' => $mGasLeaveHolidaysPlan->id));
        return $result;
    }
    
    /** @Author:DuongNV 20/08/18
     *  @Todo: send mail hr
     *  @Param: 
     **/
    public static function notifyHr($model, $approveUrl, $title = '') {
        if(empty($model)){
            return;
        }
        $userEmail=[];
        if(!empty($model->rApprovedBy->email)):
            $userEmail[] = $model->rApprovedBy->email;
        endif;
//        if(!empty($model->rCreatedBy->email)):
//            $userEmail[] = $model->rCreatedBy->email;
//        endif;
        $bodyMail = GasScheduleEmail::handleBodyHr($approveUrl);
        if(empty($title)){
            $needMore['title']       =   "Yêu cầu xét duyệt";
        } else {
            $needMore['title'] = $title;
        }
        $needMore['list_mail']   =   $userEmail;
        SendEmail::bugToDev($bodyMail, $needMore);
    }
    
    /** @Author:DuongNV 19/07/2018
     *  @Todo: send mail report
     *  @Param: 
     **/
//    public static function handleBodyHrSalaryReport($mHrSalaryReport){
//        $data = json_decode($mHrSalaryReport->data, true);
//        $mFuncType = new HrFunctionTypes();
//        $aType = $mFuncType->getArrayTypes();
//        $typeName = '';
//        foreach ($aType as $t) {
//            if($t->id == $mHrSalaryReport->type_id){
//                $typeName = $t->name;break;
//            }
//        }
//        $std = MyFormat::dateConverYmdToDmy($mHrSalaryReport->start_date);
//        $ed = MyFormat::dateConverYmdToDmy($mHrSalaryReport->end_date);
//        $title = 'Bảng '.$typeName.' từ ngày '.$std.' đến ngày '.$ed;
//        $result = '<h1><b>'.$title.'</b></h1>'
//                . '<p>Tên bảng: '. $mHrSalaryReport->name .'</p>'
//                . '<p>Trạng thái: <span class="stt-txt">'.HrSalaryReports::model()->aStatus[$mHrSalaryReport->status] .'</span></p>'
//                . '<div class="tbl-container" style="width:70%; overflow:scroll;">'
//                . '<table class="reportTable" border="1" style="border-collapse: collapse; width: 100%; overflow: scroll;">';
//                $hideFirstCol = false;
//                foreach ($data as $row_key => $row) {
//                    if ($row_key == 0) {
//                        $result .= '<thead>'
//                                .  '<tr>';
//                        foreach ($row as $key => $cell) {
//                            if ($key == 0 && $cell == "Mã nhân viên") { // hide first colum (id user)
//                                $hideFirstCol = true;
//                            }
//                            if (!($hideFirstCol && $key == 0)) {
//                                $result .= '<th>' . $cell . '</th>';
//                            }
//                        }
//                        $result .= '</tr>'
//                                .  '</thead>'
//                                .  '<tbody>';
//                    } else {
//                        $result .= '<tr>';
//                        foreach ($row as $key => $cell) {
//                            if (!($hideFirstCol && $key == 0)) {
//                                $result .= '<td>' . $cell . '</td>';
//                            }
//                        }
//                        $result .= '</tr>';
//                    }
//                }
//                $result .= '</tbody>';
//        $result .=  '</table>'
//                .   '<table class="fakeCol"></table>'
//                .   '<table class="fakeRow"></table>'
//                .   '<table class="fakeCor" border="1"></table>'
//                .   '</div>';
//        $result .= '<div style="padding-top:20px;"><a  href="'.Yii::app()->createAbsoluteUrl('/admin/hrSalaryReports/approvedReport', array('id'=>$mHrSalaryReport->id)).'"'
//                      .' style="color: #fff; font-size: 18px; letter-spacing: 1px; text-decoration: none; background: #4a84cc; padding: 10px; position: relative; top: 15px;">'
//                      .'Chấp nhận</a></div>';
//        return $result;
//    }
//    
//    /** @Author:DuongNV 13/08/18 22:07
//     *  @Todo: send mail work schedule
//     *  @Param: 
//     **/
//    public static function notifyHrWorkSchedule($mHrWorkPlan) {
//        if(empty($mHrWorkPlan)){
//            return;
//        }
//        $userEmail=[];
//        if(!empty($mHrWorkPlan->rApproved->email)):
//            $userEmail[] = $mHrWorkPlan->rApproved->email;
//        endif;
//        if(!empty($mHrWorkPlan->rCreatedBy->email)):
//            $userEmail[] = $mHrWorkPlan->rCreatedBy->email;
//        endif;
//        $title =  'kế hoạch làm việc từ ngày '
//                . MyFormat::dateConverYmdToDmy($mHrWorkPlan->date_from)
//                . ' đến ngày '
//                . MyFormat::dateConverYmdToDmy($mHrWorkPlan->date_to);
//        $bodyMail = GasScheduleEmail::handleBodyHrWorkSchedule($mHrWorkPlan);
//        $needMore['title']       =   "Yêu cầu duyệt ";
//        $needMore['title']      .=   $title;
//        $needMore['list_mail']   =   $userEmail;
//        SendEmail::bugToDev($bodyMail, $needMore);
//    }
    
    /** @Author:DuongNV 20/08/18
     *  @Todo: send mail Hr
     *  @Param: 
     **/
    public static function handleBodyHr($url){
        $result = '';
        $result .= 'Vui lòng click vào link bên dưới để xét duyệt!<br>';
//        $result .= '<a href="'.Yii::app()->createAbsoluteUrl("admin/hrWorkSchedule/Approve/id/{$mHrWorkPlan->id}") .'">';
        $result .= '<a href="' .$url. '">';
        $result .= 'Đến trang xét duyệt</a>';
        return $result;
    }
    
}