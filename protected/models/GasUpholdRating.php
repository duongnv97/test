<?php

/**
 * This is the model class for table "{{_gas_uphold_rating}}".
 *
 * The followings are the available columns in table '{{_gas_uphold_rating}}':
 * @property string $id
 * @property string $uphold_id
 * @property integer $type
 * @property integer $rating_type_id
 * @property integer $rating_type_value
 * @property string $employee_id
 * @property string $created_date_only
 */
class GasUpholdRating extends CActiveRecord
{
    const AUDIT_CUSTOMER_TEST     = 1;
    const AUDIT_EMPLOYEE_WRONG    = 2;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasUpholdRating the static model class
     */
    public $mGasUphold ,$uid_login, $created_date ,$customer_id ,$autocomplete_name ,$rCustomer ,$date_from ,$date_to , $employee_maintain_id , $autocomplete_name_2 ,$rEmployeeMaintain ,$code_no ,$autocomplete_name_third , $rUidLogin; 
    
    /** @Author: ANH DUNG May 19, 2018
     *  @Todo: get list status audit check
     **/
    public function getArrayAuditStatus() {
        return [
            GasUpholdRating::AUDIT_CUSTOMER_TEST    => 'KH test app',
            GasUpholdRating::AUDIT_EMPLOYEE_WRONG   => 'NV bảo trì kém',
        ];
    }
    
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_uphold_rating}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('audit_status, audit_note, id, uphold_id, type, rating_type_id, rating_type_value, employee_id, created_date_only, audit_status ,audit_note', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUphold' => array(self::BELONGS_TO, 'GasUphold', 'uphold_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'uphold_id' => 'Uphold',
            'type' => 'Type',
            'rating_type_value' => 'Rating Type Value',
            'employee_id' => 'Employee',
            'created_date_only' => 'Created Date Only',
            'audit_status' => 'Audit status',
            'audit_note' => 'Audit note',
            'rating_note' => 'KH đánh giá',
            'rating_type_id' => 'Loại',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
            $criteria=new CDbCriteria;
            $criteria->compare('t.id',$this->id,true);
            return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
            ));
    }
    
    public function getRatingType() {
        $mUphold = new GasUphold();
        $aType = $mUphold->getArrayRateType();
        return isset($aType[$this->rating_type_id]) ? $aType[$this->rating_type_id] : '';
    }
    
    /**
     * @Author: ANH DUNG Apr 15, 2016
     */
    public static function DeleteByUpholdId($uphold_id) {
        if(empty($uphold_id)){
            return ;
        }
        $criteria = new CDbCriteria();
        $criteria->compare("uphold_id", $uphold_id);
        self::model()->deleteAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG Apr 16, 2016
     * @Todo: insert new
     * @param: $uphold_id
     * @param: $aRatingType array rating_type_id => rating value
     */
    public static function doCreate($mUphold, $aRatingType) {
        self::DeleteByUpholdId($mUphold->id);
        $aRowInsert=array();
        foreach($aRatingType as $rating_type_id => $rating_type_value){
            if(!empty($rating_type_value)){
                $aRowInsert[] = "('$mUphold->id',
                        '$mUphold->type',
                        '$rating_type_id',
                        '$rating_type_value',
                        '$mUphold->employee_id',
                        '$mUphold->created_date_only'
                    )";
            }
        }
        $tableName = GasUpholdRating::model()->tableName();
        $sql = "insert into $tableName (uphold_id,
                        type,
                        rating_type_id,
                        rating_type_value,
                        employee_id,
                        created_date_only
                        ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert)>0){
            Yii::app()->db->createCommand($sql)->execute();
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 16, 2016
     */
    public static function getByUpholdId($uphold_id, $needMore = array()) {
        $criteria = new CDbCriteria();
        $criteria->compare("uphold_id", $uphold_id);
        $models = self::model()->findAll($criteria);
        if(isset($needMore['listData'])){
            return CHtml::listData($models, "rating_type_id", 'rating_type_value');
        }
        return $models;
        
    }
    /** @Author: PHAM THANH NGHIA 2018
     *  @Todo: 
     *  @Code: NGHIA002
     *  @Param: 
     *   admin/gasUphold/listRating 
     * Làm trang list comment + rating của table gas_gas_uphold
 admin/gasUphold/index
     **/
    public function getGasUpholdById(){
        $this->mGasUphold = GasUphold::model()->findByPk($this->uphold_id);
    }
    public function getWebCodeNo(){
        if(!empty($this->mGasUphold)){
            return $this->mGasUphold->code_no;
        }
        return '';
    }
    public function getCustomer(){
        if(!empty($this->mGasUphold)){
            return $this->mGasUphold->getCustomer();
        }
        return '';
    }
    public function getSale(){
        if(!empty($this->mGasUphold)){
            return $this->mGasUphold->getSale();
        }
        return '';
    }
    public function getTextUpholdType(){
        if(!empty($this->mGasUphold)){
            return $this->mGasUphold->getTextUpholdType();
        }
        return '';
    }
    public function getContactPerson(){
        if(!empty($this->mGasUphold)){
            return $this->mGasUphold->getContactPerson();
        }
        return '';
    }
    public function getContactTel(){
        if(!empty($this->mGasUphold)){
            return $this->mGasUphold->getContactTel();
        }
        return '';
    }
    public function getEmployee(){
        if(!empty($this->mGasUphold)){
            return $this->mGasUphold->getEmployee();
        }
        return '';
    }
    public function getContent(){
        if(!empty($this->mGasUphold)){
            return $this->mGasUphold->getContent();
        }
        return '';
    }
    public function getHasReadText(){
        if(!empty($this->mGasUphold)){
            return $this->mGasUphold->getHasReadText();
        }
        return '';
    }
    public function getTextStatus(){
        if(!empty($this->mGasUphold)){
            return $this->mGasUphold->getTextStatus();
        }
        return '';
    }
    public function getUidLoginWeb(){
        if(!empty($this->mGasUphold)){
            return $this->mGasUphold->getUidLoginWeb();
        }
        return '';
    }
    public function getCreatedInfo(){
        if(!empty($this->mGasUphold)){
            return $this->mGasUphold->getCreatedInfo();
        }
        return '';
    }
    public function getRatingNote(){
        if(!empty($this->mGasUphold)){
            return "<div style='word-wrap: break-word; max-width:200px;'>{$this->mGasUphold->rating_note}</div>";
        }
        return '';
    }
    
    public function searchForRating(){
         $criteria=new CDbCriteria;
        if(isset($_GET['GasUpholdRating'])){
            $aSearch = $this->searchGasUphold();
            $idSearch = CHtml::listData($aSearch, "id", "id");
            $sParamsIn = implode(',', $idSearch);
            
            $criteria->addCondition("t.uphold_id IN ($sParamsIn)");
        }
        $criteria->order = 't.id DESC';
            $criteria->compare('t.id',$this->id,true);
            return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
            ));
    }
    public function searchGasUphold(){
        $criteria=new CDbCriteria;
        $item = $_GET['GasUpholdRating'];

        $criteria->compare('t.employee_id ' ,$item['employee_maintain_id']);
        $criteria->compare('t.customer_id ' ,$item['customer_id']);

        $criteria->compare('t.uid_login ' ,$item['uid_login']);

         $criteria->compare('t.code_no ' ,$item['code_no']);
        if(!empty($item['date_from'])){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($item['date_from']);
        }
        if(!empty($item['date_to'])){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($item['date_to']);
        }
        if(!empty($date_from)){
            $criteria->addCondition("t.created_date_only>='$date_from'");
        }
        if(!empty($date_to)){
            $criteria->addCondition("t.created_date_only<='$date_to'");
        }
        
        $criteria->order = 't.id DESC';
        return GasUphold::model()->findAll($criteria);
        
    }
    
    public function getAuditStatus(){
        $aAuditStatic = $this->getArrayAuditStatus();
        return isset($aAuditStatic[$this->audit_status]) ? $aAuditStatic[$this->audit_status] : '';
    }
    public function getAuditNote(){
        return $this->audit_note;
    }
            
            
            
            
            
            
    
    
    
    
    
    
    
    
    
    
    
    
}