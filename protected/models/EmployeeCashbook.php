<?php

/**
 * This is the model class for table "{{_employee_cashbook}}".
 *
 * The followings are the available columns in table '{{_employee_cashbook}}':
 * @property integer $id
 * @property string $code_no
 * @property string $agent_id
 * @property integer $master_lookup_id
 * @property integer $lookup_type
 * @property string $amount
 * @property string $date_input
 * @property string $customer_id
 * @property string $customer_parent_id
 * @property integer $type_customer
 * @property string $sale_id
 * @property string $note
 * @property string $uid_login
 * @property string $created_date
 */
class EmployeeCashbook extends BaseSpj
{
    public $notViewWarehouse = 0, $oldAmount = 0, $sendSms = false, $requiredFile = true, $province_id, $file_name, $list_id_image=[],$MAX_ID, $autocomplete_name, $autocomplete_name1, $autocomplete_name2, $date_from, $date_to, $date_old;
    const TYPE_DAILY                = 1;// Thu hàng ngày
    const TYPE_SCHEDULE             = 2;// Thu theo lịch
    const TYPE_ALL                  = 3;// Toàn bộ lịch sử thu chi
    const TYPE_BANK_TRANSFER        = 4;// Chuyển khoản Ngân Hàng
    const TYPE_IN_OFFICE            = 5;// Nộp tại cty
    
    const MINUTES_APP_UPDATE        = 2;// số phút cho phép app update thu chi
    
    public static function getRoleDriver() {
        return [ROLE_DRIVER];
    }
    public static function getRoleLikeEmployeeMaintain() {
        return [ROLE_CRAFT_WAREHOUSE, ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_DRIVER, ROLE_EMPLOYEE_MAINTAIN, ROLE_DEBT_COLLECTION, ROLE_EMPLOYEE_MARKET_DEVELOPMENT];
    }
    
    public function getArrayTypeCreate() {
        return [
            EmployeeCashbook::TYPE_SCHEDULE         => 'Lịch thu nợ',
            EmployeeCashbook::TYPE_BANK_TRANSFER    => 'KH chuyển khoản',
            EmployeeCashbook::TYPE_IN_OFFICE        => 'Nộp tại cty',
        ];
    }
    public function getArrayType() {
        return [
            EmployeeCashbook::TYPE_DAILY            => 'Daily',
            EmployeeCashbook::TYPE_SCHEDULE         => 'Lịch thu nợ',
            EmployeeCashbook::TYPE_BANK_TRANSFER    => 'KH chuyển khoản',
            EmployeeCashbook::TYPE_IN_OFFICE        => 'Nộp tại cty',
        ];
    }
    
    /** @Author: DungNT Jan 25, 2019
     *  @Todo: type treo công nợ nhân viên
     **/
    public function getTypeInsertDebit() {
        return [
            GasMasterLookup::ID_CHI_TREO_CONG_NO,
        ];
    }
    
    public function getTypeAppLock() {// Jan519 array type lock không cho PVKH tạo trên app
         return [
            GasMasterLookup::ID_CHI_TREO_CONG_NO,
            GasMasterLookup::ID_CHI_SALARY_EMPLOYEE,
            GasMasterLookup::ID_CHI_BP_PHAP_LY,
            GasMasterLookup::ID_CHI_NOP_THUE,
            GasMasterLookup::ID_CHI_TIEN_DIEN,
            GasMasterLookup::ID_CHI_DIEN_THOAI,
            GasMasterLookup::ID_CHI_THE_CHAN,
            GasMasterLookup::ID_CHI_MUA_VO,
            GasMasterLookup::ID_CHI_OTHER,
            GasMasterLookup::ID_CHI_NOPTIEN_86NCV,
            GasMasterLookup::ID_TIEN_DAI_LY,
             GasMasterLookup::ID_THU_NOP_DUM,
             GasMasterLookup::ID_CHI_NOP_DUM,
        ];
    }
    
    public function getType() {
        $aType = $this->getArrayType();
        return isset($aType[$this->type]) ? $aType[$this->type] : '';
    }
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return EmployeeCashbook the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_employee_cashbook}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules(){
        return array(
            array('master_lookup_id, amount, date_input, customer_id', 'required', 'on'=>'ApiCreate'),
//            array('amount','numerical', 'tooSmall'=>'Số tiền quá nhỏ, vui lòng kiểm tra lại (tối thiểu 1000)','min'=>1000, 'on'=>'ApiCreate, WebCreate1, WebCreate4'),
            array('amount','numerical', 'tooSmall'=>'Số tiền quá nhỏ, vui lòng kiểm tra lại (tối thiểu 1000)','min'=>1, 'on'=>'ApiCreate, WebCreate1, WebCreate4'),
            array('master_lookup_id','CheckDateCreate','on'=>'ApiCreate'),
            array('uid_login, agent_id, master_lookup_id, amount_schedule, date_input, customer_id', 'required', 'on'=>'WebCreate1'),
            array('date_input, employee_id, agent_id', 'required', 'on'=>'WebCreate2'),
            array('bank_id, date_input, agent_id', 'required', 'on'=>'WebCreate4'),
            array('date_input, employee_id, agent_id', 'required', 'on'=>'WebCreate5'),
//            array('uid_login, agent_id, master_lookup_id, amount_schedule, date_input, customer_id', 'required', 'on'=>'WebUpdate'),
            array('id, code_no, agent_id, master_lookup_id, lookup_type, amount, date_input, customer_id, customer_parent_id, type_customer, sale_id, note, uid_login, created_date', 'safe'),
            array('notViewWarehouse, pageSize, type, bank_id, province_id, date_from, date_to, list_id_image, app_order_id, employee_id, amount_schedule, last_update_by, last_update_time', 'safe'),
        );
    }
    
    public function CheckDateCreate($attribute,$params){
        if($this->isNewRecord){
            $dateCheck = '';
            if(strpos($this->date_input, '/')){
                $dateCheck =  MyFormat::dateConverDmyToYmd($this->date_input);
                MyFormat::isValidDate($dateCheck);
                $date_check = MyFormat::modifyDays($dateCheck, 1);//May 17, 2017 không cho tạo ngày hôm trước
                if(!MyFormat::compareTwoDate($date_check, date('Y-m-d'))){
                    $this->addError('date_input', 'Ngày '.$this->date_input.' đã bị khóa, bạn không thể nhập ngày này');
                }
            }
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations(){
        return array(
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rSale' => array(self::BELONGS_TO, 'Users', 'sale_id'),
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rMasterLookup' => array(self::BELONGS_TO, 'GasMasterLookup', 'master_lookup_id'),
            'rEmployee' => array(self::BELONGS_TO, 'Users', 'employee_id'),
            'rFile' => array(self::HAS_MANY, 'GasFile', 'belong_id',
                'on'=>'rFile.type='.  GasFile::TYPE_8_CASHBOOK,
                'order'=>'rFile.id ASC',
            ),
            'rAppOrder' => array(self::BELONGS_TO, 'GasAppOrder', 'app_order_id'),
            'rBank' => array(self::BELONGS_TO, 'Users', 'bank_id'),
            'rUpdateBy' => array(self::BELONGS_TO, 'Users', 'last_update_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels(){
            return array(
                'id' => 'ID',
                'code_no' => 'Mã số',
                'agent_id' => 'Đại lý',
                'master_lookup_id' => 'Loại Thu Chi',
                'lookup_type' => 'Loại',
                'amount' => 'Đã Thu',
                'date_input' => 'Ngày',
                'customer_id' => 'Khách Hàng',
                'customer_parent_id' => 'Customer Parent',
                'type_customer' => 'Loại KH',
                'sale_id' => 'Sale',
                'note' => 'Ghi Chú',
                'uid_login' => 'Người Tạo',
                'created_date' => 'Ngày Tạo',
                'amount_schedule' => 'Tiền Phải Thu',
                'employee_id' => 'Nhân viên thu',
                'last_update_time' => 'Cập nhật',
                'last_update_by' => 'Người cập nhật',
                'date_from' => 'Từ ngày',
                'date_to' => 'Đến',
                'province_id' => 'Tỉnh',
                'type' => 'Loại',
                'bank_id' => 'Ngân hàng',
                'pageSize' => 'Số Dòng Hiển Thị',
                'notViewWarehouse' => 'Không xem kho',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search(){
        $criteria=new CDbCriteria;
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        $criteria->compare('t.code_no', trim($this->code_no));
        $criteria->compare('t.agent_id',$this->agent_id);
        if(is_array($this->master_lookup_id) && count($this->master_lookup_id)){
            $sParamsIn = implode(',', $this->master_lookup_id);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.master_lookup_id IN ($sParamsIn)");
            }
        }
        $criteria->compare('t.lookup_type',$this->lookup_type);
        
        $criteria->compare('t.customer_id',$this->customer_id);
        $criteria->compare('t.type_customer',$this->type_customer);
        $criteria->compare('t.sale_id',$this->sale_id);
        $criteria->compare('t.uid_login',$this->uid_login);
        $criteria->compare('t.employee_id',$this->employee_id);
        $criteria->compare('t.type',$this->type);
        $aRoleViewAll = [ROLE_HEAD_GAS_BO, ROLE_TELESALE, ROLE_SALE_ADMIN, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_ADMIN, ROLE_ACCOUNTING, ROLE_ACCOUNTING_ZONE, ROLE_CALL_CENTER, ROLE_MONITORING, ROLE_RECEPTION, ROLE_EMPLOYEE_OF_LEGAL, ROLE_CRAFT_WAREHOUSE];
        if($cRole == ROLE_SUB_USER_AGENT){
            $criteria->addCondition('t.agent_id=' . MyFormat::getAgentId());
        }
//        elseif( !in_array($cRole, $aRoleViewAll) && !in_array($cUid, GasConst::getArrUidUpdateAll())){
//            $criteria->addCondition('t.uid_login=' . MyFormat::getCurrentUid());
//        }
        GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');

        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
            DateHelper::searchGreater($date_from, 'date_input_bigint', $criteria);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
            DateHelper::searchSmaller($date_to, 'date_input_bigint', $criteria);
            
        }
        GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');
        if($this->notViewWarehouse){// không lấy dữ liệu các kho
            $sParamsIn = implode(',', GasCheck::getAgentNotGentAuto());
            $criteria->addCondition('t.agent_id NOT IN ('.$sParamsIn.')');
        }
        
        $criteria->order = 't.id DESC';
        if($cUid == GasLeave::PHUONG_PTK){
            $criteria->order = 't.date_input ASC';
        }
        
        $this->handleMoreSearch($criteria);
        $_SESSION['data-excel'] = new CActiveDataProvider($this, array(
            'pagination'=>false,
            'criteria'=>$criteria,
//            'sort' => $sort,
        ));

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> $this->pageSize,
            ),
        ));
    }
    
    /**
     * @Author: DungNT Mar 27, 2017
     */
    public function handleMoreSearch(&$criteria) {
        if(!isset($_GET['type'])){
            $criteria->addCondition('t.type=' . EmployeeCashbook::TYPE_SCHEDULE);
            $criteria->addCondition('t.amount<1');
        }elseif(isset($_GET['type']) && $_GET['type'] == EmployeeCashbook::TYPE_SCHEDULE){
            $criteria->addCondition('t.type=' . EmployeeCashbook::TYPE_SCHEDULE);
            $criteria->addCondition('t.amount>0');
        }elseif(isset($_GET['type']) && $_GET['type'] == EmployeeCashbook::TYPE_DAILY){
            $criteria->addCondition('t.type=' . EmployeeCashbook::TYPE_DAILY);
        }elseif(isset($_GET['type']) && $_GET['type'] == EmployeeCashbook::TYPE_BANK_TRANSFER){
            $criteria->addCondition('t.type=' . EmployeeCashbook::TYPE_BANK_TRANSFER);
        }elseif(isset($_GET['type']) && $_GET['type'] == EmployeeCashbook::TYPE_ALL){
            // nothing to handle limit
        }
        $aAgentId = $this->getAgentOfProvince($this->province_id);
        $sParamsIn = implode(',', $aAgentId);
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.agent_id IN ($sParamsIn)");
        }
    }
    
    /** @Author: DungNT Jul 28, 2017
     * @Todo: get list agent by array province
     */
    public function getAgentOfProvince($aProvince) {
        if(!is_array($aProvince)){
            return [];
        }
        $aRes = [];
        $mAppCache = new AppCache();
        $aAgent = $mAppCache->getAgent();
        foreach($aAgent as $item){
            if(in_array($item['province_id'], $aProvince)){
                $aRes[] = $item['id'];
            }
        }
        return $aRes;
    }
    public function setCodeNo() {// Mar2018
        if(!$this->isNewRecord){
            return ;
        }
        $prefix_code    = 'C'.date('y');
        $this->code_no  = $prefix_code.ActiveRecord::randString(6, GasConst::CODE_CHAR);
    }
    
    public function beforeSave() {
        $this->convertDate();
        $this->note = InputHelper::removeScriptTag($this->note);
        $this->date_input_bigint = strtotime($this->date_input);
        $this->setCodeNo();
        return parent::beforeSave();
    }
    
    protected function beforeDelete() {
        $this->deleteCashbookAgent();
        $this->updateGasRemainOnAppOrder();
        $this->removeGasDebts();
        GasFile::DeleteByBelongIdAndType($this->id, GasFile::TYPE_8_CASHBOOK);
        return parent::beforeDelete();
    }
    
    /** @Author: DungNT Jan 19, 2019
     *  @Todo: remove GasDebts neu co
     **/
    public function removeGasDebts() {
        $mGasDebts = new GasDebts();
        $mGasDebts->relate_id    = $this->id;
        $mGasDebts->type         = GasDebts::TYPE_DEBIT_PVKH;
        $mGasDebts->deleteByRelateId();
    }
    
    protected function beforeValidate() {
        $this->setCustomerInfo();
        $this->setLookupType();
        $this->amount           = MyFormat::removeCommaAndPoint($this->amount);
        $this->amount_schedule  = MyFormat::removeCommaAndPoint($this->amount_schedule);
        return parent::beforeValidate();
    }
    
    public function convertDate() {
        if(strpos($this->date_input, '/')){
            $this->date_input =  MyFormat::dateConverDmyToYmd($this->date_input);
            MyFormat::isValidDate($this->date_input);
        }
    }
    
    /**
     * @Author: DungNT Mar 09, 2017
     * @Todo: set some info when web create
     */
    public function setWebInfo() {
        if($this->isNewRecord){
            $this->uid_login = MyFormat::getCurrentUid();
        }else{
            $this->last_update_by      = MyFormat::getCurrentUid();
            $this->last_update_time    = date('Y-m-d H:i:s');
        }
        $mAgent = $this->rAgent;
        if($mAgent && $mAgent->role_id != ROLE_AGENT){
//            $this->employee_id = $this->agent_id;// xóa chỗ này đi, vì không đúng field
        }// Nếu người thu không phải đại lý thì là NV thu nợ nên gắn luôn vào employee_id
        
        return ;
        
        /* Mar 27, 2017 không xử lý kiểu này nữa
         * mà sẽ chỉ chọn ĐL đi thu, sẽ vào APP của 2 giao nhận 
         * ai thu người đó tự nhập vào
         */
        $mUserLogin = $this->rUidLogin;
        if($mUserLogin){
            $this->agent_id = empty($mUserLogin->parent_id) ? '' : $mUserLogin->parent_id;
        }
    }
    
    /** @Author: DungNT Feb 13, 2019
     *  @Todo: fix check only allow User PVKH select Agent of User
     **/
    public function checkSelectCustomer() {
        $mCustomer = $this->rCustomer;
        if(empty($mCustomer)){
            $eMsg = 'Chưa chọn khách hàng hoặc đại lý';
//            throw new Exception($eMsg);
            $this->addError('id', $eMsg);
        }
        if($mCustomer->role_id != ROLE_AGENT){
            return ;
        }
        if($mCustomer->id != $this->agent_id){
            $eMsg = "Bạn chọn sai đại lý, vui lòng chọn đúng {$this->getAgent()}";
//            throw new Exception($eMsg);
            $this->addError('id', $eMsg);
        }
    }
    
    /** @Author: DungNT Dec 23, 2017
     *  @Todo: check chi gas dư sau khi thu tiền từ đơn hàng nợ. Vì đơn nợ đã trừ gas dư đi rồi
     **/
    public function checkAppPayGasRemainOrderDebit() {
        if(empty($this->app_order_id) || $this->master_lookup_id != GasMasterLookup::ID_CHI_GAS_DU){
            return ;
        }
        $mAppOrder = GasAppOrder::model()->findByPk($this->app_order_id);
        $date_check = MyFormat::modifyDays($mAppOrder->date_delivery, Yii::app()->params['DaysUpdateAppBoMoi']);
        if(!MyFormat::compareTwoDate($date_check, date('Y-m-d'))){
            throw new Exception('Đơn hàng nợ không thể chi gas dư nữa, liên hệ kế toán khu vực để hỗ trợ');
        }
    }
    
    /**
     * @Author: DungNT Mar 06, 2017
     * @Todo: handle app post and validate
     */
    public function handlePost($q) {
        $this->customer_id      = $q->customer_id > 10 ? $q->customer_id : '';
        if($this->isNewRecord){
            $this->app_order_id     = isset($q->app_order_id) ? $q->app_order_id : 0;
        }
        $this->master_lookup_id = $q->master_lookup_id;
        $this->date_input       = $q->date_input;
        $this->amount           = $q->amount;
        $this->note             = trim(MyFormat::escapeValues($q->note));
        if($this->isNewRecord){
            $this->uid_login        = $this->mAppUserLogin->id;
        }
        if(isset($q->list_id_image) && is_array($q->list_id_image)){
            $this->list_id_image = $q->list_id_image;
        }
        $this->validate();
        $this->checkAppPayGasRemainOrderDebit();
        $this->handlePostCheckDate();
        $this->checkTimeUpdate();
        $this->checkSelectCustomer();
        if(in_array($this->master_lookup_id, $this->getTypeAppLock())){
            $this->addError('id', "Bạn không được làm phiếu: {$this->getMasterLookupText()}");
        }
        if(!$this->isNewRecord){
            $this->date_old     = $this->date_input;
            $this->convertDate();
            if(!$this->employeeCanUpdate()){
                $this->addError('id', 'Ngày cập nhật đã bị khóa, bạn không thể cập nhật');
            }
            $this->date_input = $this->date_old;
        }
        // Mar 27, 2017 override here nếu thu từ lịch thu nợ KH
    }
    
    /** @Author: DungNT Sep 03, 2018
     *  @Todo: check ko cho sửa sau khi tạo 2 phút
     **/
    public function checkTimeUpdate() {
        if($this->isNewRecord){
            return ;
        }
        $timeAllow = MyFormat::addDays($this->created_date, EmployeeCashbook::MINUTES_APP_UPDATE, '+', 'minutes', 'Y-m-d H:i:s');
        $ok = MyFormat::compareTwoDate($timeAllow, date('Y-m-d H:i:s'));
        if(!$ok){
            $this->addError('created_date', 'Hệ thống không cho phép cập nhật phiếu thu chi');
        }
    }
    
    /**
     * @Author: DungNT Mar 12, 2017
     * @Todo: check date valid
     */
    public function handlePostCheckDate() {
        $date_check = MyFormat::dateConverDmyToYmd($this->date_input);
        if(MyFormat::compareTwoDate($date_check, date('Y-m-d'))){
            $this->addError('date_input', 'Ngày '.$this->date_input.' không hợp lệ');
        }
        // check create duplicate row for one customer
        
//        if($this->isNewRecord && !$this->hasErrors()){// cho phép tạo nhiều thu 1 KH trong ngày
//            if($this->countByCustomerAndDate()){
//                $this->addError('customer_id', 'Không thể tạo thêm. Ngày '.$this->date_input.' đã tạo '.$this->getLookupTypeText().' cho khách hàng rồi');
//            }
//        }
    }
    
    /**
     * @Author: DungNT May 18, 2017
     * @Todo: xử lý cập nhật thông tin debit sang model app order
     */
    public function handleUpdateAppOrder() {
        $mAppOrder      = GasAppOrder::model()->findByPk($this->app_order_id);
        $status_debit   = GasAppOrder::DEBIT_YES;
        $aUpdate        = ['status_debit','money_debit'];// default GasMasterLookup::ID_THU_TIEN_KH thu tiền KH bò mối
        if($mAppOrder){
            if($this->master_lookup_id == GasMasterLookup::ID_THU_TIEN_KH){
                $mAppOrder->status_debit = GasAppOrder::DEBIT_NO;
                $mAppOrder->saveEvent(GasAppOrder::WEB_PAY_DEBIT, '');
            }
//            if($this->amount == $mAppOrder->grand_total){
//                $status_debit = GasAppOrder::DEBIT_NO;
//            }elseif($this->amount < $mAppOrder->grand_total){
//                $status_debit = GasAppOrder::DEBIT_NOT_FULL;
//                $mAppOrder->money_debit = $mAppOrder->grand_total - $this->amount;
//            }
//            $mAppOrder->status_debit = $status_debit;
            // Đoạn cập nhật status_debit này sẽ gây sai, khi số tiền thu lớn hơn grand_total thì hệ thống gắn nợ cho đơn hàng, nhưng thực tế không phải vậy
            
            if($this->master_lookup_id == GasMasterLookup::ID_CHI_GAS_DU){
                $mAppOrder->paid_gas_remain = GasAppOrder::PAID_REMAIN_YES;
                $aUpdate = ['paid_gas_remain'];
            }
            $mAppOrder->update($aUpdate);
        }
    }
    
    /**
     * @Author: DungNT Mar 12, 2017
     * @Todo: get record của 1 user tạo trong 1 ngày của 1 KH
     */
    public function countByCustomerAndDate() {
        $date_check = MyFormat::dateConverDmyToYmd($this->date_input);
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.uid_login=' . $this->uid_login . ' AND t.customer_id='. $this->customer_id);
        $criteria->addCondition("t.date_input='$date_check' AND t.master_lookup_id=".$this->master_lookup_id);
        return self::model()->count($criteria);
    }
    
    /**
     * @Author: DungNT Mar 06, 2017
     * @Todo: set some info of customer
     */
    public function setCustomerInfo() {// Mar2318 không dùng rCustomer khi form update cho change customer_id
//        $mCustomer = $this->rCustomer;// Mar2318 Xem lại dùng kiểu này, vì khi update nếu đổi customer_id thì rCustomer là của customer cũ, sẽ bị sai khi set customer info 
        $mCustomer = Users::model()->findByPk($this->customer_id);
        //là loại KH 1: bò hay 2: mối, 3: HỘ GIA ĐÌNH,4:KH khác (chưa xử lý lưu số 4 này), 5: KH LÀ ĐẠI LÝ KHÁC TRONG HỆ THỐNG 
        if($mCustomer){
            $this->type_customer        = $mCustomer->is_maintain;
            $this->customer_parent_id   = $mCustomer->parent_id;
            $this->sale_id              = $mCustomer->sale_id;
        }
    }
    // lookup_type is Thu or Chi: GasMasterLookup::MASTER_TYPE_THU, GasMasterLookup::MASTER_TYPE_CHI
    public function setLookupType() {
        // có thể sẽ phải đổi GasMasterLookup::TYPE_CASHBOOK
//         $mAppCache = new AppCache();
//        $aMasterLookup = $mAppCache->getModelTypeLookup(GasMasterLookup::TYPE_CASHBOOK, []);
        $aMasterLookup = $this->getListModelLookup();
        if(isset($aMasterLookup[$this->master_lookup_id])){
            $this->lookup_type = $aMasterLookup[$this->master_lookup_id]['type'];
        }
        return 0;
    }
    
    /**
     * @Author: DungNT Mar 06, 2017
     * @Todo: check xem User có được quyền sửa thu chi không
     */
    public function employeeCanUpdate() {
//        return '1'; // only dev test need close on live
        if(is_null($this->mAppUserLogin) || $this->app_order_id > 0 || 
         ($this->type == EmployeeCashbook::TYPE_SCHEDULE && $this->amount < 1 )
        ){
            return '0';
        }
        
//        $dayAllow = date('Y-m-d');
//        $dayAllow = MyFormat::modifyDays($dayAllow, 0, '-');// 0 là cho phép sửa trong ngày, 2 là cho phép sửa ngày mai
        $date_check = MyFormat::modifyDays($this->date_input, 1);
        if( ($this->uid_login ==  $this->mAppUserLogin->id || $this->employee_id ==  $this->mAppUserLogin->id)
            && MyFormat::compareTwoDate($date_check, date('Y-m-d'))){
            return '1';
        }
        return '0';
    }
    
    /** @Author: DungNT Feb 25, 2017
     * @Todo: handle save request from App
     */
    public function handleSave($q) {
        if($this->isNewRecord){
            // bug May 26, 2017 trước khi inser thì phải check để xóa record: date+ customer_id + type + app_order_id
            $this->removeOldRecordAppOrderId();
            $this->save();// save luôn để còn save detail
        }else{
            $this->update();
        }
        $this->updateCashbookAgent();
        $this->handleUpdateAppOrder();// cập nhật nợ cho đơn hàng
        $this->saveFile();
        $this->deleteFile();
        $this->notifyEmployee();
        $this->doSendSms();// gửi SMS thu tiền KH bò mối
    }
    
    /**
     * @Author: DungNT May 19, 2017
     * @Todo: khi inssert new trước khi inser thì phải check để xóa record: date+ customer_id + type + app_order_id
     */
    public function removeOldRecordAppOrderId() {
        $modelOld = $this->getByAppOrderId();
        if($modelOld && $modelOld->master_lookup_id == $this->master_lookup_id
            && $modelOld->customer_id == $this->customer_id){
            $modelOld->delete();
        }
    }
    
    /**
     * @Author: DungNT May 11, 2017
     * @Todo: cập nhật vào sổ quỹ Nhân Viên của đại lý
     */
    public function updateCashbookAgent() {
        $mCashBookDetail = new GasCashBookDetail();
        $mCashBookDetail->makeRecordBomoi($this);// save vào sổ quỹ thu bò mối
    }
    public function deleteCashbookAgent() {// xóa record ref
        $mCashBookDetail = new GasCashBookDetail();
        $mCashBookDetail->deleteById($this->cash_book_detail_id);
    }
    public function updateGasRemainOnAppOrder() {// xóa cờ chi gas dư bên Order nếu xóa phiếu chi gas dư đi
        if($this->master_lookup_id == GasMasterLookup::ID_CHI_GAS_DU && !empty($this->app_order_id)){
            $mAppOrder = GasAppOrder::model()->findByPk($this->app_order_id);
            if($mAppOrder){
                $mAppOrder->paid_gas_remain = 0;
                $mAppOrder->update(['paid_gas_remain']);
            }
        }
    }
    /**
     * @Author: DungNT May 19, 2017
     * @Todo: get model by app_order_id
     */
    public function getByAppOrderId($needMore = []) {
        if(empty($this->app_order_id)){
            return null;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.app_order_id='.$this->app_order_id);
        if(is_array($this->master_lookup_id)){
            $sParamsIn = implode(',', $this->master_lookup_id);
            $criteria->addCondition("t.master_lookup_id IN ($sParamsIn)");
        }elseif(!empty($this->master_lookup_id)){
            $criteria->addCondition('t.master_lookup_id='.$this->master_lookup_id);
        }
        if(isset($needMore['GetAll'])){
            return self::model()->findAll($criteria);
        }
        return self::model()->find($criteria);
    }
    
    /**
     * @Author: DungNT Mar 06, 2017
     * format record thu chi của giao nhận view from app customer BoMoi
     */
    public function appView() {
        $aRes           = array();// order_view, order_edit
//        $mAppCache = new AppCache();
//        $aLookupType    = $mAppCache->getModelTypeLookup(GasMasterLookup::TYPE_CASHBOOK, []);
        $aRes['id']                 = $this->id;
        $aRes['code_no']            = $this->code_no;
        $aRes['date_input']         = $this->getDateInput();
        $aRes['customer_name']      = $this->getCustomer();
        $aRes['customer_address']   = $this->getCustomer('address');
        $aRes['customer_phone']     = $this->getCustomer('phone');
        $aRes['note']               = $this->getNote();
        $aRes['amount']             = $this->getAmount(true);
        $aRes['master_lookup_id']   = $this->master_lookup_id;
        $aRes['customer_id']        = $this->customer_id;
        $aRes['master_lookup_text'] = $this->getMasterLookupText();
        $aRes['created_date']       = $this->getCreatedDate();
        $aRes['allow_update']       = $this->employeeCanUpdate();
        $aRes['app_order_id']       = $this->app_order_id;
        $aRes['list_image']         = GasFile::apiGetFileUpload($this->rFile);
        
        return $aRes;
    }

    public function getListdataLookupType() {
        return $this->getListModelLookup(['listdata'=>1, 'limit_type'=>1]);
    }
    public function getListModelLookup($needMore = []) {
        $mAppCache              = new AppCache();
        $aModel                 = $mAppCache->getModelTypeLookup(GasMasterLookup::TYPE_CASHBOOK, []);
        $mLookup                = new GasMasterLookup();
        if(isset($needMore['limit_type'])){// from app request
            $mLookup->sourceFrom   = BaseSpj::SOURCE_FROM_ANDROID;
        }
        $aIdLimit               = $mLookup->getArrayIdApp();
        if(isset($needMore['listdata'])){
            $aRes = [];
            foreach($aModel as $item){
                if(isset($needMore['limit_type']) && !in_array($item['id'], $aIdLimit)){
                    continue ;
                }
                $aRes[$item['id']] = $item['name'];
            }
            return $aRes;
        }
        return $aModel;
    }
    public function getDateInput() {
        return MyFormat::dateConverYmdToDmy($this->date_input);
    }
    public function getAgent() {
        $mUser = $this->rAgent;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    public function getMasterLookupText() {
        $aModelLookupType = $this->getListModelLookup();
        if(isset($aModelLookupType[$this->master_lookup_id])){
            return $aModelLookupType[$this->master_lookup_id]['name'];
        }
        return ;
    }
    public function getLookupTypeText() {
        if($this->lookup_type == GasMasterLookup::MASTER_TYPE_THU){
            return 'Thu';
        }elseif($this->lookup_type == GasMasterLookup::MASTER_TYPE_CHI){
            return 'Chi';
        }
        return '';
    }
    public function getArrayLookupType() {
        $model = new GasMasterLookup();
        return $model->getArrayLookupType();
    }
    
    public function getAmount($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->amount);
        }
        return $this->amount;
    }
    /** @Author: DungNT Sep 01, 2017
     * @Todo: xử lý render amount ở list trong Thu Chi và Lịch thu tiền
     * Nếu ở list lịch thu tiền thỉ hiển thị amount_schedule else thì amount
     */
    public function getAmountApp($format = false) {
        $value = $this->amount;
        if($this->type == EmployeeCashbook::TYPE_SCHEDULE && $this->amount < 1){
            $value = $this->amount_schedule;
        }
        if($format){
            return ActiveRecord::formatCurrency($value);
        }
        return $value;
    }
    public function getAmountThu($format = true) {
        if($this->lookup_type == GasMasterLookup::MASTER_TYPE_THU){
            return $this->getAmount($format);
        }else{
            return '';
        }
    }
    public function getAmountChi($format = true) {
        if($this->lookup_type == GasMasterLookup::MASTER_TYPE_CHI){
            return $this->getAmount($format);
        }else{
            return '';
        }
    }
    public function getAmountSchedule($format = false) {
        if($format){
            return !empty($this->amount_schedule) ? ActiveRecord::formatCurrency($this->amount_schedule) : '';
        }
        return $this->amount_schedule;
    }
    public function getCustomer($field_name='first_name') {
        $mUser = $this->rCustomer;
        if($mUser){
            return $mUser->$field_name;
//            return "<b>".$mUser->code_bussiness."-".$mUser->first_name."</b><br>".$mUser->address."";
        }
        return '';
    }
    public function getEmployee($field_name='first_name') {
        $mUser = $this->rEmployee;
        if($mUser){
            return $mUser->$field_name;
//            return "<b>".$mUser->code_bussiness."-".$mUser->first_name."</b><br>".$mUser->address."";
        }
        return '';
    }
    public function getNote() {
        return $this->note;
    }
    public function getNoteWebShow($getSum = false) {
        $sDataHide = '<span class="sumAmountSchedule">'.$this->amount_schedule.'</span>';
        if($this->lookup_type == GasMasterLookup::MASTER_TYPE_THU){
            $sDataHide .= '<span class="sumAmountThu">'.$this->amount.'</span>';
        }elseif($this->lookup_type == GasMasterLookup::MASTER_TYPE_CHI){
            $sDataHide .= '<span class="sumAmountChi">'.$this->amount.'</span>';
        }
        $sDataHide = '<span class="display_none">'.$sDataHide.'</span>';
        if(!$getSum){
            $sDataHide = '';
        }
        return nl2br($this->note).$sDataHide;
    }
    public function getLastUpdateTime() {
        if(is_null($this->last_update_time)){
            return '';
        }
        return MyFormat::dateConverYmdToDmy($this->last_update_time, "d/m/Y H:i");
    }
    public function getLastUpdateBy($field_name='first_name') {
        $mUser = $this->rUpdateBy;
        if($mUser){
            return $mUser->$field_name;
//            return "<b>".$mUser->code_bussiness."-".$mUser->first_name."</b><br>".$mUser->address."";
        }
        return '';
    }
    /**
     * @Author: DungNT Mar 06, 2017
     * @Todo: get model for app update
     */
    public function loadAppUpdate($q) {
        if(empty($q->id)){
            return null;
        }
        $appUid = $this->mAppUserLogin->id;
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.id=' . $q->id);
        $criteria->addCondition("t.uid_login=$appUid OR t.employee_id=$appUid" );
        return self::model()->find($criteria);
    }
    
    /**
     * @Author: DungNT Mar 06, 2017
     * @Todo: get model thu chi của giao nhận, những giao nhận của đl đó sẽ được view
     */
    public function getModelApp($id) {
        if(empty($id)){
            return null;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.id='.$id );
//        if(!empty($this->mAppUserLogin->parent_id)){
//            $criteria->addCondition('t.agent_id='.$this->mAppUserLogin->parent_id);
//        }
        return self::model()->find($criteria);
//        $mAppCache = new AppCache();
//        $aEmployeeMaintain = $mAppCache->getUserMaintainOfAgent($this->user_id_create);
    }
    
    /**
     * @Author: DungNT May 02, 2017
     * @Todo: get summary text
     */
    public function getSummaryText() {
        $mUsersBalance              = new UsersBalance();
        $mUsersBalance->c_month     = date('m');
        $mUsersBalance->c_year      = date('Y');
        $mUsersBalance->user_id     = $this->mAppUserLogin->id;
        $mUsersBalance->date_from   = date('d-m-Y');
        $mUsersBalance->date_to     = $mUsersBalance->date_from;
        $mUsersBalance->getBoMoi    = true;
        $mUsersBalance->getByMonthYear();
        $res = date('d-m-Y');
        if(isset($mUsersBalance->aDataBoMoi['amount_revenue'])){
            $res .= " \r\nThu Bò Mối: ".ActiveRecord::formatCurrencyRound($mUsersBalance->aDataBoMoi['amount_revenue']);
//            $res .= " Thu Bò Mối: ".ActiveRecord::formatCurrencyRound($mUsersBalance->aDataBoMoi['amount_revenue']);
            $res .= ' Chi: '.ActiveRecord::formatCurrencyRound($mUsersBalance->aDataBoMoi['amount_cost']);
        }
        return $res;
    }
    
    
    /** @Author: DungNT  Mar 06, 2017 -- handle listing api */
    public function handleApiList(&$result, $q) {
        // 1. get list order by user id
        $dataProvider   = $this->apiListing($q);
        $models         = $dataProvider->data;
        $CPagination    = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page']   = $CPagination->pageCount;
        
        if($q->page == 0){
//            $summary_text   = "Tồn cuối 15,000,000 Test";
            $summary_text   = $this->getSummaryText();
        }else{
            $summary_text   = '';
        }

        $result['message'] = $summary_text;
        
        if( $q->page >= $CPagination->pageCount ){
            $result['record'] = array();
        }else{
            foreach($models as $model){
                $model->mAppUserLogin       = $this->mAppUserLogin;// for canPickCancel
                $aRes = [];
                $aRes['id']                 = $model->id;
                $aRes['code_no']            = $model->code_no;
                $aRes['date_input']         = $model->getDateInput();
                $aRes['customer_id']        = $model->customer_id;
                $aRes['customer_name']      = $model->code_no. ' - '.$model->getCustomer();
                $aRes['customer_address']   = $model->getCustomer('address');
                $aRes['customer_phone']     = $model->getCustomer('phone');
                $aRes['note']               = $model->getNote();
                $aRes['amount']             = $model->getAmountApp(true);
                $aRes['master_lookup_text'] = $model->getMasterLookupText();
                $aRes['lookup_type_text']   = $model->getLookupTypeText();
                $aRes['allow_update']       = $model->employeeCanUpdate();
                $aRes['created_date']       = $model->getCreatedDate();
                $aRes['master_lookup_id']   = $model->master_lookup_id;
                
                $result['record'][]         = $aRes;
            }
        }
    }
    
    /**
    * @Author: DungNT Mar 06, 2017
    * @Todo: get data listing 
    * @param: $q object post params from app client
    */
    public function apiListing($q) {
        $criteria = new CDbCriteria();
//        if(in_array($this->mAppUserLogin->role_id, EmployeeCashbook::getRoleLikeEmployeeMaintain())){ Jun0119 close change to GasConst::
        if(in_array($this->mAppUserLogin->role_id, GasConst::getRoleAppReport())){
            $criteria->addCondition('t.uid_login=' . $this->mAppUserLogin->id .' OR t.employee_id='.$this->mAppUserLogin->id);
        }else{
            $criteria->addCondition('t.uid_login=1');// không cho user nào xem nữa
        }
        if(!empty($q->customer_id)){
            $criteria->addCondition('t.customer_id=' . $q->customer_id);
        }
        if(!empty($q->lookup_type)){
            $criteria->addCondition('t.lookup_type=' . $q->lookup_type);
        }
        
        // Mar 28, 2017 Tạm Close lại 
        $typeSchedule = EmployeeCashbook::TYPE_SCHEDULE;
        if($q->type == EmployeeCashbook::TYPE_SCHEDULE){
            $criteria->addCondition('t.type=' . EmployeeCashbook::TYPE_SCHEDULE);
            $criteria->addCondition('t.amount=0');
        }elseif($q->type == EmployeeCashbook::TYPE_DAILY){
            $criteria->addCondition('t.type=' . EmployeeCashbook::TYPE_DAILY . " OR (t.type=$typeSchedule AND t.amount>0)");
        }

        $criteria->order = 't.id DESC';
        $dataProvider=new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize' => GasAppOrder::API_LISTING_ITEM_PER_PAGE,
//                'pageSize' => 2,
                'currentPage' => (int)$q->page,
            ),
          ));
        return $dataProvider;
    }
    
    // web check 
    public function canUpdate(){
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        $aUidUpdateSchedule = [GasLeave::THUC_NH];
        $aUidNotUpdate      = [GasConst::UID_BINH_TQ];
        $aTypeNotUpdate     = [EmployeeCashbook::TYPE_SCHEDULE];
        $aTypeAllowUpdate   = [EmployeeCashbook::TYPE_DAILY];
        $aRoleUpdate        = [ROLE_EMPLOYEE_OF_LEGAL, ROLE_CALL_CENTER, ROLE_DIEU_PHOI, ROLE_ACCOUNTING];
        $aUidUpdateAll      = [GasConst::UID_NGUYEN_DTM, GasLeave::THUC_NH, GasLeave::PHUONG_PTK, GasConst::UID_ADMIN];
        
        if(in_array($cUid, $aUidNotUpdate)){
            return false;
        }
        
        if (in_array($cRole, $aRoleUpdate) && !in_array($cUid, $aUidUpdateAll) && !in_array($cUid, $aUidUpdateSchedule)) {
            return false; // cho phép admin sửa hết, còn những user khác thì ai tạo thì dc sửa của người đó
        }
        if($cRole != ROLE_ADMIN && $cUid != $this->uid_login && !in_array($cRole, $aRoleUpdate) && !in_array($cUid, $aUidUpdateSchedule)){
            return false;
        }
        if(in_array($cRole, $aRoleUpdate) && in_array($this->type, $aTypeNotUpdate)){
            return false;
        }
        if(in_array($cUid, $aUidUpdateSchedule) && (
            ($this->amount > 0 && $this->type == EmployeeCashbook::TYPE_SCHEDULE) ||
            $this->type == EmployeeCashbook::TYPE_DAILY
        )){
//            return true;// Sep1418 tam mo de Thuc sua
//            return false;// DungNT Sep2919 mở cho Thúc update all giống PhuongPTK
        }

//        $defaultDay = 62;// mở cho Nguyệt sửa lại báo cáo
        $defaultDay = Yii::app()->params['DaysUpdateFixAll'];// mở cho Nguyệt sửa lại báo cáo
        if($cRole == ROLE_ADMIN){
            $defaultDay = 1000;
        }elseif(!in_array($cUid, $aUidUpdateAll)){
            $defaultDay = 1;
        }

        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, $defaultDay, '-');
        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }
    
    /**
     * @Author: DungNT Mar 28, 2017
     * @Todo: map back model Schedule để update back lại, khi NV thu nợ đi thu tiền. Kế toán Thúc lên lịch
     */
    public function mapToModelSchedule($q, $mUser) {
        if(!isset($q->id) || empty($q->id)){
            return '';
        }
        $model = EmployeeCashbook::model()->findByPk($q->id);
        if(is_null($model)){
            return '';
        }
        $model->mAppUserLogin   = $mUser;
        $model->employee_id     = $mUser->id;
        $model->amount          = $this->amount;
        $model->note            = $this->note;
        $model->date_input      = date('Y-m-d');
        $model->sendSms         = true;// gửi tin báo cho KH
//        $model->uid_login       = $this->uid_login;// Sep0117 không nên overide biến này, khi tao thu tien tu lich thu se bi sai
        return $model;
    }
    
    /**
     * @Author: DungNT May 02, 2017
     * @Todo: report summary thu chi của các nhân viên, group by nhân viên
     */
    public function getRevenueByEmployee() {
        $criteria = new CDbCriteria();
        if(!empty($this->agent_id)){
            $criteria->addCondition('t.agent_id=' . $this->agent_id);
        }
        if(!empty($this->employee_id)){
            $criteria->addCondition('t.employee_id=' . $this->employee_id);
        }
        $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        $criteria->addBetweenCondition('t.date_input', $date_from, $date_to);
        
        $criteria->select = 'sum(t.amount) as amount, t.agent_id, t.employee_id, t.lookup_type';
        $criteria->group  = 't.employee_id, t.lookup_type';
        $models = EmployeeCashbook::model()->findAll($criteria);
        $aRes = [];
        foreach($models as $item){
            if(!isset($aRes[$item->employee_id]['amount_revenue'])){
                $aRes[$item->employee_id]['amount_revenue'] = 0;
            }
            if(!isset($aRes[$item->employee_id]['amount_cost'])){
                $aRes[$item->employee_id]['amount_cost'] = 0;
            }
             
            if($item->lookup_type == GasMasterLookup::MASTER_TYPE_THU){
                $aRes[$item->employee_id]['amount_revenue'] = $item->amount;
            }elseif($item->lookup_type == GasMasterLookup::MASTER_TYPE_CHI){
               $aRes[$item->employee_id]['amount_cost'] = $item->amount;
            }
        }
        return $aRes;
    }
    
    /**
     * @Author: DungNT May 16, 2017
     * @Todo: sửa lại sổ quỹ Bò Mối của đại lý trong 1 khoảng ngày
     */
    public function fixCashbookBoMoiInRangeDate() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.agent_id=' . $this->agent_id);
        $criteria->addBetweenCondition('t.date_input', $this->date_from, $this->date_to);
        $models = EmployeeCashbook::model()->findAll($criteria);
        foreach($models as $model){
            $mCashBookDetail = new GasCashBookDetail();
            $mCashBookDetail->makeRecordBomoi($model);// save vào sổ quỹ thu bò mối
        }
    }
    
    /********** FOR FILE HANDLE ************************/
    /** @Author: DungNT May 19, 2017
     * @Todo: get type required upload file
     */
    public function getTypeRequiredFile() {
        return [
            GasMasterLookup::ID_CHI_NOP_CONG_TY,
            GasMasterLookup::ID_CHI_GAS_DU,
        ];
    }
     /**
     * @Author: DungNT Oct 27, 2015
     * @Todo: Api validate multi file
     * @param: $this là model chứa error
     * ở đây là model GasUpholdReply
     */
    public function apiValidateFile() {
    try{
        $ok=false; // for check if no file submit
        if(isset($_FILES['file_name']['name'])  && count($_FILES['file_name']['name'])){
            foreach($_FILES['file_name']['name'] as $key=>$item){
                $mFile = new GasFile('UploadFile');
                $mFile->file_name  = CUploadedFile::getInstanceByName( "file_name[$key]");
                $mFile->validate();
                if(!is_null($mFile->file_name) && !$mFile->hasErrors() ){
                    $ok=true;
                    MyFormat::IsImageFile($_FILES['file_name']['tmp_name'][$key]);
//                    $FileName = MyFunctionCustom::remove_vietnamese_accents($mFile->file_name->getName());
//                    if(strlen($FileName) > 100 ){
//                        $mFile->addError('file_name', "Tên file không được quá 100 ký tự, vui lòng đặt tên ngắn hơn");
//                    }// không cần lấy file_name
                }
                if($mFile->hasErrors()){
                    $this->addError('file_name', $mFile->getError('file_name'));
                }
            }
        }
        
        if(!$ok && $this->requiredFile){
//            if($this->isNewRecord && in_array($this->master_lookup_id, $this->getTypeRequiredFile())) {
            if($this->isNewRecord && $this->lookup_type == GasMasterLookup::MASTER_TYPE_CHI ) {
                $this->addError('file_name', 'Có lỗi. Bạn phải chụp hình chứng từ của phiếu chi');
            }elseif(!$this->isNewRecord){// Jun 20, 2017 khi cập nhật, kiểm tra xem nếu chưa có hình thì yêu cầu phải up hình với phiếu chi
                if($this->lookup_type == GasMasterLookup::MASTER_TYPE_CHI){
                    if(count($this->rFile) < 1){
                        $this->addError('file_name', 'Có lỗi. Bạn phải chụp hình chứng từ của phiếu chi');
                    }
                }
            }
        }
        
    } catch (Exception $ex) {
        throw new Exception($ex->getMessage());
    }
    }
    
    /** @Author: DungNT May 18, 2017
     * @Todo: save record file if have
     */
    public function saveFile() {
        // 5. save file
        if(!is_null($this->id)){
            GasFile::ApiSaveRecordFile($this, GasFile::TYPE_8_CASHBOOK);
        }
    }
    public function deleteFile() {
        if(!is_array($this->list_id_image) || count($this->list_id_image) < 1){
            return ;
        }
        if(!$this->isNewRecord && in_array($this->master_lookup_id, $this->getTypeRequiredFile())) {
            throw new Exception('Có lỗi. Bạn không được xóa hình chứng từ của phiếu chi');
        }
        
        foreach($this->list_id_image as $file_id){
            $mFile = GasFile::model()->findByPk($file_id);
            if(is_null($mFile)){
                continue;
            }
            if($mFile->type == GasFile::TYPE_8_CASHBOOK){
                $mFile->delete();
            }
        }
    }
    /** @Author: DungNT May 27, 2016
    *   @Todo: get html file view on grid
    */
    public function getFileOnly() {
        $res = '';
        $cmsFormater = new CmsFormatter();
        $res .= $cmsFormater->formatBreakTaskDailyFile($this, array('width'=>30, 'height'=>30));
        return $res;
    }
    /********** FOR FILE HANDLE ************************/
    
    /**
     * @Author: DungNT May 21, 2017
     * @Todo: tự động cập nhật quỹ hộ GĐ khi bấm update
     */
    public function fixCashbookHgdByDate() {
        return; // May 25, 2017 cập nhật thế này sẽ gây sai quỹ cho các đại lý đã đưa quỹ về = 0
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        if($cRole != ROLE_ADMIN){
            return ;
        }
        $mCashbookCheck = new GasCashBookDetail();
        $mCashbookCheck->agent_id           = $this->agent_id;
        $mCashbookCheck->release_date       = MyFormat::dateConverYmdToDmy($this->date_input, 'd-m-Y');
        $model = new GasCashBookDetail();
        $model->makeRecordHgdAllAgent([$mCashbookCheck->agent_id],  $mCashbookCheck->release_date);
//        Logger::WriteLog("debug fix cashbook: ". $this->date_input);
    }
    
    public function getListdataBank() {
        return Users::getArrDropdown(ROLE_BANK);
    }
    public function getListdataEmployee() {
        return Users::getArrDropdown(ROLE_DEBT_COLLECTION);
    }
    public function getBank() {
        $mUser = $this->rBank;
        if($mUser){
            return $mUser->getFullName();
        }
        return "";
    }
    
    public function showFieldBank() {
        $ok = false;
        if($this->type == EmployeeCashbook::TYPE_BANK_TRANSFER){
            $ok = true;
        }
        return $ok;
    }
    public function showFieldAmount() {
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole == ROLE_ADMIN){
            return true;
        }
        $ok = false;
        $aTypeShow = [EmployeeCashbook::TYPE_DAILY, EmployeeCashbook::TYPE_BANK_TRANSFER, EmployeeCashbook::TYPE_IN_OFFICE];
        if(in_array($this->type, $aTypeShow)){
            $ok = true;
        }
        return $ok;
    }
    public function showFieldAmountSchedule() {
        $ok = false;
        $aTypeShow = [EmployeeCashbook::TYPE_SCHEDULE];
        if(in_array($this->type, $aTypeShow)){
            $ok = true;
        }
        return $ok;
    }
    public function showFieldEmployee() {
        $ok = false;
        $aTypeShow = [EmployeeCashbook::TYPE_DAILY, EmployeeCashbook::TYPE_SCHEDULE, EmployeeCashbook::TYPE_IN_OFFICE];
        if(in_array($this->type, $aTypeShow)){
            $ok = true;
        }
        return $ok;
    }
    public function showSelectEmployee() {
        $ok = false;
        $aTypeShow = [EmployeeCashbook::TYPE_SCHEDULE];
        if(in_array($this->type, $aTypeShow)){
            $ok = true;
        }
        return $ok;
    }
    public function resetScenario() {
        $this->scenario = "WebCreate$this->type";
    }
    public function showFieldAgent() {
        return true;// cho all user thấy
        $ok = false;
        $aTypeShow = [EmployeeCashbook::TYPE_DAILY];
        if(in_array($this->type, $aTypeShow)){
            $ok = true;
        }
        return $ok;
    }
    public function showFieldType() {
        $ok = false;
        $aTypeShow = [EmployeeCashbook::TYPE_SCHEDULE, EmployeeCashbook::TYPE_BANK_TRANSFER, EmployeeCashbook::TYPE_IN_OFFICE];
        if(in_array($this->type, $aTypeShow)){
            $ok = true;
        }
        return $ok;
    }
    public function showMultiInput() {
        $ok = true;
        if($this->type == EmployeeCashbook::TYPE_SCHEDULE){
            $ok = false;
        }
        return $ok;
    }
    public function getEmployeeGrid() {
        $aTypeShow = [EmployeeCashbook::TYPE_DAILY, EmployeeCashbook::TYPE_SCHEDULE, EmployeeCashbook::TYPE_IN_OFFICE];
        if(in_array($this->type, $aTypeShow)){
            return $this->getEmployee();
        }
        return $this->getBank();
    }
    public function getEmployeeCodeAccount() {
        $aTypeShow = [EmployeeCashbook::TYPE_DAILY, EmployeeCashbook::TYPE_SCHEDULE, EmployeeCashbook::TYPE_IN_OFFICE];
        if(in_array($this->type, $aTypeShow)){
            return $this->getEmployee('code_account');
        }
        return '';
    }
    
    public function notifyEmployee() {
        if($this->type != EmployeeCashbook::TYPE_SCHEDULE){
            return ;
        }
        $aUid = [GasLeave::THUC_NH, GasConst::UID_BINH_TQ];
        if(empty($this->titleNotify)){
            $this->titleNotify = "{$this->getEmployee()} [{$this->getLookupTypeText()}]: {$this->getAmount(true)} {$this->getCustomer()}";
        }
        $this->runInsertNotify($aUid, false); // Send By Cron Feb 23, 2017
    }
 
    /** @Author: DungNT Sep 01, 2017
     * @Todo: insert to table notify
     * @Param: $aUid array user id need notify
     */
    public function runInsertNotify($aUid, $sendNow = false) {
        $json_var = array();
        foreach($aUid as $uid) {
            if( !empty($uid) ){
                $mScheduleNotify = GasScheduleNotify::InsertRecord($uid, GasScheduleNotify::CUSTOMER_PAY_DEBIT, $this->id, "", $this->titleNotify, $json_var);
                if($sendNow && !is_null($mScheduleNotify)){
                    $mScheduleNotify->sendImmediateForSomeUser();
                }// Jan 05, 2016 tạm close send luôn lại, vì nó gây chậm bên PMBH khi send // sẽ gửi = cron
            }
        }
    }

    public function getUrlViewOrderApp() {
        if(empty($this->app_order_id)){
            return '';
        }
        $url = Yii::app()->createAbsoluteUrl('admin/gasAppOrder/view', ['id'=>$this->app_order_id]);
        return "<i><a href='$url' target='_blank'>Xem ĐH</a></i>";
    }
    
    public function saveEvent($action_type) {
        $mEvent = new TransactionEvent();
        $mEvent->type                       = TransactionEvent::TYPE_CASHBOOK;
        $mEvent->transaction_history_id     = $this->id;
        $mEvent->user_id                    = MyFormat::getCurrentUid();
        $mEvent->action_type                = $action_type;
        $mEvent->json_data                  = MyFormat::jsonEncode($this->getAttributes());
        $mEvent->save();
    }
    
    /** @Author: DungNT Oct 12, 2017
     * @Todo: save multi record on web create
     */
    public function saveMulti() {
        if(!is_array($_POST['customer_id'])){
            throw new Exception('Dữ liêu không hợp lệ');
        }
        $ok = false;
        $this->convertDate();
        foreach($_POST['customer_id'] as $key => $customer_id):
            $amount = isset($_POST['list_amount'][$key]) ? $_POST['list_amount'][$key] : 0;
            $note   = isset($_POST['list_note'][$key]) ? $_POST['list_note'][$key] : '';
            if(empty($customer_id) || empty($amount)){
                continue ;
            }
            $ok = true;;
            $model                  = new EmployeeCashbook();
            $model->type            = $this->type;
            $model->date_input      = $this->date_input;
            $model->agent_id        = $this->agent_id;
            $model->master_lookup_id = $this->master_lookup_id;
            if($model->type == EmployeeCashbook::TYPE_SCHEDULE){
                $model->amount_schedule = $amount;
            }else{
                $model->amount      = $amount;
            }
            $model->customer_id     = $customer_id;
            $model->note            = $note;
            $model->uid_login       = $this->uid_login;
            $model->employee_id     = $this->employee_id;
            $model->bank_id         = $this->bank_id;

            $model->validate();
            $model->save();
            $model->updateCashbookAgent();
            $model->doFlowChiTreoCongNo();
            $model->saveFileOnCreateMuti();
        endforeach;

        if(!$ok){
            $this->addError('id', 'Có lỗi: Không có khách hàng nào được thêm mới');
        }
    }
    
    /** @Author: DungNT May 22, 2019
     *  @Todo: xử lý save file khi tạo 1 record ở màn hình  tạo multi, chưa xử lý file cho từng row của KH đc
     *  
     **/
    public function saveFileOnCreateMuti() {
        $mGasFile = new GasFile();
        $mGasFile->saveRecordFile($this, GasFile::TYPE_8_CASHBOOK);
    }
    
    /** @Author: DungNT Now 19, 2017
     *  @Todo: lấy những record chi nộp NH hoặc nộp dùm NH
     **/
    public function getRecordGoBank($needMore = []) {
        $today = date('Y-m-d');
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.date_input='$today'");
        $criteria->addCondition('t.type='.EmployeeCashbook::TYPE_DAILY);
        if(isset($needMore['ArrAgentIdNotFind'])){
            $sParamsIn = implode(',', $needMore['ArrAgentIdNotFind']);
            $criteria->addCondition("t.agent_id NOT IN ($sParamsIn)");
        }
        $aTypeLookUp = [GasMasterLookup::ID_CHI_NOP_CONG_TY, GasMasterLookup::ID_CHI_NOP_DUM];
        $sParamsIn = implode(',', $aTypeLookUp);
        $criteria->addCondition("t.master_lookup_id IN ($sParamsIn)");
        return EmployeeCashbook::model()->findAll($criteria);
    }
    
    /** @Author: DungNT Feb 06, 2018
     *  @Todo: lấy số đt setup để nhân tin thu công nợ của KH
     **/
    public function getSetupPhoneOfCustomer() {
        $model = new UsersPhoneExt2();
        $model->user_id = $this->customer_id;
        $aPhone = $model->getPhoneOfCustomerByType(UsersPhoneExt2::TYPE_PAY_DEBIT);
        $this->checkSetupPhone($aPhone);
        return $aPhone;
    }
    // alert mail nếu KH chưa được setup số đt nhận công nợ
    public function checkSetupPhone($aPhone) {
        if(count($aPhone)){
            return ;
        }
        $mCustomer      = Users::model()->findByPk($this->customer_id);
        $bodyMail       = "<b>Khách hàng dưới đây chưa setup số điện thoại thu tiền công nợ, đề nghị bộ phận liên quan bổ sung</b>";
        $bodyMail       .= "<br><br><br>";
        $bodyMail       .= "<br><b>KH: $mCustomer->first_name</b>";
        $bodyMail       .= "<br>Đ/C: $mCustomer->address";
        $bodyMail       .= "<br>NV kinh doanh: {$mCustomer->getSaleName()}";
        $bodyMail       .= "<br><br><br><br><b>Gửi mail cho Sale Admin Hà để bổ sung: hapt@spj.vn</b>";
        $needMore['title']      = 'KH chưa setup số điện thoại thu tiền công nợ';
//        $needMore['list_mail']  = ['dungnt@spj.vn'];
        $needMore['list_mail']  = ['dungnt@spj.vn', 'kinhdoanh@spj.vn', 'thucnh@spj.vn'];
        SendEmail::bugToDev($bodyMail, $needMore);
    }
    
    /** @Author: DungNT Feb 06, 2018
     *  @Todo: gửi tin thông báo tiền thu đc của KH
     **/
    public function doSendSms() {
        if(!$this->sendSms){
            return;
        }
        $aPhone     = $this->getSetupPhoneOfCustomer();
        $mCustomer  = Users::model()->findByPk($this->customer_id);
        foreach($aPhone as $phone){
            $mScheduleSms = new GasScheduleSms();
            $mScheduleSms->phone = $phone;
            $mScheduleSms->announceBoMoiPayDebit($mCustomer, $this);
        }
    }
    
    /** @Author: DungNT Apr 03, 2018
     *  @Todo: check số tiền chi ra phải nhỏ hơn số tiền tồn, không được chi dư
     **/
    public function checkAmountGoBank() {
        $aRoleNotCheck = [ROLE_DRIVER, ROLE_DEBT_COLLECTION];
        if(empty($this->master_lookup_id) || $this->rMasterLookup->type == GasMasterLookup::MASTER_TYPE_THU || in_array($this->mAppUserLogin->role_id, $aRoleNotCheck)){
            return ;
        }
        // get tồn tiền của NV
        $mCashBookDetail = new GasCashBookDetail();
        $mCashBookDetail->employee_id = $this->employee_id;
        $mCashBookDetail->date_from = date('d-m-Y');
        $mCashBookDetail->date_to   = $mCashBookDetail->date_from;
        $amountClosing              = $mCashBookDetail->getClosingOneUser();
        $totalClosingBeforeUpdate   = $this->oldAmount + $amountClosing;
        $msg                        = 'Không được chi vượt số tiền trong quỹ của bạn, kiểm tra đơn hàng chưa hoàn thành';
        if($this->isNewRecord && $this->amount > $amountClosing){
//            throw new Exception($msg);
            $this->addError('id', $msg);
        }elseif (!$this->isNewRecord && $this->amount > $totalClosingBeforeUpdate) {
//            throw new Exception($msg);
            $this->addError('id', $msg);
        }
    }
    
    /** @Author: DungNT Jan 25, 2019
     *  @Todo: lam chi Treo Cong no tren web cho KTKV, không để PVKH chi nữa
     **/
    public function doFlowChiTreoCongNo() {
        $this->deleteGasDebt();
        if(!in_array($this->master_lookup_id, $this->getTypeInsertDebit()) || $this->type != EmployeeCashbook::TYPE_IN_OFFICE){
            return ;
        }
        
        $mGasDebts = new GasDebts();
        $mGasDebts->createFromEmployeeCashbook($this, 1);
    }
    
    /** @Author: DungNT Apr 05, 2019
     *  @Todo: delete record old insert to GasDebts 
     **/
    public function deleteGasDebt() {
        $mGasDebts = new GasDebts();
        $mGasDebts->relate_id    = $this->id;
        $mGasDebts->type         = GasDebts::TYPE_DEBIT_PVKH;
        $mGasDebts->deleteByRelateId();
    }
    
    /** @Author: DungNT Feb 19, 2019
     *  @Todo: check loại thu tiền KH bò mối thì gửi SMS thông báo cho KH
     *  nếu PVKH làm nợ xong làm 1 phiếu thu khác trên app
     **/
    public function setNotifySmsToCustomer() {
        if($this->master_lookup_id == GasMasterLookup::ID_THU_TIEN_KH){
            $this->sendSms         = true;// gửi tin báo cho KH
        }
    }
    
    /** @Author: DungNT Jan 16, 2019
     *  @Todo: get số date cho phép chọn ở date picket
     **/
    public function getDayLimit() {
        $cUid       = MyFormat::getCurrentUid();
        $aUidAllow  = GasConst::getArrUidUpdateAll();
        $aUidAllow[] = GasLeave::THUC_NH;
        $day = 3;
        if($cUid == GasConst::UID_ADMIN){
            $day = 180;
        }elseif(in_array($cUid, $aUidAllow)){
            $day = Yii::app()->params['DaysUpdateFixAll'];
        }
        return $day*-1;
    }
    
    /** @Author: DungNT May 22, 2019
     *  @Todo: check user can create multi
     **/
    public function canCreateMulti() {
        $cUid       = MyFormat::getCurrentUid();
        $aUidAllow  = GasConst::getArrUidUpdateAll();
        $aUidAllow[] = GasLeave::THUC_NH;
        $aUidAllow[] = GasConst::UID_ADMIN;
        return in_array($cUid, $aUidAllow);
    }
    
    /** @Author: DungNT May 22, 2019
     *  @Todo: check can delete file
     **/
    public function canDeleteFile() {
        $cUid           = MyFormat::getCurrentUid();
        $aUidAllow[]    = GasConst::UID_ADMIN;
        return in_array($cUid, $aUidAllow);
    }
    
    /** @Author: DungNT May 22, 2019
     *  @Todo: check required file upload
     **/
    public function isRequiredFileUpload() {
        $cUid           = MyFormat::getCurrentUid();
        $cRole          = MyFormat::getCurrentRoleId();
        $aRoleRequired  = [ROLE_ACCOUNTING_ZONE, ROLE_MONITORING_MARKET_DEVELOPMENT];
        return in_array($cRole, $aRoleRequired);
    }
    
    /** @Author: DungNT May 22, 2019
     *  @Todo: check required file upload for some user
     **/
    public function validateFileOnCreate() {
        $mFile = new GasFile();
        if($this->isRequiredFileUpload() && $this->lookup_type == GasMasterLookup::MASTER_TYPE_CHI){
            $mFile->checkEmpty = true;
        }
        $mFile->webValidateFile($this, 'file_name');
    }
}