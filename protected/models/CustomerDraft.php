<?php

/**
 * This is the model class for table "{{_customer_draft}}".
 *
 * The followings are the available columns in table '{{_customer_draft}}':
 * @property string $id
 * @property string $name
 * @property string $phone
 * @property string $address
 * @property string $employee_id
 * @property string $customer_id
 * @property string $owner_id
 * @property integer $status
 * @property integer $type
 * @property integer $status_cancel
 * @property string $sell_created_date
 * @property string $sell_id
 * @property string $appointment_date
 * @property string $created_date
 */
class CustomerDraft extends BaseSpj {

    public $nameCheckExcel = 'EXCEL';
    public $nameCheckDatabase = 'DATABASE';
    public $nameCount = 'COUNTACCEP';
    public $autocomplete_owner, $is_buy_complete, $pageSize,$generate,
            $file_excel, $type_search, $date_call_from, $date_call_to
            , $sell_created_date_from, $sell_created_date_to
            , $last_purchase_from, $last_purchase_to, $customer;
    public $called = 2; // đã gọi
    public $not_call = 3; // Chưa gọi
    public $call_again = 4; // Gọi lại lần nữa
    public $TYPE_TELESALE_FIND  = 1; // Nhan viên telesale tìm
    public $TYPE_BUY            = 2;
    public $date_from, $date_to, $autocomplete_name_3;
    public $countGenerate = 100;
    
    
    /** @Author: HOANG NAM 28/06/2018
     *  @Todo: get array status
     * */
    public function getArrayCallStatus() {
        return [
            Telesale::STT_PHONE_NOT_CONTACT     => 'Thuê bao không liên lạc',
            Telesale::STT_CUSTOMER_OLD          => 'KH cũ cty',
            Telesale::STT_CUSTOMER_ADD_NEW      => 'Đã tạo KH telesale',
            Telesale::STT_WRONG_NAME            => 'Sai tên',
            Telesale::STT_WRONG_ADDRESS         => 'Sai địa chỉ',
            Telesale::STT_NOT_USE_GAS           => 'Không sử dụng gas',
            Telesale::STT_NOT_ANSWER            => 'Không nghe máy',
        ];
    }
    
    /** @Author: HOANG NAM 02/07/2018
     *  @Todo: get array type
     **/
    public function getArrayType(){
        return array(
            $this->TYPE_BUY             => 'Marketing',
            $this->TYPE_TELESALE_FIND   => 'NV Telesale',
        );
    }
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CustomerDraft the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{_customer_draft}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [
            array('owner_id, type, district_id, province_id', 'required', 'on' => 'create, update'),
            array('phone', 'required', 'on' => 'UpdatePhone'),
            array('type,generate,sell_created_date,pageSize,sell_created_date_to,sell_created_date_from,customer,last_purchase_to,last_purchase_from,appointment_date, is_buy_complete,date_call_to,date_call_from', 'safe'),
            ['district_id, province_id, id, name, phone, address, employee_id, customer_id, owner_id, status, status_cancel, sell_created_date, sell_id, appointment_date, created_date', 'safe'],
            ['date_from, date_to, agent_id', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return [
            'rOwner' => array(self::BELONGS_TO, 'Users', 'owner_id'),
            'rEmployee' => array(self::BELONGS_TO, 'Users', 'employee_id'),
            'rProvince' => array(self::BELONGS_TO, 'GasProvince', 'province_id'),
            'rDistrict' => array(self::BELONGS_TO, 'GasDistrict', 'district_id'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'type' => 'Loại',
            'name' => 'Tên khách hàng',
            'phone' => 'SDT',
            'address' => 'Địa chỉ',
            'employee_id' => 'Nhân viên',
            'customer_id' => 'Khách hàng',
            'owner_id' => 'Telesale source',
            'status' => 'Trạng thái',
            'status_cancel' => 'Lý do hủy',
            'sell_created_date' => 'Ngày mua hàng',
            'sell_id' => 'Hóa đơn',
            'appointment_date' => 'Ngày hẹn',
            'created_date' => 'Ngày tạo',
            'created_date_only' => 'Ngày tạo',
            'file_excel' => 'File excel',
            'date_call_from' => 'Hẹn gọi lại từ',
            'date_call_to' => 'Đến',
            'sell_created_date_to' => 'Đến',
            'sell_created_date_from' => 'ĐH đầu tiên từ',
            'generate' => 'Gán tự động',
            'district_id' => 'Quận huyện',
            'province_id' => 'Tỉnh',
            'agent_id' => 'Đại lí',
            'date_from' => 'Từ ngày',
            'date_to' => 'Đến ngày'
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        $criteria = new CDbCriteria;
        $this->setTypeSearch($criteria);
//        set trạng thái mua và chưa mua
        if (!empty($this->is_buy_complete)) {
            switch ($this->is_buy_complete) {
                case Telesale::STT_BUY_COMPLETE:
                    $criteria->addCondition('t.sell_created_date IS NOT NULL');
                    break;
                case Telesale::STT_BUY_UNCOMPLETE:
                    $criteria->addCondition('t.sell_created_date IS NULL');
                    break;
            }
        }
//        set ngày hẹn gọi lại
        if (!empty($this->date_call_from)) {
            $dateFrom = MyFormat::dateConverDmyToYmd($this->date_call_from, '-');
            $criteria->addCondition('t.appointment_date >= "' . $dateFrom . '"');
        }
        if (!empty($this->date_call_to)) {
            $dateTo = MyFormat::dateConverDmyToYmd($this->date_call_to, '-');
            $criteria->addCondition('t.appointment_date <= "' . $dateTo . '"');
        }
//        set ngày đơn hàng đầu tiên
        if (!empty($this->sell_created_date_from)) {
            $dateFromSell = MyFormat::dateConverDmyToYmd($this->sell_created_date_from, '-');
            $criteria->addCondition('t.sell_created_date >= "' . $dateFromSell . '"');
        }
        if (!empty($this->sell_created_date_to)) {
            $dateToSell = MyFormat::dateConverDmyToYmd($this->sell_created_date_to, '-');
            $criteria->addCondition('t.sell_created_date <= "' . $dateToSell . '"');
        }
        $criteria->compare('t.name', $this->name, true);
        if(!empty($this->phone)){
            $criteria->compare('t.phone', $this->phone*1);
        }
        $criteria->compare('t.district_id', $this->district_id);
        $criteria->compare('t.province_id', $this->province_id);
        $criteria->compare('t.type', $this->type);
        $criteria->compare('t.employee_id', $this->employee_id);
        $criteria->compare('t.customer_id', $this->customer_id);
        $criteria->compare('t.owner_id', $this->owner_id);
        if (!empty($this->status) && $this->status == Telesale::STT_UPDATE_STATUS) {
            $criteria->addCondition('t.status IS NULL');
        } else {
            $criteria->compare('t.status', $this->status);
        }
        $criteria->compare('t.status_cancel', $this->status_cancel);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => !empty($this->pageSize) ? $this->pageSize : Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    /** @Author: HOANG NAM 28/06/2018
     *  @Todo: set type search
     * */
    public function setTypeSearch(&$criteria) {
        switch ($this->type_search) {
            case $this->called: // Đã gọi
//                $criteria->addCondition('t.status IS NOT NULL');
                $criteria->addCondition('t.status > 0');
                $criteria->addCondition('t.employee_id > 0');
                break;
            case $this->not_call: // chưa gọi thi status = null
                $criteria->addCondition('t.status IS NULL OR t.status <= 0');
                $criteria->addCondition('t.employee_id > 0');
                break;
            case $this->call_again: // Gọi lại hôm nay
                $criteria->addCondition('t.appointment_date = "' . date('Y-m-d') . '"');
                $criteria->addCondition('t.employee_id > 0');
                break;
            default: // mmặt định là chưa nhận
                $criteria->addCondition('t.employee_id <= 0');
                break;
        }
    }

    /** @Author: HOANG NAM 28/06/2018
     *  @Todo: save model from excel
     * */
    public function saveFromExcel() {
        $aError = [];
        $aError[$this->nameCheckExcel] = [];
        $aError[$this->nameCheckDatabase] = [];
        $aError[$this->nameCount] = 0;
        try {
            $from = time();
            $FileName = $_FILES['CustomerDraft']['tmp_name']['file_excel'];
            if (empty($FileName))
                return;
            Yii::import('application.extensions.vendors.PHPExcel', true);
            $inputFileType = PHPExcel_IOFactory::identify($FileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel    = $objReader->load(@$_FILES['CustomerDraft']['tmp_name']['file_excel']);
            $objWorksheet   = $objPHPExcel->getActiveSheet();
            $highestRow     = $objWorksheet->getHighestRow(); // e.g. 10
            if($highestRow > 10000){
                die("$highestRow row can not import, recheck file excel");
            }
//            echo $highestRow;die;
            $to = time();
            $second = $to-$from;
            $info = "Import $highestRow row done in: ".($second).'  Second  <=> '.($second/60).' Minutes';
            Logger::WriteLog($info);
            
            $aRowInsert = [];
            $employee_id = 0;
            if($this->type == $this->TYPE_TELESALE_FIND){
                $employee_id = $this->owner_id;
            }
            $aTemp = []; $date = date('Y-m-d');$created_date = date('Y-m-d H:i:s');
            $aPrefix = UpdateSql1::getArrayPrefixChangeNotZero();
            for ($row = 2; $row <= $highestRow; ++$row) {
                $customer_id = $status = 0;
                // file excel cần format column theo dạng text hết để người dùng nhập vào khi đọc không lỗi
                $customer_name      = MyFormat::removeBadCharacters($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
                $customer_phone     = MyFormat::removeBadCharacters($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
                $customer_address   = MyFormat::removeBadCharacters($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());
                $this->convertToPhoneNumber($customer_phone);
                if(empty($customer_phone)){
                    continue ;
                }
                $fieldUpdate = $customer_phone;
                $prefix_code = substr($fieldUpdate, 0, 3);
                if(isset($aPrefix[$prefix_code])){
                    $charRemove = strlen($prefix_code);
                    $customer_phone = $aPrefix[$prefix_code].substr($fieldUpdate, $charRemove);
                }
                
                if(!$this->checkDuplicate($customer_id,$aTemp,$aError[$this->nameCheckExcel],$aError[$this->nameCheckDatabase],$customer_phone) || $customer_id > 0){
                    if($customer_id > 0){
                       $status  = Telesale::STT_CUSTOMER_OLD; 
                    }
                    $aRowInsert[] = "(
                        '{$customer_name}',
                        '{$customer_phone}',
                        '{$customer_address}',
                        '{$this->owner_id}',
                        '{$this->district_id}',
                        '{$this->province_id}',
                        '{$this->type}',
                        '{$employee_id}',
                        '$date',
                        '$created_date',
                        '$customer_id',
                        '{$status}'
                        )";
                }

            }
        
            $tableName = CustomerDraft::model()->tableName();
            $sql = "insert into $tableName (
                            name,
                            phone,
                            address,
                            owner_id,
                            district_id,
                            province_id,
                            type,
                            employee_id,
                            created_date_only,
                            created_date,
                            customer_id,
                            status
                            ) values " . implode(',', $aRowInsert);
            if (count($aRowInsert)){
                $aError[$this->nameCount] = count($aRowInsert);
                Yii::app()->db->createCommand($sql)->execute();
            }
        } catch (Exception $e) {
            Yii::log("Uid: " . Yii::app()->user->id . "Exception " . $e->getMessage(), 'error');
            $code = 404;
            throw new CHttpException($code, $e->getMessage());
        }
        return $aError;
    }

    /** @Author: HOANG NAM 27/06/2018
     *  @Todo: action get
     * */
    public function getName() {
        return $this->name;
    }
    public function getPhone() {
        return '0'.$this->phone;
    }
    public function getPhoneClickCall() {
        $mCallClick = new Call();
        return $mCallClick->buildUrlMakeCall($this->getPhone());
    }
    public function getAddress() {
        return $this->address;
    }
    public function getEmployee($field = 'first_name') {
        $employee = $this->rEmployee;
        return !(empty($employee)) ? $employee->$field : '';
    }
    public function getProvince() {
        $model = $this->rProvince;
        return !(empty($model)) ? $model->name : '';
    }
    public function getDistrict() {
        $model = $this->rDistrict;
        return !(empty($model)) ? $model->name : '';
    }
    public function getDistrictView() {
        return $this->getDistrict().', '.$this->getProvince();
    }

    /** @Author: HOANG NAM 27/06/2018
     *  @Todo: action get
     * */
    public function getOwner($field = 'first_name') {
        $employee = $this->rOwner;
        return !(empty($employee)) ? $employee->$field : '';
    }

    /** @Author: HOANG NAM 27/06/2018
     *  @Todo: action get
     * */
    public function getStatus() {
        $aStatus = $this->getArrayCallStatus();
        return !empty($aStatus[$this->status]) ? $aStatus[$this->status] : '';
    }

    /** @Author: HOANG NAM 27/06/2018
     *  @Todo: action get
     * */
    public function getStatusCancel() {
        $aStatusCancel = $this->getArrayStatusCancel();
        return !empty($aStatusCancel[$this->status_cancel]) ? $aStatusCancel[$this->status_cancel] : '';
    }

    /** @Author: HOANG NAM 27/06/2018
     *  @Todo: action get
     * */
    public function getSellCreated($fomat = 'd/m/Y') {
        return MyFormat::dateConverYmdToDmy($this->sell_created_date, $fomat);
    }

    /** @Author: HOANG NAM 27/06/2018
     *  @Todo: action get
     * */
    public function getAppointment($fomat = 'd/m/Y') {
        return MyFormat::dateConverYmdToDmy($this->appointment_date, $fomat);
    }

    /** @Author: HOANG NAM 28/06/2018
     *  @Todo: get array buy status
     * */
    public function getArrayStatusBuy() {
        $mTelesale = new Telesale();
        $aRes = $mTelesale->getArrayStatusBuy();
        return $aRes;
    }

    /** @Author: HOANG NAM 28/06/2018
     *  @Todo: cancel status
     * */
    public function getArrayStatusCancel() {
        $mTelesale = new Telesale();
        $aRes = $mTelesale->getArrayStatusCancel();
        return $aRes;
    }

    /** @Author: HOANG NAM 28/06/2018
     *  @Todo: get list employee
     * */
    public function getArrayEmployee() {
        $mTelesale = new Telesale();
        $aRes = $mTelesale->getArrayEmployee();
        return $aRes;
    }

    /** @Author: HOANG NAM 28/06/2018
     *  @Todo: xét có thể update không xét hoặc xét cho toàn bộ khách hàng
     * */
    public function canUpdateApply() {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        if ($cRole == ROLE_ADMIN) {
            return true;
        }
        if ($cUid == $this->employee_id) {
            return true;
        }
        return false;
    }

    /** @Author: HOANG NAM 26/06/2018
     *  @Todo: can modify
     * */
    public function canModify() {
        return $this->employee_id <= 0 ? false : true;
    }

    /** @Author: HOANG NAM 27/06/2018
     *  @Todo: can delete
     * */
    public function canDelete() {
        $cRole = MyFormat::getCurrentRoleId();
        if ($cRole == ROLE_ADMIN) {
            return true;
        }
    }

    /** @Author: HOANG NAM 29/06/2018
     *  @Todo: get url create 
     * */
    public function getUrlDetail($isOnlyView = false) {
        return $isOnlyView ?
                Yii::app()->createAbsoluteUrl('admin/customerDraftDetail/create', array('ajax' => 1, 'OnlyView' => 1, 'CustomerDraft' => $this->id)) :
                Yii::app()->createAbsoluteUrl('admin/customerDraftDetail/create', array('ajax' => 1, 'CustomerDraft' => $this->id));
    }
    
    /** @Author: HOANG NAM 29/06/2018
     *  @Todo: get actuon to detail
     **/
    public function getActions(){
        $result      = '';
        if($this->canUpdateApply() && $this->canCreateNote()){
            $result     .= '<a class=\'targetHtml\' href=\''.$this->getUrlDetail().'\'>Tạo mới ghi chú</a>';
            $result     .= '<br>';
        }
        if($this->canViewNote()){
            $result     .= '<a class=\'targetHtml\' href=\''.$this->getUrlDetail(true).'\'>Xem ghi chú</a>';
        }
        return $result;
    }
    
    /** @Author: HOANG NAM 30/06/2018
     *  @Todo: get url create customer
     **/
    public function getUrlCreateCustomer(){
        return Yii::app()->createAbsoluteUrl('admin/ajax/iframeCreateCustomerHgd',array('sale_id' => MyFormat::getCurrentUid(), 'phone_number' => $this->getPhone(),'first_name' => $this->getName() ,'CustomerDraft'=>$this->id));
    }
    
    /** @Author: HOANG NAM 02/06/2018
     *  @Todo: update customerDraft sell
     **/
    public function updateSellDate($mSell){
        $this->customer_id = $mSell->customer_id;
        if($mSell->first_order == Sell::IS_FIRST_ORDER){
            $criteria = new CDbCriteria;
            $criteria->compare('t.customer_id', $this->customer_id);
            $criteria->addCondition('t.sell_id <= 0');
            $mCusDraft = CustomerDraft::model()->find($criteria);
            if(!empty($mCusDraft)){
                $mCusDraft->sell_id = $mSell->id;
                $mCusDraft->sell_created_date = $mSell->created_date_only;
                $mCusDraft->update();
            }
        }
    }
    
    /** @Author: HOANG NAM 02/07/2018
     *  @Todo: get type
     **/
    public function getType(){
        $aType = $this->getArrayType();
        return isset($aType[$this->type]) ?  $aType[$this->type] : '';
    }
    
    /** @Author: HOANG NAM 02/07/2018
     *  @Todo: action check can create employee
     **/
    public function canCreateNote(){
        return $this->customer_id > 0 || $this->employee_id <= 0 ? FALSE : TRUE;
    }
    
    /** @Author: HOANG NAM 02/06/2018
     *  @Todo: action check can view employee
     **/
    public function canViewNote(){
        return !empty($this->status) ? TRUE : FALSE;
    }
    
    /** @Author: ANH DUNG Jul 05, 2018
     *  @Todo: handle some format data before save
     **/
    public function formatDataSave() {
        $this->phone = $this->phone*1;
    }
    
    /** @Author: HOANG NAM 02/07/2018
     *  @Todo: update customer id
     **/
    public function updateCustomer($mUser,$idCustomerDraft){
        $mCusDraf = CustomerDraft::model()->findByPk($idCustomerDraft);
        $mCusDraf->customer_id= $mUser->id;
        $mCusDraf->update();
    }
    
    /** @Author: DungNT Jan 16, 2019 
     *  @Todo: tạo record nhận gọi cho Telesale luôn
     **/
    public function autoGenRecordTelesaleApply($mCustomer) {
        if(empty($mCustomer->sale_id)){
            return ;
        }
        $mTelesale = new Telesale();
        $_POST['Telesale']['customer'] = [$mCustomer->id];
        $aIdCustomer    = $_POST['Telesale']['customer'];
        $sale_id        = $mCustomer->sale_id;
        $mTelesale->saveOneManyBig($sale_id, $aIdCustomer);
    }
    
    public function generateCusForEmployee(){
        $criteria = new CDbCriteria;
        $criteria->limit = $this->countGenerate;
        $criteria->addCondition('t.employee_id <= 0');
        $tableName = CustomerDraft::model()->tableName();
        $sql = "update {$tableName} set employee_id = {$this->employee_id} where employee_id <= 0 LIMIT {$this->countGenerate}; ";
        Yii::app()->db->createCommand($sql)->execute();
    }
    
    /** @Author: HOANG NAM 04/07/2018
     *  @Todo: remove all character # 1-9
     **/
    public function convertToPhoneNumber(&$string){
        if(strlen($string) >= 11){ // Bỏ trường hợp đầu số 84 mà 10 số, sym vina
            $string = $this->removeFirstString($string,'84');// xóa mã vùng
        }
        $string = preg_replace("/[^0-9,.]/", "", $string);
        $string = UsersPhone::removeLeftZero($string);
        $string =  (int)$string * 1;
    }
    
    public function removeFirstString($string,$stringRemove){
        $lenght = strlen($stringRemove);
        $strL = substr($string,0,$lenght);
        if($strL == $stringRemove){
            return substr($string,$lenght);
        }
        return $string;
    }
    
    /** @Author: HOANG NAM 04/07/2018
     *  @Todo: check is duplicate
     **/
    public function checkDuplicate(&$customer_id,&$aTemp,&$aExcel,&$aDatabase,$phoneNumber){
//        Check in excel
        if(in_array($phoneNumber, $aTemp)){
            $aExcel[]       = $phoneNumber;
            return true;
        }
        $aTemp[]            = $phoneNumber;
//        check  customer draft
        $criteria           = new CDbCriteria;
        $criteria->compare('t.phone',$phoneNumber);
        $mCusDraft          = CustomerDraft::model()->find($criteria);
        if(!empty($mCusDraft)){
//            $aDatabase[]    = $phoneNumber. " ID CustomerDraft: $mCusDraft->id ** employee_id: $mCusDraft->employee_id";
            $aDatabase[]    = $phoneNumber;
            return true;
        }
//        check user_phone
        $criteriaUsersPhone           = new CDbCriteria;
        $criteriaUsersPhone->compare('t.role_id',ROLE_CUSTOMER);
        $criteriaUsersPhone->compare('t.phone',$phoneNumber);
        $aParamsIn = CmsFormatter::$aTypeIdHgd;
        $criteriaUsersPhone->addInCondition('is_maintain',$aParamsIn);
        $mUsersPhone = UsersPhone::model()->find($criteriaUsersPhone);
        if(!empty($mUsersPhone)){
            $customer_id    = $mUsersPhone->user_id;
//            $aDatabase[]    = $phoneNumber. ' ID UsersPhone: '.$mUsersPhone->id;
            $aDatabase[]    = $phoneNumber;
            return true;
        }
        return false;
    }
    
    /** @Author: HOANG NAM 05/07/2018
     *  @Todo: get view phone
     **/
    public function getViewPhoneDuplicate($aError){
        $result      = '*********************** '.$this->getOwner().' ***********************';
        $result     .= "<br><hr><br>";
        $result     .= "Thêm mới {$aError[$this->nameCount]} thành công, ".count($aError[$this->nameCheckExcel]).' KH Trùng số, '.count($aError[$this->nameCheckDatabase]).' KH cũ công ty';
        $result     .= '<br>';
        if(count($aError[$this->nameCheckExcel]) > 0){
            $result .= '<b>---------------- Trùng số ---------------- </b><br>';
            $result .= implode(', ', $aError[$this->nameCheckExcel]);
            $result .= "<br>";
        }
        if(count($aError[$this->nameCheckDatabase]) > 0){
            $result .= "<hr>";
            $result .= '<b>---------------- KH cũ công ty ---------------- </b><br>';
            $result .= implode(', ', $aError[$this->nameCheckDatabase]);
        }
        return $result;
    }
    
    /** @Author: HOANG NAM 09/07/2018
     *  @Todo: report call
     **/
    public function searchReport() {
        $aData = [];
        $aData['MODEL_EMPLOYEES'] = [];
        $aData['REPORT'] = [];
        $aData['LIST_STATUS'] = [];
        $criteria = new CDbCriteria;
        if (!empty($this->date_call_from)) {
            $dateFrom = MyFormat::dateConverDmyToYmd($this->date_call_from, '-');
            $criteria->addCondition('t.created_date_only >= "' . $dateFrom . '"');
        }
        if (!empty($this->date_call_to)) {
            $dateTo = MyFormat::dateConverDmyToYmd($this->date_call_to, '-');
            $criteria->addCondition('t.created_date_only <= "' . $dateTo . '"');
        }
        $criteria->select = 't.employee_id,t.status,count(t.id) as id';
        $criteria->compare('t.employee_id', $this->employee_id);
        $criteria->addCondition('t.employee_id > 0');
        $criteria->compare('t.status', $this->status);
        $criteria->group = 't.employee_id,t.status';
//        $criteria->order = 'id DESC';
        $aCustomerDraftDetail = CustomerDraftDetail::model()->findAll($criteria);
        foreach ($aCustomerDraftDetail as $key => $mCustomerDraftDetail) {
            if(!empty($aData['REPORT'][$mCustomerDraftDetail->employee_id][$mCustomerDraftDetail->status])){
                $aData['REPORT'][$mCustomerDraftDetail->employee_id][$mCustomerDraftDetail->status] +=  $mCustomerDraftDetail->id;
            }else{
                $aData['REPORT'][$mCustomerDraftDetail->employee_id][$mCustomerDraftDetail->status] =  $mCustomerDraftDetail->id;
            }
            $aData['MODEL_EMPLOYEES'][$mCustomerDraftDetail->employee_id] = $mCustomerDraftDetail->rEmployee;
        }
        $aData['LIST_STATUS'] = $this->getArrayCallStatus();
//        Sắp xếp lại
        uasort($aData['REPORT'], function ($item1, $item2) {
            return array_sum($item2) > array_sum($item1);
        });
        return $aData;
    }
    
    /** @Author: HOANG NAM 13/07/2018
     *  @Todo: search owner report
     **/
    public function searchOwnerReport() {
        $aData = [];
        $aData['MODEL_EMPLOYEES'] = [];
        $aData['REPORT']        = [];
        $aData['LIST_STATUS']   = [];
        $aData['LIST_SELL']     = [];
        $criteria = new CDbCriteria;
        if (!empty($this->date_call_from)) {
            $dateFrom = MyFormat::dateConverDmyToYmd($this->date_call_from, '-');
            $criteria->addCondition('t.created_date_only >= "' . $dateFrom . '"');
        }
        if (!empty($this->date_call_to)) {
            $dateTo = MyFormat::dateConverDmyToYmd($this->date_call_to, '-');
            $criteria->addCondition('t.created_date_only <= "' . $dateTo . '"');
        }
        $criteria->select = 't.owner_id,t.status,count(t.id) as id,sum(if(t.sell_id > 0,1,0)) as sell_id';
        $criteria->compare('t.owner_id', $this->owner_id);
        $criteria->addCondition('t.owner_id > 0');
        $criteria->compare('t.status', $this->status);
        $criteria->group = 't.owner_id,t.status';
//        $criteria->order = 'id DESC';
        $aCustomerDraft = CustomerDraft::model()->findAll($criteria);
        foreach ($aCustomerDraft as $key => $mCustomerDraft) {
            if(!empty($aData['REPORT'][$mCustomerDraft->owner_id][$mCustomerDraft->status])){
                $aData['REPORT'][$mCustomerDraft->owner_id][$mCustomerDraft->status] +=  $mCustomerDraft->id;
            }else{
                $aData['REPORT'][$mCustomerDraft->owner_id][$mCustomerDraft->status] =  $mCustomerDraft->id;
            }
            $aData['MODEL_EMPLOYEES'][$mCustomerDraft->owner_id] = $mCustomerDraft->rOwner;
            
            if(!empty($aData['LIST_SELL'][$mCustomerDraft->owner_id])){
                $aData['LIST_SELL'][$mCustomerDraft->owner_id] +=  $mCustomerDraft->sell_id;
            }else{
                $aData['LIST_SELL'][$mCustomerDraft->owner_id] =  $mCustomerDraft->sell_id;
            }
        }
        $aData['LIST_STATUS'] = $this->getArrayCallStatus();
//        Sắp xếp lại
        uasort($aData['REPORT'], function ($item1, $item2) {
            return array_sum($item2) > array_sum($item1);
        });
        return $aData;
    }
    
    /** @Author: KHANH TOAN 2018
     *  @Todo: update lại dữ liệu cũ
     *  update app_customer_id, agent_id, sell_id, sell_created_date
     *  @Param:
     *  UPDATE gas_customer_draft as C
        SET  app_customer_id = (SELECT max(id)
                                FROM gas_users as U
                                WHERE concat("0",C.phone) = U.username and U.is_maintain = 11)
     **/
    public function updateDataOld() {
        $from = time();
        $tableNameCustomerDraft = CustomerDraft::model()->tableName();
        $tableNameUser = Users::model()->tableName();
        $tableNameSell = Sell::model()->tableName();
        //update app_customer_id, agent_id
        $sql1    ="UPDATE `$tableNameCustomerDraft` as C "
                    . "SET app_customer_id = (select max(id) "
                    . "FROM $tableNameUser as U "
                    . "WHERE concat('0',C.phone) = U.username AND "
                    . "U.is_maintain = ".UsersExtend::STORE_CARD_HGD_APP .") , "
                    . "agent_id = (select max(area_code_id) "
                    . "FROM $tableNameUser as U "
                    . "WHERE concat('0',C.phone) = U.username AND "
                    . "U.is_maintain = ".UsersExtend::STORE_CARD_HGD_APP .") ";
        Yii::app()->db->createCommand($sql1)->execute();
        //update sell_id, sell_created_date
        $sql2    ="UPDATE  `$tableNameCustomerDraft` AS t1, `$tableNameSell` AS t2 "
                    . "SET t1.sell_id = t2.id, t1.sell_created_date = t2.created_date_only "
                    . "WHERE t1.customer_id = t2.customer_id AND t1.app_customer_id > 0";
        Yii::app()->db->createCommand($sql2)->execute();
        $to = time();
        $second = $to-$from;
        $info = " - Run  mCustomerDraft updateDataOld done in:+ ".($second).'  Second  <=> '.($second/60).' Minutes '.date('Y-m-d H:i:s');
        echo $info;
        Logger::WriteLog($info);
        die();
    }
    
    /** @Author: KHANH TOAN Dec 12 2018
     *  @Todo: update từng ngày app_customer_id, agent_id
     *  @Param:
     **/
    public function updateAppCustomerId($dateRun) {
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->compare('t.created_date_only', $dateRun);
        $models = CustomerDraft::model()->findAll($criteria);
        foreach($models as $key => $mCustomerDraft){
            $mUser = $this->findAppCustomerId($mCustomerDraft);
            if(empty($mUser))
                continue;
            $mCustomerDraft->app_customer_id    = $mUser->id;
            $mCustomerDraft->agent_id           = $mUser->area_code_id;
            $mCustomerDraft->update();
        }
        $this->updateInfoSellByDate($dateRun);
        $to = time();
        $second = $to-$from;
        $info = " - Run  mCustomerDraft updateAppCustomerId done in:+ ".($second).'  Second  <=> '.($second/60).' Minutes '.date('Y-m-d H:i:s');
        echo $info;
        Logger::WriteLog($info);
        die();
    }

    /** @Author: KHANH TOAN 2018
     *  @Todo: find AppcustpmerId
     *  @Param: $mCustomerDraft
     **/
    public function findAppCustomerId($mCustomerDraft) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.username', '0'.$mCustomerDraft->phone);
        $criteria->compare('t.is_maintain', UsersExtend::STORE_CARD_HGD_APP );
        $mUser = Users::model()->find($criteria);
        return $mUser;
    }
    
    /** @Author: KHANH TOAN Dec 12 2018
     *  @Todo:
     *  @Param:
     **/
    public function updateInfoSellByDate($dateRun) {
        $from = time();
        $created_date_only_bigint   = strtotime($dateRun);
        $tableNameCustomerDraft       = CustomerDraft::model()->tableName();
        $tableNameSell              = Sell::model()->tableName();
        
        $sql    ="UPDATE  `$tableNameCustomerDraft` AS t1, `$tableNameSell` AS t2 "
                    . "SET t1.sell_id = t2.id, t1.sell_created_date = t2.created_date_only "
                    . "WHERE t1.customer_id = t2.customer_id AND t1.app_customer_id > 0 AND t2.created_date_only_bigint = $created_date_only_bigint";
        Yii::app()->db->createCommand($sql)->execute();
        $to = time();
        $second = $to-$from;
        $info = "dateRun $dateRun - Run  TargetDaily updateInfoSellByDate done in:+ ".($second).'  Second  <=> '.($second/60).' Minutes '.date('Y-m-d H:i:s');
        Logger::WriteLog($info);
    }
    
    /** @Author: KHANH TOAN 2018
     *  @Todo: report telesale source for agent
     *  @Param:
     **/
    public function getReportTelesaleSource() {
         $criteria = new CDbCriteria;
        if (!empty($this->date_from)) {
            $dateFrom = MyFormat::dateConverDmyToYmd($this->date_from, '-');
            $criteria->addCondition('t.created_date_only >= "' . $dateFrom . '"');
        }
        if (!empty($this->date_to)) {
            $dateTo = MyFormat::dateConverDmyToYmd($this->date_to, '-');
            $criteria->addCondition('t.created_date_only <= "' . $dateTo . '"');
        }
        $criteria->select = 't.owner_id, t.agent_id, sum(if(t.app_customer_id > 0,1,0)) as app_customer_id, sum(if(t.sell_id > 0,1,0)) as sell_id';
        $criteria->addCondition('t.owner_id > 0');
        $criteria->addCondition('t.agent_id > 0');
        $criteria->compare('t.agent_id', $this->agent_id);
        $criteria->group = 't.owner_id, t.agent_id'; 
        $aCustomerDraft = CustomerDraft::model()->findAll($criteria);
        $aData = [];
        foreach ($aCustomerDraft as $key => $mCustomerDraft) {
            $aData['LIST_TELESALE'][$mCustomerDraft->owner_id] = $mCustomerDraft->owner_id;
            $aData['LIST_AGENT'][$mCustomerDraft->agent_id] = $mCustomerDraft->agent_id;
            if(isset($aData['OUT_PUT'][$mCustomerDraft->agent_id][$mCustomerDraft->owner_id]['NUMBER_SELL'])){
                $aData['OUT_PUT'][$mCustomerDraft->agent_id][$mCustomerDraft->owner_id]['NUMBER_SELL'] +=  $mCustomerDraft->sell_id;
            }else{
                $aData['OUT_PUT'][$mCustomerDraft->agent_id][$mCustomerDraft->owner_id]['NUMBER_SELL'] =  $mCustomerDraft->sell_id;
            }
            if(isset($aData['OUT_PUT'][$mCustomerDraft->agent_id][$mCustomerDraft->owner_id]['NUMBER_APP'])){
                $aData['OUT_PUT'][$mCustomerDraft->agent_id][$mCustomerDraft->owner_id]['NUMBER_APP'] +=  $mCustomerDraft->app_customer_id;
            }else{
                $aData['OUT_PUT'][$mCustomerDraft->agent_id][$mCustomerDraft->owner_id]['NUMBER_APP'] =  $mCustomerDraft->app_customer_id;
            }
            //sum từng telesale
            if(isset($aData['SUM_TELESALE'][$mCustomerDraft->owner_id]['NUMBER_SELL'])){
                $aData['SUM_TELESALE'][$mCustomerDraft->owner_id]['NUMBER_SELL'] +=  $mCustomerDraft->sell_id;
            }else{
                $aData['SUM_TELESALE'][$mCustomerDraft->owner_id]['NUMBER_SELL'] =  $mCustomerDraft->sell_id;
            }
            if(isset($aData['SUM_TELESALE'][$mCustomerDraft->owner_id]['NUMBER_APP'])){
                $aData['SUM_TELESALE'][$mCustomerDraft->owner_id]['NUMBER_APP'] +=  $mCustomerDraft->app_customer_id;
            }else{
                $aData['SUM_TELESALE'][$mCustomerDraft->owner_id]['NUMBER_APP'] =  $mCustomerDraft->app_customer_id;
            }
            //sum từng agent
            if(isset($aData['SUM_AGENT'][$mCustomerDraft->agent_id]['NUMBER_SELL'])){
                $aData['SUM_AGENT'][$mCustomerDraft->agent_id]['NUMBER_SELL'] +=  $mCustomerDraft->sell_id;
            }else{
                $aData['SUM_AGENT'][$mCustomerDraft->agent_id]['NUMBER_SELL'] =  $mCustomerDraft->sell_id;
            }
            if(isset($aData['SUM_AGENT'][$mCustomerDraft->agent_id]['NUMBER_APP'])){
                $aData['SUM_AGENT'][$mCustomerDraft->agent_id]['NUMBER_APP'] +=  $mCustomerDraft->app_customer_id;
            }else{
                $aData['SUM_AGENT'][$mCustomerDraft->agent_id]['NUMBER_APP'] =  $mCustomerDraft->app_customer_id;
            }
            //sum all
            if(isset($aData['SUM_ALL']['NUMBER_SELL'])){
                $aData['SUM_ALL']['NUMBER_SELL'] +=  $mCustomerDraft->sell_id;
            }else{
                $aData['SUM_ALL']['NUMBER_SELL'] =  $mCustomerDraft->sell_id;
            }
            if(isset($aData['SUM_ALL']['NUMBER_APP'])){
                $aData['SUM_ALL']['NUMBER_APP'] +=  $mCustomerDraft->app_customer_id;
            }else{
                $aData['SUM_ALL']['NUMBER_APP'] =  $mCustomerDraft->app_customer_id;
            }
           
        }
        $_SESSION['data-telesale-source'] = $aData;
        return $aData;
    }
    
}
