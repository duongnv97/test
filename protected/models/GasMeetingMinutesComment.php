<?php

/**
 * This is the model class for table "{{_gas_meeting_minutes_comment}}".
 *
 * The followings are the available columns in table '{{_gas_meeting_minutes_comment}}':
 * @property string $id
 * @property string $meeting_minutes_id
 * @property string $uid_login
 * @property string $message
 * @property string $created_date
 */
class GasMeetingMinutesComment extends BaseSpj
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasMeetingMinutesComment the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_meeting_minutes_comment}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                    array('id, meeting_minutes_id, uid_login, message, created_date', 'safe'),
            );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
            return array(
                'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
                'rMeetingMinutesId' => array(self::BELONGS_TO, 'GasMeetingMinutes', 'meeting_minutes_id'),
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                    'id' => 'ID',
                    'meeting_minutes_id' => 'Meeting Minutes',
                    'uid_login' => 'Uid Login',
                    'message' => 'Message',
                    'created_date' => 'Created Date',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
            $criteria=new CDbCriteria;
            $criteria->compare('t.meeting_minutes_id',$this->meeting_minutes_id);
            $criteria->compare('t.uid_login',$this->uid_login);
            $criteria->order = "t.id DESC";
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>array(
    //                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                        'pageSize'=> 10,
                    ),
            ));
    }

    /*
    public function activate()
    {
        $this->status = 1;
        $this->update();
    }

    public function deactivate()
    {
        $this->status = 0;
        $this->update();
    }
	*/

    public function defaultScope()
    {
            return array(
                    //'condition'=>'',
            );
    }       
    
    /**
     * @Author: ANH DUNG Nov 07, 2014
     * @Todo: save 1 bình luận của biên bản họp
     * @Param: $model model $mGasMeetingMinutes
     */
    public static function SaveOneMessageDetail($model){
//        if(!self::UserCanPostTicket()) return;
        $mComment = new GasMeetingMinutesComment();
        $mComment->meeting_minutes_id = $model->id;
        $mComment->uid_login = Yii::app()->user->id;
        $mComment->message = trim(InputHelper::removeScriptTag($model->message));
        $mComment->save();
        $model->last_comment = date('Y-m-d H:i:s');
        $model->update(array('last_comment'));
    }
    
    /**
     * @Author: ANH DUNG Jan 28, 2015
     * @Todo: get last comment for this
     * @Param: $model is model GasMeetingMinutes
     */
    public static function GetLastComment($model) {
        $TotalComment = MyFormat::CountComment($model->id, 'meeting_minutes_id', 'GasMeetingMinutesComment');
        $res = MyFormat::GetLastComment($model->id, 'meeting_minutes_id', 'GasMeetingMinutesComment', 'rUidLogin');
        if($TotalComment){
            $res = "<b>($TotalComment)</b> - ".$res;
        }
        return $res;
    }
   
}