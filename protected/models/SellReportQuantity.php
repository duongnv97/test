<?php

/**
 * This is the model class for table "{{_sell_report_quantity}}".
 *
 * The followings are the available columns in table '{{_sell_report_quantity}}':
 * @property string $id
 * @property integer $sell_id
 * @property integer $uid_login
 * @property integer $customer_id
 * @property integer $materials_id
 * @property integer $previous_materials_id
 * @property string $created_date_only
 * @property string $created_date_only_bigint
 */
class SellReportQuantity extends CActiveRecord
{
    public $date_from, $date_to, $date_from_ymd, $date_to_ymd;
    
    
    public function getArrayB12Cam() {
        return [
            GasMaterials::ID_GAS_12_CAM,
            GasMaterials::ID_GAS_12_CAM_SJ,
//            GasMaterials::ID_GAS_12_XANH_SIAM,
//            GasMaterials::ID_GAS_12_VANG_SJ_SGAS,
        ];
    }
    
    public function getArrayB12Xanh() {
        return [
            GasMaterials::ID_GAS_12_XANH_SIAM,
        ];
    }
    public function getArrayB12Vang() {
        return [
            GasMaterials::ID_GAS_12_VANG_SJ_SGAS,
        ];
    }
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return SellReportQuantity the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_sell_report_quantity}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                    array('sell_id, uid_login, customer_id, materials_id, previous_materials_id, created_date_only, created_date_only_bigint', 'required'),
                    array('sell_id, uid_login, customer_id, materials_id, previous_materials_id', 'numerical', 'integerOnly'=>true),
                    array('created_date_only_bigint', 'length', 'max'=>20),
                    // The following rule is used by search().
                    // Please remove those attributes that should not be searched.
                    array('id, sell_id, uid_login, customer_id, materials_id, previous_materials_id, created_date_only, created_date_only_bigint', 'safe', 'on'=>'search'),
                    array('date_from, date_to, date_from_ymd, date_to_ymd ', 'safe'),
            );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                    'id' => 'ID',
                    'sell_id' => 'Sell',
                    'uid_login' => 'Uid Login',
                    'customer_id' => 'Customer',
                    'materials_id' => 'Materials',
                    'previous_materials_id' => 'Previous Materials',
                    'created_date_only' => 'Created Date Only',
                    'created_date_only_bigint' => 'Created Date Only Bigint',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
            $criteria=new CDbCriteria;

            $criteria->compare('id',$this->id,true);
            $criteria->compare('sell_id',$this->sell_id);
            $criteria->compare('uid_login',$this->uid_login);
            $criteria->compare('customer_id',$this->customer_id);
            $criteria->compare('materials_id',$this->materials_id);
            $criteria->compare('previous_materials_id',$this->previous_materials_id);
            $criteria->compare('created_date_only',$this->created_date_only,true);
            $criteria->compare('created_date_only_bigint',$this->created_date_only_bigint,true);

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }
        
    /** @Author: KHANH TOAN 2018
     *  @Todo: chạy cron 1 câu sql
     *  @Param: $date format Y-m-d
     **/
    public function insertTable($date) {
        $from = time();
        $this->deleteOnCreatedDateBigInt($date);
        $criteria = new CDbCriteria();
        DateHelper::searchBetween($date, $date, 'created_date_only_bigint', $criteria, false);
        $criteria->compare("t.status", Sell::STATUS_PAID);
        $criteria->compare("t.materials_type_id	",GasMaterialsType::MATERIAL_BINH_12KG);
        $mSellDetail = SellDetail::model()->findAll($criteria);
        $aRowInsert = [];
        foreach ($mSellDetail as $key => $sellDetail) {
            $previousOrder = $this->findPreviousOrder($sellDetail);
            $previous_materials_id = 0;
            if($previousOrder){
                $previous_materials_id = "'$previousOrder->materials_id'";
            }
            $aRowInsert[]="('$sellDetail->sell_id',
                           '$sellDetail->uid_login',
                           '$sellDetail->customer_id', 
                           '$sellDetail->materials_id',
                            $previous_materials_id,
                           '$sellDetail->created_date_only',
                           '$sellDetail->created_date_only_bigint'
                           )";
        }
        $tableName = SellReportQuantity::model()->tableName();
        $sql = "insert into $tableName (sell_id,
                    uid_login,
                    customer_id,
                    materials_id,
                    previous_materials_id,
                    created_date_only,
                    created_date_only_bigint
                    ) values ".implode(',', $aRowInsert);

        if(count($aRowInsert)>0)
            Yii::app()->db->createCommand($sql)->execute();
        
        $to = time();
        $second = $to-$from;
        $info = count($aRowInsert).' Run report Binh Cam SellReportQuantity insertTable  done in: '.($second).'  Second  <=> '.($second/60).' Minutes';
        Logger::WriteLog($info);
    }
    
    /** @Author: KHANH TOAN 2018
     *  @Todo: lấy order trước đó cua customer_id
     *  @Param:
     **/
    public function findPreviousOrder($sellDetail) {
        $criteria = new CDbCriteria();
//        $criteria->compare("t.uid_login", $sellDetail->uid_login);// AnhDung Close Now1918 ko check dk này
        $criteria->compare("t.customer_id", $sellDetail->customer_id);
        $criteria->compare("t.status", Sell::STATUS_PAID);
        $criteria->compare("t.materials_type_id	",GasMaterialsType::MATERIAL_BINH_12KG);
        $criteria->addCondition('t.sell_id <' . $sellDetail->sell_id);
        $criteria->limit = 1;
        $criteria->order = "t.sell_id DESC";
        $lastOrder = SellDetail::model()->find($criteria);
        return $lastOrder;
    }
    
    /** @Author: KHANH TOAN Nov 2,2018
     *  @Todo: số bình mới mà nhân viên tổng đài tư vẫn đượcs trong tháng từ hóa đơn gần nhất
     *      bình gas S cam 12kg, code: 586
     *  @Param:
     **/
    public function getReportQuantity() {
        $criteria = new CDbCriteria();
        $this->date_from_ymd  = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        $this->date_to_ymd    = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
//        $criteria->addBetweenCondition('t.created_date_only',  $this->date_from_ymd, $this->date_to_ymd);
        DateHelper::searchBetween($this->date_from_ymd, $this->date_to_ymd, 'created_date_only_bigint', $criteria, false);
        $models = SellReportQuantity::model()->findAll($criteria);
        $aData = $aUid = $sumAll = [];
        foreach ($models as $key => $model) {
            $uid_login = $model->uid_login;
            //khác cam chuyển sang cam thì là mới
            if($model->previous_materials_id != GasMaterials::ID_GAS_12_CAM && $model->materials_id == GasMaterials::ID_GAS_12_CAM){
                if(isset($aData[$uid_login]['NEW'])){
                    $aData[$uid_login]['NEW'] += 1;
                }else{
                    $aUid[$uid_login] = $uid_login;
                    $aData[$uid_login]['NEW'] = 1;
                }
                if(isset($sumAll['NEW'])){
                    $sumAll['NEW'] += 1;
                }else{
                    $sumAll['NEW'] = 1;
                }
            }
             // cam chuyển sang cam thì giữ được
            if($model->previous_materials_id == GasMaterials::ID_GAS_12_CAM && $model->materials_id == GasMaterials::ID_GAS_12_CAM){
                if(isset($aData[$uid_login]['MAINTAIN'])){
                    $aData[$uid_login]['MAINTAIN'] += 1;
                }else{
                    $aUid[$uid_login] = $uid_login;
                    $aData[$uid_login]['MAINTAIN'] = 1;
                }
                if(isset($sumAll['MAINTAIN'])){
                    $sumAll['MAINTAIN'] += 1;
                }else{
                    $sumAll['MAINTAIN'] = 1;
                }
            }
            //cam chuyển sang khác cam là mất đi
            if($model->previous_materials_id == GasMaterials::ID_GAS_12_CAM && $model->materials_id != GasMaterials::ID_GAS_12_CAM){
                if(isset($aData[$uid_login]['LOSS'])){
                    $aData[$uid_login]['LOSS'] += 1;
                }else{
                    $aUid[$uid_login] = $uid_login;
                    $aData[$uid_login]['LOSS'] = 1;
                }
                if(isset($sumAll['LOSS'])){
                    $sumAll['LOSS'] += 1;
                }else{
                    $sumAll['LOSS'] = 1;
                }
            }
            //cuộc gọi cam
            if($model->previous_materials_id == GasMaterials::ID_GAS_12_CAM){
                if(isset($aData[$uid_login]['CALL'])){
                    $aData[$uid_login]['CALL'] += 1;
                }else{
                    $aUid[$uid_login] = $uid_login;
                    $aData[$uid_login]['CALL'] = 1;
                }
                if(isset($sumAll['CALL'])){
                    $sumAll['CALL'] += 1;
                }else{
                    $sumAll['CALL'] = 1;
                }
            }
           
        }
        uasort($aData, function($a, $b){
                $a['NEW'] = isset($a['NEW']) ? $a['NEW'] : 0;
                $b['NEW'] = isset($b['NEW']) ? $b['NEW'] : 0;
                if($a['NEW'] == $b['NEW']) return 0;
                return ($a['NEW'] > $b['NEW']) ? -1 : 1;
            });
       
            $aResult =[
            'data' => $aData,
            'aUid' => $aUid,
            'sumAll' => $sumAll,    
            ];

        return $aResult;
    }
    
    /** @Author: NamNH Dec 06, 2018
     *  @Todo: get report quantity v2
     *  add group by t.uid_login, t.previous_materials_id, t.materials_id
     **/
    public function getReportQuantityV2() {
        $criteria = new CDbCriteria();
        $criteria->select     = 't.uid_login, t.previous_materials_id, t.materials_id, count(id) as id';
        $this->date_from_ymd  = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        $this->date_to_ymd    = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        DateHelper::searchBetween($this->date_from_ymd, $this->date_to_ymd, 'created_date_only_bigint', $criteria, false);
        $sParamsIn = implode(',', GasMaterials::getArrayIdXam());
        $criteria->addCondition("t.previous_materials_id IN ($sParamsIn)");
        $criteria->group      = 't.uid_login, t.previous_materials_id, t.materials_id';
        $models = SellReportQuantity::model()->findAll($criteria);
        $aData = $aUid = $sumAll = [];
        foreach ($models as $key => $model) {
            $uid_login = $model->uid_login;
            //sám chuyển sang cam thì là mới
//            if(!in_array($model->previous_materials_id, $this->getArrayB12Cam()) && in_array($model->materials_id, $this->getArrayB12Cam())){
            if(in_array($model->materials_id, $this->getArrayB12Cam())){
                if(isset($aData[$uid_login]['CAM'])){
                    $aData[$uid_login]['CAM'] += $model->id;
                }else{
                    $aUid[$uid_login] = $uid_login;
                    $aData[$uid_login]['CAM'] = $model->id;
                }
                if(isset($sumAll['CAM'])){
                    $sumAll['CAM'] += $model->id;
                }else{
                    $sumAll['CAM'] = $model->id;
                }
            }
            
            if(in_array($model->materials_id, $this->getArrayB12Xanh())){
                if(isset($aData[$uid_login]['XANH'])){
                    $aData[$uid_login]['XANH'] += $model->id;
                }else{
                    $aUid[$uid_login] = $uid_login;
                    $aData[$uid_login]['XANH'] = $model->id;
                }
                if(isset($sumAll['XANH'])){
                    $sumAll['XANH'] += $model->id;
                }else{
                    $sumAll['XANH'] = $model->id;
                }
            }
            
            if(in_array($model->materials_id, $this->getArrayB12Vang())){
                if(isset($aData[$uid_login]['VANG'])){
                    $aData[$uid_login]['VANG'] += $model->id;
                }else{
                    $aUid[$uid_login] = $uid_login;
                    $aData[$uid_login]['VANG'] = $model->id;
                }
                if(isset($sumAll['VANG'])){
                    $sumAll['VANG'] += $model->id;
                }else{
                    $sumAll['VANG'] = $model->id;
                }
            }
//             // cam chuyển sang cam thì giữ được
//            if(in_array($model->previous_materials_id, $this->getArrayB12Cam()) && in_array($model->materials_id, $this->getArrayB12Cam())){
//                if(isset($aData[$uid_login]['MAINTAIN'])){
//                    $aData[$uid_login]['MAINTAIN'] += $model->id;
//                }else{
//                    $aUid[$uid_login] = $uid_login;
//                    $aData[$uid_login]['MAINTAIN'] = $model->id;
//                }
//                if(isset($sumAll['MAINTAIN'])){
//                    $sumAll['MAINTAIN'] += $model->id;
//                }else{
//                    $sumAll['MAINTAIN'] = $model->id;
//                }
//            }
//            //cam chuyển sang khác cam là mất đi
//            if(in_array($model->previous_materials_id, $this->getArrayB12Cam()) && !in_array($model->materials_id, $this->getArrayB12Cam())){
//                if(isset($aData[$uid_login]['LOSS'])){
//                    $aData[$uid_login]['LOSS'] += $model->id;
//                }else{
//                    $aUid[$uid_login] = $uid_login;
//                    $aData[$uid_login]['LOSS'] = $model->id;
//                }
//                if(isset($sumAll['LOSS'])){
//                    $sumAll['LOSS'] += $model->id;
//                }else{
//                    $sumAll['LOSS'] = $model->id;
//                }
//            }
//            //cuộc gọi cam
////            if($model->previous_materials_id == GasMaterials::ID_GAS_12_CAM){
//            if(in_array($model->previous_materials_id, $this->getArrayB12Cam())){
//                if(isset($aData[$uid_login]['CALL'])){
//                    $aData[$uid_login]['CALL'] += $model->id;
//                }else{
//                    $aUid[$uid_login] = $uid_login;
//                    $aData[$uid_login]['CALL'] = $model->id;
//                }
//                if(isset($sumAll['CALL'])){
//                    $sumAll['CALL'] += $model->id;
//                }else{
//                    $sumAll['CALL'] = $model->id;
//                }
//            }
           
        }
        uasort($aData, function($a, $b){
                $a['CAM'] = isset($a['CAM']) ? $a['CAM'] : 0;
                $b['CAM'] = isset($b['CAM']) ? $b['CAM'] : 0;
                if($a['CAM'] == $b['CAM']) return 0;
                return ($a['CAM'] > $b['CAM']) ? -1 : 1;
            });
       
            $aResult =[
            'data' => $aData,
            'aUid' => $aUid,
            'sumAll' => $sumAll,    
            ];

        return $aResult;
    }
    
    /** @Author: ANH DUNG Now 19, 2018
     *  @Todo: run gen data auto
     **/
    public static function autoGenData() {
        $from = time();
        $date_from  = '2018-12-04';// done Now0218
        $date_to    = '2018-12-05';
   
        $aDays = MyFormat::getArrayDay($date_from, $date_to);
        foreach($aDays as $dateRun){
            $mSellReportQuantity = new SellReportQuantity();
            $mSellReportQuantity->insertTable($dateRun);
        }
        
        $to = time();
        $second = $to-$from;
        $info = " - Run  mSellReportQuantity autoGenData done in:+ ".($second).'  Second  <=> '.($second/60).' Minutes '.date('Y-m-d H:i:s');
        echo $info;
        Logger::WriteLog($info);
        die();
        
    }
    
    /** @Author: KHANH TOAN 2018
     *  @Todo: xóa theo created_date_only_bigint
     *  @Param: $date format Y-m-d
     **/
    public function deleteOnCreatedDateBigInt($date) {
        $created_date_big_int = strtotime($date);
        SellReportQuantity::model()->deleteAll("created_date_only_bigint ='" . $created_date_big_int . "'");
    }

    
}