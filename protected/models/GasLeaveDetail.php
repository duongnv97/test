<?php

/**
 * This is the model class for table "{{_gas_leave_detail}}".
 *
 * The followings are the available columns in table '{{_gas_leave_detail}}':
 * @property string $id
 * @property string $leave_id
 * @property string $uid_leave
 * @property integer $status
 * @property string $date_off
 */
class GasLeaveDetail extends BaseSpj
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasLeaveDetail the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_leave_detail}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['id, leave_id, uid_leave, status, date_off', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leave_id' => 'Leave',
            'uid_leave' => 'Uid Leave',
            'status' => 'Status',
            'date_off' => 'Date Off',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
	$criteria->compare('t.uid_leave',$this->uid_leave,true);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
}