<?php
/** Aug 18, 2016 xử lý những hàm api tương tác với thẻ kho
 */
class GasStoreCardApi
{
    public static $TYPE_IN_OUT_HGD = array(
            STORE_CARD_TYPE_3, // Xuất Bán
            STORE_CARD_TYPE_9, // Xuất Tặng
            STORE_CARD_TYPE_5, // Nhập Vỏ
    );
    const IS_ADMIN_GEN              = 1;// cho vào cột phu_xe_2 cờ kiểm tra thẻ kho HGD do admin sinh ra, sẽ không cho đại lý chỉnh sửa thẻ kho này
    const IS_ADMIN_GEN_FROM_ORDER   = 2;// cho vào cột phu_xe_2 cờ kiểm tra thẻ kho do admin sinh ra từ đơn hàng
    const IS_ORDER_AGENT_CAR        = 3;// đơn hàng xe tải giao đại lý cho vào cột phu_xe_2 cờ kiểm tra thẻ kho do admin sinh ra từ đơn hàng
    public $aDate = [], $agent_id = 0;
    
    /**
     * @Author: DungNT Aug 21, 2016
     * @Todo: chỉ cho chạy cron từ 8h sáng đến 18h tối
     */
    public static function hgdCanRunCron() {
        return true;// Aug 26, 2016 không nên chạy cron nữa, 
        $cHours = date("H");
        if($cHours > 6 && $cHours <= 20 ) {
            return true;
        }
        return false;
    }
    
    /**
     * @Author: DungNT Aug 21, 2016
     * @Todo: cron auto gen ALL storecard HGD
     */
    public static function hgdCronGenStoreCard() {
        return '';// Feb 01, 2017 không dùng hàm này nữa, viết lại hàm khác cho tiện sử dụng
        if(!self::hgdCanRunCron()){
            return ;
        }
        $from = time();
//        $date = date('Y-m-d');
//        $datePrev = MyFormat::modifyDays($date, 1, '-');
        $date = "2017-01-25";
        $date1 = "2017-01-26";
//        $aDate = array($date, $datePrev);
        $aDate = array($date, $date1);// Aug 29, 2016 chỉ cho update 1 ngày từ trên giao diện của kế toán VP
        foreach($aDate as $dateRun){
            $aSellToday = Sell::getAllDataByDate($dateRun);
            $aAgent = Users::getArrObjectUserByRole(ROLE_AGENT);
            foreach($aSellToday as $mSell){
                $mSell->mAgent = $aAgent[$mSell->agent_id];
                $mStoreCardApi = new GasStoreCardApi();
                $mStoreCardApi->hgdAddByDate($mSell);
            }
        }
        
        $to = time();
        $second = $to-$from;
        $info = "Cron C# auto insert $date Storecard HGD ".count($aSellToday).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';
        Logger::WriteLog($info);
    }
    
    /**
     * @Author: DungNT Feb 01, 2017
     * @Todo: Fix cron auto gen ALL storecard HGD, trường hợp phải sửa nhiều agent
     */
    public function hgdCronGenStoreCardFix() {
        if(empty($this->agent_id)){
            echo $info = 'Phải nhập agent mới cho chạy'; Logger::WriteLog($info);die;
        }
        $from = time();
        $aSellToday = [];
        $aAgent = Users::getArrObjectUserByRole(ROLE_AGENT);
        foreach($this->aDate as $dateRun){
            $from1 = time();
            $aSellToday = Sell::getAllDataByDate($dateRun);
            foreach($aSellToday as $mSell){
                if(!empty($this->agent_id) && $this->agent_id != $mSell->agent_id){
                    continue;
                }
                $mSell->mAgent = $aAgent[$mSell->agent_id];
                $mStoreCardApi = new GasStoreCardApi();
                $mStoreCardApi->hgdAddByDate($mSell);
                
                $release_date = MyFormat::dateConverYmdToDmy($dateRun, 'd-m-Y');
                $model = new GasCashBookDetail();// May 23, 2017 bổ sung phần cập nhật sang sổ quỹ
                $model->makeRecordHgdAllAgent([$this->agent_id],  $release_date);
//                DungNT close Aug1719 - thay ko can sleep(1);// Feb 02, 2017, Hàm này chậm do gen code thẻ kho theo thứ tự find trong db ra => đã fix. Sai => hình như sleep 2 không giảm dc cpu lắm =>
            }
            $aSellToday = null;
            $to1 = time();
            $second1 = $to1-$from1;
//            Logger::WriteLog("Run hgdCronGenStoreCardFix $dateRun AgentId: $this->agent_id done in: $second1 Second");
        }
        $to = time();
        $second = $to-$from;
//        $info = "Cron C# auto insert ".  implode(' *** ', $this->aDate)." Storecard HGD ".count($aSellToday).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';
//        Logger::WriteLog($info);
    }

    /**
     * @Author: DungNT May 19, 2017
     * @Todo: tự động sinh thẻ kho vào các khung giờ 7, 14, 19h cho các đại lý chạy App
     */
    public function cronAutoGenHgd($today) {
        $from       = time();
        $mAppCache  = new AppCache();
        $aAgent     = $mAppCache->getAgentListdata();
        if(empty($today)){// Aug2919 DungNT add
            $needMore = ['title'=> 'Important! check function bug cronAutoGenHgd empty $today'];
            SendEmail::bugToDev('Need check function bug cronAutoGenHgd empty $today', $needMore);
            return ;
        }
        foreach($aAgent as $agent_id => $name):
            $mStoreCardApi = new GasStoreCardApi();
            $mStoreCardApi->agent_id    = $agent_id;
            $mStoreCardApi->aDate[]     = MyFormat::modifyDays($today, 1, '-');
//            $mStoreCardApi->aDate[] = MyFormat::modifyDays($today, 2, '-');// Sep2017 close for fix missing run cron
//            $mStoreCardApi->aDate[] = MyFormat::modifyDays($today, 3, '-');// Sep2017 close for fix missing run cron
            $mStoreCardApi->aDate[] = $today;
            $mStoreCardApi->hgdCronGenStoreCardFix();
        endforeach;
        $to = time();
        $second = $to-$from;
        $info = "Cron C# BIX FIX auto insert ". count($aAgent).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';
        Logger::WriteLog($info);
    }
    
    
    /**
     * @Author: DungNT Aug 18, 2016
     * @Todo: xóa thẻ kho hộ gia đình của đại lý theo ngày
     * @Param: $date_delivery ngày tạo
     * @Param: $user_id_create agent id
     * Xuất Bán STORE_CARD_TYPE_3
     * Xuất Tặng STORE_CARD_TYPE_9
     * Nhập Vỏ  STORE_CARD_TYPE_5
     * 1 bán hàng dưới c# liên quan đến 3 thẻ kho Xuất Bán, Xuất Tặng, Nhập Vỏ. KH Trả Tiền Ngay (Hộ GĐ)
     * nên ở đây sẽ xử lý tự động xóa đi và thêm mới lại cho 3 thẻ kho này
     */
    public function hgdDeleteByDate($date_delivery, $user_id_create) {
        if(empty($date_delivery) || empty($user_id_create)){
            return ;
        }
        // 1. xóa GasStoreCard
        $criteria = new CDbCriteria();
        $criteria->compare("date_delivery", $date_delivery);
        $criteria->compare("phu_xe_2", GasStoreCardApi::IS_ADMIN_GEN);
        $criteria->compare("user_id_create", $user_id_create); // is agent_id
        $criteria->compare("type_user", CUSTOMER_HO_GIA_DINH);// Khác detail 
//        $criteria->addInCondition("type_in_out", GasStoreCardApi::$TYPE_IN_OUT_HGD);
        $sParamsIn = implode(',', GasStoreCardApi::$TYPE_IN_OUT_HGD);
        $criteria->addCondition("type_in_out IN ($sParamsIn)");
        GasStoreCard::model()->deleteAll($criteria);
        
        // 2 xóa detail
        $criteria = new CDbCriteria();
        $criteria->compare("date_delivery", $date_delivery);
        $criteria->compare("phu_xe_2", GasStoreCardApi::IS_ADMIN_GEN);
        $criteria->compare("user_id_create", $user_id_create); // is agent_id
        $criteria->compare("type_customer", CUSTOMER_HO_GIA_DINH);// khác root
//        $criteria->addInCondition("type_in_out", GasStoreCardApi::$TYPE_IN_OUT_HGD);
        $sParamsIn = implode(',', GasStoreCardApi::$TYPE_IN_OUT_HGD);
        $criteria->addCondition("type_in_out IN ($sParamsIn)");
        GasStoreCardDetail::model()->deleteAll($criteria);

        // Note Sep 16, 2016 xử lý kiểu này bị xóa sót trong detail rất nhiều, có lẽ do xử lý nhiều nên bị lỗi => dùng delete all
//        $models = GasStoreCard::model()->findAll($criteria);
//        foreach($models as $model){
//            $model->delete();
//        }
    }
    
    /**
     * @Author: DungNT Aug 19, 2016
     * @Todo: thêm tự động vào thẻ kho dựa trên bán hàng từ c#
     * 1. xóa hết 3 loại thẻ kho Hộ GD của đại lý đi: Xuất Bán, Xuất Tặng, Nhập Vỏ
       * 2. find bên bảng bán hàng rồi group theo từng loại vật tư: gas, vỏ , KM
       * 3. run insert cho từng loại - cái này nếu nhiều user dùng sẽ làm CPU high
     * @param: $mSell model Sell
     * truyền model Sell vào để có thể chuyển cái này sang cron chạy khoảng 1h 1 lần update cho all Đại lý
     */
    public function hgdAddByDate($mSell) {
//        $from = time();
//        if($mSell->agent_id == GasConst::AGENT_CH3){// Aug 31, 2016 đang test
//            return ;
//        }// Oct 17, 2016 on live need close
        GasStoreCardApi::HgdDeleteByDate($mSell->created_date_only, $mSell->agent_id);
        $aDataSell = $mSell->getDataByDate();
        if(!isset($aDataSell['OUTPUT'])){
            return ;
        }
        foreach($aDataSell['OUTPUT'] as $agent_id => $aInfo1){
            $this->hgdAddByDateInsert($aDataSell, $mSell, $agent_id, $aInfo1);
        }
        
//        $to = time();
//        $second = $to-$from;
//        $info = "C# auto insert Storecard HGD ".count($aDataSell['OUTPUT']).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';
//        Logger::WriteLog($info);
    }
    
    public function hgdAddByDateInsert($aDataSell, $mSell, $agent_id, $aInfo1) {
        /* 1. build 3 model stocard for 3 type Xuất Bán, Xuất Tặng, Nhập Vỏ
         * 2. build stocard detail cho 3 thẻ kho riêng biệt
         */
        $mStoreCardXuatBan  = $this->hgdAddByDateBuildStorecard($aDataSell, $mSell, $agent_id, TYPE_STORE_CARD_EXPORT, STORE_CARD_TYPE_3);
        $mStoreCardXuatTang = $this->hgdAddByDateBuildStorecard($aDataSell, $mSell, $agent_id, TYPE_STORE_CARD_EXPORT, STORE_CARD_TYPE_9);
        $mStoreCardNhapVo   = $this->hgdAddByDateBuildStorecard($aDataSell, $mSell, $agent_id, TYPE_STORE_CARD_IMPORT, STORE_CARD_TYPE_5);
        $aRowInsert=array();
        $this->hgdAddByDateBuildSql($mStoreCardXuatBan, $mStoreCardXuatTang, $mStoreCardNhapVo, $aInfo1, $aRowInsert);
        $tableName = GasStoreCardDetail::model()->tableName();
        $sql = "insert into $tableName (store_card_id,
                    customer_id,
                    customer_parent_id,
                    warehouse_new,
                    type_customer,
                    sale_id,
                    sale_type,
                    type_store_card,
                    type_in_out,
                    date_delivery,
                    date_delivery_bigint,
                    materials_id,
                    materials_type_id,
                    qty,
                    price,
                    amount,
                    delivery_person,
                    user_id_create,
                    province_id_agent,
                    car_id,
                    driver_id_1,
                    driver_id_2,
                    phu_xe_1,
                    phu_xe_2,
                    road_route
                    ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert)>0){
            Yii::app()->db->createCommand($sql)->execute();
        }
    }
    
    /**
     * @Author: DungNT Aug 19, 2016
     * @Todo: build array detail
     */
    public function hgdAddByDateBuildSql($mStoreCardXuatBan, $mStoreCardXuatTang, $mStoreCardNhapVo, $aInfo1, &$aRowInsert) {
        $tmpDate        = explode('-', $mStoreCardXuatBan->date_delivery);
        $year           = $tmpDate[0];
        $emptyXuatBan   = true;
        $emptyXuatTang  = true;
        $emptyNhapVo    = true;
        $date_delivery_bigint   = strtotime($mStoreCardXuatBan->date_delivery);
        
        foreach($aInfo1 as $materials_type_id => $aInfo2){
            foreach($aInfo2 as $materials_id => $qty){
                $modelDetail    = new GasStoreCardDetail();
                $mStoreCard     = $this->getModelStoreCard($materials_type_id, $mStoreCardXuatBan, $mStoreCardXuatTang, $mStoreCardNhapVo, $emptyXuatBan, $emptyXuatTang, $emptyNhapVo);
                $modelDetail->province_id_agent     = $mStoreCard->province_id_agent;// Apr 22, 2015
                $agent_id                           = $mStoreCard->user_id_create;
                
                 // Aug 21, 2016 BIG CHANGE đã close đoạn này lại, vì sẽ chỉ chạy 1-> vài lần vào cuối năm
                if($mStoreCard->type_store_card==TYPE_STORE_CARD_IMPORT){
                    // cập nhật nxt vật tư đại lý 
                    GasMaterialsOpeningBalance::addAgentImport($year, $agent_id, $materials_id, $qty);
                }else{
                    // cập nhật nxt vật tư đại lý
                    GasMaterialsOpeningBalance::addAgentExport($year, $agent_id, $materials_id, $qty);
                    // is TYPE_STORE_CARD_EXPORT
                }
                
                $customer_id = $customer_parent_id = $sale_id = $sale_type = 0;
                $price = $amount = 0;
                $type_customer = CUSTOMER_HO_GIA_DINH;
                $aRowInsert[]="('$mStoreCard->id',
                    '$customer_id',
                    '$customer_parent_id',
                    '$mStoreCard->warehouse_new',
                    '$type_customer',
                    '$sale_id', 
                    '$sale_type', 
                    '$mStoreCard->type_store_card', 
                    '$mStoreCard->type_in_out', 
                    '$mStoreCard->date_delivery',
                    '$date_delivery_bigint',
                    '$materials_id',
                    '$materials_type_id',
                    '$qty',
                    '$price',
                    '$amount',
                    '$mStoreCard->date_delivery',
                    '$mStoreCard->user_id_create',
                    '$mStoreCard->province_id_agent',
                    '$mStoreCard->car_id',
                    '$mStoreCard->driver_id_1',
                    '$mStoreCard->driver_id_2',
                    '$mStoreCard->phu_xe_1',
                    '$mStoreCard->phu_xe_2',
                    '$mStoreCard->road_route'
                    )";
                
            }
        }
        
        if($emptyXuatBan){
            $mStoreCardXuatBan->delete();
        }
        if($emptyXuatTang){
            $mStoreCardXuatTang->delete();
        }
        if($emptyNhapVo){
            $mStoreCardNhapVo->delete();
        }
    }
    
    /**
     * @Author: DungNT Aug 19, 2016
     * @Todo: check $materials_type_id để lấy model storecard tương ứng
     */
    public function getModelStoreCard($materials_type_id, $mStoreCardXuatBan, $mStoreCardXuatTang, $mStoreCardNhapVo, &$emptyXuatBan, &$emptyXuatTang, &$emptyNhapVo) {
        if(in_array($materials_type_id, GasMaterialsType::$ARR_WINDOW_VO)){
            $emptyNhapVo = false;
            return $mStoreCardNhapVo;
        }elseif(in_array($materials_type_id, GasMaterialsType::$ARR_WINDOW_PROMOTION)){
            $emptyXuatTang = false;
            return $mStoreCardXuatTang;
        }
        $emptyXuatBan = false;
        return $mStoreCardXuatBan;
    }
    
    /**
     * @Author: DungNT Aug 19, 2016
     * @Todo: build model store card by $type_in_out
     * @Param: $aDataSell array data from sell c#
     * @Param: $mSell model sell
     * @Param: $agent_id
     * @Param: $type_store_card TYPE_STORE_CARD_EXPORT or TYPE_STORE_CARD_IMPORT
     * @Param: $type_in_out: * Xuất Bán STORE_CARD_TYPE_3
                            * Xuất Tặng STORE_CARD_TYPE_9
                            * Nhập Vỏ  STORE_CARD_TYPE_5
     */
    public function hgdAddByDateBuildStorecard($aDataSell, $mSell, $agent_id, $type_store_card, $type_in_out) {
        $mStoreCard                     = new GasStoreCard('WindowCreateHgd');
        $mStoreCard->user_id_create     = $agent_id;
        $mStoreCard->date_delivery      = $mSell->created_date_only;
        $mStoreCard->type_user          = CUSTOMER_HO_GIA_DINH;
        $mStoreCard->type_store_card    = $type_store_card;
        $mStoreCard->type_in_out        = $type_in_out;
        $mStoreCard->uid_login          = $aDataSell['EMPLOYEE'][$agent_id];
        $mStoreCard->phu_xe_2           = GasStoreCardApi::IS_ADMIN_GEN;
        $mStoreCard->mAgent             = $mSell->mAgent;

        $mStoreCard->buildStoreCardNo();
        
//        $mAgent = $mStoreCard->rAgent; // province_id_agent đc lấy ở before save 
//        if($mAgent){
//            $mStoreCard->province_id_agent  = $mAgent->province_id;
//        }
        $mStoreCard->save();
        $mStoreCard = GasStoreCard::model()->findByPk($mStoreCard->id);
        return $mStoreCard;
    }
    
    /**
     * @Author: DungNT Sep 16, 2016
     * @Todo: Xử lý sửa lỗi do khi đồng bộ từ bán hàng vào thẻ kho, xóa từng thẻ kho => xóa từng detail gây ra lỗi rất nhiều
     * @res: Test Lần 1: Sep 16, 2016 23:23 may là ko có find được row nào => dữ liệu đúng
     */
    public static function fixDeleteStorecardDetailError() {
        $criteria = new CDbCriteria();
        $criteria->addCondition("date_delivery  >= '2016-05-01' ");
        $criteria->compare("phu_xe_2", GasStoreCardApi::IS_ADMIN_GEN);
        $criteria->compare("type_customer", CUSTOMER_HO_GIA_DINH);// khác root
        $sParamsIn = implode(',', GasStoreCardApi::$TYPE_IN_OUT_HGD);
        $criteria->addCondition("type_in_out IN ($sParamsIn)");
        $models = GasStoreCardDetail::model()->findAll($criteria);
        $aRes = array();
        foreach($models as $model){
            $mStorecard = GasStoreCard::model()->findByPk($model->store_card_id);
            if(is_null($mStorecard)){
                $aRes['all'][] = $model->store_card_id;
                $aRes['record'][$model->store_card_id] = $model->store_card_id;
            }
        }
        echo '<pre>';
        print_r(count($aRes['all']));
        echo '</pre>';
        die;
    }
    
    
    /**
     * @Author: DungNT Oct 07, 2016 run cron vào 8h sáng hôm sau
     * @Todo: tự động sinh thẻ kho từ đơn hàng lên của điều phối
     * Điều Xe Kho Bến Cát => Kho Bến Cát
       Điều Xe Kho Phước Tân => Kho Phước Tân
     * SELECT * FROM `gas_gas_orders_detail` `t` WHERE ((t.date_delivery='2016-08-05') AND (t.created_by IN (59235,57916,475396,852423,295085))) AND (t.user_id_executive IN (107111,109107)) ORDER BY t.id DESC
        SELECT * FROM `gas_gas_store_card` as t  WHERE (t.date_delivery='2016-10-08') AND (t.uid_login IN (59235,57916,475396,852423,295085)) and pay_now=3 and phu_xe_2=2
        DELETE FROM `c1gas35`.`gas_gas_store_card` WHERE (date_delivery='2016-10-08') AND (uid_login IN (59235,57916,475396,852423,295085)) and pay_now=3 and phu_xe_2=2
        SELECT * FROM  `gas_gas_store_card_detail` WHERE (date_delivery='2016-10-07') and  user_id_create=26677 and (uid_login IN (59235,57916,475396,852423,295085))
     */
    public static function autoGenStoreCardFromOrder() {
        return ;// Sep0717 tắt phần sinh tự động lúc 23h đi, vì đã có app xe tải rồi
        $dateInput = date('Y-m-d');// lấy những đơn hàng giao trong ngày hôm nay
//        $dateInput = "2016-10-07";// lấy những đơn hàng giao trong ngày hôm nay
//        $dateInput = MyFormat::modifyDays($date, 1, '-');
        /* 1. get những đơn hàng của điếu phối tạo cho Điều Xe Kho Bến Cát + Điều Xe Kho Phước Tân
         * 2. foreach tạo những thẻ kho tương ứng với KH
         */
        $aOrderDetail   = self::autoGenStoreCardGetOrder($dateInput);
        $aAllAgent      = Users::getArrObjectUserByRole(ROLE_AGENT);
        
        $aRowInsert=array();
        foreach($aOrderDetail as $mOrderDetail){
            if(empty($mOrderDetail->quantity_50) && empty($mOrderDetail->quantity_45) && empty($mOrderDetail->quantity_12) && empty($mOrderDetail->quantity_6)){
                continue;
            }
            
            $agent_id           = GasConst::$DIEUXE_MAP_TO_AGENT[$mOrderDetail->user_id_executive];
            $province_id_agent  = self::getProvinceIdAgent($agent_id, $aAllAgent);
            // Xuất bán
            $mStoreCard                     = new GasStoreCard();
            $mStoreCard->uid_login          = $mOrderDetail->created_by;
            $mStoreCard->user_id_create     = $agent_id;
            $mStoreCard->customer_id        = $mOrderDetail->customer_id;
            $mStoreCard->date_delivery      = $dateInput;
            $mStoreCard->type_user          = $mOrderDetail->type_customer;
            $mStoreCard->car_id             = $mOrderDetail->car_number;
            $mStoreCard->driver_id_1        = $mOrderDetail->driver_id;
            $mStoreCard->type_store_card    = TYPE_STORE_CARD_EXPORT;
            $mStoreCard->type_in_out        = STORE_CARD_TYPE_3;
            $mStoreCard->pay_now            = GasStoreCard::CREATE_FROM_ORDER_AUTO;
            $mStoreCard->phu_xe_2           = GasStoreCardApi::IS_ADMIN_GEN_FROM_ORDER;
            $mStoreCard->note               = trim($mOrderDetail->getNoteForStoreCard());
            $mStoreCard->province_id_agent  = $province_id_agent;
            
            self::autoGenStoreCardBuildSql($mStoreCard, $aRowInsert);

            // thu vỏ
            $mStoreCard                     = new GasStoreCard();
            $mStoreCard->uid_login          = $mOrderDetail->created_by;
            $mStoreCard->user_id_create     = $agent_id;
            $mStoreCard->customer_id        = $mOrderDetail->customer_id;
            $mStoreCard->date_delivery      = $dateInput;
            $mStoreCard->type_user          = $mOrderDetail->type_customer;
            $mStoreCard->car_id             = $mOrderDetail->car_number;
            $mStoreCard->driver_id_1        = $mOrderDetail->driver_id;
            $mStoreCard->type_store_card    = TYPE_STORE_CARD_IMPORT;
            $mStoreCard->type_in_out        = STORE_CARD_TYPE_5;
            $mStoreCard->pay_now            = GasStoreCard::CREATE_FROM_ORDER_AUTO;
            $mStoreCard->phu_xe_2           = GasStoreCardApi::IS_ADMIN_GEN_FROM_ORDER;
            $mStoreCard->note               = trim($mOrderDetail->getNoteForStoreCard());
            $mStoreCard->province_id_agent  = $province_id_agent;
            self::autoGenStoreCardBuildSql($mStoreCard, $aRowInsert);
        }
        
        $tableName = GasStoreCard::model()->tableName();
        $sql = "insert into $tableName (
                    uid_login,
                    user_id_create,
                    customer_id,
                    date_delivery,
                    type_user,
                    car_id,
                    driver_id_1,
                    type_store_card,
                    type_in_out,
                    pay_now,
                    phu_xe_2,
                    note,
                    province_id_agent
                    ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert)>0){
            Yii::app()->db->createCommand($sql)->execute();
        }
    }
    
    /**
     * @Author: DungNT Oct 16, 2016
     * @Todo: get province_id of agent
     */
    public static function getProvinceIdAgent($agent_id, $aAllAgent) {
        $province_id_agent = 0;
        $mAgent     = isset($aAllAgent[$agent_id]) ? $aAllAgent[$agent_id] : false;
        if($mAgent){
            $province_id_agent = $mAgent->province_id;
        }
        return $province_id_agent;
    }
    
    /**
     * @Author: DungNT Oct 07, 2016
     * @Todo: get đơn hàng lên của điều phối Điều Xe Kho Bến Cát, Điều Xe Kho Phước Tân
     */
    public static function autoGenStoreCardGetOrder($dateInput) {
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.date_delivery='$dateInput'");// lấy đơn hàng của ngày hiện tại
//        $mAppCache = new AppCache();
//        $aIdDieuPhoiBoMoi   = $mAppCache->getArrayIdRole(ROLE_DIEU_PHOI);
//        $aIdDieuPhoiHgd     = $mAppCache->getArrayIdRole(ROLE_CALL_CENTER);
//        $aMerge  = array_merge($aIdDieuPhoiBoMoi, $aIdDieuPhoiHgd);// fix Jun 11, 2017 phải gộp 2 vào 1 vì có sự chuyển đổi giữa 2 loại này trong ca làm việc
//        $sParamsIn = implode(',', $aMerge);
//        $criteria->addCondition("t.created_by IN ($sParamsIn)");// điều phối tạo
//        $criteria->compare('created_by', GasTickets::DIEU_PHOI_THAO);// dev test only

        // Jun 26, 2017 không làm đoạn trên nữa, chỉ cần 1 điều kiện type=1 là xác định được của điều phối tạo rồi
        $criteria->addCondition('t.type=' . GasOrders::TYPE_DIEU_PHOI);
        $sParamsIn = GasConst::UID_DIEUXE_KHO_PHUOCTAN.",".GasConst::UID_DIEUXE_KHO_BENCAT;
        $criteria->addCondition("t.user_id_executive IN ($sParamsIn)");// Điều Xe Kho Bến Cát, Điều Xe Kho Phước Tân
//        $criteria->compare('user_id_executive', GasConst::UID_DIEUXE_KHO_BENCAT);// dev test only
        $criteria->order = "t.id DESC";
        return GasOrdersDetail::model()->findAll($criteria);
    }
    
    public static function autoGenStoreCardBuildSql($mStoreCard, &$aRowInsert) {
        $aRowInsert[]="(
            '$mStoreCard->uid_login',
            '$mStoreCard->user_id_create',
            '$mStoreCard->customer_id',
            '$mStoreCard->date_delivery',
            '$mStoreCard->type_user',
            '$mStoreCard->car_id',
            '$mStoreCard->driver_id_1',
            '$mStoreCard->type_store_card',
            '$mStoreCard->type_in_out',
            '$mStoreCard->pay_now',
            '$mStoreCard->phu_xe_2',
            '$mStoreCard->note',
            '$mStoreCard->province_id_agent'
        )";
    }

}