<?php

/**
 * This is the model class for table "{{_gas_bussiness_contract}}".
 *
 * The followings are the available columns in table '{{_gas_bussiness_contract}}':
 * @property string $id
 * @property string $code_no
 * @property string $agent_id
 * @property string $uid_login
 * @property integer $status
 * @property integer $type
 * @property string $customer_id
 * @property string $employee_id
 * @property string $customer_name
 * @property string $customer_contact
 * @property string $address
 * @property string $phone
 * @property string $date_plan
 * @property integer $b12
 * @property integer $b45
 * @property string $note
 * @property string $created_date
 */
class GasBussinessContract extends BaseSpj
{
    const STATUS_KY_HD = 1;
    const STATUS_THUONG_LUONG = 2;
    const STATUS_HUY_BO = 3;
    const STATUS_DATA = 4;
    const STATUS_ZONE_SPECIAL = 5;// Sep 27, 2015 thêm tab KH chuyên đề đặc biệt để theo dõi 10KH, chỉ update b/c tình hình
    
    const QTY_BIG_CUSTOMER = 30;
    public static $ARR_STATUS = array(
        GasBussinessContract::STATUS_KY_HD=>'Ký Hợp Đồng',
        GasBussinessContract::STATUS_THUONG_LUONG=>'Thương Lượng',
        GasBussinessContract::STATUS_HUY_BO=>'Hủy Bỏ',
        GasBussinessContract::STATUS_DATA=>'DATA',
        GasBussinessContract::STATUS_ZONE_SPECIAL=>'Chuyên Đề Đặc Biệt',
    );
    // cac loai user thao tac voi spancop
    const TYPE_AGENT = 1; // sub user dai ly
    const TYPE_SALE_BO = 2; // sale bo
    const TYPE_SALE_MOI_CHUYEN_VIEN = 3;// phòng sale mối mới gồm 6 người, cùng là sale nhưng khác type với sale mối hiện tại
    const TYPE_SALE_MOI_NORMAL = 4;// LÀ sale mối ban đầu của hệ thống
    const TYPE_MONITOR_AGENT = 5;// LÀ giám sát đại lý - add Oct 10, 2014
    const TYPE_NV_PTTT = 6;// LÀ NV PTTT Sep 24, 2015
    public $MAX_ID;
    public $normal_record=1;
    public $record_auto_create_new=0;
    public $year;
    public $month;
    public $number_week;
    public $mCurrentWeek;
    public $autocomplete_name;
    public $qty_contract;
    public $qty_of_big_customer; // số lượng b12 kh lấy
    
    public $mComment;
    
    public static $ARR_WEEK = array(
        1=>'Tuần 1',
        2=>'Tuần 2',
        3=>'Tuần 3',
        4=>'Tuần 4',
        5=>'Tuần 5',
    );
    public static $ARR_TYPE_SALE = array(
        GasBussinessContract::TYPE_SALE_MOI_CHUYEN_VIEN=>"Chuyên Viên Mối",
        GasBussinessContract::TYPE_SALE_BO=>"Sale Bò",
        GasBussinessContract::TYPE_AGENT=>"Sale Mối Đại Lý",
        GasBussinessContract::TYPE_MONITOR_AGENT=>"Giám Sát Đại Lý",
        GasBussinessContract::TYPE_NV_PTTT=>"CCS",
    );
    
    // Now 18, 2014. Them 1 cột  KH chuyên đề: 1: nhà hàng tiệc cưới,  2: suất ăn công nghiệp,...
    const C_ZONE_1 = 1;
    const C_ZONE_2 = 2;
    const C_ZONE_3 = 3;
    public static $ARR_C_ZONE = array(
        GasBussinessContract::C_ZONE_1=>"Nhà hàng tiệc cưới",
        GasBussinessContract::C_ZONE_2=>"Suất ăn công nghiệp",
        GasBussinessContract::C_ZONE_3=>"Hộ gia đình khó",
    );
     
    // ban đầu là array này => array(ROLE_SUB_USER_AGENT, ROLE_SALE, ROLE_MONITOR_AGENT);
    // không hiểu sao lại còn mỗi ROLE_SALE, Now 18, 2015 add thêm ROLE_SUB_USER_AGENT
    public $SAME_ROLE_SALE = array( // Những role user có thể tạo spancoop và có đặc tính giống role sale
//        ROLE_ADMIN,// for test only
        ROLE_SALE,
        ROLE_SUB_USER_AGENT, // Now 18, 2015 - chờ feedback
        ROLE_TELESALE, // Now 18, 2015 - chờ feedback
    );
    public $SAME_ROLE_MONITOR_AGEN = array( // Những role user có thể tạo spancoop và có đặc tính giống role sale
        ROLE_MONITOR_AGENT, 
        ROLE_EMPLOYEE_MARKET_DEVELOPMENT,
        ROLE_MONITORING_MARKET_DEVELOPMENT,
    );
    
    public $ROLE_PTTT = array( // Những role cua PTTT
        ROLE_EMPLOYEE_MARKET_DEVELOPMENT,
        ROLE_MONITORING_MARKET_DEVELOPMENT,
    );
    
    public $STATUS_ALLOW_UPDATE = array(
        GasBussinessContract::STATUS_DATA,
        GasBussinessContract::STATUS_ZONE_SPECIAL,
    );
    
    // lấy số ngày cho phép đại lý cập nhật
    public static function getDayAllowUpdate(){
        return Yii::app()->params['days_update_bussiness_contract'];        
    }    
    
    public static function getDayAllowUpdateGuideHelp(){
        return Yii::app()->params['days_update_guide_help'];
    }     
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
            return '{{_gas_bussiness_contract}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('b12, b45, note,status, employee_id, customer_name, address,phone,date_plan', 'required', 'on'=>'agent_create'),
            array('b12, b45, note,status, employee_id, customer_name, address,phone,date_plan', 'required', 'on'=>'agent_update'),
            
            array('b12, b45, note,status, customer_name, address,phone,date_plan', 'required', 'on'=>'sale_create'),
            array('b12, b45, note,status, customer_name, address,phone,date_plan', 'required', 'on'=>'sale_update'),
            
            array('type_customer, b12, b45, note,status, customer_name, address,phone,date_plan', 'required', 'on'=>'monitor_agent_create'),
            array('type_customer, b12, b45, note,status, customer_name, address,phone,date_plan', 'required', 'on'=>'monitor_agent_update'),
            
            array('customer_name, customer_contact', 'length', 'max'=>200),
            array('address', 'length', 'max'=>300),
            array('phone', 'length', 'max'=>100),
            array('note', 'length', 'max'=>500),
            array('date_load,belong_to_id,id, code_no, agent_id, uid_login, status, type, customer_id, employee_id, customer_name, customer_contact, address, phone, date_plan, b12, b45, note, created_date', 'safe'),
            array('type_customer, qty_of_big_customer,week_setup_id,qty_contract,first_purchase,date_assign_customer,still_thuong_luong,year,month,number_week', 'safe'),
            array('old_status, customer_zone,guide_help, guide_help_uid,guide_help_date', 'safe'),
            array('guide_help', 'length', 'max'=>5000),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rEmployee' => array(self::BELONGS_TO, 'Users', 'employee_id'),
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rWeekSetup' => array(self::BELONGS_TO, 'GasWeekSetup', 'week_setup_id'),
            'rGuideHelpUid' => array(self::BELONGS_TO, 'Users', 'guide_help_uid'),
            'rBelongToId' => array(self::BELONGS_TO, 'GasBussinessContract', 'belong_to_id'),
            'rComment' => array(self::HAS_MANY, 'GasComment', 'belong_id',
                'on'=>'rComment.type ='.GasComment::TYPE_1_SPANCOP,
                'order'=>'rComment.id DESC',
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'code_no' => 'Mã Số',
            'agent_id' => 'Đại Lý',
            'uid_login' => 'Người Tạo',
            'status' => 'Trạng Thái',
            'type' => 'Loại',
            'customer_id' => 'Khách Hàng Đã Tạo Trên Hệ Thống',
            'employee_id' => 'Nhân Viên Giao Hàng',
            'customer_name' => 'Tên Quán/Nhà Hàng',
            'customer_contact' => 'Tên Liên Hệ',
            'address' => 'Địa Chỉ',
            'phone' => 'Điện Thoại',
            'date_plan' => 'Kế Hoạch',
            'b12' => 'Bình 12',
            'b45' => 'Bình 45',
            'note' => 'Báo Cáo Tình Hình',
            'created_date' => 'Ngày Tạo',
            'year' => 'Năm',
            'month' => 'Tháng',
            'number_week' => 'Tuần',
            'date_assign_customer' => 'Ngày Gắn KH',
            'first_purchase' => 'Ngày Lấy Hàng',
            'qty_of_big_customer' => 'SL Bình 12 Từ',
            'type_customer' => 'Loại KH',            
            'guide_help' => 'HD thực hiện',
            'guide_help_uid' => 'Người tạo hướng dẫn thực hiện',
            'guide_help_date' => 'Ngày tạo hướng dẫn thực hiện',
            'customer_zone' => 'Khách Hàng Chuyên Đề',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     * admin/gasBussinessContract/index
     */    
    public function searchContractInWeek() // Báo Cáo Khách Hàng Ký HĐ tab 1
    {
        $criteria=new CDbCriteria;
        $strStatus =  GasBussinessContract::STATUS_KY_HD.','.GasBussinessContract::STATUS_HUY_BO;
        $strStatusHuyBo =  GasBussinessContract::STATUS_HUY_BO;
        $statusThuongLuong = GasBussinessContract::STATUS_THUONG_LUONG;
        $criteria->addCondition(" t.still_thuong_luong=0 AND ( ( t.status IN ($strStatus) AND row_auto=1 ) OR "
                . " ( t.status=$strStatusHuyBo AND row_auto=0 ) OR "
//        $criteria->addCondition(" t.still_thuong_luong=0 AND ( ( t.status IN ($strStatus) ) OR "
                . " (t.status=$statusThuongLuong AND row_auto=1) "
                . ")");

        self::AddRangeDate($this, $criteria);

        $sort = new CSort();
        $sort->attributes = array(
            'code_no'=>'code_no',
            'agent_id'=>'agent_id',
            'uid_login'=>'uid_login',
            'status'=>'status',
            'employee_id'=>'employee_id',
            'customer_name'=>'customer_name',
            'date_plan'=>'date_plan',
            'b12'=>'b12',
            'b45'=>'b45',
            'created_date'=>'created_date',                
        );    
        $sort->defaultOrder = 't.status DESC, t.id desc';

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'sort' => $sort,
                'pagination'=>array(
                    'pageSize'=> 50,
//                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
        ));
    }
    
    /**
     * @Author: ANH DUNG Nov 19, 2014
     * @Todo: belong to AddRangeDate
     * @Param: xử lý condition cho giám sát ĐL load thêm những ĐL được phân giám sát
     */
    public static function ProcessForMonitorAgent($model, &$criteria) {
        $session    = Yii::app()->session;
        $cUid       = MyFormat::getCurrentUid();
        //nếu có xét limit đại lý theo dõi
        if(isset($session['LIST_AGENT_OF_USER']) && count($session['LIST_AGENT_OF_USER'])){
//            $criteria->addCondition ("$nameField", $session['LIST_AGENT_OF_USER']);
            $sAgentId = implode(',', $session['LIST_AGENT_OF_USER'] );
//            if($cUid == GasLeave::TIN_TB){
                $aManyId = GasOneMany::getArrOfManyId( $cUid, ONE_USER_MANAGE_MULTIUSER );
                $aManyId[$cUid] = $cUid;
                $sManyId = implode(",", $aManyId);
                $criteria->addCondition (""
                    . "( t.agent_id IN ($sAgentId) OR t.uid_login IN ($sManyId) )");
//            }else{// có thể không cần đoạn này, vì đoạn trên nó đã bao gồm cả đoạn này rồi
//                $criteria->addCondition (""
//                    . "( t.agent_id IN ($sAgentId) OR t.uid_login=$cUid )");
//            }
        }else{ // không xét limit agent
            $criteria->compare('t.uid_login', $cUid);
        }
    }
    
    // add 1 số điều kiện chung cho 3 hàm search table, điều kiện chung cho 3 tab search
    public static function AddRangeDate($model, &$criteria, $needMore=array()){
        /* HÀM NÀY LÀ CHUNG CONDITION */
        // có thể sẽ không cần check điều kiện này, vì mỗi sale chuyên viên không làm theo đại lý
        // nên sẽ không cần giới hạn đại lý cho người xem        
        
        $userRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
//        $aRoleCheck = array(ROLE_SUB_USER_AGENT, ROLE_SALE, ROLE_MONITOR_AGENT);
        $aRoleCheck = $model->SAME_ROLE_SALE;
        // 03 Nov, 2014 - xử lý cho giám sát đại lý có thể xem của những đại lý của giám sát đó
        $aRoleCheckMonitorLimit = $model->SAME_ROLE_MONITOR_AGEN;        
        $aRoleView      = [ROLE_EXECUTIVE_OFFICER, ROLE_BRANCH_DIRECTOR];
        $aSaleVungTau    = [GasConst::UID_SU_BH,
            974623, // Nguyễn Minh Nhật
            GasConst::UID_KIEN_NT, // Nguyễn Trung Kiên
            976187,// Sale Nguyễn Văn Đông Sale Bò
        ];
        if(in_array($userRole, $aRoleCheck)){
            $criteria->addCondition('t.uid_login=' . $cUid);
        }
        
        if(in_array($userRole, $aRoleCheckMonitorLimit)){
            self::ProcessForMonitorAgent($model, $criteria);
            // 03 Nov, 2014 - xử lý cho giám sát đại lý có thể xem của những đại lý của giám sát đó
        }elseif(!in_array($userRole, $aRoleView)){
//            GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');// Feb2519 Close cho sale xem het
        }
        if(in_array($userRole, $aRoleView)){// Jun 04, 2017 phân quyền cho giám đốc điều hành xem KV Vũng Tàu
            $sParamsIn = implode(',', $aSaleVungTau);
            $criteria->addCondition("t.uid_login IN ($sParamsIn)");
        }       
        
        if( trim($model->code_no) != ""){
            $mFirst = self::GetByCodeNo($model->code_no);
            if($mFirst){                
                $criteria->compare('t.belong_to_id', $mFirst->id);
            }
        }        
        $criteria->compare('t.uid_login',$model->uid_login);
        $criteria->compare('t.type',$model->type);
        $criteria->compare('t.type_customer',$model->type_customer);
        $criteria->compare('t.customer_id',$model->customer_id);
        $criteria->compare('t.employee_id',$model->employee_id);
        $criteria->compare('t.customer_name',$model->customer_name,true);
        $criteria->compare('t.customer_contact',$model->customer_contact,true);
        $criteria->compare('t.address',$model->address,true);
        $criteria->compare('t.phone',$model->phone,true);
        
        if($userRole==ROLE_HEAD_GAS_BO){
//            $criteria->compare('t.type', GasBussinessContract::TYPE_SALE_BO);// remove at Apr 11, 2017
        }elseif($userRole==ROLE_HEAD_GAS_MOI){ // GasBussinessContract::TYPE_SALE_MOI_NORMAL;
//            $criteria->compare('t.type', GasBussinessContract::TYPE_SALE_MOI_CHUYEN_VIEN);
            $aTypeMoi = array(GasBussinessContract::TYPE_SALE_MOI_CHUYEN_VIEN, 
                        GasBussinessContract::TYPE_AGENT,
                        GasBussinessContract::TYPE_MONITOR_AGENT,
                        GasBussinessContract::TYPE_NV_PTTT,
                    );
            $sParamsIn = implode(',', $aTypeMoi);
            $criteria->addCondition("t.type IN ($sParamsIn)");
        }
        
        /******* đoạn này liên quan đến $model->number_week - load khoảng ngày của tuần, hoặc load tháng 
         * Dec 13, 2014 với KH là STATUS_DATA thì sẽ load hết lên, không load theo tuần nữa
         * vì những KH này sẽ được cập nhật STATUS sang 1 STATUS khác
         */
        if(!isset($needMore['STATUS_DATA'])){
            if(!$model->number_week){
                // nếu không chọn tuần thì sẽ load tháng lên
                if(empty($model->month))
                    $model->month = date('m');
                if(empty($model->year))
                    $model->year = date('Y');
                $criteria->compare('month(t.date_load)', $model->month);
                $criteria->compare('year(t.date_load)', $model->year);
                return;
            }
            $session=Yii::app()->session;
            if(!isset($session['YEAR_WEEK_SETUP'][$model->year][$model->month][$model->number_week])){
                unset($session['YEAR_WEEK_SETUP']);
                GasWeekSetup::InitSessionWeekNumberByYear($model->year);
                $session=Yii::app()->session;
            }
            $mWeek = $session['YEAR_WEEK_SETUP'][$model->year][$model->month][$model->number_week];
            $model->mCurrentWeek = $mWeek;
            $criteria->addBetweenCondition("t.date_load",$mWeek->date_from, $mWeek->date_to);
        }
        
        /******* đoạn này liên quan đến $model->number_week - load khoảng ngày của tuần, hoặc load tháng */
                
        
//        $criteria->compare('t.code_no',$model->code_no,true);
//        $criteria->compare('t.agent_id',$model->agent_id,true);
//        $criteria->compare('t.uid_login',$model->uid_login,true);        
//        $criteria->compare('t.date_plan',$model->date_plan,true);
//        $criteria->compare('t.b12',$model->b12);
//        $criteria->compare('t.b45',$model->b45);
//        $criteria->compare('t.note',$model->note,true);        
    }

    // admin/gasBussinessContract/index
    public function searchThuongLuong() // Báo Cáo Khách Hàng Thương Lượng tab 2
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.status', GasBussinessContract::STATUS_THUONG_LUONG);
//        $criteria->compare('t.row_auto', 0);
        $criteria->addCondition(" ( t.still_thuong_luong=1 OR "
                . " (row_auto=0) "
                . ")");
        
        self::AddRangeDate($this, $criteria);

        $sort = new CSort();
        $sort->attributes = array(
            'code_no'=>'code_no',
            'agent_id'=>'agent_id',
            'uid_login'=>'uid_login',
            'status'=>'status',
            'employee_id'=>'employee_id',
            'customer_name'=>'customer_name',
            'date_plan'=>'date_plan',
            'b12'=>'b12',
            'b45'=>'b45',
            'created_date'=>'created_date',                
        );    
        $sort->defaultOrder = 't.status DESC, t.id desc'; 

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'sort' => $sort,
                'pagination'=>array(
                    'pageSize'=> 50,
//                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
        ));
    }
    
    // admin/gasBussinessContract/index
    public function searchContractOk() // Báo Cáo Khách Hàng Phát Sinh Ký HĐ tab 3
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.status', GasBussinessContract::STATUS_KY_HD);
        $criteria->compare('t.row_auto', 0);
        self::AddRangeDate($this, $criteria);        

        $sort = new CSort();
        $sort->attributes = array(
            'code_no'=>'code_no',
            'agent_id'=>'agent_id',
            'uid_login'=>'uid_login',
            'status'=>'status',
            'employee_id'=>'employee_id',
            'customer_name'=>'customer_name',
            'date_plan'=>'date_plan',
            'b12'=>'b12',
            'b45'=>'b45',
            'created_date'=>'created_date',                
        );    
        $sort->defaultOrder = 't.status DESC, t.id desc'; 

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'sort' => $sort,
                'pagination'=>array(
                    'pageSize'=> 50,
//                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
        ));
    }
    
    // admin/gasBussinessContract/index
    public function searchCustomerData() // Báo Cáo Khách Hàng DATA tab 4
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.status', GasBussinessContract::STATUS_DATA);
        $criteria->compare('t.row_auto', 0);
//        self::AddRangeDate($this, $criteria, array( 'STATUS_DATA' => 1) );
        self::AddRangeDate($this, $criteria, array() );// Apr 03, 2015 bỏ đoạn check load hết KH DATA ra, sẽ load theo tuần như các tab khác 

        $sort = new CSort();
        $sort->attributes = array(
            'code_no'=>'code_no',
            'agent_id'=>'agent_id',
            'uid_login'=>'uid_login',
            'status'=>'status',
            'employee_id'=>'employee_id',
            'customer_name'=>'customer_name',
            'date_plan'=>'date_plan',
            'b12'=>'b12',
            'b45'=>'b45',
            'created_date'=>'created_date',                
        );    
        $sort->defaultOrder = 't.status DESC, t.id desc'; 

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'sort' => $sort,
                'pagination'=>array(
                    'pageSize'=> 20,
//                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
        ));
    }
    
    /**
     * @Author: ANH DUNG Sep 27, 2015
     * @Todo: Load hết KH chuyên đề đặc biệt để cho cập nhật báo cáo tình hình
     * @Param: $model
     */
    public function searchCustomerZoneSpecial() // Báo Cáo Khách Hàng Chuyên Đề Đặc Biệt tab 5
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.status', GasBussinessContract::STATUS_ZONE_SPECIAL);
        $criteria->compare('t.row_auto', 0);
        self::AddRangeDate($this, $criteria, array( 'STATUS_DATA' => 1) );
        // Sep 03, 2015 cho load hết KH DATA vì mỗi sale có 10 KH ở tab này, theo dõi hàn tuần, nhập Bc tình hình

        $sort = new CSort();
        $sort->attributes = array(
            'code_no'=>'code_no',
            'agent_id'=>'agent_id',
            'uid_login'=>'uid_login',
            'status'=>'status',
            'employee_id'=>'employee_id',
            'customer_name'=>'customer_name',
            'date_plan'=>'date_plan',
            'b12'=>'b12',
            'b45'=>'b45',
            'created_date'=>'created_date',                
        );    
        $sort->defaultOrder = 't.status DESC, t.id desc'; 

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'sort' => $sort,
                'pagination'=>array(
                    'pageSize'=> 20,
//                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
        ));
    }
    
    
    public static function ConditionLimit(&$criteria, $model){
//        GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');
        $userRole = Yii::app()->user->role_id;
        $aRoleCheck = $model->SAME_ROLE_SALE;
        if(in_array($userRole, $aRoleCheck)){
            $criteria->compare('t.uid_login', Yii::app()->user->id);
        }
        if($userRole == ROLE_HEAD_GAS_BO){
            $criteria->compare('t.type', GasBussinessContract::TYPE_SALE_BO);
        }
        
        if(!empty($model->qty_of_big_customer))
            $criteria->addCondition('t.b12>='.$model->qty_of_big_customer);
        
        $criteria->compare('t.uid_login',$model->uid_login);
        $criteria->compare('t.type',$model->type);
        $criteria->compare('t.type_customer',$model->type_customer);
        $criteria->compare('t.customer_id',$model->customer_id);
        $criteria->compare('t.employee_id',$model->employee_id,true);
        $criteria->compare('t.customer_name',$model->customer_name,true);
        $criteria->compare('t.customer_contact',$model->customer_contact,true);
        $criteria->compare('t.address',$model->address,true);
        $criteria->compare('t.phone',$model->phone,true);
        
    }

    public function search_was_sign_contract() // Danh Sách SPANCOP đã ký hợp đồng, chưa gắn khách hàng
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.status', GasBussinessContract::STATUS_KY_HD);
        $criteria->addCondition('t.customer_id=0');
        self::ConditionLimit($criteria, $this);
        $sort = new CSort();
        $sort->attributes = array(
            'code_no'=>'code_no',
            'agent_id'=>'agent_id',
            'uid_login'=>'uid_login',
            'status'=>'status',
            'employee_id'=>'employee_id',
            'customer_name'=>'customer_name',
            'date_plan'=>'date_plan',
            'b12'=>'b12',
            'b45'=>'b45',
            'created_date'=>'created_date',                
            'date_assign_customer'=>'date_assign_customer',                
            'first_purchase'=>'first_purchase',                
        );    
        $sort->defaultOrder = 't.date_plan'; 

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'sort' => $sort,
                'pagination'=>array(
                    'pageSize'=> 50,
//                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
        ));
    }
    
    public function search_was_sign_contract_ready_system() // Danh Sách SPANCOP đã ký hợp đồng, đã gắn khách hàng
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.status', GasBussinessContract::STATUS_KY_HD);
        $criteria->addCondition('t.customer_id!=0');
        self::ConditionLimit($criteria, $this);
        $sort = new CSort();
        $sort->attributes = array(
            'code_no'=>'code_no',
            'agent_id'=>'agent_id',
            'uid_login'=>'uid_login',
            'status'=>'status',
            'employee_id'=>'employee_id',
            'customer_name'=>'customer_name',
            'date_plan'=>'date_plan',
            'b12'=>'b12',
            'b45'=>'b45',
            'created_date'=>'created_date',
            'date_assign_customer'=>'date_assign_customer',                
            'first_purchase'=>'first_purchase',
        );    
        $sort->defaultOrder = 't.first_purchase'; 

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'sort' => $sort,
                'pagination'=>array(
                    'pageSize'=> 50,
//                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
        ));
    }
    
    public function search_big_customer() // Danh Sách SPANCOP đã ký hợp đồng, chưa gắn khách hàng
    {
        $criteria=new CDbCriteria;
//        $criteria->compare('t.status', GasBussinessContract::STATUS_KY_HD);        
//        self::ConditionLimit($criteria, $this);
        $sort = new CSort();
        $sort->attributes = array(
            'code_no'=>'code_no',
            'agent_id'=>'agent_id',
            'uid_login'=>'uid_login',
            'status'=>'status',
            'employee_id'=>'employee_id',
            'customer_name'=>'customer_name',
            'date_plan'=>'date_plan',
            'b12'=>'b12',
            'b45'=>'b45',
            'created_date'=>'created_date',
            'date_assign_customer'=>'date_assign_customer',
            'first_purchase'=>'first_purchase',
        );
        $sort->defaultOrder = 't.date_plan DESC, t.status ASC';
        
//        $aIdLimit = self::GetMaxIdOfEachGroup($this); // cho luon vao cau ben duoi de tang speed
        $criteria->addInCondition('t.id', self::GetMaxIdOfEachGroup($this));
        
//        $sort->defaultOrder = 't.date_plan DESC';
//        $criteria->group = "t.belong_to_id DESC"; // doan nay da xu ly o ben duoi (GetMaxIdOfEachGroup) roi khong can thiet nua
        //http://stackoverflow.com/questions/1313120/retrieving-the-last-record-in-each-group
        // đoạn dưới vẫn đúng, sẽ là lấy max id cho each group by
//        $criteria->select = "MAX(id) as id, t.belong_to_id, t.row_auto, t.still_thuong_luong"
//                . ", t.code_no, t.agent_id, t.uid_login, t.status, t.type "
//                . ", t.customer_id, t.employee_id, t.customer_name, t.customer_contact, t.address "
//                . ", t.phone, t.date_plan, t.date_load, t.b12, t.b45,t.note, t.created_date,t.last_update_time "
//                . ", t.date_assign_customer, t.first_purchase, t.week_setup_id";
        
        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'sort' => $sort,
                'pagination'=>array(
                    'pageSize'=> 50,
//                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
        ));
    }
    
    /**
     * @Author: ANH DUNG Nov 19, 2014
     * @Todo: get array max id of each group belong_to_id
     */
    public static function GetMaxIdOfEachGroup($model) {
        $criteria=new CDbCriteria;
        self::ConditionLimit($criteria, $model);
        $criteria->group = "t.belong_to_id DESC";
        $criteria->select = "MAX(id) as id, t.belong_to_id";
        return CHtml::listData(self::model()->findAll($criteria),'id','id');
    }
    

    public function defaultScope()
    {
        return array(
                //'condition'=>'',
        );
    }
    
    protected function beforeSave() {
        if(!isset(Yii::app()->user)){ // do chạy cron ở console nên sẽ không lấy dc user login Yii::app()->user
            return parent::beforeSave();// 0 */3 * * *   root  /usr/bin/php /var/www/daukhimiennam.com/web/cron.php CronUpdateFirstPurchase = chay 3h 1 lần
        }
        if(strpos($this->date_plan, '/')){
            $this->date_plan =  MyFormat::dateConverDmyToYmd($this->date_plan);
            MyFormat::isValidDate($this->date_plan);
        }
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        //  $this->normal_record là biến kiểm tra xem có phải record dc tạo trên giao diện ( = 1 )
        // hay được tạo ngầm bên dưới ( = 0) với status là thương lượng
        if($this->isNewRecord && $this->normal_record ){
            $this->uid_login = $cUid;
            $this->code_no = MyFunctionCustom::getNextId('GasBussinessContract', 'B'.date('y'), LENGTH_TICKET,'code_no');
            // type loại: 1: đại lý, 2: sale bò, 3: sale mối, 4: sale moi bt, 5: giam sat dai ly
            // cột đại lý sẽ là select đại lý nếu user là sale bò mối
            
            // 1. user dai ly
            if($cRole == ROLE_SUB_USER_AGENT){
                $this->type = GasBussinessContract::TYPE_AGENT;
                $this->type_customer = STORE_CARD_KH_MOI;
            }elseif($cRole == ROLE_SALE){
                // 2. user sale
                $this->type = GasBussinessContract::TYPE_SALE_MOI_CHUYEN_VIEN;
                $this->type_customer = STORE_CARD_KH_MOI;
                if(Yii::app()->user->gender == Users::SALE_BO){
                    $this->type = GasBussinessContract::TYPE_SALE_BO;
                    $this->type_customer = STORE_CARD_KH_BINH_BO;
                }elseif(Yii::app()->user->gender == Users::SALE_MOI){
                    $this->type = GasBussinessContract::TYPE_SALE_MOI_NORMAL;
                }
                
                $this->employee_id = $cUid; // nếu là sale thì nv giao hàng chính là tên sale luôn
            }elseif (in_array($cRole, $this->SAME_ROLE_MONITOR_AGEN)){
                // 3. user giam sat dai ly + NV PTTT (Sep 24, 2025 cho phep NV PTTT tao )
                // voi user nay thi $this->type_customer da dc chon tren giao dien
                $this->type = GasBussinessContract::TYPE_MONITOR_AGENT;
                if(in_array($cRole, $this->ROLE_PTTT)){
                    $this->type = GasBussinessContract::TYPE_NV_PTTT;
                }
                $this->employee_id = $cUid; // nếu là giám sát đại lý thì nv giao hàng chính là tên sale luôn
            }
            
            $this->date_load = date('Y-m-d');
        }
        
        if($cRole == ROLE_SUB_USER_AGENT && $this->normal_record){
            $this->agent_id = MyFormat::getAgentId();
        }        
        $mWeek = GasWeekSetup::GetModelWeekByDate($this->date_load);
        if($mWeek){
            $this->week_setup_id = $mWeek->id; 
        }
        
        $aAtt = array('customer_name','customer_contact', 'address', 'note');
        MyFormat::RemoveScriptBad($this, $aAtt);
        $this->guide_help = InputHelper::removeScriptTag($this->guide_help);
        
        self::HandleSpancopDataUpdate($this); // Apr 03, 2015
        
//        if(!$this->isNewRecord){ // định dùng ở HandlePostComment trên controller nhưng mà chưa test dc nên chưa làm
//            $this->last_update_time = date('Y-m-d H:i:s');
//        }

        return parent::beforeSave();
    }    
    
    /**
     * @Author: ANH DUNG Apr 03, 2015
     * @Todo: xử lý cho khi update Spancop Data từ DATA -> KÝ HỢP ĐỒNG - CHỈNH LẠI DATE_LOAD
     * @Param: $model
     */
    public static function HandleSpancopDataUpdate(&$model) {
        // Apr 03, 2015 - xử lý cho khi update Spancop Data từ DATA -> KÝ HỢP ĐỒNG - CHỈNH LẠI DATE_LOAD
        if(!$model->isNewRecord){
            $OldModel = self::model()->findByPk($model->id);
//            if($OldModel->status == GasBussinessContract::STATUS_DATA ){ // Sep 28, 2015 thêm 1 loại nữa giống STATUS_DATA là Chuyên ĐỀ ĐẶc biệt 
            if( in_array($OldModel->status, $model->STATUS_ALLOW_UPDATE) ){
                if($model->status != GasBussinessContract::STATUS_THUONG_LUONG){
                    // Jun 09, 2015 chỉnh lại nếu update từ STATUS_DATA sang STATUS_THUONG_LUONG thì sẽ không chỉnh lại DATE_LOAD
                    // Sep 28, 2015 với KH Chuyên Đề đặc biệt thì chưa check+test chỗ này dc
                    $model->date_load = $model->date_plan;
                }else{ // STATUS_THUONG_LUONG
                    // Jun 13, 2015 nếu update từ STATUS_DATA sang STATUS_THUONG_LUONG thì sẽ chỉnh lại DATE_LOAD là ngày hiện tại
                    $model->date_load = date('Y-m-d');
                }
                
//                if($model->status != GasBussinessContract::STATUS_DATA){
                // Sep 28, 2015 thêm status KH Chuyên Đề đặc biệt nữa vào đk check
                if( !in_array($model->status, $model->STATUS_ALLOW_UPDATE) ){
                    //Jun 13, 2015 nếu update từ STATUS_DATA sang bat cu status nao 
                    //thi se luu vet lai status data vao cot old_status
                    $model->old_status = GasBussinessContract::$ARR_STATUS[GasBussinessContract::STATUS_DATA];
                }
            }
        }
        // Apr 03, 2015 - xử lý cho khi update Spancop Data từ DATA -> KÝ HỢP ĐỒNG - CHỈNH LẠI DATE_LOAD
    }
    
    /**
     * @Author: ANH DUNG Jul 24, 2014
     * @Todo: cập nhật belong_to_id, là id đầu tiên của hd đó, nếu bị kéo dài nhiều lần thì sẽ có cùng belong_to_id
     * @Param: $model model 
     */
    public static function UpdateBelongToId($model, $belong_to_id){
        $model->belong_to_id = $belong_to_id;
        $model->update(array('belong_to_id'));
    }
    
    /**
     * @Author: ANH DUNG Jul 24, 2014
     * @Todo: kiểm tra nếu là thương lượng thì tạo thêm record mới cho nó nằm ở ngày đó
     * xử lý luôn khi tạo mới có trạng thái là thương lượng thì sẽ tạo mới 1 record ở tuần kế tiếp nằm ở list đầu
     * @Param: $model model 
     */
    public static function NewRecordThuongLuong($model){
        if($model->status != GasBussinessContract::STATUS_THUONG_LUONG) return;
        // nếu update row thương lượng thì cũng update luôn row mà dc add vào tuần kế tiếp
        $mNew = self::IsExistThuongLuong($model);
        $newCode=0;
        if(is_null($mNew)){
            $mNew = new GasBussinessContract();
            $newCode=1;
        }
        $mNew->belong_to_id = $model->belong_to_id;
        $mNew->row_auto = 1; // 1: auto gen khi có status là thưong lượng, 0: normal
        if($newCode){
            $mNew->code_no = MyFunctionCustom::getNextId('GasBussinessContract', 'B'.date('y'), LENGTH_TICKET,'code_no');
        }
            
        $mNew->agent_id = $model->agent_id;
        $mNew->uid_login = $model->uid_login;
        $mNew->status = $model->status;
        $mNew->type = $model->type;
        $mNew->type_customer = $model->type_customer;
        $mNew->customer_id = $model->customer_id;
        $mNew->employee_id = $model->employee_id;
        $mNew->customer_name = $model->customer_name;
        $mNew->customer_contact = $model->customer_contact;
        $mNew->address = $model->address;
        $mNew->phone = $model->phone;
        $mNew->date_plan = $model->date_plan;
        $mNew->date_load = $model->date_plan;
        $mNew->b12 = $model->b12;
        $mNew->b45 = $model->b45;
        $mNew->note = '';
        $mNew->normal_record = 0;        
        $mNew->save();        
    }
    
    public static function IsExistThuongLuong($model){
        $today = date('Y-m-d');
        $criteria = new CDbCriteria();
        $criteria->compare('t.uid_login', $model->uid_login);
        $criteria->addCondition("t.date_load > '$today'");        
        $criteria->compare('t.belong_to_id', $model->belong_to_id);
        $criteria->compare('t.row_auto', 1);
        $criteria->compare('t.still_thuong_luong', 0);
        $criteria->compare('t.status', GasBussinessContract::STATUS_THUONG_LUONG);
        //date_load: gần như ngày tạo record, để load record lên grid
        return self::model()->find($criteria);
    }
    
    protected function afterValidate() {
        // kiểm tra nếu là record bình thường thì validate, record tạo auto thì ko cần        
        if( $this->normal_record && $this->scenario !='Update_customer_sign_contract' ){            
            // scenario Update_customer_sign_contract là update KH nen ko check gì cả
            $mCurrentWeek = GasWeekSetup::GetModelCurrentWeekNumber();
            $date_plan = $this->date_plan; // có thể là $this->date_plan chứ ko phải là date('Y-m-d')
            if(strpos($this->date_plan, '/')){
                $date_plan =  MyFormat::dateConverDmyToYmd($this->date_plan);
            }
            if($this->status == GasBussinessContract::STATUS_THUONG_LUONG){
                // ngày kế hoạch phải nằm sang tuần tiếp theo, phải lớn hơn ngày cuối của tuần này
                if(! MyFormat::compareTwoDate($date_plan, $mCurrentWeek->date_to)){
                    $this->addError('date_plan', 'Ngày kế hoạch thương lượng phải là ngày của tuần tiếp theo');
                }
                
                $mNextWeek = GasWeekSetup::GetModelNextWeek();
                if($mNextWeek){
                    $date_from = MyFormat::modifyDays($mNextWeek->date_from, 1, '-');
                    $date_to = MyFormat::modifyDays($mNextWeek->date_to, 1);
                    if(! MyFormat::compareTwoDate($date_plan, $date_from) || ! MyFormat::compareTwoDate($date_to, $date_plan)){
                        $this->addError('date_plan', 'Ngày kế hoạch thương lượng phải là ngày của tuần tiếp theo. Over date range');
                    }
                }
                
            }elseif($this->status == GasBussinessContract::STATUS_KY_HD){
                // ngày kế hoạch ký hợp đồng phải nằm trong tuần hiện tại
                $date_from = MyFormat::modifyDays($mCurrentWeek->date_from, 1, '-');
                $date_to = MyFormat::modifyDays($mCurrentWeek->date_to, 1);
                        
                if(! MyFormat::compareTwoDate($date_plan, $date_from) || ! MyFormat::compareTwoDate($date_to, $date_plan)){
                    $this->addError('date_plan', 'Ngày ký hợp đồng phải là ngày của tuần hiện tại');
                }
            }
        }
        return parent::afterValidate();
    }
    
    /**
     * @Author: ANH DUNG Aug 08, 2014
     * @Todo: delete all sub of model auto gen with status STATUS_THUONG_LUONG
     * @Param: $model model STATUS_THUONG_LUONG root
     */
    public static function DeleteAllSubModelAutoGenThuongLuong($model){
        if($model->row_auto==0 && $model->status == GasBussinessContract::STATUS_THUONG_LUONG ){
            $criteria = new CDbCriteria();
            $criteria->compare('belong_to_id', $model->id);
            $criteria->compare('row_auto', 1);
            self::model()->deleteAll($criteria);
        }
    }
    
    /**
     * @Author: ANH DUNG Aug 19, 2014
     * @Todo: quick report spancop
     * @Param: $model GasBussinessContract
     */
    public static function QuickReport($model){
        $aRes = array();        
//        $criteria->select = " count(*) as qty_contract, t.type, t.employee_id, t.status";
//        $criteria->group = "t.type, t.employee_id, t.status";
        // sẽ dùng foreach để render và count mảng . Foreach mảng theo loại bò-mối, employee_id, status
        // 1. lấy khách hàng phát sinh ký hợp đồng, khách hàng thương lượng với rows auto =  0
        //  trong lúc for của 1 sẽ tách ra count chưa lấy gas và đã lấy gas  cột 1,2 là tab 2,3
        // 2. khách hàng ký hợp đồng rows auto=1. trong lúc for của 2 sẽ tách ra 
        // count chưa lấy gas và đã lấy gas - cột 3 là tab 1
        // 3. lấy khách hàng phát sinh ký hợp đồng và khách hàng ký hợp đồng chưa lấy gas, 
        // từ các tuần  trước đến tuần hiện tại
        self::GetPhatSinhHD_ThuongLuong($aRes, $model);
        self::GetKyHD($aRes, $model);
        self::GetKyHDNotFirstPurchase($aRes, $model, $model->mCurrentWeek);
        self::GetKyHDHaveFirstPurchase($aRes, $model, $model->mCurrentWeek);
        return $aRes;
    }
    
    /**
     * @Author: ANH DUNG Aug 19, 2014 belong to QuickReport
     * @Todo: 1. lấy khách hàng phát sinh ký hợp đồng, khách hàng thương lượng với rows auto =  0
    //  trong lúc for của 1 sẽ tách ra count chưa lấy gas và đã lấy gas 
     * @Param: &$aRes, $model, $criteria - cột 1,2 là tab 2,3
     */
    public static function GetPhatSinhHD_ThuongLuong(&$aRes, $model){
        $criteria = new CDbCriteria();
        self::AddConditionSame1($criteria, $model);
        self::AddConditionPost($criteria, $model);        
        
        $criteria->compare('t.row_auto', 0);
//        $criteria->addInCondition('t.status', array(GasBussinessContract::STATUS_KY_HD, GasBussinessContract::STATUS_THUONG_LUONG));
        $sParamsIn = implode(',', array(GasBussinessContract::STATUS_KY_HD, GasBussinessContract::STATUS_THUONG_LUONG));
        $criteria->addCondition("t.status IN ($sParamsIn)");
        
        $models = self::model()->findAll($criteria);
        foreach($models as $item){
            if($item->status==GasBussinessContract::STATUS_THUONG_LUONG){
                $aRes['PhatSinhHD_ThuongLuong'][$item->type][$item->employee_id][$item->status][] = $item;
            }else{
                if(empty($item->first_purchase)){  // chưa lấy hàng. type là sale bò, mối hay chuyên viên mối
                    $aRes['PhatSinhHD_ThuongLuong'][$item->type][$item->employee_id][$item->status]['not_first_purchase'][] = $item;
                } else { // đã lấy hàng
                    $aRes['PhatSinhHD_ThuongLuong'][$item->type][$item->employee_id][$item->status]['first_purchase'][] = $item;
                }
                $aRes['LOOP'][$item->type][$item->employee_id] = $item->employee_id;
                    $aRes['FOR_MODEL'][$item->employee_id] = $item->employee_id;
                if(!empty($item->agent_id)){
                    $aRes['FOR_MODEL_AGENT'][$item->employee_id] = $item->agent_id;
                }
            }            
        }        
    }
    
    /**
     * @Author: ANH DUNG Aug 19, 2014 belong to QuickReport
     * @Todo: 2. khách hàng ký hợp đồng rows auto=1. trong lúc for của 2 sẽ tách ra count chưa lấy gas và đã lấy gas 
     * @Param: &$aRes, $model, $criteria - cột 3 là tab 1
     */ 
    public static function GetKyHD(&$aRes, $model){
        $criteria = new CDbCriteria();
        self::AddConditionSame1($criteria, $model);
        self::AddConditionPost($criteria, $model);        
        
        $criteria->compare('t.row_auto', 1);
        $models = self::model()->findAll($criteria);
        foreach($models as $item){
            if($item->status!=GasBussinessContract::STATUS_KY_HD){
                $aRes['KyHD'][$item->type][$item->employee_id][$item->status][] = $item;
            }else{
                if(empty($item->first_purchase)){  // chưa lấy hàng
                    $aRes['KyHD'][$item->type][$item->employee_id][$item->status]['not_first_purchase'][] = $item;
                } else { // đã lấy hàng
                    $aRes['KyHD'][$item->type][$item->employee_id][$item->status]['first_purchase'][] = $item;
                }
            }
            $aRes['LOOP'][$item->type][$item->employee_id] = $item->employee_id;
            $aRes['FOR_MODEL'][$item->employee_id] = $item->employee_id;
            if(!empty($item->agent_id)){
                $aRes['FOR_MODEL_AGENT'][$item->employee_id] = $item->agent_id;
            }
        }
    }
    
    /**
     * @Author: ANH DUNG Aug 19, 2014 belong to QuickReport
     * @Todo: 3. lấy khách hàng phát sinh ký hợp đồng và khách hàng ký hợp đồng chưa lấy gas, từ các tuần  trước đến
     * tuần hiện tại
     * @Param: &$aRes, $model, $criteria nằm ở cột 3, 1
     */
    public static function GetKyHDNotFirstPurchase(&$aRes, $model, $mCurrentWeek){
        $criteria = new CDbCriteria();
        self::AddConditionPost($criteria, $model);
        $criteria->addCondition("t.date_load < '$mCurrentWeek->date_from' AND t.first_purchase IS NULL ");
        $criteria->compare('t.status', GasBussinessContract::STATUS_KY_HD);
        $models = self::model()->findAll($criteria);
        foreach($models as $item){
//            if($item->row_auto == 1){ // khách hàng phát sinh ký hợp đồng
            $aRes['KyHDNotFirstPurchase'][$item->type][$item->employee_id][$item->row_auto][] = $item;
            $aRes['LOOP'][$item->type][$item->employee_id] = $item->employee_id;
            $aRes['FOR_MODEL'][$item->employee_id] = $item->employee_id;
            if(!empty($item->agent_id)){
                $aRes['FOR_MODEL_AGENT'][$item->employee_id] = $item->agent_id;
            }
        }
    }
        
    /**
     * @Author: ANH DUNG Aug 19, 2014 belong to QuickReport
     * @Todo: 3. lấy khách hàng phát sinh ký hợp đồng và khách hàng ký hợp đồng chưa lấy gas, từ các tuần  trước đến
     * tuần hiện tại
     * @Param: &$aRes, $model, $criteria nằm ở cột 3, 1
     */
    public static function GetKyHDHaveFirstPurchase(&$aRes, $model, $mCurrentWeek){
        $criteria = new CDbCriteria();
        self::AddConditionPost($criteria, $model);
        $criteria->addBetweenCondition("t.first_purchase",$mCurrentWeek->date_from, $mCurrentWeek->date_to);
        $criteria->addCondition("t.date_load < '$mCurrentWeek->date_from' ");
        $criteria->compare('t.status', GasBussinessContract::STATUS_KY_HD);
        $models = self::model()->findAll($criteria);
        foreach($models as $item){
//            if($item->row_auto == 1){ // khách hàng phát sinh ký hợp đồng từ những tuần trc và tuần này lấy hàng
            $aRes['KyHDHaveFirstPurchaseCurrentWeek'][$item->type][$item->employee_id][$item->row_auto][] = $item;
            $aRes['LOOP'][$item->type][$item->employee_id] = $item->employee_id;
            $aRes['FOR_MODEL'][$item->employee_id] = $item->employee_id;
            if(!empty($item->agent_id)){
                $aRes['FOR_MODEL_AGENT'][$item->employee_id] = $item->agent_id;
            }
        }
    }
    
    public static function AddConditionPost(&$criteria, $model){
        if(!empty($model->type))
            $criteria->compare('t.type', $model->type);
        if(!empty($model->uid_login))
            $criteria->compare('t.uid_login', $model->uid_login);
    }
    
    public static function AddConditionSame1(&$criteria, $model){
        $session=Yii::app()->session;
        if(!isset($session['YEAR_WEEK_SETUP'][$model->year][$model->month][$model->number_week])){
            unset($session['YEAR_WEEK_SETUP']);
            GasWeekSetup::InitSessionWeekNumberByYear($model->year);
            $session=Yii::app()->session;
        }
        $mCurrentWeek = $session['YEAR_WEEK_SETUP'][$model->year][$model->month][$model->number_week];
        $model->mCurrentWeek = $mCurrentWeek;
        $criteria->addBetweenCondition("t.date_load",$mCurrentWeek->date_from, $mCurrentWeek->date_to);        
    }


    /**
     * @Author: ANH DUNG Aug 19, 2014
     * @Todo: cron chạy cập nhật first_purchase của KH
     * hiện tại lấy những khách hàng dc điều phối add 1 tháng trước
     */
    public static function CronUpdateFirstPurchase(){
        return ;// Anh Dung Close Dec 09, 2015, vì điều phối hiện tại không gắn KH cho phần 
        // Spancop nên sẽ không chạy function này nữa, sẽ mở lại khi nào cần
        
        $criteria = new CDbCriteria();
        $criteria->compare('t.status', GasBussinessContract::STATUS_KY_HD);
        $criteria->addCondition('t.customer_id!=0');
        $date_limit = date('Y-m-d'); // trừ đi 1 tháng tính từ ngày hiện tại
        $month_update_first_purchase = Yii::app()->setting->getItem('month_update_first_purchase');
        $date_limit = MyFormat::modifyDays($date_limit, $month_update_first_purchase, '-', 'month');
        $criteria->addCondition("t.date_assign_customer >= '$date_limit' ");
        $models = self::model()->findAll($criteria);
        foreach($models as $item){
            $mStoreCard = GasStoreCard::GetFirstPurchase($item->customer_id);
            if($mStoreCard){
                if($mStoreCard->date_delivery != $item->first_purchase){
                    $item->first_purchase = $mStoreCard->date_delivery;
                    $item->update(array('first_purchase'));
                }
            }else{
                if(!empty($item->first_purchase)){
                    $item->first_purchase = null;
                    $item->update(array('first_purchase'));
                }
            }
        }
//        SendEmail::TestCron(); //close Dec 21, 2014
    }
    
    /**
     * @Author: ANH DUNG Nov 17, 2014
     * @Todo: upate guide help 
     * @Param: $model
     */
    public static function Update_guide_help($model) {
        $model->guide_help_uid = Yii::app()->user->id;
        $model->guide_help_date = date('Y-m-d H:i:s');
        $model->update(array('guide_help', 'guide_help_uid', 'guide_help_date'));
    }
    
    /**
     * @Author: ANH DUNG Nov 18, 2014
     * @Todo: get list option
     * them 1 cột  KH chuyên đề: 1: nhà hàng tiệc cưới,  2: suất ăn công nghiệp,...
     */
    public static function GetCustomerZone() {
        return GasBussinessContract::$ARR_C_ZONE;
    }
    
    // Nov 18, 2014. get customer_zone
    public static function GetCustomerZoneView ($customer_zone) {
        return isset(GasBussinessContract::$ARR_C_ZONE[$customer_zone])?GasBussinessContract::$ARR_C_ZONE[$customer_zone]:'';
    }
    
    /**
     * @Author: ANH DUNG Nov 18, 2014
     * @Todo: get model by codeno
     */
    public static function GetByCodeNo($code_no, $like = false) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.code_no', trim($code_no), $like);
        return self::model()->find($criteria);
    }
    
    /**
     * @Author: ANH DUNG Nov 18, 2014
     * !IMPORTANT REMEMBER select = "MAX(id) AT => $criteria->select = "MAX(id) as id, t.belong_to_id";
     * http://stackoverflow.com/questions/1313120/retrieving-the-last-record-in-each-group
     * @Todo: build array id của 1 thương lượng => last id of record mới nhất của thương lượng đó
     * để phục vụ phần kiểm tra cho phép update thương lượng trên grid
     * sẽ lấy trong khoảng 3 tháng cho đỡ record
     * @Ex: Array
            (
                [belong_to_id] => id
                [4] => 6
                [2] => 3
            )
     */
    public static function BuidArrayLastIdThuongLuong() {
        $criteria = new CDbCriteria();
        $criteria->select = "MAX(id) as id, t.belong_to_id"; // remember this 
         // remember this http://stackoverflow.com/questions/1313120/retrieving-the-last-record-in-each-group
        $criteria->compare('t.status', GasBussinessContract::STATUS_THUONG_LUONG);
        $criteria->compare('t.uid_login', Yii::app()->user->id);
        $date_limit = date('Y-m-d'); // trừ đi 3 tháng tính từ ngày hiện tại
        $date_limit = MyFormat::modifyDays($date_limit, Yii::app()->params['month_limit_update_thuong_luong'] , '-', 'month');
        $criteria->addCondition("t.created_date >= '$date_limit' ");
//        $criteria->order = 't.date_plan DESC';
//        $criteria->order = 't.created_date DESC';
        $criteria->group = "t.belong_to_id DESC";
        $models = self::model()->findAll($criteria);
        return CHtml::listData($models,'belong_to_id','id');
    }
    
    // BELONG TO BuidArrayLastIdThuongLuong
    public static function InitSessionLastIdThuongLuong(){
        $session=Yii::app()->session;
        $session['ARR_LAST_ID_THUONG_LUONG'] = GasBussinessContract::BuidArrayLastIdThuongLuong();
    }
    
    protected function beforeDelete() {
        GasComment::DeleteByType($this->id, GasComment::TYPE_1_SPANCOP);
        return parent::beforeDelete();
    }
    
    
}