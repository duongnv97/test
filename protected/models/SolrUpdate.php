<?php

/**
 * This is the model class for table "{{_solr_update}}".
 *
 * The followings are the available columns in table '{{_solr_update}}':
 * @property string $id
 * @property integer $action
 * @property integer $need_sync
 * @property string $user_id
 * @property string $created_date
 */
class SolrUpdate extends BaseSpj
{
    const MAX_SEND          = 100; // giới hạn max gửi 1 lần -- SOLR SYNC 50 record done in: 11 Second
    const ACTION_ADD        = 1;
    const ACTION_DELETE     = 2;
    
    const TYPE_USER         = 1;
//    const TYPE___         = 2;
    
    public $aModelUsers = [], $aUid = [], $aId = [];
    
    public function getNameAction() {
        return $this->action == SolrUpdate::ACTION_ADD ? 'ADD' : 'DELETE';
    }
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return SolrUpdate the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_solr_update}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, type, action, need_sync, user_id, created_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'type' => 'User or ...',
            'action' => 'Action',
            'need_sync' => 'Need Sync',
            'user_id' => 'User',
            'created_date' => 'Created Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.action',$this->action);
        $criteria->compare('t.need_sync',$this->need_sync);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /** @Author: ANH DUNG Dec 14, 2017
     *  @Todo: add new record
     *  @action_handle_user:
     * 1. admin/gascustomer/create_customer_store_card
     * 2. admin/gascustomer/update_customer_store_card
     * 3. admin/ajax/iframeCreateCustomerHgd
     * 4. admin/ajax/iframeUpdateCustomerHgd
     * 5. admin/ajaxForUpdate/callUpdateCustomerPhone
     * 6. api/customer/hgdCreate -- CCS
     * 7. api/customer/hgdUpdate -- CCS
     * 8. default/updateCustomerPhone -- WINDOW PMBH
     * 9. api/default/windowCustomerCreate -- WINDOW PMBH
     * 10. api/site/login -- APP GAS24H $mAppSignup->saveNewCustomer();
     * 11. api/user/changeProfile -- APP GAS24H
     * 
     **/
    public function add($action, $user_id) {
        $this->action   = $action;
        $this->user_id  = $user_id;
        $this->save();
    }
    
    /** @Author: ANH DUNG Dec 14, 2017
     *  @Todo: call sync table user
     **/
    public function syncUsers() {
        $model            = new SolrUpdate();
        $model->type      = SolrUpdate::TYPE_USER;
        $model->action    = SolrUpdate::ACTION_ADD;
        $model->cronSync();
        $model            = new SolrUpdate();
        $model->type      = SolrUpdate::TYPE_USER;
        $model->action    = SolrUpdate::ACTION_DELETE;
        $model->cronSync();
    }
    
    /** @Author: ANH DUNG Dec 14, 2017
     *  @Todo: add new record
     * Cần khởi tạo biến $this->type
     * Cần khởi tạo biến $this->action để làm riêng xóa và add
     **/
    public function cronSync() {
        $from = time();
        $models = $this->getDataSync();
        foreach ($models as $item):
            $this->aUid[]   = $item->user_id;
            $this->aId[]    = $item->id;
        endforeach;
        /* 1. get all model theo type: 1: User, 2...
         * 2. run sync
         * 3. update all aId to need_sync=0 để không chạy sync lại nữa
         */
        if(count($this->aId) < 1){
            return ;// no data found
        }
        // 1. run sync add or delete
        $this->doSyncAdd();
        $this->doSyncDelete();
        // 2. update all aId to need_sync=0
        $this->updateAfterSync();
        
        $to = time();
        $second = $to-$from;
        $info = $this->getNameAction().' -- SOLR SYNC '.count($this->aUid).' record done in: '.($second).'  Second  <=> '.($second/60).' Minutes ';
//        Logger::WriteLog($info);
    }
    // get record to sync
    public function getDataSync() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.type='.$this->type);
        $criteria->addCondition('t.action='.$this->action);
        $criteria->addCondition('t.need_sync=1');// 1 is NEED_SYNC
//        $criteria->order = 't.action ASC';
        $criteria->limit = SolrUpdate::MAX_SEND;
        return SolrUpdate::model()->findAll($criteria);
    }
    
    // set flag need_sync = 0 after run
    public function updateAfterSync() {
        if(count($this->aId) < 1){
            return ;
        }
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',', $this->aId);
        $criteria->addCondition("id IN ($sParamsIn)");
        $aUpdate = array('need_sync' => 0);
        SolrUpdate::model()->updateAll($aUpdate, $criteria);
    }
    
    /** @Author: ANH DUNG Dec 14, 2017
     *  @Todo: delete key from solr
     **/
    public function doSyncDelete() {
        $from = time();
        if($this->action != SolrUpdate::ACTION_DELETE){
            return ;
        }
        $mExtSolr = new ExtSolr();
        $mExtSolr->deleteByKey($this->aUid);
        $to = time();
        $second = $to-$from;
        $info = 'SOLR doSyncDelete done in: '.($second).'  Second  <=> '.($second/60).' Minutes';
        Logger::WriteLog($info);
    }
    
    /** @Author: ANH DUNG Dec 14, 2017
     *  @Todo: add or update to solr
     **/
    public function doSyncAdd() {
        $from = time();
        if($this->action != SolrUpdate::ACTION_ADD){
            return ;
        }
        // 1. get all model theo type: 1: User, 2...
        $this->aModelUsers = Users::getArrObjectUserByArrUid($this->aUid, '');
        $mExtSolr       = new ExtSolr();
        $aFieldNumber   = $mExtSolr->getFieldsNumberOnly();
        $aData = [];
        foreach($this->aModelUsers as $mUser){
            $temp = [];
            foreach($mExtSolr->getFields() as $field_name){
                if ($mUser->hasAttribute($field_name)){
                    if(in_array($field_name, $aFieldNumber) && $mUser->$field_name !=''){
                        $temp[$field_name] = $mUser->$field_name*1;
                    }else{
                        $temp[$field_name] = $mUser->$field_name;
                    }
                    $temp[$field_name] = empty($temp[$field_name]) ? 0 : $temp[$field_name];
                }
            }
            $aData[] = $temp;
        }
        $mExtSolr->addMany($aData);
        $to = time();
        $second = $to-$from;
        $info = 'SOLR doSyncAdd done in: '.($second).'  Second  <=> '.($second/60).' Minutes';
//        Logger::WriteLog($info);
    }
    
    /** @Author: ANH DUNG Dec 22, 2017
     *  @Todo: cron clean record Solr Sync
     */
    public function cronClean(){
        $daysKeep = 1;
        $criteria = new CDbCriteria();
        $criteria->addCondition('need_sync=0');// 1 is NEED_SYNC
        $criteria->addCondition("DATE_ADD(created_date,INTERVAL $daysKeep DAY) < CURDATE()");
        SolrUpdate::model()->deleteAll($criteria);        
    }
    
}