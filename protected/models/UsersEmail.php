<?php

/**
 * This is the model class for table "{{_users_email}}".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $status
 * @property integer $user_id
 * @property string $list_email
 * @property string $created_date
 */
class UsersEmail extends BaseSpj
{
    const TYPE_ANNOUNCE_DEBIT   = 1;
    const TYPE_CUSTOMER_DOB     = 2;// Quản lý: Ngày Sinh Nhật Khách Hàng, Ngày Thành Lập
    
    const DOB1_BEFORE_15DAY     = 15;
    const DOB1_BEFORE_30DAY     = 30;

    const MAX_SEND              = 30;
    
    const KEY_CUSTOMER_BIRTHDATE= 1; // Sinh nhật khách hàng
    const KEY_FOUNDING_DATE     = 2; // Ngày thành lập quán
    
    public $autocomplete_name_4, $date_from, $date_to, $dateFromDob2, $dateToDob2;
    public $aEmail = [], $aListEmail = [], $aCustomer = [], $aListNameFile = [];
    public $autocomplete_name1;
    
    public function getArrayStatus(){
        return[
            STATUS_ACTIVE   => 'Hoạt động',
            STATUS_INACTIVE => 'Không hoạt động',
        ];
    }

    public function getStatus(){
        $aStatus = $this->getArrayStatus();
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : "";
    }
    
    public function getArrayType(){
        return[
            UsersEmail::TYPE_ANNOUNCE_DEBIT => 'Đối chiếu công nợ',
            UsersEmail::TYPE_CUSTOMER_DOB   => 'Ngày Sinh Nhật Khách Hàng, Ngày Thành Lập',
        ];
    }
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return UsersEmail the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_users_email}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('name_file, user_id, list_email', 'required', 'on'=>"create, update"),
            array('user_id', 'required', 'on'=>'dobAdd, dobUpdate'),
            array('user_id', 'unique', 'on'=>'dobAdd, dobUpdate','message'=>'Khách hàng này đã tồn tại, vui lòng kiểm tra lại'),
            array('name_file', 'required', 'on'=>'dobAdd, dobUpdate', 'message'=>'SDT không được để trống.'),
            array('dob1, dob2', 'requiredOne', 'on'=>'dobAdd, dobUpdate'),
            array('name_file', 'checkPhone', 'on'=>"dobAdd, dobUpdate"),
            array('list_email','email','on'=>'create_customer_store_card'),
            // Aug2418 tạm để rule unique, sẽ update nếu có thêm type mới setup 
            array('user_id','unique','message'=>'Khách hàng này đã tồn tại setup email, vui lòng kiểm tra lại','on'=>'update'),
            array('name_file, id, type, status, user_id, list_email, date_from, date_to, note, dob1, dob2, dateFromDob2, dateToDob2', 'safe'),
        );
    }
    
    public function validateListEmail()
    {
        if (!empty($this->list_email)) {
            $aEmail = explode(';', $this->list_email);
            foreach($aEmail as $email){
                $mUsersEmail                = new UsersEmail('create_customer_store_card');
                $mUsersEmail->list_email    = $email;
                $mUsersEmail->validate(['list_email']);
                if($mUsersEmail->hasErrors('list_email')){
                    $this->addError('list_email', "Email $email không hợp lệ");
                    return false;
                }
            }
        }
        return true;
    }
    
    public function requiredOne($attribute_name, $params)
    {
        if (empty($this->dob1) && empty($this->dob2)) {
            $this->addError($attribute_name, 'Ít nhất 1 trong 2 phải có giá trị');
            return false;
        }
        return true;
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUser' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'rCreatedBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'            => 'ID',
            'type'          => 'Loại',
            'status'        => 'Trạng thái',
            'user_id'       => 'Khách hàng',
            'list_email'    => 'Danh sách Email',
            'created_date'  => 'Ngày tạo',
            'created_by'    => 'Người tạo',
            'date_from'     => 'Từ ngày',
            'date_to'       => 'Đến ngày',
            'name_file'     => 'Tên file pdf',
            'note'          =>'Ghi chú',
            'dob1'          =>'SN KH',
            'dob2'          =>'Ngày thành lập',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.type',$this->type);
        $criteria->compare('t.status',$this->status);
        $criteria->compare('t.user_id',$this->user_id);
        $date_from = '';
        $date_to = '';
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        }

        if(!empty($date_from) && empty($date_to))
                $criteria->addCondition("t.created_date>='$date_from'");
        if(empty($date_from) && !empty($date_to))
                $criteria->addCondition("t.created_date<='$date_to'");
        if(!empty($date_from) && !empty($date_to))
                $criteria->addBetweenCondition("t.created_date",$date_from,$date_to);

        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /** @Author: NGUYEN KHANH TOAN 2018
     *  @Todo:search list new thêm category_id
     *  @Param:
     **/
    public function searchDobList()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.type',UsersEmail::TYPE_CUSTOMER_DOB);
        $criteria->compare('t.status',$this->status);
        $criteria->compare('t.user_id',$this->user_id);
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        if($cRole == ROLE_SALE || $cRole == ROLE_MONITORING_MARKET_DEVELOPMENT){
            $criteria->compare('t.created_by',$cUid);
        }
        $date_from = '';
        $date_to = '';
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        }
        if(!empty($date_from) && empty($date_to))
                $criteria->addCondition("t.dob1>='$date_from'");
        if(empty($date_from) && !empty($date_to))
                $criteria->addCondition("t.dob1<='$date_to'");
        if(!empty($date_from) && !empty($date_to))
                $criteria->addBetweenCondition("t.dob1",$date_from,$date_to);
        
        $date_from_dob2 = '';
        $date_to_dob2 = '';
        if(!empty($this->dateFromDob2)){
            $date_from_dob2 = MyFormat::dateDmyToYmdForAllIndexSearch($this->dateFromDob2);
        }
        if(!empty($this->dateToDob2)){
            $date_to_dob2 = MyFormat::dateDmyToYmdForAllIndexSearch($this->dateToDob2);
        }

        if(!empty($date_from_dob2) && empty($date_to_dob2))
                $criteria->addCondition("t.dob2>='$date_from_dob2'");
        if(empty($date_from_dob2) && !empty($date_to_dob2))
                $criteria->addCondition("t.dob2<='$date_to_dob2'");
        if(!empty($date_from_dob2) && !empty($date_to_dob2))
                $criteria->addBetweenCondition("t.dob2",$date_from_dob2,$date_to_dob2);
        
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    public function makeUsersEmailBuildSql(&$aRowInsert) {
        $aCustomerDuplidate = [];// check duplicate khi post lên
        foreach($_POST['customer_id'] as $key => $customer_id):
            $email          = isset($_POST['list_email'][$key]) ? trim($_POST['list_email'][$key]) : '';
            $name_file      = isset($_POST['name_file'][$key]) ? trim($_POST['name_file'][$key]) : '';
            if(empty($email) || in_array($customer_id, $aCustomerDuplidate)){
                continue ;
            }
            $mUsersEmail = new UsersEmail();
            $mUsersEmail->name_file = $name_file;
            $mUsersEmail->list_email= $email;
            $mUsersEmail->doCleanNameFile();

            $aCustomerDuplidate[$customer_id] = $customer_id;
            $aRowInsert[]="('$this->type',
                           '$this->status',
                           '$customer_id',
                           '$mUsersEmail->name_file',
                           '$mUsersEmail->list_email',
                           '$this->created_date'
                           )";
        endforeach;
    }
    
    public function makeUsersEmail() {
        // 2. build sql run one query => run
        $sql = '';
        $aRowInsert = [];
        $this->makeUsersEmailBuildSql($aRowInsert);
        // 1. delete old record
        $tableName = UsersEmail::model()->tableName();
        $sql = "insert into $tableName (type,
                    status,
                    user_id,
                    name_file,
                    list_email,
                    created_date
                    ) values ".implode(',', $aRowInsert);

        if(count($aRowInsert)>0)
            Yii::app()->db->createCommand($sql)->execute();
    }
    
   
    
    public function getDataValidate(){
        $this->created_date = date('Y-m-d H:i:s');
    }
    public function getNameUser() {
        $aUser = $this->rUser;
        return isset($aUser->first_name) ? $aUser->first_name : ''; 
    }
    public function getNameFile() {
        return isset($this->name_file) ? $this->name_file : ''; 
    }
    
    public function getType(){
        $aStatus = $this->getArrayType();
        return isset($aStatus[$this->type]) ? $aStatus[$this->type] : "";
    }
    
    public function getCreatedBy(){
        return isset($this->rCreatedBy) ? $this->rCreatedBy->getFullName() : "";
    }
    
    /** @Author: DungNT Aug 23, 2018
     *  @Todo: get info of customer
     **/
    public function getCustomer($field_name='') {
        $mUser = $this->rUser;
        if($mUser){
            if($field_name != ''){
                return $mUser->$field_name;
            }
//            return "<b>".$mUser->code_account."-".$mUser->first_name."</b><br>".$mUser->address."<br><b>Phone: </b>".$mUser->phone;
            return '<b>'.$mUser->code_bussiness.' - '.$mUser->first_name.'</b>';
        }
        return '';
    }
    
    /** @Author: NGUYEN KHANH TOAN 2018 -- old name getArrayListEmail
     *  @fix: Aug2318 DungNT - hàm này chỉ lấy 1 record của 1 type, ko thể có trên 1 record của 1 type
     *  @Todo: lấy danh sách email của user_id and type
     *  @return: array email
     *      Array
                (
                    [0] => dungnt@spj.vn
                    [1] => test2@spj.vn
                )
     **/
    public function getByTypeCompareDebit() {
        $model = $this->getByTypeAndUser($this->user_id, $this->type);
        if(empty($model)){
            return [];
        }
        $this->modelOld = $model;
        return explode(';', $model->list_email);
    }
    
    /** @Author: DungNT Aug 24, 2018
     *  @Todo: get model by type and user
     **/
    public function getByTypeAndUser($user_id, $type) {
        $criteria = new CDbCriteria;
        $criteria->compare('t.user_id', $user_id);
        $criteria->compare('t.type', $type);
        $criteria->addCondition('t.status=' . STATUS_ACTIVE);
        return UsersEmail::model()->find($criteria);
    }
    
    /** @Author: NGUYEN KHANH TOAN 2018
     *  @Todo: Table danh sách email
     *  @Param:
     **/
        public function getTableListEmail(){
        $aResult = explode(';',$this->list_email);
        $str = "<table class='f_size_15' cellpadding='0' cellspacing = '0' style = '' >";
//        $str .= "<tr style = 'color:red' style = 'border : 1px solid black'>";
//        $str .= "<td style = 'border : 1px solid black'>STT</td>";
//        $str .= "<td style = 'border : 1px solid black'>Danh sách Email</td></tr>";
        foreach ($aResult as $key => $value) {
            if($value){
                $str .= "<tr style = ''>";
                    $str .= "<td style = ''>".($key+1)."</td>";
                    $str .= "<td style = ''>".$value."</td>";
                $str .= "</tr>";
            }
        }
        $str .= "</table>";
        return $str;
    }
    
    /** @Author: NGUYEN KHANH TOAN 2018
     *  @Todo: set 
     **/
    public function exportPdf($aData) {
        try{
        $aErrors=[];
        $this->setPdfName($aData);
        $aData['mCustomer']['pdfPathAndName'] = str_replace(Yii::getPathOfAlias('webroot'), '', $this->pdfPathAndName); 
        $this->toPdf($aData);
        
        }catch (Exception $exc){
            throw new Exception($exc->getMessage());
        }
    }
    
    // Aug2318 DungNT 
    public function getPdfName() {
        return $this->name_file.'_'.date('d_m_Y').'_sj'.$this->user_id.'_'.ActiveRecord::randString();
    }
    /** @Author: NGUYEN KHANH TOAN 2018
     *  @Todo: tạo đường dẫn cho file pdf
     *  @Param:
     **/
    public function setPdfName($aData) {
        $this->pdfPathAndName = Yii::getPathOfAlias('webroot') . '/' . $this->getPathSaveFile()."/{$this->getPdfName()}.pdf";
    }
    
    public function getPathSaveFile() {
        $year   = date('Y');
        $month  = date('m');
        $pathUpload = "upload/pdf/UsersEmail/$year/$month";
        $root = Yii::getPathOfAlias('webroot');
        $currentPath = $root . '/' . $pathUpload;
        if (!is_dir($currentPath)){
            $imageProcessing = new ImageProcessing();// check thư mục trước khi tạo
            $imageProcessing->createDirectoryByPath($pathUpload);
            chmod($currentPath, 0777);
        }
        return $pathUpload;
    }
    
    
    /** @Author: NGUYEN KHANH TOAN 2018
     *  @Todo: Gửi email đối chiếu InventoryCustomer
     *  @Param: $aData = $_SESSION['data-excel'];
     **/
    public function sendEmail($aData){
        try {
            /* 1. build nội dung email save vào table GasScheduleEmail
             * 2. build file pdf vào thư mục tạm của server 
             * 3. để cron tự gửi
             * 4. bugToDev
             */ 
        $from = time();
        $aMailDev = $aJson = [];
        $mCustomer                  = $aData['mCustomer'];
        $needMore['title']          = 'Công ty Gas gửi đối chiếu công nợ';
        $needMore['list_mail']      = explode(';', $this->list_email);
        $needMore['list_mail'][]    = 'thucnh@spj.vn';
//        $needMore['list_mail'][]    = 'dungnt@spj.vn';
//        $needMore['time_send']  = 60;
//        CÔNG TY GAS GỬI ĐỐI CHIẾU CÔNG NỢ ANH CHỊ VUI LÒNG CẬP NHẬT GIÚP. MỌI PHẢN HỒI XIN GỬI VỀ MAIL thucnh@spj.vn
        $body  = 'Kính gửi anh/chị,<br><br> <p>Công ty Gas gửi đối chiếu công nợ, anh chị vui lòng cập nhật giúp.</p>'
                . '<br><p><b>Xin vui lòng không trả lời lại email này. Đây là thông báo được tự động gửi đi từ hệ thống của chúng tôi</b></p>'
                . '<p>Mọi phản hồi xin gửi về Nguyễn Hoàng Thúc</p>';
        $body  .= '<p>Email: thucnh@spj.vn</p>';
        $body  .= '<p>Tell: 02862 949 651 line 108</p>';
        $body  .= '<p>Celphone: 0909 487 171</p>';
        
        $aJson['pdf_filename']  = $this->pdfPathAndName;
        $needMore['json_data']  = json_encode($aJson);
        SendEmail::bugToDev($body, $needMore);
        $to = time();
        $second = $to - $from;
        $ResultRun = "CRON send mail: " . count($aIdUser) . ' done in: ' . ($second) . '  Second  <=> ' . round($second / 60, 2) . ' Minutes '. $sLog;
        Logger::WriteLog($ResultRun);
        } catch (Exception $exc) {
            $ResultRun = "Error".$exc->getMessage();
            Logger::WriteLog($ResultRun);
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    
    /** @Author: NGUYEN KHANH TOAN 2018
     *  @Todo: tao pdf InventoryCustomer
     *  @Param:
     **/
        public function getPdfContentQuotes($aData) {
        $resultHtml = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>'.$aData['mCustomer']->first_name.'</title>
            <style type="text/css" media="print">
                .container { margin: 0 auto; width: 860px; }
                .container table { width: 100%; }
                .tb { border-collapse: collapse; margin-top: 20px; }
                .tb th, .tb td { border: #ccc solid 1px; padding: 5px; }
                .border_black th, .border_black td { border: #000 solid 1px; padding: 5px; }
                .tb th { background: #f1f1f1; text-align: center;}
                .tb tfoot td { background: #ddd; font-weight: bolder;}
                .sprint { float: right; padding-right: 160px; }
                .agent_info { padding-top: 5px;}
                .item_c { text-align: center;}
                .item_l { text-align: left; }
                .item_r { text-align: right; }
                .item_c { text-align: center; }
                .item_b { font-weight: bold; }

                .f_size_12 { font-size: 12spx !important;}
                .f_size_14 { font-size: 14px !important;}
                .f_size_15 { font-size: 15px !important;}
                .td_last_r { border-right: none !important;}
                .td_first_t { border-top: none !important;}
                .fix_noborder td { border: none !important;}
                .WhiteSpaceNormal { white-space: normal !important;}
            </style>
            </head>
            <body style="font-family: Arial;">
            <div class="container" id="printElement">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="item_b">'.$aData['mCompany']->first_name.'</td>
                </tr>
                <tr>
                    <td>'.$aData['mCompany']->address.'</td>
                </tr>
            </table>
            <br>

            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="item_c">
                        <h1 class="f_size_14 item_b">
                            Bảng kê hóa đơn: '.$aData['mCustomer']->first_name.'<br>
                        </h1>
                        Từ '.$aData['modelEmail']->date_from.' đến '.$aData['modelEmail']->date_to.'
                    </td>
                </tr>
            </table>';
        
        $resultHtml .= $this->createInfoTable($aData);
            
        return $resultHtml;
    }
    
    /** @Author: NGUYEN KHANH TOAN 2018
     *  @Todo: tạo body pdf Invencustomer
     *  @Param:
     **/
        public function createInfoTable($aData) {
        $mAppCache          = new AppCache();
        $OUTPUT             = isset($aData['OUTPUT']) ? $aData['OUTPUT'] : [];
        $ARR_DAYS           = isset($aData['ARR_DAYS']) ? $aData['ARR_DAYS'] : [];
        $aMaterialsType     = $mAppCache->getMasterModel('GasMaterialsType', AppCache::ARR_MODEL_MATERIAL_TYPE);
        $aSumType           = isset($_SESSION['DataSumRow']['DataMaterialType']) ? $_SESSION['DataSumRow']['DataMaterialType'] : [];
        $sumAll             = isset($_SESSION['DataSumRow']['DataSumAll']) ? $_SESSION['DataSumRow']['DataSumAll'] : 0;
        $mCustomer          = $aData['mCustomer'] ? $aData['mCustomer'] : 0;
        $mInventory = new InventoryCustomer();
        $mInventory->customer_id = $mCustomer->id;
        if(in_array($mCustomer->id, $mInventory->getCustomerPriceB12Kg())){
            $b12ToKg = true; 
        }
        $resultHtml = '<table class="tb hm_table f_size_12">
                <thead>
                    <tr>
                        <th class="item_c">#</th>
                        <th class="item_c">Ngày</th>
                        <th class="item_c">Diễn giải</th>
                        <th class="item_c">Số lượng</th>
                        <th class="item_c">Gas dư</th>
                        <th class="item_c">Cân thiếu đầu vào</th>
                        <th class="item_c">Tổng số lượng</th>
                        <th class="item_c">Giá bán</th>
                        <th class="item_c">Giảm giá</th>
                        <th class="item_c">Tiền bán</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td></td><td></td>
                        <td class="item_b" colspan="7">Tổng cộng</td>
                        <td class="item_r item_b">'.ActiveRecord::formatCurrencyRound($sumAll).'</td>
                    </tr>';
        
        foreach ($OUTPUT as $materials_type_id => $aInfo):
            if(in_array($materials_type_id, $mInventory->getMaterialsTypeHideInReportDebitCashCompare())){
                continue ;
            }
            $i = 1; $sumBlock = $sumQty = $sumQtyFinal = $sumRemain = $sumPayback = 0;
            $materials_type_name = isset($aMaterialsType[$materials_type_id]) ? $aMaterialsType[$materials_type_id]['name'] : '';
            if(isset($aSumType[$materials_type_id])){
                $infoType = $aSumType[$materials_type_id];
                $resultHtml .=  '<tr ><td colspan="3" class="item_b">'.$infoType['name'].'</td>';
                $resultHtml .=  '<td class="item_r item_b">'.ActiveRecord::formatCurrencyRound($infoType['qty']).'</td>';
                $resultHtml .=  '<td class="item_r item_b">'.ActiveRecord::formatCurrency($infoType['remain']).'</td>';
                $resultHtml .=  '<td class="item_r item_b">'.($infoType['payback'] != 0 ? $infoType['payback'] : '').'</td>';
                $resultHtml .=  '<td class="item_r item_b">'.ActiveRecord::formatCurrency($infoType['totalQty']).'</td>';
                $resultHtml .=  '<td>&nbsp;</td> <td>&nbsp;</td>';
                $resultHtml .=  '<td class="item_r item_b">'.ActiveRecord::formatCurrencyRound($infoType['amount']).'</td>';
                $resultHtml .=  '</tr>';
            }
            foreach ($ARR_DAYS as $date):
                if(!isset($aInfo[$date])) continue;
                $aInfo1 = $aInfo[$date];
                $qty    = $qtyFinal = $aInfo1['qty'];
                if($mInventory->isPriceKg($materials_type_id, $aInfo1['price'])):
                    $qty = $aInfo1['qty'] * CmsFormatter::$MATERIAL_VALUE_KG[$materials_type_id];
                    $qtyFinal =  $qty - ($aInfo1['remain_kg'] + $aInfo1['pay_back']);
                    $mInventory->isPriceKgB12($materials_type_id, $b12ToKg);
                    if($b12ToKg && $materials_type_id == GasMaterialsType::MATERIAL_BINH_12KG && $aInfo1['price'] > UsersPrice::PRICE_MAX_BINH_BO){
                        $aInfo1['price'] = $aInfo1['price'] / 12;
                    }
                    if($materials_type_id == GasMaterialsType::MATERIAL_BINH_6KG && $aInfo1['price'] > UsersPrice::PRICE_MAX_BINH_BO){
                        $aInfo1['price'] = $aInfo1['price'] / 6;
                    }// Oct2517
                endif;
                $aInfo1['discount'] = isset($aInfo1['discount']) ? $aInfo1['discount'] : 0;
                $aInfo1['pay_back'] = isset($aInfo1['pay_back']) ? $aInfo1['pay_back'] : 0;
                $grandTotal = ($qtyFinal * $aInfo1['price']) - $aInfo1['discount'];
                
                $index=1;
                $resultHtml .=  '<tr class="item_c"><td>'.$i++.'</td>';
                $resultHtml .=  '<td v>'.MyFormat::dateConverYmdToDmy($date).'</td>';
                $resultHtml .=  '<td >Xuất bán</td>';
                $resultHtml .=  '<td class="item_r">'. ($qty != 0 ? ActiveRecord::formatCurrencyRound($qty) : '').'</td>';
                $resultHtml .=  '<td class="item_r">'.($aInfo1['remain_kg'] != 0 ? ActiveRecord::formatCurrency($aInfo1['remain_kg']) : '').'</td>';
                $resultHtml .=  '<td class="item_r">'.($aInfo1['pay_back'] != 0 ? ActiveRecord::formatCurrency($aInfo1['pay_back']) : '').'</td>';
                $resultHtml .=  '<td class="item_r">'.ActiveRecord::formatCurrency($qtyFinal).'</td>';
                $resultHtml .=  '<td class="item_r">'.ActiveRecord::formatCurrency($aInfo1['price']).'</td>';
                $resultHtml .=  '<td class="item_r">'.($aInfo1['discount'] != 0 ? ActiveRecord::formatCurrencyRound($aInfo1['discount']) : '&nbsp;').'</td>';
                $resultHtml .=  '<td class="item_r">'.ActiveRecord::formatCurrencyRound($grandTotal).'</td>';
                $resultHtml .=  '</tr>';
            endforeach;// end foreach ($ARR_DAYSs
        endforeach;//end foreach ($OUTPUT as $materials_type_id 
        $resultHtml .= '</tbody>';
        $resultHtml .= '</table>';
        $resultHtml .= '<br>
                        <table cellpadding="0" cellspacing="0">
                            <tr >
                                <td class="item_c" style="vertical-align: top;">
                                    <span class="item_b f_size_12">Xác nhận của công ty</span>
                                    <br><i>( Ký, ghi rõ họ tên)</i>
                                </td>
                                <td style="width: 50%;">&nbsp;</td>
                                <td class="item_c" style="vertical-align: top;">
                                    <span class="item_b f_size_12">Xác nhận của khách hàng</span>
                                    <br><i>( Ký, ghi rõ họ tên)</i>
                                </td>
                            </tr>
                        </table>
                        <br><br>';
        return $resultHtml;
    }
    
    public function checkBug(){// KHANH TOAN - ?MONTH - 2019
        $this->checkUser();
        $this->checkEmailCreate();
    }
    
    public function checkEmailCreate(){
        if(!empty($_POST['list_email'])){
            foreach($_POST['customer_id'] as $key => $customer_id):
                $sTrEmail       = isset($_POST['list_email'][$key]) ? MyFormat::removeAllWhiteSpace($_POST['list_email'][$key]) : '';
                $_POST['list_email'][$key] = $sTrEmail;// DungNT Oct0419 set again param $_POST['list_email'] after remove space if error with email
                $sTrNameFile    = isset($_POST['name_file'][$key]) ? $_POST['name_file'][$key] : '';
                $this->checkEmail($sTrEmail);
                if(empty($sTrNameFile)){
                    $this->addError('name_file', "Tên file pdf không được để trống");
                }
            endforeach;
        }
    }
    
    public function checkEmail($sTrEmail) {
        $temp       = explode(';', $sTrEmail);
        foreach($temp as $email){
            $mUsersEmail = new UsersEmail('create_customer_store_card');
            $mUsersEmail->list_email = trim($email);
            $mUsersEmail->validate(['list_email']);
            if($mUsersEmail->hasErrors('list_email')){
                $this->addError('list_email', "Email $email không hợp lệ");
            }
        }
    }
    
    public function checkUser(){
        if(!is_array($_POST['customer_id'])){
            throw new Exception('Dữ liêu không hợp lệ');
        }
        $ok = true;
        foreach($_POST['customer_id'] as $key => $customer_id){
            if(empty($customer_id)){
            $ok = false;
            break;
            }
            $this->checkUserExist($customer_id);
        }
//        if(!$ok){// Aug2318 AD fix, nếu empty customer khi tạo thì cho qua luôn, ko save ko báo lỗi
//            $this->addError('user_id', 'Có lỗi: Có khách hàng chưa được thêm mới');
//        }
    }
    
    public function checkUserExist($customer_id) {
        $criteria = new CDbCriteria;
        $criteria->compare('t.user_id', $customer_id);
        $criteria->compare('t.type', $this->type);
        $mCustomer = UsersEmail::model()->find($criteria);
        $ok = true;
        if(!empty($mCustomer))
        {
            $ok = false;
            $name = $mCustomer->getNameUser();
        }
        if(!$ok){
            $this->addError('user_id', "Có lỗi: Khách hàng $name đã tồn tại");
        }
    }
    
    public function buildArrayUpdate() {
        $aCustomer  = $_POST['customer_id'];
        $aListEMail = $_POST['list_email'];
        $aListNameFile = $_POST['name_file'];

        if(is_array($aCustomer)){
            foreach($aCustomer as $key => $customer_id){
                $mDetail = Users::model()->findByPk($customer_id);
                $this->aCustomer[] = $mDetail;
            }
        }
        if(is_array($aListEMail)){
            if(!isset($_SESSION['aListEMail']) ){
                $_SESSION['aListEMail'] = $aListEMail;
            }
        }
        if(is_array($aListNameFile)){
            if(!isset($_SESSION['aListNameFile']) ){
                $_SESSION['aListNameFile'] = $aListNameFile;
            }
        }
    }
    
    protected function beforeValidate() {
        $this->doCleanNameFile();
        return parent::beforeValidate();
    }
    
    /** @Author: DungNT Aug 24, 2018
     *  @Todo: do clean name file: tieng viet khong dau
     **/
    public function doCleanNameFile() {
        $this->name_file    = MyFunctionCustom::remove_vietnamese_accents($this->name_file);
        $this->name_file    = MyFormat::removeAllWhiteSpace($this->name_file);
        $this->list_email   = MyFormat::removeAllWhiteSpace($this->list_email);
    }
    
    public function getNote() {
        return isset($this->note) ? $this->note : ''; 
    }
    
    public function getDob1() {
        return MyFormat::dateConverYmdToDmy($this->dob1);
    }
    
    public function getDob2() {
        return MyFormat::dateConverYmdToDmy($this->dob2);
    }
    
    public function formatDataForDobAdd() {
        $this->dob1  = MyFormat::dateConverDmyToYmd($this->dob1);
        $this->dob2  = MyFormat::dateConverDmyToYmd($this->dob2);
        if($this->scenario == 'dobAdd')
            $this->created_date = date('Y-m-d H:i:s');
    }
    
    public function checkPhone()
    {
        /* 1. check chiều dài phone
         * 2. check phone phải là di động
         */
//        $phoneNumber = $this->name_file;// 1 + 2
        $aPhoneNumber = explode('-', $this->name_file);
        foreach ($aPhoneNumber as $phone) {
            if(!UsersPhone::isValidCellPhone($phone)){
                $this->addError('name_file', "Số điện thoại $phone không phải là số di động hợp lệ");
            }
        }
    }
    
    public function formatDataBeforeUpdate() {
        $this->dob1  = MyFormat::dateConverYmdToDmy($this->dob1);
        $this->dob2  = MyFormat::dateConverYmdToDmy($this->dob2);
    }
    
    /** @Author: KHANH TOAN Apr 1 2019
     *  @Todo: chay cron thong bao sinh nhat khach hang
     *  @Param:
     **/
    public function cronInformDob() {
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.type=".UsersEmail::TYPE_CUSTOMER_DOB);
        $criteria->addCondition("dob1 IS NOT NULL");
        $model = UsersEmail::model()->findAll($criteria);
        $cDate = new DateTime("now"); 
        foreach ($model as $item) {
            $diff = strtotime($item->dob1) - strtotime("now");
            $years = floor($diff / (365.25*60*60*24));
            $months = floor(($diff - $years * 365.25*60*60*24) / (30.4375*60*60*24));
            if($months == 0){
                $days = floor(($diff - $years * 365.25*60*60*24 - $months*30.4375*60*60*24)/ (60*60*24));
            }else{
                $days = 0;  //truong hop truoc ngay hien tai
            }
//            echo '<pre>';
//            print_r("date diff: ".$years."year ".$months."month ".$days);
//            echo '</pre>';
//            echo '<pre>';
//            print_r($item->attributes);
//            echo '</pre>';
            if($days == UsersEmail::DOB1_BEFORE_15DAY){
                $more['title'] = '[THÔNG BÁO] Sinh nhật khách hàng';
                $this->setupMailInform($item, $more);
            }elseif($days == UsersEmail::DOB1_BEFORE_30DAY){
                $more['title'] = '[THÔNG BÁO] Sinh nhật khách hàng';
                $this->setupMailInform($item, $more);
            }
        }
    }
    
    /** @Author: KHANH TOAN Mar 22 2019
     *  @Todo: gui email thong bao den nhan vien tam ung
     *  @Param: $mSettle
     **/
    public function setupMailInform($mUsersEmail, $more = []) {
        $aEmail = $aUid = [];
//        $aUid[$mUsersEmail->user_id] = $mUsersEmail->user_id;
//        GasIssueTickets::getArrayEmailByArrayUid($aUid, $aEmail);
        $aEmail[] = 'toanrecca@gmail.com';
        $needMore['title']  =  $more['title'];
        $this->sendEmailAlert($mUsersEmail, $aEmail, $needMore);
    }
    
    /** @Author: KHANH TOAN 24 March 2019
     *  @Todo: get info customer and uid post to email
     **/
    public function sendEmailAlert($mUsersEmail, $aEmail, $needMore = []) {
        if(is_null($mUsersEmail)){
            return ;
        }
        $mCustomer  = $this->rUser;
        $first_name = $address = '';
        if(!empty($mCustomer)){
            $first_name = $mCustomer->first_name;
            $address    = $mCustomer->address;
        }
        $titleHead  = "[THONG BAO] Khách hàng {$first_name} sắp tới có sinh nhật<br><br>";
        $body = $titleHead;
        $body .= "<br><b>Tên khách hàng: </b> {$first_name}";
        $body .= "<br><b>Địa chỉ khách hàng: </b> {$address}";
        $body .= "<br><b>Số điện thoại: </b> {$mUsersEmail->name_file}";
        if(!empty($mUsersEmail->dob1)){
            $dob = MyFormat::dateConverYmdToDmy($mUsersEmail->dob1);
            $body .= "<br><b>Sinh nhật khách hàng: </b> {$dob}";
        }
        $needMore['title']          = isset($needMore['title']) ? $needMore['title'] : 'Sinh nhật khách hàng.';
        $needMore['list_mail']      = $aEmail;
        $needMore['SendNow']    = 1;// only dev debug
        SendEmail::bugToDev($body, $needMore);
    }
    
    /** @Author: DuongNV 4 Apr 2019
     *  @Todo: get array sinh nhật KH, sinh nhật quán trc 30, 15 ngày
     **/
    public function getListBirthdate() {
        $now            = date('Y-m-d');
        $before15Days   = MyFormat::modifyDays($now, self::DOB1_BEFORE_15DAY, '-', 'days');
        $before30Days   = MyFormat::modifyDays($now, self::DOB1_BEFORE_30DAY, '-', 'days');
        $criteria       = new CDbCriteria;
        $criteria->addCondition("t.dob1 = '$before15Days' || t.dob1 = '$before30Days' || t.dob2 = '$before15Days' || t.dob2 = '$before30Days'");
        $criteria->compare('t.type', self::TYPE_CUSTOMER_DOB);
        $models         = UsersEmail::model()->findAll($criteria);
        $ret            = [];
        foreach ($models as $value) {
            if($value->dob1 == $before15Days){
                $ret[self::DOB1_BEFORE_15DAY][self::KEY_CUSTOMER_BIRTHDATE][] = $value;
            }
            if($value->dob1 == $before30Days){
                $ret[self::DOB1_BEFORE_30DAY][self::KEY_CUSTOMER_BIRTHDATE][] = $value;
            }
            if($value->dob2 == $before15Days){
                $ret[self::DOB1_BEFORE_15DAY][self::KEY_FOUNDING_DATE][] = $value;
            }
            if($value->dob2 == $before30Days){
                $ret[self::DOB1_BEFORE_30DAY][self::KEY_FOUNDING_DATE][] = $value;
            }
        }
        return $ret;
    }
    
    /** @Author: DuongNV 4 Apr 2019
     *  @Todo: cron notify sinh nhật KH, sinh nhật quán trc 30, 15 ngày
     **/
    public function cronNotifyBirthday() {
        $aBirthdate = $this->getListBirthdate();
        foreach ($aBirthdate as $numDate => $value) {
            foreach ($value as $type => $m) {
                $this->sendEmailNotifyBirthdate($m, $numDate, $type);
            }
        }
        
    }
    
    /** @Author: @Author: DuongNV 4 Apr 2019
     *  @Todo: get info customer and uid post to email
     **/
    public function sendEmailNotifyBirthdate($mUsersEmail, $numDate, $type) {
        if(is_null($mUsersEmail) || empty($mUsersEmail->rUser->email)){
            return ;
        }
        $mCustomer  = $mUsersEmail->rUser;
        $first_name = $address = $title = $body = '';
        if(!empty($mCustomer)){
            $first_name = $mCustomer->getFullName();
            $address    = $mCustomer->address;
        }
        if($type == self::KEY_CUSTOMER_BIRTHDATE){
            $dob   = MyFormat::dateConverYmdToDmy($mUsersEmail->dob1);
            $body  = "Còn $numDate ngày nữa là tới sinh nhật khách hàng {$first_name}.<br><br>"; 
            $body .= "<br><b>Sinh nhật khách hàng: </b> {$dob}";
        } else {
            $fd    = MyFormat::dateConverYmdToDmy($mUsersEmail->dob2);
            $body  = "Còn $numDate ngày nữa là tới ngày thành lập {$first_name}.<br><br>";
            $body .= "<br><b>Ngày thành lập: </b> {$fd}";
        }
        $body .= "<br><b>Tên khách hàng: </b> {$first_name}";
        $body .= "<br><b>Địa chỉ khách hàng: </b> {$address}";
        $body .= "<br><b>Số điện thoại: </b> {$mUsersEmail->name_file}";
        $needMore['title']          = '[THÔNG BÁO] Sắp tới sinh nhật khách hàng hoặc ngày thành lập quán';
//        $needMore['list_mail']      = [$mUsersEmail->rUser->email];
//        $needMore['list_mail']      = explode(';', $mUsersEmail->list_email);
        $needMore['list_mail']      = ['nvduong1211@gmail.com'];
//        $needMore['SendNow']        = 1;// only dev debug
        
        SendEmail::bugToDev($body, $needMore);
    }
    
    public function canUpdateDob(){
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole == ROLE_ADMIN){
            return true;
        }
        return $this->created_by == MyFormat::getCurrentUid();
    }
    
    public function setCreatedBy(){
        $this->created_by = MyFormat::getCurrentUid();
    }
    
}