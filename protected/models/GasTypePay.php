<?php

/**
 * This is the model class for table "{{_gas_type_pay}}".
 *
 * The followings are the available columns in table '{{_gas_type_pay}}':
 * @property integer $id
 * @property integer $type
 * @property string $pay_day
 * @property string $json_type_3
 */
class GasTypePay extends CActiveRecord
{
    const L1_1_DAY      = 12;// thanh toán tiền liền trong 1 ngày
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasTypePay the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_type_pay}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name,type', 'required'),
            // Please remove those attributes that should not be searched.
            array('id,name, type, pay_day, json_type_3', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'TypeMaster' => array(self::BELONGS_TO, 'GasTypeMaster', 'type'),
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'type' => 'Tên loại thanh toán',
            'type' => 'Thuộc loại',
            'pay_day' => 'Quy định ngày thanh toán',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('t.id',$this->id);
        $criteria->compare('t.name',$this->name);
        $criteria->order = 'type desc, id asc';

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
        ));
    }

    public static function getArrAll()
    {
        $criteria = new CDbCriteria;
        $criteria->order = 'type desc';
        $models = self::model()->findAll($criteria);
        return  CHtml::listData($models,'id','name');            
    }     

    public static function getArrAllObject()
    {
        $criteria = new CDbCriteria;
        $criteria->order = 'type desc';
        $models = self::model()->findAll($criteria);
        $res=array();
        foreach ($models as $item)
            $res[$item->id] = $item;
        return  $res;
    }

    protected function beforeSave() {

        if($this->type==PAY_TYPE_3 && trim($this->pay_day)!=''){
            $tempStr = trim($this->pay_day,',');
            $tempStr = explode(',', $tempStr);
            $json=array();
            foreach ($tempStr as $item){
                // $item is: 1-15-25-0
                $a = explode('-', $item);
                if(count($a)==4){
                    $aDay=array();
                    for($i=$a[0];$i<=$a[1];$i++)
                        $aDay[]=$i;
                    $json[JSON_DAYS][] = $aDay;
                    $json[JSON_DAY_PAY][] = $a[2];
                    $json[JSON_DAY_PAY_IN_MONTH][] = $a[3];

                }
            }
            $this->json_type_3 = json_encode($json);
        }
        return parent::beforeSave();
    }
}