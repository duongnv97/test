<?php

/**
 * This is the model class for table "{{_gas_text_file}}".
 *
 * The followings are the available columns in table '{{_gas_text_file}}':
 * @property string $id
 * @property string $text_id
 * @property string $file_name
 * @property integer $order_number
 * @property string $created_date
 */
class GasTextFile extends CActiveRecord
{
    public static $AllowFile = 'jpg,jpeg,png';
    public static $aSize = array(
        'size1' => array('width' => 128, 'height' => 96), // small size, dùng ở form update văn bản
        'size2' => array('width' => 1536, 'height' => 1200), // size view, dùng ở xem văn bản
    );
    
    public $aIdNotIn;
    
    public static $pathUpload = 'upload/text_scan';

    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_text_file}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('aIdNotIn,id, text_id, file_name, order_number, created_date', 'safe'),
            array('file_name', 'file','on'=>'UploadFileScan',
                'allowEmpty'=>true,
                'types'=> self::$AllowFile,
                'wrongType'=> "Chỉ cho phép định dạng file ".self::$AllowFile." .",
            ), 
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
            return array(
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'text_id' => 'Text',
            'file_name' => 'File Name',
            'order_number' => 'Order Number',
            'created_date' => 'Created Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
            $criteria=new CDbCriteria;
            $criteria->compare('t.id',$this->id,true);
            $criteria->compare('t.text_id',$this->text_id,true);
            $criteria->compare('t.file_name',$this->file_name,true);
            $criteria->compare('t.order_number',$this->order_number);
            $criteria->compare('t.created_date',$this->created_date,true);

            return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
            ));
    }
    
    /**
     * @Author: ANH DUNG Jun 14, 2014 
     * @Todo:validate file submit
     * @Param: $model model GasSalesFileScan
     */
    public static function validateFile($model, $needMore=array()){
        $model->aModelDetail = array();
        $ok=false;
        if(isset($_POST['GasTextFile']['file_name'])  && count($_POST['GasTextFile']['file_name'])){
            foreach($_POST['GasTextFile']['file_name'] as $key=>$item){
                $mFile = new GasTextFile('UploadFileScan');
                $mFile->file_name  = CUploadedFile::getInstance($model->mDetail,'file_name['.$key.']');
                if(!is_null($mFile->file_name)){                    
                    $ok=true;
                    MyFormat::IsImageFile($_FILES['GasTextFile']['tmp_name']['file_name'][$key]);
                }
                $mFile->validate();
                if($mFile->hasErrors()){
                    $model->addError('mDetail', "File Error");
                }
                if(!is_null($mFile->file_name))
                    $model->aModelDetail[] = $mFile;
            }
        }
        if(!$ok && isset($needMore['create'])){
            $model->addError('mDetail', "Bạn Chưa Chọn File Scan Upload ");
        }
    }
    
    /**
     * @Author: ANH DUNG May 18, 2014 
     * @Todo: May 18, 2014 - saveFileScanDetail
     * @Param: $model model GasFileScan
     */
    public static function saveFileScanDetail($model){
        try {
            $mTextFileSave = GasText::model()->findByPk($model->id); // để lấy created_date, không thể find rồi đưa vào biến $model dc vì còn liên quan đến get file
            set_time_limit(7200);
            $model->aModelDetail = array();
            if(isset($_POST['GasTextFile']['file_name'])  && count($_POST['GasTextFile']['file_name'])){
                foreach($_POST['GasTextFile']['file_name'] as $key=>$item){
                    $mFile = new GasTextFile('UploadFileScan');
                    $mFile->text_id = $model->id;
                    $mFile->created_date = $mTextFileSave->created_date;
                    $mFile->order_number = isset($_POST['GasTextFile']['order_number'][$key])?$_POST['GasTextFile']['order_number'][$key]:1;
                    $mFile->file_name  = CUploadedFile::getInstance($model->mDetail,'file_name['.$key.']');
                    if(!is_null($mFile->file_name)){
                        $mFile->file_name  = self::saveFile($model, $mFile, 'file_name', $key);
                        $mFile->save();
                        self::resizeImage($model, $mFile, 'file_name');
                    }
                }
            }
        } catch (Exception $exc) {
            throw new Exception("Có Lỗi Khi Upload File: ". $exc->getMessage());
        }
    }
    
    /**
     * May 18, 2014 - ANH DUNG
     * To do: save file 
     * @param: $model GasTextFile
     * @param: $count 1,2,3
     * @return: name of image upload/transactions/property_document
     */
    public static function  saveFile($mGasFileScan, $model, $fieldName, $count)
    {        
        if(is_null($model->$fieldName)) return '';
        $year = MyFormat::GetYearByDate($mGasFileScan->created_date);
        $pathUpload = GasTextFile::$pathUpload."/$year";
        $ext = $model->$fieldName->getExtensionName();
        $fileName = date('Y-m-d').time();
        $fileName = $fileName."-".ActiveRecord::randString().$count.'.'.$ext;
        $imageProcessing = new ImageProcessing();
        $imageProcessing->createDirectoryByPath($pathUpload);
        $model->$fieldName->saveAs($pathUpload.'/'.$fileName);
        return $fileName;
    }
    
    /**
     * @Author: ANH DUNG May 18, 2014
     * To do: resize file scan
     * @param: $mGasFileScan model GasFileScan
     * @param: $model model GasTextFile
     * @param: $fieldName 
     */
    public static function resizeImage($mGasFileScan, $model, $fieldName) {
        $year = MyFormat::GetYearByDate($mGasFileScan->created_date);
        $pathUpload = GasTextFile::$pathUpload."/$year";
        $ImageHelper = new ImageHelper();     
        $ImageHelper->folder = '/'.$pathUpload;
        $ImageHelper->file = $model->$fieldName;
        $ImageHelper->aRGB = array(0, 0, 0);//full black background
        $ImageHelper->thumbs = self::$aSize;
//        $ImageHelper->createFullImage = true;
        $ImageHelper->createThumbs();
        $ImageHelper->deleteFile($ImageHelper->folder . '/' . $model->$fieldName);        
    }
    
    /*
     * @Author: ANH DUNG May 18, 2014
     * To do: delete file scan
     * @param: $model model user
     * @param: $fieldName is avatar, agent_company_logo
     * @param: $aSize
     */
    public static function RemoveFileOnly($pk, $fieldName) {
        $modelRemove = self::model()->findByPk($pk);
        if (is_null($modelRemove) || empty($modelRemove->$fieldName))
            return;
        $year = MyFormat::GetYearByDate($modelRemove->created_date);
        $pathUpload = GasTextFile::$pathUpload."/$year";
        $ImageHelper = new ImageHelper();     
        $ImageHelper->folder = '/'.$pathUpload;
        $ImageHelper->deleteFile($ImageHelper->folder . '/' . $modelRemove->$fieldName);
        foreach ( self::$aSize as $key => $value) {
            $ImageHelper->deleteFile($ImageHelper->folder . '/' . $key . '/' . $modelRemove->$fieldName);
        }
    }     
    
    protected function beforeDelete() {
        self::RemoveFileOnly($this->id, 'file_name');
        return parent::beforeDelete();
    }
    
    public static function deleteByTextId($text_id){
        $criteria = new CDbCriteria();
        $criteria->compare("t.text_id", $text_id);
        $models = self::model()->findAll($criteria);
        Users::deleteArrModel($models);        
    }
    
    /**
     * @Author: ANH DUNG May 18, 2014
     * @Todo: delete record by file_scan_id and array id not in
     * @Param: $file_scan_id 
     * @Param: $aIdNotIn 
     */
    public static function deleteByNotInId($text_id, $aIdNotIn){
        $criteria = new CDbCriteria();
        $criteria->compare("t.text_id", $text_id);
        $criteria->addNotInCondition("t.id", $aIdNotIn);
        $models = self::model()->findAll($criteria);
        Users::deleteArrModel($models);
    } 
        
    /**
     * @Author: ANH DUNG Aug 31, 2014
     * @Todo: update order number of file
     * @param: $model model GasText
     */
    public static function UpdateOrderNumberFile($model){
        $sql='';
        $tableName = self::model()->tableName();
        if(isset($_POST['GasTextFile']['aIdNotIn'])  && count($_POST['GasTextFile']['aIdNotIn'])){
            foreach($_POST['GasTextFile']['aIdNotIn'] as $key=>$pk){
                $order_number = isset($_POST['GasTextFile']['order_number'][$key])?$_POST['GasTextFile']['order_number'][$key]:1;
                if($pk){
                    $sql .= "UPDATE $tableName SET "
                        . " `order_number`=\"$order_number\" "
                        . "WHERE `id`=$pk ;";
                }
            }
        }
        if(trim($sql)!='')
            Yii::app()->db->createCommand($sql)->execute();
    }

}