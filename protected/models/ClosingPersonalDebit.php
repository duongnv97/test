<?php
/**
 * This is the model class for table "{{_closing_personal_debit}}".
 *
 * The followings are the available columns in table '{{_closing_personal_debit}}':
 * @property integer $id
 * @property integer $sale_id
 * @property integer $status_send_mail
 * @property integer $status_debit
 * @property string $created_date
 * @property integer $status_process
 * @property string $agent_id
 */
class ClosingPersonalDebit extends BaseSpj
{
    const DEBUG             = false;
    const IS_DEBIT          = false;// Thực hiện trừ tiền và bỏ công nợ cá nhân của nhân viên khi gửi đến lần 3
    const IS_INSERT         = true;// Thực hiện trừ tiền và bỏ công nợ cá nhân của nhân viên khi gửi đến lần 3
    const STATUS_NO_DEBIT   = 0; // Trạng thái chưa thực hiện thêm vào công nợ cá nhân
    const STATUS_DEBIT      = 1; // Đã thêm vào công nợ cá nhân của khách hàng
    const SEND_MAIL_0       = 0; // Gửi mail cảnh báo lần 1
    const SEND_MAIL_10      = 1; // gửi mail cảnh báo 10 ngày
    const SEND_MAIL_20      = 2; // gửi mail cảnh báo 20 ngày
    const STATUS_PROCESSED  = 1;//da xu ly khach hang no vo
    const STATUS_NO_PROCESS = 0;//chua xu ly khach hang no vo
    const SEND_MAIL_10_DAYS = 15; //so ngay gui mail
    const SEND_MAIL_20_DAYS = 30; //so ngay gui mail
    
    public $onlySearchCustomer = false;
    public $current_date,$autocomplete_name,$autocomplete_name_1,$autocomplete_name_2;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ClosingPersonalDebit the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_closing_personal_debit}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            array('created_date_bigint,agent_id,customer_id,type_customer,created_date', 'safe'),
            ['json_vo_process,json_vo, id, sale_id, status_send_mail, status_debit, created_date, customer_id, created_date_bigint', 'safe'],
            ['status_process', 'safe']
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'rSale' => array(self::BELONGS_TO, 'Users', 'sale_id'),
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sale_id' => 'Nhân viên',
            'customer_id' => 'Khách hàng',
            'status_send_mail' => 'Trạng thái gửi mail',
            'status_debit' => 'Trạng thái trừ công nợ cá nhân',
            'status_process' => 'Trạng thái xử lý',
            'created_date' => 'Ngày tạo',
            'agent_id' => 'Đại lý',
            'type_customer' => 'Loại Khách Hàng',
        ];
    }
    
     /** @Author: NamNH Jul 30,2019
     *  @Todo: calc limit 12
     **/
    public function calcLimitVo12($kg12) {
        $hanMuc12 = (10 * $kg12) / (12*30);
        return round($hanMuc12);
    }
    
    /** @Author: NamNH Jul 30,2019
     *  @Todo: calc limit 45
     **/
    public function calcLimitVo45($kg45) {
        $hanMuc45 = (15 * $kg45) / (45*30);
        return round($hanMuc45);
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

	$criteria->compare('t.id',$this->id);
	$criteria->compare('t.sale_id',$this->sale_id);
//	$criteria->compare('t.status_send_mail',$this->status_send_mail);
//	$criteria->compare('t.status_debit',$this->status_debit);
//	$criteria->compare('t.created_date',$this->created_date,true);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /** @Author: LocNV Jul , 2019
     *  @Todo: khoi tao inventoryCustomer
     *  @Param:
     **/
    public function initDebit($date_from, $date_to, &$mInventoryCustomer) {
        $mInventoryCustomer->date_from    = $date_from;
        $mInventoryCustomer->date_to      = $date_to;
//        if(count($mInventoryCustomer->aCustomerIdLimitVo)){// DungNT Fix Jan1619
//            $aCustomerId = array_merge($aCustomerId, $mInventoryCustomer->aCustomerIdLimitVo);
//        }
        $mInventoryCustomer->province_id        = '';
        $mInventoryCustomer->price_code         = '';
        $mInventoryCustomer->aCustomerIdLimitVo = [];
        if(!empty($this->customer_id)){
            $mInventoryCustomer->customer_id        = $this->customer_id;
        }else{// Aug 07,2019 NamNH limit only users active
            $criteria = new CDbCriteria();
            $criteria->compare('t.role_id', ROLE_CUSTOMER);
            $criteria->addInCondition('t.is_maintain', CmsFormatter::$ARR_TYPE_CUSTOMER_STORECARD);
            $criteria->compare('t.channel_id', Users::CON_LAY_HANG);
            $mUsers = Users::model()->findAll($criteria);
            $aUsersID = CHtml::listData($mUsers, 'id', 'id');
            $mInventoryCustomer->customer_id = $aUsersID;
        }
        $mInventoryCustomer->agent_id           = $this->agent_id;
        $mInventoryCustomer->type_customer      = $this->type_customer;
        $mInventoryCustomer->sale_id            = $this->sale_id;
    }
    
    /** @Author: LocNV Jul , 2019
     *  @Todo: Quét tất cả các khách hàng nợ vỏ từ ngày hiện tại trở về trước để chạy cron cảnh báo
     *  @Param:
     **/
    public function checkDebitVoAll(&$aCustomer) {        
        $date_from = $this->current_date;
        $date_to = $this->current_date;
        $mInventoryCustomer = new InventoryCustomer();
        $this->initDebit($date_from, $date_to, $mInventoryCustomer);
        $aData = $mInventoryCustomer->reportDebitVo();
        
        //tinh cong no vo
        $aCustomer          = isset($aData['CUSTOMER_MODEL']) ? $aData['CUSTOMER_MODEL'] : [];
        $aMaterials         = isset($aData['MATERIALS_MODEL']) ? $aData['MATERIALS_MODEL'] : [];
        $BEGIN_VO           = isset($aData['BEGIN_VO']) ? $aData['BEGIN_VO'] : [];
        $OUTPUT_BEFORE      = isset($aData['OUTPUT_BEFORE']) ? $aData['OUTPUT_BEFORE'] : [];
        $OUTPUT_IN_PERIOD   = isset($aData['OUTPUT_IN_PERIOD']) ? $aData['OUTPUT_IN_PERIOD'] : [];
        $aIdVo              = isset($aData['ID_VO']) ? $aData['ID_VO'] : [];
        $aCongNoVo          = [];


        foreach ($aCustomer as $mCustomer){
            $customer_id = $mCustomer->id;

            $agentName   = isset($listdataAgent[$mCustomer->area_code_id]) ? $listdataAgent[$mCustomer->area_code_id] : '';

            $sum_begin = $sum_import = $sum_export = $sum_end = 0;
            $sum_begin_nho = $sum_import_nho = $sum_export_nho = $sum_end_nho = 0;
            $sum_begin_lon = $sum_import_lon = $sum_export_lon = $sum_end_lon = 0;

                //for tinh cong no vo nho
            foreach ($aIdVo[$customer_id]['MATERIALS_VO_NHO'] as $materials_id => $qty){
                $materials_no = $name  = $materials_id;
                if(isset($aMaterials[$materials_id])){
                    $materials_no = $aMaterials[$materials_id]['materials_no'];
                    $name = $aMaterials[$materials_id]['name'];
                }
                $qty_begin          = isset($BEGIN_VO[$customer_id][$materials_id]) ? $BEGIN_VO[$customer_id][$materials_id] : 0;
                $qty_import_before  = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
                $qty_export_before  = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;

                $qty_import_in_period = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
                $qty_export_in_period = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;

                $begin      = $qty_begin + $qty_export_before - $qty_import_before;
                $end        = $begin + $qty_export_in_period - $qty_import_in_period;
                $sum_begin      += $begin;
                $sum_import     += $qty_import_in_period;
                $sum_export     += $qty_export_in_period;
                $sum_end        += $end;
    //                tính toán vỏ nhỏ
                $sum_begin_nho  += $begin;
                $sum_import_nho += $qty_import_in_period;
                $sum_export_nho += $qty_export_in_period;
                $sum_end_nho    += $end;

                if($begin == 0 && $qty_import_in_period == 0 && $qty_export_in_period == 0 && $end == 0){
                    continue;
                }

            }
                //for tinh cong no vo lon
            foreach ($aIdVo[$customer_id]['MATERIALS_VO_LON'] as $materials_id => $qty){
                $materials_no = $name  = $materials_id;
                if(isset($aMaterials[$materials_id])){
                    $materials_no = $aMaterials[$materials_id]['materials_no'];
                    $name = $aMaterials[$materials_id]['name'];
                }
                $qty_begin          = isset($BEGIN_VO[$customer_id][$materials_id]) ? $BEGIN_VO[$customer_id][$materials_id] : 0;
                $qty_import_before  = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
                $qty_export_before  = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;

                $qty_import_in_period = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
                $qty_export_in_period = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;

                $begin      = $qty_begin + $qty_export_before - $qty_import_before;
                $end        = $begin + $qty_export_in_period - $qty_import_in_period;
                $sum_begin      += $begin;
                $sum_import     += $qty_import_in_period;
                $sum_export     += $qty_export_in_period;
                $sum_end        += $end;
    //                Tính toán vỏ lớn
                $sum_begin_lon  += $begin;
                $sum_import_lon += $qty_import_in_period;
                $sum_export_lon += $qty_export_in_period;
                $sum_end_lon    += $end;

                if($begin == 0 && $qty_import_in_period == 0 && $qty_export_in_period == 0 && $end == 0){
                    continue;
                }

            }
            $aCongNoVo[$customer_id]['vo_nho'] = $sum_end_nho;
            $aCongNoVo[$customer_id]['vo_lon'] = $sum_end_lon;
            $aCongNoVo[$customer_id]['tong'] = $sum_end_lon + $sum_end_nho;
        }
        foreach ($aCustomer as $customer_id => $modelCustomer) {
            $aCongNoVo[$customer_id]['sale_id'] = Users::GetSaleIdOfCustomer($modelCustomer);
            $aCongNoVo[$customer_id]['agent_id'] = Users::GetAgentIdOfCustomer($customer_id);
        }

        return $aCongNoVo;
    }
    
    public function roleOnlySearchCustomer(){
        return [
            ROLE_MONITORING_MARKET_DEVELOPMENT,
            ROLE_SALE,
        ];
    }
    /** @Author: LocNV Jul , 2019
     *  @Todo: khoi tao gas store card
     *  @Param:
     **/
    public function initGasStoreCard(&$model, $date_from, $date_to, $aCustomerId = []) {
        $model->date_from   = $date_from;
        $model->date_to     = $date_to;
        $model->agent_id    = $this->agent_id;
        $model->customer_id = $aCustomerId;
    }

    /** @Author: LocNV Jul , 2019
     *  @Todo: hàm tính sản lượng của khách hàng trong khoảng ngày hoặc trong tháng
     *          trả về danh sách sản lượng của từng khách hàng
     *  @Param:
     **/
    public function calcOutputDaily($aCustomerId = [])
    {
        $date_from = $this->date_from;
        $date_to = $this->date_to;
        $model = new GasStoreCard();
        $this->initGasStoreCard($model, $date_from, $date_to,$aCustomerId);
        
        try{
        /* 1. findAll customer id của Loại KH bò hoặc mối
         * 2. nếu có sale thì findAll luôn customer id của sale
         * 3. trong truy vấn sẽ ưu tiên cho ngày hơn là chọn tháng và năm
         * 4. lấy id các loại bình từ loại vật tư
         */
        $aRes = array();
        $aRes['customer_id']    = array();
        $aRes['store_card_id']  = array();
        $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
        $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);

        // lấy gas dư
        $this->getGasRemain($model, $aRes, $date_from, $date_to);
        
        $data = [];
        // 4. lấy id các loại bình từ loại vật tư
        foreach( CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT as $material_type_id) {
            // $material_type_id là loại vật tư MATERIAL_TYPE_BINHBO_50,// Gas Bình Bò 50 Kg ....
            // get array id những vật tư có loại là $material_type_id: Gas 12 Kg OR 45kg OR 50
            $aMaterialId = GasMaterials::getArrayIdByMaterialTypeId($material_type_id);     
            
            $criteria = new CDBcriteria();
            
            // kiểm tra xem là nhập vỏ hay xuất bán
            $GET_ID_STORECARD = 0; // biến này để xuống dưới kiểm tra xem có lấy id store card không?
            $type_in_out = array(STORE_CARD_TYPE_5); // nhập vỏ
            
            if(in_array($material_type_id, CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT)) {
                $GET_ID_STORECARD = 1;
                $type_in_out = CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC; // xuất bán
            }
            
            $sParamsIn = implode(',', $type_in_out); // nhập vỏ
            $criteria->addCondition("t.type_in_out IN ($sParamsIn)");
            
            // kiểm tra xem là nhập vỏ hay xuất bán
            if(count($aMaterialId)){
                $sParamsIn = implode(',', $aMaterialId);
                $criteria->addCondition("t.materials_id IN ($sParamsIn)");
            }
            if(!empty($model->agent_id)){
                $criteria->compare('t.user_id_create', $model->agent_id);
            }
            
            if (!empty($model->customer_id)) {
                if(is_array($model->customer_id)){
                    $aCustomerIdIn = implode(',', $model->customer_id);
                    $criteria->addCondition("t.customer_id IN ($aCustomerIdIn)");
                }else{
                    $criteria->addCondition("t.customer_id = ".$model->customer_id );
                }
                
            }
            
            // 3. trong truy vấn sẽ ưu tiên cho ngày hơn là chọn tháng và năm            
            DateHelper::searchBetween($date_from, $date_to, 'date_delivery_bigint', $criteria);
            
            $criteria->select = "t.user_id_create,t.date_delivery,t.customer_id,"
                    . "t.store_card_id, sum(qty) as qty";
            
//            $criteria->group = "t.date_delivery, t.customer_id, t.user_id_create"; 
            $criteria->group = "t.date_delivery, t.customer_id"; 
            $criteria->order = "t.date_delivery ASC";
            $mRes = GasStoreCardDetail::model()->findAll($criteria);
            
            if(count($mRes)){
                foreach($mRes as $item) {
                    //gas du chi tinh cho binh 45
                    $gasRemain = isset($aRes['GasRemain'][$item->customer_id]) ? $aRes['GasRemain'][$item->customer_id] : 0;
                    if (isset($data[$item->customer_id][$material_type_id]))
                    {
                        $data[$item->customer_id][$material_type_id] += $item->qty * CmsFormatter::$MATERIAL_VALUE_KG[$material_type_id];
                    } else {
                        if ($material_type_id == MATERIAL_TYPE_BINHBO_45) {
                            $data[$item->customer_id][$material_type_id] = $item->qty * CmsFormatter::$MATERIAL_VALUE_KG[$material_type_id] - $gasRemain;
                        } else {
                            $data[$item->customer_id][$material_type_id] = $item->qty * CmsFormatter::$MATERIAL_VALUE_KG[$material_type_id];
                        }
                            
                    }
                }
            }
                
        } // end oreach( CmsFormatter::$MATERIAL_TYPE_BINHBO_BINH12_45
        $aRes['data'] = $data;
                
//        foreach ($aRes['data'] as $customer_id => $aMaterialTypeId) {
//            
//            $sanLuong45 = isset($aMaterialTypeId[MATERIAL_TYPE_BINHBO_45]) ? $aMaterialTypeId[MATERIAL_TYPE_BINHBO_45] : 0;
//            
//            $sanLuong12 = isset($aMaterialTypeId[MATERIAL_TYPE_BINH_12]) ? $aMaterialTypeId[MATERIAL_TYPE_BINH_12] : 0;
//            
//        }

        return $aRes['data'];
        
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }  
    }
    
    /**
     * @Author: ANH DUNG Feb 28, 2014
     * @Todo: lấy gas dư của KH trong ngày đó hoặc tháng
     * @param: $model
     * @Return: $aRes: is final array return
     */      
    public function getGasRemain($model, &$aRes, $date_from, $date_to)
    {
        $criteria = new CDBcriteria();
        $criteria->addCondition("t.only_gas_remain=0");
        if(count($aRes['customer_id'])){
            $sParamsIn = implode(',', $aRes['customer_id']);
            $criteria->addCondition("t.customer_id IN ($sParamsIn)");
        }
        if(!empty($model->agent_id)){
            $criteria->compare('t.agent_id', $model->agent_id);
        }

        // 3. trong truy vấn sẽ ưu tiên cho ngày hơn là chọn tháng và năm
//        $criteria->addBetweenCondition("t.date_input",$date_from,$date_to);
        DateHelper::searchBetween($date_from, $date_to, 'date_input_bigint', $criteria, false);     

        $criteria->select = "t.date_input,t.customer_id,t.agent_id, t.materials_type_id,"
                . "sum(amount_gas) as amount_gas, sum(amount) as amount";
        $criteria->group = "t.date_input, t.customer_id, t.agent_id"; 
        $criteria->order = "t.date_input ASC";
        
        $mRes = GasRemain::model()->findAll($criteria);
        $data = [];
        if(count($mRes)){
            foreach($mRes as $item){
                if (isset($data[$item->customer_id])) {
                    $data[$item->customer_id] += $item->amount_gas;
                } else {
                    $data[$item->customer_id] = $item->amount_gas;
                }
                $aRes['GasRemain'][$item->date_input][$item->agent_id][$item->customer_id] = $item;
                if(!isset($aRes['data'][$item->date_input][$item->agent_id][$item->customer_id])){
                    $aRes['data'][$item->date_input][$item->agent_id][$item->customer_id] = 1;// phục vụ cho check isset 
                }
//                echo $item->date_input." - $item->customer_id - $item->agent_id ** debug";
            }        
        }
        $aRes['GasRemain'] = $data;
    }
    
//        set by $this->current_date;
    public function setDateFromAndDateTo(){
        $month = MyFormat::dateConverYmdToDmy($this->current_date,'m');
        $year = MyFormat::dateConverYmdToDmy($this->current_date,'Y');
        $month = $month - 1;
        if($month < 1){
            $month = 12;
            $year -= 1;
        }
        if($month < 10){
            $month  = '0'.$month;
        }
        $this->date_from    = $year . '-' . $month . '-01';
        $this->date_to      = MyFormat::dateConverYmdToDmy($this->date_from, 'Y-m-t');
    }
    
    public function getSanLuongFromData($aSanLuong,$aTypeMaterial){
        $sanLuong = 0;
        if(empty($aTypeMaterial) || !is_array($aTypeMaterial)){
            return $sanLuong;
        }
        foreach ($aTypeMaterial as $key => $material_type_id) {
            $sanLuong += isset($aSanLuong[$material_type_id]) ? $aSanLuong[$material_type_id] : 0;
        }
        return $sanLuong;
    }
    
    public function getTypeSmall(){
        return [
            MATERIAL_TYPE_BINH_12,
            MATERIAL_TYPE_BINH_6,
            GasMaterialsType::MATERIAL_BINH_4KG,// Bình 4 Kg
        ];
    }
    
    public function getTypeBig(){
        return [
            MATERIAL_TYPE_BINHBO_45,
            MATERIAL_TYPE_BINHBO_50,
        ];
    }
    /** @Author: LocNV Jul 29, 2019
     *  @Todo: hàm này để chạy cron quét tất cả từ giờ trở về trước để cảnh báo nợ vỏ
     *  @Param:
     * hàm này đang được chạy để test ở actionIndex của closingPersonalDebitController 
     **/
    public function getDataAll($notify = false) {
        $aSendMail  = [];
        $this->setDateFromAndDateTo();
        $aCustomer = [];        $aData = [];
        $all = $this->checkDebitVoAll($aCustomer);
        $aCustomerID = CHtml::listData($aCustomer, 'id', 'id');
        $aSanLuong = $this->calcOutputDaily($aCustomerID);
        $aRowInsert = [];
        foreach ($all as $customer_id => $item) {
            $sanLuong12     = isset($aSanLuong[$customer_id]) ? $this->getSanLuongFromData($aSanLuong[$customer_id],$this->getTypeSmall()) : 0;
            $sanLuong45     = isset($aSanLuong[$customer_id]) ? $this->getSanLuongFromData($aSanLuong[$customer_id],$this->getTypeBig()) : 0;
            $hanMuc12 = $this->calcLimitVo12($sanLuong12);
            $hanMuc45 = $this->calcLimitVo45($sanLuong45);
            $type_customer = !empty($aCustomer[$customer_id]) ? $aCustomer[$customer_id]->type_customer : '';
            $aData[$customer_id]['vo_nho']              = $item['vo_nho'];
            $aData[$customer_id]['customer_name']       = !empty($aCustomer[$customer_id]) ? $aCustomer[$customer_id]->code_bussiness . ' - ' . $aCustomer[$customer_id]->first_name : '';
            $aData[$customer_id]['address']             = !empty($aCustomer[$customer_id]) ? $aCustomer[$customer_id]->address : '';
            $aData[$customer_id]['phone']               = !empty($aCustomer[$customer_id]) ? $aCustomer[$customer_id]->phone : '';
            $aData[$customer_id]['vo_lon']              = $item['vo_lon'];
            $aData[$customer_id]['tong']                = $item['tong'];
            $aData[$customer_id]['han_muc_vo_nho']      = $hanMuc12;
            $aData[$customer_id]['han_muc_vo_lon']      = $hanMuc45;
            $aData[$customer_id]['vuot_muc_vo_nho']     = $item['vo_nho'] - $hanMuc12;
            $aData[$customer_id]['vuot_muc_vo_lon']     = $item['vo_lon'] - $hanMuc45;
            $aData[$customer_id]['san_luong_vo_nho']    = $sanLuong12;
            $aData[$customer_id]['san_luong_vo_lon']    = $sanLuong45;
            $aData[$customer_id]['type_customer']       = $type_customer;
            if($notify){
                if ($item['vo_nho'] > $hanMuc12 || $item['vo_lon'] > $hanMuc45) {
                    //tim tat ca thong bao chua xu ly status_process = 0 va chua phat
                    $criteria = new CDbCriteria();
                    $criteria->compare('t.customer_id', $customer_id);
                    $criteria->compare('t.status_process', self::STATUS_NO_PROCESS);
                    $mClosingDebit = ClosingPersonalDebit::model()->find($criteria);
                    
                    if(!empty($mClosingDebit)) {
                        //neu ton tai canh bao thi so sanh json_vo
                        $json_vo = json_decode($mClosingDebit->json_vo, true);
                        //neu 
                        if (
                                $json_vo['vuot_muc_vo_nho'] != $aData[$customer_id]['vuot_muc_vo_nho'] 
                                || $json_vo['vuot_muc_vo_lon'] != $aData[$customer_id]['vuot_muc_vo_lon']
                            ) {
                            //lay record cu update lai json va gui mail canh bao khong doi trang thai send mail
                                $data = [];
                                $data['vo_nho']             = $aData[$customer_id]['vo_nho'];
                                $data['vo_lon']             = $aData[$customer_id]['vo_lon'];
                                $data['han_muc_vo_nho']     = $aData[$customer_id]['han_muc_vo_nho'];
                                $data['han_muc_vo_lon']     = $aData[$customer_id]['han_muc_vo_lon'];
                                $data['vuot_muc_vo_nho']    = $aData[$customer_id]['vuot_muc_vo_nho'];
                                $data['vuot_muc_vo_lon']    = $aData[$customer_id]['vuot_muc_vo_lon'];
                                $mClosingDebit->json_vo = json_encode($data);
                                $mClosingDebit->update(['json_vo']);
                        }
                        $resultSendMail = $this->setSendMailGroupSaleId($mClosingDebit);
                        if(!empty($resultSendMail)){
                            $aSendMail[$mClosingDebit->sale_id][] = $resultSendMail;
                        }
                    } else {
                        //neu khong ton tai record canh bao no vo thi xet tiep
                        //tao 1 record canh bao no vo moi
                        $data = [];
                        $data['vo_nho']             = $aData[$customer_id]['vo_nho'];
                        $data['vo_lon']             = $aData[$customer_id]['vo_lon'];
                        $data['han_muc_vo_nho']     = $aData[$customer_id]['han_muc_vo_nho'];
                        $data['han_muc_vo_lon']     = $aData[$customer_id]['han_muc_vo_lon'];
                        $data['vuot_muc_vo_nho']    = $aData[$customer_id]['vuot_muc_vo_nho'];
                        $data['vuot_muc_vo_lon']    = $aData[$customer_id]['vuot_muc_vo_lon'];
                        $model = new ClosingPersonalDebit();
                        $model->sale_id             = $item['sale_id'];
                        $model->customer_id         = $customer_id;
                        $model->json_vo             = json_encode($data);
                        $model->json_vo_process     = json_encode($data);
                        $model->status_send_mail    = self::SEND_MAIL_0;
                        $model->status_debit        = self::STATUS_NO_DEBIT;
                        $model->status_process      = self::STATUS_NO_PROCESS;
                        $model->type_customer       = $type_customer;
                        $model->agent_id            = $item['agent_id'];
                        $date_now = date('Y-m-d H:i:s');
                        $date_now_bigint = strtotime(date('Y-m-d H:i:s'));
                        
                        $aRowInsert[] = "(
                            '$model->sale_id',
                            '$model->customer_id',
                            '$model->type_customer',
                            '$model->json_vo',
                            '$model->json_vo_process',
                            '$model->status_debit',
                            '$model->status_process',
                            '$model->status_send_mail',
                            '$date_now',
                            '$date_now_bigint'
                        )";
                        
                        $resultSendMail = $this->setSendMailGroupSaleId($model,null,true);
                        if(!empty($resultSendMail)){
                            $aSendMail[$model->sale_id][] = $resultSendMail;
                        }
                    }
                } else {
                    // update all khach hang trong db closing (khach hang khong no) 
                    // voi trang thai xu ly la 1
                    $criteria = new CDbCriteria();
                    $criteria->compare('t.customer_id', $customer_id);
                    $criteria->compare('t.status_process', self::STATUS_NO_PROCESS);
                    $aClosingDebit = ClosingPersonalDebit::model()->findAll($criteria);
                    if (!empty($aClosingDebit)) {
                        foreach ($aClosingDebit as $key => $mClosingDebit) {
                            $mClosingDebit->status_process = self::STATUS_PROCESSED;
                            $mClosingDebit->update(['status_process']);
                        }
                    }
                }
            }            
        }
        if($notify){
            $this->sendMailByList($aSendMail);
            if(self::IS_INSERT){
                $sql = $this->buildSqlInsert($aRowInsert);
                if(count($aRowInsert)){
                    Yii::app()->db->createCommand($sql)->execute();
                }
            }
//           
        }
        
        return $aData;
    }
    
    /** @Author: LocNV Jun , 2019
     *  @Todo: build sql insert closing personal debit
     *  @Param:
     **/
    public function buildSqlInsert($aRowInsert) {
        $tableName = ClosingPersonalDebit::model()->tableName();            
        $sql = "insert into $tableName (
                        sale_id,
                        customer_id,
                        type_customer,
                        json_vo,
                        json_vo_process,
                        status_debit,
                        status_process,
                        status_send_mail,
                        created_date,
                        created_date_bigint
                        ) values " . implode(',', $aRowInsert);
        return $sql;
    }

    /** @Author: LocNV Jul 31, 2019
     *  @Todo: quet canh bao vo hang ngay
     *  @Param:
     * quet don hang hang ngay trong gas_app_order
     **/
    public function gasAppOrderDaily($dateRun) {
        $this->current_date = $dateRun;
        $criteria = new CDbCriteria();
        $criteria->compare('t.status', GasAppOrder::STATUS_COMPPLETE);//lay nhung don hang hoan thanh
        $criteria->compare('t.date_delivery', $dateRun);
        $aData = GasAppOrder::model()->findAll($criteria);
        $aCustomerId = CHtml::listData($aData, 'customer_id', 'customer_id');
//        status_process_done = 0
        //lay cac khach hang chua xu ly
        $criteria = new CDbCriteria();
        $criteria->compare('t.status_process', self::STATUS_NO_PROCESS);
        $aClosingDebit = ClosingPersonalDebit::model()->findAll($criteria);
        $aCustomerId2 = CHtml::listData($aClosingDebit, 'customer_id', 'customer_id');
        //customer_id voi 2 mang
        $this->customer_id = $aCustomerId + $aCustomerId2;
        $this->getDataAll(true);
        $info = "dateRun $dateRun- Run gasAppOrderDaily ClosingPersonalDebit done ".date('Y-m-d H:i:s');
        Logger::WriteLog($info);
    }
    
    /** @Author: LocNV Aug 1 , 2019
     *  @Todo: get num of day between 2 date
     *  @Param: $date1 created_date
     *  @param date $date2 date now
     **/
    public function getNumOfDaysBetween($date1, $date2) {
        $date1 = date_format(date_create($date1), 'Y-m-d');
        $diff = MyFormat::getNumberOfDayBetweenTwoDate($date1, $date2);
        return $diff;
    }
    
    /** @Author: LocNV Jun 31, 2019
     *  @Todo: quy so vo no -> ra tien
     *  @Param:
     **/
    public function calcMoneyPersonalDebit($vo_12, $vo_45) {
        return ($vo_12 * 20000 + $vo_45 * 50000);
    }
    
    /** @Author: LocNV Aug 01, 2019
     *  @Todo: Xoa cong no khach hang
     *  @Param:
     **/
    public function createInventoryCustomerType2($mClosingDebit) {
        $mCustomer = Users::model()->findByPk($mClosingDebit->customer_id);
        $json_vo = json_decode($mClosingDebit->json_vo, true);
        $data = [];
//        $data['68'] = $json_vo['vo_nho']; //ma binh 12
//        $data['98'] = $json_vo['vo_lon']; //ma binh 45
        $data['68'] = $json_vo['vuot_muc_vo_nho']; //ma binh 12
        $data['98'] = $json_vo['vuot_muc_vo_lon']; //ma binh 45
        $model = new InventoryCustomer();
        $model->type = InventoryCustomer::TYPE_CLEAN_INVENTORY;
        $model->customer_id = $mClosingDebit->customer_id;
        $model->sale_id = $mClosingDebit->sale_id;
        $model->agent_id = $mCustomer->agent_id;
        $model->type_customer = $mClosingDebit->type_customer;
        $model->uid_login = 0;
        $model->c_year = date('Y');
        $model->customer_parent_id = 0;
        $model->debit = 0;
        $model->credit = 0;
        $model->json_vo = json_encode($data);
        $model->save();
    }

        
    /** @Author: LocNV Jul 30, 2019
     *  @Todo: tru cong no ca nhan cua sale
     *  @Param:
     **/
    public function createGasDebt($sale_id, $amount, $dateYmd,$voNho,$voLon) {
        $mCustomer = Users::model()->findByPk($this->customer_id);
        $first_name = isset($mCustomer) ? $mCustomer->first_name : '';
        $model = new GasDebts();
        $model->user_id     = $sale_id;
        $model->amount      = $amount;
        $model->reason      = 'Phạt hạn mức vỏ KH '.$first_name .': '.$voNho.' vỏ nhỏ, '.$voLon.' vỏ lớn';
        $model->month       = $dateYmd;
        $model->type        = GasDebts::TYPE_DEBIT_PVKH;//loai treo cong no ca nhan
        $model->relate_id   = 0;
        $model->save();
    }
//    NamNH Aug2019, get content seand mail
    public function getMailWarning($mClosingDebit,$time = 0) {
        $result = [];
        $mCustomer = Users::model()->findByPk($mClosingDebit->customer_id);
        $mOwner = Users::model()->findByPk($mClosingDebit->sale_id);
        if(empty($mCustomer) || empty($mOwner)){
            return $result;
        }
        $result['name_employee']    = isset($mOwner) ? $mOwner->first_name : '';
        $result['times']            = $time;
        $result['name_customer']    = '</b>'.$mCustomer->code_bussiness.'</b> - '.$mCustomer->first_name;
        $json_vo = json_decode($mClosingDebit->json_vo, true);
        $result['count_small']      = $json_vo['vuot_muc_vo_nho'];
        $result['count_big']        = $json_vo['vuot_muc_vo_lon'];
        $dieDate = MyFormat::modifyDays($mClosingDebit->created_date, self::SEND_MAIL_20_DAYS);
        $result['die_date']         = $dieDate;
        $result['contents']         = $dieDate <= date('Y-m-d') ?  'Hệ thống đã chuyển cho bộ phận đi thu' : 'Hệ thống sẽ tự động chuyển sang bộ phận đi thu vào ngày '.MyFormat::dateConverYmdToDmy($dieDate);
        return $result;
    }
    
    /**
     * Get times send mail
     * @return int
     */
    public function getTimeStatusSendMail(){
        switch ($this->status_send_mail) {
            case self::SEND_MAIL_0:
                return 1;
            case self::SEND_MAIL_10:
                return 2;
            case self::SEND_MAIL_20:
                return 3;
            default:
                return 0;
        }
    }
    
    /** @Author: LocNV Aug 01, 2019
     *  @Todo: kiem tra de gui mail 10 20 30 ngay
     *  @Param:
     **/
    public function setSendMailGroupSaleId($mClosingDebit, $date = null,$first = false ) {
        if (empty($date)) {
            $date = date('Y-m-d');   
        }
        $aResult = [];
        //tru tu ngay no vo den hien tai
        $diff = $this->getNumOfDaysBetween($mClosingDebit->created_date, $date);
        //kiem tra 10 ngay
        if ($mClosingDebit->status_send_mail == self::SEND_MAIL_0 && $diff >= self::SEND_MAIL_10_DAYS) {
            $mClosingDebit->status_send_mail = self::SEND_MAIL_10;
            //send mail lan 2 - 10 ngay
            $aResult = $this->getMailWarning($mClosingDebit, $mClosingDebit->getTimeStatusSendMail());
            $mClosingDebit->update(['status_send_mail']);
        } elseif ($mClosingDebit->status_send_mail == self::SEND_MAIL_10 && $diff >= self::SEND_MAIL_20_DAYS) {
            $mClosingDebit->status_send_mail = self::SEND_MAIL_20;
            //send mail lan 3 - 20 ngay
            $aResult = $this->getMailWarning($mClosingDebit, $mClosingDebit->getTimeStatusSendMail());
            $mClosingDebit->update(['status_send_mail']);
        }
        if($first){
            $aResult = $this->getMailWarning($mClosingDebit,1);
        }
        return $aResult;
    }
    
    public function getSale() {
        $mSale = $this->rSale;
        return isset($mSale->first_name) ? $mSale->first_name : "";
    }
    
    public function getCustomer() {
        $mCustomer = $this->rCustomer;
        return isset($mCustomer->first_name) ? $mCustomer->first_name . '<br>' . $mCustomer->address : "";
    }
    
    public function getArrayStatusSendMail(){
        return [
            self::SEND_MAIL_0 => 'Cảnh báo lần 1',
            self::SEND_MAIL_10 => 'Cảnh báo lần 2',
            self::SEND_MAIL_20 => 'Cảnh báo lần 3',
        ];
    }
    
    public function getStatusSendMail()
    {
        $aStatusSendmail = $this->getArrayStatusSendMail();
        return !empty($aStatusSendmail[$this->status_send_mail]) ? $aStatusSendmail[$this->status_send_mail] : '';
    }
    
    public function getArrayStatusDebit(){
        return [
            self::STATUS_NO_DEBIT       => 'Chưa tính CNCN',
            self::STATUS_DEBIT          => 'Đã tính CNCN - Theo giá vỏ',
        ];
    }
    
    public function getStatusDebit()
    {
        $aStatusDebit = $this->getArrayStatusDebit();
        return !empty($aStatusDebit[$this->status_debit]) ? $aStatusDebit[$this->status_debit] : '';
    }
    
    public function getArrayStatusProcess(){
        return [
            self::STATUS_NO_PROCESS     =>  'Chưa xử lý',
            self::STATUS_PROCESSED      =>  'Đã xử lý',
        ];
    }
    
    public function getStatusProcess() {
        $aStatusProcess = $this->getArrayStatusProcess();
        return !empty($aStatusProcess[$this->status_process]) ? $aStatusProcess[$this->status_process] : '';
    }
    
    public function getCreatedDate() {
        return MyFormat::dateConverYmdToDmy($this->created_date);
    }

    /** @Author: NamNH Aug 29,2019
     *  @Todo: get user_id alway CC
     **/
    public function getListSendAll(){
        return [
            'ducpv@spj.vn',  //Phạm Văn Đức
            'duchoan@spj.vn',  //Bùi Đức Hoàn
            'thailong@spj.vn',   //Vũ Thái Long
            'dungnt@spj.vn',
            'namnh@spj.vn',
        ];
    }
    
    public function sendMailByList($aListSend){
        if(empty($aListSend)){
            return;
        }
        $dateNow = MyFormat::dateConverYmdToDmy($this->current_date,'d/m/Y');
        $bodyHeader  = "<br><b>Danh sách vượt mức hạn vỏ đến ngày $dateNow: </b><br>";
        $bodyHeader .= '<br><table style="border-collapse: collapse;" border="1" cellpadding="5">';
        $bodyHeader .=    '<thead>';
        $bodyHeader .=        '<tr>';
        $bodyHeader .=            '<th>STT</th>';
        $bodyHeader .=            '<th>Khách hàng</th>';
        $bodyHeader .=            '<th>Vỏ nhỏ vượt mức</th>';
        $bodyHeader .=            '<th>Vỏ lớn vượt mức</th>';
        $bodyHeader .=            '<th>Cảnh báo lần</th>';
        $bodyHeader .=            '<th>Ngày chuyển sang bộ phận đi thu</th>';
//        $bodyHeader .=            '<th>Nội dung</th>';
        $bodyHeader .=            '<th>Nhân viên</th>';
        $bodyHeader .=        '</tr>';
        $bodyHeader .=    '</thead>';
        $bodyHeader   .=    '<tbody>';
        $bodyFoot = '</tbody>'
                .'</table>'
                .'<br><br><br>Đây là email tự động, vui lòng không reply. Gửi từ daukhimiennam.com';
        $body = $bodyHeader;
        $bodyGDKV = $bodyHeader;
        $bodyRawGDKV = [];
        $i = 1;
        $iGDKV = [];
        $aMonitor = $this->getEmailSend();
        foreach ($aListSend as $sale_id => $arrayContent) {
            $mOwner = Users::model()->findByPk($sale_id);
            $iSale = 1;
            $bodySale = $bodyHeader;
            foreach ($arrayContent as $key => $contentEmail) {
                $bodyRawI = '<td>'.$i++.'</td>';
                $bodyRawISale = '<td>'.$iSale++.'</td>';
                $bodyRawHead =        '<tr>';
                $bodyRaw =            '<td>'.$contentEmail['name_customer'].'</td>';
                $countSmall = $contentEmail['count_small'] > 0 ? $contentEmail['count_small'] : '';
                $bodyRaw .=            '<td style="text-align:center;">'.$countSmall.'</td>';
                $countBig = $contentEmail['count_big'] > 0 ? $contentEmail['count_big'] : '';
                $bodyRaw .=            '<td style="text-align:center;" >'.$countBig.'</td>';
                $bodyRaw .=            '<td style="text-align:center;" >'.$contentEmail['times'].'</td>';
                $bodyRaw .=            '<td style="text-align:center;" >'.MyFormat::dateConverYmdToDmy($contentEmail['die_date'],'d/m/Y').'</td>';
                //            $body .=            '<td>'.$contentEmail['contents'].'</td>';
                $bodyRaw .=            '<td>'.$contentEmail['name_employee'].'</td>';
                $bodyRawFoot =        '</tr>';
                
                $body    .= $bodyRawHead.$bodyRawI.$bodyRaw.$bodyRawFoot;
                $bodySale .= $bodyRawHead.$bodyRawISale.$bodyRaw.$bodyRawFoot;
                if(isset($aMonitor[$sale_id])){
                    if(!isset($iGDKV[$aMonitor[$sale_id]])){
                        $iGDKV[$aMonitor[$sale_id]] = 1;
                    }
                    $bodyRawIGdkv = '<td>'.$iGDKV[$aMonitor[$sale_id]]++.'</td>';
                    if(isset($bodyRawGDKV[$aMonitor[$sale_id]])){
                        $bodyRawGDKV[$aMonitor[$sale_id]] .= $bodyRawHead.$bodyRawIGdkv.$bodyRaw.$bodyRawFoot;
                    }else{
                        $bodyRawGDKV[$aMonitor[$sale_id]] = $bodyRawHead.$bodyRawIGdkv.$bodyRaw.$bodyRawFoot;
                    }
                }
            }
            $bodySale .= $bodyFoot;
//            send to sale
            if(!empty($mOwner->email)){
                $needMoreSale['title']     = 'Cảnh Báo Vượt Hạn Mức Vỏ';
                $needMoreSale['list_mail'] = [
                    $mOwner->email,
                ];
                SendEmail::bugToDev($bodySale, $needMoreSale);
            }
            
        }
//        send GDKV
        foreach ($bodyRawGDKV as $id_gdkv => $bodyItemGDKV) {
            $mUsers = Users::model()->findByPk($id_gdkv);
            if(!empty($mUsers)){
                $needMore['title']     = 'Cảnh Báo Vượt Hạn Mức Vỏ GĐKV';
                $needMore['list_mail'] = [
                    $mUsers->email,
                ];
                SendEmail::bugToDev($bodyGDKV.$bodyItemGDKV.$bodyFoot, $needMore);
            }
        }
        $body   .= $bodyFoot;
//        send all to somebody
        $needMore['title']     = 'Cảnh Báo Vượt Hạn Mức Vỏ Toàn Hệ Thống';
        $needMore['list_mail'] = $this->getListSendAll();
        SendEmail::bugToDev($body, $needMore);
    }
    
    
    /** @Author: NamNH Aug 29,2019
     *  @Todo: get email GDKV
     **/
    public function getEmailSend() {
        $result             = [];
        $mGasTargetMonthly  = new GasTargetMonthly();
        $mGasTargetMonthly->date_from_ymd = $this->current_date;
        $aGasOneMany        =  $mGasTargetMonthly->getArrayOneManyMonitorCcs('',GasOneMany::TYPE_MONITOR_GDKV);
        foreach ($aGasOneMany as $key => $mOneMany) {
            $result[$mOneMany->many_id] = $mOneMany->one_id;
        }
        return $result;
    }
    
    /** @Author: NamNH Aug 31,2019
     *  @Todo: thực hiện chuyển sang thu hộ
     *  @Param:param
     **/
    public function cronTransferUsers(){
        if(self::IS_DEBIT){
            $criteria = new CDbCriteria();
            $criteria->compare('t.status_process', self::STATUS_NO_PROCESS);
            $criteria->compare('t.status_send_mail', self::SEND_MAIL_20_DAYS);
            $aClosingDebit = ClosingPersonalDebit::model()->findAll($criteria);
            foreach ($aClosingDebit as $key => $mClosingDebit) {
                //tru cong no ca nhan
                $json_vo = json_decode($mClosingDebit->json_vo, true);
                $amount = $this->calcMoneyPersonalDebit($json_vo['vuot_muc_vo_nho'], $json_vo['vuot_muc_vo_lon']);
                $mClosingDebit->createGasDebt($mClosingDebit->sale_id, $amount, $date,$json_vo['vuot_muc_vo_nho'],$json_vo['vuot_muc_vo_lon']);
                $mClosingDebit->status_debit = self::STATUS_DEBIT; //da tru cong no ca nhan
                //xoa cong no khach hang
                $this->createInventoryCustomerType2($mClosingDebit);
                $mClosingDebit->status_process = self::STATUS_PROCESSED; // da xu ly
                $mClosingDebit->update(['status_debit', 'status_process']);
            }
            
        }
    }
}