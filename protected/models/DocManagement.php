<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DocManagement
 *
 * @author nhh
 */
class DocManagement extends BaseSpj {

    const IMAGE_MAX_UPLOAD = 20;
    const IMAGE_MAX_UPLOAD_SHOW = 3;

    public $mGasFile,$file_design;
    public static $AllowFileDoc = 'pdf,xls,xlsx,jpg,jpeg,png,docx,doc';

    public function tableName() {
        return '{{_doc_management}}';
    }

    public function beforeSave() {
        if($this->isNewRecord){
            $cUid = isset(Yii::app()->user) ? Yii::app()->user->id : '';
            if (empty($this->created_by)) {
                $this->created_by = $cUid;
            }
        }
        return parent::beforeSave();
    }
    
    public function removeAllFile(){
        GasFile::DeleteByBelongIdAndType($this->id, GasFile::TYPE_21_DOCUMENT);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('name', 'required','on'=>'doc_create,update'),
            array('created_date,file_design,name,description,created_date_bigint,created_by', 'safe')
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'rCreatedBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'rFile' => array(self::HAS_MANY, 'GasFile', 'belong_id',
                'on' => "rFile.type=" . GasFile::TYPE_21_DOCUMENT, 'order' => 'rFile.id ASC',
            )
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Tên văn bản',
            'description' => 'Mô tả',
            'file_design' => 'File',
            'created_date' => 'Ngày tạo',
            'created_by' => 'Người tạo',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        $criteria = new CDbCriteria;

        if (!empty($this->name)) {
            $criteria->compare('t.name', $this->name,true);
        }
        $sort = new CSort();

        $sort->attributes = array(
            'name' => 'name',
        );

        $sort->defaultOrder = 't.id desc';
//        $criteria->compare('can_access',$this->can_access,true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => $this->pageSize,
            )
        ));
    }

    /** @Author: HaoNH Aug 16, 2019
     *  @Todo: delete file uploaded
     * */
    public function deleteFile() {
        if (isset($_POST['delete_file']) && is_array($_POST['delete_file'])) {
            $aFileDelete = $_POST['delete_file'];
            $criteria = new CDbCriteria;
            $criteria->compare('t.belong_id', $this->id);
            $criteria->addInCondition('t.id', $aFileDelete);
            $models = GasFile::model()->findAll($criteria);
            foreach ($models as $model) {
                $model->delete();
            }
        }
    }

    /** @Author: HaoNH Aug 16, 2019
     *  @Todo: get files uploaded
     * */
    public function getFileUpload() {
        if (empty($this->id)) {
            return array();
        }
        $criteria = new CDbCriteria;
        $criteria->compare('t.belong_id', $this->id);
        $criteria->compare('t.type', GasFile::TYPE_21_DOCUMENT);
        $criteria->order = "t.id";
        return GasFile::model()->findAll($criteria);
    }

    public function getName() {
        return $this->name;
    }

    public function getDescription() {
        return $this->description;
    }
    public function getCreatedBy() {
        $mUser = $this->rCreatedBy;
        if ($mUser) {
            $created_by = $mUser->getFullname();
        }
        return $created_by;
    }

    /** @Author: HaoNH Aug 16, 2019
     *  @Todo: get files show at index
     * */
    public function getFileShowIndex() {
        $aFiles = $this->getFileUpload();
        $Xhtml = '';
        foreach($aFiles as $k => $file){            
            $Xhtml .= $file->getForceLinkDownload()."<br/>";
        }
        return $Xhtml;
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    /** @Author: NamNH Aug 21,2019
     *  @Todo: can update
     **/
    public function canUpdate() {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        if ($cRole == ROLE_ADMIN) {
            return true;
        }
        if ($cUid == $this->created_by) {
            return true;
        }
        return false;
    }
    
    /** @Author: NamNH Aug 21,2019
     *  @Todo: can delete
     **/
    public function canDelete() {
        $cRole = MyFormat::getCurrentRoleId();
        if ($cRole == ROLE_ADMIN) {
            return true;
        }
        return false;
    }

}
