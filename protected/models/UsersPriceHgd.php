<?php

/**
 * This is the model class for table "{{_users_price_hgd}}".
 *
 * The followings are the available columns in table '{{_users_price_hgd}}':
 * @property integer $id
 * @property string $zone_no
 * @property integer $c_month
 * @property integer $c_year
 * @property string $created_date
 * @property string $json
 * @property string $price_default
 * @property string $price_plus_1
 * @property string $price_plus_2
 * @property string $price_cut_1
 * @property string $price_custom
 */
class UsersPriceHgd extends BaseSpj
{
    public $JSON_FIELD  = array('list_co', 'list_nomal', 'list_vip', 'list_special', 'list_custom', 'list_agent', 'list_pay_employee');

    public $list_pay_employee = [], $list_co = [], $list_nomal = [], $list_vip = [], $list_special = [], $list_custom = [],$list_agent = [], $agent_id, $autocomplete_name, $autocomplete_material_1, $autocomplete_material_2, $autocomplete_material_3, $autocomplete_material_4, $autocomplete_material_5, $autocomplete_material_6;
    // trong list Vip còn có (list_material, price)
    public $material_1, $material_2, $material_3, $material_4, $material_5;
    public $list_material_co, $list_material_normal, $list_material_vip, $list_material_special, $list_material_custom;
    public $temp_agent, $temp_material_1, $temp_material_2, $temp_material_3, $temp_material_4, $temp_material_5, $temp_material_6;
    public $HgdPriceInit, $HgdAmountAdjust, $HgdAmountAdjustGas;
    public $aDataCache = null, $aUserCredit = [];
    
    // Sep2817 for report
    public $r_price = 0, $r_type = 1, $r_qty = 0, $r_amount = 0, $r_employee_maintain_id = 0, $r_materials_type_id = 0, $r_materials_id = 0;
    
    public function getListIndexGas() {
        return array(
            1 => 'list_material_co',
            2 => 'list_material_normal',
            3 => 'list_material_vip',
            4 => 'list_material_special',
            5 => 'list_material_custom',
        );
    }
    
    /** @Author: ANH DUNG Jul 31, 2017
     * @Todo: cho anh Hiếu sửa giá của KV khoán
     */
    public function getZoneAnhHieu() {
        return [17, 18, 19, 20];
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_users_price_hgd}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('zone_no, c_month, c_year', 'required', 'on'=>'create, update'),
            array('agent_id, id, zone_no, c_month, c_year, created_date, json', 'safe'),
            array('price_cut_1, price_plus_1, price_plus_2, price_default, list_co, list_nomal, list_vip, list_special, list_custom, list_agent', 'safe'),
            array('list_material_co, list_material_normal, list_material_vip, list_material_special, list_material_custom', 'safe'),
            array('HgdPriceInit, HgdAmountAdjust, HgdAmountAdjustGas', 'safe'),
            array('list_pay_employee, price_custom, json_other', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rAgent' => array(self::BELONGS_TO, 'Users', 'temp_agent'),
            'rFake' => array(self::BELONGS_TO, 'GasMaterials', 'material_1'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                'id' => 'ID',
                'zone_no' => 'Khu vực',
                'c_month' => 'Tháng',
                'c_year' => 'Năm',
                'created_date' => 'Ngày tạo',
                'json' => 'Chi tiết',
                'list_co' => 'Gas cỏ',
                'list_nomal' => 'Gas thông dụng',
                'list_vip' => 'Gas VIP',
                'list_special' => 'Đặc biệt',
                'list_custom' => 'Custom',
                'list_agent' => 'Đại lý',
                'price_default' => 'Giá Gas thông dụng',
                'agent_id' => 'Đại lý',
                'price_plus_1' => 'Khoảng tăng Thông dụng - Vip',
                'price_plus_2' => 'Khoảng tăng Vip - Đặc biệt',
                'price_cut_1' => 'Khoảng giảm Thông dụng - Cỏ',
                'price_custom' => 'Giá Custom',
                
                'list_material_co' => 'Gas cỏ',
                'list_material_normal' => 'Gas thông dụng',
                'list_material_vip' => 'Gas VIP',
                'list_material_special' => 'Đặc biệt',
                'list_material_custom' => 'Custom',
//                'HgdPriceInit' => 'Giá Khu Vực 1 (Giá A)',
                'HgdPriceInit' => 'Nhập số tiền tăng / giảm',
                'HgdAmountAdjust' => 'Khoảng tăng/giảm khu vực',
                'HgdAmountAdjustGas' => 'Khoảng tăng/giảm loại gas',
                'json_other' => 'Van - Dây - Bếp',
                'list_pay_employee' => 'Công giao nhận',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.zone_no', trim($this->zone_no),true);
        $criteria->compare('t.c_month', $this->c_month);
        $criteria->compare('t.c_year',$this->c_year);
        $criteria->order = 't.c_year DESC, t.c_month DESC, t.id ASC';
//        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 30,
            ),
        ));
    }
    
    /**
     * @Author: ANH DUNG Dec 28, 2016
     * @Todo: check trùng đại lý trong cùng tháng
     */
    public function checkDuplicateAgent() {
        if(!is_array($this->agent_id)){
            return ;
        }

        $cListAgent = $this->agent_id;
        $aOldAgent = $this->getAlAgentSetup();
        $mergeAll = array_merge($cListAgent, $aOldAgent);
        $duplicates = array_count_values($mergeAll);
        $duplicates = array_diff($duplicates, array(1));
        
        if(count($duplicates)){
            $aAgentId = [];
            foreach($duplicates as $key=>$value){
                if(!empty($key))
                    $aAgentId[] = $key;
            }
            if(count($aAgentId) < 1){
                return ;
            }
            $aModelAgent    = Users::getArrObjectUserByArrUid($aAgentId, '');
            $listdataAgent  = CHtml::listData($aModelAgent, 'id', 'first_name');
            $this->addError('zone_no', 'Trùng đại lý: <br>'. implode('<br>', $listdataAgent));
            $this->list_agent = $this->agent_id;// xử lý giữ lại biến khi có lỗi
        }
    }
    
    /**
     * @Author: ANH DUNG Dec 28, 2016
     * @Todo: lay tat ca cac dai ly da setup
     * @Param: 
     */
    public function getAlAgentSetup() {
        $criteria = new CDbCriteria();
        if(!empty($this->id)){
            $criteria->addCondition('t.id<>'.$this->id );
        }
        $criteria->addCondition('t.c_month='.$this->c_month.' AND t.c_year='.$this->c_year);
        
        $models = self::model()->findAll($criteria);
        $aRes = [];
        foreach($models as $model){
            $model->mapJsonDataField();
            if(empty($this->list_agent)){
                continue;
            }
            $list_agent = $model->list_agent;
            if(!is_array($model->list_agent)){
                $list_agent = explode(',', $model->list_agent);
            }
            $aRes = array_merge($aRes, $list_agent);
        }
        return $aRes;

        $duplicates = array_diff($arr, $withoutDuplicates);
        if(count($duplicates)>0){
            echo '<pre>';
            echo print_r($duplicates);
            echo '</pre >';
        }
    }
    
    public function trimData() {
        $this->zone_no          = trim($this->zone_no);
        $this->price_default    = MyFormat::removeComma($this->price_default);
        $this->price_plus_1     = MyFormat::removeComma($this->price_plus_1);
        $this->price_plus_2     = MyFormat::removeComma($this->price_plus_2);
        $this->price_cut_1      = MyFormat::removeComma($this->price_cut_1);
        $this->price_custom     = MyFormat::removeComma($this->price_custom);
    }
    
    protected function beforeValidate() {
        $aScenario = array('create', 'update');
        if(!in_array($this->scenario, $aScenario)){// vì khi copy có thể không cần validate
            return parent::beforeValidate();
        }
        $this->trimData();
        $this->formatDataSet();
        $this->setJsonOther();
        $this->setJsonDataField();
        $this->checkDuplicateAgent();
        return parent::beforeValidate();
    }
    
    public function formatDataSet() {
        $this->list_agent = is_array($this->agent_id) ? implode(',', $this->agent_id) : '';
        $temp = [];
        $temp['list_material']  = is_array($this->list_material_co) ? implode(',', $this->list_material_co) : '';
        $temp['price']          = $this->getPriceCo();
        $this->list_co          = $temp;
        
        $temp = [];
        $temp['list_material']  = is_array($this->list_material_normal) ? implode(',', $this->list_material_normal) : '';
        $temp['price']          = $this->getPriceNormal();
        $this->list_nomal       = $temp;
        
        $temp = [];
        $temp['list_material']  = is_array($this->list_material_vip) ? implode(',', $this->list_material_vip) : '';
        $temp['price']          = $this->getPriceVip();
        $this->list_vip         = $temp;
        
        $temp = [];
        $temp['list_material']  = is_array($this->list_material_special) ? implode(',', $this->list_material_special) : '';
        $temp['price']          = $this->getPriceSpecial();
        $this->list_special     = $temp;
        
        $temp = [];
        $temp['list_material']  = is_array($this->list_material_custom) ? implode(',', $this->list_material_custom) : '';
        $temp['price']          = $this->getPriceCustom();
        $this->list_custom      = $temp;
        
        // Sep2817 xử lý thêm giá công val dây bếp cho giao nhận
        $temp = is_array($this->list_pay_employee) ? $this->list_pay_employee : [];
        $aRes = [];
        foreach($temp as $materials_id => $amountPay){
            $v = MyFormat::removeComma($amountPay);
            if($v > 0){
                $aRes[$materials_id] = $v;
            }
        }
        $this->list_pay_employee    = $aRes;
    }
    
    public function formatDataGet() {
        $this->list_material_co         = isset($this->list_co['list_material']) ? explode(',', $this->list_co['list_material']) : [];
        $this->list_material_normal     = isset($this->list_nomal['list_material']) ? explode(',', $this->list_nomal['list_material']) : [];
        $this->list_material_vip        = isset($this->list_vip['list_material']) ? explode(',', $this->list_vip['list_material']) : [];
        $this->list_material_special    = isset($this->list_special['list_material']) ? explode(',', $this->list_special['list_material']) : [];
        $this->list_material_custom     = isset($this->list_custom['list_material']) ? explode(',', $this->list_custom['list_material']) : [];
    }
    
    /**
     * @Author: ANH DUNG Apr 29, 2017
     * @Todo: set json của giá bán val dây bếp
     */
    public function setJsonOther() {
        if(!is_array($this->json_other)){
            $this->json_other = [];
        }
        $aRes = [];
        foreach($this->json_other as $materials_id => $price){
            $v = MyFormat::removeComma($price);
            if($v > 0){
                $aRes[$materials_id] = $v;
            }   
        }
        $this->json_other = MyFormat::jsonEncode($aRes);
    }
    
    public function getJsonOther() {
        if(empty($this->json_other)){
            return '';
        }
        return $this->json_other = json_decode($this->json_other, true);
    }
    
    /**
     * @Author: ANH DUNG Jan 01, 2017
     * @Todo: get price của list gas để show khi update or view
     */
    public function getPriceShowOnly($fieldName, $format = false) {
        $price = isset($this->{$fieldName}['price']) ? $this->{$fieldName}['price'] : 0;
        if($format){
            return ActiveRecord::formatCurrency($price);
        }
        return $price;
    }
    
    /**
     * @Author: ANH DUNG Dec 28, 2016
     * @Todo: get price của list gas cỏ
     */
    public function getPriceCo($format = false) {
        $price = $this->price_default - $this->price_cut_1;
        if($format){
            return ActiveRecord::formatCurrency($price);
        }
        return $price;
    }
    public function getPriceNormal($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->price_default);
        }
        return $this->price_default;
    }
    public function getPriceVip($format = false) {
        $price = $this->price_default + $this->price_plus_1;
        if($format){
            return ActiveRecord::formatCurrency($price);
        }
        return $price;
    }
    public function getPriceSpecial($format = false) {
        $price = $this->price_default + $this->price_plus_2;
        if($format){
            return ActiveRecord::formatCurrency($price);
        }
        return $price;
    }
    
    public function getPriceDefault($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->price_default);
        }
        return $this->price_default;
    }
    
    public function getPriceCustom($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->price_custom);
        }
        return $this->price_custom;
    }
    
    public function getDataUpdate() {
        $this->mapJsonDataField();
        if(!empty($this->list_agent) && !is_array($this->list_agent)){
            $this->list_agent = explode(',', $this->list_agent);
        }
    }
    
    public function getListAgent($formatView = false) {    
        $res = [];
//        if(is_array($this->list_agent) && count($this->list_agent) ){
        if(is_array($this->list_agent)){
            $res = Users::getArrObjectUserByArrUid($this->list_agent, '');
        }
        if($formatView){
            $str = '';
            foreach($res as $key => $mAgent){
                $name = $mAgent->code_account . ' - '.$mAgent->getFullName();
                $str .= '<b>'.($key+1).'.</b> '.$name.'<br>';
            }
            return $str;
        }

        return $res;
    }
    
    public function geZoneNo() {
        return 'KV_'.$this->zone_no;
    }
    public function getListAgentView() {
        $this->getDataUpdate();
        return $this->getListAgent(true);
    }
    
    public function getMonthYear() {
        return $this->c_month.'/'.$this->c_year;
    }
    
    public function getPriceDefaultText() {
        return '<br><br><b>'. $this->getPriceDefault(true).'</b>';
    }

    public function canUpdate() {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        
        if($cRole == ROLE_ADMIN){
            return true;
        }
        if($cUid == GasLeave::UID_CHIEF_MONITOR && $this->c_month >= date('m')*1 && in_array($this->zone_no, $this->getZoneAnhHieu())){
            return true;
        }
        
        $aRoleAllow = array(ROLE_ADMIN, ROLE_DIRECTOR_BUSSINESS);
//        if(!in_array($cRole, $aRoleAllow) || $this->c_month != date('m')*1 || $this->c_year != date('Y')){
        if(!in_array($cRole, $aRoleAllow) || $this->c_year != date('Y') || $this->c_month < date('m')*1 ){
            return false;
        }
        
//        if(date('d')*1 != 1 && date('d') != date('t')):
//            return false;
//        endif; // Aug1717 mở cho anh HOàn sửa mọi lúc
        
        return true;
    }
    
    /**
     * @Author: ANH DUNG Dec 28, 2016
     * @Todo: get html detail
     */
    public function getDetail() {
        $mAppCache      = new AppCache();
        $aModelMaterial = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
        $this->getDataUpdate();
        $this->formatDataGet();
        $tmpPrice = array(
             1 => $this->getPriceShowOnly('list_co', true),
             2 => $this->getPriceShowOnly('list_nomal', true),
             3 => $this->getPriceShowOnly('list_vip', true),
             4 => $this->getPriceShowOnly('list_special', true),
             5 => $this->getPriceShowOnly('list_custom', true),
        );
        $str = '';
        $str .= '<table class="f_size_15"  cellpadding="0" cellspacing="0">';
        $str .= '<thead>';
            $str .= '<tr>';
                foreach ($this->getListIndexGas() as $index => $fieldName):
                    $str .= '<td class="item_c item_b">'.$this->getAttributeLabel($fieldName).'  -  ' . $tmpPrice[$index]. '</td>';
                endforeach;
            $str .= '</tr>';
        $str .= '</thead>';
        $str .= '<tbody>';
        $str .= '<tr>';
        foreach ($this->getListIndexGas() as $index => $fieldName):
            $str .= '<td>'.$this->getListGasView($aModelMaterial, $fieldName).'</td>';
        endforeach;
        $str .= '</tr>';
        $str .= '</tbody>';
        $str .= '</table>';
        return $str;
    }

    /**
     * @Author: ANH DUNG Dec 28, 2016
     * @Todo: get list name gas for one type
     */
    public function getListGasView($aModelMaterial, $fieldName) {
        $str = '';
        foreach($this->$fieldName as $key => $materials_id):
            if(empty($materials_id)){
                continue ;
            }
            $name = $materials_id;
            if(isset($aModelMaterial[$materials_id])){
                $name = $aModelMaterial[$materials_id]['materials_no'] .' - '. $aModelMaterial[$materials_id]['name'];
            }
            $str .= '<b>'.($key+1).'.</b> '.$name.'<br>';
        endforeach;
        return $str;
    }
    
    /**
     * @Author: ANH DUNG Dec 30, 2016
     * only for dev check
     * @Todo: kiểm tra trùng mã vật tư trong một zone
     */
    public function checkDuplicateMaterialInZone() {
        $model = new UsersPriceHgd();
        $model->c_month = 1;
        $model->c_year = 2017;
        $allZone = $model->getAllZoneInMonth();
        $aRes = [];
        foreach($allZone as $item):
            $item->getDataUpdate();
            $item->formatDataGet();
            $aCheck = array_merge($item->list_material_co, $item->list_material_normal, $item->list_material_vip, $item->list_material_special, $item->list_material_custom);
            echo '<pre>';
            print_r($aCheck);
            echo '</pre>';
            $duplicates = array_count_values($aCheck);
            $duplicates = array_diff($duplicates, array(1));
            if(count($duplicates)){
                $aRes[$item->id]['zone_name'] = $item->zone_no;
                $aRes[$item->id]['zone_duplicate'] = $duplicates;
            }
        endforeach;
        echo '<pre>';
        print_r($aRes);
        echo '</pre>';
        die;
    }
    
        
    /**
     * @Author: ANH DUNG Dec 30, 2016
     * @Todo: check setup gia gas ho gia dinh theo Month Year
     * @param: get record setup giá của Ngày tháng đưa vào
     */
    public function hasSetupByMonthYear() {
        $criteria = new CDbCriteria();
        $criteria->compare("t.c_month", $this->c_month);
        $criteria->compare("t.c_year", $this->c_year);
        return self::model()->find($criteria);
    }
    
    /**
     * @Author: ANH DUNG Dec 30, 2016
     * @Todo: check xem giá của tháng đưa vào có được setup không, nếu không có thì lấy tháng trước đó
     */
    public function getSetupMonthYearValid($date) {
        $this->c_month = MyFormat::dateConverYmdToDmy($date, "m")*1;
        $this->c_year  = MyFormat::dateConverYmdToDmy($date, "Y")*1;
        
        if(is_null($this->hasSetupByMonthYear())){// Nếu tháng request không có dữ liệu thì phải lấy của tháng trước
            $datePrevMonth = MyFormat::modifyDays($date, 1, '-', 'month');
            $this->c_month = MyFormat::dateConverYmdToDmy($datePrevMonth, "m")*1;
            $this->c_year  = MyFormat::dateConverYmdToDmy($datePrevMonth, "Y")*1;
            Logger::WriteLog("Need Check getPriceHgd From Prev Month  $this->c_month - $this->c_year Get From Cache");
        }
    }
    
    /**
     * @Author: ANH DUNG Dec 30, 2016
     * @Todo: get price của gas hộ gd + vật tư theo năm tháng
     * @param: $date Y-m-d
     * @use_at_1: AppCache::setPriceHgd($cacheKey);
     * @use_at_2: Sta2::OutputAgentData($model, $aRes)
     */
    public static function getPriceHgd($date) {
        /* 1. get price setup HGD theo thang nam
         * 2. format dinh dang theo [material_id][zone_id]=>price
         */
        $mUserPriceHgd = new UsersPriceHgd();
        $mUserPriceHgd->getSetupMonthYearValid($date);
        
        $models = $mUserPriceHgd->getAllZoneInMonth();
        $aRes = [];
        foreach($models as $item){// $item is model UsersPriceHgd
            $item->getDataUpdate();
            $item->formatDataGet();
            if(!is_array($item->list_agent) || empty($item->list_agent[0])){
                continue;
            }
            $temp = $aPriceMaterials = $aPayEmployee = [];
            foreach($item->list_material_co as $materials_id){
                if(empty($materials_id)){ continue; }
                $aPriceMaterials[$materials_id] = isset($item->list_co['price']) ? $item->list_co['price'] : 0;
            }
            foreach($item->list_material_normal as $materials_id){
                if(empty($materials_id)){ continue; }
                $aPriceMaterials[$materials_id] = isset($item->list_nomal['price']) ? $item->list_nomal['price'] : 0;
            }
            foreach($item->list_material_vip as $materials_id){
                if(empty($materials_id)){ continue; }
                $aPriceMaterials[$materials_id] = isset($item->list_vip['price']) ? $item->list_vip['price'] : 0;
            }
            foreach($item->list_material_special as $materials_id){
                if(empty($materials_id)){ continue; }
                $aPriceMaterials[$materials_id] = isset($item->list_special['price']) ? $item->list_special['price'] : 0;
            }
            foreach($item->list_material_custom as $materials_id){
                if(empty($materials_id)){ continue; }
                $aPriceMaterials[$materials_id] = isset($item->list_custom['price']) ? $item->list_custom['price'] : 0;
            }
            
            /* Apr 29, 2017 bổ sung thêm giá bán bếp val dây*/
            $item->getJsonOther();
            if(is_array($item->json_other)){
                $aPriceMaterials = $aPriceMaterials + $item->json_other;
            }
            if(is_array($item->list_pay_employee)){
                $aPayEmployee = $item->list_pay_employee;
            }

            $temp['list_agent']         = $item->list_agent;
            $temp['list_materials']     = $aPriceMaterials;
            $temp['list_pay_employee']  = $aPayEmployee;
            $aRes[]                     = $temp;
            // xử lý đưa array gent vào mảng để ra ngoài in_array lấy giá cho từng đại lý
        }
        return $aRes;
    }
    
    /** @Author: ANH DUNG Dec 28, 2016
     *  @Todo: Anh Hoàn auto set price for all 1 lần ngày 01 đầu tháng
     * @param: $this->HgdPriceInit là khoảng tăng / giảm của 1 tháng
     */
//    public function autoSetPriceOld() {
//        $allZone = $this->getAllZoneInMonth();
//        $HgdPriceInit = $this->HgdPriceInit;
//        foreach($allZone as $key => $model){
//            $model->price_default   = $HgdPriceInit;
//            $model->price_plus_1    = $this->HgdAmountAdjustGas;
//            $model->price_plus_2    = $this->HgdAmountAdjustGas*2;
//            $model->price_cut_1     = $this->HgdAmountAdjustGas;
//            $model->autoSetPriceOneZone();
//            $HgdPriceInit -= $this->HgdAmountAdjust;
//        }
//        AppCache::setPriceHgd(AppCache::PRICE_HGD);
//    }
    /** @Author: ANH DUNG Dec 28, 2016 => fix on Dec0417
     *  @Todo: Anh Hoàn auto set price for all 1 lần ngày 01 đầu tháng
     * @param: $this->HgdPriceInit là khoảng tăng / giảm của 1 tháng
     * sẽ cộng hoặc trừ giá hiện tại theo khoảng tăng hoặc giảm
     */
    public function autoSetPrice() {
        $allZone = $this->getAllZoneInMonth();
        $amountAdjust = $this->HgdPriceInit;
        foreach($allZone as $model){
            $model->price_default   += $amountAdjust;
            $model->price_custom    += $amountAdjust;
            $model->autoSetPriceOneZone($amountAdjust);
        }
        $mAppCache = new AppCache();
        $mAppCache->setPriceHgd(AppCache::PRICE_HGD);
    }
    
    /** @Author: ANH DUNG Dec 28, 2016
     *  @Todo: set update cho từng model luôn
     */
    public function autoSetPriceOneZone($amountAdjust) {
        $this->getDataUpdate();
        $this->list_co['price']         += $amountAdjust;
        $this->list_nomal['price']      += $amountAdjust;
        $this->list_vip['price']        += $amountAdjust;
        $this->list_special['price']    += $amountAdjust;
        if(isset($this->list_custom['price'])){
            $this->list_custom['price']     += $amountAdjust;
        }
        $this->setJsonDataField();
        $this->update();
    }
    
    /** @Author: ANH DUNG Feb 06, 2018
     *  @Todo: Tăng thêm một 10 KV giá để Anh Hoàn setup thêm
     * @param: $this->HgdPriceInit là khoảng tăng / giảm của 1 tháng
     * sẽ cộng hoặc trừ giá hiện tại theo khoảng tăng hoặc giảm
     * @doing: 
     * get giá của zone 1, vì zone 1 là cao nhất ko đổi như mấy thằng khoán, sau đó cộng lên
     */
    public function plusMoreZone() {
        $totalPlus = 10; $amountBetweenZone = 5000;
        $mUsersPriceHgd = new UsersPriceHgd();
        $mUsersPriceHgd->c_month = date('m');
        $mUsersPriceHgd->c_year  = date('Y');
        $allZone = $mUsersPriceHgd->getAllZoneInMonth();
        $maxNumberZone = count($allZone);
        $priceBegin = $allZone[0]->price_default - ($maxNumberZone * $amountBetweenZone);
        $mUsersPriceHgdCopy = $allZone[0];
        
        for($i = 1; $i <= $totalPlus; $i ++):
            $maxNumberZone++;
            $model = new UsersPriceHgd();
            $model->zone_no         = $maxNumberZone;
            $model->c_month         = $mUsersPriceHgd->c_month;
            $model->c_year          = $mUsersPriceHgd->c_year;
            $model->price_default   = $priceBegin;
            $model->price_plus_1    = $mUsersPriceHgdCopy->price_plus_1;
            $model->price_plus_2    = $mUsersPriceHgdCopy->price_plus_2;
            $model->price_cut_1     = $mUsersPriceHgdCopy->price_cut_1;
            $model->save();
            $priceBegin -= $amountBetweenZone;
        endfor;
        $mAppCache = new AppCache();
        $mAppCache->setPriceHgd(AppCache::PRICE_HGD);
    }
    
    
    /**
     * @Author: ANH DUNG Dec 28, 2016
     * @Todo: lấy tất cả các KV trong tháng năm
     */
    public function getAllZoneInMonth() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.c_month='.($this->c_month*1).' AND t.c_year='.$this->c_year);
        $criteria->order = 't.id ASC';
        return self::model()->findAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG Dec 28, 2016
     * cron copy data của tháng hiện tại sang tháng mới chạy vào ngày 25 tháng của tháng hiện tại
     * need setup cron UsersPriceHgd::copyFromPrevMonth();
     * $cMonth = date('Y-m-d')
     */
    public static function copyFromPrevMonth($cMonth) {
//        die;// run test ok from Dec 28, 2016
        $nextMonth  = MyFormat::modifyDays($cMonth, 1, '+', 'month');
        $nextMonth  = explode('-', $nextMonth);
        $firstDateNextMonth  = $nextMonth[0].'-'.$nextMonth[1].'-01';
        UsersPriceHgd::deleteByMonthYear($nextMonth[1], $nextMonth[0]);
        // get price all zone của tháng cũ copy
        $mUsersPriceHgd = new UsersPriceHgd();
        $currentMonth  = explode('-', $cMonth);
        $mUsersPriceHgd->c_month = $currentMonth[1];
        $mUsersPriceHgd->c_year  = $currentMonth[0];
        
        $allZone = $mUsersPriceHgd->getAllZoneInMonth();
        foreach($allZone as $model){
            $aFieldNotCopy = array('id', 'c_month', 'c_year', 'created_date');
            $newModel = new UsersPriceHgd();
            MyFormat::copyFromToTable($model, $newModel, $aFieldNotCopy);
            $newModel->c_month = $nextMonth[1];
            $newModel->c_year  = $nextMonth[0];
            $newModel->save();
        }
        $info = 'Cron NGÀY 25 HÀNG THÁNG COPY GIÁ KH hộ gd UsersPriceHgd::copyFromPrevMonth';
        Logger::WriteLog($info);
        SendEmail::bugToDev($info);
    }
    
    /** @Author: ANH DUNG Sep 29, 2017
     * @Todo: delete before copy
     */
    public static function deleteByMonthYear($month, $year) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('c_month=' . $month*1);
        $criteria->addCondition('c_year=' . $year*1);
//        $models = UsersPriceHgd::model()->findAll($criteria);
        UsersPriceHgd::model()->deleteAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG Jan 03, 2017
     * @Todo: check user có quyền chỉnh tăng giảm của 1 tháng
     */
    public function canSetAdjustAmountInMonth() {
        $cRole      = MyFormat::getCurrentRoleId();
        $aRoleAllow = array(ROLE_ADMIN, ROLE_DIRECTOR_BUSSINESS);
        return in_array($cRole, $aRoleAllow);
    }
    
    /** @Author: ANH DUNG Sep 28, 2017
     *  @Todo: get list pay for employee of one agent from cache 
     */
    public function getDataPayEmployee() {
        if(is_null($this->aDataCache)){
            $mAppCache = new AppCache();
            $this->aDataCache = $mAppCache->getPriceHgd(AppCache::PRICE_HGD);
        }
        return MyFunctionCustom::getHgdPriceSetup($this->aDataCache, $this->agent_id, 'list_pay_employee');
    }
    
    /** @Author: ANH DUNG Apr 24, 2017 - tính toán price từng vật tư cho GN
     * @param: $this->r_type : 1: vật tư, 2 bộ bình, 3: uphold bảo trì
     * @fix: Sep2817 đưa về OOP
     */
//    public function employeeMaintainCalcPrice(&$price, $type, $qty, $employee_maintain_id, $materials_type_id, $materials_id) {
    public function calcPayEmployee() {
//        public $r_price=0, $r_type=0, $r_qty=0, $r_employee_maintain_id=0, $r_materials_type_id=0, $r_materials_id=0;
        if($this->r_qty < 0){
            return 0;
        }
        if($this->r_type == Sta2::SALARY_TYPE_BOBINH){
            $this->r_price = 50000;
            return $this->r_price*$this->r_qty;// 50K
        }elseif($this->r_type == Sta2::SALARY_TYPE_UPHOLD){
            $this->r_price = 8000;
            return $this->r_price*$this->r_qty;// 8K
        }
        
        $this->r_amount = 0;
        switch ($this->r_materials_type_id) {
            case MATERIAL_TYPE_BINH_12:
                $this->r_price = 10000;
                if(!empty($this->r_employee_maintain_id) && array_key_exists($this->r_employee_maintain_id, $this->aUserCredit)){
                    $aCreditPay = GasConst::getArrCreditMapPay();
                    $this->r_price = $aCreditPay[$this->aUserCredit[$this->r_employee_maintain_id]];
                }
                break;
            case MATERIAL_TYPE_BINHBO_50:
            case MATERIAL_TYPE_BINHBO_45:
                $this->r_price = 25000;
                break;
            case MATERIAL_TYPE_BINH_6:
            case GasMaterialsType::MATERIAL_BINH_4KG:
                $this->r_price = 4500;
                break;
        }
        $this->r_amount = $this->r_price * $this->r_qty;
        if(!empty($this->r_amount)){
            return $this->r_amount;
        }
        $this->r_amount = 0;
        $aDataPay = $this->getDataPayEmployee();
        if(isset($aDataPay[$this->r_materials_id])){
            $this->r_price  = $aDataPay[$this->r_materials_id];
            $this->r_amount = $this->r_price * $this->r_qty;
        }
        return $this->r_amount;
    }
    
    /** @Author: ANH DUNG Sep2817
     *  @Todo: get text show amount pay với NV ký qũy B12
     */
    public function getTextPay($fullText = false) {
        $amountPay = 10000;
        if(!empty($this->r_employee_maintain_id) && array_key_exists($this->r_employee_maintain_id, $this->aUserCredit)){
            $aCreditPay = GasConst::getArrCreditMapPay();
            if($fullText){
                $amountPay = ' ['.($aCreditPay[$this->aUserCredit[$this->r_employee_maintain_id]]).']';
            }else{
                $amountPay = $aCreditPay[$this->aUserCredit[$this->r_employee_maintain_id]];
            }
        }
        return $amountPay;
    }

}