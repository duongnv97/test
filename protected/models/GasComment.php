<?php
/**
 * This is the model class for table "{{_gas_comment}}".
 *
 * The followings are the available columns in table '{{_gas_comment}}':
 * @property string $id
 * @property integer $type
 * @property string $belong_id
 * @property string $content
 * @property string $created_date
 */
class GasComment extends BaseSpj
{
    const TYPE_1_SPANCOP        = 1; // tab comment báo cáo KH chuyên đề đặc biệt
    const TYPE_2_SUPPORT_AGENT  = 2; // Now 02, 2015 Đề xuất sửa chữa của đại lý
    const TYPE_3_MONITOR_HGD    = 3; // Aug1317 note cho monitor đơn hàng hộ GĐ
    const TYPE_4_SELL_CANCEL    = 4; // Now2018 note đơn hàng hủy
    const TYPE_5_Sell_COMMENT   = 5; // NamNH Sep0519 lưu lịch sử test audit của đơn hàng
    const TYPE_6_SETTLE_THUQUY_NOTE     = 6; //LocNV Sep 13, 2019 ghi note cho gas settle
    const TYPE_7_SETTLE_KEOAN_NOTE      = 7; //LocNV Sep 13, 2019 ghi note cho gas settle

    // array type => name model
    public static $TYPE_MODEL = array(
        GasComment::TYPE_1_SPANCOP          => 'GasBussinessContract',
        GasComment::TYPE_2_SUPPORT_AGENT    => 'GasSupportAgent',
        GasComment::TYPE_3_MONITOR_HGD      => 'TransactionHistory',
        GasComment::TYPE_4_SELL_CANCEL      => 'Sell',
    );
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_comment}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, type, belong_id, content, created_date', 'safe'),
            array('content', 'required', 'on'=>'spancop_comment'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rSell' => array(self::BELONGS_TO, 'Sell', 'belong_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'type' => 'Type',
            'belong_id' => 'Belong',
            'uid_login' => 'Người Tạo',
            'content' => 'Nội dung',
            'created_date' => 'Created Date',
        );
    }

    public function search()
    {
        $criteria=new CDbCriteria;
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    public function defaultScope()
    {
        return array();
    }
    
    protected function beforeSave() {
        $aAttributes = array('content');
        MyFormat::RemoveScriptOfModelField($this, $aAttributes);
        return parent::beforeSave();
    }
    
    /**
     * @Author: ANH DUNG Sep 27, 2015
     * @Todo: all record will save here
     * @Param: $model
     */
    public function SaveRow($mBelongTo, $type) {
        $this->belong_id    = $mBelongTo->id;
        $this->type         = $type;
        $this->uid_login    = MyFormat::getCurrentUid();
        $this->save();
    }
    
    /**
     * @Author: ANH DUNG Sep 27, 2015
     * @Todo: something
     */
    public function getUidLogin() {
        $mUser = $this->rUidLogin;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    
    public function getContent() {
        return nl2br($this->content);
    }
    
    /**
     * @Author: ANH DUNG Nov 19, 2015
     * @Todo: something
     * @Param: 
     */
    public function ShowItem() {
        $cmsFormater = new CmsFormatter();
        return "<b>{$this->getUidLogin()}</b> <i>{$cmsFormater->formatDateTime($this->created_date)}</i>: ".nl2br($this->content)."<br>";
    }
    
    /**
     * @Author: ANH DUNG Sep 19, 2015
     * @Todo: something
     * @Param: $model
     */
    public static function DeleteByType($belong_id, $type) {
        $criteria = new CDbCriteria;
        $criteria->compare('belong_id', $belong_id);
        $criteria->compare('type', $type);
        self::model()->deleteAll($criteria);
    }
    
    // Now 02, 2015 for demo text show
//    public function formatSpancopReport($model){
//        $cmsFormater = new CmsFormatter();
//       $res = "";
//       foreach($model->rComment as $mComment){
//           $res .= "<b>{$mComment->getUidLogin()}</b> <i>{$cmsFormater->formatDateTime($mComment->created_date)}</i>: ".nl2br($mComment->getContent())."<br>";
//       }
//       $note = "<b>Ghi chú: </b>".nl2br($model->note);
//       $res = "$res".$note;
//       return $res;
//   }
    
    /** @Author: ANH DUNG Aug 13, 2017
     * @Todo: get list comment of Transaction History by List transaction history id
     */
    public function getByArrayTransactionHistoryId() {
        if(count($this->belong_id) < 1){
            return [];
        }
        $sParamsIn = implode(',', $this->belong_id);
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.belong_id IN ($sParamsIn)" .' AND t.type='.$this->type);
        $criteria->order = 't.id ASC';
        $models = self::model()->findAll($criteria);
        $aRes = [];
        foreach($models as $item){
            $aRes[$item->belong_id][] = '<span><b>'.MyFormat::dateConverYmdToDmy($item->created_date, 'H:i').'</b> '.$item->content.'</span><br>';
        }
        return $aRes;
    }
    
    /**
     * @Author: ANH DUNG Aug 13, 2017
     * @Todo: cron delete những record tạo 1 ngày trước
     */
    public static function cronDelete() {
        $day = 10; $from = time();
        $aType = [GasComment::TYPE_3_MONITOR_HGD];
        $criteria = new CDbCriteria();
        $criteria->addCondition("DATE_ADD(created_date,INTERVAL $day DAY) < NOW()");
        $criteria->addInCondition('type', $aType);
        self::model()->deleteAll($criteria);        
        $to = time();
        $second = $to-$from;
        Logger::WriteLog("GasComment delete record 1 day before ". ' done in: '.($second).'  Second  <=> '.($second/60).' Minutes');
    }
    
    /** @Author: KHANH TOAN 2018
     *  @Todo: save lí do hủy đơn hàng
     *  @Param:
     **/
    public function saveCommentCancel() {
        $cUid = MyFormat::getCurrentUid();
        $mGasCommentOld = GasComment::model()->findByAttributes(array('belong_id' => $this->belong_id, 'type' => GasComment::TYPE_4_SELL_CANCEL));
        if(!empty($mGasCommentOld)){
//            $mGasCommentOld->delete();// Apr1119 ko xóa cái cũ
        }
        $this->content = trim($this->content);
        if(empty($this->content)){
            return ;
        }
        $mGasCommentNew                 = new GasComment();
        $mGasCommentNew->type           = GasComment::TYPE_4_SELL_CANCEL;
        $mGasCommentNew->belong_id      = $this->belong_id;
        $mGasCommentNew->uid_login      = $cUid;
        $mGasCommentNew->content        = $this->content;
        $mGasCommentNew->created_date   = date("Y-m-d H:i:s");
        $mGasCommentNew->save();
    }

    public function formatNoteAudit($note_old, $note_new){ // $note_old = $this->note cũ
        if(!empty($note_old)){
            $aNote = explode($this->getSpareText(),$note_old);
            if(!empty($aNote[1])){
                $aNote[1] = $note_new ;
                return implode($this->getSpareText(), $aNote);
            }
        }
        return $note_old. $this->getSpareText() .$note_new;

    }
    
    public function getContentSell() {
        return $this->content;
    }
    
    /** @Author: DungNT Apr 11, 2019
     *  @Todo: get by belong_id and type
     **/
    public function getByBelongId($getMulti = false) {
        $criteria = new CDbCriteria();
        $criteria->compare('belong_id', $this->belong_id);
        $criteria->compare('type', $this->type);
        $criteria->order = 't.id DESC';
        if($getMulti){
            return GasComment::model()->findAll($criteria);
        }else{
            return GasComment::model()->find($criteria);
        }
    }
    
    /** @Author: NamNH Sep0519
     *  @Todo: save comment audit
     **/
    public function saveCommentAudit() {
        $cUid = MyFormat::getCurrentUid();
        $this->content = trim($this->content);
        $mGasCommentNew                 = new GasComment();
        $mGasCommentNew->type           = $this->type;
        $mGasCommentNew->belong_id      = $this->belong_id;
        $mGasCommentNew->uid_login      = $cUid;
        $mGasCommentNew->content        = $this->content;
        $mGasCommentNew->status         = $this->status;
        $mGasCommentNew->save();
    }
}