<?php

/**
 * This is the model class for table "{{_gas_break_task_daily}}".
 *
 * The followings are the available columns in table '{{_gas_break_task_daily}}':
 * @property string $id
 * @property string $break_task_id
 * @property integer $current_status
 * @property string $report_date
 * @property string $report_content 
 * @property string $job_change
 * @property string $uid_login
 * @property string $created_date
 */
class GasBreakTaskDaily extends BaseSpj
{
    public $file_name;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasBreakTaskDaily the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_break_task_daily}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('report_date, report_content,current_status', 'required', 'on'=>'create'),
            array('current_status, report_content', 'required', 'on'=>'create_leader_help'),
            array('id, break_task_id, current_status, report_date, report_content, job_change, uid_login, created_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rFile' => array(self::HAS_MANY, 'GasFile', 'belong_id',
                'on'=>'rFile.type='.  GasFile::TYPE_1_BREAK_TASK,
                'order'=>'rFile.id ASC',
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        $aLabel = array(
            'id' => 'ID',
            'break_task_id' => 'Break Task',
            'current_status' => 'Trạng thái',
            'report_date' => 'Ngày',
            'report_content' => 'Nội dung',            
            'job_change' => 'Công việc phát sinh',
            'uid_login' => 'Người Tạo',
            'created_date' => 'Ngày Tạo',
        );
        if ($this->scenario == "create_leader_help"){
            $aLabel['report_content'] = "Ý Kiến Chỉ Đạo";
        }
        return $aLabel;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.break_task_id',$this->break_task_id);
        $criteria->compare('t.current_status',$this->current_status);
        $criteria->compare('t.report_date',$this->report_date,true);
        $criteria->compare('t.report_content',$this->report_content,true);				
        $criteria->compare('t.job_change',$this->job_change,true);
        $criteria->compare('t.uid_login',$this->uid_login);
        $criteria->compare('t.created_date',$this->created_date,true);

        $criteria->order = "t.id DESC";
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    public function defaultScope()
    {
            return array(
                    //'condition'=>'',
            );
    }
    
    /**
     * @Author: ANH DUNG Dec 14, 2014
     */
    protected function beforeSave() {
        if(strpos($this->report_date, '/')){
            $this->report_date = MyFormat::dateConverDmyToYmd($this->report_date);
            MyFormat::isValidDate($this->report_date);
        }
        
        $this->report_content = InputHelper::removeScriptTag($this->report_content);
        $this->job_change = InputHelper::removeScriptTag($this->job_change);
        
        if($this->isNewRecord){
            $this->uid_login = Yii::app()->user->id;
        }
        
        return parent::beforeSave();
    }    
    
    /**
     * @Author: ANH DUNG Dec 15, 2014
     * @Todo: get last comment for this
     * @Param: $model is model GasBreakTask
     */
    public static function GetLastComment($model) {
        $res = '';
        $criteria = new CDbCriteria();
        $criteria->compare("t.break_task_id", $model->id);
        $criteria->order = "t.id DESC";
        $criteria->limit = 1;
        $mDaily = self::model()->find($criteria);
        if($mDaily){
            $cmsFormater = new CmsFormatter();
            $res = $mDaily->rUidLogin->first_name."<br>".$cmsFormater->formatDateTime($mDaily->created_date);
        }
        return $res;
    }
    
    protected function beforeDelete() {
        GasFile::DeleteByBelongIdAndType($this->id, GasFile::TYPE_1_BREAK_TASK);
        return parent::beforeDelete();
    }
    
}