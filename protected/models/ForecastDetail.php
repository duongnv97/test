<?php
 
/**
 * This is the model class for table "{{_forecast_detail}}".
 *
 * The followings are the available columns in table '{{_forecast_detail}}':
 * @property integer $id
 * @property integer $customer_id
 * @property string $date_notify 
 * @property string $date_notify_search
 * @property string $created_date
 */
class ForecastDetail extends BaseSpj
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ForecastDetail the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_forecast_detail}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['date_notify, date_notify_search, created_date', 'required','on'=>'create,update'],
            ['id, customer_id, date_notify, date_notify_search, created_date', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Khách hàng',
            'date_notify' => 'Ngày thông báo',
            'date_notify_search' => 'Ngày thông báo',
            'created_date' => 'Ngày tạo',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

	$criteria->compare('t.id',$this->id);
	$criteria->compare('t.customer_id',$this->customer_id);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /** @Author: NamNH month/2018
     *  @Todo: get date notify
     **/
    public function getDateNotify(){
        return !empty($this->date_notify) ? MyFormat::dateConverYmdToDmy($this->date_notify) : '';
    }
    
}