<?php

/**
 * This is the model class for table "{{_gas_maintain}}".
 *
 * The followings are the available columns in table '{{_gas_maintain}}':
 * @property string $id
 * @property integer $customer_id
 * @property string $maintain_date
 * @property integer $maintain_employee_id
 * @property integer $accounting_employee_id
 * @property integer $materials_id
 * @property string $seri_no
 * @property string $note
 * @property integer $agent_id
 * @property string $note_update_status
 * @property integer $update_num
 * @property integer $status
 * @property integer $is_new_customer
 * @property integer $user_id_create
 * @property string $created_date
 * @property integer $is_need_maintain_back
 * @property integer $status_maintain_back
 * @property string $note_maintain_back
 * @property integer $status_call_bad
 * @property string $note_call_bad
 * @property integer $using_gas_huongminh
 */
class GasMarketDevelopment extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GasMarketDevelopment the static model class
	 */
	 
         public $customer_name;
         public $customer_address;
         public $customer_phone;
         public $customer_code_bussiness;
         public $customer_code_account;
         public $autocomplete_name;
         public $date_from;
         public $date_to;
         public $count_row;
         public $statistic_month;
         public $statistic_year;
         public $had_selling;     	          
	 
        public $modelUser;
        public $autocomplete_name_street;
        public $autocomplete_material_name;
        public $MAX_ID;
	 
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{_gas_maintain}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('customer_id', 'required','on'=>'create_marketdevelopment,update_marketdevelopment'),
			array('maintain_date,monitoring_id, accounting_employee_id, materials_id', 'required','on'=>'create_marketdevelopment,update_marketdevelopment'),	
			array('customer_id, maintain_employee_id, accounting_employee_id, materials_id, agent_id, update_num, status, is_new_customer, user_id_create, is_need_maintain_back, status_maintain_back, status_call_bad, using_gas_huongminh', 'numerical', 'integerOnly'=>true),
			array('seri_no', 'length', 'max'=>20),
			array('note', 'length', 'max'=>600),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, customer_id, maintain_date, maintain_employee_id, accounting_employee_id, materials_id, seri_no, note, agent_id, note_update_status, update_num, status, is_new_customer, user_id_create, created_date, is_need_maintain_back, status_maintain_back, note_maintain_back, status_call_bad, note_call_bad, using_gas_huongminh', 'safe'),
			array('monitoring_id,is_new_customer', 'safe'),
			array('province_id,district_id,ward_id', 'safe'),
			array('customer_code_bussiness,customer_address', 'safe'),
			array('date_from,date_to', 'safe'),
			array('customer_id, maintain_date,monitoring_id, maintain_employee_id, accounting_employee_id, materials_id', 'required','on'=>'create_marketdevelopment_customer_old'),				
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'customer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
                    'maintain_employee' => array(self::BELONGS_TO, 'Users', 'maintain_employee_id'),
                    'accounting_employee' => array(self::BELONGS_TO, 'Users', 'accounting_employee_id'),
                    'agent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
                    'materials' => array(self::BELONGS_TO, 'GasMaterials', 'materials_id'),
                    'GasMaintainSell' => array(self::HAS_MANY, 'GasMaintainSell', 'maintain_id','joinType'=>'INNER JOIN'),		
                    'monitoring' => array(self::BELONGS_TO, 'Users', 'monitoring_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'customer_id' => 'Khách Hàng',
			'maintain_date' => 'Ngày',
			'maintain_employee_id' => 'Nhân Viên PTTT',
			'accounting_employee_id' => 'Nhân Viên Kế Toán',
			'materials_id' => 'Thương Hiệu Gas',
			'seri_no' => 'Số Seri',
			'note' => 'Ghi Chú',
			'agent_id' => 'Đại Lý',
			'note_update_status' => 'Ghi Chú Sau Cuộc Gọi',
			'update_num' => 'Số Lần Đã Cập Nhật',
			'status' => 'Trạng Thái',
			'user_id_create' => 'Người Tạo',
			'created_date' => 'Ngày Tạo',
			'customer_name' => 'Họ Tên Khách Hàng',
			'customer_address' => 'Địa Chỉ',
			'customer_phone' => 'Điện Thoại',
			'customer_code_bussiness' => 'Mã KH',
			'customer_code_account' => 'Mã Hệ Thống',
			'is_new_customer' => 'KH Mới',
			'is_need_maintain_back' => 'Cần nhân viên bảo trì xuống nữa',
			'status_maintain_back' => 'Kết Quả Xử Lý',
			'note_maintain_back' => 'Ghi Chú Sau Xử Lý',
			'status_call_bad' => 'Kết Quả Xử Lý',
			'note_call_bad' => 'Ghi Chú Sau Xử Lý',
			'date_from' => 'Từ Ngày',
			'date_to' => 'Đến Ngày',                    
			'statistic_month' => 'Chọn Tháng',
			'statistic_year' => 'Chọn Năm',
			'had_selling' => 'Bán Hàng Bảo Trì',
			'using_gas_huongminh' => 'Đang Dùng Gas Hướng Minh',
			'monitoring_id' => 'Giám Sát PTTT',
                        'province_id' => 'Tỉnh',
                        'district_id' => 'Quận Huyện',
                        'ward_id' => 'Phường/Xã',
                        'code_no' => 'Mã Số',                    
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;

		$criteria=new CDbCriteria;
		$aWith=array();
        $criteria->compare('t.code_no',$this->code_no,true);
		$criteria->compare('t.customer_id',$this->customer_id);
		if(!empty($this->maintain_date)){
            $this->maintain_date = MyFormat::dateDmyToYmdForAllIndexSearch($this->maintain_date);
            $criteria->compare('t.maintain_date',$this->maintain_date,true);
		}			
		if(!empty($this->created_date)){
            $this->created_date = MyFormat::dateDmyToYmdForAllIndexSearch($this->created_date);
            $criteria->compare('t.created_date',$this->created_date,true);
		}			
		
		if(!empty($this->customer_address)){
            $aWith[] = 'customer';			
            $criteria->compare('customer.address',$this->customer_address,true);
		}	
		if(!empty($this->customer_code_bussiness)){
            $aWith[] = 'customer';			
            $criteria->compare('customer.code_bussiness',$this->customer_code_bussiness,true);
		}
                
		$criteria->compare('t.maintain_employee_id',$this->maintain_employee_id);
		$criteria->compare('t.accounting_employee_id',$this->accounting_employee_id);
		$criteria->compare('t.materials_id',$this->materials_id);
		$criteria->compare('t.seri_no',$this->seri_no,true);
		$criteria->compare('t.note',$this->note,true);
        if($cRole==ROLE_SUB_USER_AGENT){
            $criteria->compare('t.agent_id',Yii::app()->user->parent_id);
        }else{
            if(!empty($this->agent_id)){                        
                $criteria->compare('t.agent_id', $this->agent_id);
            }
            GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');
        }
        if($cRole==ROLE_EMPLOYEE_MARKET_DEVELOPMENT){
            $criteria->compare('t.maintain_employee_id', $cUid);
        }
        
		$criteria->compare('t.note_update_status',$this->note_update_status,true);
		$criteria->compare('t.update_num',$this->update_num);
        if(is_array($this->status))
            $criteria->addInCondition('t.status', $this->status); 
        else
            $criteria->compare('t.status',$this->status); 

		if(!empty($this->date_from))
			$this->date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
		if(!empty($this->date_to))
			$this->date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);				
        if(!empty($this->date_from) && empty($this->date_to))
                $criteria->addCondition("t.maintain_date>='$this->date_from'");
        if(empty($this->date_from) && !empty($this->date_to))
                $criteria->addCondition("t.maintain_date<='$this->date_to'");
        if(!empty($this->date_from) && !empty($this->date_to))
                $criteria->addBetweenCondition("t.maintain_date",$this->date_from,$this->date_to); 				
                
                
		$criteria->compare('t.is_new_customer',$this->is_new_customer);
		$criteria->compare('t.user_id_create',$this->user_id_create);
//		$criteria->compare('t.created_date',$this->created_date,true);
		$criteria->compare('t.is_need_maintain_back',$this->is_need_maintain_back);
		$criteria->compare('t.status_maintain_back',$this->status_maintain_back);
		$criteria->compare('t.note_maintain_back',$this->note_maintain_back,true);
		$criteria->compare('t.status_call_bad',$this->status_call_bad);
		$criteria->compare('t.note_call_bad',$this->note_call_bad,true);
		$criteria->compare('t.using_gas_huongminh',$this->using_gas_huongminh);
		$criteria->compare('t.type',TYPE_MARKET_DEVELOPMENT);
        if($cRole==ROLE_MONITORING_MARKET_DEVELOPMENT){
            $criteria->compare('t.monitoring_id',Yii::app()->user->id);
        }else
            $criteria->compare('t.monitoring_id',$this->monitoring_id);
		$criteria->compare('t.province_id',$this->province_id);
		$criteria->compare('t.district_id',$this->district_id);
		$criteria->compare('t.ward_id',$this->ward_id);
        if(count($aWith)>0){
            $criteria->with = $aWith;
            $criteria->together = true;
        }
        $sort = new CSort();
        $sort->attributes = array(
            'maintain_date'=>'maintain_date',
            'created_date'=>'created_date',
            'monitoring_id'=>'monitoring_id',
            'seri_no'=>'seri_no',
            'maintain_employee_id'=>'maintain_employee_id',
//                    'note'=>'note',
            'status'=>'status',
//                    'customer_id'=>array(
//                            'asc'=>'customer.first_name',
//                            'desc'=>'customer.first_name DESC',
//                    ),         

        );    
        $sort->defaultOrder = 't.id DESC';  		
		
		return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>array(
                        'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                    ),
                    'sort' => $sort,	
		));
	}

	public function defaultScope()
	{
		return array(
			//'condition'=>'',
		);
	}
	
        public function beforeValidate() {
            $this->seri_no = trim($this->seri_no);			
            return parent::beforeValidate();
        }
        
        public function beforeSave() {
            if($this->isNewRecord){
                $this->user_id_create = Yii::app()->user->id;
                $this->agent_id = MyFormat::getAgentId();
                $this->code_no = MyFunctionCustom::getNextId('GasMaintain', 'PT_', LENGTH_ORDER,'code_no');
            }
            if(strpos($this->maintain_date, '/')){
                $this->maintain_date =  MyFormat::dateConverDmyToYmd($this->maintain_date);
                MyFormat::isValidDate($this->maintain_date);
            }
            GasMaintainSell::setAddressForModel($this);
            
            $this->note = InputHelper::removeScriptTag($this->note);
            $this->note_update_status = InputHelper::removeScriptTag($this->note_update_status);
            $this->note_maintain_back = InputHelper::removeScriptTag($this->note_maintain_back);
            $this->note_call_bad = InputHelper::removeScriptTag($this->note_call_bad);
            return parent::beforeSave();
        }
        
        public function beforeDelete() {
            return parent::beforeDelete();
        }
 	
}