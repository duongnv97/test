<?php

/**
 * This is the model class for table "{{_gas_price}}".
 *
 * The followings are the available columns in table '{{_gas_price}}':
 * @property string $id
 * @property integer $type_customer
 * @property integer $c_month
 * @property integer $c_year
 * @property string $c3
 * @property string $c4
 * @property string $exchange
 * @property string $cp
 * @property string $premium
 * @property string $tax_import
 * @property string $cost_production
 * @property string $cost_manage
 * @property string $cost_interest
 * @property string $cost_logistic 
 * @property string $json_price
 * @property string $note
 * @property string $status
 */
class GasPrice extends CActiveRecord
{
    public $VAT = 10;
    public $g10_hcm; // giá G10 tp hcm
    public $g10_tinh;// Giá G10 tỉnh
    public $change_g10_hcm; // khoảng tăng giảm giá G10 tp hcm
    public $change_g10_tinh;// khoảng tăng giảm Giá G10 tỉnh
    
    const STATUS_NEW = 1;
    const STATUS_APPROVED = 2;
    const STATUS_REFIX = 3;
    const STATUS_REFIX_OK = 4;
    
    const PLUS_VND = 5000;
    
    public $prevMonthPrice12; // Giá bình 12 của tháng trước
    
    public function getArrayStatus() {
        return array(
            GasPrice::STATUS_NEW => 'Mới Tạo',
            GasPrice::STATUS_APPROVED => 'Duyệt',
            GasPrice::STATUS_REFIX => 'Yêu Cầu Sửa Lại',
            GasPrice::STATUS_REFIX_OK => 'Đã Sửa Lại',
        );
    }
    
    // May 14, 2016 mã price KH bò TP HCM
    public function getCodePriceBo() {
        return array(
            'G0'=>'G0',
            'G1'=>'G1',
            'G2'=>'G2',
            'G3'=>'G3',
            'G4'=>'G4',
            'G5'=>'G5',
            'G6'=>'G6',
            'G7'=>'G7',
            'G8'=>'G8',
            'G9'=>'G9',
            'G10'=>'G10',
            'NPP'=>'NPP',
            'DB1'=>'ĐB1',
            'DB2'=>'ĐB2',
            'DB3'=>'ĐB3',
            'DB4'=>'ĐB4',
            'DB5'=>'ĐB5',
            'DB6'=>'ĐB6',
        );
    }
    
    // May 14, 2016 mã price KH mối TP HCM
    public function getCodePriceMoiHcm() {
        return array(
            'MG0'=>'MG0',
            'MG1'=>'MG1',
            'MG2'=>'MG2',
            'MG3'=>'MG3',
            'MG4'=>'MG4',
            'MG5'=>'MG5',
            'MG6'=>'MG6',
            'MG7'=>'MG7',
            'MG8'=>'MG8',
            'MG9'=>'MG9',
            'MG10'=>'MG10',
        );
    }
    
    // May 14, 2016 mã price KH mối Mien Dong
    public function getCodePriceMoiTinh() {
        return array(
            'GT0'=>'GT0',
            'GT1'=>'GT1',
            'GT2'=>'GT2',
            'GT3'=>'GT3',
            'GT4'=>'GT4',
            'GT5'=>'GT5',
            'GT6'=>'GT6',
            'GT7'=>'GT7',
            'GT8'=>'GT8',
            'GT9'=>'GT9',
            'GT10'=>'GT10',
        );
    }
    
    // May 14, 2016 mã price KH mối Mien Tay
    public function getCodePriceMoiMienTay() {
        return array();// không làm
        return array(
            'GMT0'=>'GMT0',
            'GMT1'=>'GMT1',
            'GMT2'=>'GMT2',
            'GMT3'=>'GMT3',
            'GMT4'=>'GMT4',
            'GMT5'=>'GMT5',
            'GMT6'=>'GMT6',
            'GMT7'=>'GMT7',
            'GMT8'=>'GMT8',
            'GMT9'=>'GMT9',
            'GMT10'=>'GMT10',
        );
    }
    
    // May 14, 2016 mong muốn Lợi nhuận (usd/tan)
    public function getInterest() {
        return array(
            'G0'=>'220',
            'G1'=>'150',
            'G2'=>'140',
            'G3'=>'130',
            'G4'=>'120',
            'G5'=>'110',
            'G6'=>'100',
            'G7'=>'90',
            'G8'=>'80',
            'G9'=>'70',
            'G10'=>'60',
            'NPP'=>'50',
            'DB1'=>'250',
            'DB2'=>'280',
            'DB3'=>'300',
            'DB4'=>'350',
            'DB5'=>'400',
            'DB6'=>'500',
        );
    }
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasPrice the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_price}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('type_customer, c_month, c_year, c3, c4, exchange, cp, premium, tax_import, cost_production, cost_manage, cost_interest, cost_logistic', 'required', 'on'=>'create1, update1'),
            array('g10_hcm, g10_tinh', 'required', 'on'=>'create2, update2'),
            array('type_customer', 'required'),
            array('note, status, change_g10_hcm, change_g10_tinh, g10_hcm, g10_tinh, id, type_customer, c_month, c_year, c3, c4, exchange, cp, premium, tax_import, cost_production, cost_manage, cost_interest, cost_logistic, json_price', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'type_customer' => 'Loại',
            'c_month' => 'Tháng',
            'c_year' => 'Năm',
            'c3' => 'C3',
            'c4' => 'C4',
            'exchange' => 'Tỉ giá',
            'cp' => 'Cp',
            'premium' => 'Premium',
            'tax_import' => 'Thuế nhập khẩu',
            'cost_production' => 'Phí hoạt động sản xuất',
            'cost_manage' => 'Phí quản lý',
            'cost_interest' => 'Phí lãi vay ',
            'cost_logistic' => 'Phí vận chuyển; cầu đường…',
            'json_price' => 'Json Price',
            'g10_hcm' => 'Giá G10 TPHCM',
            'g10_tinh' => 'Giá G10 Tỉnh',
            'change_g10_hcm' => 'Khoảng tăng giảm',
            'change_g10_tinh' => 'Khoảng tăng giảm của Tỉnh',
            'status' => 'Trạng thái',
            'note' => 'Ghi chú',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $aRoleViewAll   = [ROLE_CHIEF_ACCOUNTANT, ROLE_ADMIN, ROLE_HEAD_GAS_BO, ROLE_DIRECTOR_BUSSINESS, ROLE_DIRECTOR, ROLE_RECEPTION];
        $aUidViewAll    = [GasConst::UID_CHIEN_BQ, GasConst::UID_NGUYEN_DTM, GasConst::UID_CHAU_LNM, GasConst::UID_NHI_DT, GasLeave::UID_VIEW_ALL_TAY_NGUYEN_GIA_LAI];
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        
        $criteria=new CDbCriteria;
        if(!in_array($cRole, $aRoleViewAll) && !in_array($cUid, $aUidViewAll)){
            $criteria->compare('t.c_month', date('m'));
            $criteria->compare('t.c_year', date('Y'));
        }
        $criteria->compare('t.type_customer',$this->type_customer);
        $criteria->compare('t.c_month',$this->c_month);
        $criteria->compare('t.c_year',$this->c_year);
        $criteria->order = 't.c_year DESC, t.c_month DESC';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /**
     * @Author: ANH DUNG May 14, 2016
     */
    public function resetScenarioCreate() {
        $this->scenario = "create".$this->type_customer;
    }
    public function resetScenarioUpdate() {
        $this->scenario = 'update'.$this->type_customer;
    }
    
    public function canCreate() {
        $criteria = new CDbCriteria();
        $criteria->compare('t.c_month', $this->c_month);
        $criteria->compare('t.c_year', $this->c_year);
        $criteria->compare('t.type_customer', $this->type_customer);
        $model = self::model()->find($criteria);
        if($model){
            return false;
        }
        return true;
    }
    
    
    public function canUpdate() {
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole == ROLE_ADMIN){
            return true;// only for dev
        }
        $cHours = date('H');
        if($this->c_month <= date('m')){
            return false;
        }

        $aRoleAllow = array(ROLE_ADMIN, ROLE_DIRECTOR_BUSSINESS);
        if(!in_array($cRole, $aRoleAllow) || $this->c_year != date('Y')
                || date('d') != date('t')
        ){
            return false;
        }
//        if($this->status == GasPrice::STATUS_APPROVED){
//            return false;
//        }
        return true;
    }
    
    public function canDelete() {
        if(Yii::app()->params['enable_delete'] == 'no'){
            return false;
        }
        return true;
    }
    
    public function getStatus() {
        $aStatus = $this->getArrayStatus();
        $note = '';
        if(trim($this->note) != ''){
            $note = "<p><span class='item_b'>Ghi Chú:</span> $this->note</p>";
        }
        return $aStatus[$this->status].$note;
    }
    
    /**
     * @Author: ANH DUNG May 14, 2016
     */
    public function getArrTypeCustomer() {
        $aType = CmsFormatter::$CUSTOMER_BO_MOI;
        unset($aType[STORE_CARD_XE_RAO]);
        return $aType;
    }
    
    /**
     * @Author: ANH DUNG May 19, 2016
     * @Todo: format for number decimal at view input
     * c3, c4, exchange, cp, premium, tax_import, cost_production, cost_manage, cost_interest, cost_logistic', 'required', 'on'=>'create, update'),
     */
    public function formatInputNumber() {
        $sField = "c3,c4,exchange,cp,premium,tax_import,cost_production,cost_manage,cost_interest,cost_logistic";
        $aField = explode(",", $sField);
        foreach($aField as $field_name) {
            $this->$field_name = ActiveRecord::formatNumberInput($this->$field_name);
        }
        if($this->type_customer == STORE_CARD_KH_MOI){
            $this->g10_hcm = $this->getInfoFromJson('g10_hcm');
            $this->g10_tinh = $this->getInfoFromJson('g10_tinh');
//            $this->change_g10_hcm = $this->getInfoFromJson('change_g10_hcm');
//            $this->change_g10_tinh = $this->getInfoFromJson('change_g10_tinh');
        }
    }

    /**
     * @Author: ANH DUNG May 14, 2016
     * @Todo: save cong thuc gas Bo
     * code, 
     * price_moi, giá kh mối 
     * price_sell_before_tax_vnd, Giá bán trước thuế (vnd/kg)
     * price_sell_before_tax_usd, Giá bán trước thuế (usd/tan)
     * price_COGS_before_tax_usd, Giá vốn trước thuế (usd/tan)
     * interest, Lợi nhuận  (usd/tan)
     * vat, V
     * price_sell_after_tax_vnd, Giá bán sau thuế (vnd/kg)
     * price_sell_after_tax_b45 Giá bán sau thuế (vnd/bình 45kg)
     */
    public function buildJsonBo() {
        if($this->type_customer == STORE_CARD_KH_BINH_BO){
            $aRes = array();
            $interest = $this->getInterest();
            $this->premium = round($this->premium, 2);
            $this->tax_import = round($this->tax_import, 1);
            foreach($this->getCodePriceBo() as $code=>$text) {
                // lợi nhuận
                $temp['interest'] = $interest[$code];
                 // price_COGS_before_tax_usd Giá vốn trước thuế (usd/tan)
                $temp['price_COGS_before_tax_usd'] = $this->cp + $this->premium + $this->tax_import + $this->cost_production + $this->cost_manage + $this->cost_interest + $this->cost_logistic; 
                $temp['price_sell_before_tax_usd'] = $temp['interest'] + $temp['price_COGS_before_tax_usd'];
//                $temp['price_sell_before_tax_vnd'] = round(($temp['price_sell_before_tax_usd'] * $this->exchange)/1000) + 1; // chỉnh lệch 1 đồng
                $temp['price_sell_before_tax_vnd'] = round(($temp['price_sell_before_tax_usd'] * $this->exchange)/1000); // chỉnh lệch 1 đồng
                $temp['vat'] = $this->VAT;
                $vat = ($temp['price_sell_before_tax_vnd']*10)/100;
                $temp['price_sell_after_tax_vnd'] = round($temp['price_sell_before_tax_vnd'] + $vat);
                $temp['price_sell_after_tax_b45'] = round($temp['price_sell_after_tax_vnd']*45, -3);// Giống round excel 
                $aRes['code'][$code] = $temp;
            }
            $this->json_price = json_encode($aRes);
        }
    }

    /**
     * @Author: ANH DUNG May 14, 2016
     * @Todo: save cong thuc gas Moi
     * code, 
     */
    public function buildJsonMoi() {
        if($this->type_customer == STORE_CARD_KH_MOI){
            $aRes = array();
            $aRes['info']['g10_hcm'] = $this->g10_hcm;
            $aRes['info']['g10_tinh'] = $this->g10_tinh;
//            $aRes['info']['change_g10_hcm'] = $this->change_g10_hcm;
//            $aRes['info']['change_g10_tinh'] = $this->change_g10_tinh;
            $aReverse = array_reverse($this->getCodePriceMoiHcm());
            $price = $this->g10_hcm;
            foreach($aReverse as $code=>$text) {
                $aRes['code'][$code] = $price;
//                $price += $this->change_g10_hcm;
                $price += GasPrice::PLUS_VND;
            }
            
            $aReverse = array_reverse($this->getCodePriceMoiTinh());
            $price = $this->g10_tinh;
            foreach($aReverse as $code=>$text) {
                $aRes['code'][$code] = $price;
//                $price += $this->change_g10_tinh;
                $price += GasPrice::PLUS_VND;
            }
            $this->json_price = json_encode($aRes);
        }
    }
    
    /**
     * @Author: ANH DUNG May 14, 2016
     * @Todo: get info of json array by param
     * @Param: $paramName
     */
    public function getInfoFromJson($paramName) {
        $aParam = json_decode($this->json_price, true);
        if(isset($aParam['info'][$paramName])){
            return $aParam['info'][$paramName];
        }
        return "";
    }
    
    /**
     * @Author: ANH DUNG May 14, 2016
     * @Todo: get code array: array(G0=>142)
     */
    public function getArrayCodeFromJson() {
        $aParam = json_decode($this->json_price, true);
        return $aParam['code'];
    }
    
    /**
     * @Author: ANH DUNG Jan 31, 2017
     * @Todo: tính toán tháng và năm load default lúc tạo
     */
    public function calcMonthDefault() {
        $this->c_month  = date('m');
        $this->c_year   = date('Y');
        $cMonth         = $this->c_year.'-'.  $this->c_month.'-01';
        $nextMonthTmp   = MyFormat::modifyDays($cMonth, 1, '+', 'month');
        $this->c_month  = MyFormat::dateConverYmdToDmy($nextMonthTmp, 'm');
        $this->c_year   = MyFormat::dateConverYmdToDmy($nextMonthTmp, 'Y');
    }
    /**
     * @Author: ANH DUNG May 15, 2016
     * @Todo: load config của prev month cho Bò
     */
    public function loadPrevMonth() {
        $mPrev = $this->loadPrevMonthGetModel();
        if($mPrev){
            $this->exchange = $mPrev->exchange;
            $this->c3 = $mPrev->c3;
            $this->c4 = $mPrev->c4;
            $this->cp = $mPrev->cp;
            $this->premium = $mPrev->premium;
            $this->tax_import = $mPrev->tax_import;
            $this->cost_production = $mPrev->cost_production;
            $this->cost_manage = $mPrev->cost_manage;
            $this->cost_interest = $mPrev->cost_interest;
            $this->cost_logistic = $mPrev->cost_logistic;
        }
    }
    /**
     * @Author: ANH DUNG Feb 01, 2017
     * @Todo: get model valid for prev month
     */
    public function loadPrevMonthGetModel() {
        $cMonth     = $this->c_year.'-'. $this->c_month.'-01';
        $prevMonthTmp = MyFormat::modifyDays($cMonth, 1, '-', 'month');
        $prevMonth  = MyFormat::dateConverYmdToDmy($prevMonthTmp, 'm');
        $prevYear   = MyFormat::dateConverYmdToDmy($prevMonthTmp, 'Y');
        $mPrev = $this->getByMonthAndType(STORE_CARD_KH_BINH_BO, $prevMonth, $prevYear);
        if(is_null($mPrev)){
            $prevMonthTmp = MyFormat::modifyDays($prevMonthTmp, 1, '-', 'month');
            $prevMonth  = MyFormat::dateConverYmdToDmy($prevMonthTmp, 'm');
            $prevYear   = MyFormat::dateConverYmdToDmy($prevMonthTmp, 'Y');
            $mPrev = $this->getByMonthAndType(STORE_CARD_KH_BINH_BO, $prevMonth, $prevYear);
        }
        return $mPrev;
    }
    
    
    /** @Author: ANH DUNG May 14, 2016 */
    public function getByMonthAndType($type_customer, $month, $year) {
        $criteria = new CDbCriteria();
        $criteria->compare("t.c_month", ($month*1));
        $criteria->compare("t.c_year", $year);
        $criteria->compare("t.type_customer", $type_customer);
        $criteria->order = "t.id DESC";
        return self::model()->find($criteria);
    }
    
    /**
     * @Author: ANH DUNG May 14, 2016
     * @Todo: get price g10 tháng trước của mối
     */
    public function getPriceG10() {
        if(isset($_GET['price_g10'])){
            $g10_hcm = "228000";
            $g10_tinh = "223000";
            $date = $_GET['c_year']."-".$_GET['c_month']."-01";
            $prevMonthDate = MyFormat::modifyDays($date, 1, "-", 'month');
            $prevMonth = MyFormat::dateConverYmdToDmy($prevMonthDate, 'm')*1;
            $prevYear = MyFormat::dateConverYmdToDmy($prevMonthDate, 'Y');
            $model = $this->getByMonthAndType(STORE_CARD_KH_MOI, $prevMonth, $prevYear);
            if($model){
                // get price G10 here
                $g10_hcm = $model->getInfoFromJson('g10_hcm');
                $g10_tinh = $model->getInfoFromJson('g10_tinh');
            }
            $json = array('g10_hcm'=>$g10_hcm, "g10_tinh"=>$g10_tinh);
            echo CJavaScript::jsonEncode($json);die; 
        }
    }
    
    /**
     * @Author: ANH DUNG May 14, 2016
     * @Todo: render html view
     */
    public function getHtmlView() {
        $aCodeValue = $this->getArrayCodeFromJson();
        $str = "";
        if($this->type_customer == STORE_CARD_KH_MOI){
            $str = $this->getHtmlViewMoi($aCodeValue);
        }else{
            $this->getHtmlViewBo($str, $aCodeValue);
        }
        return $str;
    }
    
    public function getHtmlViewBo(&$str, $aCodeValue) {
//        return print_r($aCodeValue);
        $str .= "<table cellpadding='0' cellspacing='0' class=' ' style='width:100%;'>";
        
        $str .= "<thead>";
        $str .= "<tr><td class='item_b item_c' colspan='5'>Gas Bò Tháng $this->c_month - $this->c_year</td></tr>";
        $str .= "<tr><td colspan='5'>{$this->getHeadViewBo()}</td></tr>";
        $str .= "</thead>";
        $str .= "<tbody>";
        
        $str .= "<tr>";
        $str .= "<td class='w-50 item_c item_b'>Nhóm</td>";
        $str .= "<td class='w-150 item_c item_b'>Giá Bán Trước Thuế</td>";
        $str .= "<td class='w-150 item_c item_b'>VAT</td>";
        $str .= "<td class='w-150 item_c item_b'>Giá Bán Sau Thuế</td>";
        $str .= "<td class='w-150 item_c item_b'>Giá Bán Sau Thuế Bình 45</td>";
        $str .= "</tr>";
        
        foreach($this->getCodePriceBo() as $code=>$v) {
            if(!isset($aCodeValue[$code])) continue;
            $str .= "<tr>";
            $str .= "<td class='item_c'>$code</td>";
            $str .= "<td class='item_r'>".ActiveRecord::formatCurrency($aCodeValue[$code]['price_sell_before_tax_vnd'])."</td>";
            $str .= "<td class='item_c'>{$aCodeValue[$code]['vat']} %</td>";
            $str .= "<td class='item_r'>".ActiveRecord::formatCurrency($aCodeValue[$code]['price_sell_after_tax_vnd'])."</td>";
            $str .= "<td class='item_r'>".ActiveRecord::formatCurrency($aCodeValue[$code]['price_sell_after_tax_b45'])."</td>";
            $str .= "</tr>";
        }
        $str .= "<tbody>";
        $str .= "</table>";
        
    }
    
    public function getHeadViewBo() {
        $detail = "";
        $detail .= "<table  cellpadding='0' cellspacing='0' class='' style='width:100%;'> ";        
//        $str .= "<tr><td></td></tr>";
        
        $detail .= "<tr>";
        $detail .= "<td><p class='w-50'>C3</p>".ActiveRecord::formatCurrency($this->c3)."</td>";
        $detail .= "<td><p class='w-50'>Tỉ giá</p>".ActiveRecord::formatCurrency($this->exchange)."</td>";
        $detail .= "<td><p class=''>Premium</p>".ActiveRecord::formatCurrency($this->premium)."</td>";
        $detail .= "<td><p class=''>Thuế nhập khẩu</p>".ActiveRecord::formatCurrency($this->tax_import)."</td>";
        $detail .= "<td><p class=''>Phí hoạt động sản xuất</p>".ActiveRecord::formatCurrency($this->cost_production)."</td>";
        $detail .= "</tr>";
        
        $detail .= "<tr>";
        $detail .= "<td><p class='w-50'>C4</p>".ActiveRecord::formatCurrency($this->c4)."</td>";
        $detail .= "<td><p class='w-50'>CP</p>".ActiveRecord::formatCurrency($this->cp)."</td>";
        $detail .= "<td><p class=''>Phí quản lý</p>".ActiveRecord::formatCurrency($this->cost_manage)."</td>";
        $detail .= "<td><p class=''>Phí lãi vay</p>".ActiveRecord::formatCurrency($this->cost_interest)."</td>";
        $detail .= "<td><p class=''>Phí vận chuyển, cầu đường</p>".ActiveRecord::formatCurrency($this->cost_logistic)."</td>";
        $detail .= "</tr>";
        
        $detail .= "</table>";
        return $detail;
    }
    
    public function getHtmlViewMoi($aCodeValue) {
        $detail = "<table cellpadding='0' cellspacing='0' class=' ' style='width:100%;'>";
        $detail .= "<thead>";
        $detail .= "<tr><td class='item_b item_c box_350'>Gas Mối Tháng $this->c_month - $this->c_year</td></tr>";
        $detail .= "</thead>";
        $detail .= "<tbody>";
        $detail .= "<tr><td>{$this->getHtmlViewMoiTable($aCodeValue)}</td></tr>";
        $detail .= "</tbody>";
        $detail .= "</table>";
        return $detail;
    }
    
    public function getHtmlViewMoiTable($aCodeValue) {
        $detail = "<div class='box_350'>";
        $detail .= "<table  cellpadding='0' cellspacing='0' class='' style='width:100%;'> ";
        $detail .= "<tr><td class=' item_c' colspan='2'>Khu Vực Tp.Hồ Chí Minh</td></tr>";
        foreach($this->getCodePriceMoiHcm() as $code=>$v) {
            if(!isset($aCodeValue[$code])) continue;
            $detail .= "<tr>";
            $detail .= "<td class='item_c'>$code</td>";
            $detail .= "<td class='w-100 item_r'>".ActiveRecord::formatCurrency($aCodeValue[$code])."</td>";
            $detail .= "</tr>";
        }
        $detail .= "</table>";
        $detail .= "</div>";
        
        $detail .= "<div class='box_350'>";
        $detail .= "<table  cellpadding='0' cellspacing='0' class='' style='width:100%;'> ";
        $detail .= "<tr><td class=' item_c' colspan='2'>Khu Vực Tỉnh</td></tr>";
        foreach($this->getCodePriceMoiTinh() as $code=>$v) {
            if(!isset($aCodeValue[$code])) continue;
            $detail .= "<tr>";
            $detail .= "<td class='item_c'>$code</td>";
            $detail .= "<td class='w-100 item_r'>".ActiveRecord::formatCurrency($aCodeValue[$code])."</td>";
            $detail .= "</tr>";
        }
        $detail .= "</table>";
        $detail .= "</div>";
        return $detail;
    }
    
    /**
     * @Author: ANH DUNG Jul 03, 2016
     * @Todo: get array price g value by month year
     * @return: array(
            [G0] => 20342
            [G1] => 18610)
     * 
     */
    public function getArrayCodeGByMonthYear($month, $year) {
        $criteria = new CDbCriteria();
        $month = $month*1;
        $criteria->compare("t.c_month", $month);
        $criteria->compare("t.c_year", $year);
        $models = self::model()->findAll($criteria);
        $aRes = array();
        foreach($models as $item){
            $temp = json_decode($item->json_price, true);
            if($item->type_customer == STORE_CARD_KH_BINH_BO){
                foreach($temp['code'] as $code=>$info){
                    $aRes[$code] = $info['price_sell_after_tax_vnd'];
                }
            }elseif($item->type_customer == STORE_CARD_KH_MOI){
                foreach($temp['code'] as $code=>$info){
                    $aRes[$code] = $info;
                }
            }
        }
        return $aRes;
    }

    /**
     * @Author: ANH DUNG Jul 03, 2016
     * @Todo: get value money of one code
     * @param: $g_code: G0, G1, G2
     */
    public static function getPriceOfCode($g_code, $month, $year) {
        $mGasPrice =new GasPrice();
        $aPriceCode = $mGasPrice->getArrayCodeGByMonthYear($month, $year);
        return isset($aPriceCode[$g_code]) ? $aPriceCode[$g_code]:0;
    }
    /**
     * @Author: ANH DUNG Feb 31, 2017
     * @Todo: get content send sms cho cấp quản lý check lại giá Bò mối khi setup
     */
    public function notifyGasPriceSetupMsg() {
        // Template: gia gas 02-2017\nBB G10 23,126. Tang 2,338\nB12 MG10: 296,000. Tang 28,000\n Neuco sai sot bao ngay cho GD Hoan
        $gSetup = $this->getArrayCodeGByMonthYear($this->c_month, $this->c_year);
        if(!isset($gSetup['G10']) || !isset($gSetup['MG10'])){
            return '';
        }
        $prevMonth = $prevYear = $amountChangeOfKg = $amountChangeOfBinh12 = 0;
        UsersPrice::getPrevMonthYear($this->c_month, $this->c_year, $prevMonth, $prevYear);
        // Lấy GSetup của tháng trước để so sánh GSetup tháng hiện tại  cộng hoặc trừ cho giá khác không tự động tăng theo G
        $gSetupPrev = $this->getArrayCodeGByMonthYear($prevMonth, $prevYear);
        UsersPrice::calcAmountChangeEachG($gSetup, $gSetupPrev, $amountChangeOfKg, $amountChangeOfBinh12);
        $this->prevMonthPrice12 = $amountChangeOfBinh12;
        $textBB = $textB12 = 'Tang';
        if($amountChangeOfKg < 0){
            $textBB = 'Giam';
        }
        if($amountChangeOfBinh12 < 0){
            $textB12 = 'Giam';
        }
        $msg = "NOI BO\n";
        $msg .= "Gia Gas T$this->c_month-$this->c_year";
        $msg .= "\nBB G10: ".ActiveRecord::formatCurrency($gSetup['G10']).". $textBB ".ActiveRecord::formatCurrency($amountChangeOfKg);
        $msg .= "\nB12 MG10: ".ActiveRecord::formatCurrency($gSetup['MG10']).". $textB12 ".ActiveRecord::formatCurrency($amountChangeOfBinh12);
        $msg .= "\nNeu co sai sot bao ngay cho GD Hoan!";
        return $msg;
    }
    
    
    /** @Author: ANH DUNG Aug 01, 2018
     *  @Todo: cảnh báo nếu chưa setup giá CP ngày cuối tháng
     * 31 20 * * *   root  /usr/bin/php /var/www/spj.daukhimiennam.com/web/cron.php AlertSetupCp &> /dev/null
     **/
    public function alertSetupCp($cDate = '') {
        try{
        if(date('t') != date('d')){// chỉ chạy ngày cuối tháng
            return ;
        }
        if(empty($cDate)){
            $cDate = date('Y-m').'-01';
        }
        $dayNextMonth  = MyFormat::modifyDays($cDate, 1, '+', 'month');
        $tempMonth  = explode('-', $dayNextMonth);
        $nextMonth  = $tempMonth[1];
        $nextYear   = $tempMonth[0];
        
        $gSetup = $this->getArrayCodeGByMonthYear($nextMonth, $nextYear);
        if(isset($gSetup['G10']) || isset($gSetup['MG10'])){
            return ;
        }
        
        $title = "[CANH BAO]";
        $title .= "\nChua setup gia CP $nextMonth/$nextYear";
        $title .= "\nLien he GD Hoan 0983075779 de nhac nho";
        
        $title = MyFunctionCustom::remove_vietnamese_accents($title);
        $title = MyFunctionCustom::ShortenStringSMS($title, GasScheduleSms::WIDTH_SMS);
        
        $aUid = [GasLeave::UID_DIRECTOR_SMS_VINAPHONE, GasLeave::UID_DIRECTOR_BUSSINESS, GasLeave::UID_HEAD_GAS_BO,
            GasLeave::UID_DUNG_NT, GasLeave::UID_HEAD_OF_LEGAL];
//        $aUid = [GasLeave::UID_DUNG_NT];// Jul 26, 2016 send cho Dũng để check SMS send
        foreach($aUid as $user_id){
            $json_var       = [];
            $phone = UsersPhone::getPhoneUser($user_id);
            $schedule_sms_id = GasScheduleSms::InsertRecord(GasConst::UID_ADMIN, 
                    $user_id, $phone, GasScheduleSms::TYPE_NOTIFY_G_PRICE_SETUP, GasScheduleSms::ContentTypeKhongDau, 0, '', $title, $json_var);
        }
        
        } catch (Exception $ex) {
            GasCheck::CatchAllExeptiong($ex);
        }
    }
    
}