<?php

/**
 * This is the model class for table "{{_gas_pttt_daily_goback}}".
 *
 * The followings are the available columns in table '{{_gas_pttt_daily_goback}}':
 * @property string $id
 * @property string $code_no
 * @property string $agent_id
 * @property string $uid_login
 * @property string $maintain_date
 * @property string $monitoring_id
 * @property string $maintain_employee_id
 * @property string $note
 * @property string $created_date
 * @property string $last_update_by
 * @property string $last_update_time
 */
class GasPtttDailyGoback extends BaseSpj
{
    public $zone_id = 0, $province_id, $aMaintainEmployee = array();
    public $aQty = array(); // mang id employee => qty
    public $MAX_ID, $date_from_ymd, $date_to_ymd;
    public $date_from, $date_to, $ext_maintain_employee_id, $autocomplete_name;
    const TARGET_CCS = 60;
    
    // lấy số ngày cho phép đại lý cập nhật
    public static function getDayAllowUpdate(){
        return Yii::app()->params['days_update_PTTT_daily_goback'];
    }
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasPtttDailyGoback the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_pttt_daily_goback}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('agent_id, maintain_date, monitoring_id', 'required', 'on'=>'create,update'),
            array('monitoring_id', 'ValidateQty', 'on'=>'create,update'),
            array('note', 'length', 'max'=>500),
            array('id, code_no, agent_id, uid_login, maintain_date, monitoring_id, note, created_date, last_update_by, last_update_time', 'safe'),
            array('date_from,date_to', 'safe'),
            array('zone_id, province_id, ext_maintain_employee_id', 'safe'),
        );
    }

    
    public function ValidateQty($attribute,$params)
    {
        if( count($this->aQty) < 1 )
            $this->addError('monitoring_id','Chưa nhập số lượng bình.');
    }
    
    protected function beforeValidate() {
        $this->aQty = array();
        if(isset($_POST['employee_id'])){
            foreach($_POST['employee_id'] as $employee_id => $qty){
                $employee_name = isset($_POST['employee_name'][$employee_id])?$_POST['employee_name'][$employee_id]:'';
                $this->aMaintainEmployee[$employee_id] = $employee_name;
                if( !empty($qty) && is_numeric($qty)){
                    $this->aQty[$employee_id] = $qty;
                }
            }
        }
        return parent::beforeValidate();
    }
        
    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rDetail' => array(self::HAS_MANY, 'GasPtttDailyGobackDetail', 'pttt_daily_goback_id'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rMonitoring' => array(self::BELONGS_TO, 'Users', 'monitoring_id'),
            'rLastUpdateBy' => array(self::BELONGS_TO, 'Users', 'last_update_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'code_no' => 'Mã Số',
            'agent_id' => 'Đại Lý',
            'uid_login' => 'Uid Login',
            'maintain_date' => 'Ngày',
            'monitoring_id' => 'Chuyên Viên CCS',            
            'note' => 'Ghi Chú',
            'created_date' => 'Ngày Tạo',
            'last_update_by' => 'Người sửa',
            'last_update_time' => 'Ngày Sửa',
            'date_from' => 'Từ Ngày',
            'date_to' => 'Đến Ngày',
            'ext_maintain_employee_id' => 'Nhân Viên PTTT',
            'province_id' => 'Tỉnh',
            'zone_id' => 'Vùng',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        $criteria=new CDbCriteria;
        $criteria->compare('t.id',$this->id);
        $criteria->compare('t.code_no',$this->code_no,true);
        $criteria->compare('t.agent_id',$this->agent_id);
        if(!empty($this->created_date)){
            $this->created_date = MyFormat::dateDmyToYmdForAllIndexSearch($this->created_date);
            $criteria->compare('t.created_date',$this->created_date,true);
        }
        $date_from = '';
        $date_to = '';
        $aWithRelate = array();
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        }
        if(!empty($date_from) && empty($date_to))
                $criteria->addCondition("t.maintain_date>='$date_from'");
        if(empty($date_from) && !empty($date_to))
                $criteria->addCondition("t.maintain_date<='$date_to'");
        if(!empty($date_from) && !empty($date_to))
                $criteria->addBetweenCondition("t.maintain_date",$date_from,$date_to);
        
        if($cRole == ROLE_SUB_USER_AGENT){
            $criteria->compare('t.agent_id', MyFormat::getAgentId());
        }elseif($cRole == ROLE_MONITORING_MARKET_DEVELOPMENT){
            $criteria->compare('t.monitoring_id', $cUid);
        }else{
            if(!empty($this->agent_id)){
                $criteria->compare('t.agent_id', $this->agent_id);
            }
            GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');
        }
        $criteria->compare('t.monitoring_id',$this->monitoring_id);

        if(trim($this->ext_maintain_employee_id)!=''){
            $aWithRelate[] = 'rDetail';
            $criteria->compare('rDetail.maintain_employee_id', trim($this->ext_maintain_employee_id),true);
        }
        if( count($aWithRelate) ){
            $criteria->with = $aWithRelate;
            $criteria->together = true;
        }
        
        $sort = new CSort();

        $sort->attributes = array(
            'code_no'=>'code_no',
            'agent_id'=>'agent_id',
            'maintain_date'=>'maintain_date',
            'monitoring_id'=>'monitoring_id',
            'created_date'=>'created_date',                
            'last_update_time'=>'last_update_time',                
        );    
        $sort->defaultOrder = 't.id desc';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort' => $sort,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    protected function beforeSave() {
        if(strpos($this->maintain_date, '/')){
            $this->maintain_date = MyFormat::dateConverDmyToYmd($this->maintain_date);
            MyFormat::isValidDate($this->maintain_date);
        }
        if($this->isNewRecord){
            if(empty($this->agent_id)){
                $this->agent_id = MyFormat::getAgentId();
            }
            $this->uid_login = MyFormat::getCurrentUid();
            $this->code_no = MyFunctionCustom::getNextId('GasPtttDailyGoback', 'G'.date('y'), LENGTH_TICKET,'code_no');
        }else{
            $this->last_update_by = Yii::app()->user->id;
            $this->last_update_time = date('Y-m-d H:i:s');
        }
        return parent::beforeSave();
    }
    
    protected function beforeDelete() {
        MyFormat::deleteModelDetailByRootId('GasPtttDailyGobackDetail', $this->id, 'pttt_daily_goback_id');
        return parent::beforeDelete();
    }
    
    /**
     * @Author: ANH DUNG Mar 17, 2015
     * @Todo: thống kê bình quay về hàng ngày theo nhân viên giám sát vs nv pttt
     * @Param: $model GasPtttDailyGoback
     */
    public static function StatisticPttt($model) {
        try {
            $aRes = array();
            if(!MyFormat::validDateInput($model->date_from, '-') || !MyFormat::validDateInput($model->date_to, '-') )
            {
                throw new Exception("Ngày không hợp lệ");
            }
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);
            
            $criteria = new CDBcriteria();
            $criteria->addBetweenCondition("t.maintain_date",$date_from,$date_to);
            if(isset($model->agent_id) && is_array($model->agent_id)){
                $criteria->addInCondition('t.agent_id', $model->agent_id);
            }
            if(!empty($model->monitoring_id)){
                $criteria->compare('t.monitoring_id', $model->monitoring_id);
            }
            if(!empty($model->ext_maintain_employee_id)){
                $criteria->compare('t.maintain_employee_id', $model->ext_maintain_employee_id);
            }
//            $criteria->order = "t.maintain_date ASC"; // chỗ này không cần order vì mình sẽ foreach theo ngày trên giao diện
            $criteria->select = "t.maintain_date,t.agent_id,t.monitoring_id,t.maintain_employee_id, "
                    . "sum(qty) as qty";
            $criteria->group = "t.maintain_date, t.monitoring_id, t.maintain_employee_id"; 
            $mRes = GasPtttDailyGobackDetail::model()->findAll($criteria);
            self::FormatResult($aRes, $mRes);
            if(isset($aRes['UID'])){
                $aRes['USER_MODEL'] = Users::getListOptions($aRes['UID'], array('get_all'=>1));
            }
            $aRes['ARR_DAYS'] = MyFormat::getArrayDay($date_from, $date_to);
            
            return $aRes;
        } catch (Exception $exc) {
            throw new Exception("Yêu cầu không hợp lệ: ".$exc->getMessage());
        }
    }
    
    /**
     * @Author: ANH DUNG Mar 17, 2015
     * @Todo: belong to StatisticPttt
     */
    public static function FormatResult(&$aRes, $mRes) {
        foreach($mRes as $item){
            $aRes['EMPLOYEE'][$item->monitoring_id][$item->maintain_employee_id][$item->maintain_date] = $item->qty;
            $aRes['EMPLOYEE_SORT'][$item->monitoring_id][$item->maintain_employee_id] = $item->qty;
            $aRes['UID'][$item->monitoring_id] = $item->monitoring_id;
            $aRes['UID'][$item->maintain_employee_id] = $item->maintain_employee_id;
            if(!isset($aRes['SUM_MONITOR'][$item->monitoring_id])){
                $aRes['SUM_MONITOR'][$item->monitoring_id] = $item->qty;
            }else{
                $aRes['SUM_MONITOR'][$item->monitoring_id] += $item->qty;
            }
            if(!isset($aRes['SUM_EMPLOYEE'][$item->monitoring_id][$item->maintain_employee_id])){
                $aRes['SUM_EMPLOYEE'][$item->monitoring_id][$item->maintain_employee_id] = $item->qty;
            }else{
                $aRes['SUM_EMPLOYEE'][$item->monitoring_id][$item->maintain_employee_id] += $item->qty;
            }
        }
    }

    /**
     * @Author: ANH DUNG Apr 04, 2015
     * @Todo: 1/ sản lượng PTTT ở bán hàng PTTT 
     * 2/ sản lượng PTTT ở PTTT bình quay về hàng ngày
     * @Param: $className maybe: GasPtttDailyGoback, GasMaintainSell
     * @Param: $field_name_date maybe: maintain_date, date_sell
     * @Param: $field_name_qty maybe: qty, quantity_vo_back
     * $month, $year 
     */
    public static function GetOutputPttt($className, $field_name_date, $field_name_qty, $month, $year) {
        
        $criteria = new CDBcriteria();
        $criteria->compare("month(t.$field_name_date)", $month);
        $criteria->compare("year(t.$field_name_date)", $year);
        $criteria->group = "t.maintain_employee_id";
        $criteria->select = "sum($field_name_qty) as $field_name_qty, t.maintain_employee_id";
        $model_ = call_user_func(array($className, 'model'));
        $models = $model_->findAll($criteria);
        $aRes = array();
        foreach($models as $item){
            $aRes[$item->maintain_employee_id] = $item->$field_name_qty;
        }
        return $aRes;
    }
    
    
    /**
     * @Author: ANH DUNG Jul 08, 2016
     * @Todo: 1/ sản lượng CCS ở PTTT bình quay về hàng ngày
     */
    public static function GetOutputCCS() {
        $date_from  = "2016-07-01";
        $date_to    = "2016-09-01";
        $criteria = new CDBcriteria();
        $criteria->addBetweenCondition("t.maintain_date",$date_from,$date_to); 
        $criteria->group = "month(t.maintain_date), t.maintain_employee_id";
        $criteria->select = "sum(t.qty) as qty, t.maintain_employee_id, month(t.maintain_date) as date_month, year(t.maintain_date) as date_year ";
        $models = GasPtttDailyGobackDetail::model()->findAll($criteria);
        $aRes = array();
        foreach($models as $item){
            $aRes[$item->date_month][$item->maintain_employee_id] = $item->qty;
        }
        return $aRes;
    }

    /**
     * @Author: ANH DUNG Sep 28, 2016
     * @Todo: 1/ sản lượng CCS bán hàng ngày
     */
    public function getOutputCCSNew() {
        $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        $criteria = new CDBcriteria();
        $criteria->addBetweenCondition("t.maintain_date",$date_from, $date_to);
        $criteria->compare("t.agent_id", $this->agent_id);
        $criteria->select = "sum(t.qty) as qty, t.maintain_employee_id, t.monitoring_id, month(t.maintain_date) as date_month, year(t.maintain_date) as date_year ";
        $criteria->group = "month(t.maintain_date), year(t.maintain_date), t.maintain_employee_id";
        $criteria->order = "t.monitoring_id ASC";
        
        $models = GasPtttDailyGobackDetail::model()->findAll($criteria);
        $aRes = array();
        foreach($models as $item){
            $aRes['OUTPUT'][$item->date_year][$item->date_month][$item->monitoring_id][$item->maintain_employee_id] = $item->qty;
            $aRes['CCS_ID'][$item->monitoring_id][$item->maintain_employee_id] = $item->qty;
        }
        return $aRes;
    }
    
    /** @Author: ANH DUNG Sep 28, 2016
     * count số điểm của ccs trong ngày
     */
    public static function ccsCountAll($mUserLogin) {
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.maintain_employee_id=$mUserLogin->id AND t.pttt_daily_goback_id > 5344 ");
        return GasPtttDailyGobackDetail::model()->count($criteria);
    }
    
    /** @Author: ANH DUNG Sep 28, 2016
     * @Todo: get dropdown monitor
     */
    public function getDropdownMonitor() {
        $listData = Users::getSelectByRole(ROLE_MONITORING_MARKET_DEVELOPMENT);
        $listData[GasLeave::UID_DIRECTOR] = "Vũ Thái Long";
        return $listData;
    }
    
    /**
     * @Author: ANH DUNG Jul 15, 2017
     * @Todo: BC sản lượng điểm và bq về CCS
     * 1. tính bình quay về đưa vào list employee ID
     * 2. tính số điểm đi 1 ngày đưa list employee ID
     * 3. sort theo 
     */
    public function getCcsDaily() {
        $this->date_from_ymd    = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        $this->date_to_ymd      = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        $aRes = [];
        $this->getCcsDailySell($aRes);
        $this->getCcsDailyPoint($aRes);
        if(isset($aRes['CCS_ID'])){
            arsort($aRes['CCS_ID']);
        }
        $aRes['ARR_DAYS'] = MyFormat::getArrayDay($this->date_from_ymd, $this->date_to_ymd);
        
        return $aRes;
    }
    
    public function getCcsDailySell(&$aRes) {
        $criteria = new CDBcriteria();
        if(!empty($this->agent_id)){
            $criteria->addCondition('t.agent_id=' . $this->agent_id);
        }
        $criteria->addBetweenCondition('t.date_delivery', $this->date_from_ymd, $this->date_to_ymd);
        $criteria->addCondition('t.type=' . SpjCode::TYPE_PTTT. ' AND t.status='.SpjCode::STATUS_GO_BACK);
        $criteria->select = 'count(t.id) as id, t.user_id, t.date_delivery';
        $criteria->group = 't.date_delivery, t.user_id';
        $models = SpjCode::model()->findAll($criteria);
        foreach($models as $item){
            $aRes['OUTPUT'][$item->user_id][$item->date_delivery] = $item->id;
            $aRes['CCS_ID'][$item->user_id] = 0;
            if(!isset($aRes['OUTPUT_SUM'][$item->user_id])){
                $aRes['OUTPUT_SUM'][$item->user_id] = $item->id;
            }else{
                $aRes['OUTPUT_SUM'][$item->user_id] += $item->id;
            }
        }

    }
    
    /** @Author: ANH DUNG Jul 15, 2017
     * @Todo: lấy số điểm làm việc của ccs từ table User
     */
    public function getCcsDailyPoint(&$aRes) {
        $criteria = new CDBcriteria();
//        $criteria->addBetweenCondition('DATE(t.created_date)', $this->date_from_ymd, $this->date_to_ymd);
        DateHelper::searchBetween($this->date_from_ymd, $this->date_to_ymd, 'created_date_bigint', $criteria, true);
        $criteria->addCondition('t.type=' . CUSTOMER_TYPE_STORE_CARD. ' AND t.role_id='.ROLE_CUSTOMER. ' AND t.is_maintain='.STORE_CARD_HGD_CCS);
        if(!empty($this->agent_id)){
            $criteria->addCondition('t.area_code_id=' . $this->agent_id);
        }
        $criteria->select = 'count(t.id) as id, t.created_by, DATE(t.created_date) as created_date';
        $criteria->group = 'DATE(t.created_date), t.created_by';
        $models = Users::model()->findAll($criteria);
        $aRes['OUTPUT_POINT'] = [];
        foreach($models as $item){
            $aRes['OUTPUT_POINT'][$item->created_by][$item->created_date] = $item->id;
            if(!isset($aRes['CCS_ID'][$item->created_by])){
                $aRes['CCS_ID'][$item->created_by] = $item->id;
            }else{
                $aRes['CCS_ID'][$item->created_by] += $item->id;
            }
        }
        
        foreach($aRes['OUTPUT_POINT'] as $created_by => $aInfo):
            foreach($aInfo as $created_date => $point):
                $plus = 0;
                if($point >= 15){
                    $plus = 0.5;
                }
                if($point >= 30){
                    $plus = 1;
                }
            
                if(!isset($aRes['WORK_DAYS'][$created_by])){
                    $aRes['WORK_DAYS'][$created_by] = $plus;
                }else{
                    $aRes['WORK_DAYS'][$created_by] += $plus;
                }
            endforeach;
        endforeach;

    }
    
    
}