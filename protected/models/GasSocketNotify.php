<?php

/**
 * This is the model class for table "{{_socket_notify}}".
 *
 * The followings are the available columns in table '{{_socket_notify}}':
 * @property string $id
 * @property string $agent_id
 * @property string $user_id
 * @property string $message
 * @property integer $code 
 * @property string $json
 * @property integer $status
 * @property string $obj_id
 * @property string $remote_id
 */
class GasSocketNotify extends BaseSpj
{
    // for status
    const STATUS_UNCOMPLETED    = 0;
    const STATUS_COMPLETED      = 1;
    const STATUS_RECEIVED       = 2;
    
    /** for code
     *  @note: một số event được set đã gửi từ server spj là: CODE_UPHOLD_NEW, CODE_ORDER_BOMOI_NEW => để đảm bảo user nhận đc thì mới không gửi nữa
     *  Những event còn lại bị xóa luôn sau khi gửi.
     * 
     */
    const CODE_STORECARD            = 1;// notify cho đại lý khi có đơn hàng mới
    const CODE_STORECARD_DIEUPHOI   = 2;// notify back lại cho điều phối khi agent xác nhận notify -- đã không sử dụng nữa
    const CODE_APP_ORDER_NEW        = 3;// notify cho Agent khi có đơn hàng App Gas24h mới
    const CODE_SELL_ALERT_KTBH      = 4;// FEB 11, 2017 cảnh báo cho KTBH khi giao nhận không xác nhận đơn hàng hgd sau 5p phút
    const CODE_CALL_EVENT           = 5;// Mar 11, 2017 bắt các event cuộc gọi của South Telecom VOIP
    const CODE_UPHOLD_NEW           = 6;// May 05, 2017 bảo trì mới KH tạo trên app
    const CODE_ORDER_BOMOI_NEW      = 7;// May 05, 2017 đơn hàng bò mối KH tạo trên app
    const CODE_CALL_EVENT_MONITOR   = 8;// Jun 13, 2017 Monitor các event cuộc gọi của South Telecom VOIP
    const CODE_APP_ORDER_CONFIRM    = 9;// Dec 04, 2017 sau khi NV xac nhan app thi remove row o nhung tai khoan khac
    const CODE_ALERT_CALL_CENTER    = 10;// Sep 14, 2018 Alert Anything - những event cảnh báo đến NV CallCenter : chưa xn 5 phút, đơn hàng hủy ...


    public $pageSize = 20;

    public static function getRoleAllowSocket() {
        return array(ROLE_TELESALE, ROLE_ADMIN, ROLE_SUB_USER_AGENT, ROLE_EMPLOYEE_MAINTAIN, ROLE_CALL_CENTER, ROLE_DIEU_PHOI);
    }
    // chỉ cho phép role dạng sub_agent, có nghĩa là có gắn parent_id xác định user đó thuộc đại lý nào
    public static function getRoleUpdateNotify() {
        return array(ROLE_SUB_USER_AGENT, ROLE_EMPLOYEE_MAINTAIN);
    }
    public static function getRoleViewCallCenter() {
        return array(ROLE_TELESALE, ROLE_CALL_CENTER, ROLE_DIEU_PHOI, ROLE_ADMIN);
    }
    public function getArrRolePlayAudio() {
        return [ROLE_CALL_CENTER, ROLE_DIEU_PHOI];
    }
    
    /**
     * @Author: ANH DUNG Feb 11, 2017
     * @Todo: get minute alert sell
     */
    public function getMinuteAlertSell() {
        return 15;// int only
    }
    
    public static function model($className = __CLASS__){
        return parent::model($className);
    }
    /**
     * @return string the associated database table name
     */
    public function tableName(){
        return '{{_socket_notify}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, agent_id, user_id, message, code, status, json', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUser' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'rSender' => array(self::BELONGS_TO, 'Users', 'sender_id'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'agent_id' => 'Agent',
            'user_id' => 'User',
            'message' => 'Message',
            'code' => 'Code',
            'status' => 'Status',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
            'json' => 'Json',
            'remote_id' => '',
            'obj_id' => '',
            'need_sync' => '',
            'sender_id' => '',
            'sender_role_id' => '',
            'role_id' => '',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchDieuPhoi()
    {
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        $criteria = new CDbCriteria;
        if( in_array($cRole, GasSocketNotify::getRoleUpdateNotify()) ){
            $criteria->addCondition('t.agent_id='.MyFormat::getAgentId());
        }else{
            if(!empty($this->agent_id)){
                $criteria->addCondition('t.agent_id='.$this->agent_id);
            }
//            GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');
        }
        $criteria->addCondition("DATE(t.created_date)='" . date('Y-m-d')."'");
        if(is_array($this->code)){
            $sParamsIn = implode(',', $this->code);
            $criteria->addCondition("t.code IN ($sParamsIn)");
        }elseif(!empty($this->code)){
            $criteria->addCondition('t.code='.$this->code);
        }
        $this->handleForRole($cRole, $criteria, $cUid);
//        $criteria->compare('t.user_id', $this->user_id);
//        $criteria->compare('t.status', $this->status);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => $this->pageSize,
//                'pageSize' => 5,
            ),
        ));
    }
    
    public function handleForRole($cRole, &$criteria, $cUid) {
        $mAppCache = new AppCache();
        if($cRole == ROLE_DIEU_PHOI){
            $aIdDieuPhoiBoMoi = $mAppCache->getArrayIdRole(ROLE_DIEU_PHOI);
            if(in_array($cUid, $aIdDieuPhoiBoMoi)){
                $sParamsIn = implode(',', $aIdDieuPhoiBoMoi);
            }elseif(in_array($cUid, GasOrders::$ZONE_MIEN_TAY)){
                $sParamsIn = implode(',', GasOrders::$ZONE_MIEN_TAY);
            }
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.sender_id IN ($sParamsIn) OR t.user_id=$cUid");// điều phối tạo
            }
        }elseif($cRole == ROLE_CALL_CENTER){
            $criteria->addCondition("t.user_id=$cUid");
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 12, 2016
     * @Todo: format array user online, remove empty element
     */
    public static function formatArrayOnlineUser($onlineUser) {
        $aRes = array();
        foreach($onlineUser as $user_id){
            if(!empty($user_id)){
                $aRes[] = $user_id;
            }
        }
        return $aRes;
    }

    public static function getUncompletedNotify($onlineUser = array())
    {
        $onlineUser = self::formatArrayOnlineUser($onlineUser);
        if (count($onlineUser) < 1) {
            return array();
        }
        $criteria = new CDbCriteria;
        $criteria->addCondition("DATE(t.created_date)='".date('Y-m-d')."'");
//        $criteria->order = 'updated_date ASC';
        $criteria->order = 't.count_run ASC';
        if (is_array($onlineUser)) {
            $sParamsIn = implode(',', $onlineUser);
        } else {
            $sParamsIn = $onlineUser;
        }
        $sParamsIn = trim($sParamsIn, ',');
        $criteria->addCondition("t.user_id IN ($sParamsIn) AND t.status=".GasSocketNotify::STATUS_UNCOMPLETED);
        return GasSocketNotify::model()->findAll($criteria);
    }

    public static function markSentNotify($id)
    {
//        return ;// only dev test
        $model = GasSocketNotify::model()->findByPk($id);
        /** @var GasSocketNotify $model */
        if ($model) {
            $model->count_run++;
            $model->status = GasSocketNotify::STATUS_COMPLETED; //Wil close  Oct 18, 2016 không cập nhật status ở đây, mà sẽ có hàm update khi client gửi lên là đã nhận
            $model->update(array('status', 'count_run'));//Wil close 
//            $model->update(array('count_run'));
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 18, 2016
     * @Todo: cập nhật status của notify
     */
    public static function setStatusByPk($id, $status)
    {
        $model = GasSocketNotify::model()->findByPk($id);
        if($model){
            $model->setStatus($status);
        }
    }

    public function beforeSave()
    {
//        if ($this->isNewRecord)// Anh Dung Close Oct 02, 2016
//            $this->created_date = new CDbExpression('NOW()');
//
//        $this->updated_date = new CDbExpression('NOW()');

        return parent::beforeSave();
    }
    
    /**
     * @Author: ANH DUNG Sep 25, 2016
     * @Todo: add new record to db
     * luôn dùng cron để đồng bộ hóa, chứ không put sang server io luôn, vì có thể bị put 2 lần khi vừa tạo xong + cron chạy
     * $json: is array will be encode !important
     * @param: $sender_id: người gửi
     * @param: $userId: người nhận
     * @param: $code: type of notify
     * @param: $message: 
     * @param: $json: some info
     * @param: $needMore: anything
     */
    public static function addMessage($obj_id, $sender_id, $userId, $code, $message, $json, $needMore = array())
    {
        $model              = new GasSocketNotify();
        $model->obj_id      = $obj_id;
        $model->sender_id   = $sender_id;
        $model->user_id     = $userId;
        $model->code        = $code;
        $model->status      = GasSocketNotify::STATUS_UNCOMPLETED;
        $model->message     = $message;
        $model->updated_date = date('Y-m-d H:i:s');
        $model->created_date = $model->updated_date;
        $model->calcTimeForSellAlert();

        /* Now 29, 2016 tạm close lại vì không dùng cho window nữa, nên chưa thấy cần sử dụng
        $mUser              = $model->rUser;
        if($mUser){
            if($code == GasSocketNotify::CODE_STORECARD){
                $model->agent_id        = $userId;
            }
            $model->role_id             = $mUser->role_id;
//            $json['receive_role_name']  = MyFormat::getRoleName($mUser->role_id);// Now 29, 2016 tạm close lại
        }
        
        if(isset($needMore['mSender'])){// mSender là người tạo (người gửi)
            $mUser              = $needMore['mSender'];
        }else{
            $mUser              = $model->rSender;
        }
        
        if($mUser && $code < GasSocketNotify::CODE_APP_ORDER_NEW){
            $model->sender_role_id      = $mUser->role_id;
            $json['sender_role_name']   = Roles::GetRoleNameById($mUser->role_id);
            $json['sender_name']        = $mUser->getFullName();
        }
         */
        
        if(isset($needMore['agent_id'])){
            $model->agent_id            = $needMore['agent_id'];
        }
        $model->json        = MyFormat::jsonEncode($json);
        $aCodeNotSave = [GasSocketNotify::CODE_CALL_EVENT, 
            GasSocketNotify::CODE_ALERT_CALL_CENTER, GasSocketNotify::CODE_CALL_EVENT_MONITOR, 
            GasSocketNotify::CODE_APP_ORDER_NEW, GasSocketNotify::CODE_APP_ORDER_CONFIRM];
        if(in_array($model->code, $aCodeNotSave)){
            // với notify call event sẽ gửi luôn và set need_sync = 0, và có thể không cần save luôn
            $model->need_sync = 0;
            $aReturnModel = [GasSocketNotify::CODE_ALERT_CALL_CENTER, GasSocketNotify::CODE_CALL_EVENT_MONITOR, GasSocketNotify::CODE_APP_ORDER_NEW, GasSocketNotify::CODE_APP_ORDER_CONFIRM];
            if(in_array($model->code, $aReturnModel) ){// $aReturnModel là các sự kiện gửi cho 1 list user trong vòng for, nên sẽ không gửi luôn, ko đi tiếp xuống dưới xu ly
                return $model;
            }
            $mSync = new SyncData(SyncData::SERVER_IO_SOCKET, 'socket/notifyAdd');
            $mSync->notifyAdd($model);
            return ;
        }
        
        $model->save();
        if ($model->hasErrors()) {
            $info = "Insert new socket notify Error ".  json_encode($model->getErrors());
            Logger::WriteLog($info);
            return ;
        }

        $model->remote_id   = $model->id;
        $model->update(array('remote_id'));        
    }
    
    /**
     * @Author: ANH DUNG Feb 11, 2017
     * @Todo: tính toán cộng time cho thời gian alert KTBH của Sell
     * @note: sử dụng biến updated_date để làm time send 
     */
    public function calcTimeForSellAlert() {
        if($this->code == GasSocketNotify::CODE_SELL_ALERT_KTBH){
            $this->updated_date = MyFormat::addDays($this->updated_date, $this->getMinuteAlertSell(), "+", 'minutes', 'Y-m-d H:i:s');
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 25, 2016
     * @Todo: Set status of record
     */
    public function setStatus($status){
        $this->status = $status;
        $this->update(array('status'));
    }
    
    /**
     * @Author: ANH DUNG Oct 02, 2016
     * @Todo: gửi luôn all msg xuống client
     */
    public static function pushToClient() {
        Yii::import('application.commands.*');
        $command = new PushServerCommand();
        $command->run(array('check'));
    }
    
    /**
     * @Author: ANH DUNG Oct 02, 2016
     * @Todo: update notify when user KTBH confirm
     */
    public function updateAgentConfirmOrder($q, $mUserLogin) {
        /* 1. change status
         * 2. write info update to json
         */
        $this->status = GasSocketNotify::STATUS_RECEIVED;
        $json = json_decode($this->json, true);
        $json['received_id']    = $q->received_id;
        $json['received_name']  = $q->received_name;
        $json['note']           = $q->note;
        $this->json             = MyFormat::jsonEncode($json);
        $this->update(array('status','json'));
        $this->makeSocketNotifyToDieuPhoi($q, $json);
    }
    
    /**
     * @Author: ANH DUNG Oct 02, 2016
     * @Todo: gửi lại 1 notify đến điều phối sau khi đại lý xác nhận
     */
    public function makeSocketNotifyToDieuPhoi($q, $jsonOld) {
        $mAgent     = $this->rAgent;
        $message = $q->received_name." xác nhận đặt hàng: {$jsonOld['customer_name']}. $q->note";
        $json = array();
        $json['agent_name']     = $mAgent->getFullName();
//            $json['agent_name']     = 'admin agent';
        $json['customer_name']  = $jsonOld['customer_name'];
        $json['type_customer']  = $jsonOld['type_customer'];
        $json['note']           = $q->note;
        $json['received_id']    = $q->received_id;
        $json['received_name']  = $q->received_name;
        $needMore = array('mSender'=>$mAgent, 'agent_id'=>$mAgent->id);
        GasSocketNotify::addMessage(0, $mAgent->id, $this->sender_id, GasSocketNotify::CODE_STORECARD_DIEUPHOI, $message, $json, $needMore);
    }
    
    public static function cronRemoveNotifyStorecard() {
        try{ 
        $days_check = 1;// Apr 20, 2017 giữ lại notify của ĐP
        $criteria   = new CDbCriteria;
        $aCodeRemove = array(GasSocketNotify::CODE_STORECARD, GasSocketNotify::CODE_STORECARD_DIEUPHOI, GasSocketNotify::CODE_APP_ORDER_NEW);
        $sParamsIn  = implode(',', $aCodeRemove);
//        $criteria->addCondition("code IN ($sParamsIn)");
        $criteria->addCondition("DATE_ADD(created_date,INTERVAL $days_check DAY) < NOW()");
        GasSocketNotify::model()->deleteAll($criteria);// Dec 12, 2016 xóa luôn, ko xóa theo model nữa

        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 25, 2016
     * @Todo: xóa những token login mà không active trong vòng 5 ngày hoặc last_active NULL
     */
    public static function cronRemoveNotifyCall() {
        try{
        // Apr 20, 2017 xóa những record notify của CallCenter sau 2h
        $criteria   = new CDbCriteria;
        $aCodeNotRemove = [GasSocketNotify::CODE_STORECARD, GasSocketNotify::CODE_STORECARD_DIEUPHOI,
            GasSocketNotify::CODE_SELL_ALERT_KTBH
        ];
        $sParamsIn  = implode(',', $aCodeNotRemove);
        $criteria->addCondition("code NOT IN ($sParamsIn)");
        $minute = 5;
        $criteria->addCondition("DATE_ADD(created_date,INTERVAL $minute MINUTE) < NOW()");
        GasSocketNotify::model()->deleteAll($criteria);// Dec 12, 2016 xóa luôn, ko xóa theo model nữa

        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Now 29, 2016
     * @Todo: make record socket notify of app transaction
     * @param: $mTranHistory model TransactionHistory
     * @param: $mUserLogin model customer make order by App
     */
    public static function appTransaction($mTranHistory, $mUserLogin) {
        Logger::WriteLog('BIGGGGG BUG - NEED CLOSE THIS FUNCTION');
        return ;
//        $mAgent         = $mTranHistory->rAgent;
        $message = "App đặt hàng: {$mUserLogin->getFullName()}. $mUserLogin->phone";
        $json = array();
//            $needMore = array('mSender'=>$mUserLogin, 'agent_id'=>$mAgent->id);
        $needMore = array('mSender'=>$mUserLogin);
        GasSocketNotify::addMessage($mTranHistory->id, $mUserLogin->id, $mTranHistory->agent_id, GasSocketNotify::CODE_APP_ORDER_NEW, $message, $json, $needMore);
    }
    
    /**
     * @Author: ANH DUNG Nov 29, 2016
     * @Todo: update clientReceivedNotify, khi client nhận dc notify thì cập nhật lại status cho server io biết đã gửi thành công cho client
     */
    public function updateClientReceivedNotify($server_io_notify_id) {
        $mSync = new SyncData(SyncData::SERVER_IO_SOCKET, 'socket/clientReceivedNotify');
        $params = array('server_io_notify_id' => $server_io_notify_id);
        $mSync->doRequestNormal($params);
    }
    public function getCreatedDate($fomat = 'd/m/Y H:i') {
        return MyFormat::dateConverYmdToDmy($this->created_date, 'd/m/Y H:i:s');
    }
    public function getMessage(){
        return $this->message;
    }
    public function getJsonParam($field_name) {
        $json = json_decode($this->json, true);
        return isset($json[$field_name]) ? $json[$field_name] : '';
    }
    public function getStatusIcon() {
        if($this->status != GasSocketNotify::STATUS_UNCOMPLETED){
            return '';
        }
        $cRole  = MyFormat::getCurrentRoleId();
//        if($cRole != ROLE_SUB_USER_AGENT){

        $aRoleViewOnly = GasSocketNotify::getRoleUpdateNotify();
        $aRoleViewOnly[] = ROLE_CALL_CENTER;
        if( !in_array($cRole, $aRoleViewOnly) ){
            // cho phép giao nhận và KTBH có thể cập nhật notify confirm
            return $this->getImageIconNewNotify();
        }

        $title = 'Click để xác nhận và tắt âm thanh';       
        return '<a class="OrderConfirmRead" title="'.$title.'" href="javascript:;" next="'.$this->getUrlConfirm().'">'.$this->getImageIconNewNotify().'</a>';
    }

    public function getUrlConfirm() {
        if(!empty($this->id)){
            return Yii::app()->createAbsoluteUrl('admin/transaction/index', array('ConfirmReadOrder'=>1, 'id'=> $this->id));
        }else{
            return Yii::app()->createAbsoluteUrl('admin/transaction/index').'?ConfirmReadOrder=1&id=0';
        }
    }
    /**
     * @Author: ANH DUNG Dec 01, 2016
     * @Todo: update notify when user KTBH confirm ở web
     */
    public function handleConfirmReadOrder() { 
        $this->status = GasSocketNotify::STATUS_RECEIVED;
//        $this->updated_date = date('Y-m-d H:i:s');// Feb 11, 2017 bỏ không cập nhật updated_date, vì để sử dụng làm time send
        $this->update(array('status'));
    }
    
    /**
     * @Author: ANH DUNG Feb 11, 2017
     * @Todo: xóa notify by obj_id and code
     * @Note: done test with addCondition với $obj_id + $code không có trong table
     */
    public static function deleteByObjIdAndCode($obj_id, $code) {
        if(empty($obj_id) || empty($code)){
            return ;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('obj_id='.$obj_id.' AND code='.$code );
        GasSocketNotify::model()->deleteAll($criteria);
    }
    
}