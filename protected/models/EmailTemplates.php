<?php

/**
 * This is the model class for table "{{_email_templates}}".
 *
 * The followings are the available columns in table '{{_email_templates}}':
 * @property integer $id
 * @property string $email_subject
 * @property string $email_body
 * @property string $parameter_description
 * @property integer $type
 */
class EmailTemplates extends CActiveRecord
{
    
    /** @Author: DungNT Mar 08, 2019
     *  @Todo: get list Id not show to update
     **/
    public function getIdNotShow() {
        return [
            26, // id header salary
        ];
    }
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return EmailTemplates the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_email_templates}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('email_subject,email_body', 'required',),
            array('type', 'numerical', 'integerOnly'=>true),
            array('email_subject', 'length', 'max'=>255),
            array('email_body, parameter_description', 'safe'),
            array('id, email_subject, email_body, parameter_description, type', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'email_subject' => 'Email Subject',
            'email_body' => 'Email Body',
            'parameter_description' => 'Parameter Description',
            'type' => 'Type',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('id',$this->id);
        $criteria->addNotInCondition('id',$this->getIdNotShow());
        $criteria->compare('type',$this->type);
        $criteria->order = "id DESC";

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'Pagination' => array (
                    'PageSize' => 50, //edit your number items per page here
                ),			
        ));
    }
    
    public static function TestSendMail(){
        $aBody = array();            
        $aSubject = array();  
        CmsEmail::sendmail(1, $aSubject, $aBody, 'verzdev2@gmail.com');
    }    
    
}