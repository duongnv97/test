<?php

/**
 * This is the model class for table "{{_gas_costs_monthly}}".
 *
 * The followings are the available columns in table '{{_gas_costs_monthly}}':
 * @property integer $id
 * @property integer $costs_id
 * @property integer $agent_id
 * @property string $total
 * @property string $costs_month
 * @property string $costs_year
 * @property string $note
 */
class GasCostsMonthly extends CActiveRecord
{
    public $autocomplete_name;
    public $file_excel;    
    public $materials_id;    
    public $materials_sub_id;     
    
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{_gas_costs_monthly}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{

            return array(
                    array('costs_id, agent_id,costs_month,costs_year', 'required','on'=>'create_costs, update_costs'),
                    array('costs_id, agent_id', 'numerical', 'integerOnly'=>true),
                    array('total', 'length', 'max'=>16),
                    array('costs_month', 'length', 'max'=>2),
                    array('costs_year', 'length', 'max'=>4),
                    array('note', 'length', 'max'=>300),
                    array('id, costs_id, agent_id, total, costs_month, costs_year,note,materials_id,materials_sub_id', 'safe'),
            );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
                    'agent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
                    'costs' => array(self::BELONGS_TO, 'GasCosts', 'costs_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'costs_id' => 'Chi Phí',
			'agent_id' => 'Đại Lý',
			'total' => 'Số Tiền',
			'costs_month' => 'Tháng',
			'costs_year' => 'Năm',
			'note' => 'Ghi Chú',
			'materials_id' => 'Vật Tư',
			'materials_sub_id' => 'Vật Tư Con',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.costs_id',$this->costs_id);
		$criteria->compare('t.agent_id',$this->agent_id);
		$criteria->compare('t.total',$this->total,true);
		$criteria->compare('t.costs_month',$this->costs_month,true);
		$criteria->compare('t.costs_year',$this->costs_year,true);
		$criteria->compare('t.note',$this->note,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
		));
	}

	public function defaultScope()
	{
		return array(
			//'condition'=>'',
		);
	}
        
        public function beforeSave() {
            if($this->isNewRecord)
                $this->user_id_create = Yii::app()->user->id;
            return parent::beforeSave();
        }
        
        public static function getDataCostMonth($model){
            $aRes=array();
            $aCost = GasCosts::getAllObj();
            if(count($model->costs_month)>0){
                foreach ($model->costs_month as $month){
                    foreach($aCost as $cost_id=>$item){
                        $criteria=new CDbCriteria;
                        if(count($model->agent_id)>0){
//                            $criteria->addInCondition('t.agent_id',$model->agent_id);
                            $sParamsIn = implode(',', $model->agent_id);
                            $criteria->addCondition("t.agent_id IN ($sParamsIn)");
                        }
                        $criteria->compare('t.costs_month',$month);
                        $criteria->compare('t.costs_id',$cost_id);
                        $criteria->compare('t.costs_year',$model->costs_year);
                        //$criteria->select = 'agent_id, sum(total) as total';
                        //$criteria->group = 'agent_id';
                        $res = GasCostsMonthly::model()->findAll($criteria);
                               
                        if(count($res)>0)
                        foreach($res as $obj){
                                $aRes[$month][$cost_id][$obj->agent_id] = $obj->total;
                        }
                    }
                    $aRes[$month]['cost'] = $aCost;
                }
            }
			//else{
                foreach($aCost as $cost_id=>$item){
                    $criteria=new CDbCriteria;
                    if(count($model->agent_id)>0){
                        $sParamsIn = implode(',', $model->agent_id);
                        $criteria->addCondition("t.agent_id IN ($sParamsIn)");
                    }
                    $criteria->compare('t.costs_id',$cost_id);
                    $criteria->compare('t.costs_year',$model->costs_year);
                    $criteria->select = 'agent_id, sum(total) as total';
                    $criteria->group = 'agent_id';
                    $res = GasCostsMonthly::model()->findAll($criteria);

                    if(count($res)>0)
                    foreach($res as $obj){
                            $aRes[$model->costs_year][$cost_id][$obj->agent_id] = $obj->total;
                    }
                }
                $aRes[$model->costs_year]['cost'] = $aCost;
                
            //}
            return $aRes;
            // else report all year
            
        }
        
        
        public static function export_cost($model){
            $dataExport = GasCostsMonthly::getDataCostMonth($model);
            $dataAgent = Users::getArrObjectUserByRole(ROLE_AGENT,$model->agent_id);
            GasCostsMonthly::export_cost_excel($model,$dataExport,$dataAgent);            
        }
        
        public static function export_cost_excel($model,$dataExport,$dataAgent){
            
            
                Yii::import('application.extensions.vendors.PHPExcel',true);
		$objPHPExcel = new PHPExcel();
		 // Set properties
		 $objPHPExcel->getProperties()->setCreator("NguyenDung")
                            ->setLastModifiedBy("NguyenDung")
                            ->setTitle('Report Cost')
                            ->setSubject("Office 2007 XLSX Document")
                            ->setDescription("Report Cost")
                            ->setKeywords("office 2007 openxml php")
                            ->setCategory("GAS");
//		 $objPHPExcel->getActiveSheet()->setTitle('Report Cost');

		  
		// step 0: Tạo ra các sheet
				$i=0;
                if(count($model->costs_month)>0){
                    // $objPHPExcel->removeSheetByIndex(0); // may be need
                    for($i;$i<count($model->costs_month);$i++){
						$objPHPExcel->createSheet();
						$objPHPExcel->setActiveSheetIndex($i);
						// set default font and font-size
						$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
						$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
                        $objPHPExcel->getActiveSheet()->setTitle('THÁNG '.$model->costs_month[$i]);
                        $objPHPExcel->getActiveSheet()->setCellValue("A1", 'TỔNG HỢP CHI PHÍ ĐẠI LÝ THÁNG '.$model->costs_month[$i].'/'.$model->costs_year);
                        
                        $objPHPExcel->getActiveSheet()->setCellValue("A3", 'STT');
                        $objPHPExcel->getActiveSheet()->setCellValue("B3", 'Diễn Giải');
                        $objPHPExcel->getActiveSheet()->setCellValue("C3", 'Tổng Chi Phí');
                        $j=0;
                        $index = 1;	
                        foreach($dataAgent as $kAgent=>$itemAgent){
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$j+3).'3', $itemAgent->first_name);							
                            $j++;
                        }
                        		
                        $rowNum = 4;
                        $sumRow = 0;
						$sumRowAll = 0;
                        $sumAll=0;
                        $sumCol = array();                     
                        foreach($dataExport[$model->costs_month[$i]]['cost'] as $key=>$objCost){
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).($rowNum), ($key));
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).($rowNum), $objCost->name);
							$j=0;
							$sumRow = 0;
							foreach($dataAgent as $kAgent=>$itemAgent){							
								if(isset($dataExport[$model->costs_month[$i]][$objCost->id][$itemAgent->id])){
									$sumRow += $dataExport[$model->costs_month[$i]][$objCost->id][$itemAgent->id];
									$sumRowAll+=$dataExport[$model->costs_month[$i]][$objCost->id][$itemAgent->id];
									if(isset($sumCol[$itemAgent->id]))
										$sumCol[$itemAgent->id] += $dataExport[$model->costs_month[$i]][$objCost->id][$itemAgent->id];
									else
										$sumCol[$itemAgent->id] = $dataExport[$model->costs_month[$i]][$objCost->id][$itemAgent->id];
									$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$j+3).($rowNum), $dataExport[$model->costs_month[$i]][$objCost->id][$itemAgent->id]);
								}									
								$j++;
							}		
							$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+2).($rowNum), $sumRow);							
                            $rowNum++;
                        }
						$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).($rowNum), 'Tổng Cộng');
						$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+2).($rowNum), $sumRowAll);
                         // begin write sum col
							$j=0;
							foreach($dataAgent as $kAgent=>$itemAgent){							
								if(isset($sumCol[$itemAgent->id])){
									$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$j+3).($rowNum), $sumCol[$itemAgent->id]);
								}									
								$j++;
							}	
							$objPHPExcel->getActiveSheet()
									->getStyle(MyFunctionCustom::columnName($index+2).'4:'.MyFunctionCustom::columnName($index+2).($rowNum))
									->getFont()
									->setBold(true);
							$objPHPExcel->getActiveSheet()
									->getStyle(MyFunctionCustom::columnName($index+1).($rowNum).':'.MyFunctionCustom::columnName($index+2+count($dataAgent)).($rowNum))
									->getFont()
									->setBold(true);		
							$objPHPExcel->getActiveSheet()
									->getStyle(MyFunctionCustom::columnName($index).'3:'.MyFunctionCustom::columnName($index+2+count($dataAgent)).'3')
									->getFont()
									->setBold(true);									
							$objPHPExcel->getActiveSheet()
							->getStyle(MyFunctionCustom::columnName($index+2).'4:'.MyFunctionCustom::columnName($index+2+count($dataAgent)).($rowNum) )->getNumberFormat()
							->setFormatCode('#,##0');               
							 $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).'3:'.MyFunctionCustom::columnName($index).($rowNum))
									->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
									
							$objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).'3:'.MyFunctionCustom::columnName($index+2+count($dataAgent)).($rowNum) )
											->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);									
									
                    } // ********** end for 1 sheet for($i=0;$i<count($model->costs_month);$i++){
                }    
                
				// for sum one year
				if(isset($dataExport[$model->costs_year]['cost'])){
						//$objPHPExcel->createSheet();
						$objPHPExcel->setActiveSheetIndex($i);
						$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
						$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
                        $objPHPExcel->getActiveSheet()->setTitle('NĂM '.$model->costs_year);
                        $objPHPExcel->getActiveSheet()->setCellValue("A1", 'TỔNG HỢP CHI PHÍ ĐẠI LÝ NĂM '.$model->costs_year);
                        
                        $objPHPExcel->getActiveSheet()->setCellValue("A3", 'STT');
                        $objPHPExcel->getActiveSheet()->setCellValue("B3", 'Diễn Giải');
                        $objPHPExcel->getActiveSheet()->setCellValue("C3", 'Tổng Chi Phí');
                        $j=0;
                        $index = 1;	
                        foreach($dataAgent as $kAgent=>$itemAgent){
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$j+3).'3', $itemAgent->first_name);							
                            $j++;
                        }
                        		
                        $rowNum = 4;
                        $sumRow = 0;
						$sumRowAll = 0;
                        $sumAll=0;
                        $sumCol = array();      	

						foreach($dataExport[$model->costs_year]['cost'] as $key=>$objCost){
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).($rowNum), ($key));
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).($rowNum), $objCost->name);
							$j=0;
							$sumRow = 0;
							foreach($dataAgent as $kAgent=>$itemAgent){							
								if(isset($dataExport[$model->costs_year][$objCost->id][$itemAgent->id])){
									$sumRow += $dataExport[$model->costs_year][$objCost->id][$itemAgent->id];
									$sumRowAll+=$dataExport[$model->costs_year][$objCost->id][$itemAgent->id];
									if(isset($sumCol[$itemAgent->id]))
										$sumCol[$itemAgent->id] += $dataExport[$model->costs_year][$objCost->id][$itemAgent->id];
									else
										$sumCol[$itemAgent->id] = $dataExport[$model->costs_year][$objCost->id][$itemAgent->id];
									$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$j+3).($rowNum), $dataExport[$model->costs_year][$objCost->id][$itemAgent->id]);
								}									
								$j++;
							}		
							$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+2).($rowNum), $sumRow);							
                            $rowNum++;
                        }
						$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).($rowNum), 'Tổng Cộng');
						$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+2).($rowNum), $sumRowAll);
                         // begin write sum col
							$j=0;
							foreach($dataAgent as $kAgent=>$itemAgent){							
								if(isset($sumCol[$itemAgent->id])){
									$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$j+3).($rowNum), $sumCol[$itemAgent->id]);
								}									
								$j++;
							}	
							$objPHPExcel->getActiveSheet()
									->getStyle(MyFunctionCustom::columnName($index+2).'4:'.MyFunctionCustom::columnName($index+2).($rowNum))
									->getFont()
									->setBold(true);
							$objPHPExcel->getActiveSheet()
									->getStyle(MyFunctionCustom::columnName($index+1).($rowNum).':'.MyFunctionCustom::columnName($index+2+count($dataAgent)).($rowNum))
									->getFont()
									->setBold(true);		
							$objPHPExcel->getActiveSheet()
									->getStyle(MyFunctionCustom::columnName($index).'3:'.MyFunctionCustom::columnName($index+2+count($dataAgent)).'3')
									->getFont()
									->setBold(true);									
							$objPHPExcel->getActiveSheet()
							->getStyle(MyFunctionCustom::columnName($index+2).'4:'.MyFunctionCustom::columnName($index+2+count($dataAgent)).($rowNum) )->getNumberFormat()
							->setFormatCode('#,##0');               
							 $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).'3:'.MyFunctionCustom::columnName($index).($rowNum))
									->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
									
							$objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).'3:'.MyFunctionCustom::columnName($index+2+count($dataAgent)).($rowNum) )
											->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);									
						
				
				}
				
				
		 //save file 
		 $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		 //$objWriter->save('MyExcel.xslx');

		 for($level=ob_get_level();$level>0;--$level)
		 {
			 @ob_end_clean();
		 }
		 header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		 header('Pragma: public');
		 header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		 header('Content-Disposition: attachment; filename="'.'Report Cost'.'.'.'xlsx'.'"');
		 header('Cache-Control: max-age=0');				
		 $objWriter->save('php://output');			
		 Yii::app()->end();		            
            
        }
		
		
        // $dataAgent is array( agent_id => obj_agent
        public static function getDataCostRevenue($model,$dataAgent){
            $aRes=array();
            $aMaterial = GasCostsMonthly::getArrObjMaterialForExport();
            if(count($model->costs_month)>0){
                foreach ($model->costs_month as $month){					
                    foreach($dataAgent as $agent_id=>$obj_agent){
						foreach($aMaterial as $materialObj){
							$criteria=new CDbCriteria;
							if(count($materialObj['sub_arr_id'])>0){
                                                            $sParamsIn = implode(',', $materialObj['sub_arr_id']);
                                                            $criteria->addCondition("t.materials_id IN ($sParamsIn)");
                                                        }
//								$criteria->addInCondition('t.materials_id',$materialObj['sub_arr_id']);
								
							/* if(count($model->agent_id)>0)
								$criteria->addInCondition('t.agent_id',$model->agent_id);
							else */
                                                        $criteria->compare('t.agent_id',$agent_id);
							$criteria->compare('t.sell_month',$month*1);
							$criteria->compare('t.sell_year',$model->costs_year);
							$criteria->select = 'agent_id, price_root, sum(qty) as qty, sum(total_sell) as total_sell, sum(total_root) as total_root ';
							$criteria->group = 't.agent_id';
							$res = GasMaterialsSell::model()->findAll($criteria);
								   
							if(count($res)>0)
							foreach($res as $obj){
									$aRes[$month][$agent_id][$materialObj['parent_obj']->id]['materials_obj'] = $materialObj;
									$aRes[$month][$agent_id][$materialObj['parent_obj']->id]['sell_obj'] = $obj;
							}
						}
                    }

					$aRes[$month]['materials_arr'] = $aMaterial;
                }

            }
			//else{
			
            foreach($dataAgent as $agent_id=>$obj_agent){
                foreach($aMaterial as $materialObj){
                    $criteria=new CDbCriteria;
                    if(count($materialObj['sub_arr_id'])>0)
                            $criteria->addInCondition('t.materials_id',$materialObj['sub_arr_id']);

                    $criteria->compare('t.agent_id',$agent_id);
                    $criteria->compare('t.sell_year',$model->costs_year);
                    $criteria->select = 'agent_id, price_root, sum(qty) as qty, sum(total_sell) as total_sell, sum(total_root) as total_root ';
                    $criteria->group = 'agent_id';
                    $res = GasMaterialsSell::model()->findAll($criteria);

                    if(count($res)>0)
                    foreach($res as $obj){
                        $aRes[$model->costs_year][$obj->agent_id][$materialObj['parent_obj']->id]['materials_obj'] = $materialObj;
                        $aRes[$model->costs_year][$obj->agent_id][$materialObj['parent_obj']->id]['sell_obj'] = $obj;
                    }
                }
            }

            $aRes[$model->costs_year]['materials_arr'] = $aMaterial;		
						
                
            //}
            return $aRes;
            // else report all year            
        }
        
        
        public static function export_cost_revenue($model){
            $dataAgent = Users::getArrObjectUserByRole(ROLE_AGENT,$model->agent_id);
            $dataExport = GasCostsMonthly::getDataCostRevenue($model,$dataAgent);
            
            GasCostsMonthly::export_cost_revenue_excel($model,$dataExport,$dataAgent);            
        }	

		
		public static function export_cost_revenue_excel($model,$dataExport,$dataAgent){
            
            
                Yii::import('application.extensions.vendors.PHPExcel',true);
		$objPHPExcel = new PHPExcel();
		 // Set properties
		 $objPHPExcel->getProperties()->setCreator("NguyenDung")
                            ->setLastModifiedBy("NguyenDung")
                            ->setTitle('Report Revenue Cost')
                            ->setSubject("Office 2007 XLSX Document")
                            ->setDescription("Report Revenue Cost")
                            ->setKeywords("office 2007 openxml php")
                            ->setCategory("GAS");
//		 $objPHPExcel->getActiveSheet()->setTitle('Report Cost');

$styleArray = array(
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
		'color' => array(
			'argb' => '9acd32',
		),
		'endcolor' => array(
			'argb' => '9acd32',
		),
	),
);                 
		  
		// step 0: Tạo ra các sheet
				$i=0;
                if(count($model->costs_month)>0){
                    // $objPHPExcel->removeSheetByIndex(0); // may be need
                    for($i;$i<count($model->costs_month);$i++){
						$objPHPExcel->createSheet();
						$objPHPExcel->setActiveSheetIndex($i);
						// set default font and font-size
						$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
						$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
                        $objPHPExcel->getActiveSheet()->setTitle('THÁNG '.$model->costs_month[$i]);
                        $objPHPExcel->getActiveSheet()->setCellValue("A1", 'BẢNG TỔNG HỢP DOANH THU - GIÁ VỐN - LÃI GỘP THÁNG '.$model->costs_month[$i].'/'.$model->costs_year);
                        
                        $objPHPExcel->getActiveSheet()->setCellValue("A3", 'STT');
                        $objPHPExcel->getActiveSheet()->setCellValue("B3", 'Diễn Giải');
                        $objPHPExcel->getActiveSheet()->setCellValue("C3", 'ĐVT');
						$objPHPExcel->getActiveSheet()->setCellValue("D3", 'Số Lượng');
						$objPHPExcel->getActiveSheet()->setCellValue("E3", 'Doanh thu');
						//$objPHPExcel->getActiveSheet()->setCellValue("F3", 'Doanh thu');
						$objPHPExcel->getActiveSheet()->setCellValue("G3", 'Giá vốn');
						//$objPHPExcel->getActiveSheet()->setCellValue("H3", 'Giá vốn');
						$objPHPExcel->getActiveSheet()->setCellValue("I3", 'Lãi gộp');
						//$objPHPExcel->getActiveSheet()->setCellValue("J3", 'Lãi gộp');
						
						$objPHPExcel->getActiveSheet()->setCellValue("E4", 'ĐG BQ');
						$objPHPExcel->getActiveSheet()->setCellValue("F4", 'Thành tiền');
						
						$objPHPExcel->getActiveSheet()->setCellValue("G4", 'ĐG BQ');
						$objPHPExcel->getActiveSheet()->setCellValue("H4", 'Thành tiền');			
						$objPHPExcel->getActiveSheet()->setCellValue("I4", 'ĐG BQ');
						$objPHPExcel->getActiveSheet()->setCellValue("J4", 'Thành tiền');	
						
						 $objPHPExcel->getActiveSheet()->mergeCells('A3:A4');
						 $objPHPExcel->getActiveSheet()->getStyle('A3:A4')
								->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						 $objPHPExcel->getActiveSheet()->mergeCells('B3:B4');
						 $objPHPExcel->getActiveSheet()->getStyle('B3:B4')
								->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
						 $objPHPExcel->getActiveSheet()->mergeCells('C3:C4');
						 $objPHPExcel->getActiveSheet()->getStyle('C3:C4')
								->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						 $objPHPExcel->getActiveSheet()->mergeCells('D3:D4');
						 $objPHPExcel->getActiveSheet()->getStyle('D3:D4')
								->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);								
						 $objPHPExcel->getActiveSheet()->mergeCells('E3:F3');
						 $objPHPExcel->getActiveSheet()->getStyle('E3:F3')
								->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);		
						 $objPHPExcel->getActiveSheet()->mergeCells('G3:H3');
						 $objPHPExcel->getActiveSheet()->getStyle('G3:H3')
								->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
						 $objPHPExcel->getActiveSheet()->mergeCells('I3:J3');
						 $objPHPExcel->getActiveSheet()->getStyle('I3:J3')
								->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);									
						$objPHPExcel->getActiveSheet()
								->getStyle('A3:J4')
								->getFont()
								->setBold(true);						
						
                        $j=0; // is $indexAgent
                        $index = 1;		
                        $rowNum = 5;
						$rowCurrent = 5;
                        $sumRow = 0;
						$sumRowAll = 0;
                        $sumAll=0;
                        $sumCol = array();						
                        foreach($dataAgent as $kAgent=>$itemAgent){
					$sumAgentDoanhThu=0;		
					$sumAgentGiaVon=0;		
					$sumAgentLaiGop=0;		
							$countMaterial = count($dataExport[$model->costs_month[$i]]['materials_arr']);
							$rowCurrent = $rowNum+$j+($j*$countMaterial);
							$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).($rowCurrent), $j+1);
							$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).($rowCurrent), $itemAgent->first_name);							
							$objPHPExcel->getActiveSheet()
									->getStyle(MyFunctionCustom::columnName($index).($rowCurrent).':'.MyFunctionCustom::columnName($index+9).($rowCurrent))
									->getFont()
									->setBold(true);							

						$objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).($rowCurrent).':'.MyFunctionCustom::columnName($index+9).($rowCurrent))->applyFromArray($styleArray);	
							foreach($dataExport[$model->costs_month[$i]]['materials_arr'] as $keyMaterial=>$objMaterial){		
								$rowCurrent = $rowNum+$j+($j*$countMaterial)+$keyMaterial+1;
								$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).($rowCurrent), $objMaterial['parent_obj']->name);
								$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+2).($rowCurrent), $objMaterial['parent_obj']->unit);
								//$aRes[$month][$obj->agent_id][$materialObj['parent_obj']->id]['materials_obj'] = $materialObj;
								//$aRes[$month][$obj->agent_id][$materialObj['parent_obj']->id]['sell_obj'] = $obj;

							   if(isset($dataExport[$model->costs_month[$i]][$itemAgent->id][$objMaterial['parent_obj']->id]['materials_obj'])){
									
									$qty = $dataExport[$model->costs_month[$i]][$itemAgent->id][$objMaterial['parent_obj']->id]['sell_obj']->qty;
									$total_sell = $dataExport[$model->costs_month[$i]][$itemAgent->id][$objMaterial['parent_obj']->id]['sell_obj']->total_sell;
									$total_root = $dataExport[$model->costs_month[$i]][$itemAgent->id][$objMaterial['parent_obj']->id]['sell_obj']->total_root;
                                                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+3).($rowCurrent), $qty );
                                                                        $qty = ($qty==0)?1:$qty;
                                                                        $total_root_average = round($total_root/$qty);
									$total_sell_average = round($total_sell/$qty);
									$LAI_GOP_AVERAGE = round(($total_sell-$total_root)/$qty);
                                                                        $LAI_GOP = $total_sell-$total_root;
                                                                        $sumAgentDoanhThu += $total_sell;		
                                                                        $sumAgentGiaVon += $total_root;		
                                                                        $sumAgentLaiGop += $LAI_GOP;                                                                        
									
									$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+4).($rowCurrent), $total_sell_average );
									$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+5).($rowCurrent), $total_sell );
									$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+6).($rowCurrent), $total_root_average );
									$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+7).($rowCurrent), $total_root );
									$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+8).($rowCurrent), $LAI_GOP_AVERAGE );
									$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+9).($rowCurrent),  $LAI_GOP);
								}
							}
                                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+5).($rowCurrent-$keyMaterial-1), $sumAgentDoanhThu);
                                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+7).($rowCurrent-$keyMaterial-1), $sumAgentGiaVon);
                                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+9).($rowCurrent-$keyMaterial-1), $sumAgentLaiGop);
                                                        
							
                            $j++;
                        }
						$objPHPExcel->getActiveSheet()
						->getStyle(MyFunctionCustom::columnName($index+3).'5:'.MyFunctionCustom::columnName($index+9).($rowCurrent) )->getNumberFormat()
						->setFormatCode('#,##0');  						
						$objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).'3:'.MyFunctionCustom::columnName($index+9).($rowCurrent) )
							->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
						$objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index+2).'5:'.MyFunctionCustom::columnName($index+3).($rowCurrent))
							->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						$objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).'5:'.MyFunctionCustom::columnName($index).($rowCurrent))
							->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);						
                                                
                        						
									
                    } // ********** end for 1 sheet for($i=0;$i<count($model->costs_month);$i++){
                }    
                
				// for sum one year
				 if(isset($dataExport[$model->costs_year]['materials_arr'])){
						//$objPHPExcel->createSheet();
						$objPHPExcel->setActiveSheetIndex($i);
															
						$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
						$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
                        $objPHPExcel->getActiveSheet()->setTitle('NĂM '.$model->costs_year);
                        $objPHPExcel->getActiveSheet()->setCellValue("A1", 'BẢNG TỔNG HỢP DOANH THU - GIÁ VỐN - LÃI GỘP NĂM '.$model->costs_year);
                        
                        $objPHPExcel->getActiveSheet()->setCellValue("A3", 'STT');
                        $objPHPExcel->getActiveSheet()->setCellValue("B3", 'Diễn Giải');
                        $objPHPExcel->getActiveSheet()->setCellValue("C3", 'ĐVT');
						$objPHPExcel->getActiveSheet()->setCellValue("D3", 'Số Lượng');
						$objPHPExcel->getActiveSheet()->setCellValue("E3", 'Doanh thu');
						//$objPHPExcel->getActiveSheet()->setCellValue("F3", 'Doanh thu');
						$objPHPExcel->getActiveSheet()->setCellValue("G3", 'Giá vốn');
						//$objPHPExcel->getActiveSheet()->setCellValue("H3", 'Giá vốn');
						$objPHPExcel->getActiveSheet()->setCellValue("I3", 'Lãi gộp');
						//$objPHPExcel->getActiveSheet()->setCellValue("J3", 'Lãi gộp');
						
						$objPHPExcel->getActiveSheet()->setCellValue("E4", 'ĐG BQ');
						$objPHPExcel->getActiveSheet()->setCellValue("F4", 'Thành tiền');
						
						$objPHPExcel->getActiveSheet()->setCellValue("G4", 'ĐG BQ');
						$objPHPExcel->getActiveSheet()->setCellValue("H4", 'Thành tiền');			
						$objPHPExcel->getActiveSheet()->setCellValue("I4", 'ĐG BQ');
						$objPHPExcel->getActiveSheet()->setCellValue("J4", 'Thành tiền');	
						
						 $objPHPExcel->getActiveSheet()->mergeCells('A3:A4');
						 $objPHPExcel->getActiveSheet()->getStyle('A3:A4')
								->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						 $objPHPExcel->getActiveSheet()->mergeCells('B3:B4');
						 $objPHPExcel->getActiveSheet()->getStyle('B3:B4')
								->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
						 $objPHPExcel->getActiveSheet()->mergeCells('C3:C4');
						 $objPHPExcel->getActiveSheet()->getStyle('C3:C4')
								->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						 $objPHPExcel->getActiveSheet()->mergeCells('D3:D4');
						 $objPHPExcel->getActiveSheet()->getStyle('D3:D4')
								->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);								
						 $objPHPExcel->getActiveSheet()->mergeCells('E3:F3');
						 $objPHPExcel->getActiveSheet()->getStyle('E3:F3')
								->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);		
						 $objPHPExcel->getActiveSheet()->mergeCells('G3:H3');
						 $objPHPExcel->getActiveSheet()->getStyle('G3:H3')
								->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
						 $objPHPExcel->getActiveSheet()->mergeCells('I3:J3');
						 $objPHPExcel->getActiveSheet()->getStyle('I3:J3')
								->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);									
						$objPHPExcel->getActiveSheet()
								->getStyle('A3:J4')
								->getFont()
								->setBold(true);						
						
                        $j=0; // is $indexAgent
                        $index = 1;		
                        $rowNum = 5;
						$rowCurrent = 5;
                        $sumRow = 0;
						$sumRowAll = 0;
                        $sumAll=0;
                        $sumCol = array();						
                        foreach($dataAgent as $kAgent=>$itemAgent){
                                            $sumAgentDoanhThu=0;		
                                            $sumAgentGiaVon=0;		
                                            $sumAgentLaiGop=0;		
							
							$countMaterial = count($dataExport[$model->costs_year]['materials_arr']);
							$rowCurrent = $rowNum+$j+($j*$countMaterial);
							$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).($rowCurrent), $j+1);
							$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).($rowCurrent), $itemAgent->first_name);
							
							$objPHPExcel->getActiveSheet()
									->getStyle(MyFunctionCustom::columnName($index).($rowCurrent).':'.MyFunctionCustom::columnName($index+9).($rowCurrent))
									->getFont()
									->setBold(true);							
							$objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).($rowCurrent).':'.MyFunctionCustom::columnName($index+9).($rowCurrent))->applyFromArray($styleArray);	
							foreach($dataExport[$model->costs_year]['materials_arr'] as $keyMaterial=>$objMaterial){		
								$rowCurrent = $rowNum+$j+($j*$countMaterial)+$keyMaterial+1;
								$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).($rowCurrent), $objMaterial['parent_obj']->name);
								$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+2).($rowCurrent), $objMaterial['parent_obj']->unit);
								//$aRes[$month][$obj->agent_id][$materialObj['parent_obj']->id]['materials_obj'] = $materialObj;
								//$aRes[$month][$obj->agent_id][$materialObj['parent_obj']->id]['sell_obj'] = $obj;

							   if(isset($dataExport[$model->costs_year][$itemAgent->id][$objMaterial['parent_obj']->id]['materials_obj'])){
									
									$qty = $dataExport[$model->costs_year][$itemAgent->id][$objMaterial['parent_obj']->id]['sell_obj']->qty;                                                                        
									$total_sell = $dataExport[$model->costs_year][$itemAgent->id][$objMaterial['parent_obj']->id]['sell_obj']->total_sell;
									$total_root = $dataExport[$model->costs_year][$itemAgent->id][$objMaterial['parent_obj']->id]['sell_obj']->total_root;
                                                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+3).($rowCurrent), $qty );
                                                                        $qty = ($qty==0)?1:$qty;
									$total_root_average = round($total_root/$qty);
									$total_sell_average = round($total_sell/$qty);
									$LAI_GOP_AVERAGE = round(($total_sell-$total_root)/$qty);
                                                                        $LAI_GOP = $total_sell-$total_root;
                                                                        $sumAgentDoanhThu += $total_sell;		
                                                                        $sumAgentGiaVon += $total_root;		
                                                                        $sumAgentLaiGop += $LAI_GOP;                                                                        
									
									$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+4).($rowCurrent), $total_sell_average );
									$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+5).($rowCurrent), $total_sell );
									$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+6).($rowCurrent), $total_root_average );
									$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+7).($rowCurrent), $total_root );
									$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+8).($rowCurrent), $LAI_GOP_AVERAGE );
									$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+9).($rowCurrent), $LAI_GOP);
								}

							}
                                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+5).($rowCurrent-$keyMaterial-1), $sumAgentDoanhThu);
                                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+7).($rowCurrent-$keyMaterial-1), $sumAgentGiaVon);
                                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+9).($rowCurrent-$keyMaterial-1), $sumAgentLaiGop);
                                                        
							
                            $j++;
                        }
						$objPHPExcel->getActiveSheet()
						->getStyle(MyFunctionCustom::columnName($index+3).'5:'.MyFunctionCustom::columnName($index+9).($rowCurrent) )->getNumberFormat()
						->setFormatCode('#,##0');  						
						$objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).'3:'.MyFunctionCustom::columnName($index+9).($rowCurrent) )
							->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
						$objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index+2).'5:'.MyFunctionCustom::columnName($index+3).($rowCurrent))
							->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);						
						$objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).'5:'.MyFunctionCustom::columnName($index).($rowCurrent))
							->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);						
				
				} 
				
				
		 //save file 
		 $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		 //$objWriter->save('MyExcel.xslx');

		 for($level=ob_get_level();$level>0;--$level)
		 {
			 @ob_end_clean();
		 }
		 header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		 header('Pragma: public');
		 header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		 header('Content-Disposition: attachment; filename="'.'Report Revenue Cost'.'.'.'xlsx'.'"');
		 header('Cache-Control: max-age=0');				
		 $objWriter->save('php://output');			
		 Yii::app()->end();		            
            
        }
	        

        public static function export_business_results($model){
            $dataAgent = Users::getArrObjectUserByRole(ROLE_AGENT,$model->agent_id);
            $dataExport = GasCostsMonthly::getBusinessResults($model,$dataAgent);            
            GasCostsMonthly::export_business_results_excel($model,$dataExport,$dataAgent);            
        }	        
        
        
        // $dataAgent is array( agent_id => obj_agent
        public static function getBusinessResults($model,$dataAgent){
            $aRes=array();
            $aMaterialKm = GasCostsMonthly::getArrMaterialNotInBusinessResult();
            if(count($model->costs_month)>0){
                foreach ($model->costs_month as $month){
                    foreach($dataAgent as $agent_id=>$obj_agent){
                            // doanh thu vs giá vốn
                            $criteria=new CDbCriteria;
                            $criteria->compare('t.agent_id',$agent_id);
                            $criteria->compare('t.sell_month',$month*1);
                            $criteria->compare('t.sell_year',$model->costs_year);
                            $criteria->addNotInCondition('t.materials_id',$aMaterialKm);
                            $criteria->select = 'agent_id, sum(total_sell) as total_sell, sum(total_root) as total_root ';
                            $criteria->group = 't.agent_id';
                            $res = GasMaterialsSell::model()->findAll($criteria);

                            if(count($res)>0)
                            foreach($res as $obj){
                                $aRes[$month][$agent_id]['sell_obj'] = $obj;
                            }
                            // end doanh thu vs giá vốn
                            
                            // chi phí
                            $criteria=new CDbCriteria;
                            $criteria->compare('t.agent_id',$agent_id);
                            $criteria->compare('t.costs_month',$month);
                            $criteria->compare('t.costs_year',$model->costs_year);
                            $criteria->select = 'agent_id, sum(total) as total';
                            $criteria->group = 't.agent_id';
                            $res = GasCostsMonthly::model()->findAll($criteria);
                            if(count($res)>0)
                            foreach($res as $obj){
                                $aRes[$month][$agent_id]['cost_obj'] = $obj;
                            }
                            // end chi phí
                            
                    }
                            
                }

            }
			
            foreach($dataAgent as $agent_id=>$obj_agent){
                    // doanh thu vs giá vốn    
                    $criteria=new CDbCriteria;
                    $criteria->compare('t.agent_id',$agent_id);
                    $criteria->compare('t.sell_year',$model->costs_year);
                    $criteria->select = 'agent_id, sum(total_sell) as total_sell, sum(total_root) as total_root ';
                    $criteria->addNotInCondition('t.materials_id',$aMaterialKm);
                    $criteria->group = 't.agent_id';
                    $res = GasMaterialsSell::model()->findAll($criteria);

                    if(count($res)>0)
                    foreach($res as $obj){
                        $aRes[$model->costs_year][$agent_id]['sell_obj'] = $obj;
                    }
                    // end doanh thu vs giá vốn

                    // chi phí
                    $criteria=new CDbCriteria;
                    $criteria->compare('t.agent_id',$agent_id);
                    $criteria->compare('t.costs_year',$model->costs_year);
                    $criteria->select = 'agent_id, sum(total) as total';
                    $criteria->group = 't.agent_id';
                    $res = GasCostsMonthly::model()->findAll($criteria);
                    if(count($res)>0)
                    foreach($res as $obj){
                        $aRes[$model->costs_year][$agent_id]['cost_obj'] = $obj;
                    }
                    // end chi phí
            }						                
            return $aRes;
        }		
        
        

        public static function export_business_results_excel($model,$dataExport,$dataAgent){
                Yii::import('application.extensions.vendors.PHPExcel',true);
		$objPHPExcel = new PHPExcel();
		 // Set properties
		 $objPHPExcel->getProperties()->setCreator("NguyenDung")
                            ->setLastModifiedBy("NguyenDung")
                            ->setTitle('Report Business Results')
                            ->setSubject("Office 2007 XLSX Document")
                            ->setDescription("Report Business Results")
                            ->setKeywords("office 2007 openxml php")
                            ->setCategory("GAS");
//		 $objPHPExcel->getActiveSheet()->setTitle('Report Cost');

		  
		// step 0: Tạo ra các sheet
                $i=0;
                if(count($model->costs_month)>0){
                    // $objPHPExcel->removeSheetByIndex(0); // may be need
                    for($i;$i<count($model->costs_month);$i++){
                        $objPHPExcel->createSheet();
                        $objPHPExcel->setActiveSheetIndex($i);
                        // set default font and font-size
                        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
                        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
                        $objPHPExcel->getActiveSheet()->setTitle('THÁNG '.$model->costs_month[$i]);
                        $objPHPExcel->getActiveSheet()->setCellValue("A1", 'BÁO CÁO KẾT QUẢ HOẠT ĐỘNG KINH DOANH THÁNG '.$model->costs_month[$i].'/'.$model->costs_year);
                        
                        $objPHPExcel->getActiveSheet()->setCellValue("A3", 'STT');
                        $objPHPExcel->getActiveSheet()->setCellValue("B3", 'Đại Lý');
                        $objPHPExcel->getActiveSheet()->setCellValue("C3", 'Doanh Thu');
                        $objPHPExcel->getActiveSheet()->setCellValue("D3", 'Giá vốn');
                        $objPHPExcel->getActiveSheet()->setCellValue("E3", 'Lợi nhuận gộp ');
                        $objPHPExcel->getActiveSheet()->setCellValue("F3", 'Chi phí');
                        $objPHPExcel->getActiveSheet()->setCellValue("G3", 'Lợi nhuận Thuần');
                        
                        $j=0;
                        $index = 1;	
                        $rowNum = 4;
                        $sumDoanhThu = 0;
                        $sumGiaVon = 0;
                        $sumLoiNhuanGop = 0;
                        $sumChiPhi = 0;
                        $sumLoiNhuanThuan = 0;
                        
                        foreach($dataAgent as $kAgent=>$itemAgent){
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).($j+$rowNum), $j+1);
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).($j+$rowNum), $itemAgent->first_name);
                            
                            $LOI_NHUAN_GOP=0;
                            if(isset($dataExport[$model->costs_month[$i]][$itemAgent->id]['sell_obj'])){
                                $sell_obj = $dataExport[$model->costs_month[$i]][$itemAgent->id]['sell_obj'];
                                $sumDoanhThu+=$sell_obj->total_sell;
                                $sumGiaVon+=$sell_obj->total_root;
                                
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+2).($j+$rowNum), $sell_obj->total_sell);
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+3).($j+$rowNum), $sell_obj->total_root);
                                $LOI_NHUAN_GOP=$sell_obj->total_sell-$sell_obj->total_root;
                                $sumLoiNhuanGop+=$LOI_NHUAN_GOP;
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+4).($j+$rowNum), $sell_obj->total_sell-$sell_obj->total_root);    
                            }
                            
                            $cost_val=0;
                            if(isset($dataExport[$model->costs_month[$i]][$itemAgent->id]['cost_obj'])){
                                $cost_obj = $dataExport[$model->costs_month[$i]][$itemAgent->id]['cost_obj'];
                                $cost_val=$cost_obj->total; // chi phí bán hàng
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+5).($j+$rowNum), $cost_obj->total);
                            }
                            $LOI_NHUAN_THUAN=$LOI_NHUAN_GOP-$cost_val;
                            $sumChiPhi += $cost_val;
                            $sumLoiNhuanThuan += $LOI_NHUAN_THUAN;
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+6).($j+$rowNum), $LOI_NHUAN_THUAN);
                            $j++;
                        }
                        // begin write sum col                        		
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).($j+$rowNum), 'Tổng Cộng');                        
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+2).($j+$rowNum), $sumDoanhThu);                        
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+3).($j+$rowNum), $sumGiaVon);                        
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+4).($j+$rowNum), $sumLoiNhuanGop);                        
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+5).($j+$rowNum), $sumChiPhi);                        
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+6).($j+$rowNum), $sumLoiNhuanThuan);                        
                         
                        $objPHPExcel->getActiveSheet()
                                        ->getStyle(MyFunctionCustom::columnName($index).'3:'.MyFunctionCustom::columnName($index+6).'3')
                                        ->getFont()
                                        ->setBold(true);
                        $objPHPExcel->getActiveSheet()
                                        ->getStyle(MyFunctionCustom::columnName($index).($j+$rowNum).':'.MyFunctionCustom::columnName($index+6).($j+$rowNum))
                                        ->getFont()
                                        ->setBold(true);		
									
                        $objPHPExcel->getActiveSheet()
                        ->getStyle(MyFunctionCustom::columnName($index+2).($rowNum).':'.MyFunctionCustom::columnName($index+6).($j+$rowNum) )->getNumberFormat()
                        ->setFormatCode('#,##0');               
                         $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).'3:'.MyFunctionCustom::columnName($index).($j+$rowNum))
                                        ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                        $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).'3:'.MyFunctionCustom::columnName($index+6).($j+$rowNum) )
                                                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
									
                    } // ********** end for 1 sheet for($i=0;$i<count($model->costs_month);$i++){
                }    
                
                // for sum one year
                if(isset($dataExport[$model->costs_year])){
                        $objPHPExcel->setActiveSheetIndex($i);
                        // set default font and font-size
                        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
                        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
                        $objPHPExcel->getActiveSheet()->setTitle('NĂM '.$model->costs_year);
                        $objPHPExcel->getActiveSheet()->setCellValue("A1", 'BÁO CÁO KẾT QUẢ HOẠT ĐỘNG KINH DOANH NĂM '.$model->costs_year);
                        
                        $objPHPExcel->getActiveSheet()->setCellValue("A3", 'STT');
                        $objPHPExcel->getActiveSheet()->setCellValue("B3", 'Đại Lý');
                        $objPHPExcel->getActiveSheet()->setCellValue("C3", 'Doanh Thu');
                        $objPHPExcel->getActiveSheet()->setCellValue("D3", 'Giá vốn');
                        $objPHPExcel->getActiveSheet()->setCellValue("E3", 'Lợi nhuận gộp ');
                        $objPHPExcel->getActiveSheet()->setCellValue("F3", 'Chi phí');
                        $objPHPExcel->getActiveSheet()->setCellValue("G3", 'Lợi nhuận Thuần');
                        
                        $j=0;
                        $index = 1;	
                        $rowNum = 4;
                        $sumDoanhThu = 0;
                        $sumGiaVon = 0;
                        $sumLoiNhuanGop = 0;
                        $sumChiPhi = 0;
                        $sumLoiNhuanThuan = 0;
                        
                        foreach($dataAgent as $kAgent=>$itemAgent){
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).($j+$rowNum), $j+1);
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).($j+$rowNum), $itemAgent->first_name);
                            
                            $LOI_NHUAN_GOP=0;
                            if(isset($dataExport[$model->costs_year][$itemAgent->id]['sell_obj'])){
                                $sell_obj = $dataExport[$model->costs_year][$itemAgent->id]['sell_obj'];
                                $sumDoanhThu+=$sell_obj->total_sell;
                                $sumGiaVon+=$sell_obj->total_root;
                                
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+2).($j+$rowNum), $sell_obj->total_sell);
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+3).($j+$rowNum), $sell_obj->total_root);
                                $LOI_NHUAN_GOP=$sell_obj->total_sell-$sell_obj->total_root;
                                $sumLoiNhuanGop+=$LOI_NHUAN_GOP;
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+4).($j+$rowNum), $sell_obj->total_sell-$sell_obj->total_root);    
                            }
                            
                            $cost_val=0;
                            if(isset($dataExport[$model->costs_year][$itemAgent->id]['cost_obj'])){
                                $cost_obj = $dataExport[$model->costs_year][$itemAgent->id]['cost_obj'];
                                $cost_val=$cost_obj->total; // chi phí bán hàng
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+5).($j+$rowNum), $cost_obj->total);
                            }
                            $LOI_NHUAN_THUAN=$LOI_NHUAN_GOP-$cost_val;
                            $sumChiPhi += $cost_val;
                            $sumLoiNhuanThuan += $LOI_NHUAN_THUAN;
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+6).($j+$rowNum), $LOI_NHUAN_THUAN);
                            $j++;
                        }
                        // begin write sum col                        		
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).($j+$rowNum), 'Tổng Cộng');                        
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+2).($j+$rowNum), $sumDoanhThu);                        
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+3).($j+$rowNum), $sumGiaVon);                        
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+4).($j+$rowNum), $sumLoiNhuanGop);                        
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+5).($j+$rowNum), $sumChiPhi);                        
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+6).($j+$rowNum), $sumLoiNhuanThuan);                        
                         
                        $objPHPExcel->getActiveSheet()
                                        ->getStyle(MyFunctionCustom::columnName($index).'3:'.MyFunctionCustom::columnName($index+6).'3')
                                        ->getFont()
                                        ->setBold(true);
                        $objPHPExcel->getActiveSheet()
                                        ->getStyle(MyFunctionCustom::columnName($index).($j+$rowNum).':'.MyFunctionCustom::columnName($index+6).($j+$rowNum))
                                        ->getFont()
                                        ->setBold(true);		
									
                        $objPHPExcel->getActiveSheet()
                        ->getStyle(MyFunctionCustom::columnName($index+2).($rowNum).':'.MyFunctionCustom::columnName($index+6).($j+$rowNum) )->getNumberFormat()
                        ->setFormatCode('#,##0');               
                         $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).'3:'.MyFunctionCustom::columnName($index).($j+$rowNum))
                                        ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                        $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).'3:'.MyFunctionCustom::columnName($index+6).($j+$rowNum) )
                                                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                }
		// end for sum one year		
				
		 //save file 
		 $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		 //$objWriter->save('MyExcel.xslx');

		 for($level=ob_get_level();$level>0;--$level)
		 {
			 @ob_end_clean();
		 }
		 header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		 header('Pragma: public');
		 header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		 header('Content-Disposition: attachment; filename="'.'Report Business Result'.'.'.'xlsx'.'"');
		 header('Cache-Control: max-age=0');				
		 $objWriter->save('php://output');			
		 Yii::app()->end();		            
            
        }
		        
    //  Trả về mảng key là nhảy auto 1 phần tử gồm 2 obj parent_obj vs sub_arr_id
    public static function getArrObjMaterialForExport()
    {
		$aRes=array();
        $criteria = new CDbCriteria;    
        $criteria->compare('t.parent_id', 0);
        $criteria->addCondition('t.id <>'.ID_MATERIAL_KHUYEN_MAI);
        $models = GasMaterials::model()->findAll($criteria);
		foreach($models as $item){
			$criteria = new CDbCriteria;    
			$criteria->compare('t.parent_id', $item->id);		
			$modelsSub = GasMaterials::model()->findAll($criteria);		
			$temp=array();
			$temp['parent_obj'] = $item;
			$temp['sub_arr_id'] = CHtml::listData($modelsSub,'id','id');  
			$aRes[]=$temp;			
		}
        return  $aRes;            
    } 

    //  Trả về mảng id parent trỏ đến mảng id obj con của vật tư
    // Key là id parent 
    public static function getArrObjMaterialForExportV1()
    {
		$aRes=array();
        $criteria = new CDbCriteria;    
        $criteria->compare('t.parent_id', 0);
        $criteria->addCondition('t.id <>'.ID_MATERIAL_KHUYEN_MAI);
        $models = GasMaterials::model()->findAll($criteria);
		foreach($models as $item){
			$criteria = new CDbCriteria;    
			$criteria->compare('t.parent_id', $item->id);		
			$modelsSub = GasMaterials::model()->findAll($criteria);		
			$aRes[$item->id] = CHtml::listData($modelsSub,'id','id');  
		}
        return  $aRes;            
    } 

	//  Trả về mảng obj cha trỏ đến mảng obj con  của vật tư
    public static function getArrObjMaterialParentAndSubObj()
    {
		$aRes=array();
        $criteria = new CDbCriteria;    
        $criteria->compare('t.parent_id', 0);
        $criteria->addCondition('t.id <>'.ID_MATERIAL_KHUYEN_MAI);
        $models = GasMaterials::model()->findAll($criteria);
		foreach($models as $item){
			$criteria = new CDbCriteria;    
			$criteria->compare('t.parent_id', $item->id);		
			$modelsSub = GasMaterials::model()->findAll($criteria);		
			$temp=array();
			$temp['parent_obj'] = $item;
			
			$temp['sub_arr_id'] = array();  
			$temp['sub_arr_id'] = CHtml::listData($modelsSub,'id','id');  
			if(count($modelsSub)>0)
			foreach($modelsSub as $subItem)
				$temp['sub_arr_id'][$subItem->id] = $subItem;
			
			$aRes[]=$temp;			
		}
        return  $aRes;            
    } 

		
		
    public static function getArrObjMaterialForAjaxMonthly()
    {
        $aRes=array();
        $criteria = new CDbCriteria;    
        $criteria->compare('t.id', ID_MATERIAL_KHUYEN_MAI);
        $models = GasMaterials::model()->findAll($criteria);
        foreach($models as $item){
			$criteria = new CDbCriteria;    
			$criteria->compare('t.parent_id', $item->id);		
			$modelsSub = GasMaterials::model()->findAll($criteria);		
			$temp=array();
			$temp['parent_obj'] = $item;
			$temp['sub_arr_id'] = CHtml::listData($modelsSub,'id','id');  
			$aRes[]=$temp;			
        }
        return  $aRes;            
    }   		
		
    public static function getArrMaterialNotInBusinessResult()
    {        
        $criteria = new CDbCriteria;    
        $criteria->compare('t.parent_id', ID_MATERIAL_KHUYEN_MAI);
        $modelsSub = GasMaterials::model()->findAll($criteria);		
        return  CHtml::listData($modelsSub,'id','id');  
    }   		
		
    /*
	luôn có 1 dạng chung cho mảng dataExport cho tất cả các báo cáo: [year][month][agent_id]....
	*/
	// 1.1
    public static function export_agent_revenue($model){
        $model->costs_year = is_array($model->costs_year)?$model->costs_year:array();
        $model->agent_id = is_array($model->agent_id)?$model->agent_id:array();
        $aYearMonthHasData = GetData::getUniqueYear($model->costs_year,'GasMaterialsSell','sell_month','sell_year');
        
        $dataExport = GetData::getDataAgentRevenueMonth($aYearMonthHasData,$model->agent_id, 'total_sell');
        $dataAgent = Users::getArrObjectUserByRole(ROLE_AGENT,$model->agent_id);
        ExportExcel::export_agent_revenue($dataExport,$dataAgent);
    }

     // 1.2   
    public static function export_agent_price_root($model){
        $model->costs_year = is_array($model->costs_year)?$model->costs_year:array();
        $model->agent_id = is_array($model->agent_id)?$model->agent_id:array();
        $aYearMonthHasData = GetData::getUniqueYear($model->costs_year,'GasMaterialsSell','sell_month','sell_year');
        
        $dataExport = GetData::getDataAgentRevenueMonth($aYearMonthHasData,$model->agent_id, 'total_root');
        $dataAgent = Users::getArrObjectUserByRole(ROLE_AGENT,$model->agent_id);
        ExportExcel::export_agent_price_root($dataExport,$dataAgent);
    }
        
    // 1.3    
    public static function export_agent_cost($model){
        $model->costs_year = is_array($model->costs_year)?$model->costs_year:array();
        $model->agent_id = is_array($model->agent_id)?$model->agent_id:array();
        $aYearMonthHasData = GetData::getUniqueYear($model->costs_year,'GasCostsMonthly','costs_month','costs_year');
        
        $dataExport = GetData::getDataAgentCost($aYearMonthHasData,$model->agent_id, 'total');
        $dataAgent = Users::getArrObjectUserByRole(ROLE_AGENT,$model->agent_id);
        ExportExcel::export_agent_cost($dataExport,$dataAgent);
    }
        	
    // 1.4    
    public static function export_agent_gross_profit($model){
        $model->costs_year = is_array($model->costs_year)?$model->costs_year:array();
        $model->agent_id = is_array($model->agent_id)?$model->agent_id:array();
        $aYearMonthHasData = GetData::getUniqueYear($model->costs_year,'GasMaterialsSell','sell_month','sell_year');
        
        // Doanh Thu
        $dataExportRevenue = GetData::getDataAgentRevenueMonth($aYearMonthHasData,$model->agent_id, 'total_sell');
        // Giá Vốn
        $dataExportPriceRoot = GetData::getDataAgentRevenueMonth($aYearMonthHasData,$model->agent_id, 'total_root');
		
        $dataAgent = Users::getArrObjectUserByRole(ROLE_AGENT,$model->agent_id);
        ExportExcel::export_agent_gross_profit($dataExportRevenue, $dataExportPriceRoot, $dataAgent);
    }        
		
	// 1.5	
    public static function export_agent_net_profit($model){
        $model->costs_year = is_array($model->costs_year)?$model->costs_year:array();
        $model->agent_id = is_array($model->agent_id)?$model->agent_id:array();
        $aYearMonthHasData = GetData::getUniqueYear($model->costs_year,'GasMaterialsSell','sell_month','sell_year');
        
            // Doanh Thu
        $dataExportRevenue = GetData::getDataAgentRevenueMonth($aYearMonthHasData,$model->agent_id, 'total_sell');
        // Giá Vốn
        $dataExportPriceRoot = GetData::getDataAgentRevenueMonth($aYearMonthHasData,$model->agent_id, 'total_root');
        // Chi Phí
        $aYearMonthHasDataCost = GetData::getUniqueYear($model->costs_year,'GasCostsMonthly','costs_month','costs_year');
        $dataExportCost = GetData::getDataAgentCost($aYearMonthHasDataCost,$model->agent_id, 'total');
			
        $dataAgent = Users::getArrObjectUserByRole(ROLE_AGENT,$model->agent_id);
        ExportExcel::export_agent_net_profit($dataExportRevenue, $dataExportPriceRoot, $dataExportCost, $dataAgent);
    }        
		
	// 1.6
    public static function export_cost_sum_total($model){
        $model->costs_year = is_array($model->costs_year)?$model->costs_year:array();
        $aYearMonthHasData = GetData::getUniqueYear($model->costs_year,'GasCostsMonthly','costs_month','costs_year');
        $dataExport = GetData::getDataCostSumTotal($aYearMonthHasData,$model, 'total');
		$aCost = GasCosts::getAllObj();
        ExportExcel::export_cost_sum_total($dataExport, $aCost);
    }
        			
		
		
	// 2.1	
    public static function export_agent_revenue_by_material($model){
        $model->costs_year = is_array($model->costs_year)?$model->costs_year:array();
        $model->agent_id = is_array($model->agent_id)?$model->agent_id:Users::getArrIdUserByRole(ROLE_AGENT);
        $aYearMonthHasData = GetData::getUniqueYear($model->costs_year,'GasMaterialsSell','sell_month','sell_year');
        
        $dataExport = GetData::getDataAgentRevenueByMaterial($aYearMonthHasData,$model->agent_id,'total_sell');
        $dataAgent = Users::getArrObjectUserByRole(ROLE_AGENT,$model->agent_id);
        $dataAllMaterial = GasMaterials::getArrAllObjMaterial();
        ExportExcel::export_agent_revenue_by_material($dataExport, $dataAgent, $dataAllMaterial);
    }	
	
    // 2.2
    public static function export_agent_price_root_by_material($model){
        $model->costs_year = is_array($model->costs_year)?$model->costs_year:array();
        $model->agent_id = is_array($model->agent_id)?$model->agent_id:Users::getArrIdUserByRole(ROLE_AGENT);
        $aYearMonthHasData = GetData::getUniqueYear($model->costs_year,'GasMaterialsSell','sell_month','sell_year');
        
        $dataExport = GetData::getDataAgentRevenueByMaterial($aYearMonthHasData,$model->agent_id,'total_root');
        $dataAgent = Users::getArrObjectUserByRole(ROLE_AGENT,$model->agent_id);
        $dataAllMaterial = GasMaterials::getArrAllObjMaterial();
        ExportExcel::export_agent_price_root_by_material($dataExport, $dataAgent, $dataAllMaterial);
    }	
	
	// 2.3
    public static function export_agent_output_by_material($model){
        $model->costs_year = is_array($model->costs_year)?$model->costs_year:array();
        $model->agent_id = is_array($model->agent_id)?$model->agent_id:Users::getArrIdUserByRole(ROLE_AGENT);
        $aYearMonthHasData = GetData::getUniqueYear($model->costs_year,'GasMaterialsSell','sell_month','sell_year');
        
        $dataExport = GetData::getDataAgentRevenueByMaterial($aYearMonthHasData,$model->agent_id,'qty');
        $dataAgent = Users::getArrObjectUserByRole(ROLE_AGENT,$model->agent_id);
        $dataAllMaterial = GasMaterials::getArrAllObjMaterial();
        ExportExcel::export_agent_output_by_material($dataExport, $dataAgent, $dataAllMaterial);
    }	
	// 2.4
    public static function export_agent_gross_profit_by_material($model){
        $model->costs_year = is_array($model->costs_year)?$model->costs_year:array();
        $model->agent_id = is_array($model->agent_id)?$model->agent_id:Users::getArrIdUserByRole(ROLE_AGENT);
        $aYearMonthHasData = GetData::getUniqueYear($model->costs_year,'GasMaterialsSell','sell_month','sell_year');
        
        // Doanh Thu
        $dataExportRevenue = GetData::getDataAgentRevenueByMaterial($aYearMonthHasData,$model->agent_id,'total_sell');
        // Giá Vốn
        $dataExportPriceRoot = GetData::getDataAgentRevenueByMaterial($aYearMonthHasData,$model->agent_id, 'total_root');
        
        $dataAgent = Users::getArrObjectUserByRole(ROLE_AGENT,$model->agent_id);
        $dataAllMaterial = GasMaterials::getArrAllObjMaterial();
        
        ExportExcel::export_agent_gross_profit_by_material($dataExportRevenue, $dataExportPriceRoot, $dataAgent, $dataAllMaterial);
    }	
	
    // 2.5
    public static function export_agent_by_costs_2($model){
        $model->costs_year = is_array($model->costs_year)?$model->costs_year:array();
        $model->agent_id = is_array($model->agent_id)?$model->agent_id:Users::getArrIdUserByRole(ROLE_AGENT);
        $aYearMonthHasData = GetData::getUniqueYear($model->costs_year,'GasCostsMonthly','costs_month','costs_year');
        
        $dataExport = GetData::getDataAgentByCosts2($aYearMonthHasData,$model,'total');
        $dataAgent = Users::getArrObjectUserByRole(ROLE_AGENT,$model->agent_id);
		$aCost = GasCosts::getAllObj();
        ExportExcel::export_agent_by_costs_2($dataExport, $dataAgent, $aCost);
    }		
    // 2.6
    public static function export_agent_by_costs_3($model){
        $model->costs_year = is_array($model->costs_year)?$model->costs_year:array();
        $model->costs_id = is_array($model->costs_id)?$model->costs_id:GasCosts::getArrIdCostsLikeMultiselect();
        $aYearMonthHasData = GetData::getUniqueYear($model->costs_year,'GasCostsMonthly','costs_month','costs_year');
        
        $dataExport = GetData::getDataAgentByCosts3($aYearMonthHasData,$model,'total');
        $dataAgent = Users::getArrObjectUserByRole(ROLE_AGENT,$model->agent_id);
		$aCost = GasCosts::getAllObj();
        ExportExcel::export_agent_by_costs_3($dataExport, $dataAgent, $aCost);
    }	
	
    // 3.1	
    public static function export_revenue_material_by_agent($model){
        $model->costs_year = is_array($model->costs_year)?$model->costs_year:array();
        $model->materials_id = is_array($model->materials_id)?$model->materials_id:GasMaterials::getCatByParentId(0,1);
        $aYearMonthHasData = GetData::getUniqueYear($model->costs_year,'GasMaterialsSell','sell_month','sell_year');
        
        $dataExport = GetData::getDataRevenueMaterialByAgent($aYearMonthHasData,$model,'total_sell');
        $dataAgent = Users::getArrObjectUserByRole(ROLE_AGENT);
        $dataAllMaterial = GasMaterials::getArrAllObjMaterial();
        ExportExcel::export_revenue_material_by_agent($dataExport, $dataAgent, $dataAllMaterial, $model);
    }	
	
	// 3.2
    public static function export_price_root_material_by_agent($model){
        $model->costs_year = is_array($model->costs_year)?$model->costs_year:array();
        $model->materials_id = is_array($model->materials_id)?$model->materials_id:GasMaterials::getCatByParentId(0,1);
        $aYearMonthHasData = GetData::getUniqueYear($model->costs_year,'GasMaterialsSell','sell_month','sell_year');
        
        $dataExport = GetData::getDataRevenueMaterialByAgent($aYearMonthHasData,$model,'total_root');
        $dataAgent = Users::getArrObjectUserByRole(ROLE_AGENT);
        $dataAllMaterial = GasMaterials::getArrAllObjMaterial();
        ExportExcel::export_price_root_material_by_agent($dataExport, $dataAgent, $dataAllMaterial, $model);
    }	
	
	// 3.3
    public static function export_output_material_by_agent($model){
        $model->costs_year = is_array($model->costs_year)?$model->costs_year:array();
        $model->materials_id = is_array($model->materials_id)?$model->materials_id:GasMaterials::getCatByParentId(0,1);
        $aYearMonthHasData = GetData::getUniqueYear($model->costs_year,'GasMaterialsSell','sell_month','sell_year');
        
        $dataExport = GetData::getDataRevenueMaterialByAgent($aYearMonthHasData,$model,'qty');
        $dataAgent = Users::getArrObjectUserByRole(ROLE_AGENT);
        $dataAllMaterial = GasMaterials::getArrAllObjMaterial();
        ExportExcel::export_output_material_by_agent($dataExport, $dataAgent, $dataAllMaterial, $model);
    }	
	
	// 3.4
    public static function export_gross_profit_material_by_agent($model){
        $model->costs_year = is_array($model->costs_year)?$model->costs_year:array();
        $model->materials_id = is_array($model->materials_id)?$model->materials_id:GasMaterials::getCatByParentId(0,1);
        $aYearMonthHasData = GetData::getUniqueYear($model->costs_year,'GasMaterialsSell','sell_month','sell_year');
        
        // Doanh Thu
        $dataExportRevenue = GetData::getDataRevenueMaterialByAgent($aYearMonthHasData,$model,'total_sell');
        // Giá Vốn
        $dataExportPriceRoot = GetData::getDataRevenueMaterialByAgent($aYearMonthHasData,$model, 'total_root');
        
        $dataAgent = Users::getArrObjectUserByRole(ROLE_AGENT);
        $dataAllMaterial = GasMaterials::getArrAllObjMaterial();
        ExportExcel::export_gross_profit_material_by_agent($dataExportRevenue, $dataExportPriceRoot, $dataAgent, $dataAllMaterial, $model);
    }
	
    
}