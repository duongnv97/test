<?php

/**
 * This is the model class for table "{{_customer_draft_detail}}".
 *
 * The followings are the available columns in table '{{_customer_draft_detail}}':
 * @property string $id
 * @property string $appointment_date
 * @property string $customer_draft_id
 * @property integer $note
 * @property integer $status
 * @property integer $status_cancel
 * @property integer $bill_duration
 * @property integer $call_id
 * @property integer $created_date_only
 * @property integer $created_date
 */
class CustomerDraftDetail extends BaseSpj
{
    public $number_customer, $date_call_from, $date_call_to, $only_view;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CustomerDraftDetail the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_customer_draft_detail}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            array('status,customer_draft_id','required','on'=>'create,update'),
            array('status, status_cancel', 'numerical', 'integerOnly'=>true),
            array('customer_draft_id', 'length', 'max'=>10),
            array('appointment_date,date_call_from,only_view,date_call_to', 'safe'),
            ['employee_id, number_customer, id, appointment_date, customer_draft_id, note, status, status_cancel', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'rEmployee' => array(self::BELONGS_TO, 'Users', 'employee_id'),
            'rCustomerDraft' => array(self::BELONGS_TO, 'CustomerDraft', 'customer_draft_id'),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'appointment_date' => 'Ngày hẹn',
            'note' => 'Ghi chú',
            'status' => 'Trạng thái',
            'status_cancel' => 'Lý do từ chối',
            'date_call_from' => 'Ngày hẹn từ',
            'date_call_to' => 'Đến',
            'bill_duration' => 'Giây gọi',
            'created_date' => 'Ngày tạo',
            
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        
        //        set ngày hẹn gọi lại
        if(!empty($this->date_call_from)){
            $dateFrom = MyFormat::dateConverDmyToYmd($this->date_call_from,'-');
            $criteria->addCondition('t.appointment_date >= "'.$dateFrom.'"');
        }
        if(!empty($this->date_call_to)){
            $dateTo = MyFormat::dateConverDmyToYmd($this->date_call_to,'-');
            $criteria->addCondition('t.appointment_date <= "'.$dateTo.'"');
        }
	$criteria->compare('t.employee_id',$this->employee_id);
	$criteria->compare('t.status',$this->status);
	$criteria->compare('t.status_cancel',$this->status_cancel);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /** @Author: HOANG NAM 28/06/2018
     *  @Todo: search by
     **/
    public function searchByCustomerDrafty()
    {
        $criteria=new CDbCriteria;
	$criteria->compare('t.customer_draft_id',$this->customer_draft_id);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    
    /** @Author: HOANG NAM 28/06/2018
     *  @Todo: get array status
     * */
    public function getArrayCallStatus($search = false) {
        $mCustomerDraft = new CustomerDraft();
        $aRes = $mCustomerDraft->getArrayCallStatus($search);
        return $aRes;
    }

    /** @Author: HOANG NAM 28/06/2018
     *  @Todo: cancel status
     * */
    public function getArrayStatusCancel() {
        $mCustomerDraft = new CustomerDraft();
        $aRes = $mCustomerDraft->getArrayStatusCancel();
        return $aRes;
    }
    
    /** @Author: HOANG NAM 27/06/2018
     *  @Todo: get rCustomerDraft
     **/
    public function getInfoCustomerDraft(){
        $mCusDraft = $this->rCustomerDraft;
        $result = '';
        if(!empty($mCusDraft)){
//            $result      = '<b>Thông tin khách hàng:</b>';
            $result     .= '<li>Tên: '.$mCusDraft->getName().'</li>';
            $result     .= '<li>SĐT: '.$mCusDraft->getPhoneClickCall().'</li>';
            $result     .= '<li>Địa chỉ <b>Excel</b>: '.$mCusDraft->getAddress().'</li>';
            $result     .= '<li>Quận huyện: '.$mCusDraft->getDistrictView(). ''.'</li>';
            $result     .= '<br><br>';
            return $result;
        }
        return '';
    }
    
    /** @Author: HOANG NAM 27/06/2018
     *  @Todo: can update & can delete
     **/
    public function canUpdate(){
        $cUid = MyFormat::getCurrentUid();
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole == ROLE_ADMIN){
            return true;
        }
        if($this->employee_id != $cUid){
            return false;
        }
        
        $dayAllow   = date('Y-m-d');// chỉ cho phép nhân viên CallCenter cập nhật trong ngày
        $dayAllow   = MyFormat::modifyDays($dayAllow, 10, '-');// cho phép update trong ngày
        return MyFormat::compareTwoDate($this->created_date_only, $dayAllow);
    }
    
    public function canDelete(){
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole == ROLE_ADMIN){
            return true;
        }
        return false;
    }
    
    /** @Author: HOANG NAM 27/06/2018
     *  @Todo: before save
     **/
    public function handleBeforeSave(){
        if(strpos($this->appointment_date, '/')){
            $this->appointment_date = MyFormat::dateConverDmyToYmd($this->appointment_date);
            MyFormat::isValidDate($this->appointment_date);
        }
        
        $mCusDraft = $this->rCustomerDraft;
        if(!empty($mCusDraft)){
            $mCusDraft->appointment_date    = $this->appointment_date;
            $mCusDraft->status              = $this->status;
            $mCusDraft->status_cancel       = $this->status_cancel;
            $mCusDraft->update();
            $this->employee_id              = $mCusDraft->employee_id;
        }
        if($this->isNewRecord){
            $this->created_date_only = date('Y-m-d');
        }
    }
    
    /** @Author: ANH DUNG Jul 05, 2018
     *  @Todo: format data render to update
     **/
    public function formatDataUpdate() {
        $this->appointment_date = MyFormat::dateConverYmdToDmy($this->appointment_date);
    }
    
    public function getAppointment($fomat = 'd/m/Y') {
        return MyFormat::dateConverYmdToDmy($this->appointment_date, $fomat);
    }
    public function getNote() {
        return $this->note;
    }
    public function getStatus() {
        $aStatus = $this->getArrayCallStatus();
        return !empty($aStatus[$this->status]) ? $aStatus[$this->status] : '';
    }
    public function getStatusCancel() {
        $aStatusCancel = $this->getArrayStatusCancel();
        return !empty($aStatusCancel[$this->status_cancel]) ? $aStatusCancel[$this->status_cancel] : '';
    }
    
    public function getCallDuration() {
        return $this->bill_duration;
    }
    public function getCallLinkView() {
        $mSell = new Sell();
        $mSell->call_id = $this->call_id;
        return $mSell->getUrlViewCall();
    }
    
    public function getCallViewOnGrid() {
        if(empty($this->call_id)){
            return '';
        }
        $res = $this->getCallDuration()."<br>{$this->getCallLinkView()}";
        return $res;
    }
    
    /** @Author: HOANG NAM 29/06/2018
     *  @Todo: is only view
     **/
    public function isOnlyView(){
        if(!empty($this->only_view)){
            return true;
        }
        return false;
    }
    
    /** @Author: HOANG NAM 30/06/2018
     *  @Todo: get customer draft
     **/
    public function getCustomerDraft(){
        return $this->rCustomerDraft;
    }
    
    /** @Author: ANH DUNG Jul 03, 2018
     *  @Todo: KH Excel update call id and time call after call out hang up
     **/
    public function setCallId() {
        $mCusDraft = $this->rCustomerDraft;
        if(empty($mCusDraft->phone)){
            return ;
        }
        $temp       = explode('-', $mCusDraft->phone);
        $aPhone     = [];
        foreach($temp as $item){
            $aPhone[] = $item * 1;
        }
        $mCall = new Call();
        $mCall->direction       = Call::DIRECTION_OUTBOUND;
        $mCall->date_from_ymd   = date('Y-m-d');
        $mCall->date_to_ymd     = $mCall->date_from_ymd;
        $aCallOut = $mCall->getByListPhoneNumber($aPhone);
        $maxTime = 0;
        foreach($aCallOut as $item){
            if($item->bill_duration > $maxTime){
                $maxTime        = $item->bill_duration;
                $this->call_id  = $item->id;
                break;
            }
        }
        $this->bill_duration    = $maxTime;
    }
    
    /** @Author: ANH DUNG Oct 19, 2018
     *  @Todo: get record by call id
     **/
    public function getByCallId() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.call_id = ' . $this->call_id);
        return CustomerDraftDetail::model()->find($criteria);
    }
    
}