<?php

/**
 * This is the  model class for table  "{{cms}}".
 *
 * The followings are the available columns in table '{{cms}}':
 * @property integer $id
 * @property string $title
 * @property string $banner 
 * @property string $cms_content
 * @property string $created_date
 * @property integer $display_order
 * @property integer $show_in_menu
 * @property string $place_holder_id
 * @property integer $creator_id
 * @property integer $status
 * @property string $short_content
 * @property string $link
 * @property string $meta_keywords
 * @property string $meta_desc
 */
class Cms extends BaseSpj
{
    public $makeNotify = 0, $image, $fromPopup=false;
    public $pathUploadMuti = 'upload/news/editor', $uploadMulti;
    
    const ID_PAGE_PROMOTION         = 3;
    const ID_PAGE_GUIDE             = 4;
    
    const CATEGORY_ID_OLD               = 1;
    const CATEGORY_ID_SYSTEM_NOTIFY     = 2;
    const CATEGORY_ID_USER_MANUAL       = 3;
    const CATEGORY_ID_COMMON_MISTAKES   = 4;
    const CATEGORY_ID_NOTIFY_BOMOI      = 5;
    const CATEGORY_ID_ERROR_GAS24H      = 6;
    const CATEGORY_ID_UPDATE_APP        = 7;
    const CATEGORY_ID_RULE_PVKH         = 8;
    const CATEGORY_ID_APP_GAS_SERVICE   = 9;
    const CATEGORY_ID_CONTACT_FOCUS     = 10;
    
    
    /** @Author: ANH DUNG Oct 07, 2018
     *  @Todo: id notify all employee 
     **/
    public function getArrayIdNotifyEmployee() {
        return [
            Cms::CATEGORY_ID_SYSTEM_NOTIFY,
            Cms::CATEGORY_ID_USER_MANUAL,
            Cms::CATEGORY_ID_COMMON_MISTAKES,
            Cms::CATEGORY_ID_ERROR_GAS24H,
            Cms::CATEGORY_ID_UPDATE_APP,
            Cms::CATEGORY_ID_RULE_PVKH,
            Cms::CATEGORY_ID_APP_GAS_SERVICE,
            Cms::CATEGORY_ID_CONTACT_FOCUS,
        ];
    }
    
    public function getArrayCategory() {
        return[
            Cms::CATEGORY_ID_SYSTEM_NOTIFY      => 'Thông báo hệ thống',
            Cms::CATEGORY_ID_USER_MANUAL        => 'Hướng dẫn sử dụng',
            Cms::CATEGORY_ID_COMMON_MISTAKES    => 'Lỗi APP PVKH - Gas Service',
            Cms::CATEGORY_ID_ERROR_GAS24H       => 'Lỗi App Gas24h',
            Cms::CATEGORY_ID_UPDATE_APP         => 'Cập nhật app PVKH, PTTT',
            Cms::CATEGORY_ID_RULE_PVKH          => 'Quy định nhân viên giao nhận',
            Cms::CATEGORY_ID_APP_GAS_SERVICE    => 'HDSD App Gas Service',
            Cms::CATEGORY_ID_CONTACT_FOCUS      => 'Liên hệ cấp quản lý',
            Cms::CATEGORY_ID_NOTIFY_BOMOI       => '[KH] Thông báo khách hàng bò mối',
        ];
    }
    
    public function getArrayApiListCategory() {
        return[
            Cms::CATEGORY_ID_RULE_PVKH          => 'Quy định nhân viên giao nhận',
            Cms::CATEGORY_ID_COMMON_MISTAKES    => 'Lỗi APP PVKH - Gas Service',
            Cms::CATEGORY_ID_ERROR_GAS24H       => 'Lỗi App Gas24h',
            Cms::CATEGORY_ID_SYSTEM_NOTIFY      => 'Thông báo hệ thống',
            Cms::CATEGORY_ID_UPDATE_APP         => 'Cập nhật app PVKH, PTTT',
            Cms::CATEGORY_ID_USER_MANUAL        => 'Hướng dẫn sử dụng',
            Cms::CATEGORY_ID_APP_GAS_SERVICE    => 'HDSD App Gas Service',
            Cms::CATEGORY_ID_CONTACT_FOCUS      => 'Liên hệ cấp quản lý',
        ];
    }
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Cms the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_cms}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('title', 'required', 'on'=> 'create, update'),
            array('category_id, title', 'required', 'on'=> 'CreateApp, UpdateApp'),
            array('category_id, time_send_notify, makeNotify, role_id, id, title, banner, cms_content, created_date, display_order, show_in_menu, place_holder_id, creator_id, status, short_content, link, meta_keywords, meta_desc', 'safe'),
            array('banner', 'file', 
                'types'=>'jpg, gif, png',
                'allowEmpty'=>true,
                'maxSize'   => ActiveRecord::getMaxFileSize(),
                'tooLarge'  =>'The file was larger than '.(ActiveRecord::getMaxFileSize()/1024).' KB. Please upload a smaller file.',
                ),
            array(
                'banner','match',
                'pattern'=>'/^[^\\/?*:&;{}\\\\]+\\.[^\\/?*:;{}\\\\]{3}$/', 
                'message'=>'Image files name cannot include special characters: &%$#',
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'place_holder' => array(self::BELONGS_TO, 'PlaceHolders', 'place_holder_id'),
            'rCreator' => array(self::BELONGS_TO, 'Users', 'creator_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label) 
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => 'Tiêu Đề',
            'slug' => 'Slug',
            'banner' => 'Banner',
            'cms_content' => 'Nội Dung',
            'created_date' => 'Ngày Tạo',
            'display_order' => 'Thứ Tự',
            'show_in_menu' => 'Hiển Thị Ở Trang Chủ',
            'place_holder_id' => 'Place Holder',
            'creator_id' => 'Người tạo',
            'status' => 'Trạng Thái',
            'short_content' => 'Nội Dung Ngắn',
            'link' => 'Link',
            'meta_keywords' => 'Meta Keywords',
            'meta_desc' => 'Meta Desc',
            'role_id' => 'Bộ phận áp dụng',
            'category_id' => 'Loại thông báo',
            'makeNotify' => 'Tạo notify thông báo đến Nhân viên / KH',
            'time_send_notify' => 'Thời gian gửi notify',
            'uploadMulti' => 'Upload hình ảnh',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('t.role_id',$this->role_id);
        $criteria->compare('title',$this->title,true);
        if(isset($this->created_date) && !empty($this->created_date))
        {
            $date = strtotime(str_replace('/','-',$this->created_date));
            $date = date('Y-m-d',$date);
            $criteria->compare('created_date',$date ,true);	
        }
        $criteria->compare('display_order',$this->display_order);
        $criteria->compare('show_in_menu',$this->show_in_menu);
        $criteria->compare('status',$this->status);
        $criteria->compare('slug',$this->slug,true);
        $criteria->addCondition('t.category_id ='.Cms::CATEGORY_ID_OLD);
        $criteria->order = 't.id DESC';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>50,
            ),
        ));
    }
    
    /** @Author: NGUYEN KHANH TOAN 2018
     *  @Todo:search list new thêm category_id
     *  @Param:
     **/
    public function searchListNew()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('t.role_id',$this->role_id);
        $criteria->compare('title',$this->title,true);
        if(isset($this->created_date) && !empty($this->created_date))
        {
            $date = strtotime(str_replace('/','-',$this->created_date));
            $date = date('Y-m-d',$date);
            $criteria->compare('created_date',$date ,true);	
        }
        $criteria->compare('display_order',$this->display_order);
        $criteria->compare('show_in_menu',$this->show_in_menu);
        $criteria->compare('status',$this->status);
        $criteria->compare('slug',$this->slug,true);
        $criteria->addCondition('t.category_id <>'.Cms::CATEGORY_ID_OLD);
        $criteria->compare('category_id',$this->category_id);
        $criteria->order = 't.id DESC';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>50,
            ),
        ));
    }

    /**
     * This is invoked before the record is saved.
     * @return boolean whether the record should be saved.
     */
    protected function beforeSave(){
        $this->image=CUploadedFile::getInstance($this,'banner');

        if($this->isNewRecord)
        {
            $this->creator_id   = MyFormat::getCurrentUid();
            if(!is_null($this->image))
            {
                $this->banner = $this->image->getName();
                // begin for resize image
                $this->image=EUploadedImage::getInstance($this,'banner');
                $this->image->maxWidth = 750;
                //$this->image->maxHeight = 87;
                // end for resize image       
                $this->image->saveAs('upload/cms/banner/'.$this->banner);
            }
        }else{
            $model =  Cms::model()->findByPk($this->id);
            if(!is_null($this->image))
                $this->banner = $this->image->getName();
            // not update banner
            //if(empty($this->banner))
              //      $this->banner = $model->banner;
            // update new banner
            if(!empty($this->banner) && $this->banner != $model->banner)
            {
                if(file_exists('upload/cms/banner/'.$model->banner) && !empty($model->banner)) 
                    unlink('upload/cms/banner/'.$model->banner);
                $this->banner = $this->image->getName();
                // begin for resize image
                $this->image=EUploadedImage::getInstance($this,'banner');
                $this->image->maxWidth = 750;
                //$this->image->maxHeight = 87;
                // end for resize image       
                $this->image->saveAs('upload/cms/banner/'.$this->banner);
            }
        }
        $this->formatDataDb();
        return parent::beforeSave();
    }
    
    /** @Author: ANH DUNG Oct 07, 2018
     *  @Todo: handle data before save
     **/
    public function formatDataDb() {
//        $this->cms_content = InputHelper::removeScriptTagOnly($this->cms_content);
        $this->short_content = InputHelper::removeScriptTagOnly($this->short_content);
        $this->slug = '';
        if(strpos($this->time_send_notify, '/')){
            $this->time_send_notify = MyFormat::datetimeToDbDatetime($this->time_send_notify);
        }
    }

    protected function beforeDelete() {
        if(file_exists('upload/cms/banner/'.$this->banner) && !empty($this->banner)) 
            unlink('upload/cms/banner/'.$this->banner);
        return true;
    }

    public static function getByIdAndStatus($id, $status){
        $criteria=new CDbCriteria;
        $criteria->compare('id',$id);
        $criteria->compare('status',$status);
        return Cms::model()->find($criteria);
    }        

    public static function getAllShowIndexByStatus($status, $show_in_menu=1){
        $criteria=new CDbCriteria;
        $criteria->order = 't.created_date DESC, t.display_order ASC';
        $criteria->compare('t.status',$status);            
        $criteria->compare('t.show_in_menu',$show_in_menu);            
        return Cms::model()->findAll($criteria);
    }       

    public static function getAllActive(){
        $criteria=new CDbCriteria;
        $criteria->order = 't.created_date DESC, t.display_order ASC';
        $criteria->compare('t.status', STATUS_ACTIVE);
        return Cms::model()->findAll($criteria);
    }        

    public static function getNewsPopup(){
        $criteria=new CDbCriteria;
        $criteria->order = 't.created_date DESC';
        $criteria->compare('t.status', STATUS_ACTIVE);
        $criteria->compare('t.show_in_menu', STATUS_ACTIVE);
        return Cms::model()->find($criteria);
    }            

    public static function getLatestNews(){
        $criteria=new CDbCriteria;
        $criteria->order = 't.created_date DESC';
        $criteria->compare('t.status', STATUS_ACTIVE);            
        return Cms::model()->find($criteria);
    }            

    public function activate()
    {
        $this->status = 1;
        $this->update(array('status'));
    }

    public function deactivate()
    {
        $this->status = 0;
        $this->update(array('status'));
    }    

    public function activateField($field_name)
    {
        $this->$field_name = 1;
        $this->created_date = date('Y-m-d H:i:s');
        $this->update(array($field_name, 'created_date'));
    }

    public function deactivateField($field_name)
    {
        $this->$field_name = 0;
        $this->update(array($field_name));
    }
    
    public function canUpdate() {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        if($cRole == ROLE_ADMIN){
            return true;
        }
        return $cUid == $this->creator_id;
    }
    
    public function getCreatedBy($field_name = 'first_name') {
        $mUser = $this->rCreator;
        if($mUser){
            return $mUser->$field_name;
        }
        return '';
    }
        
    /** @Author: ANH DUNG Now 27, 2017
     *  @Todo: get app view 
     **/
    public function getAppView() {
        $mAppPromotion = new AppPromotion();
        $model = self::model()->findByPk($this->id);
        if(empty($model)){
            throw new Exception('Yêu cầu không hợp lệ');
        }
        $linkCss = '<link href="http://spj.daukhimiennam.com/themes/gas/css/app_style.css?v=1" rel="stylesheet" />';
        $aRes = [];
        $aRes['title']      = $model->title;
//        $aRes['content']    = "$linkCss<div class='document'>$model->cms_content</div>";
        $aRes['content']    = $mAppPromotion->buildHtmlApp($model->cms_content, $linkCss);
        return $aRes;
    }
    
    public function getArrayRole() {
        $model = new HrSalaryReports();
        return $model->getArrayRoleSalary();
    }
    
    /** @Author: ANH DUNG Aug 23, 2018
     **/
    public function getRole() {
        $session=Yii::app()->session;
        if(!isset($session['ROLE_NAME_USER']) || !isset($session['ROLE_NAME_USER'][$this->role_id]))
                $session['ROLE_NAME_USER'] = Roles::getArrRoleName();
        return isset($session['ROLE_NAME_USER'][$this->role_id]) ? $session['ROLE_NAME_USER'][$this->role_id] : '';
    }
    
    /** @Author: ANH DUNG Aug 23, 2018
     *  @Todo: get all record by role
     **/
    public function getByRole() {
        $criteria = new CDbCriteria();
        $criteria->compare("t.role_id", $this->role_id);
        $criteria->compare('t.status', STATUS_ACTIVE);
        return self::model()->findAll($criteria);
    }
    
    public function getCategoryId(){
        $aCategoryId = $this->getArrayCategory();
        return isset($aCategoryId[$this->category_id]) ? $aCategoryId[$this->category_id] : "";
    }
    
    public function getTitle() {
        return isset($this->title) ? $this->title : "";
    }
    
    public function getCmsContent() {
        return isset($this->cms_content) ? $this->cms_content : "";
    }
    
    public function getShortContent() {
        return isset($this->short_content) ? $this->short_content : "";
    }
    
    public function getLinkWeb() {
        return isset($this->link) ? $this->link : "";
    }
    
    public function getCreatedDate($fomat = 'd/m/Y H:i') {
        return MyFormat::dateConverYmdToDmy($this->created_date, $fomat);
    }
    
    /** @Author: NGUYEN KHANH TOAN Sep 10, 2018
     *  @Todo: get view cms
     **/
    public function getNewsView() {
        $criteria = new CDbCriteria();
        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.status', STATUS_ACTIVE);
        return Cms::model()->find($criteria);
    }
    
    /** @Author: ANH DUNG Oct 26, 2018
     *  @Todo: đếm số item trong mỗi category
     **/
    public function countRecordInCategory() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.status = ' . STATUS_ACTIVE);
        $criteria->select   = 'COUNT(t.id) as id, t.category_id';
        $criteria->group    = 't.category_id';
        $models = Cms::model()->findAll($criteria);
        return CHtml::listData($models, 'category_id', 'id');
    }
    
    /** @Author: NGUYEN KHANH TOAN Sep 08 2018
     *  @Todo: handle listing category api
     *  @Param:
     **/
    public function handleApiListCategory(&$result, $q) {
        $aCountRecord = $this->countRecordInCategory();
        // 1. get list order by user id
        $result['message']  = 'L';
        $result['record']   = [];
        $aApiListType = $this->getArrayApiListCategory();
        foreach($aApiListType as $key => $type)
        {
            // Return array instead object for cms response
            $count = isset($aCountRecord[$key]) ? "[{$aCountRecord[$key]}] " : '';
            $result['record'][]         = [
                'id'    => $key,
                'name'  => $count . $aApiListType[$key],
            ];
        }
    }
    
    /** @Author: NGUYEN KHANH TOAN  Sep 10, 2018 -- handle listing api */
    public function handleApiItemList(&$result, $q) {
        // 1. get list order by user id
        $dataProvider   = $this->ApiListingCms($q);
        $models         = $dataProvider->data;
        $CPagination    = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page']   = $CPagination->pageCount;
        
        $result['message']  = 'L';
        $result['record']   = [];
        
        if( $q->page >= $CPagination->pageCount ){
            $result['record'] = [];
        }else{
            foreach($models as $model){
                $result['record'][]         = $model->formatAppItemListCms();
            }
        }
    }
    
    /**
    * @Author: NGUYEN KHANH TOAN Sep 10 2018
    * @Todo: get data listing 
    * @param: $q object post params from app client
    */
    public function ApiListingCms($q) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.status = 1');
        $criteria->addCondition('t.category_id = ' . (int)$q->category_id);
        $criteria->order = 't.id DESC';
        $dataProvider=new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize' => GasAppOrder::API_LISTING_ITEM_PER_PAGE,
                'currentPage' => (int)$q->page,
            ),
          ));
        return $dataProvider;
    }
    
    /** @Author: NGUYEN KHANH TOAN Sep 10 2018
     *  @Todo: dùng chung để format item trả xuống list + view của app Gas24h
     **/
    public function formatAppItemListCms() {
        $link = Yii::app()->createAbsoluteUrl('api/cms/itemView', ['id' => $this->id]);
//        $link = '/api/cms/itemView/id/'.$this->id;
        $temp = [];
        $temp['id']                 = $this->id;
        $temp['title']              = $this->getTitle(). " ({$this->getCreatedDate('d/m/Y')})";
        $temp['link_web']           = $link;
        $temp['created_date']       = $this->getCreatedDate();
        $temp['short_content']      = $this->getShortContent();
        return $temp;
    }
    
    /** @Author: KHANH TOAN Oct 6, 2018
     *  @Todo: kiểm tra xem có check vào notify cho App Gas Service không
     **/
    public function makeNotifyGas24h() {
        if(!$this->makeNotify || $this->status == STATUS_INACTIVE){
            return ;
        }
        $mScheduleNotify                = new GasScheduleNotify();
        $mScheduleNotify->obj_id        = $this->id;
        $mScheduleNotify->time_send     = $this->time_send_notify;
        $mScheduleNotify->title         = '[Thông báo] '.$this->title;

        $mUsersTokens = new UsersTokens();
        if($this->category_id == Cms::CATEGORY_ID_NOTIFY_BOMOI){
            $mScheduleNotify->aUserAndroid   = $mUsersTokens->getUserGasServiceByPlatform(UsersTokens::LOGIN_ANDROID, ROLE_CUSTOMER, 0);
            $mScheduleNotify->aUserIos       = $mUsersTokens->getUserGasServiceByPlatform(UsersTokens::LOGIN_IOS, ROLE_CUSTOMER, 0);
            
        }elseif(in_array($this->category_id, $this->getArrayIdNotifyEmployee())){
            $mScheduleNotify->aUserAndroid  = $mUsersTokens->getUserGasServiceByPlatform(UsersTokens::LOGIN_ANDROID, 0, 0);
            $mScheduleNotify->aUserIos      = $mUsersTokens->getUserGasServiceByPlatform(UsersTokens::LOGIN_IOS, 0, 0);
        }else{
            return ;
        }
        
        $mScheduleNotify->addMultiRow($mScheduleNotify->aUserAndroid, UsersTokens::PLATFORM_ANDROID, GasScheduleNotify::CMS_NEWS);
        $mScheduleNotify->addMultiRow($mScheduleNotify->aUserIos, UsersTokens::PLATFORM_IOS, GasScheduleNotify::CMS_NEWS);
    }   
    
    /** @Author: KHANH TOAN 2018
     *  @Todo: get array user bo moi have token login app 
     **/
    public function Close_getArrayUser(&$aUid){
        $criteria = new CDbCriteria;
        $sParamsIn = implode(',', UsersTokens::$ARR_LOGIN_MOBILE);
        $criteria->addCondition("t.type IN ($sParamsIn)");
        $criteria->addCondition("t.role_id <> ".ROLE_CUSTOMER);
        $models = UsersTokens::model()->findAll($criteria);
        $aUid = CHtml::listData($models, 'user_id', 'user_id');
    }
    
    /** @Author: KHANH TOAN 2018
     *  @Todo:get array employee have token login app 
     **/
    public function Close_getArrayCustomerBoMoi(&$aUid) {
        $criteria = new CDbCriteria;
        $sParamsIn = implode(',', CmsFormatter::$aTypeIdMakeUsername);
        $criteria->addCondition("t.role_id =". ROLE_CUSTOMER);
        $criteria->addCondition("t.is_maintain IN ($sParamsIn)");
        $models = UsersTokens::model()->findAll($criteria);
        $aUid = CHtml::listData($models, 'user_id', 'user_id');
    }
    
        
    /** @Author: ANH DUNG Oct 07, 2018
     *  @Todo: count view news on app - by notify
     **/
    public function getUserViewOnApp() {
        $model = new StaView();
        $model->type    = StaView::TYPE_2_GAS_SERVICE_ANOUCE;
        $model->obj_id  = $this->id;
        return ActiveRecord::formatCurrency($model->countByObjId());
    }
    /** @Author: ANH DUNG Oct 21, 2018
     *  @Todo: get url delete file 
     **/
    public function multiFileGetUrlDelete($fileName) {
        return Yii::app()->createAbsoluteUrl('admin/cms/listNewUpdate', array('id'=>$this->id, 'delete_file'=>$fileName));
    }
    
    /** @Author: ANH DUNG Dec 09, 2018
     *  @Todo: count total notify of this news
     **/
    public function countNotifyInsert() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.type = ' . GasScheduleNotify::CMS_NEWS);
        $criteria->addCondition('t.obj_id = ' . $this->id);
        return GasScheduleNotify::model()->count($criteria);
    }
    
    /** @Author: ANH DUNG Jan 10, 2019
     *  @Todo: get html link file css app
     **/
    public function getAppLinkCss() {
//        $linkCss    = Yii::app()->createAbsoluteUrl('')."/themes/gas/css/AppWebView.css?time=".time();
        $linkCss    = Yii::app()->createAbsoluteUrl('')."/themes/gas/css/AppWebView.css";
        return "<link href='$linkCss' rel='stylesheet' type='text/css'/>";
    }
    
            
}