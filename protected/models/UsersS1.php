<?php
/** Sep 08, 2016
 * This is the model class will use static function from Users
 */
class UsersS1
{
    /**
     * @Author: ANH DUNG Sep 08, 2016
     * @Todo: lấy info KH dân dụng
     */
    public static function getHgdInfo($mUser) {
        $res = "";
        $serial             = $mUser->getUserRefField("contact_technical_name");
        $hgd_thuong_hieu    = $mUser->getVoPTTT();
        $hgd_doi_thu        = $mUser->getUserRefField("contact_technical_landline");
        
        if($serial != ""){
            $res .= "<br><b>Seri</b>: $serial";
        }
        if($hgd_thuong_hieu != ""){
            $res .= "<br><b>Thương hiệu</b>: $hgd_thuong_hieu";
        }
        if($hgd_doi_thu != ""){
            $res .= "<br><b>Đối thủ</b>: $hgd_doi_thu";
        }
        if(!empty($mUser->payment_day)){
            $res .= "<br><b>Thời gian dự kiến hết gas</b>: $mUser->payment_day ngày";
        }
        $invest = GasConst::getInvestHgdText($mUser->ip_address);
        if(!empty($invest)){
            $res .= "<br><b>Đầu tư</b>: $invest";
        }
        return $res;
    }
    
    /**
     * @Author: ANH DUNG Sep 09, 2016
     * @Todo: serch KH dân dụng
     */
    public static function searchHgd($mUser) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.role_id', ROLE_CUSTOMER);
        $criteria->compare('t.type', CUSTOMER_TYPE_STORE_CARD);
        $criteria->compare('t.is_maintain', $mUser->is_maintain);
        $criteria->compare('t.parent_id', $mUser->parent_id);
        $criteria->compare('t.province_id', $mUser->province_id);
        $criteria->compare('t.district_id', $mUser->district_id);
        if(!empty($mUser->created_by)){
            $criteria->addCondition("t.created_by=$mUser->created_by OR t.sale_id=$mUser->created_by");
        }
        $date_from  = MyFormat::dateDmyToYmdForAllIndexSearch($mUser->date_from);
        $date_to    = MyFormat::dateDmyToYmdForAllIndexSearch($mUser->date_to);
//        $criteria->addBetweenCondition('DATE(t.created_date)', $date_from, $date_to); 
        DateHelper::searchBetween($date_from, $date_to, 'created_date_bigint', $criteria, true);
        $criteria->limit = $mUser->pageSize;
        $criteria->order = 't.id DESC';
        return Users::model()->findAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG Sep 19, 2016
     * @Todo: update monitoring_id for sale_id (ccs) in table SellDetail
     */
    public static function cronSellUpdateMonitoringId() {
        return ;// Close May 08, 2017, không cần thiết phải set monitor nữa
        $from = time();
        $aMonitor = GasOneMany::getManyOneIdByType('', ONE_MONITORING_MARKET_DEVELOPMENT);
        foreach($aMonitor as $sale_id => $monitoring_id){
            $criteria = new CDbCriteria();
            $criteria->compare('sale_id', $sale_id);
//            $criteria->compare('created_date_only', date("Y-m-d"));
            $criteria->addCondition("monitoring_id IS NULL");
            $aUpdate = array('monitoring_id' => $monitoring_id);
            SellDetail::model()->updateAll($aUpdate, $criteria);
        }
        
        $to = time();
        $second = $to-$from;
        $info = "cronSellUpdateMonitoringId  ".count($aMonitor)." done in: $second Second  <=> ".($second/60)." Minutes";
        Logger::WriteLog($info);
    }
    
    /**
     * @Author: ANH DUNG Oct 26, 2016
     * @Todo: get array agent to cache
     */
    public static function getJsonModelByRole($role_id, $needMore = []) {
        $aRes = array();
        $models = Users::getArrObjectUserByRole($role_id, [], $needMore);
        foreach($models as $model){
            $aRes[$model->id] = $model->getAttributes();
        }
        return json_encode($aRes, JSON_UNESCAPED_UNICODE);
    }
    
    /**
     * @Author: ANH DUNG Now 14, 2016
     * @Todo: get array User
     */
    public static function getArrObjectUserByRole($role_id='', $aUid=array(), $needMore=array())
    {
        $criteria = new CDbCriteria;
        if(is_array($role_id)){// Add Dec 16, 2015 khong ro co anh huong den cho nao ko
            $criteria->addInCondition('t.role_id', $role_id);
        }else{
            if(!empty($role_id)){
                $criteria->compare('t.role_id', $role_id);
            }
        }
        if(count($aUid)>0){
//            $criteria->addInCondition('t.id',$aUid);
            $sParamsIn = implode(',', $aUid);// Sep 23, 2016 be careful For Big bug
            $criteria->addCondition("t.id IN ($sParamsIn)");
        }
        if(isset($needMore['parent_id']) && !empty($needMore['parent_id'])){
            $criteria->addCondition("t.parent_id={$needMore['parent_id']}");
        }
        if(isset($needMore['gender'])){ // xem có lấy kho hay đại lý
            // vì hệ thống chia ra kho và đại lý, nên có chỗ chỉ lấy đại lý, không lấy kho
            $criteria->addCondition("t.gender={$needMore['gender']}");
        }
        if(isset($needMore['status'])){
            $criteria->compare('t.status', $needMore['status']);
        }

        $criteria->order = "t.id asc";
        $models = Users::model()->findAll($criteria);
        $res=array();
        foreach ($models as $item){
            $res[$item->id] = $item;
        }
        return  $res;         
    }
    
    /**
     * @Author: ANH DUNG Nov 15, 2016
     * @Todo: kiểm tra agent có dc sửa giá gas ko,
     */
    public static function canUpdatePriceGas($mAgent) {
        $aProvinceDisable = array_merge(GasOrders::$TARGET_ZONE_PROVINCE_HCM, GasOrders::$TARGET_ZONE_PROVINCE_MIEN_DONG, GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TAY);
        if(in_array($mAgent->province_id, $aProvinceDisable)){
            return 0;
        }
        return 1;
    }
    
    
}