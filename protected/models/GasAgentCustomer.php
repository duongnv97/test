<?php

/**
 * This is the model class for table "{{_gas_agent_customer}}".
 *
 * The followings are the available columns in table '{{_gas_agent_customer}}':
 * @property string $id
 * @property integer $agent_id
 * @property integer $customer_id
 */
class GasAgentCustomer extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GasAgentCustomer the static model class
	 */
    public $code_account;
    public $first_name;
    public $name_agent;
    public $address;
    public $sale_id;
    public $province_id;
    public $channel_id;
    public $payment_day;
    public $autocomplete_name;

    
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_agent_customer}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
            return array(
                    array('id, agent_id, customer_id,employee_maintain_id,maintain_agent_id', 'safe'),
                    array('code_account,first_name,name_agent,address,sale_id,province_id,channel_id,payment_day', 'safe'),
            );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'agent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
                'customer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                    'id' => 'ID',
                    'agent_id' => 'Đại Lý',
                    'customer_id' => 'Khách Hàng',
                    'email' => 'Email',
                    'last_name' => 'Full Name',			
                    'last_logged_in' => 'Last Logged In',
                    'status' => 'Trạng Thái',
                    'phone' => 'Phone',
                    'full_name' => 'Name',
                'first_name' => 'Tên KH',
                'name_agent' => 'Tên Phụ',
                'code_account' => 'Mã KH Kế Toán',
                'code_bussiness' => 'Mã KH Kinh Doanh',
                'address' => 'Địa Chỉ',
                'province_id' => 'Tỉnh',
                'channel_id' => 'Kênh',
                'district_id' => 'Khu Vực',
                'storehouse_id' => 'Chi Nhánh Giao',
                'sale_id' => 'Nhân Viên Sale',
                'payment_day' => 'Hạn Thanh Toán',

            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;

            $criteria->compare('t.id',$this->id,true);
            $criteria->compare('t.agent_id',$this->agent_id);
            $criteria->compare('t.customer_id',$this->customer_id);

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>array(
                        'pageSize'=> 50,
                    ),
            ));
    }

/*
public function activate()
{
    $this->status = 1;
    $this->update();
}

public function deactivate()
{
    $this->status = 0;
    $this->update();
}
    */

    public function defaultScope()
    {
            return array(
                    //'condition'=>'',
            );
    }

    public function searchAgentCustomer(){
        $criteria=new CDbCriteria;
        $criteria->compare('t.agent_id',$this->agent_id);
        $criteria->compare('t.customer_id',$this->customer_id);            
        $criteria->order = 'id DESC';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
            'pageSize'=> 50,
        ),
        ));        
    }   

    public static function getCustomerOfAgent($agent_id){
        $criteria=new CDbCriteria;
        $criteria->compare('t.agent_id',$agent_id);
        return  CHtml::listData(GasAgentCustomer::model()->findAll($criteria),'customer_id','customer_id');      
    }
    
    public static function getCustomerOfAgentAllModel($agent_id){
        $criteria=new CDbCriteria;
        $criteria->compare('t.agent_id',$agent_id);
        return  GasAgentCustomer::model()->findAll($criteria);
    }

    /*
     * Nguyen Dung 10-04-2013
     * to do: save employee maintain agent employee_maintain_id,maintain_agent_id
     employee_maintain_id: la nhan vien bao tri
     maintain_agent_id: la dai ly dc employee_maintain check theo doi
     GasAgentCustomer::saveEmployeeMaintainAgent();
     */
    public static function saveEmployeeMaintainAgent($employee_maintain_id) {
        // từ giờ sẽ dùng bảng này để giới hạn đại lý theo dõi của từng user giám sát
        GasAgentCustomer::model()->deleteAll('employee_maintain_id=' . $employee_maintain_id);
        if (isset($_POST['Users']['maintain_agent_id']) && is_array($_POST['Users']['maintain_agent_id']) && count($_POST['Users']['maintain_agent_id']) > 0) {
            foreach ($_POST['Users']['maintain_agent_id'] as $item) {
                $m = new GasAgentCustomer();
                $m->employee_maintain_id = $employee_maintain_id;
                $m->maintain_agent_id = $item;
                $m->save();
            }
        }
    }
    
    /**
     * @Author: ANH DUNG Mar 23, 2015
     * @Todo: lấy mảng agent_id của một list giám sát
     * @Param: $aUidMonitor
     * @Return: array[monitor_id] => array( list agent id)
     */
    public static function GetListAgentIdOfListMonitorId($aMonitoringId) {
        $aRes = array();
        $criteria=new CDbCriteria;
//        $criteria->addInCondition('t.employee_maintain_id', $aMonitoringId);
        $sParamsIn = implode(',', $aMonitoringId);
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.employee_maintain_id IN ($sParamsIn)");
        }
        
        $models = self::model()->findAll($criteria);
        foreach($models as $item){
            $aRes[$item->employee_maintain_id][] = $item->maintain_agent_id;
        }
        return $aRes;
    }


    // giờ bảng này để quản lý mỗi Giám sát có 1 số lượng đại lý theo dõi
    public static function getEmployeeMaintainAgent($employee_maintain_id) {
        $criteria=new CDbCriteria;
        $criteria->compare('t.employee_maintain_id',$employee_maintain_id);
        return  CHtml::listData(GasAgentCustomer::model()->findAll($criteria),'maintain_agent_id','maintain_agent_id'); 
    }
    // giờ bảng này để quản lý mỗi Giám sát có 1 số lượng đại lý theo dõi

    /**
     * @Author: ANH DUNG Apr 15, 2014
     * @Todo: Feb 25, 2014, khởi tạo session list agent id để thêm vào $criteria trong phần list của các danh sách
     * @Param: &$criteria  criteria
     * @Param: $nameField  t.agent_id
     */
    public static function addInConditionAgent(&$criteria, $nameField, $operator='AND'){
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole != ROLE_SUB_USER_AGENT){
            GasAgentCustomer::initSessionAgent();
            $session=Yii::app()->session;
            if(isset($session['LIST_AGENT_OF_USER']) && count($session['LIST_AGENT_OF_USER'])){
//                $criteria->addInCondition ("$nameField", $session['LIST_AGENT_OF_USER']);
                $sParamsIn = implode(',', $session['LIST_AGENT_OF_USER']);
                $criteria->addCondition("$nameField IN ($sParamsIn)", $operator);
            }
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 13, 2016
     * @Todo: lấy query in condition agent
     */
    public static function addInConditionAgentGetQuery($nameField, $operator='AND'){
        $cRole = MyFormat::getCurrentRoleId();
        if($cRole != ROLE_SUB_USER_AGENT){
            GasAgentCustomer::initSessionAgent();
            $session=Yii::app()->session;
            if(isset($session['LIST_AGENT_OF_USER']) && count($session['LIST_AGENT_OF_USER'])){
                $sParamsIn = implode(',', $session['LIST_AGENT_OF_USER']);
                return "$operator $nameField IN ($sParamsIn)";
            }
        }
    }
    
    /**
     * @Author: ANH DUNG Jan 27, 2016 FIX thêm condition trong limit agent
     * @Todo: khởi tạo session list agent id để thêm vào $criteria trong phần list của các danh sách
     * @Param: &$criteria  criteria
     * @Param: $nameField  t.agent_id
     * @param: $moreCondition like: OR t.uid_login=$cUid
     */
    public static function FixAddConditionAgent(&$criteria, $nameField, $moreCondition, $cRole, $cUid){
        if($cRole != ROLE_SUB_USER_AGENT){
            GasAgentCustomer::initSessionAgentFixForApp($cRole, $cUid);
            $session=Yii::app()->session;
            if(isset($session['LIST_AGENT_OF_USER']) && count($session['LIST_AGENT_OF_USER'])){
                $criteria->addCondition (""
                    . "( $nameField IN (".  implode(',', $session['LIST_AGENT_OF_USER'] ).")  $moreCondition )"
                    . "");
            }
        }
    }

    public static function initSessionAgent(){
        $session=Yii::app()->session;
        // giờ bảng này để quản lý mỗi Giám sát có 1 số lượng đại lý theo dõi
        // dùng để giới hạn đại lý theo dõi cho mỗi loại user. dc set trong lúc tạo user login
//            if(!isset($session['LIST_AGENT_OF_USER']) && (Yii::app()->user->role_id == ROLE_AGENT || chưa rõ loại user nào nữa? cứ để cho load hết, khi nào có ý kiến thì giới hạn lại. Yii::app()->user->role_id == ROLE_AGENT )){	
        // phải kiểm tra (isset(Yii::app()->user->id)  vì khi login chưa có user id thì nó lấy list bị sai hết
        if(isset(Yii::app()->user->id) && !isset($session['LIST_AGENT_OF_USER']) && Yii::app()->user->role_id != ROLE_SUB_USER_AGENT){
            $session['LIST_AGENT_OF_USER'] = GasAgentCustomer::getEmployeeMaintainAgent(Yii::app()->user->id);
        }
    }
    
    /**
     * @Author: ANH DUNG Jan 27, 2016
     * @Todo: vì app không có được $session nên sẽ phải làm thế này
     */
    public static function initSessionAgentFixForApp($cRole, $cUid){
        $session=Yii::app()->session;
        // giờ bảng này để quản lý mỗi Giám sát có 1 số lượng đại lý theo dõi
        // dùng để giới hạn đại lý theo dõi cho mỗi loại user. dc set trong lúc tạo user login
//            if(!isset($session['LIST_AGENT_OF_USER']) && (Yii::app()->user->role_id == ROLE_AGENT || chưa rõ loại user nào nữa? cứ để cho load hết, khi nào có ý kiến thì giới hạn lại. Yii::app()->user->role_id == ROLE_AGENT )){	
        // phải kiểm tra (isset(Yii::app()->user->id)  vì khi login chưa có user id thì nó lấy list bị sai hết
        if(!isset($session['LIST_AGENT_OF_USER']) && $cRole != ROLE_SUB_USER_AGENT){
            $session['LIST_AGENT_OF_USER'] = GasAgentCustomer::getEmployeeMaintainAgent($cUid);
        }
        return $session['LIST_AGENT_OF_USER'];
    }
    
}