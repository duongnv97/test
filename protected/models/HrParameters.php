<?php

/**
 * This is the model class for table "{{_hr_parameters}}".
 *
 * The followings are the available columns in table '{{_hr_parameters}}':
 * @property string $id
 * @property string $role_id
 * @property string $method
 * @property string $name
 * @property string $created_date
 * @property string $created_by
 * @property integer $status
 */
class HrParameters extends CActiveRecord {

    //-----------------------------------------------------
    // Properties
    //-----------------------------------------------------
    public $status_active = DomainConst::DEFAULT_STATUS_ACTIVE;
    public $status_in_active = DomainConst::DEFAULT_STATUS_INACTIVE;

    const ROLE_GLOBAL_ID = -1; //role chung
    const ROLE_GLOBAL_NAME = 'Chung';

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return HrParameters the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{_hr_parameters}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('name, method, position_work, position_work_list', 'required', 'on' => 'create,update'),
            array('method, name, created_by, status, position_work, position_work_list', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
//            'rRole' => array(self::BELONGS_TO, 'Roles', 'role_id'),
            'rUser' => array(self::BELONGS_TO, 'Users', 'created_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id'            => DomainConst::CONTENT00003,
            'role_id'       => DomainConst::CONTENT10004,
            'method'        => DomainConst::CONTENT10013,
            'name'          => DomainConst::CONTENT10014,
            'created_date'  => DomainConst::CONTENT00010,
            'created_by'    => DomainConst::CONTENT00054,
            'status'        => DomainConst::CONTENT00026,
            'position_work' => 'Bộ phận làm việc chính',
            'position_work_list' => 'Bộ phận làm việc',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        $criteria = new CDbCriteria;
        if (!empty($this->name)) {
            $criteria->addCondition('t.name like "%' . $this->name . '%"');
        }
        if (!empty($this->role_id)) {
            $criteria->addCondition('t.role_id =' . $this->role_id);
        }
        if (!empty($this->method)) {
            $criteria->addCondition('t.method like "%' . $this->method . '%"');
        }
        if (!empty($this->position_work)) {
            $list = implode(",", $this->position_work);
            $criteria->addCondition('t.position_work IN (' . $list . ')');
        }
        if (!empty($this->status)) {
            $criteria->addCondition('t.status =' . $this->status);
        }

        $sort = new CSort();
        $sort->attributes = array(
            'name' => 'name',
            'position_work' => 'position_work',
            'method' => 'method',
        );
        $sort->defaultOrder = 't.id desc';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));
    }

    //-----------------------------------------------------
    // Parent override methods
    //-----------------------------------------------------
    /**
     * Override before save method
     * @return Parent result
     */
    public function beforeSave() {
        if ($this->isNewRecord) {   // Add
            // Handle created by
            if (empty($this->created_by)) {
                $this->created_by = MyFormat::getCurrentUid();
            }
            // Handle created date
            $this->created_date = CommonProcess::getCurrentDateTime();
        } else {                    // Update
        }
        return parent::beforeSave();
    }
    
    /**
     * Override before delete method
     * @return Parent result
     */
    public function beforeDelete() {
        $retVal = true;
        // Check foreign key from table GasOneManyBig
        $foreigns = GasOneManyBig::model()->findByAttributes(array(
            'many_id' => $this->id,
            'type'      => GasOneManyBig::FUNCTION_PARAMETER
        ));
        if (count($foreigns)) {
            throw new Exception('Param này đang được setup trong công thức');
            $retVal = false;
        }
        return $retVal;
    }

    //-----------------------------------------------------
    // Utility methods
    //-----------------------------------------------------
    /**
     * Get roles array
     * @return type
     */
    public function getArrayRoles() {
        $aRoles = Roles::getDropdownList();
        $aRoles[self::ROLE_GLOBAL_ID] = self::ROLE_GLOBAL_NAME;
        return $aRoles;
    }

    /** @Author: HOANG NAM 29/03/2018
     *  @Todo: get array status
     *  @Param: 
     * */
    public function getArrayStatus() {
        return array(
            $this->status_active => DomainConst::CONTENT10002,
            $this->status_in_active => DomainConst::CONTENT10003,
        );
    }

    /** @Author: HOANG NAM 29/03/2018
     *  @Todo: get
     *  @Param: 
     * */
    public function getRoles() {
        if ($this->role_id == self::ROLE_GLOBAL_ID)
            return self::ROLE_GLOBAL_NAME;
        return !empty($this->rRole) ? $this->rRole->role_name : '';
    }

    /**
     * Get user's name
     * @return String Fullname of user
     */
    public function getUser() {
        return !empty($this->rUser) ? $this->rUser->getFullName() : '';
    }

    /**
     * Get current status string
     * @return type
     */
    public function getStatus() {
        $aStatus = $this->getArrayStatus();
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : '';
    }

    /**
     * Get created date string
     * @return type
     */
    public function getCreatedDate() {
        return MyFormat::dateConverYmdToDmy($this->created_date, DomainConst::DATE_FORMAT_11);
    }

    /**
     * Get name of parameter
     * @return String Parameter's name
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Get method of parameter
     * @return String Parameter's method
     */
    public function getMethod() {
        return $this->method;
    }
    
    public function getPositionWork($list = false){
        $aPositionWork = HrSalaryReports::model()->getArrayWorkPosition();
        if($list){
            if(empty($this->position_work_list)) return '';
            $aIdPosition = explode(",", $this->position_work_list);
            $res = '';
            foreach ($aIdPosition as $value) {
                $res .= $aPositionWork[$value] . ', ';
            }
            return $res;
        } else {
            return $aPositionWork[$this->position_work];
        }
    }

    /** @Author: HOANG NAM 18/04/2018
     *  @Todo: get by roles
     *  @Param: 
     * */
    public function getActiveByRoles($role_id) {
        $criteria = new CDbCriteria;
        if (!empty($role_id)) {
            // DuongNV 08/08/18 add role_all
            if(is_array($role_id)){
                $criteria->addCondition('t.role_id IN (' . implode(',', $role_id) . ')');
            } else {
                $criteria->addCondition('t.role_id =' . $role_id);
            }
            $criteria->addCondition('t.status =' . $this->status_active);
            return $this->findAll($criteria);
        }
        return array();
    }
    
    /**
     * Get list parameter by role id
     * @param String $role_id Id of role
     * @return Array List parameters
     */
    public static function loadItemsByRoleId($role_id) {
        $retVal = array();
        $criteria = new CDbCriteria;
        if (!empty($role_id)) {
            $criteria->addCondition('t.role_id =' . $role_id);
            $criteria->addCondition('t.status =' . $this->status_active);
            $retVal = self::model()->findAll($criteria);
        }
        return $retVal;
    }

    /** @Author: DUONG 31/05/2018
     *  @Todo:   tabs index
     *  @Code:   DUONG001
     *  @Param:
     * */
    public function canView() {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        if ($cRole == ROLE_ADMIN) {
            return true;
        }
        if ($cUid != $this->created_by) {
            return false; // cho phép admin sửa hết, còn những user khác thì ai tạo thì dc sửa của người đó
        }
        return false;
    }

    public function getParametersByRole($role_id = '') {
        $criteria = new CDbCriteria;
        $criteria->addCondition('role_id = ' . $role_id);
        $sort = new CSort();

        $sort->attributes = array(
            'name' => 'name',
        );
        $sort->defaultOrder = 't.id desc';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => 30,
            ),
        ));
    }

    /**
     * Get value of parameter
     * @param String $from Date from (format is DATE_FORMAT_4 - 'Y-m-d')
     * @param String $to Date to (format is DATE_FORMAT_4 - 'Y-m-d')
     * @return int Value of parameter
     */
    public function getValue($from, $to, $mUser) {
        // Call method from user model. Ex: $mUser->getCombackCylinder($from, $to)
        $hrSearchFrom       = $mUser->hrFrom;// Now2118 giữ lại biến search của model User ex 2018-11-01
        $hrSearchTo         = $mUser->hrTo; // ex 2018-11-30
        $mUser->hrFrom      = $from;// với kiểu tính công theo điểm làm việc PTTT hoặc cuộc gọi 1 ngày của Telesale, tổng đài thì $from = $to
        $mUser->hrTo        = $to;
        $retVal             = $mUser->{$this->method}();
        
        $mUser->hrFrom      = $hrSearchFrom;// trả lại biến cho $mUser
        $mUser->hrTo        = $hrSearchTo;
        return (empty($retVal) ? 0 : $retVal);
    }

    /** @Author: DuongNV Dec 4,2018
     *  @Todo: get parameters by position_work
     * */
    public function getParamsByPosition($pos = '') {
        $criteria = new CDbCriteria;
        if (!empty($pos)) {
//            $criteria->addCondition('t.position_work IN (' . $pos . ',' . ROLE_ALL . ')');
            $criteria->addCondition('t.position_work_list LIKE "%' . $pos . '%"');
            $criteria->addCondition('t.status =' . $this->status_active);
            return $this->findAll($criteria);
        }
        return array();
    }
}
