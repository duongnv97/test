<?php

/**
 * This is the model class for table "{{_call}}".
 *
 * The followings are the available columns in table '{{_call}}':
 * @property string $id
 * @property string $call_uuid
 * @property string $call_uuid_extension
 * @property integer $extension
 * @property string $user_id
 * @property string $agent_id
 * @property integer $province_id
 * @property integer $direction
 * @property integer $call_status
 * @property string $start_time
 * @property string $answer_time
 * @property string $end_time
 * @property integer $total_duration
 * @property integer $bill_duration
 * @property string $created_date
 * @property integer $hour
 * @property integer $failed_status
 */
class Call extends BaseSpj
{
    const PHONE_NUMBER_HGD          = 19001565;
    const PHONE_NUMBER_BM           = 19001521;
    const PHONE_NUMBER_ALCOHOL      = 19001278;// Sep1019
    const DEFAULT_EXT           = '2000';// là id EXT mặc định
    const DEFAULT_EXT_USER_ID   = '1999';// là id của quản lý call center, khi không xác định được user online ứng với ext được chọn
    const DIRECTION_INBOUND     = 1;
    const DIRECTION_OUTBOUND    = 2;
    
//    const QUEUE_HGD     = 2000;
    const QUEUE_HGD             = 2999;
    const QUEUE_BO_MOI          = 2001;
    const QUEUE_HGD_TELESALE    = 2998;// tràn telesale khi toàn bộ máy bận
    const MAX_EXT_HGD           = 220; // EXT 218 for limit ext online
    const SOURCE_CALL_CENTER_HGD        = 1; // cuộc gọi HDG - Jul2019 sử dụng biến $sourceFrom của BaseSpj để define hướng gọi của 1 Call
    const SOURCE_CALL_CENTER_BOMOI      = 2; // cuộc gọi bò mối 
    const SOURCE_CALL_TELESALE          = 3; // cuộc gọi Telesale
    
    const STATUS_START          = 1;
    const STATUS_DIALING        = 2;
    const STATUS_DIAL_ANSWER    = 3;
    const STATUS_HANGUP         = 4;
    const STATUS_CDR            = 5;
    const STATUS_TRIM           = 6;
    const STATUS_MISS_CALL      = 7;// Apr 06, 2017 SPJ add them Status MissCall
    const STATUS_PUT_CDR        = 8;// Aug0918 &callstatus=PutCDR
    
    // 1:Start, 2:Dialing, 3:DialAnswer, 4:HangUp, 5:CDR, 6:TRIM, 7: MissCall
    const STATUS_TEXT_START          = 'Start';
    const STATUS_TEXT_DIALING        = 'Dialing';
    const STATUS_TEXT_DIAL_ANSWER    = 'DialAnswer';
    const STATUS_TEXT_HANGUP         = 'HangUp';
    const STATUS_TEXT_CDR            = 'CDR';// sự kiện CDR sẽ chỉ có 1 lần cho mỗi cuộc gọi
    const STATUS_TEXT_TRIM           = 'Trim';// Sự kiện TRIM sẽ có nhiều lần khi chuyển giữa các máy
    const STATUS_TEXT_PUT_CDR        = 'PutCDR';// Sự kiện PutCDR DC Gửi khi gói tin bị mất
    
    public $date_from, $date_to, $type, $date_from_ymd, $date_to_ymd, $date_from_review, $date_to_review ,$reason;
    public $customer_hgd_id, $autocomplete_name, $autocomplete_name_1, $autocomplete_name_2, $autocomplete_name_3; // Store user name for search
    public $hour, $source_app = ''; // Store hour for search
    
    /** @Todo: get array những event sẽ gửi notify đến EXT  */
    public function getEventNotify() {
        return [
            Call::STATUS_DIALING,
            Call::STATUS_DIAL_ANSWER,
//            Call::STATUS_HANGUP,// không gửi hangup nữa, CDR sẽ thay hangup
            Call::STATUS_CDR,
            Call::STATUS_MISS_CALL,
//            Call::STATUS_TRIM
        ];
    }
    public function getEventNotifyMonitor() {
        return [
            Call::STATUS_START,
            Call::STATUS_DIALING,
            Call::STATUS_DIAL_ANSWER,
            Call::STATUS_HANGUP,// không gửi hangup nữa, CDR sẽ thay hangup
            Call::STATUS_CDR,
            Call::STATUS_MISS_CALL,
        ];
    }
    
    public function getArrayTextStatusCall() {
        return [
            Call::STATUS_TEXT_START         => Call::STATUS_START,
            Call::STATUS_TEXT_DIALING       => Call::STATUS_DIALING,
            Call::STATUS_TEXT_DIAL_ANSWER   => Call::STATUS_DIAL_ANSWER,
            Call::STATUS_TEXT_HANGUP        => Call::STATUS_HANGUP,
            Call::STATUS_TEXT_CDR           => Call::STATUS_CDR,
            Call::STATUS_TEXT_TRIM          => Call::STATUS_TRIM,
            Call::STATUS_TEXT_PUT_CDR       => Call::STATUS_PUT_CDR,
        ];
    }
    
    public function getArrayDirection() {
        return [
            Call::DIRECTION_INBOUND     => 'Gọi đến',
            Call::DIRECTION_OUTBOUND    => 'Gọi đi',
        ];
    }
    public function getArrayTypeCall() {
        return [
            Call::QUEUE_HGD     => 'Hộ GĐ',
            Call::QUEUE_BO_MOI  => 'Bò mối',
        ];
    }
    public function getArrayCallStatus() {
        return [
            Call::STATUS_HANGUP     => 'Xong',
            Call::STATUS_MISS_CALL  => 'Miss Call',
        ];
    }
    
    public function getArrayQueueHgd() {
        return [
            Call::QUEUE_HGD,
            Call::QUEUE_HGD_TELESALE,
        ];
    }
    
    public function getArrayDirectionMap() {
        return [
            'inbound'     => Call::DIRECTION_INBOUND,
            'outbound'    => Call::DIRECTION_OUTBOUND,
        ];
    }
    public function getTimeFormat() {
        return 'H:i:s';
    }
    
    public function getArrayFailedStatus() {
        return GasConst::getSellReason();
    }

    public static function getArrayRoleExt() {
        return [
           ROLE_DIEU_PHOI,
           ROLE_CALL_CENTER,
        ];
    }
    public static function getArrayRoleCreateCustomer() {
        return [
           ROLE_DIEU_PHOI,
           ROLE_CALL_CENTER,
           ROLE_SALE,
           ROLE_EMPLOYEE_MARKET_DEVELOPMENT,
           ROLE_MONITORING_MARKET_DEVELOPMENT,
        ];
    }
    public function getArrayAudit() {
        return [
//            GasConst::UID_HA_PT, // Phạm Thị Hà
//            GasConst::UID_TUYET_LT_CALL, // Lê Thị Tuyết
            GasConst::UID_NGOC_PT,
//            GasConst::UID_PHUONG_NT,
            GasConst::UID_ADMIN,
            GasConst::UID_HIEU_PT,
            GasConst::UID_HUNG_NM,
            GasConst::UID_ANH_LQ,
        ];
    }
    
    /** @Author: DungNT Mar 11, 2017
     * define các ext của đại lý trên hệ thống, để khi NV CallCenter login vào sẽ chọn đang ngồi ở Agent nào
     * truyền vào model Users, để có thể sẽ tách từng vùng sẽ chọn list EXT riêng của vùng đó
     */
    public static function getListExtAgent($mUser) {
        /* Tại thời điểm này thì chỉ return về 1 list, nếu về sau có chia 2 cụm CallCenter thì sẽ if else ở đây để return list
         */
        return [
            201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,
            251,252,253,
            291,292,293,294,295,296,
            331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,
            500,501, // Gia Lai
            510,511,512, // Binh Dinh
            520,521,// Khanh Hoa
            530,531,// Binh Duong
            540,541,// Vung Tau
        ];
    }
    
    public function getListExtBoMoi() {
        return [
            251,252,253
        ];
    }
    public function getListExtHgd() {// Jul2019 list ext này bao gồm cả ext của Telesale tràn lên: 331,332,333,
        return [
            201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,
            296,
            331,332,333,
        ];
    }
    public function getListExtHgdOnly() {// Jul2019 list ext này chỉ có của HGĐ
        return [
            201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,
        ];
    }
    public function getListExtHomeSetup() {
        return [
            291,292,293,294,295,296
        ];
    }
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Call the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_call}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('sell_id, id, call_uuid, parent_call_uuid, child_call_uuid, user_id, agent_id, province_id, direction, call_status, start_time, answer_time, end_time, total_duration, bill_duration, created_date', 'safe'),
            array('source_app, audit_id, customer_hgd_id, type, created_date_only, customer_id, caller_number, destination_number, date_from, date_to, failed_status, date_from_review , date_to_review, reason', 'safe'),
            array('pageSize, note, customer_new','safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rSell' => array(self::BELONGS_TO, 'Sell', 'sell_id'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rProvince' => array(self::BELONGS_TO, 'GasProvince', 'province_id'),
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rCustomerHgd' => array(self::BELONGS_TO, 'Users', 'customer_hgd_id'),
            'rUser' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'rAudit' => array(self::BELONGS_TO, 'Users', 'audit_id'),
            'rUserCheckCall' => array(self::BELONGS_TO, 'Users', 'user_check_call'),
            'rCallReview' => array(self::HAS_MANY, 'CallReview', 'call_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                'id' => 'ID',
                'call_uuid' => 'Call Uuid',
                'parent_call_uuid' => 'parent_call_uuid',
                'child_call_uuid' => 'child_call_uuid',
                'user_id' => 'Nhân Viên',
                'agent_id' => 'Đại Lý',
                'province_id' => 'Tỉnh',
                'direction' => 'Direction',
                'caller_number' => 'Số gọi',
                'destination_number' => 'Số nghe',
                'call_status' => 'Call Status',
                'start_time' => 'Start',
                'answer_time' => 'Answer',
                'end_time' => 'End',
                'total_duration' => 'Total Duration',
                'bill_duration' => 'Bill Duration',
                'created_date' => 'Ngày tạo',
                'sell_id' => 'Bán hàng',
                'date_from' => 'Từ Ngày',
                'date_to' => 'Đến Ngày',
                'customer_id' => 'khách Hàng',
                'type' => 'Loại cuộc gọi',
                'customer_hgd_id' => 'KH hộ GĐ',
                'source_app' => 'KH bò mối đặt App',
                'date_from_review' => 'Từ Ngày KT',
                'date_to_review' => 'Đến Ngày KT',
                'reason' => 'Lý do',
            );
    }

    /** @Author: DungNT Jun 11, 2017
     * @Todo: get sub sql limit điều phối
     */
    public static function getSubSqlLimitDieuPhoi() {
        return 'SELECT SubReply.id  FROM `gas_users` as SubReply WHERE SubReply.role_id='.ROLE_DIEU_PHOI;
    }

    public function getArrayTypeReview() {
        return [
            GasConst::SELL_PHAN_NAN,
            GasConst::SELL_UPHOLD,
            GasConst::SELL_HOI_GAS,
            GasConst::SELL_CANCEL_ORDER,
            GasConst::SELL_MAKE_ORDER,
            0,// cuộc gọi chưa phân loại
        ];
    }
    
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $mAppCache = new AppCache();
        $hoiGia = GasConst::SELL_HOI_GIA;
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        $tuyetAudit = GasConst::UID_TUYET_LT_CALL;
        $this->caller_number        = UsersPhone::formatPhoneSaveAndSearch($this->caller_number);
        $this->destination_number   = UsersPhone::formatPhoneSaveAndSearch($this->destination_number);
        $criteria=new CDbCriteria;
        $criteria->compare('t.direction', $this->direction);
        $criteria->compare('t.caller_number', trim($this->caller_number));
        $criteria->compare('t.destination_number', trim($this->destination_number));
        $criteria->compare('t.customer_id', $this->customer_id);
        $criteria->compare('t.call_status', $this->call_status);
        $criteria->compare('t.user_id', $this->user_id);
        $criteria->compare('t.agent_id', $this->agent_id);
        
        if(!empty($this->customer_hgd_id)){
            $criteria->compare('t.customer_id', $this->customer_hgd_id);
        }
        $this->addConditionCallReview($criteria);
        if($this->failed_status == GasConst::SELL_AUDIT_TEST){
            $criteria->addCondition("t.audit_id > 0 ");
        }elseif($this->failed_status == GasConst::SELL_NOT_SUCCESS){
//            $criteria->addCondition('t.failed_status<>0 AND t.sell_id=0 AND call_status<>'.Call::STATUS_MISS_CALL);
            $criteria->addCondition("t.failed_status<>0 AND t.sell_id=0");
        }elseif($this->failed_status == GasConst::SELL_COMPLETE){
            $criteria->addCondition('t.sell_id > 0');
        }elseif($this->failed_status == GasConst::SELL_NOT_RATE){
            $criteria->addCondition("(t.failed_status=0 OR t.audit_id=$tuyetAudit ) AND (t.failed_status<>$hoiGia AND t.failed_status_old=0) AND t.sell_id=0 AND call_status<>".Call::STATUS_MISS_CALL);
        }elseif(!empty($this->failed_status)){
            $aStatusNotMove = [GasConst::SELL_HOI_GIA, GasConst::SELL_HOI_GAS];
//            if($this->failed_status != $hoiGia){
            if( !in_array($this->failed_status, $aStatusNotMove)){
                $criteria->addCondition("t.failed_status = $this->failed_status AND t.audit_id<>$tuyetAudit");
            }else{// Now1017 Cuộc hỏi giá Tuyết Ghi chú thì vẫn nằm trong hỏi giá, không move đi chỗ khác 
                $criteria->addCondition("t.failed_status = $this->failed_status");
            }
        }
        $aRoleLimit     = [ROLE_CALL_CENTER, ROLE_DIEU_PHOI, ROLE_SUB_USER_AGENT];
        $aRoleLockView  = [ROLE_EMPLOYEE_MAINTAIN];
//        $subSql     = Call::getSubSqlLimitDieuPhoi();
        $aIdDieuPhoiBoMoi   = $mAppCache->getArrayIdRole(ROLE_DIEU_PHOI);
        $sParamsIn          = implode(',', $aIdDieuPhoiBoMoi);
        $sParamsInExtBoMoi  = implode(',', $this->getListExtBoMoi());
        $cHgd = $cBoMoi = '';
        
        if($this->type == Call::QUEUE_HGD){
            $cHgd           = 't.destination_number NOT IN ('.$sParamsInExtBoMoi.')';
        }elseif($this->type == Call::QUEUE_BO_MOI){
            $cBoMoi         = 't.destination_number IN ('.$sParamsInExtBoMoi.')';
        }
//        elseif($cRole == ROLE_CALL_CENTER){
////            $criteria->addCondition('t.user_id NOT IN ('.$sParamsIn.')');
//            $cHgd           = 't.destination_number NOT IN ('.$sParamsInExtBoMoi.')';
//        }elseif($cRole == ROLE_DIEU_PHOI){
////            $criteria->addCondition('t.user_id IN ('.$sParamsIn.')');
//            $cBoMoi         = 't.user_id IN ('.$sParamsIn.') OR t.destination_number IN ('.$sParamsInExtBoMoi.')';
//        }
        if(!empty($cHgd)){
            $criteria->addCondition($cHgd);
        }
        if(!empty($cBoMoi)){
            $criteria->addCondition($cBoMoi);
        }
        if(in_array($cRole, $aRoleLockView)){
            $criteria->addCondition('t.id=-1');
        }

        $date_from = $date_to = '';
        if(!empty($this->date_from)){
            $tmpDate = explode(' ', $this->date_from);
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($tmpDate[0]);
            $date_from = $date_from.' '.$tmpDate[1];
            DateHelper::searchGreater($date_from, 'created_date_bigint', $criteria);
        }
        if(!empty($this->date_to)){
            $tmpDate = explode(' ', $this->date_to);
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($tmpDate[0]);
            $date_to = $date_to.' '.$tmpDate[1];
            DateHelper::searchSmaller($date_to, 'created_date_bigint', $criteria);
        }
        // SEP28,18 PTNGHIA 
        $date_from_review = $date_to_review = '';
        if(!empty($this->date_from_review)){
            $date_from_review = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from_review);
            DateHelper::searchGreater($date_from_review, 'reviewed_date_bigint', $criteria);
        }
        if(!empty($this->date_to_review)){
            $date_to_review = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to_review);
            DateHelper::searchSmaller($date_to_review, 'reviewed_date_bigint', $criteria, true);
        }
        GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');
        $criteria->order = 't.id DESC';
        $_SESSION['data-excel'] = new CActiveDataProvider($this, array(
            'pagination'=>false,
            'criteria'=>$criteria,
//            'sort' => $sort,
        ));

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> $this->pageSize,
            ),
        ));
    }
    
    /** @Author: DungNT Sep 02, 2018
     *  @Todo: add condition of Review
     **/
    public function addConditionCallReview(&$criteria) {
        $cUid   = MyFormat::getCurrentUid();
        $cRole  = MyFormat::getCurrentRoleId();
        $cController = strtolower(Yii::app()->controller->id);
        if($cController != 'callreview'){// AnhDung Oct0918 fix for user search admin/transaction/index/ListCall/1
            return ;
        }
        $mCallReview = new CallReview();
        if(!isset($_GET['type'])){
            $criteria->addCondition("t.user_check_call= 0");
        }elseif(isset($_GET['type']) && $_GET['type'] == CallReview::RECEIVED){
            $criteria->addCondition("t.user_check_call > 0 AND t.status_check_call = ".CallReview::RECEIVED);
        }elseif(isset($_GET['type']) && $_GET['type'] == CallReview::TESTED){
            $criteria->addCondition("t.status_check_call = ".CallReview::TESTED);
        }
        if(isset($_GET['type']) && $cRole == ROLE_TEST_CALLL){
            $criteria->addCondition("t.user_check_call=$cUid");
        }
        if($cRole == ROLE_TEST_CALLL){
            $criteria->addCondition('t.bill_duration > 0');
        }
        if(!empty($this->reason)){ // search lý do cho người kiểm tra có thể check
//            $aRes = $mCallReview->getArrCallIdByReason($this->reason);
//            $aCondition = array_keys($aRes); // bỏ đi những lỗi 0 điểm
//            $criteria->addInCondition("t.id",$aCondition);
            $criteria->with         = ['rCallReview'];
            $criteria->together     = true;
            $criteria->addCondition("rCallReview.reason={$this->reason}");
        }
        $sParamsIn = implode(',', $this->getArrayTypeReview());
        $criteria->addCondition("t.failed_status IN ($sParamsIn) OR t.sell_id>0");
    }
    
    protected function beforeSave() {
        if($this->isNewRecord){
            $this->created_date_only = date('Y-m-d');
        }
        return parent::beforeSave();
    }
    
    public function getByCallUuid() {
        $criteria = new CDbCriteria();
        $criteria->compare("t.call_uuid", $this->call_uuid);
        $criteria->order = 't.id DESC';
        return self::model()->find($criteria);
    }

    public function getSell($field_name='code_no') {
        $mSell = $this->rSell;
        if($mSell){
            return $mSell->$field_name;
        }
    }
    public function getDirection($getLinkView = false) {
        $aDirection = $this->getArrayDirection();
        $text = isset($aDirection[$this->direction]) ? $aDirection[$this->direction] : '';
        if(!$getLinkView){
            return $text;
        }
        $link = Yii::app()->createAbsoluteUrl('admin/transaction/index', ['ViewCall'=>$this->id]);
        return "<a href='$link' target='_blank'>$text</a>";
    }
    public function getCallStatus() {
        $res = '';
        if($this->call_status == Call::STATUS_HANGUP){
            $res = '';
        }elseif($this->call_status == Call::STATUS_MISS_CALL){
            $res = 'Miss Call';
        }
        return $res;
    }
    public function getCallNumber($clickCall = true) {
        if($clickCall && !in_array($this->caller_number, Call::getListExtAgent(null))){
            $phone = '0'.$this->caller_number;
            return $this->buildUrlMakeCall($phone);
        }
        return $this->caller_number;
    }
    public function getDestinationNumber($clickCall = true) {
        if($clickCall && !in_array($this->destination_number, Call::getListExtAgent(null))){
            $phone = '0'.$this->destination_number;
            return $this->buildUrlMakeCall($phone);
        }
        return $this->destination_number;
    }
    public function getStartTime() {
        return MyFormat::dateConverYmdToDmy($this->start_time, $this->getTimeFormat());
    }
    public function getAnswerTime() {
        return MyFormat::dateConverYmdToDmy($this->answer_time, $this->getTimeFormat());
    }
    public function getEndTime() {
        return MyFormat::dateConverYmdToDmy($this->end_time, $this->getTimeFormat());
    }
    public function getTotalDuration() {
        return ActiveRecord::formatCurrency($this->total_duration);
    }
    public function getBillDuration() {
        return ActiveRecord::formatCurrency($this->bill_duration);
    }
    public function getNote(){
        return $this->note;
    }
    /** @Author: DungNT Nov 16, 2016
     * @Todo: check user can update test call
     */
    public function canUpdateTestCall() {
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        if($this->sell_id > 0){
            return false;
        }

        if($cRole == ROLE_ADMIN){
            return true;
        }
        if($cUid != $this->user_id && !in_array($cUid, $this->getArrayAudit())){
            return false;
        }
        $days = 0;
        if(in_array($cUid, $this->getArrayAudit())){
            $days = 5;
        }
        
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, $days, '-');
        // Ngày hiện tại - 10 ngày mà lớn hơn ngày tạo thì không cho phép update
        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }
    
    public function getFailedStatusText() {
        $aFailedStatus = $this->getArrayFailedStatus();
        return isset($aFailedStatus[$this->failed_status]) ? $aFailedStatus[$this->failed_status] : '';
    }
    
    /** @Author: DungNT Sep 12, 2017
     *  @Todo: get list agent from session
     */
    public function getListAgent() {
        $aRes = [];
        $aDataAgent = CacheSession::getListAgent();
        foreach($aDataAgent as $agent_id => $info){
            $aRes[$agent_id] = $info['first_name'];
        }
        return $aRes;
    }
    
    public function getFailedStatus() {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        $aFailedStatus  = $this->getArrayFailedStatus();
        $aScore         = MyFormat::BuildNumberOrder(10);
        $testText = '<span class="TypeCallTestText">'.$this->getFailedStatusText().'</span><br><br>';
        if(!$this->canUpdateTestCall()){
            return $testText;
        }
        if($cRole != ROLE_ADMIN && !in_array($cUid, $this->getArrayAudit())){
//            return $testText;// Oct0417 khóa lại để Audit gọi lại - Sep2817 nên cho phép sửa trong ngày
            // Jun0118 mở cho tổng đài sửa trong ngày
        }
        $failed_note = $this->failed_note;
        if(in_array($cUid, $this->getArrayAudit())){
            $failed_note = $this->audit_note;
        }

        unset($aFailedStatus[GasConst::SELL_COMPLETE]);
        $url = Yii::app()->createAbsoluteUrl('admin/transaction/index', array('id'=>$this->id,'TestCall'=>1));
        $dropdown = CHtml::dropDownList('ChangeAgent', $this->failed_status, 
            $aFailedStatus, array('class'=>'ChangeAgentValue', 'empty'=>'Chọn lý do'));
        
        $dropdownAgent = CHtml::dropDownList('ChangeAgentId', $this->agent_id, 
            $this->getListAgent(), array('class'=>'SelectAgentId', 'empty'=>'Chọn đại lý'));
        
        $dropdownScore = CHtml::dropDownList('ChangeScore', $this->score, 
            $aScore, array('class'=>'ChangeValueScore ', 'empty'=>'Chấm điểm'));
        
        $html       = $testText.'<a class="ShowChangeAgent" href="javascript:;">Cập nhật</a>';
        $inputNote  = '<input type="text" class="ChangeAgentNote" placeholder="Audit ghi chú" value="'. $failed_note .'">';
//        $htmlSave   = "<span class='display_none WrapChangeAgentDropdown'>$dropdownAgent <br><br> $dropdown<br><br> $dropdownScore<br><br> $inputNote<br><br><a class='SaveChangeAgent' next='$url' href='javascript:;'>Save</a>&nbsp;&nbsp;&nbsp;<a class='CancelChangeAgent' href='javascript:;'>Cancel</a></span>";
        $htmlSave   = "<span class='display_none WrapChangeAgentDropdown'>$dropdownAgent <br><br> $dropdown<br><br>$inputNote<br><br><a class='SaveChangeAgent' next='$url' href='javascript:;'>Save</a>&nbsp;&nbsp;&nbsp;<a class='CancelChangeAgent' href='javascript:;'>Cancel</a></span>";
        $html       .= $htmlSave;
        
        return $html;
    }
    public function getFailedNote() {
        $failedNote = nl2br($this->failed_note);
        if(!empty($this->audit_note)){
            $failedNote .= "<br> Audit: $this->audit_note"; 
        }
        return '<span class="CallNoteText">'.$failedNote.'</span><br><br>';
    }
    public function getMonitorFilename() {
        return $this->monitor_filename;
    }
    public function getCustomer($field_name='first_name') {
        $mUser = $this->rCustomer;
        if($mUser){
            if($field_name == 'first_name'){
                return $mUser->code_bussiness . ' - '. $mUser->first_name;
            }
            return $mUser->$field_name;
        }
        return '';
    }
    public function getAgent($field_name='first_name') {
        $mUser = $this->rAgent;
        if($mUser){
            return $mUser->$field_name;
        }
        return '';
    }
    public function getAudit($field_name='first_name') {
        $mUser = $this->rAudit;
        if($mUser){
            return $mUser->$field_name;
        }
        return '';
    }
    public function getAgentGrid() {
        $agentName = CacheSession::getAgentName($this->agent_id);
        return "<span class='AgentGridName'>$agentName</span>";
    }
    public function getUser($field_name='first_name') {
        $mUser = $this->rUser;
        if($mUser){
            return $mUser->$field_name;
        }
        return '';
    }
    /** @Author: DungNT Oct 19, 2018
     **/
    public function isLockCallReview() {
        return false; // Oct1818 open all
        $aRoleLock  = [ROLE_CALL_CENTER, ROLE_DIEU_PHOI];
        $aUidAllow  = [GasConst::UID_PHUONG_NT];
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        if(in_array($cUid, $aUidAllow)){
            return false;
        }
        if(in_array($cRole, $aRoleLock)){
            return true;
        }
        return false;
    }
    
    public function getUserCheckCall($field_name='first_name') {
        if($this->isLockCallReview()){
            return '';
        }
        $mUser = $this->rUserCheckCall;
        if($mUser){
            return $mUser->$field_name;
        }
        return '';
    }
    public function getUrlMakeSell() {
        if($this->direction == Call::DIRECTION_OUTBOUND){
            return '';
        }
        
        if($this->isCallBoMoi()){
            return $this->getUrlMakeOrderBoMoi();
        }
        if(empty($this->customer_id)){
            return $this->getUrlCreateCustomerHgd();
        }
        $title = 'Tạo Đơn Hàng';
        if(empty($this->sell_id)){
            $link = Yii::app()->createAbsoluteUrl('admin/sell/create', ['customer_id'=>$this->customer_id, 'call_uuid'=> $this->call_uuid]);
        }else{
            $link = Yii::app()->createAbsoluteUrl('admin/sell/view', ['id'=>$this->sell_id]);
            $title = 'Xem';
            return "<a href='$link' target='_blank'>$title</a>";
        }
        return '';
//        return "<a href='$link' target='_blank'>$title</a>";
    }
    /**
     * @Author: DungNT May 23, 2017
     * @Todo: tạo đơn hàng bò mối
     */
    public function getUrlMakeOrderBoMoi() {
        $cRole = MyFormat::getCurrentRoleId();
        $aRoleAllow = [ROLE_DIEU_PHOI, ROLE_CALL_CENTER];
        if(!in_array($cRole, $aRoleAllow)){
            return '';
        }
        $aParams = ['customer_id'=>$this->customer_id, 'phone_number'=>$this->getCallNumber(false)];
        $link = Yii::app()->createAbsoluteUrl('admin/ajaxForUpdate/makeOrderBoMoiFromRecording', $aParams);
        return "<a href='$link' class='AjaxMakeOrderBoMoi'>Tạo Đơn Hàng</a>";
    }
    public function getUrlCreateCustomerHgd() {
        $cRole = MyFormat::getCurrentRoleId();
        $aRoleAllow = [ROLE_DIEU_PHOI, ROLE_CALL_CENTER];
        if(!in_array($cRole, $aRoleAllow)){
            return '';
        }
        $aParams = ['phone_number'=>$this->getCallNumber(false)];
        $link = Yii::app()->createAbsoluteUrl('admin/ajax/iframeCreateCustomerHgd', $aParams);
        return "<a href='$link' class='AjaxCreateCustomerHgd'>Tạo KH</a>";
    }
    
    /**
     * @Author: DungNT May 23, 2017
     * @Todo: check cuộc gọi bò mối
     */
    public function isCallBoMoi() {
        return in_array($this->destination_number, $this->getListExtBoMoi());
    }
    
    /**
     * @Author: DungNT Apr 11, 2017
     * @Todo: get url play audio from PBX server
     */
    public  function getUrlPlayAudio() {
        if(empty($this->monitor_filename)){
            return '';
        }
        $htmlDivPlay = "<div class='PlayAudioPbxDiv display_none'>{$this->getAudioElement()}</div>";
        $link = Yii::app()->createAbsoluteUrl('/').'/themes/gas/new-icon/play.png';
        return $html = "<a class='PlayAudioPbxIcon' href='javascript:;'><img src='$link' title='Nghe lại ghi âm'></a>&nbsp;&nbsp;&nbsp;{$this->getAudioDownload()}<br><br>$htmlDivPlay";
    }
    
    /**
     * @Author: DungNT Apr 11, 2017
     * @Todo: get html audio
     */
    public function getAudioElement() {
        $link   = $this->getAudioUrl();
        $html   = "<audio controls>
                    <source src='$link' type='audio/mpeg'>
                      Your browser does not support the audio element.
                </audio>";
        return $html;
    }
    public function getAudioUrl() {
        $apiController = new ApiController('');
        return "{$this->getUrlPbxApsp()}/playback2.php?calluuid={$this->call_uuid}&secrect={$apiController->getSecret()}&version=3";
    }
    public function getAudioDownload() {
        $link   = $this->getAudioUrl();
        return "<a href='$link' target='_blank'>Tải về</a>";
    }
    
    /** @Author: DungNT Sep 06, 2018
     *  @Todo: get url make call from web
     *  @Param: $callernum is ext: 201, 202 ....
     *  @Param: $destnum is phone 0988180386, 0988538360
     **/
    public function getMakeCallUrl($callernum, $destnum) {
        $apiController = new ApiController('');
        return "{$this->getUrlPbxApsp()}/makecall2.php?callernum={$callernum}&destnum={$destnum}&secrect={$apiController->getSecret()}&version=3";
    }
    
    /**
     * @Author: DungNT Apr 11, 2017
     * @Todo: get url server Pbx
     */
    public function getUrlPbxApsp() {
        return 'https://apps.worldfone.vn/externalcrm';
    }
    
    public function getCallNumberView() {
//        $res = UsersPhone::formatPhoneView($this->getCallNumber());
        $res = $this->getCallNumber();
        return "<span class='f_size_14 item_b'>$res</span>";
    }
    public function getDestinationNumberView() {
//        $res = UsersPhone::formatPhoneView($this->getDestinationNumber());
        $res = $this->getDestinationNumber();
        return "<span class='f_size_14 item_b'>$res</span>";
    }
        
    /**
     * @Author: DungNT Apr 13, 2017
     * @Todo: update info Sell
     */
    public function updateSellInfo($mSell) {
        $this->sell_id     = $mSell->id;
        $this->agent_id    = $mSell->agent_id;
        $this->province_id = $mSell->province_id;
        $this->customer_id = $mSell->customer_id;
        $this->update(['sell_id', 'agent_id', 'province_id', 'customer_id']);
    }
    
    /** @Author: DungNT Jul 06, 2017
     * @Todo: get list cuộc gọi đến theo số đt gọi trong ngày
     */
    public function getCallInByDate() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.customer_id='.$this->customer_id);
        $criteria->addCondition("t.created_date_only='$this->created_date_only'");
        $criteria->order = 't.id ASC';
        return self::model()->findAll($criteria);
    }
    
    public function getCallInHtml() {
        $res = '';
        $models = $this->getCallInByDate();
        foreach($models as $key => $model){
            $temp = explode(' ', $model->created_date);
            $br  = ($key==0) ? '': '<br>';
            $res .= "$br<b>Gọi lần ".($key+1)."</b>  - {$temp[1]} - ".$model->getFailedStatus();
        }
        if(!empty($res)){
            $res = '<hr>'.$res;
        }
        return $res;
    }
    
    /** @Author: DungNT Aug 05, 2017
     * @Todo: User check lai ly do failed
     */
    public function setCallFailed() {
//        $this->update(['failed_status', 'failed_note', 'agent_id']);
        $this->update();// Oct0817 update hết
    }
    
//$criteria->addCondition('t.uid_login IN (' . $sParamsIn . ') AND t.destination_number IN ('.$sParamsInExtBoMoi.')');
//$criteria->addCondition('t.uid_login IN (' . $sParamsIn . ') AND t.destination_number NOT IN ('.$sParamsInExtBoMoi.')');
    
    /** @Author: DungNT Jun 16, 2018
     *  @Todo: get list call in, out by list phone number
     **/
    public function getByListPhoneNumber($aPhone) {
        if(count($aPhone) < 1){
            return [];
        }
        $sParamsInExtCallCenter    = implode(',', array_merge($this->getListExtHgdOnly(), $this->getListExtBoMoi()));
        $criteria = new CDbCriteria();
        $criteria->compare('t.direction', $this->direction);
        $sParamsIn = implode(',', $aPhone);
        if($this->direction == Call::DIRECTION_OUTBOUND){
            $criteria->addCondition("t.destination_number IN ($sParamsIn)");
        }elseif($this->direction == Call::DIRECTION_INBOUND){
            $criteria->addCondition("t.caller_number IN ($sParamsIn)");
        }
        if($this->sourceFrom == Call::SOURCE_CALL_TELESALE){
            $criteria->addCondition("t.caller_number NOT IN ($sParamsInExtCallCenter)");
        }
        DateHelper::searchBetween($this->date_from_ymd, $this->date_to_ymd, 'created_date_bigint', $criteria, true);
        $criteria->order = 't.created_date_bigint DESC';// order theo cuoc gan nhat len tren
        return Call::model()->findAll($criteria);
    }
    
    /** @Author: Pham Thanh Nghia 2018
     *  @Todo: Update user_check_call by array id use on admin/callReview/listCall/
     *  @Param: import user_check_call, array id 
     **/
    public function saveUserCheckForCall(array $aId, $user_check_call) {
        $sql = '';
        $aRowInsert = [];
        // 1. delete old record
        $tableName = Call::model()->tableName();
        $sql = "UPDATE $tableName  
                SET user_check_call = $user_check_call
                    WHERE id IN (".implode(',', $aId).")";
        
        if(count($aId)>0)
            Yii::app()->db->createCommand($sql)->execute();
    }
    /** @Author: Pham Thanh Nghia Aug 20, 2018
     *  @Todo: get Action use on admin/callReview/listCall
     *  @Param:
     **/
    public function getActions($user_check_call=0){
        if($this->isLockCallReview()){
            return '';
        }
        $result      = '';
        if(isset($_GET['type']) && $_GET['type'] == CallReview::RECEIVED && $this->allowAction() ) {
           $result     .= '<a class=\'targetHtml\' href=\''.$this->getUrlDetail('create').'\'>Tạo mới kiểm tra</a><br>';
           $result     .= '<a class=\'targetDeny\' href=\''.$this->getUrlDetail('deny').'\'>Nhả cuộc gọi</a>';
           $result     .= '<br>';
        }
        if(isset($_GET['type']) && $_GET['type'] == CallReview::TESTED){
            $result     .= '<a class=\'targetHtml\' href=\''.$this->getUrlDetail('view').'\'>Xem kiểm tra</a><br><br>';
        }
        if(isset($_GET['type']) && $_GET['type'] == CallReview::TESTED && $this->allowAction() ){
            $result     .= '<a class=\'targetHtml\' href=\''.$this->getUrlDetail('create').'\'>Cập nhật kiểm tra</a><br>';
        }
        return $result;
    }
    public function allowAction(){
        $Uid = MyFormat::getCurrentUid();
        $RoleId = MyFormat::getCurrentRoleId();
        $aRoleAllow = array(ROLE_ADMIN,ROLE_TEST_CALLL);
        
        if(in_array($RoleId, $aRoleAllow ) || $Uid == $this->user_check_call){
            return true;
        }
        return false;
    }
    /** @Author: Pham Thanh Nghia Aug 20, 2018
     *  @Todo: get URL to go to link create use admin/callReview/listCall/type/1
     **/
    public function getUrlDetail($view = '') {
        if($view == 'view' || $view == ''){
            return Yii::app()->createAbsoluteUrl('admin/callReview/create', array('ajax' => 1, 'OnlyView' => 1, 'Call' => $this->id));
        }
        if($view == 'deny'){
            return Yii::app()->createAbsoluteUrl('admin/callReview/create', array('ajax' => 1, 'deny' => 0,'Call' => $this->id));
        }
        if($view == 'create'){
            return Yii::app()->createAbsoluteUrl('admin/callReview/create', array('ajax' => 1, 'Call' => $this->id));
        }
        return '';
    }
    /** @Author: Pham Thanh Nghia Aug 19, 2018
     *  @Todo: save call with customer_new, note, status_check_call by call_id
     *  @Param:
     **/
    public function handleSaveWithCallReview($id,$customer_new = 1, $note = ''){
        try{
            $model=Call::model()->findByPk($id);
            $mCallReview = new CallReview();
        if($model===null)
        {
            Yii::log("The requested model does not exist.");
            throw new CHttpException(404,'The requested page does not exist.');
        }else{
            if(empty($model->reviewed_date_bigint)){
                $model->reviewed_date_bigint    = strtotime(date('Y-m-d H:i:s'));
            }
            $model->customer_new            = $customer_new;
            $model->note                    = $note;
            $model->status_check_call       = CallReview::TESTED;
            $model->score                   = $mCallReview->calculateScoreByCallId($id);
            $model->update();
        }
        return $model;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    public function getScore(){
        return ($this->score) ? $this->score : ''; 
    }
    
    /** @Author: DungNT Sep 06, 2018
     *  @Todo: build url make call from web
     **/
    public function buildUrlMakeCall($sPhone) {
        $aRoleAllow = [ROLE_AUDIT, ROLE_CALL_CENTER, ROLE_TELESALE, ROLE_DIEU_PHOI];
        $cRole      = MyFormat::getCurrentRoleId();
        if(!in_array($cRole, $aRoleAllow)){
            return $sPhone;
        }
        $cExt = CacheSession::getCookie(CacheSession::CURRENT_USER_EXT);
        $res = [];
        $mCall = new Call();
        $urlServer = Yii::app()->createAbsoluteUrl('admin/ajax/clickToCall');
        $temp = explode('-', $sPhone);
        foreach($temp as $phone){
            $urlMakeCall    = $mCall->getMakeCallUrl($cExt, $phone);
//            $url    = 'https://apps.worldfone.vn/externalcrm/makecall2.php?callernum=331&destnum=01684331552&secrect=1923d10f88daae995b1f461c04f15165&version=3';
            $link   = "<a class='SpjClickToCall' href='javascript:;' urlMakeCall='$urlMakeCall' next='$urlServer'>$phone</a>";
            $res[]  = $link;
        }
        return implode(' - ', $res);
    }
    /** @Author: Pham Thanh Nghia Sep 26 2018
     *  @Todo: get array Average Score Employee by list array id
     **/
    public function getArrAverageScoreEmployee($aCallId = array()){
        $criteria=new CDbCriteria;
        $criteria->addInCondition("t.id",$aCallId);
        $criteria->select = "t.user_id, avg(score) as score";
        $criteria->group = "t.user_id";
        $model = Call::model()->findAll($criteria);
        $aRes = CHtml::listData($model, 'user_id', 'score');
        return $aRes;
    }
    /** @Author: Pham Thanh Nghia 2018
     *  @Todo: refure call to call review
     **/
    public function handleDenyCallToReview(){
        $this->status_check_call       = CallReview::RECEIVED;
        $this->user_check_call         = 0;
        $this->update();
    }
    /** @Author: Pham Thanh Nghia Oct 12, 2018
     *  @Todo: lấy danh sách lỗi của cuộc gọi
     **/
    public function getViewReasonCallReview(){
        $mCallReview = new CallReview();
        $aReason = $mCallReview->getArrayReason();
        $mCallReviews = $this->rCallReview;
        $str = "";
        foreach ($mCallReviews as $key => $value) {
            $stt = $key+1;
            $reason = isset($aReason[$value->reason]) ? $aReason[$value->reason] : 'none';
            $str .= $stt." - ".$reason."<br>";
        }
        return $str;
    }
    /** @Author: DungNT Oct 27, 2018
     *  @Todo: khởi tạo params date search
     **/
    public function initParamDate() {
        $this->date_from   = date('d-m-Y 00:00:00');
        $this->date_to     = date('d-m-Y 23:59:59');
    }
    
    /** @Author: DungNT Now 12, 2018
     *  @Todo: update info call to Sell
     **/
    public function updateCallIdToSell($mSell) {
        $mSell->call_id         = $this->id;
        $mSell->call_end_time   = strtotime($this->end_time);
        $mSell->update(['call_id', 'call_end_time']);
        $this->updateSellInfo($mSell);
    }
    
}

