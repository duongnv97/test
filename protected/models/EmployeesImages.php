<?php
/** @Author: HOANG NAM 24/01/2018
 *  @Todo: class employees images
 *  @Code: NAM005
 *  @Param: 
 **/
class EmployeesImages extends BaseSpj
{
    //    @Code: NAM005
    public static $max_upload   = 100;
    public $minFileRequired     = 2;
//    public $CV                          = 1; // đơn xin việc
//    public $CMND                        = 2; // Chứng minh nhân dân
//    public $Household                   = 3; // hộ khẩu
    // DungNT close some param public $CV, $CMND, $Household
    
    const TYPE_CV                               = 1;// đơn xin việc
    const TYPE_CMND                             = 2;// Chứng minh nhân dân
    const TYPE_HOUSE_HOLD                       = 3;// hộ khẩu
    
    const TYPE_CUSTOMER_CONTRACT                = 4; // hợp đồng
    const TYPE_CUSTOMER_CONTRACT_ASSEST         = 5; // biên bản mượn tài sản
    
    public $pathUpload = 'upload/employees';
    public $aSize = array(
        'size1' => array('width' => 128, 'height' => 96), // small size, dùng ở form update văn bản
        'size2' => array('width' => 1536, 'height' => 1300), // size view, dùng ở xem văn bản
    );
    
    public $autocomplete_name,$autocomplete_name_1,$autocomplete_name_2,$autocomplete_name_3,$autocomplete_name_4, $date_from, $date_to ,$employee_id,$users_id_statistic,$employee_id_statistic;
    
    public function getTypeOfCustomer() {
        return [
            EmployeesImages::TYPE_CUSTOMER_CONTRACT,
            EmployeesImages::TYPE_CUSTOMER_CONTRACT_ASSEST,
        ];
    }
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    public function tableName()
    {
        return '{{_employees_images}}';
    }

    public function rules()
    {
        return array(
            array('id,type,file_name,users_id,created_by, created_date,update_date,date_from,date_to,employee_id', 'safe'),
            array('users_id_statistic, employee_id_statistic', 'safe'),

        );
    }
    
    public function relations()
    {
        return array(
            'rUsers' => array(self::BELONGS_TO, 'Users', 'users_id'),
            'rEmployee' => array(self::BELONGS_TO, 'Users', 'employee_id'),
            'rCreatedBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'rSale' => array(self::BELONGS_TO, 'Users', 'sale_id'),
//            'rProfileDetail' => array(self::HAS_MANY, 'EmployeesImages', 'users_id'),
            'rFile' => array(self::HAS_MANY, 'EmployeesImages', '', 'foreignKey' => array('users_id'=>'users_id')),

        );
    }
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'file_name' => 'Tên file',
            'type' => 'Loại',
            'created_date'=>'Ngày tạo',
            'update_date'=>'Ngày cập nhật',
            'users_id'=>'Khách Hàng',
            'created_by' => 'Người tạo',
            'date_from' => 'Từ Ngày',
            'date_to' => 'Đến Ngày',
            'employee_id' => 'Nhân viên',
            'sale_id' => 'Nhân viên sale',
        );
    }

    public function getRoleLikeSale() {
        return [ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT];
    }
    
    /** @Author: DuongNV Sep 18, 2018
     *  @Todo: search statistic in index (old search condition move to $this->searchDetail())
     **/
    public function search()
    {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        $criteria=new CDbCriteria;
        $aUser = array();
        if(!empty($this->users_id_statistic)){
            $aUser[]= $this->users_id_statistic;
        }
        if(!empty($this->employee_id_statistic)){
            $aUser[]= $this->employee_id_statistic;
        }
        if(!empty($aUser)){
            $sParamsIn = implode(',',  $aUser);
            $criteria->addCondition("t.created_by IN ($sParamsIn) OR t.sale_id IN ($sParamsIn)");
        }
        if(in_array($cRole, $this->getRoleLikeSale())){
            $criteria->compare('t.sale_id', $cUid);
        }
        if($cRole == ROLE_SALE_ADMIN){
            $criteria->addInCondition('t.type', $this->getTypeOfCustomer());
        }
        
        $criteria->group = 't.users_id';
        
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
    public function getFileName(){
        return $this->file_name;
    }
    public function getType(){
        $aType = $this->getListTypeAll();
        return isset($aType[$this->type]) ? $aType[$this->type] : ' ';
    }
    
    public function getUpdateDate(){
        return $this->update_date;
    }
    public function getEmployee($field_name = 'first_name'){
        $mUser = $this->rUsers;
        return isset($mUser) ? $mUser->{$field_name} : '';
    }
    public function getCreatedBy($field_name = 'first_name'){
        $mUser = $this->rCreatedBy;
        return isset($mUser) ? $mUser->$field_name : '';
    }
     /** @Author: Pham Thanh Nghia Sep 2, 2018
     *  @Todo: getImage for index, hiện hình ảnh file
     **/
    public function getImages(){ 
        return '<p>
            <a rel="group1" class="gallery" href="'. ImageProcessing::bindImageByModel($this,'','',array('size'=>'size2')). '"> 
                <img width="100" height="70" src="'. ImageProcessing::bindImageByModel($this,'','',array('size'=>'size1')). '">
            </a>
        </p>';
    }
    /** @Author: Pham Thanh Nghia Sep 2, 2018
     *  @Todo: lấy hết danh sách tất cả các loại
     **/
    public function getListTypeAll(){
        return array(
            EmployeesImages::TYPE_CV                    => 'Đơn xin việc',
            EmployeesImages::TYPE_CMND                  => 'Chứng minh nhân dân',
            EmployeesImages::TYPE_HOUSE_HOLD            => 'Hộ khẩu',
            EmployeesImages::TYPE_CUSTOMER_CONTRACT     => 'Hợp đồng',
            EmployeesImages::TYPE_CUSTOMER_CONTRACT_ASSEST => 'Biên bản mượn tài sản',
        );
    }
    /** @Author: HOANG NAM 24/01/2018
     *  @Todo: Delete file
     *  @Code: NAM005
     *  @Param: 
     **/
    protected function beforeDelete() {
        $this->removeFileOnly($this, 'file_name');
        return parent::beforeDelete();
    }
    
    protected function beforeSave() {
        if($this->isNewRecord){
            $this->created_by = MyFormat::getCurrentUid();
        }
        return parent::beforeSave();
    }
    
    /** @Author: HOANG NAM 24/01/2018
     *  @Todo: add new images
     *  @Code: NAM005
     *  @Param: 
     **/
    public function getListType(){
        return array(
            EmployeesImages::TYPE_CV            => 'Đơn xin việc',
            EmployeesImages::TYPE_CMND          => 'Chứng minh nhân dân',
            EmployeesImages::TYPE_HOUSE_HOLD    => 'Hộ khẩu',
        );
    }
    /** @Author: HOANG NAM 24/01/2018
     *  @Todo: add new images
     *  @Code: NAM005
     *  @Param: $mUser
     **/
    public function addEmployeesImages($mUser){
        if(isset($_POST['Users']['file_name']['new'])):
            foreach ($_POST['Users']['file_name']['new']['image'] as $keyfile => $valueFile) {
                $mEmployeeImage = new EmployeesImages();
                $mEmployeeImage->type           = $_POST['Users']['file_name']['new']['type'][$keyfile];
                $mEmployeeImage->users_id       = $mUser->id;
                $mEmployeeImage->sale_id        = $mUser->sale_id;
                $mEmployeeImage->file_name      = CUploadedFile::getInstance($mUser,'file_name[new][image]['.$keyfile.']');
                if(!is_null($mEmployeeImage->file_name)){
                        $mEmployeeImage->file_name  = $this->saveFile($mUser, $mEmployeeImage, 'file_name', $mEmployeeImage->type);
                        $mEmployeeImage->save();
                        $this->resizeImage($mUser, $mEmployeeImage, 'file_name');
                }
            }
        endif;
    }
    /** @Author: HOANG NAM 24/01/2018
     *  @Todo: update images
     *  @Code: NAM005
     *  @Param: $mUser
     **/
    public function updateEmployeesImages($mUser){
        if(isset($_POST['Users']['file_name']['old'])):
            foreach ($_POST['Users']['file_name']['old']['id'] as $keyfile => $valueFile) {
                $mEmployeeImage = EmployeesImages::model()->findByPk($valueFile);
                if(!isset($_POST['Users']['file_name']['old']['type'][$keyfile])):
                    continue;
                endif;
                if($mEmployeeImage->type!=$_POST['Users']['file_name']['old']['type'][$keyfile]):
                    $mEmployeeImage->type = $_POST['Users']['file_name']['old']['type'][$keyfile];
                    $mEmployeeImage->update_date = date('Y-m-d H:i:s');
                    $mEmployeeImage->update();
                endif;
            }
        endif;
    }
    /** @Author: HOANG NAM 24/01/2018
     *  @Todo: delete images
     *  @Code: NAM005
     *  @Param: 
     **/
    public function deleteEmployeesImages($mUser){
        $aImages=[];
        if(isset($_POST['Users']['file_name']['old'])):
            foreach ($_POST['Users']['file_name']['old']['id'] as $valueFile) {
                $aImages[] = $valueFile;
            }
        endif;
        $criteria=new CDbCriteria;
        $criteria->addNotInCondition('id', $aImages);
        if(empty($mUser->id)){ // Nghia fix Sep 2, 2018
            throw new Exception('Yêu cầu file image không hợp lệ');
        }
        $criteria->addCondition('users_id = ' . $mUser->id);
        $aDeleteImages = EmployeesImages::model()->findAll($criteria);
        foreach ($aDeleteImages as $keyDelete => $mEmployeeImages) {
            $mEmployeeImages->delete();
        }
    }
    /** @Author: HOANG NAM 24/01/2018
     *  @Todo: save file
     *  @Code: NAM005
     *  @Param: 
     **/
    public function  saveFile($mGasFileScan, $model, $fieldName, $count)
    {        
        if(is_null($model->$fieldName)) return '';
        $pathUpload = $model->pathUpload."/$model->users_id";
        $ext = $model->$fieldName->getExtensionName();
        $fileName = date('Y-m-d').time();
        $fileName = $fileName."-".ActiveRecord::randString().$count.'.'.$ext;
        $imageProcessing = new ImageProcessing();
        $imageProcessing->createDirectoryByPath($pathUpload);
        $model->$fieldName->saveAs($pathUpload.'/'.$fileName);
        return $fileName;
    }
    /** @Author: HOANG NAM 24/01/2018
     *  @Todo: resize images
     *  @Code: NAM005
     **/
    public function resizeImage($mGasFileScan, $model, $fieldName) {
        $pathUpload = $model->pathUpload."/$model->users_id";
        $ImageHelper = new ImageHelper();     
        $ImageHelper->folder = '/'.$pathUpload;
        $ImageHelper->file = $model->$fieldName;
        $ImageHelper->aRGB = array(0, 0, 0);//full black background
        $ImageHelper->thumbs = $model->aSize;
        $ImageHelper->createThumbs();
        $ImageHelper->deleteFile($ImageHelper->folder . '/' . $model->$fieldName);        
    }
    /** @Author: HOANG NAM 24/01/2018
     *  @Todo: Delete file
     *  @Code: NAM005
     *  @Param: 
     **/
    public function removeFileOnly($model, $fieldName) {
        if (is_null($model) || empty($model->$fieldName))
            return;
        $pathUpload = $model->pathUpload."/$model->users_id";
        $ImageHelper = new ImageHelper();     
        $ImageHelper->folder = '/'.$pathUpload;
        $ImageHelper->deleteFile($ImageHelper->folder . '/' . $model->$fieldName);
        foreach ( $model->aSize as $key => $value) {
            $ImageHelper->deleteFile($ImageHelper->folder . '/' . $key . '/' . $model->$fieldName);
        }
    }
    /** @Author: HOANG NAM 05/03/2018
     *  @Todo: validate file
     *  @Code: NAM005
     *  @Param: 
     **/
    public function ValidateFile($mUser) {
        $ClassName = get_class($mUser);
        if(isset($_POST['Users']['file_name']['new'])  && count($_POST['Users']['file_name']['new'])){
            foreach($_POST['Users']['file_name']['new'] as $key=>$item){
                $mFile = new EmployeesImages();                
                $mFile->file_name  = CUploadedFile::getInstance($mUser,'file_name[new]['.$key.'][image]');
                $mFile->validate();
                if(!is_null($mFile->file_name) && !$mFile->hasErrors() ){
                    MyFormat::IsImageFile($_FILES[$ClassName]['tmp_name']['file_name']['new'][$key]['image']);
                    $FileName = MyFunctionCustom::remove_vietnamese_accents($mFile->file_name->getName());
                    if(strlen($FileName) > 100 ){
                        $mFile->addError('file_name', "Tên file không được quá 100 ký tự, vui lòng đặt tên ngắn hơn");
                    }
                }
                if($mFile->hasErrors()){
                    $mUser->addError('file_name', $mFile->getError('file_name'));
                }
            }
        }
    }
    
    /** @Author: DungNT Apr 24, 2019
     *  @Todo: check required scam 2 mat CMND
     **/
    public function doCheckRequiredCmnd($mUser, $typeCheck) {
        $countFile = 0;
        if(isset($_POST['Users']['file_name']['new'])):
            foreach ($_POST['Users']['file_name']['new']['image'] as $keyfile => $valueFile) {
                $mEmployeeImage = new EmployeesImages();
                $mEmployeeImage->type           = $_POST['Users']['file_name']['new']['type'][$keyfile];
                $mEmployeeImage->users_id       = $mUser->id;
                $mEmployeeImage->sale_id        = $mUser->sale_id;
                $mEmployeeImage->file_name      = CUploadedFile::getInstance($mUser,'file_name[new][image]['.$keyfile.']');
                if(!is_null($mEmployeeImage->file_name) && $typeCheck == $mEmployeeImage->type ):
                    $countFile++;
                endif;
            }
        endif;
        
        if($countFile < $this->minFileRequired){
            $mUser->addError('file_name', 'Bạn chưa cập nhật 2 mặt của chứng minh nhân dân hoặc thẻ căn cước công dân');
        }
    }
    
    /** @Author: DungNT Apr 24, 2019 
     *  @Todo: check required scam 2 mat CMND
     **/
    public function checkRequiredCmnd($mUser, $typeCheck) {
        $cUid = MyFormat::getCurrentUid();
        if(!in_array($cUid, $mUser->mProfile->getListUidUpdateAll())){
//        if(1){
            $this->doCheckRequiredCmnd($mUser, $typeCheck);
        }
    }
    
    /** @Author: HOANG NAM 05/03/2018
     *  @Todo: get dropdow
     *  @Code: NAM005
     *  @Param: 
     **/
    public function getDropdownType($field_name,$name='old',$type = null){
        $Dropdown ='';
        $Dropdown .='<select style="width:80%!important;" class="full-control typeSelect" name="'.$field_name.'['.$name.'][type][]">';
        $Dropdown .='<option value="">Select</option>';
        foreach ($this->getListType() as $keyView => $valueView) {
            $select = '';
            if(isset($type) && $keyView == $type):
                $select = 'selected="selected"';
            endif;
            $Dropdown .='<option '.$select.' value="'.$keyView.'">'.$valueView.'</option>';
        }
        $Dropdown .='</select>';
        return $Dropdown;
    }
    
    /** @Author: ANH DUNG Feb 19, 2018
     *  @Todo: gom gọn function
     **/
    public function handleImage($mUser) {
        $this->updateEmployeesImages($mUser);
        $this->deleteEmployeesImages($mUser);
        $this->addEmployeesImages($mUser);
    }
   
    
    
    /** @Author: ANH DUNG Aug 19, 2018
     *  @Todo: web check update
     **/
    public function canUpdate(){
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        $aRoleAllow = [ROLE_SALE_ADMIN, ROLE_ADMIN];
        if(in_array($cRole, $aRoleAllow)){
            return true;
        }
        $aRoleAllow = [ROLE_SALE_ADMIN];
        if(!in_array($cRole, $aRoleAllow)){
            return false;
        }
        $defaultDay = 1;
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, $defaultDay, '-');
        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }
    
    /** @Author: ANH DUNG Sep 02, 2018
     *  @Todo: check user upload file
     **/
    public function countByUser() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.users_id=' . $this->users_id);
        if(is_array($this->type)){
            $criteria->addInCondition('t.type', $this->type);
        }elseif(!empty($this->type)){
            $criteria->addCondition('t.type=' . $this->type);
        }
        return EmployeesImages::model()->count($criteria);
    }
    
    /** @Author: DuongNV Sep 18, 2018
     *  @Todo: get multi image for 1 user in index, hiện hình ảnh file
     **/
    public function getMultiImages(){ 
        $criteria = new CDbCriteria;
        $criteria->compare('t.users_id', $this->users_id);
        $aEmployeesImages = EmployeesImages::model()->findAll($criteria);
        $html = '';
        foreach ($aEmployeesImages as $model) {
            $html.="<a class='gallery' href='".Yii::app()->createAbsoluteUrl('admin/ajax/viewImageProfileHs', array('id'=>$model->id,'model'=>'EmployeesImages'))."'> ";
                $html.="<img width='80' height='60' src='".ImageProcessing::bindImageByModel($model,'','',array('size'=>'size1'))."'>";
            $html.="</a>";
        }
        return $html;
    }
    
    public function searchDetail()
    {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        $criteria=new CDbCriteria;
        $criteria->compare('t.type', $this->type);
        $criteria->compare('t.created_by', $this->created_by);
        $aUser = array();
        if(!empty($this->users_id)){
            $aUser[]= $this->users_id;
        }
        if(!empty($this->employee_id)){
            $aUser[]= $this->employee_id;
        }
        if(!empty($aUser)){
            $sParamsIn = implode(',',  $aUser);
            $criteria->addCondition("t.created_by IN ($sParamsIn) OR t.sale_id IN ($sParamsIn)");
        }
        if(!empty($this->created_date)){
            $created_date = MyFormat::dateDmyToYmdForAllIndexSearch($this->created_date);
        }
        $date_from = $date_to = '';
            if(!empty($this->date_from)){
                $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
                DateHelper::searchGreater($date_from, 'created_date_bigint', $criteria);
            }
            if(!empty($this->date_to)){
                $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
                DateHelper::searchSmaller($date_to, 'created_date_bigint', $criteria, true);
            }
        if(in_array($cRole, $this->getRoleLikeSale())){
            $criteria->compare('t.sale_id', $cUid);
        }
        
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
    
    public function getSale($field_name = 'first_name'){
        $mUser = $this->rSale;
        return !empty($mUser) ? $mUser->$field_name : '';
    }
}
