<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BranchCompany
 *
 * @author nhh
 * $gender type branch or address
 * $parent_id type id company
 * $area_code_id type parent id of branch
 * $first_name type name branch or address
 * $code_account type code branch or address
 * $province_id type province id branch or address
 * $district_id type district_id branch or address
 */
class BranchCompany extends Users {

    // type of branch company
    const IS_BRANCH = 0; //
    const IS_ADDRESS = 1; //địa điểm

    public $amount_discount, $agent_id, $real_status_agent, $to_excel;

    public function getArrayManageBranch() {
        return [
            BranchCompany::IS_BRANCH => 'Chi nhánh',
            BranchCompany::IS_ADDRESS => 'Địa điểm',
        ];
    }

    /** @Author: NamNH Aug 15, 2019
     * */
    public function attributeLabels() {
        $aLabels = parent::attributeLabels();
        $aLabels['created_by'] = 'Người tạo';
        return $aLabels;
    }

    public function rules() {
        $aLabels = parent::rules();
        $aLabels[] = array('gender, parent_id, code_account, province_id, district_id', 'required', 'on' => 'create_branch_company,update');
        $aLabels[] = array(' area_code_id, agent_id, real_status_agent', 'safe');
        return $aLabels;
    }

    public function searchBranchCompany($type='') {
        $criteria = new CDbCriteria;
        // DuongNV Oct1019
        if(!empty($this->agent_id)){
            $mAgent = Users::model()->findByPk($this->agent_id);
            $this->id = empty($mAgent) ? '' : $mAgent->sale_id;
        }
        if(!empty($this->real_status_agent)){
            $criteriaUser = new CDbCriteria;
            $criteriaUser->compare('t.role_id', ROLE_AGENT);
            $criteriaUser->compare('t.first_char', $this->real_status_agent);
            $aAgent = Users::model()->findAll($criteriaUser);
            $listIDAgent = CHtml::listData($aAgent, 'sale_id', 'sale_id');
            $criteria->addInCondition('t.id', $listIDAgent);
        }
        $criteria->compare('t.role_id', ROLE_BRANCH_COMPANY);
        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.created_by', $this->created_by);
        $criteria->compare('t.province_id', $this->province_id);
        $criteria->compare('t.district_id', $this->district_id);
        $criteria->compare('t.parent_id', $this->parent_id);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('t.gender', $this->gender);
        $criteria->compare('t.code_account', $this->code_account, true);
        if($type == self::IS_ADDRESS){
            $criteria->compare('t.gender', $type);
            $criteria->addCondition('t.area_code_id != 0');
        }else{
            $criteria->compare('t.area_code_id',0);
        }
        $this->LimitProvince($criteria); // ADD DEC 29, 2014

        $sort = new CSort();
        $sort->attributes = array(
            'first_name' => 'first_name',
            'code_account' => 'code_account',
            'parent_id' => 'parent_id',
            'province_id' => 'province_id',
            'district_id' => 'district_id'
        );
        $sort->defaultOrder = 't.id DESC';
        $_SESSION['data-excel-branch'] = new CActiveDataProvider($this, array(
            'pagination' => false,
            'criteria' => $criteria,
            'sort' => $sort,
        ));
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => $this->pageSize,
            ),
            'sort' => $sort,
        ));
    }

    public function getArrBranch($parent_id = '', $branch_parent_id = '', $option = '') {
        $criteria = new CDbCriteria;
        $criteria->compare('t.role_id', ROLE_BRANCH_COMPANY);
        $criteria->compare('t.status', STATUS_ACTIVE);

        if (empty($branch_parent_id) && empty($parent_id)) {
            return [];
        }
        if (!empty($parent_id)) {
            $criteria->compare('t.parent_id', $parent_id);
            $criteria->addCondition('t.area_code_id = 0');
            if (!empty($option) && $option['task'] == 'get-parent-branch') {
//                $criteria->compare('t.gender', BranchCompany::IS_BRANCH);
            }
        }
        if (!empty($branch_parent_id)) {
            $criteria->compare('t.area_code_id', $branch_parent_id);
//            $criteria->compare('t.gender', BranchCompany::IS_ADDRESS);
        }

        $criteria->order = 't.id ASC';
        $models = Users::model()->findAll($criteria);
        return CHtml::listData($models, 'id', 'first_name');
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /** @Author: NamNH Aug 15,2019
     *  @Todo: get code account
     * */
    public function getCodeAccountBranch() {
        return $this->code_account . '';
    }

    public function getArrayUserCanupdateAll(){
        return [
            GasConst::UID_HANH_NT,
        ];
    }
    /** @Author: NamNH Aug 15, 2019
     *  @Todo: can update
     * */
    public function canUpdateBranch() {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        $aUidApply = $this->getArrayUserCanupdateAll();
        if ($cRole == ROLE_ADMIN || in_array($cUid, $aUidApply)) {
            return true;
        }
        if ($cUid == $this->created_by) {
            return true;
            $currentDate = date('Y-m-d');
            $createdDate = MyFormat::dateConverYmdToDmy($this->created_date, 'Y-m-d');
            if ($currentDate == $createdDate) {
                return true;
            }
        }
        return false;
    }

    /** @Author: NamNH Aug 15, 2019
     *  @Todo: can update
     * */
    public function canCreatedBranch() {
        return true;
    }

    /** @Author: NamNH Aug 15,2019
     *  @Todo: set default attributes
     * */
    public function setDefaultAttributes() {
        $this->gender = self::IS_BRANCH;
    }

    public function getAgentList() {
        $mUsers = $this->getListModelAgent();
        $aResult = [];
        foreach ($mUsers as $key => $mUsers) {
            $str = '';
            $str .= '<b>' . $mUsers->first_name . '</b>';
            $str .= ' - ';
            $str .= $mUsers->address;
            $aResult[] = $str;
        }
        $br = $this->to_excel ? PHP_EOL : "<br><br>";
        return implode($br, $aResult);
    }
    
    public function getRealStatusAgentOfBranch() {
        $aResult = [];
        $mUsers = $this->getListModelAgent();
        foreach ($mUsers as $mUsers) {
            $str = $mUsers->getRealStatusAgent();
            $aResult[] = $str;
        }
        $br = $this->to_excel ? PHP_EOL : "<br><br>";
        return implode($br, $aResult);
    }
    
    /** @Author: DuongNV Oct1019
     *  @Todo: get list model agent of branch
     **/
    public function getListModelAgent(){
        if (empty($this->id)) {
            return '';
        }
        $criteria = new CDbCriteria;
        $criteria->compare('t.role_id', ROLE_AGENT);
        $criteria->compare('t.sale_id', $this->id);
        return Users::model()->findAll($criteria);
    }

    /** @Author: NamNH Aug
     *  @Todo: get status agent
     * */
    public function getStatusAgent() {
        $aResult = [];
        if (empty($this->id)) {
            return '';
        }
        $criteria = new CDbCriteria;
        $criteria->compare('t.role_id', ROLE_AGENT);
        $criteria->compare('t.sale_id', $this->id);
        $mUsers = Users::model()->findAll($criteria);
        foreach ($mUsers as $key => $mUsers) {
            $str = $mUsers->getStatusText();
            $aResult[] = $str;
        }
        $br = $this->to_excel ? PHP_EOL : "<br><br>";
        return implode($br, $aResult);
    }

    /** @Author: NamNH Aug 23, 2019
     *  @Todo: get view
     * */
    public function getProfileView() {
        $aResult = [];
        $aModel = $this->getProfileModel();
        if (empty($aModel)) {
            return '';
        }
        foreach ($aModel as $key => $model) {
            $aResult[] = CmsFormatter::formatDetailGasProfile($model);
        }
        return implode('<br><br>', $aResult);
    }

    public function getProfileModel() {
        if (empty($this->id)) {
            return null;
        }
        $criteria = new CDbCriteria;
        $criteria->compare('t.role_id', ROLE_AGENT);       
        $criteria->addCondition("t.sale_id = $this->id OR t.storehouse_id = $this->id");       
        $mUsers = Users::model()->find($criteria);
        if (empty($mUsers)) {
            return null;
        }
        return $this->getProfile($mUsers->id);
    }

    public function getProfile($agent_id) {
        if (empty($agent_id)) {
            return null;
        }
        $criteria = new CDbCriteria;
        $criteria->compare('t.agent_id', $agent_id);
        $criteria->compare('t.type', GasProfile::P_TYPE_1);
        $criteria->compare('t.status', 1);
        $criteria->order = 't.id DESC';
        return GasProfile::model()->findAll($criteria);
    }
    
    /** @Author: HaoNH Now 01, 2019
     *  @Todo: lấy thông tin chi nhánh/địa điểm (chi nhánh show địa điểm con)
     *  @Param:
     **/
    public function getInfoBranch(){
        $aResult = [];
       
        if (empty($this->id)) {
            return "";
        }
        $criteria = new CDbCriteria;
        $criteria->compare('t.area_code_id',$this->id);
        $mAddress = Users::model()->findAll($criteria);
        
        $str = "<b>{$this->getFullName()}</b><br />";
        foreach ($mAddress as $key => $address) { 
            $str .= '<br />- '.$address->first_name;         
            
        }
        $aResult[] = $str;
        return implode('<br/>', $aResult);
    }
    
    /**
     * @Author: HaoNH Aug 28, 2019
     */
    public function getListdataBranch($current_id = '') {
        $aPraentBranch = Users::getArrDropdown(ROLE_BRANCH_COMPANY, ['task' => 'get-parent-branch', 'current_id' => $current_id]);

        return $aPraentBranch;
    }

}
