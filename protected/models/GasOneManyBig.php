<?php

/**
 * This is the model class for table "{{_gas_one_many_big}}".
 *
 * The followings are the available columns in table '{{_gas_one_many_big}}':
 * @property string $id
 * @property integer $one_id
 * @property integer $many_id
 * @property integer $type
 */
class GasOneManyBig extends CActiveRecord
{
    public $limit = 1000;
    // các define trước của Type ở ngoài config.local
//    define('ONE_MONITORING_MARKET_DEVELOPMENT', 4); // table one_many_big: nghĩa là 1 giám sát PTTT có nhiều nhiều nhân viên
//    define('ONE_USER_MANAGE_MULTIUSER', 5); // Dec 06, 2014 table one_many_big: trạng thái 5 này có nghĩa là 1 user có thể có ( quản lý) nhiều sub user thuộc nhiều role khác nhau
    const TYPE_LEAVE                = 6;// Sep 24, 2015 type user duyệt Nghỉ phép
    const TYPE_DELOY_BY             = 7;// Sep 24, 2015  type user Đơn vị thực hiện - Giao Nhiệm Vụ
    const TYPE_UPHOLD_SCHEDULE_DAY  = 8;// Now 27, 2015  check box các ngày phải đi bảo trì trong tháng của 1 KH
    const TYPE_ISSUE_ACCOUNTING     = 9;// Dec 20, 2015 type user Phản Ánh Sự Việc - Nhân viên kế toán được Anh Long chọn Xử lý ở App
    const TYPE_ORDER_PROMOTION      = 10;// Dec 30, 2015 type user nhận đặt hàng KM
    const TYPE_SETTLE_LEADER        = 11;// TrungND June 03, 2016 type leader duyệt quyết toán lần 1
    const TYPE_SETTLE_DIRECTOR      = 12;//TrungND  Dec 30, 2015 type director duyệt quyết toán lần 2
    const TYPE_SETTLE_CHIEF_ACCOUNTANT  = 13;//AnhDung  Jul 14, 2016 type Kế toán trưởng duyệt quyết toán lần 2
    const TYPE_SETTLE_ACCOUNTING        = 14;// AnhDung  Jul 25, 2016 type Kế toán thanh toán
    const TYPE_SETTLE_CASHIER_CONFIRM   = 15;//AnhDung  Jul 25, 2016 type Thủ quỹ
    const TYPE_APPROVE_SUPPORT_CUSTOMER = 16;//AnhDung  Jul 28, 2016 type người duyệt trong phiếu giao nv
    const TYPE_BORROW_ACCOUNTING        = 17;//AnhDung  Aug 02, 2016 type kế toán quản lý tạo mới vay

    const TYPE_CUSTOMER_SPECIAL_1   = 18;// AnhDung  Sep 04, 2016 duyệt KH đặc biệt lv1
    const TYPE_CUSTOMER_SPECIAL_2   = 19;// AnhDung  Sep 04, 2016 duyệt KH đặc biệt lv2
    const TYPE_CUSTOMER_SPECIAL_3   = 20;// AnhDung  Sep 04, 2016 duyệt KH đặc biệt lv3
    const TYPE_AGENT_BOSS           = 21;// AnhDung Now 10, 2016 Chủ cơ sở của Đại lý
    
    const TYPE_PRICE_REQUEST_1      = 22;// AnhDung Dec 18, 2016 duyệt đề xuất giá lần 1
    const TYPE_PRICE_REQUEST_2      = 23;// AnhDung Dec 18, 2016 duyệt đề xuất giá lần 2
    const TYPE_PRICE_REQUEST_3      = 24;// AnhDung Dec 18, 2016 duyệt đề xuất giá lần 3
    
    const TYPE_APPROVE_HR_PLAN      = 25;// Trung 198: [PHP][Task] Tạo chức năng quản lý kế hoạch nhân sự
    const TYPE_APPROVE_HR_INTERVIEW = 26;// Trung 198: [PHP][Task] Tạo chức năng quản lý kế hoạch nhân sự
    
    const TYPE_APPROVE_LEAVE_HOLIDAY = 27;// NAM 30/03/2018: [PHP] Chức năng kế hoạch nghỉ lễ
    
    const TYPE_EFFICIENCY_TYPE      = 28; // NAM 26/04/2018
    const FUNCTION_PARAMETER        = 29; // NAM 26/04/2018
    const FUNCTION_COEFFICIENTS     = 30; // NAM 26/04/2018 
    const TIMESHEETS_TYPE           = 31; // NAM 28/04/2018 
    
    const TYPE_RELATED_EMPLOYEE     = 32; // Duong Nov 14,2018 Nhân viên liên quan gasIssueTickets
    
    const TYPE_TELESALE             = 33; // NamNH Jan 11, 2019 save not call in telesale
    const TYPE_SELL_COMPLETE        = 34; // NamNH Sep1019 save not call sell complete
        
    
    public static $TYPE_ALLOW_AUTO = array(
        GasOneManyBig::TYPE_LEAVE,
        GasOneManyBig::TYPE_DELOY_BY,
    );
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_one_many_big}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, one_id, many_id, type', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
//			'one' => array(self::BELONGS_TO, 'Users', 'one_id'),
                'material' => array(self::BELONGS_TO, 'GasMaterials', 'many_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'one_id' => 'One',
            'many_id' => 'Many',
            'type' => 'Type',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        return array();
    }
        
// get mảng many id cho drop downlist or checkbox list multiselect
    public static function getArrOfManyId($one_id, $type)
    {
        $criteria=new CDbCriteria;
        if(is_array($one_id)){
            $sParamsIn = implode(',', $one_id);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.one_id IN ($sParamsIn)");
            }
        }else{
            $criteria->compare('t.one_id', $one_id);
        }
        
        if(is_array($type)){
            $sParamsIn = implode(',', $type);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.type IN ($sParamsIn)");
            }
        }else{
            $criteria->compare('t.type', $type);
        }
        $criteria->order = "t.id asc";
        return  CHtml::listData(self::model()->findAll($criteria),'many_id','many_id');  
    }
	
	// get mảng many id cho drop downlist or checkbox list multiselect
    public static function getArrModelOfManyId($one_id, $type)
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.one_id',$one_id);
        $criteria->compare('t.type',$type);
        $criteria->order = "t.id asc";
        return  self::model()->findAll($criteria);  
    }
	
    /**
     * @Author: ANH DUNG Sep 24, 2015
     * @Todo: something
     * GasOneManyBig::saveArrOfManyId($model->id, ONE_SELL_MAINTAIN_PROMOTION, 'GasMaintainSell', 'one_many_gift');
     */
    public static function saveArrOfManyId($one_id, $type, $name_model, $name_field)
    {
        GasOneManyBig::deleteOneByType($one_id, $type);
        $aRowInsert=array();
        
        if (isset($_POST[$name_model][$name_field]) && is_array($_POST[$name_model][$name_field]) && count($_POST[$name_model][$name_field]) > 0) {
            foreach ($_POST[$name_model][$name_field] as $item) {
                $aRowInsert[]="('$one_id',
                    '$item',
                    '$type'    
                    )";
            }
            $tableName = self::model()->tableName();
            $sql = "insert into $tableName (one_id,
                            many_id,
                            type
                            ) values ".implode(',', $aRowInsert);
            if(count($aRowInsert)>0)
                Yii::app()->db->createCommand($sql)->execute();          			
			
        }		
    }
    /**
     * @Author: ANH DUNG Jan 11, 2018 
     * @Todo: save model oneManyBig but not delete old item
     * GasOneManyBig::saveArrOfManyId($model->id, ONE_SELL_MAINTAIN_PROMOTION, 'GasMaintainSell', 'one_many_gift');
     */
    public function saveArrOfManyIdNotDeleteOld($one_id, $type, $name_model, $name_field){
        $aRowInsert = [];
        if (isset($_POST[$name_model][$name_field]) && is_array($_POST[$name_model][$name_field]) && count($_POST[$name_model][$name_field]) > 0) {
            foreach ($_POST[$name_model][$name_field] as $item) {
                $aRowInsert[]="('$one_id',
                    '$item',
                    '$type'    
                    )";
            }
            $tableName = self::model()->tableName();
            $sql = "insert into $tableName (one_id,
                            many_id,
                            type
                            ) values ".implode(',', $aRowInsert);
            if(count($aRowInsert)>0)
                Yii::app()->db->createCommand($sql)->execute();
        }
    }
    
    public static function deleteOneByType($one_id, $type){
        if(empty($one_id)){
            return ;
        }
        $criteria=new CDbCriteria;
        $criteria->compare('one_id',$one_id);
        $criteria->compare('type',$type);
        self::model()->deleteAll($criteria);        
    }
    
    /**
     * @Author: ANH DUNG Sep 25, 2015
     * @Todo: get array model user order by thứ tự thêm vào
     */
    public static function getArrayModelUser($one_id, $type, $needMore=array()) {
        $aUid = GasOneManyBig::getArrOfManyId($one_id, $type);
        $aModelUser = Users::getArrayModelByArrayId($aUid);
        $aRes = array();
        if(isset($needMore['get_id_name'])){
            foreach($aUid as $id){// for kiểu này để giữ lại cái order như khi thêm mới
                $aRes[$id] = $aModelUser[$id]->first_name;
            }
        }else{
            foreach($aUid as $id){// for kiểu này để giữ lại cái order như khi thêm mới
                if(!is_null($aModelUser[$id])){
                    $aRes[$id] = $aModelUser[$id];
                }
            }
        }
        return $aRes;
    }
    
    /** @Author: DungNT Jan 16, 2019
     *  @Todo: get by type
     **/
    public function getByType($type) {
        $criteria=new CDbCriteria;
        $criteria->compare('t.type', $type);
        $criteria->limit = $this->limit;
        return GasOneManyBig::model()->findAll($criteria);  
    }
    
}