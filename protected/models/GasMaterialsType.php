<?php

/**
 * This is the model class for table "{{_gas_materials_type}}".
 *
 * The followings are the available columns in table '{{_gas_materials_type}}':
 * @property integer $id
 * @property string $name
 * @property integer $status
 */
class GasMaterialsType extends CActiveRecord
{
    const GROUP_GAS         = 1;// Jul 08, 2016 xử lý định danh cho các loại gas, KM, vỏ
    const GROUP_PROMOTION   = 2;
    const GROUP_VO          = 3;

    const MATERIAL_VO_12            = 1;
    const MATERIAL_TYPE_DAY         = 2;
    const MATERIAL_TYPE_VAL         = 3;
    const MATERIAL_BINH_12KG        = 4;
    const MATERIAL_TYPE_BEP         = 5;
    const MATERIAL_TYPE_PROMOTION   = 6;
    const MATERIAL_BINH_45KG        = 7;
    const MATERIAL_VT_CONG_NGHIEP   = 8;// Vật tư công nghiệp
    const MATERIAL_BINH_6KG         = 9;
    const MATERIAL_VO_45            = 10;
    const MATERIAL_BINH_50KG        = 11;
    const MATERIAL_VO_50            = 12;
    const MATERIAL_GAS_BON          = 13;
    const MATERIAL_VO_6             = 14;
    const VT_NHOM_BO_GOP            = 15;
    const MATERIAL_TYPE_CCDC        = 16;
    const VT_NHOM_PHU_VAN_DIEU_AP   = 17;
    const MATERIAL_TYPE_GAS_LON     = 18;
    const MATERIAL_BINH_4KG         = 19;
    const TB_HO_TRO_KH              = 20;
    const VT_NHOM_VAN_LAP_BICH      = 21;
    
    const VT_NHOM_VAN_LAP_REN       = 22;
    const VT_NHOM_PHU_KIEN_ONG_THEP = 23;
    const VT_NHOM_PHU_KIEN_ONG_INOX = 24;
    const VT_NHOM_PHU_KIEN_ONG_KEM  = 25;
    const VT_NHOM_PHU_KIEN_ONG_NHUA = 26;
    const VT_NHOM_DAY_MEM_DAU_NOI   = 27;
    const VT_NHOM_MAY_HOA_HOI       = 28;
    const VT_NHOM_HE_THONG_BAO_GAS  = 29;
    const VT_NHOM_DUONG_ONG         = 30;
    const VT_NHOM_THIET_BI_PCCC     = 31;
    const VT_NHOM_BEP_CONG_NGHIEP   = 32;
    const VT_NHOM_LINH_KIEN_BEP     = 33;
    const MATERIAL_VO_4             = 34;
    const MATERIAL_ALCOHOL          = 35;

    public static $ARR_WINDOW_GAS = array(// Jun 03, 2016 type các loại gas api window
        GasMaterialsType::MATERIAL_TYPE_DAY => GasMaterialsType::MATERIAL_TYPE_DAY,
        GasMaterialsType::MATERIAL_TYPE_VAL => GasMaterialsType::MATERIAL_TYPE_VAL,
        GasMaterialsType::MATERIAL_BINH_12KG => GasMaterialsType::MATERIAL_BINH_12KG,
        GasMaterialsType::MATERIAL_TYPE_BEP => GasMaterialsType::MATERIAL_TYPE_BEP,
        GasMaterialsType::MATERIAL_BINH_45KG => GasMaterialsType::MATERIAL_BINH_45KG,
        GasMaterialsType::MATERIAL_VT_CONG_NGHIEP => GasMaterialsType::MATERIAL_VT_CONG_NGHIEP,
        GasMaterialsType::MATERIAL_BINH_6KG => GasMaterialsType::MATERIAL_BINH_6KG,
        GasMaterialsType::MATERIAL_BINH_50KG => GasMaterialsType::MATERIAL_BINH_50KG,
        GasMaterialsType::MATERIAL_BINH_4KG => GasMaterialsType::MATERIAL_BINH_4KG,
        GasMaterialsType::MATERIAL_ALCOHOL => GasMaterialsType::MATERIAL_ALCOHOL,// DungNT Sep0219
    );
    
    public static $ARR_WINDOW_VO = array(// Jun 03, 2016 type các loại gas api window
        GasMaterialsType::MATERIAL_VO_12,
        GasMaterialsType::MATERIAL_VO_45, // Dec 28, 2016 đại lý bán lẻ cả bình 45 + thu vỏ 45
        GasMaterialsType::MATERIAL_VO_50,
        GasMaterialsType::MATERIAL_VO_6,
        GasMaterialsType::MATERIAL_VO_4,
    );
    public static $ARR_VT_HGD = array(// Feb 23, 2017 type các loại val dây của app window, sẽ không có gas ở đây
        GasMaterialsType::MATERIAL_VO_12,
        GasMaterialsType::MATERIAL_TYPE_DAY,
        GasMaterialsType::MATERIAL_TYPE_VAL,
        GasMaterialsType::MATERIAL_TYPE_BEP,
        GasMaterialsType::MATERIAL_VT_CONG_NGHIEP,
        GasMaterialsType::MATERIAL_TYPE_PROMOTION,
    );
    
    public static $ARR_WINDOW_PROMOTION = array(// Jun 03, 2016 type các loại gas api window
        GasMaterialsType::MATERIAL_TYPE_PROMOTION,
    );
    public static $ARR_WINDOW_DISCOUNT_LIKE_PROMOTION= array(// Now 25, 2016 type các loại vật tư dc KM mà không phải là vật tư KM
        GasMaterialsType::MATERIAL_TYPE_DAY,
        GasMaterialsType::MATERIAL_TYPE_VAL,
    );

    public static $ARR_NOT_SEARCH = array(
//        GasMaterialsType::MATERIAL_TYPE_CCDC,
        // add from 28, 2016 
        GasMaterialsType::VT_NHOM_BO_GOP,
//        GasMaterialsType::VT_NHOM_PHU_VAN_DIEU_AP,// Jan 26, 2017 đóng lại để search ở thẻ kho
        GasMaterialsType::VT_NHOM_VAN_LAP_BICH,
        GasMaterialsType::VT_NHOM_VAN_LAP_REN,
        GasMaterialsType::VT_NHOM_PHU_KIEN_ONG_THEP,
        GasMaterialsType::VT_NHOM_PHU_KIEN_ONG_INOX,
        GasMaterialsType::VT_NHOM_PHU_KIEN_ONG_KEM,
        GasMaterialsType::VT_NHOM_PHU_KIEN_ONG_NHUA,
        GasMaterialsType::VT_NHOM_DAY_MEM_DAU_NOI,
        GasMaterialsType::VT_NHOM_MAY_HOA_HOI,
        GasMaterialsType::VT_NHOM_HE_THONG_BAO_GAS,
        GasMaterialsType::VT_NHOM_DUONG_ONG,
        GasMaterialsType::VT_NHOM_THIET_BI_PCCC,
        
    );
    
    public static $THIET_BI = array(
        GasMaterialsType::MATERIAL_TYPE_VAL,
        GasMaterialsType::MATERIAL_TYPE_DAY,
        GasMaterialsType::MATERIAL_TYPE_BEP,
        GasMaterialsType::TB_HO_TRO_KH,
        GasMaterialsType::VT_NHOM_BO_GOP,
        GasMaterialsType::VT_NHOM_PHU_VAN_DIEU_AP,
        GasMaterialsType::MATERIAL_TYPE_CCDC,
        GasMaterialsType::MATERIAL_VT_CONG_NGHIEP,

        GasMaterialsType::VT_NHOM_VAN_LAP_BICH,
        GasMaterialsType::VT_NHOM_VAN_LAP_REN,
        GasMaterialsType::VT_NHOM_PHU_KIEN_ONG_THEP,
        GasMaterialsType::VT_NHOM_PHU_KIEN_ONG_INOX,
        GasMaterialsType::VT_NHOM_PHU_KIEN_ONG_KEM,
        GasMaterialsType::VT_NHOM_PHU_KIEN_ONG_NHUA,
        GasMaterialsType::VT_NHOM_DAY_MEM_DAU_NOI,
        GasMaterialsType::VT_NHOM_MAY_HOA_HOI,
        GasMaterialsType::VT_NHOM_HE_THONG_BAO_GAS,
        GasMaterialsType::VT_NHOM_DUONG_ONG,
        GasMaterialsType::VT_NHOM_THIET_BI_PCCC,
        GasMaterialsType::VT_NHOM_BEP_CONG_NGHIEP,
        GasMaterialsType::VT_NHOM_LINH_KIEN_BEP,
    );
    
    public static $PROMOTION = array(// for đặt hàng khuyến mãi
        GasMaterialsType::MATERIAL_TYPE_PROMOTION,
        GasMaterialsType::MATERIAL_TYPE_BEP,
        GasMaterialsType::MATERIAL_TYPE_VAL,
        GasMaterialsType::MATERIAL_TYPE_DAY,
        GasMaterialsType::TB_HO_TRO_KH,
    );
    
    public static $SETUP_PRICE_GAS_12 = array(// Jun 05, 2016 for setup gas hộ GD /admin/usersPrice/setupGas1
        GasMaterialsType::MATERIAL_BINH_12KG,
    );
    public static $HGD_SELL_GAS = array(// JuL 12, 2017 check loại Gas bán cho Hộ GĐ, hiện mới có thêm 45kg và 50kg => xử lý đưa tiền vào detail Sell->putDiscountToDetail() => type_amount. biến cũ là $SETUP_PRICE_GAS_12 không xử lý đc B45 và 50
        GasMaterialsType::MATERIAL_BINH_12KG,
        GasMaterialsType::MATERIAL_BINH_50KG,
        GasMaterialsType::MATERIAL_BINH_45KG,
    );
    
    public static $SETUP_PRICE_VATTU = array(// Jun 05, 2016 for setup gas hộ GD /admin/usersPrice/setupGas1
        GasMaterialsType::MATERIAL_TYPE_BEP,
        GasMaterialsType::MATERIAL_TYPE_VAL,
        GasMaterialsType::MATERIAL_TYPE_DAY,
    );
    
    public static $ARR_GAS_VO = array(// Jun 29, 2016 type các loại gas + vỏ - dùng trong thống kê sản lượng xe gasreports/outputCar
        GasMaterialsType::MATERIAL_BINH_50KG => 'Bình 50 KG',
        GasMaterialsType::MATERIAL_BINH_45KG  => 'Bình 45 KG',
        GasMaterialsType::MATERIAL_BINH_12KG => 'Bình 12 KG',
        GasMaterialsType::MATERIAL_BINH_6KG => 'Bình 6 KG',
        GasMaterialsType::MATERIAL_VO_50 => 'Vỏ 50 KG',
        GasMaterialsType::MATERIAL_VO_45 => 'Vỏ 45 KG',
        GasMaterialsType::MATERIAL_VO_12 => 'Vỏ 12 KG',
        GasMaterialsType::MATERIAL_VO_6 => 'Vỏ 6 KG',
    );
    
    public static $ARR_GAS_VO_45KG = array(// Jun 31, 2016 type các loại gas + vỏ bình bò  - dùng trong thống kê sản lượng xe gasreports/outputCar
        GasMaterialsType::MATERIAL_BINH_50KG,
        GasMaterialsType::MATERIAL_BINH_45KG,
        GasMaterialsType::MATERIAL_VO_50,
        GasMaterialsType::MATERIAL_VO_45,
    );
    public static $ARR_VO_45KG = array(// Jun 31, 2016 type các loại gas + vỏ bình bò  - dùng trong thống kê sản lượng xe gasreports/outputCar
        GasMaterialsType::MATERIAL_VO_50,
        GasMaterialsType::MATERIAL_VO_45,
    );
    public static $ARR_GAS_VO_6KG = array(// Jun 31, 2016 type các loại gas + vỏ  6KG - dùng trong thống kê sản lượng xe gasreports/outputCar
        GasMaterialsType::MATERIAL_BINH_6KG,
        GasMaterialsType::MATERIAL_VO_6,
    );
    
    public static $ARR_GAS_VO_NHO = array(
        GasMaterialsType::MATERIAL_VO_12,
        GasMaterialsType::MATERIAL_VO_6,
        GasMaterialsType::MATERIAL_VO_4
    );
    
    public static $ARR_GAS_VO_LON = array(
        GasMaterialsType::MATERIAL_VO_45,
        GasMaterialsType::MATERIAL_VO_50
    );
    
    public static function getArrGasKg(){ // lấy các loại gas quy đổi ra KG
        return [
            GasMaterialsType::MATERIAL_BINH_50KG,
            GasMaterialsType::MATERIAL_BINH_45KG,
        ];
    }
    public static function getArrGasBB(){
        return [
            GasMaterialsType::MATERIAL_BINH_50KG,
            GasMaterialsType::MATERIAL_BINH_45KG,
        ];
    }
    public static function getArrVoAndGas(){ // lấy các loại gas quy đổi ra KG
        return [
            GasMaterialsType::MATERIAL_VO_50    => GasMaterialsType::MATERIAL_BINH_50KG,
            GasMaterialsType::MATERIAL_VO_45    => GasMaterialsType::MATERIAL_BINH_45KG,
            GasMaterialsType::MATERIAL_VO_12    => GasMaterialsType::MATERIAL_BINH_12KG,
            GasMaterialsType::MATERIAL_VO_6     => GasMaterialsType::MATERIAL_BINH_6KG,
            GasMaterialsType::MATERIAL_VO_4     => GasMaterialsType::MATERIAL_BINH_4KG,
        ];
    }
    public static function getArrAllVo(){ // lấy các loại vỏ hệ thống
        return [
            GasMaterialsType::MATERIAL_VO_50,
            GasMaterialsType::MATERIAL_VO_45,
            GasMaterialsType::MATERIAL_VO_12,
            GasMaterialsType::MATERIAL_VO_6,
            GasMaterialsType::MATERIAL_VO_4,
        ];
    }
    public static function getArrVoShortName(){
        return [
            GasMaterialsType::MATERIAL_VO_50 => 'Vỏ 50',
            GasMaterialsType::MATERIAL_VO_45 => 'Vỏ 45',
            GasMaterialsType::MATERIAL_VO_12 => 'Vỏ 12',
            GasMaterialsType::MATERIAL_VO_6 => 'Vỏ 6',
            GasMaterialsType::MATERIAL_VO_4 => 'Vỏ 4',
        ];
    }
    
    /** @Author: ANH DUNG Sep11, 2017
    *  @Todo: get type app Bò Mối
     */
    public static function getTypeBoMoi() {
        $aRes   = GasMaterialsType::$ARR_WINDOW_GAS;
        $aRes[GasMaterialsType::MATERIAL_TYPE_PROMOTION]    = GasMaterialsType::MATERIAL_TYPE_PROMOTION;
        $aRes[GasMaterialsType::TB_HO_TRO_KH]               = GasMaterialsType::TB_HO_TRO_KH;
        $aRes[GasMaterialsType::MATERIAL_TYPE_CCDC]         = GasMaterialsType::MATERIAL_TYPE_CCDC;
        return $aRes;
    }
    
    /** @Author: DungNT Sep 02, 2019
     *  @Todo: get array type when CallCenter create other item not Gas:ex Cồn, van day bep
     **/
    public function getTypeCallCenterMakeOrderBoMoi() {
        $aRes   = GasMaterialsType::$ARR_WINDOW_GAS;
        unset($aRes[GasMaterialsType::MATERIAL_BINH_50KG]);
        unset($aRes[GasMaterialsType::MATERIAL_BINH_45KG]);
        unset($aRes[GasMaterialsType::MATERIAL_BINH_12KG]);
        unset($aRes[GasMaterialsType::MATERIAL_BINH_6KG]);
        unset($aRes[GasMaterialsType::MATERIAL_BINH_4KG]);
        unset($aRes[GasMaterialsType::MATERIAL_VT_CONG_NGHIEP]);
        return $aRes;
    }

    // Oct2418 get array vỏ ko có vỏ 12 để làm bc công giao nhận. XĐ gas đi = vỏ về HGĐ
    public static function getTypeVoNot12kg() {
        return [
            MATERIAL_TYPE_VO_50,
            MATERIAL_TYPE_VO_45,
            MATERIAL_TYPE_VO_6,//  Vỏ Bình 6 Kg
            GasMaterialsType::MATERIAL_VO_4,
        ];
    }
    
    public function getArrayTypeEmptyPrice() {// các loại vật tư HGD không có giá 
        return [
            GasMaterialsType::MATERIAL_VO_12,
            GasMaterialsType::MATERIAL_TYPE_PROMOTION,
        ];
    }
    
    /**
     * @Author: ANH DUNG Sep 05, 2014
     * @Todo: lấy material loại công cụ dụng cụ
     */
    public static function getMaterialTypeCCDC(){
        return array(GasMaterialsType::MATERIAL_TYPE_CCDC);
    }
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_materials_type}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('name', 'required'),
            array('id, name, status', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Tên Loại Vật Tư',
            'status' => 'Tình Trạng',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.id',$this->id);
        $criteria->compare('t.name',$this->name,true);
        $criteria->compare('t.status',$this->status);
        $criteria->order = 't.id DESC';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 50
            ),
        ));
    }
    
    public function beforeSave() {
        $this->name = trim($this->name);
        return parent::beforeSave();
    }

    public static function getAllItem($aId=array(), $needMore=array()){
        $criteria=new CDbCriteria;
        $criteria->compare('t.status',STATUS_ACTIVE);
        if(is_array($aId) && count($aId)){
            $sParamsIn = implode(',', $aId);
            $criteria->addCondition("t.id IN ($sParamsIn)");
        }
        $criteria->order = 't.name ASC';
        $models = self::model()->findAll($criteria);
        if(isset($needMore['for_in_condition'])){
            return  CHtml::listData($models,'id','id');
        }
        return  CHtml::listData($models,'id','name');
    }        
    public function getName(){
        if($this){
            return ($this->name) ? $this->name : "";
        }
        return "";
    }
    
}