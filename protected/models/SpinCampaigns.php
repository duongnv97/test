<?php

class SpinCampaigns extends BaseSpj
{
    const TYPE_WEEK     = 0;
    const TYPE_MONTH    = 1;
    const TAB_WEEK      = 1;
    const TAB_MONTH     = 2;

    const APP_PAGE_SIZE         = 4;
    const APP_HISTORY_PAGE_SIZE = 10;

    const LIMIT_QTY_AWARD = 10; // limit số giải thưởng / 1 lần quay

    public $aDetail;
    public $materials_name, $number, $rank, $order;
    public $mAppUserLogin; //api

    public function getArrayType(){
        return [
            self::TYPE_WEEK   => 'Tuần',
            self::TYPE_MONTH  => 'Tháng'
        ];
    }
    public function getArrayTabType(){
        return [
            self::TAB_WEEK  => 'Tuần',
            self::TAB_MONTH => 'Tháng'
        ];
    }
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return SpinCampaigns the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_spin_campaigns}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
                array('name,campaign_date', 'required'),
                array('qty_spin, type', 'numerical', 'integerOnly'=>true),
                array('user_total, created_by', 'length', 'max'=>11),
                array('name, description, created_date, date_from, date_to', 'safe'),
                array('id, name, campaign_date, qty_spin, type, user_total, description, created_date, created_by', 'safe', 'on'=>'search'),
                array('number, rank, order', 'safe'),
            );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rCampaignDetail' => array(self::HAS_MANY, 'SpinCampaignDetails', 'campaign_id'),
            'rCountDetail'    => array(self::HAS_MANY, 'SpinCampaignDetails', 'campaign_id',
                'select' =>  '*, count(rCountDetail.id) as qty_award',
                'group'  =>  'rCountDetail.campaign_id, rCountDetail.rank'),
            'rUsers'          => array(self::BELONGS_TO, 'Users', 'created_by')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
                'id'            => 'ID',
                'name'          => 'Tên chiến dịch',
                'campaign_date' => 'Ngày diễn ra chiến dịch',
                'qty_spin'      => 'Số lượt quay',
                'type'          => 'Loại',
                'user_total'    => 'Số người tham gia',
                'description'   => 'Mô tả',
                'created_date'  => 'Ngày tạo',
                'created_by'    => 'Người tạo',
                'number'        => 'Số trúng thưởng',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search(){
        $criteria=new CDbCriteria;
        $criteria->compare('id',$this->id,true);
        $criteria->addSearchCondition('name',$this->name,true);
        $criteria->compare('qty_spin',$this->qty_spin);
        $criteria->compare('type',$this->type);
        $criteria->compare('user_total',$this->user_total);
        $criteria->addSearchCondition('description',$this->description,true);
        $criteria->order = 't.id DESC';
        if (!empty($this->date_from)) {
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
            $criteria->addCondition("DATE(campaign_date) >= '$date_from'");
        }
        if (!empty($this->date_to)) {
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
            $criteria->addCondition("DATE(campaign_date) <= '$date_to'");
        }
        $criteria->compare('created_by',$this->created_by,true);
        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> 20,
                ),  
        ));
    }

    public function getName(){
        return empty($this->name)?'':$this->name;
    }


    public function getCampaignDate(){
        return empty($this->campaign_date)?'':MyFormat::dateConverYmdToDmy($this->campaign_date, 'd/m/Y');
    }

    public function getQtySpin(){
        return $this->qty_spin;
    }

    public function getType() {
        $aStatus = SpinCampaigns::getArrayType();
        return empty($aStatus[$this->type]) ? '' : $aStatus[$this->type];
    }

    public function getUserTotal(){
        return $this->user_total;
    }
    
    public function getLiveUser(){
        $criteria = new CDbCriteria();
        $criteria->select = 't.id';
        $criteria->addCondition('t.user_id > 0');
        $criteria->compare('t.status', SpinNumbers::STATUS_NORMAL);
        return SpinNumbers::model()->count($criteria);
    }

    public function getDescription(){
        return $this->description;
    }

    public function getCreatedBy(){
        return $this->created_by = empty($this->rUsers)?'':$this->rUsers->getFullName();   
    }

    /** @Author: NamLA Aug 22, 2019
    *  @Todo: get detail on table detail of create
    *  @Param: param
    **/
    public function getDetail(){
        $this->aDetail = $this->rCountDetail;
        return $this->aDetail;
    }

    /** @Author: NamLA Aug 22, 2019
    *  @Todo: get detail post on create campaign
    *  @Param: 
    **/
    public function getDetailPost(){
        $aDetail        = isset($_POST['SpinCampaignDetails']) ? $_POST['SpinCampaignDetails'] : [];
        if(empty($aDetail)) 
        if(!isset($aDetail) || !is_array($aDetail)){
            $this->addError('materials_name',"Chi tiết quà tặng không hợp lệ");
            return ;
        }
        $this->aDetail  = [];
        $this->qty_spin = 0;
        $hasError       = true;
        foreach($aDetail['qty_award'] as $keyRow => $rowDetail){ // mỗi giải thưởng có nhiều sản phẩm
            for($i = 1; $i <= $rowDetail; $i++){
                $mDetail                = new SpinCampaignDetails();
                $mDetail->rank          = isset($aDetail['rank'][$keyRow]) ? $aDetail['rank'][$keyRow] : SpinCampaignDetails::RANK_DEFAULT;
                $mDetail->status        = SpinCampaignDetails::STATUS_NOTGAVE; 
                if (empty($aDetail[$mDetail->rank]['award_json'])){
                    $json = '';
                } else {
                    $json = '['.implode(",", $aDetail[$mDetail->rank]['award_json']).']';
                }
                $mDetail->award_json = $json;
                if(!$mDetail->hasErrors()){
                    $this->aDetail[]    = $mDetail;

                    $hasError           = false;
                }
            $this->qty_spin++;
           }
        }
        if($hasError){
            $this->addError('marterials_id',"Chi tiết quà tặng không hợp lệ");
        }
    }

    /** @Author: NamLA Aug 22, 2019
    *   @Todo: save campaign_detail after save campaign
    *   @Param: 
    **/
    public function saveDetailPost(){
        $mDetail                = new SpinCampaignDetails();
        $mDetail->campaign_id   = $this->id;
        $mDetail->deleteDetail();
        $aRowInsert             = [];
        if( empty($this->aDetail) || !is_array($this->aDetail)){
            return false;
        }
        foreach($this->aDetail as $detail){
            $detail->campaign_id   = $this->id;  
            $this->windowSaveDetail($aRowInsert, $detail);
        }
        $this->windowSaveDetailRunSql($aRowInsert);
    }

    /** @Author: NamLA Aug 22, 2019
    *   @Todo: one query to insert detail
    *   @Param: 
    **/
    public function windowSaveDetail(&$aRowInsert, $mDetail){
        $aRowInsert[] = "('$mDetail->campaign_id', 
                        '$mDetail->user_id',
                        '$mDetail->number',
                        '$mDetail->order_number',
                        '$mDetail->rank',
                        '$mDetail->award_json')";
    }

    /** @Author: NamLA Aug 22, 2019
    *  @Todo: save one query to insert detail
    *  @Param: 
    **/
    public function windowSaveDetailRunSql($aRowInsert) {
        $tableName  = SpinCampaignDetails::model()->tableName();
        $sql        = "insert into $tableName (campaign_id, 
                        user_id, 
                        number,
                        order_number,
                        rank,
                        award_json
                        ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert)>0){
            Yii::app()->db->createCommand($sql)->execute();
        }
    }
    public function handleBeforeSave(){
        $this->created_by       = MyFormat::getCurrentUid();
        $this->campaign_date    = MyFormat::dateConverDmyToYmd($this->campaign_date, '-');
    }

    /**
    * @Author: NamLA Aug 16, 2019
    * @Todo: get campaign detail for view
    */
    public function getSummaryTable(){
        $str = '';
        if( !empty($this->rCountDetail) ):
            $str .= '<table style="border-collapse:collapse;">';
            $str .=     '<thead>';
            $str .=         '<tr>';
            $str .=             '<td style="border:1px solid black;"><b>Giải</b></td>';
            $str .=             '<td style="border:1px solid black;"><b>Chi tiết phần thưởng</b></td>';
            $str .=             '<td style="border:1px solid black;"><b>Số lượng giải</b></td>';
            $str .=         '</tr>';
            $str .=     '</thead>';
            $str .=     '<tbody>';
            foreach($this->rCountDetail as $detail) {
                $aMaterials = json_decode($detail->award_json, true);
                $str .= '<tr>';
                $str .=     '<td style="border:1px solid black;">'.$detail->getRank().'</td>';
                $str .=     '<td style="border:1px solid black;">';
                $str .=     '<table style="border-collapse:collapse;">';
                $str .=         '<thead>';
                $str .=             '<tr>';
                $str .=                 '<td style="border:1px solid black;"><b>Tên</b></td>';
                $str .=                 '<td style="border:1px solid black;"><b>SL</b></td>';
                $str .=             '</tr>';
                $str .=         '</thead>';
                $str .=         '<tbody>';
                if (!empty($aMaterials)){
                    foreach($aMaterials as $result){
                       $str .=      '<tr>';
                       $str .=          '<td style="border:1px solid black;">'.$result['name'].'</td>';
                       $str .=          '<td style="border:1px solid black;">'.$result['qty'].'</td>';
                       $str .=      '</tr>';
                    }
                }
                $str .=         '</tbody>';
                $str .=     '</table>';
                $str .=     '</td>';
                $str .=     '<td style="border:1px solid black; text-align: center">'.$detail->qty_award.'</td>';                  
                $str .='</tr>';
            }
            $str .=     '</tbody>';
            $str .= '</table>';
        endif;
        return $str;

    }
    
    /** @Author: NhanDT Aug 20, 2019
     *  @Todo: Update số lượng người tham gia quay số trúng thưởng
     *  @Param: param
     **/
    public function updateTotalUser($campaign_id) {
        if(empty($campaign_id))
            return;
        $model      = new SpinNumbers();
        $criteria   = new CDbCriteria();
        $criteria   ->compare('id',$campaign_id);
        $aUpdate    = array('user_total'=> $model->getQtyUsersUnexpired());
        self::model()->updateAll($aUpdate,$criteria);
    } 
    
    /** @Author: NhanDT Aug 21, 2019
     *  @Todo: check day spin, chỉ được quay khi ngày hiện tại = ngày diễn ra chương trình 
     *  @Param: param
     **/
    public function checkCanSpin() {
        $cRole  = MyFormat::getCurrentRoleId();
        if($cRole == ROLE_ADMIN){
            return TRUE;
        }
        if (empty($this->campaign_date))
            return FALSE;
        return MyFormat::getNumberOfDayBetweenTwoDate($this->campaign_date, date('Y-m-d')) == 0;
    }
    
    /** @Author: DuongNV
     *  @Todo: get campaign history, find campaign
     **/
    public function apiGetCampaignHistory(&$result, $q) {
        $result['record']           = [];
        $mUser                      = $this->mAppUserLogin;
        if( !empty($mUser) ):
            if( empty($mUser->id) ) return $result;
            
            $cDate                  = date('Y-m-d');
            $destinateDate          = MyFormat::modifyDays($cDate, '3', '-', 'month');
            $criteria               = new CDbCriteria;
            $criteria->addCondition("t.campaign_date >= '$destinateDate'");
            $criteria->order        = 't.campaign_date DESC';
            $dataProvider           = $this->apiGetDataPrivider($criteria, $this, $q->page, 2);
            $models                 = $dataProvider->data;
            $CPagination            = $dataProvider->pagination;
            $result['total_record'] = $CPagination->itemCount;
            $result['total_page']   = $CPagination->pageCount;
            $groupedData            = [];
            foreach ($models as $mCampaign) :
                $campaignDate   = MyFormat::dateConverYmdToDmy($mCampaign->campaign_date);
                $aDetail        = empty($mCampaign->rCampaignDetail) ? [] : $mCampaign->rCampaignDetail;
                $aNewData       = [];
                if( !empty($aDetail) ){
                    foreach ($aDetail as $mDetail) {
                        $aNewData = [
                            'award'  => $mDetail->getRank(),
                            'number' => empty($mDetail->number) ? '...' : $mDetail->number,
                        ];
                        $groupedData[$campaignDate]['date']     = $campaignDate;
                        $groupedData[$campaignDate]['awards'][] = $aNewData;
                    }
                }
            endforeach;
            
            $result['record'] = array_values($groupedData);
        endif;
    }
    
    /** @Author: DuongNV
     *  @Todo: get campaign history, find detail
     **/
    public function apiGetCampaignHistoryV2(&$result, $q) {
        $result['record']           = [];
        $mUser                      = $this->mAppUserLogin;
        if( !empty($mUser) ):
            if( empty($mUser->id) ) return $result;
            
            $cDate                  = date('Y-m-d');
            $destinateDate          = MyFormat::modifyDays($cDate, '3', '-', 'month');
            $criteria               = new CDbCriteria;
            $criteria->addCondition("DATE(t.created_date) >= '$destinateDate'");
            $criteria->order        = 't.created_date DESC';
            $dataProvider           = $this->apiGetDataPrivider($criteria, new SpinCampaignDetails(), $q->page, 2);
            $models                 = $dataProvider->data;
            $CPagination            = $dataProvider->pagination;
            $result['total_record'] = $CPagination->itemCount;
            $result['total_page']   = $CPagination->pageCount;
            $groupedData            = [];
            foreach ($models as $mCampaignDetail) :
                $campaignDate   = MyFormat::dateConverYmdToDmy($mCampaignDetail->created_date);
                $aNewData       = [];
                        $aNewData = [
                            'award'  => $mCampaignDetail->getRank(),
                            'number' => $mCampaignDetail->number,
                            'date'   => $campaignDate,
                        ];
                        $groupedData[$campaignDate][] = $aNewData;
            endforeach;
            $result['record'] = $groupedData;
        endif;
    }
    
    /** @Author: DuongNV Aug2219
     *  @Todo:
     *  @Param:
     **/
    public function apiGetDataPrivider($criteria, $model, $cPage = 0, $pageSize = SpinCampaigns::APP_PAGE_SIZE) {
        $dataProvider = new CActiveDataProvider($model, array(
            'criteria'  => $criteria,
            'pagination'=>array(
                'pageSize'      => $pageSize,
                'currentPage'   => $cPage,
            ),
          ));
        return $dataProvider;
    }
    
    /** @Author: NamLA Aug2219
     *  @Todo:
     *  @Param:
     **/
    public function saveDetailName(){
        $this->campaign_name    = empty($this->campaign_id) ? '' : $this->getCampaignName();
        $this->user_name        = empty($this->user_id) ? '' : $this->getUserName();
        $this->material_name    = empty($this->material_id) ? '' : $this->getMaterialName();
    }
    
    /** @Author: DuongNV Sep2019
     *  @Todo: notify winning user
     *  @Param:$id campaign id
     **/
    public function notifyWinning() {
        $criteria       = new CDbCriteria;
        $criteria->compare('t.campaign_id', $this->id);
        $models         = SpinCampaignDetails::model()->findAll($criteria);
        $mSpinNumbers   = new SpinNumbers();
        $numberNotify   = 0;
        foreach($models as $mCampaignDetail){
            $mSpinNumbers->notifyUser($mCampaignDetail->user_id, GasScheduleNotify::GAS24H_NUMBER_WINNING);
            $numberNotify++;
        }
        return $numberNotify;
    }
}
