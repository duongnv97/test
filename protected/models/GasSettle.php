<?php

/**
 * This is the model class for table "{{_settle}}".
 *  
 * The followings are the available model relations:
 * @property GasDebts[]             $rGasDebts          List gas debts models
  */
class GasSettle extends BaseSpj
{
    // 1: Mới;2: Leader đã xác nhận;3: Giám đốc đã xác nhận;4: Chờ quyết toán; 5: Hoàn tất; 6:Hủy bỏ; 7: Yêu cầu sửa lại
    const STA_NEW                   = 1;
    const STA_APPROVED_BY_MANAGE    = 2;
    const STA_APPROVED_BY_DIRECTOR  = 3;
    const STA_WAIT_SETTLE           = 4;
    const STA_COMPLETE              = 5;
    const STA_REJECT                = 6;
    const STA_REQUEST_EDIT          = 7;
    const STA_APPROVED_BY_CHIEF_ACCOUNTANT = 8;
    
    const SETTLE_UNLOCK     = 1;
    const SPJ_AUTO_GEN      = 1;// May2818 admin auto gen row quyết toán
    
//    const CASHIER_7DAY      = 7;
//    const CASHIER_15DAY     = 15;
//    const CASHIER_20DAY     = 20;
//    const CASHIER_30DAY     = 30;
    const CASHIER_FREE_DAY  = 15; // Sau số ngày này thì sẽ bắt đầu bị cảnh báo
    
//    const CASHIER_5DAY      = 5; // Cảnh báo
//    const CASHIER_10DAY     = 10; // Phạt 2% ( = 2%*amount)
//    const CASHIER_15DAY     = 15; // Phạt 5% ( = 5%*amount)
//    const CASHIER_25DAY     = 25; // Phạt 10% ( = 10%*amount + Số tiền tạm ứng)
    
    //DuongNV Jul2519 Sửa lại cách define 
    const CASHIER_DAY_ALERT         = -5; // Cảnh báo trước ngày hết hạn 5 ngày
    const CASHIER_DAY_2_PERCENT     = 1; // Sau ngày hết hạn 1 ngày thì phạt 2%
    const CASHIER_DAY_5_PERCENT     = 8; // Sau ngày hết hạn 8 ngày thì phạt 5%
    const CASHIER_DAY_10_PERCENT    = 15; // Sau ngày hết hạn 15 ngày thì phạt 10%
    const CASHIER_DAY_CUT_SALARY    = 22; // Sau ngày hết hạn 22 ngày thì trừ lương
    
    const CASHIER_2PERCENT  = 0.02;
    const CASHIER_5PERCENT  = 0.05;
    const CASHIER_10PERCENT   = 0.1;
    const CASHIER_100PERCENT  = 1;
    
    
    const DEFAULT_FINE      = 1000000;
    
    public static $LIST_STATUS_TEXT = array(
        GasSettle::STA_NEW                  => 'Chờ Duyệt',
        GasSettle::STA_APPROVED_BY_MANAGE   => 'Quản lý đã duyệt',
        GasSettle::STA_APPROVED_BY_CHIEF_ACCOUNTANT => 'Kế toán trưởng đã duyệt',
        GasSettle::STA_APPROVED_BY_DIRECTOR => 'Giám Đốc duyệt',
        GasSettle::STA_WAIT_SETTLE          => 'Chờ quyết toán',
        GasSettle::STA_COMPLETE             => 'Hoàn tất',
        GasSettle::STA_REJECT               => 'Hủy bỏ',
        GasSettle::STA_REQUEST_EDIT         => 'Yêu cầu sửa lại'
    );
    public static $STATUS_UPDATABLE = array(
        GasSettle::STA_NEW,
        GasSettle::STA_REQUEST_EDIT,
//        GasSettle::STA_WAIT_SETTLE // Anh Dung Remove Jul 10, 2016 xử lý TH Đề nghị tạm ứng, khi giám đốc duyệt thì mình sẽ cho user làm quyết toán luôn
    );
    public $MAX_ID;
    public $autocomplete_name, $autocomplete_name1;

    // 1:Đề nghị thanh toán; 2: Chuyển khoản; 3: Tạm ứng; 4: Quyết toán, 5: Quyết toán - Không có tạm ứng
    const TYPE_REQUEST_MONEY    = 1;
    const TYPE_BANK_TRANSFER    = 2;
    const TYPE_PRE_SETTLE       = 3;// Đề nghị tạm ứng
    const TYPE_AFTER_SETTLE     = 4;// Quyết toán
    const TYPE_SETTLE_KHONG_TAM_UNG = 5; // 5: Quyết toán - Không có tạm ứng - Anh Dung Jul 28, 2016
    //        @Code: NAM003 16/02/2018
    const TYPE_REQUEST_MONEY_MULTI = 6;
    const TYPE_BANK_TRANSFER_MULTI = 7;
    
    public static $LIST_TYPE_TEXT = array(
        GasSettle::TYPE_REQUEST_MONEY   => 'Đề nghị thanh toán',
        GasSettle::TYPE_BANK_TRANSFER   => 'Đề nghị chuyển khoản',
        GasSettle::TYPE_PRE_SETTLE      => 'Đề nghị tạm ứng',
        GasSettle::TYPE_AFTER_SETTLE    => 'Quyết toán',
        GasSettle::TYPE_SETTLE_KHONG_TAM_UNG => 'Quyết toán - Không có tạm ứng',
//        @Code: NAM003 16/02/2018
        GasSettle::TYPE_REQUEST_MONEY_MULTI  => 'Đề nghị thanh toán hợp đồng thuê nhà',
        GasSettle::TYPE_BANK_TRANSFER_MULTI  => 'Đề nghị chuyển khoản hợp đồng thuê nhà',
    );
    public static $ARR_TYPE_BANK = array(GasSettle::TYPE_BANK_TRANSFER, GasSettle::TYPE_SETTLE_KHONG_TAM_UNG);
    // June 06, 2016 List status available for manager
    const UPDATE_TYPE_APPROVE = 1;
    const UPDATE_TYPE_REJECT = 2;
    const UPDATE_TYPE_NEED_UPDATE = 3;
    // List status available for manager
    public static $STATUS_UPDATE_BY_MANAGER = array(
        GasSettle::UPDATE_TYPE_APPROVE      => 'Quản lý duyệt',
        GasSettle::UPDATE_TYPE_REJECT       => 'Không duyệt',// Now 11, 2016 hủy bỏ với Quyết toán của Tạm ứng thì không dc
        GasSettle::UPDATE_TYPE_NEED_UPDATE  => 'Yêu cầu cập nhật'
    );
    // Danh sách trạng thái kế toán trưởng duyệt
    public static $STATUS_UPDATE_BY_CHIEF_ACCOUNTANT = array(
        GasSettle::UPDATE_TYPE_APPROVE      => 'Kế toán trưởng duyệt',
        GasSettle::UPDATE_TYPE_REJECT       => 'Không duyệt',
        GasSettle::UPDATE_TYPE_NEED_UPDATE  => 'Yêu cầu cập nhật'
    );
    // List status available for director
    public static $STATUS_UPDATE_BY_DIRECTOR = array(
        GasSettle::UPDATE_TYPE_APPROVE      => 'Giám đốc duyệt',
        GasSettle::UPDATE_TYPE_REJECT       => 'Không duyệt',
        GasSettle::UPDATE_TYPE_NEED_UPDATE  => 'Yêu cầu cập nhật'
    );
    // List status available for accounting
    public static $STATUS_UPDATE_BY_ACCOUNTING = array(
        GasSettle::UPDATE_TYPE_APPROVE      => 'Kế toán xác nhận',
        GasSettle::UPDATE_TYPE_REJECT       => 'Không xác nhận',
    );

    public static $LIST_STATUS_CAN_APPROVE_BY_MANAGER = array(
        GasSettle::STA_NEW,
        GasSettle::STA_APPROVED_BY_MANAGE,
        GasSettle::STA_REQUEST_EDIT,
        GasSettle::STA_REJECT,
    );
    public static $LIST_STATUS_CAN_APPROVE_BY_CHIEF_ACCOUNTANT = array(
        GasSettle::STA_APPROVED_BY_MANAGE,
        GasSettle::STA_APPROVED_BY_CHIEF_ACCOUNTANT,
        GasSettle::STA_REQUEST_EDIT,
    );
    public static $LIST_STATUS_CAN_APPROVE_BY_DIRECTOR = array(
        GasSettle::STA_APPROVED_BY_DIRECTOR,
        GasSettle::STA_APPROVED_BY_CHIEF_ACCOUNTANT,
        GasSettle::STA_REQUEST_EDIT,
    );
    public static $LIST_STATUS_CAN_APPROVE_BY_ACCOUNTING = array(
        GasSettle::STA_APPROVED_BY_DIRECTOR
    );

    public $monthPayDebit = 0, $step_status; // Chỉ sử dụng trong trường hợp update status. Tránh không cho người dùng sửa trực tiếp
    public $comment_leader; // Ghi chú của leader
    public $comment_chief_accountant; // Trung Jul 11 2016: Ghi chú của tế toán trưởng
    public $comment_director; // Ghi chú của giám đốc
    public $comment_accounting; // Ghi chú của kế toán
    private $COMMENT_JSON_FIELD = array('comment_leader', 'comment_chief_accountant', 'comment_director', 'comment_accounting');
    public $mUidLogin;
    public $file_name;
    public $date_from;
    public $date_to;
    public $mParent;
    public $aDetail, $modelOld=null;
    const CASHIER_CONFIRM_YES       = 1;
    const CASHIER_CONFIRM_NO        = 0;
    const SETTLE_DONE_YES           = 1;
    const SETTLE_EXPIRE             = 2;
    const SETTLE_RENEW              = 3;
    
    const TYPE_PRE_SETTLE_CUT_SALARY                = 1;// Mar819 Tạm ứng trừ lương 
    const TYPE_PRE_SETTLE_NORMAL                    = 2;// Ko trừ lương, phải làm quyết toán
    
    const MAX_SETTLE_ITEMS                          = 100;
    public $dayAllowViewCashierConfirm              = 1;
    public $dayAllowAccountingViewCashierConfirm    = 30;//  Feb 06, 2017 chỉnh lên 1 tháng, cho phép 1 số user có thể view 10 ngày sau khi đã duyệt
    public $dayViewCashierConfirm30 = 160;// Feb 06, 2017 chỉnh lên 4 tháng. Aug 24, 2016 cho phép 1 số user có thể view 30 ngày sau khi đã duyệt
//    public $dayViewCashierConfirm30 = 65;// Jan 24, 2017 chỉnh view lên 65 ngày với 1 số user sửa
    public static $ARR_STATUS_CASHIER_VIEW  = array(GasSettle::STA_APPROVED_BY_DIRECTOR, GasSettle::STA_COMPLETE);// Jul 27, 2016 những status mà cashier có thể view để chi tiền
    public static $ARR_STATUS_KTTT_VIEW     = array(GasSettle::STA_APPROVED_BY_CHIEF_ACCOUNTANT, GasSettle::STA_APPROVED_BY_DIRECTOR, GasSettle::STA_COMPLETE);// Dec 09, 2016 những status mà KTTT có thể view để theo dõi
    public static $aRoleSubAgent    = array(ROLE_SUB_USER_AGENT, ROLE_ACCOUNTING_ZONE);
//    public static $aUidView30       = array(GasLeave::DUNG_NTT, GasLeave::PHUONG_PTK, GasConst::UID_NGAN_LT, GasConst::UID_HUE_LT);
    public static $aUidView30       = array(GasLeave::DUNG_NTT);
    public $comment_thu_quy, $comment_ke_toan;

    /** @Author: ANH DUNG Aug 15, 2016 */
    public function getUidViewAll() {
        return array(GasLeave::PHUONG_PTK, GasLeave::UID_DIRECTOR, GasSupportCustomer::UID_THUY_MDX, GasConst::UID_NHAN_NTT, GasConst::UID_HUE_LT, GasLeave::UID_HEAD_OF_LEGAL, GasLeave::UID_CHIEF_ACCOUNTANT);
//        GasLeave::UID_QUYNH_MTN,
    }
    /** @Author: ANH DUNG Sep 19, 2017 */
    public function getUidOfKimPhuong() {
        return array(
            1163847, // Đặng Hữu Phúc
            850531, // Trần Thị Vân
            793887, // Đào Thị Thu Quyên
            726576, // Đặng Thị Thiện
            136815, // Phạm Thị Quý
            138, // Trần Thị Lệ
        );
    }
    
    /** @Author: DungNT Mar 18, 2019
     *  @Todo: get array loại tạm ứng
     **/
    public function getArrayTypePreSettle() {
        return [
            GasSettle::TYPE_PRE_SETTLE_CUT_SALARY   => 'Tạm ứng có trừ lương',
            GasSettle::TYPE_PRE_SETTLE_NORMAL       => 'Tạm ứng quyết toán sau',
        ];
    }
    
    /** @Author: ANH DUNG Apr 09, 2017
     *  @Todo: Những User đc xem toàn Hệ Thống
     */
    public function getUidViewAllSystem() {
        return [
            GasSupportCustomer::UID_THUY_MDX,
            GasLeave::UID_QUYNH_MTN,
        ];
    }
    
    /** @Author: ANH DUNG Jan 30, 2019
     *  @Todo: User view only toàn Hệ Thống
     */
    public function getUidViewOnlyAllSystem() {
        return [
            GasConst::UID_CHAU_LNM,
            GasConst::UID_HIEP_TV,
            GasLeave::UID_QUYNH_MTN,
        ];
    }
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_settle}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        $rule = array(
            array('type, company_id, uid_leader', 'required', 'on' => 'create, update'),
            array('bank_user, bank_number, bank_name', 'length', 'max' => 255),
            array('type, bank_user, bank_number, bank_name', 'ruleBankAccount', 'on' => 'create, update'),
            // Trung Jul 11 , 2016 Rule for update status
            array('uid_chief_accountant, uid_director', 'ruleValidateUpdateStatus', 'on' => 'update_from_server'), // Handle bằng code nên chỉ cần gọi thôi
            array('step_status, comment_leader, comment_chief_accountant, comment_director, comment_accounting', 'safe', 'on' => 'update_from_manager'),
            array('step_status', 'required', 'on' => 'update_from_manager, update_from_server'),
            array('date_from, date_to, comment_leader, comment_chief_accountant, comment_director, comment_accounting', 'safe'),
            array('type_pre_settle, monthPayDebit, distributor_id, renew_expiry_date, uid_login_role_id, uid_cashier_confirm, code_no, company_id, id, uid_login, uid_leader, uid_chief_accountant, uid_director, uid_accounting, type, status, reason, amount, bank_user, bank_number, bank_name, created_date, approved_manager_date, approved_director_date, approved_accounting_date, note, parent_id', 'safe'),
            array('number_order,contract_id,bank_province_id,bank_branch,comment_thu_quy, comment_ke_toan', 'safe'),
        );
        return $rule;
    }

    /**
     * @Author: ANH DUNG Jul 25, 2016
     * @Todo: fix bug From Trung, chỗ này phải map biến status vào thì validate của em mới đúng được
     * của Trung khi duyệt lần đấu nó ko validate đúng, nhưng khi update thì đúng, vì nó đã có biến status rồi
     */
    public function mapStatusBeforeValidate() {
        $cUid = MyFormat::getCurrentUid();
        if ($this->step_status == GasSettle::UPDATE_TYPE_APPROVE) {
            $aStatusForLeader = [GasSettle::STA_NEW, GasSettle::STA_REQUEST_EDIT];
            if($this->uid_leader == $cUid && in_array($this->modelOld->status, $aStatusForLeader)){
                $this->status = GasSettle::STA_APPROVED_BY_MANAGE;
            }elseif($this->uid_chief_accountant == $cUid){
                $this->status = GasSettle::STA_APPROVED_BY_CHIEF_ACCOUNTANT;
            }
        }
    }
    
    public function ruleValidateUpdateStatus($attribute)
    {
        $this->mapStatusBeforeValidate();
        if ($this->step_status == GasSettle::UPDATE_TYPE_APPROVE) {
            if ($this->status == GasSettle::STA_APPROVED_BY_MANAGE) {
                if(empty($this->uid_chief_accountant)){
                    $this->addError($attribute, 'Vui lòng nhập kế toán trưởng');
                }
            } else if ($this->status == GasSettle::STA_APPROVED_BY_CHIEF_ACCOUNTANT) {
                if(empty($this->uid_director)){
                    $this->addError('uid_director', 'Vui lòng nhập giám đốc');
                }
                if(empty($this->uid_accounting) && !in_array($this->uid_login_role_id, GasSettle::$aRoleSubAgent)){
                    $this->addError('uid_accounting', 'Vui lòng nhập kế toán thanh toán');
                }
                if(empty($this->uid_cashier_confirm) && !in_array($this->uid_login_role_id, GasSettle::$aRoleSubAgent)){
                    $this->addError('uid_cashier_confirm', 'Vui lòng nhập thủ quỹ');
                }
            }
        }
    }
    
    /**
     * @Author: ANH DUNG Jul 19, 2016
     */
    public function checkCreateMoreSettle() {
        $aScenario = array('create', 'update');
        if(in_array($this->scenario, $aScenario)  && $this->type == GasSettle::TYPE_PRE_SETTLE){
            if($this->hasExpirySettle()){
                $this->addError('type', 'Bạn có tạm ứng chưa quyết toán, vui lòng quyết toán xong để làm tạm ứng mới');
            }
        }
    }

    public function ruleBankAccount($attribute)
    {
        if ($this->type == GasSettle::TYPE_BANK_TRANSFER && empty($this->$attribute)) {
            $this->addError($attribute, 'Vui lòng nhập thông tin tài khoản');
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),// người tạo
            'rUidLeader' => array(self::BELONGS_TO, 'Users', 'uid_leader'),// trưởng phòng xác nhận
            'rUidChiefAccountant' => array(self::BELONGS_TO, 'Users', 'uid_chief_accountant'),// kế toán trưởng xác nhận
            'rUidDirector' => array(self::BELONGS_TO, 'Users', 'uid_director'),// giám đốc xác nhận
            'rUidAccounting' => array(self::BELONGS_TO, 'Users', 'uid_accounting'),// kế toán xác nhận
            'rUidCashierConfirm' => array(self::BELONGS_TO, 'Users', 'uid_cashier_confirm'),// kế toán xác nhận
            'rParent' => array(self::BELONGS_TO, 'GasSettle', 'parent_id'),// kế toán xác nhận
            'rCompany' => array(self::BELONGS_TO, 'Users', 'company_id'),// Công ty trực thuộc
            'rFile' => array(self::HAS_MANY, 'GasFile', 'belong_id',
                'on' => 'rFile.type=' . GasFile::TYPE_5_SETTLE,
                'order' => 'rFile.id ASC',
            ),
            'rFileV1' => array(self::HAS_MANY, 'GasFile', 'belong_id',
                'on' => 'rFileV1.type=' . GasFile::TYPE_6_SETTLE_CREATE,
                'order' => 'rFileV1.id ASC',
            ),
            'rDetail' => array(self::HAS_MANY, 'GasSettleRecord', 'settle_id', 'order' => 'rDetail.id ASC',),// Công ty trực thuộc
            'rDistributor' => array(self::BELONGS_TO, 'Users', 'distributor_id'),
//            @Code: NAM003 16/02/2018
            'rHomeContractDetail' => array(self::HAS_MANY, 'HomeContractDetail', 'settle_id'),
            'rGasDebts' => array(
                self::HAS_MANY, 'GasDebts', 'relate_id',
                'on'    => 'type = ' . GasDebts::TYPE_GAS_SETTLE . ' AND status !=' . DomainConst::DEFAULT_STATUS_INACTIVE,
            ),
            'rCommentThuQuy' => array(self::HAS_ONE, 'GasComment', 'belong_id',
                'on'=>'rCommentThuQuy.type='. GasComment::TYPE_6_SETTLE_THUQUY_NOTE,
                'order' => 'rCommentThuQuy.id DESC',
            ),
            'rCommentKeToan' => array(self::HAS_ONE, 'GasComment', 'belong_id',
                'on'=>'rCommentKeToan.type='. GasComment::TYPE_7_SETTLE_KEOAN_NOTE,
                'order' => 'rCommentKeToan.id DESC',
            ),
            'rProvince' => array(self::BELONGS_TO, 'GasProvince', 'bank_province_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'uid_login' => 'Người tạo',
            'uid_leader' => 'Quản lý xác nhận',
            'uid_chief_accountant' => 'Kế toán trưởng xác nhận',
            'uid_director' => 'Giám đốc xác nhận',
            'uid_accounting' => 'Kế toán thanh toán',
            'type' => 'Loại',
            'status' => 'Trạng thái',
            'reason' => 'Lý do',
            'amount' => 'Số tiền',
            'bank_user' => 'Người hưởng thụ',
            'bank_number' => 'Số tài khoản',
            'bank_name' => 'Tên ngân hàng',
            'created_date' => 'Ngày tạo',
            'approved_manager_date' => 'Ngày quản lý duyệt',
            'approved_director_date' => 'Ngày giám đốc duyệt',
            'approved_chief_accountant_date' => 'Ngày kế toán trưởng duyệt',
            'approved_accounting_date' => 'Ngày kế toán xác nhận',
            'comment_leader' => 'Ghi chú của quản lý',
            'comment_chief_accountant' => 'Ghi chú của kế toán trưởng',
            'comment_director' => 'Ghi chú của giám đốc',
            'comment_accounting' => 'Ghi chú của kế toán',
            'note' => 'Ghi chú',
            'parent_id' => 'Parent',
            'step_status' => 'Cập nhật trạng thái',
            'company_id' => 'Công ty',
            'code_no' => 'Mã Số',
            'uid_cashier_confirm' => 'Thủ quỹ',
            'cashier_confirm_date' => 'Ngày chi tiền',
            'date_from' => 'Chi Tiền Từ Ngày',
            'date_to' => 'Đến Ngày',
            'renew_expiry_date' => 'Gia hạn tạm ứng đến ngày',
            'distributor_id' => 'Nhà phân phối',
            'monthPayDebit' => 'Số tháng hoàn tạm ứng',
            'type_pre_settle' => 'Loại tạm ứng',
            'bank_branch' => 'Chi nhánh',
            'bank_province_id' => 'Tỉnh',
            'number_order' => 'Số hóa đơn',
            'contract_id' => 'Số hợp đồng',
            
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $cUid           = MyFormat::getCurrentUid();
        $cRole          = MyFormat::getCurrentRoleId();
        $session        = Yii::app()->session;
        $aUidAccounting = array(GasLeave::UID_QUYNH_MTN);
        $aStatusApprove = implode(',', GasSettle::$ARR_STATUS_CASHIER_VIEW);
        $aStatusKTTTView = implode(',', GasSettle::$ARR_STATUS_KTTT_VIEW);// những status mà Kế toán thanh toán sẽ dc view
        $criteria       = new CDbCriteria;
        $aStatusNotShowForApprove = array(GasSettle::STA_REJECT);
        $this->handleCondition($criteria);
        $queryViewTypeHomeContract = $this->addQueryHomeContract();// Feb2519 add chị Nhàn xem TT thuê nhà
        // June 04, 2016 Prevent get not owner items
        $query = '';
        $this->limitAccountingViewAgent($query, $cUid, $session, $aStatusApprove);
        if ($this->isRoleManager()) {
            // Chỉ xem những item được gán cho mình
            if($cUid == GasConst::UID_HIEP_TV){
                $query .= " OR t.uid_login = $cUid OR (t.uid_leader =  $cUid ) OR $queryViewTypeHomeContract";
            }elseif(in_array($cUid, $this->getUidViewAllSystem())){
                if(!isset($_GET['cashier_confirm'])){// Apr0917 chị Thủy View All
                    $query .= " t.uid_login = $cUid OR t.uid_leader =  $cUid  OR t.uid_director = $cUid";// May0219 add t.uid_director cho chị Thủy duyệt cấp 3
                }
            }else{
                $query .= " OR t.uid_leader = $cUid";
            }
//            $criteria->addNotInCondition('t.status', $aStatusNotShowForApprove);// Aug 04, 2016 ẩn những cái reject hay request edit đi
            $sParamsIn = implode(',', $aStatusNotShowForApprove);
            $criteria->addCondition("t.status NOT IN ($sParamsIn)");
        }
        if ($this->isRoleDirector()) {
            // Chỉ xem những item được gán cho mình
            if($cUid == GasSupportCustomer::UID_THUY_MDX){
                $query      .= " OR (t.uid_director = " . $cUid . ")";
            }else{
                $sParamsIn  = "$cUid,".GasSupportCustomer::UID_THUY_MDX;
                $query      .= " OR (t.uid_director IN (" . $sParamsIn . "))";
            }
//            $criteria->addNotInCondition('t.status', $aStatusNotShowForApprove);
            $sParamsIn = implode(',', $aStatusNotShowForApprove);
            $criteria->addCondition("t.status NOT IN ($sParamsIn)");
        }
        if ($this->isRoleChiefAccountant()) {
            // Trung Jul 11 2016: Kiểm tra quyền kế toán trưởng
            // Chỉ cho xem những item được gán cho mình
            $query .= " OR (t.uid_chief_accountant = " . $cUid . ")";
            if($cUid == GasLeave::UID_CHIEF_ACCOUNTANT){
                $query .= " OR (t.uid_chief_accountant = " . GasLeave::UID_QUYNH_MTN . ")";
            }
//            $criteria->addNotInCondition('t.status', $aStatusNotShowForApprove);
            $sParamsIn = implode(',', $aStatusNotShowForApprove);
            $criteria->addCondition("t.status NOT IN ($sParamsIn)");
            // Hoặc quản lý duyệt
//            $query .= " OR t.status IN (" // Anh Dung Close on Jul 14, 2016 vì không để kế toán trưởng view hết của các kế toán khác dc
//                . GasSettle::STA_APPROVED_BY_MANAGE . ")";
        }

//        if ($this->isRoleAccounting() && !in_array($cUid, $this->getUidViewAll())) {
        if ($this->isRoleAccounting()) {
            // June 06, 2016 uid_accounting kế toán thanh toán
            // Chỉ theo dõi item được gán cho mình và được giám đốc duyệt
            if(in_array($cUid, $this->getUidViewAll()) ){// Aug0818 mở cho Huệ xem toàn bộ hệ thống
                $query .= " OR (t.status IN ($aStatusKTTTView))";
            }else{
                $query .= " OR ( t.uid_accounting = $cUid AND t.status IN ($aStatusKTTTView) )";
            }
//            // Hoặc sau khi giám đốc xác nhận
//            $query .= " OR t.status IN ("
//                . GasSettle::STA_APPROVED_BY_DIRECTOR . ")";
        }
        if ($this->isRoleCashierConfirm()) {
            // June 06, 2016 Thủ Quỹ
            // Chỉ theo dõi item được gán cho mình và được giám đốc duyệt
            $cCashier = "t.uid_cashier_confirm = $cUid";
            if($cUid == GasConst::UID_NGUYEN_DTM){
//                $uidNhan = GasConst::UID_NHAN_NTT; // Feb2519 Close
//                $cCashier = "(t.uid_cashier_confirm = $cUid OR t.uid_cashier_confirm = $uidNhan)";
            }
            if($cUid == GasConst::UID_NHAN_NTT){
                $query .= " OR ($cCashier AND t.status IN ($aStatusApprove) ) OR $queryViewTypeHomeContract";
            }else{
                $query .= " OR ($cCashier AND t.status IN ($aStatusApprove) ) ";
            }
//            // Hoặc sau khi giám đốc xác nhận
//            $query .= " OR t.status IN ("
//                . GasSettle::STA_APPROVED_BY_DIRECTOR . ")";
        }
        if ($cRole != ROLE_ADMIN && !empty($query)) {// Apr0917 chị Thủy View All
            // Chỉ thêm query cho user thường. Admin sẽ được xem tất cả
            $criteria->addCondition($query);
            $criteria->addCondition('t.status_hide=0');
        }
        if(!empty($this->contract_id)){
            $criteria->compare('t.contract_id', $this->contract_id);
        }
        
        $sort = new CSort();
        $sort->attributes       = [];
        $sort->defaultOrder     = 't.id desc';
        
        $this->conditionCashierConfirm($criteria, $aUidAccounting, $cUid, $cRole, $sort);
        if($this->type == GasSettle::TYPE_BANK_TRANSFER_MULTI){
            $_SESSION['dataGasSettleExcel'] = new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'pagination'=>false,
            ));
        }else{
            $_SESSION['dataGasSettleExcelAll'] = new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'pagination'=>false,
            ));
        }
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => 30,
            ),
        ));
    }
    
    /** @Author: DungNT Apr 25, 2019
     *  @Todo: addQueryHomeContract
     **/
    public function addQueryHomeContract() {
        $aTypeHomeContract = implode(',', $this->getListTypeSettleHomeContract());
        return  "t.type IN ($aTypeHomeContract)";// Feb2519 add chị Nhàn xem TT thuê nhà
    }
    
    /**
     * @Author: ANH DUNG Sep 16, 2016
     * @Todo: Em bổ sung cho chị những người sau thấy toàn bộ quyết toán trên web của đại lý theo từng khu vực quản lý nhé.
    1. TP. HCM                – Phan Thị Kim Phương ( thấy rồi).
    2. Bình Dương & Đồng Nai  - Nguyễn Thị Thùy Dung ( thấy rồi)
    3. Gia Lai, Kon Tum       - Nguyễn Thị Kim Tuyết
    4. Bình Định              - Lê Thị Tuyết
    5. An Giang                - Nguyễn Thị Mỹ Diệu
    6. Vĩnh long, Cần Thơ, Trà Vinh        - Huỳnh Thanh Thùy
     */
    public function limitAccountingViewAgent(&$query, $cUid, $session, $aStatusApprove) {
        $UID_THUY_MDX   = GasSupportCustomer::UID_THUY_MDX;
        $uidThuyHt      = GasConst::UID_THUY_HT;
//        if($cUid == GasLeave::PHUONG_PTK){// Chị phương view đại lý hcm
//            $aSubUserAgent  = implode(',', $session['SUB_AGENT_HCM']);
//            $aKTKV          = implode(',', $this->getUidOfKimPhuong());
//            $query .= " t.uid_login = $cUid OR t.uid_login IN ($aKTKV) OR ( ( t.uid_login IN ($aSubUserAgent) OR t.uid_login=$UID_THUY_MDX ) AND t.status IN ($aStatusApprove) )";
//        }else
        if($cUid == GasLeave::DUNG_NTT){// Aug 26, 2016 Dung view đồng nai, bình dương
            $aSubUserAgent = implode(',', $session['SUB_AGENT_MIENDONG']);
            $query .= " t.uid_login = $cUid OR ( ( t.uid_login IN ($aSubUserAgent) OR t.uid_login=$UID_THUY_MDX ) AND t.status IN ($aStatusApprove) )";
        }elseif($cUid == GasTickets::DIEU_PHOI_HIEU_TM){// Jan 18, 2017 
            $query .= " t.uid_login = $cUid OR ( t.uid_login=$UID_THUY_MDX )";
        }elseif(in_array($cUid, GasConst::$ARR_VIEW_SETTLE_MIEN_DONG)){// Aug 26, 2016 Dung view đồng nai, bình dương
            $aSubUserAgent = implode(',', $session['SUB_AGENT_MIENDONG']);
            $query .= " t.uid_login = $cUid OR ( t.uid_login IN ($aSubUserAgent) AND t.status IN ($aStatusApprove) )";
//        }elseif($cUid == GasLeave::UID_VIEW_ALL_TAY_NGUYEN_GIA_LAI){
        }elseif($cUid == GasConst::UID_LIEU_CTM){
            $aSubUserAgent = implode(',', $session['SUB_AGENT_GIALAI_KONTUM']);
            $query .= " t.uid_login = $cUid OR ( ( t.uid_login IN ($aSubUserAgent)  ) AND t.status IN ($aStatusApprove) )";
        }elseif($cUid == GasConst::UID_TUYET_LT){
            $aSubUserAgent = implode(',', $session['SUB_AGENT_BINH_DINH']);
            $query .= " t.uid_login = $cUid OR ( ( t.uid_login IN ($aSubUserAgent)  ) AND t.status IN ($aStatusApprove) )";
        }elseif($cUid == GasLeave::UID_DIEU_NTM){
            $aSubUserAgent = implode(',', $session['SUB_AGENT_AN_GIANG']);
            if(!empty($aSubUserAgent)){
                $query .= " t.uid_login = $cUid OR ( ( t.uid_login IN ($aSubUserAgent)  ) AND t.status IN ($aStatusApprove) )";
            }else{
                $query .= " t.uid_login = $cUid";
            }
        }elseif($cUid == GasConst::UID_THUY_HT || $cUid == GasConst::UID_VINH_NH){
            $aSubUserAgent = implode(',', $session['SUB_AGENT_VL_CT_TV']);
            $query .= " t.uid_login = $cUid OR ( ( t.uid_login IN ($aSubUserAgent)  ) AND t.status IN ($aStatusApprove) )";
            if($cUid == GasConst::UID_VINH_NH){
                $query .= " OR t.uid_cashier_confirm = $uidThuyHt";
            }
        }elseif($cUid == GasConst::UID_NGAN_LT){// Cho Ngân theo dõi quyết toán Biển Đông
            $aSubUserAgent = implode(',', array(GasConst::COMPANY_BIEN_DONG));
            // anh Long IT: 862877, DatLx: 868857
//            $query .= " t.uid_login = $cUid OR  t.uid_login=862877 OR ( t.company_id IN ($aSubUserAgent) )";
            $query .= " t.uid_login = $cUid OR  t.uid_login=868857 OR ( t.company_id IN ($aSubUserAgent) )";
        }elseif(in_array($cUid, GasConst::$ARR_VIEW_SETTLE_MDXT)){// Aug 26, 2016 Kế toán view phiếu của chị Thủy
            $query .= " t.uid_login = $cUid OR (  t.uid_login=$UID_THUY_MDX AND t.status IN ($aStatusApprove) )";
        }elseif(in_array($cUid, $this->getUidViewAllSystem())){
//            }elseif(in_array($cUid, $this->getUidViewAll())){
                // Apr0917 chị Thủy View All
        }else{
            $query .= "t.uid_login = $cUid " ;
        }
    }
    
    /**
     * @Author: ANH DUNG Aug 26, 2016
     * @Todo: xử lý search khi cashier confirm
     */
    public function conditionCashierConfirm(&$criteria, $aUidAccounting, $cUid, $cRole, &$sort) {
        if(isset($_GET['cashier_confirm'])){
            $criteria->compare('t.cashier_confirm', GasSettle::CASHIER_CONFIRM_YES);
            if($_GET['cashier_confirm'] == GasSettle::SETTLE_EXPIRE){// Xem tạm ứng hết hạn, quá hạn 15 ngày nhưng vẫn hiện lên để user làm quyết toán 
                $settleNormal = GasSettle::TYPE_PRE_SETTLE_NORMAL;
//                $criteria->addCondition("t.is_done_settle=0 AND t.status_unlock=0 AND t.type=".GasSettle::TYPE_PRE_SETTLE);
                $criteria->addCondition("t.type_pre_settle = $settleNormal AND t.is_done_settle=0 AND t.status_unlock=0 AND t.type=".GasSettle::TYPE_PRE_SETTLE);
                $criteria->addCondition("cashier_confirm_date IS NOT NULL AND DATE_ADD(cashier_confirm_date, INTERVAL {$this->getDayExpirySettle()} DAY) < NOW()");
            }elseif($_GET['cashier_confirm'] == GasSettle::SETTLE_RENEW){// Xem tạm ứng đã gia hạn, quá hạn 15 ngày dc gia hạn thêm
                $criteria->addCondition("t.is_done_settle=0 AND t.status_unlock=1 AND t.type=".GasSettle::TYPE_PRE_SETTLE);
                $criteria->addCondition("cashier_confirm_date IS NOT NULL AND DATE_ADD(cashier_confirm_date, INTERVAL {$this->getDayExpirySettle()} DAY) < NOW()");
            }
            else{
                $aUidViewAll = $this->getUidViewAll();
                if (in_array($cUid, $aUidAccounting)) {// ẩn hết chi sau 10 ngày
                    $criteria->addCondition("DATE_ADD(cashier_confirm_date, INTERVAL {$this->dayAllowAccountingViewCashierConfirm} DAY) >= CURDATE() ");
                }elseif (in_array($cUid, GasSettle::$aUidView30)|| in_array($cUid, GasConst::$ARR_VIEW_SETTLE_MDXT)) {// ẩn hết chi sau 30 ngày
                    $criteria->addCondition("DATE_ADD(cashier_confirm_date, INTERVAL {$this->dayViewCashierConfirm30} DAY) >= CURDATE() ");
                }elseif ($cRole != ROLE_ADMIN && !in_array($cUid, $aUidViewAll)) {// ẩn hết chi sau 1 ngày
    //                $TypeTamUng = GasSettle::TYPE_PRE_SETTLE;
    //                $is_done_settle = GasSettle::SETTLE_DONE_YES;
    //                cai nay Đúng, chưa test kỹ  $criteria->addCondition("( DATE_ADD(cashier_confirm_date, INTERVAL {$this->dayAllowViewCashierConfirm} DAY) >= CURDATE())OR (t.type=$TypeTamUng AND t.is_done_settle=0 )");
                    $criteria->addCondition("DATE_ADD(cashier_confirm_date, INTERVAL {$this->getDayViewCashierConfirm()} DAY) >= CURDATE()");
                }
            }
            $sort->defaultOrder = 't.cashier_confirm_date desc';
        }else{
            $criteria->compare('t.cashier_confirm', GasSettle::CASHIER_CONFIRM_NO);
        }
    }
    
    /**
     * @Author: ANH DUNG Jul 27, 2016
     */
    public function handleCondition(&$criteria) {
        $criteria->compare('t.amount', trim(MyFormat::removeComma($this->amount)));
        $criteria->compare('t.uid_login', $this->uid_login);
        $criteria->compare('t.uid_leader', $this->uid_leader);
        $criteria->compare('t.uid_chief_accountant', $this->uid_chief_accountant);
        $criteria->compare('t.uid_director', $this->uid_director);
        $criteria->compare('t.uid_accounting', $this->uid_accounting);
        $criteria->compare('t.uid_cashier_confirm', $this->uid_cashier_confirm);
        $criteria->compare('t.type', $this->type);
        $criteria->compare('t.company_id', $this->company_id);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('t.distributor_id', $this->distributor_id);
        $criteria->compare('t.reason', trim($this->reason), true);
        $criteria->compare('t.bank_user', trim($this->bank_user), true);
        $criteria->compare('t.bank_number', trim($this->bank_number), true);
        $criteria->compare('t.bank_name', trim($this->bank_name), true);
        $criteria->compare('t.code_no', trim($this->code_no), true);
        
        $date_from = '';
        $date_to = '';
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        }
        if(!empty($date_from) && empty($date_to))
            $criteria->addCondition("DATE(t.cashier_confirm_date) >= '$date_from'");
        if(empty($date_from) && !empty($date_to))
            $criteria->addCondition("DATE(t.cashier_confirm_date) <= '$date_to'");
        if(!empty($date_from) && !empty($date_to))
            $criteria->addBetweenCondition("DATE(t.cashier_confirm_date)",$date_from,$date_to);
        
//        if(!empty($this->distributor_id)){// Apr 14, 17 không sử dụng nữa - sai lầm khi làm request chị Thủy - Phải hỏi rõ y/c của chị, ko đc đoán
//            $criteria->addCondition('rDetail.distributor_id=' . $this->distributor_id);
//            $criteria->with     = ['rDetail'];
//            $criteria->together = true;
//        }
    }

    /**
     * @Author: TRUNG Jul 13 2016
     * Kiểm tra item đã được giám đốc chấp nhận chưa. Giám đốc chấp nhận khi status xác nhận hoặc hoàn thành
     * @return bool
     */
    public function isApprovedByDirector()
    {
        if ($this->uid_director && (
                $this->status == self::STA_COMPLETE || $this->status == self::STA_APPROVED_BY_DIRECTOR)
        ) {
            return true;
        }
        return false;
    }

    /**
     * @Author: TRUNG Jul 13 2016
     * Kiểm tra item đã được kế toán trưởng chấp nhận chưa. Chấp nhận khi status xác nhận hoặc giám đốc duyệt hoặc hoàn thành
     * @return bool
     */
    public function isApprovedByChiefAccountant()
    {
        if ($this->uid_chief_accountant && (
                $this->status == self::STA_COMPLETE || $this->status == self::STA_APPROVED_BY_DIRECTOR
                || $this->status == self::STA_APPROVED_BY_CHIEF_ACCOUNTANT)
        ) {
            return true;
        }
        return false;
    }

    /**
     * @Author: TRUNG Jul 13 2016
     * Kiểm tra item đã được trưởng phòng chấp nhận chưa. Chấp nhận khi status xác nhận hoặc kế toán trưởng
     * hoặc giám đốc duyệt hoặc hoàn thành
     * @return bool
     */
    public function isApprovedByManager()
    {
        if ($this->uid_leader && (
                $this->status == self::STA_COMPLETE || $this->status == self::STA_APPROVED_BY_DIRECTOR
                || $this->status == self::STA_APPROVED_BY_CHIEF_ACCOUNTANT ||
                $this->status == self::STA_APPROVED_BY_MANAGE)
        ) {
            return true;
        }
        return false;
    }

    /**
     * @Author: TRUNG June 06 2016
     * Update status to db
     * @note: Jul 10, 2016 Anh Dung move this function to model
     */
    public function toUpdateStatus()
    {
        $cRole              = MyFormat::getCurrentRoleId();
        $cUid               = MyFormat::getCurrentUid();
        $attUpdate          = array('status');
        $modelBeforeUpdate  = self::model()->findByPk($this->id);
        // Mặc định cho admin toàn quyền
        // Đối kiểm tra quyền Giám đốc?
        if ((in_array($this->status, GasSettle::$LIST_STATUS_CAN_APPROVE_BY_DIRECTOR))
            && ($this->uid_director == $cUid || $cUid == GasLeave::UID_DIRECTOR)
        ) {
            // Update info
            $this->updateByDirectorRole($cUid, $this->step_status, $modelBeforeUpdate);
            $this->setDoneSettle();// set flag done for Tam Ung, nếu quyết toán dc duyệt lần 3 thì coi như tạm ứng đó done 
            // Đăng ký các field cần thay đổi
            $attUpdate[] = "note";
            $attUpdate[] = "uid_director";
            $attUpdate[] = "approved_director_date";
        }// Đối kiểm tra quyền kế toán trưởng? trạng thái trước đó có nằm trong danh sách kế toán trưởng được phép chỉnh sửa?
        elseif ((in_array($this->status, GasSettle::$LIST_STATUS_CAN_APPROVE_BY_CHIEF_ACCOUNTANT))
            && ($this->uid_chief_accountant == $cUid || $cRole == ROLE_ADMIN)
        ) {
            // Update info $cRole == ROLE_CHIEF_ACCOUNTANT ||
            $this->updateByChiefAccountantRole($cUid, $this->step_status, $modelBeforeUpdate);

            // Đăng ký các field cần thay đổi
            $attUpdate[] = "note";
            $attUpdate[] = "uid_chief_accountant";
            $attUpdate[] = "uid_director";
            $attUpdate[] = "approved_chief_accountant_date";
            $attUpdate[] = "uid_accounting";
            $attUpdate[] = "uid_cashier_confirm";
            if($modelBeforeUpdate->uid_director != $this->uid_director){
                // Close on Sep 08, 2016 không gửi mail cho anh Long nữa, 
                // GasScheduleEmail::BuildListNotifySettle($this, array($this->uid_director));// Jul 10, 2016
            }
        } // Đối kiểm tra quyền quản lý? trạng thái trước đó có nằm trong danh sách quản lý được phép chỉnh sửa?
        elseif ((in_array($this->status, GasSettle::$LIST_STATUS_CAN_APPROVE_BY_MANAGER))
            && ($this->uid_leader == $cUid || $cRole == ROLE_ADMIN)
        ) {
            // Update info
            $this->updateByManagerRole($cUid, $this->step_status, $modelBeforeUpdate);

            // Đăng ký các field cần thay đổi
            $attUpdate[] = "note";
            $attUpdate[] = "uid_leader";
            $attUpdate[] = "uid_chief_accountant";
            $attUpdate[] = "approved_manager_date";
            if($modelBeforeUpdate->uid_chief_accountant != $this->uid_chief_accountant){
                // Dec 01, 2016 cảm thấy cũng không cần gửi mail cho chị Ngân nữa, ngày nào chị chả vào đó check
//                GasScheduleEmail::BuildListNotifySettle($this, array($this->uid_chief_accountant));// Jul 10, 2016
            }
        }
        // luôn gửi mail cho người tạo mỗi khi có update
        GasScheduleEmail::BuildListNotifySettle($this, array($this->uid_login));// Oct 26, 2016 send mail cho người tạo nếu có cấp trên duyệt
        $this->setFieldCommentsToJson();
        return $this->update($attUpdate);
    }

    /**
     * @Author: TRUNG Jul 13 2016
     * Cập nhật trạng thái theo role của kế toán trưởng
     * @param $cUid
     * @param $stepStatus
     */
    public function updateByChiefAccountantRole($cUid, $stepStatus, $modelBeforeUpdate )
    {
        $this->comment_chief_accountant = MyFormat::removeBadCharacters($this->comment_chief_accountant, array('RemoveScript' => 1));
        $this->uid_chief_accountant = $cUid;
        if ($stepStatus == GasSettle::UPDATE_TYPE_APPROVE) {
            // Chuyển thành chấp nhận
            $this->status = GasSettle::STA_APPROVED_BY_CHIEF_ACCOUNTANT;
        } elseif ($stepStatus == GasSettle::UPDATE_TYPE_REJECT) {
            // Chuyển thành từ chối
            $this->status = GasSettle::STA_REJECT;
            $this->uid_director = null;
            // bỏ vì đã gửi ở cuối hàm tren - Dec 01, 2016 -> thông báo ngược lại cho user khi bị reject
//            GasScheduleEmail::BuildListNotifySettle($this, array($this->uid_login));// Jul 10, 2016
        } elseif ($stepStatus == GasSettle::UPDATE_TYPE_NEED_UPDATE) {
            // Chuyển thành yêu cầu nhân viên cập nhật thông tin
            $this->status = GasSettle::STA_REQUEST_EDIT;
        }
        // Cập nhật thời gian quản lý thay đổi
        $this->approved_chief_accountant_date = date("Y-m-d H:i:s");
    }

    /**
     * @Author: TRUNG Jul 13 2016
     * Cập nhật trạng thái theo role của quản lý
     * @param $cUid
     * @param $stepStatus
     */
    public function updateByManagerRole($cUid, $stepStatus, $modelBeforeUpdate)
    {
        $this->comment_leader = MyFormat::removeBadCharacters($this->comment_leader, array('RemoveScript' => 1));
        $this->uid_leader = $cUid;
        if ($stepStatus == GasSettle::UPDATE_TYPE_APPROVE) {
            // Chuyển thành chấp nhận
            $this->status = GasSettle::STA_APPROVED_BY_MANAGE;
        } elseif ($stepStatus == GasSettle::UPDATE_TYPE_REJECT) {
            // Chuyển thành từ chối
            $this->status = GasSettle::STA_REJECT;
            $this->uid_chief_accountant = null;
            // bỏ vì đã gửi ở cuối hàm tren - Dec 01, 2016 -> thông báo ngược lại cho user khi bị reject
//            GasScheduleEmail::BuildListNotifySettle($this, array($this->uid_login));// Jul 10, 2016
        } elseif ($stepStatus == GasSettle::UPDATE_TYPE_NEED_UPDATE) {
            // Chuyển thành yêu cầu nhân viên cập nhật thông tin
            $this->status = GasSettle::STA_REQUEST_EDIT;
        }
        // Cập nhật thời gian quản lý thay đổi
        $this->approved_manager_date = date("Y-m-d H:i:s");
    }

    /**
     * @Author: TRUNG Jul 13 2016
     * Cập nhật trạng thái theo role của giám đốc
     * @param $cUid
     * @param $stepStatus
     */
    public function updateByDirectorRole($cUid, $stepStatus, $modelBeforeUpdate)
    {
        $this->uid_director = $cUid;
        $this->comment_director = MyFormat::removeBadCharacters($this->comment_director, array('RemoveScript' => 1));
        // Kiểm tra giám đốc đang thay đổi sang dạng gì?
        if ($stepStatus == GasSettle::UPDATE_TYPE_APPROVE) {
            // Chuyển thành chấp nhận
            $this->status = GasSettle::STA_APPROVED_BY_DIRECTOR;
            // bỏ vì đã gửi ở cuối hàm tren - Dec 01, 2016 -> 
//            GasScheduleEmail::BuildListNotifySettle($this, array($this->uid_login));// Oct 26, 2016 send mail cho người tạo nếu GD duyệt
        } elseif ($stepStatus == GasSettle::UPDATE_TYPE_REJECT) {
            // Chuyển thành từ chối
            $this->status = GasSettle::STA_REJECT;
            // bỏ vì đã gửi ở cuối hàm tren - Dec 01, 2016 -> thông báo ngược lại cho user khi bị reject
//            GasScheduleEmail::BuildListNotifySettle($this, array($this->uid_login));// Jul 10, 2016
        } elseif ($stepStatus == GasSettle::UPDATE_TYPE_NEED_UPDATE) {
            // Chuyển thành yêu cầu nhân viên cập nhật thông tin
            $this->status = GasSettle::STA_REQUEST_EDIT;
        }
        //Cập nhật thời gian giám đốc thay đổi
        $this->approved_director_date = date("Y-m-d H:i:s");
    }

    protected function beforeSave()
    {
        $this->setFieldCommentsToJson();
        if ($this->isNewRecord) {
            // Gán status mới
            $this->status = GasSettle::STA_NEW;
        }
        if (empty($this->code_no)) {
            $this->code_no = MyFunctionCustom::getNextId('GasSettle', 'Q' . date('y'), LENGTH_TICKET, 'code_no');
        }        

        return parent::beforeSave();
    }

    /**
     * @Author: Trung June 03, 2016
     * get listoption người duyệt là quản lý
     */
    public function getListManager()
    {
        $aModelUser = GasOneManyBig::getArrayModelUser(GasOneManyBig::TYPE_SETTLE_LEADER, GasOneManyBig::TYPE_SETTLE_LEADER);
        $aRes = [];
        foreach ($aModelUser as $item) {
            $aRes[$item->id] = $item->getNameWithRole();
        }
        return $aRes;
    }

    /**
     * @Author: Trung Jul 14, 2016
     * get listoption người duyệt là kế toán trưởng
     */
    public function getListChiefAccountant()
    {
        $aModelUser = GasOneManyBig::getArrayModelUser(GasOneManyBig::TYPE_SETTLE_CHIEF_ACCOUNTANT, GasOneManyBig::TYPE_SETTLE_CHIEF_ACCOUNTANT);
        $aRes = [];
        foreach ($aModelUser as $item) {
            $aRes[$item->id] = $item->getNameWithRole();
        }
        return $aRes;
    }

    /**
     * @Author: Trung June 04, 2016
     * get listoption người duyệt là giám đốc
     */
    public function getListDirector()
    {
        $aModelUser = GasOneManyBig::getArrayModelUser(GasOneManyBig::TYPE_SETTLE_DIRECTOR, GasOneManyBig::TYPE_SETTLE_DIRECTOR);
        $aRes = [];
        foreach ($aModelUser as $item) {
            $aRes[$item->id] = $item->getNameWithRole();
        }
        return $aRes;
    }
    
    /**
     * @Author: Anh Dung Jul 25, 2016
     * get listoption kế toán thanh toán
     */
    public function getAccounting()
    {
        $aModelUser = GasOneManyBig::getArrayModelUser(GasOneManyBig::TYPE_SETTLE_ACCOUNTING, GasOneManyBig::TYPE_SETTLE_ACCOUNTING);
        $aRes = [];
        foreach ($aModelUser as $item) {
            $aRes[$item->id] = $item->getNameWithRole();
        }
        return $aRes;
    }
    
    /**
     * @Author: Anh Dung Jul 25, 2016
     * get listoption Thủ quỹ
     */
    public function getCashierConfirm()
    {
        $aModelUser = GasOneManyBig::getArrayModelUser(GasOneManyBig::TYPE_SETTLE_CASHIER_CONFIRM, GasOneManyBig::TYPE_SETTLE_CASHIER_CONFIRM);
        $aRes = [];
        foreach ($aModelUser as $item) {
            $aRes[$item->id] = $item->getNameWithRole();
        }
        return $aRes;
    }

    /**
     * @Author: ANH DUNG Aug 25, 2016
     * @Todo: Khoi tao session user hcm cho chi Phuong
     */
    public function initSessionUserSubAgent() {
        $session = Yii::app()->session;
        if(!isset($session['SUB_AGENT_HCM'])){
            $session['SUB_AGENT_HCM']       = Users::getArrIdUserByArrayRole(array(ROLE_SUB_USER_AGENT), array('province_id'=> GasProvince::TP_HCM));
            $session['SUB_AGENT_MIENDONG']  = Users::getArrIdUserByArrayRole(array(ROLE_SUB_USER_AGENT), array('province_id'=> array(GasProvince::TINH_BINH_DUONG, GasProvince::TINH_DONG_NAI)));
            $session['SUB_AGENT_GIALAI_KONTUM']  = Users::getArrIdUserByArrayRole(array(ROLE_SUB_USER_AGENT), array('province_id'=> array(GasProvince::TINH_GIALAI, GasProvince::TINH_KONTUM)));
            $session['SUB_AGENT_BINH_DINH'] = Users::getArrIdUserByArrayRole(array(ROLE_SUB_USER_AGENT), array('province_id'=> array(GasProvince::TINH_BINH_DINH)));
            $session['SUB_AGENT_AN_GIANG']  = Users::getArrIdUserByArrayRole(array(ROLE_SUB_USER_AGENT), array('province_id'=> array(GasProvince::TINH_AN_GIANG)));
            $session['SUB_AGENT_VL_CT_TV']  = Users::getArrIdUserByArrayRole(array(ROLE_SUB_USER_AGENT), array('province_id'=> array(GasProvince::TINH_VINH_LONG, GasProvince::TINH_CAN_THO, GasProvince::TINH_TRA_VINH)));
        }
    }
    
    /**
     * @Author: Trung June 04, 2016
     * Is manager
     */
    public function isRoleManager()
    {
        $cUid = Yii::app()->user->id;// Anh Dung Fix Jul 26, 2016
        $session = Yii::app()->session;
        if(isset($session['SES_SETTLE_LEADER'][$cUid])){
            return true;
        }
        return false;
        
//        $ownerId = Yii::app()->user->id;
//        $aModelUser = GasOneManyBig::getArrayModelUser(GasOneManyBig::TYPE_SETTLE_LEADER, GasOneManyBig::TYPE_SETTLE_LEADER);
//        foreach ($aModelUser as $item) {
//            if ($ownerId == $item->id) {
//                return true;
//            }
//        }
//        return false;
    }

    /**
     * @Author: Trung June 04, 2016
     * Is director
     */
    public function isRoleDirector()
    {
        $cUid       = Yii::app()->user->id;// Anh Dung Fix Jul 26, 2016
        $session    = Yii::app()->session;
        if($cUid == GasSupportCustomer::UID_THUY_MDX){
            return false;
        }
        if(isset($session['SES_SETTLE_DIRECTOR'][$cUid])){
            return true;
        }
        return false;
        
//        $ownerId = Yii::app()->user->id;
//        $aModelUser = GasOneManyBig::getArrayModelUser(GasOneManyBig::TYPE_SETTLE_DIRECTOR, GasOneManyBig::TYPE_SETTLE_DIRECTOR);
//        foreach ($aModelUser as $item) {
//            if ($ownerId == $item->id) {
//                return true;
//            }
//        }
//        return false;
    }

    /**
     * @Author: AnhDung Jul 14, 2016
     * Is ChiefAccountant
     * SES_SETTLE_LEADER SES_SETTLE_DIRECTOR SES_SETTLE_ACCOUNTING SES_SETTLE_CASHIER_CONFIRM
     */
    public function isRoleChiefAccountant()
    {
        $cUid = Yii::app()->user->id;// Anh Dung Fix Jul 26, 2016
        $session = Yii::app()->session;
        if(isset($session['SES_SETTLE_CHIEF_ACCOUNTANT'][$cUid])){
            return true;
        }
        return false;
        
//        $ownerId = Yii::app()->user->id;
//        $aModelUser = GasOneManyBig::getArrayModelUser(GasOneManyBig::TYPE_SETTLE_CHIEF_ACCOUNTANT, GasOneManyBig::TYPE_SETTLE_CHIEF_ACCOUNTANT);
//        foreach ($aModelUser as $item) {
//            if ($ownerId == $item->id) {
//                return true;
//            }
//        }
//        return false;
    }
    
    /**
     * @Author: AnhDung Jul 14, 2016
     * Is Accounting 
     */
    public function isRoleAccounting()
    {
        $cUid = Yii::app()->user->id;// Anh Dung Fix Jul 26, 2016
        $session = Yii::app()->session;
        if(isset($session['SES_SETTLE_ACCOUNTING'][$cUid])){
            return true;
        }
        return false;
    }
    
    /**
     * @Author: AnhDung Jul 14, 2016
     * Is isRoleCashierConfirm 
     */
    public function isRoleCashierConfirm()
    {
        $cUid = Yii::app()->user->id;// Anh Dung Fix Jul 26, 2016
        $session = Yii::app()->session;
        if(isset($session['SES_SETTLE_CASHIER_CONFIRM'][$cUid])){
            return true;
        }
        return false;
    }

    /**
     * @Author: TRUNG June-03-2016
     *  Update json from variable data
     */
    public function setFieldCommentsToJson()
    {
        $json = [];
        foreach ($this->COMMENT_JSON_FIELD as $field_name) {
            $json[$field_name] = $this->$field_name;
        }
        $this->note = json_encode($json);
    }

    /**
     * @Author: TRUNG June 03 2016
     * Get data from json file
     * @param $field_name
     * @return string
     */
    public function getFieldComment($field_name)
    {
        $temp = json_decode($this->note, true);
        if (is_array($temp) && isset($temp[$field_name])) {
            return $temp[$field_name];
        }
        return '';
    }

    /**
     * @Author: TRUNG June 03 2016
     * Map comment và status của model thành object có thể hiển thị
     */
    public function mapFieldComments()
    {
        // Update comment data
        foreach ($this->COMMENT_JSON_FIELD as $field_name) {
            $this->$field_name = $this->getFieldComment($field_name);
        }
        // Update step status data
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        if ($this->status == GasSettle::STA_REJECT) {
            $this->step_status = GasSettle::UPDATE_TYPE_REJECT;
        } elseif ($this->status == GasSettle::STA_REQUEST_EDIT) {
            $this->step_status = GasSettle::UPDATE_TYPE_NEED_UPDATE;
        } elseif (
            ($this->status == GasSettle::STA_APPROVED_BY_DIRECTOR && ($cUid == $this->uid_director || $cUid == GasLeave::UID_DIRECTOR)) ||
            ($this->status == GasSettle::STA_APPROVED_BY_CHIEF_ACCOUNTANT && $cUid == $this->uid_chief_accountant) ||
            ($this->status == GasSettle::STA_APPROVED_BY_MANAGE && $cUid == $this->uid_leader)
        ) {
            $this->step_status = GasSettle::UPDATE_TYPE_APPROVE;
        } else {
            $this->step_status = 0;
        }
    }

    /**
     * @Author: TRUNG June 03 2016
     * Get status string
     * @return string
     */
    public function getStatusText()
    {
        if (isset(GasSettle::$LIST_STATUS_TEXT[$this->status])) {
            return GasSettle::$LIST_STATUS_TEXT[$this->status];
        }
        return '';
    }
    
    public function getTypePreSettle() {
        $aData = $this->getArrayTypePreSettle();
        return isset($aData[$this->type_pre_settle]) ? $aData[$this->type_pre_settle] : '';
    }
    

    /**
     * @Author: ANH DUNG Jul 14, 2016
     * @Todo: get view on web  có nhúng thêm html view tạm ứng hay quyết toán nữa
     */
    public function getStatusTextViewOnWeb()
    {
        $res = "";
        if (isset(GasSettle::$LIST_STATUS_TEXT[$this->status])) {
            $res .= GasSettle::$LIST_STATUS_TEXT[$this->status];
        }
        $aTypeCheck = array(GasSettle::TYPE_PRE_SETTLE, GasSettle::TYPE_AFTER_SETTLE);
        if (in_array($this->type, $aTypeCheck)) {
            $res .= "<br><br>" . $this->getViewParentOrChild();
        }
        return $res;
    }

    /**
     * @Author: AnhDung Jul 14, 2016
     * get string popup view parent or Child
     * @return string
     */
    public function getViewParentOrChild()
    {
        $res = "";
        if ($this->type == GasSettle::TYPE_PRE_SETTLE) {// Record Tam Ung find Child neu co
            $mBelong = $this->getSettleBelongChild();
            if ($mBelong && $mBelong->status == GasSettle::STA_APPROVED_BY_DIRECTOR ) {
                $link = Yii::app()->createAbsoluteUrl("admin/gasSettle/view", array('id' => $mBelong->id));
                $res .= "<a href='$link' class='view'>Xem quyết toán</a>";
            } else if ($this->status == GasSettle::STA_APPROVED_BY_DIRECTOR
                && $this->uid_login == Yii::app()->user->id
            ) {
                $link = Yii::app()->createAbsoluteUrl("admin/gasSettle/createSettle", array('id' => $this->id));
                $res .= "<a href='$link'>Tạo quyết toán</a>";
            }
        } elseif ($this->type == GasSettle::TYPE_AFTER_SETTLE) {// record quyet toan
            $mBelong = $this->getSettleBelongParent();
            if ($mBelong) {
                $link = Yii::app()->createAbsoluteUrl("admin/gasSettle/view", array('id' => $mBelong->id));
                $res .= "<a href='$link' class='view'>Xem tạm ứng</a>";
            }
        }
        return $res;
    }

    /**
     * @Author: TRUNG June 03 2016
     * Get type string
     * @return string
     */
    public function getTypeText($isTitleView = false)
    {
        $res = '';
        $aTypeText = GasSettle::$LIST_TYPE_TEXT;
        if($isTitleView){
            $aTypeText[GasSettle::TYPE_SETTLE_KHONG_TAM_UNG] = 'Đề Nghị Thanh Toán';
        }
        if (isset($aTypeText[$this->type])) {
            $res = $aTypeText[$this->type];
        }
        if($this->type == GasSettle::TYPE_PRE_SETTLE && !empty($this->type_pre_settle)){
            $res .= "<br><b>{$this->getTypePreSettle()}</b>";
        }
        return $res;
    }

    public function getReasonText()
    {
        $res = '';
        $renewExpiry = $this->getRenewExpiryDate();
        if(!empty($renewExpiry)){
            $res .= '<b>Gia hạn đến: '.$renewExpiry.'</b><br><br>';
        }
        return $res .= nl2br($this->reason);
    }

    // Now 06, 2015 lấy số ngày cho phép đại lý cập nhật
    public function getDayAllowUpdate()
    {
        return Yii::app()->params['days_update_order_promotion'];
    }

    /**
     * @Author: TRUNG June 03 2016
     * Allow someone see update button
     * @return bool
     */
    public function canUpdate()
    {
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        if (($cUid != $this->uid_login) || $this->spj_auto == GasSettle::SPJ_AUTO_GEN) {
            return false; // cho phép admin sửa hết, còn những user khác thì ai tạo thì dc sửa của người đó
        }
        if ($this->type == GasSettle::TYPE_PRE_SETTLE && $this->status == GasSettle::STA_APPROVED_BY_DIRECTOR && $this->is_done_settle == 0) {
            if($this->canCreateMoreQuyetToan()){
                return true;
            }else{
                return false;
            }
        }
        if (in_array($this->status, GasSettle::$STATUS_UPDATABLE)) {
            return true; // Chỉ cho phép một số status được update
        }

//        if ($this->status == GasSettle::STA_WAIT_SETTLE) { // Anh Dung close Jul 10, 2016 - chắc sẽ ko dùng trạng thái này nữa
//            return true;
//        }

        // nếu là new thì cho update thoải mái, chỉ khi nào nó update complete thì mới check
        
        return false; 
//        $dayAllow = date('Y-m-d');
//        $dayAllow = MyFormat::modifyDays($dayAllow, $this->getDayAllowUpdate(), '-');
//        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }
    
    
    /**
     * @Author: ANH DUNG Oct 20, 2016
     * @Todo: check user có dc tạo quyết toán ko, nếu có tạo 1 cái quyết toán cho tạm ứng rồi thì ko cho tạo nữa
     */
    public function canCreateMoreQuyetToan() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('parent_id='.$this->id);
        $criteria->addCondition('status<>'.GasSettle::STA_REJECT);
        $count = self::model()->count($criteria);
        if($count == 0 ){
            return true;
        }
        return false;
    }
    /**
     * @Author: ANH DUNG Jul 26, 2016
     * @Todo: get html icon status duyệt lần 3 + Thủ Quỹ Xác Nhận
     */
    public function getHtmlIconStatus() {
        $res = "";
        if($this->cashier_confirm == self::CASHIER_CONFIRM_YES){
            $img = Yii::app()->createAbsoluteUrl(""). '/themes/gas/images/Checked-icon.png';
            $res = "<span class='AddIconGrid display_none'><img src='$img' alt='Đã xác nhận chi tiền' title='Đã xác nhận chi tiền'></span>";
        }elseif(in_array($this->status, GasSettle::$ARR_STATUS_CASHIER_VIEW)){
            $img = Yii::app()->createAbsoluteUrl(""). '/themes/gas/images/dollar.png';
            $res = "<span class='AddIconGrid display_none'><img src='$img' alt='Chờ chi tiền' title='Chờ chi tiền'></span>";
        }
        return $res;
    }
    
    /**
     * @Author: TRUNG June 03 2016
     * Get html for list
     * @return string
     */
    public function getDetailHtml()
    {
        $this->mapFieldComments();
        $res = $this->getHtmlIconStatus();
        if ($this->uid_director > 0) {
            $date = empty($this->approved_director_date) ? "" : "<br><i>(".MyFormat::dateConverYmdToDmy($this->approved_director_date, 'd/m/y H:i').")</i>";
            $res .= "<p><span class='item_b'>Giám đốc </span>:{$this->getDirectorName()}$date</p>";
            if (trim($this->comment_director) != "") {
                $res .= "<p><i><span class='item_b'>Ghi chú</span>: " . nl2br($this->comment_director) . "</i></p>";
            }
        }
        if ($this->uid_chief_accountant > 0) {
            $date = empty($this->approved_chief_accountant_date) ? "" : "<br><i>(".MyFormat::dateConverYmdToDmy($this->approved_chief_accountant_date, 'd/m/y H:i').")</i>";
            $res .= "<p><span class='item_b'>Kế toán trưởng </span>:{$this->getChiefAccountantName()}$date</p>";
            if (trim($this->comment_chief_accountant) != "") {
                $res .= "<p><i><span class='item_b'>Ghi chú</span>: " . nl2br($this->comment_chief_accountant) . "</i></p>";
            }
        }

        if ($this->uid_leader > 0) {
            $res .= "<p><span class='item_b'>Quản lý </span>:{$this->getLeaderName()}</p>";
            if (trim($this->comment_leader) != "") {
                $res .= "<p><i><span class='item_b'>Ghi chú</span>: " . nl2br($this->comment_leader) . "</i></p>";
            }
        }

        if (!empty($this->uid_accounting)) {
            $res .= "<p><span class='item_b'>Kế toán TT </span>:{$this->getAccountingName()}</p>";
        }
        if (!empty($this->uid_cashier_confirm)) {
            $res .= "<p><span class='item_b'>Thủ quỹ </span>:{$this->getCashierConfirmName()}</p>";
        }

        if(in_array($this->type, GasSettle::$ARR_TYPE_BANK) && !empty($this->bank_number)){
            $res .= "<p><span class='item_b'>Thông tin chuyển khoản</span>:</p>";
            $res .= "<p>> <span class='item_b'>Tên người nhận</span>: " . $this->bank_user . "</p>";
            $res .= "<p>> <span class='item_b'>Số tài khoản</span>: " . $this->bank_number . "</p>";
            $res .= "<p>> <span class='item_b'>Tên ngân hàng</span>: " . $this->bank_name . "</p>";
            $res .= "<p>> <span class='item_b'>Chi nhánh</span>: " . $this->bank_branch . "</p>";
            $res .= "<p>> <span class='item_b'>Tỉnh</span>: " . $this->getProvinceName() . "</p>";
        }

        if (!empty($this->distributor_id)) {
            $res .= "<p><span class='item_b'>Nhà phân phối</span>: {$this->getDistributor()}</p>";
        }
        
//        $cmsFormater = new CmsFormatter();
//        $res .= $cmsFormater->formatBreakTaskDailyFile($this);
//        $res .= $cmsFormater->getViewGasFile($this->rFileV1);
        return $res;
    }

    /**
     * @Author: ANH DUNG Jun 07, 2016
     * @Todo: get name user create
     */
    public function getNameUserCreate()
    {
        $mUser = $this->rUidLogin;
        if ($mUser) {
            return $mUser->getFullName();
        } else {
            return Yii::app()->user->first_name;
        }
    }

    /**
     * @Author: ANH DUNG Jun 07, 2016
     * Lấy tên quản lý
     */
    public function getLeaderName()
    {
        $session = Yii::app()->session;
        if (isset($session['SES_SETTLE_USER_FULLNAME'][$this->uid_leader])) {
            return $session['SES_SETTLE_USER_FULLNAME'][$this->uid_leader];
        }
        $mUser = $this->rUidLeader;
        if ($mUser) {
            return $mUser->getFullName();
        }
        return "";
    }

    /**
     * @Author: TRUNG June 07 2016
     * Lấy tên giám đốc
     * @return string
     */
    public function getDirectorName()
    {
        $session = Yii::app()->session;
        if (isset($session['SES_SETTLE_USER_FULLNAME'][$this->uid_director])) {
            return $session['SES_SETTLE_USER_FULLNAME'][$this->uid_director];
        }
        $mUser = $this->rUidDirector;
        if ($mUser) {
            return $mUser->getFullName();
        }
        return "";
    }

    /**
     * @Author: TRUNG June 07 2016
     * Lấy tên kế toán xác nhận
     * @return string
     */
    public function getAccountingName()
    {
        $session = Yii::app()->session;
        if (isset($session['SES_SETTLE_USER_FULLNAME'][$this->uid_accounting])) {
            return $session['SES_SETTLE_USER_FULLNAME'][$this->uid_accounting];
        }
        $mUser = $this->rUidAccounting;
        if ($mUser) {
            return $mUser->getFullName();
        }
        return "";
    }

    /**
     * @Author: TRUNG Jul 11 2016
     * Lấy tên kế toán trưởng
     * @return string
     */
    public function getChiefAccountantName()
    {
        $session = Yii::app()->session;
        if (isset($session['SES_SETTLE_USER_FULLNAME'][$this->uid_chief_accountant])) {
            return $session['SES_SETTLE_USER_FULLNAME'][$this->uid_chief_accountant];
        }
        $mUser = $this->rUidChiefAccountant;
        if ($mUser) {
            return $mUser->getFullName();
        }
        return "";
    }
    public function getCashierConfirmName()
    {
        $session = Yii::app()->session;
        if (isset($session['SES_SETTLE_USER_FULLNAME'][$this->uid_cashier_confirm])) {
            return $session['SES_SETTLE_USER_FULLNAME'][$this->uid_cashier_confirm];
        }
        $mUser = $this->rUidCashierConfirm;
        if ($mUser) {
            return $mUser->getFullName();
        }
        return "";
    }

    /**
     * @Author: TRUNG June 09 2016
     * Get update link which consistence with model
     * @return string
     */
    public function getUpdateLink()
    {
        // Trung: Jul 27, 2016: Cập nhật lại link update
        if ($this->type == GasSettle::TYPE_PRE_SETTLE && $this->status == GasSettle::STA_APPROVED_BY_DIRECTOR) {
            return Yii::app()->createAbsoluteUrl("admin/gasSettle/createSettle", array('id' => $this->id));
        } else {
            return Yii::app()->createAbsoluteUrl("admin/gasSettle/update", array('id' => $this->id));
        }
    }

    /**
     * @Author: TRUNG June 09 2016
     * Delete all child records
     */
    public function deleteSettleChild($ignore_id)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('parent_id', $this->id);
        $criteria->condition = "id != :id AND parent_id = :parent_id";
        $criteria->params = array(
            ':id' => $ignore_id,
            ':parent_id' => $this->id,
        );
        GasSettle::model()->deleteAll($criteria);

        $criteria = new CDbCriteria;
        $criteria->compare('settle_id', $this->id);
        GasSettleRecord::model()->deleteAll($criteria);
    }

    /**
     * @Author: TRUNG June 09 2016
     * Get Settle recordData for this model
     * @return array
     */
    public function getSettleChilds()
    {
        if ($this->type == GasSettle::TYPE_PRE_SETTLE) {
            $criteria = new CDbCriteria;
            $criteria->compare('settle_id', $this->id);
            return GasSettleRecord::model()->findAll($criteria);
        }
        return null;
    }

    /**
     * @Author: ANH DUNG Jul 14, 2016
     * @Todo: get record quyết toán phụ thuộc tạm ứng
     */
    public function getSettleBelongChild()
    {
        $criteria = new CDbCriteria();
        $criteria->compare("t.parent_id", $this->id);
        return self::model()->find($criteria);
    }

    public function getSettleBelongParent()
    {
        $criteria = new CDbCriteria();
        $criteria->compare("t.id", $this->parent_id);
        return self::model()->find($criteria);
    }


    /**
     * @Author: TRUNG Jul 10 2016
     *
     * @param $parent GasSettle Đối tượng cha (Đối tượng tạm ứng)
     * @param $update_attribute Thông tin cập nhật cho model quyết toán
     * @param $records Những thành phần trong quyết toán
     * @return array|null Null nếu không thành công, và array nếu thành công.
     */
    public function saveSettleType($parent, $update_attribute, $records)
    {
        // Kiểm tra xem các thuộc tính có được truyền lên không?
        if (isset($parent) && isset($update_attribute)) {
            $items = [];
            $valid = true;
            $total = 0;

            // Kiểm tra toàn bộ các record con. Cập nhật giá trị total từ qty và price
            if (isset($records)) {
                foreach ($records as $i => $record) {
                    $item = new GasSettleRecord('create');
                    $record['settle_id'] = $parent->id;
                    $item->attributes       = $record;
                    $item->note             = trim($item->note);
                    $item->item_name        = trim($item->item_name);
                    if (empty($item->item_name)) {// Anh Dung Jul 12, 2016
                        continue;
                    }

                    // Kiểm tra record
                    $itemValid = $item->validate();

                    if ($itemValid) {
                        // Cộng giá trị tổng thể
                        $total += $item->qty * $item->price;
                        // Thêm vào item chuẩn bị thêm
                        $items[] = $item;
                    } elseif ($item->price > 0 || $item->qty > 1 || !empty($item->item_name) || !empty($item->payment_no)
                        || !empty($item->note)
                    ) {
                        $items[] = $item;
                        $valid = false;
                        $this->addError('parent_id', 'Vui lòng kiểm tra lại thông tin quyết toán!');
                    } else {
                        // Bỏ qua những item nếu nếu nó truyền lên trống.
                    }
                }
            }
            $mGasFile = new GasFile();
            // Cập nhật giá trị tổng thể
            $this->amount = $total;
            if ($valid) {
                $this->attributes = $update_attribute;
                $this->uid_login = Yii::app()->user->id;
                $this->company_id = $parent->company_id;
                $this->parent_id = $parent->id;
                $this->type = GasSettle::TYPE_AFTER_SETTLE;
                $valid = $this->validate();
                $mGasFile->validateFile($this);
            }
            $this->checkInfoViettelPay();// add Jun2919
            if($this->hasErrors()){// add Jul0219
                $valid = false;
            }
            // Lưu model hiện tại
            if ($valid && !$this->hasErrors()) {
                // Xóa những settle record cũ theo id của model
                $parent->deleteSettleChild($this->id);

                // Lưu lại những settle record mới
                if ($this->save()) {
                    $mSettle = GasSettle::model()->findByPk($this->id);// AnhDung Jul 14, 2016 Add upload file
                    $mGasFile->saveRecordFile($mSettle, GasFile::TYPE_5_SETTLE);
                    foreach ($items as $i => $item) {
                        $item->settle_id = $parent->id;
                        $valid = $item->save();
                        if (!$valid) {
                            break;
                        }
                    }
                }
            }

            if ($valid) {
                // Cập nhật item tạm ứng thành hoàn thành
//                $parent->status = GasSettle::STA_COMPLETE;
//                $parent->save();// Anh Dung close on Jul 27, 2016 chỉ update complete cho tạm ứng khi quyết toán được duyệt lần 3
                return $items;
            }
        }

        return null;
    }

    /**
     * @Author: ANH DUNG Jul 10, 2016
     * @Todo: get list data company
     */
    public function getListDataCompany()
    {
        return Users::getArrDropdown(ROLE_COMPANY);
    }

    public function getCompany()
    {
        $mUser = $this->rCompany;
        $res = "";
        if ($mUser) {
            $res .= $mUser->getFullName();
        }
        if($this->isExpirySettle()){
            $res .= "<span class='high_light_tr'></span>";
        }
        return $res;
    }
    
    public function getCompanyOnlyName()
    {
        $mUser = $this->rCompany;
        $res = "";
        if ($mUser) {
            $res .= $mUser->getFullName();
        }
        return $res;
    }

    /**
     * @Author: ANH DUNG Jul 26, 2016
     * @Todo: get html file
     */
    public function getFileOnly() {
        $res = "";
        $cmsFormater = new CmsFormatter();
        $res .= $cmsFormater->formatBreakTaskDailyFile($this, array('width'=>30, 'height'=>30));
        $res .= $cmsFormater->getViewGasFile($this->rFileV1, array('width'=>30, 'height'=>30));
        return $res;
    }
    
    
    /**
     * @Author: ANH DUNG Jul 10, 2016
     * @Todo: get fullName user login
     */
    public function getUidLoginName()
    {
        $this->mUidLogin = $this->rUidLogin;
        if ($this->mUidLogin) {
            return $this->mUidLogin->getFullName();
        }
        return "";
    }
    public function getDistributor(){
        $mUser = $this->rDistributor;
        if ($mUser) {
            return $mUser->getFullName();
        }
        return "";
    }

    /**
     * @Author: ANH DUNG Jul 10, 2016
     * @Todo: get Chức vụ user login
     */
    public function getUidLoginRole()
    {
        $session = Yii::app()->session;
        if ($this->mUidLogin) {
            return $session['ROLE_NAME_USER'][$this->mUidLogin->role_id];
        }
        return "";
    }

    public function getAmount($format = true)
    {
        if ($format) {
            return ActiveRecord::formatCurrency($this->amount);
        }
        return $this->amount;
    }

    public function getAmountString()
    {
        $res = "";
        if ($this->amount > 0) {
            $res .= MyFormat::numberToString($this->amount);
        } else {
            $res .= 'không';
        }
        $res .= " đồng";
        return $res;
    }

    public function getReason()
    {
        return nl2br($this->reason);
    }

    public function getBankUser()
    {
        return nl2br($this->bank_user);
    }

    public function getBankNumber()
    {
        return nl2br($this->bank_number);
    }

    public function getBankName()
    {
        return nl2br($this->bank_name);
    }
    public function getBankBranch()
    {
        return nl2br($this->bank_branch);
    }
    
    /** @Author: NamNH Sep2719
    *  @Todo:get province name
    **/
    public function getProvinceName(){
        $result = '';
        if(!empty($this->rProvince)){
            $result = $this->rProvince->name;
        }
        return $result;
    }
    public function getDirectorApproveDate()
    {
        return $this->approved_director_date;
    }
    public function getRenewExpiryDate()
    {
        if(empty($this->renew_expiry_date)){
            return '';
        }
        return MyFormat::dateConverYmdToDmy($this->renew_expiry_date, 'd/m/Y');
    }

    /**
     * @Author: ANH DUNG Jul 14, 2016
     * @Todo: khởi tạo session user approve để check khi view
     */
    public function initSessionUserApproved()
    {
        $session = Yii::app()->session;
        if(!isset($session['SES_SETTLE_USER_FULLNAME'])){
            $session['SES_SETTLE_LEADER'] = GasOneManyBig::getArrOfManyId(GasOneManyBig::TYPE_SETTLE_LEADER, GasOneManyBig::TYPE_SETTLE_LEADER);
            $session['SES_SETTLE_DIRECTOR'] = GasOneManyBig::getArrOfManyId(GasOneManyBig::TYPE_SETTLE_DIRECTOR, GasOneManyBig::TYPE_SETTLE_DIRECTOR);
            $session['SES_SETTLE_CHIEF_ACCOUNTANT'] = GasOneManyBig::getArrOfManyId(GasOneManyBig::TYPE_SETTLE_CHIEF_ACCOUNTANT, GasOneManyBig::TYPE_SETTLE_CHIEF_ACCOUNTANT);
            $session['SES_SETTLE_ACCOUNTING'] = GasOneManyBig::getArrOfManyId(GasOneManyBig::TYPE_SETTLE_ACCOUNTING, GasOneManyBig::TYPE_SETTLE_ACCOUNTING);
            $session['SES_SETTLE_CASHIER_CONFIRM'] = GasOneManyBig::getArrOfManyId(GasOneManyBig::TYPE_SETTLE_CASHIER_CONFIRM, GasOneManyBig::TYPE_SETTLE_CASHIER_CONFIRM);
            $aUid = array_merge($session['SES_SETTLE_LEADER'], $session['SES_SETTLE_DIRECTOR'], $session['SES_SETTLE_CHIEF_ACCOUNTANT'], $session['SES_SETTLE_ACCOUNTING'], $session['SES_SETTLE_CASHIER_CONFIRM']);
            $session['SES_SETTLE_USER_FULLNAME'] = Users::getListOptions($aUid, array('get_all'=>1));
        }
    }
    
    /** @Author: ANH DUNG Feb 28, 2018
     *  @Todo: cho phép Ngân Hcm bấm xác nhận các quyết toán của Nhàn
     **/
    public function isUidNganHcm() {
        $cUid   = MyFormat::getCurrentUid();
        $aUidAllow = [GasConst::UID_NGUYEN_DTM, GasConst::UID_NGAN_DTM, GasConst::UID_VINH_NH];
//        if($cUid == GasConst::UID_NGAN_DTM && $this->uid_cashier_confirm == GasConst::UID_NHAN_NTT){
        if(in_array($cUid, $aUidAllow)){
            return true;// Feb2818 cho Ngân xem thanh toán của Nhàn chuyển giao
        }
        return false;
    }

    /**
     * @Author: ANH DUNG Jul 14, 2016
     * @Todo: check user can view . Lấy tất cả các user ở các cấp duyệt rồi check xem user hiện tại có dc view ko
     */
    public function canView()
    {
        $session = Yii::app()->session;
        if (!isset($session['SES_SETTLE_LEADER'])) {
            $this->initSessionUserApproved();
        }
        $aUidApprovedSettle = array_merge($session['SES_SETTLE_LEADER'], $session['SES_SETTLE_DIRECTOR'], $session['SES_SETTLE_CHIEF_ACCOUNTANT']);
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        if($this->isUidNganHcm() || in_array($cUid, $this->getUidViewAll())){
            return true;// Feb2818 cho Ngân xem thanh toán của Nhàn chuyển giao
        }
        
        if($cUid == GasSupportCustomer::UID_THUY_MDX || $this->uid_login == $cUid || $this->uid_accounting == $cUid || 
                $this->uid_leader == $cUid ||
                $this->uid_cashier_confirm == $cUid){
            return true;
        }
        
        if($this->uid_login == GasSupportCustomer::UID_THUY_MDX && in_array($cUid, GasConst::$ARR_VIEW_SETTLE_MDXT)){
            return true; // Aug 25, 2016 xử lý cho 1 số ké toán theo dõi phiếu của chị Thủy
        }
        if($this->viewBienDong()){
            return true;
        }
            
        // vì 1 người có thể nằm ở 2 chỗ duyệt, nên có thêm điều kiện !in_array SES_SETTLE_CHIEF_ACCOUNTANT
        if (in_array($cUid, $session['SES_SETTLE_LEADER']) && !in_array($cUid, $session['SES_SETTLE_CHIEF_ACCOUNTANT']) && $this->uid_leader != $cUid) {// chỉ xét ở cấp leader, không cho view của các leader khác
            return false;
        }

        $aRoleViewAll = array(ROLE_ADMIN);
        if (!in_array($cRole, $aRoleViewAll) && !in_array($cUid, $aUidApprovedSettle) && $cUid != $this->uid_login) {
            return false;
        }
        return true;
    }
    
    public function viewBienDong() {
        $cUid = MyFormat::getCurrentUid();
        if($this->company_id == GasConst::COMPANY_BIEN_DONG && in_array($cUid, array(GasConst::UID_NGAN_LT))){
            return true; // Dec 09, 2016 xử lý cho Ngân xem của Biển Đông
        }
        return false;
    }
    
    /**
     * @Author: ANH DUNG Jul 25, 2016
     * @Todo: handleCashierConfirm - thủ quỹ xác nhận chi tiền
     */
    public function handleCashierConfirm($confirm_value) {
        $this->cashier_confirm      = $confirm_value;
        $this->cashier_confirm_date = date('Y-m-d H:i:s');
        $this->update(array('cashier_confirm', "cashier_confirm_date"));
        if($this->cashier_confirm == GasSettle::CASHIER_CONFIRM_YES){
            $this->saveToDebit();
            GasScheduleSms::settleCashierConfirm($this);
        }
    }
    
    /**
     * @Author: ANH DUNG Jul 25, 2016
     * @todo: check user can make Cashier confirm 
     */
    public function canCashierConfirm() {
//        return true;// only for dev test
        $session    = Yii::app()->session;
        $cUid       = MyFormat::getCurrentUid();
        if(empty($this->uid_leader) || empty($this->uid_chief_accountant) 
            || empty($this->uid_director)
            || !in_array($this->status, GasSettle::$ARR_STATUS_CASHIER_VIEW)
        ){
            return false;
        }
        if($this->uid_login == $cUid && in_array($this->uid_login_role_id, GasSettle::$aRoleSubAgent)
                && in_array($this->status, GasSettle::$ARR_STATUS_CASHIER_VIEW)
            ){// Aug 05, 2016 xử lý cho user đại lý làm quyết toán
//            return true;// Apr0419 DungNT close - do không còn User đại lý chi nữa
        }
        
        if($this->isUidNganHcm()){
            return true;// Feb2818 cho Ngân xem thanh toán của Nhàn chuyển giao
        }
        
        if(!isset($session['SES_SETTLE_CASHIER_CONFIRM'][$cUid])
                || $this->uid_cashier_confirm != $cUid 
                || ($this->uid_login == $cUid && !in_array($this->status, GasSettle::$ARR_STATUS_CASHIER_VIEW)) ){
            return false;
        }
        return true;
//        return GasCheck::isAllowAccess('GasSettle', 'CashierConfirm'); // không check cái này nữa, phải phân quyền lại nhiều
    }
    
    /**
     * @Author: ANH DUNG Jul 25, 2016
     * @Todo: render html button CashierConfirm
     */
    public function getBtnCashierConfirm() {
        $action = "";
        $this->getNameActionSettle($action);
        if($this->cashier_confirm){
            return MyFormat::SystemNotifyMsg(MyFormat::SUCCESS_UPDATE, "Đã Xác Nhận $action Tiền");
        }
        if(!$this->canCashierConfirm()){
            return '';
        }
        $res = '';
        $urlYes = Yii::app()->createAbsoluteUrl("admin/gasSettle/view", array('id'=>$this->id, 'CashierConfirm'=>1, 'confirm_value'=>  GasSettle::CASHIER_CONFIRM_YES));
        $urlNo = Yii::app()->createAbsoluteUrl("admin/gasSettle/view", array('id'=>$this->id, 'CashierConfirm'=>1, 'confirm_value'=>  GasSettle::CASHIER_CONFIRM_NO));
        if(empty($this->cashier_confirm)){
            $res .= "<div class='form item_c'><br><br>";
                $res .= "<a href='$urlYes' class='btn_cancel f_size_14 btn_closed_tickets' alert_text='Bạn chắc chắn xác nhận đã $action tiền?'>Xác Nhận $action Tiền</a>";
            $res .= "</div>";
        }else{
//            $res .= "<div class='form item_c'><br><br>";
//                $res .= "<a href='$urlNo' class='btn_cancel f_size_14 btn_closed_tickets' alert_text='Bạn chắc chắn xác nhận đã chi tiền?'>Hủy Bỏ Xác Nhận Chi Tiền</a>";
//            $res .= "</div>";
            
            $res .= MyFormat::SystemNotifyMsg(MyFormat::SUCCESS_UPDATE, "Đã Xác Nhận $action Tiền");
        }
        return $res;
    }
    
    /**
     * @Author: ANH DUNG Jul 25, 2016
     * @Todo: render html button CashierConfirm
     * @param: $showForm biến kiểm tra xem có show form không
     */
    public function getBtnStatusUnlock(&$showForm) {
//        if(!$this->isExpirySettle()){
//            return '';
//        }
        if($this->cashier_confirm != GasSettle::CASHIER_CONFIRM_YES || $this->type_pre_settle != GasSettle::TYPE_PRE_SETTLE_NORMAL){
            return '';
        }// DungNT Aug0519 xử lý chị Ngân gia hạn các tạm ứng chưa hết hạn, chỉ cần đã chi là gia hạn được
        // Now 11, 2016 xử lý gia hạn
//        if($this->status_unlock){
//            return MyFormat::SystemNotifyMsg(MyFormat::SUCCESS_UPDATE, "Đã xác nhận chi tiền - Gỡ bỏ tạm ứng quá hạn");
//        }
        if(!GasCheck::isAllowAccess('GasSettle', 'StatusUnlock')){
            return '';
        }
//        $res = "<div class='form item_c'><br><br>";
//            $res .= "<a href='$url' class='btn_cancel f_size_14 btn_closed_tickets' alert_text='Bạn chắc chắn xác nhận đã chi tiền?'>Xác nhận chi tiền - Gỡ bỏ tạm ứng quá hạ</a>";
//        $res .= "</div>";
        $showForm = true;
        return '';
    }
    
    public function getUrlStatusUnlock() {
        return Yii::app()->createAbsoluteUrl("admin/gasSettle/statusUnlock", array('id'=>$this->id));
    }
    
    /**
     * @Author: ANH DUNG Jul 26, 2016
     * @Todo: reset some field by type
     */
    public function resetFieldByType() {        
        if(!in_array($this->type, GasSettle::$ARR_TYPE_BANK) ){
            $this->bank_user = $this->bank_number = $this->bank_name = '';
        }
    }
    
    /**
     * @Author: ANH DUNG Aug 26, 2016
     * @Todo: Tạm ứng có ngày quyết toán, Hightlight + ko cho tạo mới nếu sau 15 ngày chưa quyết toán
     */
    public function getDayViewCashierConfirm() {
        return Yii::app()->params['DaysUpdateSettle'];
    }
    
    /**
     * @Author: ANH DUNG Jul 26, 2016
     * @Todo: Tạm ứng có ngày quyết toán, Hightlight + ko cho tạo mới nếu sau 15 ngày chưa quyết toán
     */
    public function getDayExpirySettle() {
        return Yii::app()->params['DaysExpiryTamUng'];
    }
    
    /**
     * @Author: ANH DUNG Jul 26, 2016
     * @Todo: check xem user có thể tạo được tạm ứng không, nếu còn tạm ứng chưa quyết toán sau 15 ngày thì ko cho tạo nữa
     */
    public function hasExpirySettle() {
        $settleNormal = GasSettle::TYPE_PRE_SETTLE_NORMAL;
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.type_pre_settle = {$settleNormal} AND t.uid_login= {$this->uid_login} AND t.is_done_settle=0 AND t.type=" . GasSettle::TYPE_PRE_SETTLE);
        $criteria->addCondition('t.status_unlock=0 OR (t.status_unlock=1 AND t.renew_expiry_date< NOW())');
        $criteria->addCondition("t.cashier_confirm_date IS NOT NULL AND DATE_ADD(t.cashier_confirm_date, INTERVAL {$this->getDayExpirySettle()} DAY) < NOW()");
        return self::model()->count($criteria);
    }
    
    /**
     * @Author: ANH DUNG Jul 26, 2016
     * @Todo: check xem tạm ứng đã hết hạn chưa
     */
    public function isExpirySettle() {
        if( $this->is_done_settle == 1 ||
            $this->type != GasSettle::TYPE_PRE_SETTLE || $this->status != GasSettle::STA_APPROVED_BY_DIRECTOR || empty($this->cashier_confirm_date)
        ){
            return false;
        }
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, $this->getDayExpirySettle() - 1, '-');
        // Ngày hiện tại - 15 ngày mà lớn hơn ngày thủ quỹ chi tiền thì là Tạm Ứng quá hạn 
        return MyFormat::compareTwoDate($dayAllow, $this->cashier_confirm_date);
    }
    
    /**
     * @Author: ANH DUNG Jul 26, 2016
     * @Todo: set done cho tạm ứng, khi quyết toán của tạm ứng đó được anh Long duyệt
     */
    public function setDoneSettle() {
        if(empty($this->parent_id) || $this->status != GasSettle::STA_APPROVED_BY_DIRECTOR){
            return ;
        }
        $mSettleParent = self::model()->findByPk($this->parent_id);
        if($mSettleParent){
            $mSettleParent->setIsDoneSettle(GasSettle::SETTLE_DONE_YES);
        }
    }
    
    /** 
     * @Author: ANH DUNG Jul 26, 2016
     * Cập nhật hoàn thành cho tạm ứng trước đó
     */
    public function setIsDoneSettle($value) {
        $aUpdate = array('is_done_settle');
        $this->is_done_settle   = $value;
        if($this->status == GasSettle::SETTLE_DONE_YES){
            $this->status   = GasSettle::STA_COMPLETE;// Jul 27, 2016
            $aUpdate[]      = 'status';
        }
        $this->update($aUpdate);
    }
    
    /**
     * @Author: ANH DUNG Jul 28, 2016
     * @Todo: check xem quyết toán là thu tiền về hay chi tiền đi
     */
    public function getNameActionSettle(&$name) {
        $name = 'Chi';
        $deltaAmount = 0;
        if( $this->type == GasSettle::TYPE_AFTER_SETTLE){
            $parentAmount = 0;
            $this->mParent = $this->rParent;
            if ($this->mParent) {
                $parentAmount = $this->mParent->amount;
            }
            $deltaAmount = $this->amount - $parentAmount;
            if($deltaAmount < 0){
                $name = 'Thu';
            }
        }
        return $deltaAmount;
    }
    
    /** @Author: ANH DUNG Oct 27, 2018
     *  @Todo: validate số tháng hoàn tạm ứng
     **/
    public function checkMonthPreSettle() {
        $aScenarioCheck = ['create', 'update'];
        if($this->type != GasSettle::TYPE_PRE_SETTLE || !in_array($this->scenario, $aScenarioCheck)){
            return ;
        }
//        if(empty($this->monthPayDebit)){ Jul2219 DungNT close
//            $this->addError('monthPayDebit', 'Chưa nhập '.$this->getAttributeLabel('monthPayDebit'));
//        }
        if(empty($this->type_pre_settle)){
            $this->addError('type_pre_settle', 'Chưa chọn '.$this->getAttributeLabel('type_pre_settle'));
        }
    }
 
    protected function beforeValidate() {
        $this->buildArrDetail();
        $this->amount = MyFormat::removeComma($this->amount);
        $this->parent_id = !empty($this->parent_id) ? $this->parent_id : 0;
        if($this->rUidLogin){
            $this->uid_login_role_id = $this->rUidLogin->role_id;
        }
        return parent::beforeValidate();
    }
    protected function afterValidate() {
        if($this->type == GasSettle::TYPE_SETTLE_KHONG_TAM_UNG){
            $aScenarioCheck = array('create', 'update');
            if(in_array($this->scenario, $aScenarioCheck) && (!is_array($this->aDetail) || count($this->aDetail) == 0)){
                $this->addError('type', 'Chưa nhập đầy đủ thông tin diễn giải của quyết toán');
            }
        }
        $this->checkMonthPreSettle();
        return parent::afterValidate();
    }
    
    /**
     * @Author: ANH DUNG Jul 30, 2016
     * @Todo: build array detail to validate
     * @Param:      */
    public function buildArrDetail() {
        $this->aDetail = [];
        if(!isset($_POST['plan_unit'])){
            return ;
        }
        foreach($_POST['plan_unit'] as $k => $v){
            $mDetail = new GasSettleRecord();
            $mDetail->item_name = isset($_POST['item_name'][$k]) ? $_POST['item_name'][$k] : '';
            if(empty($mDetail->item_name)){
                return ;
            }
            $mDetail->item_name     = isset($_POST['item_name'][$k]) ? $_POST['item_name'][$k] : '';
            $mDetail->plan_unit     = isset($_POST['plan_unit'][$k]) ? $_POST['plan_unit'][$k] : '';
            $mDetail->plan_qty      = isset($_POST['plan_qty'][$k]) ? $_POST['plan_qty'][$k] : 0;
            $mDetail->plan_price    = isset($_POST['plan_price'][$k]) ? $_POST['plan_price'][$k] : 0;
            $mDetail->plan_total    = $mDetail->plan_qty * $mDetail->plan_price;
            $mDetail->payment_no    = isset($_POST['payment_no'][$k]) ? $_POST['payment_no'][$k] : '';
            $mDetail->unit          = isset($_POST['unit'][$k]) ? $_POST['unit'][$k] : '';
            $mDetail->qty           = isset($_POST['qty'][$k]) ? $_POST['qty'][$k] : 0;
            $mDetail->price         = isset($_POST['price'][$k]) ? $_POST['price'][$k] : 0;
            $mDetail->total         = $mDetail->qty * $mDetail->price;
            $mDetail->note          = isset($_POST['note'][$k]) ? $_POST['note'][$k] : '';
            $mDetail->distributor_id = isset($_POST['distributor_id'][$k]) ? $_POST['distributor_id'][$k] : '';
            
            $mDetail->item_name     = MyFormat::removeBadCharacters($mDetail->item_name);
            $mDetail->plan_unit     = MyFormat::removeBadCharacters($mDetail->plan_unit);
            $mDetail->unit          = MyFormat::removeBadCharacters($mDetail->unit);
            $mDetail->payment_no    = MyFormat::removeBadCharacters($mDetail->payment_no);
            $mDetail->note          = MyFormat::removeBadCharacters($mDetail->note);
            $this->aDetail[]        = $mDetail;
        }
    }
    
    /**
     * @Author: ANH DUNG Jul 30, 2016
     * @Todo: save settle không tạm ứng
     */
    public function saveSettleKhongTamUng() {
        if($this->type == GasSettle::TYPE_SETTLE_KHONG_TAM_UNG){
            GasSettleRecord::deleteBySettleId($this->id);
            if(!is_array($this->aDetail)){
                return ;
            }
            $aRowInsert=[];
            foreach($this->aDetail as $mDetail){
                $mDetail->settle_id = $this->id;
                $aRowInsert[]="('$mDetail->settle_id',
                    '$mDetail->payment_no',
                    '$mDetail->item_name',
                    '$mDetail->unit',
                    '$mDetail->qty',
                    '$mDetail->price',
                    '$mDetail->total',
                    '$mDetail->note',
                    '$mDetail->plan_unit',
                    '$mDetail->plan_qty',
                    '$mDetail->plan_price',
                    '$mDetail->plan_total',
                    '$mDetail->distributor_id'
                )";
            }
            
            $tableName = GasSettleRecord::model()->tableName();
            $sql = "insert into $tableName (settle_id,
                            payment_no,
                            item_name,
                            unit,
                            qty,
                            price,
                            total,
                            note,
                            plan_unit,
                            plan_qty,
                            plan_price,
                            plan_total,
                            distributor_id
                            ) values ".implode(',', $aRowInsert);
            if(count($aRowInsert)>0){
                Yii::app()->db->createCommand($sql)->execute();
            }
        }
    }
    
    /**
     * @Author: ANH DUNG Aug 02, 2016
     * @Todo: check user can view CashierConfirm 
     */
    public function canViewCashierConfirm() {
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        if($this->cashier_confirm != GasSettle::CASHIER_CONFIRM_YES || in_array($cUid, $this->getUidViewAll())){
            return true;
        }
        if( $this->is_done_settle == 0 && $this->type == GasSettle::TYPE_PRE_SETTLE ){
            return true;
        }
        if($this->viewBienDong() || $this->isUidNganHcm()){
            return true;
        }
        
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, $this->getDayViewCashierConfirm(), '-');
        $ok = MyFormat::compareTwoDate($this->cashier_confirm_date, $dayAllow);
        $aRoleAllow = array(ROLE_ADMIN, ROLE_CHIEF_ACCOUNTANT);
        if( !in_array($cRole, $aRoleAllow) && !$ok){
            return false;
        }
        return true;
    }
    
    protected function beforeDelete() {
        GasSettleRecord::deleteBySettleId($this->id);
        $mHomeContractDetail = new HomeContractDetail();
        $mHomeContractDetail->settle_id = $this->id;
        $mHomeContractDetail->deleteBySettleId();
        
        $mGasDebts = new GasDebts();
        $mGasDebts->relate_id    = $this->id;
        $mGasDebts->type         = GasDebts::TYPE_GAS_SETTLE;
        $mGasDebts->deleteByRelateId();
        
        return parent::beforeDelete();
    }
    
    /**
     * @Author: ANH DUNG Sep 19, 2016
     */
    public function canExportExcel() {
        return in_array($this->type, array(GasSettle::TYPE_SETTLE_KHONG_TAM_UNG));
    }
    
    /**
     * @Author: ANH DUNG Sep 27, 2016
     * @Todo: set tạm ứng gỡ bỏ tạm ứng quá hạn
     */
    public function handleStatusUnlock() {
        $aUpdate = array('status_unlock');
        $this->status_unlock        = GasSettle::SETTLE_UNLOCK;
        if(strpos($this->renew_expiry_date, '/')){
            $this->renew_expiry_date = MyFormat::dateConverDmyToYmd($this->renew_expiry_date);
            MyFormat::isValidDate($this->renew_expiry_date);
            $aUpdate[] = 'renew_expiry_date';
        }
        if(isset($_POST['InputRemoveLock']) && $_POST['InputRemoveLock'] == 1){
            $this->is_done_settle   = 1;
            $aUpdate[]              = 'is_done_settle';
        }
        $this->update($aUpdate);
    }
    
    /**
     * @Author: ANH DUNG Apr 09, 2017
     * @Todo: get array NPP của chị Thủy
     */
    public function getListDistributor() {
        $aDistributor = CacheSession::getDistributor();
        $aRes = [];
        foreach($aDistributor as $item){
            $aRes[$item['id']] = $item['first_name'];
        }
        return $aRes;
    }
    
    /** @Author: ANH DUNG Dec 09, 2017
     *  @Todo: hide record hủy bỏ sau 30 ngày bị hủy
     **/
    public function setHideRecord() {
        $days = 20;
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.status=' . GasSettle::STA_REJECT);
        $criteria->addCondition("DATE_ADD(created_date,INTERVAL $days DAY) < CURDATE()");
        $models = GasSettle::model()->findAll($criteria);
        foreach($models as $item){
            $item->status_hide = STATUS_ACTIVE;
            $item->update(['status_hide']);
        }
    }
    /** @Author: HOANG NAM 16/01/2018
     *  @Todo: Lấy danh sách type settles home contract
     *  @Code: NAM003
     **/
    public function getListTypeSettleHomeContract(){
        return array(
            GasSettle::TYPE_REQUEST_MONEY_MULTI,
            GasSettle::TYPE_BANK_TRANSFER_MULTI
        );
    }
    /** @Author: Pham Thanh Nghia 2-8-2018
     **/
    public function canExportExcelHomeContract(){
        $cUid = MyFormat::getCurrentUid();
        $aUidAllow = [GasConst::UID_ADMIN, GasConst::UID_NGAN_DTM];
        return in_array($cUid, $aUidAllow);
    }
    /** @Author: Pham Thanh Nghia Sep 7, 2018
     *  @Todo: Xuất excel, gasSettle/index
     **/
    public function canExportExcelForIndex(){
        $cUid   = MyFormat::getCurrentUid();
        $aAllow = [GasConst::UID_ADMIN, GasConst::UID_NHAN_NTT, GasLeave::PHUONG_PTK];
        return in_array($cUid, $aAllow);
    }
    /** @Author: Pham Thanh Nghia Sep 7, 2018
     *  @Todo: get noteXuất excel, gasSettle/index
     **/
    public function getNote(){
        return "Cty Gas tt tiền thuê nhà tạo ".$this->formatDate()." ";
    }
    public function formatDate(){
        $date = explode('-', $this->created_date);
        $month = $date[1];
        $year = $date[0];
        return 'T'.$month.'/'.$year;
    }
    /** @Author: Pham Thanh Nghia Oct 22 ,2018
     *  Trả về số âm của tiền tạm ứng
     **/
    public function getAmountValue(){
        return $this->amount;
    }
    
    /** @Author: ANH DUNG Oct 23, 2018
     *  @Todo: handle save tiền tạm ứng sang GasDebts
     **/
    public function saveToDebit() {
//        return ; // Feb2719 tam close lai, chua xu ly dc
        if($this->type == GasSettle::TYPE_PRE_SETTLE){// Mar1819 Open chỉ save debit với loại tạm ứng có trừ lương
            $mGasDebts = new GasDebts();
//            $this->monthPayDebit = (!empty($this->monthPayDebit) ? $this->monthPayDebit : 1);
            $this->monthPayDebit = 1;//  Jul1219 DungNT fix luôn là 1 tháng, cho bc công nợ CN chị Huệ
            $mGasDebts->createFromGasSettle($this, $this->monthPayDebit);
        }
    }
    public function loadInfoDebit() {
        if($this->type != GasSettle::TYPE_PRE_SETTLE){
            return ;
        }
        $this->monthPayDebit = GasDebts::getMonthCount($this->uid_login, $this->id);
    }
    
    /** @Author: KHANH TOAN Mar 19 2019
     *  @Todo: chay cron quy trinh che tai cua tam ung
     *  @Param: $date Y-m-d
     *  Dear IT!
        Quy trình cảnh báo quá hạn tạm ứng :
        cảnh báo trước ngày hết hạn 5 ngày để nhân viên có sự chuẩn bị
        htai đang cảnh báo sau như cảnh báo dưới là sai,
        ngày 08/06 hết hạn qtoan thì phải cảnh báo ngày 03/06 mới đúng,
        và ngày 09/06 là bắt đầu quét quá hạn 2% tạm ứng rồi
        7 ngày tiếp theo sau ngày hết hạn mà chưa qtoan thì trừ 5% tạm ứng
     * 
     **/
    public function cronCashierConfirm($date = '') {
        $from               = time(); // beginning of function
        // Quét phạt quá hạn tạm ứng
        $criteria           = new CDbCriteria();
        $this->findCashierConfirm($criteria);
        $model1             = GasSettle::model()->findAll($criteria);
        // Quét phạt quá hạn tạm ứng đã gia hạn
        $criteria2          = new CDbCriteria();
        $this->findRenewExpiry($criteria2);
        $model2 = GasSettle::model()->findAll($criteria2);
        
        $models = array_merge($model1, $model2); // Gộp lại xử lý chung
        $cDate              = empty($date) ? new DateTime("now") : new DateTime($date);
        foreach ($models as $item) {
            //cashier_confirm_date là ngày chi tạm ứng
            $nuberOfDay             = 0;
            if( empty($item->renew_expiry_date) ){ // Nếu không gia hạn
                $expDateOnly        = date('Y-m-d', strtotime($item->cashier_confirm_date));
                $cashierConfirmDate = new DateTime($expDateOnly);
//                $nuberOfDay         = date_diff($cashierConfirmDate, $cDate)->days - self::CASHIER_FREE_DAY;
                $nuberOfDay         = ((int) date_diff($cashierConfirmDate, $cDate)->format('%R%a')) - self::CASHIER_FREE_DAY;
            } else { // Nếu gia hạn
                $renewExpiryDate    = new DateTime($item->renew_expiry_date);
//                $nuberOfDay         = date_diff($renewExpiryDate, $cDate)->days;
                $nuberOfDay         = (int) date_diff($renewExpiryDate, $cDate)->format('%R%a');
            }
            
            if( $nuberOfDay == GasSettle::CASHIER_DAY_ALERT ){ // Sau 5 ngày thì cảnh báo
                $more['title']      =  '[Cảnh báo] Quá hạn tạm ứng';
                $this->setupMailInform($item, $more);
            } elseif ( $nuberOfDay == GasSettle::CASHIER_DAY_2_PERCENT ){ // Sau 1 ngày phạt 2%
                $this->handle1CashierAfter15day($item, $nuberOfDay);
            } elseif ( $nuberOfDay == GasSettle::CASHIER_DAY_5_PERCENT ){ // Sau 8 ngày phạt 5%
                $this->handle1CashierAfter15day($item, $nuberOfDay);
            } elseif ( $nuberOfDay == GasSettle::CASHIER_DAY_10_PERCENT ){ // Sau 15 ngày phạt 10%
                $this->handle1CashierAfter15day($item, $nuberOfDay);
            } elseif ( $nuberOfDay == GasSettle::CASHIER_DAY_CUT_SALARY ){ // Sau 22 ngày tru luong
                $this->handle1CashierAfter15day($item, $nuberOfDay);
            }
        }
        
        $to     = time();
        $second = $to - $from;
        $info   = "********** cronCashierConfirm punish expired advance payment done in: " . ($second) . '  Second  <=> ' . ($second / 60) . ' Minutes *******';
        Logger::WriteLog($info);
    }
    
    /** @Author: KHANH TOAN Mar 22 2019
     *  @Todo: gui email thong bao den nhan vien tam ung
     *  @Param: $mSettle
     **/
    public function setupMailInform($mSettle, $more = []) {
        $aEmail = $aUid = [];
        $aUid[$mSettle->uid_login] = $mSettle->uid_login;
        $aEmail[] = 'duongnv@spj.vn'; // Following
        $aEmail[] = 'dungnt@spj.vn'; // Following
        $aEmail[] = 'ketoan.vanphong@spj.vn'; // Following
        GasIssueTickets::getArrayEmailByArrayUid($aUid, $aEmail);
        $needMore['title']      =  $more['title'];
        if(isset($more['fine'])){
            $needMore['fine']   = $more['fine'];
        }
        if(isset($more['fine_percent'])){
            $needMore['fine_percent']   = $more['fine_percent'];
        }
        if(isset($more['day'])){
            $needMore['day']   = $more['day'];
        }
        $this->sendEmailAlert($mSettle, $aEmail, $needMore);
    }
    
    /** @Author: KHANH TOAN Mar 22 2019
     *  @Todo: xu li sau 15 ngay tam ung
     *  @Param:
     **/
    public function handle1CashierAfter15day($mSettle, $day) {
        $more = [];
        if($day == GasSettle::CASHIER_DAY_2_PERCENT ){
            $more['title']          = '[PHẠT] Quá hạn tạm ứng '.GasSettle::CASHIER_DAY_2_PERCENT.' ngày';
            $more['fine']           = $mSettle->amount*GasSettle::CASHIER_2PERCENT;
            $more['fine_percent']   = GasSettle::CASHIER_2PERCENT;
        }elseif($day == GasSettle::CASHIER_DAY_5_PERCENT ) {
            $more['title']          = '[PHẠT] Quá hạn tạm ứng '.GasSettle::CASHIER_DAY_5_PERCENT.' ngày';
            $more['fine']           = $mSettle->amount*GasSettle::CASHIER_5PERCENT;
            $more['fine_percent']   = GasSettle::CASHIER_5PERCENT;
        }elseif($day == GasSettle::CASHIER_DAY_10_PERCENT) {
            $more['title']          = '[PHẠT] Quá hạn tạm ứng '.GasSettle::CASHIER_DAY_10_PERCENT.' ngày';
            $more['fine']           = $mSettle->amount*GasSettle::CASHIER_10PERCENT + $mSettle->amount;
            $more['fine_percent']   = GasSettle::CASHIER_10PERCENT;
        }elseif($day == GasSettle::CASHIER_DAY_CUT_SALARY) {
            $more['title']          = '[Trừ lương] Quá hạn tạm ứng '.GasSettle::CASHIER_DAY_CUT_SALARY.' ngày';
            $more['fine']           = $mSettle->amount*GasSettle::CASHIER_10PERCENT + $mSettle->amount;
            $more['fine_percent']   = GasSettle::CASHIER_10PERCENT;
            $more['cut_salary']     = 1;
        }
        
        if( !empty($more['fine']) ){
            // Alert mail
            $more['day'] = $day; // Thời gian trễ hạn quyết toán
            $this->setupMailInform($mSettle, $more);
            // DuongNV phạt quá hạn tạm ứng
            $mEmployeeProblems          = new EmployeeProblems();
            $mEmployeeProblems->handleSettleExpired($mSettle, $more);
        }
    }
    
    /** @Author: KHANH TOAN 24 March 2019
     *  @Todo: get info customer and uid post to email
     **/
    public function sendEmailAlert($mSettle, $aEmail, $needMore = []) {
        if(is_null($mSettle)){
            return ;
        }
        $mUidLogin  = $mSettle->rUidLogin;
        $first_name = $address = '';
        if(!empty($mUidLogin)){
            $first_name = $mUidLogin->first_name;
//            $address    = $mUidLogin->address;
        }
        $numberDay = isset($needMore['day']) ? $needMore['day'] : '';
        if(!isset($needMore['fine'])){
            $titleHead  = "[CẢNH BÁO] Nhân viên {$first_name} chưa làm quyết toán<br><br>";
            $body       = $titleHead;
            $body       .= "<br><b>Tiền tạm ứng: </b> ". $mSettle->getAmount(true);
            $body       .= "<br><b>Lý do: </b> {$mSettle->getReasonText()}";
            $body       .= "<br><b>Mã số: </b> {$mSettle->code_no}";
            if(!empty($mSettle->cashier_confirm_date)){
                $body   .= "<br><b>Ngày hết hạn: </b> {$mSettle->getDeadline()}";
            }
            if(!empty($numberDay)){
                $body       .= "<br><b>Số ngày quá hạn: </b> {$numberDay}";
            }
            $body       .= "<br>Còn 5 ngày nữa là hết hạn tạm ứng. Nếu bạn chưa làm xong vui lòng xin gia hạn để không bị phạt tự động.";
            $body       .= "<br>Sau 6 ngày nữa, bạn sẽ bị phạt 2% tổng số tiền tạm ứng nếu chưa hoàn thành.";
        }else{
            $titleHead  = empty($needMore['cut_salary']) ? '[PHẠT]' : '[TRỪ LƯƠNG]';
            $titleHead  .= " Nhân viên {$first_name} quá hạn tạm ứng<br><br>";
            $body       = $titleHead;
            $body       .= "<br><b>Tiền tạm ứng: </b> ". $mSettle->getAmount(true);
            $body       .= "<br><b>Lý do: </b> {$mSettle->getReasonText()}";
            $body       .= "<br><b>Mã số: </b> {$mSettle->code_no}";
            $fine       = ActiveRecord::formatCurrency($needMore['fine']);
            $finePercent = '';
            if(isset($needMore['fine_percent'])){
                $percent     = $needMore['fine_percent']*100;
                $finePercent = $percent.'%';
            }
            $body       .= "<br><b>Tiền phạt: {$finePercent} = </b> {$fine}";
            if(is_null($mSettle->renew_expiry_date)){
                $dateConfirm = MyFormat::dateConverYmdToDmy($mSettle->cashier_confirm_date);
                $body   .= "<br><b>Ngày chi tiền: </b> {$dateConfirm}";
            }else{
                $dateRenewExpiry = MyFormat::dateConverYmdToDmy($mSettle->renew_expiry_date);
                $body   .= "<br><b>Ngày gia hạn: </b> {$dateRenewExpiry}";
            }
            $body       .= "<br><b>Số ngày quá hạn: </b> {$numberDay}";
            $body       .= "<br>Vui lòng quyết toán trước lần phạt tự động tiếp theo trong vòng 7 ngày tới.";
        }
        $needMore['title']          = isset($needMore['title']) ? $needMore['title'] : 'Cảnh báo chưa làm quyết toán.';
        $needMore['list_mail']      = $aEmail;
//        $needMore['SendNow']        = 1;// only dev debug testing
        SendEmail::bugToDev($body, $needMore);
    }
    
    /** @Author: DuongNV Sep0619
     *  @Todo: Email thông báo gia hạn thành công
     **/
    public function sendEmailRenewSuccess() {
        $mUidLogin  = $this->rUidLogin;
        if(empty($this) || empty($this->renew_expiry_date) || empty($mUidLogin->email)){
            return ;
        }
        $first_name = '';
        if(!empty($mUidLogin)){
            $first_name = $mUidLogin->first_name;
        }
        $titleHead  = "[THÔNG BÁO] Gia hạn quyết toán thành công";
        $body       = $titleHead.'<br><br>';
        $body       .= "<br><b>Nhân viên: </b> ". $first_name;
        $body       .= "<br><b>Mã số: </b> {$this->code_no}";
        $body       .= "<br><b>Tiền tạm ứng: </b> ". $this->getAmount(true);
        $body       .= "<br><b>Lý do: </b> {$this->getReasonText()}";
        if(!empty($this->renew_expiry_date)){
            $dateRenewExpiry = MyFormat::dateConverYmdToDmy($this->renew_expiry_date);
            $body   .= "<br><b>Ngày gia hạn: </b> {$dateRenewExpiry}";
        }
        $body       .= "<br>Nhớ quyết toán đúng thời gian nhé!";
        $body       .= "<br><br>Đây là email tự động, vui lòng không reply.";
        $needMore['title']          = $titleHead;
        $needMore['list_mail']      = [
                                        'ngannt@spj.vn',
                                        'duongnv@spj.vn',
                                        'huelt@spj.vn',
                                        $mUidLogin->email
                                    ];
//        $needMore['SendNow']        = 1;// only dev debug testing
        SendEmail::bugToDev($body, $needMore);
    }

    public function getDeadline() {
        if(empty($this->cashier_confirm_date) || $this->cashier_confirm_date == '0000-00-00'){
            return '';
        }
        $deadline = MyFormat::modifyDays($this->cashier_confirm_date, 15);
        return MyFormat::dateConverYmdToDmy($deadline);
    }
    
    /** @Author: KHANH TOAN Mar 26 2019
     *  @Todo: tim record chua quyet toan 
     *  @Param:$criteria, $field
     **/
    public function findCashierConfirm(&$criteria) {
        $day1 = self::CASHIER_FREE_DAY-5; // Cảnh báo trc 5 ngày
        $day2 = 30;
        $criteria->addCondition("is_done_settle=0 AND status_unlock=0 AND type=".GasSettle::TYPE_PRE_SETTLE); // Loại là đề nghị tạm ứng
        $criteria->addCondition("type_pre_settle=".GasSettle::TYPE_PRE_SETTLE_NORMAL); // Loại tạm ứng là quyết toán sau
        $criteria->addCondition("cashier_confirm_date IS NOT NULL"); // Chưa gia hạn
        $criteria->addCondition("DATE_ADD( DATE(cashier_confirm_date), INTERVAL $day1 DAY) <= CURDATE()"); // Ngày htại >= ngày chi tiền + 10
        $criteria->addCondition("DATE_ADD( DATE(cashier_confirm_date), INTERVAL $day2 DAY) >=  CURDATE()"); // Ngày htại <= ngày chi tiền + 30
        $criteria->addCondition("renew_expiry_date IS NULL");
    }
    
    /** @Author: KHANH TOAN Mar 26 2019
     *  @Todo: tim record gia hạn quyet toan
     *  @Param:$criteria, $field
     **/
    public function findRenewExpiry(&$criteria) {
        $criteria->addCondition("is_done_settle=0 AND status_unlock=0 AND type=".GasSettle::TYPE_PRE_SETTLE);
        $criteria->addCondition("type_pre_settle=".GasSettle::TYPE_PRE_SETTLE_NORMAL);
        $criteria->addCondition("renew_expiry_date IS NOT NULL");
        $criteria->addCondition("DATE_ADD( renew_expiry_date, INTERVAL -5 DAY) <=  CURDATE()"); // Lấy trc ngày hết hạn 5 ngày để cảnh báo
    }

    /** @Author: DungNT Jun 28, 2019
     *  @Todo: check record required info viettel pay
     **/
    public function isRequiredViettelPay() {
        $ok = false;
        $aTypeCheck = [
            GasSettle::TYPE_REQUEST_MONEY, // 'Đề nghị thanh toán',
            GasSettle::TYPE_PRE_SETTLE, // 'Đề nghị tạm ứng',
            GasSettle::TYPE_AFTER_SETTLE, // 'Quyết toán',
            GasSettle::TYPE_SETTLE_KHONG_TAM_UNG, // 'Quyết toán - Không có tạm ứng'
        ];
        if(in_array($this->type, $aTypeCheck)){
            $ok = true;
        }
        return $ok;
    }
    
    /** @Author: DungNT Jun 28, 2019
     *  @Todo: check số tk + số đt viettel pay của người tạo
     **/
    public function checkInfoViettelPay() {
        if(!$this->isRequiredViettelPay()){
            return ;
        }
        $cUid = MyFormat::getCurrentUid();
        $mUsersProfile = $this->loadUsersProfile($cUid);
//        if($this->mProfile->salary_method == UsersProfile::SALARY_METHOD_VIETTEL && empty($this->mProfile->salary_method_phone)){
//            $this->mProfile->addError('salary_method_phone', 'Chưa nhập số điện thoại Viettel Pay');
//        }
        if(empty($mUsersProfile) 
            || empty($mUsersProfile->salary_method_phone) || empty($mUsersProfile->salary_method_account) ){
//            $this->addError('uid_login', 'Tài khoản của bạn chưa cập nhật số điện thoại hoặc số tài khoản Viettel Pay. Gửi mail cho hanhnt@spj.vn chị Hạnh (0918 867 569) hoặc Châu (0905 918 639) bên nhân sự để cập nhật thì mới tạo được quyết toán');
            $this->addError('uid_login', 'Tài khoản của bạn chưa cập nhật số điện thoại hoặc số tài khoản Viettel Pay. Gửi mail cho hanhnt@spj.vn bên nhân sự để cập nhật thì mới tạo được quyết toán');
        }
    }
    
    /** @Author: DungNT Jun 28, 2019
     *  @Todo: load model usersProfile
     **/
    public function loadUsersProfile($cUid) {
        $mUsersProfile = new UsersProfile();
        return $mUsersProfile->getByUserId($cUid);
    }
    
    /** @Author: DungNT Jun 28, 2019
     *  @Todo: getViettelPayPhone
     **/
    public function getViettelPayPhone() {
        $mUsersProfile = $this->loadUsersProfile($this->uid_login);
        if(empty($mUsersProfile)){
            return ;
        }
        return $mUsersProfile->getSalaryMethodPhone();
    }
    /** @Author: DungNT Jun 28, 2019
     *  @Todo: getViettelPayAccount
     **/
    public function getViettelPayAccount() {
        $mUsersProfile = $this->loadUsersProfile($this->uid_login);
        if(empty($mUsersProfile)){
            return ;
        }
        return $mUsersProfile->getSalaryMethodAccount();
    }

    /**
     * @author LocNV Sep 13, 2019
     * @todo get comment cua thu quy
     * @return string
     */
    public function getCommentThuQuy()
    {
        $res = !empty($this->rCommentThuQuy->content) ? nl2br($this->rCommentThuQuy->content) : '';
        return $res;
    }

    /**
     * @author LocNV Sep 13, 2019
     * @todo get comment cua ke toan
     * @return string
     */
    public function getCommentKeToan()
    {
        $res = !empty($this->rCommentKeToan->content) ? nl2br($this->rCommentKeToan->content) : '';
        return $res;
    }

    /**
     * @author LocNV Sep 13, 2019
     * @todo save create comment
     * @param string $comment_thu_quy
     * @param string $comment_ke_toan
     * @return string
     */
    public function saveCreateComment($comment_thu_quy = '', $comment_ke_toan = '')
    {
        //tao comment thu quy
        $mGasComment = new  GasComment();
        $mGasComment->type = GasComment::TYPE_6_SETTLE_THUQUY_NOTE;
        $mGasComment->belong_id = $this->id;
        $mGasComment->content = $comment_thu_quy;
        $mGasComment->save();

        //tao comment ke toan
        $mGasComment = new  GasComment();
        $mGasComment->type = GasComment::TYPE_7_SETTLE_KEOAN_NOTE;
        $mGasComment->belong_id = $this->id;
        $mGasComment->content = $comment_ke_toan;
        $mGasComment->save();
    }

    /**
     * @author LocNV Sep 13, 2019
     * @todo save update comment
     * @return string
     */
    public function saveUpdateComment()
    {
        $res = false;
        $commentThuQuy = '';
        $commentKeToan = '';
        if (!empty($this->comment_thu_quy) && $this->canThuQuyUpdateComment()) {
            $commentThuQuy = $this->comment_thu_quy;
        }

        if (!empty($this->comment_ke_toan) && $this->canKeToanUpdateComment()) {
            $commentKeToan = $this->comment_ke_toan;
            $commentThuQuy = !empty($this->rCommentThuQuy->content) ? $this->rCommentThuQuy->content : '';
        }

        if (empty($this->rCommentThuQuy->id) && empty($this->rCommentKeToan->id)) {
            $this->saveCreateComment($commentThuQuy, $commentKeToan);
            $res = true;
        } else {
            $mGasCommentTQ = GasComment::model()->findByPk($this->rCommentThuQuy->id);
            $mGasCommentTQ->content = $commentThuQuy;

            $mGasCommentKT = GasComment::model()->findByPk($this->rCommentKeToan->id);
            $mGasCommentKT->content = $commentKeToan;

            if ($mGasCommentTQ->update() && $mGasCommentKT->update()) {
                $res = true;
            }
        }
        return $res;
    }

    /**
     * @author LocNV Sep 13, 2019
     * @todo can update comment thu quy
     * @return string
     */
    public function canThuQuyUpdateComment()
    {
        $cUid = MyFormat::getCurrentUid();
        $cRole = MyFormat::getCurrentRoleId();
//        $cUid = GasConst::UID_NHAN_NTT; //thu quy
        if (($this->cashier_confirm == GasSettle::CASHIER_CONFIRM_NO 
                && $cUid == GasConst::UID_NHAN_NTT)
                || $cRole == ROLE_ADMIN) {
            return true;
        }
        return false;
    }

    /**
     * @author LocNV Sep 13, 2019
     * @todo can update comment ke toan
     * @return string
     */
    public function canKeToanUpdateComment()
    {
        $cUid = MyFormat::getCurrentUid();
        $cRole = MyFormat::getCurrentRoleId();
//        $cUid = GasConst::UID_HUE_LT; //chi Hue
        if (($this->cashier_confirm == GasSettle::CASHIER_CONFIRM_YES 
                && $cUid == GasConst::UID_HUE_LT)
                || $cRole == ROLE_ADMIN) {
            return true;
        }
        return false;
    }

    /**
     * @author LocNV Sep 20, 2019
     * @todo can update comment
     * @return string
     */
    public function canUpdateComment()
    {
        if ($this->canThuQuyUpdateComment() || $this->canKeToanUpdateComment()) {
            return true;
        }
        return false;
    }
    
    public function getArrAllProvince(){
        $criteria = new CDbCriteria;
        $models = GasProvince::model()->findAll($criteria);
        return  CHtml::listData($models,'id','name');
    }
    
    public function getTypeHomeContract(){
        return [
            GasSettle::TYPE_REQUEST_MONEY_MULTI,// 'Đề nghị thanh toán hợp đồng thuê nhà'
            GasSettle::TYPE_BANK_TRANSFER_MULTI,// 'Đề nghị chuyển khoản hợp đồng thuê nhà'
        ];
    }
    
    public function getTotalPayText($strMore = '',$getZero = false){
        $result = $getZero ? 0 :'';
        $aTypeHomeContract = $this->getTypeHomeContract();
        if(in_array($this->type, $aTypeHomeContract)){
            $moneyPaid = $this->getPaidHomeContractDetail();
            $result =  $moneyPaid > 0 ? $strMore . ActiveRecord::formatCurrency($moneyPaid) : ($getZero ? 0 : '');
        }
        return $result;
    }
    
    /** @Author: NamNH Oct1419
    *  @Todo:get money paid
    **/
    public function getPaidHomeContractDetail(){
        $result = 0;
        if(empty($this->id)){
            return $result;
        }
        $criteria=new CDbCriteria;
        $criteria->select = 'sum(t.amount) as amount,t.settle_id';
        $criteria->compare('t.settle_id', $this->id);
        $criteria->compare('t.status', HomeContractDetail::STATUS_PAY);
        $criteria->group = 't.settle_id';
        $mHomeContractDetail = HomeContractDetail::model()->find($criteria);
        if(!empty($mHomeContractDetail)){
            $result = $mHomeContractDetail->amount;
        }
        return $result;
    }
}