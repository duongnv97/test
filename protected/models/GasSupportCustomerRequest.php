<?php

/**
 * This is the model class for table "{{_gas_support_customer_request}}".
 *
 * The followings are the available columns in table '{{_gas_support_customer_request}}':
 * @property string $id
 * @property string $code_no
 * @property string $customer_id 
 * @property integer $type_customer
 * @property string $sale_id
 * @property string $agent_id
 * @property string $note 
 * @property string $note_manager 
 * @property integer $status
 * @property string $json
 * @property string $created_by//
 * @property string $created_date
 * @property string $last_update_by
 * @property string $last_update_time
 */
class GasSupportCustomerRequest extends BaseSpj {

    public $emailBody='',$list_id_image,$file_name;
    public $autocomplete_name_3, $autocomplete_name,
            $autocomplete_name_sale_id, $autocomplete_name_last_update_by, $autocomplete_name_2,$autocomplete_name_4, $autocomplete_name_second, $autocomplete_name_agent;

    const STATUS_NEW                = 1;
    const STATUS_PROCESS            = 2;
    const STATUS_APPROVED           = 3;
    const STATUS_REJECT             = 4;
    const STATUS_UPDATE             = 5;
    const STATUS_CREATE_PGNV        = 6;
    const MAX_ITEM              = 15;
    const MAX_ITEM_MODULE       = 15;
    // 
    public static $ROLE_EMPLOYEE_SEARCH_AUTOCOMPLETE = array(ROLE_EMPLOYEE_MAINTAIN, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_E_MAINTAIN,  ROLE_HEAD_OF_MAINTAIN,ROLE_SALE );

    /** @Author: ANH DUNG Aug 09, 2018
     *  @Todo: loại vật tư search autocomplete app và web
     **/
    public static function getMaterialsTypeAutocomplete() {
        return GasMaterialsType::$THIET_BI;
    }
    
    public function getArrayStatus() {
        return [
            GasSupportCustomerRequest::STATUS_NEW       => 'Mới',
            GasSupportCustomerRequest::STATUS_PROCESS   => 'Chờ',
            GasSupportCustomerRequest::STATUS_APPROVED  => 'Duyệt',
            GasSupportCustomerRequest::STATUS_REJECT    => 'Từ chối',
            GasSupportCustomerRequest::STATUS_UPDATE    => 'Yêu cầu cập nhật',
        ];
    }
    
    /** @Author: Pham Thanh Nghia 2018
     *  @Todo: Trong form tạo mới không có trạng thái chờ 
     *  @Param:
     **/
    public function getArrayStatusForm() {
        return [
            GasSupportCustomerRequest::STATUS_NEW           => 'Mới',
            GasSupportCustomerRequest::STATUS_APPROVED      => 'Duyệt',
            GasSupportCustomerRequest::STATUS_REJECT        => 'Từ chối',
            GasSupportCustomerRequest::STATUS_UPDATE    => 'Yêu cầu cập nhật',
        ];
    }
    
    public function getArrayActionInvest(){
        $mGasSupportCustomer = new GasSupportCustomer();
        return $mGasSupportCustomer->getListActionInvest();
    }
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{_gas_support_customer_request}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [
            array('action_invest,customer_id, status, created_by ,json', 'required', 'on'=>'create,update'),
            array('note_manager, action_invest,note, json, last_update_time', 'safe'),
            ['file_name,list_id_image,id, code_no, customer_id, type_customer, sale_id, agent_id, note, status, json, created_by, created_date, last_update_by, last_update_time', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'rFile' => array(self::HAS_MANY, 'GasFile', 'belong_id',
                'on' => 'rFile.type=' . GasFile::TYPE_12_SUPPORT_CUSTOMER_REQUEST,
                'order' => 'rFile.id ASC',
            ),
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rSale' => array(self::BELONGS_TO, 'Users', 'sale_id'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rUserUpdate' => array(self::BELONGS_TO, 'Users', 'last_update_by'),
            'rUserCreate' => array(self::BELONGS_TO, 'Users', 'created_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'code_no' => 'Mã số',
            'customer_id' => 'Khách hàng',
            'type_customer' => 'Loại khách hàng',
            'sale_id' => 'Nhân viên KD',
            'agent_id' => 'Đại lý',
            'note' => 'Ghi chú nhân viên',
            'note_manager' => 'Ghi chú quản lý',
            'status' => 'Trạng thái',
            'json' => 'Vật tư',
            'created_by' => 'Người tạo',
            'created_date' => 'Ngày tạo',
            'last_update_by' => 'Người cập nhật',
            'last_update_time' => 'Thời gian cập nhật',
            'action_invest' => 'Loại',
            'reason_reject' => 'Lý do hủy'
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        $mUser = new Users();
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        $criteria = new CDbCriteria;
        $criteria->compare('t.code_no', $this->code_no);
        $criteria->compare('t.customer_id', $this->customer_id);
        $criteria->compare('t.type_customer', $this->type_customer);
        $criteria->compare('t.created_by', $this->created_by);
        $criteria->compare('t.sale_id', $this->sale_id);
        $criteria->compare('t.agent_id', $this->agent_id);
        if($this->status == GasSupportCustomerRequest::STATUS_NEW){
            $criteria->addInCondition('t.status', [GasSupportCustomerRequest::STATUS_NEW, GasSupportCustomerRequest::STATUS_UPDATE]);
        }elseif($this->status == GasSupportCustomerRequest::STATUS_CREATE_PGNV){
            $criteria->addCondition('t.support_customer_id > 0');
        }else{
            $criteria->compare('t.status', $this->status);
        }
        if($this->status == GasSupportCustomerRequest::STATUS_APPROVED){
            $criteria->addCondition('t.support_customer_id < 1');
        }

        $criteria->addCondition("t.status <> ".GasSupportCustomerRequest::STATUS_PROCESS);

        if(in_array($cRole, $mUser->ARR_ROLE_SALE_LIMIT)){
            $criteria->compare('t.sale_id', $cUid);
        }
        
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    /** @Author: HOANG NAM 21/06/2018
     *  @Todo: chuyển đổi array thành json
     * */
    public function setJsonToArray() {
        $this->json = json_decode($this->json, true);
    }

    /** @Author: HOANG NAM 21/06/2018
     *  @Todo: chuyển đổi array thành json
     * */
    public function setArrayToJson() {
        $this->json = json_encode($this->json);
    }

    /** @Author: HOANG NAM 21/06/2018
     *  @Todo: lấy danh sách list cho api
     * */
    public function handleApiList(&$result, $q) {
        // 1. get list order by user id
        $dataProvider = $this->ApiListing($q);
        $models = $dataProvider->data;
        $CPagination = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page'] = $CPagination->pageCount;
        if ($q->page >= $CPagination->pageCount) {
            $result['record'] = [];
        } else {
            foreach ($models as $mGasSupportCustomerRequest) {
                $mGasSupportCustomerRequest->mAppUserLogin = $this->mAppUserLogin;
                $temp = $mGasSupportCustomerRequest->getApiItem(true);
                $result['record'][] = $temp;
            }
        }
    }

    /** @Author: HOANG NAM 25/06/2018
     *  @Todo: set item model apt
     * */
    public function getApiItem($isList = false) {
        $temp = array();
        $temp['id']             = $this->id;
        $temp['code_no']        = $this->code_no;
        $temp['created_date']   = $this->getCreatedDate();
        $temp['customer_id']    = $this->customer_id;
        $temp['first_name']     = $this->getCustomer('first_name');
        $temp['address']        = $this->getCustomer('address');
        $temp['status']         = "$this->status";
        $temp['note']           = $this->getNoteApp();
        $temp['note_manager']   = $this->getNoteManager();
        $temp['action_invest']  = "$this->action_invest";
        $temp['allow_update']   = $this->employeeCanUpdate($this->mAppUserLogin);
        $mAppCache              = new AppCache();
        if (!$isList) {
            $aMaterial = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
            $aModule   = Modules::model()->getArrAllModule();
            $this->setJsonToArray();
            $aMaterialNew = array();
            foreach ($this->json as $key => $objMaterial) {
                $aMaterialNew[$key]['module_type'] = ''.$objMaterial['module_type'];
                $aMaterialNew[$key]['module_id'] = ''.$objMaterial['module_id'];
                $aMaterialNew[$key]['module_name'] = $aModule[$objMaterial['module_id']]['name'];
                $aMaterialNew[$key]['qty'] = $objMaterial['qty'];
                foreach($objMaterial['detail'] as $item){
                        $aDetail = [];
                        $aDetail['materials_type_id'] = $item['materials_type_id'];
                        $aDetail['materials_id'] = $item['materials_id'];
                        $aDetail['qty'] = $item['qty'];
                        $idMaterial = isset($item['materials_id']) ? $item['materials_id'] : '';
                        $aDetail['materials_name'] = isset($aMaterial[$idMaterial]) ? $aMaterial[$idMaterial]['name'] : '';
                        $aMaterialNew[$key]['detail'][] = $aDetail;
                    }
//                $aMaterialNew[$key]['materials_id'] = $objMaterial['materials_id'];
//                $aMaterialNew[$key]['materials_type_id'] = $objMaterial['materials_type_id'];
//                $aMaterialNew[$key]['qty'] = $objMaterial['qty'];
//                $idMaterial = isset($objMaterial['materials_id']) ? $objMaterial['materials_id'] : '';
//                $aMaterialNew[$key]['materials_name'] = isset($aMaterial[$idMaterial]) ? $aMaterial[$idMaterial]['name'] : '';
            }
            $this->json = $aMaterialNew;
            $temp['type_customer']  = $this->type_customer;
            $temp['sale_id']        = $this->sale_id;
            $temp['agent_id']       = $this->agent_id;
            $temp['json']           = $this->json;
            $temp['created_by']     = $this->created_by;
            $temp['last_update_by'] = $this->last_update_by;
            $temp['last_update_time'] = MyFormat::dateConverYmdToDmy($this->last_update_time, 'd/m/Y H:i');;
        }

        return $temp;
    }

    /** @Author: HOANG NAM 21/06/2018
     *  @Todo: get api
     * */
    public function ApiListing($q) {
        $criteria   = new CDbCriteria();
        $idUser     = !empty($this->mAppUserLogin) ? $this->mAppUserLogin->id : '0';
        $criteria->compare('t.created_by', $idUser);
        $this->handleMoreCondition($q,$criteria);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => GasAppOrder::API_LISTING_ITEM_PER_PAGE,
                'currentPage' => (int) $q->page,
            ),
        ));
    }
    public function handleMoreCondition($q, &$criteria) {
//        if($q->status == Transaction::STATUS_NEW){// Feb0518 tại sao chỗ này lại check status new để limit, sao ko mở cho toàn bộ status
            if(!empty($q->date_from)){
                $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($q->date_from);
                $criteria->addCondition("DATE(t.created_date)>= '$date_from'");
            }
            if(!empty($q->date_to)){
                $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($q->date_to);
                $criteria->addCondition("DATE(t.created_date)<='$date_to'");
            }
//        }
        if(!empty($q->customer_id)){
            $criteria->addCondition('t.customer_id='.$q->customer_id);
        }
    }

    /** @Author: HOANG NAM 25/06/2018
     *  @Todo: xử lý attributes APi
     * */
    public function handleApiAttributes($q) {
        $this->json             = $q->json;
        $this->note             = $q->note;
        $this->note_manager     = isset($q->note_manager) ? $q->note_manager : '';
        $this->action_invest    = $q->action_invest;
        if(!empty($q->status)){
            $this->status = $q->status;
        }
        if(empty($this->status)){
            $this->status = self::STATUS_NEW;
        }
        $mUsersCus = Users::model()->findByPk($q->customer_id);
        if(empty($mUsersCus)){
            throw new CHttpException(404, 'Không tìm thấy khách hàng');
        }
        $this->customer_id      = !empty($mUsersCus) ? $mUsersCus->id : 0;
        $this->type_customer    = !empty($mUsersCus) ? $mUsersCus->is_maintain : 0;
        if ($this->isNewRecord) {
            $this->buildCode();
            $this->created_by   = $this->mAppUserLogin->id;
            $this->sale_id      = !empty($mUsersCus) ? $mUsersCus->sale_id : 0;
            $this->agent_id     = !empty($mUsersCus->area_code_id) ? $mUsersCus->area_code_id : 0;
            $this->status       = GasSupportCustomerRequest::STATUS_NEW;
            $this->created_date   = date('Y-m-d H:i:s');
        }
        if (!$this->isNewRecord) {
            $this->last_update_by = $this->mAppUserLogin->id;
            $this->last_update_time = date('Y-m-d H:i:s');
        }
        if($this->scenario == 'apiCreate' || $this->scenario == 'apiUpdate'){
            $this->setMoreJson();
        }
        $this->setArrayToJson();
    }

    /** @Author: HOANG NAM 28/06/2018
     *  @Todo: cho phep update
     * */
    public function employeeCanUpdate($mUserLogin) {
        if ($this->alwayUpdate($mUserLogin)) {// Cho phép Mỹ Luôn sửa trên app
            return '1';
        }
        if (is_null($mUserLogin)) {
            return '0';
        }
//        chỉ cho phép update khi status là mới và chờ
        if ($this->status == GasSupportCustomerRequest::STATUS_UPDATE ||$this->status == GasSupportCustomerRequest::STATUS_NEW || $this->status == GasSupportCustomerRequest::STATUS_PROCESS) {
            return '1';
        }
        return '0';
    }

    /** @Author: HOANG NAM 28/06/2018
     *  @Todo: alway update
     * */
    public function alwayUpdate($mUserLogin) {
        if ($mUserLogin->id == 2) {// cho phép admin luôn luôn update
            return true;
        }
        return false;
    }

    public function buildCode() {
        $prefix_code = 'R' . date('y');
//        $this->code_no = MyFunctionCustom::getNextId('Sell', 'S'.date('y'), 10, 'code_no');
        $this->code_no = $prefix_code . ActiveRecord::randString(6, GasConst::CODE_CHAR);
    }

    /** @Author: Pham Thanh Nghia 21/6/2018 * */
    public function canUpdate() {
        $aUidAllow = [GasConst::UID_ADMIN, GasSupportCustomer::UID_QUY_PV];
        $cUid = MyFormat::getCurrentUid();
        if(!in_array($cUid, $aUidAllow)){
            return false;
        }
        $dayAllow = date('Y-m-d');// chỉ cho phép nhân viên CallCenter cập nhật trong ngày
        $dayAllow = MyFormat::modifyDays($dayAllow, 55, '-');// cho phép update trong ngày
        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }
    
    public function canDelete() {
        return true;
    }
    
    public function getCustomer($field = 'first_name') {
        $mUser = $this->rCustomer;
        return !empty($mUser) ? $mUser->$field : '';
    }

    public function getUserUpdate($field = 'first_name') {
        $mUser = $this->rUserUpdate;
        return !empty($mUser) ? $mUser->$field : '';
    }

    public function getUserCreate($field = 'first_name') {
        $mUser = $this->rUserCreate;
        return !empty($mUser) ? $mUser->$field : '';
    }

    public function getTypeCustomer() {
        $aType = CmsFormatter::$CUSTOMER_BO_MOI;
        if (!empty($aType[$this->type_customer])) {
            return $aType[$this->type_customer];
        }
        return "";
    }

    public function getSale($field = 'first_name') {
        $mUser = $this->rSale;
        return !empty($mUser) ? $mUser->$field : '';
    }

    public function getAgent($field = 'first_name') {
        $mAgent = $this->rAgent;
        return !empty($mAgent) ? $mAgent->$field : '';
    }

    public function getUpdateDate($fomat = 'd/m/Y H:i') {
        return MyFormat::dateConverYmdToDmy($this->last_update_time, $fomat);
    }

    public function getRoleSale() {
        return array(ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT);
    }

    public function getStatus() {
        $aStatus = $this->getArrayStatus();
        if (!empty($aStatus[$this->status])) {
            return $aStatus[$this->status];
        }
        return '';
    }
    /** @Author: Pham Thanh Nghia 2018
     *  @Todo: format json before view on edit web
     *  @Param:
     **/
    public function getJsonToArray() { // for web
        if($this->scenario == 'update'){
//            NamNH 20180831
            $aObj =  json_decode($this->json,true);
            $this->json = $aObj;
        }
    }
    /** @Author: Pham Thanh Nghia 2018
     *  @Todo: format json product to table for index
     *  @Param:
     **/
    public function formatJsonProduct() {
        $session=Yii::app()->session;
        $aModelMaterial = $session['MATERIAL_SUPPORT'];
        
        // NamNH 20180831
        $stt =1;
        $str = '<table  cellpadding=\'0\' cellspacing=\'0\'> ';
        $str .= '<tr>';
        $str .= '<td>#</td>';
        $str .= '<td>Loại module</td>';
        $str .= '<td>Module</td>';
        $str .= '<td>Số lượng</td>';
        $str .= '</tr>';
        $sumQty = 0;
        $qtyMaterial = 0;
        $aJson = json_decode($this->json,true);
//        $aMaterialType = GasMaterialsType::getAllItem();
        foreach ($aJson as  $value) {
            $module_id      = !empty($value['module_id'])? $value['module_id'] : '';
            $mModules       = Modules::model()->findByPk($module_id);
            if(empty($mModules)){
                continue;
            }
            $countRow = isset($value['detail']) && is_array($value['detail']) ? 1 + count($value['detail']) : 1; 
            $qty            = !empty($value['qty'])? $value['qty'] : '';
            $str .= '<tr class="item_b">';
            $str .= "<td rowspan='{$countRow}'  style='border:1px solid #000; width:10px;'>".$stt.'</td>';
            $str .= '<td  style=\'border:1px solid #000;\'>' . $mModules->getField('type') . '</td>';
            $str .= '<td  style=\'border:1px solid #000;\'>' . $mModules->getField('name') . '</td>';
            $str .= '<td style=\'text-align:right; border:1px solid #000;\'>' . $qty . '</td>';
            $str .= '</tr>';
//            NamNH 20180904
            if(isset($value['detail']) && is_array($value['detail'])){
                $str .= '<tr>';
                $str .= '</tr>';
                foreach ($value['detail'] as $item) {
//                    $aMaterialType[$item['materials_type_id']]
//                    $mGasMaterials = GasMaterials::model()->findByPk($item['materials_id']);
                    $nameItem = isset($aModelMaterial[$item['materials_id']]) ? $aModelMaterial[$item['materials_id']]['name'] : '';
                    $str .= '<tr>';
                    $str .= '<td colspan="2" style=\'border:1px solid #000; width:10px;\'>'. $nameItem .'</td>';
                    $str .= '<td  style=\'border:1px solid #000; width:10px;\'>'.$item['qty'].'</td>';
                    $str .= '</tr>';
                    $qtyMaterial += $item['qty'];
                }
            }
            $sumQty += $qty;
            $stt++;
        }
        $str .= '<tr>';
        $str .= '<td style=\'text-align:right;font-weight: bold;\' colspan=\'3\'><b> Tổng module :</b></td>';
        $str .= '<td style=\'text-align:right;font-weight: bold;\'><b>' . $sumQty . '</b></td>';
        $str .= '</tr>';
        $str .= '</table>';

        return $str;
    }

    /**
     * 
     * NamNH 20180831 fix convert json
     */
    public function formatJson() {
        $aJson = $this->json;
        $aJsonSave = [];
        if (!empty($aJson) && is_array($aJson)) {
            foreach ($aJson as $key => $value) {
                if(!isset($value['detail'])|| empty($value['module_id']) || empty($value['qty']) || $value['qty'] <= 0){
                    continue;
                }
                $aJsonSave[$key]['module_type'] = $value['module_type'];
                $aJsonSave[$key]['module_id']   = $value['module_id'];
                $aJsonSave[$key]['qty']         = $value['qty'];
                $aJsonSave[$key]['detail']      = [];
                if(isset($value['detail']['materials_id']) && is_array($value['detail']['materials_id'])){
                    foreach ($value['detail']['materials_id'] as $keyDetail => $vDetail) {
                        if(empty($vDetail)){
                            continue;
                        }
                        $aJsonSave[$key]['detail'][$keyDetail]['materials_id']      = $vDetail;
                        $aJsonSave[$key]['detail'][$keyDetail]['materials_type_id'] = $value['detail']['materials_type_id'][$keyDetail];
                        $aJsonSave[$key]['detail'][$keyDetail]['qty']               = $value['detail']['qty'][$keyDetail];
                    }
                }
                
            }
        }
        if(count($aJsonSave) <= 0){
            $this->addError('json', 'Chưa chọn module.');
        }
        $this->json = $aJsonSave;
    }
    /** @Author: Pham Thanh Nghia 2018
     *  @Todo: Xét trạng thái hiện button generate
     **/
    public function canCreateGenerate(){
        $cUid = MyFormat::getCurrentUid();
        $cRole = MyFormat::getCurrentRoleId();
        if( !empty($this->support_customer_id) || $this->status == GasSupportCustomerRequest::STATUS_NEW || $this->status == GasSupportCustomerRequest::STATUS_REJECT){
            return false;
        }
        $mCustomer = $this->rCustomer;
        $aRoleAllowAll = [ROLE_SALE_ADMIN, ROLE_HEAD_GAS_BO, ROLE_DIRECTOR_BUSSINESS, ROLE_ADMIN];
        
        if(($mCustomer && $mCustomer->sale_id == $cUid)
            || in_array($cRole, $aRoleAllowAll)
        ){
            return true;
        }
        return false;
    }
    public function formatJsonToSave(){
        $this->formatJson();
        $q = $this;
        $mAppUserLogin = new Users();
        $mAppUserLogin->id = $this->created_by ; //MyFormat::getCurrentUid();
        $this->mAppUserLogin = $mAppUserLogin; 
        $this->handleApiAttributes($q);
    }
    
    /** @Author: ANH DUNG Aug 03, 2018
     *  @Todo: get array material search app + web
     **/
    public function getDataMaterial() {
        $mAppCache = new AppCache();
        return $mAppCache->getObjMaterialByType(GasSupportCustomerRequest::getMaterialsTypeAutocomplete(), AppCache::ARR_MODEL_MATERIAL_SUPPORT, ['GetActive'=>1]);
    }
    
    /** @Author: Pham Thanh Nghia Aug 03, 2018
     *  @Todo: format Obj Data to json for web
     *  @Param:
     **/
    public function formatToJsonMaterialForWeb(){
        return CacheSession::getListMaterialJson();// Oct1918 Anh Dung
        
        $aMaterial = $this->getDataMaterial();
        $aResult = array();
        foreach ($aMaterial as $key => $value) {
            $aResult[] =  array(
               'label'         => $value['materials_id'].'- '.$value['materials_name'],
               'value'         => $value['materials_name'],
               'id'            => $value['materials_id'],
               'name'          => $value['materials_name'],
               'materials_type_id' =>$value['materials_type_id'],                    
               'qty'           => 0,
           );
        }
        return CJSON::encode($aResult);
    }
    
    /** @Author: HOANG NAM 10/08/2018
     *  @Todo: get file of request
     **/
    public function getFileOnly() {
        return $this->rFile;
    }
    
    /** @Author: HOANG NAM 10/08/2018
     *  @Todo: save file upload. Xử lý chung cho api và web nên đặt ở đây
     **/
    public function afterSave() {
        $this->handleAppSaveFile(GasFile::TYPE_12_SUPPORT_CUSTOMER_REQUEST); 
        return parent::afterSave();
    }
    
    public function getNoteApp(){
        return $this->note;
    }
    
    public function getNote(){
        $res = '';
        if(!empty($this->note_manager)){
            $res .= "<b>Người duyệt: </b>{$this->getNoteManager()}";
        }
        if(!empty($this->note)){
            $res .= "<br><b>Người tạo ghi chú: </b>$this->note";
        }
        if(!empty($this->reason_reject)){
            $res .= "<br><b>Lý do hủy: </b>$this->reason_reject";
        }
        return $res;
    }
    
    public function getNoteManager(){
        return isset($this->note_manager) ? $this->note_manager : '';
    }
    
    /** @Author: NGUYEN KHANH TOAN 2018
     *  @Todo: Gửi email cho NV kinh doanh
     *  @Param: 
     **/
    public function sendEmail(){
        try {
            /* 1. build nội dung email save vào table GasScheduleEmail
             * 2. build file pdf vào thư mục tạm của server 
             * 3. để cron tự gửi
             * 4. bugToDev
             */
        $from = time();
        $aMailDev = array();
        $aJson = array();
        $needMore['title'] = 'Thông báo tạo mới yêu cầu vật tư';
//        $needMore['list_mail'] = $this->getArrayListEmail($mCustomer->id);
//        $needMore['time_send'] = 60;
        $needMore['SendNow'] = 1;
        SendEmail::bugToDev($body, $needMore);
        $to = time();
        $second = $to - $from;
        $ResultRun = "CRON send mail: " . count($aIdUser) . ' done in: ' . ($second) . '  Second  <=> ' . round($second / 60, 2) . ' Minutes '. $sLog;
        Logger::WriteLog($ResultRun);
        } catch (Exception $exc) {
            $ResultRun = "Error".$exc->getMessage();
            Logger::WriteLog($ResultRun);
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: ANH DUNG Dec 10, 2018
     *  @Todo: email notify cho a Quý + sale khi có tạo mới
     **/
    public function sendEmailAfterCreate() {
        $aEmail = [];
        $aUid[GasSupportCustomer::UID_QUY_PV]   = GasSupportCustomer::UID_QUY_PV;
//        $aUid[GasConst::UID_DUNG_NT]            = GasConst::UID_DUNG_NT;
        $this->getListEmail($aUid, $aEmail);
        $this->titleNotify  = "[Tạo mới] Yêu cầu vật tư: " . $this->titleNotify;
        $this->emailBody    = "Nhân viên [{$this->getUserCreate()}] Tạo mới yêu cầu vật tư: {$this->getActionInvest()} ".$this->emailBody;
        $this->sendEmailNotify($aEmail);
    }
    
    /** @Author: ANH DUNG Dec 10, 2018
     *  @Todo: email notify cho a Sale khi A Quý Duyệt
     **/
    public function sendEmailAfterApproved() {
        if($this->modelOld->status == $this->status){
            return ;// check only send mail when approved lần đầu
        }
        $aEmail = $aUid = [];
//        $aUid[GasConst::UID_DUNG_NT]            = GasConst::UID_DUNG_NT;
        $this->getListEmail($aUid, $aEmail);
        $this->titleNotify  = "[PV Quý Duyệt] Yêu cầu vật tư: ".$this->titleNotify;
        $this->emailBody    = "[PV Quý Duyệt] Yêu cầu vật tư: {$this->getActionInvest()} ".$this->emailBody;
        $this->sendEmailNotify($aEmail);
    }
    
    public function getListEmail(&$aUid, &$aEmail) {
        $mCustomer = Users::model()->findByPk($this->customer_id);
        if(empty($mCustomer)){
            return ;
        }
        $sale_id = $mCustomer->sale_id;
        $mGasIssueTickets = new GasIssueTickets();
        $mGasIssueTickets->sale_id = $sale_id;
        $mGasIssueTickets->getSaleIdOnly($aUid);

        $this->getArrayEmailByArrayUid($aUid, $aEmail);
        $this->titleNotify = $mCustomer->first_name;
        $detailItem = "<br><p><b>Chi tiết vật tư:</b></p>{$this->formatJsonProduct()}";
        $mSendEmail = new SendEmail();
        $this->emailBody = $mSendEmail->getHtmlCustomer($mCustomer);
        $this->emailBody .= "<br><b>Ghi chú:</b> {$this->note} $detailItem";
    }
    
    public function getArrayEmailByArrayUid(&$aUid, &$aEmail){
        if(count($aUid) < 1 ){// Jul0418 nếu không kiểm tra thì sẽ get all email hệ thống nếu bị empty array
            return ;
        }
        $needMore = array('aUid'=>$aUid);
        $aUser = Users::GetListUserMail($needMore);
        foreach($aUser as $item){
            if(!empty($item->email)){
                $aEmail[] = $item->email;
            }
        }
    }
        
    public function sendEmailNotify($aEmail, $needMore = []){
        $needMore['title']          = isset($needMore['title']) ? $needMore['title'] : $this->titleNotify;
        $needMore['list_mail']      = $aEmail;
//        $needMore['SendNow']        = 1;// only dev debug
//        $needMore['list_mail']  = ['dungnt@spj.vn'];
        SendEmail::bugToDev($this->emailBody, $needMore);
    }
    
    /** @Author: DuongNV 30/08/18 3:46 PM
     *  @Todo: Lưu modules detail vào json
     *  @Param:
     **/
    public function setMoreJson() {
        $aMaterialNew = array();
        foreach ($this->json as $key => $objMaterial) {
            $objMaterial = (array)$objMaterial;
            if($objMaterial['qty'] <= 0){
                continue;
            }
            $aMaterialNew[$key]['module_type'] = $objMaterial['module_type'];
            $aMaterialNew[$key]['module_id'] = $objMaterial['module_id'];
            $aMaterialNew[$key]['qty'] = $objMaterial['qty'];
            $aMaterialNew[$key]['detail'] = [];
            $criteria = new CDbCriteria;
            $criteria->compare('t.modules_id', $objMaterial['module_id']);
            $criteria->compare('t.type', $objMaterial['module_type']);
            $aModelModuleDetail = ModulesDetail::model()->findAll($criteria);
            if(!empty($aModelModuleDetail)){
                foreach($aModelModuleDetail as $item){
                    $aDetail = [];
                    $aDetail['materials_id'] = $item->materials_id;
                    $aDetail['materials_type_id'] = $item->materials_type_id;
                    $aDetail['qty'] = $objMaterial['qty']*$item->qty; // qty(json) = qty(module)*qty(detail)
                    $aMaterialNew[$key]['detail'][] = $aDetail;
                }
            }
        }
        $this->json = $aMaterialNew;
    }
    
    public function getViewImg(){
        $result = '';
        foreach ($this->rFile as $mFile){
            $result .= $mFile->getViewImg();
        }
        return $result;
    }
    
    /**
     * get action invest
     */
    public function getActionInvest(){
        $aAction = $this->getArrayActionInvest();
        return isset($aAction[$this->action_invest]) ? $aAction[$this->action_invest] : '';
    }
    
    public function getImageViewGrid() {
        $res = '';
        $cmsFormater = new CmsFormatter();
        $res .= $cmsFormater->formatBreakTaskDailyFile($this, array('width'=>30, 'height'=>30));
        return $res;
    }
    
    /** @Author: ANH DUNG Dec 19, 2018 
     *  @Todo: send notify to user create when status = STATUS_UPDATE
     **/
    public function notifyRequestUpdate() {
        if($this->status != GasSupportCustomerRequest::STATUS_UPDATE){
            return ;
        }
        $this->titleNotify = "Sửa lại yêu cầu vật tư: {$this->note_manager}. KH: {$this->getCustomer()}";
        $aUid = [$this->created_by, $this->sale_id];
//        $this->runInsertNotify($aUid, true); // Send Now
        $this->runInsertNotify($aUid, false);
    }
    
    /**
     * @Author: ANH DUNG Dec 21, 2018
     * @Todo: insert to table notify
     * @Param: $aUid array user id need notify
     */
    public function runInsertNotify($aUid, $sendNow) {
        $json_var = [];
        foreach($aUid as $uid) {
            if( !empty($uid) ){
                $mScheduleNotify = GasScheduleNotify::InsertRecord($uid, GasScheduleNotify::SUPPORT_REQUEST_ITEM, $this->id, '', $this->titleNotify, $json_var);
                if($sendNow && !is_null($mScheduleNotify)){
                    $mScheduleNotify->sendImmediateForSomeUser();
                }// Jan 05, 2016 tạm close send luôn lại, vì nó gây chậm bên PMBH khi send
                // sẽ gửi = cron
            }
        }
    }
    
    /**
     * @Author: ANH DUNG Dec 20, 2018
     * @Todo: get html icon green approved
     */
    public function getHtmlIconGreen() {
        if(empty($this->support_customer_id)){
            return '';
        }
        $img = Yii::app()->createAbsoluteUrl(''). '/themes/gas/images/Checked-icon.png';
        $res = "<span class='AddIconGrid display_none'><img src='$img' alt='Đã tạo phiếu giao nhiệm vụ' title='Đã tạo phiếu giao nhiệm vụ'></span>";
        return $res;
    }
    
    public function getCodeNo(){
        if(!$this->canCancelRequest()){
            return $this->code_no;
        }
        $cancelLink = "<br>" . CHtml::link('Hủy phiếu',['gasSupportCustomerRequest/index', 'cancleRequest'=>1, 'id'=>$this->id], ['class'=>'cancel_request_btn']);
        return $this->code_no . $cancelLink;
    }
    
    /** @Author: DuongNV Feb 20,2019
     *  @Todo: Check quyền xem link hủy phiếu đã duyệt
     **/
    public function canCancelRequest(){
        if($this->status != self::STATUS_APPROVED){
            return false;
        }
        $aUidAllow = [GasConst::UID_HA_PT, GasConst::UID_ADMIN];
        $cUid = MyFormat::getCurrentUid();
        return in_array($cUid, $aUidAllow);
    }
    
    /** @Author: DuongNV Feb 20,2019
     *  @Todo: hủy phiếu
     **/
    public function cancelRequest(){
        $this->status           = self::STATUS_REJECT;
        $this->last_update_by   = MyFormat::getCurrentUid();
        $this->last_update_time = $this->getCreatedDateDb();
        $this->reason_reject    = isset($_POST['GasSupportCustomerRequest']['reason_reject']) ? $_POST['GasSupportCustomerRequest']['reason_reject'] : "";
    }
    
}
