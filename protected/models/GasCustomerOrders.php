<?php

/**
 * This is the model class for table "{{_gas_customer_orders}}".
 *
 * The followings are the available columns in table '{{_gas_customer_orders}}':
 * @property string $id
 * @property integer $customer_id
 * @property integer $sale_id
 * @property string $date_delivery
 * @property integer $quantity_50
 * @property integer $quantity_45
 * @property integer $quantity_12
 * @property integer $user_id_create
 * @property string $note
 * @property string $created_date
 */
class GasCustomerOrders extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GasCustomerOrders the static model class
	 */
	public $autocomplete_name;
    public $file_excel;	 
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{_gas_customer_orders}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('customer_id', 'required','on'=>'create_GasCustomerOrders, update_GasCustomerOrders'),
			array('customer_id, sale_id, quantity_50, quantity_45, quantity_12, user_id_create', 'numerical', 'integerOnly'=>true),
			array('id', 'length', 'max'=>11),
			array('note', 'length', 'max'=>500),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, customer_id, sale_id, date_delivery, quantity_50, quantity_45, quantity_12, user_id_create, note, created_date,orders_no', 'safe'),
			array('note_extra, other_material, agent_id,time_delivery', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'customer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
		'user_create' => array(self::BELONGS_TO, 'Users', 'user_id_create'),
		'agent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'customer_id' => 'Khách Hàng',
			'sale_id' => 'NV Sale',
			'date_delivery' => 'Ngày Giao',
			'quantity_50' => 'Bình 50',
			'quantity_45' => 'Bình 45',
			'quantity_12' => 'Bình 12',
			'user_id_create' => 'Nhân Viên Tạo',
			'note' => 'Nội Dung Công Việc',
			'note_extra' => 'Ghi Chú',
			'created_date' => 'Ngày Tạo',
			'orders_no' => 'Mã Đặt Hàng',
			'time_delivery' => 'Thời Gian Giao',
			'agent_id' => 'Nơi Giao',
			'other_material' => 'Vật Tư Khác',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id,true);
		$criteria->compare('t.customer_id',$this->customer_id);
		$criteria->compare('t.sale_id',$this->sale_id);
		$criteria->compare('t.quantity_50',$this->quantity_50);
		$criteria->compare('t.quantity_45',$this->quantity_45);
		$criteria->compare('t.quantity_12',$this->quantity_12);
		$criteria->compare('t.user_id_create',$this->user_id_create);
		$criteria->compare('t.note',$this->note,true);
		
		if(!empty($this->date_delivery)){
			$this->date_delivery = MyFormat::dateConverDmyToYmd($this->date_delivery);
			$criteria->compare('t.date_delivery',$this->date_delivery,true);
		}		
		
		if(!empty($this->created_date)){
			$this->created_date = MyFormat::dateConverDmyToYmd($this->created_date);
			$criteria->compare('t.created_date',$this->created_date,true);
		}		
                
                $criteria->order = "t.id desc";
                
		Yii::app()->session['excel_customer_order'] = $criteria;
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
		));
	}
        
	public static function getCriteriaDataOrders()
	{
            $criteria=new CDbCriteria;                
            $criteria->compare('YEAR(t.date_delivery)',date('Y'));		
            $criteria->order = "t.id asc";
            Yii::app()->session['excel_customer_order'] = $criteria;
	}
        
        public static function getAllDataOrders(){
            if(!isset(Yii::app()->session['excel_customer_order']))
                $criteria = GasCustomerOrders::getCriteriaDataOrders();
            
            $criteria = Yii::app()->session['excel_customer_order'];
            $criteria->order = "t.id asc";
            unset(Yii::app()->session['excel_customer_order']);
            return GasCustomerOrders::model()->findAll($criteria);
        }

    /*
    public function activate()
    {
        $this->status = 1;
        $this->update();
    }

    public function deactivate()
    {
        $this->status = 0;
        $this->update();
    }
	*/

	public function defaultScope()
	{
		return array(
			//'condition'=>'',
		);
	}	

        public function beforeSave() {
            if($this->isNewRecord){
                $this->user_id_create = Yii::app()->user->id;
                $this->created_date = date('Y-m-d H:i:s');
                $this->orders_no = ActiveRecord::checkOrderNo('GA', GenShortId::alphaID(time()), 'GasCustomerOrders', 'orders_no');
            }
            $this->date_delivery =  MyFormat::dateConverDmyToYmd($this->date_delivery);           
            return parent::beforeSave();
        }	
        
        
      public static function Export_customer_order()
    {                    
            Yii::import('application.extensions.vendors.PHPExcel',true);
            $objPHPExcel = new PHPExcel();
             // Set properties
             $objPHPExcel->getProperties()->setCreator("NguyenDung")
                        ->setLastModifiedBy("NguyenDung")
                        ->setTitle('Customer Orders')
                        ->setSubject("Office 2007 XLSX Document")
                        ->setDescription("Customer Orders")
                        ->setKeywords("office 2007 openxml php")
                        ->setCategory("GAS");
             $objPHPExcel->getActiveSheet()->setTitle('Customer Orders');
             //             
             $objPHPExcel->setActiveSheetIndex(0);
             $objPHPExcel->getActiveSheet()->setCellValue("A1", 'Hạn thu');
             
            $rowStartFormat = 5;
            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->setTitle('GIAO HÀNG '.date('d-m-Y'));
            $objPHPExcel->getActiveSheet()->setCellValue("A1", 'LỊCH GIAO HÀNG NGÀY '.date('d-m-Y'));
            $objPHPExcel->getActiveSheet()->mergeCells("A1:E1");

            $objPHPExcel->getActiveSheet()->setCellValue("A5", 'STT');
            $objPHPExcel->getActiveSheet()->setCellValue("B5", 'THỜI GIAN GIAO HÀNG');
            $objPHPExcel->getActiveSheet()->setCellValue("C5", 'SỐ XE');
            $objPHPExcel->getActiveSheet()->setCellValue("D5", 'TÊN KHÁCH HÀNG');
            $objPHPExcel->getActiveSheet()->setCellValue("E5", 'ĐỊA ĐIỂM GIAO HÀNG');
            $objPHPExcel->getActiveSheet()->setCellValue("F5", 'NỘI DUNG CÔNG VIỆC');
            $objPHPExcel->getActiveSheet()->setCellValue("G5", 'SỐ LƯỢNG / ĐƠN VỊ TÍNH');           
            $objPHPExcel->getActiveSheet()->setCellValue("L5", 'GHI CHÚ');
            $objPHPExcel->getActiveSheet()->setCellValue("G6", 'LPG (Tấn)');
            $objPHPExcel->getActiveSheet()->setCellValue("H6", 'Bình 50 Kg');
            $objPHPExcel->getActiveSheet()->setCellValue("I6", 'Bình 45 Kg');
            $objPHPExcel->getActiveSheet()->setCellValue("J6", 'Bình 12 Kg');
            $objPHPExcel->getActiveSheet()->setCellValue("K6", 'VẬT TƯ KHÁC');
            
            $objPHPExcel->getActiveSheet()->mergeCells('A5:A6');
            $objPHPExcel->getActiveSheet()->getStyle('A5:A6')
                   ->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->mergeCells('B5:B6');
            $objPHPExcel->getActiveSheet()->getStyle('B5:B6')
                   ->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->mergeCells('c5:c6');
            $objPHPExcel->getActiveSheet()->getStyle('c5:c6')
                   ->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->mergeCells('D5:D6');
            $objPHPExcel->getActiveSheet()->getStyle('D5:D6')
                   ->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->mergeCells('E5:E6');
            $objPHPExcel->getActiveSheet()->getStyle('E5:E6')
                   ->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->mergeCells('F5:F6');
            $objPHPExcel->getActiveSheet()->getStyle('F5:F6')
                   ->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            
            $objPHPExcel->getActiveSheet()->mergeCells('G5:K5');
            $objPHPExcel->getActiveSheet()->getStyle('G5:K5')
                   ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $objPHPExcel->getActiveSheet()
                            ->getStyle('A'.$rowStartFormat.':L'.($rowStartFormat+1))
                            ->getFont()
                            //->setSize(14)
                            ->setBold(true);		 
            
            $rowNum = 7;
            
            $index = 1;
            $data = GasCustomerOrders::getAllDataOrders();            
            if(count($data)>0)
            foreach ($data as $key => $model){        
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).$rowNum, ($key+1));
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).$rowNum, $model->time_delivery);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+2).$rowNum, $model->agent?$model->agent->code_account.'_CHANGE TO BUSSINESS':'');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+3).$rowNum, $model->customer?$model->customer->first_name:'');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+4).$rowNum, $model->customer?$model->customer->address:'');
                
                $note = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $model->note);
                $remove = array( "&nbsp;");
                $note = str_replace(ExportList::$remove, " ", $note);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+5).$rowNum, $note);
                $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index+5).$rowNum)->getAlignment()->setWrapText(true);
                // đổi số kg ra tấn
                $LPG = round(($model->quantity_50*KL_BINH_50 + $model->quantity_45*KL_BINH_45 + $model->quantity_12*KL_BINH_45)/1000,3);
//                if(!strpos($LPG, '.'));
//                    $LPG = round($LPG);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+6).$rowNum, $LPG);
                
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+7).$rowNum, $model->quantity_50);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+8).$rowNum, $model->quantity_45);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+9).$rowNum, $model->quantity_12);
                $note = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $model->other_material);
                $note = str_replace(ExportList::$remove, " ", $note);
                
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+10).$rowNum, $note);
                $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index+10).$rowNum)->getAlignment()->setWrapText(true);
                $note = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $model->note_extra);
                $note = str_replace(ExportList::$remove, " ", $note);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+11).$rowNum, $note);
                $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index+11).$rowNum)->getAlignment()->setWrapText(true);
                $rowNum++;
            }    
            
            $objPHPExcel->getActiveSheet()->getStyle('A'.$rowStartFormat.':A'.($rowNum))
                    ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);    
            
            $objPHPExcel->getActiveSheet()->getStyle('H'.$rowStartFormat.':H'.($rowNum))
                    ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);    
            
            $objPHPExcel->getActiveSheet()->getStyle('I'.$rowStartFormat.':I'.($rowNum))
                    ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);    
            
            $objPHPExcel->getActiveSheet()->getStyle('J'.$rowStartFormat.':J'.($rowNum))
                    ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);    
            
            $objPHPExcel->getActiveSheet()->getStyle('A'.$rowStartFormat.':'.MyFunctionCustom::columnName($index+11).($rowNum-1))
                ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

            //save file 
             $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
             //$objWriter->save('MyExcel.xslx');

             for($level=ob_get_level();$level>0;--$level)
             {
                     @ob_end_clean();
             }
             header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
             header('Pragma: public');
             header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
             header('Content-Disposition: attachment; filename="'.'CustomerOrders-'.date('d-m-Y').'.'.'xlsx'.'"');
             header('Cache-Control: max-age=0');				
             $objWriter->save('php://output');			
             Yii::app()->end();	            
            
        }
	
}