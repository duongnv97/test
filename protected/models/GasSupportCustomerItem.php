<?php

/**
 * This is the model class for table "{{_gas_support_customer_item}}".
 *
 * The followings are the available columns in table '{{_gas_support_customer_item}}':
 * @property string $id
 * @property string $support_customer_id
 * @property string $materials_id
 * @property integer $type
 */
class GasSupportCustomerItem extends CActiveRecord
{
    public $unit;
    public $percent_draft;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasSupportCustomerItem the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_support_customer_item}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, support_customer_id, materials_id, type', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rMaterials' => array(self::BELONGS_TO, 'GasMaterials', 'materials_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'support_customer_id' => 'Support Customer',
            'materials_id' => 'Item Name',
            'type' => 'Type',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
            $criteria=new CDbCriteria;

            $criteria->compare('t.id',$this->id,true);
            $criteria->compare('t.support_customer_id',$this->support_customer_id,true);
            $criteria->compare('t.materials_id',$this->materials_id,true);
            $criteria->compare('t.type',$this->type);

            return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
            ));
    }

    public function defaultScope()
    {
        return array();
    }

    /**
     * @Author: ANH DUNG Sep 17, 2015
     * @Todo: build string json của percent theo index
     * @param: $index ở đây là chỉ số của lần update 
     * $index có những giá trị sau
     * 1: tạo lần đầu
     * 2: trưởng phòng update
     * 3: GD KD
     * 4: Tổng GD
     */
    public function buildJsonPercent($index) {
        if($this->id){ // for update record
            $mOld = self::model()->findByPk($this->id);
            $percent = json_decode($mOld->percent, true);
            if(!is_array($percent)){
                $percent = array();
            }
            $percent[$index] = $this->percent;
        }else{// for new record
            $percent = array();
            $percent[$index] = $this->percent;
        }
        return json_encode($percent);
    }
    
    /**
     * @Author: ANH DUNG Sep 17, 2015
     * @Todo: get unit text
     */
    public function getUnit() {
        return "Need Update";
    }
    
    /**
     * @Author: ANH DUNG Sep 17, 2015
     * @Todo: get percent by index
     * @Param: $index
     */
    public function getPercent($index=1) {
        $percent = json_decode($this->percent, true);
        return isset($percent[$index]) ? $percent[$index] : "";
    }
    
    /**
     * @Author: ANH DUNG Sep 18, 2015
     * @Todo: get percent with % by index
     * @Param: $index
     */
    public function getPercentText($index=1) {
        $p = $this->getPercent($index);
        if($p != ""){
            return $p." %";
        }
        return "";
    }
    
    /**
     * @Author: ANH DUNG Sep 18, 2015
     * @Todo: get html td percent 2 - của trưởng phòng bò/mối
     * @param: $model is GasSupportCustomer
     */
    public function getHtmlPercentCol234($model, $form, $index, $aRoleCheck) {
        $html = "";
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        if($cUid != GasLeave::UID_DIRECTOR && $model->status >= GasSupportCustomer::STATUS_APPROVED_3 ){
            return $this->getPercentText($index);
            // Dec 19, 2016 nếu là anh Qúy update hoàn thành thì không hiện drop down % nữa, vì bị change theo form submit => sai
        }

        if(in_array($cRole, $aRoleCheck) ){
//        if(in_array($cRole, $aRoleCheck) ){
            // nghĩa là user TP bò/mối đang update status
            $html = $form->dropDownList($this,'percent[]', $model->getListOptionPercent(),array('class'=>'w-70 support_percent',
                         'options' => array($this->getPercent($index)=>array('selected'=>true))
                ));
            $html .= "<input name='i' value='$index' type='hidden'>";
        }else{
            // chỉ view text cho Các user khác đang update status, level 2, level 3
            $html = $this->getPercentText($index);
        }
        return $html;
    }
    
    /**
     * @Author: ANH DUNG Sep 18, 2015
     */
    public function getMaterialName() {
        $mMaterial = $this->rMaterials;
        if($mMaterial){
            $this->unit = $mMaterial->unit;
            return $mMaterial->name;
        }
        return '';
    }
    
    /**
     * @Author: ANH DUNG Sep 19, 2015
     * @Todo: thành tiền sau % hỗ trợ
     */
    public function AmountAfterPercentFinal() {
        $index = $this->getMaxIndexJson();
        $percent = $this->getPercent($index);
        if($percent==""){
            $percent = 0;
        }
        return $AmountDiscount = ($this->amount * (100-$percent) )/100;
    }
    
    /**
     * @Author: ANH DUNG Sep 28, 2015
     * @Todo: something lấy index mới nhất dc cập nhật
     */
    public function getMaxIndexJson() {
        $percent = json_decode($this->percent, true);
//        $index = count($percent);
        // phải chạy for vì mảng này có thể có 2 hoặc 3 phần tử, từ khi Sep 29, 2015 trưởng phòng bò,
        // giám đốc KD có thể tạo mới => nó phá vỡ logic cũ đi
        foreach($percent as $key=>$p){
            $index = $key;
        }
        return $index;
    }
    
    /** @Author: Pham Thanh Nghia 29/06/2018
     *  @Todo: sum amount by id_customer
     *  @Param:
     **/
    public function sumAmountBySupportCustomerId($id){
        $criteria=new CDbCriteria;
//       $criteria->select ='sum(amount) as amount';
        $criteria->compare('t.support_customer_id ' ,$id);
        
        $model = GasSupportCustomerItem::model()->findAll($criteria);
        $sum = 0;
        foreach ($model as $key => $value) {
            $sum += $value->amount;
        }
        return $sum;
    }
    
    public function sumAmountPercentBySupportCustomerId($id){ // tiền đầu tư
        $criteria=new CDbCriteria;
        $criteria->compare('t.support_customer_id',$id);
        $model = GasSupportCustomerItem::model()->findAll($criteria);
        $sum = 0;
        foreach ($model as $key => $item) {
                $percent = json_decode($item->percent, true);
                $index = 0;
                if(!empty($percent)){
                    foreach($percent as $key=>$p){
                        $index = $p;
                    }
                }
            $itemPercent =  ($item->amount * (100- $index))/100;
            
            
//            $itemPercent = $item->AmountAfterPercentFinal();
            $sum+= $itemPercent;
        }
        return $sum;
    }
    
}