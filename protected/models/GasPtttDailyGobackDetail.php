<?php

/**
 * This is the model class for table "{{_gas_pttt_daily_goback_detail}}".
 *
 * The followings are the available columns in table '{{_gas_pttt_daily_goback_detail}}':
 * @property string $id
 * @property string $pttt_daily_goback_id
 * @property string $agent_id
 * @property string $uid_login
 * @property string $maintain_date
 * @property string $monitoring_id
 * @property string $maintain_employee_id
 * @property integer $qty
 */
class GasPtttDailyGobackDetail extends CActiveRecord
{
    public $date_month;
    public $date_year;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasPtttDailyGobackDetail the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_pttt_daily_goback_detail}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, pttt_daily_goback_id, agent_id, uid_login, maintain_date, monitoring_id, maintain_employee_id, qty', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'pttt_daily_goback_id' => 'Pttt Daily Goback',
            'agent_id' => 'Agent',
            'uid_login' => 'Uid Login',
            'maintain_date' => 'Maintain Date',
            'monitoring_id' => 'Monitoring',
            'maintain_employee_id' => 'Maintain Employee',
            'qty' => 'Qty',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.pttt_daily_goback_id',$this->pttt_daily_goback_id,true);
        $criteria->compare('t.agent_id',$this->agent_id,true);
        $criteria->compare('t.uid_login',$this->uid_login,true);
        $criteria->compare('t.maintain_date',$this->maintain_date,true);
        $criteria->compare('t.monitoring_id',$this->monitoring_id,true);
        $criteria->compare('t.maintain_employee_id',$this->maintain_employee_id,true);
        $criteria->compare('t.qty',$this->qty);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    public function defaultScope()
    {
        return array(
                //'condition'=>'',
        );
    }
    
    /**
     * @Author: ANH DUNG Mar 15, 2015
     * @Todo: save detail 
     * @Param: $model model GasPtttDailyGoback
     */
    public static function SaveDetailQty($model) {
        MyFormat::deleteModelDetailByRootId('GasPtttDailyGobackDetail', $model->id, 'pttt_daily_goback_id');
        $aRowInsert=array();
        foreach( $model->aQty as $employee_id => $qty ){
            $employee_id = MyFormat::removeBadCharacters($employee_id);
            $qty = MyFormat::removeBadCharacters($qty);
            $aRowInsert[]="(
                    '$model->id',
                    '$model->agent_id',
                    '$model->uid_login',
                    '$model->maintain_date',
                    '$model->monitoring_id',
                    '$employee_id',
                    '$qty'
                    )";
        }
        
        $tableName = GasPtttDailyGobackDetail::model()->tableName();            
        $sql = "insert into $tableName (
                        pttt_daily_goback_id,
                        agent_id,
                        uid_login,
                        maintain_date,
                        monitoring_id,
                        maintain_employee_id,
                        qty
                        ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert))
            Yii::app()->db->createCommand($sql)->execute();
    }
    
}