<?php

/**
 * Class GasStoreCardReport
 * @author LocNV Sep 9, 2019
 */
class GasStoreCardReport extends GasStoreCard
{
    /** @Author: HOANG NAM 11/07/2018
     *  @Todo: Revenue output
     **/
    public function revenueOutputV2(){
        $aRes = [];
//        if(!empty($this->province_id_agent) && count($this->agent_id) == 0){
//            $aAgentId = $this->getAgentOfProvince($this->province_id_agent);
//            $this->agent_id = $aAgentId;
//        }// Close Jan0219

        // chỗ ROLE_AGENT không phải đổi qua ROLE_SUB_USER_AGENT. Xác nhận Apr 13, 2014
        $aAgent = Users::getArrObjectUserByRoleHaveOrder(ROLE_AGENT, 'id');// $aAgent dạng: array[id agent]=>model agent
        $this->getTotalOutputV2($aAgent, $aRes);
        $this->getMoneyBankAndTotalRevenueV2($aRes,$aAgent, 'MONEY_BANK');
        $this->getMoneyBankAndTotalRevenueV2($aRes,$aAgent, 'TOTAL_REVENUE');

        $_SESSION['data-excel'] = $aRes;
        return $aRes;
    }

    /** @Author: HOANG NAM 11/07/2018
     *  @Todo: get total output
     **/
    public function getTotalOutputV2($aAgent,&$aRes){
        // lặp 2 loại bình thống kê là 12 và 45kg
        foreach( CmsFormatter::$MATERIAL_TYPE_OUTPUT_STATISTIC as $type_output){
            // 1. get id những vật tư có loại là Gas 12 Kg OR 45kg
            $aMaterialId = GasMaterials::getArrayIdByMaterialTypeId($type_output['list_id']);
            $this->getOutputByMonthYearV2($aMaterialId, $aRes, $type_output, $aAgent);
            if($type_output['code']=='12KG'){
                // lấy chi tiết sản lượng cho từng loại của B12, hộ gd, bò, mối
                if(empty($this->statistic_month)){
                    $this->getOutputDetail12KgByYearV2($aMaterialId, $aRes, GasStoreCard::$TYPE_CUSTOMER[CUSTOMER_HO_GIA_DINH], $aAgent);
                    $this->getOutputDetail12KgByYearV2($aMaterialId, $aRes, GasStoreCard::$TYPE_CUSTOMER[STORE_CARD_KH_BINH_BO], $aAgent);
                    $this->getOutputDetail12KgByYearV2($aMaterialId, $aRes, GasStoreCard::$TYPE_CUSTOMER[STORE_CARD_KH_MOI], $aAgent);
                }else{
                    $this->getOutputByMonthYearDetail12KgV2($aMaterialId, $aRes, GasStoreCard::$TYPE_CUSTOMER[CUSTOMER_HO_GIA_DINH], $aAgent);
                    $this->getOutputByMonthYearDetail12KgV2($aMaterialId, $aRes, GasStoreCard::$TYPE_CUSTOMER[STORE_CARD_KH_BINH_BO], $aAgent);
                    $this->getOutputByMonthYearDetail12KgV2($aMaterialId, $aRes, GasStoreCard::$TYPE_CUSTOMER[STORE_CARD_KH_MOI], $aAgent);
                }
            }
        }
    }

    /** @Author: HOANG NAM 11/07/2018
     *  @Todo: get money bank and total revenue
     **/
    public function getMoneyBankAndTotalRevenueV2(&$aRes, $aAgent, $TYPE='MONEY_BANK'){
        return;// AnhDung Dec0318
        if(empty($this->statistic_month)) return;
        // Lấy số ngày trong tháng
        $days_in_month = cal_days_in_month(0, $this->statistic_month, $this->statistic_year) ;
        $aAgentSortMonth = array();
        $aAgentSortYear = array();

        $criteria = new CDBcriteria();
        $dateFrom = $this->statistic_year.'-'.$this->statistic_month.'-01';
        $dateTo = $this->statistic_year.'-'.$this->statistic_month.'-'.$days_in_month;
        $criteria->addBetweenCondition("t.release_date",$dateFrom,$dateTo);
//            $criteria->compare("t.release_date",$date);
        if($TYPE=='MONEY_BANK'){// chi nộp ngân hàng
            $sParamsIn = implode(',', CmsFormatter::$TYPE_MONEY_FROM_BANK);
            $criteria->addCondition("t.master_lookup_id IN ($sParamsIn)");
        }else{// TỔNG DOANH THU ĐẠI LÝ - trừ kh bình bò ra
            $sParamsIn = implode(',', CmsFormatter::$REVENUE_ITEM);
            $criteria->addCondition("t.master_lookup_id IN ($sParamsIn)");
            $sParamsIn = implode(',', CmsFormatter::$REVENUE_ITEM_EXCEPT_CUSTOMER);
            $criteria->addCondition("t.type_customer NOT IN ($sParamsIn)");
        }
        $sParamsIn = implode(',', $this->agent_id);
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.agent_id IN ($sParamsIn)");
        }

        $criteria->select = "t.agent_id, sum(amount) as amount,t.release_date";
        $criteria->group = "t.agent_id,t.release_date";
        $mRes = GasCashBookDetail::model()->findAll($criteria);

        if(count($mRes)>0){

            if(!isset($aRes['month'][$this->statistic_month]))
                $aRes['month'][$this->statistic_month] = 1; // biến $this->statistic_month dùng để foreach nên chỉ cần gán =1 là dc
            foreach($mRes as $item){
                $i = date('d',strtotime($item->release_date));
                //  for tab each month
                // one cell
                $aRes[$TYPE][$this->statistic_month][$i][$item->agent_id] = $item->amount;
                // like : $aRes[12KG]['total_day'][08][agent 102]
                //sum_row -- dùng kiểu này vì sort dc theo tỉnh
                if(!isset($aAgentSortMonth[$aAgent[$item->agent_id]->province_id][$item->agent_id]))
                    $aAgentSortMonth[$aAgent[$item->agent_id]->province_id][$item->agent_id] = $item->amount;
                else
                    $aAgentSortMonth[$aAgent[$item->agent_id]->province_id][$item->agent_id] += $item->amount;

                if(!isset($aRes[$TYPE]['sum_col_total_month_all_agent'][$this->statistic_month]))
                    $aRes[$TYPE]['sum_col_total_month_all_agent'][$this->statistic_month] = $item->amount;
                else
                    $aRes[$TYPE]['sum_col_total_month_all_agent'][$this->statistic_month] += $item->amount;
                // need sum column...here, nên sum luôn column ở đây, xuống view khỏi tạo biến để sum nữa
                // sum column one day for all agent
                if(!isset($aRes[$TYPE]['sum_col'][$this->statistic_month][$i]))
                    $aRes[$TYPE]['sum_col'][$this->statistic_month][$i] = $item->amount;
                else
                    $aRes[$TYPE]['sum_col'][$this->statistic_month][$i] += $item->amount;

                //  for tab each month

                //  for tab sum year
                // one cell
                if(!isset($aRes[$TYPE]['total_year'][$this->statistic_month][$item->agent_id]))
                    $aRes[$TYPE]['total_year'][$this->statistic_month][$item->agent_id] = $item->amount;
                else
                    $aRes[$TYPE]['total_year'][$this->statistic_month][$item->agent_id] += $item->amount;

                //sum_row
                if(!isset($aAgentSortYear[$aAgent[$item->agent_id]->province_id][$item->agent_id]))
                    $aAgentSortYear[$aAgent[$item->agent_id]->province_id][$item->agent_id] = $item->amount;
                else
                    $aAgentSortYear[$aAgent[$item->agent_id]->province_id][$item->agent_id] += $item->amount;

                // need sum column...here, nên sum luôn column ở đây, xuống view khỏi tạo biến để sum nữa
                // sum column one day for all agent
                if(!isset($aRes[$TYPE]['total_year']['sum_col'][$this->statistic_month]))
                    $aRes[$TYPE]['total_year']['sum_col'][$this->statistic_month] = $item->amount;
                else
                    $aRes[$TYPE]['total_year']['sum_col'][$this->statistic_month] += $item->amount;

                if(!isset($aRes[$TYPE]['sum_col_total_year_all_agent']))
                    $aRes[$TYPE]['sum_col_total_year_all_agent'] = $item->amount;
                else
                    $aRes[$TYPE]['sum_col_total_year_all_agent'] += $item->amount;

                //  for tab sum year
            }
            for($i=1;$i<=$days_in_month; $i++){
                if($i<10)
                    $i='0'.$i;
                if(!empty($aRes[$TYPE][$this->statistic_month][$i])){
                    if(empty($aRes[$TYPE]['days'][$this->statistic_month][$i])){
                        $aRes[$TYPE]['days'][$this->statistic_month][$i] = $i;
                    }
                }
            }
        }
        // sort each month by tỉnh
        if(count($aAgentSortMonth)){
            foreach($aAgentSortMonth as $province_id=>$arrayAgentTotalMonth){
                arsort($arrayAgentTotalMonth);
                if(!isset($aRes[$TYPE]['sum_row_total_month'][$this->statistic_month]))
                    $aRes[$TYPE]['sum_row_total_month'][$this->statistic_month] = $arrayAgentTotalMonth;
                else
                    $aRes[$TYPE]['sum_row_total_month'][$this->statistic_month] += $arrayAgentTotalMonth;
            }
        }

        // sort year by tỉnh
        if(count($aAgentSortYear)){
            foreach($aAgentSortYear as $province_id=>$arrayAgentTotalMonth){
                arsort($arrayAgentTotalMonth);
                if(!isset($aRes[$TYPE]['total_year']['sum_row']))
                    $aRes[$TYPE]['total_year']['sum_row'] = $arrayAgentTotalMonth;
                else
                    $aRes[$TYPE]['total_year']['sum_row'] += $arrayAgentTotalMonth;
            }
        }
    }
}