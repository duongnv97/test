<?php

/**
 * This is the model class for table "{{_call_temp}}".
 *
 * The followings are the available columns in table '{{_call_temp}}':
 * @property string $id
 * @property string $call_uuid
 * @property string $call_uuid_extension
 * @property integer $extension
 * @property string $user_id
 * @property string $agent_id
 * @property integer $province_id
 * @property integer $direction
 * @property integer $call_status
 * @property string $start_time
 * @property string $answer_time
 * @property string $end_time
 * @property integer $total_duration
 * @property integer $bill_duration
 * @property string $created_date
 */
class CallTempHistory extends BaseSpj
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CallTemp the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_call_temp_history}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, call_uuid, call_uuid_extension, extension, user_id, agent_id, province_id, direction, call_status, start_time, answer_time, end_time, total_duration, bill_duration, created_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                'id' => 'ID',
                'call_uuid' => 'Call Uuid',
                'call_uuid_extension' => 'Call Uuid Extension',
                'extension' => 'Extension',
                'user_id' => 'User',
                'agent_id' => 'Agent',
                'province_id' => 'Province',
                'direction' => 'Direction',
                'call_status' => 'Call Status',
                'start_time' => 'Start Time',
                'answer_time' => 'Answer Time',
                'end_time' => 'End Time',
                'total_duration' => 'Total Duration',
                'bill_duration' => 'Bill Duration',
                'created_date' => 'Created Date',
            );
    }

    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.call_uuid',$this->call_uuid,true);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
}