<?php

/**
 * This is the model class for table "{{_referral_tracking}}".
 *
 * The followings are the available columns in table '{{_referral_tracking}}':
 * @property integer $id
 * @property integer $invited_id	
 * @property string $ref_id
 * @property string $status
 * @property integer $created_date
 */
class ReportReferralTracking extends BaseSpj {
    
    public $MAX_LEVEL     = 3;
    
    public $ref_date = false;
    public $showTable = true; // có hiển thị table thống kê bangr cấp độ hay không
    public $tableLevel = 1; // thống kê bảng cấp độ: 0,1,2,3,..
    const REF_STATUS_NEW                    = 0;
    const REF_STATUS_CONFIRM                = 1;
    public $countLevel = 2; // 0,1,2,3,... admin/appPromotionUser/ReportReferralTrackingTable
    public $htmlTree = '', $htmlTable = '', $autocomplete_name,$code_no,$date_from,$date_to,$type_search,$autocomplete_agent;
    public $type = 'horizontal'; //vertical
    public $backg_round_confirm = '#00c2ff54';
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{_referral_tracking}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('id,invited_id,code_no,ref_id,status,created_date,type,type_search', 'safe'),
            array('date_from,date_to,agent_id', 'safe'),
        );
    }

    public function relations() {
        return array(
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'ref_id'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'invited_id' => 'Người dùng',
            'ref_id' => 'Người giới thiệu',
            'code_no' => 'Mã giới thiệu',
            'type' => 'Hiển thị',
            'date_from' => 'Từ ngày',
            'date_to' => 'Đến ngày',
        );
    }

    public function search() {
        $criteria = new CDbCriteria;
        if (!empty($this->invited_id)) {
            $criteria->addCondition("t.invited_id = '$this->invited_id'");
        }
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    
    /** @Author: ANH DUNG May 31, 2018
     *  @Todo: gom các hàm lấy dữ liệu Tree của người giới thiệu vào 1
     **/
    public function getDataTree() {
        $aData              = $this->getTreeReferralTrackingStepByStep();
        $dataView           = $this->getHtmlReportTree($aData);
        $this->htmlTree    = $dataView['tree'];
        $this->htmlTable   = $dataView['table'];
    }
    
    /** @Author: HOANG NAM 11/05/2018
     *  @Todo: get array tree. Get by step
     **/
    public function getTreeReferralTrackingStepByStep() {
        $aEmployee = [];
        $aData = [];
        if(empty($this->ref_id) && empty($this->code_no)) { 
//            $aEmployee = $this->getListReferralTracking();
            return [];
        }else{
//            nhận 1 id search ưu tiên lượt từ trên xuống dưới
            if(!empty($this->ref_id)) {
                $aEmployee[] = $this->ref_id;
            }elseif(!empty($this->code_no)) {
                $criteriaAppPromotion = new CDbCriteria;
                $criteriaAppPromotion->addCondition("t.code_no = '{$this->code_no}'");
                $mAppPromotion = AppPromotion::model()->find($criteriaAppPromotion);
                if(!empty($mAppPromotion)){
                    $aEmployee[] = $mAppPromotion->owner_id;
                }
            }
        }
        $aReferralTracking=[];
//        [TASKREF]
        $this->getDataRef($aReferralTracking,$aEmployee);
        
//        foreach ($aEmployee as $key => $idUser) {
////          Lấy danh sách model cần sắp xếp vào cây
//            $this->getTreeStepByStepByIdRef($aReferralTracking,$idUser);
//        }
        
        $this->setTreeRef($aData,$aReferralTracking);
        return $aData;
    }
    
    /** @Author: HOANG NAM 11/05/2018
     *  @Todo: get all tree by idRef
     **/
    public function getTreeStepByStepByIdRef(&$aReferralTracking,$idRef) {
        $criteria = new CDbCriteria;
        $criteria->addCondition("t.ref_id = {$idRef}");
//        add date created
        if($this->ref_date){
            $strFile = 't.created_date';
            switch ($this->type_search){
                case self::SEARCH_TYPE_CREATED_DATE:
                    $strFile = 't.created_date';
                    break;
                case self::SEARCH_TYPE_ORDER_DATE:
                    $criteria->addCondition('t.date_order IS NOT NULL');
                    $strFile = 't.date_order';
                    break;
                default:
                    $strFile = 't.created_date';
                    break;
            }
            $strFile = 't.created_date';
            if(!empty($this->date_from)) {
                $criteria->addCondition("DATE($strFile) >= \"".MyFormat::dateConverDmyToYmd($this->date_from, '-').'"');
            }
            if(!empty($this->date_to)) {
                $criteria->addCondition("DATE($strFile) <= \"".MyFormat::dateConverDmyToYmd($this->date_to, '-').'"');
            }
        }
        $aModel = ReportReferralTracking::model()->findAll($criteria);
        if(!empty($aModel)){
            $aReferralTracking = array_merge($aReferralTracking,$aModel) ;
            foreach ($aModel as $key => $value) {
                $this->getTreeStepByStepByIdRef($aReferralTracking,$value->invited_id);
            }
        }
        
    }
    
    /** @Author: HOANG NAM 10/05/2018
     *  @Todo: get array tree. Get all after find
     **/
    public function getTreeReferralTracking() {
//        if(empty($this->ref_id) && empty($this->code_no)) { 
//            return [];
//        }
        $aData = [];
        $criteria = new CDbCriteria;
        $aReferralTracking = ReportReferralTracking::model()->findAll($criteria);
        $this->setTreeRef($aData,$aReferralTracking);
        if(!empty($this->ref_id)) {
            $aData = $this->getTreeByIdRef($aData,$this->ref_id);
        }
        if(!empty($this->code_no)) {
            $criteriaAppPromotion = new CDbCriteria;
            $criteriaAppPromotion->addCondition("t.code_no = '{$this->code_no}'");
            $mAppPromotion = AppPromotion::model()->find($criteriaAppPromotion);
            if(empty($mAppPromotion)){
                $aData = null;
            }else{
                $idUser = $mAppPromotion->owner_id;
                $aData = $this->getTreeByIdRef($aData,$idUser);
            }
            
        }
        return $aData;
    }
    
    /** @Author: HOANG NAM 10/05/2018
     *  @Todo: get array tree
     **/
    public function setTreeRef(&$aData,$aReferralTracking) {
        if(count($aReferralTracking) > 0 && count($aData) <= 0) {
            $aIdUser = [];
            foreach ($aReferralTracking as $key => $mReferralTracking) {
                $this->setConfirmByDate($mReferralTracking);//Set confirm = REF_STATUS_NEW or REF_STATUS_CONFIRM by date
                $aIdUser[] = $mReferralTracking->ref_id;
                $aIdUser[] = $mReferralTracking->invited_id;
                if(!$this->InsertRef($aData, $mReferralTracking)){
                    $aData[$mReferralTracking->ref_id]  =   array(  $mReferralTracking->invited_id => array(
                                                                        'confirm' => $mReferralTracking->status
                                                            ));
                }
            }
            
            $_SESSION['data-id-users'] = $aIdUser;
        }
    }
    
    /** @Author: HOANG NAM 11/05/2018
     *  @Todo: insert to array if exists key is model->ref_id
     *  @Return: true if insert else false
     **/
    public function InsertRef(&$aData,$mReferralTracking){
        foreach ($aData as $keyData => $valueData) {
            if($keyData === 'confirm'){
                continue;
            }
            if($keyData == $mReferralTracking->invited_id){
                $aData[$mReferralTracking->ref_id][$keyData]['confirm'] = $mReferralTracking->status;
                $aData[$mReferralTracking->ref_id][$keyData] = $aData[$keyData];
                unset($aData[$keyData]);
                return true;
            }
            if($keyData == $mReferralTracking->ref_id){
                $aData[$keyData] =  array_replace($aData[$keyData],
                                        array($mReferralTracking->invited_id => array(
                                            'confirm' => $mReferralTracking->status
                                        )
                                    ));
                return true;
            }
            if(array_key_exists($mReferralTracking->ref_id,$aData[$keyData])){
                $aData[$keyData][$mReferralTracking->ref_id] =  array_replace($aData[$keyData][$mReferralTracking->ref_id],
                                        array($mReferralTracking->invited_id => array(
                                            'confirm' => $mReferralTracking->status
                                        )
                                    ));
                return true;
            }
            
            if($this->InsertRef($aData[$keyData],$mReferralTracking)){
                return true;
            }
        }
        return false;
    }
    
    /** @Author: HOANG NAM 11/05/2018
     *  @Todo: generate html
     **/
    public function getHtmlReportTree($aData){
        $htmlResult = [];
        $htmlResult['tree'] = '';
        $htmlResult['table'] = '';
        
        if(count($aData) <= 0){
            return $htmlResult;
        }
        $aValueOfUser = [];
        if(isset($_SESSION['data-id-users'])){
            $aDataId = $_SESSION['data-id-users'];
            $criteria = new CDbCriteria;
            $stringSearch = implode(',',$aDataId);
            $criteria->addCondition("t.id IN ($stringSearch)");
            $aUser = Users::model()->findAll($criteria);
            foreach ($aUser as $key => $mUser) {
                $aValueOfUser[$mUser->id] = $mUser->getAttributes();
            }
        }
//        Tính toán lấy tổng users nhập mã từ danh sách nhân viên
        $countUsers = 0;
        $countUsersConfirm = 0;
        foreach ($aData as $keyArr => $valueArr) {
            $countUsers += $this->getCountChild($aData[$keyArr]);
            $countUsersConfirm += $this->getCountConfirm($aData[$keyArr]);
        }
        $session = Yii::app()->session;
        $session['Gas24hRefSummary'] = "Tổng nhập mã: {$countUsers}, Đã mua hàng: {$countUsersConfirm}";
        
        $this->getHtmlUl($htmlResult['tree'],$aData,$aValueOfUser);
        $this->getHtmlTable($htmlResult['table'],$aData,$aValueOfUser,$this->tableLevel);
        return $htmlResult;
    }
    
    /** @Author: HOANG NAM 11/05/2018
     *  @Todo: get ul-li of array
     **/
    public function getHtmlUl(&$html,$aData,$aUser){
        if(count($aData) > 0 && (count($aData) > 1 || !isset($aData['confirm']))){
            $html.= '<ul>';
            foreach ($aData as $key => $value) {
                if($key !== 'confirm'){
                    $strShow = isset($aUser[$key]) ? $aUser[$key]['first_name'].'<br>'.$aUser[$key]['phone'] : $key;
                    $countChild = $this->getCountChild($aData[$key]);
                    $countConfirm = $this->getCountConfirm($aData[$key]);
                    $strShow .= $countChild > 0 ? "<br>[Tổng: {$countChild}, Đã mua hàng: {$countConfirm}] ": '';
                    $html .= '<li>';
                    $background = (isset($aData[$key]['confirm']) && $aData[$key]['confirm'] == self::REF_STATUS_CONFIRM ) ? 'background-color: '.$this->backg_round_confirm.';' : '';
                    $html .= '<a style = "'.$background.'">'.$strShow.'</a>';
                    $this->getHtmlUl($html,$aData[$key],$aUser);
                    $html .= '</li>';
                }
            }
            $html.= '</ul>';
        }
        
    }
    
    /** @Author: HOANG NAM 11/05/2018
     *  @Todo: get ul-li of array
     **/
    public function getTreeByIdRef($aData,$ref_id){
        foreach ($aData as $key => $value) {
            if($key == $ref_id){
                return array(
                    $key => $value
                );
            }
            $resultTree = $this->getTreeByIdRef($aData[$key],$ref_id);
            if(!empty($resultTree)){
                return $resultTree;
            }
        }
        return null;
    }
    
    /** @Author: HOANG NAM 12/05/2018
     *  @Todo: get count child of note
     **/
    public function getCountChild($aData){
        $count = 0;
        foreach ($aData as $key => $value) {
            if($key !== 'confirm'){
                $count++;
                $count += $this->getCountChild($aData[$key]);
            }
        }
        return $count;
    }
    
    public function getCountConfirm($aData){
        $count = 0;
        foreach ($aData as $key => $value) {
            if($key !== 'confirm'){
                if(isset($aData[$key]['confirm']) && $aData[$key]['confirm'] == self::REF_STATUS_CONFIRM){
                    $count++;
                }
                $count += $this->getCountConfirm($aData[$key]);
            }
        }
        return $count;
    }
    
    /** @Author: HOANG NAM 14/05/2018
     *  @Todo: get report table
     **/
    public function getReportTable() {
        $aReport = $this->getTreeReferralTrackingStepByStep();
        $aValueOfUser = [];
        $aPromotion = [];
        if(isset($_SESSION['data-id-users'])){
            $aDataId = $_SESSION['data-id-users'];
            $criteria = new CDbCriteria;
            $stringSearch = implode(',',$aDataId);
            $criteria->addCondition("t.id IN ($stringSearch)");
            $aUser = Users::model()->findAll($criteria);
            foreach ($aUser as $key => $mUser) {
                $aValueOfUser[$mUser->id] = $mUser->getAttributes();
            }
            
            $criteriaAppPromotion = new CDbCriteria;
            $stringSearch = implode(',',$aDataId);
            $criteriaAppPromotion->addCondition("t.owner_id IN ($stringSearch)");
            $aPromotions = AppPromotion::model()->findAll($criteriaAppPromotion);
            foreach ($aPromotions as $key => $mPromotion) {
                $aPromotion[$mPromotion->owner_id] = $mPromotion->getAttributes();
            }
        }
        $aResult = [];
        foreach ($aReport as $key => $value) {
            if($key === 'confirm') {continue;}
            $aResult[$key]['code_no'] = isset($aPromotion[$key]) ? $aPromotion[$key]['code_no']: 'Không rõ';
            $aResult[$key]['name'] = isset($aValueOfUser[$key]) ? $aValueOfUser[$key]['first_name']: 'Không rõ';
            $aResult[$key]['phone'] = isset($aValueOfUser[$key]) ? $aValueOfUser[$key]['phone']: 'Không rõ';
            $aResult[$key]['sum'] = $this->getCountChild($aReport[$key]);
            $this->getArrayView($aResult[$key],$aReport[$key],1);
        }
        return $aResult;
    }
    
    /** @Author: HOANG NAM 14/05/2018
     *  @Todo: get table search
     **/
    public function getArrayView(&$aResult,$aReport,$level) {
//        Trừ 1 vì có 1 phần tử là 'confirm' => $this->status;
        $count = count($aReport);
        if(isset($aReport['confirm'])){ 
            $count--; 
        }
        $aResult[$level] = isset($aResult[$level]) ? $aResult[$level] + $count : $count;
        $level++;
        if($level <= $this->countLevel){
            foreach ($aReport as $key => $value) {
                if($key == 'confirm'){
                    continue;
                }
                $this->getArrayView($aResult,$aReport[$key],$level);
            }
        }
    }
    /** @Author: HOANG NAM 12/05/2018
     *  @Todo: get list users appPromotionReferralTracking
     **/
    public function getListReferralTracking(){
        $aId = [1014257,146
            ];
        return $aId;
    }
    
    /** @Author: HOANG NAM 21/05/2018
     *  @Todo: get list type view
     **/
    public function getArrayStyleView() {
        return array(
            'horizontal' => 'Chiều ngang',
            'vertical' => 'Chiều dọc'
        );
    }
    
    /** @Author: HOANG NAM 23/05/2018
     *  @Todo: set confirm by date
     **/
    public function setConfirmByDate($mReportReferralTracking) {
        if(empty($mReportReferralTracking->date_order)) {
            $mReportReferralTracking->status = self::REF_STATUS_NEW;
        }else {
            $mReportReferralTracking->status = self::REF_STATUS_CONFIRM;
        }
        if(!empty($this->date_from) && !empty($mReportReferralTracking->date_order)) {
            if(strtotime(MyFormat::dateConverDmyToYmd($this->date_from, '-')) > strtotime($mReportReferralTracking->date_order)) {
                $mReportReferralTracking->status = self::REF_STATUS_NEW;
            }
        }
        if(!empty($this->date_to) && !empty($mReportReferralTracking->date_order)) {
            if(strtotime(MyFormat::dateConverDmyToYmd($this->date_to, '-')) < strtotime($mReportReferralTracking->date_order)) {
                $mReportReferralTracking->status = self::REF_STATUS_NEW;
            }
        }
    }
    
    /** @Author: HOANG NAM 31/05/2018
     *  @Todo: get table view 
     **/
    public function getHtmlTable(&$html,$aData,$aUser,$level){
        if($level == 0){
            $html[0]['name'] = 'Tổng quát';
            $this->getArrTable($html[0]['value'],$aData,$aUser,$level);
        }else{
            foreach ($aData as $key => $value) {
                if($key !== 'confirm'){
                    $html[$key]['name'] = isset($aUser[$key]) ? $aUser[$key]['first_name'] : $key;
                    $this->getArrTable($html[$key]['value'],$aData[$key],$aUser,$level);
                }
                uasort($html[$key]['value'], function ($item1, $item2) {
                    return $item2['countConfirm'] >= $item1['countConfirm'];
                });
            }
        }
    }
    
    public function getArrTable(&$html,$aData,$aUser,$level){
        foreach ($aData as $key => $value) {
            if($key !== 'confirm'){
                if($level <= 1){
                    $html[$key] = array(
                        'name' => isset($aUser[$key]) ? $aUser[$key]['first_name'] : $key,
                        'phone' => isset($aUser[$key]) ? $aUser[$key]['phone'] : '',
                        'countChild' => $this->getCountChild($aData[$key]),
                        'countConfirm' => $this->getCountConfirm($aData[$key]),
                    );
                }else{
                    $this->getArrTable($html,$aData[$key],$aUser,$level-1);
                }
            }
        }
    }
    const SEARCH_TYPE_CREATED_DATE  = 1;
    const SEARCH_TYPE_ORDER_DATE  = 2;

    /** @Author: NamNH Oct 03, 2018
     *  @Todo: get array search created_date or date_order
     **/
    public function getArraySearchRef(){
        return [
            self::SEARCH_TYPE_CREATED_DATE    =>'Mã code',
            self::SEARCH_TYPE_ORDER_DATE      =>'BQV',
        ];
    }
    
    /** @Author: DuongNV Oct 16, 2018
     *  @Todo: get report data
     **/
    public function getReportReferalTracking() {
        if(empty($this->date_from) || empty($this->date_to) 
            || (empty($this->code_no) && empty($this->ref_id))) return [];
        $aData = [];
        $aInvited = $this->getListInvitedId();
        if(empty($aInvited)) return [];
        $ref_id = empty($this->ref_id) ? $this->getUserByCodeNo() : $this->ref_id;
        $aData['DATA_REF'] = Users::model()->findByPk($ref_id);

        $aData['DATA_INVITED'] = Users::getArrObjectUserByRole('', $aInvited);
        $criteria = new CDbCriteria;
        $stringSearch = implode(',',$aInvited);
        $criteria->addCondition("t.owner_id IN ($stringSearch)");
        $aPromotions = AppPromotion::model()->findAll($criteria);
        foreach ($aPromotions as $key => $mPromotion) {
            $aData['DATA_INVITED'][$mPromotion->owner_id]->code_account = $mPromotion->code_no;
        }
        
        $from  = MyFormat::dateConverDmyToYmd($this->date_from, '-');
        $to    = MyFormat::dateConverDmyToYmd($this->date_to, '-');
        $aData['DATA_DATE']    = MyFormat::getArrayDay($from, $to);
        
        $this->getArrayInputCode($aData);
        $this->getArrayOrder($aData);
        
        uksort($aData['DATA_INVITED'], function($a, $b) use ($aData){
            $sumA = isset($aData['SUM_COL_CODE'][$a]) ? $aData['SUM_COL_CODE'][$a] : 0;
            $sumB = isset($aData['SUM_COL_CODE'][$b]) ? $aData['SUM_COL_CODE'][$b] : 0;
            if($this->type_search == self::SEARCH_TYPE_ORDER_DATE){
                $sumA = isset($aData['SUM_COL_ORDER'][$a]) ? $aData['SUM_COL_ORDER'][$a] : 0;
                $sumB = isset($aData['SUM_COL_ORDER'][$b]) ? $aData['SUM_COL_ORDER'][$b] : 0;
            }
            if($sumA == $sumB) return 0;
            return ($sumA < $sumB) ? 1 : -1;
        });
        $_SESSION['data-excel'] = $aData;
        $_SESSION['data-model'] = $this;
        return $aData;
    }
    
    /** @Author: DuongNV Oct 16, 2018
     *  @Todo: get list invited id by ref_id
     **/
    public function getListInvitedId() {
        $ref_id = empty($this->ref_id) ? $this->getUserByCodeNo() : $this->ref_id;
        if(empty($ref_id)) return [];
        $criteria = new CDbCriteria;
        $criteria->compare('t.ref_id', $ref_id);
        $criteria->compare('t.agent_id', $this->agent_id);
//        if(!empty($this->date_from)){
//            $from = MyFormat::dateConverDmyToYmd($this->date_from, '-');
//            $criteria->addCondition('DATE(t.created_date) >= "' . $from . '"');
//        }
//        if(!empty($this->date_to)){
//            $to = MyFormat::dateConverDmyToYmd($this->date_to, '-');
//            $criteria->addCondition('DATE(t.created_date) <= "' . $to . '"');
//        }// Oct1818 Anh Dung close - chỗ này ko giới hạn date
        $aModel = ReportReferralTracking::model()->findAll($criteria);
        $aRes = [];
        foreach ($aModel as $item) {
            $aRes[] = $item->invited_id;
        }
        return $aRes;
    }
    
    /** @Author: DuongNV Oct 16, 2018
     *  @Todo: get id owner by code_no
     **/
    public function getUserByCodeNo() {
        if(empty($this->code_no)) return '';
        $criteria = new CDbCriteria;
        $criteria->compare('t.code_no', $this->code_no);
        $aAppPromotion = AppPromotion::model()->find($criteria);
        return empty($aAppPromotion) ? '' : $aAppPromotion->owner_id;
    }
    
    /** @Author: DuongNV Oct 16, 2018
     *  @Todo: get array user nhập mã
     **/
    public function getArrayInputCode(&$aData) {
        $criteria = new CDbCriteria;
        $criteria->select = 'COUNT(t.id) as id, t.ref_id, DATE(t.created_date) as created_date';
        $this->reportCodeAgentCondition($criteria);
        if(!empty($this->date_from)){
            $from = MyFormat::dateConverDmyToYmd($this->date_from, '-');
            $criteria->addCondition('DATE(t.created_date) >= "' . $from . '"');
        }
        if(!empty($this->date_to)){
            $to = MyFormat::dateConverDmyToYmd($this->date_to, '-');
            $criteria->addCondition('DATE(t.created_date) <= "' . $to . '"');
        }
        $criteria->group = 't.ref_id, DATE(t.created_date)';
        $aModel = ReportReferralTracking::model()->findAll($criteria);
        if(!empty($aModel)){
            foreach($aModel as $mTracking){
                $aData['DATA_INPUT_CODE'][$mTracking->ref_id][$mTracking->created_date] = $mTracking->id;
                $aData['SUM_COL_CODE'][$mTracking->ref_id]       = isset($aData['SUM_COL_CODE'][$mTracking->ref_id]) ? $aData['SUM_COL_CODE'][$mTracking->ref_id]+$mTracking->id : $mTracking->id;
                $aData['SUM_ROW_CODE'][$mTracking->created_date] = isset($aData['SUM_ROW_CODE'][$mTracking->created_date]) ? $aData['SUM_ROW_CODE'][$mTracking->created_date]+$mTracking->id : $mTracking->id;
            }
        }
    }
    
    /** @Author: ANH DUNG Oct 18, 2018
     *  @Todo: get same condition
     **/
    public function reportCodeAgentCondition(&$criteria) {
        $listInvitedId = $this->getListInvitedId();
        $strSearchRef = implode(',', $listInvitedId);
        $criteria->addCondition('t.ref_id IN (' . $strSearchRef . ')');
        $criteria->compare('t.agent_id', $this->agent_id);
        
    }
    
    /** @Author: DuongNV Oct 16, 2018
     *  @Todo: get array user nhập mã và đã mua hàng
     **/
//    public function getArrayOrder(&$aData) {
//        $criteria = new CDbCriteria;
//        $criteria->select = 'COUNT(t.id) as id, t.ref_id, t.date_order';
//        $this->reportCodeAgentCondition($criteria);
//        $criteria->addCondition('t.date_order is not null');
//        if(!empty($this->date_from)){
//            $from = MyFormat::dateConverDmyToYmd($this->date_from, '-');
//            $criteria->addCondition('DATE(t.date_order) >= "' . $from . '"');
//        }
//        if(!empty($this->date_to)){
//            $to = MyFormat::dateConverDmyToYmd($this->date_to, '-');
//            $criteria->addCondition('DATE(t.date_order) <= "' . $to . '"');
//        }
//        $criteria->group = 't.ref_id, date_order';
//        $aModel = ReportReferralTracking::model()->findAll($criteria);
//        if(!empty($aModel)){
//            foreach($aModel as $mTracking){
//                $aData['DATA_ORDER'][$mTracking->ref_id][$mTracking->date_order] = $mTracking->id;
//                $aData['SUM_COL_ORDER'][$mTracking->ref_id]       = isset($aData['SUM_COL_ORDER'][$mTracking->ref_id]) ? $aData['SUM_COL_ORDER'][$mTracking->ref_id]+$mTracking->id : $mTracking->id;
//                $aData['SUM_ROW_ORDER'][$mTracking->date_order]   = isset($aData['SUM_ROW_ORDER'][$mTracking->date_order]) ? $aData['SUM_ROW_ORDER'][$mTracking->date_order]+$mTracking->id : $mTracking->id;
//            }
//        }
//    }
    
    /** @Author: NamNH Dec 04, 2018
     *  @Todo: custom array user nhập mã và đã mua hàng
     **/
    public function getArrayOrder(&$aData) {
        if(!is_array($aData['DATA_INVITED'])){
            return;
        }
        $mTelesale  = new Telesale();
        $mTelesale->date_from = $this->date_from;
        $mTelesale->date_to     = $this->date_to;
        $aRes      = [];
        $aCustomer = is_array($aData['DATA_INVITED']) ? CHtml::listData($aData['DATA_INVITED'], 'id', 'first_name'): [];
        $mTelesale->getGasSellReport($aRes, $aCustomer);
        if(is_array($aRes['OUT_DATE_SELL'])){
            $aData['DATA_ORDER'] = $aRes['OUT_DATE_SELL'];
            foreach ($aRes['OUT_DATE_SELL'] as $user_id => $aDate) {
                $aData['SUM_COL_ORDER'][$user_id]       = array_sum($aDate);
            }
        }
        if(is_array($aRes['OUT_DATE_ROW_SELL'])){
            $aData['SUM_ROW_ORDER']  = $aRes['OUT_DATE_ROW_SELL'];
        }
    }
    
    /** @Author: NamNH Oct 31, 2018
     *  @Todo: get Data from 1 to MAX_LEVEL
     **/
    public function getDataRef(&$aReferralTracking,$aEmployee,$level = 1){
        if($level > $this->MAX_LEVEL || count($aEmployee) <= 0){
            return;
        }
        $criteria = new CDbCriteria;
        $strIdRef = implode(',', $aEmployee);
        $criteria->addCondition("t.ref_id IN ({$strIdRef})");
//        add date created
        $this->setSort($criteria);
        $aModel = ReportReferralTracking::model()->findAll($criteria);
        if(!empty($aModel)){
            $aReferralTracking = array_merge($aReferralTracking,$aModel) ;
            $aUsers            = CHtml::listData($aModel, 'invited_id', 'invited_id');
            $this->getDataRef($aReferralTracking,$aUsers,$level+1);
        }
    }
    
    /** @Author: NamNH Oct 31, 2018
     *  @Todo: set sort
     **/
    public function setSort(&$criteria){
        if($this->ref_date){
            $strFile = 't.created_date';
            switch ($this->type_search){
                case self::SEARCH_TYPE_CREATED_DATE:
                    $strFile = 't.created_date';
                    break;
                case self::SEARCH_TYPE_ORDER_DATE:
                    $criteria->addCondition('t.date_order IS NOT NULL');
                    $strFile = 't.date_order';
                    break;
                default:
                    $strFile = 't.created_date';
                    break;
            }
            if(!empty($this->date_from)) {
                $criteria->addCondition("DATE($strFile) >= \"".MyFormat::dateConverDmyToYmd($this->date_from, '-').'"');
            }
            if(!empty($this->date_to)) {
                $criteria->addCondition("DATE($strFile) <= \"".MyFormat::dateConverDmyToYmd($this->date_to, '-').'"');
            }
        }
    }
    
}
