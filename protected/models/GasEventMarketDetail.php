<?php

/**
 * This is the model class for table "{{_event_market_detail}}".
 *
 * The followings are the available columns in table '{{_event_market_detail}}':
 * @property integer $id
 * @property integer $type
 * @property integer $event_market_id
 * @property integer $employee_id // TYPE_MATERIAL_EXPORT / TYPE_MATERIAL_IMPORT => Vật tư
 * @property integer $status // TYPE_MATERIAL_EXPORT / TYPE_MATERIAL_IMPORT => Số lượng
 * @property string $created_date
 */
class GasEventMarketDetail extends BaseSpj
{
    const TYPE_EMPLOYEE             = 1;// duyệt nhân viên
    const TYPE_MATERIAL_EXPORT      = 2;// danh sách vật tư xuất kho
    const TYPE_MATERIAL_IMPORT      = 3;// danh sách vật tư nhập kho
    
    const STATUS_NO_ACCEPT          = 0; //huy thuong
    const STATUS_ACCEPT             = 1; //thuong
    
    public $autocomplete_material;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasEventMarketDetail the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_event_market_detail}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
//            array('event_market_id, employee_id, created_date', 'required'),
            array('event_market_id, employee_id, status', 'numerical', 'integerOnly'=>true),
            ['type,type,id, event_market_id, employee_id, status, created_date', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'rEventMarket' => array(self::BELONGS_TO, 'GasEventMarket', 'event_market_id'),
            'rEmployee' => array(self::BELONGS_TO, 'Users', 'employee_id'),
            'rMaterials' => array(self::BELONGS_TO, 'GasMaterials', 'employee_id'),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_market_id' => 'Sự kiện',
            'employee_id' => 'Nhân viên/Vật tư',
            'status' => 'Trạng thái',
            'created_date' => 'Ngày tạo',
            'type'=> 'Loại',
        ];
    }

//    Type Xuat Kho va Nhap Kho chợ siêu thị
    public function getArrayTypeDetailActivation(){
        return [
            self::TYPE_MATERIAL_EXPORT      =>'Xuất kho',
            self::TYPE_MATERIAL_IMPORT      =>'Nhập kho',
        ];
    }
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

	$criteria->compare('t.id',$this->id);
	$criteria->compare('t.event_market_id',$this->event_market_id);
	$criteria->compare('t.employee_id',$this->employee_id);
	$criteria->compare('t.status',$this->status);
	$criteria->compare('t.created_date',$this->created_date,true);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    public static $arrayStatus = [
        self::STATUS_NO_ACCEPT,
        self::STATUS_ACCEPT,
    ];
    
   /** @Author: LocNV Aug 14, 2019
     *  @Todo: set accept for employee with road show
     *  @Param: $roadShowId event market id
     *  @Param: $employeeId employee id
     *  @Param: $status Status
     **/
    public function setStatusRoadShowEmployee($roadShowId, $employeeId, $status) {
        $res = false;
        //check isset id gas event market
        $mGasEM = GasEventMarket::model()->findByPk($roadShowId);
        if ($mGasEM == null) {
            return ;
        }
        if (!in_array($status, GasEventMarketDetail::$arrayStatus)) {
            return ;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.event_market_id = ' . $roadShowId);
        $criteria->addCondition('t.employee_id = ' . $employeeId);
        $criteria->compare('t.type', GasEventMarketDetail::TYPE_EMPLOYEE);
        $mGasEMDetail = GasEventMarketDetail::model()->find($criteria);
        if (isset($mGasEMDetail) && $status == GasEventMarketDetail::STATUS_NO_ACCEPT) {
            if ($mGasEMDetail->delete()){
                $res = true;
            }
        } else {
            if(empty($mGasEMDetail)){
                $mGasEMDetail = new GasEventMarketDetail();
            }
            $mGasEMDetail->event_market_id  = $roadShowId;
            $mGasEMDetail->employee_id      = $employeeId;
            $mGasEMDetail->status           = $status;
            $mGasEMDetail->type             = self::TYPE_EMPLOYEE;
            $mGasEMDetail->save();
            $res = true;
        }
        
        return $res;
    } 
    
        
    /** @Author: NamNH Sep1619
     *  @Todo: save detail event market
     **/
    public function saveEventMarketDetail() {
        if(empty($this->event_market_id) || empty($this->type)){
            return;
        }
//        remove old
        $criteria   = new CDbCriteria;
        $criteria->compare('type', $this->type);
        $criteria->compare('event_market_id', $this->event_market_id);
        GasEventMarketDetail::model()->deleteAll($criteria);
        if(empty($this->employee_id)){
            return;
        }
        $aRowInsert = [];
        $createdDate = date('Y-m-d H:i:s');
        foreach ($this->employee_id as $key => $material_id) {
            $qty = isset($this->status[$key]) ? $this->status[$key] : 1;
            $aRowInsert[]="('$this->type',
                '$this->event_market_id',
                '$material_id',
                '$qty',
                '$createdDate'
            )";
        }
        $this->insertModel($aRowInsert);
    }
    
    public function setDefaultAttributes(){
        $this->employee_id  = '';
        $this->status       = '';
    }
    /** @Author: NamNH Sep1619
     *  @Todo: insert
     **/
    public function insertModel($aRowInsert){
        $tableName = GasEventMarketDetail::model()->tableName();
        $sql = "insert into $tableName (type,
                        event_market_id,
                        employee_id,
                        status,
                        created_date
                        ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert)>0){
            Yii::app()->db->createCommand($sql)->execute();
        }
    }
    
//    Lấy tất cả type nhập vào kho
    public function getArrayTypeImport(){
        return [
            self::TYPE_MATERIAL_IMPORT,
        ];
    }
    /** @Author: NhanDT Sep 20,2019
     *  @Todo: get info material
     *  @Param: $aType =  [TYPE_MATERIAL_EXPORT, TYPE_MATERIAL_IMPORT]
     **/
    public function getArrMaterials($aEventId,$aType) {
        $aResult = [];
        $aTypeImport = $this->getArrayTypeImport();
        if (empty($aEventId) || empty($aType)){
            return $aResult;
        }
        $criteria       = new CDbCriteria();
        $criteria->addInCondition('t.event_market_id',$aEventId);
        $criteria->addInCondition('t.type',$aType);
        $models     = self::model()->findAll($criteria);
        foreach($models as $key => $mGasEventMarketDetail)
        {
            $aResult['LIST_MATERIAL'][$mGasEventMarketDetail->type][$mGasEventMarketDetail->employee_id] = $mGasEventMarketDetail->rMaterials->name;
            if(isset($aResult['DETAIL'][$mGasEventMarketDetail->event_market_id][$mGasEventMarketDetail->type][$mGasEventMarketDetail->employee_id])){
                $aResult['DETAIL'][$mGasEventMarketDetail->event_market_id][$mGasEventMarketDetail->type][$mGasEventMarketDetail->employee_id] += $mGasEventMarketDetail->status;
            }else{
                $aResult['DETAIL'][$mGasEventMarketDetail->event_market_id][$mGasEventMarketDetail->type][$mGasEventMarketDetail->employee_id] = $mGasEventMarketDetail->status;
            }
            if(isset($aResult['TOTAL'][$mGasEventMarketDetail->type][$mGasEventMarketDetail->employee_id])){
                $aResult['TOTAL'][$mGasEventMarketDetail->type][$mGasEventMarketDetail->employee_id] += $mGasEventMarketDetail->status;
            }else{
                $aResult['TOTAL'][$mGasEventMarketDetail->type][$mGasEventMarketDetail->employee_id] = $mGasEventMarketDetail->status;
            }
            if(in_array($mGasEventMarketDetail->type,$aTypeImport)){
                if(isset($aResult['TOTAL_IMPORT'][$mGasEventMarketDetail->event_market_id])){
                    $aResult['TOTAL_IMPORT'][$mGasEventMarketDetail->event_market_id]  += $mGasEventMarketDetail->status;
                }else{
                    $aResult['TOTAL_IMPORT'][$mGasEventMarketDetail->event_market_id]  = $mGasEventMarketDetail->status;
                }
            }else{
                if(isset($aResult['TOTAL_EXPORT'][$mGasEventMarketDetail->event_market_id])){
                    $aResult['TOTAL_EXPORT'][$mGasEventMarketDetail->event_market_id]  += $mGasEventMarketDetail->status;
                }else{
                    $aResult['TOTAL_EXPORT'][$mGasEventMarketDetail->event_market_id]  = $mGasEventMarketDetail->status;
                }
            }
        }
        $aResult['LIST_TYPE_MATERIAL'] = $this->getArrayTypeDetailActivation();
        return $aResult;
    }    
}