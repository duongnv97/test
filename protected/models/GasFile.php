<?php

/**
 * This is the model class for table "{{_gas_file}}".
 *
 * The followings are the available columns in table '{{_gas_file}}':
 * @property string $id
 * @property integer $type
 * @property string $belong_id
 * @property string $file_name
 * @property integer $order_number 
 * @property string $created_date
 */
class GasFile extends BaseSpj
{
    const ID_BEGIN_CHANGE           = 265581;// Sep2918 define id begin change new structure upload
    
    const TYPE_1_BREAK_TASK         = 1;
    const TYPE_2_SUPPORT_CUSTOMER   = 2;
    const TYPE_3_UPHOLD_REPLY       = 3;
    const TYPE_4_SUPPORT_CUSTOMER_COMPLETE = 4;
    const TYPE_5_SETTLE             = 5;// Jul 14, 2016 file chứng từ của quyết toán
    const TYPE_6_SETTLE_CREATE      = 6;// Jul 23, 2016 file chứng từ khi tạo quyết toán - all type ( đề nghị thanh toán, chuyển khoản )
    const TYPE_7_WORK_REPORT        = 7;// Oct 06, 2016 file báo cáo công việc của các nv ngoài văn phòng
    const TYPE_8_CASHBOOK           = 8;// May 08, 2017 file hóa đơn chứng từ sổ quỹ
    const TYPE_9_STORECARD          = 9;// May 25, 2017 file hóa đơn chứng từ thẻ kho
    const TYPE_10_APP_BO_MOI        = 10;// May 25, 2017 file hóa đơn chứng từ đơn hàng bò mối
    const TYPE_11_HGD_PTTT_CODE     = 11;// Jun 08, 2017 file chụp hình pttt code của NVGN
    const TYPE_12_SUPPORT_CUSTOMER_REQUEST     = 12;// 25/06/2018 file hình ảnh hỗ trợ thay thế vật tư
    const TYPE_13_TICKET            = 13;// 17/08/2018 file hình ảnh ticket
    const TYPE_14_SCALES_1              = 14;// DungNT Dec0218 file cân lần 1 từ PM cân
    const TYPE_15_SCALES_2              = 15;// DungNT Dec0218 file cân lần 2 từ PM cân
    
    const TYPE_16_REWARD_IMAGE          = 16;
    const TYPE_17_REWARD_BANNER         = 17;
    const TYPE_18_REWARD_PARTNER_LOGO   = 18;
    
    const TYPE_19_TRUCK_PLAN            = 19;// DungNT Mayc2319 file from TruckPlan
    const TYPE_CHECK_AGENT              = 20; //LocNV Jul 9 2019 file check agent
    const TYPE_21_DOCUMENT              = 21; //HaoNH Aug 16 2019 file mẫu văn bản,biên bản
    
    // array type => name model
    public static $TYPE_MODEL = array( 
        GasFile::TYPE_1_BREAK_TASK          => 'GasBreakTaskDaily',
        GasFile::TYPE_2_SUPPORT_CUSTOMER    => 'GasSupportCustomer',
        GasFile::TYPE_3_UPHOLD_REPLY        => 'GasUpholdReply',
        GasFile::TYPE_4_SUPPORT_CUSTOMER_COMPLETE => 'GasSupportCustomer',
        GasFile::TYPE_5_SETTLE              => 'GasSettle',
        GasFile::TYPE_6_SETTLE_CREATE       => 'GasSettle',
        GasFile::TYPE_7_WORK_REPORT         => 'WorkReport',
        GasFile::TYPE_8_CASHBOOK            => 'EmployeeCashbook',
        GasFile::TYPE_9_STORECARD           => 'GasStoreCard',
        GasFile::TYPE_10_APP_BO_MOI         => 'GasAppOrder',
        GasFile::TYPE_11_HGD_PTTT_CODE              => 'TransactionHistory',
        GasFile::TYPE_12_SUPPORT_CUSTOMER_REQUEST   => 'GasSupportCustomerRequest',
        GasFile::TYPE_13_TICKET                     => 'GasTicketsDetail',
        GasFile::TYPE_14_SCALES_1                   => 'TruckScales',
        GasFile::TYPE_15_SCALES_2                   => 'TruckScales',
        GasFile::TYPE_16_REWARD_IMAGE               => 'Reward',
        GasFile::TYPE_17_REWARD_BANNER              => 'Reward',
        GasFile::TYPE_18_REWARD_PARTNER_LOGO        => 'Reward',
        GasFile::TYPE_19_TRUCK_PLAN                 => 'TruckPlanDetail',
        GasFile::TYPE_CHECK_AGENT                   => 'CheckAgent',
        GasFile::TYPE_21_DOCUMENT                   => 'DocManagement',
    );
    
    public static $TYPE_RESIZE_IMAGE = array(
        GasFile::TYPE_1_BREAK_TASK,
        GasFile::TYPE_3_UPHOLD_REPLY, 
        GasFile::TYPE_4_SUPPORT_CUSTOMER_COMPLETE, 
        GasFile::TYPE_5_SETTLE,
        GasFile::TYPE_6_SETTLE_CREATE,
        GasFile::TYPE_7_WORK_REPORT,
        GasFile::TYPE_8_CASHBOOK,
        GasFile::TYPE_9_STORECARD,
        GasFile::TYPE_10_APP_BO_MOI,
        GasFile::TYPE_11_HGD_PTTT_CODE,
        GasFile::TYPE_12_SUPPORT_CUSTOMER_REQUEST,
        GasFile::TYPE_13_TICKET,
        GasFile::TYPE_14_SCALES_1,
        GasFile::TYPE_15_SCALES_2,
        GasFile::TYPE_16_REWARD_IMAGE,
        GasFile::TYPE_17_REWARD_BANNER,
        GasFile::TYPE_18_REWARD_PARTNER_LOGO,
        GasFile::TYPE_19_TRUCK_PLAN,
        GasFile::TYPE_CHECK_AGENT,
    );
    
    /**
     * @Author: DungNT May 18, 2017
     * @Todo: get những type model khong gen slug name
     */
    public function getTypeNotSlugName() {
        return [GasFile::TYPE_15_SCALES_2, GasFile::TYPE_14_SCALES_1, GasFile::TYPE_13_TICKET, GasFile::TYPE_12_SUPPORT_CUSTOMER_REQUEST, GasFile::TYPE_3_UPHOLD_REPLY, GasFile::TYPE_5_SETTLE, GasFile::TYPE_6_SETTLE_CREATE, GasFile::TYPE_7_WORK_REPORT, GasFile::TYPE_8_CASHBOOK,
            GasFile::TYPE_9_STORECARD, GasFile::TYPE_10_APP_BO_MOI, GasFile::TYPE_11_HGD_PTTT_CODE,
            GasFile::TYPE_19_TRUCK_PLAN,
        ];
    }
    
    public static $AllowFile = 'jpg,jpeg,png';
    public static $pathUpload = 'upload/all_file';
    public static $aSize = array(
        'size1' => array('width' => 128, 'height' => 96), // small size, dùng ở form update văn bản
//        'size2' => array('width' => 1536, 'height' => 1200), // Close on Dec 10, 2015 resize ra hình to quá, không cần thiết
        'size2' => array('width' => 1024, 'height' => 900), // size view, dùng ở xem văn bản
    );
    /** @note: về size ảnh. với tấm 3mb thì 'width' => 1536 cho ra resize là 1.3 mb
     *  còn với 'width' => 1024, thì cho ra size 627 kb => cũng vẫn còn lớn
     */
    
    const IMAGE_MAX_UPLOAD          = 10;
    const IMAGE_MAX_UPLOAD_SHOW     = 1;
    const IMAGE_MAX_UPLOAD_APP      = 3; // Oct 22, 2015 limit for app upload
    
    public $checkEmpty = false;
    
    /** Mar 06, 2015
     * dùng để quản lý file của các table mà có lượng file ít và sẽ không cut data
     * dựa theo type
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_file}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, type, belong_id, file_name, order_number, created_date', 'safe'),
            array('file_name', 'file','on'=>'UploadFile',
                    'allowEmpty'=>true,
                    'types'=> GasFile::$AllowFile,
                    'wrongType'=> 'Chỉ cho phép định dạng file '.  GasFile::$AllowFile.' .',
                    'maxSize'   => ActiveRecord::getMaxFileSize(),
                    'minSize'   => ActiveRecord::getMinFileSize(),
                    'tooLarge'  =>'Dung lượng tập tin / hình ảnh quá lớn '.(ActiveRecord::getMaxFileSize()/1024).' KB. Vui lòng tải lại file dung lượng nhỏ hơn.',
                    'tooSmall'  =>'Dung lượng tập tin / hình ảnh quá nhỏ  '.(ActiveRecord::getMinFileSize()/1024).' KB. Vui lòng tải lại file dung lượng lớn hơn',
            ),
            
            array('file_name', 'file', 'on'=>'UploadFilePdf',
                'allowEmpty'=>true,
                'types'=> GasSupportCustomer::$AllowFilePdf,
                'wrongType'=>'Chỉ cho phép tải file '.GasSupportCustomer::$AllowFilePdf,
                'maxSize' => ActiveRecord::getMaxFileSize(), // 5MB
                'tooLarge' => 'File quá lớn, cho phép '.(ActiveRecord::getMaxFileSize()/1024).' KB. Vui lòng up file nhỏ hơn.',
            ),
            array('file_name', 'file', 'on'=>'UploadFileDoc',
                'allowEmpty'=>true,
                'types'=> DocManagement::$AllowFileDoc,
                'wrongType'=>'Chỉ cho phép tải file '.DocManagement::$AllowFileDoc,
                'maxSize' => ActiveRecord::getMaxFileSize(), // 5MB
                'tooLarge' => 'File quá lớn, cho phép '.(ActiveRecord::getMaxFileSize()/1024).' KB. Vui lòng up file nhỏ hơn.',
            )
            
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'type' => 'Type',
            'belong_id' => 'Belong',
            'file_name' => 'File Name',
            'order_number' => 'Order Number',
            'created_date' => 'Created Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.type',$this->type);
        $criteria->compare('t.belong_id',$this->belong_id,true);
        $criteria->compare('t.file_name',$this->file_name,true);
        $criteria->compare('t.order_number',$this->order_number);
        $criteria->compare('t.created_date',$this->created_date,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /*** MAR 06,2015  HANDLE VALIDATE AND SAVE MULTI FILE ********/
    
    /**
     * @Author: DungNT Mar 06, 2015
     * @Todo: validate multi file 
     * @Param: $mBelongTo is may be model GasBreakTaskDaily OR ANY MODEL OTHER
     * chỗ này cứ truyền vào model bình thường
     * * @Important làm theo kiểu này thì sẽ không cho update record, mà sẽ là luôn xóa đi và tạo lại
     * hoặc là cái created date của record belongto kia sẽ không update cái created date
     */
    public function validateFile($mBelongTo, $needMore = array() ) {
        $className = get_class($mBelongTo);
        $ok=false; // for check if no file submit
        if(isset($_POST[$className]['file_name'])  && count($_POST[$className]['file_name'])){
            foreach($_POST[$className]['file_name'] as $key=>$item){                
                $mFile = new GasFile('UploadFile');
                $mFile->file_name  = CUploadedFile::getInstance($mBelongTo,'file_name['.$key.']');
                $mFile->validate();
                if(!is_null($mFile->file_name) && !$mFile->hasErrors() ){
                    $ok=true;
                    MyFormat::IsImageFile($_FILES[$className]['tmp_name']['file_name'][$key]);
                    $FileName = MyFunctionCustom::remove_vietnamese_accents($mFile->file_name->getName());
                    if(strlen($FileName) > 100 ){
                        $mFile->addError('file_name', 'Tên file không được quá 100 ký tự, vui lòng đặt tên ngắn hơn');
                    }
                }
                if($mFile->hasErrors()){
                    $mBelongTo->addError('file_name', $mFile->getError('file_name'));
                }
            }
        }
    }
    
    /**
     * @Author: DungNT May 22, 2019
     * @Todo: web validate multi file 
     * @Param: $mBelongTo is may be model GasBreakTaskDaily OR ANY MODEL OTHER
     * chỗ này cứ truyền vào model bình thường
     * * @Important làm theo kiểu này thì sẽ không cho update record, mà sẽ là luôn xóa đi và tạo lại
     * hoặc là cái created date của record belongto kia sẽ không update cái created date
     */
    public function webValidateFile($mBelongTo, $fieldName = 'file_name', $needMore = array() ) {
        $className = get_class($mBelongTo);
        $isEmptyFile = true; // for check if no file submit
        if(isset($_POST[$className][$fieldName])  && count($_POST[$className][$fieldName])){
            foreach($_POST[$className][$fieldName] as $key=>$item){                
                $mFile = new GasFile('UploadFile');
                $mFile->file_name  = CUploadedFile::getInstance($mBelongTo,'file_name['.$key.']');
                $mFile->validate();
                if(!is_null($mFile->file_name) && !$mFile->hasErrors() ){
                    $isEmptyFile = false;
                    MyFormat::IsImageFile($_FILES[$className]['tmp_name'][$fieldName][$key]);
                    $FileName = MyFunctionCustom::remove_vietnamese_accents($mFile->file_name->getName());
                    if(strlen($FileName) > 100 ){
                        $mFile->addError($fieldName, 'Tên file không được quá 100 ký tự, vui lòng đặt tên ngắn hơn');
                    }
                }
                if($mFile->hasErrors()){
                    $mBelongTo->addError($fieldName, $mFile->getError($fieldName));
                }
            }
        }
        
        if($this->checkEmpty && $isEmptyFile){
            $mBelongTo->addError($fieldName, 'Chưa tải tập tin / hình ảnh, vui lòng nhập đầy đủ dữ liệu');
        }
    }
    
    /**
     * @Author: DungNT Mar 06, 2015
     * @Todo: save record multi file for one detail
     * @Param: $mGasIssueTickets GasIssueTickets
     * * chỗ này cứ truyền vào model bình thường
     * phần xử lý này $mRoot = GasIssueTickets::model()->findByPk($mGasIssueTickets->id); // để lấy created_date, không thể find rồi đưa vào biến $model dc vì còn liên quan đến get file
     * sẽ xử lý trc khi gọi hàm này
     * @Important làm theo kiểu này thì sẽ không cho update record, mà sẽ là luôn xóa đi và tạo lại
     * hoặc là cá created date của record belongto kia sẽ không update cái created date
     * @chu-y: cái explode(' ', $mBelongTo->created_date);
     */
    public function saveRecordFile($mBelongTo, $type) {
       
        $className = get_class($mBelongTo);
        $mBelongTo = MyFormat::loadModelByClass($mBelongTo->id, $className);
        set_time_limit(7200);
        if(isset($_POST[$className]['file_name'])  && count($_POST[$className]['file_name'])){
            foreach($_POST[$className]['file_name'] as $key=>$item){
                $mFile = new GasFile();
                $mFile->type =  $type;
                $mFile->belong_id = $mBelongTo->id;
                $created_date = explode(' ', $mBelongTo->created_date);
                $mFile->created_date = $created_date[0];
                $mFile->order_number = $key+1;
                $mFile->file_name  = CUploadedFile::getInstance($mBelongTo,'file_name['.$key.']');
                if(!is_null($mFile->file_name)){
                    $mFile->file_name  = self::saveFile($mFile, 'file_name', $key);
                    $mFile->save();
                    if(in_array($type, self::$TYPE_RESIZE_IMAGE)){
                        self::resizeImage($mFile, 'file_name');
                    }
                }
            }
        }        
    }
    
    /** @Author: DungNT Sep 29, 2018
     *  @Todo: build new structure save file
     *  @return: upload/all_file/2018/GasSettle
     **/
    public function getPathUpload() {
        $year           = MyFormat::GetYearByDate($this->created_date);
        $month          = MyFormat::GetYearByDate($this->created_date, array('format'=>'m'));
        $aClassName     = GasFile::$TYPE_MODEL;
        $className = isset($aClassName[$this->type]) ? $aClassName[$this->type] : 'default';
        return GasFile::$pathUpload."/$year/$month/$className";
    }
    
    /** @Author: DungNT Aug 25, 2019
     *  @Todo: getPathUploadOld path for old structure 
     **/
    public function getPathUploadOld() {
        $aDate      = explode('-', $this->created_date);
        $pathUpload = GasFile::$pathUpload."/$aDate[0]/$aDate[1]";
        if($this->id > GasFile::ID_BEGIN_CHANGE){
            $pathUpload = $this->getPathUpload();
        }
        return $pathUpload;
    }
    
    /**
     * @Author: DungNT Mar 06, 2015
     * To do: save file 
     * @param: $model model GasFile
     * @param: $count 1,2,3
     * @return: name of image 
     */
    public static function  saveFile($model, $fieldName, $count)
    {        
        if(is_null($model->$fieldName)) return '';
        $pathUpload = $model->getPathUpload();
        $ext = strtolower($model->$fieldName->getExtensionName());

        if(in_array($model->type, $model->getTypeNotSlugName())){
            $fileName = time();
            $fileName = $fileName."-".ActiveRecord::randString().$count.'.'.$ext;
        }else{
            $FileNameClient = strtolower(MyFunctionCustom::remove_vietnamese_accents($model->$fieldName->getName()));
            $FileNameClient = str_replace($ext, "", $FileNameClient);
            $fileName = time()."$count-".MyFunctionCustom::slugify($FileNameClient).'.'.$ext;
        }
        
        $imageProcessing = new ImageProcessing();
        $imageProcessing->createDirectoryByPath($pathUpload);
        $model->$fieldName->saveAs($pathUpload.'/'.$fileName);
        return $fileName;
    }
    
    /**
     * @Author: DungNT Mar 06, 2015
     * To do: resize file scan
     * @param: $model model GasFile
     * @param: $fieldName 
     */
    public static function resizeImage($model, $fieldName) {
//        $year = MyFormat::GetYearByDate($model->created_date);
//        $month = MyFormat::GetYearByDate($model->created_date, array('format'=>'m'));
//        $pathUpload = GasFile::$pathUpload."/$year/$month";
        $pathUpload = $model->getPathUpload();

        $ImageHelper = new ImageHelper();
        $ImageHelper->folder = '/'.$pathUpload;
        $ImageHelper->file = $model->$fieldName;
        $ImageHelper->aRGB = array(0, 0, 0);//full black background
        $ImageHelper->thumbs = self::$aSize;
//        $ImageHelper->createFullImage = true ;
        $ImageHelper->createThumbs();
        $ImageHelper->deleteFile($ImageHelper->folder . '/' . $model->$fieldName);        
    }
    
    /**
     * @Author: DungNT Feb 25, 2015
     * To do: delete file scan
     * @param: $modelRemove model GasFile
     * @param: $fieldName is file_name
     * @param: $aSize
     */
    public static function RemoveFileOnly($modelRemove, $fieldName) {
//        $aDate = explode('-', $modelRemove->created_date);
//        $pathUpload = GasFile::$pathUpload."/$aDate[0]/$aDate[1]";
        $pathUpload = $modelRemove->getPathUpload();

        $ImageHelper = new ImageHelper();     
        $ImageHelper->folder = '/'.$pathUpload;
        $ImageHelper->deleteFile($ImageHelper->folder . '/' . $modelRemove->$fieldName);
        foreach ( GasFile::$aSize as $key => $value) {
            $ImageHelper->deleteFile($ImageHelper->folder . '/' . $key . '/' . $modelRemove->$fieldName);
        }
    } 
    
    protected function beforeDelete() {
        self::RemoveFileOnly($this, 'file_name');
        return parent::beforeDelete();
    }
    
    /*** MAR 06,2015 END HANDLE VALIDATE AND SAVE MULTI FILE ********/
    
    /**
     * @Author: DungNT Mar 06, 2015
     * @Todo: delete by type and belong_id
     * @Param: $model
     */
    public static function DeleteByBelongIdAndType($belong_id, $type) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.belong_id', $belong_id);
        $criteria->compare('t.type', $type);
        $models = self::model()->findAll($criteria);
        foreach($models as $item){
            $item->delete();
        }
    }
    
    /**
     * @Author: DungNT Sep 04, 2015
     * @Todo: validate multi file 
     * @Param: $model is model GasSupportCustomer
     */
    public static function ValidateFilePdf($model) {
        $cRole = Yii::app()->user->role_id;
        $ok=false; // for check if no file submit
        if(isset($_POST['GasSupportCustomer']['file_name'])  && count($_POST['GasSupportCustomer']['file_name'])){
            foreach($_POST['GasSupportCustomer']['file_name'] as $key=>$item){
                $mFile = new GasFile('UploadFilePdf');
                $mFile->file_name  = CUploadedFile::getInstance($model,'file_name['.$key.']');
                if(!is_null($mFile->file_name)){
                    $ok=true;
                }
                $mFile->validate();
                if($mFile->hasErrors()){
                    $model->addError('file_design', $mFile->getError('file_name'));
                }
            }
        }
        if(!$ok && $model->scenario == 'tech_create' && $cRole != ROLE_SUB_USER_AGENT ){ // ko co file submit
            $model->addError('file_design', "Chưa upload file");
        }
    }
    
    /** @Author: HaoNH Aug 16, 2019
     *  @Todo:Validate file văn bản
     *  @Param:
     **/
     public function ValidateFileDoc($model,$aFilename) {
         
        $cRole = Yii::app()->user->role_id;
        $ok=false; // for check if no file submit
        if(isset($aFilename)  && count($aFilename)){
            foreach($aFilename as $key=>$item){
                $mFile = new GasFile('UploadFileDoc');
                $mFile->file_name  = CUploadedFile::getInstance($model,'file_name['.$key.']');
                
                if(!is_null($mFile->file_name)){
                    $ok=true;
                }
                $mFile->validate();
                if($mFile->hasErrors()){
                    $model->addError('file_design', $mFile->getError('file_name'));
                }
            }
        }
        
        if(!$ok && $model->scenario == 'doc_create' ){ // ko co file submit
            $model->addError('file_design', "Chưa upload file");
        }
    }
    
    /**
     * @Author: DungNT Sep 19, 2015
     * @Todo: something
     */
    public function getForceLinkDownload() {        
        $link = Yii::app()->createAbsoluteUrl('admin/ajax/forceDownload', array('id'=>$this->id, 'model'=>'GasFile'));
        return "<a target='_blank' href='$link'>$this->file_name</a>";
    }
    /**
     * @Author: DungNT Sep 04, 2015
     * @Todo: something
     */
    public function getSrcForceDownload() {
        $year = MyFormat::GetYearByDate($this->created_date);
        $month = MyFormat::GetYearByDate($this->created_date, array('format'=>'m'));
        $pathUpload = GasFile::$pathUpload."/$year/$month/$this->file_name";
        if($this->id > GasFile::ID_BEGIN_CHANGE){
            $pathUpload = $this->getPathUpload()."/$this->file_name";
        }// Now2918 fix apply  ID_BEGIN_CHANGE  => Sai Sep2918 không áp dụng ID_BEGIN_CHANGE cho phần này đc, vì file để ở ngoài
        return $pathUpload;
    }

    /**
     * @Author: DungNT Oct 06, 2016
     * @Todo: API validate multi file 
     */
    public static function apiValidateFile($mBelongTo, $fieldName, $needMore = array() ) {
        $ok=false; // for check if no file submit
        if(isset($_FILES[$fieldName]['name'])  && count($_FILES[$fieldName]['name'])){
            foreach($_FILES[$fieldName]['name'] as $key=>$item){
                $mFile = new GasFile('UploadFile');
                $mFile->file_name  = CUploadedFile::getInstanceByName( "{$fieldName}[$key]");
                $mFile->validate();
                if(!is_null($mFile->file_name) && !$mFile->hasErrors() ){
                    $ok=true;
                    MyFormat::IsImageFile($_FILES[$fieldName]['tmp_name'][$key]);
                    $FileName = MyFunctionCustom::remove_vietnamese_accents($mFile->file_name->getName());
                    if(strlen($FileName) > 100 ){
                        $mFile->addError('file_name', "Tên file không được quá 100 ký tự, vui lòng đặt tên ngắn hơn");
                    }
                }
                if($mFile->hasErrors()){
                    $mBelongTo->addError('file_name', $mFile->getError('file_name'));
                }
            }
        }
    }
    
    /**
     * @Author: DungNT Feb 23, 2018
     * @Todo: apply function not static API validate multi file 
     */
    public function apiValidateFileFixV1($mBelongTo, $fieldName, $needMore = array() ) {
        $isEmptyFile = true; // for check if no file submit
        if(isset($_FILES[$fieldName]['name'])  && count($_FILES[$fieldName]['name'])){
            foreach($_FILES[$fieldName]['name'] as $key=>$item){
                $mFile = new GasFile('UploadFile');
                $mFile->file_name  = CUploadedFile::getInstanceByName( "{$fieldName}[$key]");
                $mFile->validate();
                if(!is_null($mFile->file_name) && !$mFile->hasErrors() ){
                    $isEmptyFile = false;
                    MyFormat::IsImageFile($_FILES[$fieldName]['tmp_name'][$key]);
                    $FileName = MyFunctionCustom::remove_vietnamese_accents($mFile->file_name->getName());
                    if(strlen($FileName) > 100 ){
                        $mFile->addError('file_name', "Tên file không được quá 100 ký tự, vui lòng đặt tên ngắn hơn");
                    }
                }
                if($mFile->hasErrors()){
                    $mBelongTo->addError('file_name', $mFile->getError('file_name'));
                }
            }
        }
        if($this->checkEmpty && $isEmptyFile){
            $mBelongTo->addError('file_name', 'Chưa tải tập tin / hình ảnh, vui lòng nhập đầy đủ dữ liệu');
        }
        
    }
    
    /**
     * @Author: DungNT Oct 27, 2015
     * @Todo: Api save record multi file for one detail
     * @Param: $mGasIssueTickets GasIssueTickets
     * * chỗ này cứ truyền vào model bình thường
     * phần xử lý này $mRoot = GasIssueTickets::model()->findByPk($mGasIssueTickets->id); // để lấy created_date, không thể find rồi đưa vào biến $model dc vì còn liên quan đến get file
     * sẽ xử lý trc khi gọi hàm này
     * @Important làm theo kiểu này thì sẽ không cho update record, mà sẽ là luôn xóa đi và tạo lại
     * hoặc là cá created date của record belongto kia sẽ không update cái created date
     * @chu-y: cái explode(' ', $mBelongTo->created_date);
     */
    public static function ApiSaveRecordFile($mBelongTo, $type) {
        $className = get_class($mBelongTo);
        $mBelongTo = MyFormat::loadModelByClass($mBelongTo->id, $className);
        set_time_limit(7200);
        if(isset($_FILES['file_name']['name'])  && count($_FILES['file_name']['name'])){
            foreach($_FILES['file_name']['name'] as $key=>$item){
                $mFile = new GasFile();
                $mFile->type =  $type;
                $mFile->belong_id = $mBelongTo->id;
                $created_date = explode(' ', $mBelongTo->created_date);
                $mFile->created_date = $created_date[0];
                $mFile->order_number = $key+1;
                $mFile->file_name  = CUploadedFile::getInstanceByName( "file_name[$key]");
                if(!is_null($mFile->file_name)){
                    $mFile->file_name  = self::saveFile($mFile, 'file_name', $key);
                    $mFile->save();
                    if(in_array($type, self::$TYPE_RESIZE_IMAGE)){
                        self::resizeImage($mFile, 'file_name');
                    }
                }
            }
        }        
    }
    
    /**
     * @Author: DungNT Feb 23, 2018
     * @Todo: apply function not static API save multi file 
     */
    public function apiSaveRecordFileFixV1($mBelongTo, $type) {
        $className = get_class($mBelongTo);
        $mBelongTo = MyFormat::loadModelByClass($mBelongTo->id, $className);
        set_time_limit(7200);
        if(isset($_FILES['file_name']['name'])  && count($_FILES['file_name']['name'])){
            foreach($_FILES['file_name']['name'] as $key=>$item){
                $mFile = new GasFile();
                $mFile->type =  $type;
                $mFile->belong_id = $mBelongTo->id;
                $created_date = explode(' ', $mBelongTo->created_date);
                $mFile->created_date = $created_date[0];
                $mFile->order_number = $key+1;
                $mFile->file_name  = CUploadedFile::getInstanceByName( "file_name[$key]");
                if(!is_null($mFile->file_name)){
                    $mFile->file_name  = self::saveFile($mFile, 'file_name', $key);
                    $mFile->save();
                    if(in_array($type, self::$TYPE_RESIZE_IMAGE)){
                        self::resizeImage($mFile, 'file_name');
                    }
                }
            }
        }
    }
    
    /**
     * @Author: DungNT Jul 14, 2016
     * @Todo: get array model file
     */
    public static function getAllFile($mBelongTo, $type) {
        if(empty($mBelongTo->id)){
            return array();
        }
        $criteria = new CDbCriteria;
        $criteria->compare('t.belong_id', $mBelongTo->id);
        $criteria->compare('t.type', $type);
        $criteria->order = "t.id";
        return GasFile::model()->findAll($criteria);
    }
    
    // DungNT Jul 14, 2016
    public function getViewImg(){
        if(empty($this->file_name) && !in_array($this->type, GasFile::$TYPE_RESIZE_IMAGE)){
            return '';
        }
        $str ="<a class='gallery' href='".Yii::app()->createAbsoluteUrl('admin/ajax/viewImageProfileHs', array('id'=>$this->id, 'model'=>'GasFile'))."'> ";
            $str.="<img width='80' height='60' src='".ImageProcessing::bindImageByModel($this,'','',array('size'=>'size1'))."'>";
        $str.="</a>";
       return $str;
   }
   
    /**
     * @Author: DungNT Jul 14, 2016
     * @Todo: delete file in form update
     */
    public function deleteFileInUpdate($mBelongTo) {
        if(isset($_POST['delete_file']) && is_array($_POST['delete_file']) && count($_POST['delete_file'])){
            $criteria = new CDbCriteria;
            $criteria->compare('t.belong_id', $mBelongTo->id);
//            $criteria->addInCondition('t.id', $_POST['delete_file']);
            $sParamsIn = implode(',', $_POST['delete_file']);
            $criteria->addCondition("t.id IN ($sParamsIn)");
            $models = GasFile::model()->findAll($criteria);
            foreach($models as $model){
                $model->delete();
            }
        }
    }
    
    /**
     * @Author: DungNT Oct 06, 2016
     * @Todo: api get array file 
     */
    public static function apiGetFileUpload($aModel) {
        $aRes = [];
        $mImageProcessing               = new ImageProcessing();
        $mImageProcessing->sourceFrom   = BaseSpj::SOURCE_FROM_APP;
        foreach ($aModel as $mFile){
            $tmp = array();
            $tmp['id']      = $mFile->id;// mới thêm biến này May 18, 2017 chưa api nào dùng, sử dụng để delete file
//            $tmp['thumb']   = ImageProcessing::bindImageByModel($mFile,'','',array('size'=>'size1'));
//            $tmp['large']   = ImageProcessing::bindImageByModel($mFile,'','',array('size'=>'size2'));
            $tmp['thumb']   = $mImageProcessing->getUrlImageByModel($mFile,'','',array('size'=>'size1'));
            $tmp['large']   = $mImageProcessing->getUrlImageByModel($mFile,'','',array('size'=>'size2'));
            $aRes[] = $tmp;
        }
        return $aRes;
    }
    
    /**
     * @Author: PHAM THANH NGHIA Mar 07, 2019
     * @Todo: save record multi file for one detail
     * @Param: $mGasIssueTickets GasIssueTickets
     * * chỗ này cứ truyền vào model bình thường
     * phần xử lý này $mRoot = GasIssueTickets::model()->findByPk($mGasIssueTickets->id); // để lấy created_date, không thể find rồi đưa vào biến $model dc vì còn liên quan đến get file
     * sẽ xử lý trc khi gọi hàm này
     * @Important làm theo kiểu này thì sẽ không cho update record, mà sẽ là luôn xóa đi và tạo lại
     * hoặc là cá created date của record belongto kia sẽ không update cái created date
     * @chu-y: cái explode(' ', $mBelongTo->created_date);
     * 
     * @Thay-doi : them trường file_name tiện cho viêc 1 model có nhiều trường lưu hình khác nhau
     */
    public static function SaveRecordFileMore($mBelongTo,$file_name, $type) {
        $className = get_class($mBelongTo);
        $mBelongTo = MyFormat::loadModelByClass($mBelongTo->id, $className);
        set_time_limit(7200);
        $mFile = new GasFile();
        if(isset($_POST[$className][$file_name])  && count($_POST[$className][$file_name])){
            if(is_array($_POST[$className][$file_name])){
                foreach ($_POST[$className][$file_name] as $key => $value) {
                    $mFile = new GasFile();
                    $mFile->type =  $type;
                    $mFile->belong_id = $mBelongTo->id;
                    $created_date = explode(' ', $mBelongTo->created_date);
                    $mFile->created_date = $created_date[0];
                    $mFile->order_number = 1;
                    $mFile->file_name  = CUploadedFile::getInstance($mBelongTo,$file_name.'['.$key.']');
                    if(!is_null($mFile->file_name)){
                        $mFile->file_name  = self::saveFile($mFile, 'file_name', $key);
                        $mFile->save();
                        if(in_array($type, self::$TYPE_RESIZE_IMAGE)){
                            self::resizeImage($mFile, 'file_name');
                        }
                    }
                }
            }else{
                $mFile->type =  $type;
                $mFile->belong_id = $mBelongTo->id;
                $created_date = explode(' ', $mBelongTo->created_date);
                $mFile->created_date = $created_date[0];
                $mFile->order_number = 1;
                $mFile->file_name  = CUploadedFile::getInstance($mBelongTo,$file_name);
                if(!is_null($mFile->file_name)){
                    $mFile->file_name  = self::saveFile($mFile, 'file_name', 1);
                    $mFile->save();
                    if(in_array($type, self::$TYPE_RESIZE_IMAGE)){
                        self::resizeImage($mFile, 'file_name');
                    }
                }
            }
        }  
        return  $mFile->id;
    }
    
}