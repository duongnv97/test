<?php

class Telesale extends Users {
    public $isRemoveHoliday     = false;
    public $called      = 2;
    public $not_call    = 3;
    public $call_again  = 4;
    public $date_call_again_from, $date_call_again_to; // for seach call again today
    const STT_WRONG_NUMBER   = 1;
    const STT_SAME_ADDRESS   = 2;
    const STT_REFUSE         = 3;
    const STT_CALL_AGAIN     = 4;
    const STT_CALLED_AGAIN   = 5;
    const STT_TRACK_PURCHASE = 6;
    const STT_BOUGHT         = 7;
    const STT_UPDATE_STATUS  = 8;// chua phân loại trạng thái
    const STT_NOT_USE_GAS               = 9;
    const STT_PHONE_NOT_CONTACT         = 10;
    const STT_TRACK_PURCHASE_APP        = 11;
    const STT_CUSTOMER_OLD          = 12;// for customer draft
    const STT_CUSTOMER_ADD_NEW      = 13;
    const STT_WRONG_NAME            = 14;
    const STT_WRONG_ADDRESS         = 15;
    const STT_NOT_ANSWER            = 16;
    
    const STT_CANCEL_SLOW_DELIVERY      = 1;
    const STT_CANCEL_BAD_MAINTAIN       = 2;
    const STT_CANCEL_BAD_ATTITUDE       = 3;
    const STT_CANCEL_BUSY_CALL_LATER    = 4;// bận gọi lại sau
    const STT_CANCEL_NOT_NEED           = 5;// KO CO NHU CAU
    const STT_CANCEL_NOT_ANSWER         = 6;// không bắt máy
    const STT_CANCEL_CALL_AGAIN         = 7;// gọi lại lần nữa
    const STT_CANCEL_QUALITY            = 8;// chat luong Gas
    const STT_CANCEL_SETUP_APP          = 9;// NV cty tự cài, KH không biết gì về app
    
    public $provinceExt, $rData = [], $aAgent = [], $pageSize = 10, $date_from_ymd, $date_to_ymd;
    public $callFrom, $callTo, $getOrdersSecond = 0, $fromPttt = 0, $date_call_from, $date_call_to, $status_customer_cancel, $status_customer;
    public $mFollowCustomer = null, $last_purchase_from, $last_purchase_to, $is_buy_complete, $autocomplete_agent = '';
    public $monthData = 3; // Oct1718 Đổ dữ liệu thì 2 tháng cho gọi - Khi tính BQV thì TP 3 tháng, tỉnh 4 tháng 
    public $date_created_to,$date_created_from,$count_sell;
    const STT_BUY_COMPLETE      = 1;
    const STT_BUY_UNCOMPLETE    = 2;
    
    const MIN_TIME_OF_CALL = 30; // second
    
    const BQV_TELESALE      = 1;// type search in admin/sell/index
    const BQV_PTTT_DOT      = 2;
    const BQV_GDKV          = 3;// Now1718 Type GĐKV, fake type to set param $mAppPromotionUser->getFirstOrder = false
    
    const  UID_LAU_NV       = 1414826; // Nguyễn Văn Lâu
    const  UID_DONG_DV      = 1563150;// Dao Van Dong
    
    
    public function getArrayTypeBqv(){// get array CV to search by agent
        $mOneMany   = new GasOneMany;
        $a1 = [
            Telesale::BQV_TELESALE => 'Telesale',
//            AppPromotion::PARTNER_ID_APP_HOAN_BD    => '24H001',
//            AppPromotion::PARTNER_ID_APP_PHUC_HV    => '24H002',
//            AppPromotion::PARTNER_ID_APP_HIEU_TT    => '24H003',
//            AppPromotion::PARTNER_ID_APP_PHUONG_ND  => '24H004',
            AppPromotion::PARTNER_ID_APP_CONG_DOAN  => '24H006',
        ];
        $a2       = $mOneMany->getListdataByType(GasOneMany::TYPE_MONITOR_GDKV);
        return $a1 + $a2;
    }
    /** @Author: DUONG 18/04/2018
     *  @Todo:   get array for dropdownlist
     **/
    public function getArrayStatusBuy(){
        return array(
            self::STT_BUY_COMPLETE      => 'Đã lấy hàng',
            self::STT_BUY_UNCOMPLETE    => 'Chưa lấy hàng'
        );
    }
    public function getArrayCallStatus($search = false){
        $aRes = array(
            self::STT_WRONG_NUMBER          => 'Sai số',
            self::STT_SAME_ADDRESS          => 'Trùng địa chỉ',
            self::STT_REFUSE                => 'KH từ chối',
            self::STT_CALL_AGAIN            => 'KH hẹn gọi lại',
            self::STT_CALLED_AGAIN          => 'Đã gọi lại',
            self::STT_TRACK_PURCHASE        => 'Theo dõi mua hàng tổng đài',
            self::STT_TRACK_PURCHASE_APP    => 'Theo dõi mua hàng App',
            self::STT_BOUGHT                => 'Đã mua hàng',
            self::STT_NOT_USE_GAS           => 'Không sử dụng gas nữa',
            self::STT_PHONE_NOT_CONTACT     => 'Thuê bao không liên lạc',
        );
        if($search){
            $aRes[self::STT_UPDATE_STATUS] = 'Chưa phân loại';
        }
        
        return $aRes;
    }
    public function getArrayStatusCancel() {
        return array(
            self::STT_CANCEL_SLOW_DELIVERY      => 'Giao hàng chậm',
            self::STT_CANCEL_BAD_MAINTAIN       => 'Bảo trì không tốt',
            self::STT_CANCEL_BAD_ATTITUDE       => 'Thái độ nhân viên kém',
            self::STT_CANCEL_BUSY_CALL_LATER    => 'Bận gọi lại sau',
            self::STT_CANCEL_NOT_NEED           => 'Không có nhu cầu',
            self::STT_CANCEL_NOT_ANSWER         => 'Không bắt máy',
            self::STT_CANCEL_CALL_AGAIN         => 'Gọi lại lần nữa',
            self::STT_CANCEL_QUALITY            => 'Chất lượng gas kém',
            self::STT_CANCEL_SETUP_APP          => 'NV cty tự cài, KH không biết gì về app',
        );
    }
    
    public function getArrayCountSell(){
        $aResult = [];
        for($i = 0; $i <= 20 ; $i++){
            $aResult[$i] = $i;
        }
        return $aResult;
    }
    public function rules() {
        $aRules = parent::rules();
        $aRules[] = array('last_purchase_from, last_purchase_to, is_buy_complete, status_customer_cancel, status_customer','safe');
        $aRules[] = array('count_sell,date_created_to,date_created_from,provinceExt, callFrom, callTo, agent_id, getOrdersSecond, fromPttt, date_call_from, date_call_to, countVo,count_vo','safe');

        return $aRules;
    }
    
    public function attributeLabels(){
        $aLabels = parent::attributeLabels();
        $aLabels['date_call_from']      = 'Hẹn gọi lại từ';
        $aLabels['date_call_to']        = 'Đến';
        $aLabels['fromPttt']            = 'Khách hàng PTTT';
        $aLabels['getOrdersSecond']     = 'Xem BQV lần 2';
        $aLabels['callFrom']            = 'Ngày gọi từ';
        $aLabels['callTo']              = 'Đến';
        return $aLabels;
    }

    //-----------Start get all field in index page
    public function getFirstName() {
        return $this->first_name;
    }
    public function getAppointmentDate() {
        $mFollowCustomer = new FollowCustomer();
        $mFollowCustomer->customer_id = $this->id;
        $this->mFollowCustomer = $mFollowCustomer->getLastestRow();
        if($this->mFollowCustomer){
            return MyFormat::dateConverYmdToDmy($this->mFollowCustomer->appointment_date);
        }
        return '';
    }
    
    public function getPhone() {
        $aRoleAllow = [ROLE_CALL_CENTER, ROLE_TELESALE];
        $cRole      = MyFormat::getCurrentRoleId();
        if(in_array($cRole, $aRoleAllow)){
            $mCallClick = new Call();
            return $mCallClick->buildUrlMakeCall($this->phone);
        }
        return str_replace("-", "<br>", $this->phone);
    }
    
//    public function getArrayEmployeeMaintain() {
//        $model = Users::model()->findAll(array(
//            'condition' => 'role_id = :RID',
//            'params' => array(
//                 ':RID' => 9,
//            )
//         ));
//        $aEmployee = [];
//        foreach($model as $employee){
//            $aEmployee[$employee->attributes['id']] = $employee['first_name'];
//        }
//        return $aEmployee;
//    }
    /** @Author: ANH DUNG Jun 26, 2018
     *  @Todo: get array role like telesale
     **/
    public function getArrayRoleLikeTelesale() {
        return [ROLE_CALL_CENTER, ROLE_TELESALE];
    }
    
    public function getEmployee() {
        $mUser = $this->sale;
        return !empty($mUser) ?  $mUser->getFullName() : '';
    }
    
    public function getArrayEmployee(){
        $mAppCache      = new AppCache();
        $aTelesale      = $mAppCache->getListdataUserByRole(ROLE_TELESALE);
        $aTelesale[1504985]    = 'Phan Thị Loan';//Aug 23, 2019 add more users old telesale
        return $aTelesale;
    }
    /** @Author: ANH DUNG Oct 17, 2018
     *  @Todo: get array listdata NV pttt đội app của anh Đợt
     **/
    public function getArrayPttt() {
        $mAppPromotion = new AppPromotion();
        $models = Users::getArrObjectUserByArrUid($mAppPromotion->getListUidPttt(), '');
        return CHtml::listData($models, 'id', 'first_name');
    }
    
    public function canDelete() {
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        if($cRole == ROLE_ADMIN){
            return true;
        }
        return false; // cho phép admin sửa hết, còn những user khác thì ai tạo thì dc sửa của người đó
    }
    public function canModify() {
        if($this->sale_id == 0) return 'visible';
        else return 'hidden';
    }
    public function canCreateNote() {
        if($this->sale_id == 0) return 'hidden';
        else return 'visible';
    }
    public function canViewNote() {
        $mFollowCustomer = FollowCustomer::model()->findByAttributes(array('customer_id'=>$this->id));
        if(empty($mFollowCustomer)) return 'none';
        if($this->canRemoveAnyWhere()){
            return '';
        }
        return 'block';
    }
    
    /** @Author: NamNH Aug 21,2019
     *  @Todo: can remove employee, Mở users cho phép có thể remove nhân viên 
     *  Hàm này thêm vào theo follow hiện tại chạy đỡ.
     *  phải chỉnh lại, không xử dụng trên giao diện index $('.viewNote[style="display:block"]').closest('tr').find('input').attr('disabled',true);
     *  NV có thể cheat
     **/
    public function canRemoveAnyWhere() {
        $cUid = MyFormat::getCurrentUid();
        if($cUid == GasConst::UID_VEN_NTB){
            return true;
        }
        return false;
    }
    
    public function getCustomerId(){
        return $this->id;
    }
    /** @Author: DungNT Jan 16, 2019
     *  @Todo: add more user can show button apply get Customer
     **/
    public function showButtonApply(){
        $ok = false; $cUid   = MyFormat::getCurrentUid();
        $aUidAllow = [Telesale::UID_LAU_NV];
        if(in_array($cUid, $aUidAllow)){
            $ok = true;
        }
        return $ok;
    }
    public function showSetSaleId(){
        $class = 'display_none'; 
        $ok = false;
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        $aUidAllow = [GasConst::UID_CHIEN_BQ, GasConst::UID_NGOC_PT, GasConst::UID_ADMIN, GasConst::UID_ADMIN_NAM,GasConst::UID_VEN_NTB, GasConst::UID_VY_NTT];
        if(in_array($cUid, $aUidAllow)){
            $ok = true;
        }
        return $ok;
    }
    
    /** @Author: ANH DUNG Apr 16, 2018
     *  @Todo: set employee id 
     **/
    public function setSaleIdLogin() {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        if(in_array($cRole, $this->getArrayRoleLikeTelesale()) ){
            $this->sale_id = $cUid;
        }
    }
    
    /** @Author: ANH DUNG Jul 03, 2018
     *  @Todo: check current role is like callcenter
     **/
    public function isCallCenter() {
        $cRole = MyFormat::getCurrentRoleId();
        $aRoleCheck = [ROLE_CALL_CENTER, ROLE_DIEU_PHOI];
        return in_array($cRole, $aRoleCheck);
    }

    public function getCustomer() {
        $mSell = new Sell(); $mAppPromotion =new AppPromotion();
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        $aUidViewData = [
            1563150, // Dao Van Dong
            1414826, // Nguyễn Văn Lâu
        ];
        
        $criteria = new CDbCriteria;
        $criteria->compare('t.area_code_id', $this->agent_id);
        
        $criteria->compare('t.district_id', $this->district_id);
        if(!empty($this->sale_id)){
            $criteria->addCondition("t.sale_id = $this->sale_id OR t.storehouse_id = $this->sale_id" );
        }
//        $criteria->compare('t.sale_id', $this->storehouse_id);// Close Dec2818
        if( !in_array($cUid, $aUidViewData) && $cRole == ROLE_TELESALE && !$this->showSetSaleId() && empty($this->storehouse_id)){
            $criteria->addCondition('t.sale_id=' . $cUid);
        }

        $criteria->compare('t.is_maintain', $this->is_maintain);
        $criteria->compare('t.id', $this->customer_id);
        if(!empty($this->created_by)){
            $criteria->addCondition('t.created_by=' . $this->created_by);
            $criteria->addCondition("DATE(t.created_date) >= '2018-05-01'");// không cho search KH trong 3 tháng trc đây, do lần KH khi NV làm KTBH dưới đại lý 
        }
        
        $sParamsIn = implode(',', $this->getListDistrictLock());
        if(!empty($sParamsIn) && !isset($_GET['type']) ){
            $criteria->addCondition("t.district_id NOT IN ($sParamsIn)");
        }
        if(!empty($this->last_purchase_from)){
            $lastPurchaseFrom = MyFormat::dateConverDmyToYmd($this->last_purchase_from,'-');
            $criteria->addCondition('t.last_purchase >= "'.$lastPurchaseFrom.'"');
        }
        if(!empty($this->last_purchase_to)){
            $lastPurchaseTo = MyFormat::dateConverDmyToYmd($this->last_purchase_to,'-');
            $criteria->addCondition('t.last_purchase <= "'.$lastPurchaseTo.'"');
        }
        
        $dateLimit = MyFormat::modifyDays(date('Y-m-d'), $this->monthData, '-', 'month');
//        if(!empty($this->storehouse_id) && in_array($this->storehouse_id, $mAppPromotion->getListUidPttt())){// Oct1718 xử lý đội anh đợt
        if(!empty($this->storehouse_id)){// Dec2818 xử lý telesale goi lai App của PTTT
//            open all for telesale call
            $this->handleSearchPromotion($criteria);
        }elseif((!isset($_GET['type']) && !$this->fromPttt) || ($this->isCallCenter() && !$this->fromPttt)) {
            $criteria->addCondition('t.last_purchase <= "'.$dateLimit.'"');// không cho search KH trong 3 tháng trc đây
        }

        if(!empty($this->is_buy_complete)){
            $tableFollow  = FollowCustomer::model()->tableName();
            if($this->is_buy_complete == self::STT_BUY_COMPLETE){
                $criteria->join = 'JOIN '.$tableFollow.' ON '.$tableFollow.'.employee_id = t.sale_id AND '.$tableFollow.'.customer_id = t.id';
                $criteria->addCondition("{$tableFollow}.sell_id > 0");
//                $criteria->addCondition('EXISTS (SELECT id FROM '.$tableFollow.' WHERE '.$tableFollow.'.employee_id = t.sale_id AND '.$tableFollow.'.customer_id = t.id AND '.$tableFollow.'.sell_id > 0 )');
            } 
            if($this->is_buy_complete == self::STT_BUY_UNCOMPLETE){
                $criteria->join = 'LEFT JOIN '.$tableFollow.' ON '.$tableFollow.'.employee_id = t.sale_id AND '.$tableFollow.'.customer_id = t.id';
                $criteria->addCondition("{$tableFollow}.sell_id > 0");
		$criteria->addCondition($tableFollow.'.customer_id IS NULL');
//                $criteria->addCondition('NOT EXISTS (SELECT id FROM '.$tableFollow.' WHERE '.$tableFollow.'.employee_id = t.sale_id AND '.$tableFollow.'.customer_id = t.id AND '.$tableFollow.'.sell_id > 0 )');
            }
        }
        
        $criteria->addCondition('t.role_id = '.ROLE_CUSTOMER);
        $aHgdType = [STORE_CARD_HGD, STORE_CARD_HGD_CCS, UsersExtend::STORE_CARD_HGD_APP];
        if($cRole == ROLE_TELESALE){
//            $aHgdType = [STORE_CARD_HGD, STORE_CARD_HGD_CCS];
//            if(isset($_GET['type'])){
//                $aHgdType[] = UsersExtend::STORE_CARD_HGD_APP;
//            }
        }elseif($this->isCallCenter()){
//            $aHgdType = [UsersExtend::STORE_CARD_HGD_APP];
//            $criteria->addCondition('t.area_code_id<>768408');// khóa ĐL BT3 lại, để telesale gọi xong rồi mở
//            $criteria->addCondition('t.area_code_id=1');// Sep0518 khóa Callcenter lại
        }

        $sParamsIn = implode(',', $aHgdType);
        $criteria->addCondition("t.is_maintain IN ($sParamsIn)");
        if($this->fromPttt){
            $criteria->addCondition('t.last_purchase IS NULL');
        }
        GasAgentCustomer::addInConditionAgent($criteria, 't.area_code_id');
//        Nam 17042018
        $this->setTypeSearch($criteria);
        $this->limitByProvince($criteria);
        $this->moreSearch($criteria);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => $this->pageSize,
            ),
        ));
    }
    
    /** @Author: NamNH Jul 18,2019
     *  @Todo: more search users
     **/
    public function moreSearch(&$criteria) {
//        created_date model users
        if(!empty($this->date_created_to)){
            $date_to = MyFormat::dateConverDmyToYmd($this->date_created_to,"-");
            $date_to = MyFormat::modifyDays($date_to, 1);
            DateHelper::searchSmaller($date_to, 'created_date_bigint', $criteria);
        }
        if(!empty($this->date_created_from)){
            $date_from = MyFormat::dateConverDmyToYmd($this->date_created_from,"-");
            DateHelper::searchGreater($date_from, 'created_date_bigint', $criteria);
        }
        if(isset($this->count_sell) && $this->count_sell != ''){
            $tableFollow = UsersInfo::model()->tableName();
            $criteria->join .= ' JOIN '.$tableFollow.' ON '.$tableFollow.'.user_id = t.id';
            $criteria->addCondition($tableFollow.'.count_sell = '.$this->count_sell);
        }
    }
    /** @Author: ANH DUNG Now 17, 2018
     *  @Todo: xử lý search by promotion code 
     * @param: $this->storehouse_id is string code like: 24h001, 8686...
     **/
    public function handleSearchPromotion(&$criteria) {
        if(empty($this->storehouse_id)){
            return ;
        }
        $mAppPromotion = new AppPromotion();
        $mAppPromotion->code_no = $this->storehouse_id;
        $mAppPromotion = $mAppPromotion->getByCodeOnly();
        if(empty($mAppPromotion)){
            return ;
        }
        $criteria->addCondition('t.created_by = '. $mAppPromotion->owner_id);
    }
    
    /** @Author: DungNT Jan 16, 2019
     **/
    public function limitByZone() {
        $cUid = MyFormat::getCurrentUid();
        if($cUid == Telesale::UID_LAU_NV){
            $this->provinceExt = [GasProvince::TINH_BINH_DUONG];
            $this->province_id = GasProvince::TINH_BINH_DUONG;
        }
    }
    public function limitByProvince(&$criteria) {
        if(is_array($this->provinceExt)){
            $sParamsIn = implode(',', $this->provinceExt);
            $criteria->addCondition("t.province_id IN ($sParamsIn)");
        }elseif(!empty ($this->provinceExt)){
            $criteria->compare('t.province_id', $this->provinceExt);
        }
        $criteria->compare('t.province_id', $this->province_id);
    }
    
    
    /** @Author: HOANG NAM 17/04/2018
     *  @Todo: set parame type search
     **/
    public function setTypeSearch(&$criteria){
	$type           = isset($_GET['type']) ? $_GET['type'] : -1;
	$tableFollow    = FollowCustomer::model()->tableName();
        $cRole          = MyFormat::getCurrentRoleId();
        switch ($this->status_customer){
            case self::STT_WRONG_NUMBER :
                $criteria->join = 'JOIN '.$tableFollow.' ON '.$tableFollow.'.customer_id = t.id';
                $criteria->addCondition($tableFollow.'.status = '.self::STT_WRONG_NUMBER );
                break;

            case self::STT_SAME_ADDRESS:
                $criteria->join = 'JOIN '.$tableFollow.' ON '.$tableFollow.'.customer_id = t.id';
                $criteria->addCondition($tableFollow.'.status = '.self::STT_SAME_ADDRESS);
                break;

            case self::STT_REFUSE:
                $criteria->join = 'JOIN '.$tableFollow.' ON '.$tableFollow.'.customer_id = t.id';
                $criteria->addCondition($tableFollow.'.status = '.self::STT_REFUSE);
                if(isset($this->status_customer_cancel) && $this->status_customer_cancel != ''){
                    $criteria->addCondition($tableFollow.'.status_cancel = '.$this->status_customer_cancel);
                }
                break;

            case self::STT_CALL_AGAIN:
                $criteria->join = 'JOIN '.$tableFollow.' ON '.$tableFollow.'.customer_id = t.id';
                $criteria->addCondition($tableFollow.'.status = '.self::STT_CALL_AGAIN);
                break;

            case self::STT_CALLED_AGAIN:
                $criteria->join = 'JOIN '.$tableFollow.' ON '.$tableFollow.'.customer_id = t.id';
                $criteria->addCondition($tableFollow.'.status = '.self::STT_CALLED_AGAIN);
                break;

            case self::STT_TRACK_PURCHASE:
                $criteria->join = 'JOIN '.$tableFollow.' ON '.$tableFollow.'.customer_id = t.id';
                $criteria->addCondition($tableFollow.'.status = '.self::STT_TRACK_PURCHASE);
                break;

            case self::STT_BOUGHT:
                $criteria->join = 'JOIN '.$tableFollow.' ON '.$tableFollow.'.customer_id = t.id';
                $criteria->addCondition("$tableFollow.status = ".self::STT_BOUGHT . " OR $tableFollow.sell_id > 0");
                break;
            case self::STT_UPDATE_STATUS:
                $criteria->join = 'JOIN '.$tableFollow.' ON '.$tableFollow.'.customer_id = t.id';
                $criteria->addCondition($tableFollow.'.status = 0 ');
                break;
            
            case self::STT_NOT_USE_GAS:
                $criteria->join = 'JOIN '.$tableFollow.' ON '.$tableFollow.'.customer_id = t.id';
                $criteria->addCondition($tableFollow.'.status = '.self::STT_NOT_USE_GAS);
                break;
            
            case self::STT_TRACK_PURCHASE_APP:
                $criteria->join = 'JOIN '.$tableFollow.' ON '.$tableFollow.'.customer_id = t.id';
                $criteria->addCondition($tableFollow.'.status = '.self::STT_TRACK_PURCHASE_APP);
                break;
            
            case self::STT_PHONE_NOT_CONTACT:
                $criteria->join = 'JOIN '.$tableFollow.' ON '.$tableFollow.'.customer_id = t.id';
                $criteria->addCondition($tableFollow.'.status = '.self::STT_PHONE_NOT_CONTACT);
                break;
        }
        if($cRole == ROLE_TELESALE){
            $this->handleLimitTelesale($type, $tableFollow, $criteria);
        }elseif($this->isCallCenter()){
            $this->handleLimitCallCenter($type, $tableFollow, $criteria);
        }
        $this->handleLimitTelesale($type, $tableFollow, $criteria);
        if(!empty($this->date_call_from)){
            $dateFrom = MyFormat::dateConverDmyToYmd($this->date_call_from,'-');
            $criteria->join = 'JOIN '.$tableFollow.' ON '.$tableFollow.'.customer_id = t.id';
            $criteria->addCondition($tableFollow.'.appointment_date >= "'.$dateFrom.'"');
        }
        if(!empty($this->date_call_to)){
            $dateTo = MyFormat::dateConverDmyToYmd($this->date_call_to,'-');
            $criteria->join = 'JOIN '.$tableFollow.' ON '.$tableFollow.'.customer_id = t.id';
            $criteria->addCondition($tableFollow.'.appointment_date <= "'.$dateTo.'"');
        }
        
        if(!empty($this->callFrom)){
            $dateFrom = MyFormat::dateConverDmyToYmd($this->callFrom,'-');
            $criteria->join = 'JOIN '.$tableFollow.' ON '.$tableFollow.'.customer_id = t.id';
            $criteria->addCondition($tableFollow.'.created_date_only >= "'.$dateFrom.'"');
        }
        if(!empty($this->callTo)){
            $dateTo = MyFormat::dateConverDmyToYmd($this->callTo,'-');
            $criteria->join = 'JOIN '.$tableFollow.' ON '.$tableFollow.'.customer_id = t.id';
            $criteria->addCondition($tableFollow.'.created_date_only <= "'.$dateTo.'"');
        }
        if(!empty($this->date_call_from) || !empty($this->date_call_to)){
            $criteria->addCondition($tableFollow.'.status = '.self::STT_CALL_AGAIN);
        }
        
    }
    
    /** @Author: ANH DUNG Jun 26, 2018
     *  @Todo: xử lý giới hạn miền dữ liệu với loại telesale vs callcenter
     **/
    public function handleLimitTelesale($type, $tableFollow, &$criteria) {
        switch ($type){
	    case $this->called:
		$criteria->join = "JOIN $tableFollow ON $tableFollow.employee_id = t.sale_id AND $tableFollow.customer_id = t.id";
	//                $criteria->addCondition('EXISTS (SELECT id FROM '.$tableFollow.' WHERE '.$tableFollow.'.employee_id = t.sale_id AND '.$tableFollow.'.customer_id = t.id )');
		break;
	    case $this->not_call:
		$criteria->addCondition('t.sale_id > 0 OR t.storehouse_id > 0');
//		$criteria->join = "LEFT JOIN $tableFollow ON $tableFollow.employee_id = t.sale_id AND $tableFollow.customer_id = t.id";
//		$criteria->addCondition($tableFollow.'.customer_id IS NULL');
//		Jan 11, 2019 NamNH change join to addInCondition
                $aIdCustomer = $this->findIdCustomerNotCall();
                $criteria->addInCondition('t.id', $aIdCustomer);
	//                $criteria->addCondition('NOT EXISTS (SELECT id FROM '.$tableFollow.' WHERE '.$tableFollow.'.employee_id = t.sale_id AND '.$tableFollow.'.customer_id = t.id )');
		break;
            case $this->call_again:
//                $criteria->join = 'JOIN '.$tableFollow.' ON '.$tableFollow.'.customer_id = t.id';
//                $criteria->addCondition($tableFollow.'.status = '.self::STT_CALL_AGAIN);
//                $today = date('Y-m-d'); // AnhDung close May0718 - đã bổ sung ở bên dưới
//                $criteria->addCondition($tableFollow.'.appointment_date = "'.$today.'"');
                break;
                
	    default :
                if(empty($this->storehouse_id)){
                    $criteria->addCondition('t.sale_id < 1');
                }else{
                    $criteria->addCondition('t.storehouse_id < 1');
                }
		break;
	}
    }
    /** @Author: ANH DUNG Jun 26, 2018
     *  @Todo: xử lý giới hạn miền dữ liệu với loại telesale vs callcenter
     **/
    public function handleLimitCallCenter($type, $tableFollow, &$criteria) {
        $mAppCache = new AppCache();
        switch ($type){
	    case $this->called:
		$criteria->join = 'JOIN '.$tableFollow.' ON '.$tableFollow.'.employee_id = t.sale_id AND '.$tableFollow.'.customer_id = t.id';
	//                $criteria->addCondition('EXISTS (SELECT id FROM '.$tableFollow.' WHERE '.$tableFollow.'.employee_id = t.sale_id AND '.$tableFollow.'.customer_id = t.id )');
		break;
	    case $this->not_call:
		$criteria->addCondition('t.sale_id > 0');
//		$criteria->join = 'LEFT JOIN '.$tableFollow.' ON '.$tableFollow.'.employee_id = t.sale_id AND '.$tableFollow.'.customer_id = t.id';
//		$criteria->addCondition($tableFollow.'.customer_id IS NULL');
//		Jan 11, 2019 NamNH change join to addInCondition
		$aIdCustomer = $this->findIdCustomerNotCall();
		$criteria->addInCondition('t.id',$aIdCustomer);
	//                $criteria->addCondition('NOT EXISTS (SELECT id FROM '.$tableFollow.' WHERE '.$tableFollow.'.employee_id = t.sale_id AND '.$tableFollow.'.customer_id = t.id )');
		break;
            case $this->call_again:
//                $criteria->join = 'JOIN '.$tableFollow.' ON '.$tableFollow.'.customer_id = t.id';
//                $criteria->addCondition($tableFollow.'.status = '.self::STT_CALL_AGAIN);
//                $today = date('Y-m-d'); // AnhDung close May0718 - đã bổ sung ở bên dưới
//                $criteria->addCondition($tableFollow.'.appointment_date = "'.$today.'"');
                break;
	    default :
                $aCallCenter    = $mAppCache->getListdataUserByRole(ROLE_CALL_CENTER);
                $criteria->addCondition("t.sale_id NOT IN (". implode(',', array_keys($aCallCenter)).")");
		break;
	}
    }

    
    /** @Author: HOANG NAM 02/05/2018
     *  @Todo: get list employees
     **/
    public function getArrEmployeesBySearch($role_id)
    {
        $criteria = new CDbCriteria;
        if(is_array($role_id)){
            $criteria->addInCondition('t.role_id', $role_id);   
        }else{
            $criteria->compare('t.role_id', $role_id);
        }
        $criteria->compare('t.status', STATUS_ACTIVE);
        if(!empty($this->customer_id)){
            $criteria->compare('t.id', $this->customer_id);
        }
        $criteria->order = 'code_bussiness ASC';
        $models = self::model()->findAll($criteria);		
        return  CHtml::listData($models,'id','first_name');            
    }
    
    /** @Author: ANH DUNG Jun 21, 2018
     *  @Todo: get list district lock, hidden with user
     **/
    public function getListDistrictLock() {
        return [
//            13, // Quận Tân Bình
        ];
    }
    
    public function getNewestNote() {
        $criteria = new CDbCriteria;
        $criteria->addCondition('customer_id = '.$this->id);
        $criteria->order = 'id DESC';
        $model          = FollowCustomer::model()->find($criteria);
        if(empty($model)){
            return '';
        }
        $aCallStt       = $this->getArrayCallStatus();
        $aCallSttCancel = $this->getArrayStatusCancel();
        $sttCancel = '';
        $callStatus = isset($aCallStt[$model->status]) ? $aCallStt[$model->status] : '';
        if($model->status_cancel > 0){
            $sttCancel = '<br>Lý do hủy: '.$aCallSttCancel[$model->status_cancel];
        }
        return "<br><b>Trạng thái: ".$callStatus.$sttCancel.'</b><br>'.$model->note;
    }
    
    public function getLinkOrder() {
        $criteria = new CDbCriteria;
        $criteria->addCondition('sell_id > 0 AND customer_id = '.$this->id);
        $model = FollowCustomer::model()->find($criteria);
        $sellUrl = '';
        if(!empty($model)){
            $sellUrl = '<br><a target="_blank" href="'
                    .Yii::app()->createAbsoluteUrl('admin/sell/view',array('id'=>$model->sell_id))
                    . '">Xem đơn hàng</a>';
        }
        return $sellUrl;
    }
    
    public function getActionsForNote() {
        $createNoteUrl  = Yii::app()->createAbsoluteUrl('admin/followCustomer/create/id');
        $viewNoteUrl    = Yii::app()->createAbsoluteUrl('admin/followCustomer/viewMultiNote/id');
        $cUrl = '<a class="createNote" '
                . ' href='.$createNoteUrl.'/'.$this->getCustomerId().'>Tạo ghi chú</a>';
        $vUrl = '<br><a class="createNote viewNote" style="display:'.$this->canViewNote().'"'
                . ' href='.$viewNoteUrl.'/'.$this->getCustomerId().'>Xem ghi chú</a>';
        if(!$this->checkRightsCreateNote()) $cUrl = '';
        $url = $cUrl.$vUrl;
        return isset($_GET['type']) ? $url : '';
    }
    
    public function checkRightsCreateNote() {
        $cRole      = MyFormat::getCurrentRoleId();
        $cUid       = MyFormat::getCurrentUid();
        
        if($cRole == ROLE_ADMIN) return true; 
        
        if($cUid == $this->sale_id || $cUid == $this->storehouse_id){
            return true;
        }
        return false;
    }
    
    /** @Author: ANH DUNG Aug 12, 2018
     *  @Todo: get all User have report code + BQV
     **/
    public function getListUserLikeTelesale(&$aTelesale) {
        // DuongNV 26/04/2019 comment, chạy cron tạo lịch tự động, 2 biến này ko dùng nên comment
//        $cRole = MyFormat::getCurrentRoleId();
//        $cUid = MyFormat::getCurrentUid();
        $aRoleLimit         = [ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_MONITORING_MARKET_DEVELOPMENT];
        $aUidViewTelesale   = [GasLeave::UID_CHIEF_MONITOR, GasConst::UID_PHUONG_ND];
        $mAppCache          = new AppCache();
        
        // get list NV PTTT anh đợt 
        if($this->fromPttt){
            $mOneMany               = new GasOneMany();
            $aRes = $mOneMany->getListdataByTypeAndOneId(GasOneMany::TYPE_MONITOR_CCS, $this->fromPttt);
//            $mAppPromotion          = new AppPromotion();
    //        $aModelUser             = Users::getArrayModelByArrayId($mAppPromotion->getListUidPttt());
//            $aModelUser             = Users::getArrayModelByArrayId($mAppPromotion->getListUidPttt());
//            $aUidPttt               = CHtml::listData($aModelUser, 'id', 'first_name');
//            if(in_array($cRole, $aRoleLimit) && !in_array($cUid, $aUidViewTelesale)){
//                return $aUidPttt;
//            }
        }else{
            $aRes                   = $mAppCache->getListdataUserByRole(ROLE_TELESALE);
//            Namnh Jul 19,2019 Thực hiện thêm nhân viên làm kế toán có gọi telesale 
            $mGasTargetMonthly      = new GasTargetMonthly();
            $MoreTelesale           = $mGasTargetMonthly->getTelesaleAccountantName(date('y-m-d'));
            $aRes                   += $MoreTelesale;
        }
//        $aRes                   += $mAppCache->getListdataUserByRole(ROLE_CALL_CENTER);// Close Dec2018
//        $aRes                   += $aUidPttt;
        
        $aTelesale['EMPLOYEE'] = $aRes;
        return $aRes;
    }

    /** @Author: HOANG NAM 02/05/2018
     *  @Todo: report gas back 
     * SELECT * FROM `gas_sell` WHERE `sale_id` =146 and created_date_only >='2018-08-01' and created_date_only <='2018-08-31' and source <>2 and status=2 and first_order=1 and support_id=1
     * */
    public function getGasBack($isCron = false) {
        if(!$isCron){ // DuongNV 26/04/2019 Chạy cron init data tự động tạo lịch
            $_SESSION['data-model-gas-back'] = $this;
        }
        $this->date_from_ymd  = MyFormat::dateConverDmyToYmd($this->date_from, '-');
        $this->date_to_ymd    = MyFormat::dateConverDmyToYmd($this->date_to, '-');
        
        $aTelesale = [];
        $this->getListUserLikeTelesale($aTelesale);
        $aTelesale['ARRAY_DATE']    = MyFormat::getArrayDay($this->date_from_ymd, $this->date_to_ymd);
        $this->getGasFollow($aTelesale,$aTelesale['EMPLOYEE']);
        $this->getGasSell($aTelesale,$aTelesale['EMPLOYEE']);
        $this->getGasSellSourceApp($aTelesale,$aTelesale['EMPLOYEE']);
        $this->getGasSellSourceAppWithoutCall($aTelesale,$aTelesale['EMPLOYEE']);
        $this->getTelesaleCodeCallOut($aTelesale,$aTelesale['EMPLOYEE']);
        //@Code: NAM013
        $this->getReportGasReferralTracking($aTelesale, $aTelesale['EMPLOYEE']);
//        report count customer
        $this->getSellHaveCallOut($aTelesale,$aTelesale['EMPLOYEE']);
        return $aTelesale;
    }
    
    /** @Author: HOANG NAM 02/05/2018
     *  @Todo: count telesale call out each employee
     * */
    public function getGasFollow(&$aRe, $aEmployee) {
        $aRe['OUT_FOLLOW']=[];
        $aRe['OUT_FOLLOW_CHART']=[];
        $criteria = new CDbCriteria();
        if(!empty($this->customer_id)){
            $criteria->compare('t.employee_id', $this->customer_id);
        }
        $criteria->addCondition("t.created_date_only >=  '{$this->date_from_ymd}'");
        $criteria->addCondition("t.created_date_only <=  '{$this->date_to_ymd}'");
        if($this->isRemoveHoliday){
            $aHoliday = $this->getHolidayTelesale();
            $criteria->addNotInCondition('t.created_date_only',$aHoliday);
        }
        $criteria->addCondition("t.bill_duration >= ". Telesale::MIN_TIME_OF_CALL);
        $criteria->select   = "t.employee_id,t.created_date_only,count(t.id) as number_customer";
        $criteria->group = "t.employee_id,t.created_date_only";
        if(count($aEmployee)){
            $criteria->addCondition("t.employee_id IN (". implode(',', array_keys($aEmployee)).")");
        }
        $aFollowCustomer = FollowCustomer::model()->findAll($criteria);
        foreach ($aFollowCustomer as $key => $mFollowCustomer) {
            if(isset($aRe['OUT_FOLLOW'][$mFollowCustomer->employee_id][$mFollowCustomer->created_date_only])){
                $aRe['OUT_FOLLOW'][$mFollowCustomer->employee_id][$mFollowCustomer->created_date_only] += $mFollowCustomer->number_customer;
            }else {
                $aRe['OUT_FOLLOW'][$mFollowCustomer->employee_id][$mFollowCustomer->created_date_only] = $mFollowCustomer->number_customer;
            }
            //@Code: NAM013
            if(isset($aRe['SUM_OUT_FOLLOW']['SUM'])){
                $aRe['SUM_OUT_FOLLOW']['SUM'] +=  $mFollowCustomer->number_customer;
            }else{
                $aRe['SUM_OUT_FOLLOW']['SUM'] =  $mFollowCustomer->number_customer;
            }
            if(isset($aRe['SUM_OUT_FOLLOW']['DATE'][$mFollowCustomer->created_date_only])){
                $aRe['SUM_OUT_FOLLOW']['DATE'][$mFollowCustomer->created_date_only] +=  $mFollowCustomer->number_customer;
            }else{
                $aRe['SUM_OUT_FOLLOW']['DATE'][$mFollowCustomer->created_date_only] =  $mFollowCustomer->number_customer;
            }
        }
        $this->getCallOutCustomerDraft($aRe, $aEmployee);
        
        foreach ($aEmployee as $key => $nameEmployee) {
            $aRe['OUT_FOLLOW_CHART'][$key]['name'] = $nameEmployee;
            $aRe['OUT_FOLLOW_CHART'][$key]['value'] =isset($aRe['OUT_FOLLOW'][$key]) ?  array_sum($aRe['OUT_FOLLOW'][$key]) : 0;
        }
//        uasort($aRe['OUT_FOLLOW_CHART'], function ($item1, $item2) {
//            return $item2['value'] >= $item1['value'];
//        });// close AnhDung Sep1418
        
//        return $aRe;
    }
    
    /** @Author: ANH DUNG Jul 03, 2018
     *  @Todo: count call out from customer draft
     **/
    public function getCallOutCustomerDraft(&$aRe, $aEmployee) {
        $criteria = new CDbCriteria();
        if(!empty($this->customer_id)){
            $criteria->compare('t.employee_id', $this->customer_id);
        }
        $criteria->addCondition("t.created_date_only >=  '{$this->date_from_ymd}'");
        $criteria->addCondition("t.created_date_only <=  '{$this->date_to_ymd}'");
        $criteria->addCondition("t.bill_duration > ". Telesale::MIN_TIME_OF_CALL);
        $criteria->select   = "t.employee_id, t.created_date_only, count(t.id) as number_customer";
        $criteria->group    = "t.employee_id, t.created_date_only";
        if(count($aEmployee)){
            $criteria->addCondition("t.employee_id IN (". implode(',', array_keys($aEmployee)).")");
        }
        $aFollowCustomer = CustomerDraftDetail::model()->findAll($criteria);
        foreach ($aFollowCustomer as $key => $mFollowCustomer) {
            if(isset($aRe['OUT_FOLLOW'][$mFollowCustomer->employee_id][$mFollowCustomer->created_date_only])){
                $aRe['OUT_FOLLOW'][$mFollowCustomer->employee_id][$mFollowCustomer->created_date_only] += $mFollowCustomer->number_customer;
            }else {
                $aRe['OUT_FOLLOW'][$mFollowCustomer->employee_id][$mFollowCustomer->created_date_only] = $mFollowCustomer->number_customer;
            }
            //@Code: NAM013
            if(isset($aRe['SUM_OUT_FOLLOW']['SUM'])){
                $aRe['SUM_OUT_FOLLOW']['SUM'] +=  $mFollowCustomer->number_customer;
            }else{
                $aRe['SUM_OUT_FOLLOW']['SUM'] =  $mFollowCustomer->number_customer;
            }
            if(isset($aRe['SUM_OUT_FOLLOW']['DATE'][$mFollowCustomer->created_date_only])){
                $aRe['SUM_OUT_FOLLOW']['DATE'][$mFollowCustomer->created_date_only] +=  $mFollowCustomer->number_customer;
            }else{
                $aRe['SUM_OUT_FOLLOW']['DATE'][$mFollowCustomer->created_date_only] =  $mFollowCustomer->number_customer;
            }
        }
    }
    
    
    /** @Author: HOANG NAM 02/05/2018
     *  @Todo: get Output of Sell
     * */
    public function getGasSell(&$aRe,$aEmployee) {
        $mAppPromotionUser = new AppPromotionUser();
        $mAppPromotionUser->unsetAttributes();
        $mAppPromotionUser->aEmployee       = $aEmployee;
        $mAppPromotionUser->date_from_ymd   = MyFormat::dateConverDmyToYmd($this->date_from, '-');
        $mAppPromotionUser->date_to_ymd     = MyFormat::dateConverDmyToYmd($this->date_to, '-');
        if($this->getOrdersSecond){
            $mAppPromotionUser->getFirstOrder   = false;
        }

        $mAppPromotionUser->getGasSell($aRe);
        return ;// Jun1318 AnhDung chuyển đổi sử dụng 1 hàm để tính toán BQV cho 2 báo cáo.
        /* 1. admin/telesale/reportGasBack
         * 2. admin/appPromotionUser/reportMarketing
         */
    }
    
    
    
    /** @Author: ANH DUNG Jun 06, 2018
     *  @Todo: get same condition Sell
     **/
    public function getSameConditionSell(&$criteria, $aEmployee) {
        if(!empty($this->customer_id)){
            $criteria->compare('t.sale_id', $this->customer_id);
        }
        $criteria->addCondition("t.created_date_only >=  '{$this->date_from_ymd}'");
        $criteria->addCondition("t.created_date_only <=  '{$this->date_to_ymd}'");
        
        $criteria->addCondition("t.status = ".Sell::STATUS_PAID);
        if(count($aEmployee)){
            $criteria->addCondition("t.sale_id IN (". implode(',', array_keys($aEmployee)).")");
        }
        if(!$this->getOrdersSecond){
            $criteria->addCondition('t.first_order = '.Sell::IS_FIRST_ORDER);// ở đây chỉ thống kê telesale - nên sẽ tính BQV lần 2,3...
        }

    }
    
    /** @Author: ANH DUNG Jun 06, 2018
     *  @Todo: get Output of Sell source from app Gas24h
     * */
    public function getGasSellSourceApp(&$aRe,$aEmployee) {
        $aRe['OUT_SELL_APP'] = [];
        $aRe['OUT_SELL_APP_ALL'] = 0;
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.source = ".Sell::SOURCE_APP);
        $this->getSameConditionSell($criteria, $aEmployee);

        $criteria->select   = 't.sale_id,t.created_date_only,count(t.id) as MAX_ID';
        $criteria->group    = 't.sale_id,t.created_date_only';
        $aFollowCustomer    = Sell::model()->findAll($criteria);
        foreach ($aFollowCustomer as $key => $mFollowCustomer) {
            $aRe['OUT_SELL_APP'][$mFollowCustomer->sale_id][$mFollowCustomer->created_date_only] = $mFollowCustomer->MAX_ID;        
        }

        foreach ($aEmployee as $key => $nameEmployee) {
            $aRe['OUT_SELL_APP_SUM'][$key]['name']  = $nameEmployee;
            $aRe['OUT_SELL_APP_SUM'][$key]['value'] = isset($aRe['OUT_SELL_APP'][$key]) ?  array_sum($aRe['OUT_SELL_APP'][$key]) : 0;
            $aRe['OUT_SELL_APP_ALL']    += $aRe['OUT_SELL_APP_SUM'][$key]['value'];
        }
    }
    
    /** @Author: ANH DUNG Sep 13, 2018 - get bình app ko có CallOut
     *  @Todo: get Output of Sell source from App Gas24h without CallOut
     * */
    public function getGasSellSourceAppWithoutCall(&$aRe,$aEmployee) {
        $aRe['APP_WITHOUT_CALL'] = [];
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.source = '.Sell::SOURCE_APP);
        $criteria->addCondition('t.support_id = 0');
        $this->getSameConditionSell($criteria, $aEmployee);

        $criteria->select   = 't.sale_id,t.created_date_only,count(t.id) as MAX_ID';
        $criteria->group    = 't.sale_id,t.created_date_only';
        $aFollowCustomer    = Sell::model()->findAll($criteria);
        foreach ($aFollowCustomer as $key => $mFollowCustomer) {
            $aRe['APP_WITHOUT_CALL'][$mFollowCustomer->sale_id][$mFollowCustomer->created_date_only] = $mFollowCustomer->MAX_ID;        
            if(!isset($aRe['APP_WITHOUT_CALL_SUM'][$mFollowCustomer->sale_id])){
                $aRe['APP_WITHOUT_CALL_SUM'][$mFollowCustomer->sale_id] = $mFollowCustomer->MAX_ID;
            }else{
                $aRe['APP_WITHOUT_CALL_SUM'][$mFollowCustomer->sale_id] += $mFollowCustomer->MAX_ID;
            }
        }
    }
    
    /** @Author: ANH DUNG Sep 14, 2018
     *  @Todo: get data nhập code Telesale có CallOut
     * */
    public function getTelesaleCodeCallOut(&$aRe,$aEmployee) {
        $aData = [];
        $mAppPromotionUser = new AppPromotionUser();
        $mAppPromotionUser->unsetAttributes();
        $mAppPromotionUser->date_from_ymd   = MyFormat::dateConverDmyToYmd($this->date_from, '-');
        $mAppPromotionUser->date_to_ymd     = MyFormat::dateConverDmyToYmd($this->date_to, '-');
        if(!empty($this->customer_id)){
            $mAppPromotionUser->user_id = $this->customer_id;
        }
        $mAppPromotionUser->aEmployee   = $aEmployee;
        $mAppPromotionUser->getCallOut  = true;
        $mAppPromotionUser->getReportGasReferralTracking($aData);
        $aRe['TELESALE_CODE_CALLOUT'] = isset($aData['OUT_TRACKING']) ? $aData['OUT_TRACKING'] : [];
    }
    
    /** @Author: HOANG NAM 09/05/2018
     *  @Todo: get list Tracking, get số KH nhập mã của Telesale
     **/
    public function getReportGasReferralTracking(&$aData, $aEmployee){
        $mAppPromotionUser = new AppPromotionUser();
        $mAppPromotionUser->unsetAttributes();
        $mAppPromotionUser->date_from_ymd   = MyFormat::dateConverDmyToYmd($this->date_from, '-');
        $mAppPromotionUser->date_to_ymd     = MyFormat::dateConverDmyToYmd($this->date_to, '-');
        if(!empty($this->customer_id)){
            $mAppPromotionUser->user_id = $this->customer_id;
        }
//        $mAppPromotionUser->status_use = null;
        $mAppPromotionUser->aEmployee   = $aEmployee;
        $mAppPromotionUser->getReportGasReferralTracking($aData);
    }
    
    public function getArrayReportCancel() {
        return $this->getArrayStatusCancel();
    }
    
    /** @Author: HOANG NAM 28/05/2018
     *  @Todo: get cancel
     **/
    public function getCancel() {
        $aData = [];
        $aData['status'] = $this->getArrayReportCancel();
        $criteria = new CDbCriteria;
        if(!empty($this->date_from)){
            $criteria->addCondition('t.created_date_only >=  "'. MyFormat::dateConverDmyToYmd($this->date_from,'-').'"');
        }
        if(!empty($this->date_to)){
            $criteria->addCondition('t.created_date_only <=  "'. MyFormat::dateConverDmyToYmd($this->date_to,'-').'"');
        }
        $criteria->addCondition('t.status_cancel IN  ('. implode(',', array_keys($aData['status'])).')');
        if(!empty($this->customer_id)){
            $criteria->compare('t.employee_id', $this->customer_id);
        }
        $criteria->compare('t.status', self::STT_REFUSE); // lấy dánh sách từ chối
        $criteria->select = 't.employee_id,t.status_cancel,count(t.id) as id';
        $criteria->group = 't.employee_id,t.status_cancel';
        
        $aFollowCustomer =  FollowCustomer::model()->findAll($criteria);
        foreach ($aFollowCustomer as $key => $mFollowCustomer) {
//            lấy danh sách tên nhân viên
            if(!isset($aData['employee'][$mFollowCustomer->employee_id])){
                $aData['employee'][$mFollowCustomer->employee_id] = $mFollowCustomer->getEmployeeInfo();
            }
//           tính toán tổng của từng nhân viên
            if(!isset($aData['View'][$mFollowCustomer->employee_id]['Sum'])){
                $aData['View'][$mFollowCustomer->employee_id]['Sum'] = $mFollowCustomer->id;
            }else{
                $aData['View'][$mFollowCustomer->employee_id]['Sum'] += $mFollowCustomer->id;
            }
//            tính toán tổng từng nhân viên them từng loại
            if(!isset($aData['View'][$mFollowCustomer->employee_id]['Detail'][$mFollowCustomer->status_cancel])){
                $aData['View'][$mFollowCustomer->employee_id]['Detail'][$mFollowCustomer->status_cancel] = $mFollowCustomer->id;
            }else{
                $aData['View'][$mFollowCustomer->employee_id]['Detail'][$mFollowCustomer->status_cancel] += $mFollowCustomer->id;
            }
//            tính toán tổng tất cả nhân viên
            if(!isset($aData['sum'][$mFollowCustomer->status_cancel])){
                $aData['sum'][$mFollowCustomer->status_cancel] = $mFollowCustomer->id;
            }else{
                $aData['sum'][$mFollowCustomer->status_cancel] += $mFollowCustomer->id;
            }

        }
        return $aData;
    }
    
    
    /** @Author: ANH DUNG Jun 16, 2018
     *  @Todo: bc check cuộc gọi, input code và BQV của telesale 
     * 1. get all info KH nhập code của NV
     * 2. get all BQV App của Telesale đó
     * 3. get all cuộc gọi ra ứng với list số đt của KH
     **/
    public function rInputCode() {
        if(empty($this->customer_id)){
            return [];
        }
        $this->date_from_ymd        = MyFormat::dateConverDmyToYmd($this->date_from, '-');
        $this->date_to_ymd          = MyFormat::dateConverDmyToYmd($this->date_to, '-');
        $this->rData['ARR_CUSTOMER_PHONE'] = $this->rData['ARR_CUSTOMER_ID'] = [];
        
        
        $this->rInputCodeGetSellApp();
        $this->rInputCodeGetInfoRef();
        $_SESSION['rData'] = $this->rData;
        $_SESSION['model'] = $this;
        return $this->rData;
    }
    
    // 1. get all info KH nhập code của NV
    public function rInputCodeGetInfoRef() {
        // 1. get all info KH nhập code của NV
        $mRpGas24h = new RpGas24h();
        $mRpGas24h->date_from_ymd   = $this->date_from_ymd;
        $mRpGas24h->date_to_ymd     = $this->date_to_ymd;
        $mRpGas24h->user_id         = $this->customer_id;
        $mRpGas24h->aEmployee       = [];
        
        $aReferralOfUser = $mRpGas24h->getReferralOfUser();
        $aReferralOfUser = CHtml::listData($aReferralOfUser, 'invited_id', 'invited_id');
        
        $this->rData['ARR_CUSTOMER_ID'] = $this->rData['ARR_CUSTOMER_ID'] + $aReferralOfUser;
        if(count($this->rData['ARR_CUSTOMER_ID']) < 1 ){
            return [];
        }
        $needMore['order'] = 't.id DESC';
        $this->rData['ARR_CUSTOMER'] = Users::getArrObjectUserByRole(ROLE_CUSTOMER, $this->rData['ARR_CUSTOMER_ID'], $needMore);
        foreach($this->rData['ARR_CUSTOMER'] as $mUser){
            $tempPhone = explode('-', $mUser->phone);
            foreach($tempPhone as $phone){
                $this->rData['ARR_CUSTOMER_PHONE'][]  = $phone*1;
            }
        }
        
        // 3. get all cuộc gọi ra ứng với list số đt của KH trong khoảng ngày Search 
        $mCall = new Call();
        $mCall->direction = Call::DIRECTION_OUTBOUND;
        $mCall->date_from_ymd   = $mRpGas24h->date_from_ymd;
        $mCall->date_to_ymd     = $mRpGas24h->date_to_ymd;
        $aCall = $mCall->getByListPhoneNumber($this->rData['ARR_CUSTOMER_PHONE']);
        foreach($aCall as $mCall){
            $this->rData['ARR_CALL_RECORD'][$mCall->destination_number] = $mCall->created_date;
        }
    }
    
    // 2. get all BQV App của Telesale đó
    public function rInputCodeGetSellApp() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.sale_id=' . $this->customer_id);
//        $criteria->addCondition('t.source=' . Sell::SOURCE_APP);// lấy hết cả KH gọi điện 
        $criteria->addBetweenCondition('t.created_date_only', $this->date_from_ymd, $this->date_to_ymd); 
        $aSell = Sell::model()->findAll($criteria);
        foreach($aSell as $mSell){
            $this->rData['ARR_SELL'][$mSell->customer_id] = $mSell;
            $this->rData['ARR_CUSTOMER_ID'][$mSell->customer_id] = $mSell->customer_id;
        }
    }
        
    /** @Author: HOANG NAM 14/08/2018
     *  @Todo: tìm đơn hàng có cuộc gọi ra của Telesale
     **/
    public function getSellHaveCallOut(&$aRe, $aEmployee) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.support_id = 1');
        $this->getSameConditionSell($criteria, $aEmployee);

        $criteria->select   = 't.sale_id,t.created_date_only, count(t.id) as MAX_ID, SUM(t.total) as total';
        $criteria->group    = 't.sale_id,t.created_date_only';
        $aFollowCustomer    = Sell::model()->findAll($criteria);
        foreach ($aFollowCustomer as $key => $mFollowCustomer) {
            $aRe['SELL_CALLOUT'][$mFollowCustomer->sale_id][$mFollowCustomer->created_date_only] = $mFollowCustomer->MAX_ID;
            $aRe['REVENUE_HAVE_CALLOUT'][$mFollowCustomer->sale_id][$mFollowCustomer->created_date_only] = $mFollowCustomer->total;
            if(!isset($aRe['OUT_SELL_FOLLOW'][$mFollowCustomer->sale_id])){
                $aRe['OUT_SELL_FOLLOW'][$mFollowCustomer->sale_id] = $mFollowCustomer->MAX_ID;
            }else{
                $aRe['OUT_SELL_FOLLOW'][$mFollowCustomer->sale_id] += $mFollowCustomer->MAX_ID;
            }
        }
        
        $aRe['SUM_ALL_REVENUE_HAVE_CALLOUT'] = 0;// doanh thu đơn hàng có gọi ra
        foreach ($aEmployee as $key => $nameEmployee) {
            $aRe['SUM_REVENUE_HAVE_CALLOUT'][$key]   = isset($aRe['REVENUE_HAVE_CALLOUT'][$key]) ?  array_sum($aRe['REVENUE_HAVE_CALLOUT'][$key]) : 0;
            $aRe['SUM_ALL_REVENUE_HAVE_CALLOUT']    += $aRe['SUM_REVENUE_HAVE_CALLOUT'][$key];
        }
    }
    
    /** @Author: KHANH TOAN 18/09/2018
     *  @Todo: get Output of Report TeleSale
     * */
    public function getGasSellReport(&$aRe, $aEmployee) {
        $mAppPromotionUser = new AppPromotionUser();
        $mAppPromotionUser->unsetAttributes();
        $mAppPromotionUser->aEmployee       = $aEmployee;
        $mAppPromotionUser->date_from_ymd   = MyFormat::dateConverDmyToYmd($this->date_from, '-');
        $mAppPromotionUser->date_to_ymd     = MyFormat::dateConverDmyToYmd($this->date_to, '-');
        if($this->fromPttt == Telesale::BQV_GDKV){
//            $mAppPromotionUser->getFirstOrder = false;// close Dec0118 chỉ tính bình đầu
        }
        $mAppPromotionUser->getGasSellReport($aRe);
        return ;// Sep1818 KhanhToan chuyển đổi sử dụng 1 hàm để tính toán Code/BQV
        /* 1. admin/gas24hReport/ReportTeleSale
         */
    }
    
    /** @Author: KHANH TOAN 2018
     *  @Todo: report telesale Code/BQV for agent
     *  @Param: 
     **/
    public function getReportTeleSale() {
        $mAppPromotion = new AppPromotion();
        $_SESSION['model-gas-back'] = $this;
        $this->date_from_ymd  = MyFormat::dateConverDmyToYmd($this->date_from, '-');
        $this->date_to_ymd    = MyFormat::dateConverDmyToYmd($this->date_to, '-');
        $mAppPromotion          = new AppPromotion();
        $aTelesale = []; $backupFromPttt = $this->fromPttt;
        $mAppCache              = new AppCache();
        if($this->fromPttt == Telesale::BQV_TELESALE){
            $aTelesale['EMPLOYEE'] = $mAppCache->getListdataUserByRole(ROLE_TELESALE);
        }elseif($this->fromPttt != Telesale::BQV_TELESALE){
//            $aTelesale['EMPLOYEE'] = Users::getListOptions($mAppPromotion->getListUidPtttAnhDot());
            $mOneMany               = new GasOneMany();
            $aTelesale['EMPLOYEE']  = $mOneMany->getListdataByTypeAndOneId(GasOneMany::TYPE_MONITOR_CCS, $this->fromPttt);
        }elseif(array_key_exists($this->fromPttt, $mAppPromotion->getArrayPartnerFixCode())){// This line is remove Dec2018, it not need run any more
            $aSubOwner  = $mAppPromotion->getByOwnerIdRoot($this->fromPttt);
            $aIdPvkh    = CHtml::listData($aSubOwner, 'owner_id', 'owner_id');
            $aCodePvkh  = CHtml::listData($aSubOwner, 'owner_id', 'code_no');
            $aTelesale['EMPLOYEE_FULLNAME'] = Users::getListOptions($aIdPvkh);
            $aTelesale['EMPLOYEE'] = $aCodePvkh;
            $this->fromPttt = Telesale::BQV_GDKV;
        }
        //get BQV group agent_id, sale_id
        $this->getGasSellReport($aTelesale, $aTelesale['EMPLOYEE']);
        //@Code: 
        $this->getReportGasReferralTrackingTeleSale($aTelesale, $aTelesale['EMPLOYEE']);
        $this->getNameAgent($aTelesale);
        $this->fromPttt = $backupFromPttt;
        $_SESSION['data-report-telesale'] = $aTelesale;
        return $aTelesale;
    }
    
    /** @Author: DuongNV Sep 20,2018
     *  @Todo: get html agent with link update solr in index view
     **/
    public function getAgentWithLink() {
        $normalInfo = $this->getAgentOfCustomer();
        $url = Yii::app()->createAbsoluteUrl("admin/telesale", ['UpdateSolr'=>'1','_id'=>$this->id]);
        $link = CHtml::ajaxLink(
            $text = 'Update Search', 
            $url = $url, 
            $ajaxOptions = array (
                'type'=>'POST',
                'dataType'=>'json',
                'beforeSend' => 'function() {           
                    $.blockUI({ overlayCSS: { backgroundColor: "#fff" } });
                 }',
                'success'=>'function(html){ $.unblockUI(); }'
            )
        );
        
        $link = "<a class='TelesaleUpdateSolr' href='javascript:;' next='$url'>Update Search</a>";
        $cUid = MyFormat::getCurrentUid();
        $aUidAllow = [GasConst::UID_VEN_NTB, GasConst::UID_ADMIN];
        if($this->showSetSaleId()){
            $normalInfo .= '<br>' . $link;
        }
        return $normalInfo;
    }
    
    /** @Author: KHANH TOAN 21/09/2018
     *  @Todo: get list Tracking, get số KH nhập mã của Telesale
     **/
    public function getReportGasReferralTrackingTeleSale(&$aData, $aEmployee){
        $mAppPromotionUser = new AppPromotionUser();
        $mAppPromotionUser->unsetAttributes();
        $mAppPromotionUser->date_from_ymd   = MyFormat::dateConverDmyToYmd($this->date_from, '-');
        $mAppPromotionUser->date_to_ymd     = MyFormat::dateConverDmyToYmd($this->date_to, '-');
        if(!empty($this->customer_id)){
            $mAppPromotionUser->user_id = $this->customer_id;
        }
        $mAppPromotionUser->aEmployee   = $aEmployee;
        $mAppPromotionUser->getReportGasReferralTrackingTeleSale($aData);
    }
    
    public function getNameAgent(&$aData) {
        $mAppCache          = new AppCache();
        $aAgent             = $mAppCache->getAgentListdata();
       //array tên từng đại lí
        foreach ($aAgent as $key => $nameAgent) {
            $aData['AGENT'][$key] = $nameAgent;
        }
    }
    
    /** @Author: NamNH Sep 26,2018
     *  @Todo: get report for call
     **/
    public function getReportCall(){
        $aData                  = [];
        $aData['EMPLOYEE_VIEW'] = [];
        $this->getListUserLikeTelesale($aData);
        
//        Đã gọi
        $criteriaCalled         = new CDbCriteria;
        $tableFollow            = FollowCustomer::model()->tableName();
        $this->setMore($criteriaCalled);
        $criteriaCalled->join   = 'JOIN '.$tableFollow.' ON '.$tableFollow.'.employee_id = t.sale_id AND '.$tableFollow.'.customer_id = t.id';
//	Đã nhận
        $criteriaGet            = new CDbCriteria;
        $this->setMore($criteriaGet);
        $criteriaGet->addCondition('t.sale_id > 0');
//        get data
//        đã gọi
        $aUsersCalled           = self::model()->findAll($criteriaCalled);
        foreach ($aUsersCalled as $key => $mUsers) {
            if(!isset($aData['EMPLOYEE'][$mUsers->sale_id])){
                continue;
            }
            $aData['EMPLOYEE_VIEW'][$mUsers->sale_id]   = isset($aData['EMPLOYEE'][$mUsers->sale_id]) ? $aData['EMPLOYEE'][$mUsers->sale_id] : '';
            if(!isset($aData['Called'][$mUsers->sale_id])){
                $aData['Called'][$mUsers->sale_id]  = $mUsers->id;
            }else{
                $aData['Called'][$mUsers->sale_id]  += $mUsers->id;
            }
        }
//        chưa gọi
        $aUsersGet           = self::model()->findAll($criteriaGet);
        foreach ($aUsersGet as $key => $mUsers) {
            if(!isset($aData['EMPLOYEE'][$mUsers->sale_id])){
                continue;
            }
            $aData['EMPLOYEE_VIEW'][$mUsers->sale_id]   = isset($aData['EMPLOYEE'][$mUsers->sale_id]) ? $aData['EMPLOYEE'][$mUsers->sale_id] : '';
            if(!isset($aData['Get'][$mUsers->sale_id])){
                $aData['Get'][$mUsers->sale_id]  = $mUsers->id;
            }else{
                $aData['Get'][$mUsers->sale_id]  += $mUsers->id;
            }
        }
//        sort
        uksort($aData['EMPLOYEE_VIEW'], function ($item1, $item2) use ($aData) {
            $item1_count    = isset($aData['Get'][$item1]) ? $aData['Get'][$item1] : 0;
            $item2_count    = isset($aData['Get'][$item2]) ? $aData['Get'][$item2] : 0;
            return $item1_count < $item2_count;
        });
        return $aData;
    }
    
    /** @Author: NamNH Sep 26, 2018
     *  @Todo: set more
     **/
    public function setMore(&$criteria){
        $criteria->select       = 't.sale_id,count(t.id) as id';               
        $criteria->compare('t.sale_id', $this->sale_id);
        $criteria->addCondition('t.role_id = '.ROLE_CUSTOMER);
        $aHgdType               = [STORE_CARD_HGD, STORE_CARD_HGD_CCS, UsersExtend::STORE_CARD_HGD_APP];
        // Jul3018 for call KH APP BT 3 
        $sParamsIn              = implode(',', $aHgdType);
        $criteria->addCondition("t.is_maintain IN ($sParamsIn)");
        $criteria->group        = 't.sale_id';
    }
    
    /** @Author: NamNH Nov 23, 2018
     *  @Todo: get report agent telesale
     **/
    public function getReportAgent(){
        $aData = [];
        $this->date_from_ymd    = MyFormat::dateConverDmyToYmd($this->date_from, '-');
        $this->date_to_ymd      = MyFormat::dateConverDmyToYmd($this->date_to, '-');
        $mAppCache              = new AppCache();
        $aAgent                 = $mAppCache->getAgent();
        $aData['AGENT']         = $aAgent;
        $criteria               = new CDbCriteria;
        if(!empty($this->province_id)){
            $mInventoryCustomer = new InventoryCustomer();
            $aAgent = $mInventoryCustomer->getAgentOfProvince($this->province_id);
            if(!empty($aAgent)){
                $criteria->addCondition('t.agent_id IN ('.implode(',', $aAgent).')');
            }
        }
        if(!empty($this->date_from_ymd)){
            $criteria->addCondition("t.created_date_only >=  '{$this->date_from_ymd}'");
        }
        if(!empty($this->date_to_ymd)){
            $criteria->addCondition("t.created_date_only <=  '{$this->date_to_ymd}'");
        }
        if(!empty($this->sale_id)){
            $criteria->compare('t.employee_id',$this->sale_id);
        }
        if(!empty($this->agent_id)){
            $criteria->compare('t.agent_id',$this->agent_id);
        }
        $criteria->group = 't.employee_id,t.agent_id';
        $criteria->select = 't.employee_id,t.agent_id,count(t.id) as id';
        $aFollowCustomer = FollowCustomer::model()->findAll($criteria);
        foreach ($aFollowCustomer as $key => $mFollowCustomer) {
            $aData['EMPLOYEE'][$mFollowCustomer->employee_id]                               = $mFollowCustomer->getEmployeeInfo('first_name');
            $aData['OUT_PUT'][$mFollowCustomer->agent_id][$mFollowCustomer->employee_id]    = $mFollowCustomer->id;
            if(isset($aData['SUM'][$mFollowCustomer->employee_id])){
                $aData['SUM'][$mFollowCustomer->employee_id] += $mFollowCustomer->id;
            }else{
                $aData['SUM'][$mFollowCustomer->employee_id] = $mFollowCustomer->id;
            }
        }
        if(!empty($aData['OUT_PUT'])){
            uasort($aData['OUT_PUT'], function ($item1, $item2) {
                $item1_count    = array_sum($item1);
                $item2_count    = array_sum($item2);
                return $item1_count < $item2_count;
            });
        }
        if(!empty($aData['EMPLOYEE'])){
            uksort($aData['EMPLOYEE'], function ($item1, $item2) use ($aData){
                $item1_count    = isset($aData['SUM'][$item1]) ? $aData['SUM'][$item1] : 0;
                $item2_count    = isset($aData['SUM'][$item2]) ? $aData['SUM'][$item2] : 0;
                return $item1_count < $item2_count;
            });
        }
        return $aData;
    }
    
    /** @Author: NamNH Jan 11, 2019 
     *  @Todo: save one many big
     **/
    public function saveOneManyBig($sale_id,$aIdCustomer){
//        remove if empty sale id
        if(empty($sale_id)){
            $criteria=new CDbCriteria;
            $criteria->addInCondition('many_id',$aIdCustomer);
            $criteria->compare('type',GasOneManyBig::TYPE_TELESALE);
            GasOneManyBig::model()->deleteAll($criteria);  
        }else{
            $mGasOneManyBig = new GasOneManyBig();
            $mGasOneManyBig->saveArrOfManyIdNotDeleteOld($sale_id, GasOneManyBig::TYPE_TELESALE,'Telesale', 'customer');
        }
    }
    
    /** @Author: DungNT Jan 16, 2019
     *  @Todo: xóa hết KH đã nhận khi qua 1 ngày mới
     **/
    public function removeApplyWhenEndDay() {
        $from = time(); $count = 0;
        /* 1. get all type = GasOneManyBig::TYPE_TELESALE
         * 2. find customer set empty sale_id
         * 3. delete all type
         */
        $mGasOneManyBig = new GasOneManyBig();
        $mGasOneManyBig->limit = 5000;
        $aOneMany = $mGasOneManyBig->getByType(GasOneManyBig::TYPE_TELESALE);
        foreach($aOneMany as $item):
            $mCustomer = Users::model()->findByPk($item->many_id);
            if($mCustomer && $mCustomer->sale_id > 0 ){
                $mCustomer->sale_id = 0;
                $mCustomer->update(['sale_id', 'phone']);
                $item->delete();
                $count++ ;
            }
        endforeach;
        
        $to = time();
        $second = $to-$from;
        $info = count($aOneMany)." record OneManyBig have $count record Customer empty sale id ***************** Customer Record removeApplyWhenEndDay done in: ".($second).'  Second  <=> '.($second/60).' Minutes';
        Logger::WriteLog($info);
    }
    
    /** @Author: NamNH Jan 11, 2019
     *  @Todo: get all find all
     *  @return: array id customer
     **/
    public function findIdCustomerNotCall(){
        $criteria=new CDbCriteria; 
        $criteria->compare('type',GasOneManyBig::TYPE_TELESALE);
        $aGasOneManyBig = GasOneManyBig::model()->findAll($criteria);
        return CHtml::listData($aGasOneManyBig, 'many_id', 'many_id');
    }

    /** @Author: NamNH Jan 11, 2019
     *  @Todo: update sale_id
     **/
    public function updateSalsId($aIdCustomer,$idSale){
        $criteria=new CDbCriteria;
        $criteria->addInCondition('id',$aIdCustomer);
        return Telesale::model()->updateAll(['sale_id'=>$idSale], $criteria);
    }
    
    /** @Author: NamNH Apr 09,2019
     *  @Todo: get list day holiday y-m-d
     **/
    public function getHolidayTelesale(){
        $aResult    = [];
        $aNotCheck = ['sunday'];
        $aDays      = MyFormat::getArrayDay($this->date_from_ymd, $this->date_to_ymd);
        $aHoliday   = $this->getHoliday($aDays);
        foreach ($aDays as $day) {
            if(in_array($day, $aHoliday)){
                $aResult[] = $day;
                continue;
            }
            $day_of_week = strtolower(MyFormat::dateConverYmdToDmy($day,'l'));
            if(in_array($day_of_week, $aNotCheck)){
                $aResult[] = $day;
                continue;
            }
        }
        return $aResult;
    }
    
    /** @Author: NamNH
     *  @Todo: check date is holidays OR not
     * @param: $date Y-m-d
     */
    public function getHoliday($aDate) {
        $criteria   = new CDbCriteria();
        $criteria->addInCondition('t.date', $aDate);
        $aModel     = GasLeaveHolidays::model()->findAll($criteria); 
        return CHtml::listData($aModel, 'date', 'date');
    }
}
