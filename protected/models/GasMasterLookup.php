<?php

/**
 * This is the model class for table "{{_gas_master_lookup}}".
 *
 * The followings are the available columns in table '{{_gas_master_lookup}}':
 * @property integer $id
 * @property string $name
 * @property integer $display_order
 * @property integer $type
 * @property integer $type_lookup
 * @property integer $status
 * @property string $name_vi
 */
class GasMasterLookup extends BaseSpj
{
    const TYPE_CASHBOOK     = 1;// Mar, 06, 2017 
//    const TYPE_ASSETS       = 2;// Jun1117 Tài sản
//    const TYPE_UPDATE__SOON = 2;// cập nhật sau
    
    const MASTER_TYPE_THU = 1;
    const MASTER_TYPE_CHI = 2;
    
    const ID_THU_TIEN_KH        = 43;// Mar 12, 2017 Thu tiền khách hàng bò - mối
    const ID_CHI_NOP_CONG_TY    = 23;
    const ID_CHI_GAS_DU         = 24;
    
    const ID_THU_BAN_GAS50      = 5;
    const ID_THU_BAN_GAS45      = 1;
    const ID_THU_BAN_GAS12      = 2;
    const ID_THU_BAN_BEP        = 6;
    const ID_THU_BAN_VAN        = 8;
    const ID_THU_BAN_DAY        = 9;
    const ID_THU_BAN_VO         = 7;// la ban Gas + Vo
    const ID_THU_BAN_BO_BINH    = 27;
    const ID_THU_THE_CHAN       = 29;
    const ID_THU_KHAC           = 22;
    const ID_THU_BU_VO          = 44;
    const ID_THU_THE_CHAN_BM    = 29;
    const ID_THU_DONG_BHXH      = 46;
    const ID_TIEN_DAI_LY        = 49;
    const ID_THU_NOP_DUM        = 50;
    
    const ID_CHI_THE_CHAN       = 30;
    const ID_CHI_DIEN_THOAI     = 3;
    const ID_CHI_INTERNET       = 4;
    const ID_CHI_SALARY         = 10;
    const ID_CHI_TIEN_DIEN      = 12;
    const ID_CHI_TIEN_NUOC      = 13;
    const ID_CHI_OTHER          = 21;
    const ID_CHI_THUE_NHA       = 11;
    const ID_CHI_PHI_NOP_NGAN_HANG = 20;
    const ID_CHI_NOP_DUM        = 28;
    const ID_CHI_MUA_VO         = 32;
    
    const ID_CHI_GUI_XE         = 16;
    const ID_CHI_NOP_THUE       = 19;
    const ID_CHI_CONG_AN        = 26;
    const ID_CHI_RAC            = 45;
    const ID_CHI_BHXH           = 47;
    const ID_CHI_BP_PHAP_LY     = 48;
    const ID_CHI_CONG_DOAN      = 15;
    
    const ID_CHI_SALARY_EMPLOYEE        = 51;
    const ID_CHI_PC_GIAO_KHO            = 52;
    const ID_CHI_BONUS_BT_MOI           = 53;
    const ID_CHI_BONUS_KHOAN            = 54;
    const ID_CHI_BONUS_PTTT             = 39;
    const ID_CHI_NOPTIEN_86NCV          = 55;
    const ID_CHI_NOPTIEN_KHO_TRAM       = 56;
    const ID_CHI_NOP_BIDV_THUY          = 57;
    const ID_CHI_TREO_CONG_NO           = 58;
    const ID_CHI_VIETTEL_PAY            = 59;// Chi nộp tiền cửa hàng Viettel
    const ID_CHI_NOP_BIDV_NHAN          = 60;// Chi tiền nộp ngân hàng BIDV Nhàn
    
    /**
     * @Author: ANH DUNG Mar 12, 2017
     * @Todo: các loại thu chi sử dụng trên app
     */
    public function getArrayIdApp() {
        $aRes = [
            GasMasterLookup::ID_THU_TIEN_KH,
            GasMasterLookup::ID_CHI_NOP_CONG_TY,
            GasMasterLookup::ID_THU_THE_CHAN_BM,
            GasMasterLookup::ID_CHI_GAS_DU,
            GasMasterLookup::ID_THU_KHAC,
//            GasMasterLookup::ID_THU_DONG_BHXH,// Jun 08, 2017 NV tự nộp ngân hàng
            GasMasterLookup::ID_TIEN_DAI_LY,
            GasMasterLookup::ID_THU_NOP_DUM,

//            GasMasterLookup::ID_CHI_SALARY,// Dec1018 hide
            GasMasterLookup::ID_CHI_NOP_THUE,
//            GasMasterLookup::ID_CHI_RAC,
            GasMasterLookup::ID_CHI_OTHER,
            GasMasterLookup::ID_CHI_THE_CHAN,
            GasMasterLookup::ID_CHI_BP_PHAP_LY,
//            GasMasterLookup::ID_CHI_THUE_NHA,// Dec1018 hide
            GasMasterLookup::ID_CHI_PHI_NOP_NGAN_HANG,
            GasMasterLookup::ID_CHI_NOP_DUM,
            GasMasterLookup::ID_CHI_MUA_VO,
//            GasMasterLookup::ID_CHI_CONG_DOAN,// Dec1018 hide
//            GasMasterLookup::ID_CHI_BHXH,// Jun 08, 2017 NV tự nộp ngân hàng
            GasMasterLookup::ID_CHI_SALARY_EMPLOYEE,// Close Feb2819
//            GasMasterLookup::ID_CHI_TRUC_DEM,// Close Feb2819
//            GasMasterLookup::ID_CHI_PC_GIAO_KHO,// Close Feb2819
//            GasMasterLookup::ID_CHI_BONUS_BT_MOI,// Close Feb2819
//            GasMasterLookup::ID_CHI_BONUS_KHOAN,// Dec1018 hide
//            GasMasterLookup::ID_CHI_BONUS_PTTT,// Dec1018 hide
//            GasMasterLookup::ID_CHI_NOPTIEN_86NCV,// Mar2919 Hide (ko dc show, chỉ tạo mới) - Mar2819 show --- Dec1018 hide
            GasMasterLookup::ID_CHI_NOPTIEN_KHO_TRAM,
            GasMasterLookup::ID_CHI_NOP_BIDV_THUY,
            GasMasterLookup::ID_CHI_TREO_CONG_NO,
            GasMasterLookup::ID_CHI_VIETTEL_PAY,
            GasMasterLookup::ID_CHI_NOP_BIDV_NHAN,
        ];
//        if($this->sourceFrom == BaseSpj::SOURCE_FROM_WEB){
        if(1){
            $aIdAppHide = [
                GasMasterLookup::ID_CHI_DIEN_THOAI,
//                GasMasterLookup::ID_CHI_INTERNET,
                GasMasterLookup::ID_CHI_TIEN_DIEN,
//                GasMasterLookup::ID_CHI_TIEN_NUOC,// Dec1018 hide
//                GasMasterLookup::ID_CHI_GUI_XE,// Dec1018 hide
//                GasMasterLookup::ID_CHI_CONG_AN,// Dec1018 hide
//                GasMasterLookup::ID_CHI_RAC,
            ];
            $aRes = array_merge($aIdAppHide, $aRes);
        }
        return $aRes;
        
    }
    
    public function getArrayMasterType(){
        return [
//            GasMasterLookup::TYPE_ASSETS    =>'Tài sản',
            GasMasterLookup::TYPE_CASHBOOK  =>'Sổ Quỹ Tiền Mặt',
        ];
    }
        
    public function getArrayLookupType(){
        return [
            GasMasterLookup::MASTER_TYPE_THU    =>'Thu',
            GasMasterLookup::MASTER_TYPE_CHI    =>'Chi',
        ];
    }
        
        
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_master_lookup}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('name, display_order, type, type_lookup', 'required'),
            array('fromSearch, id, name, display_order, type, type_lookup, status, name_vi', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Tên',
            'display_order' => 'Thứ Tự Hiển Thị',
            'type' => 'Loại',
            'type_lookup' => 'Loại Lookup',
            'status' => 'Status',
            'name_vi' => 'Name Vi',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('t.display_order',$this->display_order);
        $criteria->compare('t.type',$this->type);
        $criteria->compare('t.type_lookup',$this->type_lookup);
        $criteria->compare('t.status',$this->status);
        $criteria->compare('t.name_vi',$this->name_vi,true);
        $sort = new CSort();

        $sort->attributes = array(
            'display_order'=>'display_order',
            'type'=>'type',
            'type_lookup'=>'type_lookup',
            'status'=>'status',
        );    
        $sort->defaultOrder = 't.id DESC, t.display_order ASC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort' => $sort,
            'pagination'=>array(
                'pageSize'=> 50,
            ),
        ));
    }


    public function beforeSave() {
        $this->name_vi = strtolower(MyFunctionCustom::remove_vietnamese_accents($this->name));
        return parent::beforeSave();
    }                
}