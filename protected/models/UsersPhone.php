<?php

/**
 * This is the model class for table "{{_users_phone}}".
 * 
 * The followings are the available columns in table '{{_users_phone}}':
 * @property string $id
 * @property string $user_id
 * @property string $phone
 */
class UsersPhone extends CActiveRecord
{
    const WINDOW_TYPE_BO_MOI    = 1;// có sử dụng ở API rồi - không xóa
    const WINDOW_TYPE_HGD       = 2;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return UsersPhone the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_users_phone}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('province_id, district_id, last_purchase, is_customer_app, network, role_id, is_maintain, id, user_id, phone', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUser' => array(self::BELONGS_TO, 'Users', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'phone' => 'Phone',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
            $criteria=new CDbCriteria;
            return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
            ));
    }
    
    /**
     * @Author: DungNT May 13, 2016
     * @Todo: get record by phone
     * @Param: $phone
     * @Param: $window_customer_type: search riêng KH bò mối + hộ gd từ window
     * @use: hiện tại sử dụng api/default/getCustomerByPhone 
     */
    public function getByPhone($phone, $window_customer_type) {
        $criteria = new CDbCriteria();
        $phoneSearch = UsersPhone::formatPhoneSaveAndSearch($phone);
        if(empty($phoneSearch)){
            return [];
        }
        $criteria->addCondition('t.phone='.$phoneSearch);// $criteria->compare('t.phone', UsersPhone::formatPhoneSaveAndSearch($phone));
        UsersPhone::handleWindowSearch($window_customer_type, $criteria);
        return self::model()->findAll($criteria);
//        if(count($models) > 0){// Oct2218 ko cần check nữa, return luôn nếu ko có thì là empty array
//            return $models;
//        }
        
        return [];// Oct2218 bỏ đoạn dưới này - ko nên sửa dụng nữa gây ra => Slow query SELECT * FROM `gas_users_phone` `t` WHERE ((t.phone LIKE '%2963980969%') AND (t.is_maintain IN (7,8,9,11))) AND (t.role_id=4);
        // Now 18, 2016 nếu search = không có thì search like lần nữa
        $criteria = new CDbCriteria();
        $criteria->compare('t.phone', $phoneSearch, true);
        UsersPhone::handleWindowSearch($window_customer_type, $criteria);
        return self::model()->findAll($criteria);
    }
    
    
    /**
     * @Author: DungNT Mar 14, 2017
     * @Todo: get record by phone
     * @Param: $phone
     * @Param: $window_customer_type: search riêng KH bò mối + hộ gd từ window
     * UsersPhone::WINDOW_TYPE_BO_MOI   = 1
     * UsersPhone::WINDOW_TYPE_HGD      = 2
     * @use: hiện tại sử dụng api/default/getCustomerByPhone
     */
    public function getByPhoneFixQuery($phone, $window_customer_type) {
        Logger::WriteLog('Need Fix: function getByPhoneFixQuery');
        die();
        $criteria = new CDbCriteria();
        $phoneSearch = UsersPhone::formatPhoneSaveAndSearch($phone);
        if(empty($phoneSearch)){
            return [];
        }
        $sParamsIn = implode(',', CmsFormatter::$aTypeIdHgd);
        $subQueryPhone  = "SubQ.phone=$phoneSearch";
        $subQuerySame   = " AND SubQ.role_id=".ROLE_CUSTOMER;
        
        if($window_customer_type == UsersPhone::WINDOW_TYPE_BO_MOI){
            $subQuerySame.= " AND SubQ.is_maintain NOT IN ($sParamsIn)";
        }elseif($window_customer_type == UsersPhone::WINDOW_TYPE_HGD){
            $subQuerySame.= " AND SubQ.is_maintain IN ($sParamsIn)";
        }
        
        $subQuery = 'SELECT DISTINCT user_id FROM `gas_users_phone` as SubQ WHERE '.$subQueryPhone." $subQuerySame";
        $criteria->addCondition('t.id IN ('.$subQuery.')');
        $models = Users::model()->findAll($criteria);
        if(count($models) > 0){
            return $models;
        }
        
        // Now 18, 2016 nếu search = không có thì search like lần nữa
        $criteria = new CDbCriteria();
        $subQueryPhone  = "SubQ.phone LIKE '%$phoneSearch%' ";
        $subQuery       = 'SELECT DISTINCT user_id FROM `gas_users_phone` as SubQ WHERE '.$subQueryPhone." $subQuerySame";
        $criteria->addCondition('t.id IN ('.$subQuery.')');
        return Users::model()->findAll($criteria);
    }
    
    
    /**
     * @Author: DungNT Jul 05, 2016
     * @Todo: get one record by phone, chỉ lấy ra 1 record phone
     * @Param: $phone
     * @Param: $window_customer_type: search riêng KH bò mối + hộ gd từ window
     * @use: hiện tại sử dụng api/default/getCustomerByPhone
     */
    public function getByPhoneOneRecord($phone, $window_customer_type) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.phone', UsersPhone::formatPhoneSaveAndSearch($phone));
        UsersPhone::handleWindowSearch($window_customer_type, $criteria);
        return self::model()->find($criteria);
    }
    
    /**
     * @Author: DungNT Jun 10, 2016
     * @Todo: get one record by user_id
     */
    public static function getOnePhoneNumberByUserId($user_id) {
        if(empty($user_id)){
            return null;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.user_id=' . $user_id);
        $criteria->order = 't.id ASC';
        return self::model()->find($criteria);
    }
    public function getAllPhoneNumberByUserId() {
        if(empty($this->user_id)){
            return [];
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.user_id=' . $this->user_id);
        return self::model()->findAll($criteria);
    }
    
    /** @Author: DungNT Apr 05, 2018
     *  @Todo: remove all white space of string
     **/
    public static function removeWhiteSpace($phone) {
        return preg_replace('/\s+/', '', $phone); // remove For all whitespace
    }
    
    /**
     * @Author: DungNT May 24, 2016
     * @Todo: format phone only
     * @input: -09 881-803+86QWE
     * @return: 988180386
     */
    public static function formatPhoneOnly($phone) {
//        $phone = preg_replace('/\s+/', '', $phone); // remove For all whitespace
        $phone = preg_replace("/[^0-9]/","", $phone);// Add Aug 14, 2016 remove everything but numbers
//        $remove = array( "-", ".");
//        $remove = array(".");
//        $phone = str_replace($remove, "", $phone);
//        $phone = trim($phone);
        return $phone;
    }
    
    /** @Author: DungNT May 13, 2016
     * @Todo: remove zero left of phone before search
     */
    public static function formatPhoneSaveAndSearch($phone) {
        $phone = UsersPhone::formatPhoneOnly($phone);
        return ltrim($phone,'0');
    }
    public static function removeLeftZero($phone) {
        return ltrim($phone,'0');
    }
    
    /**
     * @Author: DungNT May 19, 2016
     * @Todo: save phone of user or customer
     */
    public static function savePhone($mUser) {
        $sPhone = trim($mUser->phone);
        UsersPhone::deleteByUser($mUser->id);
        $is_customer_app = 0;
        if($mUser->role_id == ROLE_CUSTOMER && $mUser->is_maintain == UsersExtend::STORE_CARD_HGD_APP){
            $is_customer_app = CronUpdate::PHONE_HGD_TO_APP;
        }
        $mUser->last_purchase = !empty($mUser->last_purchase) ? $mUser->last_purchase : '0000-00-00';
        
        if($sPhone!=''){
            $arrPhone = explode("-", $sPhone);
            $aRowInsert = [];
            foreach($arrPhone as $phone){
                $phone = UsersPhone::formatPhoneSaveAndSearch($phone);
                if($phone < 9000000){
                    continue;
                }
                $aRowInsert[]="('$mUser->id',
                    '$mUser->role_id',
                    '$mUser->is_maintain',
                    '$mUser->province_id',
                    '$mUser->district_id',
                    '$mUser->last_purchase',
                    '$is_customer_app',
                    '$phone'
                    )";
            }
            $tableName = UsersPhone::model()->tableName();
            $sql = "insert into $tableName (user_id,
                            role_id,
                            is_maintain,
                            province_id,
                            district_id,
                            last_purchase,
                            is_customer_app,
                            phone
                            ) values ".implode(',', $aRowInsert);
            if(count($aRowInsert)>0){
                Yii::app()->db->createCommand($sql)->execute();
            }
        }
    }
    
    /**
     * @Author: DungNT May 13, 2016
     * @Todo: delete by user
     */
    public static function deleteByUser($user_id){
        if(empty($user_id)){
            return ;
        }
        $criteria=new CDbCriteria;
        $criteria->compare('user_id',$user_id);
        UsersPhone::model()->deleteAll($criteria);        
    }
    
    /**
     * @Author: DungNT May 17, 2016
     * @Todo: get critetira search KH bò mối trên web
     * http://spj.daukhimiennam.com/admin/gascustomer/customer_store_card
     * admin/ajax/Search_user_by_code
     */
    public static function getCriteriaCustomer(&$returnVal, &$criteria, $cRole, $keyword) {
        $criteria->addCondition(" t.role_id IN ('".ROLE_CUSTOMER."', '".ROLE_AGENT."')"); 
//        $from = time();
        $models = self::getCriteriaCustomerData($criteria, $cRole, $keyword);
//        $to = time();
//        $second = $to-$from;
//        echo count($models).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';
        
        $returnVal = UsersPhone::formatJsonCustomerReturn($models);
    }
    
    /** @Author: DungNT Mar 28, 2017
     * @Todo: format json name and role
     */
    public function jsonNameAndRole($models) {
        $returnVal = [];
        $cmsFormat = new CmsFormatter();
        foreach($models as $model){
            $label = $cmsFormat->formatNameAndRole($model);
            if($model->role_id != ROLE_AGENT){
                $label = $model->code_bussiness.' - '.$label;
            }
            $name_agent = $label;
            $name_customer = $label;
            
            $returnVal[] = array(    
                //'label'=>$model->code_account.' - '.$model->code_bussiness.' -- '.$model->first_name,
                'label'         => $label,
                'value'         => $label,
                'name_customer' => $name_customer,
                'id'            =>$model->id,
                'name_agent'    => $name_agent,
                'code_account'  =>$model->code_account,
                'code_bussiness'=>$model->code_bussiness,
                'address'       =>$model->address,
                'phone'         =>$model->phone
            );
        }
        return $returnVal;
    }
    
    /**
     * @Author: DungNT Jun 25, 2016
     * @Todo: format return json
     */
    public static function formatJsonCustomerReturn($models) {
        $returnVal = [];
        $cmsFormat = new CmsFormatter();
        foreach($models as $model)
        {
            $TypeCustomer = '';
            if($model->is_maintain){
                $TypeCustomer = (isset(CmsFormatter::$CUSTOMER_BO_MOI[$model->is_maintain])?CmsFormatter::$CUSTOMER_BO_MOI[$model->is_maintain]:"");
            }
            
//            $mDistrict = $model->district; -- cách lấy $districtName cũ truy vấn db nhiều quá => cần change lại
//            if($mDistrict){
//                $districtName = $mDistrict->name;
//            }
            $tmp = explode(",", $model->address);
            $tmp = array_reverse($tmp);
            $districtName = isset($tmp[1]) ? $tmp[1] : '';
            
            $code_account = '';
            if(in_array($model->is_maintain, [STORE_CARD_KH_BINH_BO, STORE_CARD_KH_MOI])){
                $code_account = "KT: $model->code_account -- ";
            }
            
            $fullName = $code_account.$model->code_bussiness."-".trim($model->first_name);
            $label = $fullName." - $TypeCustomer"." - $districtName";
            $name_agent = $model->name_agent;
            $name_customer = "".$fullName." - KH $TypeCustomer";
            
            if($model->role_id==ROLE_AGENT){
                $addText = "Cty HƯỚNG MINH - ";
                $label = $addText.$cmsFormat->formatNameUser($model)." - $districtName";
                $name_agent = $addText.$model->name_agent;
                $name_customer = $addText.$cmsFormat->formatNameUser($model);
            }
            $returnVal[] = array(
                //'label'=>$model->code_account.' - '.$model->code_bussiness.' -- '.$model->first_name,
               'label'=> $label,
                'value'=>$cmsFormat->formatNameUser($model),
                'name_customer'=> $name_customer,
                'id'=>$model->id,
                'name_agent'=> $name_agent,
                'code_account'=>$model->code_account,
                'code_bussiness'=>$model->code_bussiness,
                'address'=>$model->address,
                'phone'=>$model->phone,
                'is_maintain'=>$model->is_maintain, // Loại Kh bình bò, bình mối
            );
        }
        return $returnVal;
    }
    
    /** @Author: DungNT Dec 16, 2017
     *  @Todo: format return json
     */
    public function formatJsonSolr($aData) {
        $returnVal = [];
        $cmsFormat = new CmsFormatter();
        foreach($aData as $objSolr){
            $typeCustomer = isset(CmsFormatter::$CUSTOMER_BO_MOI[$objSolr->is_maintain]) ? '['.CmsFormatter::$CUSTOMER_BO_MOI[$objSolr->is_maintain].']' : '';
            $tmp = explode(',', $objSolr->address);
            $tmp = array_reverse($tmp);
            $districtName = isset($tmp[1]) ? $tmp[1] : '';
            
            $code_account = '';
//            if(in_array($objSolr->is_maintain, [STORE_CARD_KH_BINH_BO, STORE_CARD_KH_MOI])){
//                $code_account = "KT: $objSolr->code_account -- ";
//            } // DungNT Aug2119 close
            $fullName       = $code_account.$objSolr->code_bussiness." - ".$objSolr->first_name;
            $label          = $fullName." $typeCustomer - $districtName";
            $name_customer  = $fullName." - $typeCustomer";
            
            if($objSolr->role_id==ROLE_AGENT){
                $addText        = "Cty HƯỚNG MINH - $objSolr->code_bussiness";
                $label          = $addText." $objSolr->first_name - $districtName";
                $name_customer  = $addText. ' '.$objSolr->first_name;
            }
            $returnVal[] = array(
                //'label'=>$objSolr->code_account.' - '.$objSolr->code_bussiness.' -- '.$objSolr->first_name,
                'label'             => $label,
                'value'             => $objSolr->code_bussiness." - ".$objSolr->first_name,
                'name_customer'     => $name_customer,
                'id'                => $objSolr->id,
                'name_agent'        => '',
                'code_account'      => $objSolr->code_account,
                'code_bussiness'    => $objSolr->code_bussiness,
                'address'           => $objSolr->address,
                'phone'             => $objSolr->phone,
                'is_maintain'       => $objSolr->is_maintain, // Loại Kh bình bò, bình mối
            );
        }
        return $returnVal;
    }
    
    /** @Author: DungNT Dec 16, 2017
     *  @Todo: format return json
     */
    public function formatJsonSolrWindow($aData) {
        $returnVal      = [];
        $mAppCache      = new AppCache();
        $listdataAgent  = $mAppCache->getAgentListdata();
        
        foreach($aData as $objSolr){
            $returnVal[] = [
                'customer_id'       => $objSolr->id,
                'customer_name'     => $objSolr->first_name,
                'customer_address'  => $objSolr->address,
                'customer_phone'    => $objSolr->phone,
                'customer_agent'    => isset($listdataAgent[$objSolr->area_code_id]) ? $listdataAgent[$objSolr->area_code_id] : '',
                'customer_type'     => 'HGĐ',
                'customer_delivery_agent'       => '',
                'customer_delivery_agent_id'    => 0,// Aug 30, 2016 xử lý trả về đại lý gần nhất của KH bò mối ghi tạm vào cột maintain_agent_id
                'role_id'           => $objSolr->role_id,
                'agent_id'          => $objSolr->area_code_id,
                'contact'           => '',
                'contact_note'      => '',
                'sale_name'         => '',
                'sale_phone'        => '',
                'sale_type'         => '',
            ];
        }
        return $returnVal;
    }
    
    /** @Author: DungNT Dec 16, 2017
     *  @Todo: fix lại hàm search của window sang solr, vì 400k record user mà search kiểu cũ thì CPU high
     **/
    public function fixWindowSearch($keyword) {
        $keyword        = trim($keyword);
        $extSolr        = new ExtSolr();
//        $key = "address_vi:\"$keyword\" && type:".CUSTOMER_TYPE_STORE_CARD." && role_id:".ROLE_CUSTOMER." && is_maintain:(".$sParamsIn.")" ;
        $this->getSolrCondition($extSolr);
        $extSolr->sQuery = "address_vi:\"$keyword\"".$extSolr->sConditionSame ;
        $result         = $extSolr->searchByKeyword();
        if(count($result->response->docs) < 1){// tìm thêm like nếu condition bên trên ko ra
            $extSolr->sQuery    = "address_vi:*$keyword*".$extSolr->sConditionSame ;
            $result             = $extSolr->searchByKeyword();
        }
        return $this->formatJsonSolrWindow($result->response->docs);
    }
    
    /** @Author: DungNT Dec 16, 2017
     *  @Todo: sử dụng get same condition
     **/
    public function getSolrCondition(&$extSolr) {
        $extSolr->addParams('type', CUSTOMER_TYPE_STORE_CARD);
        $extSolr->addParams('role_id', ROLE_CUSTOMER);
        $sParamsIn = implode(' OR ', CmsFormatter::$aTypeIdHgd);
        $extSolr->addInCondition('is_maintain', $sParamsIn);
        $extSolr->addParams('status', STATUS_ACTIVE);
        $extSolr->sConditionSame = $extSolr->sQuery;
    }
    
    
    
    /** @Author: DungNT May 19, 2016 */
    public static function getCriteriaCustomerData(&$criteria, $cRole, $keyword) {
        $criteria->addCondition('t.status=' . STATUS_ACTIVE);
//        $criteria->addCondition(" t.role_id IN ('".ROLE_CUSTOMER."', '".ROLE_AGENT."')"); 
        $criteria->addSearchCondition('t.address_vi', $keyword, true); // true ==> LIKE '%...%'
//        $criteria->compare('t.type', CUSTOMER_TYPE_STORE_CARD);// Jun 13, 2016 không thể sử dụng kiểu này dc, vì nó sẽ không search dc Agent
        $aTypeNot = array(CUSTOMER_TYPE_MAINTAIN, CUSTOMER_TYPE_MARKET_DEVELOPMENT);
//        $criteria->addNotInCondition( 't.type', $aTypeNot);
        $sParamsIn = implode(',', $aTypeNot);
        $criteria->addCondition("t.type NOT IN ($sParamsIn)");
        $criteria->limit = 20;
        return Users::model()->findAll($criteria);
    }

    /**
     * @Author: DungNT Jun 13, 2016
     * @Todo: chia search loại KH ở window Bò Mối Riêng, Hộ Riêng
     */
    public static function handleWindowSearch($window_customer_type, &$criteria) {
        // ở đây đang search ở model:  UsersPhone không phải là model User nên chú ý điều kiện search
        if($window_customer_type == UsersPhone::WINDOW_TYPE_BO_MOI){
//            $aSearch = array_merge(CmsFormatter::$aTypeIdBoMoi, CmsFormatter::$aTypeIdAgentAndOther);
//            $criteria->addInCondition( 't.is_maintain', $aSearch);
//            $criteria->addNotInCondition( 't.is_maintain', CmsFormatter::$aTypeIdHgd);
            $sParamsIn = implode(',', CmsFormatter::$aTypeIdHgd);
            $criteria->addCondition("t.is_maintain NOT IN ($sParamsIn)");
            
            $criteria->addCondition("t.role_id=".ROLE_CUSTOMER);
        }elseif($window_customer_type == UsersPhone::WINDOW_TYPE_HGD){
//            $criteria->addInCondition( 't.is_maintain', CmsFormatter::$aTypeIdHgd);
            $sParamsIn = implode(',', CmsFormatter::$aTypeIdHgd);
            $criteria->addCondition("t.is_maintain IN ($sParamsIn)");
            $criteria->addCondition("t.role_id=".ROLE_CUSTOMER);
//            $criteria->compare("t.role_id", ROLE_CUSTOMER);
        }
    }
    
    /**
     * @Author: DungNT May 19, 2016
     * dùng cho api vì format data khác với web
     * window api: api/default/getCustomerByKeyword
     */
    public static function apiGetCustomer(&$returnVal, &$criteria, $cRole, $keyword, $window_customer_type) {
        UsersPhone::handleWindowSearch($window_customer_type, $criteria);
        $models = self::getCriteriaCustomerData($criteria, $cRole, $keyword);
        foreach($models as $mUser):
            $mUserRef = $mUser->rUsersRef;
            $mSale = $mUser->sale;
            $returnVal[] = HandleArrayResponse::handleInfoCustomer($mUser, $mUserRef, $mSale);;
        endforeach;
    }
    
    /**
     * @Author: DungNT May 23, 2016
     * @Todo: get name đại lý giao hàng cho KH
     */
    public static function getDeliveryAgent(&$mUser) {
        $mStorecard = GasStoreCard::GetLastPurchase($mUser->id);
        if($mStorecard){
            $mAgent = Users::model()->findByPk($mStorecard->user_id_create);
            if($mAgent){
                $mUser->maintain_agent_id = $mAgent->id;// Aug 30, 2016 xử lý trả về id đại lý gần nhất cho điều phối
                return $mAgent->getFullName()." - ".$mAgent->getAgentLandline();
            }
        }
        return '';
    }
        
    /**
     * @Author: DungNT Jul 19, 2016
     * @Todo: get phone of user
     */
    public static function getPhoneUser($uid) {
        $mUserPhone = UsersPhone::getOnePhoneNumberByUserId($uid);
        if(is_null($mUserPhone)){
            return '';
        }
        return $mUserPhone->phone;
    }
    
    /**
     * @Author: DungNT Jul 05, 2016
     * @Todo: get Ext of phone, lấy Ext đại lý tương ứng với số Phone gọi đến
     * @Param: $phone
     * @use_for: http://spj.daukhimiennam.com/api/spj/getAgent
     */
    public function getExtOfCallCenter($phone) {
        $from = time();
        $mUserPhone = $this->getByPhoneOneRecord($phone, UsersPhone::WINDOW_TYPE_HGD);
        if(is_null($mUserPhone)){
            return UsersPhone::DEFAULT_EXT;
        }
        $mUser = Users::model()->findByPk($mUserPhone->user_id);
        if(is_null($mUser)){
            return UsersPhone::DEFAULT_EXT;
        }
        $mAgent = Users::model()->findByPk($mUser->area_code_id);
        if(is_null($mAgent)){
            return UsersPhone::DEFAULT_EXT;
        }

        $to = time();
        $second = $to-$from;
        $log = "[1900] - $phone EXT: {$mAgent->username} REQUEST getExtOfCallCenter done in: ".($second).'  Second  <=> '.($second/60).' Minutes';
        Logger::WriteLog($log);
        return $mAgent->username;
    }
    
    /**
     * @Author: DungNT Jul 19, 2016
     * @Todo: kiểm tra số điện thoại có phải là di động hay không
     * @Param: $phone: 01684331552
     * @return: true if là di động
     *          false if ko phải số di động
     */
    public static function isValidCellPhone($phone) {
//        $phoneCheck = UsersPhone::formatPhoneSaveAndSearch($phone);
        $mSms = new GasScheduleSms();
        $ok1 = $mSms->isPhoneRealLength($phone);
        $ok2 = GasScheduleSms::isValidPhone($phone);

        return $ok1 && $ok2;
    }
    
    /**
     * @Author: DungNT Oct 06, 2018
     * @Todo: kiểm tra số điện thoại có hợp lệ hay không
     * @Param: $phone: 01684331552 or 02836223388
     */
    public static function isValidCellPhoneOrLandline($phone) {
//        $phoneCheck = UsersPhone::formatPhoneSaveAndSearch($phone);
        $mSms = new GasScheduleSms();
        $ok1 = $mSms->isPhoneRealLength($phone);
        $ok2 = GasScheduleSms::isValidPhone($phone);
        $ok3 = GasScheduleSms::isLandlinePhone($phone);

        return $ok1 && ($ok2 || $ok3);
    }
    
    /**
     * @Author: DungNT Jul 23, 2016
     * @Todo: get record by phone and role_id , is_maintain
     * @Param: $phone
     * @Param: array $role_id
     * @Param: array $is_maintain 
     * @use: hiện tại sử dụng để check cho phần tạo customer từ window c#
     */
    public static function getByPhoneAndRole($phone, $role_id, $is_maintain) {
        $criteria = new CDbCriteria();
        $criteria->compare("t.phone", UsersPhone::formatPhoneSaveAndSearch($phone));
//        $criteria->addInCondition("t.role_id", $role_id);
        $sParamsIn = implode(',', $role_id);
        $criteria->addCondition("t.role_id IN ($sParamsIn)");
        
//        $criteria->addInCondition("t.is_maintain", $is_maintain);
        $sParamsIn = implode(',', $is_maintain);
        $criteria->addCondition("t.is_maintain IN ($sParamsIn)");
        return self::model()->find($criteria);
    }
    
    /**
     * @Author: DungNT Aug 02, 2016
     * @Todo: format những số phone dài
     */
    public static function formatPhoneLong($phone) {
        if(empty($phone)){
            return "";
        }
        $tmp = explode("-", $phone);
        return implode(", ", $tmp);
        
    }
    
    /* UsersPhone::formatPhoneView($phone) */
    public static function formatPhoneView($phone) {
        $n1         = substr($phone, -3);
        $phoneCut   = substr($phone, 0, -3);
        $n2         = substr($phoneCut, -3);
        $phoneCut   = substr($phoneCut, 0, -3);
        return $phoneCut.' '.$n2.' '.$n1;
    }
    
    /** @Author: DungNT Jul 22, 2017
     * @Todo: count số phone theo loại KH và loại User
     */
    public function countByPhoneAndType($needMore = []) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.phone', $this->phone);
        if(is_array($this->role_id)){
            $sParamsIn = implode(',', $this->role_id);
            $criteria->addCondition("t.role_id IN ($sParamsIn)");
        }elseif(!empty($this->role_id)){
            $criteria->compare('t.role_id', $this->role_id);
        }
        
        if(is_array($this->is_maintain)){
            $sParamsIn = implode(',', $this->is_maintain);
            $criteria->addCondition("t.is_maintain IN ($sParamsIn)");
        }elseif(!empty($this->is_maintain)){
            $criteria->compare('t.is_maintain', $this->is_maintain);
        }
        if(isset($needMore['findAll'])){
            return self::model()->findAll($criteria);
        }
        return self::model()->count($criteria);
    }
    
}