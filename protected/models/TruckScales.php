<?php

/**
 * This is the model class for table "{{_truck_scales}}".
 *
 * The followings are the available columns in table '{{_truck_scales}}':
 * @property string $id
 * @property string $code_no
 * @property string $car_id
 * @property string $car_number
 * @property string $car_owner
 * @property string $cargo_owner
 * @property string $cargo_name
 * @property string $weight_1
 * @property string $weight_2
 * @property string $time_1
 * @property string $time_2
 * @property string $uid_login
 * @property string $agent_id
 * @property string $created_date
 * @property string $created_date_bigint
 */
class TruckScales extends BaseSpj
{
    
    public $MAX_ID;
    public $list_id_image, $file_name;// Dec0218 sẽ xóa attibutes này khi run test ok 
    
    const TYPE_INTERNAL         = 1;
    const TYPE_EXTERNAL         = 2;
    
    const MODE_ONLINE           = 1;// DEFAULT IS ONLINE
    const MODE_OFFLINE          = 2;
    
    const GVISION_TEXT_DETECTION    = 1;// Dec0218
    const GVISION_FACE              = 2;
    const GVISION_LABELS            = 3;
//    const GVISION_UPDATE            = 1;
    public $autocomplete_name_4 , $autocomplete_name_2 , $autocomplete_name_3 , $autocomplete_name_1;
    
    public function getArrayType() {
        return [
            TruckScales::TYPE_INTERNAL => 'Nội bộ',
            TruckScales::TYPE_EXTERNAL => 'Cân ngoài',
        ];
    }
    
    public function getArrayMode() {
        return [
            TruckScales::MODE_ONLINE    => 'Online',
            TruckScales::MODE_OFFLINE   => 'Local',
        ];
    }
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return TruckScales the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_truck_scales}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['weight_1','numerical', 'tooSmall'=>'Trọng lượng cân quá nhỏ, vui lòng kiểm tra lại (tối thiểu 1)','min'=>1, 'on'=>'WindowCreate1'],
            ['weight_2','numerical', 'tooSmall'=>'Trọng lượng cân quá nhỏ, vui lòng kiểm tra lại (tối thiểu 1)','min'=>1, 'on'=>'WindowCreate2'],
            ['weight_1, weight_2','numerical', 'tooSmall'=>'Trọng lượng cân quá nhỏ, vui lòng kiểm tra lại (tối thiểu 1)','min'=>1, 'on'=>'WindowSyncOffline'],
            ['offline_id, id, code_no, car_id, car_number, car_owner, cargo_owner, cargo_name, weight_1, weight_2, time_1, time_2, uid_login, agent_id, created_date, created_date_bigint, date_from, date_to, type, is_online', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rFile1' => array(self::HAS_MANY, 'GasFile', 'belong_id',
                'on' => 'rFile1.type=' . GasFile::TYPE_14_SCALES_1,
                'order' => 'rFile1.id ASC',
            ),
            'rFile2' => array(self::HAS_MANY, 'GasFile', 'belong_id',
                'on' => 'rFile2.type=' . GasFile::TYPE_15_SCALES_2,
                'order' => 'rFile2.id ASC',
            ),
            'rCarOwner' => array(self::BELONGS_TO, 'Users', 'car_owner'),
            'rCargoOwner' => array(self::BELONGS_TO, 'Users', 'cargo_owner'),
            'rTruckPlanDetail' => array(self::BELONGS_TO, 'TruckPlanDetail', 'truck_plan_detail_id'),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Loại cân',
            'is_online' => 'Trực tuyến',
            'code_no' => 'Mã Cân',
            'car_id' => 'Xe',
            'car_number' => 'Số xe',
            'car_owner' => 'Chủ xe',
            'cargo_owner' => 'Chủ hàng',
            'cargo_name' => 'Tên hàng',
            'weight_1' => 'Cân lần 1',
            'weight_2' => 'Cân lần 2',
            'time_1' => 'TG vào',
            'time_2' => 'TG ra',
            'uid_login' => 'Người tạo',
            'agent_id' => 'Đại lý',
            'created_date' => 'Ngày tạo',
            'created_date_bigint' => 'Created Date Bigint',
            'date_from' => 'Từ ngày',
            'date_to' => 'Đến',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
	$criteria->compare('t.code_no',$this->code_no,true);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    public function getCodeNo() {
        return $this->code_no;
    }
    public function getCarNumber() {
        return $this->car_number;
    }
    public function getCarOwner() {
        return $this->car_owner;
    }
    public function getCargoOwner() {
        return $this->cargo_owner;
    }
    public function getCargoName() {
        return $this->cargo_name;
    }
    public function getWeight1($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->weight_1);
        }
        return $this->weight_1;
    }
    public function getWeight2($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->weight_2);
        }
        return $this->weight_2;
    }
    public function getWeightCargo($format = false) {
        $res = $this->weight_1 - $this->weight_2;
        if($res < 0){
            $res *= -1;
        }
        if($format){
            return ActiveRecord::formatCurrency($res);
        }
        return $res;
    }
    public function getTime1($format = 'd/m/Y H:i') {
        return MyFormat::dateConverYmdToDmy($this->time_1, $format);
    }
    public function getTime2($format = 'd/m/Y H:i') {
        return MyFormat::dateConverYmdToDmy($this->time_2, $format);
    }
    public function getUidLogin() {
        $mUser = $this->rUidLogin;
        if($mUser){
            return $mUser->getFullName();
        }
        return '';
    }
    public function getAgent() {
        $mUser = $this->rAgent;
        if($mUser){
            return $mUser->getFullName();
        }
        return '';
    }
    public function getType() {
        $aData = $this->getArrayType();
        return isset($aData[$this->type]) ? $aData[$this->type] : '';
    }
    public function getMode() {
        $aData = $this->getArrayMode();
        return isset($aData[$this->is_online]) ? $aData[$this->is_online] : '';
    }

    protected function beforeValidate() {
        $this->car_number       = trim($this->car_number);
        $this->car_owner        = trim($this->car_owner);
        $this->cargo_owner      = trim($this->cargo_owner);
        $this->cargo_name       = trim($this->cargo_name);
        $this->weight_1         = trim($this->weight_1);
        return parent::beforeValidate();
    }
    
    protected function beforeDelete() {
        GasFile::DeleteByBelongIdAndType($this->id, GasFile::TYPE_14_SCALES_1);
        GasFile::DeleteByBelongIdAndType($this->id, GasFile::TYPE_15_SCALES_2);
        return parent::beforeDelete();
    }
    
    /** @Author: ANH DUNG Dec 02, 2018
    *   @Todo: map post to model scales first
    */
    public function windowGetPostFirst($q) {
        $this->type                 = $q->type;
        $this->car_number           = $q->car_number;
        $this->car_owner            = $q->car_owner;
        $this->cargo_owner          = $q->cargo_owner;
        $this->cargo_name           = $q->cargo_name;
        $this->weight_1             = MyFormat::removeComma($q->weight_1);
        $this->offline_id           = $q->offline_id;// DungNT Jan1619
        $this->uid_login            = $this->mAppUserLogin->id;
        $this->agent_id             = $this->mAppUserLogin->parent_id;
        $this->time_1               = date('Y-m-d H:i:s');
        $this->code_no              = MyFunctionCustom::getNextId('TruckScales', 'T'.date('y'), LENGTH_TICKET,'code_no');
        $this->truck_plan_detail_id = isset($q->plan_id) ? $q->plan_id : 0;
        $this->validate();
    }
    /** @Author: ANH DUNG Dec 02, 2018
    *   @Todo: map post to model scales second
    */
    public function windowGetPostSecond($q) {
        $dateTime1 = MyFormat::dateConverYmdToDmy($this->time_1, 'Y-m-d');
//        if($dateTime1 != date('Y-m-d')){
//            $this->addError('time_1', 'Cân lần 2 không hợp lệ, không cùng ngày cân lần 1 '. $this->getTime1());
//            return ;
//        }// Jan2419 Close for can lan 2 from report
        $this->weight_2             = MyFormat::removeComma($q->weight_2);
        $this->offline_id           = $q->offline_id;// DungNT Jan1619
        $this->time_2               = date('Y-m-d H:i:s');
        $this->validate();
    }
    
    /** @Author: ANH DUNG Dec 17, 2018
     *  @Todo: overide some param 
     **/
    public function windowGetPostOffline($q) {
        $this->time_1               = $q->time_1;
        $this->weight_2             = MyFormat::removeComma($q->weight_2);
        $this->time_2               = $q->time_2;
        $this->offline_id           = $q->offline_id;// DungNT Jan1619
        $this->validate();
    }
    
    /** @Author: ANH DUNG Dec 02, 2018
    *   @Todo: save new record cân lần 1
    */
    public function saveScalesFirst() {
        $this->save();
        $this->updateWeightToPlan(GasFile::TYPE_14_SCALES_1);
    }
    public function saveScalesSecond() {// update trọng lượng và image cân lần 2
        $this->update();
        $this->updateWeightToPlan(GasFile::TYPE_15_SCALES_2);
    }
    
    /** @Author: ANH DUNG Dec 02, 2018
     *  @Todo: get model TruckScales người nào tạo sẽ đc update phiếu đó
     */
    public function getModelApp() {
        if(empty($this->id)){
            return null;
        }
        $criteria = new CDbCriteria();
        $criteria->compare('t.id' , $this->id);
//        $criteria->compare('t.uid_login' , $this->uid_login);// Jan419 do 1 xe cân ở 2 người khác nhau (2 ca), nên sẽ chỉ find theo id
        return TruckScales::model()->find($criteria);
    }
    public function getModelAppView() {
        if(empty($this->id)){
            return null;
        }
        $criteria = new CDbCriteria();
        $criteria->compare('t.id' , $this->id);
        return TruckScales::model()->find($criteria);
    }
    
    /** @Author: ANH DUNG Dec 02, 2018
     *  @Todo: dùng chung để format item trả xuống list + view của window
     **/
    public function formatAppItemList() {
        $temp = [];
        $temp['id']                 = $this->id;
        $temp['type']               = $this->type;
        $temp['code_no']            = $this->getCodeNo();
        $temp['car_number']         = $this->getCarNumber();
        $temp['car_owner']          = $this->getCarOwner();
        $temp['cargo_owner']        = $this->getCargoOwner();
        $temp['cargo_name']         = $this->getCargoName();
        $temp['weight_1']           = $this->getWeight1(true);
        $temp['weight_2']           = $this->getWeight2(true);
        $temp['weight_cargo']       = $this->getWeightCargo(true);
        $temp['agent_name']         = $this->getAgent();
        $temp['time_1']             = $this->getTime1('H:i');
        $temp['time_2']             = $this->getTime2('H:i');
        $temp['created_date']       = $this->getCreatedDate('d/m/Y');
        $temp['offline_id']         = $this->offline_id;
        $temp['list_image_1']       = GasFile::apiGetFileUpload($this->rFile1);
        $temp['list_image_2']       = GasFile::apiGetFileUpload($this->rFile2);
        return $temp;
    }
    
    
    /** @Author: ANH DUNG  Dec 02, 2018 -- handle listing api */
    public function handleApiList(&$result, $q) {
        // 1. get list order by user id
        $dataProvider   = $this->apiListing($q);
        $models         = $dataProvider->data;
        $CPagination    = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page']   = $CPagination->pageCount;
        
        $result['message']  = 'L';
        $result['record']   = [];
        
        if( $q->page >= $CPagination->pageCount ){
            $result['record'] = [];
        }else{
            foreach($models as $model){
                $model->mAppUserLogin       = $this->mAppUserLogin;// for canPickCancel
                $result['record'][]         = $model->formatAppItemList();
            }
        }
    }
    
    /**
    * @Author: ANH DUNG Dec 02, 2018
    * @Todo: get data listing 
    * @param: $q object post params from app client
     * $q->date_from, $q->date_to format: d-m-Y
    */
    public function apiListing($q) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.agent_id=' . $this->agent_id);
        if(!empty($q->date_from) && !empty($q->date_to) ){
            $date_from  = MyFormat::dateDmyToYmdForAllIndexSearch($q->date_from);
            $date_to    = MyFormat::dateDmyToYmdForAllIndexSearch($q->date_to);
            DateHelper::searchBetween($date_from, $date_to, 'created_date_bigint', $criteria, true);
        }
        
        $criteria->order = 't.id DESC';
        $dataProvider=new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize' => 150,// Đưa vào List cache sẽ luôn load page 0, nên sẽ để 20 item 
                'currentPage' => (int)$q->page,
            ),
        ));
        return $dataProvider;
    }
    
    /** @Author: ANH DUNG Dec 02, 2018
     *  @Todo: get first image on ScalesFirst to detect car number
     **/
    public function getImageWithCarNumber() {
        $aFile = GasFile::apiGetFileUpload($this->rFile1);
        foreach($aFile as $aInfo){
            return $aInfo['large'];
        }
        return '';
    }
    
    /** @Author: ANH DUNG Dec 02, 2018
     *  @Todo: get car number from image
     **/
    public function setCarNumberFromImage() {
        $urlImage = $this->getImageWithCarNumber();
        if(empty($urlImage)){
            return ;
        }
        Yii::import('ext.gvision.gvision');
        $mGvision = new gvision();
//        $mGvision->path = 'http://dev.spj.vn/x2.jpg';
        $mGvision->path = $urlImage;
        $mGvision->getContent();
        if(count($mGvision->aTextFound) < 1){
            return ;
        }
        $aTemp = [];
        $aTemp['url']                   = $mGvision->path;
        $aTemp['aTextFound']            = $mGvision->aTextFound;
        $aTemp['textSelect']            = $mGvision->textSelect;
        $this->car_number_from_image    = $mGvision->textSelect;
        if($this->type == TruckScales::TYPE_INTERNAL){
//            $this->car_number               = $mGvision->textSelect;// Jan1719 Tam Close lại đã
        }
        $this->json = MyFormat::jsonEncode($aTemp);
        $this->update();
    }

    /** @Author: PHAM THANH NGHIA 2019 */
    public function searchIndex()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.type',$this->type);
        $criteria->compare('t.is_online',$this->is_online);
        $criteria->compare('t.code_no',$this->code_no);
        $criteria->compare('t.car_number',$this->car_number, true);
        $criteria->compare('t.uid_login',$this->uid_login);
        $criteria->compare('t.agent_id',$this->agent_id);
        $criteria->compare('t.uid_login',$this->uid_login);
        
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
            DateHelper::searchGreater($date_from, 'created_date_bigint', $criteria);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
            DateHelper::searchSmaller($date_to, 'created_date_bigint', $criteria, true);
        }
        
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /** @Author: DungNT Aug 08, 2019
     *  @Todo: web get html view file on grid
     **/
    public function getFileOnly() {
        $aFile1 = $this->rFile1;
        $aFile2 = $this->rFile2;
        $str = '';
        if(count($aFile1)){
            $str = '<br>Cân 1: ';
        }
        foreach($aFile1 as $key=>$item){
            $str.="<a target='_blank' class='gallery' href='".Yii::app()->createAbsoluteUrl('admin/ajax/viewImageProfileHs', array('id'=>$item->id, 'model'=>'GasFile', 'nameRelation' => 'rFile1'))."'> ";
                $str.= 'Hình '.($key+1);
            $str.="</a>";
        }
        if(count($aFile2)){
            $str .= '<br>Cân 2: ';
        }
        foreach($aFile2 as $key=>$item){
            $str.="<a target='_blank' class='gallery' href='".Yii::app()->createAbsoluteUrl('admin/ajax/viewImageProfileHs', array('id'=>$item->id, 'model'=>'GasFile', 'nameRelation' => 'rFile2'))."'> ";
                $str.= 'Hình '.($key+1);
            $str.="</a>";
        }
       return $str;
    }
    
    /** @Author: DungNT Aug 08, 2019
     *  @Todo: update weight of cargo to plan of car by truck_plan_detail_id
     *  @param: $timesWeigh lần cân
     * 1. update to plan
     * 2. notify app driver and thủ kho
     * 
     **/
    public function updateWeightToPlan($timesWeigh) {
        if(empty($this->truck_plan_detail_id)){
            return ;
        }
        $firstWeigh                 = $this->getWeight1(true);
        $secondWeigh                = $this->getWeight2(true);
        $mTruckPlanDetail           = $this->rTruckPlanDetail;
        $mTruckPlanDetail->truck_scales_id = $this->id;
        if($timesWeigh == GasFile::TYPE_15_SCALES_2){// cân lần 2
            $mTruckPlanDetail->qty_real = $this->getWeightCargo();
            $mTruckPlanDetail->update(['qty_real']);
            $mTruckPlanDetail->titleNotify = "{$mTruckPlanDetail->getPlanDate()} {$mTruckPlanDetail->getCar()} Cân lần 2 thành công: {$secondWeigh} kg {$mTruckPlanDetail->getCodeNo()}";
        }else{
            $mTruckPlanDetail->titleNotify = "{$mTruckPlanDetail->getPlanDate()} {$mTruckPlanDetail->getCar()} Cân lần 1 thành công: {$firstWeigh} kg {$mTruckPlanDetail->getCodeNo()}";
            $mTruckPlanDetail->update(['truck_scales_id']);
        }
        $mTruckPlanDetail->notifyEmployee();
    }
    
}