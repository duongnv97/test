<?php

/**
 * This is the model class for table "{{_users_price_request}}".
 *
 * The followings are the available columns in table '{{_users_price_request}}':
 * @property string $id
 * @property integer $type
 * @property integer $c_month
 * @property integer $c_year
 * @property integer $status
 * @property string $uid_approved_1
 * @property string $uid_approved_2
 * @property string $uid_approved_3
 * @property string $uid_login
 * @property string $created_date
 * @property string $json
 * @property string $json_note
 */
class UsersPriceRequest extends BaseSpj
{
    const TYPE_REQUEST_PRICE        = 1; // đề xuất tăng
    const TYPE_PRICE_ASCENDING      = 2;// tăng định kỳ theo tháng
    const TYPE_PRICE_ASCENDING_HISTORY = 3;// lịch sử tăng định kỳ hàng tháng
    
    const STATUS_NEW            = 0;
    const STATUS_APPROVED_1     = 1;
    const STATUS_APPROVED_2     = 2;
    const STATUS_APPROVED_3     = 3;
    const STATUS_REJECT         = 4;
    
    const ITEM_APPROVED = 1;
    const ITEM_REJECT   = 2;
    const PRICE_UP      = 1;
    const PRICE_DOWN    = 2;
    
    public $JSON_FIELD      = array('customer_id', 'price_code', 'value', 'note', 'approved_1', 'approved_2', 'approved_3');
    public $JSON_FIELD_NOTE = array('date_approved_1', 'date_approved_2', 'date_approved_3', 'note_approved_1', 'note_approved_2', 'note_approved_3');
    public $aModelPricePrevMonth = array(), $aDetail = array(), $aCustomerId = array(), $autocomplete_name, $listdataApproved = array(), $listdataStatus = array();
    public $viewApprovedDropdown = false, $formApproved = null, $approved_1, $approved_2, $approved_3;
    public $classTableInfo='', $note_approved_1, $note_approved_2, $note_approved_3, $fieldNote='not_setup1', $date_approved_1, $date_approved_2, $date_approved_3;
    public $price_code, $value, $price_code_small, $value_small, $showDropdown=false;
    
    public function getArrayStatus() {
        return array(
            self::STATUS_NEW             => 'Mới',
            self::STATUS_APPROVED_1      => 'Duyệt lần 1',
            self::STATUS_APPROVED_2      => 'Duyệt lần 2',
            self::STATUS_APPROVED_3      => 'Duyệt lần 3',
            self::STATUS_REJECT          => 'Hủy bỏ',
        );
    }
    public function getStatusText() {
        if($this->type != self::TYPE_REQUEST_PRICE){
            return '';
        }
        $aStatus = $this->getArrayStatus();
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : "";
    }
    
    public function getArrayActionApproved() {
        return array(
            self::ITEM_APPROVED => 'Duyệt',
            self::ITEM_REJECT   => 'Hủy bỏ',
        );
    }
    public function getActionApprovedText($value) {
        $aAction = $this->getArrayActionApproved();
        return isset($aAction[$value]) ? $aAction[$value] : '';
    }
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_users_price_request}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('uid_approved_1', 'required', 'on'=>'create, update'),
            array('status, uid_approved_2', 'required', 'on'=>'UserApproved1'),
            array('status, uid_approved_3', 'required', 'on'=>'UserApproved2'),
            array('status', 'required', 'on'=>'UserApproved3'),
            array('json_note, id, type, c_month, c_year, status, uid_approved_1, uid_approved_2, uid_approved_3, uid_login, created_date, json', 'safe'),
            array('customer_id, price_code, value, price_code_small, value_small, note, approved_1, approved_2, approved_3', 'safe'),
            array('date_approved_1, date_approved_2, date_approved_3, note_approved_1, note_approved_2, note_approved_3', 'safe'),
            
        );
    }
    
    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rApproved1' => array(self::BELONGS_TO, 'Users', 'uid_approved_1'),
            'rApproved2' => array(self::BELONGS_TO, 'Users', 'uid_approved_2'),
            'rApproved3' => array(self::BELONGS_TO, 'Users', 'uid_approved_3'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                'id' => 'ID',
                'type' => 'Type',
                'c_month' => 'Tháng',
                'c_year' => 'Năm',
                'status' => 'Trạng thái',
                'uid_approved_1' => 'Duyệt lần 1',
                'uid_approved_2' => 'Duyệt lần 2',
                'uid_approved_3' => 'Duyệt lần 3',
                'uid_login' => 'Người tạo',
                'created_date' => 'Ngày tạo',
                'json' => 'Json',
                
                'customer_id' => 'Khách hàng',
                'price_code' => 'Bình bò',
                'value' => 'Giá bình bò',
                'price_code_small' => 'Bình 12',
                'value_small' => 'Giá 12',
                
                'note' => 'Ghi chú',
                'approved_1' => 'Duyệt lần 1',
                'approved_2' => 'Duyệt lần 2',
                'approved_3' => 'Duyệt lần 3',
                'date_approved_1' => 'Ngày duyệt lần 1',
                'date_approved_2' => 'Ngày duyệt lần 2',
                'date_approved_3' => 'Ngày duyệt lần 3',
                'note_approved_1' => 'Ghi chú lần 1',
                'note_approved_2' => 'Ghi chú lần 2',
                'note_approved_3' => 'Ghi chú lần 3',

            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        $criteria=new CDbCriteria;
        $criteria->compare('t.type',$this->type);
        $criteria->compare('t.c_month',$this->c_month);
        $criteria->compare('t.c_year',$this->c_year);
        $criteria->compare('t.status',$this->status);
        $criteria->compare('t.uid_approved_1',$this->uid_approved_1);
        $criteria->compare('t.uid_approved_2',$this->uid_approved_2);
        $criteria->compare('t.uid_approved_3',$this->uid_approved_3);
        $criteria->compare('t.uid_login',$this->uid_login);
        $criteria->order = 't.id DESC';
        $this->handleRoleLimit($criteria, $cUid, $cRole);
        $data = new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
        $_SESSION['outputCustomer'] = $this->getQuantity($data);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    public function handleRoleLimit(&$criteria, $cUid, $cRole) {
        $query = $query1 = '';
        $aStatusApproved    = array(self::STATUS_REJECT);
        $typeAsc            = UsersPriceRequest::TYPE_PRICE_ASCENDING;
        $sParamsIn = implode(',', $aStatusApproved);
        if ($this->isUserApproved1()) {
            $query .= "t.status NOT IN ($sParamsIn) AND (t.uid_login = $cUid OR t.uid_approved_1=$cUid OR t.uid_approved_2=$cUid)";
        }elseif ($this->isUserApproved2()) {
            $query .= "t.status NOT IN ($sParamsIn) AND  (t.type = $typeAsc OR t.uid_login = $cUid  OR t.uid_approved_2= $cUid )";
        }elseif ($this->isUserApproved3()) {
            $query .= "t.status NOT IN ($sParamsIn) AND (t.type = $typeAsc OR t.uid_login = $cUid OR t.uid_approved_3=$cUid)";
        }
        if($query != ''){
            $criteria->addCondition($query);
        }
        if(in_array($cRole, UsersPrice::model()->getRoleLikeSale())){
            $criteria->addCondition('t.uid_login='.$cUid);
        }
    }
    
    /**
     * @Author: DungNT Apr 18, 2017
     * @Todo: limit không cho user tạo hay update trước ngày 28
     */
    public function canCreateOrUpdate() {
        return true;// Now3018 luôn open để Sale Tạo
        if(date('d') == date('t')){
            return false;// only dev test
        }
        $cRole = MyFormat::getCurrentRoleId();
//        if(date('d') > 27 && $cRole != ROLE_DIRECTOR_BUSSINESS){
//        if(date('d') > 28 && $cRole != ROLE_DIRECTOR_BUSSINESS){
//            return false;
//        }
        return true;
    }
    
    /**
     * @Author: DungNT Sep 03, 2016
     * Allow someone see update button
     * @return bool
     */
    public function canUpdate()
    {
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        if(!$this->canCreateOrUpdate()){
            return false;
        }
        if($cUid == GasLeave::UID_DIRECTOR_BUSSINESS && $this->type == UsersPriceRequest::TYPE_PRICE_ASCENDING){
            return true;
        }
//        return true;// only dev test
        
        if ($cUid != $this->uid_login || $this->status != UsersPriceRequest::STATUS_NEW) {
            return false; // đã duyệt rồi ko sửa dc, còn những user khác thì ai tạo thì dc sửa của người đó
        }
        // nếu là new thì cho update thoải mái, chỉ khi nào nó update complete thì mới check
        $nextMonth  = MyFormat::modifyDays($this->created_date, 1, '+', 'month');
        $tempMonth  = explode('-', $nextMonth);
        $firstDateNextMonth  = $tempMonth[0].'-'.$tempMonth[1].'-01';
        return MyFormat::compareTwoDate($firstDateNextMonth, date('Y-m-d'));
    }
    public function getUidLogin() {
        $mUser = $this->rUidLogin;
        if($mUser){
            return $mUser->getFullName();
        }
        return '';
    }
    public function getCreatedDate($fomat = 'd/m/Y H:i') {
        return MyFormat::dateConverYmdToDmy($this->created_date, $fomat);
    }
    public function getMonthYear() {
        return $this->c_month.'/'.$this->c_year;
    }
    public function getTypeText() {
        if($this->type == UsersPriceRequest::TYPE_PRICE_ASCENDING){
            return '<span class="item_b"><br>Tăng định kỳ</span>';
        }
        return '';
    }
    public function getApproved1() {
        $mUser = $this->rApproved1;
        if($mUser){
            return $mUser->getFullName();
        }
        return '';
    }
    public function getApproved2() {
        $mUser = $this->rApproved2;
        if($mUser){
            return $mUser->getFullName();
        }
        return '';
    }
    public function getApproved3() {
        $mUser = $this->rApproved3;
        if($mUser){
            return $mUser->getFullName();
        }
        return '';
    }
    public function initForCreate() {
        $this->type     = UsersPriceRequest::TYPE_REQUEST_PRICE;
        $firstDate      = date('Y-m').'-01';
        $nextMonth      = MyFormat::modifyDays($firstDate, 1, '+', 'month');
        $this->c_month  = MyFormat::dateConverYmdToDmy($nextMonth, 'm');
        $this->c_year   = MyFormat::dateConverYmdToDmy($nextMonth, 'Y');
    }

    public function getHistoryApproved() {
        $res = '';
        if(!empty($userApproved = $this->getApproved3())){
            $res .= '<b>Duyệt lần 3: '.$userApproved.'</b>';
            $date = $this->getJsonNoteField('date_approved_3');
            $note = nl2br($this->getJsonNoteField('note_approved_3'));
            if(!empty($date)){
//                $date = MyFormat::dateConverYmdToDmy($date,'d/m H:i');
                $date = MyFormat::dateConverYmdToDmy($date,'d/m');
                $res .= '<br>'.$date;
                $res .= '<br>'.$note;
            }
        }
        if(!empty($userApproved = $this->getApproved2())){
            $res .= '<br><b>Duyệt lần 2: '.$userApproved.'</b>';
            $date = $this->getJsonNoteField('date_approved_2');
            $note = nl2br($this->getJsonNoteField('note_approved_2'));
            if(!empty($date)){
                $date = MyFormat::dateConverYmdToDmy($date,'d/m');
                $res .= '<br>'.$date;
                $res .= '<br>'.$note;
            }
        }
        if(!empty($userApproved = $this->getApproved1())){
            $res .= '<br><b>Duyệt lần 1: '.$this->getApproved1().'</b>';
            $date = $this->getJsonNoteField('date_approved_1');
            $note = nl2br($this->getJsonNoteField('note_approved_1'));
            if(!empty($date)){
                $date = MyFormat::dateConverYmdToDmy($date,'d/m');
                $res .= '<br>'.$date;
                $res .= '<br>'.$note;
            }
        }
        return $res;
    }
    
        /**
     * @Author: DungNT Jan 31, 2017
     * @Todo: get html icon status duyệt lần 3
     */
    public function getHtmlIconStatus() {
        $res = '';
        if($this->status == self::STATUS_APPROVED_3){
            $img = Yii::app()->createAbsoluteUrl(''). '/themes/gas/images/Checked-icon.png';
            $res = '<span class="AddIconGrid display_none"><br><br><img src="'.$img.'" alt="Duyệt lần 3" title="Duyệt lần 3"></span>';
        }
        return $res;
    }
    public function getDetailInfoSchedule() {
        $str = '';
        $json = json_decode($this->json, true);
        if(!isset($json['customer_id'])){
            return '';
        }
        $aCustomerId    = array();
        $aCustomer  = $this->getModelCustomer($json, $aCustomerId);
        $str .= '<table class="f_size_15 '.$this->classTableInfo.'"  cellpadding="0" cellspacing="0">';
        $str .= '<thead>';
            $str .= '<tr>';
                $str .= '<td class="item_c item_b">#</td>';
                $str .= '<td class="item_c item_b">Khách hàng</td>';
                $str .= '<td class="item_c item_b">Tăng định kỳ</td>';
                $str .= '<td class="item_c item_b">Ghi chú</td>';
            $str .= '</tr>';
        $str .= '</thead>';
        $str .= '<tbody>';
        
        
        foreach($json['customer_id'] as $key => $customer_id){
            $customerName  = isset($aCustomer[$customer_id]) ? $aCustomer[$customer_id]->getFullName() : '';
            $value         = isset($json['value'][$key]) ? ActiveRecord::formatCurrency($json['value'][$key]) : '';
            $note          = isset($json['note'][$key]) ? $json['note'][$key] : 0;
            
            $str .= '<tr>';
                $str .= '<td class="item_c">'.($key+1).'</td>';
                $str .= '<td>'.$customerName.'</td>';
                $str .= '<td class="item_c">'.$value.'</td>';
                $str .= '<td>'.$note.'</td>';
            $str .= '</tr>';
        }
        $str .= "</tbody> ";
        $str .= "</table> ";
        return $str;
    }
    
    public function getDetailInfo() {
        if($this->type != self::TYPE_REQUEST_PRICE){
            return $this->getDetailInfoSchedule();
        }
        $str = '';
        $json = json_decode($this->json, true);
        if(!isset($json['customer_id'])){
            return '';
        }
        $OUTPUT_CUSTOMER = $_SESSION['outputCustomer'];
        $monthRequest   = $this->c_year.'-'.  $this->c_month.'-01';
        $prevMonth      = MyFormat::modifyDays($monthRequest, 1, '-', 'month');
        $prevMonthShow  = MyFormat::dateConverYmdToDmy($prevMonth, 'm/y');
        $monthRequestShow  = MyFormat::dateConverYmdToDmy($monthRequest, 'm/y');
        $aCustomerId    = array();
        $aCustomer  = $this->getModelCustomer($json, $aCustomerId);
        $tmpMonth   = explode('-', $prevMonth);
        $aModelPricePrevMonth  = UsersPrice::getBoMoiOfCustomer($tmpMonth[1], $tmpMonth[0], $aCustomerId);
        $str .= '<table class="f_size_15 '.$this->classTableInfo.'"  cellpadding="0" cellspacing="0">';
        $str .= '<thead>';
            $str .= '<tr>';
                $str .= '<td class="item_c item_b">#</td>';
                $str .= '<td class="item_c item_b">Khách hàng</td>';
                $str .= '<td class="item_c item_b">Sản lượng<br>'.$prevMonthShow.'</td>';
                $str .= '<td class="item_c item_b">Bình bò<br>'.$prevMonthShow.'</td>';
                $str .= '<td class="item_c item_b">Bình 12<br>'.$prevMonthShow.'</td>';
                $str .= '<td class="item_c item_b">Đề xuất BB</td>';
                $str .= '<td class="item_c item_b">Đề xuất B12</td>';
                $str .= '<td class="item_c item_b">Ghi chú</td>';
                $str .= '<td class="item_c item_b">Lần 1</td>';
                $str .= '<td class="item_c item_b">Lần 2</td>';
                $str .= '<td class="item_c item_b">Lần 3</td>';
            $str .= '</tr>';
        $str .= '</thead>';
        $str .= '<tbody>';
        
        foreach($json['customer_id'] as $key => $customer_id){
            $customerName  = isset($aCustomer[$customer_id]) ? '<b>'.$aCustomer[$customer_id]->code_bussiness.' - ' .$aCustomer[$customer_id]->getFullName().'</b><br>'.$aCustomer[$customer_id]->getAddress() : '';
            $output_customer = isset($OUTPUT_CUSTOMER[$customer_id]) ? $OUTPUT_CUSTOMER[$customer_id] : 0;
            $price_code    = isset($json['price_code'][$key]) ? $json['price_code'][$key] : 0;
            $value         = isset($json['value'][$key]) ? $json['value'][$key] : 0;
            $price_code_small   = isset($json['price_code_small'][$key]) ? $json['price_code_small'][$key] : 0;
            $value_small        = isset($json['value_small'][$key]) ? $json['value_small'][$key] : 0;
            $note          = isset($json['note'][$key]) ? $json['note'][$key] : '';
            $approved_1    = isset($json['approved_1'][$key]) ? $json['approved_1'][$key] : 0;
            $approved_2    = isset($json['approved_2'][$key]) ? $json['approved_2'][$key] : 0;
            $approved_3    = isset($json['approved_3'][$key]) ? $json['approved_3'][$key] : 0;
            $up_down        = isset($json['up_down'][$key]) ? $json['up_down'][$key] : 0;
            $up_down_small  = isset($json['up_down_small'][$key]) ? $json['up_down_small'][$key] : 0;
            
            if($price_code == UsersRef::PRICE_OTHER){
                $value = ActiveRecord::formatCurrency($value);
            }
            if($price_code_small == UsersRef::PRICE_OTHER){
                $value_small = ActiveRecord::formatCurrency($value_small);
            }
            $prevPrice = '';
            $prevPriceSmall = '';
            if(isset($aModelPricePrevMonth[$customer_id])){
                $prevPrice = $aModelPricePrevMonth[$customer_id]->price_code;
                if($prevPrice == UsersRef::PRICE_OTHER){
                    $prevPrice = ActiveRecord::formatCurrency($aModelPricePrevMonth[$customer_id]->price);
                }
                if(!empty($aModelPricePrevMonth[$customer_id]->price_code_small)){
                    $prevPriceSmall = $aModelPricePrevMonth[$customer_id]->price_code_small;
                    if($prevPriceSmall == UsersRef::PRICE_OTHER){
                        $prevPriceSmall = ActiveRecord::formatCurrency($aModelPricePrevMonth[$customer_id]->price_small);
                    }
                }
            }
            $classHighLight = '';
            if($up_down == UsersPriceRequest::PRICE_DOWN || $up_down_small == UsersPriceRequest::PRICE_DOWN){
                $classHighLight = 'high_light_tr';
            }
            
            $str .= '<tr class="cash_book_row">';
                $str .= '<td class="item_c '.$classHighLight.'">'.($key+1).'</td>';
                $str .= '<td>'.$customerName.'</td>';
                $str .= '<td>'.$output_customer.'</td>';
                $str .= '<td class="item_c">'.$prevPrice.'</td>';
                $str .= '<td class="item_c">'.$prevPriceSmall.'</td>';
//                $str .= '<td class="item_c">'.$price_code.'</td>';
//                $str .= '<td class="item_c">'.$price_code_small.'</td>';
                $str .= '<td class="item_c">'.$this->getDropdownRequest($customer_id, $note, 'price_code[]', $price_code, $value, 'value[]',UsersRef::model()->getListoptionPriceBinhBo()).'</td>';
                $str .= '<td class="item_c">'.$this->getDropdownRequest('','','price_code_small[]', $price_code_small, $value_small, 'value_small[]',UsersRef::model()->getListoptionPriceBinh12()).'</td>';
                $str .= '<td>'.$note.'</td>';
                $str .= '<td class="item_c">'.$this->getDropdownApproved('approved_1', $approved_1, $this->canViewDropdown1()).'</td>';
                $str .= '<td class="item_c">'.$this->getDropdownApproved('approved_2', $approved_2, $this->canViewDropdown2()).'</td>';
                $str .= '<td class="item_c">'.$this->getDropdownApproved('approved_3', $approved_3, $this->canViewDropdown3()).'</td>';
            $str .= '</tr>';
            
//            $mDetail->approved_1    = isset($json['approved_1'][$key]) ? $json['approved_1'][$key] : '';
//            $mDetail->approved_2    = isset($json['approved_2'][$key]) ? $json['approved_2'][$key] : '';
//            $mDetail->approved_3    = isset($json['approved_3'][$key]) ? $json['approved_3'][$key] : '';
        }
        $str .= '</tbody>';
        $str .= '</table>';
        return $str;
    }
    
    /**
     * @Author: DungNT Dec 22, 2016
     * @Todo: get dropdown approved when view * approved_1
     */
    public function getDropdownApproved($fieldName, $fieldValue, $canViewApprovedDropdown) {
        if(!$canViewApprovedDropdown || is_null($this->formApproved) || !$this->showDropdown){
            return $this->getActionApprovedText($fieldValue);
        }
        $html = $this->formApproved->dropDownList($this, $fieldName.'[]', $this->getArrayActionApproved(),
                    array('class'=>'w-100 ActionApproved',
                        'options' => array($fieldValue => array('selected'=>true))
        ));
//        $html .= "<input name='i' value='$index' type='hidden'>";
        return $html;
    }
    /**
     * @Author: DungNT Jan 02, 2017
     * @Todo: get dropdown đề xuất để có thể change khi khi duyệt
     */
    public function getDropdownRequest($customer_id, $note, $fieldName, $fieldValue, $value, $nameInputOther, $listdata) {
        if(!$this->showDropdown){
            if($fieldValue == UsersRef::PRICE_OTHER){
                return $value;
            }
            return $fieldValue;
        }
        $classDropdow   = 'gSelect';
        $classInput     = 'gPrice';
        if($fieldName == 'price_code_small[]'){
            $classDropdow   = 'gSelectSmall';
            $classInput     = 'gPriceSmall';
        }
        /* Jan 02, 2017 xử lý render html select
         * chỗ này có cần phải post đủ thông tin như lúc tạo luôn thì mới xử lý được?
         */
//        $html = $this->formApproved->dropDownList($this, $fieldName, $listdata,
//                    array('class'=>'w-100 ActionChangeRequest '.$classDropdow, 'empty'=>'Select',
//                        'options' => array($fieldValue => array('selected'=>true))
//        ));
        $html = CHtml::dropDownList($fieldName, $fieldValue, $listdata,
                array('class'=>'w-100 ActionChangeRequest '.$classDropdow, 'empty'=>'Select')
            );
        
        $displayInput = '';
        if($fieldValue != UsersRef::PRICE_OTHER){
            $displayInput = 'display_none';
        }
        $html .= '<input name="'.$nameInputOther.'" class="'.$displayInput. ' ' .$classInput.' w-100  ad_fix_currency f_size_15" maxlength="9" value="'.MyFormat::removeComma($value).'" type="text" placeholder="Giá Khác">';
        if($fieldName == 'price_code[]'){
            $html .= '<input name="customer_id[]" class=" " value="'.$customer_id.'" type="hidden">';
            $html .= '<input name="note[]" class=" " value="'.$note.'" type="hidden">';
        }
        return $html;
    }
    
    public function handleApprovedValidate() {
        if($this->scenario == 'UserApproved1' && !isset($_POST['UsersPriceRequest']['approved_1'])){
            $this->addError('status', 'Dữ liệu không hợp lệ 1');
        }elseif($this->scenario == 'UserApproved2' && !isset($_POST['UsersPriceRequest']['approved_2'])){
            $this->addError('status', 'Dữ liệu không hợp lệ 2');
        }elseif($this->scenario == 'UserApproved3' && !isset($_POST['UsersPriceRequest']['approved_3'])){
            $this->addError('status', 'Dữ liệu không hợp lệ 3');
        }
    }
    
    /**
     * @Author: DungNT Dec 22, 2016
     * @Todo: user submit approved
     */
    public function handleApproved() {
        $aUpdate = array('status', 'json', 'json_note');
        $aRowInsert = $aCustomerDuplicate = array();
        $this->buildArrayJsonInsert($aRowInsert, $aCustomerDuplicate);
        $aDataJsonUpdate = array(// Jan 02, 2017 xử lý cho phép change G request khi cập nhật
            'price_code'        => $aRowInsert['price_code'],
            'value'             => $aRowInsert['value'],
            'price_code_small'  => $aRowInsert['price_code_small'],
            'value_small'       => $aRowInsert['value_small'],
            'up_down'           => $aRowInsert['up_down'],
            'up_down_small'     => $aRowInsert['up_down_small'],
            'note'              => $aRowInsert['note'],
        );

        if($this->scenario == 'UserApproved1'){
            $aUpdate[] = 'uid_approved_2';
//            $this->updateJsonField('approved_1', $_POST['UsersPriceRequest']['approved_1']);
            $aDataJsonUpdate['approved_1'] = $_POST['UsersPriceRequest']['approved_1'];
            $this->updateJsonNoteField(1, $_POST['UsersPriceRequest']['note_approved_1']);
            
            $needMore = array('aUidMerge' => array($this->uid_approved_2));
//            GasScheduleEmail::buildNotifyPriceRequest($this, $needMore);
        }elseif($this->scenario == 'UserApproved2'){
            $aUpdate[] = 'uid_approved_3';
//            $this->updateJsonField('approved_2', $_POST['UsersPriceRequest']['approved_2']);
            $aDataJsonUpdate['approved_2'] = $_POST['UsersPriceRequest']['approved_2'];
            $this->updateJsonNoteField(2, $_POST['UsersPriceRequest']['note_approved_2']);
            
            $needMore = array('aUidMerge' => array($this->uid_approved_3));
//            GasScheduleEmail::buildNotifyPriceRequest($this, $needMore);// ko mail anh Long
        }elseif($this->scenario == 'UserApproved3'){
//            $this->updateJsonField('approved_3', $_POST['UsersPriceRequest']['approved_3']);
            $aDataJsonUpdate['approved_3'] = $_POST['UsersPriceRequest']['approved_3'];
            $this->updateJsonNoteField(3, $_POST['UsersPriceRequest']['note_approved_3']);
        }
        $this->updateJsonFieldMulti($aDataJsonUpdate);
        $this->update($aUpdate);
    }
    
    /**
     * @Author: DungNT Dec 22, 2016
     * @Todo: cập nhật status các request price vào json
     */
    public function updateJsonField($fieldUpdate, $fieldValue) {
        $json = json_decode($this->json, true);
        if(!isset($json['customer_id'])){
            return '';
        }
        $json[$fieldUpdate] = $fieldValue;
        $this->json         = MyFormat::jsonEncode($json);
    }
    public function updateJsonFieldMulti($aData) {
        $json = json_decode($this->json, true);
        if(!isset($json['customer_id'])){
            return '';
        }
        foreach($aData as $fieldUpdate => $fieldValue){
            $json[$fieldUpdate] = $fieldValue;
        }
        $this->json         = MyFormat::jsonEncode($json);
    }
    public function updateJsonNoteField($index, $fieldValue) {
        $json = json_decode($this->json_note, true);
        $json['note_approved_'.$index] = $fieldValue;
        $json['date_approved_'.$index] = date('Y-m-d H:i:s');
        $this->json_note         = MyFormat::jsonEncode($json);
    }
    public function getJsonNoteField($fieldName) {
        $json = json_decode($this->json_note, true);
        return $this->$fieldName = isset($json[$fieldName]) ? $json[$fieldName] : '';
    }
    
    /**
     * @Author: DungNT Dec 19, 2016
     * @Todo: get array model customer
     */
    public function getModelCustomer($json, &$aCustomerId) {
        foreach($json['customer_id'] as $key => $customer_id){
            if(empty($customer_id)){
                continue ;
            }
            $aCustomerId[]    = $customer_id;
        }
        if(count($aCustomerId) < 1){
            return $aCustomerId;
        }
        return Users::getArrObjectUserByRole('', $aCustomerId);
    }
    
    /**
     * @Author: DungNT Jul 03, 2016
     */
    public function listDataPriceCode() {
        return UsersRef::model()->getListoptionPrice();
    }
    // get list model Customer for one query
    public function getListModelCustomer() {
        if(!is_array($this->aCustomerId) || count($this->aCustomerId) < 1){
            return array();
        }
        return Users::getArrObjectUserByRole('', $this->aCustomerId);
    }
    
    
    protected function beforeValidate() {
        $this->buildPostDetail();
        return parent::beforeValidate();
    }
    
    /**
     * @Author: DungNT Dec 26, 2016
     * @Todo: lấy list customer id để check giá setup tháng hiện tại có chưa, nếu chưa có thì ko cho làm đề xuất
     */
    public function getListCustomerIdPost() {
        foreach($_POST['customer_id'] as $key => $customer_id){
            if(empty($customer_id)){
                continue ;
            }
            $this->aCustomerId[]    = $customer_id;
        }
    }
    
    public function buildPostDetail() {
        if(!isset($_POST['customer_id']) || $this->type != UsersPriceRequest::TYPE_REQUEST_PRICE){
            // hàm này chỉ xử lý cho TYPE_REQUEST_PRICE
            return ;
        }
        $this->getListCustomerIdPost();
        $monthRequest   = $this->c_year.'-'.  $this->c_month.'-01';
        $prevMonth      = MyFormat::modifyDays($monthRequest, 1, '-', 'month');
        $tmpMonth       = explode('-', $prevMonth);
        // $aModelPricePrevMonth check phải có setup giá tháng trước thì mới cho đề xuất
        $this->aModelPricePrevMonth  = UsersPrice::getBoMoiOfCustomer($tmpMonth[1], $tmpMonth[0], $this->aCustomerId);
        
        $this->aDetail      = array();
        $this->aCustomerId  = array();
        $aCustomerError     = array();$aCustomerErrorZero = array();
        foreach($_POST['customer_id'] as $key => $customer_id){
            if(empty($customer_id)){
                continue ;
            }
            if(!isset($this->aModelPricePrevMonth[$customer_id])){
                $aCustomerError[]   = $customer_id;
            }

            $mDetail = new UsersPriceRequestDetail();
            $mDetail->customer_id   = $customer_id;
            $mDetail->price_code    = isset($_POST['price_code'][$key]) ? $_POST['price_code'][$key] : 0;
            $mDetail->value         = isset($_POST['value'][$key]) ? MyFormat::removeComma($_POST['value'][$key]) : 0;
            $mDetail->price_code_small    = isset($_POST['price_code_small'][$key]) ? $_POST['price_code_small'][$key] : 0;
            $mDetail->value_small         = isset($_POST['value_small'][$key]) ? MyFormat::removeComma($_POST['value_small'][$key]) : 0;
            $mDetail->note          = isset($_POST['note'][$key]) ? $_POST['note'][$key] : 0;
            $mDetail->approved_1    = isset($_POST['approved_1'][$key]) ? $_POST['approved_1'][$key] : '';
            $mDetail->approved_2    = isset($_POST['approved_2'][$key]) ? $_POST['approved_2'][$key] : '';
            $mDetail->approved_3    = isset($_POST['approved_3'][$key]) ? $_POST['approved_3'][$key] : '';

            if( ($mDetail->price_code == UsersRef::PRICE_OTHER && $mDetail->value==0) || ($mDetail->price_code_small == UsersRef::PRICE_OTHER && $mDetail->value_small==0) ) {
                $aCustomerErrorZero[] = $mDetail->customer_id;
            }
//            if(empty($mDetail->price_code) && empty($mDetail->price_code_small)){
//                continue ;
//            }// nếu để đk này thì phần tạo định kỳ không được
            
            $this->aDetail[]        = $mDetail;
            $this->aCustomerId[]    = $mDetail->customer_id;
        }
        if(count($this->aDetail) < 1){
            $this->addError('uid_approved_1', 'Có lỗi: Chưa chọn khách hàng');
        }
        if(count($aCustomerError)){
            $msg = 'Có lỗi: Khách hàng chưa được thiết lập giá tháng hiện tại: '.date('m/Y').': <br>'. $this->getStringCustomerError($aCustomerError);
            $this->addError('uid_approved_1', $msg);
        }
        if(count($aCustomerErrorZero)){
            $msg = 'Có lỗi: Khách hàng giá khác phải lớn hơn 0 <br>'. $this->getStringCustomerError($aCustomerErrorZero);
            $this->addError('uid_approved_1', $msg);
        }
    }
    
    /**
     * @Author: DungNT Jan 02, 2017
     * @Todo: tính toán price đề xuất up or down
     */
    public function calcUpDown($customer_id, $gSetup, &$up_down, &$up_down_small, 
            $price_code, $value, $price_code_small, $value_small) {
        if(!isset($this->aModelPricePrevMonth[$customer_id])){
            return ;
        }

        $prevPrice          = $this->aModelPricePrevMonth[$customer_id]->price;
        $prevPriceSmall     = $this->aModelPricePrevMonth[$customer_id]->price_small;
        $cValue         = isset($gSetup[$price_code]) ? $gSetup[$price_code] : 0;
        $cValueSmall    = isset($gSetup[$price_code_small]) ? $gSetup[$price_code_small] : 0;
        if($price_code == UsersRef::PRICE_OTHER){
            $cValue = $value;
        }
        if($price_code_small == UsersRef::PRICE_OTHER){
            $cValueSmall = $value_small;
        }
        
        // Jan 02, 2016 xử lý xác định đang đề xuất tăng hay giảm, nhỏ hơn sẽ alert
        if(!empty($cValue) && $cValue < $prevPrice){
            $up_down = UsersPriceRequest::PRICE_DOWN;
        }
        if(!empty($cValueSmall) && $cValueSmall < $prevPriceSmall){
            $up_down_small = UsersPriceRequest::PRICE_DOWN;
        }
    }
    
    /**
     * @Author: DungNT Dec 31, 2016
     * @Todo: build string customer 
     */
    public function getStringCustomerError($aCustomer) {
        $aModelUser    = Users::getArrObjectUserByArrUid($aCustomer, '');
        $listdataUser  = CHtml::listData($aModelUser, 'id', 'first_name');
        return implode('<br>', $listdataUser);
    }
    
    /**
     * @Author: DungNT Dec 20, 2016
     * @Todo: format array detail to get update
     */
    public function buildArrayDetailUpdate() {
        $json = json_decode($this->json, true);
        if(!isset($json['customer_id'])){
            return ;
        }
        
        foreach($json['customer_id'] as $key => $customer_id){
            $mDetail = new UsersPriceRequestDetail();
            $mDetail->customer_id   = $customer_id;
            $mDetail->price_code    = isset($json['price_code'][$key]) ? $json['price_code'][$key] : 0;
            $mDetail->value         = isset($json['value'][$key]) ? $json['value'][$key] : 0;
            $mDetail->price_code_small      = isset($json['price_code_small'][$key]) ? $json['price_code_small'][$key] : 0;
            $mDetail->value_small           = isset($json['value_small'][$key]) ? $json['value_small'][$key] : 0;
            $mDetail->note          = isset($json['note'][$key]) ? $json['note'][$key] : 0;
            $mDetail->approved_1    = isset($json['approved_1'][$key]) ? $json['approved_1'][$key] : '';
            $mDetail->approved_2    = isset($json['approved_2'][$key]) ? $json['approved_2'][$key] : '';
            $mDetail->approved_3    = isset($json['approved_3'][$key]) ? $json['approved_3'][$key] : '';
            $mDetail->up_down       = isset($json['up_down'][$key]) ? $json['up_down'][$key] : 0;
            $mDetail->up_down_small = isset($json['up_down_small'][$key]) ? $json['up_down_small'][$key] : 0;
            
            $this->aDetail[]        = $mDetail;
            $this->aCustomerId[]    = $mDetail->customer_id;
        }
    }
    
    /**
     * @Author: DungNT Dec 20, 2016
     * @Todo: save create new for request multi G price bò mối
     */
    public function saveCreate() {
        if(!isset($_POST['customer_id'])){
            return ;
        }
//        $aCustomerExists    = $this->getCustomerIdRequestInMonth();// có thể sẽ không cần check cái này
        $aRowInsert = $aCustomerDuplicate = array();
        $this->buildArrayJsonInsert($aRowInsert, $aCustomerDuplicate);
        $aRowInsert['approved_1'] =  array();
        $aRowInsert['approved_2'] =  array();
        $aRowInsert['approved_3'] =  array();
        $this->json                 = MyFormat::jsonEncode($aRowInsert);
        $this->uid_login            = MyFormat::getCurrentUid();
        if($this->isNewRecord){
            $this->save();
        }else{
            $this->update();
        }
    }
    
    /**
     * @Author: DungNT Jan 02, 2017
     * @Todo: build some param of json, xử lý chung cho create, update, update status approved
     */
    public function buildArrayJsonInsert(&$aRowInsert, &$aCustomerDuplicate) {
        $aCustomerNew = array();
        $mGasPrice =new GasPrice();
        $gSetup = $mGasPrice->getArrayCodeGByMonthYear(date('m'), date('Y'));
        foreach($_POST['customer_id'] as $key => $customer_id){
            if(empty($customer_id)){
                continue ;
            }
//            if(in_array($customer_id, $aCustomerExists) || in_array($customer_id, $aCustomerNew)){// Aug 11, 2016 fix không cho tạo trùng trong tháng
            if(in_array($customer_id, $aCustomerNew)){// Aug 11, 2016 fix không cho tạo trùng trong tháng
                $aCustomerDuplicate[] = $customer_id;
                continue;
            }

            $price_code         = isset($_POST['price_code'][$key]) ? $_POST['price_code'][$key] : '';
            $value              = isset($_POST['value'][$key]) ? MyFormat::removeComma($_POST['value'][$key]) : 0;
            $price_code_small   = isset($_POST['price_code_small'][$key]) ? $_POST['price_code_small'][$key] : '';
            $value_small        = isset($_POST['value_small'][$key]) ? MyFormat::removeComma($_POST['value_small'][$key]) : 0;
            $up_down            = UsersPriceRequest::PRICE_UP;
            $up_down_small      = UsersPriceRequest::PRICE_UP;
            // Jan 02, 2016 xử lý xác định đang tăng hay giảm
            $this->calcUpDown($customer_id, $gSetup, $up_down, $up_down_small, $price_code, $value, $price_code_small, $value_small);
            
            $note               = isset($_POST['note'][$key]) ? $_POST['note'][$key] : '';
            $aCustomerNew[] = $customer_id;
//            'customer_id', 'price_code', 'value', 'note', 'approved_1', 'approved_2', 'approved_3');
            $aRowInsert['customer_id'][]    = $customer_id;
            $aRowInsert['price_code'][]     = $price_code;
            $aRowInsert['value'][]          = $value;
            $aRowInsert['price_code_small'][]   = $price_code_small;
            $aRowInsert['value_small'][]        = $value_small;
            $aRowInsert['up_down'][]        = $up_down;
            $aRowInsert['up_down_small'][]  = $up_down_small;
            $aRowInsert['note'][]           = $note;
        }
    }
    
    /**
    * @Author: DungNT Dec 20, 2016
    * @Todo: get array customer by month year, sử dụng để check không trùng khi tạo mới
    */
   public function getCustomerIdRequestInMonth() {
       $models = $this->getRequestByMonthYear();
       $aRes = array();
       return array();// chưa biết dc array kia sẽ gồm những gì, nên sẽ bổ sung sau
       foreach($models as $model){
           $json = json_decode($model->json, true);
           foreach($json as $aInfo){
               
           }
           $aRes[$model->id] = $model->customer_id;
       }
       return $aRes;
   }
   
       /**
    * @Author: DungNT Mar 19, 2017
    * @Todo: get array model customer by month year, sử dụng để check KH Trụ Sở Chính 
     phục vụ cho việc tăng giảm toàn hệ thống
    */
   public function getModelCustomerInMonth() {
       $models = $this->getRequestByMonthYear();
       $aCustomerId = array();
       foreach($models as $model){
           $json = json_decode($model->json, true);
            if(!isset($json['customer_id'])){
                continue;
            }
            foreach($json['customer_id'] as $key => $customer_id){
                $aCustomerId[$customer_id] = $customer_id;
            }
       }
       return Users::getArrayModelByArrayId($aCustomerId);
   }

   /**
     * @Author: DungNT Aug 07, 2016
     * @Todo: get all price bò mối by month year, type customer to copy
     */
    public function getRequestByMonthYear() {
        if(empty($this->c_month) || empty($this->c_year)){
            return array();
        }
        $this->c_month = $this->c_month*1;
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.c_month='. $this->c_month, ' AND t.c_year='.$this->c_year);
        $criteria->addCondition('t.type='.UsersPriceRequest::TYPE_REQUEST_PRICE);
        $criteria->addCondition('t.status<>'.UsersPriceRequest::STATUS_REJECT);
        return self::model()->findAll($criteria);
    }
    
    /**
     * @Author: DungNT Dec 22, 2016
     * @Todo: save update list tăng định kỳ của KH bò mối
     */
    public function saveUpdateSchedule() {
        if(!isset($_POST['customer_id'])){
            return ;
        }        
        $this->getListCustomerIdPost();
        $aCustomerNew = $aRowInsert = $aCustomerDuplicate = $aCustomerError = array();
        $monthRequest   = $this->c_year.'-'.  $this->c_month.'-01';
        $prevMonth      = MyFormat::modifyDays($monthRequest, 1, '-', 'month');
        $tmpMonth       = explode('-', $prevMonth);
        $aModelPricePrevMonth  = UsersPrice::getBoMoiOfCustomer($tmpMonth[1], $tmpMonth[0], $this->aCustomerId);
        
        foreach($_POST['customer_id'] as $key => $customer_id){
            if(in_array($customer_id, $aCustomerNew)){// Aug 11, 2016 fix không cho tạo trùng trong tháng
                $aCustomerDuplicate[] = $customer_id;
                continue;
            }
            $value      = isset($_POST['value'][$key]) ? MyFormat::removeComma($_POST['value'][$key]) : 0;
            $note       = isset($_POST['note'][$key]) ? $_POST['note'][$key] : '';
            if(empty($customer_id) || empty($value) ){
                continue;
            }
            
            if(!isset($aModelPricePrevMonth[$customer_id])){
                $aCustomerError[] = $customer_id;
            }
            
            $aCustomerNew[] = $customer_id;
//            'customer_id', 'price_code', 'value', 'note', 'approved_1', 'approved_2', 'approved_3');
            $aRowInsert['customer_id'][]    = $customer_id;
            $aRowInsert['value'][]          = $value;
            $aRowInsert['note'][]           = $note;
        }
        
        if(count($aCustomerError)){
            $msg = 'Có lỗi: Khách hàng chưa được thiết lập giá tháng hiện tại: '.date('m/Y').': <br>'. $this->getStringCustomerError($aCustomerError);
            $this->addError('uid_approved_1', $msg);
            return false;
        }
        
        $this->json      = MyFormat::jsonEncode($aRowInsert);
        $this->uid_login = MyFormat::getCurrentUid();
        $this->update();
        return true;
    }
    
    
    /**
     * @Author: DungNT Dec 22, 2016
     * @Todo: check user can view dropdown at approved status
     */
    public function canViewDropdown1() {
        $cUid   = MyFormat::getCurrentUid();
        $aStatusNew = array(self::STATUS_NEW, self::STATUS_APPROVED_1);
        return $cUid == $this->uid_approved_1 && in_array($this->status, $aStatusNew);
    }
    public function canViewDropdown2() {
        $cUid   = MyFormat::getCurrentUid();
        $aStatusApproved1 = array(self::STATUS_APPROVED_1, self::STATUS_APPROVED_2);
        return $cUid == $this->uid_approved_2 && in_array($this->status, $aStatusApproved1);
    }
    public function canViewDropdown3() {
        $cUid   = MyFormat::getCurrentUid();
        $aStatusApproved2 = array(self::STATUS_APPROVED_2, self::STATUS_APPROVED_3);
        return $cUid == $this->uid_approved_3 && in_array($this->status, $aStatusApproved2);
    }
    
    /**
     * @Author: DungNT Dec 22, 2016
     * @Todo: Check user can view form update status and approved
     */
    public function canViewApprovedForm() {
        $cUid   = MyFormat::getCurrentUid();
        $cRole  = MyFormat::getCurrentRoleId();
//        if($cRole == ROLE_ADMIN){// Jan 31, 2017 không hỗ trợ admin view approved, sẽ vào đúng user để test
//            return true;
//        }

        $aStatusNew = array(self::STATUS_NEW, self::STATUS_APPROVED_1);
        $aStatusApproved1 = array(self::STATUS_APPROVED_1, self::STATUS_APPROVED_2);
        $aStatusApproved2 = array(self::STATUS_APPROVED_2, self::STATUS_APPROVED_3);
        $ok = false;
        if($cUid == $this->uid_approved_1 && in_array($this->status, $aStatusNew)){
            $ok = true;
        }elseif($cUid == $this->uid_approved_2 && in_array($this->status, $aStatusApproved1)){
            $ok = true;
        }elseif($cUid == $this->uid_approved_3 && in_array($this->status, $aStatusApproved2)){
            $ok = true;
        }
        if($cUid == $this->uid_approved_3 && $this->status == self::STATUS_APPROVED_3){
//            return true; // only dev test
            // nếu đã duyệt lần 3 thì sang tháng ko cho view form duyệt nữa
            $nextMonth  = MyFormat::modifyDays($this->created_date, 1, '+', 'month');
            $tempMonth  = explode('-', $nextMonth);
            $firstDateNextMonth  = $tempMonth[0].'-'.$tempMonth[1].'-01';
            $ok         = MyFormat::compareTwoDate($firstDateNextMonth, date('Y-m-d'));
        }
//        if($cRole == ROLE_ADMIN){
//            return true;
//        }
        
        return $ok;
    }
    
    public function isUserApproved1()
    {
        $cUid       = MyFormat::getCurrentUid();
        $session    = Yii::app()->session;
        if(isset($session['SES_PRICE_REQUEST_LV1'][$cUid])){
            return true;
        }
        return false;
    }
    public function isUserApproved2()
    {
        $cUid       = MyFormat::getCurrentUid();
        $session    = Yii::app()->session;
        if(isset($session['SES_PRICE_REQUEST_LV2'][$cUid])){
            return true;
        }
        return false;
    }
    public function isUserApproved3()
    {
        $cUid       = MyFormat::getCurrentUid();
        $session    = Yii::app()->session;
        if(isset($session['SES_PRICE_REQUEST_LV3'][$cUid])){
            return true;
        }
        return false;
    }
    /**
     * @Author: DungNT Dec 22, 2016
     * get listoption người duyệt là quản lý
     */
    public function getListApprove1()
    {
        $aModelUser = GasOneManyBig::getArrayModelUser(GasOneManyBig::TYPE_PRICE_REQUEST_1, GasOneManyBig::TYPE_PRICE_REQUEST_1);
        $aRes = array();
        foreach ($aModelUser as $item) {
            $aRes[$item->id] = $item->getNameWithRole();
        }
        return $aRes;
    }
    public function getListApprove2()
    {
        $aModelUser = GasOneManyBig::getArrayModelUser(GasOneManyBig::TYPE_PRICE_REQUEST_2, GasOneManyBig::TYPE_PRICE_REQUEST_2);
        $aRes = array();
        foreach ($aModelUser as $item) {
            $aRes[$item->id] = $item->getNameWithRole();
        }
        return $aRes;
    }
    public function getListApprove3()
    {
        $aModelUser = GasOneManyBig::getArrayModelUser(GasOneManyBig::TYPE_PRICE_REQUEST_3, GasOneManyBig::TYPE_PRICE_REQUEST_3);
        $aRes = array();
        foreach ($aModelUser as $item) {
            $aRes[$item->id] = $item->getNameWithRole();
        }
        return $aRes;
    }
    
    /**
     * @Author: DungNT Dec 22, 2016
     * @Todo: get field approved
     */
    public function getFieldApproved() {
        $cUid   = MyFormat::getCurrentUid();
        $cRole  = MyFormat::getCurrentRoleId();
        $fieldApproved  = 'not_setup';
        $keepList = array();
        if($this->isUserApproved1() 
//                || $cRole == ROLE_ADMIN // only dev test
        ){
            $fieldApproved          = 'uid_approved_2';
            $this->listdataApproved = $this->getListApprove2();
            $this->fieldNote        = 'note_approved_1';
            $this->getJsonNoteField('note_approved_1');
            $keepList = array(
                self::STATUS_APPROVED_1,
                self::STATUS_REJECT,
            );
        }elseif($this->isUserApproved2()){
            $fieldApproved          = 'uid_approved_3';
            $this->listdataApproved = $this->getListApprove3();
            $this->fieldNote        = 'note_approved_2';
            $this->getJsonNoteField('note_approved_2');
            $keepList = array(
                self::STATUS_APPROVED_2,
                self::STATUS_REJECT,
            );
        }elseif($this->isUserApproved3()){
//            $fieldApproved = 'uid_approved_3';
//            $this->listdataApproved   = $this->getListApprove3();
            $fieldApproved          = 'uid_approved_3';
            $this->fieldNote        = 'note_approved_3';
            $this->getJsonNoteField('note_approved_3');
            $keepList = array(
                self::STATUS_APPROVED_3,
            );
        }
        $this->listdataStatus   = array_intersect_key( $this->getArrayStatus(), array_flip( $keepList ) );
        return $fieldApproved;
    }
    
    /**
     * @Author: DungNT Dec 19, 2016
     * @Todo: get scenario by role update approved
     */
    public function getScenarioApproved() {
        $scenario = '';
        if($this->isUserApproved1()){
            $scenario = 'UserApproved1';
        }elseif($this->isUserApproved2()){
            $scenario = 'UserApproved2';
        }elseif($this->isUserApproved3()){
            $scenario = 'UserApproved3';
        }
        $this->scenario = $scenario;
    }
    
    /**
     * @Author: DungNT Dec 22, 2016
     * @Todo: check show select chọn người duyệt cấp trên
     */
    public function showDropdownApproved() {
        $ok = true;
        if($this->scenario == 'UserApproved3'){
            $ok = false;
        }
        return $ok;
    }
    
    /**
     * @Author: DungNT Dec 22, 2016
     * @Todo: khởi tạo session user approve để check khi view
     */
    public function initSessionUserApproved()
    {
        $session = Yii::app()->session;
        if(!isset($session['SES_PRICE_REQUEST_FULLNAME'])){
            $session['SES_PRICE_REQUEST_LV1'] = GasOneManyBig::getArrOfManyId(GasOneManyBig::TYPE_PRICE_REQUEST_1, GasOneManyBig::TYPE_PRICE_REQUEST_1);
            $session['SES_PRICE_REQUEST_LV2'] = GasOneManyBig::getArrOfManyId(GasOneManyBig::TYPE_PRICE_REQUEST_2, GasOneManyBig::TYPE_PRICE_REQUEST_2);
            $session['SES_PRICE_REQUEST_LV3'] = GasOneManyBig::getArrOfManyId(GasOneManyBig::TYPE_PRICE_REQUEST_3, GasOneManyBig::TYPE_PRICE_REQUEST_3);
            $aUid = array_merge($session['SES_PRICE_REQUEST_LV1'], $session['SES_PRICE_REQUEST_LV2'], $session['SES_PRICE_REQUEST_LV3']);
            $session['SES_PRICE_REQUEST_FULLNAME'] = Users::getListOptions($aUid, array('get_all'=>1));
        }
    }
    
    /**
     * @Author: DungNT Dec 22, 2016
     * @Todo: lấy những đề xuất giá được duyệt để đưa vào giá tháng mới
     * gồm cả đề xuất duyệt và tăng tự động : by type
     */
    public static function getPriceApproved($month, $year, $type)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.c_month='.$month.' AND t.c_year='.$year.' AND t.type='.$type);
        if($type == UsersPriceRequest::TYPE_REQUEST_PRICE){
            $criteria->addCondition('t.status='.UsersPriceRequest::STATUS_APPROVED_3);
            $criteria->addCondition('t.status<>'.UsersPriceRequest::STATUS_REJECT);
            return self::model()->findAll($criteria);
        }elseif($type == UsersPriceRequest::TYPE_PRICE_ASCENDING){
            return self::model()->find($criteria);
        }
        return array();
    }
    
    /**
     * @Author: DungNT Oct 31, 2017
     * @Todo: lấy những đề xuất giá theo tháng, năm
     */
    public function getByMonthYear()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.c_month='.$this->c_month.' AND t.c_year='.$this->c_year.' AND t.type='.$this->type);
        if($this->type == UsersPriceRequest::TYPE_REQUEST_PRICE){
            $criteria->addCondition('t.status<>'.UsersPriceRequest::STATUS_REJECT);
            return self::model()->findAll($criteria);
        }elseif($this->type == UsersPriceRequest::TYPE_PRICE_ASCENDING){
            return self::model()->find($criteria);
        }
        return [];
    }
    
    /**
     * @Author: DungNT Dec 22, 2016
     * @Todo: lấy những đề xuất giá được duyệt để đưa vào giá tháng mới
     * @return: array(customer_id => price_code, value)
     * @param: $month, $year
     */
    public static function getArrayPriceRequest($month, $year) {
        $models = self::getPriceApproved($month, $year, UsersPriceRequest::TYPE_REQUEST_PRICE);
        $aRes   = [];
        $mPriceRequest          = new UsersPriceRequest();// Mar 19, 2017
        $mPriceRequest->c_month = $month;
        $mPriceRequest->c_year  = $year;
        $aModelCustomer         = $mPriceRequest->getModelCustomerInMonth();
        
        foreach($models as $key=>$model){
            $json = json_decode($model->json, true);
            if(!isset($json['customer_id'])){
                continue;
            }
            foreach($json['customer_id'] as $key => $customer_id){
                $approved_3    = isset($json['approved_3'][$key]) ? $json['approved_3'][$key] : '';
                if($approved_3 != UsersPriceRequest::ITEM_APPROVED){
                    continue;
                }
                if(isset($aRes[$customer_id])){
                    $info = $key. 'BIG ERROR Tạo trùng KH của UsersPriceRequest FUNCTION getArrayPriceRequest customer_id: '.$customer_id.' -- PK is: '.$model->id;
                    Logger::WriteLog($info);
                    continue;
                }
                
                $price_code         = isset($json['price_code'][$key]) ? $json['price_code'][$key] : 0;
                $value              = isset($json['value'][$key]) ? $json['value'][$key] : 0;
                $price_code_small   = isset($json['price_code_small'][$key]) ? $json['price_code_small'][$key] : 0;
                $value_small        = isset($json['value_small'][$key]) ? $json['value_small'][$key] : 0;
                
                $aRes[$customer_id]['price_code']       = $price_code;
                $aRes[$customer_id]['value']            = $value;
                $aRes[$customer_id]['price_code_small'] = $price_code_small;
                $aRes[$customer_id]['value_small']      = $value_small;
                
                
                // Mar 19, 2017 xử lý cho KH Trụ Sở chính, sẽ tăng cho toàn bộ các chi nhánh của KH này
                if(isset($aModelCustomer[$customer_id]) && $aModelCustomer[$customer_id]->is_maintain == UsersExtend::STORE_CARD_HEAD_QUATER){
                    /* 1. get all model customer trong chuỗi của KH 
                     * 2. sau đó gắn giá của Trụ Sở vào
                     */
                    $aSubCustomer = UsersExtend::getByParentId($customer_id);
                    foreach($aSubCustomer as $mCustomer){
                        $aRes[$mCustomer->id]['price_code']       = $price_code;
                        $aRes[$mCustomer->id]['value']            = $value;
                        $aRes[$mCustomer->id]['price_code_small'] = $price_code_small;
                        $aRes[$mCustomer->id]['value_small']      = $value_small;
                    }
                }
                
                // Mar 19, 2017 xử lý cho KH Trụ Sở chính, sẽ tăng cho toàn bộ các chi nhánh của KH này
            }
        }
        return $aRes;
    }
    
    /**
     * @Author: DungNT Dec 22, 2016
     * @Todo: lấy những KH lên lịch tăng tự động 
     * @return: array(customer_id => value)
     * @param: $month, $year, &$mPriceSchedule
     */
    public static function getArrayPriceSchedule($month, $year, &$mPriceSchedule) {
        $model = self::getPriceApproved($month, $year, UsersPriceRequest::TYPE_PRICE_ASCENDING);
        if(is_null($model)){
            return array();
        }
        $mPriceSchedule = $model;
        $aRes   = array();
        $json = json_decode($model->json, true);
        if(!isset($json['customer_id'])){
            return [];
        }
        foreach($json['customer_id'] as $key => $customer_id){
            if(isset($aRes[$customer_id])){
                $info = 'BIG ERROR Tạo trùng KH của UsersPriceRequest FUNCTION getArrayPriceSchedule customer_id: '.$customer_id;
                Logger::WriteLog($info);
                continue;
            }
            $aRes[$customer_id]['value'] = isset($json['value'][$key]) ? $json['value'][$key] : 0;
        }
        // copy sang model đề xuất của tháng sau sau khi gen xong 
        return $aRes;
    }
    
    /**
     * @Author: DungNT Dec 22, 2016
     * @Todo: copy sang model đề xuất của tháng sau sau khi tự động tạo giá KH
     * Thống nhất hàm này sẽ chạy vào đầu của mối tháng, 
     * nghĩa là copy giá KH sẽ chạy vào 10h ngày 1 hàng tháng, 
     * chỗ tăng tự động này không bị ảnh hưởng vì nó cộng Month Year của record cũ chứ không phải year hệ thống
     */
    public function copyToNewPriceSchedule() {
        $cDate = $this->c_year.'-'.$this->c_month.'-01';
        
        $nextMonth  = MyFormat::modifyDays($cDate, 1, '+', 'month');
        $tempMonth  = explode('-', $nextMonth);
        
        $mNewPriceSchedule  = new UsersPriceRequest();
        $aFieldNotCopy      = array('id', 'c_month', 'c_year', 'created_date');
        // handle add promotion of user
        MyFormat::copyFromToTable($this, $mNewPriceSchedule, $aFieldNotCopy);
        $mNewPriceSchedule->c_month = $tempMonth[1];
        $mNewPriceSchedule->c_year  = $tempMonth[0];
        $mNewPriceSchedule->save();
    }
 
    /**
     * @Author: DungNT Oct 31, 2017
     * @Todo: auto approve 3, tự động duyệt lần 3
     */
    public static function autoApprove3() {
        if(date('t') != date('d')){// chỉ chạy ngày cuối tháng
            return ;
        }
        $cDate = date('Y-m').'-01';
        $nextMonth  = MyFormat::modifyDays($cDate, 1, '+', 'month');
        $tempMonth  = explode('-', $nextMonth);
        $mPriceSchedule  = new UsersPriceRequest();
        $mPriceSchedule->c_month = $tempMonth[1];
        $mPriceSchedule->c_year  = $tempMonth[0];
        $mPriceSchedule->type    = UsersPriceRequest::TYPE_REQUEST_PRICE;
        $aData = $mPriceSchedule->getByMonthYear();
        foreach($aData as $model){
            $json = json_decode($model->json, true);
            if(!isset($json['customer_id']) || $model->status == self::STATUS_NEW){
                continue;
            }
            foreach($json['customer_id'] as $key => $customer_id){
                $autoApprove    = '';
                $approved_3     = isset($json['approved_3'][$key]) ? $json['approved_3'][$key] : '';
                if($approved_3 != ''){
                    continue;
                }
                $approved_2    = isset($json['approved_2'][$key]) ? $json['approved_2'][$key] : '';
                if($approved_2 != ''){// nếu có duyệt lần 2 thì lấy làn 2
                    $autoApprove    = $approved_2;
                }elseif($approved_2 == ''){// nếu không có duyệt lần 2 thì lấy làn 1
                    $approved_1    = isset($json['approved_1'][$key]) ? $json['approved_1'][$key] : '';
                    $autoApprove    = $approved_1;
                }
                $json['approved_3'][$key]         = $autoApprove;
            }
            $model->json            = MyFormat::jsonEncode($json);
            $model->uid_approved_3  = GasLeave::UID_DIRECTOR;
            $model->status          = self::STATUS_APPROVED_3;
            $model->updateJsonNoteField(3, '');
            $model->update();
        }
        
        $needMore               = [];
        $needMore['title']      = 'Cron Duyệt Đề xuất giá AutoApprove3 : '.date('d/m/Y');
        $needMore['list_mail']  = ['dungnt@spj.vn'];
        SendEmail::bugToDev($needMore['title'], $needMore);
        Logger::WriteLog($needMore['title']);
    }
    
    /** @Author: KHANH TOAN 2018
     *  @Todo: lấy sản lượng của từng khách hàng
     *  @Param:
     **/
    public function getOutputCustomer($date_from, $date_to, $aCustomerId ) {
        $sta2 = new Sta2();
        $aData = $aRes= array();
        $mGasStoreCard = new GasStoreCard();
        if(Yii::app()->user->role_id==ROLE_SUB_USER_AGENT){
                $mGasStoreCard->agent_id = array(MyFormat::getAgentId());
            }elseif(Yii::app()->user->role_id==ROLE_SALE){
                $mGasStoreCard->ext_sale_id = Yii::app()->user->id;
                $TypeSale = Yii::app()->user->gender;
                if(in_array($TypeSale, Users::$aIdTypeSaleMoi)){
                    $mGasStoreCard->ext_is_maintain = Users::SALE_MOI;
                }
            }
        $needMore['aCustomerId'] = $aCustomerId;    
        $sta2->getQuantityCustomer($mGasStoreCard, $aData, $date_from, $date_to, $needMore);
        $this->getData($aRes, $aData, $mGasStoreCard);
        return $aRes;
    }
    
    public function getData(&$aRes, $aData, $mGasStoreCard){
        $arr_customer_id    = isset($aData['customer_id'])?$aData['customer_id']:array();
        $OUTPUT             = isset($aData['OUTPUT'])?$aData['OUTPUT']:array();
        $OUTPUT_MOI_12      = isset($aData['OUTPUT_MOI_12'])?$aData['OUTPUT_MOI_12']:array();
        $GAS_REMAIN         = isset($aData['GAS_REMAIN'])?$aData['GAS_REMAIN']:array();
        $BO_REMAIN = array();
        foreach($arr_customer_id as $customer_id=>$arr_agent){
            foreach($arr_agent as $agent_id){
                    $REMAIN = isset($GAS_REMAIN[$customer_id][$agent_id])?$GAS_REMAIN[$customer_id][$agent_id]:0;
                    if(isset($BO_REMAIN[$customer_id]))
                        $BO_REMAIN[$customer_id] += $REMAIN;
                    else{
                        $BO_REMAIN[$customer_id] = $REMAIN;
                    }
            }
            $BO_OUTPUT = isset($OUTPUT[$customer_id])?$OUTPUT[$customer_id]:0;
            $BO_REAL = $BO_OUTPUT - $BO_REMAIN[$customer_id];
            $BO_REAL = ($BO_REAL !=0 ? ActiveRecord::formatCurrencyRound($BO_REAL) : '');
            if($mGasStoreCard->ext_is_maintain==STORE_CARD_KH_MOI){
                $MOI_12 = isset($OUTPUT_MOI_12[$customer_id])?$OUTPUT_MOI_12[$customer_id]:'';
                if($MOI_12!=''){
                    $MOI_12 = ActiveRecord::formatCurrencyRound($MOI_12);
                }
                if(empty($BO_REAL)){
                    $BO_REAL = $MOI_12;
                }
            }
            $aRes[$customer_id] = $BO_REAL;
        }
    }
    
    /** @Author: KHANH TOAN 2019
     *  @Todo: get aCustomer
     *  @Param: $data CActiveDataProvider
     **/
    public function getArrCustomerId($data) {
        $aCustomerId =  array();
        $dataAll = $data->data;
        foreach($dataAll as $data){
            $json = json_decode($data->json, true);
            if(!empty($json->customer_id)){
                $aCustomerId = array_merge($aCustomerId,$json->customer_id);
            }
        }
        return $aCustomerId;
    }
    
    /** @Author: KHANH TOAN 2019
     *  @Todo: get sản lượng tháng hiện tại của KH để người duyệt xem 
     **/
    public function getQuantity($data) {
        $aCustomerId    = $aDataOutputCustomer = [];
        $aCustomerId    = $this->getArrCustomerId($data);
        $date_from      = $this->c_year.'-'.  $this->c_month.'-01';;
        $date_from      = MyFormat::modifyDays($date_from, 1, '-', 'month');
        $date_to        = MyFormat::dateConverYmdToDmy($date_from, 'Y-m-t');
        $aDataOutputCustomer = $this->getOutputCustomer($date_from, $date_to, $aCustomerId);
        return $aDataOutputCustomer;
    }

   
}

class UsersPriceRequestDetail {
    public $customer_id, $price_code, $value, $note, $approved_1, $approved_2, $approved_3;
    public $price_code_small, $value_small, $up_down, $up_down_small;
}