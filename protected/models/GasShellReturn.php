<?php

/**
 * This is the model class for table "{{_shell_return}}".
 *
 * The followings are the available columns in table '{{_shell_return}}':
 * @property string $id
 * @property string $customer_id
 * @property integer $type_customer
 * @property string $sale_id
 * @property int $freq
 * @property integer $status
 * @property string $week Ngày thứ 2 của tuần đó
 * @property integer $old_remain Số lượng vỏ tồn trong đợt trước
 * @property integer $new_remain Số lượng vỏ tồn trong đợt này
 * @property integer $forecast Số lượng dự kiến thu trong tuần này
 * @property integer $received Số lượng đã thu được vào cuối tuần
 * @property string $note Ghi chú
 * @property integer $created_date
 * @property Users $rCustomer
 * @property Users $rSale
 */
class GasShellReturn extends CActiveRecord
{
    // Trung June 21 2016
    // Status for GasShellReturn
    const STA_NEW = 1;
    const STA_PROCESSING = 2;
    const STA_COMPLETE = 3;
    public static $STATUS_TEXT = array(
        GasReportTrouble::STA_NEW => 'Lập lịch',
        GasReportTrouble::STA_PROCESSING => 'Đang thu',
        GasReportTrouble::STA_COMPLETE => 'Đã thu'
    );

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasShellReturn the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @Author: TRUNG Jul 14 2016
     * Lấy số lượng tồn trong tuần trước
     * @param $customer_id
     * @param $currentWeek DateTime
     */
    private function getRemainLastWeek($customer_id, $currentWeek)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('week', self::getLastWeekString($currentWeek));
        $criteria->compare('customer_id', $customer_id);
        /** @var GasShellReturn $item */
        $item = GasShellReturn::model()->find($criteria);
        if ($item) {
            return $item->old_remain + $item->new_remain - $item->received;
        } else {
            return 0;
        }
    }

    /**
     * @Author: TRUNG Jul 18 2016
     * Lấy lịch còn tồn động trong tuần trước
     *
     * @param $firstWeekDay
     * @param $listModel
     * @return array
     */
    public static function getPlanningLastWeekData($firstWeekDay, $listModel)
    {
        $exceptCustomerID = [];
        if ($listModel) {
            foreach ($listModel as $value) {
                $exceptCustomerID[] = $value ['customer_id'];
            }
        }

        $result = [];
        $criteria = new CDbCriteria;
        $criteria->compare('week', GasShellReturn::getLastWeekString($firstWeekDay));// Check item in week
        $criteria->addNotInCondition('customer_id', $exceptCustomerID);
        $listLastModel = GasShellReturn::model()->findAll($criteria);
        if ($listLastModel) {
            foreach ($listLastModel as $gasRemainModel) {
                $model = new GasShellReturn();
                /** @var GasShellReturn $gasRemainModel */
                $model->customer_id = $gasRemainModel->customer_id;
                $model->type_customer = $gasRemainModel->type_customer;
                $model->sale_id = $gasRemainModel->sale_id;
                $model->status = GasShellReturn::STA_NEW;
                $model->forecast = 0;
                $model->week = $firstWeekDay;
                $model->freq = $gasRemainModel->freq;
                $model->old_remain = $gasRemainModel->old_remain + $gasRemainModel->new_remain - $gasRemainModel->received;

                $result[] = $model;
            }
        }
        return $result;
    }

    /**
     * @Author: TRUNG Jul 18 2016
     * Lấy danh sách lịch trong tuần hiện tại đã được lưu
     *
     * @param $firstWeekDay
     * @return array
     */
    public static function getPlanningThisWeekData($firstWeekDay)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('week', $firstWeekDay);// Check item in week
        $listModel = GasShellReturn::model()->findAll($criteria);
        return $listModel;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_shell_return}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('customer_id, status, week, total', 'required', 'on' => 'planning'),
            array('type_customer, status, old_remain, new_remain, forecast, received', 'numerical', 'integerOnly' => true
            , 'on' => 'planning'),
            array('', 'safe', 'on' => 'update_none'),
            array('received, note', 'safe', 'on' => 'update_dieu_phoi, update_saler'),
            array('id, customer_id, type_customer, sale_id, status, week, old_remain, new_remain, forecast, received, note, created_date, freq'
            , 'safe', 'on' => 'planning'),
            // array('id, customer_id, type_customer, sale_id, status, week, old_remain, new_remain, forecast, received, note, created_date, freq', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rSale' => array(self::BELONGS_TO, 'Users', 'sale_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'customer_id' => 'Khách hàng',
            'type_customer' => 'Loại khách hàng',
            'sale_id' => 'Nhân viên bán hàng',
            'freq' => 'Tần suất',
            'status' => 'Trạng thái',
            'week' => 'Tuần',
            'old_remain' => 'Tồn đợt trước',
            'new_remain' => 'Tồn đợt này',
            'forecast' => 'Dự kiến thu',
            'received' => 'Đã thu',
            'note' => 'Ghi chú',
            'created_date' => 'Ngày tạo'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id, true);
        $criteria->compare('t.customer_id', $this->customer_id, true);
        $criteria->compare('t.type_customer', $this->type_customer);
        $criteria->compare('t.sale_id', $this->sale_id, true);
        $criteria->compare('t.freq', $this->freq);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('t.week', $this->week, true);
        $criteria->compare('t.old_remain', $this->old_remain);
        $criteria->compare('t.new_remain', $this->new_remain);
        $criteria->compare('t.forecast', $this->forecast);
        $criteria->compare('t.received', $this->received);
        $criteria->compare('t.note', $this->note, true);
        $criteria->compare('t.created_date', $this->created_date, true);

        $sort = new CSort();
        $sort->attributes = array();
        $sort->defaultOrder = ' t.week desc, t.id desc';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    public function searchPlanning()
    {
        $criteria = new CDbCriteria;

        $criteria->limit = 3;
        $criteria->compare('t.id', $this->id, true);
        $criteria->compare('t.customer_id', $this->customer_id, true);
        $criteria->compare('t.type_customer', $this->type_customer);
        $criteria->compare('t.sale_id', $this->sale_id, true);
        $criteria->compare('t.freq', $this->freq);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('t.week', $this->week, true);
        $criteria->compare('t.old_remain', $this->old_remain);
        $criteria->compare('t.new_remain', $this->new_remain);
        $criteria->compare('t.forecast', $this->forecast);
        $criteria->compare('t.received', $this->received);
        $criteria->compare('t.note', $this->note, true);
        $criteria->compare('t.created_date', $this->created_date, true);

        $sort = new CSort();
        $sort->attributes = array();
        $sort->defaultOrder = 't.id desc';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    public function defaultScope()
    {
        return array(//'condition'=>'',
        );
    }

    /**
     * @Author: TRUNG June 21 2016
     * Get user name of customer
     * @return string
     */
    public function getCustomerName()
    {
        if (!empty($this->rCustomer)) {
            return $this->rCustomer->first_name;
        }
        return '';
    }

    /**
     * @Author: TRUNG June 21 2016
     * Get type of customer
     * @return string
     */
    public function getTypeCustomerText()
    {
        if (!empty($this->type_customer) && isset(CmsFormatter::$CUSTOMER_BO_MOI[$this->type_customer])) {
            return CmsFormatter::$CUSTOMER_BO_MOI[$this->type_customer];
        }
        return "";
    }

    /**
     * @Author: TRUNG June 21 2016
     * Get user name of sale
     * @return string
     */
    public function getSaleName()
    {
        if (!empty($this->rSale)) {
            return $this->rSale->first_name;
        } else {
            return '';
        }
    }

    /**
     * @Author: TRUNG June 21 2016
     * Get status text of model in GasShellReturn::$STATUS_TEXT
     * @return string
     */
    public function getStatusText()
    {
        if (isset(GasShellReturn::$STATUS_TEXT[$this->status])) {
            return GasShellReturn::$STATUS_TEXT[$this->status];
        } else {
            return '';
        }
    }

    /**
     * @Author: TRUNG June 21 2016
     * Get html detail for record
     * @return string
     */
    public function getDetailHtml()
    {
        $html = nl2br($this->note);
        return $html;
    }

    /**
     * @Author: TRUNG June 21 2016
     * Get html forecast for record
     * @return string
     */
    public function getForecastHtml()
    {
        $html = '<input type="hidden" name="GasShellReturn[' . $this->id . '][id]" id="GasShellReturn_id"/ value="' . $this->id . '">';
        $html .= '<input type="number" name="GasShellReturn[' . $this->id . '][forecast]" id="GasShellReturn_forecast" min="0" max="' . $this->total . '" value="' . $this->forecast . '"/>';
        return $html;
    }

    /**
     * @Author: TRUNG Jul 06 2016
     * Save list data by single sql query
     * @param $records array danh sách record cập nhật
     * @param $deleteIds array danh sách id cần delete
     * @param $currentWeek string tuần hiện tại
     * @return bool
     */
    public static function saveList($records, $deleteIds, $currentWeek)
    {
        // Kiểm tra quyền user. Nếu không phải điều phối hoặc admin thì không được chỉnh
        if (!MyRoleChecking::isRoleDieuPhoi() && !MyRoleChecking::isRoleAdmin()) {
            return false;
        }

        $result = true;
        $savedList = [];
        $attUpdate = []; // Danh sách các field sẽ được cập nhật

        // Xóa các record được yêu cầu xóa
        $criteria = new CDbCriteria;
        $criteria->addInCondition('id', $deleteIds);
        GasShellReturn::model()->deleteAll($criteria);

        foreach ($records as $i => $record) {
            if (isset($record)) {
                // Nếu có id thì đây là một record đã có sẵn, mình cập nhật thay vì tạo mới.
                $criteria = new CDbCriteria;
                $criteria->compare('week', $currentWeek);
                $criteria->compare('customer_id', $record['customer_id']);
                /** @var GasShellReturn $item */
                $model = GasShellReturn::model()->find($criteria);
                if (!isset($model)) {
                    // Tạo record mới
                    $model = new GasShellReturn('planning');
                    $model->unsetAttributes();  // clear any default values
                }
                $model->scenario = 'planning';
                $model->attributes = $record;
                $model->status = GasShellReturn::STA_NEW;
                $model->week = $currentWeek;

                // Lấy thông tin khách hàng từ ID
                if (isset($model->rCustomer)) {
                    $model->sale_id = $model->rCustomer->sale_id;
                    $model->type_customer = $model->rCustomer->is_maintain;

                    $attUpdate[] = 'customer_id';
                    $attUpdate[] = 'type_customer';
                    $attUpdate[] = 'sale_id';

                    // Lấy số tồn cũ tuần trước
                    $model->old_remain = self::getRemainLastWeek($model->customer_id,
                        $currentWeek);
                    $attUpdate[] = 'old_remain';
                }

                // Cập nhật những field sẽ được lưu
                $attUpdate[] = 'freq';
                $attUpdate[] = 'status';
                $attUpdate[] = 'week';
                $attUpdate[] = 'new_remain';
                $attUpdate[] = 'forecast';
                $attUpdate[] = 'note';
                $attUpdate[] = 'created_date';

                $model->validate();
                if ($model->isValidForecast() && // June 23 Trung forecast and total should be greater than 0
                    $model->validate() && $model->save(true, $attUpdate)
                ) {
                    $savedList[] = $model;
                } else {
                    $result = false;
                }
            }
        }

        // Check sending email to sale person
        if (count($savedList) > 0) {
            GasScheduleEmail::makeScheduleSendShellReturn($savedList);
        }
        return $result;
    }

    /**
     * @Author: TRUNG June 21 2016
     *
     * @return string
     */
    public function getWeekText()
    {
        return $this->week;
    }

    /**
     * @Author: TRUNG June 21 2016
     *
     * @return int
     */
    public function getTotalText()
    {
        return $this->total;
    }

    /**
     * @Author: TRUNG June 21 2016
     *
     * @return int
     */
    public function getForecastText()
    {
        return $this->forecast;
    }

    /**
     * @Author: TRUNG June 21 2016
     * Check this item for this saler or not
     * @return bool
     */
    public function isCustomerSaler()
    {
        $cUid = Yii::app()->user->id;
        return $cUid == $this->sale_id;
    }

    /**
     * @Author: TRUNG June 21 2016
     *
     * @return int
     */
    public function getReceivedText()
    {
        return $this->received;
    }

    protected function beforeSave()
    {
        if ($this->rCustomer) {
            $this->sale_id = $this->rCustomer->sale_id;
            $this->type_customer = $this->rCustomer->is_maintain;
        }

        if (empty($this->forecast)) {
            $this->forecast = 0;
        }
        if (empty($this->received)) {
            $this->received = 0;
        }

        return parent::beforeSave();
    }

    /**
     * @Author: TRUNG June 21 2016
     *
     * @return string
     */
    public function getFrequentText()
    {
        return $this->freq . ' ngày';
    }

    /**
     * @Author: TRUNG June 23 2016
     * Get total from old remain and new remain
     * @return int
     */
    public function getTotal()
    {
        return $this->old_remain + $this->new_remain;
    }

    /**
     * @Author: TRUNG June 23 2016
     * Check forecast data is valid or not.
     * It must be greater than 0 and lower total
     * @return bool true if valid
     */
    private function isValidForecast()
    {
        return $this->forecast >= 0 && $this->forecast <= $this->getTotal();
    }

    /**
     * @Author: TRUNG Jul 15 2016
     * Lấy text của một tuần trước theo format Y-m-d. Ví dụ 2016-06-30
     * @param $currentWeek string tuần hiện tại theo format Y-m-d. Ví dụ 2016-06-30
     * @return string
     */
    public static function getLastWeekString($currentWeek)
    {
        return date_create_from_format('Y-m-d', $currentWeek)->modify('-7 days')->format('Y-m-d');
    }

    public function saveWithRole($attributes)
    {
        if (MyRoleChecking::isRoleDieuPhoi() || MyRoleChecking::isRoleAdmin()) {
            $this->scenario = 'update_dieu_phoi';
        } elseif ($this->isCustomerSaler()) {
            $this->scenario = 'update_saler';
        } else {
            $this->scenario = 'update_none';
        }
        $this->attributes = $attributes;
        $this->validate();
        if (!$this->hasErrors() && $this->save()) {
            return true;
        } else {
            return false;
        }
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            if ($this->getTotal() < $this->received) {
                $this->addError('received', 'Số lượng nhận không thể lớn hơn tổng số!');
                return false;
            }
            if ($this->received < 0) {
                $this->addError('received', 'Số lượng không thể nhỏ hơn 0!');
                return false;
            }
            if ($this->getTotal() < $this->forecast) {
                $this->addError('forecast', 'Số lượng dự kiến không thể lớn hơn tổng số!');
                return false;
            }

            if ($this->forecast < 0) {
                $this->addError('forecast', 'Số lượng không thể nhỏ hơn 0!');
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * @Author: TRUNG June 21 2016
     * Kiểm tra item có quyền được cập nhật hay không?
     * @return bool
     */
    public function isUpdatable()
    {
        if ($this->week == self::getFirstDateOfWeek()) {
            return MyRoleChecking::isRoleAdmin() || MyRoleChecking::isRoleDieuPhoi() || $this->isCustomerSaler();
        } else {
            return false;
        }
    }

    /**
     * @Author: TRUNG Jul 19 2016
     * Lấy ngày đầu tiên trong tuần này
     *
     * @return string
     */
    public static function getFirstDateOfWeek()
    {
        return date('Y-m-d', strtotime((1 - date('w')) . " days"));
    }

    /**
     * @Author: TRUNG Jul 19 2016
     * Thêm dòng hightlight có thẻ tr nếu item chưa đủ số lượng cần thiết
     * @return string
     */
    public function getSpanCodeForHighLight()
    {
        $diff = date_diff(date_create($this->week), date_create(date('Y-m-d')));
        $diffDay = $diff->format("%R%a days") * 1;
        $diffDay = $diffDay > 5 ? 5 : $diffDay;

        if ($this->received < $this->forecast * $diffDay / 5) {
            return "<span class='high_light_tr'></span>";
        }
        return '';
    }
}