<?php

/**
 * This is the model class for table "{{_file_send}}".
 *
 * The followings are the available columns in table '{{_file_send}}':
 * @property integer $id
 * @property integer $user_id
 * @property integer $type
 * @property string $name_file
 * @property string $created_date
 * @property string $created_date_bigint
 */
class FileSend extends BaseSpj
{
    const TYPE_DOI_CHIEU_CONG_NO   = 1;
    
    public  $date_from, $date_to, $autocomplete_name1;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return FileSend the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_file_send}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            array('user_id, type, name_file, created_date, created_date_bigint', 'required'),
            array('user_id, type', 'numerical', 'integerOnly'=>true),
            array('created_date_bigint', 'length', 'max'=>20),
            array('id, user_id, type, name_file, created_date, created_date_bigint, date_from, date_to', 'safe'),
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'rUser' => array(self::BELONGS_TO, 'Users', 'user_id'),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Khách hàng',
            'type' => 'Loại',
            'list_email' => 'Danh sách email',
            'name_file' => 'Tên file',
            'created_date' => 'Ngày tạo',
            'created_date_bigint' => 'Created Date Bigint',
            'date_from' => 'Từ ngày',
            'date_to' => 'Đến ngày',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
	$criteria->compare('t.user_id',$this->user_id);
	$criteria->compare('t.type',$this->type);
        $date_from = $date_to = '';
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
            DateHelper::searchGreater($date_from, 'created_date_bigint', $criteria);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
            DateHelper::searchSmaller($date_to, 'created_date_bigint', $criteria, true);
        }
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    public function gerUserInfo() {
        $mUser = $this->rUser;
        return isset($mUser->first_name) ? $mUser->first_name : '';
    }
    
    public function getArrayType() {
        return[
            FileSend::TYPE_DOI_CHIEU_CONG_NO => 'Đối chiếu công nợ'
        ];
    }
    
    public function getType() {
        $aType = $this->getArrayType();
        return isset($aType[$this->type]) ? $aType[$this->type] : "";
    }
    
    public function getNameFile() {
        $url= isset($this->name_file) ? Yii::app()->createAbsoluteUrl("$this->name_file") : '';
        $html = '<a href='.$url.' target="_blank">File </a>'; //
        return $html;  
    }
    
     /** @Author: NGUYEN KHANH TOAN 27 Aug 2018
     *  @Todo: save bảng kê email đối chiếu công nợ
     *  @Param: $aData
     **/
    public function createSend($aData, $mUsersEmail) {
        $model = new FileSend();
        $model->user_id = $aData['mCustomer']->id;
        $model->type = FileSend::TYPE_DOI_CHIEU_CONG_NO;
        $model->list_email = $mUsersEmail->list_email;
        $model->name_file = $aData['mCustomer']['pdfPathAndName'];
        $model->created_date = date('Y-m-d H:i:s');    
        $model->created_date_bigint = strtotime($model->created_date);
        $model->validate();
        $model->save();
    }      
    
    /** @Author: NGUYEN KHANH TOAN 2018
     *  @Todo: get url của bảng kê
     *  @Param:
     **/
        public function getUrl(){
        $url =  Yii::app()->createAbsoluteUrl("$this->name_file");
        return $url;
    }
    
    /** @Author: NGUYEN KHANH TOAN Sep 2 2018
     *  @Todo: Table danh sách email
     *  @Param:
     **/
        public function getTableListEmail(){
        $aResult = explode(';',$this->list_email);
        $str = "<table class='f_size_15' cellpadding='0' cellspacing = '0' style = '' >";
//        $str .= "<tr style = 'color:red' style = 'border : 1px solid black'>";
//        $str .= "<td style = 'border : 1px solid black'>STT</td>";
//        $str .= "<td style = 'border : 1px solid black'>Danh sách Email</td></tr>";
        foreach ($aResult as $key => $value) {
            if($value){
                $str .= "<tr style = ''>";
                    $str .= "<td style = ''>".($key+1)."</td>";
                    $str .= "<td style = ''>".$value."</td>";
                $str .= "</tr>";
            }
        }
        $str .= "</table>";
        return $str;
    }
    
}