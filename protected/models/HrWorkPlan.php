<?php

/**
 * This is the model class for table "{{_work_plan}}".
 *
 * The followings are the available columns in table '{{_work_plan}}':
 * @property string $id
 * @property integer $status
 * @property string $approved
 * @property string $approved_date
 * @property string $notify
 * @property string $created_by
 * @property string $created_date
 * @property string $role_id
 * @property string $date_from
 * @property string $date_to
 */
class HrWorkPlan extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return WorkPlan the static model class
     */
    public $status_new = 1;
    public $status_approved = 2;
    public $status_cancel = 3;
    public $status_require_update = 4;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{_hr_work_plan}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('date_from, date_to', 'compareTwoDate', 'on' => 'create,update'),
            array('status, approved, role_id, date_from, date_to', 'required', 'on' => 'create,update'),
            array('status, approved, approved_date, notify, created_by, created_date, role_id, date_from, date_to', 'safe'),
        );
    }
    
    /**
     * @author: DuongNV 07/08/18
     * @todo validate date from and date to
     */
    public function compareTwoDate($attribute)
    {
        $from = MyFormat::dateConverDmyToYmd($this->date_from);
        $to   = MyFormat::dateConverDmyToYmd($this->date_to);
        if(MyFormat::compareTwoDate($from, $to))
             $this->addError($attribute, 'Từ ngày phải nhỏ hơn đến ngày!');
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'rWorkSchedule' => array(self::HAS_MANY, 'HrWorkSchedule', 'word_plan_id'),
            'rRole' => array(self::BELONGS_TO, 'Roles', 'role_id'),
            'rApprovedBy' => array(self::BELONGS_TO, 'Users', 'approved'),
            'rCreatedBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'status' => 'Trạng thái',
            'approved' => 'Người duyệt',
            'approved_date' => 'Ngày duyệt',
            'notify' => 'Thông báo',
            'created_by' => 'Người tạo',
            'created_date' => 'Ngày tạo',
            'role_id' => 'Nhóm nhân viên',
            'date_from' => 'Từ ngày',
            'date_to' => 'Đến ngày',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('t.approved', $this->approved);
        
        $this->approved_date = MyFormat::dateConverDmyToYmd($this->approved_date);
        $criteria->compare('t.approved_date', $this->approved_date);
        
        $criteria->compare('t.notify', $this->notify);
        $criteria->compare('t.created_by', $this->created_by);
        
        $this->created_date  = MyFormat::dateConverDmyToYmd($this->created_date);
        $criteria->compare('t.created_date', $this->created_date);
        
        $criteria->compare('t.role_id', $this->role_id);
        
        $this->date_from     = MyFormat::dateConverDmyToYmd($this->date_from);
        $criteria->compare('t.date_from', $this->date_from);
        
        $this->date_to       = MyFormat::dateConverDmyToYmd($this->date_to);
        $criteria->compare('t.date_to', $this->date_to);
        
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    //lay danh sach trang thai
    public function getArrayStatus() {
        return array(
            $this->status_new => "Mới",
            $this->status_approved => "Đã duyệt",
            $this->status_cancel => "Hủy",
            $this->status_require_update => "Yêu cầu cập nhật"
        );
    }

    //lay trang thai
    public function getStatus() {
        $aStatus = $this->getArrayStatus();
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : '';
    }

    //lay trang thai
    public function getApproved() {

        //return isset($aStatus[$this->status]) ? $aStatus[$this->status] : '';
    }

    //lay danh sach cac role
    public function getArrayRoles() {
        return Roles::getDropdownList();
    }

    public function beforeSave() {
        $this->created_by = MyFormat::getCurrentUid();
        return parent::beforeSave();
    }

    //Lay danh sach nguoi duyet
    public static function getArrayApprovePersons() {
        $aModelUser = GasOneManyBig::getArrayModelUser(GasOneManyBig::TYPE_APPROVE_HR_PLAN, GasOneManyBig::TYPE_APPROVE_HR_PLAN);

        $aRes = array();
        foreach ($aModelUser as $item) {
            $aRes[$item->id] = $item->getNameWithRole();
        }
        return $aRes;
    }

    public function getApprovePersons() {
        return isset($this->rApproved) ? $this->rApproved->first_name : "";
    }

    public function getDateFrom() {
        return MyFormat::dateConverYmdToDmy($this->date_from);
    }

    public function getDateTo() {
        return MyFormat::dateConverYmdToDmy($this->date_to);
    }

    public function getCreatedBy() {
        return isset($this->rCreatedBy) ? $this->rCreatedBy->first_name : "";
    }

    //get arr all plan
    public function getArrayWorkPlan() {
        $aWorkShift = [];
        $model = $this->findAll();

        foreach ($model as $shift) {
            $aWorkShift[$shift->attributes['id']] = "Từ ".$shift->attributes['date_from']." đến ".$shift->attributes['date_to'];
        }
        return $aWorkShift;
    }
    
    public function getRoleName(){
        return isset($this->rRole) ? $this->rRole->role_name : '';
    }

}
