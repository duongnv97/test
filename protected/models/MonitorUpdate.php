<?php

/**
 * This is the model class for table "{{_monitor_update}}".
 *
 * The followings are the available columns in table '{{_monitor_update}}':
 * @property string $id
 * @property integer $type
 * @property string $agent_id
 * @property string $last_update
 */
class MonitorUpdate extends BaseSpj
{
    const TIME_CASHBOOK         = 2;// in minutes
    const TIME_STORECARD_HGD    = 60;// in minutes
    
    const TYPE_CASHBOOK             = 1;// TYPE CASHBOOK
    const TYPE_STORECARD_HGD        = 2;// TYPE storecard hgd
    const TYPE_STORECARD_INVENTORY  = 3;// TYPE storecard inventory 
    const TYPE_LOCATION_HGD         = 4;// TYPE vị trí và trạng thái hiện tại của NVGN
    const TYPE_CHANGE_PASS          = 5;// TYPE check change pass for user
    const TYPE_ACTIVE_USER          = 6;// TYPE tracking user ACTIVE
    const TYPE_CHANGE_AGENT_USER    = 7;// TYPE tracking user change agent employee
    const TYPE_MOBILE_REQUIRED_LOGOUT   = 8;// TYPE required USER logout mobile
    const TYPE_CHANGE_INFO_CUSTOMER     = 9;// tracking user change INFO customer Bo Moi
    const TYPE_CHANGE_INFO_CUSTOMER_HGD = 10;// tracking user change INFO customer HGD
    
    const TYPE_ERROR_HGD_ALERT_5        = 11;// không confirm đơn HGĐ 5 phút
    const TYPE_ERROR_HGD_ALERT_10       = 12;// 
    const TYPE_ERROR_HGD_ALERT_20       = 13;// 
    const TYPE_ERROR_BANK_TRANSFER_16H  = 14;// không nộp NH trc 16h30
    const TYPE_LIMIT_CHANGE_ROLE        = 15;// giới hạn đăng nhập trong 1 ngày của nv tổng đài
    const TYPE_FIX_TICKET               = 16;// Sửa ticket
    const TYPE_ERROR_HGD_COMPLETE       = 18;// lỗi hoàn thành HGD 60 phút
    const TYPE_ERROR_BOMOI_COMPLETE     = 19;// lỗi hoàn thành bò mối180 phút

    const STATUS_CONFIRM        = 1;// xác nhận đơn hàng
    const STATUS_COMPLETE       = 2;// hoàn thành đơn hàng
    const STATUS_CANCEL         = 3;// Hủy đơn hàng
    
    const TIME_COMPLETE_HGD         = 45;// phút
    const TIME_COMPLETE_HGD_APP     = 22;// phút
    const TIME_COMPLETE_BO_MOI      = 180;// phút

    const MAX_CHANGE_ROLE       = 15; // số lần cho phép đổi role trong 1 ngày
    
    public $autocomplete_name, $province_id, $date_from_ymd, $date_to_ymd, $date_from, $date_to, $timeSetUpdate = 0, $nexTimeUpdate = 0;
    public $alertOnly = false, $aAgentUid = [], $aUidOnly = [], $modelMonitorOld = null;
    
    public function getArrayStatus() {
        return [
            MonitorUpdate::STATUS_CONFIRM   => 'Xác nhận',
            MonitorUpdate::STATUS_COMPLETE  => 'Hoàn thành',
            MonitorUpdate::STATUS_CANCEL    => 'Hủy',
        ];
    }
    public function getArrayErrors() {
        return [
            MonitorUpdate::TYPE_ERROR_HGD_ALERT_5           => 'Lỗi XN 5 phút',
            MonitorUpdate::TYPE_ERROR_HGD_ALERT_10          => 'Lỗi XN 10 phút',
            MonitorUpdate::TYPE_ERROR_HGD_ALERT_20          => 'Lỗi XN 20 phút',
            MonitorUpdate::TYPE_ERROR_BANK_TRANSFER_16H     => 'Lỗi Nộp NH 16h',
            MonitorUpdate::TYPE_FIX_TICKET                  => 'Lỗi sửa ticket',
            MonitorUpdate::TYPE_ERROR_HGD_COMPLETE          => 'Lỗi HT HGĐ sau 60 phút',
            MonitorUpdate::TYPE_ERROR_BOMOI_COMPLETE        => 'Lỗi HT bò mối sau 3h',
        ];
    }
    public function getArrayStatusText() {
        $aStatus = $this->getArrayStatus();
        return isset($aStatus[$this->status_obj]) ? $aStatus[$this->status_obj] : '';
    }
    public function getTypeText() {
        $aStatus = $this->getArrayErrors();
        return isset($aStatus[$this->type]) ? $aStatus[$this->type] : '';
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return MonitorUpdate the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName(){
        return '{{_monitor_update}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('user_id, status_obj, timeSetUpdate, id, type, agent_id, last_update', 'safe'),
            array('province_id, date_from, date_to, date_from_ymd, date_to_ymd', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations(){
        return array(
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rUser' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'rTransactionHistory' => array(self::BELONGS_TO, 'TransactionHistory', 'agent_id'),
            'rGasAppOrder' => array(self::BELONGS_TO, 'GasAppOrder', 'agent_id'),
        );

    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'type' => 'Loại',
            'agent_id' => 'Agent',
            'last_update' => 'Ngày',
            'user_id' => 'Nhân viên',
            'province_id' => 'Tỉnh',
            'date_from' => 'Từ ngày',
            'date_to' => 'Đến ngày',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $aTypeLimit = array_keys($this->getArrayErrors());
        
        $criteria=new CDbCriteria;
        $sParamsIn = implode(',',  $aTypeLimit);
        $criteria->addCondition("t.type IN ($sParamsIn)");
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
            $criteria->addCondition("DATE(t.last_update) >= '$date_from'");
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
            $criteria->addCondition("DATE(t.last_update) <= '$date_to'");
        }
        $criteria->compare('t.user_id',$this->user_id);
        $criteria->compare('t.type',$this->type);
        
        $criteria->order = 't.id DESC';
        $_SESSION['data-excel-monitorUpdate'] = new CActiveDataProvider($this, array(
            'pagination'=>false,
            'criteria'=>$criteria,
//            'sort' => $sort,
        ));
        
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 10,
            ),
        ));
    }
    
    /**
     * @Author: ANH DUNG May 14, 2017
     * @Todo: get next update
     */
    public function getLastUpdate() {
        return MyFormat::dateConverYmdToDmy($this->last_update, 'd/m/Y H:i:s');
    }
    public function getNextUpdate() {
        return MyFormat::addDays($this->last_update, $this->timeSetUpdate, "+", 'minutes', 'Y-m-d H:i:s');
    }
    
    /**
     * @Author: ANH DUNG May 10, 2017
     * @Todo: kiểm tra xem agent có allow update sinh tự động report cashbook hộ GĐ không
     */
    public function cashbookAllowGenReport() {
        /* 1. get record check, nếu chưa có thì insert new và return true
         * 2. nếu có rồi thì compate last_update + 2 phút
         */
        $model = $this->getRecordCheck();
        if(is_null($model)){
            $model = $this->makeRecordNew();
            $this->nexTimeUpdate = MyFormat::addDays($model->last_update, $this->timeSetUpdate, "+", 'minutes', 'Y-m-d H:i:s');
            return true;
        }

        if(empty($this->timeSetUpdate)){
            $this->timeSetUpdate = MonitorUpdate::TIME_CASHBOOK;
        }
        $now                = date('Y-m-d H:i:s');
        $last_update_check  = $model->last_update;
        // biến $this->nexTimeUpdate sử dụng để show next time update đang ở db cho client - mục đích chỉ có vậy
//        $this->nexTimeUpdate = MyFormat::addDays($last_update_check, $this->timeSetUpdate, "+", 'minutes', 'Y-m-d H:i:s');
        $this->nexTimeUpdate = $last_update_check;
        // dùng biến $model->last_update để update back lại db khi allow update để đỡ phải tính lại
        $model->last_update  = MyFormat::addDays($now, $this->timeSetUpdate, "+", 'minutes', 'Y-m-d H:i:s');// không hiểu chỗ này cộng vào làm gì
        $this->modelMonitorOld = $model;
        return MyFormat::compareTwoDate($now, $last_update_check);
    }
    
    public function getRecordCheck() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.agent_id=' . $this->agent_id .' AND t.type='.$this->type);
        return self::model()->find($criteria);
    }
    public function getRecordCheckLocation() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.user_id=' . $this->user_id .' AND t.type='.$this->type);
        return self::model()->find($criteria);
    }
    public function makeRecordNew() {
        $model = new MonitorUpdate();
        $model->type        = $this->type;
        $model->agent_id    = $this->agent_id;
        $model->user_id     = $this->user_id;
        $model->status_obj  = $this->status_obj;
        $model->location    = $this->location;
        $model->json        = $this->json;
        $model->last_update = date('Y-m-d H:i:s');
        $model->save();
        return $model;
    }
    
    /** @Author: ANH DUNG Aug 19, 2017
     * @Todo: function create static
     */
    public static function makeRecordParams($agent_id, $type, $user_id, $status_obj, $location, $json) {
        $model = new MonitorUpdate();
        $model->agent_id    = $agent_id;
        $model->type        = $type;
        $model->user_id     = $user_id;
        $model->status_obj  = $status_obj;
        $model->location    = $location;
        $model->json        = $json;
        $model->last_update = date('Y-m-d H:i:s');
        $model->save();
        return $model;
    }
    
    /** @Author: ANH DUNG Jun 25, 2017
     * @Todo: tạo record required user change pass khi login lần đầu
     */
    public function makeRecordChangePass($mUser) {
        $this->type         = MonitorUpdate::TYPE_CHANGE_PASS;
        $this->user_id      = $mUser->id;
        $aChangePass        = $this->getRecordChangePassAll();
        foreach($aChangePass as $item){
            $item->delete();
        }
        $this->makeRecordNew();
    }
    /** @Author: ANH DUNG Oct 10, 2017
     * @Todo: tạo record save tracking change info customer
     */
    public function saveChangeInfoCustomer($mUser) {
        $this->user_id         = $mUser->id;
        $this->agent_id        = MyFormat::getCurrentUid();// current user login
        $this->json            = MyFormat::jsonEncode($mUser->getAttributes());
        $this->makeRecordNew();
    }
    public function countRecordByType() {
        if(empty($this->user_id)){
            return 0;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.user_id=' . $this->user_id . ' AND t.type=' . $this->type);
        return self::model()->count($criteria);
    }
    
    /** @Author: ANH DUNG Jun 26, 2017
     * @Todo: lưu lại ngày Customer dùng app 
     * @note: agent_id tính là sale_id
     */
    public function makeRecordActive() {
        $mMonitorUpdate = new MonitorUpdate();
        $mMonitorUpdate->user_id    = $this->user_id;
        if($mMonitorUpdate->rUser){
            $mMonitorUpdate->agent_id       = $mMonitorUpdate->rUser->sale_id;
            $mMonitorUpdate->status_obj     = $mMonitorUpdate->rUser->role_id; // need save role_id
        }
        $mMonitorUpdate->type       = MonitorUpdate::TYPE_ACTIVE_USER;
        $mMonitorUpdate->makeRecordNew();
    }
    public function getRecordChangePass() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.user_id=' . $this->user_id . ' AND t.type=' . MonitorUpdate::TYPE_CHANGE_PASS);
        return self::model()->find($criteria);
    }
    public function getRecordChangePassAll() {
        if(empty($this->user_id)){
            return [];
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.user_id=' . $this->user_id . ' AND t.type=' . MonitorUpdate::TYPE_CHANGE_PASS);
        return self::model()->findAll($criteria);
    }
    public function deleteRecordChangePass() {
        $recordChangePass = $this->getRecordChangePass();
        if($recordChangePass){
            $recordChangePass->makeRecordActive();
            $recordChangePass->delete();
        }
    }
    
    /**
     * @Author: ANH DUNG May 19, 2017
     * @Todo: update last active update
     */
    public function setNextTimeUpdate() {
        $this->update(['last_update']);
    }
    
    /**
     * @Author: ANH DUNG May 14, 2017
     * @Todo: set time update eventory all agent
     */
    public function setTimeUpdateInventory() {
        $this->type        = MonitorUpdate::TYPE_STORECARD_INVENTORY;
        $this->agent_id    = 0;
        $this->cashbookAllowGenReport();
        if(!is_null($this->modelMonitorOld)){
            $this->modelMonitorOld->setNextTimeUpdate();
        }
    }
    public function getTimeUpdateInventory() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.type=' . MonitorUpdate::TYPE_STORECARD_INVENTORY);
        $model = self::model()->find($criteria);
        if($model){
            return $model->getLastUpdate();
        }
        return '';
    }
    
    /**
     * @Author: ANH DUNG May 18, 2017
     * @Todo: cập nhật vị trí của giao nhận khi bấm xác nhận và hoàn thành đơn hàng hộ GĐ và đơn hàng BM
     * @param: $modelObj có thể là model Sell hoặc GasAppOrder
     * @param: $status_obj có thể là xác nhận ĐH và Hoàn thành ĐH
     * MonitorUpdate::updateLocationEmployee($user_id, $status_obj, $location, $modelObj)
     */
    public static function updateLocationEmployee($user_id, $status_obj, $location, $modelObj) {
        /* 1. get record check, nếu chưa có thì insert new và return true
         * 2. nếu có rồi thì compate last_update + 2 phút
         */
        $mMonitor = new MonitorUpdate();
        $mMonitor->type         = MonitorUpdate::TYPE_LOCATION_HGD;
        $mMonitor->user_id      = $user_id;
        $mMonitor->status_obj   = $status_obj;
        $mMonitor->location     = $location;
        $mMonitor->last_update  = date('Y-m-d H:i:s');
        $mMonitor->buidJsonLocation($modelObj);
        
        $mMonitorOld = $mMonitor->getRecordCheckLocation();
        if(is_null($mMonitorOld)){
            $mMonitorOld = $mMonitor->makeRecordNew();
        }else{
            $mMonitorOld->status_obj    = $status_obj;
            $mMonitorOld->location      = $location;
            $mMonitorOld->last_update      = date('Y-m-d H:i:s');
            $mMonitorOld->buidJsonLocation($modelObj);
            $mMonitorOld->update();
        }
    }
    
    /**
     * @Author: ANH DUNG May 18, 2017
     * @Todo: set array info of this user
     */
    public function buidJsonLocation($modelObj) {
        $aData = [];
        $ClassName = get_class($modelObj);
        if($ClassName == 'TransactionHistory'){
            $aData['title']      = $this->getArrayStatusText(). ' đơn hàng hộ GĐ: '.date('H:i');
        }elseif($ClassName == 'GasAppOrder'){
            $aData['title']      = $this->getArrayStatusText(). ' đơn hàng bò mối: '.date('H:i');
        }
        $mCustomer = $modelObj->rCustomer;
        if($mCustomer){
            $aData['customer_name']      = $mCustomer->first_name;
            $aData['customer_address']   = $mCustomer->address;
        }
        $this->json     = MyFormat::jsonEncode($aData);
        $this->agent_id = $modelObj->agent_id;
    }
    
    /** @Author: ANH DUNG May 19, 2017
     * @Todo: lấy toàn bộ vị trí của giao nhận hiện tại
     */
    public function getEmployeeLocation() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.type=' . MonitorUpdate::TYPE_LOCATION_HGD);
        return self::model()->findAll($criteria);
    }
    /** @Author: ANH DUNG May 19, 2017 build json vị trí giao nhận trên bản đồ
     */
    public function getMapEmployee() {
        $aEmployee      = $this->getEmployeeLocation();
        $mAppCache      = new AppCache();
//        $aModelEmployee = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MAINTAIN);
        $aModelEmployee = $mAppCache->getUserByRole(ROLE_EMPLOYEE_MAINTAIN);
        $aModelAgent    = $mAppCache->getListdataUserByRole(ROLE_AGENT);
        $index = 1; $aDataChart = [];
        foreach($aEmployee as $model){
            if($model->location == ''){
               continue;
            }
            $tmp = explode(',', $model->location);
            if(count($tmp) < 2){
                continue ;
            }
            $json = json_decode($model->json, true);
            $employeeName = $employeePhone = '';
            if(isset($aModelEmployee[$model->user_id])){
                $sPhone = $aModelEmployee[$model->user_id]['phone'];
                if(!empty($sPhone)){
                    $aPhone = explode('-', $sPhone);
                    $sPhone = ''; 
                    foreach($aPhone as $key=>$phone){
                        $separate = ' - ';
                        if($key == 0){
                            $separate = ' ';
                        }
                        $sPhone .= $separate.UsersPhone::formatPhoneView($phone);
                    }
                }
                $employeeName   = $aModelEmployee[$model->user_id]['first_name'] . ' - '. $sPhone;
                
            }
            $agentName      = isset($aModelAgent[$model->agent_id]) ? $aModelAgent[$model->agent_id] : '';
            $title          = isset($json['title']) ? $json['title'] : '';
            $customerName   = isset($json['customer_name']) ? $json['customer_name'] : '';
            $customerAddress= isset($json['customer_address']) ? $json['customer_address'] : '';
            
            $contentString = "<div id='content'>";
            $contentString .= "<h1 id='firstHeading' class='firstHeading'>{$employeeName}</h1>";
                $contentString .= "<div id='bodyContent'>";
                    $contentString .= "<p><b>{$title}</b></p>";
                    $contentString .= "<p><b>$agentName</b></p>";
                    $contentString .= "<p><b>KH: </b> {$customerName}</p>";
                    $contentString .= "<p><b>Đ/C:</b> {$customerAddress}</p>";
                $contentString .= "</div>";
            $contentString .= "</div>";
            
            $source = '/themes/gas/new-icon/status_free_3.png';
            if ($model->status_obj == MonitorUpdate::STATUS_CONFIRM){
                $source = '/themes/gas/new-icon/status_busy_3.png';
            }
            $icon = Yii::app()->createAbsoluteUrl("/").$source;
            $aDataChart[]   = array($contentString, $tmp[0], $tmp[1], $index++, $icon);
        }
        return $aDataChart;
    }
    
    /**
     * @Author: ANH DUNG May 25, 2017
     * @Todo: delete record and Type cuối mỗi ngày => chưa cần thiết xóa, nên cứ để lại
     */
    public static function deleteByTypeAndUser($type, $user_id, $agent_id, $needMore = []) {
        if(empty($type)){
            return ;
        }
        $day = 1;
        if(isset($needMore['day'])){
            $day = $needMore['day'];
        }
        $criteria = new CDbCriteria();
        if(is_array($type)){
            $sParamsIn = implode(',', $type);
            $criteria->addCondition("type IN ($sParamsIn)");
        }else{
            $criteria->addCondition('type='.$type);
        }
        if(!empty($user_id)){
            $criteria->addCondition('user_id='.$user_id);
        }
        if(!empty($agent_id)){
            $criteria->addCondition('agent_id='.$agent_id);
        }
        $criteria->addCondition("DATE_ADD(last_update,INTERVAL $day DAY) < NOW()");
        
        MonitorUpdate::model()->deleteAll($criteria);
    }
    
    /** @Author: ANH DUNG Aug 19, 2017
     * @Todo: get lịch sử đổi đại lý của nhân viên
     */
    public static function getHistoryChangeAgent() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('type='.MonitorUpdate::TYPE_CHANGE_AGENT_USER);
        $criteria->addCondition("DATE_ADD(last_update,INTERVAL 5 DAY) >= NOW()");
        $criteria->order = 't.id DESC';
        $models = MonitorUpdate::model()->findAll($criteria);
        $aRes = [];
        foreach($models as $item){
            $aRes[$item->agent_id][] = $item->json;
        }
        return $aRes;
    }
    
    /** @Author: ANH DUNG Aug 19, 2017
     *  @Todo: check required logout
     */
    public static function isMobileRequiredLogout($user_id) {
        if(empty($user_id)){
            return 0;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('user_id='.$user_id);
        $criteria->addCondition('type='.MonitorUpdate::TYPE_MOBILE_REQUIRED_LOGOUT);
        return MonitorUpdate::model()->count($criteria);
    }
    /** @Author: ANH DUNG Aug 19, 2017
     *  @Todo: remove required logout
     */
    public static function removeMobileRequiredLogout($user_id) {
        if(empty($user_id)){
            return 0;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('user_id='.$user_id);
        $criteria->addCondition('type='.MonitorUpdate::TYPE_MOBILE_REQUIRED_LOGOUT);
        return MonitorUpdate::model()->deleteAll($criteria);
    }
    
    /** @Author: ANH DUNG Sep 22, 2017
     * @Todo: get detail active of customer by sale
     * @note: user_id ở type này là customer_id, agent_id là sale id
     */
    public function appReportActiveOfSale() {
        // 1. tổng số KH kích hoạt theo từng sale
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.type='.MonitorUpdate::TYPE_ACTIVE_USER);
        $criteria->addCondition('t.status_obj='.ROLE_CUSTOMER);
        $criteria->addCondition('t.agent_id='.$this->agent_id);
        return MonitorUpdate::model()->findAll($criteria);
    }

    /** @Author: ANH DUNG Sep 22, 2017
     * @Todo: get detail active of customer by sale
     * @note: user_id ở type này là customer_id, agent_id là sale id
     */
    public function appReportOrdersOfSale() {
        $aRes = [];
        if(empty($this->agent_id)){
            return $aRes;
        }
        // 1. Tổng số KH đặt app từng tháng theo từng sale
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.status='.  GasAppOrder::STATUS_COMPPLETE);
        $criteria->addCondition('t.uid_login_role='.ROLE_CUSTOMER);
        $criteria->addCondition('t.sale_id='.$this->agent_id);
        $criteria->select   = 'count(t.id) as id, t.customer_id';
        $criteria->group    = 't.customer_id';
        $mRes = GasAppOrder::model()->findAll($criteria);
        foreach($mRes as $item){
            $aRes['ORDERS'][$item->customer_id]  = $item->id;
            $aRes['CUSTOMER_ID'][$item->customer_id]  = $item->customer_id;
        }
        return $aRes;
    }
    
    /** @Author: ANH DUNG Now 18, 2017
     *  @Todo: get Record By type
     **/
    public function getByType() {
        if(empty($this->user_id)){
            return 0;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.user_id=' . $this->user_id);
        $sParamsIn = implode(',',  $this->type);
        $criteria->addCondition("t.type IN ($sParamsIn)");
        $criteria->order = 't.id DESC';
        $criteria->limit = 10;
        return self::model()->findAll($criteria);
    }
    
    /** @Author: ANH DUNG Now 24, 2017
     *  @Todo: delete Record By type
     **/
    public function deleteByType() {
        if(empty($this->user_id) || !is_array($this->type)){
            return ;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('user_id=' . $this->user_id);
        if(!empty($this->agent_id)){
            $criteria->addCondition('agent_id=' . $this->agent_id);
        }
        $sParamsIn = implode(',',  $this->type);
        if(empty($sParamsIn)){
            return ;
        }
        $criteria->addCondition("type IN ($sParamsIn)");
        MonitorUpdate::model()->deleteAll($criteria);
    }
    // loại ngày nghỉ + t7, CN không quét nộp NH
    public function canAlert() {
        $this->alertSetupHolidays();
        $mHoliday = new GasLeaveHolidays();
        if($mHoliday->isHoliday(date('Y-m-d'))){
            $info   = 'Ngày nghỉ lễ, không quyét nộp NGÂN HÀNG '.date('Y-m-d');
            $needMore['title']      = $info;
            $needMore['list_mail']  = ['dungnt@spj.vn'];
            SendEmail::bugToDev($info, $needMore);
            $this->alertOnly = true;// Jan2819 có mail alert nhưng ko tính lỗi
            return false;// Jan0118 Ngày nghỉ vẫn có người thu tiên, nên vẫn quét nộp NH
        }

        $day_of_week = strtolower(date('l'));
        $aNotCheck = ['saturday', 'sunday'];
//        if(in_array($day_of_week, $aNotCheck)){// chủ nhật ko alert
        if(0){// DungNT Sep2719 Open check cả T7 + CN
            $this->alertOnly = true;// Jan2819 có mail alert nhưng ko tính lỗi
            return false;
        }
        return true;
    }
    
    /** @Author: ANH DUNG Jan 01, 2018
     *  @Todo: nhắc nhở setup ngày nghỉ trong năm tiếp theo
     **/
    public function alertSetupHolidays() {
        if(date('m-d') != '01-01'){
            return ;
        }
        $cYear = date('Y');
        $info   = "Năm $cYear Important - Nhắc nhở setup Ngày nghỉ lễ Table GasLeaveHolidays - liên quan đến tính lỗi nộp NH";
        $needMore['title']      = $info;
        $needMore['list_mail']  = ['dungnt@spj.vn'];
        SendEmail::bugToDev($info, $needMore);
    }
    
    /** @Author: ANH DUNG Now 19, 2017
     *  @Todo: xử lý quét những giao nhận chưa nộp tiền NH
     * 1. get all giao nhận của toàn hệ thống, trừ các đại lý ko chạy app và đại lý CH5
     * 2. get all Giao Nhận đã có nộp tiền NH hoặc chi nộp dùm NH
     * 3. Loại giữa (1) và (2) sau đó lọc tiếp tìm ra những GN có đơn hàng hộ GĐ trong ngày để lọc lại
     * chỉ check 1 loại lúc 16h30
     **/
    public function handleErrorBankTransfer() {
        if(!$this->canAlert()){// T7 + chủ nhật ko alert
//            return ;// Close Jan2819 có mail alert nhưng ko tính lỗi, trc day return luon ko gui mail
        }
        // 1. send mail to KTKV, KTVP
        // 2. make notify
        // 3. add to table tính lỗi GN
        // to be continue - chưa xong
        $aAgentNotReport    = $this->getAgentNotReport();
        $needMore       = ['ArrAgentIdNotFind' => $aAgentNotReport];
        // tính tồn quỹ của all employee
        $mCashBookDetail = new GasCashBookDetail();
        $mCashBookDetail->date_from         = date('d-m-Y');
        $mCashBookDetail->date_to           = $mCashBookDetail->date_from;
        // Mar2818 bỏ không check đk đi ngân hàng nữa, cứ trên 3 tr là cảnh báo. Đại Lý Thủ Đức 2 - bị tồn 26tr mà không cảnh báo
//        $mCashBookDetail->aUidGoBank        = $this->getAllEmployeeGoBank($aAgentNotReport);
        $this->handleErrorBankTransferAddException($mCashBookDetail);
        $mCashBookDetail->isInventoryAlert  = true;
        $mCashBookDetail->getInventoryCashAllEmployee($needMore);
        $mHtmlFormat = new HtmlFormat();
        $mHtmlFormat->alertOnly = $this->alertOnly;
        $mHtmlFormat->emailInventoryCash($mCashBookDetail);
    }
    
    /** @Author: ANH DUNG Jan 08, 2018
     *  @Todo: Đại lý Gas Minh Hoàng anh Hoàn đã duyệt nộp tiền vào thứ 6 hàng tuần.
     *  @fix: tự động thêm NV này vào danh sách đã nộp NH nếu ngày khác thứ 6
     * @todo: bỏ qua lỗi nộp NH cho 1 số trường hợp đặc biệt 
     **/
    public function handleErrorBankTransferAddException(&$mCashBookDetail) {
        // add nhan vien khong quet o day
//        $mCashBookDetail->aUidGoBank[1107824] = 1107824;
//        $mCashBookDetail->aUidGoBank[1090747] = 1090747;
//        $mCashBookDetail->aUidGoBank[1492305] = 1492305;// Jun0618 Nguyễn Đình Hoàng (Đào tạo lại)
        
        $mAppCache      = new AppCache();
        $aEmployeeDebit = $mAppCache->getListdataUserByRole(ROLE_DEBT_COLLECTION);
        foreach($aEmployeeDebit as $uid => $first_name){// Jan1618 tính cả nhân viên thu nợ
            if(isset($mCashBookDetail->aUidGoBank[$uid])){
                unset($mCashBookDetail->aUidGoBank[$uid]);
            }
            $mCashBookDetail->aEmployeeDebit[$uid] = $uid;
        }
        
        $day_of_week = strtolower(date('l'));
        $aDateCheck = ['friday'];
        if(in_array($day_of_week, $aDateCheck)){// thứ 6 hàng tuần
            return ;
        }
//        $mCashBookDetail->aUidGoBank[1299645] = 1299645;// Phạm Minh Hoàng - Đại Lý Gas Minh Hoàng - Đồng Nai
//        $mCashBookDetail->aUidGoBank[1287306] = 1287306;// Mar1218 Phùng Tiến Đạt - nghỉ việc
//        $mCashBookDetail->aUidGoBank[887015] = 887015;// Mar1618 Nguyễn Tuấn Cường
    }
    
    /** @Author: ANH DUNG Dec 07, 2017
     *  @Todo: lưu lỗi nộp ngân hàng muộn sau 16h30
     **/
    public function saveErrorsBankTransfer() {
        $model = new MonitorUpdate();
        $model->type            = MonitorUpdate::TYPE_ERROR_BANK_TRANSFER_16H;
        $model->user_id         = $this->user_id;// employee GN
        $mUser = Users::model()->findByPk($this->user_id);
        $model->agent_id        = $mUser->parent_id;
        $model->makeRecordNew();
        $model->saveErrorsBankTransferMoveToSupport();
    }
    
    /** @Author: ANH DUNG Now 25, 2018
     *  @Todo: save to table EmployeeProblems, save toàn bộ lỗi vào 1 chỗ để trừ lương
     **/
    public function saveErrorsBankTransferMoveToSupport() {
        $aUidSend[$this->user_id]       = $this->user_id;
        
        $mTransactionHistory            = new TransactionHistory();
        $mTransactionHistory->id        = 0;// truyền id ticket vào model EmployeeProblems
        $mTransactionHistory->agent_id  = $this->agent_id;
        
        $mEmployeeProblems              = new EmployeeProblems();
        $mScheduleNotify                = new GasScheduleNotify();
        $mScheduleNotify->type          = GasScheduleNotify::TYPE_NOT_SEND_SMS;// Now2618 ko gửi sms phạt nữa, chỉ notify app
        $mScheduleNotify->aUidSend      = $aUidSend;
        $this->titleNotify = "[E04 Tinh Loi]: ".MyFormat::dateConverYmdToDmy($this->last_update)." chưa nộp tiền ngân hàng 16h30";
        
        $mEmployeeProblems->object_type     = EmployeeProblems::PROBLEM_ERROR_BANK_TRANSFER_16H;
        $mEmployeeProblems->money           = $mEmployeeProblems->getPrice();
        $mScheduleNotify->mEmployeeProblems = $mEmployeeProblems;
        $mScheduleNotify->title             = $this->titleNotify;
        $mScheduleNotify->saveErrorsHgdSms($mTransactionHistory, []);
//        $this->runInsertNotify($mScheduleNotify->aUidSend, false); // notify to phone app
    }
    
    /** @Author: ANH DUNG Now 19, 2017
     * 1. get all Giao Nhận đã có nộp tiền NH hoặc chi nộp dùm NH
     * 2. Find tiếp tìm ra những GN có đơn hàng hộ GĐ hoàn thành trong ngày để lọc lại
     * 3. get all giao nhận của toàn hệ thống, trừ các đại lý ko chạy app và đại lý CH5
     *  Loại giữa (1) và (2) => array agent cần được cảnh báo
     **/
    public function getEmployeeNeedAlert() {
        $aAgentNotReport    = $this->getAgentNotReport();
        // 1. get all Giao Nhận đã có nộp tiền NH hoặc chi nộp dùm NH
        $aUidGoBank         = $this->getAllEmployeeGoBank($aAgentNotReport);
        // 2. Find tiếp tìm ra những GN có đơn hàng hộ GĐ hoàn thành trong ngày để lọc lại
        $aUidCompleteHgd    = $this->getAllEmployeeCompleteHgd($aAgentNotReport);
        $this->getAllEmployeeNeedAlert($aAgentNotReport, $aUidGoBank, $aUidCompleteHgd);
    }
    public function getAgentNotReport() {
        $aAgentLock     = UsersExtend::getAgentNotRunApp();
//        $aAgentLock[]   = 1315; // for dev test Cửa hàng 5
        $aAgentLock[]   = GasConst::UID_AGENT_XE_TAI_1;
        $aAgentLock[]   = GasConst::UID_AGENT_XE_TAI_2;
        $aAgentLock[]   = GasConst::UID_AGENT_XE_TAI_3;// Add Mar2119
//        $aAgentLock[]   = 898894; // Cửa Hàng Gas Minh Hạnh 
//        $aAgentLock[]   = 1161356; // Đại Lý An Hiệp - Đồng Tháp Mar0118
        $aAgentLock     = array_merge($aAgentLock, GasCheck::getAgentNotGentAuto());
        $toRemove       = [GasCheck::KHO_86_NCV_DKMN];
        $aAgentLock     = array_diff($aAgentLock, $toRemove);
        return $aAgentLock;
    }
    /** @Author: ANH DUNG Now 19, 2017
     *  @Param: $aUidGoBank array user đã đi nộp NH
     **/
    public function getAllEmployeeGoBank($aAgentNotReport) {
        $mEmployeeCashbook  = new EmployeeCashbook();
        $needMore           = ['ArrAgentIdNotFind' => $aAgentNotReport];
        $aModelGoBank       = $mEmployeeCashbook->getRecordGoBank($needMore);
        return CHtml::listData($aModelGoBank,'employee_id','employee_id');  
    }
    
    /** @Author: ANH DUNG Now 19, 2017
     *  @Todo: Find tiếp tìm ra những GN có đơn hàng hộ GĐ hoàn thành trong ngày để lọc lại
     **/
    public function getAllEmployeeCompleteHgd($aAgentNotReport) {
        $today = date('Y-m-d');
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.created_date_only='$today'");
        $criteria->addCondition('t.status='.Sell::STATUS_PAID);
        $sParamsIn = implode(',', $aAgentNotReport);
        $criteria->addCondition("t.agent_id NOT IN ($sParamsIn)");
        $models = Sell::model()->find($criteria);
        return CHtml::listData($models,'employee_maintain_id','employee_maintain_id');  
    }
    
    /** @Author: ANH DUNG Now 19, 2017
     *  @Param: $aUidGoBank array user đã đi nộp NH
     * Sau khi lấy đc mảng này thì kiểm tra tồn quỹ của NV, ai trên 3 triệu thì đưa vào phạt
     * @note ==> không sử dụng hàm này, do ban đầu phân tích sai, không cần tính toán nhiều như vậy, chỉ cần  lấy bên tồn quỹ, nếu > 3 triệu là mail nhắc
     **/
    public function getAllEmployeeNeedAlert($aAgentNotReport, $aUidGoBank, $aUidCompleteHgd) {
        // 1. lọc ra các uid cần alert . Sẽ lấy array uid đã nộp NH rồi, đưa vào hàm này lọc lại, để giữ đc mảng Agent của User
        $mGasOneMany = new GasOneMany();
        $mGasOneMany->type = ONE_AGENT_MAINTAIN;
        $needMore       = ['ArrAgentIdNotFind' => $aAgentNotReport];
        $models         = $mGasOneMany->getByType($needMore);
        foreach($models as $mOneMany){
            if(!in_array($mOneMany->many_id, $aUidGoBank) && in_array($mOneMany->many_id, $aUidCompleteHgd)){
                $this->aAgentUid[$mOneMany->one_id][$mOneMany->many_id] = $mOneMany->many_id;
                $this->aUidOnly[$mOneMany->many_id] = $mOneMany->many_id;
            }
        }
        // 2. tính tồn quỹ của all employee
        $mCashBookDetail = new GasCashBookDetail();
        $mCashBookDetail->date_from     = date('d-m-Y');
        $mCashBookDetail->date_to       = $mCashBookDetail->date_from;
        $mCashBookDetail->isInventoryAlert     = true;
        $aInventoryCash = $mCashBookDetail->getInventoryCashAllEmployee($needMore);
    }

    /** @Author: ANH DUNG Dec 07, 2017
     *  @Todo: build notify alert all giao nhận nộp tiền NH
     **/
    public function alertBankTransfer() {
        if(!$this->canAlert()){// T7 + chủ nhật ko alert
            return ;
        }
        $aAgentNotReport    = $this->getAgentNotReport();
        $mGasOneMany = new GasOneMany();
        $mGasOneMany->type  = ONE_AGENT_MAINTAIN;
        $needMore           = ['ArrAgentIdNotFind' => $aAgentNotReport];
        $models             = $mGasOneMany->getByType($needMore);
        $message            = 'Nhắc nhở nộp tiền ngân hàng trước 16h30';
        foreach($models as $mOneMany){
            $uid = $mOneMany->many_id;
            GasScheduleNotify::InsertRecord($uid, GasScheduleNotify::ANNOUNCE_TO_ALL, 0, '', $message, []);
        }
    }
    
    /** @Author: ANH DUNG Dec 13, 2017
     *  @Todo: check user can change role 
     **/
    public function checkCanChangeRole($user_id) {
        $this->last_update  = date('Y-m-d');
        $this->user_id      = $user_id;
        $model = $this->getRecordChangeRole();
        if(empty($model)){
            $this->saveChangeRole();
        }else{
            $model->agent_id    += 1;// count change role
            if($model->agent_id > MonitorUpdate::MAX_CHANGE_ROLE){
                throw new Exception('Không thể thực hiên hành động này, mỗi ngày bạn chỉ được '.MonitorUpdate::MAX_CHANGE_ROLE. ' lần bấm chuyển giữa tổng đài hộ và bò mối hoặc ngược lại');
            }
            $model->update();
        }
    }
    
    /** @Author: ANH DUNG Dec 13, 2017
     *  @Todo: save change role 
     **/
    public function saveChangeRole() {
//        MAX_CHANGE_ROLE
        $model = new MonitorUpdate();
        $model->type        = MonitorUpdate::TYPE_LIMIT_CHANGE_ROLE;
        $model->user_id     = $this->user_id;// employee id
        $model->agent_id    = 1;// count change role
        $model->makeRecordNew();
    }
    
    public function getRecordChangeRole() {
//        date('Y-m-d')
        $criteria = new CDbCriteria();
        $criteria->compare('t.type', MonitorUpdate::TYPE_LIMIT_CHANGE_ROLE);
        $criteria->compare('t.user_id', $this->user_id);
        $criteria->addCondition("DATE(t.last_update)='".$this->last_update."'");
        return MonitorUpdate::model()->find($criteria);
    }
    
    /** @Author: ANH DUNG Dec 30, 2017
     *  @Todo: save lỗi sửa ticket của User
     **/
    public function saveFixTicket($mTicket) {
        $this->removeFixTicket($mTicket);
        $model = new MonitorUpdate();
        $model->type        = MonitorUpdate::TYPE_FIX_TICKET;
        $model->user_id     = $mTicket->uid_login;// employee id
        $model->agent_id    = $mTicket->id;// save ticket id
        $model->makeRecordNew();
    }
    public function removeFixTicket($mTicket) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.type='.MonitorUpdate::TYPE_FIX_TICKET . ' AND t.agent_id='.$mTicket->id );
        $model = MonitorUpdate::model()->find($criteria);
        if($model){
            $model->delete();
        }
    }
    
    /** @Author: ANH DUNG Mar 10, 2018
     *  @Todo: save lỗi hoàn thành quá thời gian đơn hàng HGD và Bò Mối
     *  @param: $type is TYPE_ERROR_HGD_COMPLETE or TYPE_ERROR_BOMOI_COMPLETE or ....
     *  @param: $user_id is employee id get error
     *  @param: $obj_id is transaction_history_id or GasAppOrderId or ...
     **/
    public function saveAllCaseError($type, $user_id, $obj_id) {
        $model = new MonitorUpdate();
        $model->type        = $type;
        $model->user_id     = $user_id;
        $model->agent_id    = $obj_id;
        $model->makeRecordNew();
    }
    
    /** @Author: ANH DUNG Feb 21, 2018
     *  @Todo: get someinfo listing
     **/
    public function getType() {
        $res = '';
        switch ($this->type) {
            case MonitorUpdate::TYPE_ERROR_HGD_ALERT_5:
            case MonitorUpdate::TYPE_ERROR_HGD_ALERT_10:
            case MonitorUpdate::TYPE_ERROR_HGD_ALERT_20:
            case MonitorUpdate::TYPE_ERROR_BANK_TRANSFER_16H:
            case MonitorUpdate::TYPE_FIX_TICKET:
            case MonitorUpdate::TYPE_ERROR_HGD_COMPLETE:
            case MonitorUpdate::TYPE_ERROR_BOMOI_COMPLETE:
                $res = $this->getTypeText();
                break;
        }
        return $res;
    }
    public function getDate() {
        return MyFormat::dateConverYmdToDmy($this->last_update, 'd/m/Y H:i');
    }
    public function getUser() {
        $mUser = $this->rUser;
        if($mUser){
            return $mUser->getFullName();
        }
        return '';
    }
    public function getDetail() {
        $res = '';
        switch ($this->type) {
            case MonitorUpdate::TYPE_ERROR_HGD_ALERT_5:
            case MonitorUpdate::TYPE_ERROR_HGD_ALERT_10:
            case MonitorUpdate::TYPE_ERROR_HGD_ALERT_20:
            case MonitorUpdate::TYPE_ERROR_HGD_COMPLETE:
                $res = $this->getDetailHgdAlert();
                break;
            case MonitorUpdate::TYPE_ERROR_BANK_TRANSFER_16H:
                $res = '';
                break;
            case MonitorUpdate::TYPE_FIX_TICKET:
                $res = $this->getDetailTicket();
                break;
            case MonitorUpdate::TYPE_ERROR_BOMOI_COMPLETE:
                $res = $this->getDetailBoMoiAlert();
                break;
        }
        return $res;
    }
    public function getDetailHgdAlert() {
        $mTransactionHistory = $this->rTransactionHistory;
        if($mTransactionHistory){
            return 'Mã đơn hàng: <b>'.$mTransactionHistory->getCodeNo().'</b>';
        }
        return '';
    }
    public function getDetailTicket() {
        $res = '';
        $mTicketDetail = new GasTicketsDetail();
        $mTicketDetail->ticket_id = $this->agent_id;
        $needMore['order'] = 't.id ASC';
        $aTicketDetail = $mTicketDetail->getByTicketIdV1($needMore);
        foreach($aTicketDetail as $mTicketDetail):
            $res .= '- '.$mTicketDetail->message.'<br>';
        endforeach;
        return $res;
    }
    public function getDetailBoMoiAlert() {
        $mAppOrder = $this->rGasAppOrder;
        if($mAppOrder){
            return 'Mã ĐH bò mối: <b>'.$mAppOrder->getCodeNo().'</b>';
        }
        return '';
    }
    
    
}   