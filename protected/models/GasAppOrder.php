<?php 
/**
 * This is the model class for table "{{_gas_app_order}}".
 *
 * The followings are the available columns in table '{{_gas_app_order}}':
 * @property integer $qty_12
 * @property integer $qty_50
 */
class GasAppOrder extends BaseSpj
{ 
    public $type_stock_new          = 1;
    public $type_stock_done         = 2;
    public $type_confirm_real       = 3;
    public $apiStockList = 0, $dateDeliveryOld, $checkPriceNexMonth = false, $allowChangePrice = 0, $file_name, $list_id_image=[], $MAX_ID, $autocomplete_name, $autocomplete_name_1, $autocomplete_name_2, $agent_id_old = 0, $isChangeAgent=false, $isFromConfirm=false, $isChangeDate=false, $isUpdateMoney=0, $isWebUpdate=false, $setNotDebit=0, $setStatusNew=0, $setStatusComplete=0;
    public $hasGas6kg = false, $webSetDebit=0, $obj_id_old, $obj_detail_id_old, $change_qty=0, $aOrderDetailIdDelete = [], $aOrderCar = [], $aPhuXe = [], $date_from_ymd = '', $date_to_ymd = '';
    public $aGasRemainExportOtherAgent = [], $aGasRemainKeepPrice = [], $aGasRemainOld = [], $aSeriOld = [], $aAgentForCar=[], $aCacheQtyExportInDay=[], $aQtyExportGas=[], $mAppUserLogin = null, $requiredFile = true;
    public $status_old, $ext_materials_id, $ext_materials_qty, $getQtyReal=false, $getQtyVo=false, $isDieuPhoiCreate=false, $date_from, $date_to, $c_day, $c_month, $c_year;
    public $sListGas='', $sListGasQty='', $sListPrice='', $sListRemain='', $sListQty=0, $sListVo='', $sListVoQty='';
    public $sTextSummary='', $doneReweighed = false, $isLocationWrong = 0, $aListUnit=[], $aListGas=[], $aListGasQty=[], $aListGasWeight=[], $aListPrice=[], $totalRemain=[], $totalWeightGas=0, $aListVo=[], $aListVoQty=[], $aListGasId = [], $aListGasAmount = [];
    const API_LISTING_ITEM_PER_PAGE = 10;
//    const API_LISTING_ITEM_PER_PAGE = 5;
    //1: mới, 2: đang giao, 3: hoàn thành, 4: hủy, 5: Khách hàng giả 
    const STATUS_NEW        = 1;
    const STATUS_CONFIRM    = 2;
    const STATUS_PROCESSING = 3;
    const STATUS_COMPPLETE  = 4;
    const STATUS_CANCEL     = 5;
    const WEB_SET_STATUS_NEW        = 6;// Jul2317 không dùng làm status chính thức, chỉ để xử lý cho show event
    const WEB_SET_ORDER_DEBIT       = 7;// Aug2817 không dùng làm status chính thức, chỉ để xử lý cho show event
    const WEB_PAY_DEBIT             = 8;// App thu tiền nợ KH
    const WEB_UPDATE                = 9;// Oct0517 không dùng làm status chính thức, chỉ để xử lý cho show event
    const WEB_UPDATE_CASHBOOK       = 10;// Oct0617 không dùng làm status chính thức, chỉ để xử lý cho show event
    
    // Mar 19, 2017 phân biệt thẻ kho thường và thẻ kho thu vỏ
    public $typeStorecard       = 1;
    const STORECARD_NORMAL      = 1;
    const STORECARD_XE_TAI      = 2;
    const STORECARD_THU_VO      = 3;
    // end Mar 19, 2017
    const DEBIT_NORMAL      = 0;
    const DEBIT_YES         = 1;// nợ full
    const DEBIT_NO          = 2;// không nợ
    const DEBIT_NOT_FULL    = 3;// trả không đủ, nợ 1 phần
    
    const APP_TAB_DEBIT     = 3;// Tab KH nợ trên App
    
    const PAID_REMAIN_NO    = 0;// chưa chi gas dư
    const PAID_REMAIN_YES   = 1;// đã chi
    
    const TYPE_PAY_DIRECT   = 1;// thu tiền liền
    const TYPE_THU_VO       = 2;// thu vỏ
    const TYPE_DISCOUNT     = 3;// Giảm giá
    
    const WRONG_LOCATION            = -1;// cập nhật sai vị trí KH
    const WRONG_NOT_SET_CUSTOMER    = -2;// Chưa có vị trí KH
    
    const API_STOCK         = 1;// is from api stock not api DH bo moi

    public function getArrayStatus(){
         return array(
            GasAppOrder::STATUS_NEW             => 'Mới',
            GasAppOrder::STATUS_CONFIRM         => 'Đã xác nhận',
            GasAppOrder::STATUS_PROCESSING      => 'Đang giao',
            GasAppOrder::STATUS_COMPPLETE       => 'Hoàn thành',
            GasAppOrder::STATUS_CANCEL          => 'Hủy bỏ',
        );
    }
    public function getArraySetLocation(){
         return array(
            GasAppOrder::WRONG_LOCATION         => 'Hoàn thành sai vị trí',
            GasAppOrder::WRONG_NOT_SET_CUSTOMER => 'KH chưa có vị trí',
        );
    }
    public function getArrayStatusDebit(){
         return array(
            GasAppOrder::DEBIT_YES      => 'Đơn hàng nợ',
            GasAppOrder::DEBIT_NO       => 'Không nợ',
        );
    }
    
    public function getAppStatusNew() {
        return [
            GasAppOrder::STATUS_NEW,
            GasAppOrder::STATUS_CONFIRM,
            GasAppOrder::STATUS_PROCESSING,
        ];
    }
    public function getAppStatusReport() {
        return [
            GasAppOrder::STATUS_CONFIRM,
            GasAppOrder::STATUS_PROCESSING,
            GasAppOrder::STATUS_COMPPLETE,
        ];
    }
    
    public function getArrayStatusConfirm() {
        return array(
            GasAppOrder::STATUS_NEW             => 'Mới',
            GasAppOrder::STATUS_CONFIRM         => 'Đã xác nhận',
            GasAppOrder::STATUS_CANCEL          => 'Hủy bỏ',
        );
    }
    public function getRoleUpdateOrder() {
        return [ROLE_DRIVER, ROLE_EMPLOYEE_MAINTAIN, ROLE_EMPLOYEE_MARKET_DEVELOPMENT];
    }
    // Aug1618 id các xe chạy test nhập STT
    public function getArrayCarRunStt() {
        return [
            1574476, // Xe 51D - 31674 - Nguyễn Hữu Thành 
            874029, // Xe 54Y - 9990 - Lê Quang Chung
        ];
    }
    // Aug1718 id các xe nhập STT từ kho Trung Chuyển Bình Thạnh 1
    public function getArrayCarAllowInputStt() {
        return [
            1574476, // Xe 51D - 31674 - Nguyễn Hữu Thành 
        ];
    }
    
    public $isCustomerCreate, $aInfoPrice = [], $b50, $b45, $b12, $b6, $info_gas, $info_vo, $customer_qty_request = [], $aDetail = [], $aDetailVo = [], $aNameVo = [], $aNameGas = [];
    public $price_bb='', $price_b12='', $name_gas='', $name_vo='', $name_driver='', $name_car='', $name_employee_maintain='';
    public $customer_name='', $customer_address='', $customer_phone='', $customer_contact='';
    public $aInventoryAgent = [], $aMaterialSetupOfCustomer = [], $isGetGasSecond = false;
//    $note_customer='', $note_employee=''; tạm bỏ 2 biến này đi vì đã có trong db rồi

    public $JSON_FIELD      = array('info_gas', 'info_vo', 'customer_qty_request');
//    public $JSON_FIELD_INFO = array('price_bb', 'price_b12', 'name_gas', 'name_vo', 'name_driver', 'name_car', 'note_customer', 'note_employee', 'name_employee_maintain', 'customer_name', 'customer_address', 'customer_phone', 'customer_contact');
    public $JSON_FIELD_INFO = array('price_bb', 'price_b12', 'name_gas', 'name_vo', 'name_driver', 'name_car', 'name_employee_maintain', 'customer_name', 'customer_address', 'customer_phone', 'customer_contact');
    
    /**
     * @Author: DungNT Jan 11, 2017
     * @Todo: những loại vật tư mà KH đc đặt ở dưới app
     */
    public function getMaterialsType() {
        return array(
            GasMaterialsType::MATERIAL_BINH_50KG    => 'Bình 50kg',
            GasMaterialsType::MATERIAL_BINH_45KG    => 'Bình 45kg',
            GasMaterialsType::MATERIAL_BINH_12KG    => 'Bình 12kg',
            GasMaterialsType::MATERIAL_BINH_6KG     => 'Bình 6kg',
            
            GasMaterialsType::MATERIAL_VO_50        => 'Vỏ bình 50kg',
            GasMaterialsType::MATERIAL_VO_45        => 'Vỏ bình 45kg',
            GasMaterialsType::MATERIAL_VO_12        => 'Vỏ bình 12kg',
            GasMaterialsType::MATERIAL_VO_6         => 'Vỏ bình 6kg',
        );
    }

    const DELIVERY_NOW      = 1;
    const DELIVERY_BY_CAR   = 2;
    const DELIVERY_THU_VO   = 3;

    /**
     * @Author: DungNT Oct 26, 2015
     */
    public function getStatusText() {
        $aStatus = $this->getArrayStatus();
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : '';
    }
    public function getArrayDeliveryText() {
        $aDelivery = $this->getArrayDelivery();
        return isset($aDelivery[$this->type]) ? $aDelivery[$this->type] : '';
    }
    public function getArrayDelivery() {
        return array(
            self::DELIVERY_NOW    => 'Giao ngay',
            self::DELIVERY_BY_CAR => 'Xe tải giao',
        );
    }
    public function getTypePayDirect() {
        return array(
            self::TYPE_PAY_DIRECT   => 'Thu tiền liền',
            self::TYPE_THU_VO       => 'Thu vỏ',
            self::TYPE_DISCOUNT     => 'Có giảm giá',
        );
    }
    public function getArrayTypeOrder() {// for form điều phối nghe 1900
        return array(
            self::DELIVERY_NOW    => 'Giao ngay',
            self::DELIVERY_BY_CAR => 'Xe tải giao',
            self::DELIVERY_THU_VO => 'Thu vỏ',
        );
    }
    public function getReasonFalse() {
        $aFalse = GasStoreCard::model()->getStatusFalse();
        return isset($aFalse[$this->reason_false]) ? $aFalse[$this->reason_false] : '';
    }
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName(){
        return '{{_gas_app_order}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules(){
        return array(
            array('customer_id', 'required', 'on'=>'ApiCreate', 'message' => "Khách hàng không hợp lệ"),
            array('customer_id', 'CheckQty', 'on'=>'ApiCreate'),
            array('type', 'required', 'on'=>'WebConfirm'),
            array('type', 'CheckWebConfirm', 'on'=>'WebConfirm'),
            array('type', 'CheckWebUpdate', 'on'=>'WebUpdate'),
            array('customer_contact', 'checkPhone', 'on'=>'ApiCreate'),
            array('id, code_no, customer_id, status, created_date', 'safe'),
            array('obj_id, obj_detail_id, type, type_customer, sale_id, uid_login, employee_maintain_id', 'safe'),
            array('webSetDebit, allowChangePrice, change_qty, type, agent_id, user_id_executive, store_card_id_gas, store_card_id_vo, driver_id, car_id, total_gas, total_gas_du, grand_total, json, json_info, note_customer, note_employee', 'safe'),
            array('receipts_no, uid_update_stt, delivery_timer, isLocationWrong, setStatusComplete, aSeriOld, customer_phone, customer_contact, obj_id_old, discount, pay_back, setStatusNew, isCustomerCreate, setNotDebit, isUpdateMoney, pay_direct, status_debit, paid_gas_remain, date_delivery, status_old, reason_false, date_from, date_to, province_id_agent, uid_confirm, b50, b45, b12, b6', 'safe'),        );
    }

    /**
     * @return array relational rules.
     */
    public function relations(){
        return array(
            'rUpdateBy' => array(self::BELONGS_TO, 'Users', 'update_by'),
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rUserLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rSale' => array(self::BELONGS_TO, 'Users', 'sale_id'),
            'rDriver' => array(self::BELONGS_TO, 'Users', 'driver_id'),
            'rEmployeeMaintain' => array(self::BELONGS_TO, 'Users', 'employee_maintain_id'),
            'rCar' => array(self::BELONGS_TO, 'Users', 'car_id'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rUidConfirm' => array(self::BELONGS_TO, 'Users', 'uid_confirm'),
            'rUserExecutive' => array(self::BELONGS_TO, 'Users', 'user_id_executive'),
            'rStorecardGas' => array(self::BELONGS_TO, 'GasStoreCard', 'store_card_id_gas'),
            'rStorecardVo' => array(self::BELONGS_TO, 'GasStoreCard', 'store_card_id_vo'),
            'rFile' => array(self::HAS_MANY, 'GasFile', 'belong_id',
                'on'=>'rFile.type='.  GasFile::TYPE_10_APP_BO_MOI,
                'order'=>'rFile.id ASC',
            ),
        );
    }

    public function CheckQty($attribute,$params)
    {
        $hasError = false;
        if(empty($this->b50) && empty($this->b45) && empty($this->b12) && empty($this->b6) ){
            $hasError = true;
        }
        if(isset($_POST['bo_moi_other_item']) && !empty($_POST['bo_moi_other_item'])){
            $hasError = false;
        }
        if($hasError){
            $this->addError('customer_id', 'Vui lòng nhập số lượng bình đặt hàng');
        }
    }
    public function CheckWebUpdate($attribute,$params)
    {
        if(!isset($_POST['materials_id']) || count($_POST['materials_id']) < 1 ){
            $this->addError('ext_materials_id', 'Vui lòng nhập vật tư');
        }
    }
    public function checkPhone($attribute,$params)
    {
        if(empty($this->customer_contact) || $this->isDieuPhoiCreate){
            return ;
        }
        /* 1. check chiều dài phone
         * 2. check phone phải là di động
         * 3. kiểm tra chỉ cho 1 số phone dc signup 1 ngày tối đa 5 lần 
         * 4. check phone có tài khoản trên hệ thống chưa
         */
        
        $phoneNumber = $this->customer_contact;// 1 + 2
        if(!UsersPhone::isValidCellPhone($phoneNumber)){
            $this->addError('customer_contact', "Số điện thoại $phoneNumber không phải là số di động hợp lệ");
        }
    }
    
    public function CheckWebConfirm($attribute,$params)
    {
        if($this->type == GasAppOrder::DELIVERY_BY_CAR && empty($this->user_id_executive)){
            $this->addError('user_id_executive', 'Vui lòng nhập '.$this->getAttributeLabel('user_id_executive'));
        }elseif($this->type == GasAppOrder::DELIVERY_NOW && empty($this->agent_id)){
            $this->addError('agent_id', 'Vui lòng nhập '.$this->getAttributeLabel('agent_id'));
        }
        
        if($this->change_qty && empty($this->b50) && empty($this->b45) && empty($this->b12) && empty($this->b6)){
            $this->addError('change_qty', 'Vui lòng nhập đúng số lượng');
        }
        $mCustomer = $this->rCustomer;
        if($mCustomer->is_maintain == UsersExtend::STORE_CARD_FOR_QUOTE){// Loại KH tiềm năng
            $this->addError('code_no', 'Khách hàng tiềm năng không thể tạo đơn hàng. Vui lòng liên hệ nhân viên kinh doanh để được hỗ trợ');
        }
    }
    
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'code_no' => 'Mã số',
            'customer_id' => 'Khách hàng',
            'status' => 'Trạng thái',
            'note_customer' => 'Chi chú KH',
            'note_employee' => 'Ghi chú NV',
            'created_date' => 'Ngày tạo',
            'agent_id' => 'Đại lý/người thực hiện',
            'type' => 'Phương thức',
            'user_id_executive' => 'Điều xe',
            'change_qty' => 'Đổi số lượng bình',
            'b50'   => 'Bình 50 kg',
            'b45'   => 'Bình 45 kg',
            'b12'   => 'Bình 12 kg',
            'b6'    => 'Bình 6 kg',
            'order_detail_note'    => 'Ghi chú đơn hàng',
            'order_detail_note_2'  => 'Ghi chú điều xe',
            'date_from'  => 'Từ ngày',
            'date_to'  => 'Đến ngày',
            'reason_false'  => 'Lý do hủy',
            'date_delivery'  => 'Ngày giao',
            'employee_maintain_id'  => 'Người giao',
            'ext_materials_id'  => 'Gas + vật tư',
            'total_gas'  => 'Tiền Gas',
            'total_gas_du'  => 'Gas Dư',
            'grand_total'  => 'Tổng thanh toán',
            'status_debit' => 'Thanh toán',
            'paid_gas_remain' => 'Chi Gas Dư',
            'pay_direct' => 'Thu tiền liền',
            'setNotDebit' => 'Remove nợ của đơn hàng',
            'isCustomerCreate' => 'KH đặt app',
            'uid_login' => 'Người tạo',
            'setStatusNew' => 'Cập nhật trạng thái mới cho đơn hàng',
            'pay_back' => 'Trừ tiền thiếu đầu vào',
            'sale_id' => 'Nhân viên kinh doanh',
            'discount' => 'Giảm giá',
            'allowChangePrice' => 'Sửa giá đơn hàng',
            'setStatusComplete' => 'Hoàn thành đơn hàng',
            'driver_id' => 'Tài xế',
            'car_id' => 'Số xe',
            'phu_xe_1' => 'Phụ xe 1',
            'webSetDebit' => 'Set đơn hàng nợ',
            'uid_confirm' => 'NV xác nhận App',
            'delivery_timer' => 'Hẹn giờ giao',
            'province_id_agent' => 'Tên tỉnh',
            'receipts_no' => 'Số biên bản giao nhận',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        $criteria=new CDbCriteria;
        $criteria->compare('t.status', $this->status);
        $criteria->compare('t.uid_login', $this->uid_login);
        $criteria->compare('t.code_no', trim($this->code_no));
        $criteria->compare('t.receipts_no', trim($this->receipts_no));
        $this->handleSearchDate($criteria);
        if($this->isLocationWrong == GasAppOrder::WRONG_LOCATION){
            $criteria->addCondition('t.reason_false=' . GasAppOrder::WRONG_LOCATION);
        }
        GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');
        $this->getCustomerNotSetLocation($criteria);
        $this->handleRole($cRole, $criteria);
        $this->handleProvince($criteria);
        $criteria->order = 't.id DESC';
        $_SESSION['data-excel'] = new CActiveDataProvider($this, array(
            'pagination'=>false,
            'criteria'=>$criteria,
        ));
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 10,
            ),
        ));
    }
    
    /**
     * @Author: DungNT Apr 14, 2017
     */
    public function handleRole($cRole, &$criteria) {
        $criteria->compare('t.customer_id', $this->customer_id);
        $criteria->compare('t.agent_id', $this->agent_id);
        $criteria->compare('t.sale_id', $this->sale_id);
        $criteria->compare('t.driver_id', $this->driver_id);
        $criteria->compare('t.car_id', $this->car_id);
        if(!empty($this->uid_confirm)){
            $criteria->addCondition('t.uid_confirm=' . $this->uid_confirm);
            $criteria->addCondition('t.uid_login_role=' . ROLE_CUSTOMER);
        }
        
        $mUser = new Users();
        $aRoleCallCenter = [ROLE_DIEU_PHOI, ROLE_CALL_CENTER];
        $deliveryNow        = GasAppOrder::DELIVERY_NOW;
        $deliveryByCar      = GasAppOrder::DELIVERY_BY_CAR;
        $statusNew          = GasAppOrder::STATUS_NEW;
        $roleCustomer       = ROLE_CUSTOMER;
        $cUid               = MyFormat::getCurrentUid();
        $cAgent             = MyFormat::getAgentId();
        if($cRole == ROLE_SUB_USER_AGENT && $cAgent != MyFormat::KHO_PHUOC_TAN){
            $criteria->addCondition('t.agent_id='.MyFormat::getAgentId());
        }elseif(in_array($cRole, $aRoleCallCenter)){
            if(!isset($_GET['is_confirm'])){
                $criteria->addCondition('t.status='.GasAppOrder::STATUS_NEW.' AND t.uid_login_role=' . ROLE_CUSTOMER);
            }elseif(isset($_GET['is_confirm']) && $_GET['is_confirm']==GasAppOrder::STATUS_CONFIRM){
//                $criteria->addCondition('t.status <> '.GasAppOrder::STATUS_NEW.' AND t.uid_login_role=' . ROLE_CUSTOMER);
//                $criteria->addCondition("t.status <> $statusNew AND ( t.type=$deliveryNow OR (t.type=$deliveryByCar AND t.uid_login_role=$roleCustomer))");
                $criteria->addCondition("t.status<>$statusNew");
            }
        }
        $this->handleSameCriteria($criteria);
        $this->handleStorecardNo($criteria);
        
        $session=Yii::app()->session;// DungNT Jul271 add for sale view Order of agent limit
        $aUidAllow = [GasConst::UID_SU_BH]; $sCondition = '';
        if(in_array($cUid, $aUidAllow) && isset($session['LIST_AGENT_OF_USER']) && count($session['LIST_AGENT_OF_USER'])){
            $sParamsIn = implode(',', $session['LIST_AGENT_OF_USER']);
            $sCondition = "OR t.agent_id IN ($sParamsIn)";
        }
        
        if($cUid == GasConst::UID_KIEN_NT || $cUid == GasConst::UID_HOAN_NH){
            $sUid = GasConst::UID_HOAN_NH.','.GasConst::UID_KIEN_NT;
            $criteria->addCondition("t.sale_id IN ($sUid)");
        }elseif(in_array($cRole, $mUser->ARR_ROLE_SALE_LIMIT)){
            $criteria->addCondition("t.sale_id=$cUid $sCondition");
        }
    }
    
    public function handleSameCriteria(&$criteria) {
        if($this->status_debit == GasAppOrder::DEBIT_YES){
            $criteria->addCondition('t.status_debit='.GasAppOrder::DEBIT_YES);
        }elseif($this->status_debit == GasAppOrder::DEBIT_NO){
            $criteria->addCondition('t.status_debit<>'.GasAppOrder::DEBIT_YES);
        }
        
        if($this->paid_gas_remain == GasConst::STATUS_YES){
            $criteria->addCondition('t.paid_gas_remain='.GasConst::STATUS_YES);
        }

        if($this->pay_direct){
            if($this->pay_direct == GasAppOrder::TYPE_DISCOUNT){
                $criteria->addCondition('t.discount>0');
            }else{
                $criteria->addCondition('t.pay_direct='.$this->pay_direct);
            }
        }
        if($this->isCustomerCreate){
            $criteria->addCondition('t.uid_login_role=' . ROLE_CUSTOMER);
        }
        
        if(is_array($this->type_customer) && count($this->type_customer)){
            $sParamsIn = implode(',', $this->type_customer);
            $criteria->addCondition('t.type_customer IN ('.$sParamsIn.')');
        }elseif(!empty($this->type_customer)){
            $criteria->addCondition('t.type_customer=' . $this->type_customer);
        }
        if(!empty($this->type)){
            $criteria->addCondition('t.type=' . $this->type);
        }
    }
    
    /** @Author: DungNT Sep 15, 2017
     *  @Todo: xử lý limit KH thông báo 95
     */
    public function handleLimitCustomer(&$criteria) {
        $mUser = new Users();
        if(!empty($this->customer_contact) && array_key_exists ( $this->customer_contact , $mUser->getArrayGroup() )){
//        $mUserif($this->customer_contact == Users::GROUP_CNDB){
            $aCustomerId = $this->getCustomerTB95();
            $sParamsIn = implode(',', $aCustomerId);
            if(!empty($sParamsIn)){
                $criteria->addCondition('t.customer_id IN ('.$sParamsIn.')');
            }
        }

        if($this->customer_phone == 1){// không lấy dữ liệu các kho
            $sParamsIn = implode(',', GasCheck::getAgentNotGentAuto());
            $criteria->addCondition('t.agent_id NOT IN ('.$sParamsIn.')');
        }
    }
    public function getCustomerTB95() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.first_char= '. $this->customer_contact);
        $this->getConditionBoMoi($criteria);
        return CHtml::listData(Users::model()->findAll($criteria), 'id', 'id');
    }
    public function getConditionBoMoi(&$criteria) {
        $aTypeBoMoi = array(STORE_CARD_KH_BINH_BO, STORE_CARD_KH_MOI);
        $sParamsIn = implode(',', $aTypeBoMoi);
        $criteria->addCondition("t.is_maintain IN ($sParamsIn)");
        $criteria->addCondition('t.role_id=' . ROLE_CUSTOMER);
    }
    
    public function getCustomerNotSetLocation(&$criteria) {
        if($this->isLocationWrong != GasAppOrder::WRONG_NOT_SET_CUSTOMER){
            return ;
        }
        $criteriaUser = new CDbCriteria();
        $criteriaUser->addCondition("t.slug='' OR t.slug IS NULL");
        $this->getConditionBoMoi($criteriaUser);
        $aUid = CHtml::listData(Users::model()->findAll($criteriaUser), 'id', 'id');
        $sParamsIn = implode(',', $aUid);
        if(!empty($aUid)){
            $criteria->addCondition('t.customer_id IN ('.$sParamsIn.')');
        }
    }
    
    /** @Author: DungNT Aug 04, 2018
     *  @Todo: xử lý search mã thẻ kho
     **/
    public function handleStorecardNo(&$criteria) {
        if(empty($this->store_card_id_gas)){
            return ;
        }
        $criteriaExt = new CDbCriteria();
        $criteriaExt->compare("t.store_card_no", $this->store_card_id_gas);
        $mStoreCard = GasStoreCard::model()->find($criteriaExt);
        if($mStoreCard){
            $criteria->addCondition("t.store_card_id_gas=$mStoreCard->id OR t.store_card_id_vo=$mStoreCard->id");
        }else{
            $criteria->addCondition('t.store_card_id_gas=-1');
        }
    }
    
    /**
     * @Author: DungNT May 06, 2017
     * @Todo: Search daily report
     */
    public function searchDaily()
    {
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        $criteria=new CDbCriteria;
        $criteria->compare('t.status',$this->status);
        $criteria->compare('t.customer_id',$this->customer_id);
        $criteria->compare('t.agent_id',$this->agent_id);
        $criteria->compare('t.employee_maintain_id',$this->employee_maintain_id);
        $criteria->compare('t.driver_id',$this->driver_id);
        $criteria->compare('t.car_id',$this->car_id);
        $criteria->compare('t.sale_id',$this->sale_id);
        $this->handleSameCriteria($criteria);
        $this->handleProvince($criteria);
        $this->handleLimitCustomer($criteria);
        $this->handleSearchDate($criteria);
        $sParamsIn = implode(',', $this->getAppStatusReport());
        $criteria->addCondition('t.status IN ('.$sParamsIn.')');
        $_SESSION['data-excel'] = new CActiveDataProvider($this, array(
            'pagination'=>false,
            'criteria'=>$criteria,
        ));
        
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 50,
            ),
        ));
    }
    
    public function handleSearchDate(&$criteria) {
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
            DateHelper::searchGreater($date_from, 'date_delivery_bigint', $criteria);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
            DateHelper::searchSmaller($date_to, 'date_delivery_bigint', $criteria);
        }
    }
    
    /** @Author: DungNT Sep 15, 2017
     *  @Todo: handle limit agent by province
     */
    public function handleProvince(&$criteria) {
        $mInventoryCustomer = new InventoryCustomer();
        $aAgentId           = $mInventoryCustomer->getAgentOfProvince($this->obj_id_old);
        $sAgentId           = implode(',', $aAgentId);
        $this->limitAgent($criteria, $sAgentId, 'agent_id');
    }
    
    public function limitAgent(&$criteria, $sParamsIn, $fieldName) {
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.$fieldName IN ($sParamsIn)");
        }
    }
    
    protected function beforeSave() {
        if($this->isNewRecord){
            $this->setCodeNo();
        }
        $this->date_delivery_bigint = strtotime($this->date_delivery);
        $this->setUserLoginRole();
//        $this->note_customer = InputHelper::removeScriptTag($this->note_customer);
//        $this->note_employee = InputHelper::removeScriptTag($this->note_employee);
        return parent::beforeSave();
    }
    
    public function setUserLoginRole() {
        if(!empty($this->uid_login_role)){
            return ;
        }
        if($this->rUserLogin){
            $this->uid_login_role = $this->rUserLogin->role_id;
        }
    }
    public function setCodeNo() {// Aug2817
        $prefix_code    = 'A'.date('y');
        $this->code_no  = $prefix_code.ActiveRecord::randString(6, GasConst::CODE_CHAR);
    }
    
    /**
     * @Author: DungNT Jan 11, 2017 -- handle listing api
     * @param: $apiController api controller 
     */
    public function handleApiList(&$result, $q) {
        // 1. get list order by user id
        $dataProvider   = $this->apiListing($q);
        $models         = $dataProvider->data;
        $CPagination    = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page']   = $CPagination->pageCount;
        if($this->mAppUserLogin->role_id == ROLE_DRIVER){
            $this->setListdataPhuXe();
        }
        
        if( $q->page >= $CPagination->pageCount ){
            $result['record'] = [];
        }else{
            foreach($models as $model){
                $model->mAppUserLogin       = $this->mAppUserLogin;// for canPickCancel
                $model->aPhuXe              = $this->aPhuXe;// for get string name PhuXe
                $model->apiStockList        = $this->apiStockList;
                $model->mapJsonFieldOneDecode('JSON_FIELD', 'json', 'baseArrayJsonDecode');
                $model->mapJsonFieldOneDecode('JSON_FIELD_INFO', 'json_info', 'baseArrayJsonDecodeV1');
//                $model->getCustomerInfo();// Feb 02, 2017 không lấy kiểu này nữa, mà đã set trong json
                $temp = [];
                $temp['id']                 = $model->id;
                $temp['code_no']            = $model->getCodeNoApp();
                $temp['date_delivery']      = $model->getDateDelivery();
                $temp['customer_name']      = $model->getAppTimeCreated().' '.$model->getDeliveryTimerGrid(true).$model->getCustomerName().$model->getNoteApp().$model->getNoteCustomer();
//                $temp['customer_name']      = $model->getCustomerName();
                $temp['customer_address']   = $model->getCustomerAddress();
                $temp['customer_phone']     = $model->getCustomerPhone();
//                $temp['grand_total']        = $model->getGrandTotal(true);
                $temp['grand_total']        = $model->getTotalGas(true);// May 01, 2017 lấy tổng tiền hiển thị, không trừ gas dư
                $temp['total_gas']          = $model->getTotalGas(true);
                $temp['total_gas_du']       = $model->getTotalGasDu(true);
                $temp['total_gas_du_kg']    = $model->getRemainKg();
                
                $temp['note_customer']      = $model->note_customer;
                $temp['note_employee']      = $model->note_employee;
                $temp['customer_id']        = $model->customer_id;
                
//                $temp['name_gas']           = $model->getJsonFieldOneDecode('name_gas', 'json_info', 'baseArrayJsonDecodeV1');
                $temp['name_gas']               = $model->getNameGasApp();// Không thể bỏ vì show bên KH <=> bị dư trên list nên hide đi Jul1017
                $temp['status_number']          = $model->getStatusAppNumber();
                $temp['created_date']           = $model->getCreatedDate();
                $temp['show_nhan_giao_hang']    = $model->canPick();
                $temp['show_huy_giao_hang']     = $model->canPickCancel();
                $temp['customer_name_chain']    = $model->getCustomerNameChain();
                $temp['app_status_text']        = $model->getAppStatusText();
                $this->handleMoreParam($temp, $model);
                $result['record'][]             = $temp;
            }
        }
    }
    
    /** @Author: DungNT Jun 30, 2018
     *  @Todo: set more param for api stock
     *  @param: $temp array params
     *  @param: $mAppOrder obj model GasAppOrder
     **/
    public function handleMoreParam(&$temp, $mAppOrder) {
        if($this->apiStockList != GasAppOrder::API_STOCK){
            return ;
        }
        $temp['type']        = $mAppOrder->getType();
    }   

    /**
    * @Author: DungNT Jan 11, 2017
    * @Todo: get listing Order
    * @param: $q object post params
    */
    public function apiListing($q) {
        $mUser                  = $this->mAppUserLogin;
        $this->qParam           = $q;
        $pageSize               = GasAppOrder::API_LISTING_ITEM_PER_PAGE;
        
        $mTransactionHistory = new TransactionHistory();
        $agent_id = 0; $cRole = $mUser->role_id;
        $criteria = new CDbCriteria();
//        set advance search by type in stock:  1: mới, 2: đã xuất
        $this->setMoreAdvance($q,$criteria);
        $this->handleMoreCondition($q, $criteria);
        
        $days_check = Yii::app()->params['DaysUpdateAppBoMoi'];// Apr 20, 2017 giữ lại notify của ĐP
//        if(!GasCheck::isServerLive()){
//            $days_check = 100;
//        }
        if($mUser->role_id == ROLE_CUSTOMER){
            $this->apiListingRoleCustomer($mUser, $criteria);
        }elseif($mUser->role_id == ROLE_DRIVER){
            $criteria->addCondition('t.driver_id=' . $mUser->id);
            $days_check = 10;
        }elseif(in_array($mUser->role_id, $mTransactionHistory->getRoleCanHandleHgd())){
            $criteria->addCondition('t.type='.GasAppOrder::DELIVERY_NOW);
//            $criteria->addCondition('t.employee_maintain_id=' . $mUser->id);
            $aAgent = Users::GetKeyInfo($mUser, UsersExtend::JSON_ARRAY_AGENT);
            $sParamsIn = implode(',', $aAgent);
            if($sParamsIn != ''){
                $agent_id = reset($aAgent);
                $criteria->addCondition(' (t.agent_id IN ('.$sParamsIn.') AND t.status='.GasAppOrder::STATUS_CONFIRM.') OR t.employee_maintain_id='.$mUser->id);
            }else{
                $criteria->addCondition('t.agent_id=1');
            }
            $this->handleLockTimeHighTraffic($q, $criteria, $agent_id);
        }elseif(in_array($mUser->id, GasConst::getUidKhoanOnlyHighPrice())){
            $mTransactionHistory->mAppUserLogin = $mUser;// Oct0101
            $mTransactionHistory->handleLimitByRole($q, $criteria);
        }elseif(in_array($cRole, GasConst::getRoleSaleOfCustomer())){
            $criteria->addCondition('t.sale_id='.$mUser->id);
        }elseif($mUser->role_id == ROLE_CRAFT_WAREHOUSE){
            $qCar = $qDriver = '';
            if(!empty($q->car_id)){
                $qCar = ' OR t.car_id = ' . $q->car_id;
            }
            if(!empty($q->driver_id)){
                $qDriver = ' OR t.driver_id = ' . $q->driver_id;
            }
            $criteria->addCondition('t.agent_id=' . $this->agent_id . $qCar.$qDriver);
        }

        if(isset($q->status) && $mUser->role_id != ROLE_CUSTOMER){
            $sParamsIn      = implode(',', $this->getAppStatusNew());
            $sParamsInDebit = implode(',', [GasAppOrder::DEBIT_YES]);
            if($q->status == Transaction::STATUS_NEW){
                $this->appLimitId($cRole, $criteria);
                if($mUser->role_id == ROLE_DIEU_PHOI){
                    $criteria->addCondition('t.status='.GasAppOrder::STATUS_NEW.' AND t.uid_login_role=' . ROLE_CUSTOMER);
                }
                
                $criteria->addCondition('t.status IN ('.$sParamsIn.')');
                /**  Apr 21, 2017 khi mà tất cả các đại lý chạy thì set status về hoàn thành rồi close dòng DATE_ADD này lại 
                 * xử lý ẩn các record cũ cho các đại lý mới chạy đi, không hiện lên nó lại bấm lung tung
                 */
                if(!empty($agent_id) && in_array($agent_id, UsersExtend::getAgentNotRunApp())){
                    $criteria->addCondition("DATE_ADD(t.date_delivery,INTERVAL $days_check DAY) >= CURDATE()");
                }else{
//                    $criteria->addCondition("DATE_ADD(t.date_delivery,INTERVAL $days_check DAY) >= CURDATE()");
//                    $criteria->addCondition("t.date_delivery = CURDATE()");
                }
            }elseif($q->status == Transaction::STATUS_COMPLETE){
                $pageSize = 20;
                $this->appLimitId($cRole, $criteria);
                $criteria->addCondition('t.status NOT IN ('.$sParamsIn.')');
                $criteria->addCondition('t.status_debit NOT IN ('.$sParamsInDebit.')');
            }elseif($q->status == GasAppOrder::APP_TAB_DEBIT){
                $criteria->addCondition('t.status_debit IN ('.$sParamsInDebit.')');
            }
        }
        
        $criteria->order = 't.date_delivery_bigint DESC';
        $dataProvider=new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize'      => $pageSize,
                'currentPage'   => (int)$q->page,
            ),
          ));
        return $dataProvider;
    }
    
    /** @Author: DungNT Dec 21, 2018
     *  @Todo: limit condition by role customer
     **/
    public function apiListingRoleCustomer($mUser, &$criteria) {
        if(isset($this->qParam->customer_chain_store_id) && !empty($this->qParam->customer_chain_store_id)){
            $criteria->addCondition('t.customer_id='. $this->qParam->customer_chain_store_id );// Jan1119 cho search đơn của chi nhánh trên app KH
        }elseif($mUser->is_maintain == UsersExtend::STORE_CARD_HEAD_QUATER){
            $mOneMany = new GasOneMany();
            $sParamsIn = implode(',', $mOneMany->getManyIdByTypeOrOneId(GasOneMany::TYPE_CUSTOMER_ACCOUNT_APP, $mUser->id));
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.customer_id IN ($sParamsIn)");
            }
        }else{
            $criteria->addCondition('t.parent_chain_store='.$mUser->id.' OR (t.customer_id=' . $mUser->id. ')');// Jul2417 load tất cả đơn hàng xuống app cho KH
        }
        
    }

    /** @Author: DungNT Jan 31, 2018
     *  @Todo: giới hạn id để giảm time query
     **/
    public function appLimitId($cRole, &$criteria) {
        return ;// Aug2118 check kiểu này gây chậm, dùng EXPLAIN SELECT SQL_NO_CACHE COUNT( * )
        if($cRole != ROLE_CUSTOMER && GasCheck::isServerLive()){
            $criteria->addCondition('t.id > 128505');// Jan1618 OK để giới hạn record giảm đc time của query không? bình thường đang hơn 2 second - sellId=448513-01/01/2018
        }
    }
    
    /** @Author: DungNT May 23, 2017
     * @Todo: xử lý add thêm điều kiện search apilisting
     */
    public function handleMoreCondition($q, &$criteria) {
        if( !empty($q->date_from) && !empty($q->date_to) ){
            $date_from  = MyFormat::dateDmyToYmdForAllIndexSearch($q->date_from);
            $date_to    = MyFormat::dateDmyToYmdForAllIndexSearch($q->date_to);
            if(isset($q->status) && $q->status == GasAppOrder::APP_TAB_DEBIT){// Apr2519 nếu ở tab nợ thì cho xem 1,5 tháng, hiện tại đang là 15 ngày
                $date_from = MyFormat::modifyDays($date_from, 40, '-');// 40 + 15 = 45 day
            }
            DateHelper::searchBetween($date_from, $date_to, 'date_delivery_bigint', $criteria, false);
        }
        if(!empty($q->customer_id)){
            $criteria->addCondition('t.customer_id='.$q->customer_id);
        }
    }

    /** @Author: DungNT Jan 15, 2018
     *  @Todo: Không hiển thị đơn bò mối trong 1 tiếng cao điểm trưa và tối. cho 3 khu vực chạy app TPHCM, ĐN, BÌnh Dương 
     *  @Param: $q param query từ aoo
     *  @Param: $agent_id agent hiện tại của user
     **/
    public function handleLockTimeHighTraffic($q, &$criteria, $agent_id) {
        $aAgentOpen = [
            103, // Đại lý Quận 8.1
            104, // Đại lý Quận 8.2
        ];

        if(!GasCheck::isServerLive() || in_array($agent_id, $aAgentOpen)){
            return ;
        }
        $aProvinceLock  = [GasProvince::TP_HCM];
        $aAgentLock     = [
            110, // Đại lý Thủ Dầu Một
            111, // Đại lý Bến Cát
            114, // Đại lý Long Bình Tân 
            117, // Đại lý Thống Nhất
            116, // Đại lý Bình Đa
        ];
        $mGasRemain = new GasRemain();
        $mGasRemain->getArrayAgentCar();
        
        $mAppCache  = new AppCache();
        $aAgent     = $mAppCache->getAgent();
        $cProvice   = isset($aAgent[$agent_id]) ? $aAgent[$agent_id]['province_id'] : 0;

        if( (in_array($cProvice, $aProvinceLock) && !array_key_exists($agent_id, $mGasRemain->getArrayAgentCar())) || in_array($agent_id, $aAgentLock)){
            $cHours     = date('H')*1;
            $aTimeLock  = [11, 17];// từ 11h -> 12h, 17h -> 18h
            if(in_array($cHours, $aTimeLock)){
                $criteria->addCondition('t.agent_id=1');
            }
        }
    }

    public function mapInfoCustomer() {
        $mCustomer = $this->rCustomer;
        if($mCustomer){
            if($mCustomer->is_maintain == UsersExtend::STORE_CARD_HEAD_QUATER){
                GasScheduleEmail::alertLockBookApp($mCustomer, $this);
                Logger::WriteLog('Chan Dat Hang: '.$mCustomer->first_name);
                throw new Exception('Không thể đặt hàng cho tài khoản trụ sở chính, vui lòng liên hệ nhân viên kinh doanh để được hỗ trợ');
            }
            $this->type_customer    = $mCustomer->is_maintain;
            $this->sale_id          = $mCustomer->sale_id;
            $this->customer_name    = $mCustomer->getFullName();
            $this->customer_address = $mCustomer->getAddress();
//            $this->customer_phone   = $mCustomer->phone;
            $this->customer_phone   = ''; // May 18, 2017  không set biến này nữa vì phone của customer quá dài mà không có ý nghĩa
        }
    }
    
    /**
     * @Author: DungNT Jan 11, 2017
     * @Todo: handle get post client app
     */
    public function handlePost($q) {
        $this->checkPriceNexMonth   = true;//Oct2717 khi tạo mới sẽ kiểm tra đơn hàng khác tháng
        $this->b50                  = trim(MyFormat::escapeValues($q->b50));
        $this->b45                  = trim(MyFormat::escapeValues($q->b45));
        $this->b12                  = trim(MyFormat::escapeValues($q->b12));
        $this->b6                   = trim(MyFormat::escapeValues($q->b6));
        $this->note_customer        = isset($q->note_customer) ? trim(MyFormat::escapeValues($q->note_customer)) : '';
        if(empty($this->customer_id)){
            $this->customer_id      = $this->mAppUserLogin->id;
        }
        if(empty($this->customer_contact)){
            $this->customer_contact = isset($q->customer_contact) ? $q->customer_contact : '';
        }
        $this->uid_login            = $this->mAppUserLogin->id;
        $this->uid_login_role       = $this->mAppUserLogin->role_id;
        $this->ip_address           = MyFormat::getIpUser();
        $this->validate();
        $this->handlePostChainStore($q);
        $this->checkChanHang();
    }
    
    /** @Author: DungNT Jun 28, 2017
     * @Todo: handle order chain store, đặt cho chuỗi cửa hàng
     */
    public function handlePostChainStore($q) {
        $is_chain_store = isset($q->is_chain_store) ? $q->is_chain_store : 0;
        if(empty($is_chain_store) || !isset($q->customer_chain_store_id)){
            return ;
        }
        if(empty($q->customer_chain_store_id)){
            $this->addError('customer_id', 'Bạn chưa chọn nơi giao');
            return ;
        }
        /* 1. xử lý set lại customer id là id người đặt put lên từ client
         * 2. xử lý set parent_chain_store
         */
        $this->customer_id          = $q->customer_chain_store_id;
        $this->parent_chain_store   = $this->uid_login;
    }
    
    /**
     * @Author: DungNT Jan 21, 2017
     * @Todo: lưu lại sl đặt ban đầu của KH để tính toán lại khi có thay đổi
     */
    public function setCustomerQtyRequest() {
        $this->customer_qty_request['b50']  = $this->b50;
        $this->customer_qty_request['b45']  = $this->b45;
        $this->customer_qty_request['b12']  = $this->b12;
        $this->customer_qty_request['b6']   = $this->b6;
    }
    public function getCustomerQtyRequest() {
        $this->b50  = isset($this->customer_qty_request['b50']) ? $this->customer_qty_request['b50'] : 0;
        $this->b45  = isset($this->customer_qty_request['b45']) ? $this->customer_qty_request['b45'] : 0;
        $this->b12  = isset($this->customer_qty_request['b12']) ? $this->customer_qty_request['b12'] : 0;
        $this->b6   = isset($this->customer_qty_request['b6']) ? $this->customer_qty_request['b6'] : 0;
    }
    
    /** @Author: DungNT Aug 03, 2017
     * @Todo: định dạng lại input gas dư cho xe tải
     * vd nhập: 123 => 12.3
     * vd nhập: 1234 => 123.4 còn các trường hợp khác giữ nguyên
     * @note: chỉ sử dụng cho gas dư ko dùng cho tiền
     */
    public function formatInputGasRemain($kg) {
        $kg = str_replace(',', '.', $kg);
        if(strpos($kg, '.') || strpos($kg, ',') || empty($kg) || $kg < 100){
            return $kg;
        }
        return $kg/10;
    }
    
    /** @Author: DungNT Jan 26, 2018
     *  @Todo: cập nhật lại date complete nếu done khác ngày đơn hàng
     * ANH DŨNG CÁC ĐƠN HÀNG ANH CẬP NHẬT LẠI GIÚP EM VÌ KHÁCH HÀNG ĐẶT HÀNG NGÀY TRƯỚC NGÀY SAU MỚI GIAO, ANH CẬP NHẬT LẠI CÔNG NỢ LÀ NGÀY HOÀN THÀNH ĐƠN HÀNG, CHỨ ANH ĐỪNG CẬP NHẬT NGÀY GIAO HÀNG NHA.
     * @note: cần theo dõi xem có gì sai để cập nhật lại
     **/
    public function changeCurrentDateComplete() {
        if($this->type == GasAppOrder::DELIVERY_BY_CAR){
            return ; // Add Feb2618 vì sai cho xe tải, khi mà mở ra để họ sửa. Theo dõi thêm xem có bị bất cập với ĐH giao ngay không
        }
        $today = date('Y-m-d');
        if($this->date_delivery != $today){
            $this->date_delivery = $today;
        }
    }
    
    /**
     * @Author: DungNT Jan 11, 2017 updatedriver 
     * @Todo: handle get post driver update
     */
    public function handlePostDriverUpdate($q) {
        try{
        $this->getInventoryAgent();
        $this->mapJsonFieldOneDecode('JSON_FIELD', 'json', 'baseArrayJsonDecode');
        $this->mapJsonFieldOneDecode('JSON_FIELD_INFO', 'json_info', 'baseArrayJsonDecodeV1');
        $this->setPriceCustomer();
        if(!$this->isWebUpdate){
            $this->setGasRemainStatus(GasRemain::STATUS_NOT_EXPORT);
        }
        
        if(!isset($q->order_detail) || !is_array($q->order_detail) || count($q->order_detail) < 1 ){
            $info = 'Empty: Đơn hàng không hợp lệ. Vui lòng nhập số lượng bình đặt hàng';
            $this->addError('customer_id', $info);
            $info = 'Important review BUG ====> ' . $info;
            Logger::WriteLog($info);
            return ;
        }
        /* xử lý update của gas và add new của vỏ
         * $this->aDetail[$mDetail->materials_id] = $mDetail;
         */
        $mAppCache          = new AppCache();
        $aMaterial          = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
        $this->aDetail      = [];
        $this->total_gas    = 0; $hasGas = false;

        $gasRemain = 0;
        foreach($q->order_detail as $objDetail){
            if(empty($objDetail->materials_id)){
                throw new Exception('Vật tư bị sai, vui lòng kiểm tra tên vật tư, nếu tên vật tư trống vui lòng xóa đi và thêm lại');
            }
            $mDetail = new GasAppOrderDetail();
            $mDetail->materials_type_id = $objDetail->materials_type_id;
            $mDetail->materials_id      = $objDetail->materials_id;
            $mDetail->materials_name    = isset($aMaterial[$mDetail->materials_id]) ? $aMaterial[$mDetail->materials_id]['name'] : '';
            $mDetail->unit              = isset($aMaterial[$mDetail->materials_id]) ? $aMaterial[$mDetail->materials_id]['unit'] : '';
            if(in_array($mDetail->materials_type_id, GasMaterialsType::$ARR_VO_45KG) && $objDetail->qty > 100){
                throw new Exception('Bạn nhập số lượng quá lớn '.$objDetail->qty);
            }
            $mDetail->qty               = $objDetail->qty;
            $mDetail->qty_real          = $objDetail->qty_real;
            if(empty($mDetail->qty) && empty($mDetail->qty_real)){
                throw new Exception('Chưa nhập số lượng đầy đủ, vui lòng kiểm tra lại, vật tư nào không có thì xóa đi');
            }
            if(!$this->isWebUpdate && $this->isOutOfStock($mDetail->qty_real, $mDetail->materials_type_id, $mDetail->materials_id)){
                throw new Exception("Kho của bạn đã hết $mDetail->materials_name. không thể hoàn thành đơn hàng");
            }
            
            $mDetail->price             = $objDetail->price;
            $mDetail->amount            = 0;
            $this->calcAmountGas($mDetail, $mDetail->qty_real);// for live
            $mDetail->seri              = trim($objDetail->seri);
            $mDetail->kg_empty          = $this->formatInputGasRemain($objDetail->kg_empty);
            $mDetail->kg_has_gas        = $this->formatInputGasRemain($objDetail->kg_has_gas);
            if(!empty($mDetail->kg_empty) && $mDetail->kg_empty > $mDetail->kg_has_gas){
                throw new Exception('Trọng lượng vỏ phải nhỏ hơn trọng lượng cân, vui lòng kiểm tra lại');
            }
            $gasRemain                  += ($mDetail->kg_has_gas - $mDetail->kg_empty);
            $this->total_gas            += $mDetail->amount;// 3. cộng tổng  tiền đơn hàng lại
            
            // vì bên dưới post lên cả gas lẫn vỏ, server phải xử lý tách riêng 2 loại ra
            if(in_array($mDetail->materials_type_id, CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT)){
                $this->aDetail[$mDetail->materials_id] = $mDetail;
                $this->aNameGas[] = $this->getStringNameGas($mDetail->materials_type_id, $mDetail->qty_real, $mDetail->materials_id);
            }elseif(in_array($mDetail->materials_type_id, CmsFormatter::$MATERIAL_TYPE_BINHBO_INPUT)){// vỏ
                $this->aDetailVo[] = $mDetail;// vì vỏ post lên là thương hiệu giống nhau, chỉ có khác seri nên lưu materials_id là chỉ số mảng không được
                $this->aNameVo[] = $this->getStringNameGas($mDetail->materials_type_id, $mDetail->qty, $mDetail->materials_id);
            }else{// vật tư val dây bếp
                $this->aDetail[$mDetail->materials_id] = $mDetail;
            }
            if($mDetail->materials_type_id == GasMaterialsType::MATERIAL_BINH_4KG){
                $this->hasGas6kg = true;
            }
            
            if(in_array($mDetail->materials_type_id, GasMaterialsType::getArrGasBB())){
                $hasGas = true;
            }
            
        }
        if(!$hasGas){// Aug2818 nếu ko có gas BB thì ko check STT
            $this->hasGas6kg = true;
        }
        $this->calcPriceKgSpecial();
        $priceKg            = isset($this->aInfoPrice[UsersPrice::PRICE_B_50]) ? $this->aInfoPrice[UsersPrice::PRICE_B_50] : 0;
        if($this->allowChangePrice){
            $priceKg        = $this->price_bb;// Sep2117 sửa đơn giá gas + gas dư
        }
        $this->pay_back     = isset($q->pay_back) ? MyFormat::removeComma($q->pay_back) : $this->pay_back;
        $this->discount     = isset($q->discount) ? MyFormat::removeCommaAndPoint($q->discount) : $this->discount;
        $this->pay_back_amount  = $this->pay_back * $priceKg;
        $this->total_gas_du = $gasRemain * $priceKg;
        $this->grand_total  = $this->total_gas - $this->total_gas_du - $this->pay_back_amount - $this->discount;
        
        $this->name_gas     = implode(', ', $this->aNameGas);
        $this->name_vo      = implode(', ', $this->aNameVo);
        $this->info_gas     = $this->aDetail;
        $this->info_vo      = $this->aDetailVo;
        $this->note_employee    = isset($q->note_employee) ? trim($q->note_employee) : $this->note_employee;
        $this->receipts_no      = isset($q->receipts_no) ? trim($q->receipts_no) : $this->receipts_no;
        $this->setJsonDataField();
        $this->setJsonDataField('JSON_FIELD_INFO', 'json_info');
        $this->validate();
        $this->checkReceiptsNo();
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage(), SpjError::SELL001);
        }
    }
    
    /** @Author: DungNT Dec 18, 2017
     *  @Todo: xử lý set lại giá BB cho KH chỉ lấy bình 12 để tính đúng cho công nợ gas dư
     **/
    public function calcPriceKgSpecial() {
        $mInventoryCustomer = new InventoryCustomer();
        if(!in_array($this->customer_id, $mInventoryCustomer->getCustomerPriceB12Kg())){
            return ;
        }
        $this->price_bb = round($this->price_b12 / 12);
    }
    
    /**
     * @Author: DungNT Jan 19, 2017
     * @Todo: build string name gas
     */
    public function getStringNameGas($materials_type_id, $qty, $materials_id) {
        $aMaterialsType = $this->getMaterialsType();
        $qty = $qty*1;
        $mAppCache = new AppCache();
        if(isset($aMaterialsType[$materials_type_id])){
            return $qty. ' '. (isset($aMaterialsType[$materials_type_id]) ? strtolower($aMaterialsType[$materials_type_id]) : '');
        }else{
            $aMaterial      = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
            return $qty. ' '. (isset($aMaterial[$materials_id]) ? $aMaterial[$materials_id]['name'] : '');
        }
    }
    
    /**
     * @Author: DungNT Jan 11, 2017
     * @Todo: build array de foreach 
     * @note: các biến $this->b50, $this->b45, $this->b12 đã được khởi tạo gán ở hàm handlePost
     */
    public function getArrayTypeQty() {
        return array(
            GasMaterialsType::MATERIAL_BINH_50KG    => $this->b50,
            GasMaterialsType::MATERIAL_BINH_45KG    => $this->b45,
            GasMaterialsType::MATERIAL_BINH_12KG    => $this->b12,
            GasMaterialsType::MATERIAL_BINH_6KG     => $this->b6,
        );
    }

    public function setProvinceIdAgent() {
        $mAppCache = new AppCache();
        $aAgent = $mAppCache->getAgent();
        $this->province_id_agent = isset($aAgent[$this->agent_id]) ? $aAgent[$this->agent_id]['province_id'] : 0;
    }
    /** @Author: DungNT Feb 02, 2017
     * @Todo: set một số biến name của User trong json
     */
    public function setJsonNameUser() {
//        $this->name_driver = $this->name_car = $this->name_employee_maintain = '';
        $mAppCache = new AppCache();
        if(!empty($this->driver_id)){
            $aModelUsers = $mAppCache->getListdataUserByRole(ROLE_DRIVER);
            $this->name_driver = isset($aModelUsers[$this->driver_id]) ? $aModelUsers[$this->driver_id] : '';
        }
        if(!empty($this->car_id)){
            $aModelUsers = $mAppCache->getListdataUserByRole(ROLE_CAR);
            $this->name_car = isset($aModelUsers[$this->car_id]) ? $aModelUsers[$this->car_id] : '';
        }
        if(!empty($this->employee_maintain_id)){
            $aModelUsers    = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MAINTAIN);
            $aModelUsers1   = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MARKET_DEVELOPMENT);
            $aModelUsers    = $aModelUsers + $aModelUsers1;
            $this->name_employee_maintain = isset($aModelUsers[$this->employee_maintain_id]) ? $aModelUsers[$this->employee_maintain_id] : '';
        }
    }
    
    /**
     * @Author: DungNT Jan 18, 2017
     * @Todo: lấy đl sẽ giao của KH, để tính được tồn kho và loại Gas sẽ giao cho KH
     */
    public function setAgent() {
        if($this->isDieuPhoiCreate){// Feb 24, 2017 không set dl nữa vì thẻ kho điều phối tạo đã set rồi
            return ;
        }
        $mCustomer = $this->rCustomer;
        if($mCustomer){
            UsersPhone::getDeliveryAgent($mCustomer);
            if(!empty($mCustomer->maintain_agent_id)){
                $this->agent_id = $mCustomer->maintain_agent_id;
            }else{
                $this->agent_id = $mCustomer->area_code_id;
            }
        }
    }
    
    public function setPriceCustomer() {
        if($this->type == GasAppOrder::DELIVERY_BY_CAR && $this->type_of_order_car == GasOrders::TYPE_AGENT){
            return ;
        }
        $this->initInfoPrice();
        $this->price_bb     = $this->aInfoPrice[UsersPrice::PRICE_B_50];
        $this->price_b12    = $this->aInfoPrice[UsersPrice::PRICE_B_12];
    }
    
    /**
     * @Author: DungNT Feb 22, 2017
     * @Todo: set type của order theo đơn hàng cuối của KH. const DELIVERY_NOW = 1; const DELIVERY_BY_CAR= 2;
     * @BIG_NOTE: chỗ này nếu KH đặt thì có vẻ hệ thống sẽ xác định không đúng nếu lấy đơn hàng cuối,
     * mà Chương sẽ xác nhận lại đơn hàng trên giao diện xem là Giao Ngay hay Mai Giao
     */
    public function setOrderType() {
        if($this->isDieuPhoiCreate){//Apr 27, 2017  điều phối tạo từ PM thì giao ngay luôn, ko nghĩ
            return ;
        }
        $mOrderLastest = $this->getRecordLastest();
        if($mOrderLastest){
            $this->type = $mOrderLastest->type;
        }
    }
    
    /**
     * @Author: DungNT Feb 22, 2017
     */
    public function getRecordLastest() {
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',', [GasAppOrder::STATUS_COMPPLETE, GasAppOrder::STATUS_CONFIRM]);
        $criteria->addCondition('t.customer_id=' . $this->customer_id ." AND t.status IN ($sParamsIn)");
        $criteria->order = "t.id DESC";
        $criteria->limit = 1;
        return self::model()->find($criteria);
    }
    
    /**
     * @Author: DungNT Jan 11, 2017
     * save new record KH create from app customer BoMoi
     * ở bước này chưa có tạo thẻ kho hay đơn hàng xe tải gì cả.
     * Sau khi confirm thì mới sinh ra
     */
    public function handleSave() {
        $this->setOrderType();
        $this->setCustomerQtyRequest();// tìm đl đang giao cho KH, hoặc gắn đl dc Sale set cho KH
        $this->mapInfoCustomer();// map some info customer
        $this->setAgent();// tìm đl đang giao cho KH, hoặc gắn đl dc Sale set cho KH
        $this->setProvinceIdAgent();
//        $this->setInfoTest();// for test
        $this->setDateDelivery();
        $this->setGasDetailToJson();// gắn gas và vỏ dc SETUP cho đl dựa theo tồn kho hiện tại của ĐL
        $this->save();
        $this->notifyDieuPhoi();
    }
    
    /**
     * @Author: DungNT Jan 31, 2017
     * @Todo: set array gas detail gas vào json cho đặt hàng của đại lý
     * vì với đơn hàng đại lý thì đã xác định được loại gas và qty rồi
     */
    public function setArrayOrderCarDetailToJson() {
        $mAppCache      = new AppCache();
        $aMaterial      = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
        foreach($this->aOrderCar as $mOrderCar){
            /* 1. chỗ này chỉ xử lý lấy lấy tên gas và qty cho vào json
            */
           $mDetail = new GasAppOrderDetail();
           $mOrderCar->getMaterialsOfAgent($mDetail);
           $mDetail->materials_type_id = isset($aMaterial[$mDetail->materials_id]) ? $aMaterial[$mDetail->materials_id]['materials_type_id'].'' : '';
           $mDetail->materials_name    = isset($aMaterial[$mDetail->materials_id]) ? $aMaterial[$mDetail->materials_id]['name'] : '';
           $mDetail->unit              = isset($aMaterial[$mDetail->materials_id]) ? $aMaterial[$mDetail->materials_id]['unit'] : '';
           $mDetail->price             = '0';// 2. xử lý lây giá của KH trong tháng
           $mDetail->amount            = '0';
           $mDetail->qty_real          = $mDetail->qty;
           $mDetail->seri              = '';
           $mDetail->kg_empty          = '';
           $mDetail->kg_has_gas        = '';
           $this->aDetail[$mDetail->materials_id] = $mDetail;

           $this->aNameGas[] =  $mDetail->qty. ' '. $mDetail->materials_name;
        }
        $this->total_gas    = 0;
        $this->getCustomerInfo();// fix Jul2317
        $this->customer_phone = '';
        $this->setSomeInfoJsonSame();
        // Feb 02, 2017 chưa xử lý lấy loại gas đặt theo tồn kho của Xưởng (kho)

    }
    
    /**
     * @Author: DungNT Jan 31, 2017
     * @Todo: gộp 1 số hàm sử dụng chung khi set json
     */
    public function setSomeInfoJsonSame() {
        $this->setJsonNameUser();
        $this->name_gas     = implode(', ', $this->aNameGas);
        $this->name_vo      = implode(', ', $this->aNameVo);
        $this->info_gas     = $this->aDetail;
        $this->info_vo      = $this->aDetailVo;
        $this->grand_total  = $this->total_gas.'';
        $this->setJsonDataField();
        $this->setJsonDataField('JSON_FIELD_INFO', 'json_info');
    }
    
    /** Feb 18, 2017 @Todo: khởi tạo biến giá của KH */
    public function initInfoPrice() {
        $aRes = [];
        $mUsersPrice =new UsersPrice();
        $mUsersPrice->initEmpptyPrice($aRes);
        if(count($this->aInfoPrice) > 0){
            return ;
        }elseif(empty($this->date_delivery)){
            return $this->aInfoPrice = $aRes;
        }
        $temp = explode('-', $this->date_delivery);
        if(!isset($temp[2])){
            return $this->aInfoPrice = $aRes;
        }
        $this->setMonthYear();
        $this->checkEmptySetupPrice();
        $this->setPriceOfNextMonth();
    }
    
    /** @Author: DungNT Jul 21, 2017
     * @Todo: set month year của đơn hàng
     */
    public function setMonthYear() {
        $temp = explode('-', $this->date_delivery);
        if(!isset($temp[2])){
            $this->c_month  = date('m');
            $this->c_year   = date('Y');
            $this->c_day    = date('d');
        }else{
            $this->c_month  = $temp[1];
            $this->c_year   = $temp[0];
            $this->c_day    = $temp[2];
        }
    }
    
    /**
     * @Author: DungNT Jun 03, 2017
     * @Todo: kiểm tra overide giá của KH, nếu chưa setup thì đưa về G5 và MG5
     */
    public function checkEmptySetupPrice() {
        $mUsersPrice    =new UsersPrice();
        $mGasPrice      =new GasPrice();
        $aInfoPrice     = $mUsersPrice->getPriceOfCustomer($this->c_month, $this->c_year, $this->customer_id);
        // Jun 03, 2017 xử lý set default giá G5 + MG5 cho KH chưa setup giá
        // Jun 10, 2017 đã qua 1 tuần cho NVKD setup giá, hệ thống tự app giá G5 cho các KH chưa setup giá
//        if($this->customer_id > 1053863 && ( empty($aInfoPrice[UsersPrice::PRICE_B_50]) || empty($aInfoPrice[UsersPrice::PRICE_B_12])) ){
        $gSetup = $mGasPrice->getArrayCodeGByMonthYear($this->c_month, $this->c_year);
        if( empty($aInfoPrice[UsersPrice::PRICE_B_12]) ){
            $aInfoPrice[UsersPrice::PRICE_B_12] = isset($gSetup['MG5']) ? $gSetup['MG5'] : 999999;
        }
        if( empty($aInfoPrice[UsersPrice::PRICE_B_50]) ){
            $aInfoPrice[UsersPrice::PRICE_B_50] = isset($gSetup['G5']) ? $gSetup['G5'] : 999999;
        }
        if( empty($aInfoPrice[UsersPrice::PRICE_B_4]) ){// DungNT Sep0219
            $aInfoPrice[UsersPrice::PRICE_B_4] = isset($gSetup['G5']) ? $gSetup['G5'] : 999999;
        }
        if( empty($aInfoPrice[UsersPrice::PRICE_B_6]) ){
            $aInfoPrice[UsersPrice::PRICE_B_6] = isset($gSetup['G5']) ? $gSetup['G5'] : 999999;
        }
        if( empty($aInfoPrice[UsersPrice::PRICE_OTHER_ITEM]) ){
            $aInfoPrice[UsersPrice::PRICE_OTHER_ITEM] = [];// DungNT Sep2319
        }
        $aInfoPrice['sPrice'] = 'Giá BB: '. ActiveRecord::formatCurrency($aInfoPrice[UsersPrice::PRICE_B_50]). ' - Giá B12: '. ActiveRecord::formatCurrency($aInfoPrice[UsersPrice::PRICE_B_12]);
        // end Jun 03, 2017 
        $this->aInfoPrice = $aInfoPrice;
    }
    
    
    /** @Author: DungNT Oct 01, 2017 
     *  @Todo: set giá = 0 đối với đơn hàng xe tải đặt cho tháng sau
     *  @Check_location:
     *  1. KH tạo trên app -- done - was test
     *  2. Điều phối tạo đơn hàng xe tải -- done - was test
     *  3. Điều phối update đơn hàng xe tải -- done - was test
     *  4. Điều phối tạo đơn xe tải từ Line CallCenter -- done - was test
     *  5. Điều phối Xác nhận đơn hàng xe tải -- done - was test
     * 
     */
    public function setPriceOfNextMonth() {
        if(!$this->checkPriceNexMonth){
            return ;
        }
//        if($this->type != GasAppOrder::DELIVERY_BY_CAR || $this->type_of_order_car != GasOrders::TYPE_DIEU_PHOI){
//            Logger::WriteLog('setPriceFirstDayOfMonth - Not Car');
//            return ;
//        }//  check cả đơn giao ngay mà đặt cho tháng sau => đều đưa giá về = 0
        if($this->c_month*1 == date('m')*1){// nếu là tháng hiện tại thì ko set = 0
            return ;
        }
        $aRes = [];
        $mUsersPrice =new UsersPrice();
        $mUsersPrice->initEmpptyPrice($aRes);
        $this->aInfoPrice = $aRes;
//        Logger::WriteLog('setPriceFirstDayOfMonth - first day of month '.$this->date_delivery);
    }
    
    /** @Author: DungNT Jan 18, 2017
     *  @Todo: only dev test
     */
    public function setInfoTest() {
        $this->driver_id    = 874257;//huydd -123123 only dev test
        $this->car_id       = 874262;//xe tai only dev test
//        $this->driver_id    = 121257;// localhost huydd -123123 only dev test
//        $this->car_id       = 345457;// localhost xe tai only dev test
//        $this->customer_id       = 163618;// localhost xe tai only dev test
//        $this->date_delivery     = date('Y-m-d');// localhost xe tai only dev test
        $this->name_driver  = $this->getDriver();// only dev test -- need remove
        $this->name_car     = $this->getCar();// only dev test -- need remove
        $this->name_employee_maintain = 'Nguyễn Thanh Em';// only dev test -- need remove
    }
    
    /**
     * @Author: DungNT Jan 19, 2017
     * @Todo: set các gas detail gas vào json
     * 1. Xác định loại gas dc setup cho customer
     * 2. Tính toán tồn kho xem loại Gas chính hay Phụ được xuất cho customer
     * 3. Nếu là Kho Xe Tải lấy gas thì phải lưu biến tạm những vật tư đã xuất trong ngày của Kho đó
     */
    public function setGasDetailToJson() {
        $this->total_gas = 0;
        $this->getInventoryAgent();
        $mAppCache      = new AppCache();
        $aMaterial      = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
//        $this->initInfoPrice();// Oct1517 bỏ hàm initInfoPrice() vì đã có trong initInfoPrice
        $this->setPriceCustomer();
        $this->initArrayAgentXeTai();
        $this->initCacheQtyExportInDay();
        foreach($this->getArrayTypeQty() as $materials_type_id => $qty){
            if(empty($qty)){
                continue ;
            }
            $this->mapInfoGas($aMaterial, $materials_type_id, $qty);
        }
        $this->mapInfoOtherItem();
        $this->setSomeInfoJsonSame();
        $this->setQtyExportInDayOfAgent();
    }

    /** @Author: DungNT Sep 02, 2019
     *  @Todo: Map other material not Gas to order detail, this is new function 
     *  @Note: Nếu có biến $_POST['bo_moi_other_item']; thì xử lý 
     *  $_POST['bo_moi_other_item'] is string format id - qty: 106-1,107-6,109-8,
     **/
    public function mapInfoOtherItem() {
        if(!isset($_POST['bo_moi_other_item']) || empty($_POST['bo_moi_other_item'])){
            return ;
        }
        $mAppCache  = new AppCache();
        $aMaterial  = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
        $temp       = trim($_POST['bo_moi_other_item'], ',');
        $aDataItem  = explode(',', $temp);
        foreach($aDataItem as $sInfoOne ): // $sInfoOne format: 109-8 material_id - qty
            $temp = explode('-', $sInfoOne);
            $materials_id       = $temp[0] * 1;
            $qty                = $temp[1] * 1;
            $materials_type_id  = isset($aMaterial[$materials_id]) ? $aMaterial[$materials_id]['materials_type_id'] : 0;
            $materials_name     = isset($aMaterial[$materials_id]) ? $aMaterial[$materials_id]['name'] : '';
            $unit               = isset($aMaterial[$materials_id]) ? $aMaterial[$materials_id]['unit'] : '';
        
            $mDetail = new GasAppOrderDetail();
            $mDetail->materials_type_id = ''.$materials_type_id; // 1. MATERIAL_TYPE_BINHBO_50
            $mDetail->materials_id      = $materials_id;
            $mDetail->materials_name    = $materials_name;
            $mDetail->unit              = $unit;
            $mDetail->qty               = $qty;
            $this->calcAmountGas($mDetail, $mDetail->qty);// for live
            $mDetail->qty_real          = $qty;
            $mDetail->seri              = '';
            $mDetail->kg_empty          = '';
            $mDetail->kg_has_gas        = '';
            if($this->typeStorecard == GasAppOrder::STORECARD_NORMAL){
                $this->aDetail[$mDetail->materials_id] = $mDetail;
                $this->total_gas        += $mDetail->amount;// 3. cộng tổng  tiền đơn hàng lại
                $this->aNameGas[]       = $this->getStringNameGas($materials_type_id, $qty, $mDetail->materials_id);
            }
        endforeach;
        
    }

    /**
     * @Author: DungNT Jan 11, 2017
     * driver update record from app customer BoMoi
     */
    public function handleSaveDriverUpdate() {
        $this->handleUpdateStorecard();
        $this->update();
        if($this->isWebUpdate){
//            if($this->isUpdateMoney){// Oct0517 luôn cập nhật tiền của đơn hàng mỗi khi sửa lại, vì sẽ gây bug nếu nhập thêm gas mà không bấm cập nhật
                $this->autoAddPayCashbook();
//            }
        }else{
            $this->autoAddPayCashbook();
        }
        $mAppOrderDetail = new GasAppOrderCDetailReal();
        $mAppOrderDetail->detailMakeNew($this);
    }
    
    /**
     * @Author: DungNT Jan 11, 2017
     * @Todo: xử lý get thông tin materials_type_id, $materials_id, $price của vt KH đặt
     */
    public function mapInfoGas($aMaterial, $materials_type_id, $qty) {
        /* 1. chỗ này phải xử lý lấy loại Gas KH sử dụng dc setup trong model Customer để đưa vào
         * 2. kiểm tra với tồn kho, nếu không có gas chính thì lấy gas phụ của loại VT đó -- done
         * 3. xử lý lây giá của KH trong tháng
         * 3. cộng tổng  tiền đơn hàng lại
         */
        $mDetail = new GasAppOrderDetail();
        $mDetail->materials_type_id = ''.$materials_type_id; // 1. MATERIAL_TYPE_BINHBO_50
        $mDetail->materials_id      = $this->getMaterialIdOfCustomer($qty, $materials_type_id, false);// để tạm để test. chỗ này xem thương hiệu chính còn tồn kho không, nếu ko thì xét thương hiệu phụ có tồn kho không
        $mDetail->materials_name    = isset($aMaterial[$mDetail->materials_id]) ? $aMaterial[$mDetail->materials_id]['name'] : '';
        $mDetail->unit              = isset($aMaterial[$mDetail->materials_id]) ? $aMaterial[$mDetail->materials_id]['unit'] : '';
        $mDetail->qty               = $qty;
        $this->calcAmountGas($mDetail, $mDetail->qty);// for live
        $mDetail->qty_real          = $qty;
        $mDetail->seri              = '';
        $mDetail->kg_empty          = '';
        $mDetail->kg_has_gas        = '';
        if($this->typeStorecard == GasAppOrder::STORECARD_NORMAL){
            $this->aDetail[$mDetail->materials_id] = $mDetail;
            $this->total_gas        += $mDetail->amount;// 3. cộng tổng  tiền đơn hàng lại
        }

        if($this->typeStorecard == GasAppOrder::STORECARD_NORMAL && in_array($materials_type_id, CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT)){
            $this->aNameGas[] = $this->getStringNameGas($materials_type_id, $qty, $mDetail->materials_id);
            $this->buildQtyExportInDayOfAgent($mDetail->materials_id, $qty);
        }
        
        $this->addVo($aMaterial, $materials_type_id, $qty);
    }
    
    /**
     * @Author: DungNT Jan 18, 2017
     * @Todo: thêm record vỏ vào mỗi record gas
     */
    public function addVo($aMaterial, $materials_type_id, $qty) {
        $mDetail = new GasAppOrderDetail();
        $mDetail->materials_id      = ''.$this->getMaterialIdOfCustomer($qty, $materials_type_id, true);// để tạm để test. chỗ này xem thương hiệu chính còn tồn kho không, nếu ko thì xét thương hiệu phụ có tồn kho không
        $mDetail->materials_type_id = isset($aMaterial[$mDetail->materials_id]) ? $aMaterial[$mDetail->materials_id]['materials_type_id'] : '';
        $mDetail->materials_name    = isset($aMaterial[$mDetail->materials_id]) ? $aMaterial[$mDetail->materials_id]['name'] : '';
        $mDetail->unit              = isset($aMaterial[$mDetail->materials_id]) ? $aMaterial[$mDetail->materials_id]['unit'] : '';
        $mDetail->qty               = $qty;
        $mDetail->price             = '0';
        $mDetail->amount            = '0';// vỏ giá = 0
        $mDetail->qty_real          = $qty;
        $mDetail->seri              = '';
        $mDetail->kg_empty          = '';
        $mDetail->kg_has_gas        = '';
        $this->aDetailVo[]  = $mDetail;
        $this->aNameVo[]    = $this->getStringNameGas($mDetail->materials_type_id, $qty, $mDetail->materials_id);
    }
    
    /**
     * @Author: DungNT Jan 11, 2017 - tính đơn giá và thành tiền
     * @Todo: tính toán số tiền gas của mỗi row, vì giá gas của mỗi KH là khác nhau, có người thì là kg, có người thì laf bình
     * @note phải truyển biến $qty riêng, không lấy qty của model $mDetail được 
     * @set_price: $mDetail->price
     */
    public function calcAmountGas(&$mDetail, $qty) {
        if($this->calcAmountGasWebChangePrice($mDetail, $qty)){
            return ;
        }
        $amount         = 0;
        $priceKg        = isset($this->aInfoPrice[UsersPrice::PRICE_B_50]) ? $this->aInfoPrice[UsersPrice::PRICE_B_50] : 0;
        $priceBinh12    = isset($this->aInfoPrice[UsersPrice::PRICE_B_12]) ? $this->aInfoPrice[UsersPrice::PRICE_B_12] : 0;
        $priceBinh4     = isset($this->aInfoPrice[UsersPrice::PRICE_B_4]) ? $this->aInfoPrice[UsersPrice::PRICE_B_4] : 0;
        $priceBinh6     = isset($this->aInfoPrice[UsersPrice::PRICE_B_6]) ? $this->aInfoPrice[UsersPrice::PRICE_B_6] : 0;

        switch ($mDetail->materials_type_id) {
            case GasMaterialsType::MATERIAL_BINH_50KG:
            case GasMaterialsType::MATERIAL_BINH_45KG:
//            case GasMaterialsType::MATERIAL_BINH_6KG:
                $amount = $qty * CmsFormatter::$MATERIAL_VALUE_KG[$mDetail->materials_type_id] * $priceKg;
                $mDetail->price = $priceKg;
                break;
            case GasMaterialsType::MATERIAL_BINH_12KG:
                if($priceBinh12 > UsersPrice::PRICE_MAX_BINH_BO){
                    $amount = $qty * $priceBinh12;
                }else{
                    $amount = $priceBinh12 * $qty * CmsFormatter::$MATERIAL_VALUE_KG[$mDetail->materials_type_id];
                }
                $mDetail->price = $priceBinh12;
                break;
            case GasMaterialsType::MATERIAL_BINH_4KG: // DungNT Sep0219
                if($priceBinh4 > UsersPrice::PRICE_MAX_BINH_BO){
                    $amount = $qty * $priceBinh4;
                }else{
                    $amount = $priceBinh4 * $qty * CmsFormatter::$MATERIAL_VALUE_KG[$mDetail->materials_type_id];
                }
                $mDetail->price = $priceBinh4;
                break;
            case GasMaterialsType::MATERIAL_BINH_6KG:
                if($priceBinh6 > UsersPrice::PRICE_MAX_BINH_BO){
                    $amount = $qty * $priceBinh6;
                }else{
                    $amount = $priceBinh6 * $qty * CmsFormatter::$MATERIAL_VALUE_KG[$mDetail->materials_type_id];
                }
                $mDetail->price = $priceBinh6;
                break;
            default:
                $this->calcAmountValDayBep($mDetail);
                $amount = $mDetail->amount;
                break;
        }
        $mDetail->amount = $amount;
    }
    
    /** @Author: DungNT Sep 13, 2017 - Fix allow Some User sửa lại đơn giá trên web  */
    public function calcAmountGasWebChangePrice(&$mDetail, $qty) {
        if(!$this->isWebUpdate || !$this->allowChangePrice){
            return false;
        }
        $amount         = 0;
        switch ($mDetail->materials_type_id) {
            case GasMaterialsType::MATERIAL_BINH_50KG:
            case GasMaterialsType::MATERIAL_BINH_45KG:
                $amount = $qty * CmsFormatter::$MATERIAL_VALUE_KG[$mDetail->materials_type_id] * $mDetail->price;
                $this->price_bb     = $mDetail->price;// Sep2117 đổi giá để tính đúng gas dư
                break;
            case GasMaterialsType::MATERIAL_BINH_6KG:
            case GasMaterialsType::MATERIAL_BINH_4KG:
            case GasMaterialsType::MATERIAL_BINH_12KG:
                if($mDetail->price > UsersPrice::PRICE_MAX_BINH_BO){
                    $amount = $qty * $mDetail->price;
                }else{
                    $amount = $mDetail->price * $qty * CmsFormatter::$MATERIAL_VALUE_KG[$mDetail->materials_type_id];
                }
                break;
            default:
                $amount = $mDetail->price * $qty;
                break;
        }
        
        $mDetail->amount = $amount;
        return true;
    }

    /** @Author: DungNT Aug 03, 2017
     * @Todo: calc giá val dây bếp
     */
    public function calcAmountValDayBep(&$mDetail) {
        if($this->type == GasAppOrder::DELIVERY_BY_CAR && $this->type_of_order_car == GasOrders::TYPE_AGENT){
            return ; // Aug1417 add không tính giá cho đơn hàng đại lý
        }
        if(isset($this->aInfoPrice[UsersPrice::PRICE_OTHER_ITEM][$mDetail->materials_id])){
            $this->calcAmountProductOther($mDetail);
            return;
        }
        $mAppCache      = new AppCache();
        $aDataCache     = $mAppCache->getPriceHgd(AppCache::PRICE_HGD);
        $priceHgd       = MyFunctionCustom::getHgdPriceSetup($aDataCache, $this->agent_id);
        $mSellDetail = new SellDetail('WindowCreate');
        $mSellDetail->materials_type_id = $mDetail->materials_type_id;
        $mSellDetail->materials_id      = $mDetail->materials_id;
        $mSellDetail->qty               = $mDetail->qty_real;
        $mSellDetail->price             = 0;
        $mSellDetail->amount            = 0;
        $mSellDetail->calcPriceVanDayV1($priceHgd);
        $mDetail->price     = $mSellDetail->price;
        $mDetail->amount    = $mSellDetail->amount;
    }
    
    /** @Author: DungNT Sep 24, 2019
     *  @Todo: calc price alcohol and some new product special
     *  Sep2419 xử lý tính giá cho vật tư cồn, sau này nếu có nước, gạo sẽ đc xử lý ở đây
     **/
    public function calcAmountProductOther(&$mDetail) {
        $mDetail->price     = $this->aInfoPrice[UsersPrice::PRICE_OTHER_ITEM][$mDetail->materials_id];
        $mDetail->amount    = $mDetail->price * $mDetail->qty_real;
    }
    
    /** @Author: DungNT May 13, 2017
     * @Todo: kho Phước Tân và Bến Cát chưa làm xác định tồn kho để set loại gas
     */
    public function getAgentNotCheckInventory() {
        return CmsFormatter::$LIST_WAREHOUSE_ID;
    }
    
    /** @Author: DungNT Jan 18, 2017
     * @Todo: tính toán xem đl còn tồn loại gas nào KH đặt
     */
    public function getMaterialIdOfCustomer($qty, $materials_type_id, $getVo) {
        if($getVo){
            $keyVo = $this->isGetGasSecond ? UsersRef::SECOND_VO : UsersRef::PRIMARY_VO;
            $this->isGetGasSecond = false;// sau khi get vỏ xong thì set lại bằng false để chạy cho vật tư gas sau
            return isset($this->aMaterialSetupOfCustomer[$materials_type_id][$keyVo]) ? $this->aMaterialSetupOfCustomer[$materials_type_id][$keyVo] : 0;
        }
        $materials_id_1 = isset($this->aMaterialSetupOfCustomer[$materials_type_id][UsersRef::PRIMARY_GAS]) ? $this->aMaterialSetupOfCustomer[$materials_type_id][UsersRef::PRIMARY_GAS] : 0;
        $materials_id_2 = isset($this->aMaterialSetupOfCustomer[$materials_type_id][UsersRef::SECOND_GAS]) ? $this->aMaterialSetupOfCustomer[$materials_type_id][UsersRef::SECOND_GAS] : 0;

        return $materials_id_1;// May 13, 2017 chắc để thế này, chưa care được đơn xe tải mà đại lý giao, lần sau lấy hàng nó lại check đơn hàng gần nhất đại lý giao thì không đúng lắm
        
        if(in_array($this->agent_id, $this->getAgentNotCheckInventory())){
            return $materials_id_1;// Feb 17, 2017 sẽ luôn lấy loại gas chính, cho nhân viên chỉnh sửa loại gas ở App Gas Service
            // không cần care tồn kho
        }

        // kiểm tra xem loại tồn kho của vật tư nào  còn thỏa mãn thì set vào
        if($this->isMaterialsValid($qty, $materials_type_id, $materials_id_1)){
            // chỗ này xử lý cho đặt hàng của xe tải nữa, set vào 1 biến tạm để lấy ra kiểm tra với những đăt hàng xe tải
            return $materials_id_1;
        }
        
        return 0;// May 13, 2017 kiểm tra tồn kho không có thì trả về rỗng, không có gas GN tự nhập vào
        /* Close May 13, 2017
        if($this->isMaterialsValid($qty, $materials_type_id, $materials_id_2)){
            $this->isGetGasSecond = true;// Lấy bình gas phụ cho KH, nhưng hiện tại đã bỏ không setup loại gas phụ của KH nữa
            return $materials_id_2;
        }
        return 0;
         */
    }
    
    /**
     * @Author: DungNT Jan 18, 2017
     * @Todo: get tồn kho của đại lý và vật tư gas + vỏ setup bán cho KH
     */
    public function getInventoryAgent() {
        $mAppCache              = new AppCache();
        $aInventory             = $mAppCache->getCacheInventoryAllAgent(AppCache::APP_AGENT_INVENTORY);
        $this->aInventoryAgent  = isset($aInventory[$this->agent_id]) ? $aInventory[$this->agent_id] : [];
        
        $mCustomer = $this->rCustomer;
        if(is_null($mCustomer->rUsersRef)){
            return ;
        }
        $mCustomer->rUsersRef->getGasBrandFromJson();
        
        if(empty($mCustomer->rUsersRef->gas_brand)){
            return ;
        }
        $this->aMaterialSetupOfCustomer = $mCustomer->rUsersRef->gas_brand;
    }
    
    /**
     * @Author: DungNT Feb 02, 2017
     * @Todo: tách hàm check vật tư hợp lệ còn tồn kho
     * @Param:      */
    public function isMaterialsValid($qty, $materials_type_id, $materials_id) {
        if(!isset($this->aInventoryAgent[$materials_type_id][$materials_id])){
            return false;
        }
        $qtyInventory = $this->aInventoryAgent[$materials_type_id][$materials_id];
        if(in_array($this->agent_id, $this->aAgentForCar)){
            $qtyInventory += $this->getQtyExportInDayOfAgent($materials_id);
        }
        
        if( $qtyInventory > 0 && $qtyInventory >= $qty ){
            // chỗ này xử lý cho đặt hàng của xe tải nữa, set vào 1 biến tạm để lấy ra kiểm tra với những đăt hàng xe  tải
            return true;
        }
        return false;
    }
    
    /** @Author: DungNT Apr 03, 2018
     *  @Todo: không cho xuất kho nếu kho hết hàng
     **/
    public function isOutOfStock($qty, $materials_type_id, $materials_id) {
        $aMaterialsTypeNotCheck = [GasMaterialsType::MATERIAL_VO_50, GasMaterialsType::MATERIAL_VO_45, GasMaterialsType::MATERIAL_VO_12, GasMaterialsType::MATERIAL_VO_6, GasMaterialsType::MATERIAL_VO_4];
        if($this->type == GasAppOrder::DELIVERY_BY_CAR || in_array($materials_type_id, $aMaterialsTypeNotCheck)){
            return false;
        }
        
        if(!isset($this->aInventoryAgent[$materials_type_id][$materials_id])){
            return true;
        }
        
        $qtyInventory = $this->aInventoryAgent[$materials_type_id][$materials_id];
        
        if( $qtyInventory > 0 && $qtyInventory >= $qty ){
            // chỗ này xử lý cho đặt hàng của xe tải nữa, set vào 1 biến tạm để lấy ra kiểm tra với những đăt hàng xe  tải
            return false;
        }
        return true;
    }
    
    /**
     * @Author: DungNT Feb 02, 2017
     * @Todo: mảng các đại lý mà xe tải lấy gas, sẽ tính tồn kho của đại lý theo sl tạm tính
     * vì các đơn hàng xe tải là chưa giao nên tồn kho ko đúng. mà phải cộng 
     * vd: xưởng đồng nai, Xưởng Bến Cát
     */
    public function initArrayAgentXeTai() {
        $this->aAgentForCar = [100];
    }
    
    /**
     * @Author: DungNT Feb 02, 2017
     * @Todo: get json decode sl một số vt xuất bán trong ngày của đl xe tải
     * vì các đơn hàng xe tải là chưa giao nên tồn kho ko đúng. mà phải cộng vào 1 biến tạm rồi lấy ra để so sánh 
     */
    public function initCacheQtyExportInDay() {
        // sẽ là json_decode
        $mAppCache = new AppCache();
        $this->aCacheQtyExportInDay = $mAppCache->getCacheDecode(AppCache::QTY_EXPORT_IN_DAY_OF_AGENT);
    }    
    /**
     * @Author: DungNT Feb 02, 2017
     * @Todo: get json decode sl vt xuất bán trong ngày của đl xe tải, đã đưa vào biến tạm rồi
     */
    public function getQtyExportInDayOfAgent($materials_id) {
        // sẽ là json_decode
        return isset($this->aCacheQtyExportInDay[$this->agent_id][$materials_id]) ? $this->aCacheQtyExportInDay[$this->agent_id][$materials_id] : 0;
    }
    /**
     * @Author: DungNT Feb 02, 2017
     * @Todo: set json_encode sl vt xuất bán trong ngày của đl xe tải, đưa vào biến tạm rồi
     */
    public function setQtyExportInDayOfAgent() {
        // sẽ là json_decode
        if(!in_array($this->agent_id, $this->aAgentForCar)){
            return ;
        }
        foreach($this->aQtyExportGas as $materials_id => $qty){
            if(!isset($this->aCacheQtyExportInDay[$this->agent_id][$materials_id])){
                $this->aCacheQtyExportInDay[$this->agent_id][$materials_id] = $qty;
            }else{
                $this->aCacheQtyExportInDay[$this->agent_id][$materials_id] += $qty;
            }
        }
        $value = json_encode($this->aCacheQtyExportInDay, JSON_UNESCAPED_UNICODE);
        $mAppCache = new AppCache();
        $mAppCache->setCache(AppCache::QTY_EXPORT_IN_DAY_OF_AGENT, $value, 360000);// 360000s = 100 hour
    }
    /**
     * @Author: DungNT Feb 02, 2017
     * @Todo: chỗ này sẽ build cho all agent, còn lúc set vào Cache thì mới check đại lý
     * đại lý nào xe tải giao cần check thì mới save vào Cache
     */
    public function buildQtyExportInDayOfAgent($materials_id, $qty) {
        // sẽ là json_decode
        if(!isset($this->aQtyExportGas[$materials_id])){
            $this->aQtyExportGas[$materials_id] = $qty;
        }else{
            $this->aQtyExportGas[$materials_id] += $qty;
        }
    }
    
    /**
     * @Author: DungNT Jan 11, 2017 app view 
     * format record view from app customer BoMoi
     */
    public function appView() {
        try {
        $aRes           = [];// order_view, order_edit
//        $mAppCache = new AppCache();
//        $aMaterial      = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
        $this->mapJsonFieldOneDecode('JSON_FIELD', 'json', 'baseArrayJsonDecode');
        $this->mapJsonFieldOneDecode('JSON_FIELD_INFO', 'json_info', 'baseArrayJsonDecodeV1');
        
//        $this->getCustomerInfo();// Feb 02, 2017 không lấy kiểu này nữa, mà đã set trong json
        $aRes['id']                 = $this->id;
        $aRes['code_no']            = $this->getCodeNo();
        $aRes['date_delivery']      = $this->getDateDelivery();
        $aRes['customer_id']        = $this->customer_id;
        $aRes['customer_name']      = $this->getCustomerName();
        $aRes['customer_address']   = $this->getCustomerAddress();
//        $aRes['customer_contact']   = $this->getCustomerPhone();
        $aRes['customer_contact']   = $this->getCustomerPhoneOrder();// chỉ show số nào gọi gas cho NVGN
        
        $aRes['total_gas']          = $this->getTotalGas(true);
        $aRes['grand_total']        = $this->getGrandTotal(true);
        $aRes['total_gas_du']       = $this->getTotalGasDuApp(true);
        $aRes['total_gas_du_kg']    = $this->getRemainKg();

        $aRes['note_customer']      = $this->getNoteCustomerApp();
        $aRes['note_employee']      = $this->getNoteEmployeeApp();
        $aRes['created_date']       = $this->getCreatedDate();
        $aRes['status_number']      = $this->getStatusAppNumber();
        
        $aRes['name_driver']        = $this->getNameDriverApp();// Feb 23, 2017 gom vào 1 biến trả xuống
        $aRes['name_car']           = $this->getNameCar();
        $aRes['name_employee_maintain'] = '';
//        $aRes['name_car']           = $this->getNameCar();
//        $aRes['name_employee_maintain'] = $this->getNameEmployeeMaintain();
        $aRes['info_gas']           = $this->getInfoGasVoArray($this->info_gas);
        $aRes['info_vo']            = $this->getArrayInfoVo();
        $aRes['allow_update']       = $this->employeeCanUpdate();
        $aRes['info_price']         = $this->getPriceString();
        $aRes['agent_name']         = $this->getAgent();
        $aRes['show_thu_tien']      = $this->getShowThuTien();
        $aRes['show_chi_gas_du']    = $this->getShowChiGasDu();
        $aRes['show_button_debit']  = $this->showDebit();
        $aRes['pay_direct']         = $this->getPayDirectText();
        
        $aRes['show_button_complete']   = $this->showAppComplete();
        $aRes['show_button_save']       = $this->showAppSave();
        $aRes['show_button_cancel']     = $this->showAppCancel();
        $aRes['show_pay_back']          = $this->showAppPayBack();
        $aRes['show_discount']          = $this->showAppDiscount();
        $aRes['pay_back']               = $this->pay_back*1;
        $aRes['discount']               = $this->discount*1;
        $aRes['list_image']             = GasFile::apiGetFileUpload($this->rFile);
        $aRes['show_confirm_stt']       = $this->showConfirmStt();
        $aRes['text_summary']           = $this->sTextSummary;
        $aRes['sale_name']              = $this->getSaleName();
        $aRes['sale_phone']             = $this->getSalePhone();
        $aRes['receipts_no']            = $this->getReceiptsNo();
        
        return $aRes;
        } catch (Exception $exc) {
            throw new Exception($exc->getMessage());
        }
    }
    
    /** @Author: DungNT Jan 24, 2017
     * @Todo: parse array not id => array để Trung parse
     * @param: $aValue is json_decode of: info_gas, info_vo đã test ok
     */
    public function getInfoGasVoArray($aValue) {
        if(empty($aValue) || !is_array($aValue)){
            return [];
        }
        $aRes = [];
        foreach($aValue as $item){
            $item['materials_id']   = $item['materials_id'].'';
            $aRes[]     = $item;
        }
        return $aRes;
    }
    
    /** @Author: DungNT Sep 16, 2017
     * @Todo: tự động xuất các bình gas dư của đại lý về xưởng
     * 1. kiểm tra đã có vỏ trong đơn hàng đại lý chưa, get all gas dư của đại lý đó mà chưa xuất
     * 2. đưa xuống view app Tài Xế
     * 3. xử lý update back lại trạng thái của tất cả gas dư
     *  - find all seri cũ set has_export = 0
     *  - set has_export cho seri moi = 1 -> done
     */
    public function getArrayInfoVo() {
        $aDataVo = $this->getInfoGasVoArray($this->info_vo);
//        return $aDataVo;// Oct1117 không trả tự động nữa, để tài xế tự nhập
        if($this->type != GasAppOrder::DELIVERY_BY_CAR 
                || $this->type_of_order_car != GasOrders::TYPE_AGENT
                || $this->status == GasAppOrder::STATUS_COMPPLETE
            ){
            return $aDataVo;
        }
        if(count($aDataVo) > 0){
            return $aDataVo;
        }
        return $this->getGasRemainAgent();
    }
    // build data info_vo from GasRemain
    public function getGasRemainAgent($needMore = []) {
        $aData  = [];
        $models = $this->getGasRemainWaitExport();
        $mAppCache      = new AppCache();
        $aMaterial      = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
        foreach($models as $item){
            $mDetail = new GasAppOrderDetail();
            $mDetail->materials_id      = $item->materials_id;
            $mDetail->materials_type_id = $item->materials_type_id;
            $mDetail->materials_name    = isset($aMaterial[$mDetail->materials_id]) ? $aMaterial[$mDetail->materials_id]['name'] : '';
            $mDetail->unit              = isset($aMaterial[$mDetail->materials_id]) ? $aMaterial[$mDetail->materials_id]['unit'] : '';
            $mDetail->price             = '0';// 2. xử lý lây giá của KH trong tháng
            $mDetail->amount            = '0';
            $mDetail->qty               = 1;
            $mDetail->qty_real          = 1;
            $mDetail->seri              = $item->seri;
            $mDetail->kg_empty          = $item->amount_empty*1;
            $mDetail->kg_has_gas        = $item->amount_has_gas*1;
            $aData[] = $mDetail;
        }
        $json = MyFormat::jsonEncode($aData);
        return json_decode($json, true);
    }

    /** @Author: DungNT Feb 19, 2018
     *  @Todo: sử dụng chung hàm GasRemain getInventory
     * 1. dùng ở AppView: getGasRemainAgent
     * 2. dùng khi Tài xế hoàn thành app đơn hàng Đại lý: employeeComplete
     **/
    public function getGasRemainWaitExport($needMore = []) {
        $mRemain = new GasRemain();
        $mRemain->agent_id = $this->customer_id;
        $models = $mRemain->getInventory();
        if(isset($needMore['GetKeepPrice'])){
            foreach($models as $item){
                $keySeri = $item->materials_id.'-'.$item->seri;
                $this->aGasRemainKeepPrice[$keySeri] = $item;
            }
        }
        return $models;
    }
    /** @Author: DungNT Feb 23, 2018
     *  @Todo: khởi tạo biến aGasRemainKeepPrice xử lý cho đơn hàng đại lý, 
     * giữ lại price gas dư, và thông tin customer_id để save vào customer_id_of_agent
     **/
    public function getGasRemainKeepPrice() {
        if(!$this->isOrderAgent()){
            return ;
        }
        $needMore['GetKeepPrice'] = 1;
        $this->getGasRemainWaitExport($needMore);
    }
    
    /** @Author: DungNT Sep 17, 2017
     *  @Todo: cập nhật trạng thái seri gas dư
     */
    public function setGasRemainStatus($has_export) {
        if($this->type != GasAppOrder::DELIVERY_BY_CAR || $this->type_of_order_car != GasOrders::TYPE_AGENT 
            || $this->status != GasAppOrder::STATUS_COMPPLETE){
            return ;
        }
        if(count($this->aSeriOld)){
            $mRemain = new GasRemain();
            $mRemain->agent_id = $this->customer_id;
            $mRemain->status_employee_debit = GasRemain::EMPLOYEE_DEBIT_YES;
            if($has_export == GasRemain::STATUS_EXPORT){// add Mar1118 xử lý biến cộng công nợ cho PVKH + tài xế
                $mRemain->status_employee_debit = GasRemain::EMPLOYEE_DEBIT_NO;
            }
            $mRemain->updateHasExport($this->aSeriOld, $has_export);
        }
    }
    
    /** @Author: DungNT Jul 01, 2018
     *  @Todo: xử lý khi update đơn Xe tải Kho Phước Tân trên web.
     *       Đối Tượng check: đơn xe tải Kho Phước Tân
     *  Tình huống: khi Xe Tải Thành xuất vỏ cho Bình Thạnh 1 (thanh đa) thì vỏ đó về BT1, nhưng khi web cập nhật lại thì vỏ đó lại về Kho PT
     * Gây dư thừa cho thủ kho Quảng cân lần 2.
     * @Xử_lý: find all vỏ của KH trong này hôm đó, không phải của kho Phước Tân đưa vào mảng để check
     * Trong lúc sinh Gas Remain khi web update thì check mảng này, nếu tồn tại rồi thì không insert new nữa
     **/
    public function getGasRemainCustomerExportOtherAgent() {
        if($this->type != GasAppOrder::DELIVERY_BY_CAR || $this->type_of_order_car != GasOrders::TYPE_DIEU_PHOI
            || $this->status != GasAppOrder::STATUS_COMPPLETE){
            return ;
        }
        $aAgentCheck = [MyFormat::KHO_PHUOC_TAN];
        if(!in_array($this->agent_id, $aAgentCheck) || !$this->isWebUpdate){
            return ;
        }
        
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.customer_id = ' . $this->customer_id . ' AND ' . 't.agent_id <> ' . $this->agent_id);
        $criteria->addCondition("t.date_input='$this->date_delivery'");
        $models = GasRemain::model()->findAll($criteria);
        foreach($models as $item){
            $keySeri = $item->materials_id.'-'.$item->seri;
            $this->aGasRemainExportOtherAgent[$keySeri] = $item;
        }
    }
    
    
    
    /** @Author: DungNT Mar 11, 2018
     *  @Todo: set status debit gas dư của PVKH, không còn nợ trên record chính nữa
     **/
    public function getPayDirectText() {
        if($this->mAppUserLogin->role_id == ROLE_CUSTOMER){
            return '';
        }elseif($this->mAppUserLogin->role_id == ROLE_DRIVER){
            return !empty($this->order_detail_note_2) ? 'Điều xe ghi chú: '.$this->order_detail_note_2 : '';
        }
        
        if($this->pay_direct == GasAppOrder::TYPE_PAY_DIRECT){
            return 'Thu tiền liền';
        }elseif($this->pay_direct == GasAppOrder::TYPE_THU_VO){
            return 'Thu vỏ';
        }
        return '';
    }
    public function getCodeNo() {
        return $this->code_no;
    }
    public function getCodeNoApp() {
        if($this->mAppUserLogin->role_id == ROLE_CRAFT_WAREHOUSE){
            $res = $this->code_no;
            if(!empty($this->getNameDriverApp())){
                $res .= ' - '.$this->getNameDriverApp().' - '.$this->getNameCar();
            }
            return $res;
        }
        if($this->mAppUserLogin->role_id != ROLE_DRIVER){
            return $this->code_no;
        }
        return $this->getPhuXe();
    }
    public function getPhuXeWeb() {
        if($this->type == GasAppOrder::DELIVERY_BY_CAR){
            return $this->getPhuXe();
        }
        return '';
    }
    public function getPhuXe() {
        $namePhuXe1 = $namePhuXe2 = '';
        if(isset($_SESSION['listdataPhuXe'][$this->phu_xe_1])){// for listing on web /admin/gasAppOrder/index
            $namePhuXe1 = $_SESSION['listdataPhuXe'][$this->phu_xe_1];
        }else{// for listing on app
            $namePhuXe1 = isset($this->aPhuXe[$this->phu_xe_1]) ? $this->aPhuXe[$this->phu_xe_1] : '';
        }
        if(isset($_SESSION['listdataPhuXe'][$this->phu_xe_2])){
            $namePhuXe2 = $_SESSION['listdataPhuXe'][$this->phu_xe_2];
        }else{
            $namePhuXe2 = isset($this->aPhuXe[$this->phu_xe_2]) ? ', '.$this->aPhuXe[$this->phu_xe_2] : '';
        }
        if(empty($namePhuXe1) && !empty($this->phu_xe_1)){
            $mUser      = Users::model()->findByPk($this->phu_xe_1);
            $namePhuXe1 = $mUser ? $mUser->first_name : '';
        }
        return $this->getRoadRoute(). ". Phụ xe: $namePhuXe1 $namePhuXe2";
    }
    
    public function getRoadRoute() {
        $mStorecard = new GasStoreCard();
        return $mStorecard->getRoadRoute();
    }
        
    public function getDateDelivery() {
        return MyFormat::dateConverYmdToDmy($this->date_delivery);
    }
    public function getDateDeliveryWeb() {
        $cRole  = MyFormat::getCurrentRoleId();
        $res    = $this->getDateDelivery();
        if($cRole == ROLE_DIEU_PHOI){
            $url = Yii::app()->createAbsoluteUrl('admin/gasAppOrder/view', ['id'=>$this->id]);
            $res = "<a href='$url' target='_blank'>$res</a>";
        }
        return $res;
    }
    /** @Author: DungNT Oct 12, 2018
     *  @Todo: only map data json field
     **/
    public function mapJsonOnly() {
        $this->mapJsonFieldOneDecode('JSON_FIELD', 'json', 'baseArrayJsonDecode');
        $this->mapJsonFieldOneDecode('JSON_FIELD_INFO', 'json_info', 'baseArrayJsonDecodeV1');
    }
    
    public function getNoteCustomer() {
        return !empty($this->note_customer) ? '. '.$this->note_customer : '';
    }
    public function getNoteCustomerApp() {
        if($this->mAppUserLogin->role_id == ROLE_CUSTOMER){
            return $this->note_customer;
        }
//        return $this->order_detail_note.' '.$this->order_detail_note_2;
        return $this->order_detail_note;
    }
    public function getNoteEmployee() {
        return $this->note_employee;
    }
    public function getNoteEmployeeApp() {
        if($this->mAppUserLogin->role_id == ROLE_CUSTOMER){
            return '';
        }
        return $this->note_employee;
    }
    public function getNoteEmployeeWebShow() {
        return nl2br($this->note_employee);
    }
    public function getNameEmployeeMaintain() {
        return $this->name_employee_maintain;
    }
    public function getNameDriver() {
        return $this->name_driver;
    }
    public function getNameDriverApp() {
        if($this->type == GasAppOrder::DELIVERY_NOW){
            return $this->getNameEmployeeMaintain();
        }
        if(!empty($this->getNameDriver())){
            return $this->getNameDriver();
        }
        return '';
    }
    public function getNameGasApp() {
        if($this->mAppUserLogin->role_id != ROLE_CUSTOMER){
            return '';
        }
        return $this->name_gas;
    }
    
    public function getNameCar() {
        return $this->name_car;
    }
    public function getAppTimeCreated() {
        if($this->mAppUserLogin->role_id == ROLE_CUSTOMER){
            return '';
        }
        return "[{$this->getCreatedDate('H:i')}]";
    }
    public function getCustomerName() {
        return $this->customer_name;
    }
    public function getCustomerNameChain() {
        if($this->mAppUserLogin->is_maintain == UsersExtend::STORE_CARD_HEAD_QUATER){
            return $this->customer_name;
        }
        if(empty($this->parent_chain_store) || $this->mAppUserLogin->role_id != ROLE_CUSTOMER){
            return '';
        }
        return $this->customer_name;
    }
    public function getCustomerAddress() {
        return $this->customer_address;
    }
    public function getCustomerPhone() {
        return $this->customer_phone;
    }
    public function getAppStatusText() {
        if($this->status == GasAppOrder::STATUS_NEW || $this->mAppUserLogin->role_id != ROLE_CUSTOMER){
            return '';
        }
        if($this->uid_login_role == ROLE_CUSTOMER){
            return $this->getStatusText().' '.$this->getUpdateTime();
        }
        return $this->getStatusText().' '.$this->getCreatedDate('H:i d/m/Y');
    }
    public function getCustomerPhoneOrderGrid() {
        $res = $this->getCustomerPhoneOrder();
        if(!empty($res)){
            return 'Số KH: '. $res;
        }
        return '';
    }
    public function getCustomerPhoneOrder() { // hiển thị số gọi gas cho đơn hàng
        if(empty($this->customer_contact)){
            $this->mapJsonFieldOneDecode('JSON_FIELD_INFO', 'json_info', 'baseArrayJsonDecodeV1');
        }
        return $this->customer_contact;// lấy số đt đặt của KH
//        if($this->uid_login_role == ROLE_CUSTOMER){
//            return $this->customer_contact;
//        }
//
//        if($this->type == GasAppOrder::DELIVERY_BY_CAR){
//            return $this->getCustomerPhone();
//        }
//        $note = $this->getNoteEmployee();
//        $findme   = 'ĐT:';
//        $pos    = strpos($note, $findme);
//        $phone  = substr($note, ($pos+5), 13);
//        return UsersPhone::formatPhoneOnly($phone);
    }
    
    /** @Author: DungNT Jul 09, 2017
     * @Todo: get note trên app list cho gọn
     */
    public function getNoteApp() {
        $note = $this->getNoteEmployee();
        if($note == 'null'){
            return $note='';
        }
        $note   = explode('.', $note);
        $note   = isset($note[0]) ? $note[0] : '';
        $findme = 'ĐT: '.  $this->getCustomerPhoneOrder();
        $note   = str_replace($findme, '', $note);
        return !empty($note) ? '. '.$note : '';
        
        $pos    = strpos($note, $findme);
        return @substr_replace($note, '', $pos , 16);// substr_replace gay error neu chuoi khong du 16 ky tu
    }
    public function getNoteOnly() {// Jul 1017 get note điều phối ghi chú
        $note = $this->getNoteApp();
        $note = explode('-', $note);
        return isset($note[1]) ? trim($note[1]) : '';
    }
    
    public function getOrderDetailNote() {
        return $this->order_detail_note;
    }
    public function getOrderDetailNote2() {
        return $this->order_detail_note_2;
    }
    public function getStatusAppNumber() {
        if($this->status == GasAppOrder::STATUS_CONFIRM){
            return GasAppOrder::STATUS_NEW.'';// ở dưới app không hiển thị STATUS_CONFIRM
        }
        return $this->status;
    }
    public function getStorecardNo() {
        $mStoreCardXuatBan  = $this->rStorecardGas;
        $mStoreCardNhapVo   = $this->rStorecardVo;
        $res = '';
        if($mStoreCardXuatBan){
            $res = '<br>'.$mStoreCardXuatBan->store_card_no;
        }
        if($mStoreCardNhapVo){
            $res .= '<br>'.$mStoreCardNhapVo->store_card_no;
        }
        return $res;
    }
    public function getInfoCreatedByOnGrid() {
        if($this->uid_login_role == ROLE_CUSTOMER){
            return '<br><b>APP KH</b>';
        }
        return $this->getUserInfo("rUserLogin", "first_name");
    }
    public function getPriceBB() {
        return $this->price_bb;
    }
    public function getPriceB12() {
        return $this->price_b12;
    }
    public function getPriceString() {
        $res   = '';
        $split = '';
        if(!empty($this->getPriceBB())){
            $res .= 'Giá BB: '.ActiveRecord::formatCurrency($this->getPriceBB());
            $split = ' - ';
        }
        if(!empty($this->getPriceB12())){
            $res .= $split.'Giá B12: '.ActiveRecord::formatCurrency($this->getPriceB12());
        }
        return $res;
    }

    public function getUserInfo($relation, $field_name='') {
        $mUser = $this->$relation;
        if($mUser){
            if($field_name != ''){
                return $mUser->$field_name;
            }
//            return "<b>".$mUser->code_bussiness."-".$mUser->first_name."</b><br>".$mUser->address."<br><b>Phone: </b>".$mUser->phone;
            return $mUser->code_bussiness."-".$mUser->first_name;
        }
        return '';
    }
    public function getCustomerInfo() {
        $mCustomer = $this->rCustomer;
        if($mCustomer){
            $this->customer_name    = $mCustomer->getFullName();
            $this->customer_address = $mCustomer->getAddress();
            $this->customer_phone   = $mCustomer->phone;
//            $this->customer_contact = '';
        }
    }
    public function getCustomerInfoV1() {
        $mCustomer = $this->rCustomer;
        $res = '';
        if($mCustomer){
            $res .= '<b>'.$mCustomer->code_bussiness .' - '. $mCustomer->getFullName().'</b><br>';
            $res .= $mCustomer->getAddress().'<br>';
//            $res .= $mCustomer->getPhoneShow();
            if(!empty($mCustomer->slug)){
                $res .= '<b>Tọa độ: '.$mCustomer->slug.'</b><br>';
            }
        }
        return $res;
    }
    public function getCustomer($field_name='') {
        $mUser = $this->rCustomer;
        if($mUser){
            if($field_name != ''){
                return $mUser->$field_name;
            }
//            return "<b>".$mUser->code_account."-".$mUser->first_name."</b><br>".$mUser->address."<br><b>Phone: </b>".$mUser->phone;
            return '<b>'.$mUser->code_bussiness.' - '.$mUser->first_name.'</b>';
        }
        return '';
    }
    public function getTotalGas($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->total_gas);
        }
        return $this->total_gas;
    }
    public function getTotalGasHavePromotion($format = false) {
        $total_gas = $this->total_gas - $this->discount;
        if($format){
            return ActiveRecord::formatCurrency($total_gas);
        }
        return $total_gas;
    }
    public function getTotalGasDu($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->total_gas_du);
        }
        return $this->total_gas_du;
    }
    public function getTotalGasDuApp($format = false) {
        $amount = $this->total_gas_du + $this->pay_back_amount;
        if($format){
            return ActiveRecord::formatCurrency($amount);
        }
        return $amount;
    }
    
    public function getGrandTotal($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->grand_total);
        }
        return $this->grand_total;
    }
    public function getPayBack($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->pay_back_amount);
        }
        return $this->pay_back_amount;
    }
    public function getDiscount($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->discount);
        }
        return $this->discount;
    }

    public function getDriver() {
        $mUser = $this->rDriver;
        if($mUser){
            return $mUser->getFullName();
        }
        return '';
    }
    public function getCar() {
        $mUser = $this->rCar;
        if($mUser){
            return $mUser->getFullName();
        }
        return '';
    }
    public function getAgent() {
        $mUser = $this->rAgent;
        if($mUser){
            return $mUser->getFullName();
        }
        return '';
    }
    public function getAgentName() {
//        $mAppCache = new AppCache();
//        $aAgent = $mAppCache->getAgent();
        $aAgent = CacheSession::getListAgent();
        return isset($aAgent[$this->agent_id]) ? $aAgent[$this->agent_id]['first_name'] : '';
    }
    
    public function getUpdateBy() {
        $mUser = $this->rUpdateBy;
        if($mUser){
            return $mUser->getFullName();
        }
        return '';
    }
    public function getConfirmBy() {
        $mUser = $this->rUidConfirm;
        if($mUser){
            return $mUser->getFullName();
        }
        return '';
    }
    public function getUpdateTime() {
        return MyFormat::dateConverYmdToDmy($this->update_time, ' H:i d/m/Y');
    }
    public function getAgentDisplayGrid() {
        if($this->type == self::DELIVERY_NOW){
            return $this->getAgentName();
        }
        $res = $this->getUserInfo('rUserExecutive', 'first_name').'<br><b>'.$this->getAgentName().'</b>';
        return $res;
    }
    public function getDisplayErrors() {
        $aStatusShow = [GasAppOrder::STATUS_NEW, GasAppOrder::STATUS_CANCEL];
        $res = '';
        if(!in_array($this->status, $aStatusShow) && $this->type == self::DELIVERY_BY_CAR && $this->type_of_order_car == GasOrders::TYPE_DIEU_PHOI && empty($this->obj_detail_id)){
            $res .= '<span class="high_light_tr"><br><b>Đơn hàng lỗi</b></span>';;
        }
        return $res;
    }
    
    public function getSaleName() {
        if($this->mAppUserLogin->role_id == ROLE_CUSTOMER ){
            if(!empty($this->employee_maintain_id) || !empty($this->driver_id)){
                return 'Nhân viên giao gas';
            }
            return '';
        }
        $mUser = $this->rSale;
        if($mUser){
            return 'Sale: '.$mUser->getFullName();
        }
        return '';
    }
    public function getSalePhone() {
        $mUser  = null;
        if($this->mAppUserLogin->role_id == ROLE_CUSTOMER ){
            if(!empty($this->employee_maintain_id)){
                $mUser = $this->rEmployeeMaintain;
            }elseif(!empty($this->driver_id)){
                $mUser = $this->rDriver;
            }
            if($mUser){
                return $mUser->phone;
            }
        }
        if(in_array($this->sale_id, GasLeave::getArraySaleIdCompany())){
            return '0909875420-0933037660';// số Đức + Hà
        }
        $mUser = $this->rSale;
        if($mUser){
            return $mUser->phone;
        }
        return '';
    }

    public function getReceiptsNo() {
        return !empty($this->receipts_no) ? $this->receipts_no : '';
    }

    public function getArrayScheduleCar() {
        return Users::getSelectByRoleNotRoleAgent(ROLE_SCHEDULE_CAR);
    }
    
    public function getViewDetail() {
        $res = $remain = '';
        $this->mapJsonFieldOneDecode('JSON_FIELD', 'json', 'baseArrayJsonDecode');
        $this->mapJsonFieldOneDecode('JSON_FIELD_INFO', 'json_info', 'baseArrayJsonDecodeV1');
        if(!empty($this->getNameDriver())){
            $res .= '<b>'.$this->getNameDriver().' - '.$this->getNameCar().'</b>';
        }
        if(!empty($this->getNameEmployeeMaintain())){
            $res .= '<b>'.$this->getNameEmployeeMaintain().'</b>';
        }
        if(!empty($this->total_gas_du)){
            $remain = '<b>  --- Gas Dư: '.$this->getTotalGasDu(true).'</b>';
        }
        if(!empty($this->getReceiptsNo())){
            $res .= '<b>  --- Số BB giao nhận: '.$this->getReceiptsNo().'</b>';
        }
        $res .= '<br>'.$this->getJsonFieldOneDecode('name_gas', 'json_info', 'baseArrayJsonDecodeV1');
        $res .= '<br>'.$this->getStringGas();
        $res .= '<br><b>Tổng TT: '.$this->getGrandTotal(true).'</b>'.$remain;
        $statusDebit = $this->getStatusDebit();
        if($this->pay_back > 0){
            $res .= '<br><b>Trừ tiền thiếu đầu vào: '.$this->getPayBack(true).'</b>';
        }
        if($this->discount > 0){
            $res .= '<br><b>Giảm giá: '.$this->getDiscount(true).'</b>';
        }
        if(!empty($statusDebit)){
            $res .= '<br><b>Trạng thái thanh toán: '.$this->getStatusDebit().'</b>';
        }

        return $res;
    }

    /**
     * @Author: DungNT Jan 19, 2017
     * @Todo: get string info gas
     */
    public function getStringGas() {
//        $aInfoGas = $this->getJsonDataField('info_gas');
        $aInfoGas = $this->getJsonFieldOneDecode('info_gas', 'json', 'baseArrayJsonDecode');
        $res = ''; $index = 1;
        if(!is_array($aInfoGas)){
            return '';
        }
        foreach($aInfoGas as $materials_id => $detail){
            if($index == 1){
                $res .= '<b>SL '.($detail['qty_real']*1).'</b>  '.$detail['materials_name']; 
            }else{
                $res .= '<br><b>SL '.($detail['qty_real']*1).'</b>  '.$detail['materials_name']; 
            }
            $index++;
        }
        return $res;
    }
    
    /**
     * @Author: DungNT May 09, 2017
     * @Todo: get số KG gas dư
     */
    public function getRemainKg() {
        $aInfoVo    = $this->getJsonFieldOneDecode('info_vo', 'json', 'baseArrayJsonDecode');
        $aInfoGas   = $this->getJsonFieldOneDecode('info_gas', 'json', 'baseArrayJsonDecode');
        $gasRemainKg = $gasTotalKg = 0;
        $gasValueWeight = CmsFormatter::$MATERIAL_VALUE_KG;
        foreach($aInfoVo as $detail){
            $remain = !empty($detail['kg_has_gas']) ? ($detail['kg_has_gas'] - $detail['kg_empty']) : 0;
            $gasRemainKg += $remain;
        }
        foreach($aInfoGas as $materials_id => $detail){
            $gasWeight      = (isset($gasValueWeight[$detail['materials_type_id']]) ? $gasValueWeight[$detail['materials_type_id']]*$detail['qty_real'] : 0 );
            $gasTotalKg     += $gasWeight;
        }
        if($gasTotalKg){
            $gasTotalKg -= $gasRemainKg;
        }
        $this->sTextSummary = 'Tổng gas: '.ActiveRecord::formatCurrency($gasTotalKg).' kg, thành tiền: '.$this->getGrandTotal(true);
        return $gasRemainKg.'';
    }
    
    
    /**
     * @Author: DungNT May 06, 2017 admin/gasAppOrder/daily report daily
     * @Todo: get string info gas + vỏ, và tách ra array để xử lý view
     */
    public function getStringGasReportDaily() {
        $this->mapJsonFieldOneDecode('JSON_FIELD_INFO', 'json_info', 'baseArrayJsonDecodeV1');
        $aInfoGas   = $this->getJsonFieldOneDecode('info_gas', 'json', 'baseArrayJsonDecode');
        $aInfoVo    = $this->getJsonFieldOneDecode('info_vo', 'json', 'baseArrayJsonDecode');
        $index = 1;$gasValueWeight = CmsFormatter::$MATERIAL_VALUE_KG;
        $sumQtyGas = $sumRemain = $sumWeightGas = $sumTotalGas = $sumQtyVo = 0;
// $aListGas=[], $aListGasQty=[], $aListPrice=[], $aListRemain=[], $totalWeightGas=[], $aListVo=[], $aListVoQty=[];
        
        foreach($aInfoGas as $materials_id => $detail){
            $price                  = $detail['price'] > 0 ? ActiveRecord::formatCurrencyRound($detail['price']) : '';
            $this->aListGas[]       = $detail['materials_name'];
            $this->aListGasQty[]    = $detail['qty_real'];
            $this->aListPrice[]     = $price;
            $this->aListGasId[]     = $materials_id;
            $this->aListGasAmount[] = $detail['amount'];
            $this->aListUnit[]      = $detail['unit'];
            
            if($index == 1){
                $this->sListGas     .= $detail['materials_name'];
                $this->sListGasQty  .= $detail['qty_real'];
                $this->sListPrice   .= $price;
            }else{
                $this->sListGas     .= '<br>'.$detail['materials_name'];
                $this->sListGasQty  .= '<br>'.$detail['qty_real'];
                $this->sListPrice   .= '<br>'.$price;
            }
            $gasWeight          = (isset($gasValueWeight[$detail['materials_type_id']]) ? $gasValueWeight[$detail['materials_type_id']]*$detail['qty_real'] : 0 );
            $this->aListGasWeight[] = $gasWeight;
            $this->sListQty     += $gasWeight;
            $index++;
            $sumQtyGas          += $detail['qty_real'];
//            $sumWeightGas   += $gasWeight;
        }

        $index = 1; $aTempVo = [];

        // xử lý cộng dồn vỏ lại để hiển thị cho đúng
        foreach($aInfoVo as $detail){
            $materials_id = $detail['materials_id'];
            $remain = !empty($detail['kg_has_gas']) ? ($detail['kg_has_gas'] - $detail['kg_empty']) : 0;
            $aTempVo[$materials_id]['materials_name'] = $detail['materials_name'];
            if(!isset($aTempVo[$materials_id]['qty'])){
                $aTempVo[$materials_id]['qty']      = $detail['qty'];
                $aTempVo[$materials_id]['remain']   = $remain;
            }else{
                $aTempVo[$materials_id]['qty']      += $detail['qty'];
                $aTempVo[$materials_id]['remain']   += $remain;
            }
        }
        
        foreach($aTempVo as $materials_id => $detail){
            $remain = $detail['remain'];
            if($index == 1){
                $remain += $this->pay_back;
            }
            $sumRemain += $remain;
            $remainTex =  ($remain > 0 ? ActiveRecord::formatCurrency($remain) : '');
            if($index == 1){
                $this->sListVo     .= $detail['materials_name'];
                $this->sListVoQty  .= $detail['qty'];
                $this->sListRemain .= $remainTex;
            }else{
                $this->sListVo     .= '<br>'.$detail['materials_name'];
                $this->sListVoQty  .= '<br>'.$detail['qty'];
                $this->sListRemain   .= '<br>'.$remainTex;
            }
            $this->aListVo[]        = $detail['materials_name'];
            $this->aListVoQty[]     = $detail['qty'];
            $index++;
            $sumQtyVo += $detail['qty'];
        }
//        foreach($aInfoVo as $materials_id => $detail){
//            $remain = !empty($detail['kg_has_gas']) ? ($detail['kg_has_gas'] - $detail['kg_empty']) : 0;
//            $sumRemain += $remain;
//            $remainTex =  ($remain > 0 ? ActiveRecord::formatCurrencyRound($remain) : '');
//            if($index == 1){
//                $this->sListVo     .= $detail['materials_name'];
//                $this->sListVoQty  .= $detail['qty'];
//                $this->sListRemain .= $remainTex;
//            }else{
//                $this->sListVo     .= '<br>'.$detail['materials_name'];
//                $this->sListVoQty  .= '<br>'.$detail['qty'];
//                $this->sListRemain   .= '<br>'.$remainTex;
//            }
//            $this->aListVo[]        = $detail['materials_name'];
//            $this->aListVoQty[]     = $detail['qty'];
//            $index++;
//            $sumQtyVo += $detail['qty'];
//        }
//        $sumRemain              += $this->pay_back; da cong ben tren roi
        $this->total_gas_du     += $this->pay_back_amount;
        $this->sListQty         -= $sumRemain;
        $sumWeightGas           = $this->sListQty;
        $this->totalRemain      = $sumRemain;
        $this->totalWeightGas   = $this->sListQty;
        $this->sListQty         = ActiveRecord::formatCurrencyRound($this->sListQty);
        
        $sDataHide = '<span class="sumQtyGas">'.$sumQtyGas.'</span>';
        $sDataHide .= '<span class="sumRemain">'.$sumRemain.'</span>';
        $sDataHide .= '<span class="sumWeightGas">'.$sumWeightGas.'</span>';
        $sDataHide .= '<span class="sumTotalGas">'.$this->getTotalGas().'</span>';
        $sDataHide .= '<span class="sumRemainAmount">'.$this->total_gas_du.'</span>';
        $sDataHide .= '<span class="sumQtyVo">'.$sumQtyVo.'</span>';
        $sDataHide = '<span class="display_none">'.$sDataHide.'</span>';
        
        return $this->sListGas.$sDataHide;
    }
    
    
    /**
     * @Author: DungNT Jan 11, 2017
     * @Todo: get valid model view
     * @param: $mUserLogin, $order_id
     */
    public static function getModelView($mUserLogin, $order_id) {
        try{
            return self::model()->findByPk($order_id);
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    
    /**
     * @Author: DungNT Jan 11, 2017
     * @Todo: get valid model update
     */
    public function getModelUpdate() {
        try{
            $criteria = new CDbCriteria();
            $criteria->addCondition('t.id=' . $this->id.' AND (t.driver_id='.$this->driver_id.' OR t.employee_maintain_id='.$this->employee_maintain_id.')' );
            return self::model()->find($criteria);
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    
    /**
     * @Author: DungNT Jan 19, 2017
     * khi điều phối confirm thì tạo thẻ kho và đơn hàng
     * @Todo: xử lý tạo thẻ kho cho ĐL, và thẻ kho + đơn hàng của xe tải
     */
    public function handleConfirm() {
        $this->isFromConfirm        = true;
        $this->checkPriceNexMonth   = true;//Oct2717 khi tạo mới sẽ kiểm tra đơn hàng khác tháng
        $this->mapJsonFieldOneDecode('JSON_FIELD', 'json', 'baseArrayJsonDecode');
        $this->mapJsonFieldOneDecode('JSON_FIELD_INFO', 'json_info', 'baseArrayJsonDecodeV1');
//        $aUpdate = array('type', 'agent_id');
        $this->checkIsChangeAgent();
        $this->setUidConfirm();
        $this->formatDateDelivery();
//        $this->mapJsonFieldOneDecode();// Jan 25, 2017  vì đã gọi ở controller rồi
        if( ($this->isChangeAgent || $this->change_qty) && $this->status == GasAppOrder::STATUS_CONFIRM){// nếu đổi agent thì tính toán lại gas theo tồn kho đl mới
//            $this->getCustomerQtyRequest();// Jan 25, 2017  vì đã gọi ở controller rồi
            $this->setCustomerQtyRequest();
            $this->setGasDetailToJson();// gắn gas và vỏ dc SETUP cho đl dựa theo tồn kho hiện tại của ĐL
        }
        
        $this->setUserUpdate();
        
//        $this->setDateDelivery();// May 30, 2017 mở bổ sung cho điều phối nhập date ở form web formatDateDelivery, nên close hàm này lại
        $this->setPriceCustomer();
        $this->deleteStorecard();// Aug1617 move ham nay len day. để xóa ở đây vì có thể trường hợp change type giữa Giao Ngay và Xe Tải
        if($this->type == GasAppOrder::DELIVERY_NOW && $this->status == GasAppOrder::STATUS_CONFIRM){// Giao ngay tạo thẻ kho cho đl luôn
//            $this->makeStorecard();// Jul, 2117 không sinh thẻ kho ở đây nữa, mà khi nào GN xác nhận ở App thì mới sinh ra thẻ kho
            $this->notifyEmployeeMaintain();
            /** @note_1: đã làm update back từ xuất bán thẻ kho vào lại AppOrder
            /** @note_2: không làm update back từ thu vỏ thẻ kho vào lại AppOrder - vì điều đó là không thể, bên thẻ kho lưu tổng sl vỏ, bên AppOrder lưu từng vỏ
            /** @note_3: done -- đang xử lý update back từ điều phối đặt hàng Xe tải lại AppOrder
             */
        }elseif($this->type == GasAppOrder::DELIVERY_BY_CAR && $this->status == GasAppOrder::STATUS_CONFIRM){// xử lý cho xe tải giao
            // insert vào đặt hàng hiện tại điều phối đang làm
//            $this->deleteStorecard();// để xóa ở đây vì có thể trường hợp change type giữa Giao Ngay và Xe Tải
            $this->makeOrderCar();
            /** @Task_1. DONE -- xử lý update lại driver_id khi điều xe chọn tài xế từ web cho AppOrder -- done
             *  - @function: GasOrdersDetail::updateCarNumber
             * @Task_1_1 DONE -- khi lên đơn hàng trên web thì sao? có tạo AppOrder không?
             * chưa làm cập nhật sửa sl từ đơn hàng Xe tải trên web sang AppOrder, không cho change customer_id khi update đơn hàng xe tái
             *   - @function: GasOrdersDetail::updateOrderDetail
             * * @Task_1_2: tách ra làm 3 phần: 
             *  - DONE -- record tạo từ AppOrder cho chỉnh sửa SL, xóa, view
             *  - DONE -- record tạo từ web - chỉnh sửa -> update back AppOrder -- done
             *  - DONE -- record tạo từ web - thêm mới -> update back AppOrder add new -- done
             *  - DONE -- record tạo từ web - Xóa -> update back AppOrder delete -- done
             *  - DONE -- record record đại lý tạo chưa xử lý add to AppOrder hoặc ngược lại ???
             *      + chỉ xử lý 1 chiều từ đại lý tạo Order rồi cập nhật sang AppOrder, xử lý cập nhật lại từ AppOrder chưa làm, vì cũng không cần thiết. 
             *      + Sẽ không cho confirm lại AppOrder của đại lý.
             * - DONE -- record GasOrders được tạo mới từ web xử lý cho cả điều phối và đại lý tạo -- done
             * 
             * @Task_2. - DONE -- xử lý update lại thực tế giao vào thẻ kho
             *   - @function: GasAppOrder->doUpdateBackStorecardDetail
             * @Task_3. các thẻ kho xe tải có thể phát sinh = cron vào lúc 11h tối như bây giờ - sau đó NV vào check lại, cái nào sai thì sửa lại.
             *   - @function: 
             * @Task_4. DONE -- xử lý tạo, update AppOrder của xe tải khi điều phối lên đơn hàng trên web hoặc c# -- done
             *   - @function: GasOrdersDetail::updateOrderDetail
             * @Task_5. DONE -- xử lý đơn hàng đại lý đặt, gom hết các row vào 1. rồi sinh 1 lần với 1 customer_id -- done
             *   - @function: GasOrdersDetail->handleFlowUpdateToAppOrder($mAppOrder);// Jan 31, 2017
             * @Task_6. DONE -- NOT TEST xử lý notify điều phối khi có đơn hàng tạo từ APP - có thể notify qua Phone
             *   - @function: 
             * @Task_7. DONE -- NOT TEST xử lý notify Giao Nhận khi có đơn hàng điều phối xác nhận từ APP - có thể notify qua Phone
             *   - @function: 
             * @Task_8. DONE -- xử lý kiểm tra tồn kho đại lý của Xe Tải, lấy loại gas hợp lệ 
             *   - @function: 
             * @Task_9. DONE -- xử lý set các biến name user trong json, 
             *   - @function: 
             * chắc phải viết hàm riêng chạy = cron để update name của Driver với Car
             * vì các biến đó được cập nhật bằng sql chứ không qua model
             * @note_Task_9: DONE -- không thể cập nhật = cron được mà phải cập nhật ngay tức khắc tên của lái xe vào model luôn
             * vì điều xe xếp xe ngay trong ngày đi luôn
             * @note_Task_9_1: DONE -- Ko the lam dc vi co Bug => Có thể cập nhật được name của Driver với Car trong 1 câu SQL khi update build theo model find all ra
             * rồi for model để build name rồi mới build sql update json
             * @Task_10. các thẻ kho xe tải có thể phát sinh = cron vào lúc 11h tối như bây giờ - sau đó NV vào check lại, cái nào sai thì sửa lại.
             */
        }
        
        $this->makeNotifyCustomer();
        $this->setJsonDataField();
        $this->setJsonDataField('JSON_FIELD_INFO', 'json_info');
        $this->orderCancel();
        $this->update();
        $this->mAppUserLogin = Users::model()->findByPk(MyFormat::getCurrentUid());
        $this->saveEvent(Transaction::EMPLOYEE_CONFIRM, 0);
    }
    
    /**
     * @Author: DungNT Apr 19, 2017
     * @Todo: notify cho KH khi điều phối xác nhận đơn hàng
     */
    public function makeNotifyCustomer() {
        if($this->status == GasAppOrder::STATUS_CONFIRM){
            $this->titleNotify = 'Đơn đặt hàng ngày '.$this->getDateDelivery().' của bạn đã được xác nhận';
        }elseif($this->status == GasAppOrder::STATUS_CANCEL){
            $this->titleNotify = 'Đơn đặt hàng ngày '.$this->getDateDelivery().' của bạn đã bị hủy bỏ';
        }
        $this->notifyPhoneAppCustomer();
    }
    
    public function setUidConfirm() {
        $this->uid_confirm  = MyFormat::getCurrentUid();
    }
    public function setUserUpdate() {
        $this->update_by    = MyFormat::getCurrentUid();
        $this->update_time  = date('Y-m-d H:i:s');
    }
    public function setDateDelivery() {
        if(!is_null($this->date_delivery) && $this->date_delivery != '0000-00-00' && !$this->isFromConfirm){
            return ;
        }
        $this->date_delivery = date('Y-m-d');
        if($this->type == GasAppOrder::DELIVERY_BY_CAR){
            $this->date_delivery = MyFormat::modifyDays($this->date_delivery, 1);
        }
    }
    public function formatDateDelivery() {
        if(strpos($this->date_delivery, '/')){
            $this->date_delivery = MyFormat::dateConverDmyToYmd($this->date_delivery);
        }
    }
    public function canConfirm() {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        if(!GasCheck::isServerLive()){
            return true;
        }
        $mAppCache = new AppCache();
        if(!in_array($cUid, $mAppCache->getArrayIdRole(ROLE_DIEU_PHOI)) || $this->uid_login_role != ROLE_CUSTOMER){
//        if(!in_array($cUid, $mAppCache->getArrayIdRole(ROLE_DIEU_PHOI))){
            return false;
        }

        $ok             = false;
        $aStatusAllow   = array(GasAppOrder::STATUS_NEW, GasAppOrder::STATUS_CONFIRM, GasAppOrder::STATUS_CANCEL);
        $aStatusAllowReconfirm   = array(GasAppOrder::STATUS_CONFIRM, GasAppOrder::STATUS_CANCEL);
        if($this->status == GasAppOrder::STATUS_NEW || empty($this->update_by) ||
            ( in_array($this->status, $aStatusAllowReconfirm) && $this->update_by == MyFormat::getCurrentUid())
        ){
            $ok = true;
        }
        $aRoleAllow = [ROLE_CUSTOMER];
        if(!in_array($this->status, $aStatusAllow) || !empty($this->driver_id)
            || !empty($this->employee_maintain_id)
//            || !in_array($this->uid_login_role, $aRoleAllow)// Feb 12, 2017 tạm close lại, sẽ open when live
        ){
            $ok = false;
        }
        
        return $ok;
    }
    public function orderCancel() {
        if($this->status == GasAppOrder::STATUS_CANCEL){
            $this->deleteStorecard();
            $this->deleteOrderDetail($this->obj_detail_id);// Jun 27, 2017 đáng lẽ xử lý deleteOrderDetail để xóa detail xe tải đi, nhưng hình như cho cập nhật detail đó - Nên không xóa nữa, mà xóa ở đây
        }
    }
    
    /**
     * @Author: DungNT Jan 19, 2017
     * @Todo: kiểm tra xem có đổi agent không, nếu đổi thì tính lại gas theo tồn kho của đại lý mới change
     */
    public function checkIsChangeAgent() {
        if($this->type == self::DELIVERY_NOW && $this->agent_id_old != $this->agent_id){
            $this->isChangeAgent = true;
            $this->setProvinceIdAgent();
        }elseif($this->type == self::DELIVERY_BY_CAR){
            $this->agent_id = $this->agent_id_old;
            if($this->type_of_order_car == GasOrders::TYPE_DIEU_PHOI){
                // Jul 31, 2017 xử lý set cứng cho agent_id của đơn hàng xe tải là của Kho Phước Tân Giao
                $this->agent_id = $this->carAgentId();
            }
        }
    }
    
    /**
     * @Author: DungNT Jan 21, 2017
     * @Todo: tạo thẻ kho detail khi điều phối tạo đơn hàng Giao Ngay từ PMBH
     * @note_Jul_2117: sẽ bỏ hàm này đi, khi nào NV hoàn thành dưới app thì sẽ sinh thẻ kho từ model GasAppOrder
     * chỗ này cứ để bình thường với đại lý không chạy app, Với đại lý chạy app thì khi bấm hoàn thành mới sinh thẻ kho
     * bấm hủy thì xóa thẻ kho bình thường
     */
    public function makeStorecardFromDieuPhoi($mStoreCard, $mStoreCardNhapVo) {
        if(is_null($this->id) || empty($this->id)){
            return ;
        }
        // Xuất bán detail 
        $this->makeParamsPostDetail($this->getJsonDataField('info_gas'));
        $this->store_card_id_gas        = $mStoreCard->id;
        GasStoreCard::saveStoreCardDetail($mStoreCard);
        
        // Thu vỏ detail 
        $this->makeParamsPostDetail($this->getJsonDataField('info_vo'));
        $this->store_card_id_vo         = $mStoreCardNhapVo->id;
        GasStoreCard::saveStoreCardDetail($mStoreCardNhapVo);
        $aUpdate = array('store_card_id_gas', 'store_card_id_vo');
        $this->update($aUpdate);
    }
    
    /** @Author: DungNT Jan 19, 2017
     * @Todo: đơn hàng này được book từ AppOrder 
     *  tạo thẻ kho cho đl trong trường hợp giao ngay, lúc này chưa có id Giao Nhận
     */
    public function makeStorecard() {
//        if($this->type != GasAppOrder::DELIVERY_NOW){
//            return ;// Jun 02, 2017 xử lý chỉ cho tạo thẻ kho với đơn hàng giao Ngay
//        }// Jul2117 allow tạo thẻ kho với đơn hàng xe tải
        if($this->status != GasAppOrder::STATUS_COMPPLETE){
            return ;// Oct1017 không sinh thẻ kho với đơn hàng chưa hoàn thành,  vì sẽ gây dư, ko đúng
        }
        $this->deleteStorecard();// Oct2017 đưa xuống dưới return, vì nếu đơn xe tải chưa hoàn thành mà sửa thì sẽ xóa mất trong List Điều Phối
        $this->setMonthYear();
//        throw new Exception("test month $this->c_month");
//        $mAppCache          = new AppCache();
//        $aAllAgent          = $mAppCache->getAgent();
//        $province_id_agent  = isset($aAllAgent[$this->agent_id]) ? $aAllAgent[$this->agent_id]['province_id'] : 0;
        // Xuất bán
        $mStoreCard                         = new GasStoreCard();
        $this->mapSameInfoStorecard($mStoreCard);
        $mStoreCard->type_store_card        = TYPE_STORE_CARD_EXPORT;
        $mStoreCard->type_in_out            = STORE_CARD_TYPE_3;
        $mUsersPrice =new UsersPrice();
        $aInfoPrice  = $mUsersPrice->getPriceOfCustomer($this->c_month, $this->c_year, $this->customer_id);
        $mStoreCard->note                   = $this->name_gas.'<br> '.$aInfoPrice['sPrice'];
        $this->getQtyReal   = true;
        $this->makeParamsPostDetail($this->getJsonDataField('info_gas'));
        $this->getQtyReal   = false;
        $mStoreCard->save();// save luôn để còn save detail
        $this->store_card_id_gas            = $mStoreCard->id;
        GasStoreCard::saveStoreCardDetail($mStoreCard);

        // Thu vỏ
        $mStoreCard                         = new GasStoreCard();
        $this->mapSameInfoStorecard($mStoreCard);
        $mStoreCard->type_store_card        = TYPE_STORE_CARD_IMPORT;
        $mStoreCard->type_in_out            = STORE_CARD_TYPE_5;
        $this->makeParamsPostDetail($this->getJsonDataField('info_vo'));
        $mStoreCard->save();// save luôn để còn save detail
        $this->store_card_id_vo             = $mStoreCard->id;
        GasStoreCard::saveStoreCardDetail($mStoreCard);
    }
    
    /** @Author: DungNT Jul 21, 2017
     *  @Todo: tự động tạo 4 thẻ kho khi xe tải giao hàng cho đại lý đặt: 2 Nhập xuất của xưởng, 2 nhập xuất của agent
     *  1. Xác định thẻ kho xe tải nào là của đại lý?
     *  1. Kho Phước Tân thẻ kho xuất gas nội bộ + Nhập vỏ nội bộ, 
     *  2. Tự động sinh Kèm theo thẻ kho nhập Gas nội bộ của Đại Lý Tương ứng, Kèm theo thẻ kho Xuất Vỏ nội bộ của Đại Lý Tương ứng
     *  @note: làm cờ phân biệt đơn hàng đại lý và đơn hàng bò mối cho xe tải giao
     */
    public function makeStorecardOrderAgent() {
        $this->deleteStorecard();// beforesave đã tính province_id_agent rồi
        // 1. xử lý tạo 2 thẻ nhập xuất cho Xưởng Phước Tân Của Xe tải trước
        // Xuất nội bộ Gas
        $mStoreCard                         = new GasStoreCard();
        $this->mapSameInfoStorecard($mStoreCard);
        $mStoreCard->type_store_card        = TYPE_STORE_CARD_EXPORT;
        $mStoreCard->type_in_out            = STORE_CARD_TYPE_4;// // Xuất nội bộ Gas
        $this->getQtyReal   = true;
        $this->makeParamsPostDetail($this->getJsonDataField('info_gas'));
        $this->getQtyReal   = false;
        $mStoreCard->save();// save luôn để còn save detail
        $this->store_card_id_gas            = $mStoreCard->id;
        GasStoreCard::saveStoreCardDetail($mStoreCard);
        // tạo thẻ kho nhập gas nội bộ tương ứng cho đại lý
        $mStorecardNew = $mStoreCard->makeSubRecord();
        $mStoreCard->makeSubRecordDetail($mStorecardNew);

        // Nhập nội bộ Vỏ
        $mStoreCard                         = new GasStoreCard();
        $this->mapSameInfoStorecard($mStoreCard);
        $mStoreCard->type_store_card        = TYPE_STORE_CARD_IMPORT;
        $mStoreCard->type_in_out            = STORE_CARD_TYPE_1;// nhập nội bộ
        $this->makeParamsPostDetail($this->getJsonDataField('info_vo'));
        $mStoreCard->save();// save luôn để còn save detail
        $this->store_card_id_vo             = $mStoreCard->id;
        GasStoreCard::saveStoreCardDetail($mStoreCard);
        // tạo thẻ kho Xuất Vỏ nội bộ tương ứng cho đại lý
        $mStorecardNew = $mStoreCard->makeSubRecord();
        $mStoreCard->makeSubRecordDetail($mStorecardNew);
    }
    
    /**
     * @Author: DungNT Jan 19, 2017
     * @Todo: map some info same của 2 thẻ kho gas và vỏ
     */
    public function mapSameInfoStorecard(&$mStoreCard) {
        if($this->uid_login_role == ROLE_CUSTOMER){// KH tạo
            $mStoreCard->uid_login          = $this->update_by;
        }else{// điều phối tạo
            $mStoreCard->uid_login          = $this->uid_login;
        }
        $mStoreCard->user_id_create         = $this->agent_id;
        $mStoreCard->customer_id            = $this->customer_id;
        $mStoreCard->date_delivery          = $this->date_delivery;
        $mStoreCard->type_user              = $this->type_customer;
//        $mStoreCard->province_id_agent  = $province_id_agent;// đã dc lấy ở before save
        $mStoreCard->pay_now                = GasStoreCard::CREATE_AUTO_FROM_APP_ORDER;
//      Aug0317 remove this line  $mStoreCard->phu_xe_2               = GasStoreCardApi::IS_ADMIN_GEN_FROM_ORDER;
        
        $mStoreCard->employee_maintain_id    = $this->employee_maintain_id;
        $mStoreCard->driver_id_1             = $this->driver_id;
        $mStoreCard->car_id                  = $this->car_id;
        $mStoreCard->road_route              = $this->road_route;// add Aug0317
        $mStoreCard->phu_xe_1                = $this->phu_xe_1;
        $mStoreCard->phu_xe_2                = $this->phu_xe_2;
        
        $mStoreCard->buildStoreCardNo();
        $this->setInfoOrderAgent($mStoreCard);
    }
    
    /** @Author: DungNT Jul 23, 2017
     * @Todo: set lại info thẻ kho đơn hàng xe tải giao cho đại lý
     */
    public function setInfoOrderAgent(&$mStoreCard) {
        if($this->type != GasAppOrder::DELIVERY_BY_CAR || $this->type_of_order_car != GasOrders::TYPE_AGENT){
            return ;
        }
        $mStoreCard->user_id_create         = $this->carAgentId();
        $mStoreCard->type_user              = CUSTOMER_IS_AGENT;
        $mStoreCard->receivables_customer   = GasStoreCardApi::IS_ORDER_AGENT_CAR;
        // Oct0917 không lưu vào phu_xe_2 mà lưu vào receivables_customer, vì phụ xe 2 có sử dụng thực tế cho báo cáo
    }
    
    /**
     * @Author: DungNT Jan 19, 2017
     * @Todo: init param $_POST['materials_id'] và $_POST['materials_qty']
     * @param: $aDetailInfo is $this->getJsonDataField('info_gas');
     * xử lý cộng dồn với thu vỏ, còn các loại khác thì gắn biến bình thường
     */
    public function makeParamsPostDetail($aDetailInfo) {
        $aMaterialsId   = $aMaterialsQty  = $aMaterialsSum  = [];
        $nameQty        = $this->getQtyReal ? 'qty_real':'qty';
        foreach($aDetailInfo as $aInfo){
            $aMaterialsId[]     = $aInfo['materials_id'];
            $aMaterialsQty[]    = $aInfo[$nameQty];
            if(!isset($aMaterialsSum[$aInfo['materials_id']])){
                $aMaterialsSum[$aInfo['materials_id']] = $aInfo[$nameQty];
            }else{
                $aMaterialsSum[$aInfo['materials_id']] += $aInfo[$nameQty];
            }
        }
        
        if($this->getQtyVo){// Feb 02, 2017 nếu là lấy vỏ thì cộng dồn lại theo loại vỏ
            $aId = $aQty = [] ;
            foreach ($aMaterialsSum as $materials_id => $qty){
                $aId[]  = $materials_id;
                $aQty[] = $qty;
            }
            $_POST['materials_id']  = $aId;
            $_POST['materials_qty'] = $aQty;
        }else{
            $_POST['materials_id']  = $aMaterialsId;
            $_POST['materials_qty'] = $aMaterialsQty;
        }
    }
    
    /**
     * @Author: DungNT Feb 19, 2017
     * @Todo: Cập nhật lại thẻ kho khi Giao nhận hoặc tài xế cập nhật thực tế giao và sl vỏ + gas dư
     * Có thể Đơn Hàng của Tài Xế sẽ không cập nhật luôn, sẽ sinh tự động = cron
     */
    public function handleUpdateStorecard() {
//        if(!$this->isWebUpdate && $this->status != GasAppOrder::STATUS_COMPPLETE){
        if($this->status != GasAppOrder::STATUS_COMPPLETE){// Dec1218 fix khi update web tự sinh phiếu => 100% khi hoàn thành mới sinh lại thẻ kho
            return ;// Sep2917 khong tao the kho khi chua hoan thanh
        }
        if($this->type == GasAppOrder::DELIVERY_NOW){// Giao ngay tạo thẻ kho cho đl luôn
//            $this->doUpdateBackStorecardDetail();// không thể change customer theo hàm này được,
            $this->makeStorecard();// add Jul2117
        }elseif($this->type == GasAppOrder::DELIVERY_BY_CAR){
            // không xử lý ở đây khi tài xế cập nhật vỏ, mà sẽ sinh = cron
            /* Jul 20, 2017 
             * 1/ xử lý sinh thẻ kho cho bò mối
             * 2/ xử lý sinh thẻ kho  đại lý đặt
             */
            if($this->type_of_order_car == GasOrders::TYPE_DIEU_PHOI){
                // Jul 31, 2017 xử lý set cứng cho agent_id của đơn hàng xe tải là của Kho Phước Tân Giao
                $this->agent_id = $this->carAgentId();
//                if($this->driver_id == 121254){// Jul3117 only dev fix bug
//                    Logger::WriteLog("Fix bug  $this->id");
//                    $this->agent_id = 106;// BT 1 fix bug
//                }
                $this->makeStorecard();// add Jul2117
            }elseif($this->type_of_order_car == GasOrders::TYPE_AGENT){
                $this->makeStorecardOrderAgent();// add Jul2317
            }else{
                throw new Exception($this->id. ' - Dữ liệu xảy ra lỗi, liên hệ Dũng IT để xử lý: 01684331552');
            }
        }
    }
    
    /**
     * @Author: DungNT Feb 19, 2017
     * @Todo: Cập nhật lại thẻ kho khi Giao nhận hoặc tài xế cập nhật thực tế giao và sl vỏ + gas dư
     * Có thể Đơn Hàng của Tài Xế sẽ không cập nhật luôn, sẽ sinh tự động = cron
     * có thể xử lý cho cả đơn hàng xe tải ở đây
     */
    public function doUpdateBackStorecardDetail() {
        $mStoreCardXuatBan  = GasStoreCard::model()->findByPk($this->store_card_id_gas);
        $mStoreCardNhapVo   = GasStoreCard::model()->findByPk($this->store_card_id_vo);
        if($mStoreCardXuatBan){
            $mStoreCardXuatBan->date_delivery   = $this->date_delivery;
            $this->getQtyReal   = true;
            $this->makeParamsPostDetail($this->getJsonDataField('info_gas'));
            $this->getQtyReal   = false;
            $mStoreCardXuatBan->employee_maintain_id    = $this->employee_maintain_id;
            $mStoreCardXuatBan->driver_id_1             = $this->driver_id;
            $mStoreCardXuatBan->car_id                  = $this->car_id;
            GasStoreCard::saveStoreCardDetail($mStoreCardXuatBan);
            if($this->isChangeDate){
                $mStoreCardXuatBan->update(['date_delivery']);
            }
        }
        if($mStoreCardNhapVo){
            $mStoreCardNhapVo->date_delivery    = $this->date_delivery;
            $this->getQtyVo     = true;
            $this->makeParamsPostDetail($this->getJsonDataField('info_vo'));
            $mStoreCardNhapVo->employee_maintain_id     = $this->employee_maintain_id;
            $mStoreCardNhapVo->driver_id_1             = $this->driver_id;
            $mStoreCardNhapVo->car_id                  = $this->car_id;
            GasStoreCard::saveStoreCardDetail($mStoreCardNhapVo);
            if($this->isChangeDate){
                $mStoreCardNhapVo->update(['date_delivery']);
            }
        }
        
    }
    
    /**
     * @Author: DungNT Jan 19, 2017
     * @Todo: xóa thẻ kho của KH trước khi insert new
     * đang xử lý check chỉ tạo thẻ kho với Giao Ngay, còn xe tải chưa xử lý
     */
    public function deleteStorecard() {
        /** 1. xóa đơn hàng xe tải/ cái này kiểm tra đúng $this->type == GasAppOrder::DELIVERY_NOW. vì đơn hàng xe tải thì mình sẽ xóa obj_detail_id
         *  @note_bug: không được xóa obj_detail_id sau khi điều phối xác nhận xong $this->status == GasAppOrder::STATUS_CONFIRM
         *  @2017_08_20 bị bug khi Xe Tải lưu hoặc hoàn thành thì nó sẽ xóa detail Order Xe Tải nên ở đây phải kiểm tra status mới cho xóa detai xe tải
         */
        $aStatusAllowDelete = [GasAppOrder::STATUS_NEW, GasAppOrder::STATUS_CONFIRM];
        if(in_array($this->status, $aStatusAllowDelete) && !empty($this->obj_detail_id)){
            $this->deleteOrderDetail($this->obj_detail_id);
        }
        // delete thẻ kho
        $models = $this->getModelStoreCard();
        $this->store_card_id_gas    = 0;
        $this->store_card_id_vo     = 0;
        foreach($models as $mStorecard){
            if($this->type == GasAppOrder::DELIVERY_BY_CAR && $this->type_of_order_car == GasOrders::TYPE_AGENT){
                $mStorecard->deleteSubRecord();
            }
            $mStorecard->delete();
        }
    }
    public function deleteOrderDetail($order_detail_id) {
        $mOrderCarDetail = GasOrdersDetail::model()->findByPk($order_detail_id);
        if($mOrderCarDetail){
            $this->obj_detail_id    = 0;
            $this->obj_id           = 0;
            $mOrderCarDetail->delete();
        }
    }
    public function deleteOrderAgent() {// Jul2317 Xóa đơn hàng đại lý
        $mOrderAgent = GasOrders::model()->findByPk($this->obj_id);
        if($mOrderAgent){
            $mOrderAgent->delete();
        }
    }
    
    /** @Author: DungNT Feb 24, 2017
     * @Todo: get model storecard nếu có
     */
    public function getModelStoreCard() {
        // 2. xóa thẻ kho
        if(empty($this->store_card_id_gas) && empty($this->store_card_id_vo)){
            return [];
        }
        $aIdStorecard   = [];
        $criteria       = new CDbCriteria();
        if(!empty($this->store_card_id_gas)){
            $aIdStorecard[] = $this->store_card_id_gas;
        }
        if(!empty($this->store_card_id_vo)){
            $aIdStorecard[] = $this->store_card_id_vo;
        }
        $sParamsIn = implode(',', $aIdStorecard);
        $criteria->addCondition("t.id IN ($sParamsIn)");
        return GasStoreCard::model()->findAll($criteria);
    }
    
    /** @Author: DungNT Jan 23, 2017
     * @Todo: tạo 1 row map đơn hàng của xe tải
     */
    public function makeOrderCar() {
        $this->obj_id_old           = $this->obj_id;
        $this->obj_detail_id_old    = $this->obj_detail_id;
        $this->type_of_order_car    = GasOrders::TYPE_DIEU_PHOI;
        
        $mOrderCar = GasOrders::getByDateAndUserExecutive($this->date_delivery, $this->user_id_executive, $this->update_by);
        if(empty($mOrderCar->id)){
            throw new Exception('Dữ liệu không hợp lệ, vui lòng kiểm tra lại');
        }
        $mOrderCar->uid_login       = MyFormat::getCurrentUid();
        $mOrderCar->scenario        = 'WindowCreate';
        $this->makeOrderCarMapQty($mOrderCar);
        $mOrderCarDetail = $mOrderCar->saveWindowRecord();
        /* save obj_id và obj_detail_id, mã đơn hàng và chi tiết của đơn hàng
         */
        $this->obj_id           = $mOrderCar->id;
        $this->obj_detail_id    = $mOrderCarDetail->id;
        $info = "MakeOrderCar line 2308 . id $this->id Error không thể lưu đơn hàng xe tải: obj_id => $this->obj_id -- obj_detail_id=> $this->obj_detail_id";
//        Logger::WriteLog($info);// Aug0817 để debug Xác nhận đơn hàng không lưu được id detail trong order xe tải, dẫn đến không update được tài xế
        $this->makeOrderCarDeleteOldId();
        if(empty($this->obj_id) || empty($this->obj_detail_id)){
            SendEmail::bugToDev($info);// Aug1517 đã fix lỗi này, do ở hàm deleteStorecard mình không xóa order detail (obj_detail_id) đi dẫn đến cuối hàm nó sẽ bị xóa bởi hàm $this->makeOrderCarDeleteOldId();
            throw new Exception('Dữ liệu không hợp lệ, không thể lưu đơn hàng xe tải');
        }
    }
    
    /**
     * @Author: DungNT Jan 23, 2017
     * @Todo: Xóa những detail cũ khi bị change điều xe.
     */
    public function makeOrderCarDeleteOldId() {
        if(!empty($this->obj_detail_id_old) && $this->obj_detail_id_old != $this->obj_detail_id){
            $this->deleteOrderDetail($this->obj_detail_id_old);
        }
    }
    public function deleteOrderCar() {// xóa detail của đơn hàng xe tải
        if(!empty($this->obj_detail_id) && $this->type == GasAppOrder::DELIVERY_BY_CAR){
            $this->deleteOrderDetail($this->obj_detail_id);
        }
        if(!empty($this->obj_id) && $this->type == GasAppOrder::DELIVERY_BY_CAR && $this->type_of_order_car == GasOrders::TYPE_AGENT){
            $this->deleteOrderAgent();
        }
    }
    
    protected function beforeDelete() {
        $info = "GasAppOrder beforeDelete Admin Debug => customer_id = $this->customer_id and id=$this->id";
        Logger::WriteLog($info);

        $this->deleteStorecard();
        $this->deleteOrderCar();
        $this->removePayCashbook();
        GasFile::DeleteByBelongIdAndType($this->id, GasFile::TYPE_10_APP_BO_MOI);
        // xóa gas dư nếu có
        $this->deleteGasRemainOld();
        // xóa table detail 
        $mAppOrderDetail = new GasAppOrderCDetailReal();
        $mAppOrderDetail->detailDelete($this);

        return parent::beforeDelete();
    }
    
    /** @Author: DungNT Jul 28, 2017. xóa gas dư cũ trước khi insert
     */
    public function deleteGasRemainOld() {
        $aSeri = $this->getArraySeriVo();
        $this->makeGasRemainDeleteOld($aSeri);
    }
    
    /**
     * @Author: DungNT Jan 23, 2017
     * @Todo: map data qty từ json vào biến order
     */
    public function makeOrderCarMapQty(&$mOrderCar) {
        $mOrderCar->customer_contact     = $this->customer_contact;
        $mOrderCar->user_id_executive    = $this->user_id_executive;
        $mOrderCar->customer_id          = $this->customer_id;
        $mOrderCar->b50                  = $this->customer_qty_request['b50'];
        $mOrderCar->b45                  = $this->customer_qty_request['b45'];
        $mOrderCar->b12                  = $this->customer_qty_request['b12'];
        $mOrderCar->b6                   = $this->customer_qty_request['b6'];
        $mOrderCar->user_id_create       = $this->update_by;
        $mOrderCar->note                 = $this->note_customer;
        $mOrderCar->from_app             = GasOrders::FROM_APP;
        $mOrderCar->validate();
        if($mOrderCar->hasErrors()){
            throw new Exception(HandleLabel::FortmatErrorsModel($mOrderCar->getErrors()));
        }
    }

    /**
     * @Author: DungNT Dec 13, 2016 
     * @Todo: check employee can pick / Cancel Order. check có thể Hủy nhận đơn hàng
     */
    public function canPick() {
//        }elseif(in_array($mUser->role_id, $mTransactionHistory->getRoleCanHandleHgd())){
        if($this->status == GasAppOrder::STATUS_CONFIRM && $this->type == GasAppOrder::DELIVERY_NOW && empty($this->employee_maintain_id) &&
        (!is_null($this->mAppUserLogin) && in_array($this->mAppUserLogin->role_id, $this->getRoleUpdateOrder()) )
        ){
            return '1';
        }
        return '0';
    }
    public function canPickCancel() {// old name canCancelOrder
        if($this->isWebUpdate){
            return '1';
        }
        if($this->type == GasAppOrder::DELIVERY_BY_CAR) {// Xe tải ko cho hủy nhận đơn
            return '0';
        }
        if(!is_null($this->mAppUserLogin) && $this->status == GasAppOrder::STATUS_PROCESSING && 
            ( $this->employee_maintain_id == $this->mAppUserLogin->id || $this->driver_id == $this->mAppUserLogin->id) ) {// giao ngay
            return '1';
        }

        return '0';
    }
    
    /** @Author: DungNT Jan 27, 2019
     *  @Todo: check user can Cancel order  cả xe tải và giao ngay
     **/
    public function canCancel() {// old name canCancelOrder
        if($this->isWebUpdate){
            return '1';
        }
        if(!is_null($this->mAppUserLogin) && $this->status == GasAppOrder::STATUS_PROCESSING && 
            ( $this->employee_maintain_id == $this->mAppUserLogin->id || $this->driver_id == $this->mAppUserLogin->id) ) {// giao ngay
            return '1';
        }
        return '0';
    }

    // May1118Check hẹn giờ giao
    public function checkDeliveryTimer() {
        if(empty($this->delivery_timer) || $this->delivery_timer == '0000-00-00 00:00:00'){
            return '';
        }
        if(MyFormat::compareTwoDate($this->delivery_timer, date('Y-m-d H:i:s'))){
            throw new Exception("Không thể hủy đơn hàng, sau thời gian hẹn giao hàng {$this->getDeliveryTimer()} bạn mới được hủy đơn. Thời gian bắt đầu tính để hoàn thành đơn từ: {$this->getDeliveryTimer()}");
        }
    }
    
    public function appCheckFile($q) {
        $mCustomer = $this->rCustomer;
        $mTransactionHistory = new TransactionHistory();
        if(in_array($this->mAppUserLogin->role_id, $mTransactionHistory->getRoleCanHandleHgd())){
//            $this->requiredFile = false; // Now2217 required file với toàn bộ đơn hàng của giao nhận
        }elseif($this->mAppUserLogin->role_id == ROLE_DRIVER){
//            if(count($this->rFile) > 0 || $q->platform == UsersTokens::PLATFORM_IOS){
//            if(count($this->rFile) > 0){
//                $this->requiredFile = false;
//            }
        }
        if(count($this->rFile) > 0 || $q->action_type != GasAppOrder::STATUS_COMPPLETE || $this->isOrderAgent()
            || $mCustomer->is_maintain == STORE_CARD_KH_MOI ){// Now2217
            $this->requiredFile = false;
        }
        
        $this->apiValidateFile();// Tạm close lại đã vì chưa có app cho user up image
        if($this->getError('file_name') && $q->action_type == GasAppOrder::STATUS_COMPPLETE){
//            throw new Exception($this->getError('file_name'));
            $this->addError('id', $this->getError('file_name'));
        }
    }
    
    /** @Author: DungNT Jan 19, 2017
     *  @Todo: handle set event of App Order
     *  for debug: $mUserLogin
     */
    public function handleSetEvent($q) {
        try{
        if($this->hasErrors()){
            return ;
        }
        $this->mapJsonFieldOneDecode('JSON_FIELD', 'json', 'baseArrayJsonDecode');
        $this->mapJsonFieldOneDecode('JSON_FIELD_INFO', 'json_info', 'baseArrayJsonDecodeV1');
        $status_obj = ''; // $aSeri = $this->getArraySeriVo();
        switch ($q->action_type) {
            case Transaction::EMPLOYEE_NHAN_GIAO_HANG:
                $this->employeePick($q);
                $status_obj = MonitorUpdate::STATUS_CONFIRM;
                break;
            case Transaction::EMPLOYEE_HUY_GIAO_HANG:
                $this->employeeCancelPick($q);
                $status_obj = MonitorUpdate::STATUS_CANCEL;
                break;
            case GasAppOrder::STATUS_COMPPLETE:
                $this->employeeComplete($q);
                $status_obj = MonitorUpdate::STATUS_COMPLETE;
                break;
            case GasAppOrder::STATUS_CANCEL:
                $this->employeeCancelOrder($q);
                $status_obj = MonitorUpdate::STATUS_CANCEL;
                break;
            default:
                $this->addError('id', 'Yêu cầu không hợp lệ');
                return ;
        }
        
        if(!empty($status_obj) && !$this->hasErrors()){
            $location = $q->latitude.",$q->longitude";
            MonitorUpdate::updateLocationEmployee($this->mAppUserLogin->id, $status_obj, $location, $this);
            $this->saveEvent($q->action_type, $location);
        }
        } catch (Exception $ex) {
            $this->addError('id', $ex->getMessage());
            return ;
        }
    }
    
    /**
     * @Author: DungNT Jan 19, 2017
     * @Todo: handle employee Pick (nhận đơn hàng)
     */
    public function employeePick($q) {
        if(!$this->canPick()){
            $nameEmployee = $this->getUserInfo('rEmployeeMaintain', 'first_name');
            $this->addError('id', 'Đơn hàng đã có '.$nameEmployee.' nhận giao, bạn không thể nhận đơn hàng này');
            return;
        }
//        $this->driver_id            = $this->mAppUserLogin->id;
        $this->employee_maintain_id = $this->mAppUserLogin->id;
        $this->status               = GasAppOrder::STATUS_PROCESSING;
        $this->setJsonNameUser();
        $this->setJsonDataField();
        $this->setJsonDataField('JSON_FIELD_INFO', 'json_info');
//        $aUpdate = array('employee_maintain_id', 'status', 'json_info');
        $this->update();
        // May 19, 2017 tự động set đơn hàng đã thu tiền
//        $this->autoAddPayCashbook();/ không cần set ở đây, vì mỗi khi save và hoàn thành sẽ save rồi

        // notify cho KH
        $this->notifyPhoneAppCustomer();
        
//        $this->titleNotify = $this->getNameEmployeeMaintain(). ' nhận giao đơn hàng '.$this->getCodeNo(). '. '. $this->getCustomerName().', '.$this->getCustomerAddress();
//        $this->notifyEmployeeMaintain();// for test, có thể live không cần
    }
    
    /**
     * @Author: DungNT Jan 19, 2017
     * @Todo: handle employee Cancel Pick (Hủy nhận đơn hàng)
     */
    public function employeeCancelPick($q) {
        if(!$this->canPickCancel()){
            $this->addError('id', 'Bạn không thể hủy nhận đơn hàng này');
            return;
        }
        $this->deleteStorecard();
//       Jul218 fix dữ liệu chạy app của Vũng Tàu $mOrderCDetailReal = new GasAppOrderCDetailReal(); $mOrderCDetailReal->detailDelete($this);
        $this->titleNotify = $this->getNameEmployeeMaintain(). ' hủy giao đơn hàng '.$this->getCodeNo(). '. '. $this->getCustomerName().', '.$this->getCustomerAddress();
//        $this->driver_id            = 0;
        $this->employee_maintain_id = 0;
        $this->name_employee_maintain = '';
        $this->status               = GasAppOrder::STATUS_CONFIRM;
        $this->setJsonNameUser();
        $this->setJsonDataField();
        $this->setJsonDataField('JSON_FIELD_INFO', 'json_info');
        $this->update();
        // May 19, 2017 Xóa row tự động set đơn hàng đã thu tiền
        $this->removePayCashbook();

        $this->notifyEmployeeMaintain();// for test, có thể live không cần
// Chưa làm notify cho giao nhận        
//        $employeeMaintainName = $this->getUserInfo('rDriver', 'first_name');
//        $aUid   = $this->getListEmployeeMaintainOfAgent();
//        $title  = $employeeMaintainName.' HỦY NHẬN ĐƠN HÀNG '.$this->getFirstName(). ' - '.$this->getAddress();
//        $this->notifyPhoneAppEmployee($aUid, $title);
        // notify cho KH
//        $this->notifyPhoneAppCustomer();// có thể live không cần
    }
    
    public function canDoSetStatusNew() {
        $aStatusNotAllow = [GasAppOrder::STATUS_NEW, GasAppOrder::STATUS_CONFIRM, GasAppOrder::STATUS_PROCESSING];
        if(in_array($this->status, $aStatusNotAllow)){
            return false;
        }
        return true;
    }
    
    /** @Author: DungNT Jul 23, 2017
     * @Todo: chuyển trạng thái mới của đơn hàng from web
     */
    public function doSetStatusNew() {
        /* 1. set employeeCancelPick
         * 2. write event voi trang thai chuyen moi cho don hang
         */
        if($this->type == GasAppOrder::DELIVERY_BY_CAR){// Jan3118
            $this->mAppUserLogin    = $this->rDriver;
            $this->status           = GasAppOrder::STATUS_PROCESSING;
            $this->status_debit     = GasAppOrder::DEBIT_NO;
            $this->update(['status', 'status_debit']);
            $this->saveEvent(GasAppOrder::WEB_SET_STATUS_NEW, '');
            return ;
        }
        
        $this->mapJsonFieldOneDecode('JSON_FIELD', 'json', 'baseArrayJsonDecode');
        $this->mapJsonFieldOneDecode('JSON_FIELD_INFO', 'json_info', 'baseArrayJsonDecodeV1');
        $this->mAppUserLogin = $this->rEmployeeMaintain;
        $this->status_debit  = GasAppOrder::DEBIT_NO;
        $this->reason_false  = 0;
        $this->employeeCancelPick([]);
        $this->saveEvent(GasAppOrder::WEB_SET_STATUS_NEW, '');
        if($this->hasErrors()){
            throw new Exception(HandleLabel::FortmatErrorsModel($this->getErrors())); 
        }
    }
    
    /** @Author: DungNT Feb 10, 2017
     *  @Todo: handle employee complete đơn hàng
     */
    public function employeeComplete($q) {
        try{
        if( ($this->type == GasAppOrder::DELIVERY_NOW && !$this->canPickCancel()) || ($this->type == GasAppOrder::DELIVERY_BY_CAR && $this->mAppUserLogin->id != $this->driver_id) ){
            $this->addError('id', 'Bạn không thể hoàn thành đơn hàng này');
            return;
        }
        $mEmail = new SendEmail();
        $this->checkCarNumber();
        $this->getGasRemainKeepPrice();
        $this->handlePostDriverUpdate($q);
        if($this->hasErrors()){
            $this->addError('id', HandleLabel::FortmatErrorsModel($this->getErrors()));
            return;
        }
        $this->checkCompleteApp();
        if(!in_array($this->agent_id, $this->getListAgentAllowDebitVo())){
            $mEmail->checkOrderDebitVo($this);
        }
        $this->status           = GasAppOrder::STATUS_COMPPLETE;
        $this->complete_time    = date('Y-m-d H:i:s');
//        $this->changeCurrentDateComplete();// Mar0118 tạm close lại 
        $this->setUserUpdate();// Sep2118 ghi log lại để app KH biết đc time hoàn thành/hủy 
        $this->handleSaveDriverUpdate();
        $this->makeGasRemain();
        $this->checkErrorComplete();

        // notify cho KH
        $this->titleNotify  = "Hoàn thành đơn hàng {$this->getDateDelivery()} - ". $this->getCustomerName();
        $this->notifyPhoneAppCustomer();
        $this->setCacheInventoryAgent();// Close on Mar2618 thấy không cần thiết lắm
//        [TASKTIMER]
//        update forecast Sep 27, 2018
        $mForecast = new Forecast();
        $mForecast->handleGenerateForecast([$this->customer_id]);
//        $mGasScheduleSms = new GasScheduleSms();
//        $mGasScheduleSms->announceBoMoiPayL1($this->rCustomer, $this); 
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage(), $ex->getCode());
        }
    }
    /**
     * @Author: DungNT Feb 10, 2017
     * @Todo: handle employee drop đơn hàng
     */
    public function employeeCancelOrder($q) {
        if(!$this->canCancel()){
            $this->addError('id', 'Bạn không thể hủy bỏ đơn hàng này');
            return;
        }
        $this->checkDeliveryTimer();
//        $aSeri = $this->getArraySeriVo();// Sep1717 close
        $this->note_employee    = isset($q->note_employee) ? $q->note_employee : '';
        $this->status           = GasAppOrder::STATUS_CANCEL;
        $this->reason_false     = isset($q->status_cancel) ? $q->status_cancel : 0;
        $this->complete_time    = date('Y-m-d H:i:s');
        $this->setUserUpdate();// Sep2118 ghi log lại để app KH biết đc time hoàn thành/hủy 
        $this->setJsonDataField('JSON_FIELD_INFO', 'json_info');
        $aUpdate = array('complete_time', 'update_by', 'update_time', 'status', 'json_info', 'reason_false');
        $this->update($aUpdate);

        // hủy thẻ kho
        $this->cancelStorecard($q);
        // notify cho KH
        $this->titleNotify  = "Hủy đơn hàng {$this->getCodeNo()} - ". $this->getCustomerName();
        $this->notifyPhoneAppCustomer();
        $this->doCancel();
        $this->makeSocketNotifyCancelOrder();
    }
    
    /** @Author: DungNT Aug 27, 2017
     * @Todo: làm 1 số action sau khi cancel 
     */
    public function doCancel() {
        $aSeri = $this->getArraySeriVo();
        // May 19, 2017 Xóa row tự động set đơn hàng đã thu tiền
        $this->removePayCashbook();
        // 2. delete old record gas remain
        $this->makeGasRemainDeleteOld($this->aSeriOld);
        // 3. xóa table detail
        $mAppOrderDetail = new GasAppOrderCDetailReal();
        $mAppOrderDetail->detailDelete($this);
    }
    
    /** @Author: DungNT Mar 24, 2018
     *  @Todo: xử lý set cache inventory khi hoàn thành đơn xe tải
     **/
    public function setCacheInventoryAgent() {
        if(!$this->isOrderAgent()){
            return ;
        }
//        Sta2::setCacheInventoryAllAgent(); DungNT Sep0119 close Move to $mCronTask->addTaskSyncAppCache
        $mCronTask = new CronTask();
        $mCronTask->addTaskSyncAppCache(CronTask::TYPE_UPDATE_AGENT_INVENTORY, '5_phut_check_setCacheInventoryAgent', 0);
    }
    
    /** @Author: DungNT Dec 11, 2018
     *  @Todo: get list agent allow nợ vỏ
     **/
    public function getListAgentAllowDebitVo() {
        return [
//            CodePartner::AGENT_THANH_SON,
        ];
    }
    
    /** @Author: DungNT Jul 21, 2017
     * @Todo: get array seri vỏ để delete bên Gas Remain trước khi insert
     */
    public function getArraySeriVo() {
        $aSeri = [GasRemain::SERI_TDV];
        $aDetailInfo = $this->getJsonDataField('info_vo');
        foreach($aDetailInfo as $aInfo){
            if(trim($aInfo['seri']) != ''){
                $aSeri[]    = $aInfo['seri'];
            }
        }
        $this->aSeriOld = $aSeri;
        return $aSeri;
    }
    
    /**
     * @Author: DungNT Feb 24, 2017
     * @Todo: cancel model storecard thẻ kho, khi giao nhận hủy đơn hàng
     */
    public function cancelStorecard($q) {
        $aModelStorecard = $this->getModelStoreCard();
        foreach($aModelStorecard as $mStorecard){
            $mStorecard->reason_false = isset($q->status_cancel) ? $q->status_cancel : 0;
            $mStorecard->handleCancel();
        }
    }
    
    /**
     * @Author: DungNT Feb 11, 2017
     * @Todo: save event each time user action to Order
     */
//    public function saveEvent($q, $mUserLogin) {
    public function saveEvent($action_type, $google_map) {
        $mEvent = new TransactionEvent();
        $mEvent->type                       = TransactionEvent::TYPE_BO_MOI;
        $mEvent->transaction_history_id     = $this->id;
        $mEvent->user_id                    = $this->mAppUserLogin ? $this->mAppUserLogin->id : '';
        $mEvent->action_type                = $action_type;
        $mEvent->google_map                 = $google_map;
//        $mEvent->status_cancel              = $status_cancel;
        if($action_type == GasAppOrder::WEB_SET_STATUS_NEW){
            $mEvent->user_id                = MyFormat::getCurrentUid();
        }
        if($action_type == GasAppOrder::WEB_UPDATE){
            $mEvent->json_data              = MyFormat::jsonEncode($this->getAttributes());
            $mEvent->user_id                = MyFormat::getCurrentUid();
        }
        $mEvent->save();
    }
    
    public function notifyPhoneAppCustomer() {
        $aUid   = [$this->customer_id => $this->customer_id];
        if(!empty($this->parent_chain_store)){
            $aUid[$this->parent_chain_store] = $this->parent_chain_store;
        }
        if(empty($this->titleNotify)){
            $this->titleNotify  = "Đơn hàng {$this->getDateDelivery()} - Nhân viên giao hàng: ". $this->getUserInfo('rEmployeeMaintain', 'first_name').' - '.$this->getUserInfo('rEmployeeMaintain', 'phone');
        }
//        $this->runInsertNotify($aUid, $this->titleNotify, true); // Send Now Dec 21, 2015 
        $this->runInsertNotify($aUid, $this->titleNotify, false); // Send By Cron Dec 21, 2015 
    }
    /** Feb 02, 2017
     *  notify cho các giao nhận của đại lý đó khi có đơn hàng xác nhận của điều phối là giao ngay
     */
    public function notifyEmployeeMaintain() {
        if(empty($this->titleNotify)){
            $this->titleNotify = "ĐH Bò Mối: {$this->getCustomerName()}";
        }
        $this->runInsertNotify($this->getListUserIdEmployeeMaintain(), $this->titleNotify, true); // Send Now Dec 21, 2015  - Fix fcm send by nodejs Sep1118
//        $this->runInsertNotify($this->getListUserIdEmployeeMaintain(), $this->titleNotify, false); // Send By Cron Feb 23, 2017
    }
    public function notifyDriver() {
        if(empty($this->titleNotify)){
            $this->titleNotify = "ĐH Bò Mối: {$this->getCustomerName()}";
        }
//        $this->runInsertNotify($this->driver_id, $this->titleNotify, true); // Send Now Dec 21, 2015 
        $this->runInsertNotify([$this->driver_id], $this->titleNotify, false); // Send By Cron Feb 23, 2017
    }
    /** Feb 02, 2017
     *  căn cứ vào Id tỉnh để notify cho điều phối của tỉnh đó
     * Khi KH đặt hàng từ app thì phải notify cho điều phối để biết vào confirm
     */
    public function notifyDieuPhoi() {
        if($this->uid_login_role != ROLE_CUSTOMER){
            return ;
        }
        if(empty($this->titleNotify)){
            $this->titleNotify = "Đặt hàng APP {$this->getCustomerName()}";
        }
        $this->runInsertNotify($this->getListUserIdDieuPhoi(), $this->titleNotify, true); // Dec 21, 2015 - Fix fcm send by nodejs Sep1118
//        $this->runInsertNotify($this->getListUserIdDieuPhoi(), $this->titleNotify, false); // Dec 21, 2015 
    }
    
    /** @Author: DungNT Feb 02, 2017 belong notifyDieuPhoi*/
    public function getListUserIdDieuPhoi() {
        $mAppCache = new AppCache();
        $aUid = $mAppCache->getArrayIdRole(ROLE_DIEU_PHOI);
        unset($aUid[GasConst::UID_NGOC_PT]);
        unset($aUid[GasConst::UID_PHUONG_NT]);
        return $aUid;
//        return GasOrders::$ZONE_HCM;// remove from Jun 11, 2017
        if($this->province_id_agent){
            // Feb 02, 2017 xử lý sau
        }
    }
    /* lấy danh sách các giao nhận của đại lý đó để notify */
    public function getListUserIdEmployeeMaintain() {
        return GasOneMany::getArrOfManyIdByType(ONE_AGENT_MAINTAIN, $this->agent_id);
    }

    /**
     * @Author: DungNT Dec 06, 2016
     * @Todo: insert to table notify 
     * @Param: $aUid array user id need notify
     */
    public function runInsertNotify($aUid, $title, $sendNow = false) {
        $json_var = [];
        foreach($aUid as $uid) {
            if( !empty($uid) ){
                $mScheduleNotify = GasScheduleNotify::InsertRecord($uid, GasScheduleNotify::APP_ORDER_BO_MOI, $this->id, '', $title, $json_var);
                if($sendNow && !is_null($mScheduleNotify)){
                    $mScheduleNotify->sendImmediateForSomeUser();
                }// Jan 05, 2016 tạm close send luôn lại, vì nó gây chậm bên PMBH khi send
                // sẽ gửi = cron
            }
        }
    }
    
    /**
     * @Author: DungNT Jan 26, 2017
     * @Todo: get AppOrder theo obj_id (order_id: đơn hàng xe tải)
     * Sử dụng để update khi điều phối cập nhật Order xe tải
     */
    public function getByOrderId() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.obj_id=' . $this->obj_id );
        $models = self::model()->findAll($criteria);
        $aRes   = [];
        foreach($models as $item){
            $aRes[$item->obj_detail_id] = $item;
        }
        return $aRes;
    }
    
    /**
     * @Author: DungNT Jan 31, 2017
     * @Todo: get AppOrder theo obj_id (order_id: đơn hàng xe tải)
     * Sử dụng để update khi điều phối cập nhật Order xe tải của Agent Not Bò Mối
     */
    public function getByOrderIdAndType() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.obj_id=' . $this->obj_id );
        $criteria->addCondition('t.type=' . $this->type );
        return self::model()->find($criteria);
    }
    
    /**
     * @Author: DungNT Jan 27, 2017
     * @Todo: delete some order khi điều phối delete ở đơn hàng xe tải
     */
    public function deleteByOrderDetail() {
        if(count($this->aOrderDetailIdDelete) < 1 || empty($this->obj_id)){
            return ;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('obj_id=' . $this->obj_id );
        $sParamsIn = implode(',', $this->aOrderDetailIdDelete);
        $info = "GasAppOrder deleteByOrderDetail admin Debug => obj_id = $this->obj_id and $sParamsIn";
        Logger::WriteLog($info);
        $criteria->addCondition("obj_detail_id IN ($sParamsIn)");
        self::model()->deleteAll($criteria);
    }
    
    // kiểm tra xem trạng thái Order có hợp lệ để show 1 số button 
    public function isValidStatusAppPay() {
        $aStatusShow = [GasAppOrder::STATUS_COMPPLETE];
        if(!in_array($this->status, $aStatusShow) || $this->mAppUserLogin->role_id == ROLE_CUSTOMER){
            return 0;
        }
        return 1;
    }
    
    /**
     * @Author: DungNT May 19, 2017
     * @Todo: show or hide button Thu tiền trên app 
     */
    public function getShowThuTien() {
//        return 1;// only dev test
        if(!$this->isValidStatusAppPay()){
            return 0;
        }
        $mTransactionHistory = new TransactionHistory();
        if($this->status_debit == GasAppOrder::DEBIT_YES && in_array($this->mAppUserLogin->role_id, $mTransactionHistory->getRoleCanHandleHgd())){
            return 1;
        }
        return 0;
    }
    public function getShowChiGasDu() {
//        return 1;// only dev test
        if(!$this->isValidStatusAppPay() || $this->status_debit == GasAppOrder::DEBIT_YES){
            return 0;
        }
        if($this->paid_gas_remain == GasAppOrder::PAID_REMAIN_NO && $this->total_gas_du > 0){
            return 1;
        }
        return 0;
    }
    public function showDebit() {
//        return 1;// only dev test
        if(!$this->isValidStatusAppPay() || $this->paid_gas_remain == GasAppOrder::PAID_REMAIN_YES){
            return 0;
        }
        
        $date_check = MyFormat::modifyDays($this->date_delivery, Yii::app()->params['DaysUpdateAppBoMoi']);
        if(!MyFormat::compareTwoDate($date_check, date('Y-m-d'))){
            return '0';
        }
        
        if($this->status_debit == GasAppOrder::DEBIT_NO){
            return 1;
        }
        return 0;
    }
    public function showAppComplete() {
        $aStatusShow = [GasAppOrder::STATUS_COMPPLETE];
        if(!in_array($this->status, $aStatusShow)){
            return 1;
        }
        return 0;
    }
    public function showAppCancel() {
//        if($this->type == GasAppOrder::DELIVERY_BY_CAR){
//            return 0;
//        }Now2918 close, allow tài xế hủy đơn app
        $aStatusShow = [GasAppOrder::STATUS_PROCESSING];
        if(in_array($this->status, $aStatusShow)){
            return 1;
        }
        return 0;
    }
    // show input cân thiếu đầu vào
    public function showAppPayBack() {
//        return 1;
//        return 0;// tạm close lại khi nào xử lý xong việc cho nhập 2 fieled số KG và tiền trả lại thì mở ra
        if($this->type == GasAppOrder::DELIVERY_BY_CAR && $this->mAppUserLogin->role_id == ROLE_CUSTOMER && $this->pay_back < 1){
            return 0;
        }
        $mGasRemain = new GasRemain();
        if(array_key_exists($this->agent_id, $mGasRemain->getArrayAgentCar())){
            return 1;
        }

        if($this->type == GasAppOrder::DELIVERY_BY_CAR && $this->type_of_order_car == GasOrders::TYPE_DIEU_PHOI){
            return 1;
        }
        return 0;
    }
    public function showAppDiscount() {
        return 0;// Now0218 đóng hết, ticket lên để chị Phương sửa
        if($this->mAppUserLogin->role_id == ROLE_CUSTOMER && $this->discount < 1){
            return 0;
        }
        $aRoleAllow = [ROLE_EMPLOYEE_MAINTAIN];
        $mTransactionHistory = new TransactionHistory();
//        if(!in_array($this->mAppUserLogin->role_id, $aRoleAllow)){
        if(!in_array($this->mAppUserLogin->role_id, $mTransactionHistory->getRoleCanHandleHgd())){
            return 0;
        }
        return 1;
    }
    public function showAppSave() {
        return 0;// Jun1218 disable button save
        if($this->mAppUserLogin->role_id == ROLE_CUSTOMER){
            return 0;
        }

        if(in_array($this->mAppUserLogin->role_id, $this->getRoleUpdateOrder())){
//            if(isset($_GET['platform']) && $_GET['platform'] == UsersTokens::PLATFORM_ANDROID){// Now2217 ios cần fix chỗ này, hiện tại hoàn thành ko up load file được
                return 0;// // Oct0717 không cho lưu, chỉ cho hoàn thành với giao nhận
//            }// close On Jun1218
        }

        $aStatusHide = [GasAppOrder::STATUS_CANCEL, GasAppOrder::STATUS_COMPPLETE];
//        $aStatusHide = [GasAppOrder::STATUS_CANCEL];
        if(in_array($this->status, $aStatusHide) || 
            ( $this->status_debit == GasAppOrder::DEBIT_YES && $this->type == GasAppOrder::DELIVERY_NOW)
        ){
            return 0;
        }

//        if($this->mAppUserLogin->role_id == ROLE_DRIVER && $this->status == GasAppOrder::STATUS_COMPPLETE){
//            return 0;// Chặn chỉ cho xe tải cập nhật trong ngày.
//        }// Dec2517 chỗ này cho phép lưu trước khi sửa.
        return 1;
    }
    
    /** @Author: DungNT Jul 02, 2018
     *  @Todo: check ẩn hiện nút confirm stt đơn xe tải giao cho KH bò mối
     **/
    public function showConfirmStt() {
//        return 0;
        $aAgentCheck = [GasCheck::KHO_PHUOC_TAN];
        if($this->mAppUserLogin->role_id == ROLE_CUSTOMER || in_array($this->status, $this->getArrayLockConfirmStt()) ){
            return 0;
        }
        $agentId = $this->carAgentId();
//        if($this->type == GasAppOrder::DELIVERY_BY_CAR && $this->type_of_order_car == GasOrders::TYPE_DIEU_PHOI
        if($this->type == GasAppOrder::DELIVERY_BY_CAR
                && in_array($agentId, $aAgentCheck) 
        ){
            return 1;
        }
        return 0;
    }
    public function getArrayLockConfirmStt() {
        return [GasAppOrder::STATUS_CANCEL];
    }
    
    /**
     * @Author: DungNT Feb 07, 2017
     * @Todo: check xem User có được quyền sửa Order không
     */
    public function employeeCanUpdate() {
//        return '1'; // only dev test need close on live
        if(is_null($this->mAppUserLogin) || $this->mAppUserLogin->role_id == ROLE_CUSTOMER){
            return '0';
        }
//        $aNotAllow = array(GasAppOrder::STATUS_COMPPLETE, GasAppOrder::STATUS_CANCEL);
//        if(in_array($this->status, $aNotAllow) 
//                || ($this->type == GasAppOrder::DELIVERY_NOW && $this->employee_maintain_id != $this->mAppUserLogin->id)
//                || ($this->type == GasAppOrder::DELIVERY_BY_CAR && $this->driver_id != $this->mAppUserLogin->id)
//                ){
//            return '0';
//        }// May 17,  2017 allow update in day
        if(($this->type == GasAppOrder::DELIVERY_NOW && $this->employee_maintain_id != $this->mAppUserLogin->id)
            || ($this->type == GasAppOrder::DELIVERY_BY_CAR && $this->driver_id != $this->mAppUserLogin->id)
        ){// May 22, 2017 Big Bug, nếu không check cái này thì NV sẽ không xác nhận mà tự hoàn thành luôn => fix oải
            return '0';
        }

        $date_check = MyFormat::modifyDays($this->date_delivery, Yii::app()->params['DaysUpdateAppBoMoi']);
        if(!MyFormat::compareTwoDate($date_check, date('Y-m-d'))){
            return '0';
        }
        return '1';
    }
    
    /** @Author: DungNT Jul 19, 2017
     * @Todo: set lại agent_id với đơn hàng xe tải khi tạo gas dư nhập về xưởng
     */
    public function makeGasRemainChangeAgentId() {
        if($this->type == GasAppOrder::DELIVERY_BY_CAR && $this->type_of_order_car == GasOrders::TYPE_AGENT){
            return GasConst::$DIEUXE_MAP_TO_AGENT[$this->user_id_executive];
        }
        return $this->agent_id;
    }
    // map agent_id tương ứng với điều xe
    public function carAgentId() {
        if($this->type == GasAppOrder::DELIVERY_BY_CAR && isset(GasConst::$DIEUXE_MAP_TO_AGENT[$this->user_id_executive])){
            return GasConst::$DIEUXE_MAP_TO_AGENT[$this->user_id_executive];
        }
        return $this->agent_id;
    }

    /**
     * @Author: DungNT Feb 18, 2017
     * @Todo: sinh tự động gas dư
     * 1. delete old record
     * 2. build sql run one query => run
     */
    public function makeGasRemain() {
        // 2. build sql run one query => run
        $sql = '';
        $aRowInsert = $aSeri = [];
        $this->makeGasRemainBuildSql($aRowInsert, $aSeri);
        //  xử lý check không cập nhật lại gas dư với đơn xe tải đã hoàn thành và cân lại gas dư rồi
//        if($this->type == GasAppOrder::DELIVERY_BY_CAR && $this->doneReweighed){
//            return ;// có thể ko cần check chỗ này 
//        }
        
        // 1. delete old record
        $this->makeGasRemainDeleteOld($this->aSeriOld);
        
        $tableName = GasRemain::model()->tableName();
        $sql = "insert into $tableName (date_input,
                    date_input_bigint,
                    customer_id,
                    customer_parent_id,
                    sale_id,
                    type_customer,
                    agent_id,
                    customer_id_of_agent,
                    materials_id,
                    materials_type_id,
                    seri,
                    price,
                    amount,
                    amount_empty,
                    amount_has_gas,
                    amount_gas,
                    uid_login,
                    has_export,
                    amount_gas_2,
                    user_update_2,
                    driver_id,
                    car_id,
                    phu_xe_1,
                    phu_xe_2,
                    weight_from_customer,
                    pvkh_miss_kg,
                    pvkh_miss_money,
                    driver_miss_kg,
                    driver_miss_money,
                    status_employee_debit,
                    created_date,
                    code_no
                    ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert)>0)
            Yii::app()->db->createCommand($sql)->execute();
        $this->aSeriOld = $aSeri;
        if(!$this->isWebUpdate){
            // Chỉ set export cho đơn xe tải hoàn thành đơn hàng app của Đại Lý
            $this->setGasRemainStatus(GasRemain::STATUS_EXPORT);
        }
    }

    /** @Todo: build sql run 
     * khi PVKH hoàn thành đơn bò mối hoặc xuất gas dư từ đại lý về xưởng */
    public function makeGasRemainBuildSql(&$aRowInsert, &$aSeri) {
        $mCustomer = $this->rCustomer;
        $uid_login = empty($this->employee_maintain_id) ? $this->driver_id : $this->employee_maintain_id;        
        $agent_id  = $this->makeGasRemainChangeAgentId();
        $this->makeGasRemainGetOldModel();
        $created_date = date('Y-m-d H:i:s');
 
        foreach($this->aDetailVo as $mDetail){
            $mDetail->seri = trim($mDetail->seri);
            if($mDetail->seri == ''){
                continue ;
            }
            $keySeri = $mDetail->materials_id.'-'.$mDetail->seri;
            if(isset($this->aGasRemainExportOtherAgent[$keySeri])){
                continue ;// Jul0118, Thúc update web đơn có gas dư Xe Tải Thành xuất cho BT1, sau đó từ BT mới về Kho PT
            }
            if($mDetail->seri != ''){
                $aSeri[]    = $mDetail->seri;
            }
            if( !in_array($mDetail->materials_type_id, GasMaterialsType::$ARR_VO_45KG) && 
                (empty($mDetail->kg_has_gas) || ($mDetail->kg_has_gas - $mDetail->kg_empty)*1 <= 0)
            ){// Now0817 xử lý cho vỏ 45, 50 không có gas dư vẫn vào app giao nhận để bấm xuất đi
                continue ;
            }// Sep0417 xử lý cho xóa gas dư ở giao diện, theo số seri 
            $mGasRemain = new GasRemain();
            $mGasRemain->uid_login = $uid_login;
            $this->calcInfoGasRemain($mGasRemain, $mDetail);

            $qtyRemain  = $mDetail->kg_has_gas - $mDetail->kg_empty;
            $amount     = $qtyRemain * $mGasRemain->price;// Thành tiền của gas dư
            $aRowInsert[]="('$this->date_delivery',
                        '$this->date_delivery_bigint',
                        '$this->customer_id',
                        '$mCustomer->parent_id',
                        '$mCustomer->sale_id',
                        '$mCustomer->is_maintain',
                        '$agent_id',
                        '$mGasRemain->customer_id_of_agent',
                        '$mDetail->materials_id',
                        '$mDetail->materials_type_id',
                        '$mDetail->seri',
                        '$mGasRemain->price',
                        '$amount',
                        '$mDetail->kg_empty',
                        '$mDetail->kg_has_gas',
                        '$qtyRemain',
                        '$mGasRemain->uid_login',
                        '$mGasRemain->has_export',
                        '$mGasRemain->amount_gas_2',
                        '$mGasRemain->user_update_2',
                        '$this->driver_id',
                        '$this->car_id',
                        '$this->phu_xe_1',
                        '$this->phu_xe_2',
                        '$mGasRemain->weight_from_customer',
                        '$mGasRemain->pvkh_miss_kg',
                        '$mGasRemain->pvkh_miss_money',
                        '$mGasRemain->driver_miss_kg',
                        '$mGasRemain->driver_miss_money',
                        '$mGasRemain->status_employee_debit',
                        '$created_date',
                        '$mGasRemain->code_no'
                        )";
        }
        $this->makeGasRemainPayBack($aRowInsert, $aSeri, $mCustomer, $agent_id, $uid_login);
    }
    
    /** @Author: DungNT Mar 09, 2018
     *  @Todo: tính toán lại một số thông tin của 1 item gas remain
     **/
    public function calcInfoGasRemain(&$mGasRemain, $mAppOrderDetail) {
        $keySeri        = $mAppOrderDetail->materials_id.'-'.$mAppOrderDetail->seri;
        $mGasRemain->seri           = $mAppOrderDetail->seri;
        $mGasRemain->amount_empty   = $mAppOrderDetail->kg_empty;
        $mGasRemain->amount_has_gas = $mAppOrderDetail->kg_has_gas;
        $mGasRemain->has_export     = GasRemain::STATUS_NOT_EXPORT;
        $mGasRemain->price          = $this->price_bb;
        $mGasRemain->status_employee_debit = GasRemain::EMPLOYEE_DEBIT_YES;
        $mGasRemain->amount_gas_2   = $mGasRemain->user_update_2 = $mGasRemain->customer_id_of_agent = $mGasRemain->pvkh_miss_money = 0;
        $mGasRemain->weight_from_customer = $mGasRemain->pvkh_miss_kg = 0.00;
        if(isset($this->aGasRemainOld[$keySeri])){// LAN 2: giữ lại 1 số biến model cũ khi update web
            $mGasRemain->has_export     = $this->aGasRemainOld[$keySeri]->has_export;
            $mGasRemain->amount_gas_2   = $this->aGasRemainOld[$keySeri]->amount_gas_2;
            $mGasRemain->user_update_2  = $this->aGasRemainOld[$keySeri]->user_update_2;
            $mGasRemain->price          = $this->aGasRemainOld[$keySeri]->price;// khi update web thì giữ lại price record hiện tại 
            $mGasRemain->uid_login      = $this->aGasRemainOld[$keySeri]->uid_login;// Lần 2 khi update uid_login from web thì giữ lại uid_login
            // Mar0918
            $mGasRemain->weight_from_customer       = $this->aGasRemainOld[$keySeri]->weight_from_customer;// Lần 2 khi update uid_login from web thì giữ lại uid_login + ...
            $mGasRemain->pvkh_miss_kg               = $this->aGasRemainOld[$keySeri]->pvkh_miss_kg;
            $mGasRemain->pvkh_miss_money            = $this->aGasRemainOld[$keySeri]->pvkh_miss_money;
            $mGasRemain->driver_miss_kg             = $this->aGasRemainOld[$keySeri]->driver_miss_kg;
            $mGasRemain->driver_miss_money          = $this->aGasRemainOld[$keySeri]->driver_miss_money;
            $mGasRemain->status_employee_debit      = $this->aGasRemainOld[$keySeri]->status_employee_debit;
            $mGasRemain->code_no                    = $this->aGasRemainOld[$keySeri]->code_no;
            
            if($mGasRemain->user_update_2){
                $this->doneReweighed = true;// chưa xử lý, chờ review, vì code đã handle giữ lại cân lần 2 rồi -> - Jun2118 không update gas dư nếu Kho đã cân lần 2 - theo dõi xem có bị ảnh hưởng gì không, xem có cần đổi lại không
            }
        }
        if(isset($this->aGasRemainKeepPrice[$keySeri])){// LAN 1: Tài Xế Complete App đơn hàng đại lý
            $mGasRemain->price          = $this->aGasRemainKeepPrice[$keySeri]->price;// giữ price record của đại lý thu từ KH về
            $mGasRemain->uid_login      = $this->aGasRemainKeepPrice[$keySeri]->uid_login;// Lần 1 (Tài Xế Complete App) khi update uid_login from app thì giữ lại uid_login (là PVKH) của đại lý
            $mGasRemain->customer_id_of_agent = $this->aGasRemainKeepPrice[$keySeri]->customer_id;
            $mGasRemain->code_no        = $this->aGasRemainKeepPrice[$keySeri]->code_no;
            // uid_login ở aGasRemainKeepPrice là uid của PVKH ở đơn hàng đại lý phải giữ lại biến này để hỗ trợ tính tiền công nợ gas dư cho PVKH
            // Mar0918
            $mGasRemain->weight_from_customer = $this->aGasRemainKeepPrice[$keySeri]->amount_has_gas;// Mar0918 trọng lượng gas + vỏ cân lại ở KH, sẽ đc lưu lại khi xuất từ đại lý cho xe tải
            $mGasRemain->calcPvkhMissWeight();
        }
        $mGasRemain->buildCodeNo();// nếu add new thì tự sinh code_no
    }
    
    /** @Author: DungNT Aug 15, 2017
     * @Todo: tạo thêm row thiếu đầu vào nếu có
     */
    public function makeGasRemainPayBack(&$aRowInsert, &$aSeri, $mCustomer, $agent_id, $uid_login) {
        if($this->pay_back <= 0){
            return ;
        }
        $created_date           = date('Y-m-d H:i:s');
        $has_export             = GasRemain::STATUS_EXPORT;
        $status_employee_debit  = GasRemain::EMPLOYEE_DEBIT_NO;
        $amount_gas_2 = $user_update_2 = $customer_id_of_agent = 0;
        $weight_from_customer = $pvkh_miss_kg = $driver_miss_kg = 0.00; $pvkh_miss_money = $driver_miss_money = 0;
        $code_no = '';
        $aSeri[]    = $seri = GasRemain::SERI_TDV;
        $qtyRemain  = $this->pay_back;
        $amount     = $qtyRemain * $this->price_bb;// Thành tiền của gas dư
        $aRowInsert[]="('$this->date_delivery',
                        '$this->date_delivery_bigint',
                        '$this->customer_id',
                        '$mCustomer->parent_id',
                        '$mCustomer->sale_id',
                        '$mCustomer->is_maintain',
                        '$agent_id',
                        '$customer_id_of_agent',
                        '0',
                        '0',
                        '$seri',
                        '$this->price_bb',
                        '$amount',
                        '0',
                        '0',
                        '$qtyRemain',
                        '$uid_login',
                        '$has_export',
                        '$amount_gas_2',
                        '$user_update_2',
                        '$this->driver_id',
                        '$this->car_id',
                        '$this->phu_xe_1',
                        '$this->phu_xe_2',
                        '$weight_from_customer',
                        '$pvkh_miss_kg',
                        '$pvkh_miss_money',
                        '$driver_miss_kg',
                        '$driver_miss_money',
                        '$status_employee_debit',
                        '$created_date',
                        '$code_no'
                        )";
    }

    /**
     * @Todo: delete old record gas dư ứng với record
     * need test before run
     */
    public function makeGasRemainDeleteOld($aSeri) {
        $agent_id   = $this->makeGasRemainChangeAgentId();
        if(count($aSeri) < 1 || empty($agent_id) || empty($this->customer_id) || empty($this->date_delivery)){
            return ;
        }
        $this->getModelRemainOld($aSeri, $agent_id);
        $criteria   = new CDbCriteria();
        $this->makeGasRemainDeleteCondition($criteria, $aSeri, $agent_id);
        GasRemain::model()->deleteAll($criteria);
    }
    
    /** @Author: DungNT Dec 20, 2017
     *  @Todo: tách condition get model GasRemain cũ
     **/
    public function makeGasRemainDeleteCondition(&$criteria, $aSeri, $agent_id) {
        $this->getGasRemainCondition($criteria, $agent_id);
        $sParamsIn = implode(',', $aSeri);
        if($sParamsIn != ''){
            $criteria->addCondition("seri IN ($sParamsIn)");
        }
        $criteria->addCondition('source='.GasRemain::SOURCE_WEB);
    }
    
    /** @Author: DungNT Dec 20, 2017
     *  @Todo: get model GasRemain cũ xử lý giữ lại biến cân lần 2 của thủ kho
     * xử lý bấm lưu lại khi đã hoàn thành đơn hàng trên app Xe tải
     *  @note: hàm này sẽ không hỗ trợ sửa trên web vì sẽ bị trùng biến aGasRemainOld
     * không cần check GN hay tài xế, vì chỉ có tài xế mới được lưu sau khi hoàn thành
     **/
    public function getModelRemainOld($aSeri, $agent_id) {
        if($this->isWebUpdate || $this->status != GasAppOrder::STATUS_COMPPLETE){
            return ;
        }
        $criteria   = new CDbCriteria();
        $this->makeGasRemainDeleteCondition($criteria, $aSeri, $agent_id);
        $aGasRemain = GasRemain::model()->findAll($criteria);
        foreach($aGasRemain as $item){
            $keySeri = $item->materials_id.'-'.$item->seri;
            $this->aGasRemainOld[$keySeri] = $item;
        }
    }
    
    /** @Author: DungNT Now 09, 2017
     *  @Todo: xử lý find all seri cũ để giữ lại trạng thái đã xuất hoặc còn tồn của seri
     **/
    public function getGasRemainCondition(&$criteria, $agent_id) {
        $date = $this->date_delivery;
        if($this->isWebUpdate){
            $date = $this->dateDeliveryOld;
        }
        $criteria->addCondition("date_input='$date'");
        $criteria->addCondition('agent_id='.$agent_id);
        $criteria->addCondition('customer_id='.$this->customer_id );
    }
    
    /** @Author: DungNT Now 09, 2017
     *  @Todo: Hàm này xử lý cho web update, còn ở app thì sẽ không xử lý, vì khi web update sẽ set hết seri về trạng thái còn tồn.
     * xử lý find all seri cũ để giữ lại trạng thái đã xuất hoặc còn tồn của seri
     * hàm này chỉ find lại record đã insert của Kho PT, chứ không xử lý lấy record cũ của đại lý xuất cho xe
     **/
    public function makeGasRemainGetOldModel() {
        if(!$this->isWebUpdate){
            return ;
        }
        $agent_id   = $this->makeGasRemainChangeAgentId();
        if(empty($agent_id) || empty($this->customer_id) || empty($this->date_delivery)){
            return ;
        }
        $criteria   = new CDbCriteria();
        $this->getGasRemainCondition($criteria, $agent_id);
        $models = GasRemain::model()->findAll($criteria);
        foreach($models as $item){
            $keySeri = $item->materials_id.'-'.$item->seri;
            $this->aGasRemainOld[$keySeri] = $item;
        }
    }
    
    /**
     * @Author: DungNT Apr 10, 2017
     * @Todo: cập nhật hoàn thành các đơn hàng bò mối cho agent mới chạy APP
     */
    public static function updateCompleteNewAgentApp() {
        $from = time();
        $aAgent = [242369];$total = 0;
        
        foreach($aAgent as $agent_id){
            $criteria = new CDbCriteria();
            $criteria->addCondition('t.agent_id='.$agent_id );
            $models = GasAppOrder::model()->findAll($criteria);
            foreach($models as $item){
                $item->status       = GasAppOrder::STATUS_COMPPLETE;
                $item->update_by    = GasConst::UID_ADMIN;
                $item->update_time  = date('Y-m-d H:i:s');
                $item->update();
                $total++;
            }
        }
        $to = time();
        $second = $to-$from;
        Logger::WriteLog("updateCompleteNewAgentApp $total record". ' done in: '.($second).'  Second  <=> '.($second/60).' Minutes');
    }
    
    /**
     * @Author: DungNT Apr 19, 2017
     * @Todo: get AppOrder theo array obj_detail_id (obj_detail_id: Detail từng đơn hàng xe tải)
     * Sử dụng để update khi View Order xe tải
     */
    public function getByArrayOrderDetailId($aDetailId) {
        if(count($aDetailId) <1 ){
            return [];
        }
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',', $aDetailId);
        $criteria->addCondition("t.obj_detail_id IN ($sParamsIn)");
        $models = self::model()->findAll($criteria);
        $aRes   = [];
        foreach($models as $item){
            $aRes[$item->obj_detail_id] = $item;
        }
        return $aRes;
    }
    
    /**
     * @Author: DungNT May 05, 2017
     * @Todo: gửi lại 1 notify đến điều phối sau khi KH tạo đơn hàng mới
     */
    public function makeSocketNotify() {
        if(!GasCheck::isServerLive()){
            return '';
        }
        
        $this->titleNotify    = '<b>Liên hệ: '.$this->customer_contact.'</b><br>'.$this->getCustomer('first_name').' tạo mới đặt hàng bò mối trên APP KH';
        $json       = $needMore = [];// xử lý save vào db để send = cron bình thường, không cần thiết phải send ngay
        $mAppCache = new AppCache();
        $aIdDieuPhoiBoMoi = $mAppCache->getArrayIdRole(ROLE_DIEU_PHOI);
        foreach($aIdDieuPhoiBoMoi as $uid){
            GasSocketNotify::addMessage(0, GasConst::UID_ADMIN, $uid, GasSocketNotify::CODE_ORDER_BOMOI_NEW, $this->titleNotify, $json, $needMore);
        }
    }
    
    /**
     * @Author: DungNT Jun 26, 2019
     * @Todo: alert popupu notify đến điều phối sau khi PVKH hủy đơn
     */
    public function makeSocketNotifyCancelOrder() {
        if(!GasCheck::isServerLive()){
            return '';
        }
        
        $this->titleNotify    = $this->getUserInfo('rEmployeeMaintain' , 'first_name'). 'Hủy đơn: '. $this->getCodeNo().' '.$this->getCustomer('first_name').'. ' . $this->getAgent();
        $json       = $needMore = [];// xử lý save vào db để send = cron bình thường, không cần thiết phải send ngay
        $mAppCache          = new AppCache();
        $aIdDieuPhoiBoMoi   = $mAppCache->getArrayIdRole(ROLE_DIEU_PHOI);
        $aModelSocket   = []; // xử lý gửi 1 lần 1 list sự kiện
        $aUidNotSend    = [GasConst::UID_PHUONG_NT, 766823, 1029122];
        foreach($aIdDieuPhoiBoMoi as $toUserId){
            if(in_array($toUserId, $aUidNotSend)){
                continue ;
            }
            $aModelSocket[] = GasSocketNotify::addMessage(0, GasConst::UID_ADMIN, $toUserId, GasSocketNotify::CODE_ALERT_CALL_CENTER, $this->titleNotify, $json, $needMore);
        }
        $mSync = new SyncData(SyncData::SERVER_IO_SOCKET, 'socket/notifyAdd');
        $mSync->notifyAdd($aModelSocket);
    }
    
    /**
     * @Author: DungNT May 07, 2017 -- done test ok
     * @Todo: cập nhật lại gas dư nếu có chỉnh sửa ở phần quản lý gas dư
     * Vì cái này liên quan đến báo cáo daily để nhập vào phần mềm POS
     */
    public function updateBackGasRemain($mRemain) {
        return ;
        // không cho update gas dư nữa, chỉ có thể chỉnh sửa 1 chiều từ List GasAppOrder
        // Dec2617 có thể hàm này gây ra sai số tiền gas dư của đơn hàng 
        // Đặng Thị Thiện Kiểm tra giúp em phiếu này nhé A17TPINVP lại bị lỗi chi gas dư nữa rồi, hiện công nợ đang lệch 105.393 tương đương 3,8kg gas dư.
        $models = $this->getOrderOfCustomer();
        foreach($models as $model){
            $model->mapJsonFieldOneDecode('JSON_FIELD', 'json', 'baseArrayJsonDecode');
            $model->mapJsonFieldOneDecode('JSON_FIELD_INFO', 'json_info', 'baseArrayJsonDecodeV1');
            $aInfoVo = $model->info_vo;
            foreach($aInfoVo as $key => $detail){
                if(!empty($detail['seri']) && $detail['seri'] == $mRemain->seri){
                    $model->info_vo[$key]['kg_empty']     = $mRemain->amount_empty;
                    $model->info_vo[$key]['kg_has_gas']   = $mRemain->amount_has_gas;
                    $gasRemain = $mRemain->amount_has_gas - $mRemain->amount_empty;
                    $model->total_gas_du = $gasRemain * $model->price_bb;
                    $model->grand_total  = $model->total_gas - $model->total_gas_du;
                    
                    $model->setJsonDataField();
                    $model->update(['json', 'grand_total', 'total_gas_du']);
                    return ;
                }
            }
        }
    }
    
    /**
     * @Author: DungNT May 07, 2017
     * @Todo: lấy những đơn hàng trong ngày của KH
     */
    public function getOrderOfCustomer() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.customer_id=' . $this->customer_id .' AND t.agent_id=' . $this->agent_id);
        $criteria->addCondition("t.date_delivery='$this->date_delivery'");
        return self::model()->findAll($criteria);
    }
    
    public function canUpdateWeb() {
        $cRole      = MyFormat::getCurrentRoleId();
        $cUid       = MyFormat::getCurrentUid();
        if($cRole == ROLE_ADMIN){
            return true;
        }
        $aStatusAllow = [GasAppOrder::STATUS_NEW, GasAppOrder::STATUS_CANCEL];
        if(in_array($cUid, GasConst::getArrUidUpdateAll()) && !in_array($this->status, $aStatusAllow)){
            return GasConst::canUpdateAll($this->created_date);
        }
        $aRoleAllow = [ROLE_ADMIN, ROLE_DIEU_PHOI, ROLE_ACCOUNTING];
        if(!in_array($cRole, $aRoleAllow)){
            // chỉ cho update record tạo từ web và của đại lý đó, thỏa mãn ngày update
            return false;
        }
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, 60, '-');
        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }
    
    /**
     * @Author: DungNT May 13, 2017
     * @Todo: xử lý build data post from web like data from App
     * để sử dụng chung hàm handlePostDriverUpdate và hàm save :d hix...
     */
    public function handleSaveWebUpdate() {
        $aRes = [];
        foreach($_POST['materials_id'] as $key => $materials_id){
            $mDetail = new GasAppOrderDetail();
            $mDetail->materials_id      = $materials_id;
            $mDetail->materials_type_id = $_POST['materials_type_id'][$key];
            $mDetail->qty               = $_POST['qty'][$key];
            $mDetail->qty_real          = $_POST['qty_real'][$key];
            if(in_array($mDetail->materials_type_id, GasMaterialsType::getArrAllVo())){
                $mDetail->qty_real      = $mDetail->qty;
            }
            $mDetail->seri              = $_POST['seri'][$key];
            $mDetail->kg_empty          = $_POST['kg_empty'][$key];
            $mDetail->kg_has_gas        = $_POST['kg_has_gas'][$key];
            $mDetail->price             = isset($_POST['price'][$key]) ? MyFormat::removeComma($_POST['price'][$key]) : 0;
            $mDetail->amount            = 0;
            $aRes[] = $mDetail;
        }
        $q = new stdClass();
        $q->order_detail = $aRes;
        $aSeri = $this->getArraySeriVo();
        $this->handlePostDriverUpdate($q);
        $this->mapInfoCustomer();
        $this->setJsonNameUser();
        $this->setJsonDataField('JSON_FIELD_INFO', 'json_info');
        if($this->hasErrors()){
            throw new Exception(HandleLabel::FortmatErrorsModel($this->getErrors()));
        }
//        $this->makeStorecard();// Jun 02, 2017
        $this->handleSaveDriverUpdate();
        $this->getGasRemainCustomerExportOtherAgent();
    }

    /**
     * @Author: DungNT May 13, 2017
     * @Todo: convert date web to db 
     */
    public function setDbDate() {
        $this->date_delivery = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_delivery);
        MyFormat::isValidDate($this->date_delivery);
    }
    
    /** @Author: DungNT Sep 16, 2018 - list KH mối đc bấm nợ lấy đc set trong GasOneMany và set trong AppCache
     *  @Todo: get array id allow set debit - exception
     * update => http://spj.daukhimiennam.com/admin/gasmember/setAgentForUser?type=17&one_id=26676
     **/
    public function getArrayCustomerAllowDebit() {
        $mGasOneMany = new GasOneMany();
        return $mGasOneMany->getCacheCustomerAllowDebit();
    }
    
    /** @Author: DungNT Sep 15, 2018
     *  @Todo: check before set debit
     **/
    public function checkSetDebit() {
        if(in_array($this->customer_id, $this->getArrayCustomerAllowDebit())){
            return ;
        }
        $mCustomer = Users::model()->findByPk($this->customer_id);
        if($mCustomer->is_maintain == STORE_CARD_KH_MOI){
            throw new Exception('Khách hàng mối không bán nợ, vui lòng liên hệ chuyên viên hoặc giám đốc khu vực');
        }
        if($mCustomer->payment_day == GasTypePay::L1_1_DAY){
            throw new Exception('Khách hàng này không bán nợ, vui lòng liên hệ nhân viên kinh doanh để chỉnh lại hạn thanh toán của khách hàng');
        }// Dec3018 tam close lai, qua le Open
    }
    
    /**
     * @Author: DungNT May 19, 2017
     * @Todo: set nợ cho đơn hàng, vì mặc định là đã trả tiền
     */
    public function setDebit() {
        $this->checkSetDebit();
        $this->status_debit = GasAppOrder::DEBIT_YES;
        $this->update(['status_debit']);
        $this->removePayCashbook();
        $this->saveFile();
        $this->deleteFile();
        $this->saveEvent(GasAppOrder::WEB_SET_ORDER_DEBIT, '');
        $mEmail = new SendEmail();
        $mEmail->alertOrderDebit($this);
    }
    
    public function getStatusDebit() {
        if($this->status != GasAppOrder::STATUS_COMPPLETE){
            return '';
        }
        $res = 'Thu tiền';
        if($this->status_debit == GasAppOrder::DEBIT_YES){
            $res = 'KH Nợ';
        }
        return $res;
    }
    
    /**
     * @Author: DungNT May 19, 2017
     * @Todo: xóa sổ quỹ khi bấm đơn hàng nợ
     * làm action xóa sổ quỹ khi hủy xác nhận, hủy đơn hàng, btn KH nợ
     */
    public function removePayCashbook() {
        $mEmployeeCashbook = new EmployeeCashbook();
        $mEmployeeCashbook->app_order_id        = $this->id;
        $mEmployeeCashbook->master_lookup_id    = [GasMasterLookup::ID_THU_TIEN_KH, GasMasterLookup::ID_CHI_GAS_DU];
        $models = $mEmployeeCashbook->getByAppOrderId(['GetAll'=>1]);
        foreach($models as $item){// Oct0101 xử lý lại findAll
            $item->delete();
        }
    }
    /**
     * @Author: DungNT May 19, 2017
     * @Todo: auto add sổ quỹ khi NV xác nhận giao hàng
     * làm action tự tạo sổ quỹ khi bấm xác nhận
     */
    public function autoAddPayCashbook() {
        $amount = round($this->getTotalGasHavePromotion());
        $this->removePayCashbook();
        if($this->status != GasAppOrder::STATUS_COMPPLETE || $this->status_debit == GasAppOrder::DEBIT_YES || $amount == 0){// Jul 18, 2017 đơn hàng nợ thì không được tạo phiếu thu, hoặc chi gas dư nữa
            return ;
        }
        /* 1. remove nếu có tạo rồi
         * 1.2 save ở EmployeeCashbook list thu chi trên app GN
         * 2. save bên GasCashBookDetail -> sổ quỹ chính thức của GN
         */
        $mEmployeeCashbook = new EmployeeCashbook();
        $mEmployeeCashbook->customer_id         = $this->customer_id;
        $mEmployeeCashbook->app_order_id        = $this->id;
        $mEmployeeCashbook->master_lookup_id    = GasMasterLookup::ID_THU_TIEN_KH;
        $mEmployeeCashbook->amount              = $amount;// fix function round Aug0817
        $mEmployeeCashbook->date_input          = $this->date_delivery;
        $mEmployeeCashbook->uid_login           = $this->employee_maintain_id;
        if($this->type == GasAppOrder::DELIVERY_BY_CAR){
            $mEmployeeCashbook->uid_login       = $this->driver_id;
        }
        $mEmployeeCashbook->employee_id         = $mEmployeeCashbook->uid_login;
        $mEmployeeCashbook->agent_id            = $this->agent_id;
        $mEmployeeCashbook->note                = $this->getNoteOnly();
        $mEmployeeCashbook->save();
        $mEmployeeCashbook->updateCashbookAgent();
    }
    
    /********** FOR FILE HANDLE ************************/
     /**
     * @Author: DungNT Oct 27, 2015
     * @Todo: Api validate multi file
     * @param: $this là model chứa error
     * ở đây là model GasUpholdReply
     */
    public function apiValidateFile() {
    try{
        $ok=false; // for check if no file submit
        if(isset($_FILES['file_name']['name'])  && count($_FILES['file_name']['name'])){
            foreach($_FILES['file_name']['name'] as $key=>$item){
                $mFile = new GasFile('UploadFile');
                $mFile->file_name  = CUploadedFile::getInstanceByName( "file_name[$key]");
                $mFile->validate();
                if(!is_null($mFile->file_name) && !$mFile->hasErrors() ){
                    $ok=true;
                    MyFormat::IsImageFile($_FILES['file_name']['tmp_name'][$key]);
//                    $FileName = MyFunctionCustom::remove_vietnamese_accents($mFile->file_name->getName());
//                    if(strlen($FileName) > 100 ){
//                        $mFile->addError('file_name', "Tên file không được quá 100 ký tự, vui lòng đặt tên ngắn hơn");
//                    }// không cần lấy file_name
                }
                if($mFile->hasErrors()){
                    $this->addError('file_name', $mFile->getError('file_name'));
                }
            }
        }

        if(!$ok && $this->requiredFile){
            $this->addError('file_name', 'Có lỗi. Bạn phải chụp hình chứng từ của đơn hàng');
        }
    } catch (Exception $ex) {
        throw new Exception($ex->getMessage());
    }
    }
    
    /** @Author: DungNT May 18, 2017
     * @Todo: save record file if have
     */
    public function saveFile() {
        // 5. save file
        if(!is_null($this->id)){
            GasFile::ApiSaveRecordFile($this, GasFile::TYPE_10_APP_BO_MOI);
        }
    }
    public function deleteFile() {
        if(!is_array($this->list_id_image) || count($this->list_id_image) < 1){
            return ;
        }
        foreach($this->list_id_image as $file_id){
            $mFile = GasFile::model()->findByPk($file_id);
            if(is_null($mFile)){
                continue;
            }
            if($mFile->type == GasFile::TYPE_10_APP_BO_MOI){
                $mFile->delete();
            }
        }
    }
    /** @Author: DungNT May 27, 2016
    *   @Todo: get html file view on grid
    */
    public function getFileOnly() {
        $res = '';
        $cmsFormater = new CmsFormatter();
        $res .= $cmsFormater->formatBreakTaskDailyFile($this, array('width'=>30, 'height'=>30));
        return $res;
    }
    /********** FOR FILE HANDLE ************************/
    
    /** @Author: DungNT Jul 12, 2017
     * @Todo: get record to monitor Order
     */
    public function getDataMonitor($needMore = []) {
        $criteria = new CDbCriteria();
        if(!empty($this->agent_id)){
            $criteria->addCondition('t.agent_id='.$this->agent_id);
        }
        if(!empty($this->uid_login)){
            $criteria->addCondition('t.uid_login='.$this->uid_login);
        }
        $sParamsIn = implode(',', UsersExtend::getAgentForwardPhone());
        $criteria->addCondition("t.agent_id IN ($sParamsIn)");
        $criteria->addCondition("t.status IN ($this->status)");
        $criteria->addCondition("t.date_delivery='$this->date_delivery'");
        $criteria->addCondition("DATE(t.created_date)='$this->date_delivery'");
        $criteria->order = 't.id ASC';
        if(isset($needMore['order'])){
            $criteria->order = $needMore['order'];
        }
//        $criteria->limit = 15;
        return self::model()->findAll($criteria);
    }
    
    /** for HISTORY CUSTOMER STORE CARD
     * @Author: DungNT Now 22, 2017
     * @Todo: get history of customer Bò mối 3 latest Order
     */
    public function windowGetHistory($needMore = []) {
        if(empty($this->customer_id)){
            return [];
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.customer_id='.$this->customer_id);
        $criteria->limit = 8;
        $criteria->order = "t.id DESC";
        return self::model()->findAll($criteria);
    }
    
    /**
     * @Author: DungNT Now 22, 2017
     * @Todo: chỉ sử dụng hàm này để get name điều phối của thẻ kho. không sử dụng cho các loại user khác
     */
    public function getUidLoginByCache() {
        if(isset(GasTickets::$DIEU_PHOI_FULLNAME[$this->uid_login])){
            return GasTickets::$DIEU_PHOI_FULLNAME[$this->uid_login];
        }
        $listdataCallCenter = CacheSession::getListdataByRole([ROLE_CALL_CENTER, ROLE_DIEU_PHOI]);
        if(isset($listdataCallCenter[$this->uid_login])){
            return $listdataCallCenter[$this->uid_login];
        }
        return '';
    }
    
    public function getEventLog() {
        $mTransactionEvent = new TransactionEvent();
        $mTransactionEvent->transaction_history_id = $this->id;
        $mTransactionEvent->type = TransactionEvent::TYPE_BO_MOI;
        return $mTransactionEvent->getToday();
    }
    
    /** @Author: DungNT Aug 01, 2017
     * @Todo: kiểm tra đơn hàng tạo của KH phải cách nhau 5 phút, không bấm nhiều 
     */
    public function checkLimitCreate() {
        $model = $this->getOrderLatest();
        if($model && GasCheck::isServerLive()){
            $minutes = 1; $now = date('Y-m-d H:i:s');
            $latestTime  = MyFormat::addDays($model->created_date, $minutes, '+', 'minutes', 'Y-m-d H:i:s');
            if(MyFormat::compareTwoDate($latestTime, $now)){
                $info = "Bạn đã đặt hàng thành công đơn hàng ngày {$model->getDateDelivery()}. Vui lòng chờ $minutes phút để đặt tiếp"; Logger::WriteLog("Customer: $model->customer_id $info");
                throw new Exception($info);
            }
        }
        return true;
    }
    
    public function checkChanHang() {
        $mStoreCard = new GasStoreCard();
        $mStoreCard->customer_id = $this->customer_id;
        $mStoreCard->checkChanHang();
        if($mStoreCard->hasErrors('customer_id')){
            throw new Exception($mStoreCard->getError('customer_id'));
        }
    }
    
    /** @Author: DungNT Aug 01, 2017
     * @Todo: get đơn hàng mới nhât KH
     */
    public function getOrderLatest() {
        $today = date('Y-m-d');
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.customer_id=' . $this->customer_id);
        $criteria->addCondition("DATE(t.created_date)='$today' AND t.uid_login_role=".ROLE_CUSTOMER);
        $criteria->order = 't.id DESC';
        return self::model()->find($criteria);
    }
    
    /** @Author: DungNT Aug 05, 2017
     * @Todo: get list data phụ xe theo id điều xe
     */
    public function getListdataPhuXe($user_id_executive) {
        return Users::getSelectByRoleForAgent($user_id_executive, ONE_AGENT_PHU_XE, '', array('status'=>1, 'GetNameOnly'=>1));
    }
    
    public function setListdataPhuXe() {
        if(isset($_SESSION['listdataPhuXe'])){
            return ;
        }
        $aPhuXePhuocTan = $this->getListdataPhuXe(GasConst::UID_DIEUXE_KHO_PHUOCTAN);
        $aPhuXeBenCat   = $this->getListdataPhuXe(GasConst::UID_DIEUXE_KHO_BENCAT);
        $this->aPhuXe = $aPhuXePhuocTan + $aPhuXeBenCat;
        $_SESSION['listdataPhuXe'] = $this->aPhuXe;
    }
    
    /** @Author: DungNT Aug 15, 2017
    * @Todo: xóa một số định dạng number
     */
    public function removeSomeFormat() {
        $this->discount = MyFormat::removeComma($this->discount);
    }
    public function resetRelation() {
        $this->rCustomer = Users::model()->findByPk($this->customer_id);
    }
    // Dec0217 dùng cho audit cập nhật đơn hàng hoàn thành sai vị trí của KH
    public function setLocationWrong() {
        $this->reason_false = GasAppOrder::WRONG_LOCATION;
        $this->update(['reason_false']);
        $this->setLocationCustomer('');
    }
    // Dec0217 dùng cho audit vị trí của KH
    public function setLocationCustomer($latLong) {
        $mCustomer = Users::model()->findByPk($this->customer_id);
        $mCustomer->slug = $latLong;
        $mCustomer->update(['slug']);
        if(!empty($latLong)){
            $this->reason_false = 0;
            $this->update(['reason_false']);
        }
    }
    
    public function isOrderAgent() {// Jan1518 kiểm tra đơn hàng có phải của xe tài giao cho đại lý không
        $ok =  false;
        if($this->type == GasAppOrder::DELIVERY_BY_CAR && $this->type_of_order_car == GasOrders::TYPE_AGENT){
            $ok =  true;
        }
        return $ok;
    }
    public function isOrderCarOfCustomer() {// Aug2019 kiểm tra đơn hàng có phải của xe tài giao cho KH không
        $ok =  false;
        if($this->type == GasAppOrder::DELIVERY_BY_CAR && $this->type_of_order_car != GasOrders::TYPE_AGENT){
            $ok =  true;
        }
        return $ok;
    }
    
    /** @Author: DungNT Mar 10, 2018
     *  @Todo: check lỗi hoàn thành Bò Mối 180 phút - đơn hàng giao ngay
     **/
    public function checkErrorComplete() {
        if($this->type == GasAppOrder::DELIVERY_BY_CAR){
            return;
        }
        $datetimeCheck = $this->created_date;
        if(!empty($this->delivery_timer) && $this->delivery_timer != '0000-00-00 00:00:00'){
            $datetimeCheck = $this->delivery_timer;
        }
        
        $mTransactionHistory = new TransactionHistory();
        if(!$mTransactionHistory->isOverTime($datetimeCheck, MonitorUpdate::TIME_COMPLETE_BO_MOI)){
            return ;
        }
        /*  vượt thời gian 60 phút -> tính lỗi
         * 1. save table Errors MonitorUpdate
         * 2. make notify to pvkh
         */
        $mMonitorUpdate = new MonitorUpdate();
        $mMonitorUpdate->saveAllCaseError(MonitorUpdate::TYPE_ERROR_BOMOI_COMPLETE, $this->employee_maintain_id, $this->id);
        
        $aUid   = [$this->employee_maintain_id];
        $this->titleNotify = $this->code_no. ' Bạn bị lỗi hoàn thành ĐH Bò Mối quá '.MonitorUpdate::TIME_COMPLETE_BO_MOI.' phút';
        $this->runInsertNotify($aUid, $this->titleNotify, false); // Send By Cron Feb 23, 2017
        $sendEmail = new SendEmail();
        $sendEmail->alertCompleteOrderLate($this);
    }
    
    /** @Author: DungNT Mar 29, 2018
     *  @Todo: get số phút hoàn thành vượt quy định
     **/
    public function getTimeOver() {
        $mTransactionHistory = new TransactionHistory();
        if($this->type == GasAppOrder::DELIVERY_BY_CAR || empty($this->complete_time) || !$mTransactionHistory->isOverTime($this->created_date, MonitorUpdate::TIME_COMPLETE_BO_MOI, $this->complete_time)){
            return '';
        }
        return $this->getTimeOverOnlyNumber();
    }
    public function getTimeOverOnlyNumber() {
        if($this->type == GasAppOrder::DELIVERY_BY_CAR || empty($this->complete_time)){
            return '';
        }
        return MyFormat::getMinuteTwoDate($this->created_date, $this->complete_time);
    }
    /** @Author: DungNT Mar 29, 2018
     *  @Todo: get số phút hoàn thành vượt quy định, view on web
     **/
    public function getTimeOverWebView() {
        if($this->status != GasAppOrder::STATUS_COMPPLETE){
            return '';
        }
        $minutes = $this->getTimeOver();
        if(empty($minutes)){
            return '';
        }
        return "($minutes phút)";
    }
    /** @Author: DungNT Aug 08, 2018
     *  @Todo: check some condition User can complete order 
     **/
    public function checkCompleteApp() {
//        return ;// chờ update app thì mới check
//        if( !in_array($this->car_id, $this->getArrayCarRunStt()) || $this->hasGas6kg ){// Close Aug3118
        if($this->hasGas6kg){ 
            return ;// chỉ check những xe test, những xe còn lại return 
        }
        // Aug2718 fix nếu để $this->total_gas > 0 thì sẽ bị bỏ qua check STT đơn của đại lý
//        if($this->total_gas > 0 && $this->showConfirmStt() && $this->update_stt != $this->type_confirm_real){
        if($this->showConfirmStt() && $this->update_stt != $this->type_confirm_real){
            throw new Exception('Bạn chưa xác nhận STT, không thể hoàn thành đơn hàng');
        }
    }
    /** @Author: DungNT Apr 01, 2018
     *  @Todo: check số xe khi hoàn thành đơn xe tải
     **/
    public function checkCarNumber() {
        if($this->type != GasAppOrder::DELIVERY_BY_CAR){
            return;
        }
        if(empty($this->car_id)){
            throw new Exception('Chưa nhập số xe, bạn gọi cho điều xe cập nhật đúng số xe để hoàn thành');
        }
    }
    public function formatDbDatetime() {
        if(strpos($this->delivery_timer, '/')){
            $this->delivery_timer = MyFormat::datetimeToDbDatetime($this->delivery_timer);
        }
    }
    public function getDeliveryTimer() {
        if(empty($this->delivery_timer) || $this->delivery_timer == '0000-00-00 00:00:00'){
            return '';
        }
        return MyFormat::dateConverYmdToDmy($this->delivery_timer, 'H:i d/m');
    }
    // get thời gian hẹn giao hàng
    public function getDeliveryTimerGrid($fromApp = false) {
        $text = $this->getDeliveryTimer();
        if(empty($text)){
            return '';
        }
        $res = "Hẹn giao: $text";
        if($fromApp){
            return "[$res] ";
        }
        return $res;
    }

    /** @Author: HOANG NAM 25/07/2018
     *  @Todo: type : Nếu không tồn tại stock nào => chưa xuất 0-chưa xuất , 1 đã xuất
     **/
    public function getType(){
//        $criteria=new CDbCriteria;
//        $criteria->compare('t.app_order_id', $this->id);
//        $aModel = Stock::model()->find($criteria);
//        if(empty($aModel)){
        if($this->update_stt == $this->type_stock_new){
            return '0';
        }
        return '1';
    }
    
    /**
     * 
     * @param obj $q
     * @param type $criteria
     */
    public function setMoreAdvance($q,&$criteria){
        if($this->apiStockList == GasAppOrder::API_STOCK){
            if($q->type == $this->type_stock_new){
                $criteria->compare('t.update_stt', $q->type);
            }else{
                $criteria->addCondition('t.update_stt != '.$this->type_stock_new);
            }
        }
        if(!empty($q->driver_id)){
            $criteria->compare('t.driver_id', $q->driver_id);
        }
        if(!empty($q->car_id)){
            $criteria->compare('t.car_id', $q->car_id);
        }
    }
    /** @Author: Pham Thanh Nghia 2018
     *  @Todo: cho phép người dùng tạo stock mới gửi app_order_id từ admin/gasAppOrder/ sang admin/gasAppOrder/createStock
     **/
    public function canCreateStock(){
        $cUid = MyFormat::getCurrentUid();
        $ok = false;
        if($cUid == GasConst::UID_ADMIN){
            $ok = true;    
        }
        return $ok;
    }
    /** @Author: Pham Thanh Nghia Oct 5, 2018
     *  @Todo: admin/gasAppOrder/soldOut
     **/
    public function canExportExcelReport(){
        return true;
    }
    
    /** @Author: DuongNV Oct 10, 2018
     *  @Todo: báo cáo khách hàng tiềm năng
     **/
    public function appReportPotential() {
        $criteria = new CDbCriteria();
        $aIdPotential = $this->getArrayPotentialCustomer();
        $aParamsIn = implode(",", $aIdPotential);
        $criteria->addCondition('t.user_id IN (' . $aParamsIn . ')');
        $criteria->addCondition('t.type = '.MonitorUpdate::TYPE_ACTIVE_USER);
        $criteria->addCondition('t.status_obj='.ROLE_CUSTOMER);
        $criteria->compare('t.agent_id', $this->agent_id);
        if(isset($this->date_from) && $this->date_from != ''){
            $from_ymd = MyFormat::dateConverDmyToYmd($this->date_from, "-");
            $criteria->addCondition('t.last_update >= "'. $from_ymd .'"');
        }
        if(isset($this->date_to) && $this->date_to != ''){
            $to_ymd = MyFormat::dateConverDmyToYmd($this->date_to, "-");
            $criteria->addCondition('t.last_update <= "'. $to_ymd .'"');
        }
        $criteria->select   = 'count(t.id) as id, t.agent_id, DATE(t.last_update) as last_update';
        $criteria->group    = 't.agent_id, DATE(t.last_update)';
        $criteria->order    = 'DATE(t.last_update)';
        $mRes = MonitorUpdate::model()->findAll($criteria);
        $aRes = [];
        $aRes['SALE_ID'] = $aRes['DATA'] = $aRes['DATE'] = $aRes['SUM_ROW'] = $aRes['SUM_COLUMN'] = [];
        foreach($mRes as $item){
            if(!in_array($item->agent_id, $aRes['SALE_ID'])){
                $aRes['SALE_ID'][] = $item->agent_id;
            }
            $aRes['DATA'][$item->agent_id][$item->last_update]  = $item->id;
            if(isset($aRes['SUM_COLUMN'][$item->last_update])){
                $aRes['SUM_COLUMN'][$item->last_update] += $item->id;
            } else {
                $aRes['SUM_COLUMN'][$item->last_update] = $item->id;
            }
            if(isset($aRes['SUM_ROW'][$item->agent_id])){
                $aRes['SUM_ROW'][$item->agent_id] += $item->id;
            } else {
                $aRes['SUM_ROW'][$item->agent_id] = $item->id;
            }
        }
        $aRes['DATE'] = MyFormat::getArrayDay($from_ymd, $to_ymd);
        // Sort array by sum of sale_id
        uasort($aRes['DATA'], function($a, $b){
                $sumA = array_sum($a);
                $sumB = array_sum($b);
                if($sumA == $sumB) return 0;
                return ($sumA > $sumB) ? -1 : 1;
            });
        return $aRes;
    }
    
    /** @Author: DuongNV Oct 10, 2018
     *  @Todo: get array id kh tiềm năng (Dùng join load không được)
     **/
    public function getArrayPotentialCustomer(){
        $criteria = new CDbCriteria;
        $criteria->compare('t.is_maintain', UsersExtend::STORE_CARD_FOR_QUOTE);
        $mCustomer = Users::model()->findAll($criteria);
        $aRes = [];
        foreach($mCustomer as $item){
            $aRes[] = $item->id;
        }
        return $aRes;
    }

    /** @Author: DuongNV Feb 27,2018
     *  @Todo: get array kh đặt app trong vòng 3 tháng
     **/
    public function getArrayRecentUseApp() {
        $criteria   = new CDbCriteria;
        $criteria->select = array('distinct(t.customer_id)');
        $criteria->compare('t.status', self::STATUS_COMPPLETE);
        $criteria->compare('t.uid_login_role', ROLE_CUSTOMER);
        $last3Month = strtotime("-3 months");
        DateHelper::searchGreater($last3Month, 'date_delivery_bigint', $criteria);
        $models     = GasAppOrder::model()->findAll($criteria);
        $aUid       = [];
        foreach ($models as $value) {
            $aUid[$value->customer_id] = $value->customer_id;
        }
        return $aUid;
    }
    
    /** @Author: DungNT Mar 06, 2019 
     *  @Todo: check user can set debit on app, allow one click
     **/
    public function canSetDebitOnApp() {
        $mTransactionEvent = new TransactionEvent();
        $mEventSetDebit = $mTransactionEvent->getByObjIdAndType($this->id, TransactionEvent::TYPE_BO_MOI, GasAppOrder::WEB_SET_ORDER_DEBIT);
        if($mEventSetDebit){
            $this->addError('id', 'Đơn hàng này đã bấm nợ 1 lần, không thể bấm nợ thêm nữa. Liên hệ kế toán khu vực để hỗ trợ');
            return false;
        }
        return true;
    }
    
    /** @Author: DungNT Apr 03, 2019
     *  @Todo: get url update customer from grid
     **/
    public function getUrlUpdateCustomer() {
        $aUidAllow = [GasConst::UID_PHUONG_NT, GasConst::UID_ADMIN];
        $cUid = MyFormat::getCurrentUid();
        if(!in_array($cUid, $aUidAllow)){
            return '';
        }
        $link = Yii::app()->createAbsoluteUrl('admin/gascustomer/update_customer_store_card', ['id' => $this->customer_id]);
        return "<a href='$link' target='_blank'>Update Customer</a>";
    }
    
    /** @Author: DungNT Aug 20, 2019
     *  @Todo: validate receipts_no when input from app bò mối
     **/
    public function checkReceiptsNo() {
        if(empty($this->receipts_no)){
            return ;
        }
        if($this->isOrderAgent()){
            return ;
        }
        if($this->isWebUpdate){
            if($this->modelOld->receipts_no == $this->receipts_no){
                return ;// not change
            }
            $this->removeReceiptsNo();
        }
        $mGasReceipts = $this->initModelReceipt();
        if(!$mGasReceipts->assignNumber()){
            $this->addError('id', HandleLabel::FortmatErrorsModel($mGasReceipts->getErrors()));
        }
    }
    
    /** @Author: DungNT Aug 20, 2019
     *  @Todo: init data for model Receipts
     **/
    public function initModelReceipt() {
        $mGasReceipts = new GasReceipts();
        $mGasReceipts->object_id        = $this->employee_maintain_id;// PVKH or PTTT
        if($this->isOrderCarOfCustomer()){
            $mGasReceipts->object_id    = $this->car_id;// id xe tai
        }
        $mGasReceipts->number           = $this->receipts_no;
        return $mGasReceipts;
    }
    /** @Author: DungNT Aug 20, 2019
     *  @Todo: remove old receipts_no when web change receipts_no
     **/
    public function removeReceiptsNo() {
        $mGasReceipts = $this->initModelReceipt();
        $mGasReceipts->removeNumber();
    }
    
}
