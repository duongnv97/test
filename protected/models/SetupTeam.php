<?php

/**
 * This is the model class for table "{{_setup_team}}".
 *
 * The followings are the available columns in table '{{_setup_team}}':
 * @property string $id
 * @property integer $type
 * @property string $user_id
 * @property string $date_apply
 * @property string $json
 * @property string $created_date
 * @property string $created_by
 */
class SetupTeam extends BaseSpj
{
    public $date_from, $date_to, $autocomplete_name, $autocomplete_name_2, $user_create, $user_create_search;
    public $aEmployeeOfKtkv; // DuongNV Aug2919 ds nv (bao tri, pvkh) thuoc cac dai ly do ktkv quan ly
    
    const TYPE_KTKV_AGENT 	= 1; // list agent của ktkv 
    const TYPE_EMPLOYEE_PTTT 	= 2; // list NV pttt của chuyên viên 
    const TYPE_NOT_CONTACT 	= 3; // không liên lạc được với User 
    
    public function getArrayType(){
        return [
            self::TYPE_KTKV_AGENT 	=> "Kế toán khu vực - Đại lý", // list agent của ktkv 
            self::TYPE_EMPLOYEE_PTTT 	=> "Nhân viên PTTT", // list NV pttt của chuyên viên 
            self::TYPE_NOT_CONTACT 	=> "Không liên lạc được", // không liên lạc được với User 
        ];
    }
    
    public function getArrayTypeSearch(){
        return [
            self::TYPE_KTKV_AGENT 	=> "Kế toán khu vực - Đại lý", // list agent của ktkv 
            self::TYPE_EMPLOYEE_PTTT 	=> "Nhân viên PTTT", // list NV pttt của chuyên viên 
        ];
    }
    
    
    /** @Author: ANH DUNG Now 05, 2018
     *  @Todo: get list role can view button ko liên lạc đc 
     **/
    public function canMakeNotContact() {
        $cRole = MyFormat::getCurrentRoleId();
        $aRoleAllow = [ROLE_ADMIN, ROLE_CALL_CENTER, ROLE_DIEU_PHOI];
        return in_array($cRole, $aRoleAllow);
    }
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return SetupTeam the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_setup_team}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['user_id, type, date_apply', 'required', 'on' => 'create'],
            ['note, user_create, id, type, user_id, date_apply, json, created_date, created_by', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'rUser' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'rCreatedBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'type' => 'Loại',
            'user_id' => 'Nhân viên',
            'date_apply' => 'Ngày áp dụng',
            'created_date' => 'Ngày tạo',
            'created_by' => 'Người tạo',
            'date_from' => 'Từ ngày',
            'date_to' => 'Đến ngày',
            'note' => 'Ghi chú',
            'json' => 'Chi tiết',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

	$criteria->compare('t.type',$this->type);
	$criteria->compare('t.user_id',$this->user_id);
        if(!empty($this->date_apply)){
            $date_apply = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_apply);
            $criteria->compare('t.date_apply', $date_apply);
        }
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    protected function beforeSave() {
        return parent::beforeSave();
    }
    
    public function getExistsTeam(){
        if(empty($this->type) || empty($this->user_id)){
            return null;
        }
        $dateApply = MyFormat::dateConverDmyToYmd($this->date_apply, '/');
        $criteria = new CDbCriteria;
        $criteria->compare('t.type', $this->type);
        $criteria->compare('t.user_id', $this->user_id);
        $criteria->compare('t.date_apply', $dateApply);
        return SetupTeam::model()->find($criteria);
    }
    
    /** @Author: DuongNV Oct 30, 2018
     *  @Todo: change url autocomplete when change type admin/setupTeam/create
     **/
    public function getUrlAutocomplete() {
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login');
        if($this->type == SetupTeam::TYPE_KTKV_AGENT){
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', ['role' => ROLE_AGENT]);
        }
        return $url;
    }
    
    public function canUpdate() {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        if($cRole == ROLE_ADMIN){
            return true;
        }
        if($cUid != $this->created_by){
            return false;
        }
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, 90, '-');// cho phép update trong ngày
        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }
    
    public function canShowFormInput() {
        if(!empty($this->type) && !empty($this->date_apply) && !empty($this->user_id)):
            return true;
        endif;
        return false;
    }
    
    public function getType() {
        $aType = $this->getArrayType();
        return isset($aType[$this->type]) ? $aType[$this->type] : '';
    }
    public function getUser() {
        $mUser = $this->rUser;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    public function getCreatedBy() {
        $mUser = $this->rCreatedBy;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    
    public function getNote() {
        return nl2br($this->note);
    }
    public function getDateApply() {
        return MyFormat::dateConverYmdToDmy($this->date_apply, 'd/m/Y');
    }
    public function getDateApplyShort() {
        return substr($this->date_apply, -7);
    }
    public function getJson() {
        $aData  = json_decode($this->json, true);
        if(!is_array($aData)){
            return [];
        }
        return $aData;
    }
    
    public function getJsonView() {
        $aData  = $this->getJson();
        $sRet   = ''; $index = 1;
        foreach($aData as $item){
            $sRet   .= $index++.". $item<br>";
        }
        return $sRet;
    }
    /** @Author: ANH DUNG Now 01, 2018
     *  @Todo: init data when create
     **/
    public function initDataCreate() {
        $this->date_apply  = date('d/m/Y');
        $this->type        = SetupTeam::TYPE_KTKV_AGENT;
    }
    /** @Author: ANH DUNG Oct 31, 2018
     *  @Todo: format before render view
     **/
    public function formatDataBeforeUpdate() {
        $this->date_apply = MyFormat::dateConverYmdToDmy($this->date_apply);
    }
    
    /** @Author: ANH DUNG Oct 31, 2018
     *  @Todo: format before save to db
     **/
    public function formatDataBeforeSave() {
        $this->json = MyFormat::jsonEncode($this->user_create);
        if(strpos($this->date_apply, '/')){
            $this->date_apply = MyFormat::dateConverDmyToYmd($this->date_apply);
            MyFormat::isValidDate($this->date_apply);
        }
    }
    
    /** @Author: ANH DUNG Now 02, 2018
     *  @Todo: set some cache
     **/
    public function setSomeCache() {
        $mAppCache = new AppCache();
        $mAppCache->setListdataKtkvAgent();
    }
    
    /** @Author: ANH DUNG Now 02, 2018
     *  @Todo: copy some type to new month
     **/
    public function cronCopyNewMonth() {
        if(date('t') != date('d')){// chỉ chạy ngày cuối tháng
            return ;
        }
        $models = $this->cronCopyGetData();
        foreach($models as $item){
            $newRecord = new SetupTeam();
            $aFieldNotCopy = array('id');
            MyFormat::copyFromToTable($item, $newRecord, $aFieldNotCopy);
            $newRecord->date_apply      = MyFormat::modifyDays($item->date_apply, 1, '+', 'month');
            $newRecord->created_date    = $this->getCreatedDateDb();
            $newRecord->save();
        }
        $this->setSomeCache();
        Logger::WriteLog("cronCopyNewMonth SetupTeam done ".date('Y-m-d H:i:s'));
    }
    
    /** @Author: ANH DUNG Now 02, 2018
     *  @Todo: belogn to cronCopyNewMonth get data
     **/
    public function cronCopyGetData() {
        $aTypeCopy = [SetupTeam::TYPE_KTKV_AGENT];
        if(empty($this->date_from)){
            $this->date_from  = date('Y-m').'-01';
        }
        if(empty($this->date_to)){
            $this->date_to    = date('Y-m-t');
        }
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition('t.date_apply', $this->date_from, $this->date_to);
        $criteria->addInCondition('t.type', $aTypeCopy);
        return SetupTeam::model()->findAll($criteria);
    }
    
    /** @Author: ANH DUNG Now 02, 2018
     *  @Todo: get list agent của 1 list user by type 
     **/
    public function getByType($aType) {
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition('t.date_apply', $this->date_from, $this->date_to);
        $criteria->addInCondition('t.type', $aType);
        $models = SetupTeam::model()->findAll($criteria);
        $aRes = [];
        foreach($models as $item){
            $aData  = json_decode($item->json, true);
            if(!is_array($aData)){
                continue ;
            }
            foreach($aData as $agent_id => $agent_name){// chỗ này có thể là $agent_id hoặc user_id tùy theo type, với  type KTKV thì là $agent_id
                $aRes[$item->user_id][$agent_id] = $agent_id;
            }
        }
        return $aRes;
    }
    
    /**
     * @Author: ANH DUNG Now 05, 2018
     * @Todo: insert to table notify
     * @Param: $aUid array user id need notify
     */
    public function runInsertNotify($aUid, $sendNow = false) {
        $json_var = array();
        foreach($aUid as $uid) {
            if( !empty($uid) ){
                GasScheduleNotify::InsertRecord($uid, GasScheduleNotify::TRANSACTION_CHANGE_EMPLOYEE, $this->id, '', $this->titleNotify, $json_var);
            }
        }
    }
    
    /** @Author: ANH DUNG Now 05, 2018
     *  @Todo: auto save to EmployeeProblems, cut salary
     **/
    public function moveToSupport() {
        $listUser = json_decode($this->json);
        $aUidSend = [];
        foreach($listUser as $user_id => $fullName){
            $aUidSend[$user_id] = $user_id;
        }
        
        $mTransactionHistory = new TransactionHistory();
        $mTransactionHistory->id            = $this->id;// truyền id ticket vào model EmployeeProblems
        
        $mEmployeeProblems  = new EmployeeProblems();
        $mScheduleNotify    = new GasScheduleNotify();
        $mScheduleNotify->type = 0;
        $mScheduleNotify->aUidSend = $aUidSend;
        $this->titleNotify  = "[E03 Tinh Loi] Khong Lien Lac Duoc: {$this->note}";
        
        $mEmployeeProblems->object_type     = EmployeeProblems::PROBLEM_NOT_CONTACT;
        $mEmployeeProblems->money           = $mEmployeeProblems->getPrice();
        $mScheduleNotify->mEmployeeProblems = $mEmployeeProblems;
        $mScheduleNotify->title             = $this->titleNotify;
        $mScheduleNotify->saveErrorsHgdSms($mTransactionHistory, []);
        $this->runInsertNotify($mScheduleNotify->aUidSend, false); // notify to phone app
    }
    
    /** @Author: DuongNV Jan 4,2018
     *  @Todo: copy price support from current month to next month
     **/
    public function cronCopyNewMonthPriceSupport() {
        if(date('t') != date('d')){// chỉ chạy ngày cuối tháng
            return ;
        }
        $models = $this->cronCopyGetDataPriceSupport();
        foreach($models as $item){
            $newRecord = new PriceSupport();
            $aFieldNotCopy = array('id');
            MyFormat::copyFromToTable($item, $newRecord, $aFieldNotCopy);
//            $newRecord->date_apply = date('Y-m-d', strtotime('first day of next month')); -- nếu fix data sẽ chạy bị sai
            $newRecord->date_apply      = MyFormat::modifyDays($item->date_apply, 1, '+', 'month');
            $newRecord->created_date    = $newRecord->getCreatedDateDb();
            $newRecord->save();
        }
        Logger::WriteLog("cronCopyNewMonth PriceSupport done ".date('Y-m-d H:i:s'));
    }
    
    /** @Author: DuongNV Jan 4,2018
     *  @Todo: get data to copy PriceSupport
     **/
    public function cronCopyGetDataPriceSupport() {
        if(empty($this->date_from)){
            $this->date_from  = date('Y-m-01');
        }
        if(empty($this->date_to)){
            $this->date_to    = date('Y-m-t');
        }
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition('t.date_apply', $this->date_from, $this->date_to);
        $criteria->compare('t.type', PriceSupport::TYPE_HARD_DELIVERY);
        return PriceSupport::model()->findAll($criteria);
    }
    
    /** @Author: DuongNV Aug2919
     *  @Todo: get list agent of ktkv
     *  @params: date_from Y-m-01
     *  @params: date_to   Y-m-t
     **/
    public function getListAgentOfKtkv($ktkvID = '') {
        $mSetupTeam                 = new SetupTeam();
        $mSetupTeam->date_from      = empty($this->date_from) ? date('Y-m').'-01' : $this->date_from;
        $mSetupTeam->date_to        = empty($this->date_to) ? date('Y-m-t') : $this->date_to;
        $aType                      = [SetupTeam::TYPE_KTKV_AGENT];
        $aData                      = $mSetupTeam->getByType($aType);
        $ret                        = [];
        $this->aEmployeeOfKtkv      = [];
        $aEmployeeOfAgent           = [];
        foreach($aData as $user_id => $aAgentId){
            if( !empty($ktkvID) && $user_id != $ktkvID )  continue;
            foreach($aAgentId as $agent_id){
                $ret[$user_id][]    = $agent_id;
                
                // DS nhân viên do ktkv quản lý
                $aEmployeeOfAgent[$agent_id]            = empty($aEmployeeOfAgent[$agent_id]) ? GasOneMany::getArrOfManyIdByType(ONE_AGENT_MAINTAIN, $agent_id) : $aEmployeeOfAgent[$agent_id];
                if( empty($this->aEmployeeOfKtkv[$user_id]) ){
                    $this->aEmployeeOfKtkv[$user_id]    = $aEmployeeOfAgent[$agent_id];
                } else {
                    $this->aEmployeeOfKtkv[$user_id]   += $aEmployeeOfAgent[$agent_id];
                }
            }
        }
        return $ret;
    }
    
    
}