<?php

/**
 * This is the model class for table "{{_call_review}}".
 *
 * The followings are the available columns in table '{{_call_review}}':
 * @property string $id
 * @property string $call_id
 * @property string $agent_id
 * @property string $province_id
 * @property string $call_center_id
 * @property string $call_created_date 
 * @property integer $reason
 * @property string $created_date
 * @property string $creates_by
 */
class CallReview extends BaseSpj
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CallReview the static model class
     */
    // PHẠM THÀNH NGHĨA Aug 17, 2018
    const RECEIVED      = 1; // TRẠNG THÁI NHẬN CALL user_check_call  != 0, chưa test: status_check_call = 1
    const TESTED        = 2; // TRẠNG THÁI ĐÃ TEST user_check_call  != 0, status_check_call = 2
    const MAX_SCORE     = 10;// one call have max score is 10
    
    const BC_KIEMTRA    = 1;
    const BC_TONGDAI    = 2;
    const BC_KIEMTRA_TONGDAI = 3; // tong dai so voi nv kiem tra
    
    public $customer_new ,$note,$date_from,$date_to, $autocomplete_name, $autocomplete_name_1;
    public $mCall;
    
    const BUG_TAO_MQH           = 1;
    const BUG_KHONG_MAU         = 2;
    const BUG_KHONG_BAO_GIA_QUA = 3;
    const BUG_GIONG_NOI         = 4;
    const BUG_THAI_DO           = 5;
    const BUG_GIU_LOI_HUA       = 6;
    const BUG_GOI_TOT           = 7;
    const BUG_ADDRESS           = 11;
    const BUG_KH_REPORT         = 12;
    const BUG_NOISE_SOUND       = 13;
    const BUG_SOLUTION_BAD      = 14;
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_call_review}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['id, call_id, agent_id, province_id, call_center_id, call_created_date, reason, created_date, creates_by ,note, customer_new', 'safe'],
            ['note, customer_new,date_from,date_to','safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'rCall' => array(self::BELONGS_TO, 'Call', 'call_id'),
            'rProvince' => array(self::BELONGS_TO, 'GasProvince', 'province_id'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rCallCenter' => array(self::BELONGS_TO, 'Users', 'call_center_id'),
            'rCreatesBy' => array(self::BELONGS_TO, 'Users', 'creates_by'),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'call_id' => 'Cuộc gọi',
            'agent_id' => 'Đại lý',
            'province_id' => 'Tỉnh',
            'call_center_id' => 'NV gọi',
            'call_created_date' => 'Ngày tạo cuộc gọi',
            'reason' => 'Lý do',
            'created_date' => 'Ngày kiểm tra',
            'creates_by' => 'NV kiểm tra',
            'note'=>'ghi chú',
            'date_from'=>'Từ ngày',
            'date_to' => 'Đến ngày',
        ];
    }

    /** @Author: Pham Thanh Nghia Sep 24, 2018
     *  @Todo: search list callreivew cho views/callReview/listDetail/index.php
     **/
    public function search()
    {
        $criteria=new CDbCriteria;
	$criteria->compare('t.agent_id',$this->agent_id);
	$criteria->compare('t.call_center_id',$this->call_center_id);
	$criteria->compare('t.reason',$this->reason);
	$criteria->compare('t.creates_by',$this->creates_by);
        if(!empty($this->province_id) && is_array($this->province_id)){
            $criteria->addInCondition('t.province_id',$this->province_id);
        }
        $date_from = $date_to = '';
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        }
        if(!empty($date_from)){
            $criteria->addCondition("DATE(t.created_date) >='$date_from'");
        }
        if(!empty($date_to)){
            $criteria->addCondition("DATE(t.created_date) <='$date_to'");
        }
        $criteria->order = 't.id DESC';
        $_SESSION['dataCallReview'] = new CActiveDataProvider($this, array(
            'pagination'=>false,
            'criteria'=>$criteria,
//            'sort' => $sort,
        ));
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    /** @Author: Pham Thanh Nghia Sep 24, 2018
     *  @Todo: search list callreivew cho views/callReview/_form_view.php
     **/
    public function searchForView()
    {
        $criteria=new CDbCriteria;
	$criteria->compare('t.call_id',$this->call_id);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    /** @Author: Pham Thanh Nghia Sep 28 2018
     * @Note: nếu có lỗi 0đ thì cập nhật thêm vào bên list for view
     **/
        public function getArrayReason(){
        return [
            CallReview::BUG_TAO_MQH               => 'Tạo MQH Theo quy trình(5đ)',
//            self::BUG_KHONG_MAU             => 'Không hỏi màu bình gas (1đ)',
//            self::BUG_KHONG_BAO_GIA_QUA     => 'Không báo giá,không báo quà (1đ)',
            self::BUG_ADDRESS               => 'Xác định địa chỉ (5đ)',  
//            self::BUG_GIONG_NOI             => 'Giọng nói (2đ)', 
            self::BUG_SOLUTION_BAD          => 'Giải quyết tình huống (5đ)', 
            self::BUG_THAI_DO               => 'Thái độ với khách hàng(30đ)',
            self::BUG_GIU_LOI_HUA           => 'Giữ lời hứa với khách hàng(5đ)',  
            self::BUG_GOI_TOT               => 'Gọi tốt',  
            self::BUG_KH_REPORT             => 'Khách hàng phàn nàn (0đ)',  
            self::BUG_NOISE_SOUND           => 'Âm thanh ngoài (0đ)',  
        ];
    }
    /** @Author: Pham Thanh Nghia 2018
     *  @Todo: được cap nhat từ tren
     **/
    public function getArrayReasonByScore(){
        return [
            self::BUG_TAO_MQH           => 5,
//            self::BUG_KHONG_MAU         => 1,
//            self::BUG_KHONG_BAO_GIA_QUA => 1,
            self::BUG_ADDRESS           => 5,
//            self::BUG_GIONG_NOI         => 2,
            self::BUG_SOLUTION_BAD      => 5,
            self::BUG_THAI_DO           => 30,
            self::BUG_GIU_LOI_HUA       => 5, // NGHIA 22-09-18
            self::BUG_GOI_TOT           => 0,
            self::BUG_KH_REPORT         => 0,
            self::BUG_NOISE_SOUND       => 0,
        ];
    }
    // for view
    public function getArrayReasonForView(){
        $aReason = $this->getArrayReason();
        unset($aReason[self::BUG_KH_REPORT]);
        unset($aReason[self::BUG_NOISE_SOUND]);
        unset($aReason[self::BUG_GOI_TOT]);
        return $aReason;
    }
    // danh sách điểm
    
    /** @Author: Pham Thanh Nghia Aug 20, 2018
     *  @Todo: show info Call by call_id
     *  @Param:
     **/
    public function getInfoCall(){
        $mCall = $this->rCall;
        $result = '';
        $cRole = MyFormat::getCurrentRoleId();
        if(!empty($mCall)){
            $this->customer_new = $mCall->customer_new;
            $this->note  = $mCall->note;
            $result      = '<b>Thông tin cuộc gọi:</b>';
            $result     .= '<li>Nhân viên : '.$mCall->getUser().' - Đại lý: '.$mCall->getAgentGrid().'</li>';
            if($cRole != ROLE_TEST_CALLL){
                $result     .= '<li>Khách hàng<b>: '.$mCall->getCustomer().'</b><br>'.$mCall->getCustomer("address").'</li>';
            }
            $result     .= '<li>Trạng thái: '.$mCall->getCallStatus(). ''.'</li>';
            $result     .= '<li>Số gọi : '.$mCall->getCallNumberView().' - Số nghe : '.$mCall->getDestinationNumberView(). ''.'</li>';
            $result     .= '<li>Ngày tạo cuộc gọi : '.$mCall->getCreatedDate(). ''.'</li>';
            $result     .= '<br><br>';
            return $result;
        }
        return '';
    }
    /** @Author: Pham Thanh Nghia Sep 5, 2018
     *  @Todo: lấy tên tỉnh
     **/
    public function getProvince() {
        $model = $this->rProvince;
        return !(empty($model)) ? $model->name : '';
    }
    /** @Author: Pham Thanh Nghia Sep 5, 2018
     *  @Todo: lấy tên đại lý
     **/
    public function getAgent($field = 'first_name') {
        $mAgent = $this->rAgent;
        return !empty($mAgent) ? $mAgent->$field : '';
    }
    /** @Author: Pham Thanh Nghia Sep 5, 2018
     *  @Todo: lấy tên người tạo
     **/
    public function getCreatesBy($field = 'first_name') {
        $mCreatesBy = $this->rCreatesBy;
        return !empty($mCreatesBy) ? $mCreatesBy->$field : '';
    }
    /** @Author: Pham Thanh Nghia Sep 5, 2018
     *  @Todo: lấy tên nhân viên call
     **/
    public function getCallCenter($field ='first_name') {
        $mCallCenter = $this->rCallCenter;
        return !empty($mCallCenter) ? $mCallCenter->$field : '';
    }
    /** @Author: Pham Thanh Nghia Sep 5, 2018
     *  @Todo: lấy nghe cuộc gọi
     **/
    public function getUrlPlayAudio() {
        $mCall = $this->rCall;
        return !empty($mCall) ? $mCall->getUrlPlayAudio() : '';
    }
    
    public function getNote(){
        $mCall = $this->rCall;
        return !empty($mCall) ? $mCall->getNote() : '';
    }
    public function handlToSave(){
        $mCall = $this->rCall;
        if(!empty($mCall)){
            $this->agent_id = $mCall->agent_id;
            $this->province_id = $mCall->province_id;
            $this->call_center_id = $mCall->user_id;
            $this->creates_by = MyFormat::getCurrentUid();
            $this->call_created_date = $mCall->created_date;
            $this->created_date = MyFormat::getDateTimeNow();
        }
        
    }
    public function makeCallReviewBuildSql(&$aRowInsert) {
        $aReason = $this->reason;
        $mCall = $this->rCall;
        $creates_by = MyFormat::getCurrentUid();
        $created_date = MyFormat::getDateTimeNow();
        if(!empty($mCall)){
            foreach($aReason as $key => $value):
                $aRowInsert[]="('$mCall->id',
                '$mCall->agent_id',
                '$mCall->province_id',
                '$mCall->user_id',
                '$value',
                '$creates_by',
                '$mCall->created_date',
                '$created_date')";
            endforeach;
        
        }
    }
    // xóa hết tất cả bởi call_id
    public function deleteAllByCallId(){
        $mCall = $this->rCall;
        $tableName = CallReview::model()->tableName();
        if(!empty($mCall)){
             $sql = "DELETE FROM $tableName 
                WHERE call_id = $mCall->id";
            Yii::app()->db->createCommand($sql)->execute();     
        }else{
            throw new Exception('Do not have model call');
        }
                                  
    }
    /** @Author: Pham Thanh Nghia 2018
     *  @Todo: save call review theo query
     **/
    public function makeCallReview() {
        $sql = '';
        $aRowInsert = [];
        $this->makeCallReviewBuildSql($aRowInsert);
        // 1. delete old record
        $tableName = CallReview::model()->tableName();
        $sql = "insert into $tableName (call_id
                                        ,agent_id
                                        ,province_id
                                        ,call_center_id
                                        ,reason
                                        ,creates_by
                                        ,call_created_date
                                        ,created_date
                    ) values ".implode(',', $aRowInsert);

        if(count($aRowInsert)>0)
            Yii::app()->db->createCommand($sql)->execute();
    }
    /** @Author: Pham Thanh Nghia 2018
     *  @Todo: lấy ra danh sách array reason cho index create
     **/
    public function handleForReason(){
        $criteria=new CDbCriteria;
        $criteria->compare('t.call_id', $this->call_id);
        $this->reason =  CHtml::listData(self::model()->findAll($criteria),'reason','reason');
    }
    /** @Author: Pham Thanh Nghia 2018
     **/
    public function getReaSon(){
        $aReason = $this->getArrayReason();
        return ($aReason[$this->reason]) ? $aReason[$this->reason] : '';
    }
    /** @Author: Pham Thanh Nghia Aug 21, 2018
     *  @Todo: lấy dand sach array call_id => reason
     **/
    // get array to handle source for call
    public function getArrayToHandleScoreForCall($call_id = ''){
        if($call_id < 1){
            return [];
        }
        $criteria=new CDbCriteria;
        if($call_id != ''){
            $criteria->compare('t.call_id', $call_id);
        }
        return CHtml::listData(self::model()->findAll($criteria),'reason','reason','call_id');
    }
    // cộng tất cả các điểm trừ lại
    public function sumScoreToSubtract(array $aList){
        $aScore  = $this->getArrayReasonByScore(); // danh sách điểm
        $sum = 0;
        foreach ($aList as $key => $value) {
            $sum += $aScore[$value];
        }
        return $sum;
    }
    /** @Author: Pham Thanh Nghia Aug 20, 2018
     *  @Todo: xử lý điểm xuất ra danh sách call_id => điểm
     **/
    public function handleArrayScoreForCall(){
        $aCallReview = $this->getArrayToHandleScoreForCall();
        $aResult = array();
        foreach ($aCallReview as $key => $aValue) {
            $score = self::MAX_SCORE - $this->sumScoreToSubtract($aValue);
            if($score >= 0){
                $aResult[$key] = $score;
            }else{
                $aResult[$key] = 0;
            }
        }
        return $aResult;
    }
    /** @Author: Pham Thanh Nghia Aug 20, 2018
     *  @Todo: cập nhật lại tất cả điểm trong call
     **/
    public function makeScoreCallAllBuildSql(&$aRowInsert) {
        $aScore = $this->handleArrayScoreForCall();
        foreach($aScore as $key => $value):
            $aRowInsert[]="($key,$value,0)";
        endforeach;
        
    }
    /** @Author: Pham Thanh Nghia Aug 20, 2018
     *  @Todo: cập nhật toàn bô điểm // ít sử dụng và chưa tối ưu
     **/
    public function saveScoreCallAll() {
        $sql = '';
        $aRowInsert = [];
        $tableName = Call::model()->tableName();
//        $sql = "insert into $tableName (id,score,did) values ". implode(',', $aRowInsert).
//                    " ON DUPLICATE KEY UPDATE did= VALUES(did), score = VALUES(score)";
        $aScore = $this->handleArrayScoreForCall();
        foreach ($aScore as $key => $value) {
           $sql = "UPDATE $tableName SET score = '$value' WHERE id = '$key' ";  
           Yii::app()->db->createCommand($sql)->execute();
        }
    }
    /** @Author: Pham Thanh Nghia Aug 20 , 2018
     *  @Todo: Tính điểm cho cuộc gọi bởi call_id
     **/
    public function calculateScoreByCallId($call_id){
        $aScore  = $this->getArrayReasonByScore(); // danh sách điểm
        $sum = 0;
        $aItemScore = $this->getArrayToHandleScoreForCall($call_id);
        if(count($aItemScore) < 1){
            return 0;
        }
        $aItemScore = $aItemScore[$call_id];
        foreach ($aItemScore as $key => $value) {
            $point = isset($aScore[$value]) ? $aScore[$value] : 0;
            $sum += $point;
        }
        return $score = self::MAX_SCORE - $sum;// Now3018 cho phép lưu số âm
//        return ($score >= 0) ? $score : 0;
    }
    
    public function getArrayEmployee(){
        $mAppCache = new AppCache();
        return $mAppCache->getListdataUserByRole(ROLE_TEST_CALLL);
    }
    /** @Author: Pham Thanh Nghia Sep 24, 2018
     *  @Todo: lấy dữ liêu để xuất báo cáo theo user check 
     **/
    public function getDataReportForUserCheck(){  // hỏi lại yêu cầu chị phương
        $criteria=new CDbCriteria;
        $aRes = $aSumRow = $aSumCol = [];
        $date_from = $date_to = '';
        $toTalSum = 0;
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        }
        if(!empty($date_from)){
            $criteria->addCondition("DATE(t.created_date) >='$date_from'");
        }
        if(!empty($date_to)){
            $criteria->addCondition("DATE(t.created_date) <='$date_to'");
        }
        $criteria->select = "t.creates_by,DATE(t.created_date) as created_date, count(id) as id";
        $criteria->group = "t.creates_by, DATE(t.created_date) ";
        $model = CallReview::model()->findAll($criteria);
        $aDay = MyFormat::getArrayDay($date_from, $date_to);
        foreach ($model as $value) {
            if(!isset($aRes[$value->creates_by][$value->created_date])){
                $aRes[$value->creates_by][$value->created_date] = $value->id;
            }else{
                $aRes[$value->creates_by][$value->created_date] += $value->id;
            }
            if(!isset($aSumRow[$value->creates_by])){
                $aSumRow[$value->creates_by] = $value->id;
            }else{
                $aSumRow[$value->creates_by] += $value->id;
            }
            if(!isset($aSumCol[$value->created_date])){
                $aSumCol[$value->created_date] = $value->id;
            }else{
                $aSumCol[$value->created_date] += $value->id;
            }
            
            $toTalSum+=$value->id;
        }
        
        arsort($aSumRow);
        $result = [
            'data' => $aRes,
            'toTalSum'  => $toTalSum,
            'aDay' => $aDay,
            'aSumCol' => $aSumCol,
            'aSumRow' => $aSumRow,
        ];
        $_SESSION['dataReportForUserCheck'] = $result;
        return $result;
    }
    /** @Author: Pham Thanh Nghia Sep 26, 2018
     *  @Todo: lấy dữ liêu để xuất báo cáo theo NV Tổng dài == Call Center
     **/
    public function getDataReportForCallCenter(){  
        $criteria=new CDbCriteria;
        $mCall = new Call();
        $aRes  = $aSumColReason = $aSumRowReason = $aSumRowApp = $aCallId = $aScore = [];
        
        $criteria->compare('t.call_center_id',$this->call_center_id);
	$criteria->compare('t.creates_by',$this->creates_by);
        $date_from = $date_to = '';
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        }
        if(!empty($date_from)){
            $criteria->addCondition("DATE(t.created_date) >='$date_from'");
        }
        if(!empty($date_to)){
            $criteria->addCondition("DATE(t.created_date) <='$date_to'");
        }
        $aCondition = array_keys($this->getArrayReasonForView()); // bỏ đi những lỗi 0 điểm
//        $criteria->addInCondition("t.reason",$aCondition);
        $aSumRowApp = $this->countCallIdByCallCenterId($criteria); // tong so cuoc goi theo id NV
        $criteria->select = "t.call_center_id,t.reason, t.call_id, count(id) as id";
        $criteria->group = "t.call_center_id,t.reason,t.call_id";
        $model = CallReview::model()->findAll($criteria);
        foreach ($model as $value) {
            if(!isset($aRes[$value->call_center_id][$value->reason])){
                $aRes[$value->call_center_id][$value->reason] = $value->id;
            }else{
                $aRes[$value->call_center_id][$value->reason] += $value->id;
            }
            if(!isset($aSumColReason[$value->reason])){
                $aSumColReason[$value->reason] = $value->id;
            }else{
                $aSumColReason[$value->reason] += $value->id;
            }
            if(in_array($value->reason,$aCondition)){ // chỉ đếm những lỗi trừ điểm
                if(!isset($aSumRowReason[$value->call_center_id])){
                    $aSumRowReason[$value->call_center_id] = $value->id;
                }else{
                    $aSumRowReason[$value->call_center_id] += $value->id;
                }
            }
            
            if(!isset($aCallId[$value->call_id])){
                $aCallId[$value->call_id]= $value->call_id;
            }
        }
        $aCallId = array_keys($aCallId);
        $aScore = $mCall->getArrAverageScoreEmployee($aCallId);
        arsort($aRes);
        $result = [
            'data' => $aRes,
            'aSumColReason' => $aSumColReason, 
            'aSumRowReason' => $aSumRowReason, // tong so ly theo id NV
            'aSumRowApp'    => $aSumRowApp, // tong so cuoc goi theo id NV
            'aScore'        => $aScore, // diem trung binh
        ];
        $_SESSION['dataReportForCallCenter'] = $result;
        return $result;
    }
    /** @Author: Pham Thanh Nghia Sep 28, 2018
     *  @Todo: điêm sô cuộc gọi của NV tổng đại không bị lập lại
     **/
    public function countCallIdByCallCenterId($criteria){
        $criteria->select = "t.call_center_id, count(DISTINCT call_id) as call_id";
        $criteria->group = "t.call_center_id";
        $model = CallReview::model()->findAll($criteria);
        $aRes = CHtml::listData($model, 'call_center_id', 'call_id');
        return $aRes;
    }
    /** @Author: Pham Thanh Nghia Sep 28, 2018
     *  @Todo: nghe cuộc gọi
     **/
    public function getModelCallForUrlPlayAudio(){
        $mCall = $this->rCall;
        if(!empty($mCall)){
            return $mCall->getUrlPlayAudio();
        }
        return '';
    }
    /** @Author: Pham Thanh Nghia Oct 3, 2018
     *  @Todo:get Array call_id on callreview by reason
     **/
    public function getArrCallIdByReason($reason){
        $criteria=new CDbCriteria;
        $criteria->compare('t.reason',$reason);
        $model = CallReview::model()->findAll($criteria);
        $aRes = CHtml::listData($model, 'call_id', 'call_id');
        return $aRes;
    }
        
    public function canExportExcel(){
        $aUidAllow  = [GasConst::UID_ADMIN, GasConst::UID_PHUONG_NT];
        $cUid       = MyFormat::getCurrentUid();
        return in_array($cUid, $aUidAllow);
    }
    /** @Author: ANH DUNG Oct 16, 2018
     *  @Todo: user can pick call review 
     **/
    public function canPickCallReview() {
        $aRoleAllow  = [ROLE_ADMIN, ROLE_TEST_CALLL];
        $cRole = MyFormat::getCurrentRoleId();
        return in_array($cRole, $aRoleAllow);
    }
    /** @Author: Pham Thanh Nghia 2018
     *  @Todo: 
     * @sql:SELECT `call_center_id`,DATE(`created_date`),  COUNT(call_id) FROM `gas_call_review`
            WHERE DATE(`created_date`) ='2018-10-17'
            GROUP by `call_center_id`,`call_id`,DATE(`created_date`)
     **/
    public function getDataReportForCallCenterOnDay(){
        $criteria=new CDbCriteria;
        $aRes = $aSumRow = $aSumCol = [];
        $date_from = $date_to = '';
        $toTalSum = 0;
        $criteria->compare('t.creates_by',$this->creates_by);
        $criteria->compare('t.call_center_id',$this->call_center_id);
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        }
        if(!empty($date_from)){
            $criteria->addCondition("DATE(t.created_date) >='$date_from'");
        }
        if(!empty($date_to)){
            $criteria->addCondition("DATE(t.created_date) <='$date_to'");
        }
        $criteria->select = "t.call_center_id ,DATE(t.created_date) as created_date, count( call_id) as id";
        $criteria->group = "t.call_center_id, DATE(t.created_date), t.call_id  ";
        $model = CallReview::model()->findAll($criteria);
        $aDay = MyFormat::getArrayDay($date_from, $date_to);
        foreach ($model as $value) {
            if(!isset($aRes[$value->call_center_id][$value->created_date])){
                $aRes[$value->call_center_id][$value->created_date] = $value->id;
            }else{
                $aRes[$value->call_center_id][$value->created_date] += $value->id;
            }
            if(!isset($aSumRow[$value->call_center_id])){
                $aSumRow[$value->call_center_id] = $value->id;
            }else{
                $aSumRow[$value->call_center_id] += $value->id;
            }
            if(!isset($aSumCol[$value->created_date])){
                $aSumCol[$value->created_date] = $value->id;
            }else{
                $aSumCol[$value->created_date] += $value->id;
            }
            
            $toTalSum+=$value->id;
        }
        arsort($aSumRow);
        
        $result = [
            'data' => $aRes,
            'toTalSum'  => $toTalSum,
            'aDay' => $aDay,
            'aSumCol' => $aSumCol,
            'aSumRow' => $aSumRow,
        ];
        $_SESSION['dataReportForCallCenterOnDay'] = $result;
        return $result;
    }
}