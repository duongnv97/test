<?php

/**
 * This is the model class for table "{{_app_promotion}}".
 *
 * The followings are the available columns in table '{{_app_promotion}}':
 * @property integer $id
 * @property integer $status
 * @property string $title
 * @property string $code_no
 * @property integer $type
 * @property integer $amount
 * @property integer $time_use
 * @property string $expiry_date
 * @property string $note
 * @property string $created_date
 * @property integer $owner_id
 * @property integer $owner_id_root
 */
class AppPromotion extends BaseSpj {

    const TYPE_ONE_TIME         = 1;
    const TYPE_MULTI_TIME       = 2;
    const TYPE_REFERENCE_USER   = 10; // trung handle app book - thưởng giới thiệu 20k
    const TYPE_FIRST_USER       = 11; // KM nhập mã giới thiệu của người khác
    const TYPE_FREE_ONE_GAS     = 12;
    const TYPE_FIRST_ORDER      = 13;// Jan0218 dùng 1 lần cho KH mới, kh cũ không nhập đc mã này
    const TYPE_COMPANY_EMPLOYEE = -1;// biến sử dụng cho search, không dùng vào KM chính thức
    
    const STATUS_ACTIVE         = 1;
    const STATUS_INACTIVE       = 0;
    
    const PID_FREE_ONE          = 1; // id KM free 1 bình gas
    const PID_FREE_TWO          = 2; // id KM tặng 100k cho số user đăng nhập vào htt
    const PID_GAS24H            = 4168; // id KM GAS24H

//    const P_AMOUNT_INPUT_REF    = 100000;// 100K số tiền KM khi nhập mã ref
    const P_AMOUNT_INPUT_REF        = 40000;// 40K số tiền KM khi nhập mã ref Mar1918
    const P_AMOUNT_OF_OWNER         = 20000;// số tiền KM Người cho Mã nhận đc khi người ref hoàn thành đơn đầu tiên
    const P_AMOUNT_INPUT_REF_PTTT   = 40000;// 100K số tiền KM khi PTTT nhập mã ref from Jul1018

    const P_POINT                   = 10;// số điểm thưởng tích lũy
    
    const NEWS_NORMAL               = 1;// only news
    const NEWS_INPUT_PROMOTION      = 2;// click button then auto fill promotion code to form
    const NEWS_OPEN_WEB             = 3;// click go to web
    const NEWS_INPUT_PROMOTION_AND_WEB = 4;// show 2 button input promotion + open web
    
    const PARTNER_TYPE_GRAB             = 1;// May2218 không cho sử dụng tiền nhập mã Giới thiệu với các đối tác Cấp 2 của Grab, gạo, rác
    const PARTNER_DEFAULT_LENGTH        = 6;// length code default partner type grab
    const PARTNER_DEFAULT_CODE          = '24H';// prefix code default partner type grab
    const PARTNER_MAX_USER              = 800;// max user có thể gen theo kiểu 24hxxx
    const CODE_REF_MONTH_PLUS           = 3;// May1419 fix cong 3 thang --> Jan3019 6 tháng cộng sử dụng KM mã giới thiệu
    
    const PARTNER_ID_XUAN_THANH         = 1612045;// id user bảo hiểm xuân thành SG
    const PARTNER_ID_APP_HOAN_BD        = 1647597;// id user Bùi Đức Hoàn
    const PARTNER_ID_APP_PHUC_HV        = 1647660;// id user Phúc HV
    const PARTNER_ID_APP_HIEU_TT        = 1647667;// id user Hiếu TT
    const PARTNER_ID_APP_PHUONG_ND      = 1647672;// id user Phương ND
    const PARTNER_ID_APP_YTU            = 1648750;// id user App - Y Tú
    const PARTNER_ID_APP_CONG_DOAN      = 1514535;// May3119 id user App - công đoàn
    
    public $dirMenuList = 0, $MAX_ID, $mPromotion = null, $makeNotify = 0, $autocomplete_name, $fromPopup=false;
    
    const APP_MENU_LIST = 1;// ACTION FROM APP LIST

    public function getArrayType($getTypeReffence = false) {
        $aRes = array(
            AppPromotion::TYPE_ONE_TIME         => 'Sử dụng 1 lần + check ngày hết hạn',
            AppPromotion::TYPE_MULTI_TIME       => 'Sử dụng nhiều lần + check ngày hết hạn',
            AppPromotion::TYPE_FREE_ONE_GAS     => 'Miễn phí 1 bình gas',
            AppPromotion::TYPE_FIRST_ORDER      => 'KH mới, chỉ áp dụng cho bình đầu tiên',
        );
        if($getTypeReffence){
            $aRes[AppPromotion::TYPE_FIRST_USER] = 'Nhập mã giới thiệu của KH or Nhân viên';
        }
        return $aRes;
    }
    /** @Author: DungNT Aug 19, 2018
     *  @Todo: get array id partner fix code theo ý muốn
     * @Note_Sep0519 Vẫn phải define các Level 1 ở đây để xác định change lại code giới thiệu ở app
     **/
    public function getArrayPartnerFixCode() {
        return [
            AppPromotion::PARTNER_ID_XUAN_THANH     => 'XT',
            AppPromotion::PARTNER_ID_APP_HOAN_BD    => 'C1',
            AppPromotion::PARTNER_ID_APP_PHUC_HV    => 'C2',
            AppPromotion::PARTNER_ID_APP_HIEU_TT    => 'C3',
            AppPromotion::PARTNER_ID_APP_PHUONG_ND  => 'C4',
            AppPromotion::PARTNER_ID_APP_YTU        => 'A24H',
            AppPromotion::PARTNER_ID_APP_CONG_DOAN  => 'B24H',// B24H is not use for anything
        ];
    }
    
    /** @Author: DungNT Jan 02, 2019
     *  @Todo: get array type for report App Gas24h
     **/
    public function getTypeReport() {
        return [AppPromotion::TYPE_FIRST_ORDER, AppPromotion::TYPE_FREE_ONE_GAS, AppPromotion::TYPE_ONE_TIME];
    }
    public function getListIdOneTimeUse() { /** @Author: DungNT Jan 01, 2019 **/
        return [131431, AppPromotion::PID_FREE_TWO, AppPromotion::PID_GAS24H];
    }
    /** @Author: DungNT Jan 04, 2019
     *  @Todo: get array type One time user
     **/
    public function getTypeOneTimeUse() {
        return [AppPromotion::TYPE_FIRST_ORDER, AppPromotion::TYPE_ONE_TIME];
    }
    
    /** @Author: DungNT Now 16, 2018
     *  @Todo: get array length code partner fix code format like GDKV
     **/
    public function getArrayPartnerGdkv() {
        return [
            AppPromotion::PARTNER_ID_APP_HOAN_BD,
            AppPromotion::PARTNER_ID_APP_PHUC_HV,
            AppPromotion::PARTNER_ID_APP_HIEU_TT,
            AppPromotion::PARTNER_ID_APP_PHUONG_ND,
        ];
    }
    
    /** @Author: DungNT Jun 17, 2019
     *  @Todo: get array user allow update ip limit
     **/
    public function getArrayUserUpdateIpLimit() {
        return [ // là 1 user PTTT của NV KofiKai
//            user_id => id_promotion id promotion check ip limit, chỉ cho phép user đc cập nhật ip của 1 promotion xác định
            1042346 => 282454,// PVKH Demo Dũng Test App
            2112356 => 282454,// PTTT kofikai
        ];
    }
    
    /** @Author: DungNT Aug 30, 2018
     *  @Todo: get array length code partner fix code theo ý muốn
     **/
    public function getArrayPartnerLength() {
        return [
//            AppPromotion::PARTNER_ID_APP_YTU        => 7,// tạm bỏ đoạn này đi
        ];
    }
    
    /** @Author: DungNT Mar 08, 2019
     *  @Todo: get array agent lock nhập KM trên app
     **/
    public function getAgentLockPromotion() {
        return [
            1294352, // ĐLLK Gas Minh Hoàng - Đồng Nai
        ];
    }
    
    /** @Author: DungNT Aug 19, 2018
     *  @Todo: get array id partner fix code theo ý muốn
     **/
    public function getAmountOfRefCode() {
        $res = AppPromotion::P_AMOUNT_INPUT_REF;
        if(in_array($this->owner_id, $this->getListUidPttt())){
            $res = AppPromotion::P_AMOUNT_INPUT_REF_PTTT;
        }
        return $res;
    }
    
    /** @Author: DungNT Aug 10, 2018
     *  @Todo: get list user id PTTT có mã giới thiệu nhập mã giảm 100k
     **/
    public function getListUidPttt() {
        return $this->getListUidPtttAnhDot();
    }
    
    /** @Author: DungNT Oct 18, 2018
     *  @Todo: tách riêng đội PTTT của từng người ra, có thể có nhiều đội kiểu này
     **/
    public function getListUidPtttAnhDot() {
        return [];
    }
    
    /** @Author: DungNT Now 06, 2018
     *  @Todo: get id những level 2 của bảo hiểm xuân thành, apply giảm 60k cho đơn app ở tất cả các tỉnh
     **/
    public function getIdBhxtLevel2() {
        return [35612, 35795, 35796, 35797, 35800, 35801, 35802, 35805, 35806, 36625];
    }
    
    /** @Author: DungNT May 14, 2018
     *  @Todo: get array owner_id hide
     **/
    public function getOwnerHide() {
        return [
            1029122, 1029818, 1029822, 1029823, 1029821, // Call 201-> 204
            710307, 1046289, 
        ];
    }
    
    public function getTypeNotAddByHand() {
        return [AppPromotion::TYPE_REFERENCE_USER, AppPromotion::TYPE_FIRST_USER];
    }
    public function getTypeAllowUserInput() {
//        return [AppPromotion::TYPE_ONE_TIME, AppPromotion::TYPE_MULTI_TIME, AppPromotion::TYPE_REFERENCE_USER];
//        Mar0218 mở cho user có thể nhập mã miễn phí 1 bình gas. Sau đó thì close lại
        return [AppPromotion::TYPE_FIRST_ORDER, AppPromotion::TYPE_FREE_ONE_GAS, AppPromotion::TYPE_ONE_TIME, AppPromotion::TYPE_MULTI_TIME, AppPromotion::TYPE_REFERENCE_USER];
    }
    

    public function getType() {
        $aType = $this->getArrayType();
        return isset($aType[$this->type]) ? $aType[$this->type] : '';
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{_app_promotion}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('title, amount, time_use, expiry_date, note', 'required', 'on' => 'create, update'),
            array('code_no', 'required', 'on' => 'updateCode'),
            array('code_no', 'unique', 'message' => 'Mã khuyến mãi này đã tồn tại, vui lòng chọn mã khác', 'on' => 'create, update, updateCode'),
            array('ip_limit, owner_id_root, partner_type, owner_role_id, time_send_notify, makeNotify, province_limit, news_title, plus_month_use, is_news, is_home_popup, link_web, news_content, url_banner, url_banner_popup, owner_id, status, id, title, code_no, type, amount, time_use, expiry_date, note, created_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'rOwner' => array(self::BELONGS_TO, 'Users', 'owner_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'title' => 'Tiêu đề',
            'code_no' => 'Mã khuyến mãi',
            'type' => 'Loại',
            'amount' => 'Số tiền giảm giá',
            'time_use' => 'Số lần sử dụng',
            'expiry_date' => 'Ngày hết hạn',
            'note' => 'Mô tả khuyến mãi',
            'created_date' => 'Ngày tạo',
            'owner_id' => 'Khách hàng',
            'owner_role_id' => 'Chức vụ',
            'is_news' => 'Là tin tức của App Gas24h',
            'is_home_popup' => 'Bật popup Gas24h',
            'link_web' => 'Link Open Web',
            'news_content' => 'Nội dung tin tức',
            'url_banner' => 'Url banner',
            'url_banner_popup' => 'Url banner popup',
            'plus_month_use' => 'Tháng sử dụng (được ưu tiên hơn ngày hết hạn)',
            'news_title' => 'News title',
            'province_limit' => 'Giới hạn tỉnh áp dụng',
            'makeNotify' => 'Tạo notify thông báo đến KH',
            'time_send_notify' => 'Thời gian gửi notify',
            'partner_type' => 'Partner Type',
            'ip_limit' => 'Ip Limit allow add Code',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        $type = isset($_GET['type']) ? $_GET['type'] : 0;
        $criteria = new CDbCriteria;
        $criteria->compare('t.owner_id', $this->owner_id);
        $criteria->compare('t.owner_id', $this->owner_id_root);
        $criteria->compare('t.title', trim($this->title));
        $criteria->compare('t.code_no', trim($this->code_no));
        $criteria->compare('t.type', $this->type);
        $criteria->compare('t.time_use', $this->time_use);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('t.owner_role_id', $this->owner_role_id);
        $criteria->order = 't.id DESC';
        if($type == AppPromotion::TYPE_REFERENCE_USER){
            $criteria->addCondition('t.type=' . AppPromotion::TYPE_REFERENCE_USER);
            $criteria->addCondition('t.owner_role_id=' . ROLE_CUSTOMER);
        }elseif($type == AppPromotion::TYPE_COMPANY_EMPLOYEE){
            $criteria->addCondition('t.type=' . AppPromotion::TYPE_REFERENCE_USER);
            $criteria->addCondition('t.owner_role_id<>' . ROLE_CUSTOMER);
            $aRoleSearch = [ROLE_CALL_CENTER, ROLE_DIEU_PHOI];
            $sParamsIn = implode(',',  $aRoleSearch);
//            $criteria->addCondition("t.owner_role_id IN ($sParamsIn)"); 
            $criteria->order = 't.id DESC';
            $sParamsIn = implode(',', $this->getOwnerHide());
            $criteria->addCondition("t.owner_id NOT IN ($sParamsIn)");
        }else{
            $criteria->addCondition('t.type<>' . AppPromotion::TYPE_REFERENCE_USER);
        }
        
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    public function canUpdate() {
//        return true;// Now1618 for dev test
        $aRoleLock = [ROLE_CUSTOMER];
        if($this->type == AppPromotion::TYPE_REFERENCE_USER && in_array($this->owner_role_id, $aRoleLock)){
            return false;
        }// May1418 open để Phương sửa mã cho NV nội bộ
        $cRole      = MyFormat::getCurrentRoleId();
        $cUid       = MyFormat::getCurrentUid();
        $aUidAllow  = [GasConst::UID_PHUONG_NT];
        if($cRole == ROLE_ADMIN){
            return true;
        }
        if($this->type == AppPromotion::TYPE_REFERENCE_USER && in_array($cUid, $aUidAllow)){
            return true;
        }
        return false;
    }
    public function canDelete() {
        return false;
    }
    
    protected function beforeDelete() {
//        if($this->type == AppPromotion::TYPE_REFERENCE_USER){
//            return false;
//        }
        return parent::beforeDelete();
    }

    public function getArrayMonth($max) {
        $res = array();
        for ($i = 1; $i <= $max; $i++):
            $res[$i] = $i.' tháng';
        endfor;
        return $res;
    }
    public function getTitle() {
        return $this->title;
    }
    public function getTitleWeb() {
        $res = $this->title;
        if($this->type == AppPromotion::TYPE_REFERENCE_USER){
            $mUser = $this->rOwner;
            if($mUser){
                $link =Yii::app()->createAbsoluteUrl('admin/gascustomer/view', ['id'=> $this->owner_id]);
                $res = "<a href='$link' target='_blank'>$mUser->first_name</a>";
            }
        }
        return $res;
    }
    public function getTitleNews() {
        if(!empty($this->news_title)){
            return $this->news_title;
        }
        return $this->title;
    }

    public function getStatus() {
        $aStatus = ActiveRecord::getUserStatus();
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : '';
    }
    public function getCodeNo() {
        return $this->code_no;
    }
    public function getAmount($format = false) {
        $res = $this->amount;
        if($this->type == AppPromotion::TYPE_REFERENCE_USER){
            $res = AppPromotion::P_AMOUNT_INPUT_REF;
        }
        if ($format) {
            return ActiveRecord::formatCurrency($res);
        }
        return $res;
    }
    public function getTimeUse() {
        return $this->time_use;
    }
    public function getExpiryDate() {
        return MyFormat::dateConverYmdToDmy($this->expiry_date);
    }
    public function getMonthUse() {
        if($this->plus_month_use){
            return $this->plus_month_use.' Tháng';
        }
        return '';
    }
    public function getNote() {
        return nl2br($this->note);
    }
    public function getOwner($field_name, $getLinkViewCustomer) {
        $mUser = $this->rOwner;
        $res = '';
        if($mUser){
            $res = $mUser->code_bussiness. ' - ' .$mUser->first_name;
            if($field_name != ''){
                $res = $mUser->$field_name;
            }
            if($getLinkViewCustomer){
                $url = Yii::app()->createAbsoluteUrl('admin/gascustomer/view', ['id'=> $mUser->id]);
                $res = "<a href='$url' target='_blank'>$res</a>";
            }
        }
        return $res;
    }
    public function getOwnerRole(){
        if($this->type != AppPromotion::TYPE_REFERENCE_USER){
            return '';
        }
        $mUser = $this->rOwner;
        if(empty($mUser)){
            return '';
        }
        $session=Yii::app()->session;
        if(!isset($session['ROLE_NAME_USER']) || !isset($session['ROLE_NAME_USER'][$mUser->role_id]))
                $session['ROLE_NAME_USER'] = Roles::getArrRoleName();
        return $session['ROLE_NAME_USER'][$mUser->role_id];
    }

    public function getCreatedDate($fomat = 'd/m/Y H:i') {
        return MyFormat::dateConverYmdToDmy($this->created_date, $fomat);
    }
    public function getLinkWeb() {
//        if(isset($_GET['platform']) && $_GET['platform'] == UsersTokens::PLATFORM_IOS && $_GET['version_code'] <= 215 ){
//            return $this->link_web.'';
//        }
//        return '';
        return $this->link_web.'';
    }
    public function getNewsContent() {
        return $this->news_content.'';
    }
    public function getUrlBannerPopup() {
        return $this->url_banner_popup.'';
    }
    public function getUrlBanner() {
        return $this->url_banner.'';
    }
    public function getNewsType() {
        $res = AppPromotion::NEWS_NORMAL;
        if(empty($this->code_no) && !empty($this->link_web)){
            $res = AppPromotion::NEWS_OPEN_WEB;
        }
        if(!empty($this->code_no)){
            $res = AppPromotion::NEWS_INPUT_PROMOTION;
        }
        if(!empty($this->code_no) && !empty($this->link_web)){
            $res = AppPromotion::NEWS_INPUT_PROMOTION_AND_WEB;
        }
        return $res;
    }
    public function getWebIsNews() {
        if($this->is_news){
            return 'Show on App Gas24h';
        }
        return '';
    }
    public function getAppListTitle() {
        if($this->code_no){
            return 'Khuyến mãi';
        }
        return 'Thông báo';
    }
    

    protected function beforeValidate() {
        $this->trimData();
        $this->amount   = MyFormat::removeComma($this->amount);
        $this->code_no  = strtoupper($this->code_no);
        return parent::beforeValidate();
    }

    public function trimData() {
        $this->title    = trim($this->title);
        $this->code_no  = trim($this->code_no);
        $this->note     = trim($this->note);
        if($this->type == AppPromotion::TYPE_REFERENCE_USER){
            $mUser = $this->rOwner;
            if($mUser){
                $this->owner_role_id = $mUser->role_id;
            }
        }
    }

    protected function beforeSave() {
        if (strpos($this->expiry_date, '/')) {
            $this->expiry_date = MyFormat::dateConverDmyToYmd($this->expiry_date);
            MyFormat::isValidDate($this->expiry_date);
        }
        if(strpos($this->time_send_notify, '/')){
            $this->time_send_notify = MyFormat::datetimeToDbDatetime($this->time_send_notify);
        }
        return parent::beforeSave();
    }
    
    public function setProvince() {
        if(is_array($this->province_limit)){
            $this->province_limit = implode(',', $this->province_limit);
        }else{
            $this->province_limit = '';
        }
    }
    public function getProvinceArray() {
        if(!empty($this->province_limit)){
            $this->province_limit = explode(',', $this->province_limit);
        }
    }

    /**
     * @Author: DungNT Nov 24, 2016
     * @Todo: màn hình nhập mã giảm giá riêng, get active promotion ByCode
     * @Param: $code_no
     */
    public function getByCode() {
        $this->code_no = trim($this->code_no);
        if(empty($this->code_no)){
            return null;
        }
        $criteria = new CDbCriteria();
        $criteria->compare('t.code_no', strtoupper($this->code_no));
        $criteria->addCondition('t.status=' . STATUS_ACTIVE . ' AND t.expiry_date >= CURDATE()');
        $sParamsIn = implode(',',  $this->getTypeAllowUserInput());
        $criteria->addCondition("t.type IN ($sParamsIn)");
        return self::model()->find($criteria);
    }
    
    /** @Author: DungNT Nov 17, 2018
     *  @Todo: get by promotion ByCode
     */
    public function getByCodeOnly() {
        $this->code_no = trim($this->code_no);
        if(empty($this->code_no)){
            return null;
        }
        $criteria = new CDbCriteria();
        $criteria->compare('t.code_no', strtoupper($this->code_no));
        return self::model()->find($criteria);
    }

    /**
     *  Get referral info of user
     * @param $owner_id integer of owner
     * @return AppPromotion
     */
    public function getMyReferralInfo() {
        $model = self::model()->findByAttributes(array(
            'owner_id' => $this->owner_id,
            'type' => self::TYPE_REFERENCE_USER
        ));
        // Need generate automatically if doesn't existed
        if (!$model) {
            $model = $this->generateNewReferralInfo($this->owner_id);
        }
        return $model;
    }

    /** @Author: DungNT Now 14, 2017
     *  @Todo: get code only
     * */
    public function getMyInviteCode() {
        $model = $this->getMyReferralInfo();
        return $model->code_no;
    }

    /** Generate new referral code
     * @param $owner_id
     * @return AppPromotion
     */
    public function generateNewReferralInfo($owner_id) {
        // Generate code
        while (true) {
            $length = 6;
            $code = ActiveRecord::randString($length, 'ABCDEFGHKMNPQRSTUVXY123456789');
            if (!$this->getAppPromotionByCode($code)) {
                $mUser = Users::model()->findByPk($owner_id);
//          Now1417 Anh Dung close =>  $date = strtotime(date("Y-m-d", strtotime($date)) . " +24 month");
                $date = MyFormat::modifyDays(date('Y-m-d'), 120, '+', 'year');// Mar2018 mã giới thiệu ko có ngày hết hạn, nếu để cộng 12 month thì bị sai hết 
                $appPromotion = new AppPromotion();
                $appPromotion->code_no          = $code;
                $appPromotion->owner_id         = $owner_id;
                $appPromotion->owner_role_id    = $mUser->role_id;
                $appPromotion->amount           = $appPromotion->getAmountOfRefCode();
                $appPromotion->time_use     = 1;
                $appPromotion->status       = self::STATUS_ACTIVE;
                $appPromotion->expiry_date  = $date;
                $appPromotion->type         = self::TYPE_REFERENCE_USER;
                if ($appPromotion->save()) {
                    return $appPromotion;
                }
            }
        }
    }

    /**
     *  Get referral info by code no
     * @param $code_no String code no
     * @return AppPromotion
     */
    public function getAppPromotionByCode($code_no) {
        $model = self::model()->findByAttributes(array(
            'code_no' => $code_no,
        ));
        return $model;
    }
    
    public function getListdata() {
        $today = date('Y-m-d');
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',', $this->getTypeNotAddByHand());
        $criteria->addCondition("t.type NOT IN ($sParamsIn)");
        $criteria->addCondition('t.status=' . STATUS_ACTIVE);
        $criteria->addCondition("t.expiry_date>'$today'" );
        $criteria->order = 't.id DESC';
        return CHtml::listData(self::model()->findAll($criteria), 'id', 'title');
    }
    public function getListdataAll($needMore = []) {
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',', $this->getTypeNotAddByHand());
        $criteria->addCondition("t.type NOT IN ($sParamsIn)");
        $models = self::model()->findAll($criteria);
        if(isset($needMore['GetModel'])){
            $aRes = [];
            foreach($models as $item){
                $aRes[$item->id] = $item;
            }
            return $aRes;
        }
        $name = 'title';
        if(isset($needMore['GetCodeNo'])){
            $name = 'code_no';
        }
        
        return CHtml::listData($models, 'id', $name);
    }
    
    /** @Author: DungNT  Jan 29, 2018 -- handle listing api */
    public function handleApiList(&$result, $q) {
        // 1. get list order by user id
        $dataProvider   = $this->apiListing($q);
        $models         = $dataProvider->data;
        $CPagination    = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page']   = $CPagination->pageCount;
        
        $result['message']  = 'L';
        $result['record']   = [];
        
        if( $q->page >= $CPagination->pageCount ){
            $result['record'] = [];
        }else{
            foreach($models as $model){
                $model->mAppUserLogin       = $this->mAppUserLogin;// for canPickCancel
                $model->appAction           = $this->appAction;// for canPickCancel
//                $model->isAgentOfPromotion();// 
                $result['record'][]         = $model->formatAppItemList();
            }
        }
    }
    
    /** @Author: DungNT Jan 19, 2018 
     *  @Todo: dùng chung để format item trả xuống list + view của app Gas24h
     **/
    public function formatAppItemList() {
        
        $temp = [];
        $temp['news_type']          = $this->getNewsType();
        $temp['id']                 = $this->id;
        $temp['code_no']            = $this->code_no;
        $temp['code_no_text']       = 'NHẬP MÃ NGAY'; // 'Nhập mã ngay';
        $temp['title']              = $this->getTitleNews(); 
        $temp['link_web']           = $this->getLinkWeb();
        $temp['link_web_text']      = 'XEM CHI TIẾT'; //'Xem chi tiết';
        $temp['url_banner_popup']   = $this->getUrlBannerPopup();
        $temp['url_banner']         = $this->getUrlBanner();
        $temp['list_title']         = $this->getAppListTitle();
        $temp['created_date']       = $this->getCreatedDate();
        
        $content = $this->getHtmlAppView();
        if($this->fromPopup && isset($_GET['platform']) && $_GET['platform'] == UsersTokens::PLATFORM_IOS){
            $content = '';
        }
//        $temp['news_content']       = ($this->appAction == GasConst::ACTION_LIST ? '' : $content);
        $temp['news_content']       = $content;
        return $temp;
    }
    
    /** @Author: DungNT Jul 25, 2019
     *  @Todo: build html for app render 
     **/
    public function getHtmlAppView() {
        $mCms = new Cms();
        return $this->buildHtmlApp($this->getNewsContent(), $mCms->getAppLinkCss());
    }
    
    /** @Author: DungNT Jul 29, 2019
     *  @Todo: build page html for app view
     *  @param: $content div html content
     *  @param: $cssLink ex string: <link href="http://spj.daukhimiennam.com/themes/gas/css/app_style.css?v=1" rel="stylesheet" />
     **/
    public function buildHtmlApp($content, $cssLink) {
        $html = '';
        $html .= "<html lang='en'>";
        $html .= "<head>";
            $html .= $cssLink;
            $html .= "<meta charset='utf-8' />";
            $html .= "<meta http-equiv='X-UA-Compatible' content='IE=edge' />";
            $html .= "<meta name='viewport' content='width=device-width, initial-scale=1' />";
        $html .= "</head>";
        $html .= "<body>";
            $html .= "<div class='appContent'>{$content}";
            $html .= "</div>";
        $html .= "</body>";
        $html .= "</html>";
        return $html;
    }
    
    /**
    * @Author: DungNT Jan 29, 2018
    * @Todo: get data listing 
    * @param: $q object post params from app client
    */
    public function apiListing($q) {
        $criteria = new CDbCriteria();
        if($this->dirMenuList == AppPromotion::APP_MENU_LIST){
            $criteria->addCondition('t.status=' . STATUS_ACTIVE);
        }else{
            $criteria->addCondition('t.status=' . STATUS_ACTIVE . ' AND t.expiry_date >= CURDATE()');
        }
        $criteria->addCondition('t.is_news=' . STATUS_ACTIVE);
        $criteria->order = 't.id DESC';
        $dataProvider=new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
//                'pageSize' => GasAppOrder::API_LISTING_ITEM_PER_PAGE,
                'pageSize' => 20,// Đưa vào List cache sẽ luôn load page 0, nên sẽ để 20 item 
                'currentPage' => (int)$q->page,
            ),
          ));
        return $dataProvider;
    }
    
    /** @Author: DungNT Jan 29, 2018
     *  @Todo: get news popup on home app Gas24h
     **/
    public function formatNewsPopup() {
        $model = $this->getNewsPopup();
        if(empty($model)){
            return null;
        }
        $model->mAppUserLogin   = $this->mAppUserLogin;
        $model->fromPopup       = true;
        return $model->formatAppItemList();
    }
    // belong to function formatNewsPopup()
    public function getNewsPopup() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.status=' . STATUS_ACTIVE . ' AND t.expiry_date >= CURDATE()');
        $criteria->addCondition('t.is_news=' . STATUS_ACTIVE);
        $criteria->addCondition('t.is_home_popup=' . STATUS_ACTIVE);
        return AppPromotion::model()->find($criteria);
    }
    
    /** @Author: DungNT Mar 19, 2018
     *  @Todo: check radius mã sj100 của BT3
     * chỉ cho phép nhập mã sj100 với bán kính của BT 3
     **/
    public function checkRadiusAllow($q) {
        $idSj100    = 2;
        $aAgent     = [106, 658920, 768408];// BT 1,2,3 giới hạn 3 đại lý Bình Thạnh
        if($this->id != $idSj100 || !isset($q->latitude) || !isset($q->longitude) ){
            return ;
        }
        if(empty($q->latitude) || empty($q->longitude)){
            return ;
        }
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',',  $aAgent);
        $criteria->addCondition("t.id IN ($sParamsIn)");
        $models = Users::model()->findAll($criteria);
        $okRadius = false;
        foreach($models as $mAgent):
            $temp = explode(',', $mAgent->slug);
            $mMapLib    = new MapLib();
            $distance   = $mMapLib->haversineGreatCircleDistance($q->latitude, $q->longitude, $temp[0], $temp[1]);
            if($distance < $mAgent->price_other){
                $okRadius = true;
            }
        endforeach;
        
        if(!$okRadius){
            throw new Exception('SJ100 Mã khuyến mãi không hợp lệ');
        }
    }
    
    /** @Author: DungNT Oct 13, 2018 
     *  @Todo: check radius nhập mã phải nằm trong bán kính đại lý
     **/
    public function checkRadiusAgent($q) {
        $code               = (int)$q->code;
        if($code < 1000 || $code > 5000){// Jan2919 Telesale + Callcenter code từ 60xx, chỗ này chỉ check code của PTTT từ 10xx -> 50xx
           return ; 
        }
        $mapLib             = new MapLib();
        $agent_id           = $mapLib->gas24hGetAgentNearest($q->latitude, $q->longitude);
        if(empty($agent_id)){// Apr2419 tạm close do không lấy đc Vị trí của IOS bản mới change code React
            throw new Exception("$q->code - E035 Mã khuyến mãi không hợp lệ, không thể nhập mã khuyến mãi", SpjError::SELL001);
        }
    }
    
    /** @Author: DungNT Mar 27, 2018
     *  @Todo: Nhập KM trên app, check đại lý có nằm trong danh sách được nhập KM không
     *  nếu mã KM được giới hạn tỉnh thì kiểm tra xem đại lý của user có nhập được không
     **/
    public function checkProvinceAllow($q) {
        /* 1. tìm đại lý gần user nhất, nếu ko có đại lý nào thỏa mãn thì không cho nhập
         * 2. lấy đại lý đó convert ra tỉnh
         * 3. kiểm tra tỉnh đó có nằm trong giới hạn của KM không
         */
        if(empty($this->province_limit)){
            return ;
        }
        $msg = "Mã khuyến mãi {$this->code_no} không áp dụng cho khu vực của bạn";
        // 1. tìm đại lý gần user nhất, nếu ko có đại lý nào thỏa mãn thì không cho nhập KM
        $mapLib     = new MapLib();
        $agent_id   = $mapLib->gas24hGetAgentNearest($q->latitude, $q->longitude);
        if(empty($agent_id)){
            throw new Exception($msg);
        }
        $province_id = MyFormat::convertAgentToProvinceId($agent_id);
        if(!$this->isAgentOfPromotion($province_id, $agent_id)){
            throw new Exception($msg);
        }
    }
    
    /** @Author: DungNT Apr 18, 2018
     *  @Todo: tập trung các hàm check KM
     **/
    public function handleSomeCheck($q) {
//        $this->checkOver200Free($q);
        $this->checkTypeFirstOrder();
    }
    
    /** @Author: DungNT Mar 08, 2019
     *  @Todo: check lock agent không cho nhập KM App
     **/
    public function checkAgentLock($q, $mUser) {
        if(in_array($mUser->area_code_id, $this->getAgentLockPromotion())){
            throw new Exception('Mã khuyến mãi đã hết hạn hoặc không áp dụng cho khu vực của bạn');
        }
    }
    
    /** @Author: DungNT Apr 18, 2018
     *  @Todo: Nhập KM trên app, check nhập vượt 200 người, không cho phép
     **/
    public function checkOver200Free($q) {
        $idCheck = 9706;// id KM free 1 bình gas 30-04
        if($this->id != $idCheck){
            return ;
        }
        $totalInput = $this->countById();
        if($totalInput >= 60){
            throw new Exception('Số lượng bình gas miễn phí đã được sử dụng hết, chúc bạn may mắn trong những chương trình sau. Bạn nhập mã GAS24H để được hưởng khuyến mãi giảm giá 100.000 đồng nhé.');
        }
    }
    
    /** @Author: DungNT Apr 18, 2018
     *  @Todo: đếm số người đã nhập mã KM
     **/
    public function countById() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.promotion_id=' . $this->id);
        return AppPromotionUser::model()->count($criteria);
    }
    
    /** @Author: DungNT Mar 27, 2018
     *  @Todo: các chỗ cần check limit province với mã KM
     *  1. list new trên app
     *  2. popp new app
     *  3. nhập mã KM
     *  chuyển thành province từ agent_id để check
     *  Kiểm tra xem agent có phải là của promotion này không
     * @return: 
     * true if $province_id IN province_limit 
     * false if $province_id NOT IN province_limit 
    **/
    public function isAgentOfPromotion($province_id, $agent_id) {
        if(empty($this->province_limit)){
            return true;
        }
        $this->getProvinceArray();
        if(!is_array($this->province_limit) || count($this->province_limit) == 0){
            return true;
        }
        return in_array($province_id, $this->province_limit);
    }
    
    /** @Author: DungNT Apr 03, 2018
     *  @Todo: get view news
     **/
    public function getNewsView() {
        $criteria = new CDbCriteria();
        $criteria->compare('t.id', $this->id);
        return AppPromotion::model()->find($criteria);
    }
    
    /** @Author: DungNT Apr 08, 2018
     *  @Todo: insert to table notify
     *  @Param: $aUid array user id need notify
     */
    public function RunInsertNotify($aUid, $title) {
        $json_var = '';
        foreach($aUid as $uid) {
            if( !empty($uid) && $uid>0 ){
                                // InsertRecord($user_id, $type, $obj_id, $time_send, $title, $json_var)
                GasScheduleNotify::InsertRecord($uid, GasScheduleNotify::GAS24H_ANNOUNCE, $this->id, '', $title, $json_var);
            }
        }
    }
    
    /** @Author: DungNT Apr 10, 2018
     *  @Todo: kiểm tra xem có check vào notify cho Gas24h không
     **/
    public function makeNotifyGas24h() {
        if(!$this->makeNotify){
            return ;
        }
        $model = new CronTask();
        $model->type    = CronTask::TYPE_GAS24H_ANNOUNCE;
        $model->obj_id  = $this->id;
        $model->save();
    }
    /** @Author: DungNT Apr 16, 2018
     *  @Todo: get by owner_id 
     **/
    public function getByOwner() {
        $criteria = new CDbCriteria();
        $criteria->compare("t.owner_id", $this->owner_id);
        $criteria->compare("t.type", self::TYPE_REFERENCE_USER);
        return self::model()->find($criteria);
    }
    
    /** @Author: DungNT Apr 16, 2018
     *  @Todo: get ref code of User
     **/
    public static function getRefCodeUser($mUser) {
        $mAppPromotion = new AppPromotion();
        $mAppPromotion->owner_id = $mUser->id;
        $model = $mAppPromotion->getByOwner();
        if(!empty($model)){
            return $model->code_no;
        }
        return '';
    }
    
    /** @Author: DungNT May 04, 2018
     *  @Todo: count view news on app - by notify
     **/
    public function getUserViewOnApp() {
        $dayAllow = date('Y-m-d');
        $monthPrev = MyFormat::modifyDays($dayAllow, 3, '-', 'month');
        if(MyFormat::compareTwoDate($monthPrev, $this->created_date)){
            return '';
        }
        
        $model = new StaView();
        $model->type    = StaView::TYPE_1_APP_NEWS;
        $model->obj_id  = $this->id;
        return ActiveRecord::formatCurrency($model->countByObjId());
    }
    
    /** @Author: DungNT May 22, 2018
     *  @Todo: set partner type 
     **/
    public function setPartnerType($partner_type) {
        $this->partner_type = $partner_type;
        $this->update(['partner_type']);
    }
    
    /** @Author: DungNT May 06, 2018
     *  @Todo: get code only
     **/
    public function getOnlyCodeByUser() {
        $mCode = $this->getMyReferralInfo();
        $this->mPromotion = $mCode;
        return $mCode->code_no;
    }
    /** @Author: DungNT May 14, 2018
     *  @Todo: check user can update code
     **/
    public function canUpdateCodeRef() {
        $cUid = MyFormat::getCurrentUid();
        return in_array($cUid, GasConst::getAdminArray());
    }
    
    public function isUpdateCodeRef() {
//        return true;// Now1618 for dev test
        $aRoleLock = [ROLE_CUSTOMER];
        if($this->canUpdateCodeRef() && $this->type == AppPromotion::TYPE_REFERENCE_USER && !in_array($this->owner_role_id, $aRoleLock)){
            return true;
        }
        return false;
    }
    
    /** @Author: DungNT Aug 19, 2018
     *  @Todo: xử lý gen code clear for some partner
     **/
    public function handleMakeNewCodeClear() {
         $newCode = $this->doMakeNewCodeClear();
         if(!empty($newCode)){
            $this->code_no = $newCode;
         }
    }
    
    /** @Author: DungNT Aug 19, 2018
     *  @Todo: doing general 
     **/
    public function doMakeNewCodeClear() {
        $aPartnerFixCode        = $this->getArrayPartnerFixCode();
        $aPartnerLength         = $this->getArrayPartnerLength();
        $className              = 'AppPromotion';
        // Sep0518 bỏ đoạn $prefix_code này đi, không sử dụng đc, gây ra sai cho hàm $this->doMakeNewCode24h($prefix_code);
//        $prefix_code            = isset($aPartnerFixCode[$this->owner_id_root]) ? $aPartnerFixCode[$this->owner_id_root] : AppPromotion::PARTNER_DEFAULT_CODE;
        $length_max_id          = isset($aPartnerLength[$this->owner_id_root]) ? $aPartnerLength[$this->owner_id_root] : AppPromotion::PARTNER_DEFAULT_LENGTH;

        $fieldName              = 'code_no';
        if($this->owner_id_root == AppPromotion::PARTNER_ID_XUAN_THANH){
//        if(!in_array($this->owner_id_root, $this->getArrayPartnerGdkv())){// Now1618 note có thể phải fix theo kiểu này, nếu 24hxxx tràn hết mã
            $prefix_code = $aPartnerFixCode[$this->owner_id_root];
            return $this->doMakeNewCodeXuanThanh($className, $prefix_code, $length_max_id, $fieldName);
        }else{// fix cho all partner về sau cùng 1 mã 24hxxx
            return $this->doMakeNewCode24h();
        }
    }
    
    /** @Author: DungNT Sep 05, 2018
     *  @Todo: sinh code cho partner Xuan Thanh XT001, XT002
     **/
    public function doMakeNewCodeXuanThanh($className, $prefix_code, $length_max_id, $fieldName) {
        $needMore['condition']  = "t.owner_id_root = $this->owner_id_root AND t.partner_type = " . AppPromotion::PARTNER_TYPE_GRAB;
        while (true) {
            $code = MyFunctionCustom::getNextId($className, $prefix_code, $length_max_id, $fieldName, $needMore);
            if (!$this->getAppPromotionByCode($code)) {
                return $code;
            }
        }
    }
    
    /** @Author: DungNT Sep 05, 2018
     *  @Todo: xử lý sinh code mới theo 1 chuẩn 24hxxx cho all type Grab
     **/
    public function doMakeNewCode24h() {
        if($this->countCodeGrab() >= AppPromotion::PARTNER_MAX_USER){
            SendEmail::bugToDev("Mã code 24hxxx đã đạt max user. Kiểm tra và cho giải pháp sinh tiếp code");
            return $this->code_no;// ko thay đổi code cũ
        }
        $prefix_code = AppPromotion::PARTNER_DEFAULT_CODE;
        while (true) {
            $code = $prefix_code.$this->doMakeNewCode24hGenNumber();
            if (!$this->getAppPromotionByCode($code)) {
                return $code;
            }
        }
    }
    /** @Author: DungNT Sep 05, 2018
     *  @Todo: xử lý sinh 3 số ngẫu nhiên lớn hơn 100
     * từ 001 - > 099 dành cho các đối tác và GĐKV
     **/
    public function doMakeNewCode24hGenNumber() {
        while (true) {
            $number = ActiveRecord::randString(3, '0123456789') * 1;
            if ($number > 100) {
                return $number;
            }
        }
    }
   
    /** @Author: DungNT Sep 09, 2018
     *  @Todo: count code 24hxxx, if > 800 then stop gen 
     **/
    public function countCodeGrab() {
        $aPartnerFixCode        = $this->getArrayPartnerFixCode();
        unset($aPartnerFixCode[AppPromotion::PARTNER_ID_XUAN_THANH]);
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.owner_id_root IN (". implode(',', array_keys($aPartnerFixCode)).")");
        return AppPromotion::model()->count($criteria);
    }
    
    /** @Author: DungNT Sep 30, 2018
     *  @Todo: only update cache with news on app
     **/
    public function updateCache() {
        if(!$this->is_news){
            return ;
        }
        $this->setGas24hCacheNews();
        $this->updateCacheNewsList();
    }
    
    /** @Author: DungNT Sep 30, 2018
     *  @Todo: get name cache key
     **/
    public function getCacheKey() {
        return 'AppGas24hNews-'.$this->id;
    }
    /** @Author: DungNT Oct 11, 2018
     *  @Todo: get name cache key list
     **/
    public function getCacheKeyList() {
        return 'AppGas24hNewsList-'.$this->dirMenuList;
    }

    /** @Author: DungNT Sep 30, 2017
     *  @Todo: get from cache
     */
    public function getGas24hCacheNews() {
        $mAppCache = new AppCache();
        $value = $mAppCache->getCache($this->getCacheKey());
        if(empty($value)){
            $value = $this->setGas24hCacheNews();
        }
        return json_decode($value, true);
    }
    
    /** @Author: DungNT Sep 30, 2018
     *  @Todo: set cache for app view
     * 24 hours equal 86400 second
     **/
    public function setGas24hCacheNews() {
        $mAppPromotion = $this->getNewsView();
        if(is_null($mAppPromotion)){
            throw new Exception('Yêu cầu không hợp lệ');
        }
        $value = MyFormat::jsonEncode($mAppPromotion->formatAppItemList());
        $mAppCache = new AppCache();
        $mAppCache->setCache($this->getCacheKey(), $value, 86400);
        return $value;
    }
    
    /** @Author: DungNT Oct 11, 2017 
     *  @Todo: get from cache
     */
    public function getGas24hCacheNewsList() {
        $mAppCache = new AppCache();
        return  $mAppCache->getCache($this->getCacheKeyList());
    }
    public function setGas24hCacheNewsList($result) {
        $value = MyFormat::jsonEncode($result);
        $mAppCache = new AppCache();
        $mAppCache->setCache($this->getCacheKeyList(), $value, 0);// 86400 = 24h
        return $value;
    }
    
    /** @Author: DungNT Oct 12, 2018
     *  @Todo: set cache newslist
     **/
    public function updateCacheNewsList() {
        $q = new stdClass();
        $q->page            = 0;
//        $q->dirMenuList     = AppPromotion::APP_MENU_LIST;
        $result = GasConst::$defaultSuccessResponse;
        $mAppPromotion = new AppPromotion();
        $mAppPromotion->mAppUserLogin   = Users::model()->findByPk(1706360);// random user - is fake
        $mAppPromotion->appAction       = GasConst::ACTION_LIST;
        $mAppPromotion->dirMenuList     = AppPromotion::APP_MENU_LIST;
        $mAppPromotion->handleApiList($result, $q);
        $mAppPromotion->setGas24hCacheNewsList($result);
    }
    
    /** @Author: DungNT Now 17, 2018
     *  @Todo: get list owner_id by owner_id_root
     **/
    public function getByOwnerIdRoot($owner_id_root) {
        if(empty($owner_id_root)){
            return [];
        }
        $criteria = new CDbCriteria();
        $criteria->compare('t.owner_id_root', $owner_id_root);
        return self::model()->findAll($criteria);
    }
    
    /** @Author: DungNT Jan ,02 2019
     *  @Todo: validate type first order 
     *  Nếu KH đã mua hàng bằng 1 mã Khuyến mãi: sj100, gas24h, hcm100 thì không cho nhập tiếp mã này nữa
     *  @return: true if ok first order 
     *  false if not first order 
     **/
    public function checkTypeFirstOrder() {
//        if($this->type == AppPromotion::TYPE_FIRST_ORDER || in_array($this->id, $this->getListIdCheckFirstOrder())){
        if($this->type == AppPromotion::TYPE_FIRST_ORDER){
            $mReferralTracking = new ReferralTracking();
            $totalOrder = $mReferralTracking->countOrderDone($this->mAppUserLogin->id);
            if($totalOrder > 0){
                throw new Exception('Mã không hợp lệ, mã khuyến mãi này chỉ áp dụng cho đơn đặt app lần đầu');
            }
        }
    }
    
    /** @Author: DungNT Jan 17, 2019
     *  @Todo: for test notify on app
     **/
    public function runTestNotifyServerTest() {
        Logger::WriteLog('xxxxxxxx');
        if(GasCheck::isServerLive()){
            return ;
        }
        $aUid = [1298080];
        $this->RunInsertNotify($aUid, $this->title);
    }
    
    /** @Author: DungNT Jan 30, 2019
     *  @Todo: xử lý tạo lại khuyến mãi Cong tien thuong 20k gioi thieu nguoi dung
     *  do bị lỗi khi nhập type PARTNER_TYPE_GRAB he thong khong cong 20 cho KH binh thuong
     *  ham nay quet se fix, count ma gioi thieu da su dung cua KH x 20k va tao record moi
     * 1. Kiem tra xem co record chua promotion_type = AppPromotion::TYPE_REFERENCE_USER;
     *  neu co roi thi return
     * 2. neu chua co thi count ma gioi thieu da su dung cua KH va save record moi
     **/
    public function handleFixRecordBonusRef() {
        if($this->type != AppPromotion::TYPE_REFERENCE_USER || $this->partner_type == AppPromotion::PARTNER_TYPE_GRAB){
            return ;
        }
        $mAppPromotionUser = $this->getRecordBonusRef();
        if(!empty($mAppPromotionUser)){
            return ;
        }

        $model = new AppPromotionUser();
        $model->user_id             = $this->owner_id;
        $model->promotion_id        = $this->id;
        $model->promotion_amount    = $this->countTotalRefCompleteOrder() * AppPromotion::P_AMOUNT_OF_OWNER;
        $model->buildTitleRefUser();
        $model->promotion_type      = AppPromotion::TYPE_REFERENCE_USER;
        $model->time_use            = $this->time_use;
        $model->status_use          = AppPromotionUser::STATUS_NEW;
        $model->expiry_date         = MyFormat::modifyDays(date('Y-m-d'), 12, '+', 'month');
        $model->code_no             = $this->code_no;
        $model->save();
    }
    
    
    /** @Author: DungNT Jan 30, 2019
     *  @Todo: get record Bonus khi nguoi gioi thieu mua hang
     **/
    public function getRecordBonusRef() {
        $criteria = new CDbCriteria();
        $criteria->compare('t.promotion_type', AppPromotion::TYPE_REFERENCE_USER);
        $criteria->compare('t.user_id', $this->owner_id);
        $criteria->compare('t.promotion_id', $this->id);
        return AppPromotionUser::model()->find($criteria);
    }
    
    /** @Author: DungNT Jan 30, 2019
     *  @Todo: count total KH nhap ma gioi thieu da mua hang
     **/
    public function countTotalRefCompleteOrder() {
        $model = new AppPromotionUser();
        $model->promotion_id        = $this->id;
        $model->status_use          = AppPromotionUser::STATUS_COMPLETE;
        return $model->countPromotionByStatus();
    }
    
    /** @Author: DungNT Jun 17, 2019
     *  @Todo: update ip allow
     **/
    public function updateIpLimit($ip_address, $userId) {
        if(!array_key_exists($userId, $this->getArrayUserUpdateIpLimit())){
            return ;
        }
        if(empty($this->ip_limit)){
            $this->ip_limit = $ip_address;
        }else{
            $arrayIpLimit = explode(';', $this->ip_limit);
            if(in_array($ip_address, $arrayIpLimit)){
                return;
            }
            $this->ip_limit .= ';'.$ip_address;
        }
        $this->update(['ip_limit']);
    }
    
    /** @Author: DungNT Jun 17, 2019
     *  @Todo: check ip allow apply code OR not
     * kiểm tra xem ip có đc phép nhập code không
     **/
    public function canApplyCode($ip_address) {
        if(empty($this->ip_limit)){
            return false;
        }
        $arrayIpLimit = explode(';', $this->ip_limit);
        return in_array($ip_address, $arrayIpLimit);
    }
    
    /** @Author: DungNT Jun 17, 2019
     *  @Todo: check id promotion có phải loại phải check ip không
     *  @flow: cafe Kofikai chỉ cho phép nhập mã ở 1 địa chỉ IP của quán cafe
     **/
    public function checkIpLimit() {
        if(!in_array($this->id, $this->getArrayUserUpdateIpLimit())){
            return ;
        }
        $ip_address = MyFormat::getIpUser();
        if(!$this->canApplyCode($ip_address)){
            throw new Exception('Mã khuyến mãi không hợp lệ hoặc không áp dụng cho khu vực của bạn');
        }
    }
}
