<?php

/**
 * This is the model class for table "{{_error_hgd}}".
 *
 * The followings are the available columns in table '{{_error_hgd}}':
 * @property integer $id
 * @property integer $type
 * @property integer $agent_id
 * @property integer $employee_id
 * @property integer $transaction_history_id
 * @property string $created_date
 *  Aug 28, 2018 By Pham Thanh Nghia
 */
class ErrorHgd extends BaseSpj
{
    public $autocomplete_name,$autocomplete_name_1, $date_from,$date_to;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ErrorHgd the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_error_hgd}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            array('type, agent_id, employee_id, transaction_history_id, created_date', 'required'),
            array('type, agent_id, employee_id, transaction_history_id', 'numerical', 'integerOnly'=>true),
            ['id, type, agent_id, employee_id, transaction_history_id, created_date,date_from,date_to', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rEmployee' => array(self::BELONGS_TO, 'Users', 'employee_id'),
            'rTransactionHistory' => array(self::BELONGS_TO, 'TransactionHistory', 'transaction_history_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Loại lỗi',
            'agent_id' => 'Đại lý',
            'employee_id' => 'Nhân viên GN',
            'transaction_history_id' => 'Transaction History',
            'created_date' => 'Ngày tạo',
            'date_from' => 'Từ Ngày',
            'date_to' => 'Đến Ngày',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
	$criteria->compare('t.id',$this->id,true);
	$criteria->compare('t.type',$this->type,true);
        $criteria->compare('t.agent_id', $this->agent_id, true);
	$criteria->compare('t.employee_id',$this->employee_id,true);
	
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        }
        if(!empty($date_from)){
            $criteria->addCondition("DATE(t.created_date) >='$date_from'");
        }
        if(!empty($date_to)){
            $criteria->addCondition("DATE(t.created_date) <='$date_to'");
        }
        
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    /** @Author: Pham Thanh Nghia Aug 29 2018
     *  @Todo: taọ mới khi trong GasScheduleNotify :: saveErrorsHgd line 403
     **/
    public function makeRecordNew() {
        $model = new ErrorHgd();
        $mUsers = Users::model()->findByPk($this->employee_id);
        $model->type                    = $this->type;
        $model->employee_id             = $this->employee_id;
        $model->transaction_history_id  = $this->transaction_history_id;
        $model->agent_id                = !empty($mUsers->area_code_id) ? $mUsers->area_code_id : 0;
        $model->created_date            = date('Y-m-d H:i:s');
        $model->save();
    }
    /** @Author: Pham Thanh Nghia 2018
     *  @Todo: get Type error
     **/
    public function getTypeError(){
        $mGasScheduleNotify = new GasScheduleNotify();
        $aTypeError = $mGasScheduleNotify->getArrayTypeErrorsHgd();
        return isset($aTypeError[$this->type]) ? $aTypeError[$this->type] : '';
    }
    public function getAgent($field = 'first_name') {
        $mAgent = $this->rAgent;
        return !empty($mAgent) ? $mAgent->$field : '';
    }
    public function getEmployee($field = 'first_name') {
        $mEmployee = $this->rEmployee;
        return !empty($mEmployee) ? $mEmployee->$field : '';
    }
}