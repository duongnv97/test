<?php

/**
 * This is the model class for table "{{_gas_materials}}".
 *
 * The followings are the available columns in table '{{_gas_materials}}':
 * @property integer $id
 * @property integer $parent_id
 * @property string $materials_no 
 * @property string $name
 * @property string $unit
 * @property integer $materials_type_id
 * @property integer $status
 * @property integer $need_delete
 */
class GasMaterials extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasMaterials the static model class
     */
     public $MAX_ID, $autocomplete_material;
     // $MATERIAL_GAS dùng cho khi tạo PTTT Thương Hiệu Gas *	
     public static $MATERIAL_GAS = array(7,12,14,17,21,25,27,29,32,34,39,
                                        149, 150, 151, 166, 188, 190,
                                        218, 219, 220,225,236,
                                        67, 
                                        );
     // $MATERIAL_GAS dùng cho khi tạo PTTT  Thương Hiệu Gas *	
     public static $MATERIAL_KM = array(41);
     public static $MATERIAL_OTHER = array(105,113,116,127);

     public static $MATERIAL_TYPE_GAS_FOR_PROFILE = array(
            4, // Loại Vật Tư: Gas Bình 12 Kg
            7, // Loại Vật Tư: Gas Bình Bò 45 Kg
            9, // Loại Vật Tư: Gas Bình 6 Kg
            10, // Loại Vật Tư: Vỏ Bình Bò 45 Kg
            12, // Loại Vật Tư: Vỏ Bình Bò 50 Kg
            14, // Loại Vật Tư: Vỏ Bình Bò 6 Kg
    );
     
    /** @Author: DungNT Oct 04, 2017
     *  @Todo: id các vật tư không tính công
     */
    public static function getIdNotReportPay() {
        return [
            597, // Nút vặn bếp gia đình
        ];
    }
    // get id các loại gas bán trên app
    public function getIdApp() {
        return [
            594, // S Gas Vang
            586, // S Gas Cam
            400, // Unique Gas
            33, // Siam Gas
            10, // Origin Gas đỏ
            
//            1400, // Gas Petrosetco Hồng 12Kg
//            22, // Gas Gia đình xám 12
//            888, // Gas Hoàng Sa Hồng 12Kg
//            566, // Gas Hoàng Sa Xám 12Kg
//            8, // Gas Origin xám 12
//            222, // Gas Total Xám 12
        ];
    }

    public function getIdSetupBoMoi() {
        return [
            // Gas 50
            1405,1404, 883, 35,
            // Gas 45
            1403, 1402, 581, 37,
            // Gas 12
            371, 586, 595, 604, 8, 33, 575, 222, 378, 606, GasMaterials::ID_GAS_12_VANG_SJ_SGAS,
            // Gas 6
            578, 216,
            // Vo 50
            884, 100,
            // Vo 45
            582, 98,
            // Vo 12
            372, 587, 587, 68, 75, 372, 92, 369, 608,
            // Vo 6
            104, 248
        ];
    }
    
    /** @Author: DungNT Now 16, 2017
     *  @Todo: get 1 price cho app từ config setting
     * @use_1: model Transaction function getPostDetail
     * @use_2: model AppOrder function handleGetConfig
     **/
    public function getAppPrice() {
        return Yii::app()->setting->getItem('Gas24hPrice');
    }

    const NAME_MATERIAL_JSON = 'NAME_MATERIAL_JSON';
    
    const ID_VO_TAP_12      = 138;
    const ID_VO_12_SOUTHERN = 587;// Vỏ Southern Petroleum 12
    const ID_VO_12_SJ_PETROL  = 372;// Vỏ SJ Petrol 12
    const ID_VO_12_SJ_6       = 104;// Vỏ SJ Petro, Sgas 6 kg
    const ID_VO_12_SJ_4       = 103;// Vỏ SJ Petro, Sgas 4 kg
    
    const ID_DAY_DEN            = 114;// Dây đen
    const ID_DAY_INOX           = 115;
    const ID_DAY_MEM_CN         = 124;// Dây mềm công nghiệp
    
    /** Gas cty **/
    const ID_GAS_12_CAM                 = 586;// S Gas Cam Van Ngang 12Kg
    const ID_GAS_12_CAM_SJ              = 1882;// Gas SJ Petrol Cam Van Ngang 12Kg
    const ID_GAS_12_XANH_SIAM           = 279;//  Siam Xanh Van Ngang 12Kg
    const ID_GAS_12_VANG_SJ_SGAS        = 594;// S Gas Vàng Van Ngang 12Kg
    
    /** Gas Xám **/
    const ID_GAS_12_XAM_ORIGIN          = 8;// Origin Xám Van Ngang 12Kg
    const ID_GAS_12_XAM_GIA_DINH        = 22;// Gia Đình Xám Van Ngang 12Kg
    const ID_GAS_12_XAM_TOTAL           = 222;// Total Xám Van Ngang 12Kg
    const ID_GAS_12_XAM_SJ_PETROL       = 371;// SJ Petrol Xám Van Ngang 12Kg 
    const ID_GAS_12_XAM_YES             = 378;// Yes Gas Xám Van Ngang 12Kg
    const ID_GAS_12_XAM_HOANG_SA        = 566;// Gas Hoàng Sa Xám 12Kg
    const ID_GAS_12_XAM_LP_PETRO        = 1424;// Lp Petro Xám Van Ngang 12Kg
    
    
    
    const ID_GAS_BON        = 245;// Gas Bồn
    
    const STATUS_ACTIVE     = 1;
    const STATUS_DEACTIVE   = 0;
    
    
    /** @Author: DungNT Jan 23, 2019
     *  @Todo: get array id binh Xam 
     **/
    public function getArrayIdXam() {
        return [
            GasMaterials::ID_GAS_12_XAM_ORIGIN,
            GasMaterials::ID_GAS_12_XAM_GIA_DINH,
            GasMaterials::ID_GAS_12_XAM_TOTAL,
            GasMaterials::ID_GAS_12_XAM_SJ_PETROL,
            GasMaterials::ID_GAS_12_XAM_YES,
            GasMaterials::ID_GAS_12_XAM_HOANG_SA,
            GasMaterials::ID_GAS_12_XAM_LP_PETRO,
        ];
    }
    
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_materials}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('materials_no, name,materials_type_id', 'required'),
            array('materials_no','unique','message'=>'Mã vật tư này đã tồn tại, vui lòng nhập mã khác'),
        //,'on'=>'create_materials,update_materials'
            array('materials_no', 'length', 'max'=>20),
            array('name', 'length', 'max'=>300),
            array('name_vi,id, parent_id, materials_no, name, unit, materials_type_id, status,name_store_card', 'safe'),
            array('description, materials_id_vo, need_delete, image, price', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'parent' => array(self::BELONGS_TO, 'GasMaterials', 'parent_id'),
            'rMaterialsVo' => array(self::BELONGS_TO, 'GasMaterials', 'materials_id_vo'),
            'materials_type' => array(self::BELONGS_TO, 'GasMaterialsType', 'materials_type_id'),
            'rMaterialsType' => array(self::BELONGS_TO, 'GasMaterialsType', 'materials_type_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'parent_id' => 'Parent - Nhóm',
            'materials_no' => 'Mã Vật Tư',
            'name' => 'Tên Vật Tư',
            'unit' => 'Đơn Vị',
            'materials_type_id' => 'Loại Vật Tư',
            'status' => 'Trạng thái',
            'name_store_card' => 'Tên ngắn cho App Gas24h',
            'price' => 'Giá tiền',
            'image' => 'URL hình ảnh',
            'need_delete' => 'Đánh dấu vật tư cần xóa',
            'materials_id_vo' => 'Vỏ tương ứng',
            'description' => 'Quy cách vật tư',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.id',$this->id);
        $criteria->compare('t.parent_id',$this->parent_id);
        $criteria->compare('t.materials_no',$this->materials_no,true);
        $criteria->compare('t.name_vi',$this->name,true);
        $criteria->compare('t.materials_type_id',$this->materials_type_id);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('t.need_delete', $this->need_delete);

        $sort = new CSort();
        $sort->attributes = array(
            'parent_id'=>'parent_id',
            'materials_no'=>'materials_no',
            'name'=>'name',
            'materials_type_id'=>'materials_type_id',
            'status'=>'status',
            'unit'=>'unit',
        );    
        $sort->defaultOrder = "t.id DESC";
        
         $_SESSION['data-excel-materials'] = new CActiveDataProvider($this, array(
            'pagination'=>false,
            'criteria'=>$criteria,
            'sort' => $sort,
        ));
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 60,
            ),
            'sort' => $sort,
        ));
    }

    
    public function activate()
    {
        $this->status = 1;
        $this->update();
    }

    public function deactivate()
    {
        $this->status = 0;
        $this->update();
    }
    public function getNeedDelete(){
        if($this->need_delete){
            return 'YES';
        }
        return '';
    }
    /*
     * 23-07-2013 - Nguyen Dung
     * To do: get array categories by parent id 
     * @param: $parent_id
     * @return: array sub category
     */
    public static function getCatByParentId($parent_id,$for_export=0, $arrIdParent=array()){
        $criteria=new CDbCriteria;
        $criteria->compare('t.parent_id', $parent_id);
        if($for_export)
            $criteria->addCondition('t.id <>'.ID_MATERIAL_KHUYEN_MAI);
        if(count($arrIdParent)>0){
            $sParamsIn = implode(',', $arrIdParent);
            $criteria->addCondition("t.id IN ($sParamsIn)");
        }
        $criteria->compare('t.status',STATUS_ACTIVE);
        $criteria->order = 't.id DESC';
        $models = GasMaterials::model()->findAll($criteria);
        if($for_export)
            return  CHtml::listData($models,'id','id');
        return  CHtml::listData($models,'id','name');
    }

    public static function getOnlyCatByParentId($parent_id,$needMore=array()){
        $criteria=new CDbCriteria;
        $criteria->compare('t.parent_id', $parent_id);
//		$criteria->compare('t.status',STATUS_ACTIVE);
        $models = GasMaterials::model()->findAll($criteria);
        return  CHtml::listData($models,'id','name');
    }

    public static function getObjCatByParentId($parent_id){
            $criteria=new CDbCriteria;
            $criteria->compare('t.parent_id', $parent_id);
//		$criteria->compare('t.status',STATUS_ACTIVE);
            $models = GasMaterials::model()->findAll($criteria);
            $aRes = array();
            if(count($models)>0)
            foreach($models as $item)
                $aRes[]=$item; 
            return  $aRes;
    }


    public function beforeSave() {
        if(is_null($this->parent_id) || empty($this->parent_id)){
            $this->parent_id=0;
        }
        $this->name = trim($this->name);
        $this->name_vi = strtolower(MyFunctionCustom::remove_vietnamese_accents($this->name));
        return parent::beforeSave();
    }
    
    protected function afterSave() {
        $mAppCache = new AppCache();
        $mAppCache->setAppMaterialsStorecard(AppCache::ARR_APP_MATERIAL_STORECARD, []);
        return parent::afterSave();
    }

/*
     * 23-07-2013 - Nguyen Dung
     * To do: get array Material by parent id 
     * @param: $parent_id
     * @return: array sub Material
     */
    public static function getCatLevel2($arrIdParent=array()){
            $level1 = GasMaterials::getCatByParentId(0, 0, $arrIdParent);

            $aRes = array();
            if(count($level1)>0)
            foreach($level1 as $key=>$itemL1){
                $criteria=new CDbCriteria;
                $criteria->addCondition('t.parent_id<>0');
                $criteria->compare('t.parent_id', $key);
                $criteria->compare('t.status',STATUS_ACTIVE);
                $criteria->order = "t.name asc";
                $models = GasMaterials::model()->findAll($criteria);
                if ($models) {
                    foreach ($models as $item)
                        $aRes[] = array('id' => $item->id, 'name' => $item->name, 'group' => $itemL1);
                }
            }
            return  $aRes;
    }    

    public static function getArrMaterialForImport()
    {
        $criteria = new CDbCriteria;      
        $models = self::model()->findAll($criteria);
        return  CHtml::listData($models,'materials_no','id');            
    }            
    			
    public static function getArrObjMaterialForExport()
    {
		$aRes=array();
        $criteria = new CDbCriteria;    
		$criteria->compare('t.parent_id', 0);		
        $models = self::model()->findAll($criteria);
		foreach($models as $item){
			$criteria = new CDbCriteria;    
			$criteria->compare('t.parent_id', $item->id);
                        $criteria->compare('t.status', STATUS_ACTIVE);
			$modelsSub = self::model()->findAll($criteria);		
			$temp=array();
			$temp['parent_obj'] = $item->id;
			$temp['sub_arr_id'] = CHtml::listData($modelsSub,'id','id');  
			$aRes[]=$temp;			
		}
		/* echo '<pre>';
		echo print_r($aRes);
		echo '</pre>';
		die; */
        return  $aRes;            
    }  
	
    // return array id=>obj
    public static function getArrAllObjMaterial()
    {
        $aRes=array();	
        $models = self::model()->findAll();
        foreach($models as $item){
            $aRes[$item->id]=$item;			
        }
        return  $aRes;            
    }    	
    
    public function canDelete()
    {
        $count = self::model()->count("parent_id = ".$this->id);
        $count1 = GasMaintain::model()->count("materials_id = ".$this->id);
        $count2 = GasMaintainSell::model()->count("materials_id_sell = ".$this->id);
        $count3 = GasMaintainSell::model()->count("materials_id_back = ".$this->id);
        $count4 = GasOneManyBig::model()->count("many_id = ".$this->id.' AND type='.ONE_SELL_MAINTAIN_PROMOTION);
        
        return !$count && !$count1 && !$count3 && !$count4;
        if( $count > 0)
        {
            return false;
        }
        return true;
    }  
    
    /**
     * @Author: DungNT 12-18-2013
     * @Todo: get array material parent=> array sub material
     */    
    public static function getArrayModelMaterials(){
            $aRes=array();
        $criteria = new CDbCriteria;    
        $criteria->compare('t.parent_id', 0);		
        $criteria->order = 't.name ASC';		
        $models = self::model()->findAll($criteria);
        foreach($models as $item){
                $criteria = new CDbCriteria;    
                $criteria->compare('t.parent_id', $item->id);
                $criteria->compare('t.status', STATUS_ACTIVE);
                $criteria->order = 't.name ASC';
                $modelsSub = self::model()->findAll($criteria);		
                $temp=array();
                $temp['parent_obj'] = $item;
                $temp['sub_arr_model'] = $modelsSub;  
                $aRes[]=$temp;			
        }
        return  $aRes;    
    }
    
    /**
     * @Author: DungNT 12-30-2013
     * @Todo: get array material parent=> array sub material + array sub_chtml_listData
     */    
    public static function getArrayModelParentAndListSubId($needMore = array()){
        $aRes=array();
        $criteria = new CDbCriteria;    
        $criteria->compare('t.parent_id', 0);
//        $criteria->order = 't.name ASC';
        $criteria->order = 't.materials_type_id ASC, t.name ASC';
        $models = self::model()->findAll($criteria);
        foreach($models as $item){
                $criteria = new CDbCriteria;    
                $criteria->compare('t.parent_id', $item->id);
                if(!isset($needMore['GetAll'])){
                    $criteria->compare('t.status', STATUS_ACTIVE);
                }
                $criteria->order = 't.name ASC';
                $modelsSub = self::model()->findAll($criteria);		
                $temp=array();
                $temp['parent_obj'] = $item;
                $temp['sub_arr_model'] = $modelsSub;  
                $temp['sub_chtml_listData'] = CHtml::listData($modelsSub,'id','id');
                $aRes[]=$temp;			
        }
        return  $aRes;    
    }
	
    /**
     * @Author: DungNT 02-07-2014
     * @Todo: lấy id những vật tư theo loại vật tư (materials_type_id)
     * @Param: array $material_type_id 
     * @Return: array CHtml::listData($models,'id','id');
     */    
    public static function getArrayIdByMaterialTypeId($arr_material_type_id, $needMore=array()){
        $criteria=new CDbCriteria;
        if(is_array($arr_material_type_id)){
            $sParamsIn = implode(',', $arr_material_type_id);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.materials_type_id IN ($sParamsIn)");
            }
        }elseif(!empty ($arr_material_type_id)){
            $criteria->addCondition('t.materials_type_id=' . $arr_material_type_id);
        }
        if(isset($needMore['order'])){
            $criteria->order = $needMore['order'];
        }
        
        if(isset($needMore['NOT_IN_1'])){ // at Statistic::AgentNXT is CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT
            $sParamsIn = implode(',', $needMore['NOT_IN_1']);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.materials_type_id NOT IN ($sParamsIn)");
            }
        }
        if(isset($needMore['NOT_IN_2'])){ // is CmsFormatter::$MATERIAL_TYPE_BINHBO_INPUT,
            $sParamsIn = implode(',', $needMore['NOT_IN_2']);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.materials_type_id NOT IN ($sParamsIn)");
            }
        }
        if(isset($needMore['listId'])){ // at Statistic::AgentNXT is CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT
            $sParamsIn = implode(',', $needMore['listId']);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.id IN ($sParamsIn)");
            }
        }
        
        if(!isset($needMore['GetAll'])){// add Now 27, 2016
            $criteria->addCondition('t.status=' . STATUS_ACTIVE);
        }
        $criteria->addCondition('t.parent_id<>0');
        $models = GasMaterials::model()->findAll($criteria);
        if(isset($needMore['ListIdName'])){
            return  CHtml::listData($models,'id','name');
        }
        if(isset($needMore['ListIdModel'])){
            $aRes = array();// Jun 12, 2016
            foreach($models as $item){
                $aRes[$item->id] = $item;
            }
            return  $aRes;
        }
        return  CHtml::listData($models,'id','id');
    }    
    
    /**
     * @Author: DungNT Jun 08, 2014
     * @Todo: lấy mảng id=>model
     * @Param: $aMaterialId mảng id
     */
    public static function getArrayModel($aMaterialId){
        $aRes = array();
        $criteria=new CDbCriteria;
        if(is_array($aMaterialId)){
            $sParamsIn = implode(',', $aMaterialId);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.id IN ($sParamsIn)");
            }
        }else{
//            return array(); // Oct 01, 2016 chỗ này không cần xử lý elsse, sẽ cho load hết model ra
        }
        $models = GasMaterials::model()->findAll($criteria);
        foreach($models as $item){
            $aRes[$item->id] = $item;
        }
        return $aRes;
    }
    
    /**
     * @Author: DungNT Jun 28, 2014
     * @Todo: lấy mảng materials_type_id=>model
     * @Param: $aMaterialTypeId mảng materials_type_id
     */
    public static function getArrayModelMaterialType($aMaterialTypeId){
        $aRes = array();
        $criteria=new CDbCriteria;
        $sParamsIn = implode(',', $aMaterialTypeId);
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.materials_type_id IN ($sParamsIn)");
        }
        $criteria->addCondition('t.parent_id<>0');
        $criteria->compare('t.status', STATUS_ACTIVE);
        $criteria->order = 't.materials_no ASC';
        $models = GasMaterials::model()->findAll($criteria);
        foreach($models as $item){
            $aRes[$item->materials_type_id][] = $item;
        }
        return $aRes;
    }
    
    /**
     * @Author: DungNT Jun 28, 2014 
     * @Todo: lấy mảng materials_type_id=>model
     * @Param: $aMaterialTypeId mảng materials_type_id
     */
    public static function getByMaterialType($aMaterialTypeId){
        $criteria = new CDbCriteria();
        if(is_array($aMaterialTypeId)){
            $sParamsIn = implode(',', $aMaterialTypeId);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.materials_type_id IN ($sParamsIn)");
            }
        }else{
            $criteria->addCondition('t.materials_type_id='.$aMaterialTypeId);
        }

        $criteria->addCondition('t.parent_id<>0 AND t.status='.STATUS_ACTIVE);
        $criteria->order = "t.name_vi ASC";
        return GasMaterials::model()->findAll($criteria);
    }
    
    /**
     * @Author: DungNT Apr 18, 2016
     * @Todo: make any session 
     */
    public static function sessionSetListData($name, $value) {
        $session=Yii::app()->session;
        if(!isset($session[$name])){
            $session[$name] = $value;
        }
    }
    public static function sessionGetListData($name) {
        $session=Yii::app()->session;
        if(isset($session[$name])){
            return $session[$name];
        }
        return array();
    }
    
    /**
     * @Author: DungNT Sep 19, 2014
     * @Todo: admin/gasProfile/create
     */
    public static function getListOptionForProfile()
    {
        $criteria = new CDbCriteria;
        $sParamsIn = implode(',', GasMaterials::$MATERIAL_TYPE_GAS_FOR_PROFILE);
        $criteria->addCondition("t.materials_type_id IN ($sParamsIn)");
        $criteria->addCondition('t.parent_id=0');
        $criteria->order = 't.materials_type_id';
        $models = self::model()->findAll($criteria);
        return  CHtml::listData($models,'id','name');
    }   
    
    /**
     * @Author: DungNT Sep 19, 2014
     * @Todo: admin/gasProfile/create
     */
    public static function getListOptionByArrId($aId,$needMore=array()){
        $criteria=new CDbCriteria;
        $sParamsIn = implode(',', $aId);
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.id IN ($sParamsIn)");
        }else{
            return array();
        }
        $criteria->compare('t.status', STATUS_ACTIVE);
        $criteria->order = 't.name';
        $models = GasMaterials::model()->findAll($criteria);
        return  CHtml::listData($models,'id','name');
    }
    
    /**
     * @Author: DungNT Jun 05, 2016
     */
    public function getName() {
        return ($this->name) ? $this->name : "";
    }
    public function getUnit() {
        return $this->unit;
    }
    public function getNameShort() {
        return $this->name_store_card;
    }
    public function getDescription() {
        return $this->description;
    }
    
    /**
     * @Author: DungNT Nov 06, 2016
     */
    public function getImage($getFull = false) {
        if(empty($this->image)){
            return '';
        }
        $str = '';
        $str.="<a class='gallery' href='{$this->image}'> ";
            if($getFull){
                $str.="<img src='{$this->image}'>";
            }else{
                $str.="<img width='80' height='60' src='{$this->image}'>";
            }
        $str.='</a>';
        return $str;
    }
    public function getImageUrl() {
        return $this->image;
    }
    
    /**
     * @Author: DungNT Nov 20, 2016
     */
    public static function getListParentByType($materials_type_id, $needMore = array()) {
        $criteria=new CDbCriteria;
//        $criteria->addCondition('t.materials_type_id='.$materials_type_id.' AND t.parent_id=0 AND t.status='.STATUS_ACTIVE);// Close on Jan 17, 2017
        $criteria->addCondition('t.materials_type_id='.$materials_type_id.' AND t.status='.STATUS_ACTIVE);
        $criteria->select = 'id, CONCAT(materials_no," - ", name) as name, parent_id, materials_type_id, name_vi ';
//        $criteria->order = 't.name ASC';
        $models = self::model()->findAll($criteria);
        if(isset($needMore['getModel'])){
            return $models;
        }
        return  CHtml::listData($models,'id','name');
    }
    
    
    /**
     * @Author: DungNT Mar 02, 2017
     * @use_at: get dataCacheApp chỉ lấy dữ liệu sử dụng cho thẻ kho App hoặc web
     */
    public function getMaterialsForStorecard($needMore = array()){
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.parent_id<>0');
//        $sParamsIn = implode(',', GasMaterialsType::$ARR_NOT_SEARCH);
//        $criteria->addCondition("t.materials_type_id NOT IN ($sParamsIn)");
        
        if(!isset($needMore['GetAll'])){
            $criteria->compare('t.status', STATUS_ACTIVE);
        }
        $models = GasMaterials::model()->findAll($criteria);        
        $aRes = [];// sẽ format 1 định dạng chung cho app
        foreach($models as $model){
            $aRes[] = $model->appFormatDetail();
        }
        return $aRes;
    }
    
    /**
     * @Author: DungNT Mar 02, 2017
     * @Todo: format chung cho detail response của record Appp
     */
    public function appFormatDetail() {
        $temp = [];
        $temp['materials_id']       = $this->id;
        $temp['materials_no']       = $this->materials_no;
        $temp['materials_type_id']  = $this->materials_type_id;
//        $temp['materials_name']     = $this->name.' - '.$this->materials_no;
        $temp['materials_name']     = $this->name;
        $temp['unit']               = $this->unit;
        return $temp;
    }
    
    public function getVo() {
        if(empty($this->materials_id_vo)){
            return ;
        }
        $mMaterial = $this->rMaterialsVo;
        if($mMaterial){
            return $mMaterial->getName();
        }
    }

    /**
     * @Author: DungNT Jun 04, 2017
     * @Todo: admin/inventoryCustomer/create get các loại vỏ để đưa vào tồn kho của KH
     */
    public static function getListVoInventory()
    {
        $aType      = [GasMaterialsType::MATERIAL_VO_50, GasMaterialsType::MATERIAL_VO_45];
        $aIdVo12    = [GasMaterials::ID_VO_12_SOUTHERN, GasMaterials::ID_VO_12_SJ_6, GasMaterials::ID_VO_12_SJ_4, GasMaterials::ID_VO_12_SJ_PETROL];
        $criteria = new CDbCriteria;
        $sParamsIn = implode(',', $aType);
        $sParamsIn1 = implode(',', $aIdVo12);
        $criteria->addCondition("t.materials_type_id IN ($sParamsIn) OR t.id IN ($sParamsIn1)");
        $criteria->addCondition('t.parent_id=0');
        $criteria->order = 't.materials_type_id';
        $models = self::model()->findAll($criteria);
        return  CHtml::listData($models,'id','name');
    }
    
    /** @Author: DungNT Jan 03, 2018 **/
    public function getAllActive(){
        $criteria=new CDbCriteria;
        $criteria->addCondition('t.status=' . STATUS_ACTIVE);
        $models = GasMaterials::model()->findAll($criteria);
        $aRes = [];
        foreach($models as $model){
            $aRes[$model->id] = $model;
        }
        return $aRes;
    }
    
    /** @Author: DungNT Aug 03, 2018
     *  @Todo: map vỏ với gas tương ứng
     * @return: true if map, false if not map
     **/
    public function mapGasVo($aGasVo, $gas_id, $vo_id) {
        $res  = false;
        if(isset($aGasVo[$gas_id]) && $aGasVo[$gas_id] == $vo_id){
            $res  = true;
        }
        return $res;
    }
    
    /** @Author: DungNT Dec 09, 2018
     *  @Todo: get list các id vỏ của cty SPJ
     **/
    public function getSpjBrandVo() {
        return [
            369,// Vỏ Yes Gas 12
            372,// Vỏ SJ Petrol 12
            587,// Vỏ S Gas 12Kg
            608,// Vỏ PT 12
        ];
    }
    
     public function getArrayStatus(){
        return[
            GasMaterials::STATUS_ACTIVE => 'Hoạt động',
            GasMaterials::STATUS_DEACTIVE => 'Không hoạt động',
        ];
    }
    
     public function getStatus(){
        $aStatus = $this->getArrayStatus();
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : "";
    }
    
}