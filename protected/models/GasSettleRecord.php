<?php

/**
 * This is the model class for table "{{_settle_record}}".
 *
 * The followings are the available columns in table '{{_settle_record}}':
 * @property string $id
 * @property integer $settle_id
 * @property string $payment_no
 * @property string $item_name
 * @property string $unit
 * @property string $qty
 * @property string $price
 * @property string $total
 * @property string $note
 */
class GasSettleRecord extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasSettleRecord the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_settle_record}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('settle_id, item_name, qty, price', 'required', 'on'=>'create'),
            array('distributor_id, plan_unit, plan_qty, plan_price, plan_total, id, settle_id, payment_no, item_name, unit, qty, price, total, note', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'settle_id' => 'Tham chiếu tạm ứng',
            'payment_no' => 'Số hóa đơn',
            'item_name' => 'Diễn giải',
            'unit' => 'Đơn vị',
            'qty' => 'Số lượng',
            'price' => 'Đơn giá',
            'total' => 'Thành tiền',
            'note' => 'Ghi chú',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('t.settle_id', $this->settle_id);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    /**
     * @Author: ANH DUNG Jul 30, 2016
     */
    public function getItemName() {
        return $this->item_name;
    }
    
    public function getPlanUnit() {
        return $this->plan_unit;
    }
    public function getPlanQty($fomat = false) {
        if($fomat){
            return ActiveRecord::formatCurrency($this->plan_qty);
        }
        return $this->plan_qty;
    }
    public function getPlanPrice($fomat = false) {
        if($fomat){
            return ActiveRecord::formatCurrency($this->plan_price);
        }
        return $this->plan_price;
    }
    public function getPlanTotal($fomat = false) {
        if($fomat){
            return ActiveRecord::formatCurrency($this->plan_total);
        }
        return $this->plan_total;
    }
    public function getPaymentNo() {
        return $this->payment_no;
    }
    public function getUnit() {
        return $this->unit;
    }
    public function getQty($fomat = false) {
        if($fomat){
            return ActiveRecord::formatCurrency($this->qty);
        }
        return $this->qty;
    }
    public function getPrice($fomat = false) {
        if($fomat){
            return ActiveRecord::formatCurrency($this->price);
        }
        return $this->price;
    }
    public function getTotal($fomat = false) {
        if($fomat){
            return ActiveRecord::formatCurrency($this->total);
        }
        return $this->total;
    }
    public function getNote() {
        return nl2br($this->note);
    }
    
    /**
     * @Author: ANH DUNG Jul 30, 2016
     * @Todo: remove child item of settle
     * @note: chú ý nên deleteAll với những detail ko có ref liên quan, để đỡ perform
     */
    public static function deleteBySettleId($settle_id) {
        if(empty($settle_id)){
            return ;
        }
        $criteria = new CDbCriteria;
        $criteria->compare("settle_id", $settle_id);
        GasSettleRecord::model()->deleteAll($criteria);
    }
    
        
    /**
     * @Author: ANH DUNG Apr 09, 2017
     * @Todo: get name NPP của chị Thủy
     */
    public function getDistributor() {
        $aDistributor = CacheSession::getDistributor();
        return isset($aDistributor[$this->distributor_id]) ? $aDistributor[$this->distributor_id]['first_name'] : '';
    }
    
}