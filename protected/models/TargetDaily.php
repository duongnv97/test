<?php

/** 
 * Kiểm tra có trùng sell id hay không: SELECT * FROM `gas_target_daily` t WHERE t.sell_id >0 and exists (select * from `gas_target_daily` a WHERE a.sell_id = t.sell_id and t.id < a.id)
 * This is the model class for table "{{_target_daily}}".
 *
 * The followings are the available columns in table '{{_target_daily}}':
 * @property string $id
 * @property integer $type
 * @property string $customer_id
 * @property integer $type_customer
 * @property string $sale_id 
 * @property string $agent_id
 * @property integer $province_id
 * @property string $created_by
 * @property string $monitoring_id
 * @property string $gdkv_id
 * @property integer $materials_id
 * @property integer $seri
 * @property string $phone
 * @property string $sell_id
 * @property string $sell_date
 * @property string $sell_date_bigint
 * @property string $sell_total
 * @property string $sell_grand_total
 * @property string $created_date
 * @property string $created_date_bigint
 */
class TargetDaily extends BaseSpj
{
    const TYPE_REMOVE_OVERLOAD          = 1; // Remove các bình quá 3 tháng không quay về từ khi cài app
    const TYPE_REMOVE_OVERLOAD_120      = 2; // Remove các bình quá 4 tháng không quay về từ khi cài app
    const TYPE_REMOVE_OVERLOAD_60       = 3; // Remove các bình quá 2 tháng không quay về từ khi cài app
    const COUNT_OUT_OF_DATE_APP         = 90;
    const COUNT_OUT_OF_DATE_APP_120     = 120;
    const COUNT_OUT_OF_DATE_APP_60      = 60;
    const COUNT_OUT_OF_DATE_POINT       = 30;// số ngày kế tiếp có thể 
    
    const TYPE_CCS                      = 1;
    const TYPE_CCS_SHARE                = 2;
    const TYPE_TELESALE_BQV_CALL        = 3;
    const TYPE_TELESALE_BQV_APP         = 4;
    const TYPE_PTTT_BQV                 = 5;
    const TYPE_PTTT_POINT               = 6;
    const TYPE_EXPIRED                  = 7;
    const TYPE_PTTT_NO_CODE             = 8;
    const TYPE_TELESALE_RESCAN          = 9;
    const TYPE_USERS_HGD_PTTT           = 10;
    
    const TYPE_CUSTOMER_OF_AGENT        = 1;
    
    const PROMOTION_ITEM                = 1;
    const PROMOTION_ITEM_V2             = 2;
    public $autocomplete_name, $autocomplete_name_1, $autocomplete_name_2,$autocomplete_name_3,$support_id_yes,$code_no;
    public $is_purchased,$file_excel;
    public $date_c_from, $date_c_to,$date_from_v2,$date_to_v2,$dateFollow,$promotionItem,$is_telesale,$is_pttt;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return TargetDaily the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_target_daily}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['promotionItem,date_from_v2,date_to_v2,date_from,date_to,id, type, customer_id, type_customer, sale_id, agent_id, province_id, created_by, monitoring_id, gdkv_id, materials_id, seri, phone, sell_id, sell_date, sell_total, sell_grand_total, created_date, created_date_bigint', 'safe'],
            ['is_customer_agent,code_no,is_pttt,is_telesale,file_excel,support_id_yes,support_id,type_remove,app_promotion_user_id,is_purchased, date_c_from, date_c_to', 'safe']
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rSale' => array(self::BELONGS_TO, 'Users', 'sale_id'),
            'rCreatedBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'rMonitoring' => array(self::BELONGS_TO, 'Users', 'monitoring_id'),
            'rGdkv' => array(self::BELONGS_TO, 'Users', 'gdkv_id'),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Loại',
            'customer_id' => 'Khách hàng',
            'type_customer' => 'Loại khách hàng',
            'sale_id' => 'Sale',
            'agent_id' => 'Đại lý',
            'province_id' => 'Tỉnh',
            'created_by' => 'Người tạo',
            'monitoring_id' => 'Giám sát',
            'gdkv_id' => 'Gdkv',
            'materials_id' => 'Vật tư',
            'seri' => 'Seri',
            'phone' => 'Phone',
            'sell_id' => 'Sell',
            'sell_date' => 'Ngày mua',
            'sell_total' => 'Sell Total',
            'sell_grand_total' => 'Sell Grand Total',
            'created_date' => 'Ngày tạo',
            'created_date_bigint' => 'Created Date Bigint',
            'date_from' => 'Từ ngày',
            'date_to' => 'Đến',
            'is_customer_agent' => 'Nguồn KH',
        ];
    }
    /** @Author: NamNH Aug 05, 2019
     *  @Todo: get array remove
     **/
    public function getArrayRemove() {
        return [
            self::TYPE_REMOVE_OVERLOAD,//90 ngày
            self::TYPE_REMOVE_OVERLOAD_120,//120 ngày
            self::TYPE_REMOVE_OVERLOAD_60,// 60 ngày
        ];
    }
    
    /** @Author: DuongNV Feb 22,2019
     *  @Todo: get array type
     **/
    public function getArrayType() {
        return [
            self::TYPE_CCS              => "PTTT App + CCS",
            self::TYPE_CCS_SHARE        => "BQV Chia sẽ",
            self::TYPE_TELESALE_BQV_CALL => "TELESALE CALL",
            self::TYPE_TELESALE_RESCAN  => 'TELESALE Rescan',
            self::TYPE_TELESALE_BQV_APP => "APP Rescan",
            self::TYPE_PTTT_BQV         => "Code PTTT",
            self::TYPE_PTTT_POINT       => "PTTT Có Telesale gọi",
            self::TYPE_EXPIRED          => "App hết hạn",
            self::TYPE_PTTT_NO_CODE     => "BQV Seri + Vỏ + SĐT",
            self::TYPE_USERS_HGD_PTTT   => "Hgd PTTT",
        ];
    }

    public function getTypeRemoveTelesaleBqv(){
        return [
          self::TYPE_CCS,
          self::TYPE_TELESALE_BQV_CALL,
          self::TYPE_TELESALE_BQV_APP,
          self::TYPE_TELESALE_RESCAN,
        ];
    }
    
    public function getTypeTelesaleBqv($isCcs = true){
        $aResult = [
            self::TYPE_CCS,
            self::TYPE_TELESALE_BQV_CALL,
            self::TYPE_TELESALE_BQV_APP,
            self::TYPE_TELESALE_RESCAN,
        ];
        if(!$isCcs){ // truong hợp có loại bỏ BQV self::TYPE_CCS do đã tính trước đó
            $aResult = [
                self::TYPE_TELESALE_BQV_CALL,
                self::TYPE_TELESALE_BQV_APP,
                self::TYPE_TELESALE_RESCAN,
            ];
        }
        return $aResult;
    }
    
    public function getTypeNotUpdateCreatedBy(){
        return [
            self::TYPE_PTTT_BQV,
            self::TYPE_PTTT_NO_CODE
        ];
    }
    
    public function getArrayTypeRemoveDays(){
        return [
            self::TYPE_REMOVE_OVERLOAD_60 => self::COUNT_OUT_OF_DATE_APP_60,
            self::TYPE_REMOVE_OVERLOAD => self::COUNT_OUT_OF_DATE_APP,
            self::TYPE_REMOVE_OVERLOAD_120 => self::COUNT_OUT_OF_DATE_APP_120,
        ];
    }
        
//      NamNH Jul 25,2019  get month check thời gian được xem là khách hàng củ nếu có đặt gas.
    public function getMonthCheck($mSell){
        $mGasTargetMonthly = new GasTargetMonthly();
        $aOneMany   = $mGasTargetMonthly->getArrayOneManyMonitorCcs('',GasOneMany::TYPE_AGENT_CITY);
        $AgentCity      = CHtml::listData($aOneMany, 'many_id', 'many_id');
        $monthCheck = 4;// tỉnh thì check lui 4 tháng self::TYPE_REMOVE_OVERLOAD_120
        if($mSell->province_id == GasProvince::TP_HCM){
            $monthCheck = 2;// TP thì check lui 2 tháng self::TYPE_REMOVE_OVERLOAD_60
        }elseif(in_array($mSell->agent_id, $AgentCity)){
            $monthCheck = 3;// Thanh phố trực thuộc tỉnh thì 3 tháng self::TYPE_REMOVE_OVERLOAD
        }
//            if(in_array($mSell->province_id,GasProvince::$PROVINCE_TAY_NGUYEN)){
//                $monthCheck = 4;//Khu vực tây nguyên lui 4 tháng
//            }
        return $monthCheck;
    }
    
    public function getTypeRemoveBySell($mSell){
        $mGasTargetMonthly = new GasTargetMonthly();
        $aOneMany   = $mGasTargetMonthly->getArrayOneManyMonitorCcs('',GasOneMany::TYPE_AGENT_CITY);
        $AgentCity      = CHtml::listData($aOneMany, 'many_id', 'many_id');
        $monthCheck = self::TYPE_REMOVE_OVERLOAD_120;// tỉnh thì check lui 4 tháng self::TYPE_REMOVE_OVERLOAD_120
        if($mSell->province_id == GasProvince::TP_HCM){
            $monthCheck = self::TYPE_REMOVE_OVERLOAD_60;// TP thì check lui 2 tháng self::TYPE_REMOVE_OVERLOAD_60
        }elseif(in_array($mSell->agent_id, $AgentCity)){
            $monthCheck = self::TYPE_REMOVE_OVERLOAD;// Thanh phố trực thuộc tỉnh thì 3 tháng self::TYPE_REMOVE_OVERLOAD
        }
//            if(in_array($mSell->province_id,GasProvince::$PROVINCE_TAY_NGUYEN)){
//                $monthCheck = 4;//Khu vực tây nguyên lui 4 tháng
//            }
        return $monthCheck;
    }
    
    /** @Author: NamNH Aug 30,2019
     *  @Todo: get source kh
     **/
    public function getSourceCustomer() {
        return [
            self::TYPE_CUSTOMER_OF_AGENT => 'KH công ty',
        ];
    }
    
    /** @Author: NamNH Aug 30,2019
     *  @Todo: get source kh
     **/
    public function getIsCustomerAgent() {
        $aSource = $this->getSourceCustomer();
        return isset($aSource[$this->is_customer_agent]) ? $aSource[$this->is_customer_agent] : '';
    }
    
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

	$criteria->compare('t.customer_id',$this->customer_id);
	$criteria->compare('t.monitoring_id',$this->monitoring_id);
	$criteria->compare('t.gdkv_id',$this->gdkv_id);
	$criteria->compare('t.type',$this->type);
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateConverDmyToYmd($this->date_from,"-");
//            $criteria->addCondition('t.sell_date >= "' . $date_from . '"');
            DateHelper::searchGreater($date_from, 'sell_date_bigint', $criteria);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateConverDmyToYmd($this->date_to,"-");
//            $criteria->addCondition('t.sell_date <= "' . $date_to . '"');
            DateHelper::searchSmaller($date_to, 'sell_date_bigint', $criteria);
        }
        if(!empty($this->date_c_from) || !empty($this->date_c_to)){
            $mGasTargetMonthly = new GasTargetMonthly();
            $criteria->addInCondition('t.type',$mGasTargetMonthly->getTypePointPTTT());
        }
        if(!empty($this->date_c_from)){
            $date_from = MyFormat::dateConverDmyToYmd($this->date_c_from,"-");
            DateHelper::searchGreater($date_from, 'created_date_only_bigint', $criteria);
        }
        if(!empty($this->date_c_to)){
            $date_to = MyFormat::dateConverDmyToYmd($this->date_c_to,"-");
            DateHelper::searchSmaller($date_to, 'created_date_only_bigint', $criteria);
        }
        if(!empty($this->date_from_v2)){
            $date_from = MyFormat::dateConverDmyToYmd($this->date_from_v2,"-");
            DateHelper::searchGreater($date_from, 'sell_date_bigint_v2', $criteria);
        }
        if(!empty($this->date_to_v2)){
            $date_to = MyFormat::dateConverDmyToYmd($this->date_to_v2,"-");
            DateHelper::searchSmaller($date_to, 'sell_date_bigint_v2', $criteria);
        }
        if($this->is_purchased){
            $criteria->addCondition('t.sell_id > 1');
        }
        if($this->promotionItem){
            switch ($this->promotionItem){
                case self::PROMOTION_ITEM:
                    $criteria->addCondition('t.sell_promotion_item > 0');
                    break;
                case self::PROMOTION_ITEM_V2:
                    $criteria->addCondition('t.sell_promotion_item_v2 > 0');
                    break;
                default:
                    break;
            }
        }
        if($this->app_promotion_user_id){
            $criteria->addCondition('t.app_promotion_user_id IS NOT NULL AND t.app_promotion_user_id > 0');
        }
        if($this->support_id){
            $criteria->addCondition('t.support_id < 1');
        }
        if($this->is_telesale){
            $mGasTargetMonthly = new GasTargetMonthly();
            $aTelesale = $this->getArrayEmployeeCcs('',$mGasTargetMonthly->getArrayMonitorTelesale());
            $criteria->addInCondition('t.created_by', $aTelesale);
        }
        if($this->is_pttt){
            $mGasTargetMonthly = new GasTargetMonthly();
            $aTelesale = $this->getArrayEmployeeCcs('',$mGasTargetMonthly->getArrayMonitorTelesale());
            $criteria->addNotInCondition('t.created_by', $aTelesale);
        }
        if($this->support_id_yes){
            $criteria->addCondition('t.support_id = 1');
        }
        if($this->created_by){
            $criteria->compare('t.created_by',$this->created_by);
        }
        if($this->is_customer_agent){
            $criteria->compare('t.is_customer_agent',$this->is_customer_agent);
        }
        if($this->code_no){
            $SellId = $this->getSellIdByCode();
            $criteria->addCondition('t.sell_id = '.$SellId.' OR '. 't.sell_id_v2 = '.$SellId);
        }
        if($this->type_customer){
            $criteria->compare('t.type_customer',$this->type_customer);
        }
        if(!empty($this->type_remove)){
            $criteria->compare('t.type_remove',$this->type_remove);
        }
        if(!empty($this->phone)){
            $criteria->compare('t.phone',$this->phone);
        }
        $criteria->order = 't.id DESC';
        $_SESSION['data-model-target'] = $this;
        $_SESSION['data-excel-target'] = new CActiveDataProvider($this, array(
            'pagination'=>false,
            'criteria'=>$criteria,
        ));
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /** @Author: DungNT Dec 07, 2018
     *  @Todo: update sell_id by date, nếu KH đó lấy hàng thì update thông tin đơn hàng qua
     *  @param: $dateRun 'Y-m-d'
//        UPDATE `gas_target_daily` AS t1, `gas_sell` AS t2  SET t1.sell_id = t2.id, t1.sell_date = t2.created_date_only, t1.sell_total= t2.total, t1.sell_grand_total = t2.grand_total
//        WHERE t1.customer_id = t2.customer_id AND t2.created_date_only_bigint = 1522170000
     **/
    public function updateInfoSellByDate($dateRun) {
        $from = time();
        $created_date_only_bigint   = strtotime($dateRun);
        $date_to                    = MyFormat::modifyDays($dateRun, 1);
        $date_to_second             = strtotime($date_to);
        $tableNameTargetDaily       = TargetDaily::model()->tableName();
        $tableNameSell              = Sell::model()->tableName();
        
        $sql    ="UPDATE  `$tableNameTargetDaily` AS t1, `$tableNameSell` AS t2 "
                    . "SET t1.sell_complete_time_bigint = t2.complete_time_bigint, t1.sale_id = t2.sale_id,t1.sell_id = t2.id, t1.sell_date = t2.created_date_only, t1.sell_date_bigint = t2.created_date_only_bigint, t1.sell_total= t2.total, t1.sell_grand_total = t2.grand_total, t1.gas_remain = t2.gas_remain, t1.gas_remain_amount = t2.gas_remain_amount, t1.agent_id = t2.agent_id , t1.support_id = t2.support_id "
                    . "WHERE t1.sell_id < 1 AND t1.customer_id = t2.customer_id AND t2.complete_time_bigint >= $created_date_only_bigint AND t2.complete_time_bigint <= $date_to_second AND t2.status = ".Sell::STATUS_PAID;
        Yii::app()->db->createCommand($sql)->execute();
        $to = time();
        $second = $to-$from;
        $info = "dateRun $dateRun - Run  TargetDaily updateInfoSellByDate done in:+ ".($second).'  Second  <=> '.($second/60).' Minutes '.date('Y-m-d H:i:s');
        Logger::WriteLog($info);
    }
    
    /** @Author: DungNT Dec 07, 2018
     *  @Todo: insert data Point + app of PTTT + Ccs by date
     *  @param: $dateRun 'Y-m-d'
     * addMulti: Thêm vào targetdaily danh sách khách hàng được tạo mới
     * updateInfoSellByDate: Cập nhật các hóa đơn lần 1
     * removeSellInvalid: bỏ các hóa đơn không phù hơp: khách hàng củ, vỏ công ty
     * updateInfoSell: Cập nhật các hóa đơn lần 2
     * insertInfoShare: Thêm các khách hàng chia sẽ
     * insertAppTelesale: Thêm các hóa đơn app khi thiếu ở updateInfoSellByDate
     * insertInfoTelesale: Thêm BQV telesale có cuộc gọi
     * insertPTTT: Thêm BQV lần 1 các đơn hàng có code_ptt
     * updateTargetDaily: Cập nhật created_by cho telesale các BQV có telesale gọi và không xử dụng mã PTTT
     * updateTargetDailyDublicate: Cập nhật các DLV bị trùng
     * updateTargetDailyBQV: Cập nhật các BQV quá hạn
     * updateGrandTotal: cập nhật lại giá bán chỉ GAS
     * removeTelesaleRewarded: Bỏ các BQV telesale đã được thưởng trước đó
     * changeCreatedByTelesale: Chỉnh sữa lại nhân viên telesale do thay đổi liên tục người gọi
     * removePtttInvalid: Xóa các BQV PTTT không nhập code lỗi hệ thống TargetDaily::TYPE_CCS
     **/
    public function addByDate($dateRun = '') {
        /* 1. delete by date
         * 2. findAll customer of PTTT by date - then run addMulti
         * 3. findAll customer of Ccs by date - then run addMulti
         */
        if(empty($dateRun)){
            $dateRun = date('Y-m-d');
        }
        // 1. delete by date
        $this->deleteByDateBigInt($dateRun);
        $aCustomer = $this->getDataCustomerApp($dateRun);
        $this->addMulti(TargetDaily::TYPE_CCS, $dateRun, $aCustomer);
        $aCustomer = $this->getDataCustomerCcs($dateRun);
        $this->addMulti(TargetDaily::TYPE_CCS, $dateRun, $aCustomer);
        $aCustomer = null;
        $this->changeAfterUpdateCode($dateRun);
        $this->updateInfoSellByDate($dateRun);
        $this->removeSellInvalid($dateRun);
        $this->updateInfoSell($dateRun);
        $this->insertInfoShare($dateRun);
        $this->insertPTTT($dateRun);
        $this->insertPtttNoCode($dateRun);
        $this->insertBqvByHgdPttt($dateRun);
        $this->insertAppTelesale($dateRun);
        $this->insertInfoTelesale($dateRun);
        $this->insertReTelesaleBqv($dateRun); // thêm telesale đơn hàng gọi lại khách hàng cũ
        $this->updateTargetDaily($dateRun);
        $this->updateTargetDailyDublicate($dateRun);
        $this->updateTargetDailyBQV($dateRun);
        $this->updateGrandTotal($dateRun);
        $this->removeTelesaleRewarded($dateRun);
        $this->changeCreatedByTelesale($dateRun);
        $this->removePtttInvalid($dateRun);
        $this->updateEventMarket($dateRun);
        $this->updateCustomerOfAgent($dateRun);// cập nhật khách hàng công ty
//        sleep(1); DungNT Aug1719 close - thấy ko cần thiết phải sleep, theo dõi lại xem có bị lỗi ko
        $info = "dateRun $dateRun- Run addByDate done ".date('Y-m-d H:i:s');
        Logger::WriteLog($info);
    }
    
    /** @Author: DungNT Dec 07, 2018
     *  @Todo: get customer điểm làm việc nhập code
     **/
    public function getDataCustomerCcs($dateRun) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.is_maintain = ' . STORE_CARD_HGD_CCS);
        $criteria->addCondition('t.role_id = ' . ROLE_CUSTOMER);
        DateHelper::searchBetween($dateRun, $dateRun, 'created_date_bigint', $criteria, true);
        return Users::model()->findAll($criteria);
    }
    
    /** @Author: DungNT Dec 07, 2018
     *  @Todo: get customer KH App nhập code
     **/
    public function getDataCustomerApp($dateRun) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.created_by > 0 AND t.is_maintain = ' . UsersExtend::STORE_CARD_HGD_APP);
        $criteria->addCondition('t.role_id = ' . ROLE_CUSTOMER);
        DateHelper::searchBetween($dateRun, $dateRun, 'created_date_bigint', $criteria, true);
        return Users::model()->findAll($criteria);
    }

    /** @Author: DungNT Dec 06, 2018
     *  @Todo: insert data Point + app of PTTT + Ccs
     *  @param: $type type of target daily
     *  @param: $aCustomer array model User
     **/
    public function addMulti($type, $dateRun, $aCustomer) {
        $from = time();
        $mOneMany                   = new GasOneMany();
        $aTreeTeamCcs               = $mOneMany->getCacheTreeTeam($dateRun);
        $created_date               = date('Y-m-d H:i:s');
        $created_date_only_bigint   = strtotime($dateRun);
        
        $aRowInsert = [];
        foreach ($aCustomer as $mCustomer) {
            $mTargetDaily = new TargetDaily();
            $mTargetDaily->created_by = $mCustomer->created_by;
            $mTargetDaily->getTeam($aTreeTeamCcs, $mCustomer);
            if(empty($mTargetDaily->monitoring_id)){
                continue ;
            }
            $aRowInsert[]="('$type',
                    '$mCustomer->id',
                    '$mCustomer->is_maintain',
                    '$mCustomer->sale_id',
                    '$mCustomer->area_code_id',
                    '$mCustomer->province_id',
                    '$mCustomer->created_by',
                    '$mTargetDaily->monitoring_id',
                    '$mTargetDaily->gdkv_id',
                    '$mTargetDaily->materials_id',
                    '$mTargetDaily->seri',
                    '$mTargetDaily->phone',
                    '$created_date',
                    '$created_date_only_bigint',
                    '$mCustomer->created_date'
            )";
        }
        
        $tableName = TargetDaily::model()->tableName();
        $sql = "insert into $tableName (type,
                        customer_id,
                        type_customer,
                        sale_id,
                        agent_id,
                        province_id,
                        created_by,
                        monitoring_id,
                        gdkv_id,
                        materials_id,
                        seri,
                        phone,
                        created_date,
                        created_date_only_bigint,
                        created_date_user
                        ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert)>0){
            Yii::app()->db->createCommand($sql)->execute();
        }

        $to = time();
        $second = $to-$from;
        $info = count($aCustomer) . " dateRun $dateRun- Run  TargetDaily addMulti done in:+ ".($second).'  Second  <=> '.($second/60).' Minutes '.date('Y-m-d H:i:s');
        Logger::WriteLog($info);
    }
    
    /** @Author: AnhDung Dec072018
     *  @Todo: delete record by date
     *  @Param: $date format Y-m-d
     **/
    public function deleteByDateBigInt($dateRun) {
        $dateBigint = strtotime($dateRun);
        TargetDaily::model()->deleteAll("created_date_only_bigint = $dateBigint");
    }   
    
    /** @Author: DungNT Dec 07, 2018
     *  @Todo: get id CV va Gdkv cua 1 NV ccs
     **/
    public function getTeam($aTreeTeamCcs, $mCustomer) {
        if(isset($aTreeTeamCcs[$this->created_by])){
            $this->monitoring_id    = $aTreeTeamCcs[$this->created_by]['monitoring_id'];
            $this->gdkv_id          = $aTreeTeamCcs[$this->created_by]['gdkv_id'];
        }
        if($mCustomer->is_maintain == STORE_CARD_HGD_CCS && !empty($this->monitoring_id)){
            $mCustomer->LoadUsersRef();
            $this->materials_id     = $mCustomer->mUsersRef->contact_technical_phone;
            $this->seri             = $mCustomer->mUsersRef->contact_technical_name;
        }
        $explore                = !empty($mCustomer->phone) ? explode('-',$mCustomer->phone) : [];
        $phone                  = (!empty($explore[0])) ? $explore[0] : '';
        $this->phone            = str_replace(MyFormat::$BAD_CHAR, '', $phone);
    }
    
    
    /** @Author: DungNT Dec 09, 2018
     *  @Todo: IT cập nhập các tiêu chí tính bình quay về cho NV PTTT, CCS ..... như sau:
        - Không tính bình về cùng thương hiệu vỏ
        - Không tính bình về nằm trong danh mục khách hàng của Cty
        + Tỉnh trong vong 3 tháng (qua 3 tháng mới được tính)
        + TPHCM trong vòng 2 tháng (qua 2 tháng mới được tính)
     * @do_find hết các SellId không thỏa mãn và update 1 lần
     **/
    public function removeSellInvalid($dateRun) {
        $from = time();
//        $sell_date_bigint   = strtotime($dateRun);
        $criteria = new CDbCriteria();
//        $criteria->addCondition('t.sell_date_bigint = ' . $sell_date_bigint);
        DateHelper::searchBetween($dateRun, $dateRun, 'sell_complete_time_bigint', $criteria, true);
        $models = TargetDaily::model()->findAll($criteria);
        $aSellId = CHtml::listData($models, 'sell_id', 'sell_id');
        // 1. Không tính bình về cùng thương hiệu vỏ
        $aSellIdRemove1 = $this->findSpjBrandVo($aSellId);
        $aSellIdRemove2 = $this->findCustomerOld($aSellId);
        $aSellIdRemove3 = $this->findSellNotGas($aSellId);
        
        $aSellIdRemove = array_merge($aSellIdRemove1, $aSellIdRemove2,$aSellIdRemove3);
        foreach($models as $mTargetDaily){
            if(in_array($mTargetDaily->sell_id, $aSellIdRemove)){
                $mTargetDaily->setDataSellInvalid();
            }
        }
        $models = null;
        
        $to = time();
        $second = $to-$from;
        $info = "dateRun $dateRun - Run  TargetDaily removeSellInvalid done in:+ ".($second).'  Second  <=> '.($second/60).' Minutes '.date('Y-m-d H:i:s');
        Logger::WriteLog($info);
    }
    
    /** @Author: DungNT Dec 09, 2018
     *  @Todo: Không tính bình về cùng thương hiệu vỏ
     * 1. find all sell detail với thương hiệu vỏ cty
     * 2. for ngược lại model TargetDaily xóa info của Sell đi
     **/
    public function findSpjBrandVo(&$aSellId) {
        $mMaterials = new GasMaterials();
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',',  $aSellId);
        $criteria->addCondition("t.sell_id IN ($sParamsIn)");
        
        $sParamsIn = implode(',',  $mMaterials->getSpjBrandVo());
        $criteria->addCondition("t.materials_id IN ($sParamsIn)");
        $aSellDetail    = SellDetail::model()->findAll($criteria);
        $aSellIdRemove  = CHtml::listData($aSellDetail, 'sell_id', 'sell_id');
        foreach($aSellIdRemove as $sell_id){
            unset($aSellId[$sell_id]);
        }
        $aSellDetail = null;
        return $aSellIdRemove;
    }
    
    /** @Author: DungNT Dec 09, 2018
     *  @Todo: - Không tính bình về nằm trong danh mục khách hàng của Cty
        + Tỉnh trong vong 3 tháng (qua 3 tháng mới được tính)
        + TPHCM trong vòng 2 tháng (qua 2 tháng mới được tính)
     * 1. find all sell by array sell_id
     * 2. for list sell find order trc đây của KH by phone 
     * 3. check index field phone of table Sell
     **/
    public function findCustomerOld(&$aSellId) {
        $aCodeEx = [];
        $aSellIdRemove = [];
        if(empty($aSellId)){
            return $aSellIdRemove;
        }
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',',  $aSellId);
        $criteria->addCondition("t.id IN ($sParamsIn)");
        $aSell    = Sell::model()->findAll($criteria);
        foreach($aSell as $mSell):
            $monthCheck = $this->getMonthCheck($mSell);
            $date_from_ymd    = MyFormat::modifyDays($mSell->created_date_only, $monthCheck, '-', 'month');
            $criteria = new CDbCriteria();
            $criteria->compare('t.phone', $mSell->phone);
            $criteria->compare('t.status', Sell::STATUS_PAID);
            $criteria->addCondition("t.id < $mSell->id");
            DateHelper::searchBetween($date_from_ymd, $mSell->created_date_only, 'created_date_only_bigint', $criteria);
            $modelSellOld    = Sell::model()->findAll($criteria);
            if(!empty($modelSellOld)){
                $aSellNotGas = CHtml::listData($modelSellOld, 'id', 'id');
                $aRemove = $this->findSellNotGas($aSellNotGas);
                if(count($aRemove) < count($modelSellOld)){
                    $aSellIdRemove[$mSell->id] = $mSell->id;
                    continue;
                }
            }
            $aUsersId = $this->getUsersIdByPhone($mSell->phone);
            $aUsersId[$mSell->customer_id] = $mSell->customer_id;
//            remove by customer_id
            $criteria1 = new CDbCriteria();
            $criteria1->addInCondition('t.customer_id', $aUsersId);
            $criteria1->compare('t.status', Sell::STATUS_PAID);
            $criteria1->addCondition("t.id < $mSell->id");
            DateHelper::searchBetween($date_from_ymd, $mSell->created_date_only, 'created_date_only_bigint', $criteria1);
            $modelSellOld1    = Sell::model()->findAll($criteria1);
            if(!empty($modelSellOld1)){
                $aSellNotGas1 = CHtml::listData($modelSellOld1, 'id', 'id');
                $aRemove1 = $this->findSellNotGas($aSellNotGas1);
                if(count($aRemove1) < count($modelSellOld1)){
                    if(!isset($aSellIdRemove[$mSell->id])){
                        $aCodeEx[] = $mSell->code_no;
                    }
                    $aSellIdRemove[$mSell->id] = $mSell->id;
                }
            }
        endforeach;
        $aSell = null;
        $info = "Run find out by customer_id <=> " . implode(',', $aCodeEx);
//        Logger::WriteLog($info);
        return $aSellIdRemove;
    }

    public function getUsersIdByPhone($phone,$is_maintain = []){
        $result = [];
        if(!empty($phone)){
            $criteria = new CDbCriteria();
            if(!empty($is_maintain)){
                if(is_array($is_maintain)){
                    $criteria->addInCondition('t.is_maintain', $is_maintain);
                }else{
                    $criteria->compare('t.is_maintain', $is_maintain);
                }
            }
            if(is_array($phone)){
                $criteria->addInCondition('t.phone', $phone);
            }else{
                $criteria->compare('t.phone', $phone);
            }
            $aModel = UsersPhone::model()->findAll($criteria);
            $result = CHtml::listData($aModel, 'user_id', 'user_id');
//            Lấy từ sorl 
            /* DungNT Oct0819 close - truy vấn bị lỗi - có rủi ro khi truy vấn với Solr => 'Solr error: '400' Status: Bad Request' 
             * không nên dùng solr trong các logic update + create 
            $mSellReport = new SellReport();
            if(is_array($phone)){
                foreach ($phone as $key => $numberPhone) {
                    $aIdMore = $mSellReport->getAllIdUsersByPhone($numberPhone,$is_maintain);
                    $result += $aIdMore;
                }
            }else{
                $aIdMore = $mSellReport->getAllIdUsersByPhone($phone,$is_maintain);
                $result += $aIdMore;
            }
            */
            
        }
        return $result;
    }
    /** @Author: DungNT Dec 09, 2018
     *  @Todo: update data for sell invalid
     **/
    public function setDataSellInvalid() {
        $this->sell_id              = 0;
        $this->sell_date            = null;
        $this->sell_date_bigint     = 0;
        $this->sell_total           = 0;
        $this->sell_grand_total     = 0;
        $this->gas_remain           = 0;
        $this->gas_remain_amount    = 0;
        $this->sell_complete_time_bigint    = 0;
        $this->update();
    }
    
    
    /** @Author: NamNH Jan 21, 2019
     *  @Todo: get data of report daily ccs
     *  @params $this->date_from, $this->date_to format d-m-Y
     **/
    public function getReportDailyCcs(){
        $this->date_from_ymd = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);                 
        $this->date_to_ymd = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        $mGasTargetMonthly = new GasTargetMonthly();
        $_SESSION['data-model-daily'] = $this;
        $aData              = [];
        
        $this->getPtttPoint($aData,$mGasTargetMonthly->getTypeBqvPTTT());
        $this->getDataDaily($aData,'BQV','sell_date','sell_date_bigint',$mGasTargetMonthly->getTypeBqvPTTT());
        $date_from = $this->date_from_ymd;
        $date_to = $this->date_to_ymd;
        $dateCurrent = $date_from;
        while ($dateCurrent <= $date_to){
            $aData['DATE'][$dateCurrent] = $dateCurrent;
            $dateCurrent = MyFormat::modifyDays($dateCurrent, 1);
        }
        if(!empty($aData['DATE'])){
            asort($aData['DATE']);
        }
        $aData['EMPLOYEE_MODEL'] = !empty($aData['EMPLOYEE']) ? Users::getArrayModelByArrayId($aData['EMPLOYEE']) : [];
        return $aData;
    }
    
//    NamNH get point by date from model UsersHgdPttt
    public function getPtttPoint(&$aData,$aType){
        $dateStart = $date_end = '';
        if($this->date_from_ymd < UsersHgdPttt::DATE_APPLY && $this->date_to_ymd >= UsersHgdPttt::DATE_APPLY){
            $dateStart          = UsersHgdPttt::DATE_APPLY;
            $date_end           = $this->date_to_ymd;
            $this->date_to_ymd  = MyFormat::addDays(UsersHgdPttt::DATE_APPLY, 1, '-');
        }
        
        if($this->date_from_ymd < UsersHgdPttt::DATE_APPLY){
            $this->getDataDaily($aData,'POINT_APP','created_date_user','created_date_only_bigint',$aType,true);
        }else{
            $this->getDataDaily($aData,'POINT_APP','created_date_user','created_date_only_bigint',$aType,false);
        }
        
//        cập nhật lại ngày
        if(!empty($dateStart) && !empty($date_end)){
            $this->date_from_ymd    = $dateStart;
            $this->date_to_ymd      = $date_end;
        }
        if($this->date_to_ymd >= UsersHgdPttt::DATE_APPLY){
            $aEmployeeCcs           = $this->getArrayEmployeeCcs($this->date_from_ymd);
            $this->getDataPointUsersHgdPttt($aData['POINT_APP'],$aEmployeeCcs,true);
        }
        
        //        đặt lại ngày bắt đầu và ngày kết thúc để chạy các hàm phía sau
        $this->date_from_ymd = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);                 
        $this->date_to_ymd = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
    }
    
    /** @Author: NamNH May 04, 2019
     *  @Todo: get DLV app
     **/
    public function getDataPointUsersHgdPttt(&$aData,$aUser, $groupByDate = false){
        $criteria           = new CDbCriteria;
        if(!empty($this->date_from_ymd)){
            DateHelper::searchGreater($this->date_from_ymd, 'created_date_bigint', $criteria);
        }
        if(!empty($this->date_to_ymd)){
            DateHelper::searchSmaller($this->date_to_ymd, 'created_date_bigint', $criteria, true);
        }
        $criteria->addInCondition('t.created_by', $aUser);
        $criteria->compare('t.is_maintain', STORE_CARD_HGD_CCS);
        $criteria->compare('t.role_id', ROLE_CUSTOMER);
        $criteria->addCondition('t.first_char <> '. UsersHgdPttt::TYPE_DUBLICATE);
        $criteria->compare('t.type', CUSTOMER_TYPE_STORE_CARD);
        $criteria->select   = 'count(t.id) as id,DATE(t.created_date) as created_date,t.created_by';
        $criteria->group    = 'DATE(t.created_date),t.created_by';
        $aUsersHgdPttt      = UsersHgdPttt::model()->findAll($criteria);
        foreach ($aUsersHgdPttt as $key => $mUsersHgdPttt) {
            if($groupByDate){
                if(isset($aData[$mUsersHgdPttt->created_by][$mUsersHgdPttt->created_date]['point'])){
                    $aData[$mUsersHgdPttt->created_by][$mUsersHgdPttt->created_date]['point']         += $mUsersHgdPttt->id;
                }else{
                    $aData[$mUsersHgdPttt->created_by][$mUsersHgdPttt->created_date]['point']         = $mUsersHgdPttt->id;
                }
            }else{
                if(isset($aData[$mUsersHgdPttt->created_by]['point'])){
                    $aData[$mUsersHgdPttt->created_by]['point']      += $mUsersHgdPttt->id;
                }else{
                    $aData[$mUsersHgdPttt->created_by]['point']      = $mUsersHgdPttt->id;
                }            
            }
        }
    }
    
    /** @Author: NamNH Jan 21, 2019
     *  @Todo: get data by field group and field search date
     **/
    public function getDataDaily(&$aData, $nameSave = 'POINT_APP',$field_group = 'created_date_user',$field_date = 'created_date_only_bigint',$type = TargetDaily::TYPE_CCS,$getPoint = true){
        $aData[$nameSave]            = [];
        $criteria           = new CDbCriteria;
        // Jan3019 DungNT change Select sale_id to created_by, do lay sale_id thong ke bi sai
//        $criteria->select   = 't.type_customer,DATE('.$field_group.') as '.$field_group.',sale_id,count(id) as id,sum(if(t.sell_id > 0,1,0)) as sell_id';
        $mGasTargetMonthly      = new GasTargetMonthly();
        $aMonitorTelesale       = $mGasTargetMonthly->getArrayTelesaleMonitor();
        if(is_array($type)){
            $criteria->addInCondition('t.type',$type);
        }else{
            $criteria->compare('t.type',$type);
        }
//        NamNH Jul 06, 2019 lấy hết danh sách nhân viên đã từng làm ccs
//        $mGasTargetMonthly->date_from_ymd = MyFormat::dateConverDmyToYmd($this->date_from, '-'); 
        $aGasOneMany            = $mGasTargetMonthly->getArrayOneManyMonitorCcs($this->monitoring_id);
        foreach ($aGasOneMany as $key => $mOneMany) {
            if(in_array($mOneMany->many_id, $aMonitorTelesale)){
                continue;
            }
            $aData['EMPLOYEE_MONITOR'][$mOneMany->many_id] = $mOneMany->one_id;
            $aData['EMPLOYEE'][$mOneMany->many_id]      = $mOneMany->many_id;
            $aData['EMPLOYEE'][$mOneMany->one_id]       = $mOneMany->one_id;
        }
        $this->setMoreCondition($criteria,['field_compare_date_bigint' => $field_date,'ccs' => array_keys($aData['EMPLOYEE_MONITOR']) ]);
        $criteria->select   = 't.type_remove,t.app_promotion_user_id,SUM(t.sell_grand_total) as sell_grand_total,t.type_customer,DATE('.$field_group.') as '.$field_group.', t.created_by, count(id) as id,sum(if(t.sell_id > 0,1,0)) as sell_id';
        $criteria->group    = 't.type_remove,t.app_promotion_user_id,t.type_customer,DATE('.$field_group.'), t.created_by';
        $aTargetDaily       = self::model()->findAll($criteria);
        foreach ($aTargetDaily as $key => $mTargetDaily) {
            if($getPoint && $mTargetDaily->type_customer == STORE_CARD_HGD_CCS && empty($mTargetDaily->app_promotion_user_id)){
                if(isset($aData[$nameSave][$mTargetDaily->created_by]['point'])){
                    $aData[$nameSave][$mTargetDaily->created_by][$mTargetDaily->$field_group]['point']       += $mTargetDaily->id;
                }else{
                    $aData[$nameSave][$mTargetDaily->created_by][$mTargetDaily->$field_group]['point']       = $mTargetDaily->id;
                }
            }
//            May 04, 2019 move to :[changeApp]
            if($mTargetDaily->type_customer == UsersExtend::STORE_CARD_HGD_APP && !empty($mTargetDaily->app_promotion_user_id)){
                if(isset($aData[$nameSave][$mTargetDaily->created_by][$mTargetDaily->$field_group]['app'])){
                    $aData[$nameSave][$mTargetDaily->created_by][$mTargetDaily->$field_group]['app']         -= $mTargetDaily->id;
                }else{
                    $aData[$nameSave][$mTargetDaily->created_by][$mTargetDaily->$field_group]['app']         = 0 - $mTargetDaily->id;
                }
            }
//            if(!empty($mTargetDaily->app_promotion_user_id)){
//                continue;
//            }
//            Loại bỏ BQV quá 3 tháng kể từ khi nhập mã code PTTT
            if(in_array($mTargetDaily->type_remove,$this->getArrayRemove())){
                continue;
            }
            if(isset($aData[$nameSave][$mTargetDaily->created_by][$mTargetDaily->$field_group]['bqv'])){
                $aData[$nameSave][$mTargetDaily->created_by][$mTargetDaily->$field_group]['bqv']             += $mTargetDaily->sell_id;
            }else{
                $aData[$nameSave][$mTargetDaily->created_by][$mTargetDaily->$field_group]['bqv']             = $mTargetDaily->sell_id;
            }
            //      NamNH Feb 13, 2019 add sell_grand_total
            if(isset($aData[$nameSave]['GRAND_TOTAL'][$mTargetDaily->created_by])){
                $aData[$nameSave]['GRAND_TOTAL'][$mTargetDaily->created_by]   += $mTargetDaily->sell_grand_total;
            }else{
                $aData[$nameSave]['GRAND_TOTAL'][$mTargetDaily->created_by]   = $mTargetDaily->sell_grand_total;
            }
            // DungNT Feb1319 for Salary report
            if(isset($aData[$nameSave]['EMPLOYEE_SUM'][$mTargetDaily->created_by])){
                $aData[$nameSave]['EMPLOYEE_SUM'][$mTargetDaily->created_by]    += $mTargetDaily->sell_id;
            }else{
                $aData[$nameSave]['EMPLOYEE_SUM'][$mTargetDaily->created_by]    = $mTargetDaily->sell_id;
            }
        }
//        [changeApp]
        if(!empty($aData['EMPLOYEE'])){
            $this->getDataApp($aData[$nameSave],$aData['EMPLOYEE'],true);
        }
    }
    
    /** @Author: NamNH May 04, 2019
     *  @Todo: get DLV app
     **/
    public function getDataApp(&$aData,$aUser, $groupByDate = false){
        $criteria           = new CDbCriteria;
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
            DateHelper::searchGreater($date_from, 'created_date_bigint', $criteria);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
            $date_to = MyFormat::modifyDays($date_to, 1);
            DateHelper::searchSmaller($date_to, 'created_date_bigint', $criteria);
        }
        $criteria->addInCondition('t.promotion_owner_id', $aUser);
        $criteria->compare('t.promotion_type', AppPromotion::TYPE_FIRST_USER);
        $criteria->select = 'count(t.id) as id,DATE(t.created_date) as created_date,t.promotion_owner_id';
        $criteria->group = 'DATE(t.created_date),t.promotion_owner_id';
        $aAppPromotionUser = AppPromotionUser::model()->findAll($criteria);
        foreach ($aAppPromotionUser as $key => $mAppPromotionUser) {
            if($groupByDate){
                if(isset($aData[$mAppPromotionUser->promotion_owner_id][$mAppPromotionUser->created_date]['app'])){
                    $aData[$mAppPromotionUser->promotion_owner_id][$mAppPromotionUser->created_date]['app']         += $mAppPromotionUser->id;
                }else{
                    $aData[$mAppPromotionUser->promotion_owner_id][$mAppPromotionUser->created_date]['app']         = $mAppPromotionUser->id;
                }
            }else{
                if(isset($aData[$mAppPromotionUser->promotion_owner_id]['app'])){
                    $aData[$mAppPromotionUser->promotion_owner_id]['app']      += $mAppPromotionUser->id;
                }else{
                    $aData[$mAppPromotionUser->promotion_owner_id]['app']      = $mAppPromotionUser->id;
                }            
            }
        }
    }
    
    /** @Author: NamNH Jan 21, 2019
     *  @Todo: add more condition
     **/
    public function setMoreCondition(&$criteria, $aCondition = []){
        $criteria       = new CDbCriteria;
        $fieldSearch    = 'created_date_only_bigint';
        if(!empty($aCondition['field_compare_date_bigint'])){
            $fieldSearch =  $aCondition['field_compare_date_bigint'];
        }
        if(!empty($this->date_from_ymd)){
            DateHelper::searchGreater($this->date_from_ymd, $fieldSearch, $criteria);
        }
        if(!empty($this->date_to_ymd)){
            DateHelper::searchSmaller($this->date_to_ymd, $fieldSearch, $criteria);
        }
        $criteria->compare('t.agent_id', $this->agent_id);
        $criteria->compare('t.province_id', $this->province_id);
//        $criteria->compare('t.monitoring_id', $this->monitoring_id);
        if(!empty($aCondition['ccs'])){
            $criteria->addInCondition('t.created_by', $aCondition['ccs']);
        }
    }
    
    public function getCustomerName(){
        return isset($this->rCustomer) ? $this->customer_id.' - '.$this->rCustomer->getFullName() : "";
    }
    
    public function getCustomerAddress(){
        return isset($this->rCustomer) ? $this->rCustomer->address : "";
    }
    
    public function getCustomerPhone(){
        $aResult =  !empty($this->phone) ? $this->phone : (isset($this->rCustomer) ? $this->rCustomer->phone : "");
        return $aResult;
    }
    
    public function getCreatedBy(){
        return isset($this->rCreatedBy) ? $this->rCreatedBy->getFullName() : "";
    }
    
    public function getSale(){
        return isset($this->rSale) ? $this->rSale->getFullName() : "";
    }
    
    public function getMonitoring(){
        return isset($this->rMonitoring) ? $this->rMonitoring->getFullName() : "";
    }
    
    public function getDub(){
        $text = !empty($this->app_promotion_user_id) ? 'Có' : '';
        return $text;
    }
    public function getCall(){
        $text = $this->support_id == 1 ? 'Có' : '';
        return $text;
    }
    public function getListPromotionItem(){
        return [
            self::PROMOTION_ITEM => 'Vật tư BQV 1',
            self::PROMOTION_ITEM_V2 => 'Vật tư BQV 2',
        ];
    }
    public function getCreatedDate($format = 'd/m/Y',$getId = true){
        $result = '';
        $mGasTargetMonthly = new GasTargetMonthly();
        $aTelesale = $this->getArrayEmployeeCcs('',$mGasTargetMonthly->getArrayMonitorTelesale());
        if($this->support_id == 1 && in_array($this->created_by,$aTelesale)){
            $mFollowCustomer = $this->getFollowCustomer($this->created_by,$this->customer_id);
            if(!empty($mFollowCustomer)){
                $result = MyFormat::dateConverYmdToDmy($mFollowCustomer->created_date_only, $format);
            }
        }elseif($this->type_customer == UsersExtend::STORE_CARD_HGD_APP){
            $aAppPromotionUser = $this->getAppromotionUserByOwner($this->created_by,$this->customer_id);
            if(!empty($aAppPromotionUser)){
                $result = MyFormat::dateConverYmdToDmy($aAppPromotionUser->created_date, $format);
            }
        }elseif($this->type == self::TYPE_PTTT_BQV){
            $pttt_Code = '';
            if($this->sell_id > 1){
                $mSell = Sell::model()->findByPk($this->sell_id);
                if(!empty($mSell)){
                    $pttt_Code = $mSell->pttt_code;
                }
            }
            if(!empty($pttt_Code)){
                $criteriaSub = new CDbCriteria();
                $criteriaSub->addCondition("t.code='".trim($pttt_Code)."'");
                $model = SpjCode::model()->find($criteriaSub);
                if(!empty($model)){
                    $UserId = $model->pttt_customer_id;
                    $mUser = Users::model()->findByPk($UserId);
                    $result = !empty($mUser) ? MyFormat::dateConverYmdToDmy($mUser->created_date, $format) : '';
                }
            }
        }
        if(empty($result)){
            $result = date($format, $this->created_date_only_bigint);
        }
        if($getId){
            $result .= ' - '.$this->id;
        }
        return $result;
    }
    
//  lấy model follow của khách hàng >= 30s gần nhất  
    public function getFollowCustomer(){
        $needMore = ['findAll' => 1];
        $mUsersPhone = new UsersPhone();
        $mUsersPhone->is_maintain       = CmsFormatter::$aTypeIdHgd;
        $mUsersPhone->phone             = $this->phone;
        $aUsersPhone =  $mUsersPhone->countByPhoneAndType($needMore);
        $aUsersID = CHtml::listData($aUsersPhone, 'user_id', 'user_id');
        $criteria=new CDbCriteria;
        $criteria->addInCondition('t.customer_id', $aUsersID);
        $criteria->order = 't.id DESC';
//        $criteria->compare('t.employee_id', $created_by);
        $criteria->addCondition('t.bill_duration >= '.Telesale::MIN_TIME_OF_CALL);
        $mFollow = FollowCustomer::model()->find($criteria);
        return $mFollow;
    }
    
    public function getArrayTypeRemove(){
        return [
            self::TYPE_REMOVE_OVERLOAD_60 => self::COUNT_OUT_OF_DATE_APP_60.' ngày',
            self::TYPE_REMOVE_OVERLOAD => self::COUNT_OUT_OF_DATE_APP.' ngày',
            self::TYPE_REMOVE_OVERLOAD_120 => self::COUNT_OUT_OF_DATE_APP_120.' ngày',
        ];
    }
    
    public function getTypeRomove(){
        $aType = $this->getArrayTypeRemove();
        return isset($aType[$this->type_remove]) ? $aType[$this->type_remove] : '';
    }
    public function getSell($isFirst = false){
        $ret = [];
        $criteria=new CDbCriteria;
        $sellId = $isFirst ? $this->sell_id : $this->sell_id_v2;
        $fieldQty = $isFirst ? $this->sell_qty : $this->sell_qty_v2;
        $sellGrandTotal = $isFirst ? 'sell_grand_total' : 'sell_grand_total_v2';
        $amountPromotionItem = $isFirst ? 'sell_promotion_item' : 'sell_promotion_item_v2';
        $criteria->addInCondition('t.id', [$sellId]);
        $mSell = Sell::model()->find($criteria);
        if(!empty($mSell)){
            $aFund = $this->getFund([$mSell->created_date_only], [$mSell->agent_id]);
            $vFund = isset($aFund[MyFormat::dateConverYmdToDmy($mSell->created_date_only, 'Y')][MyFormat::dateConverYmdToDmy($mSell->created_date_only, 'm')][$mSell->agent_id]) ? $aFund[MyFormat::dateConverYmdToDmy($mSell->created_date_only, 'Y')][MyFormat::dateConverYmdToDmy($mSell->created_date_only, 'm')][$mSell->agent_id] : 0;
            $ret[] = MyFormat::dateConverYmdToDmy($mSell->created_date,'d/m/Y H:i:s');
            $ret[] = '';
            $ret[] = '<b>' . $mSell->code_no . '</b>';
            $ret[] = '<b>' . MyFormat::dateConverYmdToDmy($mSell->complete_time,'d/m/Y H:i:s'). '</b>';
            $ret[] = 'SL Thực: ' . (($fieldQty * 12 ) - $mSell->gas_remain) . ' kg';
            $ret[] = 'Vật tư KM: ' . ActiveRecord::formatCurrency($this->$amountPromotionItem);
            $ret[] = 'Doanh thu: ' . ActiveRecord::formatCurrency($this->$sellGrandTotal);
            $ret[] = 'Tiền vốn: ' . ActiveRecord::formatCurrency((($fieldQty * 12 ) - $mSell->gas_remain) * ($vFund / 12));
        }
        return implode('<br>', $ret);
    }
    
    public function getInfoSell(){
        $criteria=new CDbCriteria;
        $criteria->addInCondition('t.id', [ $this->sell_id,$this->sell_id_v2]);
        $mSell = Sell::model()->findAll($criteria);
        $aDate = CHtml::listData($mSell, 'created_date_only', 'created_date_only');
        $aAgent = CHtml::listData($mSell, 'agent_id', 'agent_id');
        $aFund = $this->getFund($aDate, $aAgent);
        foreach ($mSell as $key => $value) {
            $fieldQty = $value->id == $this->sell_id ? $this->sell_qty : $this->sell_qty_v2;
            $ret[$value->id]['date'] = $value->created_date_only;
            $ret[$value->id]['sell'] = $value->code_no;
            $ret[$value->id]['kg'] = (($fieldQty * 12 ) - $value->gas_remain);
            $ret[$value->id]['total'] = $value->grand_total;
            $vFund = isset($aFund[MyFormat::dateConverYmdToDmy($value->created_date_only, 'Y')][MyFormat::dateConverYmdToDmy($value->created_date_only, 'm')][$value->agent_id]) ? $aFund[MyFormat::dateConverYmdToDmy($value->created_date_only, 'Y')][MyFormat::dateConverYmdToDmy($value->created_date_only, 'm')][$value->agent_id] : 0;
            $ret[$value->id]['fund'] = ($vFund / 12) * (($fieldQty * 12 ) - $value->gas_remain);
        }
        return $ret;
    }
    
    public function getFund($aDate,$aAgent){
        $aData  = [];
        foreach ($aDate as $key => $value) {
            $month  = MyFormat::dateConverYmdToDmy($value, 'm');
            $year   = MyFormat::dateConverYmdToDmy($value, 'Y');
            $criteria       = new CDbCriteria;
            $criteria->compare('month(t.date_apply)', $month);
            $criteria->compare('year(t.date_apply)', $year);
            $criteria->addInCondition('t.agent_id', $aAgent);
            $aFunds = Funds::model()->findAll($criteria);
            foreach ($aFunds as $key => $mFunds) {
                $aData[MyFormat::dateConverYmdToDmy($mFunds->date_apply, 'Y')][MyFormat::dateConverYmdToDmy($mFunds->date_apply, 'm')][$mFunds->agent_id] = $mFunds->funds;
            }
        }
        return $aData;
    }
    
    public function getAgent(){
        return isset($this->rAgent) ? $this->rAgent->getFullName() : "";
    }
    
    public function getSellDate(){
        return MyFormat::dateConverYmdToDmy($this->sell_date);
    }
    
    public function getTypeCustomer(){
        return isset(CmsFormatter::$CUSTOMER_BO_MOI[$this->type_customer]) ? CmsFormatter::$CUSTOMER_BO_MOI[$this->type_customer] : "";
    }
    
    public function getType(){
        $aType = $this->getArrayType();
        return isset($aType[$this->type]) ? $aType[$this->type] : "";
    }
    
    /** @Author: NamNH Feb 26, 2019
     *  @Todo: update id event market
     *  $date Y-m-d
     **/
    public function updateEventMarket($date = ''){
        if(empty($date)){
            $date = date('Y-m-d');
        }
        $aGasEventMarket        = $this->getEventMarketActive($date);
        $sql                    = '';
        $countInsert            = 0;
        foreach ($aGasEventMarket as $key => $mGasEventMarket) {
            $aIdEmployee        = json_decode($mGasEventMarket->employees, true);
            $aIdAgent           = json_decode($mGasEventMarket->agents, true);
            $date_from_second   = $mGasEventMarket->start_date;
            $date_to_second     = $mGasEventMarket->end_date;
            if(count($aIdEmployee) > 0 && count($aIdAgent) > 0 && !empty($date_from_second) && !empty($date_to_second)){
                $strEmployees   =   '('. implode(',', $aIdEmployee). ')';
                $strAgents      =   '('. implode(',', $aIdAgent). ')';
                $sql .= 'UPDATE `gas_target_daily` SET `event_market_id` = '.$mGasEventMarket->id
                        .' WHERE `event_market_id` <= 0 '
                                .'AND (`created_date_user` BETWEEN "'.$date_from_second.'" AND "'.$date_to_second.'")'
                                .' AND `created_by` IN '.$strEmployees
                                .' AND `agent_id` IN '.$strAgents.';';
                $countInsert++;
            }
        }
        $info = "dateRun $date-$countInsert Run updateEventMarket done ".date('Y-m-d H:i:s');
        Logger::WriteLog($info);
        if($countInsert > 0){
            Yii::app()->db->createCommand($sql)->execute();
        }
    }
    
    /** @Author: NamNH Feb 26, 2019
     *  @Todo: get array model event market active
     **/
    public function getEventMarketActive($date){
        $mGasEventMarketCal = new GasEventMarket();
        $aStatusActive  = $mGasEventMarketCal->getStatusActive();
        $aTypeActive    = $mGasEventMarketCal->getTypeActivationMaket();
        $criteria   = new CDbCriteria;
        $criteria->addInCondition('t.status', $aStatusActive);
        $criteria->addInCondition('t.type', $aTypeActive);
        DateHelper::searchGreater($date, 'end_date_bigint', $criteria);
        DateHelper::searchSmaller($date, 'start_date_bigint', $criteria,true);
        $aModel     = GasEventMarket::model()->findAll($criteria);
        return $aModel;
    }
    
    /** @Author: DungNT Mar 08, 2019
     *  @Todo: count record by SellId
     **/
    public function countBySellId($created_by = '') {
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.sell_id = '$this->sell_id' OR t.sell_id_v2 = '$this->sell_id'"); // Apr 09, 2019 NamNH search by BQV 2, phải đi sau 
        if(!empty($created_by)){
            $criteria->compare('t.created_by', $created_by);
        }
        return self::model()->count($criteria);
    }
    
    /** @Author: NamNH Mar 05, 2019
     *  @Todo: update info sell v2
     *  @Param:$dateRun Y-m-d
     *  $aSet: array field_target_daily => field_sell
     *  $aWhere: array condition
     **/
    public function updateInfoSellByDateV2($aSet, $aWhere = [],$aliasTarger = 't1', $aliasSell = 't2') {
        if(empty($aSet)){
            return;
        }
        
        $tableNameTargetDaily       = TargetDaily::model()->tableName();
        $tableNameSell              = Sell::model()->tableName();
        $sql    ="UPDATE  `$tableNameTargetDaily` AS $aliasTarger, `$tableNameSell` AS $aliasSell ";
        if(!empty($aSet)){
            $aSetConvert        = [];
            foreach ($aSet as $field_target => $field_sell) {
                $aSetConvert[]  = " $field_target = $field_sell ";
            }
            $setStr             = implode(' , ', $aSetConvert);
            $sql                .= ' SET '.$setStr;
        }  
        if(!empty($aWhere)){
            $whereStr    = implode(' AND ', $aWhere);
            $sql         .= ' WHERE '.$whereStr;
        }
        Yii::app()->db->createCommand($sql)->execute();
    }
    
    /** @Author: NamNH Mar 05, 2019
     *  @Todo: update data for sell invalid
     **/
    public function setDataSellInvalidV2() {
        $this->sell_id_v2              = 0;
        $this->sell_date_v2            = null;
        $this->sell_date_bigint_v2     = 0;
        $this->sell_total_v2           = 0;
        $this->sell_grand_total_v2     = 0;
        $this->gas_remain_v2           = 0;
        $this->gas_remain_amount_v2    = 0;
        $this->sell_complete_time_bigint_v2    = 0;
        $this->update();
    }
    
    /** @Author: NamNH Mar 05, 2019
     *  @Todo: update info sell
     *  @Param:$dateRun Y-m-d
     **/
    public function removeSellInvalidV2($dateRun) {
//        $sell_date_bigint   = strtotime($dateRun);
        $criteria = new CDbCriteria();
//        $criteria->addCondition('t.sell_date_bigint_v2 = ' . $sell_date_bigint);
        DateHelper::searchBetween($dateRun, $dateRun, 'sell_complete_time_bigint_v2', $criteria, true);
        $models = TargetDaily::model()->findAll($criteria);
        $aSellId = CHtml::listData($models, 'sell_id_v2', 'sell_id_v2');
        $aSellId2 = CHtml::listData($models, 'sell_id', 'sell_id');
        $aSellIdRemove1 = $this->findSellNotGas($aSellId);
        $aSellIdRemove2 = $this->findCustomerOld($aSellId2);
        $aSellIdRemove = array_merge($aSellIdRemove1);
        foreach($models as $mTargetDaily){
            if(in_array($mTargetDaily->sell_id_v2, $aSellIdRemove)){
                $mTargetDaily->setDataSellInvalidV2();
                continue;
            }
            if(in_array($mTargetDaily->sell_id, $aSellIdRemove2)){
                $mTargetDaily->setDataSellInvalidV2();
            }
        }
        $models = null;
    }
    
    /** @Author: NamNH Mar 05, 2019
     *  @Todo: update info sell
     *  @Param:$dateRun Y-m-d
     **/
    public function updateInfoSell($dateRun){
        $date_from_ymd              = MyFormat::modifyDays($dateRun, 3, '-', 'month');
        $date_from_date_only_bigint = strtotime($date_from_ymd);
        $created_date_only_bigint   = strtotime($dateRun);
        $date_to                    = MyFormat::modifyDays($dateRun, 1);
        $date_to_second             = strtotime($date_to);
        $aSet = [
            't1.sell_complete_time_bigint_v2' => 't2.complete_time_bigint',
            't1.sell_id_v2'             => 't2.id',
            't1.sell_date_v2'           => 't2.created_date_only',
            't1.sell_date_bigint_v2'    => 't2.created_date_only_bigint',
            't1.sell_total_v2'          => 't2.total',
            't1.sell_grand_total_v2'    => 't2.grand_total',
            't1.gas_remain_v2'          => 't2.gas_remain',
            't1.gas_remain_amount_v2'   => 't2.gas_remain_amount'
        ];
        $aWhere = [
            't1.sell_id_v2 < 1',
            't1.sell_id > 1',
            "t1.sell_date_bigint <= $created_date_only_bigint",
            't1.customer_id = t2.customer_id',
            't1.sell_id <> t2.id',
            't1.sell_date_bigint >= '. $date_from_date_only_bigint,
            "t2.complete_time_bigint >= $created_date_only_bigint",
            "t2.complete_time_bigint <= $date_to_second",
            't2.status ='.Sell::STATUS_PAID,
        ];
        $this->updateInfoSellByDateV2($aSet,$aWhere);
        sleep(1);        
        $this->removeSellInvalidV2($dateRun);
    }
    
        
    /** @Author: NamNH Mar 05, 2019
     *  @Todo: insert target share
     **/
    public function insertInfoShare($dateRun){
        $aRowInsert                         = [];
        $type                               = TargetDaily::TYPE_CCS_SHARE;
        $created_date_only_bigint           = strtotime($dateRun);
        $created_date                       = date('Y-m-d H:i:s');
        $this->removeShareByDate($created_date_only_bigint);
        $criteria=new CDbCriteria;
        DateHelper::searchBetween($dateRun, $dateRun, 'complete_time_bigint', $criteria, true);
//        $criteria->compare('t.created_date_only_bigint', $created_date_only_bigint);
        $criteria->addCondition('t.first_order='.Sell::IS_FIRST_ORDER);
        $criteria->compare('t.status', Sell::STATUS_PAID);
        $aSell          = Sell::model()->findAll($criteria);
        $aSellId        = CHtml::listData($aSell, 'id', 'id');
        $aCustomerId    = CHtml::listData($aSell, 'customer_id', 'customer_id');
        $aSellIdRemove1 = $this->findSpjBrandVo($aSellId);
        $aSellIdRemove2 = $this->findCustomerOld($aSellId);
        $aSellIdRemove3 = $this->findSellNotGas($aSellId);
        $aSellIdRemove  = array_merge($aSellIdRemove1, $aSellIdRemove2,$aSellIdRemove3);
        $aEmployeeCcs   = $this->getArrayEmployeeCcs($dateRun);
        $aUserOwnerLv2  =$this->getListParentLv2($aCustomerId);
        foreach($aSell as $mSell){
            if(!in_array($mSell->id, $aSellIdRemove) && isset($aUserOwnerLv2[$mSell->customer_id]) && in_array($aUserOwnerLv2[$mSell->customer_id], $aEmployeeCcs)){
//              isert vao db
                $employee_id = $aUserOwnerLv2[$mSell->customer_id];
                $aRowInsert[]="('$type',
                    '$mSell->customer_id',
                    '$mSell->type_customer',
                    '$mSell->sale_id',
                    '$mSell->agent_id',
                    '$mSell->province_id',
                    '$employee_id',
                    '0',
                    '0',
                    '$mSell->id',
                    '$mSell->created_date_only',
                    '$mSell->created_date_only_bigint',
                    '$mSell->complete_time_bigint',
                    '$mSell->total',
                    '$mSell->grand_total',
                    '$mSell->gas_remain',
                    '$mSell->gas_remain_amount',
                    '$created_date',
                    '$created_date_only_bigint',
                    '0',
                    '$mSell->phone',
                    '0'
                )";
            }
        }
        $this->insertTargetDaiLy($aRowInsert);
    }
    
    /** @Author: NamNH Mar 05, 2019
     *  @Todo: insert target share
     **/
    public function insertInfoTelesale($dateRun){
        $info = "dateRun $dateRun - Run insertInfoTelesale";
        Logger::WriteLog($info);
        $aRowInsert                         = [];
        $type                               = TargetDaily::TYPE_TELESALE_BQV_CALL;
        $created_date_only_bigint           = strtotime($dateRun);
        $created_date                       = date('Y-m-d H:i:s');
        $this->removeShareByDate($created_date_only_bigint,TargetDaily::TYPE_TELESALE_BQV_CALL);
        $aEmployeeCcs   = $this->getArrayEmployeeCcs($dateRun);
        $criteria=new CDbCriteria;
//        Apr 09, 2019 NamNH change to search conplate date
        DateHelper::searchBetween($dateRun, $dateRun, 'complete_time_bigint', $criteria, true);
//        $criteria->compare('t.created_date_only_bigint', $created_date_only_bigint);
        $criteria->compare('t.status', Sell::STATUS_PAID);
        $criteria->addInCondition('t.sale_id', $aEmployeeCcs);
        $criteria->addInCondition('t.type_customer', [STORE_CARD_HGD, STORE_CARD_HGD_CCS]);
        $aSell          = Sell::model()->findAll($criteria);
        if(empty($aSell)){
            return;
        }
        $aSellId        = CHtml::listData($aSell, 'id', 'id');
        $aSellIdRemove1 = $this->findSpjBrandVo($aSellId);
        $aSellIdRemove2 = $this->findCustomerOld($aSellId);
        $aSellIdRemove3 = $this->findCustomerOldNotCall($aSell,$dateRun);
        $aSellIdRemove4 = $this->findSellNotGas($aSellId);
        $aSellIdRemove5 = $this->findTargetDaily($aSell,true);
        $aSellIdRemove  = array_merge($aSellIdRemove1, $aSellIdRemove2,$aSellIdRemove3,$aSellIdRemove4,$aSellIdRemove5);
        foreach($aSell as $mSell){
            if(!in_array($mSell->id, $aSellIdRemove)){
//              isert vao db
                $aRowInsert[]="('$type',
                    '$mSell->customer_id',
                    '$mSell->type_customer',
                    '$mSell->sale_id',
                    '$mSell->agent_id',
                    '$mSell->province_id',
                    '$mSell->sale_id',
                    '0',
                    '0',
                    '$mSell->id',
                    '$mSell->created_date_only',
                    '$mSell->created_date_only_bigint',
                    '$mSell->complete_time_bigint',
                    '$mSell->total',
                    '$mSell->grand_total',
                    '$mSell->gas_remain',
                    '$mSell->gas_remain_amount',
                    '$created_date',
                    '$created_date_only_bigint',
                    '$mSell->support_id',
                    '$mSell->phone',
                    '0'
                )";
            }
        }
        $this->insertTargetDaiLy($aRowInsert);
    }
    
    /** @Author: NamNH Mar 05, 2019
     *  @Todo: insert target share
     **/
    public function insertAppTelesale($dateRun){
        $aRowInsert                         = [];
        $type                               = TargetDaily::TYPE_TELESALE_BQV_APP;
        $created_date_only_bigint           = strtotime($dateRun);
        $created_date                       = date('Y-m-d H:i:s');
        $this->removeShareByDate($created_date_only_bigint,TargetDaily::TYPE_TELESALE_BQV_APP);
        $aEmployeeCcs   = $this->getArrayEmployeeCcs($dateRun);
        $criteria=new CDbCriteria;
        DateHelper::searchBetween($dateRun, $dateRun, 'complete_time_bigint', $criteria, true);
//        $criteria->compare('t.created_date_only_bigint', $created_date_only_bigint);
        $criteria->compare('t.status', Sell::STATUS_PAID);
        $criteria->addInCondition('t.sale_id', $aEmployeeCcs);
        $criteria->addInCondition('t.type_customer', [UsersExtend::STORE_CARD_HGD_APP]);
        $aSell          = Sell::model()->findAll($criteria);
        if(empty($aSell)){
            return;
        }
        $aSellId        = CHtml::listData($aSell, 'id', 'id');
        $aSellIdRemove1 = $this->findSpjBrandVo($aSellId);
        $aSellIdRemove2 = $this->findCustomerOld($aSellId);
        $aSellIdRemove3 = $this->findTargetDaily($aSell,true);
        $aSellIdRemove4 = $this->findCustomerOldNotCall($aSell,$dateRun);
        $aSellIdRemove5 = $this->findSellNotGas($aSellId);
        $aSellIdRemove  = array_merge($aSellIdRemove1, $aSellIdRemove2,$aSellIdRemove3,$aSellIdRemove4,$aSellIdRemove5);
        foreach($aSell as $mSell){
            if(!in_array($mSell->id, $aSellIdRemove)){
//              isert vao db
                $aRowInsert[]="('$type',
                    '$mSell->customer_id',
                    '$mSell->type_customer',
                    '$mSell->sale_id',
                    '$mSell->agent_id',
                    '$mSell->province_id',
                    '$mSell->sale_id',
                    '0',
                    '0',
                    '$mSell->id',
                    '$mSell->created_date_only',
                    '$mSell->created_date_only_bigint',
                    '$mSell->complete_time_bigint',
                    '$mSell->total',
                    '$mSell->grand_total',
                    '$mSell->gas_remain',
                    '$mSell->gas_remain_amount',
                    '$created_date',
                    '$created_date_only_bigint',
                    '$mSell->support_id',
                    '$mSell->phone',
                    '0'
                )";
            }
        }
        $this->insertTargetDaiLy($aRowInsert);
    }
    
    /** @Author: NamNH Mar 05, 2019
     *  @Todo: insert target share
     **/
    public function insertPTTT($dateRun){
        $info = "dateRun $dateRun - Run insertPTTT";
        Logger::WriteLog($info);
        $aRowInsert                         = [];
        $type                               = TargetDaily::TYPE_PTTT_BQV;
        $created_date_only_bigint           = strtotime($dateRun);
        $created_date                       = date('Y-m-d H:i:s');
        $this->removeShareByDate($created_date_only_bigint,TargetDaily::TYPE_PTTT_BQV);
        $criteria=new CDbCriteria;
        DateHelper::searchBetween($dateRun, $dateRun, 'complete_time_bigint', $criteria, true);
//        $criteria->compare('t.created_date_only_bigint', $created_date_only_bigint);
        $criteria->compare('t.status', Sell::STATUS_PAID);
        $criteria->addCondition('t.pttt_code <> ""');
        $aSell          = Sell::model()->findAll($criteria);
        if(empty($aSell)){
            return;
        }
        $aSellId        = CHtml::listData($aSell, 'id', 'id');
        $aSellIdRemove1 = $this->findSpjBrandVo($aSellId);
        $aSellIdRemove2 = $this->findCustomerOld($aSellId);
        $aSellIdRemove3 = $this->findTargetDaily($aSell);
        $aSellIdRemove4 = $this->findSellNotGas($aSellId);
        $aCodeUser      = $this->findCodeUser($aSell);
        $aSellIdRemove  = array_merge($aSellIdRemove1, $aSellIdRemove2,$aSellIdRemove3,$aSellIdRemove4);
        foreach($aSell as $mSell){
            $employeeId    = isset($aCodeUser[$mSell->pttt_code]) ? $aCodeUser[$mSell->pttt_code] : 0;
            if($employeeId == 0 ){continue;}
            if(!in_array($mSell->id, $aSellIdRemove)){
//              isert vao db
                $aRowInsert[]="('$type',
                    '$mSell->customer_id',
                    '$mSell->type_customer',
                    '$mSell->sale_id',
                    '$mSell->agent_id',
                    '$mSell->province_id',
                    '$employeeId',
                    '0',
                    '0',
                    '$mSell->id',
                    '$mSell->created_date_only',
                    '$mSell->created_date_only_bigint',
                    '$mSell->complete_time_bigint',
                    '$mSell->total',
                    '$mSell->grand_total',
                    '$mSell->gas_remain',
                    '$mSell->gas_remain_amount',
                    '$created_date',
                    '$created_date_only_bigint',
                    '$mSell->support_id',
                    '$mSell->phone',
                    '0'    
                )";
            }
        }
        $this->insertTargetDaiLy($aRowInsert);
    }
    
    /** @Author: NamNH Mar 28,2019
     *  @Todo: add old customer for telesale target daily
     **/
    public function findTargetDaily($aSell,$isTelesale = true) {
        $aSellIdRemove = [];
        foreach ($aSell as $key => $mSell) {
            $criteria = new CDbCriteria();
            $criteria->compare('t.customer_id', $mSell->customer_id);
            if($isTelesale){
                $criteria->addCondition('t.sell_id = '. $mSell->id. ' OR '. 't.sell_id_v2 = '. $mSell->id);
            }
            $mTarget    = TargetDaily::model()->find($criteria);
            if(!empty($mTarget)){
                $aSellIdRemove[] = $mSell->id;
            }
        }
        return $aSellIdRemove;
    }
    
    /** @Author: NamNH Mar 28,2019
     *  @Todo: add old customer for telesale target daily
     **/
    public function findCodeUser($aSell) {
        $aCodeSearch    = CHtml::listData($aSell, 'pttt_code', 'pttt_code');
        $criteria       = new CDbCriteria();
        $criteria->addInCondition('t.code', $aCodeSearch);
        $aCode          = SpjCode::model()->findAll($criteria);
        return CHtml::listData($aCode, 'code', 'user_id');
    }
    
    /** @Author: NamNH Mar 27, 2019
     *  @Todo: insert target daily
     **/
    public function insertTargetDaiLy($aRowInsert){
        $tableName = TargetDaily::model()->tableName();
        $sql = "insert into $tableName (type,
                        customer_id,
                        type_customer,
                        sale_id,
                        agent_id,
                        province_id,
                        created_by,
                        monitoring_id,
                        gdkv_id,
                        sell_id,
                        sell_date,
                        sell_date_bigint,
                        sell_complete_time_bigint,
                        sell_total,
                        sell_grand_total,
                        gas_remain,
                        gas_remain_amount,
                        created_date,
                        created_date_only_bigint,
                        support_id,
                        phone,
                        sell_id_v2
                        ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert)>0){
            Yii::app()->db->createCommand($sql)->execute();
        }
    }
    /** @Author: NamNH Mar 05, 2019
     *  @Todo: get get list ccs
     **/
    public function getArrayEmployeeCcs($dateRun,$one_id = ''){
        $dateRun = MyFormat::dateConverYmdToDmy($dateRun, 'Y-m-01');
        $mGasTargetMonthly  = new GasTargetMonthly();
        if(!empty($dateRun)){
            $mGasTargetMonthly->date_from_ymd = $dateRun;
        }
        $aGasOneMany        =  $mGasTargetMonthly->getArrayOneManyMonitorCcs($one_id);
        return CHtml::listData($aGasOneMany, 'many_id', 'many_id');
    }
    
    /** @Author: NamNH Mar 05, 2019
     *  @Todo: get get list ccs
     **/
    public function getListParentLv2($listUserId){
        if(!is_array($listUserId)){
            return;
        }
        $aData      = [];
//        get owner
        $criteria   = new CDbCriteria;
        $criteria->compare('t.promotion_type', AppPromotion::TYPE_FIRST_USER);
        $criteria->addInCondition('t.user_id', $listUserId);
        $aPromotionUser = AppPromotionUser::model()->findAll($criteria);
        $listUserOwner  = CHtml::listData($aPromotionUser, 'user_id', 'promotion_owner_id');
//        get owner lv2
        $criteriaLv2   = new CDbCriteria;
        $criteriaLv2->compare('t.promotion_type', AppPromotion::TYPE_FIRST_USER);
        $criteriaLv2->addInCondition('t.user_id', $listUserOwner);
        $aPromotionUserLv2 = AppPromotionUser::model()->findAll($criteriaLv2);
        $listUserOwnerLv2  = CHtml::listData($aPromotionUserLv2, 'user_id', 'promotion_owner_id');
        foreach ($listUserOwner as $user_id => $promotion_owner_id) {
            $aData[$user_id] = !empty($listUserOwnerLv2[$promotion_owner_id]) ? $listUserOwnerLv2[$promotion_owner_id] : 0;
        }
        return $aData;
    }
    
    /** @Author: NamNH Mar 13, 2019
     **/
    public function removeShareByDate($created_date_only_bigint,$type = TargetDaily::TYPE_CCS_SHARE){
        if(!empty($created_date_only_bigint)){
            $criteria   = new CDbCriteria;
            $criteria->compare('type', $type);
            $criteria->compare('created_date_only_bigint', $created_date_only_bigint);
            TargetDaily::model()->deleteAll($criteria);
        }
    }
    
    /** @Author: NamNH Jul 18, 2019
     *  @Todo: convert new phone to old phone
     * not zero
     **/
    public function convertToOldPhone($oldPhone){
        $result = $oldPhone;
        $aPrefix = UpdateSql1::getArrayPrefixChangeNotZero();
        $prefix_code = substr($result, 0, 2);
        $aPrefixPrev = [];
        foreach ($aPrefix as $key => $value) {
            $aPrefixPrev[$value] = $key;
        }
        if(isset($aPrefixPrev[$prefix_code])){
            $charRemove     = strlen($prefix_code);
            $result       = $aPrefixPrev[$prefix_code].substr($result, $charRemove);
        }
        return $result;
    }
    
    /** @Author: NamNH Jul 18, 2019
     *  @Todo: convert new phone to old phone
     * not zero
     **/
    public function convertToNewPhone($phoneNeedConvert){
        $phone = $phoneNeedConvert;
        if($phone < 1){
            return $phone;
        }
        $aPrefix    = UpdateSql1::getArrayPrefixChangeNotZero();
        $phone      = UsersPhone::formatPhoneSaveAndSearch($phone);
        $prefix_code = substr($phone, 0, 3);
        if(isset($aPrefix[$prefix_code])){// Now0818 fix change dau 11 so
            $charRemove = strlen($prefix_code);
            $phone = $aPrefix[$prefix_code].substr($phone, $charRemove);
        }
        return $phone;
    }
    
    /** @Author: NamNH Apr 24, 2019
     *  @Todo: lây danh sách hóa đơn không có cuộc gọi ra và không có mã khuyến mãi
     **/
    public function findCustomerOldNotCall($aSell,$dateRun){
        $aSellIdRemove = [];
                foreach($aSell as $mSell):
            $monthCheck = $this->getMonthCheck($mSell);
            $date_from_ymd    = MyFormat::modifyDays($mSell->created_date_only, $monthCheck, '-', 'month');
            $criteria = new CDbCriteria();
            $oldPhone = $this->convertToOldPhone($mSell->phone);
            $criteria->addCondition('t.phone = '.$mSell->phone . ' OR t.phone = '.$oldPhone);
            $criteria->compare('t.status', Sell::STATUS_PAID);
            $criteria->addCondition("t.id < $mSell->id");
            DateHelper::searchSmaller($date_from_ymd, 'created_date_only_bigint', $criteria);
            $modelSellOld    = Sell::model()->find($criteria);
//            lấy danh sách mã khuyến mãi của nhân viên telesale
            if(!empty($mSell->promotion_id)){
                $aEmployeeCcs   = $this->getArrayEmployeeCcs($dateRun);
                $criteriaPromotion  = new CDbCriteria();
                $criteriaPromotion->addInCondition('t.owner_id', $aEmployeeCcs);
                $criteriaPromotion->compare('t.id', $mSell->promotion_id);
                $countPromotion     = AppPromotion::model()->count($criteriaPromotion);
                if($countPromotion > 0){
                    continue;
                }
            }
            if(!empty($modelSellOld) && $mSell->support_id != 1){
                $aSellIdRemove[$mSell->id] = $mSell->id;
            }
        endforeach;
        return $aSellIdRemove;
    }
    
    /** @Author: NamNH Apr 11, 2019
     *  @Todo: remove sell not gas
     **/
    public function findSellNotGas(&$aSellId) {
        $aSellIdRemove = [];
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',',  $aSellId);
        $criteria->addInCondition('t.sell_id',$aSellId);
        $criteria->compare("t.materials_type_id", GasMaterialsType::MATERIAL_BINH_12KG);
        $aSell    = SellDetail::model()->findAll($criteria);
        $aSellApply = CHtml::listData($aSell, 'sell_id', 'sell_id');
        foreach($aSellId as $sell_id){
            if(!in_array($sell_id, $aSellApply)){
                $aSellIdRemove[] = $sell_id;
            }
        }
        if(!empty($aSellIdRemove)){
            $strIdSellRemove = implode(',', $aSellIdRemove);
            $info = "findSellNotGas $strIdSellRemove - list sell remove";
//            Logger::WriteLog($info);
        }
        return $aSellIdRemove;
    }
    
    /** @Author: NamNH Apr 12, 2019
     *  @Todo: update created_by 
     *  @Param:
     **/
    public function updateTargetDaily($dateRun){
        $aTypeNotUpdate = $this->getTypeNotUpdateCreatedBy();
        $from = time();
        $aTargetDailyUpdate = [];
//        $sell_date_bigint   = strtotime($dateRun);
        $criteria           = new CDbCriteria();
//        $criteria->addCondition('t.sell_date_bigint = ' . $sell_date_bigint);
        DateHelper::searchBetween($dateRun, $dateRun, 'sell_complete_time_bigint', $criteria, true);
        $criteria->addCondition('t.sell_id > 0');
        $criteria->addCondition('t.sale_id > 0');
        $criteria->addCondition('t.created_by != t.sale_id');
        $criteria->addCondition('t.support_id > 0');
        $criteria->addNotInCondition('t.type',$aTypeNotUpdate);
        $models             = TargetDaily::model()->findAll($criteria);
        $aSellId            = CHtml::listData($models, 'id', 'sell_id');
//        get all model sell where promotion_id > 0.
        $criteriaSell = new CDbCriteria();
        $criteriaSell->addInCondition('t.id', $aSellId);
        $criteriaSell->addCondition('t.promotion_id > 0');
        $aSell        = Sell::model()->findAll($criteriaSell);
        $aPromotionId = CHtml::listData($aSell, 'id', 'promotion_id');
//        lấy danh sách promotion của PTTT code
        $aEmployeeCcs   = $this->getArrayEmployeeCcs($dateRun);
        $criteriaPromotion  = new CDbCriteria();
        $criteriaPromotion->addInCondition('t.owner_id', $aEmployeeCcs);
        $criteriaPromotion->addInCondition('t.id', $aPromotionId);
        $aPromotion         = AppPromotion::model()->findAll($criteriaPromotion);
        $aPromotionPTTT     = CHtml::listData($aPromotion, 'id', 'id');
        foreach ($aSellId as $targetDailyId => $sell_id) {
            if(!array_key_exists($sell_id, $aPromotionId) || !array_key_exists($aPromotionId[$sell_id],$aPromotionPTTT)){
                $aTargetDailyUpdate[$targetDailyId] = $sell_id;
            }
        }
        $sql                = '';
        if(!empty($aTargetDailyUpdate)){
            $this->coppyPtttPoint(array_keys($aTargetDailyUpdate));
            $tableNameTargetDaily       = TargetDaily::model()->tableName();
            $strIdUpdate    = implode(',', array_keys($aTargetDailyUpdate));
            $sql    ="UPDATE  `$tableNameTargetDaily` "
                    . 'SET created_by = sale_id '
                    . 'WHERE id IN ('.$strIdUpdate.')';
            if(!empty($aTypeNotUpdate)){
                 $strNotUpdate    = implode(',', $aTypeNotUpdate);
                $sql.= ' AND type NOT IN ('.$strNotUpdate.')';
            }
            $sql.=';';
            Yii::app()->db->createCommand($sql)->execute();
        }
        $strIdSellRemove    = implode(',', $aTargetDailyUpdate);
        $to = time();
        $second = $to-$from;
        $info = "updateTargetDaily change created_by $dateRun <=> list sell_id: $strIdSellRemove <=>sql excute : $sql <=> done in:+ ".($second).'  Second  <=> '.($second/60).' Minutes '.date('Y-m-d H:i:s');
        Logger::WriteLog($info);
    }
    
    
    /** @Author: NamNH Apr 12, 2019
     *  @Todo: clone Point PTTT
     **/
    public function coppyPtttPoint($aTargetDaily){
        if(empty($aTargetDaily)){
           return; 
        }
        $type = self::TYPE_PTTT_POINT;
        $aUserDelele = [];
        $criteria           = new CDbCriteria();
        $criteria->addInCondition('t.id', $aTargetDaily);
        $aModel = TargetDaily::model()->findAll($criteria);
        foreach ($aModel as $key => $mTargetDaily) {
            if($mTargetDaily->sale_id == $mTargetDaily->created_by){
                continue;
            }
            $aUserDelele[] = $mTargetDaily->customer_id;
            $aRowInsert[]="('$type',
                '$mTargetDaily->customer_id',
                '$mTargetDaily->type_customer',
                '$mTargetDaily->sale_id',
                '$mTargetDaily->agent_id',
                '$mTargetDaily->province_id',
                '$mTargetDaily->created_by',
                '$mTargetDaily->monitoring_id',
                '$mTargetDaily->gdkv_id',
                '0',
                '0',
                '0',
                '0',
                '$mTargetDaily->id',
                '0',
                '0',
                '0',
                '$mTargetDaily->created_date',
                '$mTargetDaily->created_date_only_bigint',
                '0',
                '$mTargetDaily->phone',
                '0'
            )";
        }
        if(empty($aRowInsert)){
            return;
        }
//        Delete old
        $criteriaDelete           = new CDbCriteria();
        $criteriaDelete->addInCondition('customer_id', $aUserDelele);
        $criteriaDelete->compare('type', $type);
        TargetDaily::model()->deleteAll($criteriaDelete);
        $this->insertTargetDaiLy($aRowInsert);
    }
    
    /** @Author: NamNH May 18, 2019
     *  @Todo: update target daily old
     **/
    public function updateTargetDailyDublicate($dateRun,$countRevert = 0) {
        $needUpdate = [];
        $dateFrom = $dateRun;
        if($countRevert > 0 ){
            $dateFrom = MyFormat::modifyDays($dateRun, $countRevert, '-');
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.app_promotion_user_id <= 0 OR t.app_promotion_user_id IS NULL');
        $criteria->addCondition('t.phone > 0');
        DateHelper::searchBetween($dateFrom, $dateRun, 'created_date_only_bigint', $criteria);
        $models = TargetDaily::model()->findAll($criteria);
        foreach ($models as $key => $mTargetDaily) {
            $dateLast = MyFormat::modifyDays(date('Y-m-d',$mTargetDaily->created_date_only_bigint), self::COUNT_OUT_OF_DATE_POINT, '-');
            $criteria1 = new CDbCriteria();
                        $criteria1->addCondition('t.id < '.$mTargetDaily->id);
            $criteria1->addCondition('t.phone = '. $mTargetDaily->phone);
            $criteria1->addCondition('t.id != '.$mTargetDaily->id);
            DateHelper::searchGreater($dateLast, 'created_date_only_bigint', $criteria1);
            $mDub = TargetDaily::model()->find($criteria1);
            if(!empty($mDub)){
                $needUpdate[$mTargetDaily->id] = $mTargetDaily->id;
            }
        }
        if(count($needUpdate) > 0){
            $strId = implode(',', $needUpdate);
            $tableNameTargetDaily       = TargetDaily::model()->tableName();
            $sql = "UPDATE `$tableNameTargetDaily` as t set app_promotion_user_id = 1 "
                    . "WHERE t.id IN ($strId)";
            Yii::app()->db->createCommand($sql)->execute();
        }
        $count = count($needUpdate);
        $info = "dublicate change $dateRun <=> $count <=> $sql";
        Logger::WriteLog($info);
    }
    
    public function getArrSell($aSellId){
        $aResult = [];
        $aSell   = Sell::model()->findAllByPk($aSellId);
        foreach ($aSell as $key => $mSell) {
            $aResult[$mSell->id] = $mSell;
        }
        return $aResult;
    }
    /** @Author: NamNH May 18, 2019
     *  @Todo: remove BQV > 3 month
     **/
    public function updateTargetDailyBQV($dateRun,$countRevert = 0) {
        $typeRemoveDays = $this->getArrayTypeRemoveDays();
        $needUpdate = [];
        $dateFrom = $dateRun;
        if($countRevert > 0 ){
            $dateFrom = MyFormat::modifyDays($dateRun, $countRevert, '-');
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.type_remove <= 0');
        DateHelper::searchBetween($dateFrom, $dateRun, 'sell_date_bigint', $criteria);
        $models = TargetDaily::model()->findAll($criteria);
        $aSellId = CHtml::listData($models, 'sell_id', 'sell_id');
        $aSell      = $this->getArrSell($aSellId);
        foreach ($models as $key => $mTargetDaily) {
            $mSell = isset($aSell[$mTargetDaily->sell_id]) ? $aSell[$mTargetDaily->sell_id] : null;
            $typeCheck = $this->getTypeRemoveBySell($mSell);
            $daysCheck  = isset($typeRemoveDays[$typeCheck]) ? $typeRemoveDays[$typeCheck] : 90;
            $dateToCompare  = $mTargetDaily->sell_date_bigint;
            $dateCreated = $mTargetDaily->getCreatedDate('Y-m-d',false);
            $dateFromCompare = strtotime($dateCreated);
            $countDay  = DateHelper::getCountDayBetween($dateFromCompare, $dateToCompare);
            if($countDay > $daysCheck){
                $needUpdate[$typeCheck][] = $mTargetDaily->id;
            }
        }
        foreach ($needUpdate as $type_remove => $aValue) {
            if(count($aValue) > 0){
                $strId = implode(',', $aValue);
                $tableNameTargetDaily       = TargetDaily::model()->tableName();
                $sql = "UPDATE `$tableNameTargetDaily` as t set type_remove = $type_remove "
                        . "WHERE t.id IN ($strId)";
                Yii::app()->db->createCommand($sql)->execute();
            }
            $count = count($aValue);
            $days = isset($typeRemoveDays[$type_remove]) ? $typeRemoveDays[$type_remove] : 0;
            $info = "remove out of $days days app change $dateRun <=> $count <=> $sql";
            Logger::WriteLog($info);
        }
    }
    
    /** @Author: NamNH May 21, 2019
     *  @Todo: get list created_date code
     * Lấy ngày khuyến mãi khách hàng được tạo
     **/
    public function getAppromotionUser($dateRun,$aUserId) {
        if(empty($aUserId)){
            return [];
        }
        $aResult        = []; 
        $aEmployeeCcs   = $this->getArrayEmployeeCcs($dateRun);
        $criteria = new CDbCriteria();
        $criteria->order = 't.id ASC';
        $criteria->addInCondition('t.user_id', $aUserId);
        $criteria->addInCondition('t.promotion_owner_id', $aEmployeeCcs);
        $criteria->compare('t.promotion_type', AppPromotion::TYPE_FIRST_USER);
        $aModels = AppPromotionUser::model()->findAll($criteria);
        foreach ($aModels as $key => $mAppPromotionUser) {
            $aResult[$mAppPromotionUser->user_id] = $mAppPromotionUser->created_date_bigint;
        }
        return $aResult;
    }
    
    /** @Author: NamNH May 21, 2019
     *  @Todo: get list appromotion user by user id
     **/
    public function getAppromotionUserByOwner($owner,$user_id) {
        $criteria = new CDbCriteria();
        $criteria->order = 't.id DESC';
        $criteria->compare('t.user_id', $user_id);
        $criteria->compare('t.promotion_owner_id', $owner);
        $criteria->compare('t.promotion_type', AppPromotion::TYPE_FIRST_USER);
        $model = AppPromotionUser::model()->find($criteria);
        return $model;
    }
    
    /** @Author: NamNH May 18, 2019
     *  @Todo: update grand total - remove not gas
     **/
    public function updateGrandTotal($dateRun,$countRevert = 0) {
        $dateFrom = $dateRun;
        if($countRevert > 0 ){
            $dateFrom = MyFormat::modifyDays($dateRun, $countRevert, '-');
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.sell_id > 0');
        DateHelper::searchBetween($dateFrom, $dateRun, 'sell_date_bigint', $criteria);
        $models = TargetDaily::model()->findAll($criteria);
        $aSell = CHtml::listData($models, 'sell_id', 'sell_id');
        $criteria1 = new CDbCriteria();
        $criteria1->addCondition('t.sell_id_v2 > 0');
        DateHelper::searchBetween($dateFrom, $dateRun, 'sell_date_bigint_v2', $criteria1);
        $modelsV2 = TargetDaily::model()->findAll($criteria1);
        $aSellV2 = CHtml::listData($modelsV2, 'sell_id_v2', 'sell_id_v2');
        $aSellId = array_merge($aSell,$aSellV2);
        $aAmountNotGas = $this->updateTotalTargetDaily($aSellId);
        $this->updateTargetGrandTotal($models,$aAmountNotGas,'sell_id','sell_grand_total','sell_qty','sell_promotion_item');
        $this->updateTargetGrandTotal($modelsV2,$aAmountNotGas,'sell_id_v2','sell_grand_total_v2','sell_qty_v2','sell_promotion_item_v2');
    }
    
    public function updateTotalTargetDaily($aSellId){
        $mGasStoreCard = new GasStoreCard();
        $result['GRAND_TOTAL'] = [];
        $result['QTY'] = [];
        $criteria = new CDbCriteria();
        $criteria->addInCondition('t.sell_id',$aSellId);
        $criteria->addInCondition("t.materials_type_id", [GasMaterialsType::MATERIAL_TYPE_PROMOTION,GasMaterialsType::MATERIAL_BINH_12KG,GasMaterialsType::MATERIAL_VO_12]);
        $criteria->select = 't.qty_discount,sum(if(t.materials_type_id = '.GasMaterialsType::MATERIAL_BINH_12KG.',t.qty,0)) as qty,sum(if(t.materials_type_id = '.GasMaterialsType::MATERIAL_TYPE_PROMOTION.',t.qty,0)) as agent_id, t.sell_id,t.materials_type_id,t.materials_id,sum(t.amount) as amount, sum(t.promotion_amount) as promotion_amount, sum(t.amount_discount) as amount_discount, sum(t.gas_remain_amount) as gas_remain_amount';
        $criteria->group = 't.qty_discount,t.sell_id,t.materials_type_id,t.materials_id';
        $aSell    = SellDetail::model()->findAll($criteria);
        foreach ($aSell as $key => $mSellDetail) {
            $mSell = new Sell();
            $mSell->attributes = $mSellDetail->attributes;
            $price = 0;
            $amountGift = 0;
            $discount = $mSell->getDiscountNormal();
            if($mSellDetail->materials_type_id == GasMaterialsType::MATERIAL_TYPE_PROMOTION){
                $mGasStoreCard->getAmountGift($price,$amountGift,$mSellDetail->materials_id,$mSellDetail->agent_id); // agent_id is qty
            }
            if(isset($result['GRAND_TOTAL'][$mSellDetail->sell_id])){
                $result['GRAND_TOTAL'][$mSellDetail->sell_id] += $mSellDetail->amount - $mSellDetail->promotion_amount - $discount - $mSellDetail->gas_remain_amount - $amountGift;
                $result['QTY'][$mSellDetail->sell_id] += $mSellDetail->qty;
                $result['AMOUNT_GIFT'][$mSellDetail->sell_id] += $amountGift;
            }else{
                $result['GRAND_TOTAL'][$mSellDetail->sell_id] = $mSellDetail->amount - $mSellDetail->promotion_amount - $discount - $mSellDetail->gas_remain_amount - $amountGift;
                $result['QTY'][$mSellDetail->sell_id] = $mSellDetail->qty;
                $result['AMOUNT_GIFT'][$mSellDetail->sell_id] = $amountGift;
            }
        }
        return $result;
    }
    
    /** @Author: NamNH May 22, 2019
     *  @Todo: Update grand total
     **/
    public function updateTargetGrandTotal($aTargetDaily, $AmountNotGas,$fieldCompare = 'sell_id', $fieldChange = 'sell_grand_total',$fieldQty = 'sell_qty',$fieldPromotion = 'sell_promotion_item'){
        foreach ($aTargetDaily as $key => $mTargetDaily) {
            if(empty($AmountNotGas['GRAND_TOTAL'][$mTargetDaily->$fieldCompare])){
                continue;
            }
            $mTargetDaily->$fieldChange = $AmountNotGas['GRAND_TOTAL'][$mTargetDaily->$fieldCompare];
            $mTargetDaily->$fieldQty = $AmountNotGas['QTY'][$mTargetDaily->$fieldCompare];
            $mTargetDaily->$fieldPromotion = $AmountNotGas['AMOUNT_GIFT'][$mTargetDaily->$fieldCompare];
            $mTargetDaily->update();
        }
    }
    
    /** @Author: NamNH May 29, 2019
     *  @Todo: update grand total - remove not gas
     **/
    public function removeTelesaleRewarded($dateRun) {
        $from = time();
        $mGasTargetMonthly = new GasTargetMonthly();
        $aTelesale = $this->getArrayEmployeeCcs($dateRun,$mGasTargetMonthly->getArrayMonitorTelesale());
        $dateFrom = $dateRun;
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.sell_id > 0');
        $criteria->addCondition('t.support_id < 1');
        $criteria->addInCondition('t.created_by',$aTelesale);
        $criteria->addInCondition('t.type',$this->getTypeRemoveTelesaleBqv());
        DateHelper::searchBetween($dateFrom, $dateRun, 'sell_date_bigint', $criteria);
        $models = TargetDaily::model()->findAll($criteria);
        $criteria1 = new CDbCriteria();
        $criteria1->addCondition('t.sell_id_v2 > 0');
        $criteria1->addCondition('t.support_id < 1');
        $criteria1->addInCondition('t.created_by',$aTelesale);
        $criteria1->addInCondition('t.type',$this->getTypeRemoveTelesaleBqv());
        DateHelper::searchBetween($dateFrom, $dateRun, 'sell_date_bigint_v2', $criteria1);
        $modelsV2 = TargetDaily::model()->findAll($criteria1);
        $this->removeRewarded($aTelesale,$models);
        $this->removeRewarded($aTelesale,$modelsV2,true);
        $to = time();
        $second = $to-$from;
        $info = "dateRun $dateRun - Run  removeTelesaleRewarded done in:+ ".($second).'  Second  <=> '.($second/60).' Minutes '.date('Y-m-d H:i:s');
        Logger::WriteLog($info);
    }
    
    /** @Author: NamNH May 29, 2019
     *  @Todo: function remove sell was rewarded
     *  @Param:
     **/
    public function removeRewarded($aTelesale,$aTargetDaily = [],$v2 = false){
        if(empty($aTargetDaily)){
            return;
        }
        $Sell1Sell2 = [];
        if($v2){
            $Sell1Sell2 = CHtml::listData($aTargetDaily, 'sell_id', 'sell_id_v2');
        }
        $aCodeEx = $aSellIdRemove = [];
        $aSellId = CHtml::listData($aTargetDaily, 'sell_id', 'sell_id');
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',',  $aSellId);
        $criteria->addCondition("t.id IN ($sParamsIn)");
        $aSell    = Sell::model()->findAll($criteria);
        foreach ($aSell as $key => $mSell) {
            if(!($this->checkIsReward([$mSell], $aTelesale))){
                $aSellIdRemove[$mSell->id] = $mSell->id;
                if($v2){
                    $sellIdV2 = isset($Sell1Sell2[$mSell->id]) ? $Sell1Sell2[$mSell->id] : 0;
                    $aCodeEx[$sellIdV2] = $sellIdV2;
                }else{
                    $aCodeEx[$mSell->id] = $mSell->id;
                }
                continue;
            }
//            check is reward for employee by suport_id
            $criteria = new CDbCriteria();
            $criteria->compare('t.phone', $mSell->phone);
            $criteria->compare('t.status', Sell::STATUS_PAID);
            $criteria->addCondition('t.support_id > 0 OR t.promotion_type = '.AppPromotion::TYPE_FIRST_USER);
            $criteria->addCondition("t.id < $mSell->id");
            $modelSellOld    = Sell::model()->findAll($criteria);
            if($this->checkIsReward($modelSellOld,$aTelesale)){
                $aSellIdRemove[$mSell->id] = $mSell->id;
                if($v2){
                    $sellIdV2 = isset($Sell1Sell2[$mSell->id]) ? $Sell1Sell2[$mSell->id] : 0;
                    $aCodeEx[$sellIdV2] = $sellIdV2;
                }else{
                    $aCodeEx[$mSell->id] = $mSell->id;
                }
                continue;
            }
            $criteria1 = new CDbCriteria();
            $criteria1->compare('t.customer_id', $mSell->customer_id);
            $criteria1->compare('t.status', Sell::STATUS_PAID);
            $criteria1->addCondition('t.support_id > 0 OR t.promotion_type = '.AppPromotion::TYPE_FIRST_USER);
            $criteria1->addCondition("t.id < $mSell->id");
            $modelSellOld1    = Sell::model()->findAll($criteria1);
            if($this->checkIsReward($modelSellOld1,$aTelesale)){
                $aSellIdRemove[$mSell->id] = $mSell->id;
                if($v2){
                    $sellIdV2 = isset($Sell1Sell2[$mSell->id]) ? $Sell1Sell2[$mSell->id] : 0;
                    $aCodeEx[$sellIdV2] = $sellIdV2;
                }else{
                    $aCodeEx[$mSell->id] = $mSell->id;
                }
                continue;
            }
            
        }
//        update
        if(!empty($aSellIdRemove)){
            foreach($aTargetDaily as $mTargetDaily){
                if(in_array($mTargetDaily->sell_id, $aSellIdRemove)){
                    if($v2){
                        $mTargetDaily->setDataSellInvalidV2();
                    }else{
                        $mTargetDaily->setDataSellInvalid();
                    }
                }
            }
        }
        $info = "rewarded <=> " . implode(',', $aCodeEx);
        Logger::WriteLog($info);
    }
    
    public function checkIsReward($aSell, $aTelesale){
        foreach ($aSell as $key => $mSell) {
            if($mSell->support_id == 1){
                return true;
            }
            if($mSell->app_promotion_user_id > 0){
//                $criteria = new CDbCriteria();
//                $criteria->compare('t.id', $mSell->app_promotion_user_id);
//                $criteria->compare('t.promotion_type', AppPromotion::TYPE_FIRST_USER);
//                $criteria->addInCondition('t.promotion_owner_id', $aTelesale);
//                $mPromotion  = AppPromotionUser::model()->find($criteria);
//                if(!empty($mPromotion)){
//                    return true;
//                }
                if($this->checkPromotionId($mSell,$aTelesale)){
                    return true;
                }
            }
            
        }
        return false;
    }
    
//    Kiểm tra mã khuyến mãi đơn hàng có phải của telesale không
    public function checkPromotionId($mSell,$aTelesale){
        $criteria = new CDbCriteria();
        $criteria->compare('t.id', $mSell->app_promotion_user_id);
        $criteria->compare('t.promotion_type', AppPromotion::TYPE_FIRST_USER);
        $criteria->addInCondition('t.promotion_owner_id', $aTelesale);
        $mPromotion  = AppPromotionUser::model()->find($criteria);
        if(!empty($mPromotion)){
            return true;
        }
        return false;
    }
    
    
    /** @Author: NamNH Jun 15, 2019
     *  @Todo: change created by
     **/
    public function changeAfterUpdateCode($dateRun = ''){
       if(empty($dateRun)){
           return;
       }
       $aEmployeeCcs   = $this->getArrayEmployeeCcs($dateRun);
       $aAppPromotionUser = $this->getAppPromotionUserByDate($dateRun,$aEmployeeCcs);
       $aRowInsert = [];
       foreach ($aAppPromotionUser as $user_id => $value) {
            if(empty($value['AppPromotionUser']) || empty($value['TargetDaily'])){
                continue;
            }
            $mAppPromotionUser  = $value['AppPromotionUser'];
            $mTargetDaily       = $value['TargetDaily'];
            $this->coppyExpired($aRowInsert,self::TYPE_EXPIRED,$mTargetDaily);
//            update sau khi coppy
            $mTargetDaily->created_by = $mAppPromotionUser->promotion_owner_id;
            $mTargetDaily->created_date = $mAppPromotionUser->created_date;
            $created_date_only = explode(' ', $mAppPromotionUser->created_date);
            $mTargetDaily->created_date_only_bigint = !empty($created_date_only[0]) ? strtotime($created_date_only[0]) : strtotime(date('Y-m-d'));
            if($mTargetDaily->sell_id > 1){
                $info = "dateRun $dateRun - Run changeAfterUpdateCode <=> customer_id: ".$mTargetDaily->customer_id . ' - phone: ' . $mTargetDaily->phone;
                Logger::WriteLog($info);
            }
//            Update
            $mTargetDaily->save();
        }
        $info = "dateRun $dateRun - Run changeAfterUpdateCode";
        Logger::WriteLog($info);
        $this->insertTargetDaiLy($aRowInsert);
    }
    
    /** @Author: NamNH Jun 28, 2019
     *  @Todo: coppy to expired
     **/
    public function coppyExpired(&$aRowInsert,$type,$mTargetDaily) {
        $sell_id    =  ($mTargetDaily->sell_id > 1) ? 2 : 1;
        $sell_id_v2 =  ($mTargetDaily->sell_id_v2 > 1) ? 2 : 1;
        $aRowInsert[]="('$type',
                    '$mTargetDaily->customer_id',
                    '$mTargetDaily->type_customer',
                    '$mTargetDaily->sale_id',
                    '$mTargetDaily->agent_id',
                    '$mTargetDaily->province_id',
                    '$mTargetDaily->created_by',
                    '$mTargetDaily->monitoring_id',
                    '$mTargetDaily->gdkv_id',
                    '$sell_id',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '$mTargetDaily->created_date',
                    '$mTargetDaily->created_date_only_bigint',
                    '0',
                    '$mTargetDaily->phone',
                    '$sell_id_v2'
                )";
    }
    
    /** @Author: NamNH Jun 15, 2019
     *  @Todo: get all appPromotionUser create on date
     *  @Param:$dateRun
     * return array ['AppPromotionUser'  => $mAppPromotionUser,
                    'TargetDaily'       => $mTargetDaily]
     **/
    public function getAppPromotionUserByDate($dateRun,$aEmployeeCcs){
        $aNeedAdd = [];
        $criteria = new CDbCriteria();
        DateHelper::searchBetween($dateRun, $dateRun, 'created_date_bigint', $criteria, true);
        $criteria->addInCondition('t.promotion_owner_id', $aEmployeeCcs);
        $criteria->compare('t.promotion_type', AppPromotion::TYPE_FIRST_USER);
        $aAppPromotionUser = AppPromotionUser::model()->findAll($criteria);
        $aUsersId = CHtml::listData($aAppPromotionUser, 'user_id', 'promotion_owner_id');
        $aCustomerOwner = $this->getTargetDaiLyByCustomer(array_keys($aUsersId));
        foreach ($aAppPromotionUser as $key => $mAppPromotionUser) {
//            không coppy trường hợp không tồn tại trong model vì đã có hàm insertAppTelesale thực hiện thêm BQV app và DLV được tính bằng bảng AppPromotionUser
            if(empty($aCustomerOwner[$mAppPromotionUser->user_id])){
                continue;
            }
            if($aCustomerOwner[$mAppPromotionUser->user_id]->created_by == $mAppPromotionUser->promotion_owner_id){
                continue;
            }
            $aNeedAdd[$mAppPromotionUser->user_id] = [
                    'AppPromotionUser'  => $mAppPromotionUser,
                    'TargetDaily'       => $aCustomerOwner[$mAppPromotionUser->user_id]
                ];
        }
        return $aNeedAdd;
    }
    
    /** @Author: NamNH Jun 27, 2019
     *  @Todo: check is exists targetdaily
     *  @Param:$mAppPromotion
     **/
    public function getTargetDaiLyByCustomer($aCustomer){
        $aResult = [];
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.type !='. self::TYPE_EXPIRED);
        $criteria->addInCondition('t.customer_id', $aCustomer);
        $criteria->order = 't.id ASC';
        $aModel = TargetDaily::model()->findAll($criteria);
        foreach ($aModel as $key => $mTargetDaily) {
            $aResult[$mTargetDaily->customer_id] = $mTargetDaily;
        }
        return $aResult;
    }
    /** @Author: DuongNV Jun 12,2018
     *  @Todo: cron gửi mail cảnh báo những nhân viên ko phát sinh điểm làm việc trong 10 ngay
     *  @params $date Y-m-d
     **/
    public function cronAlertNoPoint($date = '') {
        if(empty($date)){
            $date = date('Y-m-d');
        }
        $mTargetDaily               = new TargetDaily();
        $mTargetDaily->unsetAttributes();
        $mTargetDaily->date_from    = MyFormat::modifyDays($date, 10, '-', 'day', 'd-m-Y');
        $mTargetDaily->date_to      = MyFormat::dateConverYmdToDmy($date, 'd-m-Y');
        $aData                      = $mTargetDaily->getReportDailyCcs();
        $aIdUserNoPoint             = [];
        foreach ($aData['POINT_APP'] as $uid => $item) {
            if(!is_numeric($uid)) {
                continue;
            }
            $sum        = 0;
            foreach ($item as $date => $value) {
                $point  = isset($value['point']) ? $value['point'] : 0;
                $app    = isset($value['app']) ? $value['app'] : 0;
                $sum    += ($point + $app);
            }
            if($sum <= 0){
                $aIdUserNoPoint[$uid] = $uid;
            }
        }
        
        $criteria   = new CDbCriteria;
//        $sParamsIn  = implode(',', $aIdUserNoPoint);
//        $criteria->addCondition("t.user_id IN ($sParamsIn)");
        $criteria->addInCondition('t.user_id',$aIdUserNoPoint);
        $aProfile   = UsersProfile::model()->findAll($criteria);
        foreach($aProfile as $mProfile){
            $mUsersHr                   = new UsersHr();
            $mSalaryReport              = new HrSalaryReports();
            $mSalaryReport->cacheUsersProfile[$mProfile->user_id] = $mProfile;
            $mUsersHr->id               = $mProfile->user_id;
            $mUsersHr->mSalaryReport    = $mSalaryReport;
            $dateFromYmd                = MyFormat::dateConverDmyToYmd($mTargetDaily->date_from, '-');
            if(!$mUsersHr->isInWorkingTime($dateFromYmd)){
                unset($aIdUserNoPoint[$mProfile->user_id]);
            }
        }
        
        if(count($aIdUserNoPoint) > 0){
            $aUser = Users::getArrObjectUserByArrUid($aIdUserNoPoint, '');
            $this->sendEmailAlertNoPoint($aUser);
        }
        $strLog = '******* cronAlertNoPoint done at '.date('d-m-Y H:i:s').', '.count($aIdUserNoPoint).' user warning!';
        Logger::WriteLog($strLog);
    }
    
    /** @Author: DuongNV Jun 12,2018
     *  @Todo: gửi mail cảnh báo những nhân viên ko phát sinh điểm làm việc
     **/
    public function sendEmailAlertNoPoint($aUser) {
        if(empty($aUser)){
            return ;
        }
        
        $body  = '[CẢNH BÁO] Danh sách nhân viên PTTT, CCS không phát sinh điểm làm việc trong vòng 10 ngày.<br>';
        $body .= '<br><table style="border-collapse: collapse;" border="1" cellpadding="5">';
        $body .=    '<thead>';
        $body .=        '<tr>';
        $body .=            '<th>STT</th>';
        $body .=            '<th>Nhân viên</th>';
        $body .=            '<th>Chức vụ</th>';
        $body .=            '<th>Đại lý</th>';
        $body .=            '<th>Tỉnh</th>';
        $body .=        '</tr>';
        $body .=    '</thead>';
        $body .=    '<tbody>';
        $no = 1;
        $aRole = Roles::loadItems();
        foreach ($aUser as $mUser) {
            $name           = empty($mUser) ? '' : $mUser->getFullName();
            $role           = empty($aRole[$mUser->role_id]) ? '' : $aRole[$mUser->role_id];
            $agent          = empty($mUser->rAgent) ? '' : $mUser->rAgent->getFullName();
            $province       = empty($mUser->province) ? '' : $mUser->province->name;
            $body .=    '<tr>';
            $body .=        '<td style="text-align:center;">'.$no++.'</td>';
            $body .=        '<td>'.$name.'</td>';
            $body .=        '<td>'.$role.'</td>';
            $body .=        '<td>'.$agent.'</td>';
            $body .=        '<td>'.$province.'</td>';
            $body .=    '</tr>';
        }
        $body .=    '</tbody>';
        $body .= '</table>';
        
        $needMore['title']     = '[CẢNH BÁO] Danh sách nhân viên không phát sinh điểm làm việc';
        $needMore['list_mail'] = [
            'duongnv@spj.vn',
            'giamsat@spj.vn',
            'dungnt@spj.vn',
            'hanhnt@spj.vn',
            'chaulnm@spj.vn',
            'duchoan@spj.vn',
            'phuchv@spj.vn'
        ];
//        $needMore['SendNow']        = 1;// only dev debug
        SendEmail::bugToDev($body, $needMore);
    }
    
//    Lấy danh sách 
    public function getArrayEmployeeMonitor($dateRun,$one_id = ''){
        $dateRun = MyFormat::dateConverYmdToDmy($dateRun, 'Y-m-01');
        $mGasTargetMonthly  = new GasTargetMonthly();
        if(!empty($dateRun)){
            $mGasTargetMonthly->date_from_ymd = $dateRun;
        }
        $aGasOneMany        =  $mGasTargetMonthly->getArrayOneManyMonitorCcs($one_id);
        return CHtml::listData($aGasOneMany, 'many_id', 'one_id');
    }
    
//    NamNH Jul 11, 2019
//    Kiểm tra xem có trùng sdt và trùng số serri và loại vỏ
//    chang target_v1 là về created_by users
    public function checkPtttCodeIsCreate(&$mSell){
//        return false; // return false để xét tất cả đều không được tạo.
        $isPhone = $isSeri = $isVo = $isCode= false;
        if(!empty($mSell->pttt_code)){
            $isCode =  true;
        }
        $mUser = Users::model()->findByPk($mSell->customer_id);
        if(empty($mUser) || empty($mSell)){
            return false;
        }
        $mUserRef   = $this->getUsersRef($mUser,$mSell);
//        check 90 days
        $userCreated = !empty($mUserRef) ? Users::model()->findByPk($mUserRef->user_id) : null;
        $mSell->target_v1 = !empty($userCreated) ? $userCreated->created_by : 0;
        $dateFromCompare    = !empty($userCreated) ? $userCreated->created_date_bigint : 0;
        $dateToCompare      = !empty($mSell) ? $mSell->created_date_only_bigint : 0;
        $countDay  = DateHelper::getCountDayBetween($dateFromCompare, $dateToCompare);
        $typeRemoveDays = $this->getArrayTypeRemoveDays();
        $typeCheck = $this->getTypeRemoveBySell($mSell);
        $daysCheck  = isset($typeRemoveDays[$typeCheck]) ? $typeRemoveDays[$typeCheck] : 90;
        if($countDay > $daysCheck){
            return false;
        }//check 90 days
        $ptttSeri   = !empty($mUserRef->contact_technical_name) ? $mUserRef->contact_technical_name : '';// seri và loại vỏ mà PTTT đã nhập trc đây
        $ptttVo     = !empty($mUserRef->contact_technical_phone) ? $mUserRef->contact_technical_phone : '';
        if(!empty($mSell->rDetail) && is_array($mSell->rDetail)){
            foreach($mSell->rDetail as $mDetail){
                if($mDetail->materials_type_id == GasMaterialsType::MATERIAL_VO_12){
                    if((!empty($mDetail->seri) && $mDetail->seri == $ptttSeri) ){
                        $isSeri =  true; 
                    }
                    if($mDetail->materials_id == $ptttVo){
                        $isVo = true;
                    }
                }
            }
        }
        $phoneSell = '0'.$mSell->phone;
        $strPhone = isset($mUserRef->rUser->phone) ? $mUserRef->rUser->phone : '';
        $tempPhone = explode('-', $strPhone);
        if(in_array($phoneSell, $tempPhone)){
            $isPhone = true;
        }
        return ($isCode && $isSeri && $isVo)|| ($isCode && $isPhone && $isVo) || ($isCode && $isPhone && $isSeri) || ($isPhone && $isSeri && $isVo);
    }
    
    /** @Author: NamNH Jul 31, 2019
     *  @Todo: lấy users ref mới nhất từ số điện thoại, trường hợp nhân viên tổng đài chọn khách hàng củ không phải khách hàng nhân viên vừa làm điểm
     *  @Param:param
     **/
    public function getUsersRef($mUser,$mSell = null){
//        lấy tất cả user_id bới số phone
        $tempPhone = explode('-', $mUser->phone);
        $UsersId = $this->getUsersIdByPhone($tempPhone,STORE_CARD_HGD_CCS); // chỉ lấy user thuộc Dân Dụng
        if($mUser->is_maintain == STORE_CARD_HGD_CCS){
            $UsersId[$mUser->id] = $mUser->id;
        }
        if(!empty($mSell->pttt_code)){
            $criteriaUsers = new CDbCriteria();
            $criteriaUsers->compare('t.temp_password', $mSell->pttt_code);
            $criteriaUsers->compare('t.role_id', ROLE_CUSTOMER);
            $criteriaUsers->compare('t.is_maintain', STORE_CARD_HGD_CCS);
            $criteriaUsers->addCondition('t.type='.CUSTOMER_TYPE_STORE_CARD);
            $criteriaSub = new CDbCriteria();
            $criteriaSub->addCondition("t.code='".trim($mSell->pttt_code)."'");
            $model = SpjCode::model()->find($criteriaSub);
            if(!empty($model)){
                $criteriaUsers->addCondition('t.id='.$model->pttt_customer_id);
            }
            $criteriaUsers->order = 't.id DESC';
            $mUserRescan = Users::model()->find($criteriaUsers);
            if(!empty($mUserRescan)){
                $UsersId[$mUserRescan->id] = $mUserRescan->id;
            }
        }
//        lấy danh sách usersRef
        $criteria = new CDbCriteria();
        $criteria->addInCondition('t.user_id', $UsersId);
        $criteria->order = 't.id DESC';
        $mUsersRef = UsersRef::model()->find($criteria);
        if(!empty($mUsersRef)){
            $mUsersRef->getMapFieldContact();
        }
//        $mUser->LoadUsersRef();
//        $mUsersRef = $mUser->mUsersRef;
        return $mUsersRef;
    }
    /** @Author: NamNH Jul 05, 2019
     *  @Todo: remove invalid pttt code
     *  @Param:$dateRun Y-m-d
     **/
    public function removePtttInvalid($dateRun){
        if(empty($dateRun)){
           return;
        }
        $aRemove = $aUsersHgdPttt =[];
        $mGasTargetMonthly = new GasTargetMonthly();
        $aTargetDailyNeedCheck = [];
        $aEmployee = $this->getArrayEmployeeCcs($dateRun);
        $aTelesale = $this->getArrayEmployeeCcs($dateRun,$mGasTargetMonthly->getArrayMonitorTelesale());
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.sell_id > 0');
        $criteria->addInCondition('t.created_by',$aEmployee);
        $criteria->addInCondition('t.type',$mGasTargetMonthly->getTypeBqvPTTT());
        DateHelper::searchBetween($dateRun, $dateRun, 'sell_date_bigint', $criteria);
        $models = TargetDaily::model()->findAll($criteria);
        foreach ($models as $key => $mTargetDaily) {
            if(in_array($mTargetDaily->created_by,$aTelesale)){
                continue;
            }
            $aTargetDailyNeedCheck[$mTargetDaily->id] = $mTargetDaily;
        }
        if(empty($aTargetDailyNeedCheck)){
            return;
        }
        $aSellId = CHtml::listData($aTargetDailyNeedCheck, 'sell_id', 'created_by');
        $criteriaSell = new CDbCriteria();
        $criteriaSell->addInCondition('t.id', array_keys($aSellId));
        $aSell   = Sell::model()->findAll($criteriaSell);
        foreach ($aSell as $key => $mSell) {
            if(empty($aSellId[$mSell->id])){
                continue;
            }
            if(!$this->checkPromotionId($mSell, [$aSellId[$mSell->id]]) && !$this->checkPtttCodeIsCreate($mSell) && !$this->isPtttCodeByUsersHgdPttt($mSell,$aUsersHgdPttt)){
                $aRemove[$mSell->id] = $mSell->code_no;
            }
            if($this->isPtttCodeByUsersHgdPttt($mSell,$aUsersHgdPttt)){
                echo '<pre>';
                print_r($mSell->code_no);
                echo '</pre>';
            }
        }
        foreach ($aTargetDailyNeedCheck as $id => $mTargetDaily) {
            if(in_array($mTargetDaily->type_remove,$this->getArrayRemove())){
                continue;
            }
            if(array_key_exists($mTargetDaily->sell_id, $aRemove)){
                $mTargetDaily->setDataSellInvalid();
                $mTargetDaily->setDataSellInvalidV2();
            }
        }
    }
    
    /** @Author: NamNH Jul 15, 2019
     *  @Todo: Thay đổi người tạo telesale vì cuộc gọi từ người khác so với người tạo app
     **/
    public function changeCreatedByTelesale($dateRun){
        $mGasTargetMonthly = new GasTargetMonthly();
        $aTelesale = $this->getArrayEmployeeCcs($dateRun,$mGasTargetMonthly->getArrayMonitorTelesale());
        $dateFrom = $dateRun;
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.sell_id > 0');
        $criteria->addCondition('t.support_id = 1');
        $criteria->addInCondition('t.created_by',$aTelesale);
        $criteria->addInCondition('t.type',$this->getTypeRemoveTelesaleBqv());
        DateHelper::searchBetween($dateFrom, $dateRun, 'sell_date_bigint', $criteria);
        $aTargetDaily = TargetDaily::model()->findAll($criteria);
        foreach ($aTargetDaily as $key => $mTargetDaily) {
            if($mTargetDaily->checkUserCall()){
                $mTargetDaily->update();
            }
        }
    }
    
    /** @Author: NamNH Jul 16,2019
     *  @Todo: check users call to customer and update it
     *  update created_by after check
     **/
    public function checkUserCall($checkAppPromotion = true) {
        $mSell = Sell::model()->findByPk($this->sell_id);
        if(($checkAppPromotion && $mSell->promotion_type == AppPromotion::TYPE_FIRST_USER) && !in_array($this->type_remove,$this->getArrayRemove())){
            return false;
        }
        $countMonth = $this->getMonthCheck($mSell);
        $minTime = Telesale::MIN_TIME_OF_CALL;
        $date    = $this->sell_date;
        $aUserId = $this->getUsersIdByPhone($this->phone);
        $criteria = new CDbCriteria;
        $criteria->addCondition("t.bill_duration > $minTime");
        $criteria->addCondition("t.created_date_only <= '$date' ");
        $dateLimit = MyFormat::modifyDays($date, $countMonth, '-', 'month');
        $criteria->addCondition("t.created_date_only >= '$dateLimit' ");// KH phải có cuộc gọi trong 3 tháng gần đây thì mới tính CallOut
        $criteria->addInCondition('t.customer_id', $aUserId);
        $criteria->order = 't.id ASC';
        $aFollow     = FollowCustomer::model()->find($criteria);
        if(!empty($aFollow)){
            $this->created_by = $aFollow->employee_id;
            $this->dateFollow = $aFollow->created_date_only;
            return true;
        }
        return false;
    }
    
    public function canRemove(){
        $cUid       = MyFormat::getCurrentUid();
        $aApply     = GasConst::getAdminArray();
        $aApply[]   = GasConst::UID_NGOC_PT;
        $aApply[]   = GasConst::UID_VEN_NTB;
        if(in_array($cUid, $aApply)){
            if($this->sell_id > 1){
                return true;
            }
        }
        return false;
    }
    
    public function canRemoveV2(){
        $cUid = MyFormat::getCurrentUid();
        $aApply     = GasConst::getAdminArray();
        $aApply[]   = GasConst::UID_NGOC_PT;
        $aApply[]   = GasConst::UID_VEN_NTB;
        if(in_array($cUid, $aApply)){
            if($this->sell_id_v2 > 1){
                return true;
            }
        }
        return false;
    }
    /** @Author: NamNH Jul 16,2019
     *  @Todo: check users call to customer and update it
     **/
    public function checkCall(&$countMonth) {
        $mSell = Sell::model()->findByPk($this->sell_id);
        if($mSell->app_promotion_user_id > 0 ){
            return true;
        }
        $monthCheck = $this->getMonthCheck($mSell);
        $countMonth = $monthCheck;
        $minTime = Telesale::MIN_TIME_OF_CALL;
        $date    = $this->sell_date;
        $aUserId = $this->getUsersIdByPhone($this->phone);
        $criteria = new CDbCriteria;
        $criteria->addCondition("t.bill_duration > $minTime");
        $criteria->addCondition("t.created_date_only <= '$date' ");
        $dateLimit = MyFormat::modifyDays($date, $monthCheck, '-', 'month');
        $criteria->addCondition("t.created_date_only >= '$dateLimit' ");// KH phải có cuộc gọi trong 3 tháng gần đây thì mới tính CallOut
        $criteria->addInCondition('t.customer_id', $aUserId);
        $criteria->order = 't.id ASC';
        $aFollow     = FollowCustomer::model()->find($criteria);
        if(!empty($aFollow)){
            return true;
        }
        return false;
    }
    
    /** @Author: NamNH Jul 25, 2019
     *  @Todo: remove telesale call out 2 month in TP.HCM
     **/
    public function removeCallOutTpHCM($dateRun) {
        $mGasTargetMonthly = new GasTargetMonthly();
        $aTelesale = $this->getArrayEmployeeCcs($dateRun,$mGasTargetMonthly->getArrayMonitorTelesale());
        $dateFrom = $dateRun;
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.sell_id > 0');
        $criteria->addCondition('t.support_id = 1');
        $criteria->addInCondition('t.created_by',$aTelesale);
        $criteria->addInCondition('t.type',$this->getTypeRemoveTelesaleBqv());
        DateHelper::searchBetween($dateFrom, $dateRun, 'sell_date_bigint', $criteria);
        $aTargetDaily   = TargetDaily::model()->findAll($criteria);
        $aSellId2       = CHtml::listData($aTargetDaily, 'sell_id', 'sell_id');
//        $aSellIdRemove2 = $this->findCustomerOld($aSellId2);
        
//        Lấy danh sách khách hàng đã gọi trong 2 tháng TP.HCM
        foreach ($aTargetDaily as $key => $mTargetDaily) {
//            $countMonth = 0;
//            if(!$mTargetDaily->checkCall($countMonth)){ //Kiểm tra đơn hàng có cuộc gọi theo quy định hay không
//                $mSell =  Sell::model()->findByPk($mTargetDaily->sell_id);
//                $mUser = Users::model()->findByPk($mTargetDaily->created_by);
//                print_r($countMonth);
//                echo '<br>';
//            }
//            if(in_array($mTargetDaily->sell_id, $aSellIdRemove2)){
//                $mSell =  Sell::model()->findByPk($mTargetDaily->sell_id);
//                $mUser = Users::model()->findByPk($mTargetDaily->created_by);
//                print_r($mSell->code_no);
//                echo '<br>';
//            }
            $mSell =  Sell::model()->findByPk($mTargetDaily->sell_id);
            if($mTargetDaily->CheckDublicate($mTargetDaily->id,$mSell)){
                print_r($mSell->code_no);
                echo '<br>';
            }
        }
    }
    
    /** @Author: NamNH Aug 01, 2019
     *  @Todo: check remove pttt code
     **/
    public function findNotPtttCode(&$aSell) {
        $aResult = [];
        foreach ($aSell as $key => $mSell) {
            if(!$this->checkPtttCodeIsCreate($mSell)){
                $aResult[$mSell->id] = $mSell->id;
            }
        }
        return $aResult;
    }
    /** @Author: NamNH Aug 01, 2019
     *  @Todo: insert đơn hàng dân dụng của PTTT khi không nhập mã code
     *  @Param:$dateRun Y-m-d
     **/
    public function insertPtttNoCode($dateRun) {
        $aRowInsert                         = [];
        $type                               = TargetDaily::TYPE_PTTT_NO_CODE;
        $created_date_only_bigint           = strtotime($dateRun);
        $created_date                       = date('Y-m-d H:i:s');
        $this->removeShareByDate($created_date_only_bigint,TargetDaily::TYPE_PTTT_NO_CODE);
        $criteria=new CDbCriteria;
        DateHelper::searchBetween($dateRun, $dateRun, 'complete_time_bigint', $criteria, true);
        $criteria->compare('t.status', Sell::STATUS_PAID);
        $criteria->addInCondition('t.type_customer', [STORE_CARD_HGD_CCS,STORE_CARD_HGD]);// Aug namnh - add more HGD STORE_CARD_HGD
        $criteria->addCondition('t.app_promotion_user_id <= 0 OR t.app_promotion_user_id IS NULL');
        $criteria->addCondition('t.pttt_code = ""');
        $aSell          = Sell::model()->findAll($criteria);
        if(empty($aSell)){
            return;
        }
        $aSellId        = CHtml::listData($aSell, 'id', 'id');
        $aSellIdRemove1 = $this->findSpjBrandVo($aSellId);
        $aSellIdRemove2 = $this->findCustomerOld($aSellId);
        $aSellIdRemove3 = $this->findTargetDaily($aSell);
        $aSellIdRemove4 = $this->findSellNotGas($aSellId);
        $aSellIdRemove5 = $this->findNotPtttCode($aSell);
        $aSellIdRemove  = array_merge($aSellIdRemove1, $aSellIdRemove2,$aSellIdRemove3,$aSellIdRemove4,$aSellIdRemove5);
        foreach($aSell as $mSell){
            if(!in_array($mSell->id, $aSellIdRemove)){
                $employeeId = $mSell->target_v1;
//              isert vao db
                $aRowInsert[]="('$type',
                    '$mSell->customer_id',
                    '$mSell->type_customer',
                    '$mSell->sale_id',
                    '$mSell->agent_id',
                    '$mSell->province_id',
                    '$employeeId',
                    '0',
                    '0',
                    '$mSell->id',
                    '$mSell->created_date_only',
                    '$mSell->created_date_only_bigint',
                    '$mSell->complete_time_bigint',
                    '$mSell->total',
                    '$mSell->grand_total',
                    '$mSell->gas_remain',
                    '$mSell->gas_remain_amount',
                    '$created_date',
                    '$created_date_only_bigint',
                    '$mSell->support_id',
                    '$mSell->phone',
                    '0'    
                )";
            }
        }
        $this->insertTargetDaiLy($aRowInsert);
        $info = "dateRun $dateRun - Run insertPtttNoCode";
        Logger::WriteLog($info);
    }
    
    public function removeCustomerOld($dateRun){
        $aTelesale = $this->getArrayEmployeeCcs($dateRun);
        $dateFrom = $dateRun;
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.sell_id > 0');
        $criteria->addInCondition('t.created_by',$aTelesale);
        DateHelper::searchBetween($dateFrom, $dateRun, 'sell_date_bigint', $criteria);
        $aTargetDaily   = TargetDaily::model()->findAll($criteria);
        $aSellId = CHtml::listData($aTargetDaily, 'sell_id', 'sell_id');
        $aRemove    = $this->findCustomerOld($aSellId);
        $aMonitor = $this->getArrayEmployeeMonitor($dateRun);
        foreach ($aTargetDaily as $key => $mTargetDaily) {
            if(in_array($mTargetDaily->sell_id, $aRemove)){
                $mUser = Users::model()->findByPk($aMonitor[$mTargetDaily->created_by]);
                print_r($mUser->first_name);
                echo '<br>';
            }
        }
    }
//    kiểm tra đơn hàng support đã đặt gas trong khoản thời gian từ cuộc gọi trước đó hay chưa
//    $dateFollow Y-m-d
    public function checkRewardedBySell($mSell,$aTelesale,$dateFollow){
        $criteria = new CDbCriteria();
        $criteria->compare('t.phone', $mSell->phone);
        $criteria->compare('t.status', Sell::STATUS_PAID);
        $criteria->addCondition('t.support_id > 0 OR t.promotion_type = '.AppPromotion::TYPE_FIRST_USER);
        $criteria->addCondition("t.id < $mSell->id");
        $criteria->addCondition('t.created_date_only >= "'.$dateFollow.'"' );
        $modelSellOld    = Sell::model()->findAll($criteria);
        if($this->checkIsReward($modelSellOld,$aTelesale)){
            return true;
        }
        return false;
    }
    
    /** @Author: NamNH Aug 02,2019
     *  @Todo: insert verry old customer when telesale call
     *  @Param:$dateRun Y-m-d
     **/
    public function insertReTelesaleBqv($dateRun) {
        $mGasTargetMonthly = new GasTargetMonthly();
        $aRowInsert                         = [];
        $type                               = TargetDaily::TYPE_TELESALE_RESCAN;
        $created_date_only_bigint           = strtotime($dateRun);
        $created_date                       = date('Y-m-d H:i:s');
        $this->removeShareByDate($created_date_only_bigint,TargetDaily::TYPE_TELESALE_RESCAN);
        $aEmployeeCcs   = $this->getArrayEmployeeCcs($dateRun);
        $aTelesale = $this->getArrayEmployeeCcs($dateRun,$mGasTargetMonthly->getArrayMonitorTelesale());
        $criteria=new CDbCriteria;
        DateHelper::searchBetween($dateRun, $dateRun, 'complete_time_bigint', $criteria, true);
        $criteria->compare('t.status', Sell::STATUS_PAID);
        $criteria->addCondition('t.support_id = 1');
//        lấy những đơn hàng mà KH đã tạo quá củ không tồn tại trong target.
        $criteria->addNotInCondition('t.sale_id', $aEmployeeCcs);
        $aSell          = Sell::model()->findAll($criteria);
        if(empty($aSell)){
            return;
        }
        $aSellId        = CHtml::listData($aSell, 'id', 'id');
        $aSellIdRemove1 = $this->findSpjBrandVo($aSellId);
        $aSellIdRemove2 = $this->findCustomerOld($aSellId);
        $aSellIdRemove3 = $this->findTargetDaily($aSell,true);
        $aSellIdRemove4 = $this->findCustomerOldNotCall($aSell,$dateRun);
        $aSellIdRemove5 = $this->findSellNotGas($aSellId);
        $aSellIdRemove  = array_merge($aSellIdRemove1, $aSellIdRemove2,$aSellIdRemove3,$aSellIdRemove4,$aSellIdRemove5);
        foreach($aSell as $mSell){
            if(!in_array($mSell->id, $aSellIdRemove)){
                $mTargetDaily = new TargetDaily();
                $mTargetDaily->sell_id = $mSell->id;
                $mTargetDaily->sell_date = $mSell->created_date_only;
                $mTargetDaily->phone = $mSell->phone;
                $mTargetDaily->created_by = 0;
                if($mTargetDaily->checkUserCall(false)){
                    if($mTargetDaily->created_by == 0 || !in_array($mTargetDaily->created_by,$aTelesale)){
                        continue;
                    }
                    if($this->checkRewardedBySell($mSell,$aTelesale,$mTargetDaily->dateFollow)){
                        continue;
                    }
    //              isert vao db
                    $aRowInsert[]="('$type',
                        '$mSell->customer_id',
                        '$mSell->type_customer',
                        '$mTargetDaily->created_by',
                        '$mSell->agent_id',
                        '$mSell->province_id',
                        '$mTargetDaily->created_by',
                        '0',
                        '0',
                        '$mSell->id',
                        '$mSell->created_date_only',
                        '$mSell->created_date_only_bigint',
                        '$mSell->complete_time_bigint',
                        '$mSell->total',
                        '$mSell->grand_total',
                        '$mSell->gas_remain',
                        '$mSell->gas_remain_amount',
                        '$created_date',
                        '$created_date_only_bigint',
                        '$mSell->support_id',
                        '$mSell->phone',
                        '0'
                    )";
                }
            }
        }
        $this->insertTargetDaiLy($aRowInsert);
    }
    
    public function getSellIdByCode($code_no){
        $sellId = -1;
        $criteria = new CDbCriteria();
        $criteria->compare('t.code_no', trim($this->code_no));
        $mSell = Sell::model()->find($criteria);
        if(!empty($mSell)){
            $sellId = $mSell->id;
        }
        return $sellId;
    }
    
//  NamNH Aug  20, 2019 check dublicate BQV
    public function CheckDublicate($mTargetDailyId,$mSell){
        $criteria = new CDbCriteria();
        $criteria->compare('t.customer_id', $mSell->customer_id);
        $criteria->addCondition('t.id <> '.$mTargetDailyId);
        $criteria->addCondition('t.sell_id = '. $mSell->id. ' OR '. 't.sell_id_v2 = '. $mSell->id);
        $mTarget    = TargetDaily::model()->find($criteria);
        if(!empty($mTarget)){
            return true;
        }
        return false;
    }
    
//  Lấy tất cả danh sách BQV theo đại lý  - ngày tạo cùng ngày với ngày mua hàng
//    2076752 ĐLLK Thiện Phương Bù Đăng - Bình Phước
    public function getAllBqvOfAgent($agentId,$dateRun,$onlyCCS = true){
        $mGasTargetMonthly = new GasTargetMonthly();
        $aEmployee = $this->getArrayEmployeeCcs($dateRun);
        $aTelesale = $this->getArrayEmployeeCcs($dateRun,$mGasTargetMonthly->getArrayMonitorTelesale());
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.sell_id > 0');
        $criteria->addInCondition('t.created_by',$aEmployee);
        $criteria->addInCondition('t.type',$mGasTargetMonthly->getTypeBqvPTTT());
        DateHelper::searchBetween($dateRun, $dateRun, 'sell_date_bigint', $criteria);
        $models = TargetDaily::model()->findAll($criteria);
        $aSellId = CHtml::listData($models, 'sell_id', 'sell_id');
        $aSell   = Sell::model()->findAllByPk($aSellId);
        $aSellById = [];
        foreach ($aSell as $key => $mSell) {
            $aSellById[$mSell->id] = $mSell;
        }
        $aMonitor = $this->getArrayEmployeeMonitor($dateRun);
        foreach ($models as $key => $mTargetDaily) {
            if($onlyCCS){
                if(in_array($mTargetDaily->created_by, $aTelesale)){
                    continue;
                }
            }
            $mSell = !empty($aSellById[$mTargetDaily->sell_id]) ? $aSellById[$mTargetDaily->sell_id] : null;
            if(empty($mSell)){
                continue;
            }
            $date = $mTargetDaily->getCreatedDate('Y-m-d',false);
            if($date < $mTargetDaily->sell_date){
                continue;
            }
            if(in_array($mSell->agent_id, $agentId)){
                $mUsers = Users::model()->findByPk($aMonitor[$mTargetDaily->created_by]);
//                $mUsers = Users::model()->findByPk($mTargetDaily->created_by);
                print_r($mUsers->first_name);
//                print_r($mSell->phone);
                echo '<br>';
            }
        }
    }
    
    /** @Author: NamNH Aug 30,2019
     *  @Todo: update customer of agent Telesale
     *  @Param:$dateRun Y-m-d
     **/
    public function updateCustomerOfAgent($dateRun) {
        $needUpdate = [];
        $mGasTargetMonthly = new GasTargetMonthly();
        $aTelesale = $this->getArrayEmployeeCcs($dateRun,$mGasTargetMonthly->getArrayMonitorTelesale());
        $dateFrom = $dateRun;
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.sell_id > 0');
        $criteria->addInCondition('t.created_by',$aTelesale);
        $criteria->addInCondition('t.type',$this->getTypeRemoveTelesaleBqv());
        DateHelper::searchBetween($dateFrom, $dateRun, 'sell_date_bigint', $criteria);
        $models             = TargetDaily::model()->findAll($criteria);
        $aCustomerId        = CHtml::listData($models, 'customer_id', 'customer_id');
        $aCustomerIdAgent = $this->getCustomerAgent($aCustomerId,$aTelesale,$dateRun);
        if(!empty($aCustomerIdAgent)){
            foreach ($models as $key => $mTargetDaily) {
                if($mTargetDaily->event_market_id > 0){
                    $needUpdate[$mTargetDaily->id] = $mTargetDaily->id;
                    continue;
                }
                if(in_array($mTargetDaily->customer_id, $aCustomerIdAgent)){
                    $needUpdate[$mTargetDaily->id] = $mTargetDaily->id;
                }
            }
        }
        if(count($needUpdate) > 0){
            $strId = implode(',', $needUpdate);
            $tableNameTargetDaily       = TargetDaily::model()->tableName();
            $sql = "UPDATE `$tableNameTargetDaily` as t set t.is_customer_agent = ".self::TYPE_CUSTOMER_OF_AGENT
                    . " WHERE t.id IN ($strId)";
            Yii::app()->db->createCommand($sql)->execute();
        }
        $count = count($needUpdate);
        $info = "updateCustomerOfAgent change $dateRun <=> $count <=> $sql";
        Logger::WriteLog($info);
    }
        
    /** @Author: NamNH Aug 30,2019
     *  @Todo: get customer of agent
     *  @Param:$aCustomerId
     **/
    public function getCustomerAgent($aCustomerId,$aTelesale,$dateRun) {
        $result = [];
        if(empty($aCustomerId)){
            return $result;
        }
        $criteria = new CDbCriteria();
        $criteria->addInCondition('t.user_id', $aCustomerId);
        $aUsersInfo = UsersInfo::model()->findAll($criteria);
        foreach ($aUsersInfo as $key => $mUsersInfo) {
            if((empty($mUsersInfo->first_purchase) || $mUsersInfo->first_purchase == $dateRun )
                && in_array($mUsersInfo->created_by,$aTelesale)){
                continue;
            }
            $result[$mUsersInfo->user_id] = $mUsersInfo->user_id;
        }
        return $result;
    }
    
    /** @Author: NamNH Oct0719
    *  @Todo:add pttt point by new
    **/
    public function insertBqvByHgdPttt($dateRun){
        $aRowInsert = [];
        $type       = self::TYPE_USERS_HGD_PTTT;
        $created_date_only_bigint           = strtotime($dateRun);
        $created_date                       = date('Y-m-d H:i:s');
        $this->removeShareByDate($created_date_only_bigint,TargetDaily::TYPE_USERS_HGD_PTTT);
        $criteria   =   new CDbCriteria;
        DateHelper::searchBetween($dateRun, $dateRun, 'complete_time_bigint', $criteria, true);
        $criteria->compare('t.status', Sell::STATUS_PAID);
        $criteria->addCondition('t.app_promotion_user_id <= 0 OR t.app_promotion_user_id IS NULL');
        $aSell          = Sell::model()->findAll($criteria);
        if(empty($aSell)){
            return;
        }
        $aUsersHgdPttt = [];
        $aSellId        = CHtml::listData($aSell, 'id', 'id');
        $aSellIdRemove1 = $this->findSpjBrandVo($aSellId);
        $aSellIdRemove2 = $this->findCustomerOld($aSellId);
        $aSellIdRemove3 = $this->findTargetDaily($aSell);
        $aSellIdRemove4 = $this->findSellNotGas($aSellId);
        $aSellIdRemove5 = $this->findPtttCodeByUsersHgdPttt($aSell,$aUsersHgdPttt);
        $aSellIdRemove  = array_merge($aSellIdRemove1, $aSellIdRemove2,$aSellIdRemove3,$aSellIdRemove4,$aSellIdRemove5);
        foreach($aSell as $mSell){
            if(!in_array($mSell->id, $aSellIdRemove)){
                $mUsersHgdPttt = isset($aUsersHgdPttt[$mSell->id]) ? $aUsersHgdPttt[$mSell->id] : '';
                if(empty($mUsersHgdPttt)){
                    continue;
                }
//                update old
                $aRowInsert[]="('$type',
                    '$mSell->customer_id',
                    '$mSell->type_customer',
                    '$mSell->sale_id',
                    '$mSell->agent_id',
                    '$mSell->province_id',
                    '$mUsersHgdPttt->created_by',
                    '0',
                    '0',
                    '$mSell->id',
                    '$mSell->created_date_only',
                    '$mSell->created_date_only_bigint',
                    '$mSell->complete_time_bigint',
                    '$mSell->total',
                    '$mSell->grand_total',
                    '$mSell->gas_remain',
                    '$mSell->gas_remain_amount',
                    '$created_date',
                    '$created_date_only_bigint',
                    '$mSell->support_id',
                    '$mUsersHgdPttt->phone',
                    '0'    
                )";
            }
        }
        $this->insertTargetDaiLy($aRowInsert);
        $info = "dateRun $dateRun- Run insertBqvByHgdPttt done ".date('Y-m-d H:i:s').' :'. count($aRowInsert);
        Logger::WriteLog($info);
    }
    
    /** @Author: NamNH Oct0719
    *  @Todo: get list remove by code pttt
     * $aUsersHgdPttt lưu lại biến để khi thêm vào không cần phải tìm kiếm lại 1 lần nữa
    **/
    public function findPtttCodeByUsersHgdPttt($aSell,&$aUsersHgdPttt){
        $aResult = [];
        foreach ($aSell as $key => $mSell) {
//            check code
            if(!$this->isPtttCodeByUsersHgdPttt($mSell,$aUsersHgdPttt)){
                $aResult[$mSell->id] = $mSell->id;
                continue;
            }
        }
        return $aResult;
    }
    
    /** @Author: NamNH Oct0819
    *  @Todo: check is pttt code Hgd
    **/
    public function isPtttCodeByUsersHgdPttt($mSell,&$aUsersHgdPttt){
        $mUsersHgdPtttCal = new UsersHgdPttt();
        $mUsersHgdPttt      = $mUsersHgdPtttCal->getCustomerByPhone($mSell->phone,$mSell->pttt_code);
        if(empty($mUsersHgdPttt)){
            return false;
        }
        $aUsersHgdPttt[$mSell->id] = $mUsersHgdPttt;
        
        $isPhone = $isSeri = $isVo = $isCode= false;
        if(!empty($mSell->pttt_code)){
            $isCode =  true;
        }
        if(empty($mUsersHgdPttt) || empty($mSell)){
            return false;
        }
        $dateFromCompare    = !empty($mUsersHgdPttt) ? $mUsersHgdPttt->created_date_bigint : 0;
        $dateToCompare      = !empty($mSell) ? $mSell->created_date_only_bigint : 0;
        $countDay  = DateHelper::getCountDayBetween($dateFromCompare, $dateToCompare);
        $typeRemoveDays = $this->getArrayTypeRemoveDays();
        $typeCheck = $this->getTypeRemoveBySell($mSell);
        $daysCheck  = isset($typeRemoveDays[$typeCheck]) ? $typeRemoveDays[$typeCheck] : 90;
        if($countDay > $daysCheck){
            return false;
        }
        $ptttSeri   = !empty($mUsersHgdPttt->username) ? $mUsersHgdPttt->username : '';
        $ptttVo     = !empty($mUsersHgdPttt->email) ? $mUsersHgdPttt->email : '';
        if(!empty($mSell->rDetail) && is_array($mSell->rDetail)){
            foreach($mSell->rDetail as $mDetail){
                if($mDetail->materials_type_id == GasMaterialsType::MATERIAL_VO_12){
                    if((!empty($mDetail->seri) && $mDetail->seri == $ptttSeri) ){
                        $isSeri =  true; 
                    }
                    if($mDetail->materials_id == $ptttVo){
                        $isVo = true;
                    }
                }
            }
        }
        $ptttPhone = isset($mUsersHgdPttt->phone) ? $mUsersHgdPttt->phone : '';
        $phoneSell = $mSell->phone;
        if(!empty($phoneSell) && $phoneSell == $ptttPhone){
            $isPhone = true;
        }
        return ($isCode && $isSeri && $isVo)|| ($isCode && $isPhone && $isVo) || ($isCode && $isPhone && $isSeri) || ($isPhone && $isSeri && $isVo);
    }
    
    /** @Author: NamNH Jul 11, 2019
     *  @Todo: Hàm thực hiện cron test function 
     *  $aMonitor = $this->getArrayEmployeeMonitor($dateRun);
        $aMonitor[$mTargetDaily->created_by];
     **/
    public function targetCronTest(){
        error_reporting(1); // var_dump(PHP_INT_SIZE);
        ini_set('memory_limit','1500M');
//        $dateRun = '2019-08-01';
//        for($i=0; $i < 19; $i++){
////            print_r($dateRun);
//            $this->removeCallOutTpHCM($dateRun);
//            $dateRun = MyFormat::modifyDays($dateRun, 1);
//        }
        die;
//        import excel
        
//        $mCustomerDraft = new CustomerDraft();
//        $attributes = $_POST['TargetDaily'];
//        try {
//            $count = 0;
//            $from = time();
//            $FileName = $_FILES['TargetDaily']['tmp_name']['file_excel'];
//            if (empty($FileName))
//                return;
//            Yii::import('application.extensions.vendors.PHPExcel', true);
//            $inputFileType = PHPExcel_IOFactory::identify($FileName);
//            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
//            $objPHPExcel    = $objReader->load(@$_FILES['TargetDaily']['tmp_name']['file_excel']);
//            $objWorksheet   = $objPHPExcel->getActiveSheet();
//            $highestRow     = $objWorksheet->getHighestRow(); // e.g. 10
//            if($highestRow > 10000){
//                die("$highestRow row can not import, recheck file excel");
//            }
//            $to = time();
//            $second = $to-$from;
//            $info = "Import $highestRow row done in: ".($second).'  Second  <=> '.($second/60).' Minutes';
//            Logger::WriteLog($info);
//            for ($row = 3; $row <= $highestRow; ++$row) {
//                $customer_id = $status = 0;
//                // file excel cần format column theo dạng text hết để người dùng nhập vào khi đọc không lỗi
//                $name      = MyFormat::removeBadCharacters($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
//                $cmnd      = MyFormat::removeBadCharacters($objWorksheet->getCellByColumnAndRow(4, $row)->getValue());
//                $phone     = MyFormat::removeBadCharacters($objWorksheet->getCellByColumnAndRow(5, $row)->getValue());
//                $location  = MyFormat::removeBadCharacters($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());
//                $job       = MyFormat::removeBadCharacters($objWorksheet->getCellByColumnAndRow(3, $row)->getValue());
//                $card_number   = MyFormat::removeBadCharacters($objWorksheet->getCellByColumnAndRow(6, $row)->getValue());
//                $cmnd = str_replace(' ', '', $cmnd);
//                $card_number = str_replace(' ', '', $card_number);
//                $card_number = str_replace("'", '', $card_number);
//                $mCustomerDraft->convertToPhoneNumber($phone);
//                if(!is_numeric($cmnd) || !is_numeric($phone) || !is_numeric($card_number)){
//                    continue;
//                }
//                $criteria = new CDbCriteria;
//                $criteria->compare('t.id_number', $cmnd);
//                $aUserProfile = UsersProfile::model()->find($criteria);
//                if(!empty($aUserProfile)){
////                    Thực hiện kiểm tra và cập nhật
////                    salary_method_phone, salary_method_account
//                    if(!empty($aUserProfile->salary_method_phone) && $aUserProfile->salary_method_phone == $phone){// || $aUserProfile->salary_method_account != $card_number
//                        if(empty($aUserProfile->salary_method_account)){
//                            $aUserProfile->salary_method_account = $card_number;
//                            $aUserProfile->update();
//                            print_r($aUserProfile->salary_method_account);
//                            echo '<br>';
//                            $count++; 
//                        }
//                        
//                    }
//                }
//            }
//        } catch (Exception $e) {
//            Yii::log("Uid: " . Yii::app()->user->id . "Exception " . $e->getMessage(), 'error');
//            $code = 404;
//            throw new CHttpException($code, $e->getMessage());
//        }
    }
}
