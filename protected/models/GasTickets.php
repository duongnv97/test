<?php

/**
 * This is the model class for table "{{_gas_tickets}}".
 *
 * The followings are the available columns in table '{{_gas_tickets}}':
 * @property string $id
 * @property string $code_no
 * @property string $agent_id
 * @property string $uid_login
 * @property string $title
 * @property string $send_to_id
 * @property integer $admin_new_message
 * @property integer $status
 * @property integer $process_status
 * @property string $process_time
 * @property string $process_user_id
 * @property string $created_date
 */
class GasTickets extends BaseSpj
{
    public $date_from_ymd, $date_to_ymd, $date_from, $date_to, $province_id, $MAX_ID, $autocomplete_name_1 ;
    public $message, $uid_post, $autocomplete_name, $isChangeHandleUser = false;
    public $oldIssueType = 0, $list_id_image, $file_name, $mDetail;
    
    const TYPE_SENT         = 1;
    const TYPE_RECEIVED     = 2;
    const STATUS_OPEN       = 1;
    const STATUS_CLOSE      = 2;
    const ADMIN_SEND        = 1;
    
    const PROCESS_STATUS_NEW = 1;
    const PROCESS_STATUS_PICK = 2;
    const PROCESS_STATUS_FINISH = 3;
    
    const DIEU_PHOI_NHUNG   = 345432;
    const DIEU_PHOI_CHUONG  = 57916;
    const DIEU_PHOI_TRANG   = 852423;//  Phạm Ngọc Thùy Trang Mar 23, 2016
    const DIEU_PHOI_LOAN    = 572967; //  Trần Thị Thanh Loan
    const DIEU_PHOI_THAO    = 475396;//  Nguyễn Thị Thu Thảo
    const DIEU_PHOI_THUY    = 59235;// Phan Xuân Thùy
    const DIEU_PHOI_HUYEN   = 863106;// Bùi Ngọc Huyền - An Giang
    const DIEU_PHOI_HIEU_TM = 1046289; // DP điều phối Trần Minh Hiếu Add ở 2 model: GasTickets + GasOrders
    const DIEU_PHOI_HCM     = 766823; // điều phối HCM để fix tách đơn hàng
    
    public static $HANDLE_TICKET = array(
        GasConst::UID_NGOC_PT           => 'Phương PTK (Đơn hàng Bò Mối, HGĐ)',
//        GasConst::UID_HUNG_NT           => 'Nguyễn Thế Hùng - Sale Admin (Thêm giá Bò Mối)',
//        GasTickets::DIEU_PHOI_CHUONG    => 'Trần Quang Chương (KH bò mối, KV HCM)',
//        GasConst::UID_PHUONG_TT         => 'Trần Thị Phượng (KH bò mối, KV HCM)',
//        GasConst::UID_VAN_TV            => 'Trần Thị Vân (KH bò mối)',
//        GasConst::UID_TRAM_PTN          => 'Phạm Thị Ngọc Trâm (Duyệt KH bò mối)',
//        GasConst::UID_PHUONG_NT         => 'Nguyễn Thị Phương (KH bò mối, KV HCM)',
//        GasConst::UID_ANH_HTN           => 'Hoàng Thị Ngọc Ánh - Xưởng Phước Tân',
        GasLeave::THUC_NH               => 'Nguyễn Hoàng Thúc - Kế toán công nợ',
        GasConst::UID_HA_PT             => 'Phạm Thị Hà (Sale Admin)',
        GasConst::UID_ANH_LQ            => 'Lê Quốc Anh (Tổng Đài)',
        GasConst::UID_ADMIN             => 'Admin - Quản trị',
    );
    public static function getAppHandle($mUser){
        $aUserHandle = array(
            GasConst::UID_NGOC_PT           => 'Phan Thị Kim Phương (Xử lý đơn hàng)',
//            GasConst::UID_TRAM_PTN          => 'Phạm Thị Ngọc Trâm (KH bò mối, KV HCM)',
            GasConst::UID_ADMIN             => 'Admin - Quản trị',
        );
        if($mUser->role_id == ROLE_CUSTOMER){
            unset($aUserHandle[GasConst::UID_VAN_TV]);
        }
        return $aUserHandle;
    }
    
    public static $UID_DIEU_PHOI = array(
//        GasTickets::DIEU_PHOI_CHUONG,
//        GasTickets::DIEU_PHOI_TRANG,
//        GasTickets::DIEU_PHOI_THAO,
//        GasTickets::DIEU_PHOI_THUY,
//        GasConst::UID_PHUONG_TT,
//        GasTickets::DIEU_PHOI_HIEU_TM,
//        GasTickets::DIEU_PHOI_NHUNG,
//        GasConst::UID_DANG_TH,
    );
    
    // all user điều phối set ở đây, để get name ra cho nhanh
    public static $DIEU_PHOI_FULLNAME = array(
//        GasTickets::DIEU_PHOI_CHUONG    => 'Trần Quang Chương',
//        GasTickets::DIEU_PHOI_TRANG     => 'Phạm Ngọc Thùy Trang',
//        GasTickets::DIEU_PHOI_THAO      => 'Nguyễn Thị Thu Thảo',
//        GasTickets::DIEU_PHOI_THUY      => "Phan Xuân Thùy",
//        GasConst::UID_PHUONG_TT         => 'Trần Thị Phượng',
//        GasTickets::DIEU_PHOI_HIEU_TM   => 'Trần Minh Hiếu',
//        GasTickets::DIEU_PHOI_NHUNG     => 'Nguyễn Thị Bảo Trâm',
//        GasConst::UID_DANG_TH           => 'Trần Hải Đăng', 
//        GasConst::UID_VAN_TV            => 'Trần Thị Vân', 
        GasConst::UID_PHUONG_NT         => 'Nguyễn Thị Phương',
    );
    
    public static $UID_DIEU_PHOI_HCM_CUSTOMER = array(// Jun 04, 2016, 2 điều phối có thể tạo và sửa KH
//        GasTickets::DIEU_PHOI_CHUONG,
//        GasTickets::DIEU_PHOI_THUY,
//        GasConst::UID_PHUONG_TT,
//        GasConst::UID_VAN_TV,
//        GasConst::UID_TUYET_LT_CALL,
//        GasTickets::DIEU_PHOI_TRANG,
        GasConst::UID_PHUONG_NT,
        GasLeave::UID_HEAD_GAS_BO,
        GasConst::UID_ANH_LQ,
        GasConst::UID_HIEU_PT, 
        GasConst::UID_VEN_NTB,
        GasLeave::UID_DIRECTOR_BUSSINESS,
    );
    
    /** @Author: ANH DUNG Now 23, 2017
     *  @Todo: get array uid allow create KH hộ GĐ
     **/
    public static function getUidCreateKhApp() {
        return [
            GasConst::UID_PHUONG_NT,
        ];
    }
      
    const FIX_PHIEU_THU     = 1;
    const FIX_PHIEU_CHI     = 2;
    const FIX_CHANGE_GAS    = 3;
    const FIX_CHANGE_VO     = 4;
    const FIX_HGD_PTTT_CODE = 5;
    const FIX_HGD_DISCOUNT  = 6;
    const FIX_HGD_PROMOTION = 7;
    const FIX_GAS_REMAIN    = 8;
    const FIX_SERI          = 9;
    const FIX_STORECARD     = 10;
    const FIX_DATE          = 11;
    const FIX_QTY           = 12;
    const FIX_ORDER_COMPLETE            = 13;
    const FIX_GAS_MISS                  = 14;
    const FIX_MISS_DISCOUNT             = 15;
    const FIX_CHANGE_PRICE              = 16;
    const FIX_SERI_BBGN                 = 17; // Sửa số serri biên bản GNHH
    
    /** @Author: ANH DUNG Dec 30, 2017
     *  @Todo: các lỗi sửa ticket của GN
     **/
    public function getArrayIssue() {
        return [
            GasTickets::FIX_PHIEU_THU       => 'Sửa phiếu thu',
            GasTickets::FIX_PHIEU_CHI       => 'Sửa phiếu chi',
            GasTickets::FIX_CHANGE_GAS      => 'Sửa loại gas',
            GasTickets::FIX_CHANGE_VO       => 'Sửa vỏ',
            GasTickets::FIX_HGD_PTTT_CODE   => 'Sửa PTTT code',
//            GasTickets::FIX_HGD_DISCOUNT    => 'Sửa chiết khấu',// cái sửa chiết khấu anh bỏ đi vì có có liên quan tới quà tặng rồi
            GasTickets::FIX_HGD_PROMOTION   => 'Sửa quà',
            GasTickets::FIX_GAS_REMAIN      => 'Sửa gas dư',
            GasTickets::FIX_SERI            => 'Sửa Seri',
            GasTickets::FIX_STORECARD       => 'Sửa thẻ kho',
            GasTickets::FIX_DATE            => 'Sửa ngày',
            GasTickets::FIX_QTY             => 'Sửa số lượng',
            GasTickets::FIX_ORDER_COMPLETE  => 'Chưa giao gas đã hoàn thành app',
            GasTickets::FIX_GAS_MISS        => 'Gas đầu vào thiếu',
            GasTickets::FIX_MISS_DISCOUNT   => 'Quên bấm giảm giá',
            GasTickets::FIX_CHANGE_PRICE    => 'Sửa setup giá bò mối',
            GasTickets::FIX_SERI_BBGN       => 'Sửa số serri biên bản GNHH',
        ];
    }
    public function getIssue() {
        $aData = $this->getArrayIssue();
        return isset($aData[$this->issue_type]) ? $aData[$this->issue_type] : '';
    }
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_tickets}}';
    }

    /** @return array validation rules for model attributes. */
    public function rules()
    {
        return array(
            array('title, message', 'required','on'=>'create'),
            array('title, message, send_to_id', 'required','on'=>'ApiCreate'),
            array('message', 'required','on'=>'reply'),
            array('title', 'length', 'max'=>250),
            array('issue_type, uid_post, message, id, code_no, agent_id, uid_login, title, send_to_id, admin_new_message, status, process_status, process_time, process_user_id, created_date', 'safe'),
            array('file_name,list_id_image,province_id, date_from, date_to, date_from_ymd, date_to_ymd', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rTicketDetail' => array(self::HAS_MANY, 'GasTicketsDetail', 'ticket_id',
                'order'=>'rTicketDetail.id DESC',
            ),
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rSendToId' => array(self::BELONGS_TO, 'Users', 'send_to_id'),
            'rProcessUserId' => array(self::BELONGS_TO, 'Users', 'process_user_id'),
//            'rTicketDetail'=>array(self::HAS_MANY, 'GasTicketsDetail', 'ticket_id', 'order'=>'rTicketDetail.id DESC'),
//            'rTicketDetail' => array(self::HAS_MANY, 'GasTicketsDetail', 'ticket_id', 'order'=>'rTicketDetail.id DESC'),
            'rFile' => array(self::HAS_MANY, 'GasFile', 'belong_id',
                'on' => 'rFile.type=' . GasFile::TYPE_13_TICKET,
                'order' => 'rFile.id ASC',
            ),
            'rTicketError' => array(self::HAS_MANY, 'MonitorUpdate', 'agent_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'code_no' => 'Code No',
            'agent_id' => 'Agent',
            'uid_login' => 'Uid Login',
            'title' => 'Tiêu Đề',
            'send_to_id' => 'Người xử lý',
            'admin_new_message' => 'Admin New Message',
            'status' => 'Status',
            'process_status' => 'Trạng Thái',
            'process_time' => 'Thời Điểm',
            'process_user_id' => 'Người Xử Lý',
            'created_date' => 'Ngày Tạo',
            'message' => 'Nội Dung Yêu Cầu Hỗ Trợ',
            'issue_type' => 'Phân loại lỗi',
            'province_id' => 'Tỉnh',
            'date_from' => 'Từ ngày',
            'date_to' => 'Đến ngày',
        );
    }

    /**
     * @Author: ANH DUNG Aug 10, 2014
     * @Todo: dùng cho đại lý list những ticket của đại lý đã submit
     */
    public function searchOpen()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.code_no',$this->code_no);
        $criteria->compare('t.agent_id',$this->agent_id);
        $cUid = MyFormat::getCurrentUid();
//        $criteria->addCondition("t.uid_login=$cUid OR t.send_to_id=$cUid" );
        $this->getConditionOpen($criteria, $cUid);
        $criteria->compare('t.process_status',$this->process_status);
        $criteria->order = 't.process_time DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 10,
            ),
        ));
    }
    
    /**
     * @Author: ANH DUNG May 14, 2017
     * @Todo: sử dụng chung điều kiện để load Open giống trên web
     */
    public function getConditionOpen(&$criteria, $cUid) {
        $criteria->addCondition('t.status=' . GasTickets::STATUS_OPEN);
        $condition = '';
        $this->handleViewMore($cUid, $condition);
        $criteria->addCondition(
                " ( t.uid_login=$cUid $condition OR ".
                " ( t.send_to_id=$cUid AND t.admin_new_message=".GasTickets::ADMIN_SEND." ) )"
                ); 
    }
    public function getConditionClose(&$criteria, $cUid) {
        $criteria->addCondition('t.status=' . GasTickets::STATUS_CLOSE);
        $condition = '';
        $this->handleViewMore($cUid, $condition);
        $criteria->addCondition("( t.uid_login=$cUid $condition OR ( t.send_to_id=$cUid AND t.process_user_id<>$cUid ))" );
    }
    
    /** @Author: ANH DUNG Jul 09, 2017
     * @Todo: get list user can view all ticket của Nguyệt
     */
    public function getUidViewAll() {
        $mAppCache = new AppCache();
        $aUidKtkv = $mAppCache->getListdataUserByRole(ROLE_ACCOUNTING_ZONE);
        $aUid = [GasConst::UID_NGUYEN_DTM, GasLeave::PHUONG_PTK,
//            GasConst::UID_NGUYET_PT,
            GasConst::UID_HA_PT,
            GasLeave::UID_VIEW_ALL_TAY_NGUYEN_GIA_LAI,
            GasConst::UID_DOT_NV,
            GasConst::UID_TRUONG_NN,
            GasConst::UID_NHUNG_TTT,
            GasConst::UID_QUYEN_PTL,
        ];
        foreach($aUidKtkv as $uid => $first_name){
            $aUid[] = $uid;
        }
        return array_merge($aUid, GasConst::getArrUidUpdateAll());
    }
    
    /**
     * @Author: ANH DUNG May 20, 2017
     * @Todo: xử lý cho Chị Phương và Nguyên xem ticket của Nguyệt
     */
    public function handleViewMore($cUid, &$condition) {
        $aUidView           = [GasConst::UID_NGUYEN_DTM, GasLeave::PHUONG_PTK, GasConst::UID_NGOC_PT];
        $aUidViewBoMoi      = [GasConst::UID_PHUONG_NT];
        $uidHandleHgd       = GasConst::UID_NGOC_PT;
        $uidHandleBoMoi     = GasConst::UID_VAN_TV;
//        $uidHandleBoMoi     = GasConst::UID_TRAM_PTN;
        if(in_array($cUid, $aUidViewBoMoi)){
            $condition = "OR t.process_user_id=$uidHandleBoMoi OR send_to_id=$uidHandleBoMoi OR t.process_user_id=$uidHandleHgd OR send_to_id=$uidHandleHgd";
        }elseif(in_array($cUid, $this->getUidViewAll())){
            $condition = "OR t.process_user_id=$uidHandleHgd OR send_to_id=$uidHandleHgd";
        }
        
    }
    
    public function searchClose()
    {
        $cUid = MyFormat::getCurrentUid();
        $criteria=new CDbCriteria;
        $criteria->compare('t.code_no',$this->code_no);
        $criteria->compare('t.agent_id',$this->agent_id);        
        $criteria->compare('t.title',$this->title,true);
        $criteria->compare('t.process_status',$this->process_status);
        $criteria->compare('t.process_user_id',$this->process_user_id,true);
        $criteria->order = 't.process_time DESC';
        
        $this->getConditionClose($criteria, $cUid);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 10,
            ),
        ));
    }
    
    /**
     * @Author: ANH DUNG Aug 10, 2014
     * @Todo: dùng cho user được phép xử lý ticket, sẽ load những ticket mới của đại lý submit lên
     */
    public function searchNeedProcess()
    {
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        $criteria=new CDbCriteria;
        $criteria->compare('t.code_no',$this->code_no);
        $criteria->compare('t.agent_id',$this->agent_id);
        $criteria->compare('t.title',$this->title,true);
        $criteria->addCondition("t.uid_login<>".$cUid);
//        if($cRole != ROLE_ADMIN && $cRole != ROLE_DIEU_PHOI){
        if($cRole != ROLE_ADMIN){
            $condition = '';
            $this->handleViewMore($cUid, $condition);
//            $criteria->compare('t.send_to_id', $cUid);
            $criteria->addCondition("(t.send_to_id=$cUid $condition)");
        }
        $criteria->addCondition("t.admin_new_message<>".GasTickets::ADMIN_SEND);
        // loại những ticket do admin send cho user ra, 
        // những ticket đó sẽ đưa vào phần Open Ticket của từng user
        $criteria->addInCondition('t.process_status', array(GasTickets::PROCESS_STATUS_NEW,GasTickets::PROCESS_STATUS_PICK));        
        $criteria->compare('t.process_user_id',$this->process_user_id);
        $criteria->compare('t.status', GasTickets::STATUS_OPEN);
        $criteria->order = 't.process_time DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> self::GetPageSize(),
            ),
        ));
    }
    /**
     * @Author: ANH DUNG Aug 10, 2014
     * @Todo: dùng cho user được phép xử lý ticket, sẽ load những ticket đã xử lý xong của đại lý submit lên
     */
    public function searchNeedProcessDone()
    {
        $cUid = MyFormat::getCurrentUid();
        $criteria=new CDbCriteria;
        $criteria->compare('t.code_no',$this->code_no);
        $criteria->compare('t.agent_id',$this->agent_id);
        $criteria->compare('t.title',$this->title,true);
        $criteria->addCondition("t.uid_login<>".$cUid);
        if(Yii::app()->user->role_id != ROLE_ADMIN){
            $condition = '';
            $this->handleViewMore($cUid, $condition);
//            $criteria->compare('t.send_to_id', $cUid);
            $criteria->addCondition("(t.send_to_id=$cUid $condition)");
//            if($cUid != GasConst::UID_NGUYET_PT){// Jul 03,17 xử lý cho Nguyệt nhìn thấy Ticket Ngọc xử lý
            if(!in_array($cUid, $this->getUidViewAll())){// Jul 03,17 xử lý cho Nguyệt nhìn thấy Ticket Ngọc xử lý
                $criteria->addCondition("t.process_user_id=$cUid" );
            }
        }
        $criteria->addCondition("t.admin_new_message<>".GasTickets::ADMIN_SEND); 
        // loại những ticket do admin send cho user ra, 
        // những ticket đó sẽ đưa vào phần Open Ticket của từng user
        $criteria->addInCondition('t.process_status', array(GasTickets::PROCESS_STATUS_FINISH));
//        $criteria->compare('t.process_user_id',$this->process_user_id,true);
        $criteria->order = 't.process_time DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 10,
            ),
        ));
    }
    
    public function searchNeedProcessButClosed()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.code_no',$this->code_no);
        $criteria->compare('t.agent_id',$this->agent_id);
        $criteria->compare('t.title',$this->title,true);
        if(Yii::app()->user->role_id != ROLE_ADMIN){
            $criteria->compare('t.send_to_id', Yii::app()->user->id);
        }        
        $criteria->addInCondition('t.process_status', array(GasTickets::PROCESS_STATUS_NEW,GasTickets::PROCESS_STATUS_PICK));
        $criteria->compare('t.status', GasTickets::STATUS_CLOSE);
        $criteria->compare('t.process_user_id',$this->process_user_id,true);
        $criteria->order = 't.process_time DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 10,
            ),
        ));
    }
    
    public static function GetPageSize(){
        return Yii::app()->params['ticket_page_size'];
    }

    public function defaultScope()
    {
        return array();
    }
    
    /**
     * @Author: ANH DUNG Aug 09, 2014
     * @Todo: save new ticket and detail new ticket
     * @Param: $model model 
     */
    public function SaveNewTicket(){
        $this->title   = trim(InputHelper::removeScriptTag($this->title));
        $this->code_no = MyFunctionCustom::getNextId('GasTickets', 'T'.date('y'), LENGTH_TICKET,'code_no');
        $this->uid_post = $this->uid_login;
        $this->save();
        $this->SaveOneMessageDetail();
    }
    
    // to do save one message to detail
    public function SaveOneMessageDetail(){
//        if(!self::UserCanPostTicket()) return;
        $mDetail = new GasTicketsDetail();
        $mDetail->ticket_id     = $this->id;
        $mDetail->message       = trim(InputHelper::removeScriptTag($this->message));
        $mDetail->uid_post      = $this->uid_post;
        $mDetail->type          = GasTickets::TYPE_SENT;
        if($this->uid_login != $mDetail->uid_post){
            $mDetail->type = GasTickets::TYPE_RECEIVED;
            GasTickets::UpdateStatusTicket($this, GasTickets::PROCESS_STATUS_FINISH);
        }else{
            if($this->process_status != GasTickets::PROCESS_STATUS_PICK)
                GasTickets::UpdateStatusTicket($this, GasTickets::PROCESS_STATUS_NEW);
        }
        // Jan 26, 2015 thêm cột lưu lại tên user ở thời điểm xử lý ticket
        if($mDetail->rUidPost){
            $mDetail->c_name = MyFormat::GetNameWithLevel($mDetail->rUidPost);
        }
        $mDetail->save();
        $this->mDetail = $mDetail;
        $this->notifyEmployee();
    }
       
    /**
     * @Author: ANH DUNG Aug 09, 2014
     * @Todo: cập nhật trạng thái của ticket, chỗ này dùng cho update pending vs finish
    // trạng thái free của ticker: 1: new, 2: user pick, 3: finish
     * @Param: $mTicket model gastickets
     * @Param: $status 1,2,3 GasTickets::PROCESS_STATUS_FINISH
     */
    public static function UpdateStatusTicket($mTicket, $status){
        $attUpdate = array('process_status','process_time');
        $mTicket->process_status = $status;
        $mTicket->process_time = date('Y-m-d H:i:s');
//        if($status != GasTickets::PROCESS_STATUS_NEW){ // luôn cập nhật process_user_id vì mình sẽ căn cứ vào process_status để show or hide name 
            $mTicket->process_user_id = $mTicket->uid_post;
            $attUpdate[] = 'process_user_id';
//        }
        if($mTicket->isChangeHandleUser){
            $mTicket->process_status = GasTickets::PROCESS_STATUS_NEW;
            $attUpdate[] = 'send_to_id';
            $mTicket->process_user_id = $mTicket->send_to_id;
        }
        if(!empty($mTicket->issue_type)){
            $attUpdate[] = 'issue_type';
            $mMonitorUpdate = new MonitorUpdate();
            $mMonitorUpdate->saveFixTicket($mTicket);
        }
        
        $mTicket->update($attUpdate);
    }
    
    public function CloseTicket(){
        $aUpdate = array('status');
        $this->status = GasTickets::STATUS_CLOSE;
        if($this->uid_post == GasConst::UID_ADMIN){
            $this->process_status    = GasTickets::PROCESS_STATUS_FINISH;
            $this->process_time      = date('Y-m-d H:i:s');
            $aUpdate[] = 'process_status';
            $aUpdate[] = 'process_time';
        }
        $this->update($aUpdate);
//        $this->titleNotify = 'Ticket close: '. $this->title;// có thể không cần send notify này
//        $this->notifyEmployee();
    }
        
    // kiểm tra cho user giới hạn 50 hoặc hơn 50 ticket 1 ngày, trong sysconfig
    public static function UserCanPostTicket(){
        $session=Yii::app()->session;
        $today = date('Y-m-d');
        if(!isset($session['CURRENT_POST_TICKET'][$today])){
            $aSetTo = array($today=>1);
            $session['CURRENT_POST_TICKET'] = $aSetTo;            
        }else{
            if($session['CURRENT_POST_TICKET'][$today] > Yii::app()->params['limit_post_ticket']){
                return false;
            }
            $CurrentPost = $session['CURRENT_POST_TICKET'][$today];
            $CurrentPost++;
            $aSetTo = array($today=>$CurrentPost);
            $session['CURRENT_POST_TICKET'] = $aSetTo;
        }
        return true;
    }
    
    
    // dùng để hiện thị tên ở ngoài, có thể format màu mè hoặc link gì đó ở chỗ này dc
    public static function ShowNameReply($mUser){
        if(is_null($mUser)) return '';
        return $mUser->first_name;
//        $session=Yii::app()->session;
//        return $mUser->first_name." - ".$session['ROLE_NAME_USER'][$mUser->role_id];
    }
    // dùng để hiện thị tên ở ngoài, có thể format màu mè hoặc link gì đó ở chỗ này dc
    public static function ShowNameReplyAtDetailTicket($mUser, $needMore = array()){
        if(is_null($mUser)) return '';
        if ( isset($needMore['mDetailTicket']) ){
            // Jan 26, 2015 thêm cột lưu lại tên user ở thời điểm xử lý ticket
            $mDetailTicket = $needMore['mDetailTicket'];
            if( !empty($mDetailTicket->c_name) ){
                return $mDetailTicket->c_name;
            }
        }
        
        return $mUser->first_name;
//        $session=Yii::app()->session;
//        return $mUser->first_name." - ".$session['ROLE_NAME_USER'][$mUser->role_id];
    }
    
    protected function beforeDelete() {
        GasTicketsDetail::deleteByTicketId($this->id);
        return parent::beforeDelete();
    }
    
    protected function beforeSave() {
        if(empty($this->uid_login_role)){
            $mUser = $this->rUidLogin;
            $this->uid_login_role = $mUser->role_id;
        }
        return parent::beforeSave();
    }
    
    protected function beforeValidate() {
        $this->message = trim($this->message);
        return parent::beforeValidate();
    }
    /**
     * @Author: ANH DUNG Aug 10, 2014
     * @Todo: lấy danh sách người xử lý, hiện tại chỉ cho 1 điều phối xử lý tạo KH
     */
    public static function GetListSendTo(){
//        $res = array(57916=>'Nguyễn Hoàng Thúc(Tạo KH, Sale)');
        $res = GasTickets::$HANDLE_TICKET;
        return $res;
    }
    
    /**
     * @Author: ANH DUNG Aug 10, 2014
     * @Todo: lấy danh sách người xử lý, hiện tại chỉ cho 1 điều phối xử lý tạo KH
     */
    public static function CountNotify(){
        $cUid       = MyFormat::getCurrentUid();        
        $criteria   = new CDbCriteria();
        $criteria->compare('t.status', GasTickets::STATUS_OPEN);
        $strStatus =  GasTickets::PROCESS_STATUS_NEW.','.GasTickets::PROCESS_STATUS_PICK;
        if(GasCheck::isAllowAccess('tickets', 'pick_ticket')): 
            // nếu là user xử lý ticket thì sẽ ntn
            if(Yii::app()->user->role_id == ROLE_ADMIN){
                $criteria->addCondition(
                    " ( t.send_to_id=0 AND t.process_status=".GasTickets::PROCESS_STATUS_NEW .") OR ".
                    " (t.process_status=".GasTickets::PROCESS_STATUS_NEW ." AND t.uid_login<>$cUid) OR".
//                    " ( t.process_status IN ($strStatus) AND t.uid_login<>$cUid) OR".
                    " (t.process_status=".GasTickets::PROCESS_STATUS_PICK ." AND t.uid_login<>$cUid AND t.process_user_id=$cUid) OR".
                    " (t.process_status=".GasTickets::PROCESS_STATUS_FINISH ." AND t.uid_login=$cUid AND t.process_user_id<>$cUid)"
                    ); 
            }else{ // điều phối xử lý ticket
                $criteria->addCondition(
                    " ( t.uid_login=$cUid AND t.process_status=".GasTickets::PROCESS_STATUS_FINISH.") OR ".
                    " ( t.send_to_id=$cUid AND t.process_status=".GasTickets::PROCESS_STATUS_NEW ." ) OR ".
                    " ( t.send_to_id=$cUid AND t.process_status=".GasTickets::PROCESS_STATUS_PICK ." AND t.process_user_id=$cUid )"
//                    " ( t.send_to_id=$cUid AND t.process_status IN ($strStatus) )"
                    ); 
            }
        else: // nếu là đại lý gửi
//            $criteria->compare('t.uid_login', $cUid);
//            $criteria->addInCondition('t.process_status', array(GasTickets::PROCESS_STATUS_FINISH));
            $criteria->addCondition(
                " ( t.uid_login=$cUid AND t.process_status=".GasTickets::PROCESS_STATUS_FINISH.") OR ".
                " ( t.send_to_id=$cUid AND t.process_status=".GasTickets::PROCESS_STATUS_NEW ." )"
                ); 
        endif;
        
        return self::model()->count($criteria);
    }
    
    public function canClose() {
        $cUid = MyFormat::getCurrentUid();
        $ok = false;
        $aUidAllow = [$this->uid_login, GasConst::UID_ADMIN];
        if(in_array($cUid, $aUidAllow) || in_array($cUid, GasConst::getArrUidUpdateAll()) ){
            $ok = true;
        }
        return $ok;
    }
    
    public function getCodeNo() {
        return $this->code_no;
    }
    public function getTitle() {
        $res = $this->title;
        if(!empty($this->issue_type)){
            $res = "[{$this->getIssue()}] $res";
        }
        return $res;
    }
    public function canAppClose() {
        $ok =  '1';
        if($this->status == GasTickets::STATUS_CLOSE){
            $ok =  '0';
        }
        return $ok;
    }
    public function canAppReply() {
        $ok =  '1';
        if($this->status == GasTickets::STATUS_CLOSE){
            $ok =  '0';
        }
        return $ok;
    }
    
    public function getSendToName() {
        $mUser = $this->rSendToId;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    public function getCreatedDate($fomat = 'd/m/Y H:i') {
        return MyFormat::dateConverYmdToDmy($this->created_date, $fomat);
    }
    
    /**
     * @Author: ANH DUNG May 14, 2017
     * @Todo: get list app reply
     */
    public function getAppReply() {
        $aRes = $aReply = [];
        $aRes['id']                 = $this->id;
        $aRes['code_no']            = $this->getCodeNo();
        $aRes['title']              = $this->getTitle();
        $aRes['created_date']       = $this->getCreatedDate();
        $aRes['name_user_to']       = $this->getSendToName();
        $aRes['name_user_reply']    = $this->getProcessUserId();
        $aRes['time_reply']         = $this->getProcessTime();
        $aRes['can_close']          = $this->canAppClose();
        $aRes['can_reply']          = $this->canAppReply();
        
        foreach($this->rTicketDetail as $mDetail){
            $nameTmp = GasTickets::ShowNameReplyAtDetailTicket($mDetail->rUidPost, array('mDetailTicket'=>$mDetail));
            $nameTmp = explode('[', $nameTmp);
            $temp = [];
            $temp['id']             = $mDetail->id;
            $temp['name_user_reply']= strip_tags($nameTmp[0]);
            $temp['position']       = str_replace(']', '', $nameTmp[1]);
            $temp['content']        = $mDetail->message;
            $temp['created_date']   = $mDetail->getCreatedDate();
            $temp['list_image']     = $mDetail->hanleFileView();
            $aReply[] = $temp;
        }
        $aRes['list_reply'] = $aReply;
        return $aRes;
    }
    /**
     * @Author: ANH DUNG May 17, 2017 -- handle listing api
     */
    public function handleApiList(&$result, $q, $mUser) {
        // 1. get list order by user id
        $dataProvider   = $this->ApiListing($q, $mUser);
        $models         = $dataProvider->data;
        $CPagination    = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page']   = $CPagination->pageCount;
        $result['record']       = array();
        if( $q->page >= $CPagination->pageCount ){
            $result['record'] = array();
        }else{
            foreach($models as $model){
                $model->mAppUserLogin       = $mUser;// for canPickCancel
                $temp = array();
                $temp['id']                 = $model->id;
                $temp['code_no']            = $model->getCodeNo();
                $temp['title']              = $model->getTitle();
                $temp['created_date']       = $model->getCreatedDate();
                $temp['name_user_to']       = $model->getSendToName();
                $temp['name_user_reply']    = $model->getProcessUserId();
                $temp['time_reply']         = $model->getProcessTime();
                $temp['can_close']          = $model->canAppClose();
                $temp['can_reply']          = $model->canAppReply();
//                $temp['list_reply']         = $model->getAppReply();
                $result['record'][]         = $temp;
            }
        }
    }

    /**
    * @Author: ANH DUNG Jan 11, 2017
    * @Todo: get listing Order
    * @param: $q object post params
    */
    public function ApiListing($q, $mUser) {
        $criteria = new CDbCriteria();
        if($q->status == Transaction::STATUS_NEW){
            $this->getConditionOpen($criteria, $mUser->id);
        }elseif($q->status == Transaction::STATUS_COMPLETE){
            $this->getConditionClose($criteria, $mUser->id);
        }

        $criteria->order = 't.id DESC';
        $dataProvider=new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize' => GasAppOrder::API_LISTING_ITEM_PER_PAGE,
//                'pageSize' => 5,
                'currentPage' => (int)$q->page,
            ),
          ));
        return $dataProvider;
    }
    
    /**
     * @Author: ANH DUNG May 14, 2017
     * @Todo: handle app post create
     */
    public function handleAppPost($q) {
        $this->send_to_id   = !empty($q->send_to_id) ? $q->send_to_id : '';
        if($this->mAppUserLogin->role_id == ROLE_CUSTOMER){
            $this->send_to_id   = GasConst::UID_NGOC_PT;
        }
        $this->title        = $q->title;
        $this->message      = $q->message;
        $this->uid_login    = $this->mAppUserLogin->id;
        $this->validate();
    }
    
    
    /**
     * @Author: ANH DUNG May 14, 2017
     * @Todo: notify cho các GN khi có trả lời ticket
     * @Param: 
     */
    public function notifyEmployee() {
        if($this->uid_login == $this->uid_post){
            return ;
        }
        if(empty($this->titleNotify)){
            $this->titleNotify = 'Trả lời ticket: '.$this->title;
        }
//        $this->runInsertNotify($this->getListUserIdEmployeeMaintain(), $this->titleNotify, true); // Send Now Dec 21, 2015 
        $this->runInsertNotify($this->getUserIdNotify(), $this->titleNotify, false); // Send By Cron Feb 23, 2017
    }
    
    /**
     * @Author: ANH DUNG May 14, 2017
     * @Todo: get list user send notify ticket event
     */
    public function getUserIdNotify() {
        return [
            $this->uid_login    => $this->uid_login,
            $this->uid_post     => $this->uid_post,
        ];  
    }
    
    /**
     * @Author: ANH DUNG May 14, 2016
     * @Todo: insert to table notify
     * @Param: $aUid array user id need notify
     */
    public function runInsertNotify($aUid, $title, $sendNow = false) {
        $json_var = array();
        foreach($aUid as $uid) {
            if( !empty($uid) ){
                $mScheduleNotify = GasScheduleNotify::InsertRecord($uid, GasScheduleNotify::TICKET_EVENT, $this->id, '', $title, $json_var);
                if($sendNow && !is_null($mScheduleNotify)){
                    $mScheduleNotify->sendImmediateForSomeUser();
                }// Jan 05, 2016 tạm close send luôn lại, vì nó gây chậm bên PMBH khi send
                // sẽ gửi = cron
            }
        }
    }
    
    public function getProcessTime() {
        if($this->process_status != GasTickets::PROCESS_STATUS_FINISH){
            return '';
        }
        return MyFormat::dateConverYmdToDmy($this->process_time, 'd/m/Y H:i');
    }
    public function getProcessUserId() {
        $mUser = $this->rProcessUserId;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    
    /** @Author: ANH DUNG Jan 08, 2018
     *  @Todo: bc sửa ticket
     **/
    public function auditFix() {
        $this->date_from_ymd  = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        $this->date_to_ymd    = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        $aRes   = [];
        $models = $this->auditFixGetData($aRes);
        foreach($models as $item){
            $aRes['OUTPUT'][$item->uid_login][$item->issue_type] = $item->id;
            if(!isset($aRes['OUTPUT_SUM'][$item->uid_login])){
                $aRes['OUTPUT_SUM'][$item->uid_login]   = $item->id;
            }else{
                $aRes['OUTPUT_SUM'][$item->uid_login]   += $item->id;
            }
            $aRes['ARR_UID'][$item->uid_login] = $item->uid_login;
            if(!isset($aRes['SUM_ISSUE'][$item->issue_type])){
                $aRes['SUM_ISSUE'][$item->issue_type]   = $item->id;
            }else{
                $aRes['SUM_ISSUE'][$item->issue_type]   += $item->id;
            }
        }
        if(isset($aRes['OUTPUT_SUM'])){
            arsort($aRes['OUTPUT_SUM']);
        }
        
        return $aRes;
    }
    public function auditFixGetData(&$aRes) {
        $criteria = new CDbCriteria();
        $this->auditFixLimitProvince($aRes, $criteria);
        $criteria->with = ['rTicketError'];
//        $criteria->addCondition('t.issue_type > 0');
        $criteria->addCondition('rTicketError.type='.MonitorUpdate::TYPE_FIX_TICKET);
        $criteria->addBetweenCondition('DATE(rTicketError.last_update)', $this->date_from_ymd, $this->date_to_ymd);
        if(!empty($this->uid_login)){
            $criteria->addCondition('t.uid_login='.$this->uid_login);
        }
        $criteria->select   = 'count(t.id) as id, t.uid_login, issue_type';
        $criteria->group    = 't.uid_login, t.issue_type';
        return GasTickets::model()->findAll($criteria);
    }
    /** Giới hạn các nhân viên theo tỉnh **/
    public function auditFixLimitProvince(&$aRes, &$criteria) {
        $aRole = [ROLE_EMPLOYEE_MAINTAIN, ROLE_EMPLOYEE_MARKET_DEVELOPMENT, ROLE_DRIVER];
        $needMore['aProvinceId']    = is_array($this->province_id) ? $this->province_id : [];
        $needMore['only_id_model']  = 1;
        $needMore['GetAll']         = 1;
        $aUsers = Users::getArrObjecByRoleGroupByProvince($aRole, $needMore);
        $aRes['ARR_USER'] = $aUsers;
        
        if(!is_array($this->province_id)){
            return ;
        }
        $idUserLimit = CHtml::listData($aUsers, 'id', 'id');
        $sParamsIn = implode(',', $idUserLimit);
        $criteria->addCondition("t.uid_login IN ($sParamsIn)");
    }
    
    /** @Author: ANH DUNG Jan 09, 2018
     *  @Todo: check user can change người xử lý
     **/
    public function canChangeUserHandle() {
        $cRole          = MyFormat::getCurrentRoleId();
        $allowChangeUser = false;
        $aRoleAllow     = [ROLE_ADMIN, ROLE_CALL_CENTER, ROLE_DIEU_PHOI, ROLE_SALE_ADMIN, ROLE_ACCOUNTING];
        if(in_array($cRole, $aRoleAllow)){
            $allowChangeUser = true;
        }
        return $allowChangeUser;
    }
    
    /** @Author: PHAM THANH NGHIA 2018
     *  @Todo: search  gasTickets for list
     *  @Code: NGHIA001
     *  @Param: 
     *  
     **/
    
    public function search(){
        $criteria= new CDbCriteria;
        
        $criteria->compare('t.uid_login',$this->uid_login);
        $criteria->compare('t.send_to_id',$this->send_to_id);
        $criteria->compare('t.code_no',$this->code_no);
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        }
        
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        }
        if(!empty($date_from) && !empty($date_to)){
            $criteria->addBetweenCondition("DATE(t.created_date)",$date_from,$date_to);
        }
        $criteria->order = 't.process_time DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> $this->pageSize,
            ),
        ));
    }
    
    public function getUidLoginName() {
        $mUser = $this->rUidLogin;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    
    public function getSendToNameForList() {
        $mUser = $this->rSendToId;
        if($mUser){
            return $mUser->first_name;
        }
        return 'admin';
    }
    
    public function getStatus(){
        if($this->status == GasTicKets::STATUS_OPEN){
            return 'OPEN';
        }
        return 'CLOSE';
    }
    
    public function canUpdateWeb(){
        return false;
    }
    
    public function getListMessage(){
        
        $criteria= new CDbCriteria;
        $criteria->compare('t.ticket_id',$this->id);
        
        $result = GasTicketsDetail::model()->findAll($criteria);
        $message = [];
        foreach($result as $mDetail){
            
            $message[] =   " ( ". Users::model()->findByPk($mDetail->uid_post)->first_name ." - ".$mDetail->getCreatedDate(). " )" . $mDetail->message . "<br>";
        }
        return implode("<br> --------------------- <br>",$message);
    }
    
    /** @Author: HOANG NAM 17/08/2018
     *  @Todo: get file of request
     **/
    public function getFileOnly() {
        return $this->rFile;
    }
    
    /** @Author: ANH DUNG Oct 28, 2018
     *  @Todo: get array id quản lý kho trạm
     **/
    public function getArrayManageWarehouse() {
        return [
            GasCheck::DL_KHO_VINH_LONG      => GasConst::UID_VINH_NH,
            GasCheck::DL_KHO_BEN_CAT        => GasConst::UID_HUY_DD,
            GasCheck::KHO_PHUOC_TAN         => GasLeave::UID_DAT_NV,
            GasCheck::KHO_DAU_KHI_BINH_DINH => GasConst::UID_LOI_TV,
        ];
    }
    
    /** @Author: ANH DUNG Oct 28, 2018
     *  @Todo: get array id quản lý kho trạm
     **/
    public function getIdManageWarehouse($agent_id) {
        $aManage = $this->getArrayManageWarehouse();
        return isset($aManage[$agent_id]) ? $aManage[$agent_id] : 0;
    }
    
    /** @Author: ANH DUNG Oct 28, 2018
     *  @Todo: check lỗi phạt
     **/
    public function handleFlowPunish() {
        if(!empty($this->issue_type) && $this->oldIssueType != $this->issue_type){
            $this->doFlowPunish();
        }
    }
    
    /** @Author: ANH DUNG Now 02, 2018
     *  @Todo: check 1 loại User ko tính lỗi, ko tính lỗi với NV vào làm 2 tháng thử việc
     **/
    public function isExceptionPunish($mUser) {
        $mUsersProfile = $mUser->rUsersProfile;
        if(empty($mUsersProfile)){
            return true;
        }
        if(empty($mUsersProfile->date_begin_job) || $mUsersProfile->date_begin_job == '0000-00-00'){
            return true;
        }
        $dateCheck = MyFormat::modifyDays($mUsersProfile->date_begin_job, 2, '+', 'month');
        return MyFormat::compareTwoDate($dateCheck, date('Y-m-d'));
    }
    
    /** @Author: ANH DUNG Oct 28, 2018
     *  @Todo: handle save errors punish all level user
     *  1. send sms
     *  2. send notify app
     * 
     *  LV1: NV phạt 5k / lỗi
     *  LV1: KTKV, KT kho - trạm: 5k / lỗi
     *  LV1: CVKD/GSKV, quản lý đội xe: 5k / lỗi
     *  LV2: GDKV, quản đốc kho - trạm: 10 / lỗi
     * Nếu NV là PVKH thì lấy KTKV
     * Nếu NV là PTTT thi lấy chuyên viên
     * Nếu NV là Tài xế, Phụ Xe thì lấy id manage warehouse
     **/
    public function doFlowPunish() {
        $mUser = Users::model()->findByPk($this->uid_login);
        if($this->isExceptionPunish($mUser)){
            return ;
        }
        $mTransactionHistory = new TransactionHistory();
        $mTransactionHistory->agent_id      = $mUser->parent_id;
        $mTransactionHistory->province_id   = $mUser->province_id;
        $mTransactionHistory->id            = $this->id;// truyền id ticket vào model EmployeeProblems

        $aUidLevel2 = [];
        $idKtkv             = $mTransactionHistory->getOnlyIdKeToanKv();
        $idGdkv             = $mTransactionHistory->getOnlyIdGdkv();
        $idChuyenVien       = $mTransactionHistory->getOnlyIdChuyenVien();
        $idManageWarehouse  = $this->getIdManageWarehouse($mTransactionHistory->agent_id);
        $idChuyenVien       = !empty($idChuyenVien) ? $idChuyenVien : $idGdkv;
        $idKtkv             = !empty($idKtkv) ? $idKtkv : $idGdkv;
        $agentName          = $mTransactionHistory->getAgent();
        $mEmployeeProblems  = new EmployeeProblems();
        $mScheduleNotify    = new GasScheduleNotify();
//        $mScheduleNotify->type = 0;
        $mScheduleNotify->type = GasScheduleNotify::TYPE_NOT_SEND_SMS;// Oct0218 ko gửi sms phạt nữa, chỉ notify app
        $mScheduleNotify->aUidSend[$this->uid_login] = $this->uid_login; // LV1: NV phạt 5k / lỗi
        $aRoleWarehouse     = [ROLE_DRIVER, ROLE_PHU_XE];
        $this->titleNotify  = "[E02 Tinh Loi] Sua Ticket: {$this->getCodeNo()} - $agentName - NV:{$this->getUidLoginName()} - {$this->getTitle()}";
        // 1. level 1 - 5k
        if($mUser->role_id == ROLE_EMPLOYEE_MAINTAIN){// PVKH
            $mScheduleNotify->aUidSend[$idKtkv] = $idKtkv;
            $aUidLevel2[$idGdkv]                = $idGdkv;
        }elseif($mUser->role_id == ROLE_EMPLOYEE_MARKET_DEVELOPMENT){// PTTT
            $mScheduleNotify->aUidSend[$idChuyenVien] = $idChuyenVien;
            $aUidLevel2[$idGdkv]                = $idGdkv;
        }elseif(in_array($mUser->role_id, $aRoleWarehouse) && !empty ($idManageWarehouse)){// Tai Xe, Phu Xe
            $aUidLevel2[$idManageWarehouse]                = $idManageWarehouse;
        }
        
        $mEmployeeProblems->object_type     = EmployeeProblems::PROBLEM_FIX_TICKET;
        $mEmployeeProblems->money           = $mEmployeeProblems->getPrice();
        $mScheduleNotify->mEmployeeProblems = $mEmployeeProblems;
        $mScheduleNotify->title             = $this->titleNotify;
        $mScheduleNotify->saveErrorsHgdSms($mTransactionHistory, []);
        $this->runInsertNotify($mScheduleNotify->aUidSend, $this->titleNotify, false); // notify to phone app

        // 2. Level 2 - 10k
        $mScheduleNotify->aUidSend          = $aUidLevel2;
        $mEmployeeProblems->object_type     = EmployeeProblems::PROBLEM_FIX_TICKET;
        $mEmployeeProblems->money           = GasConst::PUNISH_10K;
        $mScheduleNotify->mEmployeeProblems = $mEmployeeProblems;
        $mScheduleNotify->saveErrorsHgdSms($mTransactionHistory, []);
        $this->runInsertNotify($mScheduleNotify->aUidSend, $this->titleNotify, false); // notify to phone app
    }
    
}