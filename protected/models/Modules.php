<?php

/**
 * This is the model class for table "{{_modules}}".
 *
 * The followings are the available columns in table '{{_modules}}':
 * @property integer $id
 * @property integer $type 
 * @property string $name
 * @property string $created_date
 * @property string $last_update
 */
class Modules extends BaseSpj
{
    const MODULE_BEP        = 1;
    const MODULE_GOP        = 2;
    const MODULE_DUONG_ONG  = 3;
    const MODULE_OTHER      = 4;
    const MODULE_KHO_90_NCV = 5;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Modules the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_modules}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            array('type, name', 'required','on'=>'create, update'),
            array('id, type, name, created_date, last_update', 'safe'),
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'rDetail' => array(self::HAS_MANY, 'ModulesDetail', 'modules_id'),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Loại',
            'name' => 'Tên',
            'created_date' => 'Ngày tạo',
            'last_update' => 'Lần sửa cuối',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

	$criteria->compare('t.id',$this->id);
	$criteria->compare('t.type',$this->type);
        // Mặc định ẩn loại này đi, search thì show ra DuongNV
        if( empty($this->type) ){
            $criteria->addCondition('t.type != '.self::MODULE_KHO_90_NCV);
        }
	$criteria->compare('t.name',$this->name,true);
	$criteria->compare('t.created_date',$this->created_date,true);
	$criteria->compare('t.last_update',$this->last_update,true);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /** @Author: DuongNV 30/8/18 6:03 PM
     *  @Todo: get a field in model
     *  @Param: $field_name field name in model
     **/
    public function getField($field_name) {
        switch ($field_name){
            case 'type':
                $aType = $this->getArrayTypeNameModuleForWeb();
                return $aType[$this->type];
            case 'created_date':
            case 'last_update':
                $date = MyFormat::dateConverYmdToDmy($this->{$field_name});
                $time = substr($this->{$field_name},10,9);
                return $date.' '.$time;
            default: return $this->{$field_name};
        }
    }
    
    /** @Author: DuongNV 30/08/18 11:55 AM
     *  @Todo: get array type module for app
     **/
    public function getArrayTypeModule() {
        return [
            self::MODULE_BEP,
            self::MODULE_GOP,
            self::MODULE_DUONG_ONG,
            self::MODULE_OTHER,
        ];
    }
    
    /** @Author: DuongNV 30/08/18 11:55 AM
     *  @Todo: get array type module for web
     **/
    public function getArrayTypeModuleForWeb() {
        return [
            self::MODULE_BEP,
            self::MODULE_GOP,
            self::MODULE_DUONG_ONG,
            self::MODULE_OTHER,
            self::MODULE_KHO_90_NCV,
        ];
    }
    
    /** @Author: DuongNV 30/08/18 11:55 AM
     *  @Todo: get array name module for app
     **/
    public function getArrayTypeNameModule() {
        return [
            self::MODULE_BEP        => 'Module bếp',
            self::MODULE_GOP        => 'Module góp',
            self::MODULE_DUONG_ONG  => 'Module đường ống gas',
            self::MODULE_OTHER      => 'Module các thiết bị khác',
        ];
    }
    
    /** @Author: DuongNV 30/08/18 11:55 AM
     *  @Todo: get array name module for web
     **/
    public function getArrayTypeNameModuleForWeb() {
        return [
            self::MODULE_BEP        => 'Module bếp',
            self::MODULE_GOP        => 'Module góp',
            self::MODULE_DUONG_ONG  => 'Module đường ống gas',
            self::MODULE_OTHER      => 'Module các thiết bị khác',
            self::MODULE_KHO_90_NCV => 'Module kho 90 NCV',
        ];
    }
    
    /** @Author: DuongNV 30/08/18 11:48 AM
     *  @Todo: get custom array all module
     *  @return: array
     **/
    public function getCustomArrAllModule() {
        $aTypeModule = $this->getArrayTypeModule();
        $aRes = [];
        $mModuleDetail = [];
        $mModules = Modules::model()->findAll();
        foreach ($mModules as $item) {
            $mModuleDetail[$item->type][] = $item;
        }
//        add module detail
        $mModuleDetailsSort = [];
        $aModuleDetailsMaterial = ModulesDetail::model()->findAll();
        foreach ($aModuleDetailsMaterial as $itemDetail) {
            $mModuleDetailsSort[$itemDetail->modules_id][] = $itemDetail;
        }
        foreach ($aTypeModule as $type) {
            $aData = [];
            foreach($mModuleDetail[$type] as $detail){
                $aData[] = [
                            'module_id'         => "$detail->id",
                            'module_name'       => $detail->name,
                            'module_type'       => "$type",
                            'material_list'     => isset($mModuleDetailsSort[$detail->id]) ? $this->getDetailModule($mModuleDetailsSort[$detail->id]) : [],
                        ];
            }
            $aModule = [
                'id' => "$type",
                'name' => Modules::model()->getArrayTypeNameModule()[$type],
            ];
            $aModule['data'] = $aData;
            $aRes[] = $aModule;
        }
        return $aRes;
    }
    
    /** @Author: DuongNV 30/08/18 11:48 AM
     *  @Todo: get array all module
     *  @return: array
     **/
    public function getArrAllModule($field = '') {
        $mModules = Modules::model()->findAll();
        $res = [];
        foreach ($mModules as $item) {
            if(empty($field)){
                $res[$item->id] = $item->attributes;
            } else {
                $res[$item->id] = $item->{$field};
            }
            
        }
        return $res;
    }
    
    /** @Author: DuongNV 31/08/18
     *  @Todo: Save detail with modules
     *  @Param:
     **/
    public function handleSaveModulesDetail() {
        $this->save();
        if(!empty($_POST['ModulesDetail'])){
            $aRowInsert = [];
            $tblName = ModulesDetail::model()->tableName();
            $sql = "DELETE FROM {$tblName} WHERE modules_id = {$this->id}"; // Delete all old record
            Yii::app()->db->createCommand($sql)->execute();
            $aDetailId = $_POST['ModulesDetail']['materials_id'];
            if(empty($aDetailId)){
                return false;
            }
            foreach ($aDetailId as $key => $item) {
                $aRowInsert[] = '('
                    .'NULL,'
                    .$this->id.','
                    .$this->type.','
                    .$_POST['ModulesDetail']['materials_type_id'][$key].','
                    .$_POST['ModulesDetail']['materials_id'][$key].','
                    .$_POST['ModulesDetail']['qty'][$key]
                .')';
            }
            $sql = "INSERT INTO {$tblName} VALUES "
                 . implode(',', $aRowInsert);
            if (count($aRowInsert)){
                Yii::app()->db->createCommand($sql)->execute();
            }
        }
    }
    
    /** @Author: DuongNV 31/08/18
     *  @Todo: get detail for index view
     *  @Param:
     **/
    public function getHtmlDetail() {
        $mDetail = $this->rDetail;
        $html   = '<table style="width:100%; background:white;">'
                .     '<tr style="display:none;">'
                .       '<td>#</td>'
                .       '<td>Tên</td>'
                .       '<td>DVT</td>'
                .       '<td>SL</td>'
                .     '</tr>'
                .     '<tbody>';
        if(!empty($mDetail)){
            $index = 1;
            foreach ($mDetail as $item) {
                $mMaterial = empty($item->rMaterials) ? '' : $item->rMaterials;
                if(empty($mMaterial)){
                    return '';
                }
                $html   .= '<tr>'
                        .      "<td>".$index++."</td>"
                        .      "<td>{$mMaterial->name}</td>"
                        .      "<td>{$mMaterial->unit}</td>"
                        .      "<td>".ActiveRecord::formatNumberInput($item->qty)."</td>"
                        . '</tr>';
            }
        }
        $html .= '</tbody>'
               . '</table>';
        return $html;
    }
    
    /** @Author: DuongNV 31/08/18
     *  @Todo: Delete all detail before delete module
     **/
    public function beforeDelete() {
        $tblName = ModulesDetail::model()->tableName();
        $sql = "DELETE FROM {$tblName} WHERE modules_id = {$this->id}";
        Yii::app()->db->createCommand($sql)->execute();
        return parent::beforeDelete();
    }

    /**
     * get info of module on view update, create in view web
     * @return array
     */
    public function getArrayDetailForAjax(){
        $aData                  = [];
        $aData['module_id']     = $this->id;
        $aData['module_type']   = $this->type;
        $aData['module_name']   = $this->name;
        $mAppCache              = new AppCache();
        $aMaterial              = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
        $aGasMaterialsType      = GasMaterialsType::getAllItem();
        if(!empty($this->rDetail)){
            foreach($this->rDetail as $item){
                $aDetail                        = [];
                $aDetail['materials_id']        = $item->materials_id;
                $aDetail['materials_type_id']   = $item->materials_type_id;
                $aDetail['materials_type_name'] = !empty($aGasMaterialsType[$item->materials_type_id]) ? $aGasMaterialsType[$item->materials_type_id] : $item->materials_type_id;
                $aDetail['qty']                 = $item->qty;
                $aDetail['materials_name']      = isset($aMaterial[$item->materials_id]) ? $aMaterial[$item->materials_id]['name'] : '';
                $aData['detail'][]              = $aDetail;
            }
        }
        return $aData;
    }
    
    /** @Author: NamNH Dec 21, 2018
     *  @Todo: get view for api material in modules
     * $aModulesDetail => array model  ModulesDetail
     **/
    public function getDetailModule($aModulesDetail){
        $aResult = [];
        $mAppCache              = new AppCache();
        $aMaterial              = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
        $aGasMaterialsType      = GasMaterialsType::getAllItem();
        foreach ($aModulesDetail as $key => $mModulesDetail) {
            $aDetail                        = [];
            $aDetail['materials_id']        = $mModulesDetail->materials_id;
            $aDetail['materials_type_id']   = $mModulesDetail->materials_type_id;
            $aDetail['materials_type_name'] = !empty($aGasMaterialsType[$mModulesDetail->materials_type_id]) ? $aGasMaterialsType[$mModulesDetail->materials_type_id] : $mModulesDetail->materials_type_id;
            $aDetail['qty']                 = $mModulesDetail->qty;
            $aDetail['materials_name']      = isset($aMaterial[$mModulesDetail->materials_id]) ? $aMaterial[$mModulesDetail->materials_id]['name'] : '';
            $aResult[] = $aDetail;
        }
        return $aResult;
    }
}