<?php

class HrWorkSchedule extends CActiveRecord {

    public $status_new              = 1;
    public $status_approved         = 2;
    public $status_cancel           = 3;
    public $status_required_update  = 4;
    public $autocomplete_employee   = '';
    public $search_work_schedule , $datePlanFrom, $datePlanTo, $role, $approver ,$search_province, $search_position, $autocomplete_name, $autocomplete_name_2; //workScheduleUI
    public $is_view_sum = false;
    public $manager_name;
    public $factor;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return '{{_hr_work_schedule}}';
    }

    public function rules() {
        return array(
            array('work_day, work_shift_id, work_plan_id, employee_id, status', 'required', 'on' => 'create,update'),
            array('work_day, work_shift_id, work_plan_id, employee_id, status, manager_name', 'safe'),
        );
    }

    public function relations() {
        return array(
            'rWorkShift' => array(self::BELONGS_TO, 'HrWorkShift', 'work_shift_id'),
            'rWorkPlan' => array(self::BELONGS_TO, 'HrWorkPlan', 'work_plan_id'),
            'rEmployee' => array(self::BELONGS_TO, 'Users', 'employee_id'),
            'rManager' => array(self::BELONGS_TO, 'Users', 'manager_name'), // relation for autocomplete
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'work_day' => 'Ngày làm việc',
            'work_shift_id' => 'Ca làm việc',
            'work_plan_id' => 'Kế hoạch làm việc',
            'employee_id' => 'Nhân viên',
            'status' => 'Trạng thái',
            'approver' => 'Người duyệt',
            'manager_name' => 'Tên KTKV, GS'
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        if (!empty($this->work_day)) {
            $criteria->addCondition('t.work_day =' . $this->work_day);
        }
        if (!empty($this->work_shift_id)) {
            $criteria->addCondition('t.work_shift_id =' . $this->work_shift_id);
        }
        if (!empty($this->work_plan_id)) {
            $criteria->addCondition('t.work_plan_id =' . $this->work_plan_id);
        }
        if (!empty($this->employee_id)) {
            $criteria->addCondition('t.employee_id =' . $this->employee_id);
        }
        $sort = new CSort();
        $sort->attributes = array(
            'employee_id' => 'employee_id',
        );
        $sort->defaultOrder = 't.id desc';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));
    }

    public function getArrayStatus() {
        return array(
            $this->status_new               => 'Mới',
            $this->status_approved          => 'Đã chấp nhận',
            $this->status_cancel            => 'Hủy',
            $this->status_required_update   => 'Yêu cầu cập nhật',
        );
    }

    public function getEmployee() {
        $mUser = Users::model()->findByPk($this->employee_id);
        if(empty($mUser)) return '';
        return $mUser->attributes['first_name'];
    }

    public function getWorkDay() {
        return isset($this->rWorkDay) ? $this->rWorkDay->day : "";
    }
    
    public function getWorkShift() {
        $aWorkShift = HrWorkShift::model()->getArrayWorkShift();
        return isset($aWorkShift[$this->work_shift_id]) ? $aWorkShift[$this->work_shift_id] : "";
    }

    public function getWorkPlan() {
        $aWorkPlan = HrWorkPlan::model()->getArrayWorkPlan();
        return isset($aWorkPlan[$this->work_plan_id]) ? $aWorkPlan[$this->work_plan_id] : "";
    }

    public function getStatus() {
        $aStatus = $this->getArrayStatus();
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : '';
    }
    
    /** @Author: ANH DUNG Oct 19, 2018
     *  @Todo: check user can view WorkSchedule
     **/
    public function canViewWorkSchedule($role_id) {
        $cUid = MyFormat::getCurrentUid();
        $aUidAllow = [GasConst::UID_CHAU_LNM, GasConst::UID_ADMIN];
        if(in_array($cUid, $aUidAllow)){
            return true;
        }
        return true;
        
        $aUidViewCallCenter = [GasConst::UID_ANH_LQ, GasConst::UID_PHUONG_NT];
        $aRoleCallCenter    = [ROLE_CALL_CENTER, ROLE_DIEU_PHOI];
        if(in_array($role_id, $aRoleCallCenter) && !in_array($cUid, $aUidViewCallCenter)){
            return false;
        }elseif(!in_array($role_id, $aRoleCallCenter) && in_array($cUid, $aUidViewCallCenter)){
            return false;
        }
        return true;
    }
    
    public function buildHtmlWorkSchedule() {
        $aData = [];
        if(!$this->canViewWorkSchedule($role = 1)){
            return '';
        }
        
        $criteria = new CDbCriteria;
        $criteria->compare('t.status', HrWorkShift::STATUS_ACTIVE);
        $criteria->order = "name ASC";
        $aData['shift'] = HrWorkShift::model()->findAll($criteria);
        
        $dateFrom    = MyFormat::dateConverDmyToYmd($this->datePlanFrom, '-'); // Ymd date format -------------
        $dateTo      = MyFormat::dateConverDmyToYmd($this->datePlanTo, '-'); // Ymd date format -------------
        
        // Search user
        $aData['object_user'] = $this->getArrayUserHr();
//        uasort($aData['object_user'], function($a, $b){
//            // Compare utf8 string
//            $str1 = iconv('utf-8', 'ascii//TRANSLIT', $a->first_name);
//            $str2 = iconv('utf-8', 'ascii//TRANSLIT', $b->first_name);
//            return $str1 > $str2;
//        });
//        if(!empty($this->employee_id)){
//            $aData['object_user'][] = Users::model()->findByPk($this->employee_id);
//        } elseif(isset($this->search_position) && isset($this->search_province)) {
//            $sPosition = implode(",", $this->search_position);
//            $sProvince = implode(",", $this->search_province);
//            $crtUser = new CDbCriteria;
//            $crtUser->join = 'JOIN gas_users_profile p ON t.id = p.user_id AND p.position_work IN (' . $sPosition . ')';
//            $crtUser->addCondition('t.province_id IN ('. $sProvince . ')');
//            $crtUser->addCondition('t.status=' . STATUS_ACTIVE);
//            $crtUser->addCondition('t.payment_day=1');// Oct018 get User có trả lương
//            $crtUser->order = "t.first_name asc";
//            $aData['object_user'] = Users::model()->findAll($crtUser);
//        }

        $aData['list_id_user'] = [];
        if(!empty($aData['object_user'])){
            foreach ($aData['object_user'] as $mUser) {
                $aData['list_id_user'][$mUser->id] = $mUser->id;
            }
        }

        // search schedule
        $criteria=new CDbCriteria;     
        $criteria->addCondition('work_day BETWEEN "'.$dateFrom.'" AND "'.$dateTo.'"');
        $criteria->addInCondition('employee_id', $aData['list_id_user']);
        $criteria->order = "employee_id DESC";
        $mWorkSchedule = HrWorkSchedule::model()->findAll($criteria);

        //organize data as custom arr
        $aData['schedule']    = [];
        $aData['sum_column']  = [];
        $aData['sum_row']     = [];
        $aData['info']        = HrSalaryReports::model()->getArrayProfile($aData['list_id_user']);
        
        $aData['date'] = new DatePeriod(
             new DateTime($dateFrom),
             new DateInterval('P1D'),
             new DateTime(date('Y-m-d', strtotime('+1 day', strtotime($dateTo))))
        ); //number dates between 2 dates
        
        $aSumShiftShow = ["H1", "H2", "H3"];
        if(!empty($mWorkSchedule)){
            foreach ($mWorkSchedule as $dateSchedule) {
                $shiftInfo = isset($dateSchedule->rWorkShift) ? $dateSchedule->rWorkShift->attributes : '';
                $shiftInfo['employee'] = $dateSchedule->employee_id;
                $indexOfArr = date('dm', strtotime($dateSchedule->work_day));
                
                /* $aSchedule[ work day (datemonth [ex 0107]) ] [ employee id ] [] */
                $aData['schedule'][$indexOfArr][$dateSchedule->employee_id][] = $shiftInfo; 
                // Sum theo nhân viên (mở lúc test) DuongNV close Nov 1,2018
                if($this->is_view_sum){
                    if(isset($aData['sum_column'][$dateSchedule->employee_id][$shiftInfo['id']])){
                        $aData['sum_column'][$dateSchedule->employee_id][$shiftInfo['id']]['num'] += 1 * $shiftInfo['factor'];
                    } else {
                        $aData['sum_column'][$dateSchedule->employee_id][$shiftInfo['id']]['num']  = 1 * $shiftInfo['factor'];
                        $aData['sum_column'][$dateSchedule->employee_id][$shiftInfo['id']]['name'] = $shiftInfo['name'];
                    }
                    
                    if(isset($aData['sum_off'][$dateSchedule->employee_id])){
                        $aData['sum_off'][$dateSchedule->employee_id]--;
                    } else {
                        $aData['sum_off'][$dateSchedule->employee_id]  = iterator_count($aData['date'])-1; // Number day of month
                    }
                }
                // Sum theo ngày (table footer của lịch làm việc)
                if(in_array($shiftInfo['name'], $aSumShiftShow)){
                    if(isset($aData['sum_row'][$shiftInfo['id']]['num'][$dateSchedule->work_day])){
                        $aData['sum_row'][$shiftInfo['id']]['num'][$dateSchedule->work_day] += 1 * $shiftInfo['factor'];
                    } else {
                        $aData['sum_row'][$shiftInfo['id']]['num'][$dateSchedule->work_day]  = 1 * $shiftInfo['factor'];
                        $aData['sum_row'][$shiftInfo['id']]['name'] = $shiftInfo['name'];
                    }
                }
            }
        }
        
        // Get array leave day
        $mGasLeave = new GasLeave();
        $mGasLeave->leave_date_from = $dateFrom;
        $mGasLeave->leave_date_to   = $dateTo;
        $aData['leave']             = $mGasLeave->getTotalInRangeDate(); // [uid=>count]
        $aData['leave_date']        = $mGasLeave->getArrayLeaveDate();   // [uid=>['Y-m-d'=>'Y-m-d']]
        
        // Get array holiday in year
        $mHoliday                   = new GasLeaveHolidays();
        $mHoliday->date             = $dateFrom;
        $aData['holiday']           = $mHoliday->getHolidaysInYear(); // ['Y-m-d'=>'Y-m-d']
        
        if($this->canViewMonitor()){
            $mGasTargetMonthly                  = new GasTargetMonthly();
            $mGasTargetMonthly->date_from_ymd   = $dateFrom;
            $aGasOneMany                        = $mGasTargetMonthly->getArrayOneManyMonitorCcs();
            $aMonitorTelesale                   = $mGasTargetMonthly->getArrayTelesaleMonitor();
            $aMonitorId                          = [];
            foreach ($aGasOneMany as $mOneMany) {
                if(in_array($mOneMany->many_id, $aMonitorTelesale)){
                    continue;
                }
                $aData['employee_monitor'][$mOneMany->many_id] = $mOneMany->one_id;
                $aMonitorId[$mOneMany->one_id] = $mOneMany->one_id;
            }
            $aData['model_monitor'] = Users::getArrayModelByArrayId($aMonitorId);
        }
        
        return $aData;
    }    

    public function getArrayTabs() {
        $aRes = array();
        $mSReport = new HrSalaryReports();
        $aRoles = $mSReport->getArrayRoleSalary();
        $i=0;
        
        foreach ($aRoles as $key => $role) {
            $i++;
            $aRes[$i]['id'] = 'tab'.$i;
            $aRes[$i]['label'] = $role;
            $aRes[$i]['content'] = '';
            $aRes[$i]['linkOptions'] = array('class'=>'tabLink', 'data-role_id'=>$key);
            if($i==1){
                $aRes[$i]['active'] = true;
            }
        }
        return $aRes;
    }
    
    /*
     * @author: DuongNV 27/07/2018
     * Delete schedule by role and date from, date to of work plan
     */
    public function deleteScheduleByRoleAndDate() {
        $dateFromYmd = MyFormat::dateConverDmyToYmd($this->datePlanFrom, '-');
        $dateToYmd = MyFormat::dateConverDmyToYmd($this->datePlanTo, '-');
        $aUsersInRole = Users::getArrIdUserByRole($this->role);
        $criteria = new CDbCriteria;
        $criteria->addCondition("work_day BETWEEN '{$dateFromYmd}' AND '{$dateToYmd}'");
        $criteria->addInCondition('employee_id', $aUsersInRole);
        $mWorkSchedule = self::findAll($criteria);
        if(empty($mWorkSchedule)) return false;
        $idDelete = [];
        foreach ($mWorkSchedule as $schedule) {
            $idDelete[] = $schedule->id;
        }
        $tableName = self::tableName();
        $sql = 'DELETE FROM ' . $tableName . ' WHERE id IN ('. implode(',', $idDelete) . ')';
        Yii::app()->db->createCommand($sql)->execute();
    }
    
    /*
     * @author: DuongNV 27/07/2018
     * Delete schedule by  date from, date to of work plan and list user
     * @params: from, to : d-m-Y
     */
    public function deleteOldSchedule($aUser, $from, $to) {
        $dateFromYmd = MyFormat::dateConverDmyToYmd($from, '-');
        $dateToYmd = MyFormat::dateConverDmyToYmd($to, '-');
        $criteria = new CDbCriteria;
        $criteria->addCondition("work_day BETWEEN '{$dateFromYmd}' AND '{$dateToYmd}'");
        $criteria->addInCondition('employee_id', $aUser);
        $mWorkSchedule = self::findAll($criteria);

        if(empty($mWorkSchedule)) return false;
        $idDelete = [];
        foreach ($mWorkSchedule as $schedule) {
            $idDelete[] = $schedule->id;
        }
        $tableName = self::tableName();
        $sql = 'DELETE FROM ' . $tableName . ' WHERE id IN ('. implode(',', $idDelete) . ')';
        Yii::app()->db->createCommand($sql)->execute();
    }

    /**
     * Get number of work schedule days
     * @param String $from Date from (format is DATE_FORMAT_4 - 'Y-m-d')
     * @param String $to Date to (format is DATE_FORMAT_4 - 'Y-m-d')
     * @return Int Number of work schedule days
     */
    public static function getNumberOfWorkScheduleDays($from, $to, $employee_id, $type = '') {
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition("t.work_day", $from, $to);
        $criteria->compare("t.employee_id", $employee_id);
        if (!empty($type)) {
            $tblName = HrWorkShift::model()->tableName();
            $criteria->join = ' JOIN ' . $tblName . ' ws ON ws.id = t.work_shift_id';
            $criteria->compare('ws.type', $type);
            $criteria->select = 'DISTINCT t.*';
        }
//        $sql = "SELECT * FROM gas_hr_work_schedule ws JOIN gas_hr_work_shift as wsh"
//                . " ON ws.work_shift_id = wsh.id"
//                . " WHERE (ws.workday BETWEEN {$from} AND {$to}) AND (ws.employee_id = {$employee_id})"
//                . " AND (wsh.type = $type)";
        $count = self::model()->count($criteria);
        Logger::WriteLog('Result join table: ' . $count);
        return $count;
    }
    
    /**
     * @author: DuongNV 25/07/2018
     * check schedule is exists
     * params: employee_id and work_day
     * return model if exists, else return false
     */
    public function checkExistsSchedule(){
        $criteria = new CDbCriteria;
        $criteria->compare('t.work_day', $this->work_day);
        $criteria->compare('t.employee_id', $this->employee_id);
        $model = self::find($criteria);
        return empty($model) ? false : $model;
    }
    
    /**
     * @author: DuongNV 25/07/2018
     * create workshift form params
     */
    public function createRowValueInsert($emp_id, $date, $shift_id) {
        $plan_id = 0;
        if(is_array($emp_id)){ // multiple employee
            $aObjectUser = Users::getArrObjectUserByRole("", $emp_id);
            $aRowInsert  = [];
            foreach ($emp_id as $eid) {
                $role_id = $aObjectUser[$eid]->role_id;
                $aRowInsert[] = "(NULL,
                                '{$date}',
                                '{$shift_id}',
                                '{$plan_id}', 
                                '{$role_id}', 
                                '{$eid}', 
                                '{$this->status_new}'
                                )";
                                    
            }
        } else { // one employee
            $role_id = Users::model()->findByPk($emp_id)->role_id;
            $aRowInsert[] = "(NULL,
                            '{$date}',
                            '{$shift_id}',
                            '{$plan_id}', 
                            '{$role_id}', 
                            '{$emp_id}', 
                            '{$this->status_new}'
                            )";
        }
        return implode(',',$aRowInsert);
    }
    
    /** @Author: DuongNV Sep 28, 2018
     *  @Todo: get array user manager
     **/
    public function getArrayManager() {
        // role => user_id (user quản lý role chỉ sửa dc role mà mình quản lý)
        return [
            3  => 2, // Test (NVKD => admin)
            46 => 2
        ];
    }
    
    /** @Author: DuongNV Oct 2, 2018
     *  @Todo: get array role officer
     **/
    public function getArrayRoleOfficer() {
        return [
            ROLE_ACCOUNTING,
            ROLE_TELESALE,
            ROLE_SALE
        ];
        
    }
    
    public function getArrayUIDSchedule() {
        return [
//            GasConst::UID_NHAN_NTT,
            GasConst::UID_PHUONG_NT,
//            GasConst::UID_ANH_LQ,
            GasConst::UID_HANH_NT,
            GasConst::UID_HIEU_PT,
        ];
    }
    
    /** @Author: DuongNV Mar 4, 19
     *  @Todo: array uid được chỉnh sửa lịch làm việc từ ngày 1 đến ngày 5
     **/
    public function getArrayUIDScheduleAllDays() {
        return [
            1436021, // UID DuongNV
            1979734, // UID UITDuongNV
            GasConst::UID_ADMIN,
            GasConst::UID_CHAU_LNM,
        ];
    }
    
    /** @Author: DuongNV
     *  @Todo: Có thể tự động tạo lịch hay k
     **/
    public function canAutoSchedule() {
        $firstDayOfMonth    = date('Y-m-01');
        $destinationDate    = MyFormat::dateConverDmyToYmd($this->datePlanFrom, "-");
        $cUid               = MyFormat::getCurrentUid();
        $aAllowAllDays      = $this->getArrayUIDScheduleAllDays();
        if(in_array($cUid, $aAllowAllDays)){ // Nếu là user trong arr thì sẽ được tạo lịch của tháng trc
            $destinationDate = MyFormat::modifyDays($destinationDate, 1, '+', 'month');
        }
        // Chỉ dc tự động tạo lịch từ tháng hiện tại (hoặc tháng trc) trở về sau
        if(MyFormat::compareTwoDate($firstDayOfMonth, $destinationDate)){
            return false;
        }
        return $this->canEditSchedule();
    }
    
    /** @Author: DuongNV
     *  @Todo: Có thể chỉnh sửa lịch làm việc hay k
     **/
    public function canEditSchedule() {
        $cUid               = MyFormat::getCurrentUid();
        $aAllow5Days        = $this->getArrayUIDSchedule();
        $aAllowAllDays      = $this->getArrayUIDScheduleAllDays();
        if(in_array($cUid, $aAllow5Days)){
            $cDate = date('Y-m-d');
            // Destination date là ngày cuối cùng được chỉnh sửa lịch làm việc
//            $dDate = date('Y-m-06', strtotime('+1 month', strtotime($this->datePlanFrom)));
            $nextFrom = MyFormat::modifyDays($this->datePlanFrom, 1, '+', 'month');
            $dDate = date('Y-m-06', strtotime($nextFrom));
            return MyFormat::compareTwoDate($dDate, $cDate);
        }
        return in_array($cUid, $aAllowAllDays);
    }
    
    /** @Author: DuongNV Apri 18,2019
     *  @Todo: can view date job begin, date leave in schedule
     **/
    public function canViewJobInfo() {
        $cUid   = MyFormat::getCurrentUid();
        $cRole  = MyFormat::getCurrentRoleId();
        $aAllow = [
            GasConst::UID_CHAU_LNM, 
            1436021, // UID duongnv
            1979734 // UID uitduongnv
        ];
        $aRoleAllow = [
            ROLE_ADMIN,
            ROLE_ACCOUNTING_ZONE,
            ROLE_MONITORING_MARKET_DEVELOPMENT,
        ];
        return in_array($cUid, $aAllow) || in_array($cRole, $aRoleAllow);
    }
    
    /** @Author: DuongNV Apri 18,2019
     *  @Todo: can view date job begin, date leave in schedule
     **/
    public function canDeleteSchedule() {
        $cUid   = MyFormat::getCurrentUid();
        $cRole  = MyFormat::getCurrentRoleId();
        $aAllow = [
            GasConst::UID_CHAU_LNM, 
            1436021, // UID duongnv
            1979734 // UID uitduongnv
        ];
        $aRoleAllow = [
            ROLE_ADMIN,
        ];
        return in_array($cUid, $aAllow) || in_array($cRole, $aRoleAllow);
    }
    
    
    /** @Author: DuongNV
     *  @Todo: can view monitor info in schedule
     **/
    public function canViewMonitor() {
        if(empty($this->search_position)) return false;
        $aUidAllow = [
            GasConst::UID_CHAU_LNM,
            1436021, // UID DuongNV
            1979734, // UID UITDuongNV
            2
        ];
        $aPttt  = [
            UsersProfile::WORK_ROOM_PTTT,
            UsersProfile::WORK_ROOM_CCS
        ];
        $ret    = false;
        foreach ($aPttt as $value) {
            $ret = in_array($value, $this->search_position);
            if($ret){ // Chỉ cần 1 trong 2 là dc
                break;
            }
        }
        $cUid   = MyFormat::getCurrentUid();
        $cRole  = MyFormat::getCurrentRoleId();
        $aRoleAllow = [
            ROLE_ADMIN,
            ROLE_ACCOUNTING_ZONE,
            ROLE_MONITORING_MARKET_DEVELOPMENT,
        ];
        return ($ret && in_array($cUid, $aUidAllow)) || in_array($cRole, $aRoleAllow);
    }
    
    /** @Author: DuongNV Jun 18,2019
     *  @Todo: can view position work, province
     **/
    public function canViewPosition() {
        return true; // DuongNV Jul0619 mở cho nv xem hết
        $cUid = MyFormat::getCurrentUid();
        $aAllow = [
            GasConst::UID_CHAU_LNM, 
            GasConst::UID_HANH_NT,
            1436021, // UID duongnv
            1979734 // UID uitduongnv
        ];
        return in_array($cUid, $aAllow);
    }
    
    /** @Author: DuongNV Nov 3, 18
     *  @Todo: search array user for hrWorkSchedule, hrSalaryReports, ...
     *  @return: array object user
     **/
    public function getArrayUserHr() {
        $aUser                      = [];
        if( !empty($this->employee_id) ){
            $mUser                  = UsersHr::model()->findByPk($this->employee_id);
            if($mUser){
                $aUser[$mUser->id]  = $mUser;
            }
        } else {
            // Chỉ dc empty bp hoặc ng quản lý, ko empty cả 2
            if( empty($this->search_position) && empty($this->manager_name) ){
                return [];
            }
            
            $crtUser                = new CDbCriteria;
            $crtUser->join          = 'JOIN gas_users_profile p ON t.id = p.user_id';
            if( !empty($this->search_position) ){
                $sPosition          = implode(",", $this->search_position);
                $crtUser->join      = 'JOIN gas_users_profile p ON t.id = p.user_id AND p.position_work IN (' . $sPosition . ')';
            }
            if( !empty($this->search_province) ){
                $sProvince          = implode(",", $this->search_province);
                $crtUser->addCondition('t.province_id IN ('. $sProvince . ')');
            }
            // DuongNV Aug2819 search GS/KTKV ra ds nhân viên ng đó quản lý
            if( !empty($this->manager_name) ){
                $mUser              = Users::model()->findByPk($this->manager_name);
                if( !empty($mUser) ){
                    $roleManager    = $mUser->role_id;
                    switch ($roleManager) {
                        case ROLE_MONITORING_MARKET_DEVELOPMENT:
                            $dateRun            = date('Y-m-d', strtotime($this->datePlanFrom));
                            $mTargetDaily       = new TargetDaily();
                            $aEmployee          = $mTargetDaily->getArrayEmployeeCcs($dateRun, $this->manager_name);
                            $crtUser->addInCondition('t.id', $aEmployee);
                            break;
                        case ROLE_ACCOUNTING_ZONE:
                            $mSetupTeam             = new SetupTeam();
                            $mSetupTeam->date_from  = date('Y-m-01', strtotime($this->datePlanFrom));
                            $mSetupTeam->date_to    = date('Y-m-t', strtotime($this->datePlanTo));
                            $mSetupTeam->getListAgentOfKtkv($mUser->id);
                            $aEmployee              = empty($mSetupTeam->aEmployeeOfKtkv[$mUser->id]) ? [] : $mSetupTeam->aEmployeeOfKtkv[$mUser->id];
                            $crtUser->addInCondition('t.id', $aEmployee);
                            break;

                        default:
                            break;
                    }
                    
                }
            }
            
            $crtUser->addCondition('t.status=' . STATUS_ACTIVE);
            // Filter những nhân viên nghỉ việc trước ngày bắt đầu
            if( !empty($this->datePlanFrom) ){
                $startDate          = MyFormat::dateConverDmyToYmd($this->datePlanFrom, '-');
                $crtUser->addCondition("p.leave_date = '0000-00-00' OR p.leave_date > '$startDate'");
            }
            // Filter những nhân viên chưa đi làm trc ngày kết thúc
            if( !empty($this->datePlanTo) ){
                $endDate            = MyFormat::dateConverDmyToYmd($this->datePlanTo, '-');
                $crtUser->addCondition("p.date_begin_job <= '$endDate'");
            }
            $crtUser->addCondition('t.payment_day=1');// Oct018 get User có trả lương
//            $crtUser->order = "t.province_id asc, p.position_work asc, t.first_name";
            $crtUser->order         = "p.position_work asc, t.province_id asc, t.first_name";
            $models                 = UsersHr::model()->findAll($crtUser);
            foreach ($models as $item){
                $aUser[$item->id] = $item;
            }
        }
        return $aUser;
    }
    
    /** @Author: DuongNV Nov 28, 18
     *  @Todo: Đếm số ngày công theo type
     *  @param xFactor: true => có nhân hệ số
     **/
    public function getCacheDataWorkScheduleDays($from, $to, $type = '', $xFactor = false) {
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition("t.work_day", $from, $to);
        if (!empty($type)) {
            $tblName = HrWorkShift::model()->tableName();
            $criteria->join = ' JOIN ' . $tblName . ' ws ON ws.id = t.work_shift_id';
            $criteria->compare('ws.type', $type);
            $criteria->select = 'DISTINCT t.*, ws.factor as factor';
        }
        $aData = [];
        $number = 1;
        $mSchedule = HrWorkSchedule::model()->findAll($criteria);
        foreach ($mSchedule as $item) {
            if($xFactor) $number = $item->factor;
            if(isset($aData[$item->employee_id])){
                $aData[$item->employee_id] += $number;
            } else {
                $aData[$item->employee_id] = $number;
            }
        }
        return $aData;
    }
    
    /** @Author: DuongNV Oct 4, 2018
     *  @Todo: Tự động tạo lịch làm việc
     *  @params: $date_from/to d-m-Y
     *  @idea: Mỗi ngày sẽ lấy ra n nhân viên trong array để xếp lịch, sau đó đưa n nhân viên này xuống 
     * dưới cùng để đảm bảo chia đều các ca cho nhân viên
     **/
    public function autoGenerateWorkSchedule($aEmp, $from, $to) {
        $model = new HrWorkSchedule('search');
        // get array ca làm việc và số người mỗi ca [shift_id => number_people_per_shit]
        $aShift = [];
        if( isset($_POST['ShiftAutoGenerate']['list_shift']) ){
            $aShift = array_filter($_POST['ShiftAutoGenerate']['list_shift']);
            $_SESSION['autoSchedule'] = $aShift;
        }

        // Được nghỉ CN hay không
        $is_off_sunday = isset($_POST['ShiftAutoGenerate']['is_off_sunday']) ? $_POST['ShiftAutoGenerate']['is_off_sunday'] : 0;

        if(empty($aEmp) || empty($aShift)){ // Nếu ca hoặc array NV rỗng thì báo lỗi
            return false;
        }

        // Get arr ngày làm việc (return array Datetime object)
        $aDate = new DatePeriod(
            new DateTime($from),
            new DateInterval('P1D'),
            new DateTime(date('d-m-Y', strtotime('+1 day', strtotime($to))))
        );
        
        // Nếu có cài đặt công thức cho lịch làm việc thì sẽ vào đây
        $aRowInsert = $this->handleScheduleFunction($aEmp, $from, $to);
        
        if( empty($aRowInsert) ):
            $aEmpTmp    = $aEmp;
            shuffle($aEmpTmp); // Mỗi lần tạo sẽ shuffle lại ds nhân viên

            // Nếu nhập số người mỗi ca là -1 thì sẽ phân theo kiểu dưới đây
            $forCallCenter = 0;
            $needMore = $this->hanleCallCenter($aDate, $aShift, $aEmp, $forCallCenter);  

            // Bắt đầu phân lịch
            $aRowInsert = $this->handleSchedule($aDate, $aShift, $aEmpTmp, $is_off_sunday, $needMore);
        endif;
        
        // Xóa lịch làm việc cũ
        $model->deleteOldSchedule($aEmp, $from, $to);

        $tableName = HrWorkSchedule::model()->tableName();
        $sql = 'INSERT INTO ' . $tableName . ' VALUES '. implode(',', $aRowInsert);
        Yii::app()->db->createCommand($sql)->execute();
        return true;
    }
    
    /** @Author: DuongNV Mar 19, 2019
     *  @Todo: kiểm tra công thức lịch làm việc, nếu có sẽ tạo lịch từ công thức
     *  @param $from/$to d-m-Y
     *  @param $aIdUser array id user
     **/
    public function handleScheduleFunction($aIdUser, $from, $to) {
        $aAllFunctions      = HrFunctions::loadItems();
        $aFunctionSchedule  = $aAllFunctions[UsersHr::getIdWorkSchedule()];
        $mReport            = new HrSalaryReports();
        $aProfile           = $mReport->getArrayProfile($aIdUser);
        $aUserHr            = $mReport->getArrayUsersById($aIdUser);
        $aSchedule          = [];
        $ret                = [];

        $mReport->dateFromDmy   = $from;
        $mReport->dateToDmy     = $to;
        $fromYmd                = MyFormat::dateConverDmyToYmd($from, '-');
        $toYmd                  = MyFormat::dateConverDmyToYmd($to, '-');
        $mReport->start_date    = $fromYmd;
        $mReport->end_date      = $toYmd;
        $mReport->type_id       = UsersHr::model()->getIdWorkSchedule();
        $mReport->initData();
        $aDate = new DatePeriod(
            new DateTime($from),
            new DateInterval('P1D'),
            new DateTime(date('d-m-Y', strtotime('+1 day', strtotime($to))))
        );
        foreach ($aDate as $date) :
            foreach ($aProfile as $user_id => $value) :
                if( isset($aUserHr[$user_id]) ){
                    $aUserHr[$user_id]->mSalaryReport = $mReport;
                    if($aUserHr[$user_id]->isLeaveDate($date->format('Y-m-d'))) continue;
                } else {
                    continue;
                }
                $aFunction = isset($aFunctionSchedule[$value['position_work']][$value['position_room']]) ? $aFunctionSchedule[$value['position_work']][$value['position_room']] : [];
                foreach ($aFunction as $mFunction) :
                    $aUserHr[$user_id]->hrFrom        = $fromYmd;
                    $aUserHr[$user_id]->hrTo          = $toYmd;
                    $aUserHr[$user_id]->mSalaryReport = $mReport;
                    $aSchedule[$user_id][$date->format('Y-m-d')] = empty($aSchedule[$user_id][$date->format('Y-m-d')])
                            ? $mFunction->getValue($date->format('Y-m-d'), $date->format('Y-m-d'), $aUserHr[$user_id])
                            : $aSchedule[$user_id][$date->format('Y-m-d')];
                endforeach;
                if(isset($aSchedule[$user_id])){
                    $aSchedule[$user_id] = array_filter($aSchedule[$user_id]);
                }
            endforeach;
            $aSchedule = array_filter($aSchedule);
        endforeach;

        $aShift = HrWorkShift::model()->loadItems(false);
        foreach ($aSchedule as $user_id => $value) {
            foreach ($value as $date_ymd => $shift_name) {
                $shift_id = array_search($shift_name, $aShift);
                if( $shift_id !== FALSE ){
                    $ret[] = $this->createRowValueInsert($user_id, $date_ymd, $shift_id);
                }
            }
        }
        return $ret;
    }
    
    /**
     * @author: DuongNV Nov 22,18
     * @todo: Xử lý phần tự động tạo lịch
     */
    public function handleSchedule($aDate, $aShift, $aEmpTmp, $is_off_sunday, $needMore) {
        $i = 1;
        $aRowInsert                 = [];
        $mLeave                     = new GasLeave();
        $mLeave->leave_date_from    = $aDate->start->format('Y-m-d');
        $mLeave->leave_date_to      = $aDate->end->format('Y-m-d');
        $aLeaveUser                 = $mLeave->getArrayLeaveDate();
        $mLeaveHolidays             = new GasLeaveHolidays();
        $mLeaveHolidays->date       = $aDate->start->format('Y-m-d');
        $aHoliday                   = $mLeaveHolidays->getHolidaysInYear();
        foreach ($aDate as $date) :
            // chủ nhật nghỉ
            if (($date->format('w') == 0 && $is_off_sunday) || isset($aHoliday[$date->format('Y-m-d')])) { 
                continue;
            }
            // nếu tổng số nguời làm 1 ngày = tổng số nhân viên hiện có thì trộn arr NV lại
            if(array_sum($aShift) == count($aEmpTmp)){
                shuffle($aEmpTmp);
            }

            // Mỗi ca sẽ lấy ra n nhân viên từ 0-n, n+1 tới m, m+1 tới ... của array NV $aEmpTmp
            $index = 0;
            if($i++ <= $needMore['restLow'] && $needMore['forCallCenter']){
                $index = floor($needMore['avgRestPerDay']);
            }
            if($i++ > $needMore['restLow'] && $needMore['forCallCenter']) {
                $index = ceil($needMore['avgRestPerDay']);
            }
            $nthShift = 1;
            foreach($aShift as $shift_id => $empPerShi):
                $aEmpInOneShift     = array_slice($aEmpTmp, $index, $empPerShi); // array NV trong 1 ca
                if($nthShift++ == count($aShift) && $needMore['forCallCenter']){
                    $aEmpInOneShift = array_slice($aEmpTmp, $index, count($aEmpTmp)-$index); // array NV trong 1 ca
                }
                // Nếu ngày đó nhân viên có phép thì không thêm vào lịch làm việc
                foreach ($aEmpInOneShift as $key => $user_id) {
                    if(isset($aLeaveUser[$user_id][$date->format('Y-m-d')])){
                        unset($aEmpInOneShift[$key]);
                    }
                }
                $aRowInsert[]       = $this->createRowValueInsert($aEmpInOneShift, $date->format('Y-m-d'), $shift_id);
                $index += $empPerShi;
            endforeach;
            // Sau 1 ngày thì cho ds nhân viên đã làm trong ngày này xuống dưới cùng của array NV $aEmpTmp
            $aEmpInThisDay = array_slice($aEmpTmp, 0, $index); // get array NV làm ngày này
            $aAnotherEmp   = array_diff($aEmpTmp, $aEmpInThisDay); // get aray NV không làm ngày ngày
            $aEmpTmp       = array_merge($aAnotherEmp, $aEmpInThisDay); // đưa nhân viên không làm lên trên cùng
        endforeach;
        return $aRowInsert;
    }
    
    /**
     * @author: DuongNV Nov 22,18
     * @todo: Xử lý các tham số cho call center
     */
    public function hanleCallCenter($aDate, &$aShift, $aEmp, &$forCallCenter){
        $avgRestPerDay = 0; $restLow = 0;
        if(array_sum($aShift) < 0){
            $forCallCenter  = 1;
            $numSunday      = 0;
            $numDate        = 0;
            $numEmp         = count($aEmp);
            $numShift       = count($aShift);
            foreach ($aDate as $date) :
                if ($date->format('w') == 0) { 
                    $numSunday++;
                }
                $numDate++;
            endforeach;
            $avgRestPerDay = $numSunday*$numEmp/$numDate; // trung bình Số người nghỉ 1 ngày
            $restLow   = floor( (ceil($avgRestPerDay) - $avgRestPerDay)*$numDate ); //Số ngày nghỉ floor($avgRestPerDay)
            $empPerShi = floor( ($numEmp - floor($avgRestPerDay)) / $numShift ); //Maximum employee per shift

            foreach ($aShift as $key => $value) {
                $aShift[$key] = $empPerShi;
            }
        }
        return [
            'restLow'       => $restLow,
            'forCallCenter' => $forCallCenter,
            'avgRestPerDay' => $avgRestPerDay
            ];
    }
    
    public function getArrayCustomSchedule(){
        return [
            UsersProfile::WORK_ROOM_PVKH,
            UsersProfile::WORK_ROOM_PTTT,
            UsersProfile::WORK_ROOM_CCS,
            UsersProfile::WORK_ROOM_TELESALE
        ];
    }
    
    /** @Author: DuongNV Apr 24,19
     *  @Todo: cron lịch làm việc cho bp BP PVKH, PTTT, CCS, Telesale
     *  @params: $date Y-m-d
     **/
    public function cronCustomSchedule($date = '') {
        $cDate = empty($date) ? date('d-m-Y') : MyFormat::dateConverYmdToDmy($date, 'd-m-Y');
//        $from = '01-05-2019';$to = '17-05-2019';
        $aPositionAllow             = $this->getArrayCustomSchedule();
//        $cDate                      = date('d-m-Y');
        $mSchedule                  = new HrWorkSchedule();
        $mSchedule->search_position = $aPositionAllow;
//        $mSchedule->search_position = [UsersProfile::WORK_ROOM_PTTT,UsersProfile::WORK_ROOM_CCS];
        $aIdUser                    = array_keys($mSchedule->getArrayUserHr());
        $aRowInsert                 = $this->handleScheduleFunction($aIdUser, $cDate, $cDate);
//        $aRowInsert                 = $this->handleScheduleFunction($aIdUser, $from, $to);
        
        // Xóa lịch làm việc cũ
        $mSchedule->deleteOldSchedule($aIdUser, $cDate, $cDate);
//        $mSchedule->deleteOldSchedule($aIdUser, $from, $to);
        
        $tableName              = HrWorkSchedule::model()->tableName();
        $sql                    = 'INSERT INTO ' . $tableName . ' VALUES '. implode(',', $aRowInsert);
        Yii::app()->db->createCommand($sql)->execute();
        
        // Send log
        $needMore['title']      = 'Cron schedule';
        $needMore['list_mail']  = ['duongnv@spj.vn'];
//        $strLogData             = preg_replace('/\s+/', '', json_encode($aRowInsert));
        $numRow                 = count($aRowInsert);
        $infoLog                = "Cron auto generate work schedule at ".date('d/m/Y H:i:s')."<br><br>$numRow rows inserted.";
//        SendEmail::bugToDev($infoLog, $needMore);
        $strLog = '******* cronCustomSchedule done at '.date('d-m-Y H:i:s');
        Logger::WriteLog($strLog);
    }
    
}
