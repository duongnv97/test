<?php

/**
 * This is the model class for table "{{_hr_salary_report}}".
 *
 * The followings are the available columns in table '{{_hr_salary_report}}':
 * @property integer $id
 * @property string $start_date
 * @property string $end_date
 * @property string $name
 * @property integer $type_id
 * @property integer $position_work
 * @property string $data
 * @property string $created_date
 * @property integer $created_by
 * @property integer $status
 * @property string $approved_by
 * @property string $approved_date
 * @property string $notify
 */
class HrSalaryReports extends BaseSpj
{
    // Jul0618 AnhDung define các biến (sản lượng, cuộc gọi, điểm làm việc ) sử dụng để tính lương, các biến này tối ưu query vào db ở mức thấp nhất
    public $aDaysLoop               = []; // array days để lặp khi tính toán
    public $cacheHrFunction         = []; // cache all model HrFunction
    public $cacheUsersProfile       = []; // cache all model UsersProfile -> get position_work, position_room
    public $cacheSchedule           = []; // cache lịch làm việc của nhân viên
    public $cacheLeave              = []; // cache lịch nghỉ phép của nhân viên
    public $cacheArrLeaveDate       = []; // cache lịch nghỉ phép của nhân viên
    public $cacheCalculated         = []; // cache lấy từ những bảng lương đã tính
    
//    public $cachePtttWorkingPoint   = [];// cache điểm làm việc pttt trong range date
//    public $cachePtttGasBack        = [];// cache BQV pttt trong range date
    public $cachePttt               = [];// Feb1319 cache data pttt Point/App/Bqv trong range date
    
    public $cacheCallCenterCallHgdOneDay       = [];// Tổng số lượng cuộc gọi vào HGD theo từng ngày
    public $cacheCallCenterCallBoMoiOneDay     = [];// Tổng số lượng cuộc gọi vào bò mối theo từng ngày
    public $cacheCallCenterCallInHgd        = [];// Tổng số lượng cuộc gọi vào HGD CallIn 
    public $cacheCallCenterSellHgd          = [];// Tổng số lượng các đơn hàng HGD + status
    public $cacheCallCenterCallInBoMoi      = [];// Tổng số lượng cuộc gọi vào BO MOI
    public $cacheCallCenterOrderBoMoi       = [];// Tổng số lượng các đơn hàng BO MOI + status
    public $cacheTelesaleCallOut            = [];// Tổng số lượng cuộc gọi ra CallOut
    public $cacheTelesaleCallOutByDate      = [];// Tổng số lượng cuộc gọi ra CallOut - theo từng ngày
    public $cacheTelesaleCode               = [];// Tổng số lượng KH nhập mã code Telesale 
    public $cacheTelesaleSell               = [];// Tổng số lượng BQV (đơn hàng hoàn thành)
    public $cacheTelesaleRevenue            = [];// Doanh thu trên BQV
    public $cacheTelesaleSellHaveCallOut        = [];// BQV có cuộc gọi
    public $cacheTelesaleCodeHaveCallOut        = [];// Mã code có cuộc gọi
    public $cacheTelesaleCodeNotCallOut         = [];// Mã code không có cuộc gọi
    public $cacheTelesaleRevenueHaveCallOut     = [];// Doanh thu trên BQV có cuộc gọi
    public $cacheTelesaleRevenueNotCallOut      = [];// Doanh thu trên BQV khong có cuộc gọi
    
    public $cacheCompleteOrderHGDPvkh   = []; // Số đơn hàng hoàn thành (HGD) pvkh
    public $cacheCompleteOrderBoMoiPvkh = []; // Số đơn hàng hoàn thành (Bò mối) pvkh
    
    public $cachePvkhOutput             = [];// All sản lượng gas + vật tư của pvkh 
    public $cachePvkhGroupByPrice       = [];// Param lưu group các vật tư cùng đơn giá Day10k, Day20k, Bep20k, Bep40k, Bep60k
    public $cachePvkhCredit             = [];// info pvkh thế chân
    public $cacheLeaveNormal            = [];// array phép normal < 12 day quota
    public $cacheLeaveWithoutSalary     = [];// array phép không lương > 12 day quota
    public $aHolidaysInYear             = [];// array ngày nghỉ lễ trong năm
    public $aBaseSalary                 = [];// array all Base Salary user
    public $aSalaryInsurance            = [];// array all SalaryInsurance user
    public $cacheAgentOfUser            = [];// array các agent mà 1 NV quản lý
    public $cachePunishUser             = [];// array các khoản phạt của 1 NV
    public $cacheDebts                  = []; // array công nợ của nhân viên
    
    public $cacheTarget                 = [];// array Số công để tính chỉ tiêu
    public $cacheFoneTarget             = [];// array Ngày công CG - Telesale
    public $cacheHaftTarget             = [];// array Số ngày công 1/2 chỉ tiêu
    public $cacheQtyCam                 = [];// AnhDung Jan0719 sản lượng bình cam NV tổng đài 
    public $cacheCommonDebts            = [];// DuongNV Jul1919 cache dùng chung cho công nợ
    public $cacheArrModelHoliday        = [];// DuongNV Oct0719 Cach list model GasLeaveHoliday
    
    public $aDebtsMonthly               = [];//DuongNV Jul3119 Biến công nợ(30% lương kỳ 2) để thêm vào bên qlcn 
    
//    public more here       = [];// cache BQV pttt trong range date
    public $dateFromDmy, $dateToDmy; // d-m-Y for calculating salary reports 
    public $aIdUser, $isInitData = false; // Để init data 1 lần khi tính nhiều bp
    public $search_month, $search_year, $isCalculateAll = false, $MAX_ID;
    public $search_position, $search_province;

    const DATE_SALARY_START     = '2019-07-01';
    const DATE_BLOCK_PREPAID    = 24; // (2 digit, ex 01) Sau ngày 24 (tức ngày 25) thì sẽ khóa tạm ứng
    
    const ROLE_ALL_NAME = 'Chung';
    
    const STT_CALCULATING           = 1; // Đang tính
    const STT_REQUEST               = 2; // Đang yêu cầu duyệt (htai chỉ sử dụng ở tab thu nhập)
    const STT_APPROVED              = 3; // Đã duyệt
    const STT_FINAL                 = 4; // Bảng lương đã chốt
    const STT_COMPLETE              = 5; // Bảng lương đã chi
    
    const LIMIT_DATE_COMPLETE_PROFILE   = 30;
    const REDUCE_MONTHLY_DEBTS          = 0.3; // DuongNV Jul3119 trừ công nợ = 30% lương kỳ 2
    const REDUCE_MONTHLY_DEBTS_HIGH     = 0.5; // DuongNV Aug0519 trừ công nợ = 50% lương kỳ 2, dv những nv lương trên 20tr
    const ID_FUNCTION_TERM_2_SALARY     = 915; // ID công thức của lương kỳ 2
    const ID_FUNCTION_TOTAL_SALARY      = 909; // ID công thức tổng thu nhập (30% lương gán vào công nợ)
    
    public $aStatus = [
        self::STT_CALCULATING       => 'Đang tính',
        self::STT_REQUEST           => 'Đang gửi yêu cầu',
        self::STT_APPROVED          => 'Đã duyệt',
        self::STT_FINAL             => 'Đã chốt',
        self::STT_COMPLETE          => 'Đã hoàn thành',
    ];
    
    const KEY_TAB           = 'tb'; // tab
    const KEY_CT            = 'ct'; // công thức
    const KEY_TS            = 'ts'; // tham số
    const KEY_TONG          = 'tg'; // cột tổng
    const KEY_NHANVIEN      = 'nv'; // cột nhân viên
    const KEY_POS_ROOM      = 'cv'; // cột chức vụ
    const KEY_POSITION      = 'bp'; // cột bp làm việc
    const KEY_PROVINCE      = 'ti'; // cột tỉnh
    const KEY_BEGIN_JOB     = 'bj'; // cột ngày vào làm
    const KEY_END_JOB       = 'ej'; // cột ngày nghỉ
    const KEY_HEADER        = 'header'; // header
    const KEY_BASE_SALARY   = 'ct943'; // Lương căn bản
    const KEY_DEBTS_MONTHLY = 'ct964'; // 30% công nợ trừ hàng tháng
    const KEY_CAN_PAID      = 'cp'; // Đủ dk trả lương ? (đủ hồ sơ)
    const KEY_NOTE          = 'no'; // Ghi chú
    const KEY_MONITOR       = 'mn'; // Giám sát/chuyên viên
    const KEY_TERM2_SALARY  = 'ct915'; // lương kỳ 2
    const KEY_TERM1_WORKDAY = 'ts95'; // Tổng ngày công (ngày 1-19)
    
    public static $aColHide = [
        self::KEY_BASE_SALARY,
        self::KEY_DEBTS_MONTHLY,
    ];
    
    public static $aReportKeyInfo = [
        HrSalaryReports::KEY_TAB           => 'Tab', // tab
        HrSalaryReports::KEY_CT            => 'Công thức', // công thức
        HrSalaryReports::KEY_TS            => 'Tham số', // tham số
        HrSalaryReports::KEY_TONG          => 'Tổng', // cột tổng
        HrSalaryReports::KEY_NHANVIEN      => 'Nhân viên', // cột nhân viên
        HrSalaryReports::KEY_POS_ROOM      => 'Chức vụ', // cột chức vụ
        HrSalaryReports::KEY_POSITION      => 'BP', // cột bp làm việc
        HrSalaryReports::KEY_PROVINCE      => 'Tỉnh', // cột tỉnh
        HrSalaryReports::KEY_BEGIN_JOB     => 'Ngày vào làm', // cột ngày vào làm
        HrSalaryReports::KEY_END_JOB       => 'Ngày nghỉ', // cột ngày nghỉ
        HrSalaryReports::KEY_HEADER        => 'Header', // header
        HrSalaryReports::KEY_CAN_PAID      => 'Trả lương', // Đủ dk trả lương ? (đủ hồ sơ)
        HrSalaryReports::KEY_NOTE          => 'Ghi chú', // Ghi chú
        HrSalaryReports::KEY_MONITOR       => 'Chuyên viên', // Giám sát/chuyên viên
    ];

    /** @Author: ANH DUNG Jul 12, 2018
     *  @Todo: query hết 1 lần dữ liệu để tính công, sản lượng của NV
     *   và khởi tạo vào các biến global của model
     * start_date, end_date is Y-m-d
     **/
    public function initData() {
        if(empty($this->dateFromDmy) || empty($this->dateToDmy)){
            $this->dateFromDmy  = MyFormat::dateConverYmdToDmy($this->start_date, 'd-m-Y');
            $this->dateToDmy    = MyFormat::dateConverYmdToDmy($this->end_date, 'd-m-Y');
        }
        $mHrData = new HrData();
        $this->cacheHrFunction     = HrFunctions::model()->loadItems();
        $this->cacheUsersProfile   = $this->getArrayProfile([], true);
        $this->cacheSchedule       = $this->getCacheSchedule();
        $this->aDaysLoop           = MyFormat::getArrayDay($this->start_date, $this->end_date);
        $mGasLeave                 = new GasLeave();
        $mGasLeave->leave_date_from= $this->start_date;
        $mGasLeave->leave_date_to  = $this->end_date;
        $this->cacheLeave          = $mGasLeave->getTotalInRangeDate();
        $this->cacheArrLeaveDate   = $mGasLeave->getArrayLeaveDate();
        
        $mHoliday                   = new GasLeaveHolidays();
        $mHoliday->date             = $this->start_date;
        $this->cacheArrModelHoliday = $mHoliday->getModelHolidaysInYear();
                
        // get cachePtttWorkingPoint, cachePtttGasBack
        $mHrData->getPttt($this);
        $mHrData->getOutputCallCenter($this);
        $mHrData->getOutputTelesale($this);
        $mHrData->getOutputEmployeeMaintain($this);
        $mHrData->getLeave($this);
        $mHrData->getBaseSalary($this);
        $mHrData->getPunishUser($this);
        $mHrData->getAgentOfUser($this);
        $mHrData->getParameterForTarget($this);
        $mHrData->getOutputCam($this);
        $mHrData->initDataCompleteOrderPVKH($this);
        $mHrData->getCacheCalculated($this);
        $mHrData->getCacheDebts($this);
        $mHrData->getCacheCommonDebts($this);
    }
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return HrSalaryReports the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_hr_salary_reports}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('dateFromDmy, dateToDmy, name, type_id', 'required', 'on'=>'UserCalculate'),
            array('dateFromDmy, dateToDmy, id, start_date, end_date, name, type_id, data, created_date, created_by, status, approved_by, approved_date, notify, position_work, province_id, code_no', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rType' => array(
                self::BELONGS_TO, 'HrFunctionTypes', 'type_id',
                'on' => 'status != ' . DomainConst::DEFAULT_STATUS_INACTIVE,
            ),
            'rCreatedBy' => array(
                self::BELONGS_TO, 'Users', 'created_by',
                'on'    => 'status != ' . DomainConst::DEFAULT_STATUS_INACTIVE,
            ),
            'rApprovedBy' => array(
                self::BELONGS_TO, 'Users', 'approved_by',
                'on'    => 'status != ' . DomainConst::DEFAULT_STATUS_INACTIVE,
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                'id' => 'ID',
                'start_date' => DomainConst::CONTENT00139,
                'end_date' => DomainConst::CONTENT00140,
                'name' => DomainConst::CONTENT00042,
                'type_id' => DomainConst::CONTENT10008,
                'data' => 'Data',
                'created_date' => DomainConst::CONTENT00010,
                'created_by' => DomainConst::CONTENT00054,
                'status' => DomainConst::CONTENT00026,
                'approved_by' => DomainConst::CONTENT10009,
                'approved_date' => DomainConst::CONTENT10010,
                'notify' => DomainConst::CONTENT10011,
                'position_work'=>'Bộ phận làm việc',
                'dateFromDmy' => "Từ ngày",
                'dateToDmy' => "Đến ngày",
                'date_from' => "Từ ngày",
                'date_to' => "Đến ngày",
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('t.id',$this->id);
        $criteria->compare('t.status',$this->status);
        $criteria->compare('t.type_id',$this->type_id);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    public function getStatus(){
        $aStatus = $this->aStatus;
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : '';
    }
    
    /** @Author: DuongNV Aug0619
     *  @Todo: get status report, đang tính, duyệt bởi ..., hoàn thành, thủ quỹ đã chi
     **/
    public function getStatusDetail(){
        $ret            = $this->getStatus();
        if( !empty($this->code_no) ){
            $criteria   = new CDbCriteria;
            $criteria->compare('t.code_no', $this->code_no);
            $criteria->addCondition('t.status != '.HrApprovedReports::STT_CANCEL);
            $mApprove   = HrApprovedReports::model()->find($criteria);
            $ret        = empty($mApprove) ? $this->getStatus() : $mApprove->getStatus();
        }
        return $ret;
    }

    //-----------------------------------------------------
    // Parent override methods
    //-----------------------------------------------------
    /**
     * Override before save method
     * @return Parent result
     */
    protected function beforeSave() {
        // Format action date value
        $this->start_date = CommonProcess::convertDateTimeToMySqlFormat(
                $this->start_date, DomainConst::DATE_FORMAT_4);
        $this->end_date = CommonProcess::convertDateTimeToMySqlFormat(
                $this->end_date, DomainConst::DATE_FORMAT_4);
        
        if ($this->isNewRecord) {   // Add
            // Handle created by
            if (empty($this->created_by)) {
                $this->created_by = MyFormat::getCurrentUid();
            }
            // Handle created date
            $this->created_date = CommonProcess::getCurrentDateTime();
            
        } else {                    // Update
            
        }
        return parent::beforeSave();
    }

    //-----------------------------------------------------
    // Utility methods
    //-----------------------------------------------------
    public function canUpdate() {
        return true;
    }

    //-----------------------------------------------------
    // Static methods
    //-----------------------------------------------------
    /**
     * Create report
     * @param String $from Date from (format is 'Y-m-d')
     * @param String $to Date to (format is 'Y-m-d')
     * @param Array $arrUserHr Array id of users (UsersHr.php) -> Must be sort by Role id
     * @param String $type Id of salary time (function type)
     * @return HrSalaryReports Model
//        $this->start_date  = $from; 'Y-m-d'
//        $this->end_date    = $to; 'Y-m-d'
//        $this->name        = $name;
//        $this->type_id     = $type;
     */
//    public function createReport($from, $to, $arrUserHr, $type, $name) {
    public function createReport($arrUserHr) {
        $retVal         = [];
        $retVal[self::KEY_HEADER] = [];
        $currentRole    = 0;
//        $beginTime = microtime(true);
        if(!$this->isInitData) { // Nếu chưa init data thì sẽ init (case tính nhiều bp)
            $this->initData();
        }
//        $middleTime = microtime(true);
        $this->aDebtsMonthly = [];
        foreach ($arrUserHr as $key => $mUser) {
            // Get report value
            $mUser->hrFrom          = $this->start_date;
            $mUser->hrTo            = $this->end_date;
            $mUser->mSalaryReport   = $this;
            $mUser->totalSalary     = [];
            $mUser->monthlyDebts    = [];
            // Create header
            if ($currentRole != $mUser->role_id) {
                $currentRole = $mUser->role_id;
                // Calculate report first (to detect parameter using in functions of user)
//                $result = $mUser->getSalaryReport($this->type_id);
                $result = $mUser->createSalaryReport($this->type_id);
                
                // Generate header
                $aHeader                  = $mUser->getSalaryReportHeader($this->type_id);
                // Khởi tạo header vào nếu là user đầu tiên
                if( array_keys($arrUserHr)[0] == $key ){
                    $retVal[self::KEY_HEADER] = $mUser->getSalaryReportHeader($this->type_id);
                } else {
                    $retVal[self::KEY_HEADER] = (count($aHeader) <= 2) ? $retVal[self::KEY_HEADER] : $aHeader;
                }
                // Save report
                $retVal[$mUser->id]       = $result;
            } else {
                $retVal[$mUser->id] = $mUser->createSalaryReport($this->type_id);
            }
            
            if( !empty($mUser->monthlyDebts[$mUser->id]) ){
                $this->aDebtsMonthly[$mUser->id] = $mUser->monthlyDebts[$mUser->id];
            }
        }
        $this->aDebtsMonthly = array_filter($this->aDebtsMonthly);
        $this->data = CommonProcess::json_encode_unicode($retVal);
//        $endTime = microtime(true);
//        Logger::WriteLog('Calculating salary | init data last: '.($middleTime-$beginTime));
//        Logger::WriteLog('Calculating salary | for each user last: '.($endTime-$middleTime));
//        Logger::WriteLog($this->data);
    }
    
    /**
     * Get salary report by type/id
     * @param type $type
     * @param type $id
     * @return \CActiveDataProvider
     */
    public function getSalaryReport($type, $id = ''){
        $criteria=new CDbCriteria;
        if(!empty($id)){
            $criteria->compare('t.id',$id);
        }
        $criteria->compare('t.type_id',$type);
        
        $sort = new CSort();

        $sort->attributes = array(
            'end_date'=>'end_date',
        );    
        $sort->defaultOrder = 't.id desc';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort' => $sort,
            'pagination'=>array(
                'pageSize'=> 10,
            ),
        ));
    }
    
    /**
     * 
     * @param $type report type (salary, timesheets, ..)
     * @param $sameCodeNo if true => những bảng cùng code_no (tính trong cùng 1 lần, nhiều bộ phận) sẽ render thành 1 bảng (thay vì nhiều bảng như bình thường)
     * @return array
     */
    public function getArraySalaryReportsByType($type, $status = '', $sameCodeNo = false) {
        $criteria           =new CDbCriteria;
        $criteria->compare('t.type_id',$type);
        $criteria->order    = "id desc";
        $ret                = [];
        if(!empty($status)){
            if(is_array($status)){
                $criteria->addInCondition('t.status', $status);
            } else {
                $criteria->compare('t.status',$status);
            }
        }
        $models = HrSalaryReports::model()->findAll($criteria);
        if($sameCodeNo){
            $ret = $this->handleSameCodeNo($models);
        } else {
            $ret = $models;
        }
        return $ret;
    }
    
    /** @Author: DuongNV Mar 19,2019
     *  @Todo: đưa những bảng lương cùng code_no thành 1 bảng duy nhất để dễ nhìn
     *  @Param: $models array model HrSalaryReports
     *  @return array report (cùng code_no thì chỉ tính 1)
     **/
    public function handleSameCodeNo($models) {
        $ret        = [];
        $aReport    = [];
        foreach ($models as $value) {
            $code_no = empty($value->code_no) ? 'single' : $value->code_no;
            $aReport[$code_no][$value->id] = $value;
        }
//        $aReport = [
//            'single' => [
//                1 => $obj1,
//                2 => $obj2,
//            ],
//            'S123' => [
//                3 => $obj3,
//                4 => $obj4,
//            ],
//            'S124' => [
//                5 => $obj5,
//                6 => $obj6,
//            ]
//        ];
        foreach ($aReport as $key => $aHrSalary) {
            if( $key == 'single' ){ // Những bảng lương riêng (không cùng code_no)
                foreach ($aHrSalary as $report_id => $mReport) {
                    $ret[$report_id] = $mReport;
                }
            } else { // Những bảng lương cùng code_no
                $mReportSameCodeNo = reset($aHrSalary);
                $aPosition = [];
                $aProvince = [];
                foreach ($aHrSalary as $value) {
                    $aPosition[$value->position_work] = $value->position_work;
                    $aProvince[$value->province_id] = $value->province_id;
                }
                $mReportSameCodeNo->position_work = implode(',', $aPosition);
                $mReportSameCodeNo->position_work = implode(',', $aProvince);
                $ret[$mReportSameCodeNo->id] = $mReportSameCodeNo;
            }
        }
        return $ret;
    }
    
    /** @Author: DuongNV Mar 19,2019
     *  @Todo: gộp data của những report cùng code no thành 1
     *  @return model report with new data
     **/
    public function getReportSameCodeNo($code_no = '', $needMore = []) {
        if(empty($code_no)){
            $code_no = $this->code_no;
        }
        $criteria           = new CDbCriteria;
        $criteria->compare('t.code_no', $code_no);
        if( !empty($needMore['position']) ){
            $criteria->addInCondition('t.position_work', $needMore['position']);
        }
        if( !empty($needMore['province']) ){
            $criteria->addInCondition('t.province_id', $needMore['province']);
        }
//        $criteria->order = "t.province_id asc, t.position_work asc";
        $criteria->order = "t.position_work asc, t.province_id asc";
        $models             = HrSalaryReports::model()->findAll($criteria);
        $aHeader            = [];
        $aBody              = [];
        $mReportSameCodeNo  = new HrSalaryReports();
        $aPosWork           = [];
        $aProvince          = [];
        foreach ($models as $mReport) {
            $data           = json_decode($mReport->data, true);
            $aHeader        += $data[HrSalaryReports::KEY_HEADER];
            unset($data[HrSalaryReports::KEY_HEADER]);

            uasort($data, function($a, $b){
                // Compare utf8 string
                $str1 = iconv('utf-8', 'ascii//TRANSLIT', $a[self::KEY_NHANVIEN]);
                $str2 = iconv('utf-8', 'ascii//TRANSLIT', $b[self::KEY_NHANVIEN]);
                return $str1 > $str2;
            });
            
            $aBody          += $data;
            $mReportSameCodeNo = $mReport;
            $aPosWork[]     = $mReport->position_work;
            $aProvince[]    = $mReport->province_id;
        }
        $aPosWork  = array_unique($aPosWork);
        $aProvince = array_unique($aProvince);
        // Đưa cột tổng ra sau cùng
        if( isset($aHeader[HrSalaryReports::KEY_TONG]) ){
            $sumHeader = $aHeader[HrSalaryReports::KEY_TONG];
            unset($aHeader[HrSalaryReports::KEY_TONG]);
            $aHeader[HrSalaryReports::KEY_TONG] = $sumHeader;
        }
        
        $newData = [
            HrSalaryReports::KEY_HEADER => $aHeader,
        ];
        
        $newData += $aBody;
        if($mReportSameCodeNo->type_id == UsersHr::model()->getIdCalcSalary()){
            $mReportSameCodeNo->name        = "Bảng lương chung";
        }
        $mReportSameCodeNo->data            = json_encode($newData);
        $mReportSameCodeNo->position_work   = implode(',', $aPosWork);
        $mReportSameCodeNo->province_id     = implode(',', $aProvince);
        return $mReportSameCodeNo;
    }
    
    /** @Author: DuongNV Apr 9,2019
     *  @Todo: delete model with same code no
     **/
    public function deleteModelSameCodeNo($code_no = '') {
        if( empty($code_no) && empty($this->code_no) ) return;
        if( empty($code_no) ){
            $code_no = $this->code_no;
        }
        $tableName = HrSalaryReports::model()->tableName();
        $sql = 'DELETE FROM ' . $tableName . ' WHERE code_no = "'.$code_no.'"';
        Yii::app()->db->createCommand($sql)->execute();
    }
    
    /** @Author: DuongNV Mar 29,2019
     *  @Todo: get data report same code no (để xem và duyệt bảng lương)
     *  @return [position_work => [user_id=>data]]
     **/
    public function getDataByCodeNo($code_no = '') {
        if(empty($code_no)){
            $code_no = $this->code_no;
        }
        $criteria = new CDbCriteria;
        $criteria->compare('t.code_no', $code_no);
        $models   = HrSalaryReports::model()->findAll($criteria);
        $ret      = [];
        foreach ($models as $value) {
            $data = json_decode($value->data, true);
            $ret[$value->position_work] = $data;
        }
        return $ret;
    }
    
    
    /** @Author: DuongNV Mar 29,2019
     *  @Todo: get data theo format cố định để duyệt bảng lương
     *  @return model with data [position_work => [user_id=>data]]
     **/
   public function getDataForReview($code_no = '') {
        if (empty($code_no)) {
            $code_no    = $this->code_no;
        }
        $mAppCache      = new AppCache();
        $criteria       = new CDbCriteria;
        $criteria->compare('t.code_no', $code_no);
        $models         = HrSalaryReports::model()->findAll($criteria);
        $aCompany       = $mAppCache->getListdataUserByRole(ROLE_COMPANY);
        $aRole          = Roles::loadItems();
        $ret            = [];
        $modelRet       = new HrSalaryReports();
        $aUid           = [];
        foreach ($models as $value) {
            $aData      = json_decode($value->data, true);
            $tmpHeader  = array_keys($aData);
            unset($tmpHeader[0]); // remove 'header'
            $aUid       = array_merge($aUid, $tmpHeader);
            $modelRet->start_date = $value->start_date;
            $modelRet->end_date   = $value->end_date;
        }
        $startDate      = date('Y-m-01', strtotime('-1 month', strtotime($modelRet->start_date)));
        $endDate        = date("Y-m-t", strtotime($startDate));
        $aUser          = $this->getArrayUsersById($aUid);
        $aProfile       = $this->getArrayProfile($aUid, true);
        $aSalaryData    = $this->getSomeColumnData($startDate, $endDate); // data lương tháng trc
//        $aSalaryData    = $this->getSomeColumnDataByCodeNo($code_no); 
        
        $prevMonth  = date('m/Y', strtotime('-1 month', strtotime($modelRet->start_date)));
        $cMonth     = date('m/Y', strtotime($modelRet->start_date));
        $ret[self::KEY_HEADER] = [
            [
                'no'=>'STT',
                'name'=>'Họ và tên',
                'role'=>'Chức vụ',
                'posi'=>'Bộ phận',
                'prov'=>'Tỉnh',
                'bejo'=>'Ngày vào làm',
                'enjo'=>'Ngày nghỉ việc',
                'comp'=>'Công ty đóng BHXH',
                'prmo'=>'Tháng '. $prevMonth,
                'cumo'=>'Tháng '. $cMonth,
                'note'=>'Ghi chú',
            ],
            [
                'lcb' => 'Lương cơ bản',
                'lsl' => 'Lương sản lượng',
                'ttn' => 'Tổng thu nhập',
                'tg'  => 'Tăng (giảm)',
                'l'   => 'Lương',
            ]
        ];

        foreach ($models as $value) {
            $aData      = json_decode($value->data, true);
            $aAdd       = [];
            foreach ($aData as $uid => $data) {
                if ($uid == self::KEY_HEADER)
                    continue;
                $baseSalary   = empty($aSalaryData['BASE_SALARY'][$uid]) ? 0 : $aSalaryData['BASE_SALARY'][$uid];
                $outputSalary = empty($aSalaryData['OUTPUT_SALARY'][$uid]) ? 0 : $aSalaryData['OUTPUT_SALARY'][$uid];
                $dateBegin    = empty($aProfile[$uid]->date_begin_job) ? '' : $aProfile[$uid]->date_begin_job;
                $isBeginJob   = MyFormat::compareTwoDate($modelRet->end_date, $dateBegin);
                if(!$isBeginJob) continue;
                $dateEnd      = empty($aProfile[$uid]->leave_date) ? '' : $aProfile[$uid]->leave_date;
                $salary       = empty($data['ct943']) ? 0 : $data['ct943'];
                $note         = empty($aProfile[$uid]) ? '' : $aProfile[$uid]->getJsonFieldOneDecode('note', 'json', 'baseArrayJsonDecode');
//                $note         = isset($aProfile[$uid]->json) ? $aProfile[$uid]->json : '';
                $aAdd[$uid]['name'] = empty($data[self::KEY_NHANVIEN]) ? '' : $data[self::KEY_NHANVIEN];
                $aAdd[$uid]['role'] = empty($aRole[$aUser[$uid]->role_id]) ? '' : $aRole[$aUser[$uid]->role_id]; // chuc vu;
                $aAdd[$uid]['posi'] = empty($data[self::KEY_POSITION]) ? '' : $data[self::KEY_POSITION]; // bp
                $aAdd[$uid]['prov'] = empty($data[self::KEY_PROVINCE]) ? '' : $data[self::KEY_PROVINCE]; // tinh
                $aAdd[$uid]['bejo'] = MyFormat::dateConverYmdToDmy($dateBegin);
                $aAdd[$uid]['enjo'] = MyFormat::dateConverYmdToDmy($dateEnd);
                $aAdd[$uid]['comp'] = empty($aCompany[$aUser[$uid]->storehouse_id]) ? '' : $aCompany[$aUser[$uid]->storehouse_id]; // cty
                $aAdd[$uid]['lcb']  = $baseSalary; // Lương cơ bản (tháng trước)
                $aAdd[$uid]['lsl']  = $outputSalary; // Lương sản lượng (tháng trước)
                $aAdd[$uid]['ttn']  = $baseSalary+$outputSalary; // Tổng thu nhập (tháng trước)
                $aAdd[$uid]['tg']   = ($salary-$baseSalary); // Tăng giảm (tháng này so với tháng trước)
                $aAdd[$uid]['l']    = $salary; // Lương (tháng này)
                $aAdd[$uid]['note'] = $note; // note
            }
            $ret[$value->position_work] = empty($ret[$value->position_work]) ? $aAdd : array_merge($ret[$value->position_work], $aAdd);
        }
        $modelRet->data = $ret;
        $_SESSION['data-excel-approve'] = $modelRet;
        return $modelRet;
    }
    
    /** @Author: DuongNV May 04,2019
     *  @Todo: get some column data
     *  @return [user_id=>data]
     *  @params $start_date, $end_date Y-m-d
     **/
   public function getSomeColumnData($start_date, $end_date) {
        $ret                 = [];
        $mUserHr             = new UsersHr();
        $mReport             = new HrSalaryReports();
        $mReport->start_date = $start_date;
        $mReport->end_date   = $end_date;
//        $ret['OUTPUT_SALARY']=  $mReport->getFromCalculated($mUserHr->getIdCalcOutput(), 'ts86'); // DuongNV replace below, fix sl 0 duyệt bảng tổng hợp
        $ret['OUTPUT_SALARY']=  $mReport->getFromCalculated($mUserHr->getIdCalcOutput());
        $ret['BASE_SALARY']  =  $mReport->getFromCalculated($mUserHr->getIdCalcSalary(), 'ct943');
        return $ret;
   }
    
    /** @Author: DuongNV Aug0719
     *  @Todo: get some column data by code no
     *  @return [user_id=>data]
     *  @params code_no
     **/
   public function getSomeColumnDataByCodeNo($code_no) {
        $ret        = [];
        if(empty($code_no)) return $ret;
        $criteria   = new CDbCriteria;
        $criteria->compare('t.code_no', $code_no);
        $mHrSalary  = HrSalaryReports::model()->findAll($criteria);
        if(empty($mHrSalary)) {
            return $ret;
        }
        $ret['OUTPUT_SALARY'] = $this->getDataByColID($mHrSalary, 'ts86');
        $ret['BASE_SALARY']   = $this->getDataByColID($mHrSalary, 'ct943');
        return $ret;
   }
   
   /** @Author: DuongNV Aug0719
     *  @Todo: get some column data by code no
     *  @return [user_id=>data]
     *  @params $mHrSalary array model HrSalaryReports with $model->data, col id
     **/
   public function getDataByColID($mHrSalary, $colID = self::KEY_TONG) {
        $aData      = $this->getArrayData($mHrSalary);
        $retVal     = [];
        $key        = $colID;
        foreach ($aData as $user_id => $aValue) {
            $splitter         = DomainConst::SPLITTER_TYPE_2;
            $retVal[$user_id] = isset($aValue[$key]) ? (int) str_replace($splitter, '', $aValue[$key]) : 0;
        }
        return $retVal;
   }
    
    /** @Author: DuongNV May 03,2019
     *  @Todo: get data theo format cố định thủ quỹ chi lương
     *  @return model with data [user_id=>data]
     **/
   public function getDataForTreasurer($code_no = '') {
        $code_no = empty($code_no) ? $this->code_no : $code_no;
        if (empty($code_no)) {
            return [];
        }
        
        $criteria       = new CDbCriteria;
        $criteria->compare('t.code_no', $code_no);
        $models         = HrSalaryReports::model()->findAll($criteria);
        $ret            = [];
        $aUid           = [];
        $mUserHr        = new UsersHr();
        $mReport        = new HrSalaryReports();
        foreach ($models as $value) {
            $aData      = json_decode($value->data, true);
            $tmpHeader  = array_keys($aData);
            unset($tmpHeader[0]); // remove 'header'
            $aUid       = array_merge($aUid, $tmpHeader);
            $mReport->start_date = $value->start_date;
            $mReport->end_date   = $value->end_date;
        }
        $aProfile       = $this->getArrayProfile($aUid, true);
        $aUser          = $this->getArrayUsersById($aUid);
        $aPrepaidData   = $mReport->getFromCalculated($mUserHr->getIdPrepaidWages());
        $aSalaryData    = $mReport->getFromCalculated($mUserHr->getIdCalcSalary(), self::KEY_TERM2_SALARY);
        
        $ret[self::KEY_HEADER] = [
            'STT',
            'Mã nhân viên',
            'Tên nhân viên',
            'Hình thức thanh toán',
            'Số điện thoại Viettel Pay',
            'Số CMND',
            'STK ngân hàng',
            'Chi nhánh',
//            'Số tiền',
//            'Nội dung',
//            'Nội dung SMS',
            
//            'Lương kỳ 1',
//            'Nội dung SMS kỳ 1',
            'Lương kỳ 2',
            'Nội dung SMS kỳ 2',
            'Ghi chú',
        ];
        
        $no         = 1;
        foreach ($models as $value) {
            $aData      = json_decode($value->data, true);
            foreach ($aData as $uid => $data) {
                if ($uid == self::KEY_HEADER)
                    continue;
                $vtpPhone       = isset($aProfile[$uid]->salary_method_phone) ? $aProfile[$uid]->salary_method_phone : '';
                $bankNo         = isset($aProfile[$uid]->bank_no) ? $aProfile[$uid]->bank_no : '';
                $bankBranch     = isset($aProfile[$uid]) ? $aProfile[$uid]->getBankBranch() : '';
                $moneyTerm1     = isset($aPrepaidData[$uid]) ? $aPrepaidData[$uid] : 0;
                $moneyTerm2     = isset($aSalaryData[$uid]) ? $aSalaryData[$uid] : 0;
                $codeAccount    = empty($aUser[$uid]) ? '' : $aUser[$uid]->code_account; // ma nv
                $month          = date('m', strtotime($value->start_date));
                $year           = date('Y', strtotime($value->start_date));
                if($moneyTerm1 == 0 && $moneyTerm2 == 0) continue;
                
                // Nếu thanh toán qua viettel pay thì ẩn stk đi cho đỡ rối
                if(isset($aProfile[$uid]->salary_method) && $aProfile[$uid]->salary_method == UsersProfile::SALARY_METHOD_VIETTEL){
                    $bankNo     = '';
                    $bankBranch = '';
                } else {
                    $vtpPhone   = '';
                }
                
                $ret[$uid][] = $no++;
                $ret[$uid][] = $codeAccount;
                $ret[$uid][] = $data[self::KEY_NHANVIEN];
                $ret[$uid][] = isset($aProfile[$uid]) ? $aProfile[$uid]->getSalaryMethod() : ''; // Hình thức thanh toán
                $ret[$uid][] = $vtpPhone; // SDT Viettel pay
                $ret[$uid][] = isset($aProfile[$uid]->id_number) ? $aProfile[$uid]->id_number : ''; // CMND
                $ret[$uid][] = $bankNo; // STK
                $ret[$uid][] = $bankBranch; // Chi nhanh
//                $ret[$uid][] = $money; // Số tiền
//                $ret[$uid][] = 'Lương kỳ 2 T'.$month.'.'.$year; // Nội dung
//                $ret[$uid][] = 'Lương kỳ 2 T'.$month.'.'.$year; // Nội dung SMS
                
//                $ret[$uid][] = $moneyTerm1;
//                $ret[$uid][] = "Lương kỳ 1 T{$month}/{$year}";
                $ret[$uid][] = $moneyTerm2;
                $ret[$uid][] = "Lương kỳ 2 T{$month}/{$year}";
                $ret[$uid][] = ''; // Ghi chú
            }
        }
        $mReport->data = $ret;
        $_SESSION['data-excel-approve'] = $mReport;
        return $mReport;
    }

    /**
     * 
     * @param $id report id
     * @return model
     */
    public function getArraySalaryReportsById($id) {
        return HrSalaryReports::model()->findByPk($id);
    }
    
    /**
     * 
     * @param $type report type
     * @param $month report month
     * @return array
     */
    public function getArraySalaryReports($type, $month = '') {
        $criteria=new CDbCriteria;
        if(!empty($month)){
            $criteria->compare('MONTH(t.end_date)',$month);
        }
        $criteria->compare('t.type_id',$type);
        return HrSalaryReports::model()->findAll($criteria);
    }
    
    public function getName() {
        return $this->name;
    }
    
    public function getNameForApprove() {
        return 'Bảng tổng hợp lương T'.date('m/Y', strtotime($this->start_date));
    }
    
    public function getStartDate() {
        return MyFormat::dateConverYmdToDmy($this->start_date);
    }
    
    public function getEndDate() {
        return MyFormat::dateConverYmdToDmy($this->end_date);
    }
    
    public function getType() {
        return isset($this->rType) ? $this->rType->name : '';
    }
    
    /** @Author: NVDuong 03/08/18
     *  @Return: json
     **/
    public function getData() {
        return 'data';
//        return json_decode($this->data, true);
    }
    
    /** @Author: NVDuong 03/08/18
     *  @todo get array data from multiple report
     *  @Return: array
     **/
    public function getArrayData($aModel = []) {
        if(empty($aModel)){
            return json_decode($this->data, true);
        } else {
            $ret = [];
            foreach ($aModel as $value) {
                $tmpData                = json_decode($value->data, true);
                $tmpHeader              = $tmpData[self::KEY_HEADER];
                $ret[self::KEY_HEADER]  = isset($ret[self::KEY_HEADER]) ? ($ret[self::KEY_HEADER] + $tmpHeader) : $tmpHeader;
                unset($tmpData[self::KEY_HEADER]);
                $ret                    = $ret + $tmpData;
            }
            return $ret;
        }
    }
    
    /** @Author: ANH DUNG Oct 20, 2018
     *  @Todo: get array id user test
     **/
    public function getUserIdTest() {
        return [
            1029122, 1029818, 1029821,1029822, 1029823, // Call201 -> 205
        ];
    }
    
    /** @todo: get array user by role for create report
     * @return array
     */
    public function getArrayUsersByRole($role){
        $criteria=new CDbCriteria;
        $criteria->compare('role_id', $role);
        $criteria->addCondition("t.status=".STATUS_ACTIVE);
        $sParamsIn = implode(',', $this->getUserIdTest());
        $criteria->addCondition("t.id NOT IN ($sParamsIn)");
        $criteria->addCondition('t.payment_day=1');// Oct018 get User có trả lương
        $criteria->order = 't.code_bussiness';

        $mUsers = new UsersHr();
        return $mUsers->findAll($criteria);
    }
    
    public function getArrayUsersByPositionWork($pos){
        $crtUser = new CDbCriteria;
        $crtUser->join = 'JOIN gas_users_profile p ON t.id = p.user_id AND p.position_work = ' . $pos;
        $crtUser->addCondition('t.status=' . STATUS_ACTIVE);
        $crtUser->addCondition('t.payment_day=1');// Oct018 get User có trả lương
        $crtUser->order = "t.first_name asc";
        $mUsers = new UsersHr();
        $models = $mUsers->findAll($crtUser);
        $ret    = [];
        foreach ($models as $value){
            $ret[$value->id] = $value;
        }
        return $ret;
    }
    /**
     * @Author: DuongNV 08/08/18 11:26
     * @param bool $isRoleAll including role_all or not
     * @return array
     */
    public function getArrayRoleSalary($isRoleAll = false) {
        $reVal = [];
        $aRole = array(
            ROLE_EMPLOYEE_MARKET_DEVELOPMENT,
            ROLE_EMPLOYEE_MAINTAIN,
            ROLE_CALL_CENTER,
            ROLE_DIEU_PHOI,
            ROLE_TELESALE,
            ROLE_ACCOUNTING,
            ROLE_SALE,
            ROLE_CASHIER,
            ROLE_ACCOUNTING_ZONE,
            ROLE_RECEPTION,
            ROLE_DEBT_COLLECTION,
            ROLE_E_MAINTAIN,
            ROLE_DRIVER,
            ROLE_MARKETING,
            ROLE_DIRECTOR,
            ROLE_DIRECTOR_BUSSINESS,
            ROLE_HEAD_TECHNICAL,
            ROLE_HEAD_GAS_BO,
            ROLE_IT,
            ROLE_IT_EMPLOYEE,
            ROLE_HEAD_OF_LEGAL,
            ROLE_EMPLOYEE_OF_LEGAL,
            ROLE_CHIEF_ACCOUNTANT,
            ROLE_SALE_ADMIN,
            ROLE_MONITORING_MARKET_DEVELOPMENT,
            ROLE_CLEANER,
            ROLE_MECHANIC,
            
        );
        if(!empty($isRoleAll)){
            $aRole[] = ROLE_ALL;
        }
        $mRole          = new Roles();
        $aModelRoles    = $mRole->getByListId($aRole);
        foreach ($aRole as $id) {
            if(empty($id)){
                $reVal[$id] = self::ROLE_ALL_NAME;
                continue;
            }
            $reVal[$id] = $aModelRoles[$id]->role_name;
        }
        return $reVal;
    }
    
    /**
     * @todo: get array user by array id for create report
     * @return array
     */
    public function getArrayUsersById($aId){
        $criteria = new CDbCriteria;
        if(empty($aId)){
            return [];
        } else {
            $criteria->addCondition('t.id IN ('. implode(',', $aId) .')');
        }
        $criteria->join = 'JOIN gas_users_profile p ON t.id = p.user_id';
        // Filter những nhân viên nghỉ việc trước ngày bắt đầu
        if(!empty($this->dateFromDmy)){
            $startDate = MyFormat::dateConverDmyToYmd($this->dateFromDmy, '-');
            $criteria->addCondition("p.leave_date = '0000-00-00' OR p.leave_date > '$startDate'");
        }
        // Filter những nhân viên chưa đi làm trc ngày kết thúc
        if( !empty($this->dateFromDmy) ){
            $endDate            = MyFormat::dateConverDmyToYmd($this->dateFromDmy, '-');
            $criteria->addCondition("p.date_begin_job <= '$endDate'");
        }
        $criteria->addCondition("t.status = ".STATUS_ACTIVE);
        $criteria->addCondition('t.payment_day = 1');
//        $criteria->order = "t.province_id asc, p.position_work asc, t.first_name";
        $criteria->order = "p.position_work asc, t.province_id asc, t.first_name";
        $mUsers = new UsersHr();
        $models = $mUsers->findAll($criteria);
        $res    = [];
        foreach ($models as $value) {
            $res[$value->id] = $value;
        }
        return $res;
    }
    
    /*
     * Get field (first_name, last_name, role_id, ..) by user id
     */
    public function getFieldUserById($field, $id) {
        $mUser = Users::model()->findByPk($id);
        if(!empty($mUser->{$field})){
            return $mUser->{$field};
        }
        return '';
    }
    
    /*
     * Get array appover
     */
    public function getArrayApprover() {
        return array(
            2     => self::getFieldUserById('first_name', 2),
            26676 => self::getFieldUserById('first_name', 26676), // GD
        );
    }
    
    /*
     * @author: DuongNV 31/07/18
     * Check if enough table
     * $params: $aIdReport : array ID report ( user select report to calculate )
     */
    public function isReadyToCalculateSalary($aIdReport){
        $aFunctionType = HrFunctionTypes::model()->findAll();
//        if((count($aFunctionType)-1) != count($aIdReport)) return false;
        $resVal = true;
        foreach ($aFunctionType as $rpType) {
            if($rpType->id == 3) continue; // if type is tinh luong
            if(empty($aIdReport)){
                echo 'Thiếu bảng '. $rpType->name . '<br>';
                $resVal = false;
            } else {
                $mReport = HrSalaryReports::model()->find("type_id = {$rpType->id} AND id IN (". implode(',', $aIdReport) .")");
                if(empty($mReport)){
                    echo 'Thiếu bảng '. $rpType->name . '<br>';
                    $resVal = false;
                } else {
                    unset($aIdReport[array_search($mReport->id,$aIdReport)]);
                    if($mReport->isSalaryReportReady()) {
                        continue;
                    } else {
                        echo "Bảng {$mReport->name} không đủ điều kiện để tính!<br>";
                        $resVal = false;
                    }
                }
            }
        }
        return $resVal;
    }
    
    /*
     * @author: DuongNV 31/07/18
     * Check if a report is ready
     */
    public function isSalaryReportReady(){
        return true;
        $aData = json_decode($this->data, true);
        // Get array employee in report data
        $aUsersInReport = [];
        foreach ($aData as $key => $row) {
            if($key == 0)   continue;
            $aUsersInReport[] = $key;
        }
        // Get array employee in database with the same role
        $mUsers = HrSalaryReports::model()->getArrayUsersByPositionWork($this->position_work);
        $aUsersInReport = [];
        foreach ($mUsers as $user) {
            $aUsersInRole[] = $user->id;
        }
        // if not all employee is selected
        if($aUsersInReport != $aUsersInReport) {
//            echo 'Số lượng nhân viên không đủ!';
            return false;
        }
        // Check status is final
        if($this->status != self::STT_FINAL){
//            echo 'Trạng thái phải là Đã Chốt!';
            return false;
        }
        // Check if start date is 01 and end date is last date of month
        $firstDateOfMonth = date('Y-m-01', strtotime($this->start_date));
        if($this->start_date != $firstDateOfMonth || $this->end_date != date('Y-m-t', strtotime($firstDateOfMonth))){
//            echo 'Ngày phải là ngày đầu tháng và cuối tháng!';
            return false;
        }
        return true;
    }
    
    /** @Author: NVDuong 03/08/18
     *  @Todo: Get final (stt = STT_FINAL of stt = STT_COMPLETE if type = final salary (3)) salary reports by type 
     *  @Param: $type:   function type
     *  @Param: $hrFrom: date from (model UsersHr)
     *  @Param: $hrTo:   date to (model UsersHr)
     **/
    public function getFinalReportByTypeAndDate($type, $hrFrom, $hrTo) {
        $mUserHr    = new UsersHr();
        $status     = HrSalaryReports::STT_FINAL;
        if($type == $mUserHr->getIdCalcSalary()){
//            $status = HrSalaryReports::STT_COMPLETE;
            $status = '';
        }
        $criteria = new CDbCriteria; 
        $criteria->compare('start_date', $hrFrom);
        $criteria->compare('end_date', $hrTo);
        $criteria->compare('type_id', $type);
        $criteria->compare('status', $status);
        $criteria->order = "id ASC";
        return HrSalaryReports::model()->findAll($criteria);
    }

    
    /** @Author: DuongNV Nov 29,2018
     *  @Todo:
     **/
    public function getArrayWorkPosition() {
        $aPositionWork = UsersProfile::model()->getArrWorkRoom();
        $aPositionWork[ROLE_ALL] = 'Chung';
        return $aPositionWork;
    }
    
    /** @Author: DuongNV Nov 6,2018
     *  @Todo: get array user profile user_id => [position_work, position_room]
     *  @Param: $aIdUser array id user
     **/
    public function getArrayProfile($aIdUser = [], $return_object = false){
//        if(count($aIdUser) < 1){
//            return [];
//            if(!empty($this->position_work)){
//                $mSchedule = new HrWorkSchedule();
//                $mSchedule->search_position = [$this->position_work];
//                $mSchedule->search_province = [1]; // HCM
//                $aIdUser = array_keys($mSchedule->getArrayUserHr());
//            } else {
//                return [];
//            }
//        }
        $criteria = new CDbCriteria;
        if(!empty($aIdUser)){ // get all user when init for calculate salary
            $cond   = implode(",", $aIdUser);
            $criteria->addCondition('t.user_id IN ('. $cond . ')');
        }
        $model  = UsersProfile::model()->findAll($criteria);
        $res    = [];
        foreach ($model as $value) {
            if($return_object){
                $res[$value->user_id] = $value;
            } else {
                $res[$value->user_id] = [
                    'position_work' => $value->position_work,
                    'position_room' => $value->position_room,
                    'date_begin_job'=> $value->date_begin_job, // Ngày bắt đầu làm
                    'leave_date'    => ($value->leave_date == '0000-00-00') ? '' : $value->leave_date, // Ngày nghỉ việc
                    'id_province'   => $value->id_province, 
                    'profile_status'=> $value->profile_status, 
                ];
            }
        }
        return $res;
    }
    
    /** @Author: DuongNV Nov 18,2018
     *  @Todo: cache work schedule of list user
     **/
    public function getCacheSchedule($aIdUser = []) {
        $criteria = new CDbCriteria;
        if(!empty($aIdUser)){
            $criteria->addInCondition('t.employee_id', $aIdUser);
        }
        $criteria->addCondition('t.work_day >= "' . $this->start_date . '"');
        $criteria->addCondition('t.work_day <= "' . $this->end_date . '"');
        $models     = HrWorkSchedule::model()->findAll($criteria);
        $aWorkShift = HrWorkShift::model()->loadItems();
        $res        = [];
        foreach ($models as $sch) {
            $factor = isset($aWorkShift[$sch->work_shift_id]) ? $aWorkShift[$sch->work_shift_id]->factor : 0;
            $res[$sch->employee_id][$sch->work_day] = [
                                    'work_shift_id' => $sch->work_shift_id,
                                    'factor'        => $factor,
                                ];
        }
        return $res;
    }
    
    /** @Author: DuongNV Feb 1,2019
     *  @Todo: get data for export excel final salary
     **/
    public function getDataExcelFinal() {
        $aData          = json_decode($this->data, true);
        $aUid           = array_filter(array_keys($aData));
        unset($aUid[0]); // bỏ header ra
        $aProfile       = $this->getArrayProfile($aUid, true);
        $aUser          = $this->getArrayUsersById($aUid);
        $mAppCache      = new AppCache();
        $aCompany       = $mAppCache->getListdataUserByRole(ROLE_COMPANY);
        $aNewData       = [];
        $aHeaderBefore = [
            'STT',
            'Mã NV', 
            'Tên NV', 
            'Chức vụ', 
            'Bộ phận', 
            'Tỉnh', 
            'Ngày vào làm',
            'Ngày nghỉ việc',
            'Công ty', 
//            'Lương BHXH',
            ];
        $aHeaderAfter = [
            'Hình thức trả lương',
            'SDT Viettel Pay',
            'CMND', 
            'STK ngân hàng', 
            'Chi nhánh ngân hàng',
        ];
//        $aHeaderReport      = $aData[self::KEY_HEADER];
        $aHeaderReport      = $this->getArrayHeaderReport($aData);
            
        unset($aHeaderReport[self::KEY_NHANVIEN]);
        unset($aHeaderReport[self::KEY_BASE_SALARY]);
        unset($aHeaderReport[self::KEY_POS_ROOM]);
        unset($aHeaderReport[self::KEY_POSITION]);
        unset($aHeaderReport[self::KEY_PROVINCE]);
        unset($aHeaderReport[self::KEY_BEGIN_JOB]);
        unset($aHeaderReport[self::KEY_END_JOB]);
        unset($aHeaderReport[self::KEY_TONG]);
        $aHeader       = array_merge($aHeaderBefore, $aHeaderReport, $aHeaderAfter);
        
        $aNewData[HrSalaryReports::KEY_HEADER] = $aHeader;
        $aPositionWork = UsersProfile::model()->getArrWorkRoom();
        $aPositionRoom = UsersProfile::model()->getArrPositionRoom();
        $aProvince     = GasProvince::getArrAll();
        $aSalaryMethod = UsersProfile::model()->getArraySalaryMethod();
        $no            = 1; // STT
        foreach ($aUid as $user_id) {
            $position_room        = isset($aProfile[$user_id]) ? $aProfile[$user_id]->position_room : '';
            $position_work        = isset($aProfile[$user_id]) ? $aProfile[$user_id]->position_work : '';
            $province_id          = isset($aUser[$user_id]) ? $aUser[$user_id]->province_id : '';
            $begin_job            = isset($aProfile[$user_id]) ? $aProfile[$user_id]->date_begin_job : '';
            $end_job              = isset($aProfile[$user_id]) ? $aProfile[$user_id]->leave_date : '';
            $company_id           = isset($aUser[$user_id]->storehouse_id) ? $aUser[$user_id]->storehouse_id : "";
            $salary_method        = isset($aProfile[$user_id]) ? $aProfile[$user_id]->salary_method : '';

            $aNewData[$user_id][] = $no++; // STT
            $aNewData[$user_id][] = empty($aUser[$user_id]) ? '' : $aUser[$user_id]->code_account; // ma nv
            $aNewData[$user_id][] = $aData[$user_id][self::KEY_NHANVIEN]; // Tên NV
            $aNewData[$user_id][] = isset($aPositionRoom[$position_room]) ? $aPositionRoom[$position_room] : ''; // chuc vu
            $aNewData[$user_id][] = isset($aPositionWork[$position_work]) ? $aPositionWork[$position_work] : ''; // bp
            $aNewData[$user_id][] = isset($aProvince[$province_id]) ? $aProvince[$province_id] : ""; // Tỉnh
            $aNewData[$user_id][] = empty($begin_job) ? '' : MyFormat::dateConverYmdToDmy($begin_job); // ngay vao lam
            $aNewData[$user_id][] = (empty($end_job) || $end_job == '0000-00-00') ? '' : MyFormat::dateConverYmdToDmy($end_job); // ngay nghi viec
            $aNewData[$user_id][] = isset($aCompany[$company_id]) ? $aCompany[$company_id] : "";// Công ty
//            $aNewData[$user_id][] = isset($aProfile[$user_id]) ? (int)$aProfile[$user_id]->salary_insurance : ''; // luong bhxh
            
//            $aBank                = Users::getArrDropdown(ROLE_BANK);
//            $aNewData[$user_id][] = $aBank[$aProfile[$user_id]->bank_id]; // Ngân hàng
            
            foreach ($aHeaderReport as $key => $value) {
                if($key == HrSalaryReports::KEY_BASE_SALARY || $key == HrSalaryReports::KEY_TONG) continue;
                $money = isset($aData[$user_id][$key]) ? $aData[$user_id][$key] : 0;
                $aNewData[$user_id][] = (is_numeric($money) && $money >= 10000) ? (int) $money : $money;
            }
            
            $aNewData[$user_id][] = isset($aSalaryMethod[$salary_method]) ? $aSalaryMethod[$salary_method] : ''; // hinh thuc tra luong
            $aNewData[$user_id][] = isset($aProfile[$user_id]) ? $aProfile[$user_id]->salary_method_phone.'' : ''; // SDT viettel pay (string)
            $aNewData[$user_id][] = isset($aProfile[$user_id]) ? $aProfile[$user_id]->id_number.'' : ''; // CMND (string)
            $aNewData[$user_id][] = isset($aProfile[$user_id]) ? $aProfile[$user_id]->bank_no.'' : ''; // STK (string)
            
            $aNewData[$user_id][] = $aProfile[$user_id]->bank_branch; // chi nhanh
        }
        return $aNewData;
    }
    
    /** @Author: DuongNV Feb 1,2019
     *  @Todo: get data for export excel final salary
     **/
    public function getDataExcelPrepaid() {
        $aData          = json_decode($this->data, true);
        $aUid           = array_filter(array_keys($aData));
        unset($aUid[0]); // bỏ header ra
        $aProfile       = $this->getArrayProfile($aUid, true);
        $aUser          = Users::getArrObjectUserByRole('', $aUid);
        $mUserProfile   = new UsersProfile();
        $aNewData       = [];
        $aHeader        = [
            '#',
            'Mã NV', 
            'Tên NV', 
            'BP', 
            'Tỉnh', 
            'Ngày vào làm', 
            'Ngày nghỉ việc', 
            'Hình thức trả lương',
            'Số dt viettelpay',
            'STK',
            'Chi nhánh',
            'Tổng ngày công(1-19)',
            'Số tiền',
            'Ghi chú'
            ];
        
        $aNewData[HrSalaryReports::KEY_HEADER] = $aHeader;
        $i = 1;
        foreach ($aUid as $user_id) {
            $money                = empty($aData[$user_id][self::KEY_TONG]) ? 0 : $aData[$user_id][self::KEY_TONG];
            if(empty($money)){
                continue;
            }
            $mNewProfile          = new UsersProfile(); // khoi check isset
            $mProfile             = empty($aProfile[$user_id]) ? $mNewProfile : $aProfile[$user_id];
            $dateBegin            = !empty($mProfile->date_begin_job) ? MyFormat::dateConverYmdToDmy($mProfile->date_begin_job) : ''; // Ngay vao lam
            $dateEnd              = (!empty($mProfile->leave_date) && $mProfile->leave_date != '0000-00-00') ? $mProfile->leave_date : ''; // Ngay nghi viec;
            $aNewData[$user_id][] = $i++;
            $aNewData[$user_id][] = isset($aUser[$user_id]->code_account) ? $aUser[$user_id]->code_account : '';
            $aNewData[$user_id][] = $aData[$user_id][self::KEY_NHANVIEN]; // Tên NV
            $aNewData[$user_id][] = $aData[$user_id][self::KEY_POSITION]; // BP
            $aNewData[$user_id][] = $aData[$user_id][self::KEY_PROVINCE]; // Tinh
            $aNewData[$user_id][] = $dateBegin; // Ngay vao lam
            $aNewData[$user_id][] = ($dateEnd == '0000-00-00') ? '' : $dateEnd; // Ngay nghi viec
            $aSalaryMethod        = $mUserProfile->getArraySalaryMethod();
            $aNewData[$user_id][] = $aSalaryMethod[$mProfile->salary_method];
            if($mProfile->salary_method == UsersProfile::SALARY_METHOD_VIETTEL){
                $aNewData[$user_id][] = $mProfile->salary_method_phone; // SDT viettel pay
                $aNewData[$user_id][] = '';
                $aNewData[$user_id][] = '';
            } elseif( $mProfile->salary_method == UsersProfile::SALARY_METHOD_BANK ){
                $aNewData[$user_id][] = '';
                $aNewData[$user_id][] = $mProfile->bank_no; // STK
                $aNewData[$user_id][] = $mProfile->bank_branch; // chi nhanh
            } else {
                $aNewData[$user_id][] = '';
                $aNewData[$user_id][] = '';
                $aNewData[$user_id][] = '';
            }
            $aNewData[$user_id][] = $aData[$user_id][self::KEY_TERM1_WORKDAY]; // tong ngay cong (1-19)
            $aNewData[$user_id][] = $money; // so tien
            $aNewData[$user_id][] = $mProfile->getJsonFieldOneDecode('note', 'json', 'baseArrayJsonDecode'); // ghi chu
        }
        return $aNewData;
    }
    
    /** @Author: DuongNV Mar 8,19
     *  @Todo: return id header template in table EmailTemplates
     **/
    public function getIdTemplateHeader(){
        return 26;
    }
    
    /** @Author: DuongNV Mar 8,19
     *  @Todo: return array header
     **/
    public function getTemplateHeader(){
        $template   = EmailTemplates::model()->findByPk($this->getIdTemplateHeader());
        $data       = htmlspecialchars($template->email_body);
        $data       = str_replace('&lt;p&gt;', '', $data);
        $data       = str_replace('&lt;/p&gt;', '', $data);
        $data       = htmlspecialchars_decode($data);
        $json       = trim(preg_replace('/\s\s+/', '', $data));
        $json       = trim($json, "\0");
        $json       = stripslashes(html_entity_decode($json));
        return json_decode($json, true);
    }
    
    /** @Author: DuongNV Mar 8,19
     *  @Todo: return array pair [ header key => name ]
     *  @params: $aHeader array header key [ header key ]
     **/
    public function getArrayPairHeader($aHeader){
        $aIdParams              = [];
        $aIdFunctions           = [];
        foreach ($aHeader as $key => $value) {
            if (strpos($value, HrSalaryReports::KEY_CT) !== false) {
                $aIdFunctions[] = str_replace(HrSalaryReports::KEY_CT, '', $value);
            }
            if (strpos($value, HrSalaryReports::KEY_TS) !== false) {
                $aIdParams[]    = str_replace(HrSalaryReports::KEY_TS, '', $value);
            }
        }
        
        $criParams  = new CDbCriteria;
        $criParams->addInCondition('t.id', $aIdParams);
        $mParams    = HrParameters::model()->findAll($criParams);
        $aParams    = [];
        foreach ($mParams as $value) {
            $aParams[self::KEY_TS.$value->id] = $value->name;
        }
        $criFunc    = new CDbCriteria;
        $criFunc->addInCondition('t.id', $aIdFunctions);
        $mFunc      = HrFunctions::model()->findAll($criFunc);
        $aFunc      = [];
        foreach ($mFunc as $value) {
            $aFunc[self::KEY_CT.$value->id] = $value->name;
        }
        $ret        = [];
        foreach ($aHeader as $key => $value) {
            if( in_array($value, self::$aColHide) ){ // ẩn một số cột đi
                continue;
            }
            if (strpos($value, HrSalaryReports::KEY_CT) !== false) {
                $ret[$value] = isset($aFunc[$value]) ? $aFunc[$value] : '';
            }
            if (strpos($value, HrSalaryReports::KEY_TS) !== false) {
                $ret[$value] = isset($aParams[$value]) ? $aParams[$value] : '';
            }
            
            switch ($value) {
                case HrSalaryReports::KEY_NHANVIEN:
                    $ret[$value] =  'Nhân viên';
                    break;
                case HrSalaryReports::KEY_POSITION:
                    $ret[$value] =  'Bộ phận';
                    break;
                case HrSalaryReports::KEY_PROVINCE:
                    $ret[$value] =  'Tỉnh';
                    break;
                case HrSalaryReports::KEY_CAN_PAID:
                    $ret[$value] =  'Trả lương';
                    break;
                case HrSalaryReports::KEY_BEGIN_JOB:
                    $ret[$value] =  'Ngày vào làm';
                    break;
                case HrSalaryReports::KEY_END_JOB:
                    $ret[$value] =  'Ngày nghỉ việc';
                    break;
                case HrSalaryReports::KEY_NOTE:
                    $ret[$value] =  'Ghi chú';
                    break;
                case HrSalaryReports::KEY_POS_ROOM:
                    $ret[$value] =  'Chức vụ';
                    break;

                default:
                    break;
            }
//            DuongNV May 23, remove Sum column
//            if ($value == HrSalaryReports::KEY_TONG){
//                $ret[$value] =  'Tổng';
//            }
        }
        return $ret;
    }
    
    /** @Author: DuongNV Mar 8,19
     *  @Todo: Merge 2 header with Sum col at the end 
     *  @param: $aHeader1 array with sum col
     *  @param: $aHeader2 additional array
     **/
    public function mergeHeader($aHeader1, $aHeader2){
        $sumCol = array_slice( $aHeader1, -1, 1, TRUE );
        array_pop($aHeader1); // remove last col
        $aTemp  = array_merge($aHeader1, $aHeader2);
        return array_merge($aTemp, $sumCol); // add last col
    }
    
    /*
     * array template header
     */
    public function aFirstTemplate(){
        return [
            'nv',
            'ts98',
            'ts86',
            'ts87',
            'ts101',
            'ts100',
            'ts76',
            'ts75',
            'ct909',
            'ct910',
            'ct911',
            'ct912',
            'ct913',
            'ct914',
            'ct915',
            'tg',
        ];
    }
    
    /** @Author: DuongNV
     *  @Todo: can sort header of report
     **/
    public function canSortHeader() {
        $cUid   = MyFormat::getCurrentUid();
        $aAllow = [
            1436021, // uid DuongNV
            1979734, // uid uitDuongNV
            GasConst::UID_ADMIN,
            GasConst::UID_CHAU_LNM,
        ];
        return in_array($cUid, $aAllow);
    }
    
    /** @Author: DuongNV
     *  @Todo: set is_show, view trên list Tổng hợp lương
     **/
    public function canSetShow() {
        $cUid   = MyFormat::getCurrentUid();
        $cRole  = MyFormat::getCurrentRoleId();
        $aAllow = [
            GasConst::UID_CHAU_LNM,
        ];
        return in_array($cUid, $aAllow) || $cRole == ROLE_ADMIN;
    }
    
    public function generateCodeNo(){
        return MyFunctionCustom::getNextId('HrSalaryReports', 'S'.date('y'), LENGTH_TICKET,'code_no');
    }
    
    /** @Author: DuongNV 26 Mar,2019
     *  @Todo: get array report theo tháng và năm
     *  @Param: $notSalary không bao gồm bảng thu nhập (chỉ chấm công, sản lượng, ...)
     *  @return: [ position_work => [type_id => [ id => model ]] ]
     **/
    public function getArrayReportByMonthAndYear($month, $year, $status = self::STT_FINAL, $notSalary = true) {
        $criteria = new CDbCriteria;
        $criteria->compare('MONTH(t.start_date)', $month);
        $criteria->compare('YEAR(t.start_date)', $year);
        if( is_array($status) ){
            $criteria->addInCondition('t.status', $status);
        } else {
            $criteria->compare('t.status', $status);
        }
        
        if($notSalary){
            $idSalary = UsersHr::model()->getIdCalcSalary();
            $criteria->addCondition('t.type_id != '.$idSalary);
        }
        $models = HrSalaryReports::model()->findAll($criteria);
        $ret    = [];
        foreach ($models as $value) {
            $ret[$value->position_work][$value->type_id][$value->id] = $value;
        }
        return $ret;
    }
    
    /** @Author: DuongNV Apri 24,19
     *  @Todo: delete final report 
     **/
    public function deleteSameFinal() {
        $criteria = new CDbCriteria;
        $criteria->compare('t.type_id', UsersHr::model()->getIdCalcSalary());
        $criteria->compare('t.start_date', $this->start_date);
        $criteria->compare('t.end_date', $this->end_date);
        $criteria->compare('t.status', HrSalaryReports::STT_CALCULATING);
        $models     = HrSalaryReports::model()->findAll($criteria);
        $aId        = [];
        $code_no    = '';
        foreach ($models as $value) {
            $aId[]  = $value->id;
            $code_no= empty($value->code_no) ? $code_no : $value->code_no;
        }
        if(!empty($aId)){
            HrSalaryReports::model()->deleteAll('id IN ('. implode(',', $aId).')');
        }
        return $code_no;
    }
    
    /** @Author: DuongNV 28 Mar, 19
     *  @Todo: check dk để tính lương, 
     * chưa có bảng lương hoặc bảng lương đã bị hủy mới được tính, 
     * nếu bảng lương đã hoặc đang duyệt thì không được tính
     **/
    public function canCalculateFinalSalary($month, $year, &$message = '') {
        $isOk       = true;
        $criteria1  = new CDbCriteria; 
        $criteria1->compare('t.type_id', UsersHr::model()->getIdCalcSalary()); // Type là tính lương
        $criteria1->compare('MONTH(t.start_date)', $month);
        $criteria1->compare('YEAR(t.start_date)', $year);
        $mSalary = HrSalaryReports::model()->find($criteria1);
        if( !empty($mSalary) && $mSalary->status != self::STT_CALCULATING ){
            $isOk    = false;
            $message = ($mSalary->status == self::STT_COMPLETE) ? '<i style="color:green;">Bảng lương đã hoàn thành</i>' : '<i style="color:red;">Bảng lương đang yêu cầu duyệt!</i>';
        }
        return $isOk;
    }
    
    public function getArraySendReview(){
        return [
            1979734,
            GasConst::UID_CHAU_LNM,
            GasConst::UID_ADMIN
        ];
    }
    
    /** @Author: DuongNV 28 Mar, 19
     *  @Todo: check quyen gui yeu cau xet duyet bang luong
     **/
    public function canSendReview() {
        $aAllow = $this->getArraySendReview();
        $cUid   = MyFormat::getCurrentUid();
        return in_array($cUid, $aAllow);
    }
    
    /** @Author: DuongNV 28 Mar, 19
     *  @Todo: check quyen approve bang luong
     **/
    public function changeReportStatusByCodeNo($code_no, $status) {
        if(empty($status) || empty($code_no)) return;
        $tableName  = HrSalaryReports::model()->tableName();
        $sql        = 'UPDATE '.$tableName.' SET status = '.$status.' WHERE code_no="'.$code_no.'"';
        Yii::app()->db->createCommand($sql)->execute();
    }
    
    /** @Author: DuongNV 28 Mar, 19
     *  @Todo: get array header for final report
     *  @params: $data json_decode($this->data)
     **/
    public function getArrayHeaderReport($data){
        $aTemplateHeaderKey = $this->getTemplateHeader();
        $aTemplatePair      = $this->getArrayPairHeader( $aTemplateHeaderKey );
        
        // DuongNV get fixed header only
        $aCurrentHeaderKey  = isset($data[HrSalaryReports::KEY_HEADER]) ? array_keys($data[HrSalaryReports::KEY_HEADER]) : [];
        $aDiff              = array_diff($aCurrentHeaderKey, $aTemplateHeaderKey);
        $aPair              = [];
        if(!empty($aDiff)){
            $aExtraPair     = $this->getArrayPairHeader($aDiff);
            $aPair          = $this->mergeHeader($aTemplatePair, $aExtraPair);
        } else {
            $aPair          = $aTemplatePair;
        }
        return $aPair;
//        return $aTemplatePair;
    }
    
    /** @Author: DuongNV April 17, 2019
     *  @Todo: get data from report đã tính
     *  @Param: $this->start_date, $this->end_date Y-m-d
     **/
    public function getFromCalculated($type, $indexCol = self::KEY_TONG) {
        $mHrSalaryReport    = new HrSalaryReports();
        $models             = $mHrSalaryReport->getFinalReportByTypeAndDate($type, $this->start_date, $this->end_date);
        if(empty($models)) {
            return 0;
        }
        $aData              = $this->getArrayData($models);
        $retVal             = [];
//        $key = ($indexCol == self::KEY_TONG) ? $indexCol : (count($aData[0]) - $indexCol);
        $key = $indexCol;
        foreach ($aData as $user_id => $aValue) {
            $splitter         = DomainConst::SPLITTER_TYPE_2;
            $retVal[$user_id] = isset($aValue[$key]) ? (int) str_replace($splitter, '', $aValue[$key]) : 0;
        }
        return $retVal;
    }
    
    /** @Author: DuongNV 17 Apr, 2019
     *  @Todo: flush data for progress bar
     *  @referrences: https://stackoverflow.com/questions/4706525/php-flush-not-working/4978642#4978642
     **/
    public function flushData($data) {
        $mUsersHr = new UsersHr();
        if($this->type_id == $mUsersHr->getIdWorkSchedule()){ // Nếu lịch làm việc thì không cần progress bar
            return;
        }
        if($this->type_id != $mUsersHr->getIdCalcSalary()){
            echo '<script>document.getElementsByTagName("body")[0].innerHTML = "";</script>';
        }
        if(is_numeric($data)){
            echo "<div style='width:100%;' class='pbcontainer'>"
            . "<div style='width:{$data}%;background:#4bc24b;text-align:center;'>{$data}%</div>"
                    . "</div>";
        } else {
            echo '<b class="pbcontainer">'.$data.'</b>';
        }
        echo str_repeat(' ',1024*64);
    }
    
    /** @Author: DuongNV Aug0519
     *  @Todo: get user salary info
     *  @params: $this->date_from, $this->date_to format d-m-Y
     **/
    public function getUserSalary($uid = '', &$mHrSalary = '') {
        $aData              = [];
        if(empty($uid)) return $aData;
        $tblUsers           = Users::tableName();
        $crProfile          = new CDbCriteria;
        $crProfile->select  = "t.*, u.province_id as id_province"; // lấy province bên table users
        $crProfile->join    = "JOIN $tblUsers u ON u.id = t.user_id";
        $crProfile->compare('t.user_id', $uid);
        $mProfile           = UsersProfile::model()->find($crProfile);
        $criteria           = new CDbCriteria;
        $criteria->compare('MONTH(t.start_date)', intval($this->onlyMonth));
        $criteria->compare('YEAR(t.start_date)', intval($this->onlyYear));
        $criteria->compare('t.type_id', UsersHr::model()->getIdCalcSalary());
        if($mProfile){
            $criteria->compare('t.position_work', $mProfile->position_work);
            $criteria->compare('t.province_id', $mProfile->id_province);
        }
        $model         = HrSalaryReports::model()->find($criteria);
        if($model){
            $aDataMultiUser = json_decode($model->data, true);
            $aData = [
                self::KEY_HEADER => isset($aDataMultiUser[self::KEY_HEADER]) ? $aDataMultiUser[self::KEY_HEADER] : [],
                $uid             => isset($aDataMultiUser[$uid]) ? $aDataMultiUser[$uid] : []
            ];
            if( !empty($mHrSalary) ){
                $mHrSalary = $model;
                $mHrSalary->onlyMonth = $this->onlyMonth;
                $mHrSalary->onlyYear  = $this->onlyYear;
            }
        }
        return $aData;
    }
    
    /** @Author: DuongNV
     *  @Todo: get list button can do with current salary report
     **/
    public function getListButtonOfReport() {
        $cssClassOkBtn      = 'okBtn changeSttBtn btn btn-primary ';
        $cssClassCancelBtn  = 'cancelBtn changeSttBtn btn btn-primary moveToCalculatingTab ';
        $isCalculatingTab   = false;
        $result             = '';
        $isBlock            = false;
        switch ($this->status) {
            case self::STT_CALCULATING:
                $isCalculatingTab = true;
                break;
            case self::STT_APPROVED:
                $cssClassCancelBtn .= 'disabled';
                break;
            case self::STT_FINAL:
                $cssClassOkBtn .= 'disabled';
                break;

            default:
                break;
        }
        
        if($this->type_id == UsersHr::model()->getIdPrepaidWages()){
            $dateOnly   = self::DATE_BLOCK_PREPAID;
            $blockDate  = date('Y-m-'.$dateOnly, strtotime($this->start_date));
            $curDate    = date('Y-m-d');
            $isBlock    = MyFormat::compareTwoDate($curDate, $blockDate);
        }
        
        if($isCalculatingTab):
            $result .= '<ul class="month gr-btn" style="padding-right:15px;">';
            if(GasCheck::isAllowAccess('HrSalaryReports', 'Calculate')):
                $result .= '<a class="newReportBtn btn btn-primary calculate-btn'.$this->type_id.'" href="'.Yii::app()->createAbsoluteUrl('/admin/HrSalaryReports/calculate', array('type'=>$this->type_id)).'">Thêm</a>';
                $result .= $isBlock ? '' : '<a class="recalculateBtn btn btn-primary calculate-btn'.$this->type_id.'" data-action="calculate" href="'.Yii::app()->createAbsoluteUrl('/admin/HrSalaryReports/calculate', array('type'=>$this->type_id, 're'=>1)).'">Tính lại</a>';
                $result .= $isBlock ? '' : '<a class="moveToApprovedTab changeSttBtn btn btn-primary" data-type-btn="'.HrSalaryReports::STT_APPROVED.'">Duyệt</a>';
            endif;
            if(GasCheck::isAllowAccess('HrSalaryReports', 'Delete')):
                $result .= '<a class="deleteReportBtn btn btn-primary" data-action"delete">Xoá</a>';
            endif;
            $result .= '</ul>';
        else:
            $result .= '<ul class="month gr-btn" style="padding-right:15px;">';
            if(GasCheck::isAllowAccess('HrSalaryReports', 'Calculate')):
                $result .= $isBlock ? '' : '<a class="'.$cssClassOkBtn.'" data-type-btn="'.HrSalaryReports::STT_FINAL.'">OK</a>';
                $result .= $isBlock ? '' : '<a class="'.$cssClassCancelBtn.'" data-type-btn="'.HrSalaryReports::STT_CALCULATING.'">Cancel</a>';
            endif;
                if(GasCheck::isAllowAccess('HrSalaryReports', 'ExportExcel')):
                $result .= '<a class="ExportExcelBtn btn btn-primary" data-action="" href="'.Yii::app()->createAbsoluteUrl('admin/HrSalaryReports/ExportExcel').'">Export Excel</a>';
            endif;
            $result .= '</ul>';
        endif;
        return $result;
    }
    
    /** @Author: DuongNV Aug0719
     *  @Todo: get array user trừ 30% công nợ, trường hợp đặc biệt
     **/
    public function getArrayDebtsSpecial() {
        return [
            GasLeave::UID_DIRECTOR_BUSSINESS    => 1000000,
            GasLeave::PHUC_HV                   => 5000000,
            GasSupportCustomer::UID_QUY_PV      => 5000000, // Phan Văn Quý
            865752                              => 2000000, // Tran Van Binh
            1119774                             => 3000000, // Nguyen Xuan Dat
            1930781                             => 3000000, // Huynh Tieu An
            1738502                             => 1000000, // Nguyen Mau Binh
            1714261                             => 1000000, // Nguyễn Hữu Toàn
            257116                              => 1000000, // Nguyễn Trung Hiếu (GS) 
            863286                              => 1000000, // Nguyễn Đình Thành – NVKD
            217782                              => 1000000, // Nguyễn Văn Đợt - GS
            2060700                             => 1000000, // Trần Tấn Phi - PVKH
        ];
    }
    
}
