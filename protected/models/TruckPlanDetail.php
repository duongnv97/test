<?php

/**
 * This is the model class for table "{{_truck_plan_detail}}".
 *
 * The followings are the available columns in table '{{_truck_plan_detail}}':
 * @property string $id
 * @property string $code_no 
 * @property string $truck_plan_id
 * @property integer $type
 * @property string $plan_date
 * @property string $plan_date_bigint
 * @property string $supplier_id
 * @property string $supplier_warehouse_id
 * @property integer $supplier_price
 * @property integer $transport_price
 * @property string $driver_id
 * @property string $car_id
 * @property string $customer_id
 * @property integer $customer_role_id
 * @property string $customer_warehouse_id
 * @property string $customer_warehouse_other
 * @property integer $customer_price
 * @property integer $qty_plan
 * @property integer $qty_real
 * @property integer $status
 * @property string $created_by_note
 * @property string $driver_note
 * @property string $cancel_note
 * @property string $created_date
 * @property string $price_usd_ton
 * @property string $exchange_rate
 */
class TruckPlanDetail extends BaseSpj
{
    public $agent_id = 0, $autocomplete_name, $MAX_ID;
    
    /** @Author: DungNT Aug 14, 2019
     *  @Todo: list kho có Phần mềm cân kết nối với app
     **/
    public function getAgentHaveScales() {
        return [
            GasCheck::KHO_PHUOC_TAN,
        ];
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return TruckPlanDetail the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_truck_plan_detail}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['id, code_no, truck_plan_id, type, plan_date, plan_date_bigint, supplier_id, supplier_warehouse_id, supplier_price, transport_price, driver_id, car_id, customer_id, customer_role_id, customer_warehouse_id, customer_warehouse_other, customer_price, qty_plan, qty_real, status, created_by_note, driver_note, cancel_note, created_date', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rCustomerWarehouse' => array(self::BELONGS_TO, 'Users', 'customer_warehouse_id'),
            'rDriver' => array(self::BELONGS_TO, 'Users', 'driver_id'),
            'rCar' => array(self::BELONGS_TO, 'Users', 'car_id'),
            'rSupplier' => array(self::BELONGS_TO, 'Users', 'supplier_id'),
            'rWarehouse' => array(self::BELONGS_TO, 'Users', 'supplier_warehouse_id'),
            'rTruckPlan' => array(self::BELONGS_TO, 'TruckPlan', 'truck_plan_id'),
            'rFile' => array(self::HAS_MANY, 'GasFile', 'belong_id',
                'on'=>'rFile.type='.  GasFile::TYPE_19_TRUCK_PLAN,
                'order'=>'rFile.id ASC',
            ),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code_no' => 'Code No',
            'truck_plan_id' => 'Truck Plan',
            'type' => 'Type',
            'plan_date' => 'Plan Date',
            'plan_date_bigint' => 'Plan Date Bigint',
            'supplier_id' => 'Supplier',
            'supplier_warehouse_id' => 'Supplier Warehouse',
            'supplier_price' => 'Supplier Price',
            'transport_price' => 'Transport Price',
            'driver_id' => 'Driver',
            'car_id' => 'Car',
            'customer_id' => 'Khách hàng',
            'customer_role_id' => 'Customer Role',
            'customer_warehouse_id' => 'Customer Warehouse',
            'customer_warehouse_other' => 'Customer Warehouse Other',
            'customer_price' => 'Customer Price',
            'qty_plan' => 'Qty Plan',
            'qty_real' => 'Qty Real',
            'status' => 'Status',
            'created_by_note' => 'Created By Note',
            'driver_note' => 'Driver Note',
            'cancel_note' => 'Cancel Note',
            'created_date' => 'Created Date',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
	$criteria->compare('t.id',$this->id);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    protected function beforeSave() {
        $this->plan_date_bigint = strtotime($this->plan_date);
        $this->generateCodeNo();
        if(empty($this->customer_role_id) && !empty($this->customer_id)){
            $mCustomer = $this->rCustomer;
            $this->customer_role_id = $mCustomer->role_id;
        }
        return parent::beforeSave();
    }
    protected function beforeDelete() {
        GasFile::DeleteByBelongIdAndType($this->id, GasFile::TYPE_19_TRUCK_PLAN);
        $this->deleteStorecardRef();
        return parent::beforeDelete();
    }
    public function generateCodeNo(){
        if(empty($this->code_no)){
            $this->code_no = MyFunctionCustom::getNextId('TruckPlanDetail', 'TD'.date('y'), LENGTH_TICKET, 'code_no');
        }
    }
    
    /** @Author: DungNT May 27, 2019
     *  @Todo: format and trim data before save to db
     **/
    public function trimData() {
        $this->customer_price       = MyFormat::removeComma($this->customer_price);
        $this->qty_plan             = MyFormat::removeComma($this->qty_plan);
        $this->qty_real             = MyFormat::removeComma($this->qty_real);
        $this->price_usd_ton        = MyFormat::removeComma($this->price_usd_ton);
        $this->exchange_rate        = MyFormat::removeComma($this->exchange_rate);
        $this->created_by_note      = trim($this->created_by_note);
    }
    
    /**
     * @author DuongNV May 17,2019
     * @todo get array detail by truck_plan_id
     * number $type_order: TruckPlan::TYPE_ORDER_EXPORT or TruckPlan::TYPE_ORDER_IMPORT
     */
    public function getListDetail($nTypeOrder){
        if(empty($this->truck_plan_id)) return [];
        $criteria   = new CDbCriteria;
        $criteria->compare('t.truck_plan_id', $this->truck_plan_id);
        $criteria->compare('t.type_order', $nTypeOrder);// DungNT May2719
        $criteria->order = 't.id';
        $models     = self::model()->findAll($criteria);
        $ret        = [];
        foreach ($models as $value) {
            $ret[$value->id] = $value;
        }
        return $ret;
    }
    
    
    /** @Author: DungNT Jun 21, 2019
     *  @Todo: sửa status để đồng bộ với app đang define
     *  // For status number on app
        val ORDER_NEW = "1"
        val ORDER_PROCESSING = "3"
        val ORDER_COMPLETE = "4"
        val ORDER_CANCEL = "5"
     **/
    public function getStatusAppNumber() {
        /* 1: New
         * 2: confirm
         * 3: Done
         * 4: cancel 
         * 5: complete Import
         */
        $aMapStatusApp = $this->getArrayMapStatusApp();
        return isset($aMapStatusApp[$this->status]) ? $aMapStatusApp[$this->status] : 0;
    }
    
    public function getArrayMapStatusApp() {
        return [
            TruckPlan::STT_NEW          => TruckPlan::STT_NEW,
            TruckPlan::STT_CONFIRM      => 3,
            TruckPlan::STT_DONE         => 4,
            TruckPlan::STT_CANCEL       => 5,
        ];
    }
    
    public function getCodeNo() {
        return $this->code_no;
    }
    
    public function getType(){
        $aType = TruckPlan::$aType;
        return isset($aType[$this->type]) ? $aType[$this->type] : '';
    }
    
    public function getStatus(){
        $aStatus = TruckPlan::$aStatus;
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : '';
    }
    public function getPlanDate(){
        $mTruckPlan = $this->rTruckPlan;
        return $mTruckPlan->getPlanDate();
    }
    
    public function getSupplier(){
        $mTruckPlan = $this->rTruckPlan;
        return $mTruckPlan->getSupplier();
    }
    
    public function getSupplierWarehouse(){
        $mTruckPlan = $this->rTruckPlan;
        return $mTruckPlan->getSupplierWarehouse();
    }
    
    public function getDriver($field_name = 'first_name'){
        $mTruckPlan = $this->rTruckPlan;
        return $mTruckPlan->getDriver($field_name);
    }
    
    public function getCar(){
        $mTruckPlan = $this->rTruckPlan;
        return $mTruckPlan->getCar();
    }
    public function getCreatedByNote(){
        return $this->created_by_note;
    }
    
    public function getCreatedBy(){
        return isset($this->rCreatedBy) ? $this->rCreatedBy->getFullName() : '';
    }
    public function getQtyPlan($format = false){
        if($format){
            return ActiveRecord::formatCurrency($this->qty_plan);
        }
        return $this->qty_plan;
    }
    public function getQtyReal($format = false){
        if($format){
            return ActiveRecord::formatCurrency($this->qty_real);
        }
        return $this->qty_real;
    }
    
    public function getCustomer(){
        return isset($this->rCustomer) ? $this->rCustomer->getFullName() : '';
    }
    public function getCustomerWarehouse(){
        if(!empty($this->customer_warehouse_other)){
            return $this->customer_warehouse_other;
        }
        return isset($this->rCustomerWarehouse) ? $this->rCustomerWarehouse->getFullName() : '';
    }
    // May2719 do sử dụng chung list import + export dưới app nên xử lý lại dùng chung hàm
    public function getAppCustomer(){
        if($this->type_order == TruckPlan::TYPE_ORDER_IMPORT){// nhan hang
            return $this->getSupplier();
        }
        return $this->getCustomer();
    }
    public function getAppCustomerWarehouse(){
        if($this->type_order == TruckPlan::TYPE_ORDER_IMPORT){// nhan hang
            return 'Kho nhận: '.$this->getSupplierWarehouse();
        }
        $qty = '';
        if($this->appAction == GasConst::ACTION_LIST){
            $qty = $this->getQtyPlan(true).' kg';
        }
        $sSupplierWarehouse = $this->getSupplierWarehouse();
        return !empty($sSupplierWarehouse) ? $this->getSupplierWarehouse(). ": {$this->getDriver()}" : '';
    }
    public function getDriverNote(){
        return $this->driver_note;
    }
    public function getCompleteTime($format = 'd/m/Y H:i'){// 'Y-m-d'
        return MyFormat::dateConverYmdToDmy($this->complete_time, $format);
    }
    /** @Author: DungNT Jul 30, 2019
     *  @Todo: check type order import or not
     * @return: true is import -- nhận hàng từ NCC
     *          else false
     **/
    public function isOrderImport() {
        if($this->type_order == TruckPlan::TYPE_ORDER_IMPORT){
            return true;
        }
        return false;
    }
    
    /** @Author: ANH DUNG May2219 -- handle listing api */
    public function handleApiList(&$result, $q) {
        // 1. get list order by user id
        $dataProvider           = $this->apiListing($q);
        $models                 = $dataProvider->data;
        $CPagination            = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page']   = $CPagination->pageCount;
        
        $result['message']  = 'Tổng số Truck Plan 1';
        $result['record']   = [];
        
        if( $q->page >= $CPagination->pageCount ){
            $result['record'] = [];
        }else{
            foreach($models as $model){
                $model->mAppUserLogin       = $this->mAppUserLogin;// for canPickCancel
                $result['record'][]         = $model->formatAppItemList();
            }
        }
    }
    
    /** @Author: ANH DUNG May2219
    *  @Todo: get data listing 
    *  @param: $q object post params from app client
    */
    public function apiListing($q) {
        $mTruckPlan = new TruckPlan();
        $sParamsIn      = implode(',', $mTruckPlan->getAppStatusNew());
        $criteria = new CDbCriteria();
        $criteria->compare('t.type_order', $this->type_order);
        if($this->mAppUserLogin->role_id == ROLE_DRIVER){
            $criteria->compare('t.driver_id', $this->mAppUserLogin->id);
        }elseif($this->mAppUserLogin->role_id == ROLE_CRAFT_WAREHOUSE){
            $criteria->compare('t.customer_id', $this->agent_id);
        }
        
        if(!empty($q->customer_id)){
            $criteria->compare('t.customer_id', $q->customer_id);
        }
        
        if( !empty($q->date_from) && !empty($q->date_to) ){
            $date_from  = MyFormat::dateDmyToYmdForAllIndexSearch($q->date_from);
            $date_to    = MyFormat::dateDmyToYmdForAllIndexSearch($q->date_to);
            if($q->app_tab == AppConst::TAB_COMPLETE){
                $date_from = MyFormat::modifyDays($date_from, 30, '-');// 40 + 15 = 45 day
            }
            DateHelper::searchBetween($date_from, $date_to, 'plan_date_bigint', $criteria, false);
        }
        if($q->app_tab == AppConst::TAB_NEW){
            $criteria->addCondition('t.status IN ('.$sParamsIn.')');
        }elseif($q->app_tab == AppConst::TAB_COMPLETE){
            $criteria->addCondition('t.status NOT IN ('.$sParamsIn.')');
        }else{
            throw new Exception('Yêu cầu tab không hợp lệ');
        }

        $criteria->order = 't.id DESC';
        $dataProvider=new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
//                'pageSize' => GasAppOrder::API_LISTING_ITEM_PER_PAGE,
                'pageSize' => 5,// Đưa vào List cache sẽ luôn load page 0, nên sẽ để 20 item 
                'currentPage' => (int)$q->page,
            ),
          ));
        return $dataProvider;
    }
    
    /** @Author: ANH DUNG May2219
     *  @Todo: dùng chung để format item trả xuống list + view của app Gas Service
     **/
    public function formatAppItemList() {
        $temp = [];
        $temp['id']                     = $this->id;
        $temp['code_no']                = $this->getCodeNo();
        $temp['customer_name']          = $this->getAppCustomer();
        $temp['customer_warehouse']     = $this->getAppCustomerWarehouse();
        $temp['plan_date']              = "{$this->getPlanDate()} - {$this->getQtyReal(true)} kg - {$this->getCar()}";
        if($this->appAction == GasConst::ACTION_VIEW){
            $this->formatAppItemView($temp);
        }
        
        $temp['status_number']          = $this->getStatusAppNumber();
        $temp['show_nhan_giao_hang']    = $this->canPick();
        $temp['show_huy_giao_hang']     = $this->canPickCancel();
        
        return $temp;
    }
    
    /** @Author: DungNT May 24, 2019
     *  @Todo: add more item view
     **/
    public function formatAppItemView(&$temp) {
        $temp['driver_name']            = $this->getDriver('first_name');
        $temp['driver_phone']           = $this->getDriver('phone');
        $temp['driver_note']            = $this->getDriverNote();
        $temp['customer_phone']         = '0384331552-0389945321-0988550190';
        $temp['web_note']               = $this->getCreatedByNote();
        $temp['qty_plan']               = $this->getQtyPlan(true);
        $temp['qty_real']               = $this->getQtyReal(true) > 0 ? $this->getQtyReal(true) : '';
        $temp['show_button_complete']   = $this->showAppComplete();
        $temp['show_button_cancel']     = $this->showAppCancel();
        $temp['allow_update']           = $this->showAppComplete();
        
        $temp['list_image']             = GasFile::apiGetFileUpload($this->rFile);
    }
    
    /**
     * @Author: DungNT Jan 11, 2017 app view
     * format record view from app
     */
    public function appView() {
        return $this->formatAppItemList();
    }
    
    /** @Author: DungNT May2219
     *  @Todo: handle set event of App TruckPlan
     */
    public function handleSetEvent($q) {
        $status_obj = '';
        switch ($q->action_type) {
            case AppConst::EVENT_ORDER_CONFIRM:
                $this->employeePick($q);
                $status_obj = MonitorUpdate::STATUS_CONFIRM;
                break;
            case AppConst::EVENT_ORDER_CANCEL_CONFIRM:
                $this->employeeCancelPick($q);
                $status_obj = MonitorUpdate::STATUS_CANCEL;
                break;
            case AppConst::EVENT_ORDER_COMPLETE:
                $this->employeeComplete($q);
                $status_obj = MonitorUpdate::STATUS_COMPLETE;
                break;
            case AppConst::EVENT_ORDER_CANCEL:
                $this->employeeCancelOrder($q);
                $status_obj = MonitorUpdate::STATUS_CANCEL;
                break;
            default:
                $this->addError('id', 'Yêu cầu không hợp lệ');
                return ;
        }
        
        $location = $q->latitude.",$q->longitude";
        $this->saveEvent($q->action_type, $location);
    }
    
    /** @Author: DungNT May 28, 2019
     *  @Todo: get param post of set event
     **/
    public function getPostEvent($q) {
        // || empty($this->customer_id) is ĐH gas bồn nhận
//        if($this->customer_role_id == ROLE_CUSTOMER || $this->isOrderImport()){
        if(!in_array($this->customer_id, $this->getAgentHaveScales())){
            $this->qty_real         = MyFormat::removeComma($q->qty_real);
        }
        $this->driver_note      = MyFormat::escapeValues($q->driver_note);
        if(empty($this->complete_time) || $this->complete_time == '0000-00-00 00:00:00'){
            $this->complete_time    = $this->getCreatedDateDb();
        }
        if($this->status == TruckPlan::STT_DONE && $this->qty_real < 100){
            throw new Exception('Số lượng phải lớn hơn 100 kg');
        }
    }
    
     /** @Author: DungNT May2219
     *  @Todo: handle employee Pick (nhận đơn hàng)
     */
    public function employeePick($q) {
        if(!$this->canPick()){
            throw new Exception('Đơn hàng đã có nhân viên nhận, bạn không thể xác nhận đơn này');
        }
        $this->status               = TruckPlan::STT_CONFIRM;
        $this->update();
        // notify cho KH
    }
    
    /** @Author: DungNT May2219
     *  @Todo: handle employee Cancel Pick (Hủy nhận đơn hàng)
     */
    public function employeeCancelPick($q) {
        if(!$this->canPickCancel()){
            throw new Exception('Yêu cầu không hợp lệ, bạn không thể hủy xác nhận đơn hàng này');
        }
        $this->status               = TruckPlan::STT_NEW;
        $this->update();
    }
    
    /** @Author: DungNT May2219
     *  @Todo: handle employee complete
     */
    public function employeeComplete($q) {
        if(!$this->showAppComplete()){
            throw new Exception('Đơn hàng đã hoàn thành, không thể cập nhật');
        }
        if(!$this->canCompleteExport()){
            throw new Exception('Lái xe chưa hoàn thành Đơn nhận hàng ở kho của nhà cung cấp, không thể hoàn thành đơn giao hàng cho '.$this->getCustomer());
        }
//        if($this->isAgentInternal()){
//            throw new Exception('Đơn hàng giao kho nội bộ bạn không thể hoàn thành, hệ thống tự hoàn thành sau khi cân xe lần 2');
//        }// DungNT cho thu kho hoan thanh
        $this->status               = TruckPlan::STT_DONE;
        $this->getPostEvent($q);
        $this->update();
        $this->doSameAction();
    }
    
    /** @Author: DungNT May2219
     *  @Todo: handle employee cancel
     */
    public function employeeCancelOrder($q) {
        if(!$this->showAppCancel()){
            throw new Exception('Đơn hàng đã hủy, không thể cập nhật');
        }
        $this->status               = TruckPlan::STT_CANCEL;
        $this->getPostEvent($q);
        $this->update();
        $this->doSameAction();
    }
    
    /** @Author: DungNT Jul3019
     *  @Todo: handle some same action when Complete OR Cancel
     */
    public function doSameAction() {
        $this->updateStatusTruckPlan();
        $this->autoCreateStorecardOrderAgentInternal();
    }
    
    /** @Author: DungNT May 28, 2019
     *  @Todo: update back status to TruckPlan
     * if done Import -> TruckPlan::STT_COMPLETE_ORDER_SUPPLIER   => 'Đã nhận hàng',
     * if done all => TruckPlan::STT_DONE                      => 'Hoàn thành',
     **/
    public function updateStatusTruckPlan() {
        $mTruckPlan = $this->rTruckPlan;
        $attUpdate = ['status'];
        if($this->type_order == TruckPlan::TYPE_ORDER_IMPORT){// nhan hang
            $mTruckPlan->status = $this->status;
            if($this->status == TruckPlan::STT_DONE){
                $mTruckPlan->status = TruckPlan::STT_COMPLETE_ORDER_SUPPLIER;
            }
            $mTruckPlan->qty_real = $this->qty_real;
            $attUpdate[] = 'qty_real';
        }elseif($this->type_order == TruckPlan::TYPE_ORDER_EXPORT){// giao hàng multi KH - check từng order
            $isDoneAll = true;
            $aDetail = $this->getListDetail(TruckPlan::TYPE_ORDER_EXPORT);
            foreach($aDetail as $item){
                if(in_array($item->status, $mTruckPlan->getAppStatusNew())){
                    $isDoneAll = false;
                }
            }
            if($isDoneAll){
                $mTruckPlan->status = TruckPlan::STT_DONE;
            }
        }
        $mTruckPlan->update($attUpdate);
        $this->updateGasRemain($mTruckPlan);
    }
    
    /** @Author: DungNT May2219
     *  @Todo: save tracking action from app
     */
    public function saveEvent($action_type, $google_map) {
        $mEvent = new TransactionEvent();
        $mEvent->type                       = TransactionEvent::TYPE_TRUCK_PLAN;
        $mEvent->transaction_history_id     = $this->id;
        $mEvent->user_id                    = $this->mAppUserLogin ? $this->mAppUserLogin->id : '';
        $mEvent->action_type                = $action_type;
        $mEvent->google_map                 = $google_map;
        $mEvent->save();
    }
    
    /** @Author: ANH DUNG May2219
     *  @Todo: check employee can pick / Cancel Order. check có thể Hủy nhận đơn hàng
     */
    public function canPick() {
        if($this->status == TruckPlan::STT_NEW && $this->mAppUserLogin->id == $this->driver_id){
            return 1;
        }
        return 0;
    }
    public function canPickCancel() {
        if($this->status == TruckPlan::STT_CONFIRM && $this->mAppUserLogin->id == $this->driver_id){
            return 1;
        }
        return 0;
    }
    public function showAppComplete() {
        $aStatusShow = [TruckPlan::STT_CONFIRM];
        if(in_array($this->status, $aStatusShow) && $this->canComplete()){
            return 1;
        }
        return 0;
    }
    public function showAppCancel() {
        $aStatusShow = [TruckPlan::STT_CONFIRM];
        if(in_array($this->status, $aStatusShow) && $this->canComplete()){
            return 1;
        }
        return 0;
    }
    
    /** @Author: DungNT Jul 29, 2019
     *  @Todo: check user can complete order
     * Nếu là đơn KH thường thì allow all
     * Nếu là đơn KH kho nội bộ thì chỉ cho Thủ Kho hoàn thành
     **/
    public function canComplete() {
        $aRoleAllow = [ROLE_CRAFT_WAREHOUSE];
        if($this->customer_role_id == ROLE_AGENT){
            if(in_array($this->customer_id, $this->getAgentHaveScales())){
                return in_array($this->mAppUserLogin->role_id, $aRoleAllow);
            }
            return true;
        }
        if($this->customer_role_id == ROLE_CUSTOMER || $this->isOrderImport()){
            return true;
        }
        return false;
    }
    /** @Author: DungNT Jun 21, 2019
     *  @Todo: check driver hoàn thành đơn giao hàng cho KH, nếu chưa hoàn thành đơn nhận thì ko cho hoàn thành đơn giao
     **/
    public function canCompleteExport() {
        $mTruckPlan = $this->rTruckPlan;
        if($this->type_order == TruckPlan::TYPE_ORDER_IMPORT){// nhan hang
            return true;
        }
        $aStatusImportAllow = [TruckPlan::STT_DONE, TruckPlan::STT_COMPLETE_ORDER_SUPPLIER];// DungNT add Sep0519
//        if($mTruckPlan->rDetailImport->status != TruckPlan::STT_DONE){ 
        if(!in_array($mTruckPlan->rDetailImport->status, $aStatusImportAllow)){
            return false;
        }
        return true;
    }
    
    /** @Author: DungNT Jun 21, 2019
     *  @Todo: check driver hoàn thành đơn giao hàng cho kho nôi bộ
     *  Hiện tại check ko cho hoàn thành đơn kho phước tân, Đơn này sẽ được hoàn thành khi cân lần 2 ở PM cân window
     * chỗ này có thể check theo customer_role_id là role agent
     **/
    public function isAgentInternal() {
        $res = false;
        if($this->customer_role_id == ROLE_AGENT):
            $res = true;
        endif;
        return $res;
    }

    /** @Author: DungNT Jun 20, 2019
     *  @Todo: view image on web
     **/
    public function getViewImg(){
        $res = '';
        $cmsFormater = new CmsFormatter();
        $res .= $cmsFormater->formatBreakTaskDailyFile($this, array('width'=>30, 'height'=>30));
        return $res;
    }
    
    /** @Author: DungNT Jun 20, 2019
     *  @Todo: update gas remain when status done all 
     * A = Số lượng thực tế giao - Số lượng thực tế nhận
     **/
    public function updateGasRemain($mTruckPlan) {
        if($mTruckPlan->status != TruckPlan::STT_DONE){
            return ;
        }
        $mTruckPlan = TruckPlan::model()->findByPk($mTruckPlan->id);
        $qtyExport = $qtyImport = 0;
        // 1. Tính Số lượng thực tế giao
        foreach($mTruckPlan->rDetailExport as $mDetail){
            if($mDetail->status == TruckPlan::STT_DONE){
                $qtyExport += $mDetail->qty_real;
            }
        }
        // 2. Tính Số lượng thực tế nhận -- nhận ở kho của NCC để đi giao cho KH( hoặc kho nhà)
        if($mTruckPlan->rDetailImport){
            $qtyImport += $mTruckPlan->rDetailImport->qty_real;
        }
        $mTruckPlan->gas_remain         = $qtyExport - $qtyImport;
        $mTruckPlan->amount_gas_remain  = $mTruckPlan->gas_remain * $mTruckPlan->supplier_price;
        $mTruckPlan->update();
    }
    
    /** Apr 14, 2017
     *  notify cho các giao nhận của đại lý đó khi có bảo trì của CallCenter put xuống
     */
    public function notifyEmployee() {
        if(empty($this->titleNotify)){
            $this->titleNotify = "[{$this->getStatus()}] {$this->getPlanDate()} Gas bồn giao: {$this->getCustomer()}";
        }
        $aUid[] = $this->driver_id;
        $aUid[] = GasConst::UID_DEMO_PVKH;
        $this->runInsertNotify($aUid, true); // Send Now
//        $this->runInsertNotify($aUid, false); // Send By Cron
    }
    
    /** @Author: DungNT Jun 21, 2019
     *  insert to table notify
     * @Param: $aUid array user id need notify
     **/
    public function runInsertNotify($aUid, $sendNow) {
        $json_var = [];
        foreach($aUid as $uid) {
            if( !empty($uid) ){
                $mScheduleNotify = GasScheduleNotify::InsertRecord($uid, GasScheduleNotify::TYPE_TRUCK_PLAN, $this->id, '', $this->titleNotify, $json_var);
                if($sendNow && !is_null($mScheduleNotify)){
                    $mScheduleNotify->sendImmediateForSomeUser();
                }// Jan 05, 2016 tạm close send luôn lại, vì nó gây chậm bên PMBH khi send
                // sẽ gửi = cron
            }
        }
    }
    
    /**
    * @Author: DungNT Jun 22, 2019
    * @Todo: window: get data listing, list các xe bồn đc lên lịch đi lấy hàng, trả xuống window cho PM cân xe tải chọn
    * @param: $q object post params from app client
     * $q->date_from, $q->date_to format: d-m-Y
    */
    public function apiListingTruckPlan($q) {
        $aStatusOnList = [TruckPlan::STT_COMPLETE_ORDER_SUPPLIER];
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.customer_id=' . $this->customer_id);
        $criteria->addInCondition('rTruckPlan.status', $aStatusOnList);
        $criteria->addCondition('t.truck_scales_id = 0');
        $criteria->with = ['rTruckPlan'];
        $criteria->together = true;
        $criteria->order = 't.id DESC';
        $dataProvider=new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize' => 150,// Đưa vào List cache sẽ luôn load page 0, nên sẽ để 20 item 
                'currentPage' => (int)$q->page,
            ),
        ));
        return $dataProvider;
    }
    
    /** @Author: DungNT Jun 22, 2019 from Dec 26, 2018 -- handle listing api car */
    public function handleApiListTruckPlan(&$result, $q) {
        // 1. get list order by user id
        $dataProvider   = $this->apiListingTruckPlan($q);
        $models         = $dataProvider->data;
        $CPagination    = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page']   = $CPagination->pageCount;
        
        $result['message']  = 'L';
        $result['record']   = [];
        
        if( $q->page >= $CPagination->pageCount ){
            $result['record'] = [];
        }else{
            foreach($models as $model){
                $model->mAppUserLogin       = $this->mAppUserLogin;// for canPickCancel
                $result['record'][]         = $model->formatAppItemCar();
            }
        }
    }
    
    /** @Author: DungNT Dec 26, 2018
     *  @Todo: format item trả xuống list + view của PM Can window
     **/
    public function formatAppItemCar() {
        $mTruckPlan                 = $this->rTruckPlan;
        $temp = [];
        $temp['plan_id']            = $this->id;
        $temp['plan_no']            = $this->code_no;
        $temp['car_number']         = $this->getCar();
        $temp['driver_name']        = $this->getDriver();
        $temp['note']               = "ID$this->id - {$this->getPlanDate()} - {$mTruckPlan->getCreatedByNote()} - ".$this->getCreatedByNote();
        return $temp;
    }
    
    /** @Author: DungNT Jul 30, 2019
     *  @Todo: auto make thẻ kho nhập gas bồn khi thủ kho hoàn thành đơn xe bồn
     *  - Nhập mua Gas Bồn - LPG0001 Gas Bồn
     **/
    public function autoCreateStorecardOrderAgentInternal() {
        if(!$this->isAgentInternal()){
            return ;
        }
        // 1. delete old
        $this->deleteStorecardRef();
        if($this->status == TruckPlan::STT_CANCEL){
            return ;
        }
        // 2. insert new
        $mGasStoreCard = new GasStoreCard();
        $mGasStoreCard->mTruckPlanDetail = $this;
        $mGasStoreCard->autoCreateWhenTruckDoneOrderAgentInternal();
    }
    
    
    /** @Author: DungNT Jul 30, 2019
     *  @Todo: delete storecard Relate
     **/
    public function deleteStorecardRef() {
        if(empty($this->store_card_id)){
            return ;
        }
        $mStorecard = GasStoreCard::model()->findByPk($this->store_card_id);
        if($mStorecard){
            $mStorecard->delete();
        }
    }
    
}