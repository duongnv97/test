<?php

/**
 * This is the model class for table "{{_gas_schedule_sms_history}}".
 *
 * The followings are the available columns in table '{{_gas_schedule_sms_history}}':
 * @property string $id
 * @property string $smsid
 * @property integer $code_response
 * @property string $phone
 * @property string $user_id
 * @property string $username
 * @property integer $type
 * @property string $obj_id
 * @property string $title
 * @property string $json_var
 * @property string $count_run
 * @property string $time_send
 * @property string $created_date
 * @property string $created_date_on_history
 * @property string $role_id
 * @property string $network
 */
class GasScheduleSmsHistory extends BaseSpj
{
    public $report_year, $date_from, $date_to;
    const PRICE_VIETTEL = 150;
    const PRICE_OTHER   = 810;
    
    public static function getPrice($network) {
        if(empty($network)){
            return 0;
        }
        if($network == GasScheduleSms::NETWORK_VIETTEL){
            return self::PRICE_VIETTEL;
        }
        return self::PRICE_OTHER;
    }
    
    public function getArrayNetwork() {
        return array(
            GasScheduleSms::NETWORK_VIETTEL => 'Viettel',
            GasScheduleSms::NETWORK_MOBI    => 'Mobiphone',
            GasScheduleSms::NETWORK_VINA    => 'Vina',
            GasScheduleSms::NETWORK_VIETNAM_MOBILE  => 'Vietnam MB',
            GasScheduleSms::NETWORK_G_MOBILE        => 'G Mobile',
            GasScheduleSms::NETWORK_OTHER   => '#',
        );
    }
    public function getArrayType() {
        return array(
            GasScheduleSms::TYPE_UPHOLD                 => 'NV bảo trì',
            GasScheduleSms::TYPE_UPHOLD_CUSTOMER        => 'KH bảo trì',
            GasScheduleSms::TYPE_CREATE_STORE_CARD_WINDOW    => 'Đơn hàng ĐP',
            GasScheduleSms::TYPE_SETTLE_CASHIER_CONFIRM => 'Quyêt toán',
            GasScheduleSms::TYPE_CUSTOMER_INFO_LOGIN    => 'Gas24h Login',
            GasScheduleSms::TYPE_NOTIFY_G_PRICE_SETUP   => 'NB Price',
            GasScheduleSms::TYPE_FORGOT_PASS_CONFIRM    => 'Forgot Pass',
            GasScheduleSms::TYPE_NEW_PASSWORD           => 'New Pass',
            GasScheduleSms::TYPE_QUOTES_BO_MOI          => 'Quotes BoMoi',
            GasScheduleSms::TYPE_ANNOUNCE               => 'Thông báo KH',
            GasScheduleSms::TYPE_ANNOUNCE_BOMOI_PAY_DEBIT   => 'Thu nợ KH',
            GasScheduleSms::TYPE_TELESALE_CODE          => 'Telesale code',
            GasScheduleSms::TYPE_ANNOUNCE_INTERNAL      => 'Support phạt nội bộ ',
            GasScheduleSms::TYPE_GAS24H_PARTNER         => 'Code KH partner Thanh Sơn',
            GasScheduleSms::TYPE_HGD_SMS_POINT          => 'HGD SMS Reward',
        );
    }
    
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_schedule_sms_history}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('date_from, date_to, report_year, network, role_id, id, phone, user_id, username, type, obj_id, title, json_var, count_run, time_send, created_date, created_date_on_history', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUser' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'phone' => 'Phone Number',
            'user_id' => 'User',
            'username' => 'Username',
            'type' => 'Type',
            'obj_id' => 'Obj',
            'title' => 'Title',
            'json_var' => 'Json Var',
            'count_run' => 'Count Run',
            'time_send' => 'Time Send',
            'created_date' => 'Created Date',
            'created_date_on_history' => 'Created Date On History',
            'network' => 'Network',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.user_id',$this->user_id);
        $criteria->compare('t.phone', $this->phone);
        $criteria->compare('t.type',$this->type);
        $criteria->compare('t.network', $this->network);
        $criteria->order = "t.id DESC";
        $date_from = '';
        $date_to = '';
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        }
        if(!empty($date_from)){
            $criteria->addCondition("date(t.created_date_on_history)>='$date_from'");
        }
        if(!empty($date_to)){
            $criteria->addCondition("date(t.created_date_on_history)<='$date_to'");
        }
        
        $cUid = MyFormat::getCurrentUid();
        $aAllow = [GasConst::UID_PHUONG_NT, GasConst::UID_VEN_NTB];
        if(in_array($cUid, $aAllow)){
            $criteria->compare('t.type', GasScheduleSms::TYPE_CUSTOMER_INFO_LOGIN);
        }

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    /**
     * @Author: ANH DUNG Jan 05, 2016
     * @Todo: copy từ bảng schedule SMS notify sang bảng history
     * khi send thành công thì sẽ move sang history
     * @Param: $mScheduleSms model schedule notify
     */
    public static function InsertNew($mScheduleSms) {
        $mHistory = new GasScheduleSmsHistory('InsertNew');
        $aFieldNotCopy = array('id');
        MyFormat::copyFromToTable($mScheduleSms, $mHistory, $aFieldNotCopy);
        // Add new history
        $mHistory->save();
        //client was received then delete schedule notify SMS
        $mScheduleSms->delete();
    }
    
    protected function beforeSave() {
        if($this->scenario == 'UpdateNetwork'){
            return parent::beforeSave();
        }
        $mUser = $this->rUser;
        if($mUser){
            $this->role_id = $mUser->role_id;
        }
        if($this->isNewRecord){
            $this->created_date_on_history = date('Y-m-d H:i:s');
        }
        return parent::beforeSave();
    }
    
    /**
     * @Author: ANH DUNG Jun 16, 2016
     */
    public function getUserReceive() {
        $mUser = $this->rUser;
        if($mUser){
            return $mUser->getFullName();
        }
        return '';
    }
    
    /**
     * @Author: ANH DUNG Dec 25, 2016
     * @Todo: update network viettel, Mobi, Vina cho các SMS để thống kê
     * will run by cron every day once
     */
    public static function updateNetwork() {
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->addCondition('DATE_ADD(created_date_on_history, INTERVAL 10 DAY) > NOW()');
        $criteria->addCondition('network=0');
//        $criteria->addCondition('network=4');// sử dụng để fix những số phone chưa dc cập nhật đầu số phân loại 

        $criteria->limit = 5000;
        $models = GasScheduleSmsHistory::model()->findAll($criteria);
        foreach($models as $model){
            $model->doUpdateNetwork();
        }
        $to = time();
        $second = $to - $from;
        $ResultRun = "CRON updateNetwork GasScheduleSmsHistory Viettel Mobi Vina: ". count($models) . ' done in: ' . ($second) . '  Second  <=> ' . round($second / 60, 2) . ' Minutes';
        Logger::WriteLog($ResultRun);
    }
    
    /**
     * @Author: ANH DUNG Dec 25, 2016
     * @Todo: handle doing update network
     */
    public function doUpdateNetwork() {
        $phone = substr($this->phone, 2);// xóa số 84 ở đầu phone đi
        $this->findNetworkOfPhone($phone);
        $this->update(array('network'));
    }
    
    /** @Author: ANH DUNG Sep 11, 2018
     *  @Todo: find netword of phone
     *  @Param: $phone format 988180386, 1684331552
     **/
    public function findNetworkOfPhone($phone) {
        $mScheduleSms = new GasScheduleSms();
        if($mScheduleSms->isPhoneViettel($phone)){
            $this->network = GasScheduleSms::NETWORK_VIETTEL;
        }elseif($mScheduleSms->isPhoneMobi($phone)){
            $this->network = GasScheduleSms::NETWORK_MOBI;
        }elseif($mScheduleSms->isPhoneVina($phone)){
            $this->network = GasScheduleSms::NETWORK_VINA;
        }elseif($mScheduleSms->isPhoneVietNamMobile($phone)){
            $this->network = GasScheduleSms::NETWORK_VIETNAM_MOBILE;
        }elseif($mScheduleSms->isPhoneGMobile($phone)){
            $this->network = GasScheduleSms::NETWORK_G_MOBILE;
        }else{
            $this->network = GasScheduleSms::NETWORK_OTHER;
        }
        return $this->network;
    }
    
    public function getNetwork() {
        $aNetwork = $this->getArrayNetwork();
        return isset($aNetwork[$this->network]) ? $aNetwork[$this->network] : '';
    }
    public static function getNetworkV1($network) {
        $aNetwork = GasScheduleSmsHistory::model()->getArrayNetwork();
        return isset($aNetwork[$network]) ? $aNetwork[$network] : '';
    }
    public static function getTypeV1($type) {
        $aType = GasScheduleSmsHistory::model()->getArrayType();
        return isset($aType[$type]) ? $aType[$type] : '';
    }
    public function getType() {
        $aType = $this->getArrayType();
        return isset($aType[$this->type]) ? $aType[$this->type] : '';
    }
    
    /**
     * @Author: ANH DUNG Dec 25, 2016
     * @Todo: làm cái báo cáo sử dụng
     */
    public function report() {
        if(empty($this->report_year)){
            return array();
        }
        $criteria = new CDbCriteria();
        $this->addMoreCondition($criteria);
        $criteria->select   = 'count(t.id) as id, t.network, t.type, month(t.created_date_on_history) as created_date_on_history';
        $criteria->group    = 'month(t.created_date_on_history), t.network, t.type';
        $models = self::model()->findAll($criteria);
        $aRes = array();
        foreach($models as $model){
            $aRes[$model->created_date_on_history][$model->network][$model->type] = $model->id;
        }
        return $aRes;
    }
    
    public function addMoreCondition(&$criteria) {
        if(!empty($this->network)){
            $criteria->addCondition('t.network='.$this->network);
        }
        $date_from = '';
        $date_to = '';
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        }
        if(!empty($date_from)){
            $criteria->addCondition("date(t.created_date_on_history)>='$date_from'");
        }
        if(!empty($date_to)){
            $criteria->addCondition("date(t.created_date_on_history)<='$date_to'");
        }
        if(empty($this->date_from) && empty($this->date_to)){
            $criteria->addCondition('year(t.created_date_on_history)='.$this->report_year);
        }
    }
    
    
}