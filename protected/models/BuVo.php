<?php

/**
 * This is the model class for table "{{_bu_vo}}".
 *
 * The followings are the available columns in table '{{_bu_vo}}':
 * @property integer $id
 * @property integer $zone_id
 * @property string $list_materials_parent_id_gas
 * @property string $list_materials_id_vo
 * @property string $price
 * @property string $json_pair
 */
class BuVo extends CActiveRecord
{
    public $autocomplete_material, $autocomplete_material_1, $list_hide;
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_bu_vo}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('zone_id, list_materials_parent_id_gas, list_materials_id_vo', 'required', 'on'=>'create, update'),
            array('id, zone_id, list_materials_parent_id_gas, list_materials_id_vo, price, json_pair', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rFake' => array(self::BELONGS_TO, 'GasMaterials', 'zone_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                'id' => 'ID',
                'zone_id' => 'Khu vực',
                'list_materials_parent_id_gas' => 'Nhóm Gas',
                'list_materials_id_vo' => 'Thương hiệu vỏ',
                'price' => 'Số tiền bù',
                'json_pair' => 'Json Pair',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.zone_id',$this->zone_id);
        $criteria->compare('t.price',$this->price, true);
//        $criteria->order = 't.zone_id ASC, t.price';
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 50,
            ),
        ));
    }
    
    public function getListdataZone() {
        $aZone = GasOrders::getTargetZone();
        unset($aZone[GasOrders::ZONE_TARGET_MIENDONG]);
        $aZone[GasOrders::ZONE_TARGET_HCM] = 'TP HCM - Miền Đông';
        return $aZone;
    }
    
    public function getListdataMaterialParentGas() {
        return GasMaterials::getListParentByType(GasMaterialsType::MATERIAL_BINH_12KG);
    }
    public function getListdataMaterialParentGasJson() {
        $models = GasMaterials::getListParentByType(GasMaterialsType::MATERIAL_BINH_12KG, array('getModel'=>1));
        $returnVal=array();
        foreach($models as $model)
        {
            $returnVal[] = MyFunctionCustom::formatMaterialJsonItem($model, $model->price);
        }
        return CJSON::encode($returnVal);
        
    }
    /**
     * @Author: ANH DUNG Jan 17, 2017
     * @Todo: get cả parent và gas luôn
     */
    public function getListdataMaterialGas12AndParentJson() {
        $models = GasMaterials::getListParentByType(GasMaterialsType::MATERIAL_BINH_12KG, array('getModel'=>1));
        $returnVal=array();
        foreach($models as $model)
        {
            $returnVal[] = MyFunctionCustom::formatMaterialJsonItem($model, $model->price);
        }
        return CJSON::encode($returnVal);
        
    }
    public function getListdataMaterialVo() {
        $models = GasMaterials::getByMaterialType(GasMaterialsType::MATERIAL_VO_12);
        return CHtml::listData($models,'id','name');
    }
    
    public function getListdataMaterialVoJson() {
        $models = GasMaterials::getByMaterialType(GasMaterialsType::MATERIAL_VO_12);
        $returnVal=array();
        foreach($models as $model)
        {
            $returnVal[] = MyFunctionCustom::formatMaterialJsonItem($model, $model->price);
        }
        return CJSON::encode($returnVal);
        
    }
    
    public function getListdataGasJson() {
        $models = GasMaterials::getByMaterialType(GasMaterialsType::MATERIAL_BINH_12KG);
        $returnVal=array();
        foreach($models as $model)
        {
            $returnVal[] = MyFunctionCustom::formatMaterialJsonItem($model, $model->price);
        }
        return CJSON::encode($returnVal);
    }
    
    public function getListdataJsonOtherMaterial() {
        $models = GasMaterials::getByMaterialType(GasMaterialsType::$SETUP_PRICE_VATTU);
        $returnVal=array();
        foreach($models as $model)
        {
            $returnVal[] = MyFunctionCustom::formatMaterialJsonItem($model, $model->price);
        }
        return CJSON::encode($returnVal);
    }
    
    
    protected function beforeValidate() {
        $this->price = MyFormat::removeComma($this->price);
        return parent::beforeValidate();
    }
    
    public function buildJsonPair() {
        $aRes = array();
//        if($this->zone_id != GasOrders::ZONE_TARGET_TAY_NGUYEN){
        if(1){// Jan 17, 2017 tất cả các zone đều set như nhau
            foreach($this->list_materials_parent_id_gas as $parent_id){
                foreach($this->list_materials_id_vo as $materials_id){
                    $aRes[$parent_id.'-'.$materials_id] = $this->id;
                }
            }
        }elseif($this->zone_id == GasOrders::ZONE_TARGET_TAY_NGUYEN){
        // chú ý xử lý riêng cho Tây Nguyên. khi lấy ra compare ở Sell=>setBuVo() cũng phải viết lại nếu đổi cách lưu ở đây
            foreach($this->list_materials_id_vo as $materials_id){
                $aRes[$materials_id] = $this->id;
            }
        }
        
        
        $this->json_pair = json_encode($aRes);
    }
    public function formatDataSet() {
        $this->buildJsonPair();
        $this->list_materials_parent_id_gas = implode(',', $this->list_materials_parent_id_gas);
        $this->list_materials_id_vo         = implode(',', $this->list_materials_id_vo);
    }
    public function formatDataGet() {
        if(!empty($this->list_materials_parent_id_gas)){
            $this->list_materials_parent_id_gas = explode(',', $this->list_materials_parent_id_gas);
        }
        if(!empty($this->list_materials_parent_id_gas)){
            $this->list_materials_id_vo         = explode(',', $this->list_materials_id_vo);
        }
    }
    protected function beforeSave() {
        if(!$this->isNewRecord){
            $this->formatDataSet();
        }
        return parent::beforeSave();
    }
    
    /**
     * @Author: ANH DUNG Nov 22, 2016
     * @Todo: xử lý lưu tạm rồi redirect qua update để đưa dc PK vào json
     */
    public function makeNewRecord() {
        $this->scenario = null;
        $this->save();
    }

    public function getZone() {
        $aZone = GasOrders::getTargetZone();
        return isset($aZone[$this->zone_id]) ? $aZone[$this->zone_id] : '';
    }
    public function getListGas($needMore = array()) {
        $aRes = [];
        $listGas = $this->getListdataMaterialParentGas();
        $this->formatDataGet();
        if(!is_array($this->list_materials_parent_id_gas)){
            return '';
        }
        foreach($this->list_materials_parent_id_gas as $parent_id){
            $aRes[$parent_id] = isset($listGas[$parent_id]) ? $listGas[$parent_id] : '';
        }
        if(isset($needMore['getArray'])){
            return $aRes;
        }
        return implode('<br>', $aRes);
    }
    public function getListVo($needMore = array()) {
        $aRes = [];
        $listGas = $this->getListdataMaterialVo();
        if(!is_array($this->list_materials_id_vo)){
            return '';
        }
        foreach($this->list_materials_id_vo as $material_id){
            $aRes[$material_id] = isset($listGas[$material_id]) ? $listGas[$material_id] : '';
        }
        if(isset($needMore['getArray'])){
            return $aRes;
        }
        return implode('<br>', $aRes);
    }
    public function getPrice() {
        return ActiveRecord::formatCurrency($this->price);
    }
    
    /**
     * @Author: ANH DUNG Nov 22, 2016
     * @Todo: build array để đưa vào cache file, rồi lấy ra so sánh khi có tính toán bù vỏ
     * 3/ khi 1 record dc insert thì xác định record có dạng parent_id_gas - id_vỏ không
        nếu có thì mới tìm trong array setup bù vỏ
        định dạng của array setup bù vỏ
        array(
            'key_compare' có dạng pair => id_pk
            'key_price' có dạng id_pk => price
        )
     * 
     * Array
    (
    [key_compare] => Array
        (
            [3] => Array
                (
                    [25-381] => 1
                    [21-93] => 3
                    [389-93] => 3
                )

            [1] => Array
                (
                    [389-94] => 1
                    [39-136] => 1
                    [21-72] => 4
                )
        )
    [key_price] => Array
        (
            [1] => 10000023
            [2] => 50000
            [3] => 0
            [4] => 234
        )
     */
    public static function buildDataCache() {
        /* 1. get data đưa và array
         * 2. set vào Cache
         */
        $aRes   = $tmp = $tmp1 = [];
        $models = BuVo::model()->findAll();
        foreach($models as $model){
            if(isset($tmp[$model->zone_id])){
                $tmp[$model->zone_id] += json_decode($model->json_pair, true);
            }else{
                $tmp[$model->zone_id] = json_decode($model->json_pair, true);
            }
            $tmp1[$model->id]    = $model->price;
        }
        $aRes['key_compare']    = $tmp;
        $aRes['key_price']      = $tmp1;
        return $aRes;
    }
    
    /** @Author: DuongNV 4, June 2019
     *  @Todo: cron gửi mail thông báo những sell_detail chưa được set bù vỏ (admin/buVo) trong vòng 1 tháng gần đây
     *  @params: $date Y-m-d
     **/
    public function cronAlertBuVo($date = '') {
//        $cDate              = empty($date) ? strtotime(date('Y-m-d')) : strtotime($date);
        $cDate              = empty($date) ? date('Y-m-d') : $date;
        $to                 = $cDate;
        $from               = MyFormat::modifyDays($to, 1, '-', 'month');
        $criteria           = new CDbCriteria;
        $criteria->select   = 'DISTINCT t.materials_id';
        $this->getCondAlert($criteria, $from, $to);
        $criteria->order    = 't.materials_id ASC';
        $aDetails           = SellDetail::model()->findAll($criteria);
        $aMaterialId        = []; // list vỏ trong sell detail
        foreach ($aDetails as $value) {
            $aMaterialId[$value->materials_id] = $value->materials_id;
        }
        
        // Lấy toàn bộ vỏ
        $aBuVo              = BuVo::model()->findAll();
        $aVo12              = [];
        foreach ($aBuVo as $value) {
            $aVo            = explode(',', $value->list_materials_id_vo);
            $aVo12          += array_combine($aVo, $aVo);
        }
        ksort($aVo12);
        $aVo12              = array_filter($aVo12);
        
        $aMaterialIdAlert   = array_diff($aMaterialId, $aVo12); // Lấy những vỏ chưa dc set bù vỏ
        $crSellDetail       = new CDbCriteria;
        $this->getCondAlert($crSellDetail, $from, $to);
        $crSellDetail->addInCondition('t.materials_id', $aMaterialIdAlert);
        $crSellDetail->order= 't.agent_id ASC';
        $aSellDetail        = SellDetail::model()->findAll($crSellDetail);
        $this->sendEmailAlert($aSellDetail);
        $strLog = '******* cronAlertBuVo done at '.date('d-m-Y H:i:s');
        Logger::WriteLog($strLog);
    }
    
    /** @Author: DuongNV
     *  @Todo: get same cond cronAlertBuVo
     *  @Param: $from, $to Y-m-d
     **/
    public function getCondAlert(&$criteria, $from, $to){
//        $criteria->compare('t.created_date_only_bigint', $cDate);
        DateHelper::searchBetween($from, $to, 'created_date_only_bigint', $criteria);
        $criteria->compare('t.materials_type_id', GasMaterialsType::MATERIAL_VO_12);
    }
    
    public function sendEmailAlert($aSellDetail) {
        if(empty($aSellDetail)){
            return ;
        }
        // Array model sell to get code no
        $aSellId    = [];
        foreach ($aSellDetail as $value) {
            $aSellId[$value->sell_id] = $value->sell_id;
        }
        $criteria   = new CDbCriteria;
        $criteria->addInCondition('t.id', $aSellId);
        $aSell      = Sell::model()->findAll($criteria);
//        $aSell      = Sell::model()->findAllByPk($aSellId);
        $aCodeNo    = CHtml::listData($aSell, 'id', 'code_no');
        
        $titleHead  = '[CẢNH BÁO] Vỏ lạ';
        $body       = "<br><b>Danh sách vỏ lạ chưa set bù vỏ trong vòng 1 tháng gần đây: </b><br>";
        $body .= '<br><table style="border-collapse: collapse;" border="1" cellpadding="5">';
        $body .=    '<thead>';
        $body .=        '<tr>';
        $body .=            '<th>STT</th>';
        $body .=            '<th>Ngày</th>';
        $body .=            '<th>Mã đơn hàng</th>';
        $body .=            '<th>Đại lý</th>';
        $body .=            '<th>Mã vỏ</th>';
        $body .=            '<th>Tên vỏ</th>';
        $body .=        '</tr>';
        $body .=    '</thead>';
        $body .=    '<tbody>';
        $no = 1;
        // Sort tăng dần theo ngày tạo
        uasort($aSellDetail, function($a, $b){
            $created_date_a = empty($a->created_date_only) ? '' : $a->created_date_only;
            $created_date_b = empty($b->created_date_only) ? '' : $b->created_date_only;
            return $created_date_a > $created_date_b;
        });
        foreach ($aSellDetail as $mSellDetail) {
            $agent          = isset($mSellDetail->rAgent) ? $mSellDetail->rAgent->getFullName() : '';
            $mMaterial      = isset($mSellDetail->rMaterial) ? $mSellDetail->rMaterial : '';
            $materialNo     = isset($mMaterial->materials_no) ? $mMaterial->materials_no : '';
            $materialName   = isset($mMaterial->name) ? $mMaterial->name : '';
            $codeNo         = isset($aCodeNo[$mSellDetail->sell_id]) ? $aCodeNo[$mSellDetail->sell_id] : '';
            $body .=    '<tr>';
            $body .=        '<td style="text-align:center;">'.$no++.'</td>';
            $body .=        '<td>'.MyFormat::dateConverYmdToDmy($mSellDetail->created_date_only).'</td>';
            $body .=        '<td>'.$codeNo.'</td>';
            $body .=        '<td>'.$agent.'</td>';
            $body .=        '<td>'.$materialNo.'</td>';
            $body .=        '<td>'.$materialName.'</td>';
            $body .=    '</tr>';
        }
        $body .=    '</tbody>';
        $body .= '</table>';
        
        $needMore['title']     = $titleHead;
        $needMore['list_mail'] = [
            'duchoan@spj.vn', 
            'kiennn@spj.vn', 
            'dungnt@spj.vn',
            'ketoan.kv@spj.vn',
//            'thailong@spj.vn',
            'duongnv@spj.vn',
            'chienbq@spj.vn'
        ];
//        $needMore['SendNow']        = 1;// only dev debug
        SendEmail::bugToDev($body, $needMore);
    }
}