<?php

/**
 * This is the model class for table "{{_log_update}}".
 *
 * The followings are the available columns in table '{{_log_update}}':
 * @property string $id
 * @property integer $type
 * @property string $user_id
 * @property integer $role_id
 * @property string $json
 * @property string $json_after
 * @property string $created_date
 * @property string $obj_id
 */
class LogUpdate extends BaseSpj
{
    const TYPE_1_SELL               = 1;
    const TYPE_2_APP_BO_MOI         = 2;
    const TYPE_3_CASHBOOK           = 3;
    const TYPE_4_GAS_REMAIN         = 4;
    const TYPE_5_GAS_STORECARD      = 5;
    const TYPE_6_APPLY_PROMOTION            = 6;// khi code apply cho 1 đơn hàng
    const TYPE_7_USER               = 7;
    
    
    public $autocomplete_name, $date_from, $date_to;
    /** @Author: DungNT May 19, 2018
     *  @Todo: 
     *  @Param: 
     **/
    public function getArrayType() {
        return [
            LogUpdate::TYPE_1_SELL          => 'Đơn HGD',
            LogUpdate::TYPE_2_APP_BO_MOI    => 'Đơn bò mối',
            LogUpdate::TYPE_3_CASHBOOK      => 'Sổ quỹ',
            LogUpdate::TYPE_4_GAS_REMAIN    => 'Gas dư',
            LogUpdate::TYPE_5_GAS_STORECARD     => 'Thẻ kho',
            LogUpdate::TYPE_6_APPLY_PROMOTION   => 'Gas24h input code',
            LogUpdate::TYPE_7_USER          => 'Update nhân sự',
        ];
    }
    
    
    public function getTypeNotDelete() {// cron not cut data by type 
        return [
            LogUpdate::TYPE_6_APPLY_PROMOTION,
            LogUpdate::TYPE_7_USER,
        ];
    }
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return LogUpdate the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_log_update}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['obj_id, date_from, date_to, id, type, user_id, role_id, json, json_after, created_date', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'rUser' => array(self::BELONGS_TO, 'Users', 'user_id'),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Loại',
            'user_id' => 'Nhân viên',
            'role_id' => 'Role',
            'json' => 'Json',
            'json_after' => 'Json After',
            'created_date' => 'Created Date',
            'obj_id' => 'Obj Id',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
	$criteria->compare('t.type', $this->type);
	$criteria->compare('t.user_id', $this->user_id);
	$criteria->compare('t.role_id', $this->role_id);
	$criteria->compare('t.obj_id', $this->obj_id);
        if(!empty($this->date_from)){
            $dateFrom = MyFormat::dateConverDmyToYmd($this->date_from,'-');
            $criteria->addCondition("DATE(t.created_date) >= '". $dateFrom."'");
        }
        if(!empty($this->date_to)){
            $dateTo   = MyFormat::dateConverDmyToYmd($this->date_to,'-');
            $criteria->addCondition("DATE(t.created_date) <= '". $dateTo."'");
        }
        
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /** @Author: DungNT May 08, 2018
     *  @Todo: save new log
     **/
    public function addRow() {
        if(empty($this->user_id)){
            $this->user_id  = MyFormat::getCurrentUid();
            $this->role_id  = MyFormat::getCurrentRoleId();
        }
        $this->save();
    }
    
    public function getCustomerInfo($field_name=''){
        $mUser = $this->rUser;
        if($mUser){
            if($field_name != ''){
                return $mUser->$field_name;
            }
            return $mUser->first_name;
        }
        return '';
    }
    
    public function getType() {
        $aType = $this->getArrayType();
        return isset($aType[$this->type]) ? $aType[$this->type] : '';
    }
    
     /**
     * @Author: DungNT Jan 26, 2019
     * @Todo: cron clean track login
     */
    public function cronCleanLog(){
        $days_keep_track_login = 120;
        $criteria = new CDbCriteria();
        $criteria->addCondition("DATE_ADD(created_date,INTERVAL $days_keep_track_login DAY) < CURDATE()");
        $criteria->addNotInCondition('type', $this->getTypeNotDelete());
        LogUpdate::model()->deleteAll($criteria);        
    }
    
}