<?php

/**
 * This is the model class for table "{{_debts}}".
 *
 * The followings are the available columns in table '{{_debts}}':
 * @property string $id                 Id of record
 * @property string $user_id            Id of user
 * @property integer $amount            Amount money
 * @property string $reason             Reason
 * @property string $month              Month (Ex: 092018)
 * @property integer $type              Type of debts
 * @property string $relate_id          Id of relation model
 * @property string $salary_code_no     Code no of salary
 * @property integer $status            Status
 * @property string $created_date       Created date
 * @property string $created_by         Created user
 * 
 * The followings are the available model relations:
 * @property Users                  $rCreatedBy         User created this record
 * @property Users                  $rUser              User own this debt
 * @property GasText                $rGasText           GasText model
 * @property GasManageTool          $rGasManageTool     GasManageTool model
 * @property GasSettle              $rGasSettle         GasManageTool model
 */
class GasDebts extends BaseSpj {
    public $date_create_from, $date_create_to;
    //-----------------------------------------------------
    // Type of relation
    //-----------------------------------------------------
    /** Gas text */
    const TYPE_GAS_TEXT                 = '0'; // 'Văn bản'
    /** Gas manage tool */
    const TYPE_GAS_MANAGE_TOOL          = 1;// hoàn đồng phục - DungNT May2819
    /** Gas settle */
    const TYPE_GAS_SETTLE               = 2; // Tạm ứng
    /** Type other */
    const TYPE_OTHER                    = 3;// khác
    
    const TYPE_ISSUE_PUNISH             = 4; // 'Phạt Support',
    const TYPE_DEBIT_PVKH               = 5;// CHI TREO CN NV
    const TYPE_BORROW_TOOL              = 6; // mượn vật tư - DungNT May2819
    const TYPE_OTHER_SUPPORT            = 7; // Trực đêm và CPS - DungNT Jul0419
    const TYPE_CUT_SALARY               = 8; // Trừ lương QLNS - DungNT Jul1219
    const TYPE_MAKE_DEBT_LOW            = 9; // Giảm trừ công nợ - DungNT Jul1619
    const TYPE_SALARY_OF_PRODUCTS       = 10; // Lương khoán - DungNT Jul1919
    
    //-----------------------------------------------------
    // Constants
    //-----------------------------------------------------
    const MONTH_FORMAT                  = 'mY';
    //-----------------------------------------------------
    // Status
    //-----------------------------------------------------
    const STATUS_INTACTIVE              = 0;
    const STATUS_ACTIVE                 = 1;
    const STATUS_DONE                   = 2;
    
    const MONTH_PLUS_DONG_PHUC          = 12;// default 12 tháng hoàn đồng phục - DungNT Jul0519
    
    const TAB_DETAIL        = 1;
    const TAB_CUT_SALARY    = 2;
    
    //-----------------------------------------------------
    // Properties
    //-----------------------------------------------------
    public $autocomplete_user, $autocomplete_name;
    public $monthCount, $date_from, $date_to;
    public $view_cut_salary;
    
    /**
     * Get types array
     * @return Array Array type of debt
     */
    public function getTypes() {
        return array(
            GasDebts::TYPE_OTHER            => 'Khác',
            GasDebts::TYPE_GAS_TEXT         => 'Văn bản',
            GasDebts::TYPE_GAS_MANAGE_TOOL  => 'Hoàn đồng phục',
            GasDebts::TYPE_GAS_SETTLE       => 'Tạm ứng',
            GasDebts::TYPE_ISSUE_PUNISH     => 'Phạt Support',
            GasDebts::TYPE_DEBIT_PVKH       => 'Treo công nợ nhân viên',
            GasDebts::TYPE_BORROW_TOOL      => 'Mượn vật tư',
            GasDebts::TYPE_OTHER_SUPPORT    => 'Trực đêm và CPS',
            GasDebts::TYPE_CUT_SALARY               => 'Trừ lương',
            GasDebts::TYPE_MAKE_DEBT_LOW            => 'Giảm trừ công nợ',
            GasDebts::TYPE_SALARY_OF_PRODUCTS       => 'Lương khoán',
        );
    }
    
    /** @Author: DungNT Jul 12, 2019
     *  @Todo: BC công nợ cá nhân: các khoản Phát sinh nợ
     **/
    public function getArrTypeDebit() {
        // DuongNV Sep0319 Bỏ loại mượn vật tư từ t8
        $date = empty($this->date_from) ? date('Y-m-d') : $this->date_from;
        $date = date('Y-m-d', strtotime($date));
        if(MyFormat::compareTwoDate('2019-08-01', $date)){ // Nếu t7 trở về trc thì tính luôn loại mượn vật tư(ko ẩn)
            return [
                GasDebts::TYPE_GAS_TEXT, // 'Văn bản',
                GasDebts::TYPE_GAS_SETTLE,// 'Tạm ứng',
                GasDebts::TYPE_DEBIT_PVKH,// 'Treo công nợ nhân viên', ==> tao thu chi, xuat kho treo
                GasDebts::TYPE_BORROW_TOOL, // 'Mượn vật tư', DuongNV Jul1919, remove Aug2919
            ];
        }
        
        return [
            GasDebts::TYPE_GAS_TEXT, // 'Văn bản',
            GasDebts::TYPE_GAS_SETTLE,// 'Tạm ứng',
            GasDebts::TYPE_DEBIT_PVKH,// 'Treo công nợ nhân viên', ==> tao thu chi, xuat kho treo
        ];
    }
    
    /** @Author: DungNT Jul 12, 2019
     *  @Todo: BC công nợ cá nhân: các khoản Phát sinh có
     **/
    public function getArrTypeCredit() {
        return [
            GasDebts::TYPE_CUT_SALARY, // 'Trừ lương',
            GasDebts::TYPE_MAKE_DEBT_LOW, // Giảm trừ công nợ
        ];
    }
    
    /** @Author: DungNT Jul 12, 2019
     *  @Todo: các khoản sẽ trừ vào lương hàng tháng trong bc lương
     *  @used_in tham so Lỗi Support-Khác        - getUserDebt
     **/
    public function getArrTypeCutSalary() {
        return [
//            GasDebts::TYPE_CUT_SALARY, // 'Trừ lương', -- DungNT Jul3119 close
            GasDebts::TYPE_OTHER, // 'Khác',
            GasDebts::TYPE_GAS_MANAGE_TOOL, // 'Hoàn đồng phục',
            GasDebts::TYPE_ISSUE_PUNISH, // 'Phạt Support',
            GasDebts::TYPE_BORROW_TOOL, // 'Mượn vật tư', -- DungNT Jul3119 close, -- DuongNVAug2919 open
//            GasDebts::TYPE_OTHER_SUPPORT, // 'Trực đêm và CPS', // Jul1919 không cho vào đây, đưa ra công thức
        ];
    }
    
    /** @Author: DuongNV Jul1919
     *  @Todo: các loại dc hiện ở tab trừ lương trong qlcn, ko dùng cho tính lương
     **/
    public function getArrTypeCutSalaryForView() {
        return [
            GasDebts::TYPE_CUT_SALARY       => 'Trừ lương', // 'Trừ lương',
            GasDebts::TYPE_OTHER            => 'Khác',// 'Khác',
            GasDebts::TYPE_GAS_MANAGE_TOOL  => 'Hoàn đồng phục',// 'Hoàn đồng phục',
            GasDebts::TYPE_ISSUE_PUNISH     => 'Phạt Support',// 'Phạt Support',
            GasDebts::TYPE_BORROW_TOOL      => 'Mượn vật tư',// 'Mượn vật tư', DuongNV Sep0919 open
//            GasDebts::TYPE_OTHER_SUPPORT    => 'Trực đêm và CPS',// 'Trực đêm và CPS',
        ];
    }
    
    /** @Author: DuongNV Jul1919
     *  @Todo: Những loại tính vào công nợ nhân viên (tham số UsersHr->getUserDebt )
     * Close Aug019 dung chung vs ham getArrTypeCutSalary
     **/
//    public function getArrUserDebts() {
//        return [
////            GasDebts::TYPE_CUT_SALARY       => 'Trừ lương', // 'Trừ lương', // DuongNV Jul3119 bỏ loại trừ lương, vì đã có tham số trừ 30% rồi, nếu thêm thì nv sẽ bị trừ 2 lần
//            GasDebts::TYPE_OTHER            => 'Khác',// 'Khác',
//            GasDebts::TYPE_GAS_MANAGE_TOOL  => 'Hoàn đồng phục',// 'Hoàn đồng phục',
//            GasDebts::TYPE_ISSUE_PUNISH     => 'Phạt Support',// 'Phạt Support',
//        ];
//    }
    
    /** @Author: DungNT Jul3019
     *  @Todo: Những loại ko cho sửa
     **/
    public function getArrTypeLockUpdate() {
        return [
            GasDebts::TYPE_CUT_SALARY,
        ];
    }
    
    /** @Author: DungNT Jul 12, 2019
     *  @Todo: user đc tạo Trừ lương ở QLNS
     **/
    public function getUidCutSalary() {
        return [
            GasConst::UID_HUE_LT,
            GasConst::UID_NHI_DT,
            GasConst::UID_NGUYEN_DTM,
            GasLeave::UID_VIEW_ALL_TAY_NGUYEN_GIA_LAI,
        ];
    }
    
    /** @Author: DuongNV Aug0919
     *  @Todo: những loại ẩn trong BC chi tiết CNCN, giao diện xem lương của nhân viên.
     **/
    public function getArrTypeHideInDetail() {
        // DuongNV Sep0319 Bỏ loại mượn vật tư từ t8
        $date = empty($this->date_from) ? date('Y-m-d') : $this->date_from;
        $date = date('Y-m-d', strtotime($date));
        if(MyFormat::compareTwoDate('2019-08-01', $date)){ // Nếu t7 trở về trc thì tính luôn loại mượn vật tư(ko ẩn)
            return [
                GasDebts::TYPE_OTHER,  
                GasDebts::TYPE_GAS_MANAGE_TOOL,
                GasDebts::TYPE_ISSUE_PUNISH,
                GasDebts::TYPE_OTHER_SUPPORT,
            ];
        }
        
        return [
            GasDebts::TYPE_OTHER,  
            GasDebts::TYPE_GAS_MANAGE_TOOL,
            GasDebts::TYPE_ISSUE_PUNISH,
            GasDebts::TYPE_OTHER_SUPPORT,
            GasDebts::TYPE_BORROW_TOOL,
        ];
    }
    
    /** @Author: DuongNV Jul2519
     *  @Todo: những role_id xem all user bc quản lý công nợ
     **/
    public static function getArrRoleViewAllIndex() {
        return [
            ROLE_ACCOUNTING, 
            ROLE_CHIEF_ACCOUNTANT, 
            ROLE_ADMIN, 
        ];
    }
    /** @Author: DuongNV Jul2519
     *  @Todo: những role_id xem PVKH/CCS/PTTT quản lý công nợ
     **/
    public static function getArrRoleViewSomeIndex() {
        return [
            ROLE_MONITORING, 
            ROLE_MONITORING_MARKET_DEVELOPMENT, 
            ROLE_ACCOUNTING_ZONE
        ];
    }
    
    /** @Author: DungNT Jul 02, 2019
     *  @Todo: Hide type with some user
     **/
    public function getTypesOnWebCreate() {
        $aUidShow       = GasConst::getAdminArray();
        $aUidShow[]     = GasConst::UID_CHAU_LNM;
        $cUid           = MyFormat::getCurrentUid();
        $cRole          = MyFormat::getCurrentRoleId();
        $aType          = $this->getTypes();
        unset($aType[GasDebts::TYPE_GAS_TEXT]);
//        unset($aType[GasDebts::TYPE_GAS_SETTLE]);
        if(!in_array($cUid, $aUidShow)){
            unset($aType[GasDebts::TYPE_GAS_SETTLE]); // DuongNV Oct0419 mở loại tạm ứng cho chị Châu
            unset($aType[GasDebts::TYPE_ISSUE_PUNISH]);
            unset($aType[GasDebts::TYPE_OTHER]);
            if(!in_array($cUid, $this->getUidCutSalary())){
                unset($aType[GasDebts::TYPE_CUT_SALARY]);
            }
        }
        if($cRole == ROLE_ACCOUNTING_ZONE){
            unset($aType[GasDebts::TYPE_MAKE_DEBT_LOW]);
        }
        return $aType;
    }
    
    /** @Author: DungNT Jul 04, 2019
     *  @Todo: get Select month with some user
     **/
    public function getArrayMonthOnWebCreate() {
        $aUidShow   = GasConst::getAdminArray();
        $aUidShow[] = GasConst::UID_CHAU_LNM;
        $cUid       = MyFormat::getCurrentUid();
        $aNumberMonth = MyFormat::BuildNumberOrder(360);
        if(!in_array($cUid, $aUidShow)){
            $aNumberMonth = MyFormat::BuildNumberOrder(1);
        }
        return $aNumberMonth;
    }
    
    /** @Todo: array type là số âm  **/
    public function getTypeNegative() {
        return [
            GasDebts::TYPE_GAS_MANAGE_TOOL,
//            GasDebts::TYPE_OTHER_SUPPORT,// DugnNT Close Jul1919 -- đưa vào 1 tham số
        ];
    }
    
    /**
     * Get status array
     * @return Array Array status of debt
     */
    public function getStatus() {
        return array(
            GasDebts::STATUS_INTACTIVE      => 'Inactive',
            GasDebts::STATUS_ACTIVE         => 'Hoạt động',
            GasDebts::STATUS_DONE           => 'Hoàn tất',
        );
    }
    
    /** @Author: DungNT Jul 10, 2019
     *  @Todo: day allow user select at datepicker
     **/
    public function getDayLimit() {
        $mUsersProfile  = new UsersProfile();
        $cUid           = MyFormat::getCurrentUid();
        $aDaysLock  = [1,2,3,4,5];
        $cDay       = date('d')*1;
        if(!in_array($cDay, $aDaysLock) && !in_array($cUid, $mUsersProfile->getListUidUpdateAll())){
            $cDay--;// Jul1919 ChauLNM cho chọn trong tháng
            return "-$cDay";
        }
        $days = -5;
        if(in_array($cUid, $mUsersProfile->getListUidUpdateAll())){
            $days = -15;
        }
        return $days;
    }
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasDebts the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{_debts}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('user_id, month, type', 'required', 'on' => 'create, update'),
            array('date_from, date_to, id, user_id, amount, reason, month, type, relate_id, status, created_date, created_by, monthCount', 'safe'),
            array('date_create_from, date_create_to', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'rUser' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'rCreatedBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'rGasText' => array(
                self::BELONGS_TO, 'GasText', 'relate_id',
//                'on'    => 'type = ' . self::TYPE_GAS_TEXT,
            ),
            'rGasManageTool' => array(
                self::BELONGS_TO, 'GasManageTool', 'relate_id',
//                'on'    => 'type = ' . self::TYPE_GAS_MANAGE_TOOL,
            ),
            'rGasSettle' => array(
                self::BELONGS_TO, 'GasSettle', 'relate_id',
//                'on'    => 'type = ' . self::TYPE_GAS_SETTLE,
            ),
            'rEmployeeCashbook' => array(
                self::BELONGS_TO, 'EmployeeCashbook', 'relate_id',
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id'            => 'ID',
            'user_id'       => 'Người thực hiện',
            'amount'        => 'Số tiền',
            'reason'        => 'Lý do/Diễn giải',
            'month'         => 'Tháng thực hiện',
            'type'          => 'Loại',
            'relate_id'     => 'Id liên quan',
            'status'        => 'Trạng thái',
            'created_date'  => 'Ngày tạo',
            'created_by'    => 'Người tạo',
            'monthCount'    => 'Số tháng hoàn trả',
            'date_from'     => 'Từ ngày',
            'date_to'       => 'Đến',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        $aRoleViewAll = GasDebts::getArrRoleViewAllIndex();
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        $criteria = new CDbCriteria;
        $criteria->compare('t.user_id', $this->user_id);
        $criteria->compare('t.month', $this->month);
        $criteria->compare('t.type', $this->type);
        if($this->view_cut_salary){ // Nếu là tab trừ lương thì chỉ search những loại trừ lương thôi
            $criteria->addInCondition('t.type', array_keys($this->getArrTypeCutSalaryForView()));
        }
        // Tab trừ lương giới hạn user xem
        if(!in_array($cRole, $aRoleViewAll)){
            $criteria->addCondition("t.created_by=$cUid OR t.user_id=$cUid");
        }
        $criteria->compare('t.relate_id', $this->relate_id);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('t.created_by', $this->created_by);
        
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
            $criteria->addCondition("t.month >= '$date_from'");
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
            $criteria->addCondition("t.month <= '$date_to'");
        }
        
        // search created date
        if(!empty($this->date_create_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_create_from);
            $criteria->addCondition("DATE(t.created_date) >= '$date_from'");
        }
        if(!empty($this->date_create_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_create_to);
            $criteria->addCondition("DATE(t.created_date) <= '$date_to'");
        }
        
        $criteria->order = 't.id DESC';
//        $criteria->order = 't.month DESC';
        $_SESSION['data-excel-debts'] = new CActiveDataProvider($this, array(
            'pagination'=>false,
            'criteria'=>$criteria,
        ));
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    //-----------------------------------------------------
    // Override methods
    //-----------------------------------------------------
    /**
     * Before save
     * @return boolean Parent methods return
     */
    public function beforeSave() {
        if ($this->isNewRecord) {
            if (empty($this->created_by)) {
                $this->created_by = MyFormat::getCurrentUid();
            }
        }
        return parent::beforeSave();
    }
    
    /**
     * Can update
     * @return boolean
     */
    public function canUpdate() {
        if(in_array($this->type, $this->getArrTypeLockUpdate())){
            return false;
        }
        $cRole          = MyFormat::getCurrentRoleId();
        $cUid           = MyFormat::getCurrentUid();
        $today          = date('Y-m-d');
        $aUidAllow      = [GasLeave::UID_CHIEF_ACCOUNTANT, GasConst::UID_CHAU_LNM, GasConst::UID_ADMIN];
        $aRoleAllowFull = [ROLE_ADMIN];
        if(in_array($cRole, $aRoleAllowFull)){
            return true;
        }
        if(in_array($cUid, $aUidAllow)){
            $dayAllow   = MyFormat::modifyDays($today, 60, '-');
            return MyFormat::compareTwoDate($this->created_date, $dayAllow);
        }
        if($cUid != $this->created_by){
            return false;
        }
        // DungNT Aug1219 ko cho sửa phiếu treo CN cá nhân sinh tự động từ thẻ kho
        /*
        $aTypeLock = [GasDebts::TYPE_DEBIT_PVKH];
        if(in_array($this->type, $aTypeLock) && !empty($this->relate_id)){
            return false;
        } Aug1519 DungNT close - allow user sửa trong 1 ngày */ 
        
        $dayAllow = MyFormat::modifyDays($today, 1, '-');
        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }
    
    //-----------------------------------------------------
    // Utility methods
    //-----------------------------------------------------
    public function getUserName() {
        if (isset($this->rUser)) {
            return $this->rUser->getFullName();
        }
        return '';
    }
    
    /**
     * Get amount formated
     * @return String
     */
    public function getAmount() {
        return CmsFormatter::formatCurrency($this->amount);
    }
    
    /**
     * Get relation information
     * @return string
     */
    public function getRelationInfo() {
        $retVal = '';
        $name = 'link';
        switch ($this->type) {
            case self::TYPE_GAS_TEXT:
                $retVal = Yii::app()->createAbsoluteUrl('admin/gasText/text_view', array(
                    'id'    => $this->relate_id,
                ));
                if (isset($this->rGasText)) {
                    $name = $this->rGasText->code_no;
                }
                
                break;
            case self::TYPE_GAS_MANAGE_TOOL:
//                $retVal = Yii::app()->createAbsoluteUrl('admin/gasManageTool/view', array(
//                    'id'    => $this->relate_id,
//                ));
//                if (isset($this->rGasManageTool)) {
//                    $name = $this->rGasManageTool->code_no;
//                } DungNT close May2819
                $name = '';
                break;
            case self::TYPE_GAS_SETTLE:
                $retVal = Yii::app()->createAbsoluteUrl('admin/gasSettle/view', array(
                    'id'    => $this->relate_id,
                ));
                if (isset($this->rGasSettle)) {
                    $name = $this->rGasSettle->code_no;
                }
                break;
            default:
                break;
        }
        if(empty($retVal)){
            return '';
        }
        return '<a target="_blank" href="' . $retVal . '">' . $name . '</a>';
    }
    
    /**
     * Get created user
     * @return string
     */
    public function getCreatedBy() {
        if (isset($this->rCreatedBy)) {
            return $this->rCreatedBy->getFullName();
        }
        return '';
    }
    
    /**
     * Get month value
     * @return String Month value in format 'm/Y'
     */
    public function getMonth() {
        return CommonProcess::convertDateTime($this->month, DomainConst::DATE_FORMAT_4, DomainConst::DATE_FORMAT_12);
    }
    
    /** @Author: ANH DUNG Oct 26, 2018
     *  @Todo: handle post create multi
     **/
    public function handleCreateMulti() {
        foreach ($_POST['GasDebts']['user_id'] as $key => $user_id) {
            $mDebts = new GasDebts('create');
            $mDebts->user_id    = $user_id;
            $mDebts->amount     = isset($_POST['GasDebts']['amount'][$key]) ? MyFormat::removeComma($_POST['GasDebts']['amount'][$key]) : 0;
            $mDebts->reason     = isset($_POST['GasDebts']['reason'][$key]) ? $_POST['GasDebts']['reason'][$key] : '';
            $mDebts->month      = isset($_POST['GasDebts']['month'][$key]) ? $_POST['GasDebts']['month'][$key] : '';
            $mDebts->monthCount = isset($_POST['GasDebts']['monthCount'][$key]) ? $_POST['GasDebts']['monthCount'][$key] : 1;
            $mDebts->type       = isset($_POST['GasDebts']['type'][$key]) ? $_POST['GasDebts']['type'][$key] : GasDebts::TYPE_OTHER;
            if($mDebts->type == GasDebts::TYPE_GAS_MANAGE_TOOL){
                $mDebts->monthCount = GasDebts::MONTH_PLUS_DONG_PHUC;
            }
            if (empty($user_id) || empty($mDebts->month)) {
                continue ;
            }
            // 1. tạo tháng đầu tiên trc, --> AnhDung remove Oct2618
//                    $mDebts->validate();
//                    if (!$mDebts->hasErrors()) {
//                        $mDebts->save();
//                    }
            // 2. tạo tháng tiếp theo nếu có
            if ($mDebts->monthCount >= 1) { // Input multi gas debts
                $mDebts->amount = $mDebts->amount / $mDebts->monthCount;
                $mDebts->createMulti();
            }
        }
    }
    
    /**
     * Create multi gas debts
     */
    public function createMulti() {
        $format     = DomainConst::DATE_FORMAT_4;
        $month      = CommonProcess::convertDateTime($this->month, DomainConst::DATE_FORMAT_12, $format);
//        $nextMonth  = CommonProcess::getNextMonthOfDate($month, $format);
        $nextMonth  = CommonProcess::getFirstDateOfMonth($month);// Oct2618 AnhDung move all to one create, ko get next month nữa
        $lastMonth  = CommonProcess::getNextMonthOfDate($month, $format, $this->monthCount - 1);
        $period     = CommonProcess::getMonthPeriod($nextMonth, $lastMonth);
        foreach ($period as $dt) {
            $model = new GasDebts();
            $model->user_id = $this->user_id;
            $model->amount  = $this->amount;
            $model->reason  = $this->reason;
            $model->month   = $dt->format($format);
            $model->type    = $this->type;
            $model->relate_id   = '';
            $model->validate();
            $model->convertNumberToNegative();
            if (!$model->hasErrors()) {
                $model->save();
            } else {
                throw new Exception('Có lỗi xảy ra: '. json_encode($model->getErrors()));
            }
        }
    }
    
    //-----------------------------------------------------
    // Static methods
    //-----------------------------------------------------
    
    public function getStatusText() {
        $aStatus = $this->getStatus();
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : '';
    }
    public function getTypesText() {
        $aStatus = $this->getTypes();
        return isset($aStatus[$this->type]) ? $aStatus[$this->type] : '';
    }
    /**
     * Create gas_debts records from gas_text
     * @param GasText $mGasText Model gas text
     * @param Int $monthCount Number of month user should pay back money
     * @param Int $amount Amount money 
     * @param String $userId Id of user
     */
    public function createFromGasText($mGasText, $monthCount, $amount, $userId) {
        $amount     = $amount / $monthCount;
        $mMyFormat  = new MyFormat();
        $dateBegin  = MyFormat::dateConverYmdToDmy($mGasText->date_published, 'Y-m-d');
        $aMonth     = $mMyFormat->plusMonth($dateBegin, $monthCount);
        foreach ($aMonth as $dateYmd) {
            $model = new GasDebts();
            $model->user_id     = $userId;
            $model->amount      = $amount;
            $model->reason      = 'Thưởng phạt bởi quyết định số: ' . $mGasText->code_no;
            $model->month       = $dateYmd;
            $model->type        = GasDebts::TYPE_GAS_TEXT;
            $model->relate_id   = $mGasText->id;
            $model->save();
        }
    }
    
    /**
     * @param GasManageTool $mGasManageTool
     * @param type $monthCount
     */
    public function createFromGasManageTool($mGasManageTool, $mGasManageToolDetail, $monthCount) {
        // Apr2319 function delete was call before run this, do hàm inser này nằm trong vòng foreach nên cái inser sau sẽ xóa cái trc đi
        $amount     = $mGasManageToolDetail->getAmount() / $monthCount;
        $format     = DomainConst::DATE_FORMAT_4;
        $month      = $mGasManageTool->date_allocation;
        $nextMonth  = $month;       // Current month
        $lastMonth  = CommonProcess::getNextMonthOfDate($month, $format, $monthCount - 1);
        $period     = CommonProcess::getMonthPeriod($nextMonth, $lastMonth);
        foreach ($period as $dt) {
            $model = new GasDebts();
            $model->user_id = $mGasManageTool->employee_id;
            $model->amount  = $amount;
            $model->reason  = 'Hoàn trả tiền mượn vật tư: ' . $mGasManageTool->code_no;
            $model->month   = $dt->format($format);
            $model->type    = GasDebts::TYPE_GAS_MANAGE_TOOL;
            $model->relate_id   = $mGasManageTool->id;
            $model->save();
        }
    }
    
    /** @Author: DungNT Mar 18, 2019
     *  @Todo: remove by settle id
     **/
    public function deleteTypeSettle($mSettle) {
        // Remove old
        $this->relate_id    = $mSettle->id;
        $this->type         = GasDebts::TYPE_GAS_SETTLE;
        $this->deleteByRelateId();
    }
    
    /**
     * Create debts from gas settles
     * @param GasSettle $mSettle
     * @param Int $monthCount
     */
    public function createFromGasSettle($mSettle, $monthCount) {
        // Remove old
        $this->deleteTypeSettle($mSettle);
        
        if($monthCount < 1 || $mSettle->type_pre_settle != GasSettle::TYPE_PRE_SETTLE_CUT_SALARY){
            return ;
        }        
        $amount     = $mSettle->getAmountValue() / $monthCount;
//      DungNT close Jun0719 $format     = DomainConst::DATE_FORMAT_4;
//        $nextMonth  = CommonProcess::getNextMonth($format);
//        $lastMonth  = CommonProcess::getNextMonth($format, $monthCount);
//        $period     = CommonProcess::getMonthPeriod($nextMonth, $lastMonth);
        $mMyFormat  = new MyFormat();
        $dateBegin  = MyFormat::dateConverYmdToDmy($mSettle->cashier_confirm_date, 'Y-m-d');
        $aMonth     = $mMyFormat->plusMonth($dateBegin, $monthCount);
        foreach ($aMonth as $dateYmd) {
            $model = new GasDebts();
            $model->user_id = $mSettle->uid_login;
            $model->amount  = $amount;
            $model->reason  = 'Hoàn trả tiền tạm ứng : ' . $mSettle->code_no;
            $model->month   = $dateYmd;
            $model->type    = GasDebts::TYPE_GAS_SETTLE;
            $model->relate_id   = $mSettle->id;
            $model->save();
        }
    }
    
    /** Jan2519 DungNT
     * Create debts from EmployeeCashbook
     * @todo: lam chi Treo Cong no tren web cho KTKV, không để PVKH chi nữa
     * @param model EmployeeCashbook $mEmployeeCashbook
     * @param Int $monthCount
     */
    public function createFromEmployeeCashbook($mEmployeeCashbook, $monthCount) {
        // Remove old
        $this->relate_id    = $mEmployeeCashbook->id;
        $this->type         = GasDebts::TYPE_DEBIT_PVKH;
        $this->deleteByRelateId();
        
        if($monthCount < 1){
            return ;
        }        
        $amount     = $mEmployeeCashbook->getAmount() / $monthCount;
        $format     = DomainConst::DATE_FORMAT_4;
        $nextMonth  = CommonProcess::getNextMonth($format);
        $lastMonth  = CommonProcess::getNextMonth($format, $monthCount);
        $period     = CommonProcess::getMonthPeriod($nextMonth, $lastMonth);
        foreach ($period as $dt) {
            $model = new GasDebts('create');
            $model->user_id     = $mEmployeeCashbook->employee_id;
            $model->amount      = $amount;
            $model->reason      = "Treo công nợ : $mEmployeeCashbook->code_no $mEmployeeCashbook->note";
//            $model->month       = $dt->format($format);
            $model->month       = $mEmployeeCashbook->date_input;// Aug0719 DungNT fix
            $model->type        = GasDebts::TYPE_DEBIT_PVKH;
            $model->relate_id   = $mEmployeeCashbook->id;
            $model->created_by  = $mEmployeeCashbook->uid_login;
            $model->save();
        }
    }
    
    /**
     * Get user debt value
     * @param String $from From date (Format: Y-m-d)
     * @param String $to To date (Format: Y-m-d)
     * @param String $userId Id of user
     * @return Int Debt value
     */
    public static function getUserDebt($from, $to, $userId) {
        $fromYear = CommonProcess::convertDateTime($from, DomainConst::DATE_FORMAT_4, 'Y');
        $fromMonth = CommonProcess::convertDateTime($from, DomainConst::DATE_FORMAT_4, 'm');
        $toYear = CommonProcess::convertDateTime($to, DomainConst::DATE_FORMAT_4, 'Y');
        $toMonth = CommonProcess::convertDateTime($to, DomainConst::DATE_FORMAT_4, 'm');
        $criteria = new CDbCriteria();
        $criteria->addCondition('YEAR(month) >= ' . $fromYear . ' AND MONTH(month)>=  '. $fromMonth
                . ' AND YEAR(month) <= ' . $toYear . ' AND MONTH(month) <= ' . $toMonth);
        $criteria->addCondition('user_id=' . $userId);
        $models = self::model()->findAll($criteria);
        $retVal = 0;
        foreach ($models as $model) {
            $retVal += $model->amount;
        }
        return $retVal;
    }
    
    /** @Author: ANH DUNG Now 05 2018
     *  @Todo: make from 
     *  @param: $listdata array user_id => money
     *  @param: $dateApply date Y-m-d 
     **/
    public function saveMultiPunishUser($listdata) {
        $tableName      = GasDebts::model()->tableName();
        if(empty($this->created_date)){
            $this->created_date = date('Y-m-d H:i:s');
        }
        $aRowInsert     = [];
        $this->created_by = !empty($this->created_by) ? $this->created_by : GasConst::UID_ADMIN;
        foreach ($listdata as $user_id => $money) {
            if(empty($user_id)){
                continue ;
            }
            $money = $money;// trừ tiền phải đưa vào số âm 
            $aRowInsert[] = "(
                        '{$user_id}',
                        '{$money}',
                        '$this->reason',
                        '{$this->month}',
                        '{$this->type}',
                        '{$this->created_date}',
                        '{$this->created_by}'
                        )";
        }
        
        $sql = "insert into $tableName (
                        user_id,
                        amount,
                        reason,
                        month,
                        type,
                        created_date,
                        created_by
                        ) values " . implode(',', $aRowInsert);
        if (count($aRowInsert)){
            Yii::app()->db->createCommand($sql)->execute();
        }
    }    
    
    
    /**
     * Get number of month that debts was split 
     * @return Int Month count
     */
    public static function getMonthCount($user_id, $relate_id) {
        if (!empty($relate_id)) {
            $criteria = new CDbCriteria();
            $criteria->compare('relate_id', $relate_id);
            $criteria->compare('user_id', $user_id);
//            $models = self::model()->findAll($criteria);
//            $count = count($models);
            $count = self::model()->count($criteria);
            if ($count > 0) {
                return $count;
            }
        }
        return 1;
    }
    
    public static function getAmountTotal($user_id, $relate_id) {
        $retVal = 0;
        if (!empty($relate_id)) {
            $criteria = new CDbCriteria();
            $criteria->compare('relate_id', $relate_id);
            $criteria->compare('user_id', $user_id);
            $models = self::model()->findAll($criteria);
            foreach ($models as $model) {
                $retVal += $model->amount;
            }
        }
        return $retVal;
    }
    
    /** @Author: ANH DUNG Oct 23, 2018
     *  @Todo: remove by relate id
     **/
    public function deleteByRelateId() {
        if(empty($this->relate_id)){
            return ;
        }
        $criteria = new CDbCriteria();
        $criteria->compare('relate_id', $this->relate_id);
        $criteria->compare('type', $this->type);
        $models = GasDebts::model()->findAll($criteria);
        Users::deleteArrModel($models);
    }
    
    /**
     * Get total value
     * @param type $records
     * @param type $column
     * @return type
     */
    public function getTotal($records, $column) {
        $total = 0;
        foreach ($records as $record) {
            $total += $record->$column;
        }
        return $total;
    }
    
    /** @Author: ANH DUNG Oct 31, 2018
     *  @Todo: format before render view
     **/
    public function formatDataBeforeUpdate() {
        $this->month = MyFormat::dateConverYmdToDmy($this->month);
    }
    
    /** @Author: ANH DUNG Oct 31, 2018
     *  @Todo: format before save to db
     **/
    public function formatDataBeforeSave() {
        $this->amount = MyFormat::removeComma($this->amount);
        if(strpos($this->month, '/')){
            $this->month = MyFormat::dateConverDmyToYmd($this->month);
            MyFormat::isValidDate($this->month);
        }
        $this->convertNumberToNegative();
    }
    
    /** @Author: DungNT May 28, 2019
     *  @Todo: xử lý Hoàn đồng phục luôn là số âm (default)
     **/
    public function convertNumberToNegative() {
        if(in_array($this->type, $this->getTypeNegative()) && $this->amount > 0){
            $this->amount = $this->amount * -1;
        }
    }
    
    /** Jul0319 DungNT
     * Create debts from GasStoreCard
     * @todo: lam chi Treo Cong no tren App PTTT cho KTKV
     * @param model GasStoreCard $mGasStoreCard
     * @param Int $monthCount
     */
    public function createFromStoreCard($mGasStoreCard) {
        $this->relate_id    = $mGasStoreCard->id;
        $this->type         = GasDebts::TYPE_DEBIT_PVKH;
        $this->deleteByRelateId();// delete before save
        $totalDebts         = $mGasStoreCard->calcDebtsPvkh();
        if($mGasStoreCard->type_in_out != STORE_CARD_TYPE_24 || $totalDebts == 0){// Xuất kho treo CN trừ lương
            return ;
        }
        $mUserPvkh          = $mGasStoreCard->getPvkhFromAgent();
        if(empty($mUserPvkh)){ return false; }
        $this->user_id      = $mUserPvkh->id;
        $this->amount       = $totalDebts;
        $this->reason       = "{$mGasStoreCard->getUidLogin()} treo công nợ PVKH $mUserPvkh->first_name ".ActiveRecord::formatCurrency($totalDebts)." Mã thẻ kho: $mGasStoreCard->store_card_no. $mGasStoreCard->note";
        $this->month        = date('Y-m-d', strtotime($mGasStoreCard->created_date));
        $this->json         = $mGasStoreCard->jsonDetail;
        $this->status       = GasDebts::STATUS_ACTIVE;
        $this->created_by   = $mGasStoreCard->uid_login;
        $this->save();
        $this->notifyEmployee();
    }
    
    /** Jul0319 DungNT
     *  notify cho các giao nhận của đại lý đó khi có bảo trì của CallCenter put xuống
     */
    public function notifyEmployee() {
        if(empty($this->titleNotify)){
            $this->titleNotify = $this->reason;
        }
        $aUid[] = $this->user_id;
        $aUid[] = $this->created_by;
//        $this->runInsertNotify($aUid, true); // Send Now
        $this->runInsertNotify($aUid, false); // Send By Cron
    }
    
    /** @Author: Jul0319 DungNT
     *  insert to table notify
     * @Param: $aUid array user id need notify
     **/
    public function runInsertNotify($aUid, $sendNow) {
        $json_var = [];
        foreach($aUid as $uid) {
            if( !empty($uid) ){
                $mScheduleNotify = GasScheduleNotify::InsertRecord($uid, GasScheduleNotify::TRANSACTION_CHANGE_EMPLOYEE, $this->id, '', $this->titleNotify, $json_var);
                if($sendNow && !is_null($mScheduleNotify)){
                    $mScheduleNotify->sendImmediateForSomeUser();
                }// Jan 05, 2016 tạm close send luôn lại, vì nó gây chậm bên PMBH khi send
                // sẽ gửi = cron
            }
        }
    }
    
    /** @Author: DuongNV Jul3119
     *  @Todo: Chuyển trừ 30% lương vào công nợ, loại trừ lương
     *  @param: $aAmount[ uid => amount ]
     *  @param: $mHrSalaryReport->date     Y-m-d
     *  @param: $mHrSalaryReport->code_no  code no of salary report
     **/
    public function createFromSalary($aAmount, $mHrSalaryReport) {
        if(empty($mHrSalaryReport->code_no)) return false;
        $date = date('Y-m-d', strtotime($mHrSalaryReport->end_date));
        $cUid = MyFormat::getCurrentUid();
        $criteria = new CDbCriteria;
        $criteria->compare('salary_code_no', $mHrSalaryReport->code_no);
        $criteria->compare('month', $date);
        $criteria->compare('type', GasDebts::TYPE_CUT_SALARY);
        $numRowDelete = GasDebts::model()->deleteAll($criteria);
        Logger::WriteLog($numRowDelete .' rows GasDebts deleted | type: '.GasDebts::TYPE_CUT_SALARY.' | month: '.$date);
        
        foreach ($aAmount as $uid => $amount) {
            if(empty($amount) || empty($uid)) continue;
            $mDebts                 = new GasDebts();
            $mDebts->user_id        = $uid;
            $mDebts->amount         = $amount;
            $mDebts->reason         = 'Trừ công nợ T'.date('n/Y', strtotime($date));
            $mDebts->month          = $date;
            $mDebts->type           = GasDebts::TYPE_CUT_SALARY;
            $mDebts->status         = GasDebts::STATUS_ACTIVE;
            $mDebts->created_by     = $cUid;
            $mDebts->salary_code_no = $mHrSalaryReport->code_no;
            $mDebts->save();
        }
    }

}
