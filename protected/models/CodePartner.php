<?php

/**
 * This is the model class for table "{{_code_partner}}".
 *
 * The followings are the available columns in table '{{_code_partner}}':
 * @property string $id
 * @property integer $type
 * @property string $code_no
 * @property integer $agent_id
 * @property integer $sell_id
 * @property integer $customer_id
 * @property integer $status
 * @property integer $apply_date
 * @property string $apply_date_bigintr
 * @property integer $apply_by_user
 * @property string $created_date
 * @property string $created_date_bigint
 * @property integer $sms_phone
 * @property string $json
 */
class CodePartner extends BaseSpj
{
    const STATUS_NEW        = 1;
    const STATUS_COMPLETE   = 2;
    const STATUS_EXPIRED    = 3;
    const STATUS_CANCEL     = 4;
    
    const API_LISTING_EXCHANGE_REWARD_PER_PAGE = 10;
    
    const MAX_TIME_INPUT    = 50;// Max số lần cho phép nhập mã code trên app, nếu sai liên tục 5 lần sẽ bị lock lại luôn, ko cho nhập nữa
    const DAY_EXPIRED       = 2;// số ngày mã code hết hạn 
    
    const TYPE_XANG         = 1;// code free xăng
    const TYPE_REWARD       = 2;//reward from web
    const TYPE_REWARD_APP   = 3;//reward from app
    
    public $sms_content, $first_name, $address, $date_from_ymd, $date_to_ymd, $date_from_ymd_apply, $date_to_ymd_apply, $date_from, $date_to, $date_from_apply, $date_to_apply;
    public $autocomplete_name,  $autocomplete_name_3, $autocomplete_name_1, $autocomplete_name_2, $autocomplete_name_4 ;
    
    const AGENT_THANH_SON       = 1773595;// id agent
    
    //    get list type reward in code partner
    public function getArrayTypeReward(){
        return [
            CodePartner::TYPE_REWARD,
            CodePartner::TYPE_REWARD_APP,
        ];
    }
    
    public function getArrayStatus() {
        return [
            CodePartner::STATUS_NEW         => 'Mới',
            CodePartner::STATUS_COMPLETE    => 'Đã sử dụng',
            CodePartner::STATUS_EXPIRED     => 'Hết hạn',
            CodePartner::STATUS_CANCEL      => 'Hủy',
        ];
    }
    
    public function getArrayType(){
        return[
        CodePartner::TYPE_XANG => 'Code xăng',
        CodePartner::TYPE_REWARD => 'Quà tặng',
        CodePartner::TYPE_REWARD_APP => 'Quà tặng Từ App',
        ];
    }
    
    /** @Author: DungNT Now 23, 2018
     *  @Todo: get array allow make code Xang
     **/
    public function getArrayAgentCodeXang() {
        return [
//            100, // BT2 dev test 
//        CodePartner::AGENT_THANH_SON, // ĐLLK Thanh Sơn 1 -- close Feb1519
        ];
    }
    
    /** @Author: DungNT Dec 01, 2018
     *  @Todo: get list id user only view App Menu SJ CODE 
     * 1/ User input code Cay Xang Thanh Son
     **/
    public function getUidLimitMenu() {
        return [
//            138559 => 138559, // Trần Văn Hoàn -- server test
            1790169 => 1790169, // Nguyễn Thị Kiều Trinh -- chỉ show menu SJ CODE
            1790173 => 1790173, // Phan Thị Sâm
        ];
    }
    /** @Author: DungNT Dec 01, 2018
     *  @Todo: get list id user ko gửi notify
     * 1/ User input code Cay Xang Thanh Son
     **/
    public function getUidTurnOffNotify() {
        return $this->getUidLimitMenu();
    }
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CodePartner the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_code_partner}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['id, type, code_no, agent_id, sell_id, customer_id, status, apply_date, apply_date_bigint, apply_by_user, created_date, created_date_bigint, sms_phone, json', 'safe'],
            ['date_from_ymd, date_to_ymd, date_from, date_to, date_from_apply, date_to_apply, date_from_ymd_apply, date_to_ymd_apply', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rApply' => array(self::BELONGS_TO, 'Users', 'apply_by_user'),
            'rReward' => array(self::BELONGS_TO, 'Reward', 'reward_id'),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Loại',
            'code_no' => 'Mã code',
            'agent_id' => 'Đại lí',
            'sell_id' => 'Sell',
            'customer_id' => 'Khách hàng',
            'status' => 'Trạng thái',
            'apply_date' => 'Ngày sử dụng',
            'apply_date_bigint' => 'Ngày sử dụng',
            'apply_by_user' => 'Người nhập code',
            'created_date' => 'Ngày tạo',
            'created_date_bigint' => 'Created Date Bigint',
            'sms_phone' => 'SMS Phone',
            'json' => 'Json',
            'date_from' => 'Từ ngày',
            'date_to' => 'Đến ngày',
            'date_from_apply' => 'Từ ngày',
            'date_to_apply' => 'Đến ngày',
            'reward_point' => 'Điểm',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $cRole          = MyFormat::getCurrentRoleId();
        $cUid           = MyFormat::getCurrentUid();
        $aUidAllowAll   = GasConst::getAdminArray();
        $criteria=new CDbCriteria;

	$criteria->compare('t.type',$this->type);
	$criteria->compare('t.code_no',$this->code_no);
	$criteria->compare('t.agent_id',$this->agent_id);
	$criteria->compare('t.customer_id',$this->customer_id);
	$criteria->compare('t.apply_by_user',$this->apply_by_user);
	$criteria->compare('t.status',$this->status);
	$criteria->compare('t.sms_phone',$this->sms_phone);
        if(!empty($this->date_from)||!empty($this->date_to)){
            $this->date_from_ymd  = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
            $this->date_to_ymd    = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
            DateHelper::searchBetween($this->date_from_ymd, $this->date_to_ymd, 'created_date_bigint', $criteria, true);
        }
        if(!empty($this->date_from_apply)||!empty($this->date_to_apply)){
            $this->date_from_ymd_apply  = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from_apply);
            $this->date_to_ymd_apply    = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to_apply);
            DateHelper::searchBetween($this->date_from_ymd_apply, $this->date_to_ymd_apply, 'apply_date_bigint', $criteria, true);
        }

        GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');
        if(!in_array($cUid, $aUidAllowAll)){
            $criteria->addCondition('t.status<>' . CodePartner::STATUS_NEW);// giới hạn không cho xem mã code chưa dùng
        }
        
        $criteria->order = 't.id DESC';
        $_SESSION['data-excel'] = new CActiveDataProvider($this, array(
            'pagination'=>false,
            'criteria'=>$criteria,
//            'sort' => $sort,
        ));
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    public function jsonSetData() {
        $json['sms_content']    = $this->sms_content;
        $json['first_name']     = $this->first_name;
        $json['address']        = $this->address;
        $this->json = MyFormat::jsonEncode($json);
    }
    
    /** @Author: DungNT Now 22, 2018
     *  @Todo: map data to field from json
     **/
    public function jsonMapData() {
        $aData = json_decode($this->json, true);
        $this->sms_content  = isset($aData['sms_content']) ? $aData['sms_content'] : '';
        $this->first_name   = isset($aData['first_name']) ? $aData['first_name'] : '';
        $this->address      = isset($aData['address']) ? $aData['address'] : '';
    }

    public function getSmsContent() {
        return $this->sms_content;
    }
    public function getFirstName() {
        return $this->first_name;
    }
    public function getAddress() {
        return $this->address;
    }
    public function getApplyDate($format = 'd/m/Y H:i') {
        if(empty($this->apply_date_bigint)){
            return '';
        }
        return date($format, $this->apply_date_bigint);
    }
    public function getExpiryDate($format = 'd/m/Y') {
        return date($format, $this->expiry_date_bigint);
    }
    
    /** @Author: DungNT  Now 22, 2018 -- handle listing api */
    public function handleApiList(&$result, $q) {
        // 1. get list order by user id
        $dataProvider   = $this->apiListing($q);
        $models         = $dataProvider->data;
        $CPagination    = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page']   = $CPagination->pageCount;
        
        $result['message']  = 'Tổng số code ' . ActiveRecord::formatCurrency($this->sumByAgent());
        $result['record']   = [];
        
        if( $q->page >= $CPagination->pageCount ){
            $result['record'] = [];
        }else{
            foreach($models as $model){
                $model->mAppUserLogin       = $this->mAppUserLogin;// for canPickCancel
                $result['record'][]         = $model->formatAppItemList();
            }
        }
    }
    
    /**
    * @Author: DungNT Now 22, 2018
    * @Todo: get data listing 
    * @param: $q object post params from app client
    */
    public function apiListing($q) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.agent_id', $this->agent_id);
        if(!empty($q->code_no)){
            $criteria->compare('t.code_no', trim($q->code_no));
        }
        $criteria->addCondition('t.status=' . CodePartner::STATUS_COMPLETE);
//        $criteria->addCondition('t.type=' . CodePartner::TYPE_XANG);// DungNT Jun1519
        $criteria->order = 't.apply_date_bigint DESC';
        $dataProvider=new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize' => GasAppOrder::API_LISTING_ITEM_PER_PAGE,
                'currentPage' => (int)$q->page,
            ),
          ));
        return $dataProvider;
    }
    
    /** @Author: DungNT Now 22, 2018
     *  @Todo: dùng chung để format item trả xuống list + view của app Gas Service
     **/
    public function formatAppItemList() {
        $this->jsonMapData();
        $temp = [];
        $temp['id']                 = $this->id;
        $temp['code_no']            = "{$this->code_no} - {$this->getApplyDate()}";
        $temp['customer_info']      = "{$this->getFirstName()} - {$this->getAddress()}";
        return $temp;
    }
    
    /** @Author: DungNT Now 22, 2018 
     *  @Todo:  get and validate code from app input
     **/
    public function getByCodeFromApp() {
        $criteria = new CDbCriteria();
        $criteria->compare('t.code_no', trim($this->code_no));
        $criteria->compare('t.agent_id', $this->agent_id);
        $model = CodePartner::model()->find($criteria);
        if(empty($model)){
            $this->addError('code_no', "Mã code $this->code_no không hợp lệ");
            return $this;
        }
        if($model->status != CodePartner::STATUS_NEW){
            $this->addError('code_no', "Mã code đã được sử dụng ngày {$model->getApplyDate()}");
            return $this;
        }
        $today = strtotime(date('Y-m-d'));
        if($today > $model->expiry_date_bigint){
            $this->addError('code_no', "Mã code đã hết hạn ngày {$model->getExpiryDate()}");
            return $this;
        }
        
        $model->mAppUserLogin = $this->mAppUserLogin;
        return $model;
    }
    
    /** @Author: DungNT Now 22, 2018
     *  @Todo: handle apply code
     **/
    public function appApplyCode() {
        $this->status               = CodePartner::STATUS_COMPLETE;
        $this->apply_date           = date('Y-m-d');
        $this->apply_date_bigint    = $this->getCurrentDateBigint();
        $this->apply_by_user        = $this->mAppUserLogin->id;
        $this->update();
        $this->sendSmsAfterApplyCode();
        $this->trackingClear();
    }
    
    /** @Author: DungNT Now 22, 2018
     *  @Todo: when input from app then plus 1 time input code, if over Max then block this user
     **/
    public function trackingPlusOneTimeInput() {
        if($this->mAppUserLogin->login_attemp >= CodePartner::MAX_TIME_INPUT){
            throw new Exception('Bạn đã bị khóa nhập code vì đã nhập sai mã '.CodePartner::MAX_TIME_INPUT.' lần. Hỗ trợ 19001565');
        }
        $this->mAppUserLogin->login_attemp += 1;
        $this->mAppUserLogin->update(['login_attemp']);
    }
    
    /** @Author: DungNT Now 22, 2018
     *  @Todo: clear tracking count when enter success code
     **/
    public function trackingClear() {
        $this->mAppUserLogin->login_attemp = 0;
        $this->mAppUserLogin->update(['login_attemp']);
    }
    
    /** @Author: KHANH TOAN Nov 23, 2018
     *  @Todo: báo cáo
     **/
    public function getReport() {
        $criteria               = new CDbCriteria();
        GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');
//        $criteria->select       = 'count(id) as id, t.type, t.agent_id, t.apply_date';
//        $criteria->group        = 't.type, t.agent_id, t.apply_date';
        $criteria->select       = 'count(t.id) as id, t.status, t.agent_id, t.apply_date';
        $criteria->group        = 't.status, t.agent_id, t.apply_date';
        $this->date_from_ymd    = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        $this->date_to_ymd      = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        DateHelper::searchBetween($this->date_from_ymd, $this->date_to_ymd, 'apply_date_bigint', $criteria, true);
//        $criteria->compare('t.status', CodePartner::STATUS_COMPLETE);
        $criteria->compare('t.agent_id', $this->agent_id);
        $models         = CodePartner::model()->findAll($criteria);
        $aData          = [];
        $mGasDebtsRp    = new GasDebtsReport();
        foreach ($models as $model) {
            if($model->status == CodePartner::STATUS_COMPLETE){ 
                // Tổng code đã nhập
                $mGasDebtsRp->addValue($aData['OUTPUT'][$model->agent_id][$model->apply_date]['COMPLETE'], $model->id);
                // Sum code hoàn thành theo đại lý
                $mGasDebtsRp->addValue($aData['SUM_AGENT'][$model->agent_id]['COMPLETE'], $model->id);
                // Sum code hoàn thành theo ngày
                $mGasDebtsRp->addValue($aData['SUM_DATE'][$model->apply_date]['COMPLETE'], $model->id);
                // Sum all tổng code
                $mGasDebtsRp->addValue($aData['SUM_ALL']['COMPLETE'], $model->id);
            } elseif($model->status == CodePartner::STATUS_CANCEL){ 
                // Tổng code đã hủy
                $mGasDebtsRp->addValue($aData['OUTPUT'][$model->agent_id][$model->apply_date]['CANCEL'], $model->id);
                // Sum code đã hủy theo đại lý
                $mGasDebtsRp->addValue($aData['SUM_AGENT'][$model->agent_id]['CANCEL'], $model->id);
                // Sum code đã hủy theo ngày
                $mGasDebtsRp->addValue($aData['SUM_DATE'][$model->apply_date]['CANCEL'], $model->id);
                // Sum all code đã hủy
                $mGasDebtsRp->addValue($aData['SUM_ALL']['CANCEL'], $model->id);
            }
            // Tổng code sinh ra
            $mGasDebtsRp->addValue($aData['OUTPUT'][$model->agent_id][$model->apply_date]['ALL'], $model->id);
            
            // Sum tổng code theo đại lý
            $mGasDebtsRp->addValue($aData['SUM_AGENT'][$model->agent_id]['ALL'], $model->id);
            
            // Sum tổng code theo ngày
            $mGasDebtsRp->addValue($aData['SUM_DATE'][$model->apply_date]['ALL'], $model->id);
            
            // Sum all tổng code
            $mGasDebtsRp->addValue($aData['SUM_ALL']['ALL'], $model->id);
            
            // Array date
            $aData['DATE'][$model->apply_date] = $model->apply_date;
        }
        return $aData;
    }
    
    
    public function getType(){
        $aType = $this->getArrayType();
        return isset($aType[$this->type]) ? $aType[$this->type] : "";
    }
    
    public function getAgent() {
        $aAgent = $this->rAgent;
        return isset($aAgent->first_name) ? $aAgent->first_name : "";
    }
    
    public function getCustomer() {
        $aCustomer = $this->rCustomer;
        return isset($aCustomer->first_name) ? $aCustomer->first_name : "";
    }
    
    public function getApplyByUser() {
        $aApplyByUser = $this->rApply;
        return isset($aApplyByUser->first_name) ? $aApplyByUser->first_name : "";
    }
    
    public function getStatus(){
        $aStatus = $this->getArrayStatus();
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : "";
    }
    
    public function getCodeNo() {
        return $this->code_no;
    }
    public function getSmsPhone() {
        return '0' . $this->sms_phone;
    }
    
    public function getPoint(){
        return $this->reward_point;
    }

    public function setExpiryDate() {
        $expiryDate = MyFormat::modifyDays(date('Y-m-d'), CodePartner::DAY_EXPIRED, '+', 'day');
        $this->expiry_date_bigint   = strtotime($expiryDate);
    }
  
    
    /** @Author: DungNT Now 23, 2018
     *  @Todo: flow do send sms code for customer 
     * 1. check agent allow
     * 1.1 check valid phone di động, chỉ gửi SMS cho số di động
     * 3. save new record - gen code_no không trùng theo agent + type
     * 2. build content sms
     * 4. send sms
     * 5. notify customer -- có thể chưa cần
     **/
    public function makeNewCode($mSell) {
        // 1. check agent allow
        if(!in_array($mSell->agent_id, $this->getArrayAgentCodeXang())){
            return ;
        }
        // 1.1 check valid phone di động, chỉ gửi SMS cho số di động
        if(!$this->isPhoneValid($mSell)){
            return ;
        }
        
        $aSellDetail = SellDetail::getByRootId($mSell->id);
        foreach($aSellDetail as $mDetail){
            if($mDetail->materials_type_id == GasMaterialsType::MATERIAL_BINH_12KG){
                for($i = 0; $i < $mDetail->qty; $i++){
                    $this->saveRecordXang($mSell);// Dec0818 cứ 1 bình 12 thì gửi 1 mã code
                }
            }
        }
    }
    
    /** @Author: DungNT Dec 08, 2018
     *  @Todo: only save one record and SMS
     **/
    public function saveRecordXang($mSell) {
        $mCodePartner = new CodePartner();
        // 2. build content sms
        // 3. save new record 
        $mCodePartner->type = CodePartner::TYPE_XANG;
        $mCodePartner->setData($mSell);
        $mCodePartner->save();
        // 4. send sms
        $mCodePartner->doSendSms();
    }
    
    /** @Author: DungNT Now 23, 2018
     *  @Todo: check valid phone di động, chỉ gửi SMS cho số di động
     **/
    public function isPhoneValid($mSell) {
        $ok = true;
        if(empty($mSell->phone)){
            $ok = false;
        }
        $phoneNumber = $mSell->phone;
        if(!UsersPhone::isValidCellPhone($phoneNumber)){
            Logger::WriteLog("isPhoneValid Phone not valid $phoneNumber");
            $ok = false;
        }
        return $ok;
    }
    
    /** @Author: DungNT Now 23, 2018
     *  @Todo: set data to model
     **/
    public function setData($mSell) {
        $this->agent_id             = $mSell->agent_id;
        $this->sell_id              = $mSell->id;
        $this->customer_id          = $mSell->customer_id;
        $this->sms_phone            = $mSell->phone;
        $this->code_no              = $this->makeCodeNo();
        $this->setExpiryDate();
        $this->makeSmsContent($mSell);
        $this->first_name       = $mSell->getCustomer('first_name');
        $this->address          = $mSell->getCustomer('address');
        $this->jsonSetData();
    }
    
    /** @Author: DungNT Now 23, 2018
     *  @Todo: xử lý sinh code mới kiểm tra ko trùng
     **/
    public function makeCodeNo() {
        while (true) {
            $code = ActiveRecord::randString(6, '123456789');
            if (!$this->getByAgentAndCode($code)) {
                return $code;
            }
        }
    }
    
    /** @Author: DungNT Now 23, 2018
     *  @Todo: get by code of agent and type
     **/
    public function getByAgentAndCode($code_no) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.type', $this->type);
        $criteria->compare('t.agent_id', $this->agent_id);
        $criteria->compare('t.code_no', $code_no);
//    -- read detail before use ->  $criteria->compare('t.status', CodePartner::STATUS_NEW);// Now2318 chỗ này có thể check sinh code không trùng với status=new, có thể trùng với những code đã sử dụng rồi - nếu kho số hết
        return CodePartner::model()->find($criteria);
    }
    
    /** @Author: DungNT Now 23, 2018
     *  @Todo: build SMS content
     **/
    public function makeSmsContent($mSell) {
        /* Jun1519 DungNT close - từ nay về sau không build theo agent_id nữa, build theo type cho tổng quát trường hợp, theo agent có thể sẽ rất nhiều case
        switch ($mSell->agent_id) {
            case CodePartner::AGENT_THANH_SON:
                $this->makeSmsContentAgentThanhSon($mSell);
                break;

            default:
                $this->makeSmsContentAgentThanhSon($mSell);
                break;
        }
        */
        switch ($this->type) {
            case CodePartner::TYPE_REWARD:
            case CodePartner::TYPE_REWARD_APP:
                $this->makeSmsContentTypeReward($mSell);
                break;

            default:
                return ;
                break;
        }
        
    }
    // build sms content for one agent
    public function makeSmsContentAgentThanhSon($mSell) {
        $this->sms_content = "Ban duoc MIEN PHI 20,000 tien XANG o Cay Dau Thanh Son 1";
        $this->sms_content .= "\nMa CODE: ".$this->code_no;
        $this->sms_content .= "\nHet han vao: ".$this->getExpiryDate();
        $this->sms_content .= "\nChi tiet lien he 19001565";
    }
    public function makeSmsContentAfterApplyCode() {
        $this->sms_content = "Ma CODE: ".$this->code_no." da duoc su dung luc: ". $this->getApplyDate('H:i d/m/Y');
        $this->sms_content .= "\nCam on Quy Khach da su dung Gas24h!";
        $this->sms_content .= "\nChi tiet lien he 19001565";
    }
    /** @Author: DungNT Jun 15, 2019
     *  @Todo: build sms content for TYPE_REWARD 
     **/
    public function makeSmsContentTypeReward($mSell) {
        $this->sms_content = "Ma doi qua cua Quy Khach la $this->code_no";
        $this->sms_content .= "\nHet han vao: ".$this->getExpiryDate();
        $this->sms_content .= "\nChi tiet lien he 19001565";
    }
    
    public function doSendSms() {
        $mSms = new GasScheduleSms();
        $mSms->phone            = $this->sms_phone;
        $mSms->title            = $this->sms_content;
        $mSms->purgeMessage();
        $mSms->uid_login        = GasConst::UID_ADMIN;;
        $mSms->user_id          = $this->customer_id;
        $mSms->type             = GasScheduleSms::TYPE_HGD_SMS_POINT;
        $mSms->obj_id           = $this->customer_id;
        $mSms->content_type     = GasScheduleSms::ContentTypeKhongDau;
        $mSms->time_send        = '';
        $mSms->json_var         = [];
        $schedule_sms_id        = $mSms->fixInsertRecord();
        // chỗ này nếu lưu $schedule_sms_id là sai, vì sau khi send thì $schedule_sms_id sẽ bị xóa, mà bên history ko lưu lại biến này
    }
    
    /** @Author: DungNT Oct 30, 2018
     *  @Todo: send Sms cho KH after nhập code thành công
     **/
    public function sendSmsAfterApplyCode() {
        $this->makeSmsContentAfterApplyCode();
        $this->doSendSms();
    }
    
    /** @Author: DungNT Now 23, 2018
     *  @Todo:  count total code complete by agent
     **/
    public function sumByAgent() {
        $criteria = new CDbCriteria();
        $criteria->compare('t.agent_id', $this->agent_id);
        $criteria->compare('t.status', CodePartner::STATUS_COMPLETE);
        return CodePartner::model()->count($criteria);
    }
    
    /** @Author: ToanNK  Sep 10, 2018 -- handle listing api */
    public function handleApiListExchangeReward(&$result, $q) {
        // 1. get list order by user id
        $dataProvider   = $this->ApiListingExchangeReward($q);
        $models         = $dataProvider->data;
        $CPagination    = $dataProvider->pagination;
        $result['total_record'] = $CPagination->itemCount;
        $result['total_page']   = $CPagination->pageCount;
        
        $result['message']  = 'L';
        $result['record']   = [];
        if( $q->page >= $CPagination->pageCount ){
            $result['record'] = [];
        }else{
            $criteria = new CDbCriteria(); 
            $criteria->addInCondition('t.id', CHtml::listData($models, 'reward_id', 'reward_id'));
            $aReward  = Reward::model()->findAll($criteria);
            $aRewardConvert = [];
            foreach ($aReward as $key => $value) {
                $aRewardConvert[$value->id] = $value;
            }
            foreach($models as $model){
                $result['record'][]         = $model->formatAppItemListExchange($aRewardConvert);
            }
        }
    }

     /**
    * @Author: ToanNK Sep 10 2018
    * @Todo: get data listing 
    * @param: $q object post params from app client
    */
    public function ApiListingExchangeReward($q) {
        $mUser = $this->mAppUserLogin;
        $criteria = new CDbCriteria();
        $criteria->compare('t.customer_id',$mUser->id);
        $criteria->addInCondition('t.type',  $this->getArrayTypeReward());
        $criteria->order = 't.id DESC';
        $dataProvider = new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize' => CodePartner::API_LISTING_EXCHANGE_REWARD_PER_PAGE,
                'currentPage' => (int)$q->page,
            ),
          ));
        return $dataProvider;
    }
    
     /** @Author: ToanNK Sep 10 2018
     *  @Todo: dùng chung để format trả xuống list + view của app Gas24h
     **/
    public function formatAppItemListExchange($aRewardConvert) {
        
        $temp = [];
        $temp['id']                 = $this->id;
        $temp['title']              = !empty($aRewardConvert[$this->reward_id]) ? $aRewardConvert[$this->reward_id]->getTitle() : '';
        $temp['sub_title']          = !empty($aRewardConvert[$this->reward_id]) ? $aRewardConvert[$this->reward_id]->getSubTitle() : '';
        $temp['reward_id']          = $this->reward_id;
        $temp['reward_point']       = (int)($this->reward_point);
        $temp['status']             = $this->status;
        $temp['apply_date']         = MyFormat::dateConverYmdToDmy($this->apply_date);
        $temp['code_no']            = $this->code_no;
        return $temp;
    }
    
    /** @Author: KHANH TOAN Mar 12 2019
     *  @Todo: app exchange point -- KH đổi điểm lấy quà trên app Gas24h
     *  @Param: $result, $q
     *  model Reward => checkReward xử lý response trên web
     **/
    public function handleApiExchangePoint(&$result, $q) {
        $mUser = $this->mAppUserLogin;
        $mRewardPoint = new RewardPoint();
        $pointNow = $mRewardPoint->calculatePoint($mUser->id);
        $mReward = Reward::model()->findByPk($q->reward_id);
        if(empty($mReward)){
            $result['status']   = 0;
            $result['code']     = 400;
            $result['message']  = 'Không tìm thấy quà cần đổi!';
            return;
        }
        $mReward->mAppUserLogin = $this->mAppUserLogin;
        if(!$mReward->allowBuy($mUser->id)){
            $result['status']   = 0;
            $result['code']     = 400;
            $result['message']  = 'Không được phép đổi!';
            $result['record']   = $mReward->formatAppItemListReward(true);
            return;
        }
        $reward_point = $mReward->point_apply;        
        if($pointNow < $reward_point ){
            $result['status']   = 0;
            $result['code']     = 400;
            $result['message']  = 'Bạn không đủ diểm để đổi quà này!';
            $result['record']   = $mReward->formatAppItemListReward(true);
        }else{
            $result['record']   = [];
            $mSell              = new Sell();
            $mSell->id          = 0;
            $mSell->agent_id    = $mUser->area_code_id;
            $mSell->customer_id = $mUser->id;
            $mCodePartner = $this->saveReward($q->reward_id, $mUser, $reward_point, $mSell, CodePartner::TYPE_REWARD_APP);
            $result['record']['code_no']        = $mCodePartner->code_no;
            $result['record']['point_used']     = $reward_point;
            $result['record']['point_now']      = $pointNow - $reward_point;
            $result['record']   = $mReward->formatAppItemListReward(true);
        }
    }
    /** @Author: NamNH Jun 12, 2019
     *  @Todo: save exchange in web
     **/
    public function handleExchangeWeb($mSell) {
        if(empty($mSell->customer_id)){
            return;
        }
        $mUser = Users::model()->findByPk($mSell->customer_id);
        if(empty($mUser)){
            return;
        }
        $mOld = $this->getRewardByCustomer($mUser,$mSell->id);
        if(empty($mSell->reward_id)){ // remove old
            if(!empty($mOld)){
                $mOld->delete();
            }
            return;
        }
        $mRewardPoint = new RewardPoint();
        $pointNow = $mRewardPoint->calculatePoint($mUser->id);
        $mReward = Reward::model()->findByPk($mSell->reward_id);
        if(empty($mReward)){
            return;
        }
        if(!$mReward->allowBuy($mUser->id)){
            return;
        }
        
        
        if(!empty($mOld)){
            $reward_point = $mReward->point_apply;        
            if(($pointNow+$mOld->reward_point) < $reward_point ){
                return;
            }
            $mOld->changeReward($mSell->reward_id,$reward_point);
        }else{
            $reward_point = $mReward->point_apply;        
            if($pointNow < $reward_point ){
                return;
            }
            $mCodePartner = $this->saveReward($mSell->reward_id,$mUser,$reward_point,$mSell);
        }
        return $mCodePartner;
    }
    
    /** @Author: NamNH Sep1919
    *  @Todo:set default codePartner from web
    *  @Param:$mSell
    **/
    public function removeSellIdCodePartner($mSell,$type = self::TYPE_REWARD_APP){
        $tableName          = CodePartner::model()->tableName();
        $sql = "UPDATE `$tableName` as t set t.sell_id = 0 "
                    . "WHERE t.sell_id = $mSell->id AND t.type = ".$type;
        Yii::app()->db->createCommand($sql)->execute();
    }
    
    /** @Author: NamNH Sep1919
    *  @Todo:update codePartner from web
    *  @Param:$mSell
    **/
    public function handleExchangeAppFromWeb($mSell) {
        $this->removeSellIdCodePartner($mSell);
        if(empty($mSell->rewardApp) && !is_array($mSell->rewardApp)){
            return;
        }
        $tableName          = CodePartner::model()->tableName();
        $strIdCode          = implode(',',$mSell->rewardApp);
        $sql = "UPDATE `$tableName` as t set sell_id = {$mSell->id} "
                    . "WHERE t.id IN ($strIdCode)";
        Yii::app()->db->createCommand($sql)->execute();
    }
    
    /** @Author: NamNH Jun 15, 2019
     *  @Todo: do save type reward
     **/
    public function saveReward($reward_id, $mUser, $reward_point, $mSell,$type = CodePartner::TYPE_REWARD){
        $mRewardPoint   = new RewardPoint();
        $phone          = $mRewardPoint->getPhoneReward($mUser->id);
        $mCodePartner   = new CodePartner();
        $mCodePartner->type                 = $type;
        $mCodePartner->setData($mSell);
        $mCodePartner->reward_id            = $reward_id;
        $mCodePartner->sms_phone            = $phone;
        $mCodePartner->reward_point         = $reward_point;
        if($mCodePartner->save()){
            $mRewardPoint->doSendSms($mCodePartner->sms_content, $mCodePartner->customer_id);
        }
        return $mCodePartner;
    }
    
    /** @Author: NamNH Jun 15, 2019
     *  @Todo: lấy quà tặng đã thêm trên web của khách hàng theo sell_id
     **/
    public function getRewardByCustomer($mUser,$sell_id, $type = CodePartner::TYPE_REWARD){
        $criteria = new CDbCriteria();
        if(!empty($mUser->id)){
            $criteria->compare('customer_id', $mUser->id);
        }
        $criteria->compare('sell_id', $sell_id);
        $criteria->compare('type', $type);
        return self::model()->find($criteria);
    }
    /** @Author: NamNH Jun 15, 2019
     *  @Todo: Thay đổi quà trên web, update sell
     **/
    public function changeReward($reward_id, $reward_point){
        $this->reward_id            = $reward_id;
        $this->reward_point         = $reward_point;
        $this->status               = $this->status == self::STATUS_CANCEL ? self::STATUS_NEW : $this->status;
        $this->update();
        return $this;
    }
    
    /** @Author: NamNH Jun 15, 2019
     *  @Todo: update status code partner when cancel order
     **/
    public function updateCancel($mSell){
        $mUser = new Users();
        $mUser->id = 0;
        $mCodePartner = $this->getRewardByCustomer($mUser, $mSell->id);
        if(!empty($mCodePartner)){
            $mCodePartner->status = self::STATUS_CANCEL;
            $mCodePartner->update();
        }
//        remove point
        $mRewardPoint = new RewardPoint();
        $mRewardPoint->removeBySell($mSell);
    }
    /** @Author: NamNH Jun 26, 2019
     *  @Todo: format json response
     **/
    public function formatAppItemListReward(){
        $mReward = $this->rReward;
        $temp = [];
        if(!empty($mReward)){
            $mReward->mAppUserLogin = $this->mAppUserLogin;
            $temp = $mReward->formatAppItemListReward();
        }
        $temp['code_id'] = $this->id;
        $temp['code_no'] = $this->code_no;
        return $temp;
    }
    
    /** @Author: NamNH Jun 26, 2019
     *  @Todo: format json response vew
     **/
    public function handleViewReward(&$result,$q){
        $criteria = new CDbCriteria();
        $criteria->compare('t.id', $q->code_id);
        $criteria->addInCondition('t.type', $this->getArrayTypeReward());
        $mCodePartner = self::model()->find($criteria);
           if(!empty($mCodePartner)){
                if($mCodePartner->customer_id != $this->mAppUserLogin->id){
                    $result = ApiModule::$defaultResponse;
                    $result['message'] = 'Mã code không phải của khách hàng';
                }
                $result['record'] = $mCodePartner->formatApiView();
            }else{
                $result = ApiModule::$defaultResponse;
                $result['message'] = 'Không tìm thấy thông tin mã code';
            } 
    }
    
    /** @Author: NamNH Jun 26, 2019
     *  @Todo: format json response vew
     **/
    public function formatApiView(){
        $mReward = $this->rReward;
        $temp = [];
        if(!empty($mReward)){
            $temp['partner_logo'] = $mReward->getUrlImage('rFileLogo');
            $temp['title'] = $mReward->getTitle();
        }
        $temp['code_id'] = $this->id;
        $temp['code_no'] = $this->code_no;
        $temp['user_manual'] = 'Đọc mã code cho nhân viên phục vụ khách hàng khi bạn đổi điểm';
        return $temp;
    }
    
    /** @Author: NamNH Sep1919
    *  @Todo:get array code partner request from app by customer id
    **/
    public function getCodePartner(){
        if(empty($this->customer_id)){
            return [];
        }
        $criteria = new CDbCriteria();
        $criteria->compare('t.customer_id', $this->customer_id);
        $criteria->compare('t.type', self::TYPE_REWARD_APP);
        $criteria->addCondition('t.status != '.self::STATUS_CANCEL);
        $criteria->addCondition('t.status != '.self::STATUS_COMPLETE);
        $criteria->addCondition('t.status != '.self::STATUS_EXPIRED);
        $criteria->addCondition('t.sell_id = "'.$this->sell_id .'" OR t.sell_id <= 0');
        $aCodePartner = CodePartner::model()->findAll($criteria);
        return $aCodePartner;
    }
    
    /** @Author: NamNH Sep2019
    *  @Todo:get html web create sell
    *  @Param:$idCustomer
    **/
    public function getCodePartnerWeb($customer_id,$idSell){
        $model = Sell::model()->findByPk($idSell);
        $strResult = '';
        $this->customer_id   = $customer_id;
        $this->sell_id       = $idSell;
        $aCodePartner = $this->getCodePartner();
        if(!empty($aCodePartner)){
            $strResult .= '<div class="row">';
                $strResult .= '<label for="Sell_reward_app">Ưu Đãi App</label>';
                $strResult .= '<div style="display: inline-block;">';
                    foreach ($aCodePartner as $key => $mCodePartner){
                        $id                 = $mCodePartner->id;
                        $mReward            = !empty($mCodePartner->rReward) ? $mCodePartner->rReward : null;
                        $mMaterial          = !empty($mReward->rMaterial) ? $mReward->rMaterial : null;
                        if(empty($mReward) || empty($mMaterial)){
                            continue;
                        }
                        $aMaterialFormat    = MyFunctionCustom::formatMaterialJsonItem($mMaterial, 0);
                        $materialName       = !empty($mReward) ? $mReward->getTitle() : '';
                        $material_id        = !empty($aMaterialFormat['id']) ? $aMaterialFormat['id'] : '';
                        $material_no        = !empty($aMaterialFormat['materials_no']) ? $aMaterialFormat['materials_no'] : '';
                        $material_name      = !empty($aMaterialFormat['name']) ? $aMaterialFormat['name'] : '';
                        $material_unit      = !empty($aMaterialFormat['unit']) ? $aMaterialFormat['unit'] : '';
                        $material_qty       = !empty($mReward) ? $mReward->qty : '';
                        $material_unit_use  = !empty($aMaterialFormat['unit_use']) ? $aMaterialFormat['unit_use'] : '';
                        $material_price     = !empty($aMaterialFormat['price']) ? $aMaterialFormat['price'] : 0;
                        $strResult          .= '<label class="reward" for="reward_app_'.$id.'">'.$materialName.'</label>';
                        $checked            = !empty($model->id) && $model->id == $mCodePartner->sell_id ? 'checked' : '';
                        $strResult          .= '<input '.$checked.'class="checkbox_reward"';
                        $strResult          .= 'data-reward="'.$id.'"';
                        $strResult          .= 'data-material_id ="'.$material_id.'"';
                        $strResult          .= 'data-material_no ="'.$material_no.'"';
                        $strResult          .= 'data-material_name ="'.$material_name.'"';
                        $strResult          .= 'data-material_unit ="'.$material_unit.'"';
                        $strResult          .= 'data-material_qty ="'.$material_qty.'"';
                        $strResult          .= 'data-material_unit_use ="'.$material_unit_use.'"';
                        $strResult          .= 'data-material_price ="'.$material_price.'"';
                        $strResult          .= 'name="Sell[rewardApp][]" id="reward_app_'.$id.'" value="'.$id.'" type="checkbox">';
                        $strResult          .= '<div class="clr"></div>';
                    }
                $strResult .= '</div>';
            $strResult .= '</div>';
        }
        return $strResult;
    }
}