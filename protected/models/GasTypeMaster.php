<?php

/**
 * This is the model class for table "{{_gas_type_master}}".
 *
 * The followings are the available columns in table '{{_gas_type_master}}':
 * @property integer $id
 * @property string $name 01669 686 933
 * @property string $short_name
 * @property string $note
 */
class GasTypeMaster extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasTypeMaster the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_type_master}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                    array('name, short_name', 'required'),
                    array('name', 'length', 'max'=>200),
                    array('short_name', 'length', 'max'=>100),
                    array('note', 'length', 'max'=>300),
                    // The following rule is used by search().
                    // Please remove those attributes that should not be searched.
                    array('id, name, short_name, note', 'safe'),
            );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                    'id' => 'ID',
                    'name' => 'Tên loại thanh toán',
                    'short_name' => 'Tên viết tắt',
                    'note' => 'Chú thích',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;

            $criteria->compare('t.id',$this->id);
            $criteria->compare('t.name',$this->name,true);
            $criteria->compare('t.short_name',$this->short_name,true);
            $criteria->compare('t.note',$this->note,true);

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
        'pagination'=>array(
            'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
        ),
            ));
    }

    public static function getArrAll()
    {
        $criteria = new CDbCriteria;
        $models = self::model()->findAll($criteria);
        return  CHtml::listData($models,'id','name');            
    }
        
   
}