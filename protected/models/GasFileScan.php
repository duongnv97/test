<?php

/**
 * This is the model class for table "{{_gas_file_scan}}".
 *
 * The followings are the available columns in table '{{_gas_file_scan}}':
 * @property string $id
 * @property string $code_no
 * @property string $agent_id
 * @property string $uid_login
 * @property string $maintain_date
 * @property string $monitoring_id
 * @property string $maintain_employee_id
 * @property string $note
 * @property string $created_date
 * @property string $last_update_by
 * @property string $last_update_time
 */
class GasFileScan extends BaseSpj
{
    
    public $aModelDetail;
    public $mDetail;
    public $MAX_ID;
    public $date_from;
    public $date_to;        
    public $aModelFileScanInfo;   
    
    public $ext_customer_name, $autocomplete_name;
    public $ext_customer_address;   
    public $ext_customer_phone;   
    public $ext_materials_name;   
    public $ext_seri;   
    public $ext_note;
    public $file_excel;
    public static $AllowExcel = "xls,xlsx";
    
        
    
    // lấy số ngày cho phép đại lý cập nhật
    public static function getDayAllowUpdate(){
        return Yii::app()->params['PTTT_update_file_scan'];
    }
    
    
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_file_scan}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('agent_id, maintain_date, monitoring_id, maintain_employee_id', 'required'),
            array('note', 'required', 'message'=>'Chưa nhập tên đường của PTTT'),
            array('note', 'length', 'max'=>500),
            array('aModelFileScanInfo,id, code_no, agent_id, uid_login, maintain_date, monitoring_id, maintain_employee_id, note, created_date, last_update_by, last_update_time', 'safe'),
            array('date_from,date_to', 'safe'),
            array('file_excel,ext_customer_name,ext_customer_address, ext_customer_phone ,ext_materials_name,ext_seri,ext_note', 'safe'),
            array('file_excel', 'file',
                'allowEmpty'=>true,
                'types'=> 'xls,xlsx',
                'wrongType'=>"Chỉ cho phép tải file ".GasFileScan::$AllowExcel,
                'maxSize' => ActiveRecord::getMaxFileSize(), // 5MB
                'tooLarge' => 'File lớn hơn '.(ActiveRecord::getMaxFileSize()/1024).' KB. Vui lòng up file nhỏ hơn.',
            ),   
        );
    }
    
    /**
     * @return array relational rules.
     */
    public function relations()
    {
            return array(
                'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
                'rFileScanDetail' => array(self::HAS_MANY, 'GasFileScanDetail', 'file_scan_id'),
                'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
                'rMonitoring' => array(self::BELONGS_TO, 'Users', 'monitoring_id'),
                'rEmployee' => array(self::BELONGS_TO, 'Users', 'maintain_employee_id'),
                'rLastUpdateBy' => array(self::BELONGS_TO, 'Users', 'last_update_by'),
                'rDetailInfo' => array(self::HAS_MANY, 'GasFileScanInfo', 'file_scan_id',
                     'order'=>'rDetailInfo.id ASC',
                ),
                'rMaintainSell' => array(self::HAS_MANY, 'GasMaintainSell', 'file_scan_id'),
                
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                    'id' => 'ID',
                    'code_no' => 'Mã Số',
                    'agent_id' => 'Đại Lý',
                    'uid_login' => 'Nhân Viên Login',
                    'maintain_date' => 'Ngày PTTT',
                    'monitoring_id' => 'Giám Sát',
                    'maintain_employee_id' => 'Nhân Viên PTTT',
                    'note' => 'Ghi Chú',
                    'created_date' => 'Ngày Tạo',
                    'last_update_by' => 'Người Sửa Lần Cuối',
                    'last_update_time' => 'Ngày Sửa',
                    'date_from' => 'Từ Ngày',
                    'date_to' => 'Đến Ngày',      
                
                    'ext_customer_name' => 'Tên Khách Hàng',
                    'ext_customer_address' => 'Địa Chỉ KH',
                    'ext_customer_phone' => 'Số Điện Thoại KH',
                    'ext_materials_name' => 'Thương Hiệu Gas',
                    'ext_seri' => 'Seri',
                    'ext_note' => 'Ghi Chú Chi Tiết',
                    'file_excel' => 'File Excel',                
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
            $criteria=new CDbCriteria;

            $criteria->compare('t.id',$this->id,true);
            $criteria->compare('t.code_no',$this->code_no,true);
            $criteria->compare('t.agent_id',$this->agent_id);
            $criteria->compare('t.uid_login',$this->uid_login);
            
            if(!empty($this->created_date)){
                $this->created_date = MyFormat::dateDmyToYmdForAllIndexSearch($this->created_date);
                $criteria->compare('t.created_date',$this->created_date,true);
            }
            
            $date_from = '';
            $date_to = '';
            if(!empty($this->date_from)){
                $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
            }
            if(!empty($this->date_to)){
                $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
            }
            if(!empty($date_from) && empty($date_to))
                    $criteria->addCondition("t.maintain_date>='$date_from'");
            if(empty($date_from) && !empty($date_to))
                    $criteria->addCondition("t.maintain_date<='$date_to'");
            if(!empty($date_from) && !empty($date_to))
                    $criteria->addBetweenCondition("t.maintain_date",$date_from,$date_to);            
            
            if(Yii::app()->user->role_id==ROLE_SUB_USER_AGENT){
                $criteria->compare('t.agent_id', MyFormat::getAgentId());
            }else{
                if(!empty($this->agent_id)){                        
                    $criteria->compare('t.agent_id', $this->agent_id);
                }
                GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');
            }
            
            $criteria->compare('t.monitoring_id',$this->monitoring_id);
            $criteria->compare('t.maintain_employee_id',$this->maintain_employee_id);
            $criteria->compare('t.note', trim($this->note),true);
            $withRelate = '';

            if(trim($this->ext_customer_name)!=''){
                $withRelate = 'rDetailInfo';
                $criteria->compare('rDetailInfo.customer_name', trim($this->ext_customer_name),true);
            }
            if(trim($this->ext_customer_address)!=''){
                $withRelate = 'rDetailInfo';
                $criteria->compare('rDetailInfo.customer_address', trim($this->ext_customer_address),true);
            }
            if(trim($this->ext_customer_phone)!=''){
                $withRelate = 'rDetailInfo';
                $criteria->compare('rDetailInfo.customer_phone', trim($this->ext_customer_phone),true);
            }
            if(trim($this->ext_materials_name)!=''){
                $withRelate = 'rDetailInfo';
                $criteria->compare('rDetailInfo.materials_name', trim($this->ext_materials_name),true);
            }
            if(trim($this->ext_seri)!=''){
                $withRelate = 'rDetailInfo';
                $criteria->compare('rDetailInfo.seri', trim($this->ext_seri),true);
            }
            if(trim($this->ext_note)!=''){
                $withRelate = 'rDetailInfo';
                $criteria->compare('rDetailInfo.note', trim($this->ext_note),true);
            }
            if($withRelate!=''){
                $criteria->with = array('rDetailInfo');
                $criteria->together = true;
            }
            
            $sort = new CSort();

            $sort->attributes = array(
                'code_no'=>'code_no',
                'agent_id'=>'agent_id',
                'maintain_date'=>'maintain_date',
                'monitoring_id'=>'monitoring_id',
                'maintain_employee_id'=>'maintain_employee_id',
                'created_date'=>'created_date',                
                'last_update_time'=>'last_update_time',                
            );    
            $sort->defaultOrder = 't.id desc';
            
            $_SESSION['data-excel'] = new CActiveDataProvider($this, array(
                'pagination'=>false,
                'criteria'=>$criteria,
    //            'sort' => $sort,
            ));
            
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'sort' => $sort,
                    'pagination'=>array(
                        'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                    ),
            ));
    }

    protected function beforeSave() {
        if(strpos($this->maintain_date, '/')){
            $this->maintain_date = MyFormat::dateConverDmyToYmd($this->maintain_date);
            MyFormat::isValidDate($this->maintain_date);
        }
        if($this->isNewRecord){
            if(empty($this->agent_id)){
                $this->agent_id = MyFormat::getAgentId();
            }
            $this->uid_login = Yii::app()->user->id;
            $this->code_no = MyFunctionCustom::getNextId('GasFileScan', 'F'.date('y'), LENGTH_TICKET,'code_no');
        }else{
            $this->last_update_by = Yii::app()->user->id;
            $this->last_update_time = date('Y-m-d H:i:s');
        }
        
        
        return parent::beforeSave();
    }
    
    protected function beforeDelete() {
        GasFileScanDetail::deleteByFileScanId($this->id);
        GasFileScanInfo::delete_by_file_scan_id($this->id);
        self::DeletePTTTByFileScanId($this->id);
        return parent::beforeDelete();
    }
    
    protected function beforeValidate() {
        $this->aModelFileScanInfo = array();
        if(isset($_POST['GasFileScanInfo']['customer_name']) && is_array($_POST['GasFileScanInfo']['customer_name'])){
            foreach($_POST['GasFileScanInfo']['customer_name'] as $key=>$item){
                if(trim($item)!=''){
                    $mDetail = new GasFileScanInfo();
                    $mDetail->customer_name = trim($item);
                    $mDetail->customer_address = trim($_POST['GasFileScanInfo']['customer_address'][$key]);
                    $mDetail->customer_phone = trim($_POST['GasFileScanInfo']['customer_phone'][$key]);
                    $mDetail->materials_name = trim($_POST['GasFileScanInfo']['materials_name'][$key]);
                    $mDetail->seri = trim($_POST['GasFileScanInfo']['seri'][$key]);
                    $mDetail->note = trim($_POST['GasFileScanInfo']['note'][$key]);
                    self::RemoveBadWord($mDetail);
                    $this->aModelFileScanInfo[] = $mDetail;
                }                
            }
        }
        if(count($this->aModelFileScanInfo)<1){
            $this->aModelFileScanInfo[] = new GasFileScanInfo();
        }
        
        return parent::beforeValidate();
    }
    
    public static function RemoveBadWord($model){
        $model->customer_name = MyFormat::removeBadCharacters($model->customer_name, array('RemoveScript'=>1));
        $model->customer_address = MyFormat::removeBadCharacters($model->customer_address, array('RemoveScript'=>1));
        $model->customer_phone = MyFormat::removeBadCharacters($model->customer_phone, array('RemoveScript'=>1));
        $model->materials_name = MyFormat::removeBadCharacters($model->materials_name, array('RemoveScript'=>1));
        $model->seri = MyFormat::removeBadCharacters($model->seri, array('RemoveScript'=>1));
        $model->note = MyFormat::removeBadCharacters($model->note, array('RemoveScript'=>1));
    }
 
    public static function CanRemoveFile($model){
        return $model->isNewRecord;
    }
    
    public static function importExcel($model){
    try {
        $FileName = $_FILES['GasFileScan']['tmp_name']['file_excel'];
        if(empty($FileName)) return;
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $inputFileType = PHPExcel_IOFactory::identify($FileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);

        $objPHPExcel = @$objReader->load(@$_FILES['GasFileScan']['tmp_name']['file_excel']);
        $objWorksheet = $objPHPExcel->getActiveSheet();
        $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
        //$highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
        $highestColumn = 'F';
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

        $success = 1;
        $aRowInsert=array();
        $file_scan_id = $model->id;
        $maintain_date = $model->maintain_date;
        for ($row = 2; $row <= $highestRow; ++$row)
        {
            // file excel cần format column theo dạng text hết để người dùng nhập vào khi đọc không lỗi
            $customer_name = MyFormat::removeBadCharacters($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
            $customer_address = MyFormat::removeBadCharacters($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
            $customer_phone = MyFormat::removeBadCharacters($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());
            $materials_name = MyFormat::removeBadCharacters($objWorksheet->getCellByColumnAndRow(3, $row)->getValue());
            $seri = MyFormat::removeBadCharacters($objWorksheet->getCellByColumnAndRow(4, $row)->getValue());
            $note = MyFormat::removeBadCharacters($objWorksheet->getCellByColumnAndRow(5, $row)->getValue());
            $customer_name = MyFormat::RemoveNumberOnly($customer_name);
            if($customer_name!=''){
                $aRowInsert[]="(
                    '$file_scan_id',
                    '$maintain_date',
                    '$customer_name',
                    '$customer_address',	
                    '$customer_phone',
                    '$materials_name',
                    '$seri',
                    '$note'
                    )";
            }
            if($row > 150) {
                break;
            }
        }

        $tableName = GasFileScanInfo::model()->tableName();            
        $sql = "insert into $tableName (
                        file_scan_id,
                        maintain_date,
                        customer_name,
                        customer_address,
                        customer_phone,
                        materials_name,
                        seri,
                        note
                        ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert))
            Yii::app()->db->createCommand($sql)->execute();
        }catch (Exception $e)
        {
            Yii::log("Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
            $code = 404;
            if(isset($e->statusCode))
                $code=$e->statusCode;
            if($e->getCode())
                $code=$e->getCode();
            throw new CHttpException($code, $e->getMessage());
        }
    }
    
    
    /**
     * @Author: ANH DUNG Aug 02, 2014
     * @Todo: tạo mới custoer từ file excel và lưu vào bảng PTTT - PTTT  BIG CHANGE
     * @Param: $model model GasFileScan
     */
    public static function SavePTTTFromExcel($model){
        $FileName = $_FILES['GasFileScan']['tmp_name']['file_excel'];
        if(empty($FileName)) return;
        set_time_limit(7200);
        // 1. Find all PTTT có file_scan_id và Xóa những customer_id ở bảng PTTT đó đi ( dùng cho update và create )
        $model = self::model()->findByPk($model->id);
        self::DeletePTTTByFileScanId($model->id);
        self::CreateNewCustomerAndNewPTTT($model);
        
    }
    
    /**
     * @Author: ANH DUNG Oct 14, 2014
     * @Todo: sửa 1 số PTTT ngày trước chỉ nhập bên file scan, không tự động tạo bên PTTT và new Customer
     * @Param: $pk primary key của file scan cần fix
     */
    public static function FixFileScanOld($pk) {
        $model = self::model()->findByPk($pk);
        self::DeletePTTTByFileScanId($model->id);
        self::CreateNewCustomerAndNewPTTT($model);
    }
    
    
    // belong to SavePTTTFromExcel
    public static function DeletePTTTByFileScanId($file_scan_id){
        if(empty($file_scan_id)){ return; }
        $criteria = new CDbCriteria();
        $criteria->compare('t.file_scan_id', $file_scan_id);
        $models = GasMaintain::model()->findAll($criteria);
        if(count($models)<1) return;
        $aUid = CHtml::listData($models,'customer_id','customer_id');
        $aModelUser = Users::getArrObjectUserByArrUid($aUid, ROLE_CUSTOMER);
        Users::deleteArrModel($aModelUser);
    }
    
    /**
     * @Author: ANH DUNG Aug 02, 2014
     * @Todo: tạo mới custoer từ file excel và lưu vào bảng PTTT
     * @Param: $model model GasFileScan
     * belong to SavePTTTFromExcel
     */    
    public static function CreateNewCustomerAndNewPTTT($model){
        $parent_id = Yii::app()->user->parent_id;
        $type = TYPE_MARKET_DEVELOPMENT;
        $aRowInsert=array();
        foreach($model->rDetailInfo as $mDetail){
            self::BuildRowInsert($model, $mDetail, $aRowInsert, $parent_id, $type);
        }
        $tableName = GasMarketDevelopment::model()->tableName();
        $sql = "insert into $tableName (customer_id,
                        code_no,
                        maintain_date,
                        monitoring_id,
                        maintain_employee_id,
                        seri_no,
                        agent_id,
                        user_id_create,
                        type,
                        materials_name,
                        file_scan_id
                        ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert)>0)
            Yii::app()->db->createCommand($sql)->execute();           
    }
    
    /**
     * @Author: ANH DUNG Aug 02, 2014
     * @Todo: tạo mới custoer từ file excel và lưu vào bảng PTTT
     * @Param: $model model GasFileScan
     * @Param: $mDetail model GasFileScanInfo
     * belong to SavePTTTFromExcel
     */       
    public static function BuildRowInsert($model, $mDetail, &$aRowInsert, $parent_id, $type){
        $customer_id = self::CreateNewCustomer($mDetail, $parent_id);
        $code_no= "PT".date('y')."_$customer_id";
        if($customer_id){
            $aRowInsert[]="('$customer_id',
                '$code_no',
                '$model->maintain_date',
                '$model->monitoring_id',
                '$model->maintain_employee_id',
                '$mDetail->seri',
                '$model->agent_id',
                '$model->uid_login',
                '$type',
                '$mDetail->materials_name',
                '$model->id'
                )";
        }
    }
    
    public static function CreateNewCustomer($mDetail, $parent_id){
        $aAttSave = array('first_name','phone','address','address_vi','name_agent','area_code_id',
//                    'province_id','district_id','ward_id','house_numbers','street_id',
                    );
        $mUserNew = new Users();
        $mUserNew->scenario = null;
        $mUserNew->IsFromPtttExcel = 1;// biến kiểm tra KH được tạo từ import pttt file excel thì = 1
        // vì tạo từ file excel nên không build address_vi như cũ được, phải làm khác
        $mUserNew->first_name = $mDetail->customer_name;
        $mUserNew->phone = $mDetail->customer_phone;
        $mUserNew->address = $mDetail->customer_address;
        
        $mUserNew->role_id = ROLE_CUSTOMER;
        $mUserNew->application_id = BE;
        $mUserNew->type = CUSTOMER_TYPE_MARKET_DEVELOPMENT;                
        $mUserNew->code_account = MyFunctionCustom::getNextIdForUser('Users', Users::getCodeAccount($parent_id).'_', MAX_LENGTH_CODE, 'code_account', ROLE_CUSTOMER);
        $mUserNew->code_bussiness = MyFunctionCustom::genCodeBusinessCustomerAgent($mUserNew->first_name);
        $aAttSave[] = 'role_id';
        $aAttSave[] = 'application_id';
        $aAttSave[] = 'type';	
        $aAttSave[] = 'code_bussiness';
        $aAttSave[] = 'code_account';
        $mUserNew->save(true, $aAttSave);        
        return $mUserNew->id;
    }
    
    
    /**
     * @Author: ANH DUNG Mar 25, 2017
     */
    public function getAgent() {
        $aAgent = CacheSession::getListAgent();
        return isset($aAgent[$this->agent_id]) ? $aAgent[$this->agent_id]['first_name'] : '';
    }
    
    public function getEmployee() {
        $mUser = $this->rEmployee;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    public function getMonitoring() {
        $mUser = $this->rMonitoring;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    public function getMaintainDate() {
        return MyFormat::dateConverYmdToDmy($this->maintain_date);
    }
    
    
    
}