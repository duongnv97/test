<?php

/**
 * This is the model class for table "{{_gas_uphold_reply}}".
 *
 * The followings are the available columns in table '{{_gas_uphold_reply}}':
 * @property string $id
 * @property string $uphold_id
 * @property integer $status
 * @property string $date_time_handle
 * @property string $note
 * @property string $uid_login
 * @property string $created_date
 */
class GasUpholdReply extends BaseSpj
{
    public $code_complete, $mUphold, $mGasFile, $getCodeComplete = false;
    public $file_name;
    const  HOURS_ADD_COMPLETE       = 4;
    const  HOURS_ADD_COMPLETE_12    = 48;
//    const  HOURS_ADD_COMPLETE_12    = 240;// 240h = 10 ngay
    public $autocomplete_name, $autocomplete_name_second, $autocomplete_name_third, $autocomplete_name_sale;
    public $date_from, $date_to;
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_uphold_reply}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('uphold_id,status,date_time_handle', 'required', 'on'=>'create'),
            array('uphold_id, status', 'required', 'on'=>'ApiCreate'),
            array('uphold_id, status, google_map', 'required', 'on'=>'ApiReply'),
            array('note', 'checkApiReply', 'on'=>'ApiReply'),
            array('id, uphold_id, status, date_time_handle, note, uid_login, created_date', 'safe'),
            array('code_complete, customer_id, date_from, date_to, google_map, note_internal', 'safe'),
        );
    }

    /** @Author: DungNT Aug 19, 2018
     *  @Todo: custom rule validate
     **/
    public function checkApiReply($attribute,$params){
        $this->checkPhone();
        $this->checkTimeXiGas();
        if($this->status == GasUphold::STATUS_COMPLETE){
            if( $this->note == ''){
                $this->addError("note", 'Bạn chưa nhập '.$this->getAttributeLabel('note'));
            }
            if( $this->contact_phone == ''){
                $this->addError('contact_phone', 'Bạn chưa nhập '.$this->getAttributeLabel('contact_phone'). ' liên hệ của khách hàng');
            }
            $this->checkTimeReplyComplete();
            $this->checkCodeComplete();// Dec2217 Open when live
        }
    }
    
    /** @Author: DungNT Aug 08, 2019
     *  @Todo: hạn chế vấn đề thời gian hẹn giờ xử lý sự cố “xì gas” quy định là 30 phút phải đến nơi xử lý cho khách hàng
     *  biến put lên time chọn từ app là hours_handle, value = 0.08 = 5 phút, 0.17 = 10 phút, 0.50 = 30 phút --> 1 = 1 giờ
     **/
    public function checkTimeXiGas() {
        if($this->type != GasUphold::TYPE_SU_CO || $this->status != GasUphold::STATUS_HANLDE){
            return ;
        }
        $mUphold    = $this->mUphold;
        $timeAllow  = 0.5;
        if($mUphold->type_uphold == GasUphold::UPHOLD_XI_GAS && $this->hours_handle > $timeAllow ){
            $this->addError('hours_handle', "Sự cố xì Gas thời gian hẹn phải dưới $timeAllow giờ, vui lòng chọn lại thời gian xử lý");
        }
        
    }
    
    /** @Author: ANH DUNG Nov 02, 2016
     *  @Todo: kiểm tra không cho bảo trì bc sau 2h tính từ lúc chọn xử lý
     */
    public function checkTimeReplyComplete() {
        if($this->type != GasUphold::TYPE_SU_CO){
            return ;
        }
        $mReplyHandle = $this->getLastestHandle();
        $mReplyLatest = $this->getLatestReply();
        if(is_null($mReplyHandle) || $mReplyLatest->status == GasUphold::STATUS_HANLDE_LONG){
            return ;
        }
        

        $mUphold = $this->mUphold;
        if($mUphold->type_uphold == GasUphold::UPHOLD_XI_GAS){
            $timeAllow = MyFormat::addDays($mReplyHandle->date_time_handle, round(GasUpholdReply::HOURS_ADD_COMPLETE*60), "+", 'minutes', 'Y-m-d H:i:s');
        }else{
            $timeAllow = MyFormat::addDays($mReplyHandle->date_time_handle, round(GasUpholdReply::HOURS_ADD_COMPLETE_12*60), "+", 'minutes', 'Y-m-d H:i:s');
        }
        
        $ok = MyFormat::compareTwoDate($timeAllow, date('Y-m-d H:i:s'));
        if(!$ok){
            $this->addError('status', "Bạn đã vượt quá thời gian cập nhật hoàn thành. Thời gian hoàn thành cho phép của sự cố này là: ". MyFormat::dateConverYmdToDmy($timeAllow, 'd/m/Y H:i:s'));
        }
    }
    
    /** @Author: ANH DUNG Dec 22, 2017
     *  @Todo: kiểm tra mã bảo trì khi hoàn thành
     */
    public function checkCodeComplete() {
//        if($this->type != GasUphold::TYPE_SU_CO){
//            return ;
//        }// Feb0518 Run Live check code cả sự cố lẫn định kỳ
        $mUphold = $this->mUphold;
        $this->code_complete = trim($this->code_complete);
        if(empty($this->code_complete) || $mUphold->code_complete != $this->code_complete){
            $this->addError('code_complete', 'Mã bảo trì không đúng');
        }
    }
    
    /** @Author: ANH DUNG Dec 22, 2017
     *  @Todo: kiểm tra số điện thoại nhập có hợp lệ không
     */
    public function checkPhone() {
        if(empty($this->contact_phone)){
            return ;
        }
        $phoneNumber = $this->contact_phone;// 1 + 2
        if(!UsersPhone::isValidCellPhone($phoneNumber)){
            $this->addError('phone', "Số điện thoại $phoneNumber không phải là số di động hợp lệ");
        }
    }
    
    /** @Author: ANH DUNG Dec 22, 2017
     *  @Todo: sinh code định kỳ khi tạo reply trạng thái đến KH và gửi SMS đến số đt nhân viên nhập
     **/
    public function genCodeDinhKy() {
        if($this->type == GasUphold::TYPE_DINH_KY && $this->status == GasUphold::STATUS_CUSTOMER_CHECKIN){
            $this->mUphold = GasUphold::model()->findByPk($this->uphold_id);
            $this->mUphold->updateCodeComplete();
            $this->mUphold->NotifySmsReplyConfirmToCustomer($this);
        }
    }
    
    /** @Author: ANH DUNG Nov 02, 2016
     *  @Todo: lấy record có trạng thái xử lý gần nhất để check
     */
    public function getLastestHandle() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.uphold_id='.$this->uphold_id.' AND t.status='.GasUphold::STATUS_HANLDE);
        $criteria->limit = 1;// vì hàm này chạy ở beforeSave của Reply nên record chưa insert vào db => limit 1
        $criteria->order = "t.id DESC";
        return GasUpholdReply::model()->find($criteria);
    }
    
    /** @Author: ANH DUNG Jul 12, 2018
     *  @Todo: lấy record reply latest
     */
    public function getLatestReply() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.uphold_id='.$this->uphold_id);
        $criteria->limit = 1;// vì hàm này chạy ở beforeSave của Reply nên record chưa insert vào db => limit 1
        $criteria->order = "t.id DESC";
        return GasUpholdReply::model()->find($criteria);
    }
    
    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUphold' => array(self::BELONGS_TO, 'GasUphold', 'uphold_id'),
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rFile' => array(self::HAS_MANY, 'GasFile', 'belong_id',
                'on'=>'rFile.type='.  GasFile::TYPE_3_UPHOLD_REPLY,
                'order'=>'rFile.id ASC',
            ),
            'rEmployee' => array(self::BELONGS_TO, 'Users', 'employee_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'uphold_id'         => 'Uphold',
            'status'            => 'Trạng Thái',
            'date_time_handle'  => 'Ngày',
            'note'              => 'Người nghiệm thu',
            'uid_login'         => 'Nhân viên',
            'created_date'      => 'Ngày tạo',
            'note_internal'     => 'Ghi chú nội bộ',
            'contact_phone'     => 'Số điện thoại',
            'google_map'        => 'Vị trí hiện tại',
            'customer_id'        => 'Khách hàng',
            'date_from'         => 'Từ ngày',
            'date_to'           => 'Đến ngày',
            'employee_id'       => 'Nhân viên',
        );
    }

    public function searchSchedule()
    {
        $aRoleAllow = array(ROLE_DIEU_PHOI); // User tao Bt
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        
        $criteria=new CDbCriteria;
        $criteria->addCondition('t.type='.$this->type);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('t.uid_login', $this->uid_login);
        $criteria->compare('t.customer_id', $this->customer_id);
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
            $criteria->addCondition("t.created_date_only >= '$date_from'");
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
            $criteria->addCondition("t.created_date_only <= '$date_to'");
        }
        $this->handleRoleEmaintain($cRole, $cUid, $criteria);
        
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
//                'pageSize'=> 10,
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    /**
     * @Author: ANH DUNG Feb 16, 2016
     */
    public function handleRoleEmaintain($cRole, $cUid, &$criteria) {
        if(in_array($cRole, GasUphold::$ROLE_EMPLOYEE_BAOTRI) && $cUid != GasSupportCustomer::UID_HUAN_DN ){
            $criteria->compare('t.employee_id', $cUid);
        }
        $aRoleSale = array(ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT);
        if(in_array($cRole, $aRoleSale)){
            $criteria->compare('rUphold.sale_id', $cUid);
            $criteria->with = array('rUphold');
            $criteria->together = true;
        }
    }
    
    public function defaultScope()
    {
        return array();
    }
    
    protected function beforeDelete() {
        GasFile::DeleteByBelongIdAndType($this->id, GasFile::TYPE_3_UPHOLD_REPLY);
        return parent::beforeDelete();
    }
    
    protected function beforeSave() {
        $aScenarioNotCheck = array('ApiCreate', 'ApiReply');
        if($this->isNewRecord &&  !in_array($this->scenario, $aScenarioNotCheck) && isset(Yii::app()->user)){
            $this->uid_login = Yii::app()->user->id;
        }
        $this->note = InputHelper::removeScriptTag($this->note);
        if(strpos($this->date_time_handle, '/')){
            $this->date_time_handle = MyFormat::datetimeToDbDatetime($this->date_time_handle);
        }
        
        $this->created_date_only = date("Y-m-d"); // sử dụng date này để làm bc + search
        $mUphold = $this->rUphold;
        if($mUphold && $this->scenario != 'CronAutoCreate' ){
            $aUpdate = array('status'=>$this->status);
//            $mUphold->updateStatus();
            if($this->report_wrong){
                $aUpdate['report_wrong'] = $this->report_wrong;// Add on May 31, 2016 fix notify bị sai
//                $mUphold->report_wrong = $this->report_wrong;
//                $aField = array('report_wrong');
//                $mUphold->updateField($aField);
            }
            $mUphold->updateAllNotNotify($aUpdate);
            $mUphold = GasUphold::model()->findByPk($mUphold->id);
            if($mUphold->type == GasUphold::TYPE_SU_CO){
                $mUphold->NotifyAfterRepy($this);
            }
            
            // Dec 02, 2015 xử lý thêm biến xuống model detail cho phần list định kỳ của KH ở APP
            $this->customer_id = $mUphold->customer_id;
            $this->employee_id = $mUphold->employee_id;
            $this->type = $mUphold->type;
            // Dec 02, 2015 xử lý thêm biến xuống model detail cho phần list định kỳ của KH ở APP
        }
        return parent::beforeSave();
    }
    
    protected function afterSave() {
        $this->RemoveNotify();// xóa hết notify 10' nếu Bảo trì 
        return parent::afterSave();
    }
    
    /**
     * @Author: ANH DUNG Dec 02, 2015
     */
    public function RemoveNotify() {
        if($this->type == GasUphold::TYPE_SU_CO){
            GasScheduleNotify::DeleteAllByType(GasScheduleNotify::UPHOLD_ALERT_10, $this->uphold_id);
        }
    }
    
    public function getCustomer($field_name='') {
        $mUser = $this->rCustomer;
        if($mUser){
            if($field_name != ''){
                return $mUser->$field_name;
            }
            return $mUser->first_name;
        }
        return '';
    }
    
    public function getCustomerType() {
        $mUser = $this->rCustomer;
        if($mUser){
            return $mUser->getTypeCustomerText();
        }
        return '';
    }

    public function getUidLogin() {
        $mUser = $this->rUidLogin;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }

    public function getNote() {
        return nl2br($this->note);
    }
    public function getApiNote() {
        return $this->note;
    }
    public function getApiContactPhone() {
        return $this->contact_phone;
    }
    public function getApiNoteInternal() {
        return $this->note_internal;
    }
    public function getNoteInternal() {
        return nl2br($this->note_internal);
    }
    
    public function getDateTimeHandle() {
        $cmsFormater = new CmsFormatter();
        return $cmsFormater->formatDateTime($this->date_time_handle);
    }
    public function getCreatedDate($fomat = 'd/m/Y H:i') {
        return MyFormat::dateConverYmdToDmy($this->created_date, $fomat);
    }
    
    public function getHtmlMap() {
        return '<div id="map'.$this->id.'" style="width: 100%; height: 300px;"></div>';
    }
    
    public function getCustomerInfo() {
        $cmsFormater = new CmsFormatter();
        $res = '<b>Nhân viên: </b>'.$this->getUidLogin();
        $res .= '<br><br><strong>'.$this->getCustomer().'</strong><br>'.$this->getCustomer("address").'<br><br><b>'.$this->getCustomerType().'</b>';
        $res .= '<br>'. $cmsFormater->formatBreakTaskDailyFile($this);
        return $res;
    }
    
    /**
     * @Author: ANH DUNG Dec 23, 2016
     * @Todo: get info Uphold sự cố
     */
    public function getUpholdInfo() {
        $mUphold = $this->rUphold;
        $date = MyFormat::dateConverYmdToDmy($mUphold->created_date, 'H:i d/m');
        $res  = "<b>KH yêu cầu: $date</b><br>";
        $res .= '<b>'.$mUphold->getTextUpholdType().'</b>: '.$mUphold->getContentOnly();
        $res .= '<br><b>Liên hệ: '.$mUphold->getContactPerson().'</b>: '.$mUphold->getContactTel().'<br><br>';
        return $res;
    }
    
    /**
     * @Author: ANH DUNG Jun 15, 2016
     * @Todo: nếu là xử lý trong ngày thì chỉ hiển thị giờ đến, còn khác ngày thì hiển thị đầy đủ ngày giờ cho KH
     */
    public function getDateTimeHandleSMS() {
        $format = 'H:i';
        $day = MyFormat::dateConverYmdToDmy($this->date_time_handle, 'd');
        if($day != date('d')){
            $format = 'H:i d/m/y';
        }// Jun 17, 2016 luôn có cả ngày giờ vào tin sms
        $res = MyFormat::dateConverYmdToDmy($this->date_time_handle, $format);
        return $res;
    }

    public function getReplyContent() {
        $mUphold = new GasUphold();
        $res = '';
        $date           = MyFormat::dateConverYmdToDmy($this->created_date, 'H:i d/m/Y');
        $timeToCustomer = MyFormat::dateConverYmdToDmy($this->date_time_handle, 'H:i d/m/Y');
        if($this->type == GasUphold::TYPE_SU_CO){
            $res .= "<p><b>Hẹn KH:</b> {$timeToCustomer}</p>";
        }
//        $res .= "<p><b>Gửi báo cáo :</b> {$this->getCreatedDate()}</p>";
        $res .= "<p><b>Gửi báo cáo :</b> {$date}</p>";
        $res .= "<p><b>Trạng thái:</b> {$mUphold->getTextStatus($this->status)}</p>";
        if(trim($this->getNote()) != ''){
            $res .= "<p><b>{$this->getAttributeLabel('note')}:</b> {$this->getNote()}</p>";
        }
        if(trim($this->getApiContactPhone()) != ''){
            $res .= "<p><b>{$this->getAttributeLabel('contact_phone')}:</b> {$this->getApiContactPhone()}</p>";
        }
        if(trim($this->getApiNoteInternal()) != ''){
            $res .= "<p><b>Ghi chú nội bộ:</b> ".nl2br($this->getApiNoteInternal())."</p>";
        }
        $res .= "<p><b>Vị trí:</b> ".$this->google_map."</p>";

        return $res;
    }
    
    /**
     * @Author: ANH DUNG Oct 27, 2015
     * @Todo: api get data select số giờ hẹn xử lý
     */
    public static function ApiGetHoursListOption() {
        $aRes = array();
        $aRes[] = array('id'=>'', 'name'=>"Chọn Giờ Xử Lý");
        $i = 0;
        while( $i < 0.6){ // 60 phut = 1h 
            $minutes = ($i*100);
            $hours = number_format((double)($minutes/60),2);
            
            $tmp = array();
            $tmp['id'] = $hours;
            $tmp['name'] = ($i*100)." phút";
            $aRes[] = $tmp;
            
            $i += 0.05;
        }
        
        $i = 1;
        while( $i <= 48){
            $tmp = array();
            $tmp['id'] = $i;
            $tmp['name'] = "$i giờ";
            $aRes[] = $tmp;
            
            $i += 0.5;
        }
        return $aRes;
    }
    
    /**
     * @Author: ANH DUNG Oct 27, 2015
     * @Todo: Api validate multi file
     * @param: $mBelongTo là model chứa error
     * ở đây là model GasUpholdReply
     */
    public static function ApiValidateFile(&$mBelongTo) {
    try{
        $ok=false; // for check if no file submit
        if(isset($_FILES['file_name']['name'])  && count($_FILES['file_name']['name'])){
            foreach($_FILES['file_name']['name'] as $key=>$item){
                $mFile = new GasFile('UploadFile');
                $mFile->file_name  = CUploadedFile::getInstanceByName( "file_name[$key]");
                $mFile->validate();
                if(!is_null($mFile->file_name) && !$mFile->hasErrors() ){
                    $ok=true;
                    MyFormat::IsImageFile($_FILES['file_name']['tmp_name'][$key]);
                    $FileName = MyFunctionCustom::remove_vietnamese_accents($mFile->file_name->getName());
                    if(strlen($FileName) > 100 ){
                        $mFile->addError('file_name', "Tên file không được quá 100 ký tự, vui lòng đặt tên ngắn hơn");
                    }
                }
                if($mFile->hasErrors()){
                    $mBelongTo->addError('file_name', $mFile->getError('file_name'));
                }
            }
        }
//        if($mBelongTo->type == GasUphold::TYPE_DINH_KY && !$ok){
        if(0){// Dec 26, 2016 bỏ tạm không required up file với BT định kỳ nữa
            $mBelongTo->addError('hours_handle', "Bạn chưa nhập hình ảnh phiếu nghiệm thu");//bỏ tạm lỗi vào biến hours_handle
        }
        
    } catch (Exception $ex) {
        throw new Exception($ex->getMessage());
    }
    }
    
    /**
     * @Author: ANH DUNG Nov 09, 2015
     * @Todo: get array file
     */
    public function ApiGetFileUpload() {
        $aRes = array();
        foreach ($this->rFile as $mFile){
            $tmp = array();
            $tmp['thumb'] = ImageProcessing::bindImageByModel($mFile,'','',array('size'=>'size1'));
            $tmp['large'] = ImageProcessing::bindImageByModel($mFile,'','',array('size'=>'size2'));
            $aRes[] = $tmp;
        }
        return $aRes;
    }
    
    /**
     * @Author: ANH DUNG Now 28, 2015
     * @Todo: get hourse < 1
     */
    public function formatHoursHandle() {
        // nothing to do Jun 10, 2016
        return ;
        $replace = array("0.0","0.");
    }
    
    public function getReportWrongApi(){
        if($this->report_wrong){
            return "Khách hàng báo sai sự cố";
        }
        return '';
    }
    
    /**
     * @Author: ANH DUNG Dec 02, 2015
     * @param: model $mUphold
     */
    public static function CronCreateReplyDinhKy($mUphold) {
        $mUpholdReply = new GasUpholdReply("CronAutoCreate");
        $mUpholdReply->uphold_id = $mUphold->id;
        $mUpholdReply->status = GasUphold::STATUS_NEW;
        $mUpholdReply->uid_login = $mUphold->uid_login;
        $mUpholdReply->created_date_only = date("Y-m-d");
        $mUpholdReply->customer_id = $mUphold->customer_id;
        $mUpholdReply->employee_id = $mUphold->employee_id;
        $mUpholdReply->type = $mUphold->type;
        $mLatestReply = $mUpholdReply->findRecordDinhKy();
        if($mLatestReply){
            return $mLatestReply;
        }
        $mUpholdReply->save();
        return $mUpholdReply;
    }
    
    /**
     * @Author: ANH DUNG Dec 03, 2015
     * @Todo: find with created_date_only and customer id
     */
    public function findRecordDinhKy() {
        $criteria = new CDbCriteria();
        $criteria->compare("t.created_date_only", date("Y-m-d"));
        $criteria->compare("t.customer_id", $this->customer_id);
        $criteria->compare("t.employee_id", $this->employee_id);
        $criteria->compare("t.type", $this->type);
        $criteria->order = "t.id DESC";
        return self::model()->find($criteria);
    }
    
    /**
     * @Author: ANH DUNG Sep 09, 2016
     * @Todo: validate some field of api
     */
    public function apiValidateReply() {
        return ; // Sep 09, 2016 không cần validate ở đây nữa đã làm ở $mUpholdReply->validate();
        if($this->type == GasUphold::TYPE_DINH_KY){
            if($this->note == ''){
                $this->addError('note',$this->getAttributeLabel('note')." không thể trống");
            }
            if($this->contact_phone == ''){
                $this->addError('contact_phone',$this->getAttributeLabel('contact_phone')." không thể trống");
            }
        }
    }
    
    /** @Author: ANH DUNG Apr 05, 2018
     *  @Todo: update uphold_reply_id to uphold plan, khi NV hoàn thành định kỳ
     **/
    public function updateToPlan() {
        if($this->type == GasUphold::TYPE_SU_CO || $this->status != GasUphold::STATUS_COMPLETE){
            return;
        }
        $mUpholdPlan = new GasUpholdPlan();
        $mUpholdPlan->customer_id   = $this->customer_id;
        $mUpholdPlan = $mUpholdPlan->getPlanLastestOfCustomer();
        if($mUpholdPlan && empty($mUpholdPlan->uphold_reply_id)){
            $mUpholdPlan->uphold_id         = $this->uphold_id;
            $mUpholdPlan->uphold_reply_id   = $this->id;
            $mUpholdPlan->update();
        }
        $this->updateToNextUphold();
    }
    
    /** @Author: ANH DUNG May 18, 2018
     *  @Todo: update next uphold với sau khi trả lời hoàn thành định kỳ
     **/
    public function updateToNextUphold() {
        $mUphold = GasUphold::model()->findByPk($this->uphold_id);
        $mUphold->updateNextUpholdOneRecord();
    }
    
    /** @Author: ANH DUNG May 10, 2018
     *  @Todo: get lastest reply định kỳ của KH
     **/
    public function getLastestReplyDinhKy() {
        $criteria = new CDbCriteria();
        $criteria->compare('t.customer_id', $this->customer_id);
        $criteria->compare('t.type', GasUphold::TYPE_DINH_KY);
        $criteria->compare('t.status', GasUphold::STATUS_COMPLETE);
        $criteria->order = 't.id DESC';
        $criteria->limit = 1;
        return self::model()->find($criteria);
    }
    
}