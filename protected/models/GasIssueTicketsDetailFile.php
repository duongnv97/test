<?php

/**
 * This is the model class for table "{{_gas_issue_tickets_detail_file}}".
 *
 * The followings are the available columns in table '{{_gas_issue_tickets_detail_file}}':
 * @property string $id
 * @property string $issue_tickets_detail_id
 * @property string $file_name
 * @property integer $order_number
 * @property string $created_date
 */
class GasIssueTicketsDetailFile extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasIssueTicketsDetailFile the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_issue_tickets_detail_file}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id, issue_tickets_detail_id, file_name, order_number, created_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                'id' => 'ID',
                'issue_tickets_detail_id' => 'Ticket',
                'file_name' => 'File Name',
                'order_number' => 'Order Number',
                'created_date' => 'Created Date',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id,true);
		$criteria->compare('t.issue_tickets_detail_id',$this->issue_tickets_detail_id,true);
		$criteria->compare('t.file_name',$this->file_name,true);
		$criteria->compare('t.order_number',$this->order_number);
		$criteria->compare('t.created_date',$this->created_date,true);

            return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                ),
            ));
    }

    /*
    public function activate()
    {
        $this->status = 1;
        $this->update();
    }

    public function deactivate()
    {
        $this->status = 0;
        $this->update();
    }
	*/

    public function defaultScope()
    {
            return array(
                    //'condition'=>'',
            );
    }
    
    protected function beforeDelete() {
        GasIssueTickets::RemoveFileOnly($this->id, 'file_name');
        return parent::beforeDelete();
    }
}