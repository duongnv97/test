<?php
include_once 'config.local.php';

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=> $THEME_NAME,
        'defaultController' => 'admin/site/index',
        'theme'=> $THEME,
		'language' => 'en',
		
	// preloading 'log' component
	'preload'=>array('log', 'ELangHandler'),

	// autoloading model and component classes
	'import'=>array(
                'application.commands.PushServerCommand',// Add From Oct 06, 2016
		'application.models.*',
		'application.components.*',
		'application.components.report.*',
		'application.components.excel.*',
                'application.extensions.yii-mail.*',
		'application.extensions.EUploadedImage.*',
                'application.extensions.EPhpThumb.*',
                'application.extensions.MyDebug.*',
		'application.extensions.editMe.*',
                'application.extensions.ControllerActionsName.*',

//                'application.cms.controllers.*',
//                'application.cms.models.*',
//                'application.cms.components.*',
//                'application.modules.auditTrail.models.AuditTrail',
                'application.extensions.toSlug.*',
                'application.modules.api.models.*',// Oct 09, 2015
                'application.modules.api.components.*',
                'application.extensions.yii-apns-gcm-master.*',// Oct 31, 2015
                'application.components.widgets.*',// Jun 09, 2016
                'application.extensions.PushWebSocket.PushWebSocket', // Trung Jul 25, 2016: Bật tính năng push
                'application.components.barcode.*',// Feb 04, 2017
                'application.extensions.solr.*',
            ),
	'modules'=>array(
        // Sep 27, 2015 Open gii nếu tạo thêm model
//		'gii'=>array( // CLOSE WHEN LIVE SITE
//			'class'=>'system.gii.GiiModule',
//			'password'=>'123',
//		 	// If removed, Gii defaults to localhost only. Edit carefully to taste.
//			'ipFilters'=>array('127.0.0.1','::1'),
//                        'generatorPaths'=>array(
//                            'application.gii',   // a path alias
//                            'bootstrap.gii',
//                        ),
//		),
        
        'admin',
        'member',
        'api'// Oct 09, 2015
//        'auditTrail'=>array(
//			'userClass' => 'Users', // the class name for the user object
//			'userIdColumn' => 'id', // the column name of the primary key for the user
//			'userNameColumn' => 'username', // the column name of the primary key for the user
//		),
	),

	// application components
	'components'=>array(
            'geoip' => array(
                    // http://www.yiiframework.com/extension/geoip/
                    // !important => please read readme
                          'class' => 'application.extensions.geoip.CGeoIP',
                          // specify filename location for the corresponding database
                          'filename' => ROOT.'/upload/GeoLiteCity.dat',
                          // Choose MEMORY_CACHE or STANDARD mode
                          'mode' => 'STANDARD',
                      ),
            'session' => array(
                'class' => 'CDbHttpSession',
                'timeout' => 58800, // 12 hours, 46800 - 13 hours, 50400 - 14 hours, 54000 - 15 hours, 57600 - 16 hours, 
                'cookieParams' => array(
                    // http://www.yiiframework.com/forum/index.php/topic/12849-chttpcookie-httponly/
    //                'httpOnly' => true,// not run http://www.yiiframework.com/forum/index.php/topic/12849-chttpcookie-httponly/
                    'httponly' => true,// The Yii code which sets the cookie uses extract($value) to convert the array into variables, and it's looking for $httponly
                ),
            ),

            'user'=>array(
                // enable cookie-based authentication
                'allowAutoLogin'=>true,
                'class' => 'WebUser',
                'loginUrl'=>array('/admin/site/login'),
            ),

            'urlManager'=>array(
                'urlFormat'=>'path',
    //            'rules'=>array(
    //                '<action:(error|unsubscribe)>'=>'admin/site/<action>',
    //                '<action:(login|logout|error|changePassword)>'=>'admin/site/<action>',
    //                // for hide admin in url
    //                '<controller:\w+>/<id:\d+>'=>'admin/<controller>/view',
    //                '<controller:\w+>/<action:\w+>/<id:\d+>'=>'admin/<controller>/<action>',
    //                '<controller:\w+>/<action:\w+>'=>'admin/<controller>/<action>',
    //                '<controller:\w+>'=>'admin/<controller>/index',
    //                // for hide admin in url
    //                '<url:(admin|member)>'=>'<url>/site/',
    //            ),            

                'rules'=>array(
                    // profile scan QR code Now2418
                    'code_account/<code_account:[a-zA-Z0-9-]+>'=> array('site/profile'),
                    
                    // seo page
                    'page/<slug:[a-zA-Z0-9-]+>'=> array('member/page/view_page_info'),

                    //seo tag
                    'tag/<name:[a-zA-Z0-9-]+>'=> array('member/tag/view_tag_info'),

                    //seo tag
                    'category/<slug:[a-zA-Z0-9-]+>'=> array('member/category/view_category_info'),

                    //seo post : post/view_post_info/slug/this-post-demo2/cat/clothes
                    'category/<cat:[a-zA-Z0-9-]+>/<slug:[a-zA-Z0-9-]+>'=> array('member/post/detail_post'),

                    '<action:(error|unsubscribe|map)>'=>'site/<action>',
                    'admin/<action:(login|logout|error|changePassword)>'=>'admin/site/<action>',
                    'member/<action:(error|login|logout|forgot_password|register|change_password)>'=>'member/site/<action>',
                    'member/<action:(profile|edit_profile|change_password)>'=>'member/users/<action>',
                    'member/verify_register/<id>'=>'member/site/verify_register',
                    '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                    '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                    '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                    '<url:(admin|member)>'=>'<url>/site/',

                ),

                'showScriptName'=>false,
            ),

            'db'=>array(
                'connectionString' => "mysql:host=$MYSQL_HOSTNAME;dbname=$MYSQL_DATABASE",
                'emulatePrepare' => true,
                'username' => $MYSQL_USERNAME,
                'password' => $MYSQL_PASSWORD,
                'tablePrefix'=> $TABLE_PREFIX,
                'charset' => 'utf8',
                'enableProfiling'=>true,
                'enableParamLogging'=>true,
            ),

            'authManager'=>array(
                'class'=>'CDbAuthManager',
                'connectionID'=>'db',
            ),

                    'errorHandler'=>array(
                            // use 'site/error' action to display errors
                'errorAction'=>'admin/site/error',
            ),
            'log'=>array(
                'class'=>'CLogRouter',
                'routes'=>array(
                    array(
                        'class'=>'CFileLogRoute',
                        //'levels'=>'error, warning',
                    ),
                    array(
                        'class' => 'DbLogRoute',
                        'connectionID' => 'db',
                        'autoCreateLogTable' => false,
                        'logTableName' => $TABLE_PREFIX."_logger",
                        'levels' => 'info, error'
                        // nota:categories removed from me
                        //'categories' => 'cclinica',
                    ),
                                    // uncomment the following to show log messages on web pages
    //				array(
    //					'class'=>'CWebLogRoute',
    //				),
                    array(
                        'class'=>'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
                        'ipFilters'=>array(isset($_COOKIE['debug']) ? '127.0.0.1':'0.0.0.0'),
                    ),
                ),
            ),

            'mail' => array(
                'class' => 'application.extensions.yii-mail.YiiMail',
                'transportType'=>'smtp', /// case sensitive!
                'transportOptions'=>array(
                    'host'=>'smtp.gmail.com',
                    'username'=>'dungverz@gmail.com',
                    'password'=>'dung!@#123',
                    'port'=>'465',
                    'encryption'=>'ssl',
                    'timeout'=>'120',
                ),
                'viewPath' => 'application.mail',
                'logging' => true,
    //            'logging' => false,
                'dryRun' => false
            ),

            'setting'=>array(
                'class' =>  'application.extensions.MyConfig.MyConfig',
                'cacheId'=>null,
                'useCache'=>false,
                'cacheTime'=>0,
                'tableName'=> $TABLE_PREFIX . '_settings',
                'createTable'=>false,
                'loadDbItems'=>true,
                'serializeValues'=>true,
                'configFile'=>'',
            ),

            'format'=>array(
                'class'=>'CmsFormatter',
            ),

            // Sep 21, 2015 - begin notify app ios+android https://github.com/bryglen/yii-apns-gcm -- 
            'apns' => array(
                'class' => 'ext.yii-apns-gcm-master.YiiApns',
                'environment' => 'production',
//                 'environment' => 'sandbox',
    //            'pemFile' => Yii::getPathOfAlias('application.vendors').'/apnssert/apns-dev.pem', // wrong
    //            'pemFile' => Yii::getPathOfAlias('application.vendors').'/apnssert/ck.pem', // wrong
    //            'pemFile' => dirname(__FILE__).'/apnssert/ck.pem',// ok for test ios bb
                'pemFile' => dirname(__FILE__).'/apnssert/apns-dev.pem',
                'dryRun' => false, // setting true will just do nothing when sending push notification
                // 'retryTimes' => 3,
                'enableLogging' => false,// Dec 21, 2016 disable log
                'options' => array(
                    'sendRetryTimes' => 5
                ),
            ),
            'gcm' => array(
                'class' => 'ext.yii-apns-gcm-master.YiiGcm',
                'apiKey' => $GcmApiKey // daukhimiennam.com
    //            'apiKey' => $GcmApiKey // android.huongminhgroup.com
            ),
            // using both gcm and apns, make sure you have 'gcm' and 'apns' in your component
            'apnsGcm' => array(
                'class' => 'ext.yii-apns-gcm-master.YiiApnsGcm',
                // custom name for the component, by default we will use 'gcm' and 'apns'
                //'gcm' => 'gcm',
                //'apns' => 'apns',
            ),
            // Sep 21, 2015 - begin notify app ios+android
            'cache'=>array(// add Oct 19, 2016
                'class'=>'system.caching.CFileCache',
            ),
            
            'ePdf' => array(// Feb 28, 2017
                'class'         => 'ext.yii-pdf.EYiiPdf',
                'params'        => array(
                    'mpdf'     => array(
                        'librarySourcePath' => 'application.vendors.mpdf.*',
                        'constants'         => array(
                            '_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
                        ),
                        'class'=>'mpdf', // the literal class filename to be loaded from the vendors folder
                        /*'defaultParams'     => array( // More info: http://mpdf1.com/manual/index.php?tid=184
                            'mode'              => '', //  This parameter specifies the mode of the new document.
                            'format'            => 'A4', // format A4, A5, ...
                            'default_font_size' => 0, // Sets the default document font size in points (pt)
                            'default_font'      => '', // Sets the default font-family for the new document.
                            'mgl'               => 15, // margin_left. Sets the page margins for the new document.
                            'mgr'               => 15, // margin_right
                            'mgt'               => 16, // margin_top
                            'mgb'               => 16, // margin_bottom
                            'mgh'               => 9, // margin_header
                            'mgf'               => 9, // margin_footer
                            'orientation'       => 'P', // landscape or portrait orientation
                        )*/
                    ),
                    'HTML2PDF' => array(
                        'librarySourcePath' => 'application.vendors.html2pdf.*',
                        'classFile'         => 'html2pdf.class.php', // For adding to Yii::$classMap
                        /*'defaultParams'     => array( // More info: http://wiki.spipu.net/doku.php?id=html2pdf:en:v4:accueil
                            'orientation' => 'P', // landscape or portrait orientation
                            'format'      => 'A4', // format A4, A5, ...
                            'language'    => 'en', // language: fr, en, it ...
                            'unicode'     => true, // TRUE means clustering the input text IS unicode (default = true)
                            'encoding'    => 'UTF-8', // charset encoding; Default is UTF-8
                            'marges'      => array(5, 5, 5, 8), // margins by default, in order (left, top, right, bottom)
                        )*/
                    )
                ),
            ),  
            
            'solrUser' => array(// Dec1417
                'class' => 'CSolrComponent',
//                'host' => '128.199.148.191',
                'host' => '128.199.205.12',
                'port' => 8983,
                'indexPath' => '/solr/gas_users',
            ),
            
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
            'niceditor'=>array(),
            'niceditor_v_1'=>array('bold','italic','underline','indent','outdent','ol','ul','fontSize','left','center','right','justify','forecolor','bgcolor','image','upload'),
            'niceditor_v_2'=>array('xhtml','bold','italic','underline','indent','outdent','ol','ul','fontSize','left','center','right','justify','forecolor','bgcolor','image','upload'),
//            'niceditor_v_2'=>array(),

            'adminEmail'=>'webmaster@example.com',
            'autoEmail'=>'auto_mailer@starlets22.com',
            'dateFormat'=>'d/m/Y',
            'timeFormat'=>'H:i',
            'defaultPageSize'=>20,
        ),
);