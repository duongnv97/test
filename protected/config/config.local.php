<?php 
include_once 'host/info.php';

define('BE', 1);
define('FE', 2);

define('ROLE_ALL', 0); // DuongNV Hr 08/08/18 Role chung cho tất cả
define('ROLE_MANAGER', 1);
define('ROLE_ADMIN', 2);
define('ROLE_SALE', 3); // Nhân Viên Kinh Doanh
define('ROLE_CUSTOMER', 4);
define('ROLE_AGENT', 5);
define('ROLE_MEMBER', 6);
define('ROLE_EMPLOYEE_MAINTAIN', 7); // NV Phục Vụ Khách Hàng 
define('ROLE_CHECK_MAINTAIN', 8);// NV Test Bảo Trì
define('ROLE_ACCOUNTING_AGENT', 9); // NV Kế Toán Bán Hàng
define('ROLE_ACCOUNTING_AGENT_PRIMARY', 10); // NV Kế Toán Đại Lý
define('ROLE_ACCOUNTING_ZONE', 11); // NV Kế Toán Khu Vực
define('ROLE_MONITORING', 12); // NV Giám Sát
define('ROLE_MONITORING_MAINTAIN', 13);
define('ROLE_MONITORING_MARKET_DEVELOPMENT', 14);// Mar0216 Chuyên Viên CCS <- Giám Sát PTTT
define('ROLE_EMPLOYEE_MARKET_DEVELOPMENT', 15); // NV PTTT => NV CCS (on Jun 08, 2016)
define('ROLE_MONITORING_STORE_CARD', 16); // Quản Lý Kho
define('ROLE_DIEU_PHOI', 17); // Điều Phối
define('ROLE_SCHEDULE_CAR', 18); // Điều Xe
define('ROLE_DIRECTOR', 19); // Giám Đốc
define('ROLE_SUB_USER_AGENT', 20); // User Đại Lý
define('ROLE_DRIVER', 21); // Lái Xe
define('ROLE_ACCOUNT_RECEIVABLE', 22); // Kế Toán Công Nợ
define('ROLE_HEAD_GAS_BO', 23);// Trưởng Phòng KD Gas Bò
define('ROLE_HEAD_GAS_MOI', 24); // Trưởng Phòng KD Gas mối
define('ROLE_DIRECTOR_BUSSINESS', 25);// Giám Đốc Kinh Doanh
define('ROLE_RECEPTION', 26);// lễ tân
define('ROLE_CHIEF_ACCOUNTANT', 27);// Kế Toán Trưởng
define('ROLE_CHIEF_MONITOR', 28);// Tổng giám sát
define('ROLE_MONITOR_AGENT', 29);// giám sát đại lý
define('ROLE_SECRETARY_OF_THE_MEETING', 30);// THƯ KÝ CUỘC HỌP
define('ROLE_HEAD_OF_LEGAL', 31);// Trưởng Phòng Pháp Lý
define('ROLE_EMPLOYEE_OF_LEGAL', 32);// Nhân Viên Pháp Lý
define('ROLE_ACCOUNTING', 33);// Nhân Viên Kế Toán VP
define('ROLE_DEBT_COLLECTION', 34);// Nhân Viên Thu Nợ
define('ROLE_HEAD_TECHNICAL', 35); // Trưởng Phòng Kỹ Thuật
define('ROLE_HEAD_OF_MAINTAIN', 36); // Tổ Trưởng Tổ Bảo Trì
define('ROLE_E_MAINTAIN', 37); // NV Bảo Trì
define('ROLE_SECURITY_SYSTEM', 38); // Security System - Setting cho hệ thống
define('ROLE_BUSINESS_PROJECT', 39); // NV Kinh Doanh Dự Án
define('ROLE_HEAD_OF_BUSINESS', 40); // Giám Đốc Kinh Doanh Khu Vực
define('ROLE_WORKER', 41); // Công nhân Dec 26, 2014
define('ROLE_SECURITY', 42); // Bảo Vệ Dec 26, 2014
define('ROLE_MANAGING_DIRECTOR', 43); // Quản Đốc Dec 29, 2014
define('ROLE_CRAFT_WAREHOUSE', 44); // Thủ Kho Dec 29, 2014
define('ROLE_HEAD_GAS_FAMILY', 45); // Trưởng Phòng Gas Gia Đình Jan 20, 2015
define('ROLE_CALL_CENTER', 46); // NV tổng đài Fix Mar 10, 2017 -> Sep 03, 2015
define('ROLE_CHIET_NAP', 47); // NV chiết nạp Sep 03, 2015
define('ROLE_PHU_XE', 48); // NV Phụ Xe Sep 03, 2015
define('ROLE_SUB_DIRECTOR', 49); // Phó Giám Đốc Chi Nhánh SEP 11, 2015
define('ROLE_ITEMS', 50); // Vật Tư SEP 11, 2015
define('ROLE_CASHIER', 51); // Thủ Quỹ SEP 11, 2015
define('ROLE_MECHANIC', 52); // Cơ Khí SEP 11, 2015
define('ROLE_TECHNICAL', 53); // NV Kỹ Thuật SEP 19, 2015
define('ROLE_AUDIT', 54); // NV AUDIT dec 15, 2015
define('ROLE_SALE_ADMIN', 55); // NV QUAN LY KH SALE ADMIN MAR 12, 2016
define('ROLE_IT', 56); // Phòng Công Nghệ Thông Tin Apr 04, 2016
define('ROLE_IT_EMPLOYEE', 57); // NV Công Nghệ Thông Tin Apr 04, 2016
define('ROLE_BRANCH_DIRECTOR', 58); // Giám Đốc Chi Nhánh May 08, 2016
define('ROLE_CLEANER', 59); // NV Tạp Vụ May 08, 2016
define('ROLE_MANAGER_DRIVER', 60); // Quản Lý Đội Xe May 17, 2016
define('ROLE_COMPANY', 61); // Công Ty Trực Thuộc Jul 10, 2016
define('ROLE_CAR', 62); // Xe Ô Tô Tải - Jul 25, 2016
define('ROLE_BANK', 63); // Ngân Hàng - Jul 31, 2016
define('ROLE_GUEST', 64); // User Khách - Dec 19, 2016
define('ROLE_EXECUTIVE_OFFICER', 65); // Giám Đốc Điều Hành - Apr 05, 2017
define('ROLE_ASSETS', 66); // Tài Sản - Jun 11, 2017
define('ROLE_FAMILY', 67); // Người thân nhân viên - Aug2117
define('ROLE_MARKETING', 68);// Marketing
define('ROLE_DEPARTMENT', 69);// Phòng Ban
define('ROLE_CANDIDATE', 70);// Ứng viên xin việc
define('ROLE_PARTNER', 71); // GAS24H Partner
define('ROLE_TELESALE', 72);// Telesale
define('ROLE_TEST_CALLL', 73); // Test Call
define('ROLE_DLLK_MANAGE', 74);// ĐLLK - Tài khoản quản lý 
define('ROLE_BRANCH_COMPANY', 75);// Chi Nhánh

define('BANNER_1_WIDTH', 960);
define('BANNER_1_HEIGHT', 300);
define('BANNER_2_WIDTH', 230);
define('BANNER_2_HEIGHT', 69);
define('STATUS_INACTIVE', 0);
define('STATUS_ACTIVE', 1);
define('STATUS_WAIT_ACTIVE', 2);
define('STATUS_REJECT', 3);
define('STATUS_LEAVE_BORN', 4);// Aug1817 nghỉ thai sản
define('STATUS_LEAVE_TEMP', 5);// Aug1817 nghỉ tạm

define('PASSW_LENGTH_MIN', 6);
define('PASSW_LENGTH_MAX', 50);
define('IMAGE_ADMIN_WIDTH', 260);
define('IMAGE_ADMIN_HEIGHT', 200);
define('IMAGE_ADMIN_THUMB_WIDTH', 117);
define('IMAGE_ADMIN_THUMB_HEIGHT', 90);
//max records in logger table
define('LOGGER_TABLE_MAX_RECORDS', 10000);
define('IMAGE_ADMIN_GALLERY_WIDTH', 560);
define('IMAGE_ADMIN_GALLERY_HEIGHT', 400);
define('IMAGE_ADMIN_GALLERY_THUMB_WIDTH', 153);
define('IMAGE_ADMIN_GALLERY_THUMB_HEIGHT', 111);
define('BLOCK_UI_COLOR', '#fff');
define('MIN_LENGTH_AUTOCOMPLETE', 2);

define('KL_BINH_50', 50);
define('KL_BINH_45', 45);
define('KL_BINH_12', 12);
define('KL_BINH_6', 6);
define('KL_BINH_4', 4);

define('PAY_TYPE_1', 1);
define('PAY_TYPE_2', 2);
define('PAY_TYPE_3', 3);
define('PAY_TYPE_4', 4);

define('PAY_IN_MONTH', 0);
define('PAY_NEXT_MONTH', 1);

define('JSON_DAYS', 'JSON_DAYS');
define('JSON_DAY_PAY', 'JSON_DAY_PAY');
define('JSON_DAY_PAY_IN_MONTH', 'JSON_DAY_PAY_IN_MONTH');

define('TRONG_HAN', 1);
define('TOI_HAN', 2);
define('QUA_HAN', 3);

define('DATE_TON_DAU', '2013-06-01');

define('ID_CHI_PHI_KHUYEN_MAI', 4);
define('ID_MATERIAL_KHUYEN_MAI', 41);

define('EDITOR_WIDTH', '515px');
define('EDITOR_HEIGHT', '150px');

define('STATUS_NOT_YET_CALL', 1);
define('STATUS_HAS_CALL_OK', 2);
define('STATUS_HAS_CALL_FAILED', 3);
define('STATUS_HAS_CALL_OK_LEVEL_1', 4);
define('STATUS_HAS_CALL_OK_LEVEL_2', 5);
define('STATUS_HAS_CALL_OK_LEVEL_3', 6);

define('MAX_LENGTH_CODE', 15); // like CH7_0003703
define('CUSTOMER_NEW', 1); // Khách hàng mới
define('CUSTOMER_OLD', 2); // Khách hàng cũ
define('CUSTOMER_PAY_NOW', 3); // Khách hàng trả tiền ngay, dùng trong thẻ kho

define('ONE_AGENT_MAINTAIN', 1); // table one_many, nghia la 1 agent co nhieu nhân viên bảo trì
define('ONE_AGENT_ACCOUNTING', 2); // table one_many, nghia la 1 agent co nhieu nhân viên kế toán
define('ONE_SELL_MAINTAIN_PROMOTION', 3); // table one_many_big: nghĩa là 1 bán hàng của bảo trì dc tặng nhiều quà tặng 
define('ONE_MONITORING_MARKET_DEVELOPMENT', 4); // table one_many_big: nghĩa là 1 giám sát PTTT có nhiều nhiều nhân viên
define('ONE_USER_MANAGE_MULTIUSER', 5); // Dec 06, 2014 table one_many_big: trạng thái 5 này có nghĩa là 1 user có thể có ( quản lý) nhiều sub user thuộc nhiều role khác nhau
define('ONE_AGENT_CCS', 6); // Jul 09, 2016 table one_many, nghia la 1 agent co nhieu nhân viên CCS làm chương trình
define('ONE_AGENT_CAR', 7); // Jul 30, 2016 table one_many, nghia la 1 agent co nhieu Xe Tải :d
define('ONE_AGENT_DRIVER', 8); // Jul 30, 2016 table one_many, nghia la 1 agent co nhieu Lái Xe Tải :d
define('ONE_AGENT_PHU_XE', 9); // Jul 30, 2016 table one_many, nghia la 1 agent co nhieu Phụ Xe Tải :d
define('ONE_COMPANY_USER', 10); // Aug 02, 2016 table one_many GasOneMany, nghia la 1 user thuộc về nhiều công ty
//define('ONE_ABC', 11); ==>> số 11 đã dc define trong model, không define ở đây nữa
/* Sep 04,,2016 NOTE IMPORTANT TABLE one_many SẼ ĐƯỢC DEFINE Ở TRONG MODEL GasOneMany, KHÔNG DEFINE Ở ĐÂY NỮA */
// ******* Sep 24, 2015 NOTE: từ type = 6 trở đi sẽ define ở model one_many_big *******/
// ******* Sep 24, 2015 NOTE: từ type = 6 trở đi sẽ define ở model one_many_big *******/

define('CUSTOMER_TYPE_MAINTAIN', 1); // là cột type trong bảng user, KH bảo trì - đã bỏ ko dùng Jun 08,2 016
define('CUSTOMER_TYPE_MARKET_DEVELOPMENT', 2); // KH phát triển thị trường - đã bỏ Jun ko dùng 08,2 016
define('CUSTOMER_TYPE_STORE_CARD', 3); // tách riêng khách hàng bình bò+mối vs KH bảo trì or pttt
define('TYPE_MAINTAIN', 1);
define('TYPE_MARKET_DEVELOPMENT', 2);
define('TYPE_SELLING_3', 3);
define('NO_PHONE_NUMBER', 'NA');

define('TYPE_STORE_CARD_IMPORT', 1);
define('TYPE_STORE_CARD_EXPORT', 2);
define('LENGTH_STORE_CARD', 14);
define('LENGTH_STORE_CARD_18', 18);
define('LENGTH_CASH_BOOK', 15);
define('LENGTH_ORDER', 14);
define('LENGTH_ORDER_BY_YEAR', 10);
define('LENGTH_TICKET', 8);

define('STORE_CARD_TYPE_1', 1); // nhập nội bộ
define('STORE_CARD_TYPE_2', 2); // nhập mua
define('STORE_CARD_TYPE_3', 3); // xuất bán
define('STORE_CARD_TYPE_4', 4); // xuất nội bộ
define('STORE_CARD_TYPE_5', 5); // nhập vỏ
define('STORE_CARD_TYPE_6', 6); // nhập khác
define('STORE_CARD_TYPE_7', 7); // xuất khác
define('STORE_CARD_TYPE_8', 8); // xuất trả
define('STORE_CARD_TYPE_9', 9); // xuất tặng
define('STORE_CARD_TYPE_10', 10); // Xuất Bán Bộ Bình
define('STORE_CARD_TYPE_11', 11); // Xuất Bán Thế Chân
define('STORE_CARD_TYPE_12', 12); // Xuất Bán Vỏ
define('STORE_CARD_TYPE_13', 13); // Xuất Cho Bảo Trì
define('STORE_CARD_TYPE_14', 14); // Nhập Vỏ Thế Chân
define('STORE_CARD_TYPE_15', 15); // Nhập Thu Hồi Và Đổi
define('STORE_CARD_TYPE_16', 16); // Nhập Chiết
define('STORE_CARD_TYPE_17', 17); // Xuất chiết
define('STORE_CARD_TYPE_18', 18); // NHẬP ĐỔI VỎ
define('STORE_CARD_TYPE_19', 19); // Xuất ĐỔI VỎ
define('STORE_CARD_TYPE_20', 20); // Xuất Dùm May 30, 20216
define('STORE_CARD_TYPE_21', 21); // Nhập mua Vỏ Feb0618
define('STORE_CARD_TYPE_22', 22); // Nhập chiết thuê Feb1819
define('STORE_CARD_TYPE_23', 23); // Xuất chiết thuê Feb1819
define('STORE_CARD_TYPE_24', 24); // Xuất kho treo CN trừ lương

define('MASTER_TYPE_REVENUE', 1); // THU 
define('MASTER_TYPE_COST', 2); // CHI
define('MASTER_TYPE_LOOKUP_CASH_BOOK', 1); // SỔ QUỸ TIỀN MẶT

define('OPENING_BALANCE_MATERIAL', 1); // DƯ ĐẦU KỲ VẬT TƯ ĐẠI LÝ
define('OPENING_BALANCE_CASH_BOOK', 2); // DƯ ĐẦU KỲ SỔ QUỸ TIỀN MẶT ĐẠI LÝ

define('STORE_CARD_KH_BINH_BO', 1); // khách hàng bình bò là cột is_maintain trong table user
define('STORE_CARD_KH_MOI', 2); // khách hàng bình mối
define('CUSTOMER_HO_GIA_DINH', 3); // hooj gia dinh, (Jun 08, 2016 đây là loại KH của hộ GD thẻ kho )
define('CUSTOMER_OTHER', 4); // Không phải KH 3 loai tren
define('CUSTOMER_IS_AGENT', 5); // KH LÀ ĐẠI LÝ KHÁC TRONG HỆ THỐNG
define('CUSTOMER_OTHER_OBJ', 0); // LOẠI NÀY CHÍNH LÀ LOẠI 4 NHƯNG MÀ SỢ CHANGE LOẠI 4 NÊN THÊM 0 CÁI NÀY VÔ
define('STORE_CARD_XE_RAO', 6); // Apr 14, 2015 khách hàng xe rao, cùng cấp với kh bò mối
define('STORE_CARD_VIP_HGD', 7); // Jun 08, 2016 khách hàng vip hộ gia đình, cùng cấp với kh bò mối
define('STORE_CARD_HGD', 8); // Jun 08, 2016 khách hàng hộ gia đình, tạo ở C# window và web sẽ có bán hàng qua điện thoại
define('STORE_CARD_HGD_CCS', 9); // Aug 08, 2016 khách hàng hộ gia đình, tạo ở ANDROI do CCS đi từng nhà lấy thông tin
// define('STORE_CARD_HGD_xxx', 10); // Sep 05, 2016 số 11 đã dc define trong model UsersExtend, không define ở đây nữa 
// STOP DEFINE STORE_CARD_ customer ở đây => move vào model UsersExtend.  có thể khai báo thêm các loại KH storecard ở đây 7,8,9,10

define('TYPE_GAS_12KG', 4); // khách hàng bình mối
define('TYPE_GAS_BINH_BO', 7); // khách hàng bình bò
define('CHI_NOP_NGAN_HANG', 23); // chi nộp ngân hàng
define('ID_KH_HO_GIA_DINH', 48694); // chi nộp ngân hàng

define('MATERIAL_TYPE_BINHBO_50', 11); // Gas Bình Bò 50 Kg
define('MATERIAL_TYPE_BINHBO_45', 7); // Gas Bình Bò 45 Kg
define('MATERIAL_TYPE_BINH_12', 4); // Gas Bình 12 Kg
define('MATERIAL_TYPE_BINH_6', 9); // Gas Bình 6 Kg
define('MATERIAL_TYPE_BINH_4', 19); // Gas Bình 4 Kg
define('MATERIAL_TYPE_VO_50', 12); // Vỏ Bình Bò 50 Kg
define('MATERIAL_TYPE_VO_45', 10); // Vỏ Bình Bò 45 Kg
define('MATERIAL_TYPE_VO_12', 1); // Vỏ Bình 12 Kg
define('MATERIAL_TYPE_VO_6', 14); // Vỏ Bình 6 Kg
define('MATERIAL_TYPE_VO_4', 34); // Vỏ Bình 4 Kg

// Toan
define('VERZ_COOKIE_ADMIN', md5('verz_cookie_admin'));
define('VERZ_COOKIE', md5('verz_cookie'));
define('VERZLOGIN', md5('verz_login'));
define('VERZLPASS', md5('verz_pass'));
// Nguyen Dung
define('VERZ_COOKIE_MEMBER', md5('verz_cookie_member'));
define('VERZLOGIN_MEMBER', md5('verz_login_member'));
define('VERZLPASS_MEMBER', md5('verz_pass_member'));