<?php
/** Mar 01, 2017 Class for write html export pdf */
class ToPdf
{
    
    /**
     * @Author: ANH DUNG Mar 19, 2017
     * @Todo: export pdf
     * @Param: $aCompany is array attribute model User
     * @Param: $gPriceInMonth is array price of customer in current month
     * @Param: $aErrors array error return
     */
    public function customerQuotes($mUser, $aCompany, $gPriceInMonth, &$aErrors) {
        $month = date('m')*1; $year = date('Y');
        if(!empty($mUser->date_month)){
            $month = $mUser->date_month;
        }else{
            $mUser->date_month = $month;
        }
        if(!empty($mUser->date_year)){
            $year = $mUser->date_year;
        }else{
            $mUser->date_year = $year;
        }
        if($mUser->date_month < 10){
            $mUser->date_month = '0'.($mUser->date_month*1);
        }
        
        $dateApply = "$year-$mUser->date_month-01";
        $daysInMonth = MyFormat::dateConverYmdToDmy($dateApply, 't');
        
        $aCompanyAllow = [GasConst::COMPANY_DKMN, GasConst::COMPANY_HUONGMINH];
        $aData = [];
        if(!in_array($mUser->storehouse_id, $aCompanyAllow)){
            $mUser->storehouse_id = GasConst::COMPANY_DKMN;
        }
        $mCompany = isset($aCompany[$mUser->storehouse_id]) ? $aCompany[$mUser->storehouse_id] : $aCompany[GasConst::COMPANY_DKMN];
        $customerName = $mUser->getFullNameTruSoChinh();
        $mUser->loadExtPhone2();

        $companyLogo = 'http://spj.daukhimiennam.com/themes/gas/image_pdf/quotes_customer/dkmn_logo.png';
        $companySign = 'http://spj.daukhimiennam.com/themes/gas/image_pdf/quotes_customer/dkmn_sign.png';
        $companyBoss = 'Vũ Thái Long';
        $companyName = 'CÔNG TY CỔ PHẦN DẦU KHÍ MIỀN NAM';
        $companyadd  = 'Số 86 Nguyễn Cửu Vân, P.17, Q.BT, Tp.HCM';
        $companyDirector  = 'Tổng Giám Đốc';
        $cssFile    = 'view_pdf';
        $lineColor  = '#ff3300';
        if($mCompany['id'] == GasConst::COMPANY_HUONGMINH){
            $companyLogo = 'http://spj.daukhimiennam.com/themes/gas/image_pdf/quotes_customer/hm_logo.png';
            $companySign = 'http://spj.daukhimiennam.com/themes/gas/image_pdf/quotes_customer/hm_sign.png';
            $companyBoss = 'Bùi Đức Hoàn';
            $companyName = 'CÔNG TY TNHH HƯỚNG MINH';
            $companyadd  = '3, Kp.2, ĐS 3, P.Bình An, Q.2, Tp.HCM';
            $companyDirector  = 'Giám Đốc';
            $cssFile    = '';
            $lineColor  = '#212121';
        }

//            $aData['COMPANY_NAME']  = strtoupper($mCompany['first_name']);// không dùng strtoupper được, méo chữ        
        $aData['COMPANY_ID']    = $mCompany['id'];
        $aData['COMPANY_NAME']  = $companyName;
        $aData['COMPANY_NAME_NORMAL']  = $mCompany['first_name'];
        $aData['COMPANY_ADD']   = $companyadd;
        $aData['COMPANY_PHONE'] = $mCompany['phone'];
        $aData['COMPANY_FAX']   = $mCompany['code_account'];// set tạm code_account là fax cho loại user là Công ty
        $aData['COMPANY_LOGO']  = $companyLogo;
        $aData['COMPANY_SIGN']  = $companySign;
        $aData['COMPANY_BOSS']  = $companyBoss;
        $aData['COMPANY_DIRECTOR']  = $companyDirector;
        $phoneCall = GasConst::PHONE_BOMOI;
        if($mUser->is_maintain == STORE_CARD_KH_MOI){
            $phoneCall = GasConst::PHONE_BOMOI;
        }
        $aData['PHONE_CALL']        = $phoneCall;
        $aData['PHONE_UPHOLD']      = GasConst::PHONE_BOMOI;
        $aData['PHONE_DIRECTOR_BUSSINESS']  = '0983 075 779';

        $aData['PDF_NAME']      = $mUser->getQuotesName();
        $aData['CUSTOMER_NAME'] = $customerName;
        $aData['CUSTOMER_TEL']  = $mUser->phone_first;
        $aData['CUSTOMER_FAX']  = !empty($mUser->getUserRefField('contact_boss_fax')) ?  'Fax - '.$mUser->getUserRefField('contact_boss_fax') : '';
        $aData['CUSTOMER_CONTACT'] = $mUser->getUserRefField('contact_person_name');
        $aData['QUOTES_BEGIN']  = "01/$mUser->date_month/$year";
        $aData['QUOTES_END']    = "$daysInMonth/$mUser->date_month/$year";
        
        $aData['CSS_FILE']      = $cssFile;
        $aData['LINE_COLOR']    = $lineColor;

        $priceBB    = 0;
        $priceB12   = 0;
        if(isset($gPriceInMonth[$mUser->id])){
            $priceBB    = $gPriceInMonth[$mUser->id][UsersPrice::PRICE_B_50];
            $priceB12   = $gPriceInMonth[$mUser->id][UsersPrice::PRICE_B_12];
        }
        if(empty($priceBB) && empty($priceB12)){
            $aErrors[] = $mUser->id;
            return false;
        }

        $aData['PRICE_BB']  = $priceBB > 0 ? ActiveRecord::formatCurrency($priceBB).' ₫ /kg' : '';

        if($priceB12 > UsersPrice::PRICE_MAX_BINH_BO){
            $aData['PRICE_B12'] = $priceB12 > 0 ? ActiveRecord::formatCurrency($priceB12).' ₫ /bình' : '';
        }else{
            $aData['PRICE_B12'] = $priceB12 > 0 ? ActiveRecord::formatCurrency($priceB12).' ₫ /kg' : '';
        }

        $mUser->setQuotesPdfName();
        $mUser->toPdf($aData);
    }
    
   /**
     * @Author: ANH DUNG Feb 28, 2017
     * @Todo: build html pdf content quotes
     */
    public function getPdfContentQuotes($aData, $mUser) {
        if($mUser->titleNotify == UsersPrice::TYPE_QUOTE_PRICE){
            return $this->getPdfContentQuotesPrice($aData, $mUser);
        }
        $trSpaceDkmn = '<tr>
                            <td colspan="2"><img src="http://spj.daukhimiennam.com/themes/gas/image_pdf/quotes_customer/space.png" alt=""/></td>
                        </tr>
                        <tr>
                            <td colspan="2"><img src="http://spj.daukhimiennam.com/themes/gas/image_pdf/quotes_customer/space.png" alt=""/></td>
                        </tr>';
        if($aData['COMPANY_ID'] == GasConst::COMPANY_HUONGMINH){
            $trSpaceDkmn = '';
        }

        $monthYear = $mUser->date_month.'/'.$mUser->date_year;
        
        return '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>'.$aData['PDF_NAME'].'</title>
<style type="text/css" media="print">
</style>
</head>
<body>

<table width="800px" style="margin-left:50px; border: none;font-family: Arial;border: none; font-size:14px;">
    '.$trSpaceDkmn.'
   <tr>
      <td width= "420px">
        <img src="'.$aData['COMPANY_LOGO'].'" alt="">
      </td>
     <td width= "370px;">
        <h3 style="margin: 0;font-size: 18px;">'.$aData['COMPANY_NAME'].'</h3>
        <p style="margin: 0; line-height:25px; font-size:16px">ĐC  : '.$aData['COMPANY_ADD'].'<br/>ĐT  : '.$aData['COMPANY_PHONE'].' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fax: '.$aData['COMPANY_FAX'].'</p>
        
      </td>
   </tr>
   <tr>
    	<td colspan="2"  align="center" width="100%" height="1px" bgcolor="'.$aData['LINE_COLOR'].'"></td>
    </tr>
    <tr>
    <td colspan="2">
    	<table width="800px">
            <tr>
                <td  width= "350px;" style="font-family: Arial, sans-serif;font-size: 14px; font-style:italic;">Số: 01/'.$mUser->date_year.'/DKMN - BG</td>
                <td  width= "450px;" style="font-family: Arial, sans-serif;font-size: 14px; font-style:italic;" align="right">TP. Hồ Chí Minh, ngày 01 tháng '.$mUser->date_month.' năm '.$mUser->date_year.'</td>
            </tr>
        </table>
    </td>
    	
    </tr>
    
    <tr>
    	<td colspan="2">
        	<table width="800px">
            	<tr>
                	<td width="200px" style="font-size: 18px;font-weight: bold;">
                    Kính gửi : 
                    </td>
                    <td style=" font-size: 20px;font-weight: bold;font-style: italic;">
                   '.$aData['CUSTOMER_NAME'].'
                    </td>
                </tr>
                
                <tr style="">
                    <td width="200px" style="font-size: 18px;font-weight: bold;">
                        Người Nhận:
                    </td>
                    <td style=" font-size: 18px;font-weight: bold;font-style: italic;">
                        '.$aData['CUSTOMER_CONTACT'].'
                    </td>
                </tr>
                
            </table>
        </td>
    </tr>

   <tr>
    	<td colspan="2"><img src="http://spj.daukhimiennam.com/themes/gas/image_pdf/quotes_customer/space.png" alt=""/></td>
    </tr>
   <tr>
    	<td colspan="2" align="center"><p style="text-align: center;font-family: Arial, sans-serif; font-weight: bold;font-size: 27px;margin: 0;">BẢNG BÁO GIÁ GAS THÁNG '.$monthYear.'</p></td>
    </tr>
    <tr>
    	<td colspan="2"><img src="http://spj.daukhimiennam.com/themes/gas/image_pdf/quotes_customer/space.png" alt=""/></td>
    </tr>
    <tr>
    	<td colspan="2" style="font-family: Arial, sans-serif;">
            <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;"><strong>'.$aData['COMPANY_NAME_NORMAL'].' </strong> xin gửi lời cảm ơn trân trọng vì sự hợp tác của Quý Công ty. <br/>Căn cứ vào nhu cầu hoạt động của hai bên, Công ty chúng tôi thông báo giá Gas tháng '.$monthYear.' như sau: </p>
            <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;">1. Giá Bán : <br/>
                <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style=" display: inline-block;width: 200px;">a. Bình 45kg & 50kg: </span>&nbsp;&nbsp;&nbsp;<strong> '.$aData['PRICE_BB'].'</strong></p>
                <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="  display: inline-block;width: 200px;">b. Bình 12kg: </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong> '.$aData['PRICE_B12'].' </strong></p>
            </p>
            <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;">2. Đơn giá: Đã bao gồm 10% thuế VAT.</p>
            <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;">3. Lắp đặt miễn phí hệ thống Gas và Bảo trì định kỳ miễn phí trong suốt quá trình sử dụng Gas của Công ty chúng tôi.</p>
            <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;">4. Thương hiệu Gas: <strong>S.Gas & SJ Petrol</strong></p>
            <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;">5. Thành phần và Chất lượng Gas: C3/C4: 50/50, Gas áp cao.</p>
            <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;">6. Bảo hiểm bình Gas và hệ thống Gas: 10,000,000,000 đồng (Mười tỉ đồng)</p>
            <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 55px">7. Phương thức thanh toán: Theo hợp đồng ký kết giữa 2 bên.</p>
            <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 55px">8. Bản chào này có giá trị từ ngày <strong>'.$aData['QUOTES_BEGIN'].'</strong> đến hết ngày <strong>'.$aData['QUOTES_END'].'</strong>.</p>
                <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 55px">9. Thông tin liên lạc: </p>
                <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 55px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Giám Đốc KD:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$aData['PHONE_DIRECTOR_BUSSINESS'].'</p>
                <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 55px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tổng đài gọi Gas:&nbsp;&nbsp;'.$aData['PHONE_CALL'].'</p>
                <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 55px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tổng đài bảo trì:&nbsp;&nbsp;&nbsp;'.$aData['PHONE_UPHOLD'].'</p>
                <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 55px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email liên hệ:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;saleadmin@spj.vn</p>
       </td>
    </tr>
   
    <tr>
    	<td colspan="2"><img src="http://spj.daukhimiennam.com/themes/gas/image_pdf/quotes_customer/space.png" alt=""/></td>
    </tr>
    <tr>
    	<td colspan="2"><img src="http://spj.daukhimiennam.com/themes/gas/image_pdf/quotes_customer/space.png" alt=""/></td>
    </tr>
 
   <tr>
   	<td colspan="2" style="font-family: Arial, sans-serif;" align="right">
    	<table width="800px">
        	<tr>
            	<td width="400px" valign="top" align="left">
                    <p style="font-size:20px; margin-top:0"><strong></strong></p>
                </td>
                <td width="400px" style="text-align: center; font-family: Arial, sans-serif;font-weight: bold;line-height:160%"><span style="font-size: 20px;">'.$aData['COMPANY_NAME'].'</span><br/><span style="font-size: 20px;">'.$aData['COMPANY_DIRECTOR'].'</span><br/>
        <img src="'.$aData['COMPANY_SIGN'].'" alt=""/><br/>
        <span style="font-size: 16px;padding-top: 6px;display: inline-block; line-height:160%"><strong>'.$aData['COMPANY_BOSS'].' <br/></strong></span></td>
            </tr>
        </table>
    </td>
   </tr>
 </table>

</body>
</html>';
    }
   
    /**
     * @Author: NamLA Oct3,2019
     * @Todo: export pdf
     * @Param: 
     */
    public function quotesPrice($info,$mUser) {
        $date_month = date('m', strtotime($info['ngay_bao']));
        $date_year = date('Y', strtotime($info['ngay_bao']));
        $mUser->date_month = $date_month;
        $mUser->date_year  = $date_year;
        $mUser->id         = $info['kinh_gui'];
        $companyLogo = 'http://noibo.daukhimiennam.com/themes/gas/image_pdf/quotes_customer/dkmn_logo.png';
        $companySign = 'http://noibo.daukhimiennam.com/themes/gas/image_pdf/quotes_customer/dkmn_sign.png';
        $companyBoss = 'Vũ Thái Long';
        $companyName = 'CÔNG TY CỔ PHẦN DẦU KHÍ MIỀN NAM';
        $companyadd  = 'Số 86 Nguyễn Cửu Vân, P.17, Q.BT, Tp.HCM';
        $companyDirector  = 'Tổng Giám Đốc';
        $cssFile    = 'view_pdf';
        $lineColor  = '#ff3300';
        $aData['COMPANY_NAME']  = $companyName;
        $aData['COMPANY_NAME_NORMAL']  = 'Công Ty Cổ Phần Dầu Khí Miền Nam';
        $aData['COMPANY_ADD']   = $companyadd;
        $aData['COMPANY_PHONE'] = '19001521';
        $aData['COMPANY_FAX']   = '';// set tạm code_account là fax cho loại user là Công ty
        $aData['COMPANY_LOGO']  = $companyLogo;
        $aData['COMPANY_SIGN']  = $companySign;
        $aData['COMPANY_BOSS']  = $companyBoss;
        $aData['COMPANY_DIRECTOR']  = $companyDirector;
        $phoneCall = GasConst::PHONE_BOMOI;
        $aData['PHONE_CALL']        = $phoneCall;
        $aData['PHONE_UPHOLD']      = GasConst::PHONE_BOMOI;
        $aData['PHONE_DIRECTOR_BUSSINESS']  = '0983 075 779';
        $aData['PDF_NAME']      = $mUser->getQuotesName();
        $aData['CUSTOMER_NAME'] = $info['kinh_gui'];
        $aData['RECEIVER_NAME'] = $info['nguoi_nhan'];
        $aData['CUSTOMER_TEL']  = '';
        $aData['CUSTOMER_FAX']  ='';
        $aData['CUSTOMER_CONTACT'] = '';
        $aData['QUOTES_BEGIN']  = date('01/m/Y',strtotime($info['ngay_bao']));
        $daysInMonth = MyFormat::dateConverYmdToDmy($info['ngay_bao'], 't');
        $aData['QUOTES_END']  = date("$daysInMonth/m/Y",strtotime($info['ngay_bao']));
        $aData['CSS_FILE']      = $cssFile;
        $aData['LINE_COLOR']    = $lineColor;
        $aData['PRICE_BB']  = $info['gia_45'] > 0 ? ActiveRecord::formatCurrency($info['gia_45']).' ₫ /kg' : '';
        if($info['gia_12'] > UsersPrice::PRICE_MAX_BINH_BO){
            $aData['PRICE_B12'] = $info['gia_12'] > 0 ? ActiveRecord::formatCurrency($info['gia_12']).' ₫ /bình' : '';
        }else{
            $aData['PRICE_B12'] = $info['gia_12'] > 0 ? ActiveRecord::formatCurrency($info['gia_12']).' ₫ /kg' : '';
        }
        $mUser->setQuotesPdfName();
        $mUser->pdfOutput = 'I';
        $mUser->toPdf($aData);
        
    }
    
   /**
     * @Author: NamLA Oct3, 2019
     * @Todo: build html pdf content quotes
     */
    public function getPdfContentQuotesPrice($aData, $mUser) {
        $day = date('d', strtotime($_POST['ngay_bao']));
        $trSpaceDkmn = '<tr>
                            <td colspan="2"><img src="http://noibo.daukhimiennam.com/themes/gas/image_pdf/quotes_customer/space.png" alt=""/></td>
                        </tr>
                        <tr>
                            <td colspan="2"><img src="http://noibo.daukhimiennam.com/themes/gas/image_pdf/quotes_customer/space.png" alt=""/></td>
                        </tr>';
        $monthYear = $mUser->date_month.'/'.$mUser->date_year;
        return '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>'.$aData['PDF_NAME'].'</title>
            <style type="text/css" media="print">
            </style>
            </head>
            <body>

            <table width="800px" style="margin-left:50px; border: none;font-family: Arial;border: none; font-size:14px;">
                '.$trSpaceDkmn.'
               <tr>
                  <td width= "420px">
                    <img src="'.$aData['COMPANY_LOGO'].'" alt="">
                  </td>
                 <td width= "370px;">
                    <h3 style="margin: 0;font-size: 18px;">'.$aData['COMPANY_NAME'].'</h3>
                    <p style="margin: 0; line-height:25px; font-size:16px">ĐC  : '.$aData['COMPANY_ADD'].'<br/>ĐT  : '.$aData['COMPANY_PHONE'].' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fax: '.$aData['COMPANY_FAX'].'</p>

                  </td>
               </tr>
               <tr>
                    <td colspan="2"  align="center" width="100%" height="1px" bgcolor="'.$aData['LINE_COLOR'].'"></td>
                </tr>
                <tr>
                <td colspan="2">
                    <table width="800px">
                        <tr>
                            <td  width= "350px;" style="font-family: Arial, sans-serif;font-size: 14px; font-style:italic;">Số: 01/'.$mUser->date_year.'/DKMN - BG</td>
                            <td  width= "450px;" style="font-family: Arial, sans-serif;font-size: 14px; font-style:italic;" align="right">TP. Hồ Chí Minh, ngày '.$day.' tháng '.$mUser->date_month.' năm '.$mUser->date_year.'</td>
                        </tr>
                    </table>
                </td>

                </tr>

                <tr>
                    <td colspan="2">
                            <table width="800px">
                            <tr>
                                    <td width="200px" style="font-size: 18px;font-weight: bold;">
                                Kính gửi : 
                                </td>
                                <td style=" font-size: 20px;font-weight: bold;font-style: italic;">
                               '.$aData['CUSTOMER_NAME'].'
                                </td>
                            </tr>

                            <tr>
                                <td width="200px" style="font-size: 18px;font-weight: bold;">
                                    Người Nhận:
                                </td>
                                <td style=" font-size: 20px;font-weight: bold;font-style: italic;">
                               '.$aData['RECEIVER_NAME'].'
                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>

               <tr>
                    <td colspan="2"><img src="http://spj.daukhimiennam.com/themes/gas/image_pdf/quotes_customer/space.png" alt=""/></td>
                </tr>
               <tr>
                    <td colspan="2" align="center"><p style="text-align: center;font-family: Arial, sans-serif; font-weight: bold;font-size: 27px;margin: 0;">BẢNG BÁO GIÁ GAS THÁNG '.$monthYear.'</p></td>
                </tr>
                <tr>
                    <td colspan="2"><img src="http://spj.daukhimiennam.com/themes/gas/image_pdf/quotes_customer/space.png" alt=""/></td>
                </tr>
                <tr>
                    <td colspan="2" style="font-family: Arial, sans-serif;">
                        <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;"><strong>'.$aData['COMPANY_NAME_NORMAL'].' </strong> xin gửi lời cảm ơn trân trọng vì sự hợp tác của Quý Công ty. <br/>Căn cứ vào nhu cầu hoạt động của hai bên, Công ty chúng tôi thông báo giá Gas tháng '.$monthYear.' như sau: </p>
                        <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;">1. Giá Bán : <br/>
                            <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style=" display: inline-block;width: 200px;">a. Bình 45kg & 50kg: </span>&nbsp;&nbsp;&nbsp;<strong> '.$aData['PRICE_BB'].'</strong></p>
                            <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="  display: inline-block;width: 200px;">b. Bình 12kg: </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong> '.$aData['PRICE_B12'].' </strong></p>
                        </p>
                        <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;">2. Đơn giá: Đã bao gồm 10% thuế VAT.</p>
                        <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;">3. Lắp đặt miễn phí hệ thống Gas và Bảo trì định kỳ miễn phí trong suốt quá trình sử dụng Gas của Công ty chúng tôi.</p>
                        <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;">4. Thương hiệu Gas: <strong>S.Gas & SJ Petrol</strong></p>
                        <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;">5. Thành phần và Chất lượng Gas: C3/C4: 50/50, Gas áp cao.</p>
                        <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;">6. Bảo hiểm bình Gas và hệ thống Gas: 10,000,000,000 đồng (Mười tỉ đồng)</p>
                        <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 55px">7. Phương thức thanh toán: Theo hợp đồng ký kết giữa 2 bên.</p>
                        <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 55px">8. Bản chào này có giá trị từ ngày <strong>'.$aData['QUOTES_BEGIN'].'</strong> đến hết ngày <strong>'.$aData['QUOTES_END'].'</strong>.</p>
                            <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 55px">9. Thông tin liên lạc: </p>
                            <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 55px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Giám Đốc KD:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$aData['PHONE_DIRECTOR_BUSSINESS'].'</p>
                            <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 55px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tổng đài gọi Gas:&nbsp;&nbsp;'.$aData['PHONE_CALL'].'</p>
                            <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 55px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tổng đài bảo trì:&nbsp;&nbsp;&nbsp;'.$aData['PHONE_UPHOLD'].'</p>
                            <p style="margin-top: 0px; font-size: 16px; color: #333333; margin-bottom: 0px;line-height: 55px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email liên hệ:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;saleadmin@spj.vn</p>
                   </td>
                </tr>

                <tr>
                    <td colspan="2"><img src="http://noibo.daukhimiennam.com/themes/gas/image_pdf/quotes_customer/space.png" alt=""/></td>
                </tr>
                <tr>
                    <td colspan="2"><img src="http://noibo.daukhimiennam.com/themes/gas/image_pdf/quotes_customer/space.png" alt=""/></td>
                </tr>

               <tr>
                    <td colspan="2" style="font-family: Arial, sans-serif;" align="right">
                    <table width="800px">
                            <tr>
                            <td width="400px" valign="top" align="left">
                                <p style="font-size:20px; margin-top:0"><strong></strong></p>
                            </td>
                            <td width="400px" style="text-align: center; font-family: Arial, sans-serif;font-weight: bold;line-height:160%"><span style="font-size: 20px;">'.$aData['COMPANY_NAME'].'</span><br/><span style="font-size: 20px;">'.$aData['COMPANY_DIRECTOR'].'</span><br/>
                    <img src="'.$aData['COMPANY_SIGN'].'" alt=""/><br/>
                    <span style="font-size: 16px;padding-top: 6px;display: inline-block; line-height:160%"><strong>'.$aData['COMPANY_BOSS'].' <br/></strong></span></td>
                        </tr>
                    </table>
                </td>
               </tr>
             </table>

            </body>
            </html>';
    }

}
