<?php
class UpdateSql
{
    /**
     * @Author: ANH DUNG 11-19-2013
     * @Todo: cập nhật tỉnh, quận huyện, phường xã cho bảo trì vs bán hàng bảo trì
     * UpdateSql::updateAddressMaintain();
     * UpdateSql::updateAddressMaintain('GasMaintainSell');
     */
    public static function updateAddressMaintain($name_model='GasMaintain'){        
        $from = time();
        $model_ = call_user_func(array($name_model, 'model'));
        $mRes = $model_->findAll();            
        foreach($mRes as $item){
            $item->update(array('province_id','district_id','ward_id'));
        }
        $to = time();
        $second = $to-$from;
        echo count($mRes).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;  	
    }
	  
    /**
     * @Author: ANH DUNG 11-21-2013
     * @Todo: cập nhật thêm cột maintain_employee_id bên bán hàng bảo trì
     * UpdateSql::updateAddressMaintain();
     * UpdateSql::updateAddressMaintain('GasMaintainSell');
     */         
    public static function updateMaintainEmployee($name_model='GasMaintainSell'){
//        set_time_limit(7200);
        $from = time();
        $model_ = call_user_func(array($name_model, 'model'));
        $criteria = new CDbCriteria();
//        $criteria->addCondition('maintain_id<>"" AND maintain_id<>0'); 
        $tableName = $model_->tableName();        
        $mRes = $model_->findAll($criteria);
        foreach($mRes as $item){
            $item->update(array('maintain_employee_id','monitoring_id',
                'accounting_employee_id','is_same_seri_maintain','type',
                'maintain_id',
                ));
        }
        $to = time();
        $second = $to-$from;
        echo count($mRes).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;  	

    }

    /**
     * @Author: ANH DUNG 11-25-2013
     * @Todo: cập nhật một số cột như: address, address_vi, name_agent, 
     * @từ khi mà thay đổi quản lý địa chỉ khách hàng theo tỉnh, quận huyện, phường, đường
     * UpdateSql::updateColumnAddressUser();
     * Run 1: 10,000 74 Second <=> 1.23 Minutes
     * Run 2: 20,000 done in: 197 Second <=> 3.28 Minutes
     * Execution Time: 232.15 sec Memory Usage: 460.79 MB 20000 done in: 232 Second <=> 3.86 Minutes
     * Execution Time: 216.867 sec Memory Usage: 458.34 MB 20000 done in: 217 Second <=> 3.61 Minutes
     * Execution Time: 204.62 sec Memory Usage: 435.48 MB 20000 done in: 205 Second <=> 3.41 Minutes
     * Execution Time: 198.417 sec Memory Usage: 425.12 MB 20000 done in: 198 Second <=> 3.3 Minutes
     * Execution Time: 366.65 sec Memory Usage: 676.79 MB 30000 done in: 367 Second <=> 6.11 Minutes
     * Execution Time: 383.645 sec Memory Usage: 696.95 MB 30000 done in: 383 Second <=> 6.38 Minutes
     * Execution Time: 322.547 sec Memory Usage: 480.69 MB 20000 done in: 322 Second <=> 5.36 Minutes
     * Execution Time: 309.754 sec Memory Usage: 476.75 MB 20000 done in: 310 Second <=> 5.16 Minutes
     * Execution Time: 338.53 sec Memory Usage: 476.49 MB 20000 done in: 339 Second <=> 5.65 Minutes
     * Execution Time: 312.058 sec Memory Usage: 476.88 MB 20000 done in: 312 Second <=> 5.2 Minutes
     * Execution Time: 276.523 sec Memory Usage: 476.17 MB 20000 done in: 276 Second <=> 4.6 Minutes
     * Execution Time: 270.626 sec Memory Usage: 476.11 MB 20000 done in: 270 Second <=> 4.5 Minutes
     * Execution Time: 270.717 sec Memory Usage: 470.42 MB 20000 done in: 271 Second <=> 4.51 Minutes
     * Execution Time: 95.702 sec Memory Usage: 125.9 MB 4929 done in: 96 Second <=> 1.6 Minutes
     * Execution Time: 20.174 sec Memory Usage: 6.98 MB 0 done in: 20 Second <=> 0.33 Minutes
     */       
    public static function updateColumnAddressUser(){
        $from = time(); $count = 0;
        ini_set('memory_limit','2000M');
        for($i = 0; $i < 20; $i++):
            $criteria = new CDbCriteria();
            $criteria->addCondition('t.role_id=' . ROLE_CUSTOMER . " AND (t.last_name IS NULL OR t.last_name='')");
            $criteria->limit = 1000;
            $mRes = Users::model()->findAll($criteria);
            $aUpdate = array('address_vi', 'last_name');
            foreach($mRes as $item)
            {
                $item->last_name = 1;
                $item->update($aUpdate);
                $count++;
            }
            sleep(1);
            $mRes = null;
        endfor;
        
        $to = time();
        $second = $to-$from;
        UpdateSql::logMemory();
        echo $count.' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;  	
    }
    
    public static function logMemory() {
        $log    = new CLogger();
        $time   = round($log->getExecutionTime(),3);
        $mem    = round(memory_get_usage(false)/1048576,2);
        echo "Execution Time:  $time sec Memory Usage: $mem MB  ";
    }

    public static function updateAllNameVi($className){
        $from = time();
        $model_ = call_user_func(array($className, 'model'));        
        $mRes = $model_->findAll();
        foreach($mRes as $item)
        {
            $item->update();
        }		
        $to = time();
        $second = $to-$from;
        echo count($mRes).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;  	
    }    
    
    /**
     * @Author: ANH DUNG 12-21-2013
     * @Todo: sửa lại dữ liệu, thêm cột agent_id (  đã thay = area_code_id ) 
     * vào table user, không dùng session để lưu KH của đại lý nữa, viết sql cập nhật
     * step khi đổi, 
    1/ change lúc save cho các model, khi tạo bảo trì or pttt
    2/ change autocomplete search khi tạo bảo trì or pttt, bán hàng bảo trì
    3/ change criteria search bảo trì or pttt
    4/ run sql cập nhật hết các customer Bảo trì vs pttt
     * UpdateSql::updateColumnAddressUser();
     */        
    public static function changeSaveCustomerOfAgent(){
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.agent_id IS NOT NULL AND t.customer_id IS NOT NULL  ");
//        $criteria->addCondition("t.employee_maintain_id IS NOT NULL AND t.maintain_agent_id IS NOT NULL  ");
        $models = GasAgentCustomer::model()->findAll($criteria);
        $sql='';
        $tableName = Users::model()->tableName();
        foreach($models as $item){
            $sql .= "UPDATE $tableName SET `area_code_id`=$item->agent_id WHERE `id`=$item->customer_id ;";
            //UPDATE mytable SET (id, column_a, column_b) FROM VALUES ( (1, 12, 6), (2, 1, 45), (3, 56, 3), … );
        }
        Yii::app()->db->createCommand($sql)->execute();
        
        $to = time();
        $second = $to-$from;
        echo count($models).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;          
    }
    
    // 12-21-2013  dùng sau khi chạy hàm trên changeSaveCustomerOfAgent
    public static function deleteCustomerOfAgent(){
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->addCondition("agent_id IS NOT NULL AND customer_id IS NOT NULL  ");
        $models = GasAgentCustomer::model()->deleteAll($criteria);
        $to = time();
        $second = $to-$from;
        echo count($models).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;          
    }
    
    /**
     * @Author: ANH DUNG 02-11-2014
     * @Todo: cập nhật cột type_customer // loại KH 1: Bình Bò, 2: Mối, dùng cho thống kê doanh thu
     * @dùng cho xuất báo cáo doanh thu, ta sẽ không cộng doanh thu của KH bình bò
     * UpdateSql::updateColumnAddressUser();
     */       
    public static function updateCashBookTypeCustomer(){        
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.customer_id<>0');
        $mRes = GasCashBookDetail::model()->findAll($criteria);
//        echo count($mRes);die;
        $aUpdate = array('type_customer');
        foreach($mRes as $item)
        {
            $item->update($aUpdate);
        }		
        $to = time();
        $second = $to-$from;
        echo count($mRes).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;  	
    }    
    
    /**
     * @Author: ANH DUNG Apr 01, 2014
     * @Todo: update sale_id cho table thẻ kho
     * cập nhật sale id cho khách hàng thẻ kho trong bảng GasStoreCardDetail + GasRemain
     */
    public static function updateSaleIdForStorecardDetail(){
        set_time_limit(72000); ini_set('memory_limit','2500M');
        $from = time();
        $criteria = new CDbCriteria();
        $mAppCache = new AppCache();
       
        $criteria->addCondition(" t.role_id IN ('".ROLE_CUSTOMER."')");
        $aTypeNot = array(CUSTOMER_TYPE_MAINTAIN, CUSTOMER_TYPE_MARKET_DEVELOPMENT);
        $criteria->addNotInCondition( 't.type', $aTypeNot);
        $criteria->addInCondition( 't.is_maintain', CmsFormatter::$ARR_TYPE_CUSTOMER_STORECARD); // Aug 19, 2014 chỉ update KH bò mối thôi, vì cái 
        $criteria->compare('t.channel_id', Users::CON_LAY_HANG);
        // type KH của store card hiện tại nó gồm nhiều loại, ko thể update bừa dc
//        $criteria->addCondition( 't.sale_id<>"" AND t.sale_id IS NOT NULL');// có KH không có sale
//        $criteria->addCondition( 't.sale_id=281997');// 281997 = y tu
        $mRes = Users::model()->findAll($criteria);
//        echo count($mRes);die; 
        $aModelSale = $mAppCache->getUserByRole(ROLE_SALE);//Apr 17, 2017 lấy thêm model sale để lấy được $sale_type cập nhật sang table Storecard Detail 
        $dateRun    = '2019-07-01';
        $dateRun    = strtotime($dateRun);// Jul2519 DungNT change
        foreach($mRes as $item)
        { 
            $sale_type = 0;
            if(isset($aModelSale[$item->sale_id])){
                $sale_type = $aModelSale[$item->sale_id]['gender'];
            }
            
            // 1. update GasStoreCardDetail CPU run about 30% too low, it so good
            $aUpdate = ['sale_id'=>$item->sale_id, 'sale_type'=>$sale_type, 'type_customer'=>$item->is_maintain];
            GasStoreCardDetail::model()->updateAll($aUpdate,
//                    "`customer_id`=$item->id");
                    "`customer_id`=$item->id AND date_delivery_bigint>=$dateRun ");
            
            // mới thêm phần cập nhật từ ngày nào
//                     run 1: Apr 01, 2014 1173 done in: 107 Second <=> 1.7833333333333 Minutes
//                     run 1: Apr 16, 2014  1228 done in: 133 Second <=> 2.2166666666667 Minutes 
//                     run 1: Apr 25, 2014 sau khi index chạy nhanh hơn 1264 done in: 4 Second <=> 0.066666666666667 Minutes
//                     run 1: Apr 25, 2014 after index  1820 done in: 6 Second <=> 0.1 Minutes
//                     run 2: May 07, 2014 after index  1893 done in: 12 Second <=> 0.2 Minutes
//                     run 3: May 10, 2014 after index  1893 done in: 8 Second <=> 0.13333333333333 Minutes
            // 2. update gas_gas_remain
            GasRemain::model()->updateAll(
                    array('sale_id'=>$item->sale_id, 
                        'type_customer'=>$item->is_maintain                        
                    ),
//                    "`customer_id`=$item->id  ");
                    "`customer_id`=$item->id  AND date_input_bigint>=$dateRun "); 
                        
            // 3/ dùng để cập nhật loại KH trong bảng GasStoreCard. Sẽ ít dùng
//            GasStoreCard::model()->updateAll(array('type_user'=>$item->is_maintain),
//                    "`customer_id`=$item->id");
            // May 21, 2014 - 1985 done in:+ 5 Second <=> 0.083333333333333 Minutes
        }
        $to = time();
        $second = $to-$from;
        echo count($mRes).' done in:+ '.($second).'  Second  <=> '.($second/60).' Minutes';die;
    }   
    
    
    // dung để copy danh sách kh từ 1 đại lý có sẵn sang 1 đại lý mới tạo
    public static function copyCustomerFromAgentToNewAgent(){
        $from = time();
        $agent_from = 106; // Đại lý Bình Thạnh
        $agent_to = 118239; // Kho Bình Thạnh
        $aRowInsert=array();
        $allCustomer = GasAgentCustomer::getCustomerOfAgentAllModel($agent_from);

        if(count($allCustomer)){
            foreach($allCustomer as $model){
                $aRowInsert[]="('$agent_to',
                        '$model->customer_id'
                        )";
            }
            $tableName = GasAgentCustomer::model()->tableName();
            $sql = "insert into $tableName (agent_id,
                        customer_id
                        ) values ".implode(',', $aRowInsert);
            if(count($aRowInsert)>0)
                Yii::app()->db->createCommand($sql)->execute();	
        }
        
        $to = time();
        $second = $to-$from;
        echo count($allCustomer).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;  	
        
        
    }
    
    // cập nhật cột agent id với record do ROLE_DIEU_PHOI tạo, để search cho đúng
    public static function UpdateAgentForOrder(){
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->compare("t.type", GasOrders::TYPE_DIEU_PHOI);
        $models = GasOrders::model()->findAll($criteria);
        if(count($models)){
            foreach($models as $model){
               $model->agent_id = $model->user_id_create;
               $model->update(array('agent_id'));
            }
        }
        
        $criteria = new CDbCriteria();
        $criteria->compare("t.type", GasOrders::TYPE_DIEU_PHOI);
        $models = GasOrdersDetail::model()->findAll($criteria);
        
        if(count($models)){
            foreach($models as $model){
               $model->agent_id = $model->user_id_create;
               $model->update(array('agent_id'));
            }
        }
        $to = time();
        $second = $to-$from;
        echo count($models).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;  	
    }
    
    /**
     * @Author: ANH DUNG May 12, 2014
     * @Todo: đổi của KH sale_id A sang Sale id B
     * cập nhật change sale id cho khách hàng trong bảng 
     * User - GasStoreCardDetail + GasRemain + gasOrderDetail + gasCashbookDetail
     */
    public static function updateChangeSaleIdOfCustomer(){
        set_time_limit(72000);
        $from = time();
        $criteria = new CDbCriteria();
        $sale_old = 49170; // Phạm Văn Đức
        $sale_new = 49184; // Khách Hàng Công Ty - Bò

        $criteria->compare( 't.sale_id', $sale_old);
        $mRes = Users::model()->findAll($criteria);
        echo count($mRes);die;
        
        // 1. update table User
            Users::model()->updateAll(array('sale_id'=>$sale_new),
                    "`sale_id`=$sale_old"); 
        // 2. update GasStoreCardDetail CPU run about 30% too low, it so good
            GasStoreCardDetail::model()->updateAll(array('sale_id'=>$sale_new),
                    "`sale_id`=$sale_old"); 
        // 3. update gas_gas_remain
            GasRemain::model()->updateAll(array('sale_id'=>$sale_new),
                    "`sale_id`=$sale_old");
        // 4. update GasOrdersDetail
            GasOrdersDetail::model()->updateAll(array('sale_id'=>$sale_new),
                    "`sale_id`=$sale_old");
        // 5. update GasOrdersDetail
            GasCashBookDetail::model()->updateAll(array('sale_id'=>$sale_new),
                    "`sale_id`=$sale_old");
                        
//                    1173 done in: 9 Second <=> 0.15 Minutes
//            sleep(3);
        $to = time();
        $second = $to-$from;
        echo count($mRes).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;  	
    }       
    
  
    
    /**
     * @Author: ANH DUNG May 20, 2014
     * @Todo: update materials_type_id cho mỗi materials_id 
     * dùng cho báo cáo
     */
    public static function updateMaterialTypeIdStorecardDetail(){
        set_time_limit(72000);
        $from = time();
        $mRes = GasMaterials::model()->findAll();
//        echo count($mRes);die;
//        May 20, 2014 - 276 done in: 7 Second <=> 0.11666666666667 Minutes
        foreach($mRes as $item)
        {            
            GasStoreCardDetail::model()->updateAll(array('materials_type_id'=>$item->materials_type_id),
                    "`materials_id`=$item->id");
        }		
        $to = time();
        $second = $to-$from;
        echo count($mRes).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;  	
    }       
    
    
    /**
     * @Author: ANH DUNG Jun 02, 2014
     * @Todo: update materials_type_id cho mỗi materials_id theo model
     * dùng cho báo cáo
     */
    public static function updateMaterialTypeIdByModel($className){
        set_time_limit(72000);
        $from = time();
        $mRes = GasMaterials::model()->findAll();
        $model = call_user_func(array($className, 'model'));        
        foreach($mRes as $item)
        {            
            $model->updateAll(array('materials_type_id'=>$item->materials_type_id),
                    "`materials_id`=$item->id");
        }
        $to = time();
        $second = $to-$from;
        echo count($mRes).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;  	
    }
    
    /**
     * @Author: ANH DUNG Jun 08, 2014 - 46 done in: 6 Second <=> 0.1 Minutes
     * @Todo: cập nhật loại KH đại lý là 5 cho bảng store card
     */
    public static function updateTypeCustomerAgent(){
        set_time_limit(72000);
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->compare( 't.role_id', ROLE_AGENT);
        $mRes = Users::model()->findAll($criteria);    
//        echo count($mRes);die;
        foreach($mRes as $item){
            GasStoreCard::model()->updateAll(array('type_user'=>CUSTOMER_IS_AGENT),
                    "`customer_id`=$item->id");
            GasStoreCardDetail::model()->updateAll(array('type_customer'=>CUSTOMER_IS_AGENT),
                    "`customer_id`=$item->id");
        }
        
        $to = time();
        $second = $to-$from;
        echo count($mRes).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;          
    }
    
    /******************** IMPORTANT ********************
     * @Author: ANH DUNG Jun 09, 2014
     * @Todo: cập nhật nhập xuất của đại lý trong năm 2014 table GasMaterialsOpeningBalance
     * cái này có khi cứ mỗi đầu năm mới là sẽ chạy vào ngày 01-01, để nó ra đúng Nhập xuất ở trang chủ
     * vì dữ liệu insert vào bảng này có thể sẽ bị ngắt quãng không kiểm soát được
     * tháng 5 chạy lại hàm này 1 lần để check lại phần cut data, vì khi cut data phần dữ liệu 
     * của năm cũ sẽ phục vụ phần báo cáo tồn kho của đại lý
     ******************* IMPORTANT  ****************/
    public static function Agent2014UpdateImportExport($year = ''){
        /** @Note: chay theo thu tu 1: delete all record in 2014
         * 2: find all agent 
         * 3. foreach agent và tính nhập xuất từng vật tư  ... 
         */
        set_time_limit(72000);
        ini_set('memory_limit','2500M');
        $from = time();
//        $year = 2014; 
//        $year = 2015; // 76 done in: 40 Second <=> 0.67 Minutes
        if(empty($year)){
            $year = date('Y')-1; // Dec 25, 2016 chỉnh lại run hàm này trên giao diện hoặc cron cho nhanh
        }
//        echo 'need close';die; // Mar 11, 2015 
//         1. delete all record in 2014 -- đoạn này xóa của năm 2014 đã chạy xong ngày Jun 09, 2014
        $criteria = new CDbCriteria();
        $criteria->compare('year', $year);
        GasMaterialsOpeningBalance::model()->deleteAll($criteria);
//        1. delete all record in 2014 -- đoạn này xóa của năm 2014 đã chạy xong ngày Jun 09, 2014
//        2. Run lan 2 vao Mar 11, 2015 delete all record in 2014
//        3. Run lan 3 vao May 16, 2015 delete all record in 2014
//        4. Run Delete lan 4 vao Dec 06, 2015 => delete all record in 2014
//        5. Run Delete lan 5 vao Dec 25, 2016 => delete all record in 2015 run by Cron Console
//        6. Run Delete lan 6 vao Dec 03, 2017 => delete all record in 2015 run by Cron Console
       
        // 2. find all agent 
        $list_agent_id = Users::getArrIdUserByRole();
        // 3. foreach agent và tính nhập xuất từng vật tư - material của từng đại lý rồi insert từng đại lý 1
        foreach($list_agent_id as $agent_id){
            self::Agent2014BuildOneAgent($agent_id, $year);
        }
        $to = time();
        $second = $to-$from;
        echo $ResultRun = date('d/m/Y H:i')." run YEAR $year UpdateSql::Agent2014UpdateImportExport() -- ".count($list_agent_id).' done in: '.($second).'  Second  <=> '.round($second/60, 2).' Minutes';
        Logger::WriteLog($ResultRun);
        SendEmail::bugToDev($ResultRun, ['title' => "Run YEAR $year UpdateSql::Agent2014UpdateImportExport()"]);
        /** Mar 11, 2015 RUN 63 done in: 15 Second <=> 0.25 Minutes
         * Dec 06, 2015 76 done in: 50 Second <=> 0.833 Minutes
         * Dec 25, 2016 93 done in: 58 Second <=> 0.966 Minutes - run by Cron Console
         * Dec 25, 2016 93 done in: 58 Second <=> 0.966 Minutes - run by Cron Console
         * YEAR 2016 UpdateSql::Agent2014UpdateImportExport() -- 93 done in: 68  Second  <=> 1.13
         * 01/12/2017 00:01 run YEAR 2016 UpdateSql::Agent2014UpdateImportExport() -- 119 done in: 92 Second <=> 1.53 Minutes
         * 02/01/2019 05:20 run YEAR 2017 UpdateSql::Agent2014UpdateImportExport() -- 195 done in: 150  Second  <=> 2.5 Minutesroot@daukhimiennam:~#
         */
    }
    
    // belong to Agent2014UpdateImportExport
    public static function Agent2014BuildOneAgent($agent_id, $year){
        $aRes = array();
        self::Agent2014CalcImportExport($aRes, 'IMPORT', $agent_id, $year, TYPE_STORE_CARD_IMPORT);
        self::Agent2014CalcImportExport($aRes, 'EXPORT', $agent_id, $year, TYPE_STORE_CARD_EXPORT);
        if(!isset($aRes['UNIQUE_MATERIAL_ID'])) return;
        self::Agent2014SqlAdd($aRes, $agent_id, $year);
    }
    // belong to Agent2014UpdateImportExport
    public static function Agent2014CalcImportExport(&$aRes, $NAME_VAR, $agent_id, $year, $type_store_card){
        $dateFrom  = "$year-01-01";
        $dateTo    = "$year-12-31";
        $criteria = new CDbCriteria();
        $criteria->compare('t.type_store_card', $type_store_card); // 1: Nhập, 2: xuất
//        $criteria->compare('year(t.date_delivery)', $year);
        DateHelper::searchBetween($dateFrom, $dateTo, 'date_delivery_bigint', $criteria, false);
        $criteria->compare('t.user_id_create', $agent_id);
        $criteria->select = "t.materials_type_id, t.materials_id,"
                    . " sum(qty) as qty";
        $criteria->group = "t.materials_id";
        $mRes = GasStoreCardDetail::model()->findAll($criteria);
        foreach ($mRes as $item){
            $aRes[$NAME_VAR][$item->materials_id] = $item->qty;
            $aRes['UNIQUE_MATERIAL_ID'][$item->materials_id] = $item->materials_type_id;
        }        
    }
    // belong to Agent2014UpdateImportExport
    public static function Agent2014SqlAdd($aRes, $agent_id, $year){
        $aRowInsert=array();
        foreach($aRes['UNIQUE_MATERIAL_ID'] as $materials_id=>$materials_type_id){
            $import = isset($aRes['IMPORT'][$materials_id])?$aRes['IMPORT'][$materials_id]:0;
            $export = isset($aRes['EXPORT'][$materials_id])?$aRes['EXPORT'][$materials_id]:0;

            $model = new GasMaterialsOpeningBalance();
            $model->year = $year;
            $model->agent_id = $agent_id;
            $model->materials_id = $materials_id;
            $model->materials_type_id = $materials_type_id;
            $model->import = $import;
            $model->export = $export;
            $model->qty = $model->import-$model->export;
            $aRowInsert[]="('$model->year',
                        '$model->agent_id',
                        '$model->materials_id',
                        '$model->materials_type_id',
                        '$model->import', 
                        '$model->export', 
                        '$model->qty'
                        )";
        }
        $tableName = GasMaterialsOpeningBalance::model()->tableName();
        $sql = "insert into $tableName (year,
                    agent_id,
                    materials_id,
                    materials_type_id,
                    import,
                    export,
                    qty
                    ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert)>0)
            Yii::app()->db->createCommand($sql)->execute();
    }
    
    /**
     * @Todo: cập nhật nhập parent_id (hệ thống cho KH) của tất cả các table liên quan
     */
    public static function CustomerUpdateParentId(){
        set_time_limit(72000);
        $from = time();
        $aModelCustomer = self::getAllCustomerStoreCard();
        self::CustomerUpdateParentIdOneTable('GasCashBookDetail', $aModelCustomer);
        self::CustomerUpdateParentIdOneTable('GasStoreCard', $aModelCustomer);
        self::CustomerUpdateParentIdOneTable('GasStoreCardDetail', $aModelCustomer);
        self::CustomerUpdateParentIdOneTable('GasRemain', $aModelCustomer);
        $to = time();
        $second = $to-$from;
        echo count($aModelCustomer).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;          
    }
    
    public static function CustomerUpdateParentIdOneTable($className, $aModelCustomer){
        $model = call_user_func(array($className, 'model'));                
        foreach ($aModelCustomer as $item){
            $customer_parent_id = MyFormat::getParentIdForCustomer($item);
            $model->updateAll(array('customer_parent_id'=>$customer_parent_id),
                    "`customer_id`=$item->id");
        }
    }    
    
    // lấy toàn bộ KH thẻ kho, kh chính là bò mối cần theo dõi
    public static function getAllCustomerStoreCard(){
        $criteria = new CDbCriteria();
        $criteria->addCondition(" t.role_id IN ('".ROLE_CUSTOMER."')");
        $aTypeNot = array(CUSTOMER_TYPE_MAINTAIN, CUSTOMER_TYPE_MARKET_DEVELOPMENT);
        $criteria->addNotInCondition( 't.type', $aTypeNot);
        return Users::model()->findAll($criteria);
    }
        
    // Aug 19, 2014 to update week_setup_id for table gas
    public static function UpdateWeekSetupId(){
        $models = GasBussinessContract::model()->findAll();
        foreach($models as $item){
            $item->update(array('week_setup_id'));
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 24, 2014
     * @Todo: resize file scan
     * @Param: $model
     */
    public static function ResizeFileScan() {
        
    }
    
    /**
     * @Author: ANH DUNG Dec 28, 2015
     * @Todo: get $criteria của KH bò mỐi
     * @Param: $criteria
     */
    public static function getCriteriaKhBoMoi(&$criteria) {
        // Jan 25, 2016 xử lý cho chạy cron update những KH có last_purchase là null để chạy cron thôi
        $criteria->addCondition(" t.last_purchase IS NULL");
        // Jan 25, 2016 xử lý cho chạy cron update những KH có last_purchase là null để chạy cron thôi
        
        $criteria->addCondition(" t.role_id IN ('".ROLE_CUSTOMER."')");
        $aTypeNot = array(CUSTOMER_TYPE_MAINTAIN, CUSTOMER_TYPE_MARKET_DEVELOPMENT);
        $criteria->addNotInCondition( 't.type', $aTypeNot);
        $aTypeBoMoi = array(STORE_CARD_KH_BINH_BO, STORE_CARD_KH_MOI);
        $criteria->addInCondition( 't.is_maintain', $aTypeBoMoi); // Aug 19, 2014 chỉ update KH bò mối thôi, vì cái 
        // type KH của store card hiện tại nó gồm nhiều loại, ko thể update bừa dc
        // $criteria->addCondition( 't.sale_id<>"" AND t.sale_id IS NOT NULL');// có KH không có sale
    }
    
    /**
     * @Author: ANH DUNG Feb 13, 2016
     * @Todo: get $criteria của KH Thẻ kho gồm bò + mỐi+ khác
     * @Param: $criteria
     */
    public static function getCriteriaCustomerStorecard(&$criteria) {
        $criteria->compare('t.role_id', ROLE_CUSTOMER);
        $aTypeNot = array(CUSTOMER_TYPE_MAINTAIN, CUSTOMER_TYPE_MARKET_DEVELOPMENT);
        $criteria->addNotInCondition('t.type', $aTypeNot);
        $aTypeCustomer = array(STORE_CARD_KH_BINH_BO, STORE_CARD_KH_MOI);
        $criteria->addInCondition( 't.is_maintain', $aTypeCustomer); // Aug 19, 2014 chỉ update KH bò mối thôi, vì cái 
    }
    /**
     * @Author: ANH DUNG Feb 13, 2016
     * @Todo: get $criteria của KH Thẻ kho gồm bò + mỐi+ khác
     * @Param: $criteria
     */
    public static function getCriteriaCustomerHgd(&$criteria) {
        $criteria->compare('t.role_id', ROLE_CUSTOMER);
        $aTypeNot = array(CUSTOMER_TYPE_MAINTAIN, CUSTOMER_TYPE_MARKET_DEVELOPMENT);
        $criteria->addNotInCondition('t.type', $aTypeNot);
        $aTypeCustomer = CmsFormatter::$aTypeIdHgd;
        $criteria->addInCondition( 't.is_maintain', $aTypeCustomer); // Aug 19, 2014 chỉ update KH bò mối thôi, vì cái 
    }
    
    /**
     * @Author: ANH DUNG Apr 04, 2015
     * @Todo: update LastPurchase cua kh bo moi
     * chỉ update cho KH bò mối, còn KH không là bò, mối thì không làm
     * Apr 04, 2015: 662 done in:+ 7 Second <=> 0.116 Minutes
     */
    public static function UpdateLastPurchase() {
        set_time_limit(72000);
        $from = time();
        $criteria = new CDbCriteria();
        self::getCriteriaKhBoMoi($criteria);
        
        // type KH của store card hiện tại nó gồm nhiều loại, ko thể update bừa dc
//        $criteria->addCondition( 't.sale_id<>"" AND t.sale_id IS NOT NULL');// có KH không có sale
        $mRes = Users::model()->findAll($criteria);
//        echo count($mRes);die;
        $run = 0;
        foreach($mRes as $item)
        {
            if(empty($item->last_purchase)){
                $mStoreCardLast = GasStoreCard::GetLastPurchase($item->id);
                if($mStoreCardLast){
                    $item->last_purchase = $mStoreCardLast->date_delivery;
                    $item->update(array('last_purchase'));
                    $run++;
                }
            }
        }
        
        $to = time();
        $second = $to-$from;
        $ResultRun = "CRON UpdateLastPurchase cho những model NULL.  ".$run.' done in:+ '.($second).'  Second  <=> '.($second/60).' Minutes';die;
        if($run){
            Logger::WriteLog($ResultRun);
        }
    }
    
    
    /**
     * @Author: ANH DUNG Apr 10, 2015
     * @Todo: ngày nhập gas dư và thẻ kho mới nhất của đại lý
     * @important hàm này run hàng ngày qua cron job không được sửa
     */
    public static function UpdateLastInputRemainStorecard() {
//        set_time_limit(72000);
//        $from = time();
        $criteria = new CDbCriteria();
        $criteria->compare('t.role_id', ROLE_AGENT);
        $mRes = Users::model()->findAll($criteria);
//        echo count($mRes);die;
        foreach($mRes as $mUser){
            $date_last_gas_remain = self::GetLastInputRemain($mUser->id);
            $date_last_storecard = self::GetLastInputStorecard($mUser->id);
            if(!empty($date_last_gas_remain)){
                Users::UpdateKeyInfo($mUser, 'date_last_gas_remain', $date_last_gas_remain);
            }
            if(!empty($date_last_storecard)){
                Users::UpdateKeyInfo($mUser, 'date_last_storecard', $date_last_storecard);
            }
        }
//        $to = time();
//        $second = $to-$from;
//        echo count($mRes).' done in:+ '.($second).'  Second  <=> '.($second/60).' Minutes';die;
    }
    
    /**
     * @Author: ANH DUNG Apr 10, 2015
     * @Todo: belong to UpdateLastInputRemainStorecard
     * get ngay nhap gas du moi nhat 
     * @Param: $agent_id
     */
    public static function GetLastInputRemain($agent_id) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.agent_id', $agent_id);
        $criteria->order = 't.date_input DESC';
        $criteria->limit = 1;
        $model = GasRemain::model()->find($criteria);
        if($model){
            return $model->date_input;
        }
        return '';
    }
    
    /**
     * @Author: ANH DUNG Apr 10, 2015
     * @Todo: belong to UpdateLastInputRemainStorecard
     * get ngay nhap the kho moi nhat 
     * @Param: $agent_id
     */
    public static function GetLastInputStorecard($agent_id) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.user_id_create', $agent_id);
        $criteria->order = 't.date_delivery DESC';
        $criteria->limit = 1;
        $model = GasStoreCard::model()->find($criteria);
        if($model){
            return $model->date_delivery;
        }
        return '';
    }
    
    
     /**
     * @Author: ANH DUNG Apr 22, 2015
     * @Todo: cập nhật province_id của đại lý cho thẻ kho và chi tiết thẻ kho
      * Run Apr 22, 2015 done in 2.85 minutes khá lâu
     */
    public static function UpdateProvinceIdAgentOfStorecard(){
        set_time_limit(72000);
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->compare( 't.role_id', ROLE_AGENT);
        $mRes = Users::model()->findAll($criteria);    
        echo count($mRes);die;
        foreach($mRes as $item){
            GasStoreCard::model()->updateAll(array('province_id_agent'=> $item->province_id ),
                    "`user_id_create`=$item->id");
            GasStoreCardDetail::model()->updateAll(array('province_id_agent'=> $item->province_id ),
                    "`user_id_create`=$item->id");
        }
        
        $to = time();
        $second = $to-$from;
        echo count($mRes).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;          
    }
    
    
     /**
     * @Author: ANH DUNG Dec 18, 2015
     * @Todo: cập nhật chuyển sale của KH 
      * Run Dec 18, 2015 1 done in: 1 Second
     */
    public static function ChangeSaleCustomer(){
//        die('Open check code before run');
        $sale_from  = [1241317];
//        $sale_to    = 1011646;// Khách Hàng Công Ty - Mối
//        $sale_to    = 49184;// Khách Hàng Công Ty - Bò
        $sale_to    = 136962; // Đặng Hoàng Tuấn
        $area_code_id_change = 475830;
        $aAreaCodeId = [1048571, 1079730, 1120439, 1161320];

//        die(' can check lai, ham nay chuyen all Kh cả BO + MOI. Ham chuyen KH moi Ben duoi ChangeSaleCustomerByDistrict');
        set_time_limit(72000);
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->addInCondition( 'sale_id', $sale_from);
        $criteria->compare( 'role_id', ROLE_CUSTOMER);
//        $criteria->addInCondition( 'area_code_id', $aAreaCodeId);// Chuyển KH của 1 số đại lý giới hạn 
        $aTypeCustomer = [STORE_CARD_KH_BINH_BO, STORE_CARD_KH_MOI];
//        $aTypeCustomer = [STORE_CARD_KH_BINH_BO];
//        $aTypeCustomer = [STORE_CARD_KH_MOI];
        $criteria->addInCondition( 'is_maintain', $aTypeCustomer);
        $mRes = Users::model()->findAll($criteria);

//        echo count($mRes);die;
        $aUpdate = array('sale_id' => $sale_to);
//        $aUpdate = array('sale_id' => $sale_to, 'area_code_id' => $area_code_id_change);
        Users::model()->updateAll($aUpdate, $criteria);
        foreach($mRes as $mUser){
            $mUser->solrAdd();
        }
        
        /* for change sale on setup price */
        $month = date('m')*1; $year = date('Y');
        $criteria = new CDbCriteria();
        $criteria->addCondition('c_month='.($month*1).
                ' AND c_year='.$year.
                ' AND type='.UsersPrice::TYPE_BO_MOI
            );
        $criteria->addInCondition( 'sale_id', $sale_from);
//        $criteria->addInCondition( 'agent_id', $aAreaCodeId);
        UsersPrice::model()->updateAll($aUpdate, $criteria);
        
        /* for change sale on setup price */

        $to = time();
        $second = $to-$from;
        echo count($mRes).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;          
    }
    
    /**
     * @Author: ANH DUNG Dec 24, 2015
     * @Todo: cập nhật type_customer cua issue ticket
      *
     */
    public static function IssueTicketUpdateTypeCustomer(){
        set_time_limit(72000);
        $from = time();
        $criteria = new CDbCriteria();
        $mRes = GasIssueTickets::model()->findAll($criteria);
        echo count($mRes). " -- need close to run function";die;
        foreach($mRes as $item){
            $item->update(array('type_customer'));
        }
        
        $to = time();
        $second = $to-$from;
        echo count($mRes).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;          
    }
    
    /**
     * @Author: ANH DUNG Dec 28, 2015
     * @Todo: update sale id cho Issueticket
     */         
    public static function UpdateSaleIdSomeTable(){
        $from = time();
        $criteria = new CDbCriteria();
        self::getCriteriaKhBoMoi($criteria);
        $mRes = Users::model()->findAll($criteria);
        echo count($mRes);die;
        foreach($mRes as $item)
        {
            $item->changeSale();
        }
        $to = time();
        $second = $to-$from;
        echo count($mRes).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;  	
        // Dec 28, 2015: 5055 done in: 6 Second <=> 0.1 Minutes
    }    
    
    
    /**
     * @Author: ANH DUNG Feb 13, 2016
     * @Todo: update lại address_vi giảm tải cho index table user
     * chỉ update cho KH storecard
     * 5699 done in:+ 38 Second <=> 0.633 Minutes Feb 13, 2016
     * 5715 done in:+ 42 Second <=> 0.7 Minutes Feb 18, 2016
     * 6664 done in:+ 73 Second <=> 1.216 Minutes May 23, 2016
     */
    public static function UpdateOnlyAddressVi() {
        set_time_limit(72000);
        $from = time();
        $criteria = new CDbCriteria();
        self::getCriteriaCustomerStorecard($criteria);

        $mRes = Users::model()->findAll($criteria);
        echo count($mRes);die;
        $aUpdate = array('address_vi');
        foreach($mRes as $item)
        {
            $item->update($aUpdate);
        }

        $to = time();
        $second = $to-$from;
        echo $ResultRun = "".count($mRes).' done in:+ '.($second).'  Second  <=> '.($second/60).' Minutes';
        die;
    }
    
    /**
     * @Author: ANH DUNG Mar 03, 2016
     * @Todo: cập nhật chuyển sale của KH theo đại lý
     *  Xử lý Y/C dạng: Quý này thay đổi CVKV ôm đại lý nên em chuyển đúng cho anh như sau:
        1. Đặng Trung Hiếu: Thủ Dầu 1, bến cát, lái thiêu, tân định ( Cắt Phú Hoà)
      * Run Dec 18, 2015 1 done in: 1 Second
        Lê thiện chương ( tạm nghỉ) long bình tân, long thành, đông hòa, ngã 3 trị an.
     */
    public static function  ChangeSaleCustomerByDistrict(){
//        $aDistrict = array(
//            105,
//            113,
//        );
        $sale_from  = [1657571]; // tb vuong
        $sale_to = 937389; // Lê Trung Hiển
        $aAgentId = [
            392141,
        ];
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->addInCondition( 'sale_id', $sale_from);
//        $criteria->addInCondition( 'district_id', $aDistrict);
        $criteria->addInCondition( 'area_code_id', $aAgentId);
        $criteria->compare( 'role_id', ROLE_CUSTOMER);
        $aTypeCustomer = [STORE_CARD_KH_BINH_BO, STORE_CARD_KH_MOI, STORE_CARD_XE_RAO];
//        $aTypeCustomer = [STORE_CARD_KH_MOI];
        $criteria->addInCondition( 'is_maintain', $aTypeCustomer);
//        $criteria->addCondition( "DATE(created_date) < '2017-03-01'" );// Chương Y/C kh trước ngày 01-03-17
//        $mRes = Users::model()->count($criteria);
//        echo $mRes; die(' - ok run');
        $mRes = Users::model()->findAll($criteria);
        $aUpdate = array('sale_id' => $sale_to);
        Users::model()->updateAll($aUpdate, $criteria);
        foreach($mRes as $mUser){
            $mUser->solrAdd();
        }
        
        /* for change sale on setup price */
        $month = date('m')*1; $year = date('Y');
        $criteria = new CDbCriteria();
        $criteria->addCondition('c_month='.($month*1).
                ' AND c_year='.$year.
                ' AND type='.UsersPrice::TYPE_BO_MOI
            );
        $criteria->addInCondition( 'sale_id', $sale_from);
        $criteria->addInCondition( 'agent_id', $aAgentId);
        UsersPrice::model()->updateAll($aUpdate, $criteria);
        
        /* for change sale on setup price */

        $to = time();
        $second = $to-$from;
        echo count($mRes).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;          
    }
    
    /**
     * @Author: ANH DUNG Apr 11, 2016
     * @Todo: update created_by cho KH
     * @Param: 
     */
    public static function updateCreatedByForCustomer() {
        die('CHECK kỹ khi run cái này');
        $dpBinhDinh = GasLeave::DIEU_PHOI_BINH_DINH;
        $criteria = new CDbCriteria();
        
        // FOR HCM DONE
//        $aZoneOtherHcm = implode(",", array_merge(GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TAY, GasOrders::$PROVINCE_ZONE_MIEN_TRUNG)) ;
//        $criteria->addCondition("province_id NOT IN ($aZoneOtherHcm) AND (created_by IS NULL OR created_by <> $dpBinhDinh) " );
//        // Bị lỗi so sánh NULL với số sẽ không compare dc: vd created_by <> $dpBinhDinh sẽ không so sánh với NULL dc
//        self::getCriteriaCustomerStorecard($criteria);
//        $aUpdate = array('created_by' => GasTickets::DIEU_PHOI_CHUONG);
        // FOR HCM DONE
        
        // FOR MIEN TAy DONE
        $aZoneMienTay = implode(",", GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TAY) ;
        $criteria->addInCondition("province_id", GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TAY);
        // Bị lỗi so sánh NULL với số sẽ không compare dc: vd created_by <> $dpBinhDinh sẽ không so sánh với NULL dc
        self::getCriteriaCustomerStorecard($criteria);
        $aUpdate = array('created_by' => GasTickets::DIEU_PHOI_NHUNG);
        // FOR MIEN TAy DONE
        
        Users::model()->updateAll($aUpdate, $criteria);
//        $models = Users::model()->count($criteria);
//        echo '<pre>';
//        print_r($models);
//        echo '</pre>';
//        die;
        die;
        $aRoleCheck = array(ROLE_DIEU_PHOI, ROLE_HEAD_OF_MAINTAIN);
        if(in_array($cRole, $aRoleCheck)){
            if (in_array($cUid, GasOrders::$ZONE_HCM)){
                $aZoneOtherHcm = array_merge(GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TAY, GasOrders::$PROVINCE_ZONE_MIEN_TRUNG);
                $criteria->addNotInCondition('t.province_id', $aZoneOtherHcm);
            }elseif (in_array($cUid, GasOrders::$ZONE_MIEN_TAY)){
                $criteria->addInCondition('t.province_id', GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TAY);
            }elseif (in_array($cUid, GasOrders::$ZONE_MIEN_TRUNG)){
//                $criteria->addInCondition('t.province_id', GasOrders::$PROVINCE_ZONE_MIEN_TRUNG);// tạm thời làm kiểu bên dưới cho 1 user điều phối, nếu có 2 tạo thì phải làm khas
                $sZoneMienTrung = implode(",", GasOrders::$PROVINCE_ZONE_MIEN_TRUNG);
                $tGiaLai = GasProvince::TINH_GIALAI;
                $dpBinhDinh = GasLeave::DIEU_PHOI_BINH_DINH;
                $criteria->addCondition("t.province_id IN ($sZoneMienTrung) OR (t.province_id=$tGiaLai AND t.created_by=$dpBinhDinh)" );// tạm thời làm kiểu bên dưới cho 1 user điều phối, nếu có 2 tạo thì phải làm khas
            }
        }
    }
    
    /**
     * @Author: ANH DUNG Apr 21, 2016
     * @Todo: gen new code for customer
     */
    public static function genCodeCustomer() {
//        die('need close');
        set_time_limit(72000);
        $from = time();
        $criteria = new CDbCriteria();
//        self::getCriteriaCustomerStorecard($criteria);
        $criteria->addCondition('t.role_id=' . ROLE_CUSTOMER);
        $criteria->addCondition('t.type=' . CUSTOMER_TYPE_STORE_CARD);
        $criteria->addInCondition('t.is_maintain', CmsFormatter::$aTypeIdQuotes); // Aug 19, 2014 chỉ update KH bò mối thôi, vì cái 
        
        $criteria->compare('t.channel_id', Users::CON_LAY_HANG);
        $criteria->addCondition("t.username IS NULL OR t.username=''");
        $models = Users::model()->findAll($criteria);
        // sj0001
        $prefix_code = "sj";
        $length_max_id = 6;
//        echo '<pre>';
//        print_r(count($models));
//        echo '</pre>';
//        die;
        $max_id = 5132;
        foreach($models as $model) {
            $addition_zero_num = $length_max_id - strlen($max_id) - strlen($prefix_code);
            $code = $prefix_code;
            for($i=1;$i<=$addition_zero_num;$i++){
                $code.='0';
            }
            $code.= $max_id;
//            echo $code." -- ";
            $max_id++;
            $model->username = $code;
            $model->makePassword();
            $model->update(array('username', 'temp_password', 'password_hash'));
        }
        $to = time();
        $second = $to-$from;
        echo count($models).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;          
    }
    
    /**
     * @Author: ANH DUNG Jun 25, 2017
     * @Todo: make pass word for old customer
     */
    public static function makePasswordCustomer() {
//        die('need close');
        set_time_limit(72000);
        $from = time();
        $criteria = new CDbCriteria();
//        self::getCriteriaCustomerStorecard($criteria);
        $criteria->addCondition('t.role_id=' . ROLE_CUSTOMER);
        $criteria->addCondition('t.type=' . CUSTOMER_TYPE_STORE_CARD);
//        $criteria->addInCondition('t.is_maintain', [UsersExtend::STORE_CARD_HEAD_QUATER, STORE_CARD_XE_RAO]);
        $criteria->addInCondition('t.is_maintain', CmsFormatter::$aTypeIdMakeUsername); // Aug 19, 2014 chỉ update KH bò mối thôi, vì cái 
        $criteria->compare('t.channel_id', Users::CON_LAY_HANG);
//        $criteria->addCondition("t.username<>''");
        $models = Users::model()->findAll($criteria);
        echo '<pre>';
        print_r(count($models));
        echo '</pre>';
        die;
        foreach($models as $model) {
            $model->makePassword();
            $model->update(array('temp_password', 'password_hash'));
//            make record change pass
            $mMonitorUpdate = new MonitorUpdate();
            $mMonitorUpdate->makeRecordChangePass($model);
        }
        $to = time();
        $second = $to-$from;
        echo count($models).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;          
    }
    
    /**
     * @Author: ANH DUNG May 13, 2016
     * @Todo: tách số phone của KH ra 1 table khác để phục vụ điều phối search
     */
    public static function changePhoneCustomer() {
        set_time_limit(72000);
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->compare('role_id', ROLE_CUSTOMER);
        $criteria->compare('type', CUSTOMER_TYPE_STORE_CARD);
        $models = Users::model()->findAll($criteria);
        foreach($models as $mUser) {
            UsersPhone::savePhone($mUser);
        }
        $to = time();
        $second = $to-$from;
        echo count($models).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;
    }
    
    /**
     * @Author: ANH DUNG Jul2217
     * @Todo: Update phone của KH CCS fix phone fixphone hgd ccs
     */
    public static function updatePhoneCustomer() {
        set_time_limit(72000);
        $from = time();$run = 0; $record = 0;$sPhone='';
        for($i = 1; $i< 10; $i++):
            $criteria = new CDbCriteria();
            $criteria->compare('role_id', ROLE_CUSTOMER);
            $criteria->compare('is_maintain', STORE_CARD_HGD_CCS);
            $criteria->addCondition('password_hash=-1');
            $criteria->limit = 1000;
            $models = Users::model()->findAll($criteria);
            foreach($models as $mUser) {
                UsersPhone::savePhone($mUser);
                Users::model()->updateAll(['password_hash'=>-2],"id=$mUser->id");
                $run++;
            }
        endfor;
        
        $to = time();
        $second = $to-$from;
        echo " record -- ".$run.' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;
    }
    
    public static function deletePhoneCustomer() {
        set_time_limit(72000);
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->addCondition("role_id=0 OR role_id<1");
        $models = UsersPhone::model()->findAll($criteria);
        foreach($models as $mUserPhone) {
            $mUser = Users::model()->findByPk($mUserPhone->user_id);
            if($mUser){
                UsersPhone::savePhone($mUser);
            }
        }
        $to = time();
        $second = $to-$from;
        echo count($models).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;
    }
    
    /**
     * @Author: ANH DUNG May 23, 2016
     * @Todo: update role for Usertoken
     */
    public static function updateRoleForToken() {
        set_time_limit(72000);
        $from = time();
        $models = UsersTokens::model()->findAll();
        foreach($models as $model) {
            $model->role_id = $model->rUser->role_id;
            $model->update(array('role_id'));
        }
        $to = time();
        $second = $to-$from;
        echo count($models).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;
    }
    
    /**
     * @Author: ANH DUNG Jun 08, 2016
     * @Todo: cập nhật những Kh không lấy hàng thì thôi không bt nữa
     */
    public static function updateFixStatusUpholdSchedule() {
        set_time_limit(72000);
        $from = time();
        
        $criteria = new CDbCriteria();
        UpdateSql::getCriteriaCustomerStorecard($criteria);
        $criteria->addInCondition('channel_id', [Users::KHONG_LAY_HANG, Users::CHAN_HANG]);
        $models = Users::model()->findAll($criteria);
        $aId = CHtml::listData($models, 'id', 'id');
        echo '<pre>';
        print_r($aId);
        echo '</pre>';
        die;
        GasUphold::setStatusUpholdSchedule($models);
        $to = time();
        $second = $to-$from;
        echo count($models).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;
    }
    
    /**
     * @Author: ANH DUNG Jul 14, 2016
     * @Todo: xóa KH của CCS oR Sale
     */
    public static function deleteCustomerOfSale() {
//        $sale_id = 866213;// Hồng Quân
        $sale_id = array(360792, 866633);//
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->addInCondition("t.sale_id", $sale_id);
        $criteria->compare('t.role_id', ROLE_CUSTOMER);
        $criteria->addInCondition( 't.is_maintain', CmsFormatter::$aTypeIdHgd); // Jun 13, 2016 xử lý search KH bò mối bị lẫn cả hộ GD vào
        $criteria->compare('month(t.created_date)', 06);
        $criteria->compare('year(t.created_date)', 2016);
        
        $models = Users::model()->findAll($criteria);
        echo '<pre>';
        print_r(count($models));
        echo '</pre>';
        die;
        foreach($models as $mCustomer) {
            $mCustomer->delete();
        }
        $to = time();
        $second = $to-$from;
        echo count($models).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;
    }
    
    
    /**
     * @Author: ANH DUNG Jul 18, 2016
     * @Todo: change phone for agent, make it run one time
     */
    public static function changePhoneAgent() {
        $criteria = new CDbCriteria();
        $criteria->compare('t.role_id', ROLE_AGENT);
        $models = Users::model()->findAll($criteria);
        echo '<pre>';
        print_r(count($models));
        echo '</pre>';
        die;
        foreach($models as $item){
//            $item->code_bussiness = $item->phone;
//            $item->update(array('code_bussiness'));
            $item->phone = "";
            $item->update(array('phone'));
        }
        die;
    }
    
    /**
     * @Author: ANH DUNG Sep 20, 2016
     * @Todo: update lại address_vi giảm tải cho index table user 
     * set empty cho những KH không lấy hàng, đã run Sep 20, 2016 có vẻ không cải thiện được nhiều 
     * chỉ update cho KH storecard
     */
    public static function UpdateAddressViCustomerKhongLayHang() {
        set_time_limit(72000);
        $from = time();
        
        // 1. fix xóa address_vi cho KH bò mối không lấy hàng
        $criteria = new CDbCriteria();
        $criteria->compare('role_id', ROLE_CUSTOMER);
        $criteria->compare('type', CUSTOMER_TYPE_STORE_CARD);
        $aTypeBoMoi = array(STORE_CARD_KH_BINH_BO, STORE_CARD_KH_MOI);
        $criteria->addInCondition( 'is_maintain', $aTypeBoMoi); // Aug 19, 2014 chỉ update KH bò mối thôi, vì cái 
        $criteria->compare('channel_id', Users::KHONG_LAY_HANG);
//        $aUpdate = array('address_vi' => "");
//        Users::model()->updateAll($aUpdate, $criteria);
        $mRes = Users::model()->findAll($criteria);
//        $aFixUpdate = array('address_vi');
//        foreach($mRes as $item)
//        {
//            $item->update($aFixUpdate);
//        }

//        $mRes = Users::model()->count($criteria);
//        echo $mRes;die;
        
        // 2. fix xóa address_vi cho KH CUSTOMER_TYPE_MAINTAIN, CUSTOMER_TYPE_MARKET_DEVELOPMENT
        // SELECT * FROM `gas_users` where role_id=4 and type in (1,2)
        // DELETE FROM `c1io`.`gas_users` WHERE role_id=4 and type in (1,2)
//        $criteria = new CDbCriteria();
//        $criteria->compare('role_id', ROLE_CUSTOMER);
//        $criteria->addInCondition('type', array(CUSTOMER_TYPE_MAINTAIN, CUSTOMER_TYPE_MARKET_DEVELOPMENT));
//        $aUpdate = array('address_vi' => "");
//        Users::model()->updateAll($aUpdate, $criteria);
        
        $to = time();
        $second = $to-$from;
        echo $ResultRun = "".count($mRes).' done in:+ '.($second).'  Second  <=> '.($second/60).' Minutes';
        die;
    }
    
    /**
     * @Author: ANH DUNG Oct 08, 2016
     * @Todo: run check thẻ kho detail mồ côi, không có storecard tương ứng
     */
    public static function checkStorecarDetailError() {
        /* OK SQL run to check
        SELECT * FROM `gas_gas_store_card_detail` where store_card_id not in (SELECT id FROM `gas_gas_store_card`);
        SELECT * FROM `gas_gas_store_card` where id not in (SELECT store_card_id FROM `gas_gas_store_card_detail`);
         * SELECT * FROM `gas_gas_store_card` where id not in (SELECT store_card_id FROM `gas_gas_store_card_detail`)  AND (uid_login not IN (59235,57916,475396,852423,295085))
         */
        die;
        $year = 2016;
        $criteria = new CDbCriteria();
        $criteria->addCondition("year(t.date_delivery)=$year");
        $criteria->select   = "t.store_card_id";
        $criteria->group    = "t.store_card_id";
        $models = GasStoreCardDetail::model()->count($criteria);
        echo '<pre>';
        print_r(($models));
        echo '</pre>';
        die;
    }
    
    /**
     * @Author: ANH DUNG Oct 16, 2016
     * @Todo: xóa data những cửa hàng test PM bán hàng
     */
    public static function deleteTestSell() {
        $aAgent = array(
            262526, // Cửa Hàng Ô Môn
            138544, // Cần Thơ 2
            652695, // Cửa Hàng Bình Minh
            197436, // Cửa Hàng Phú Thuận
            30751, // Cửa hàng Vĩnh Long 1 -- ĐL Đại hàn Vĩnh Long
            148404, // Cửa hàng Vũng Liêm
            171449, // Cửa hàng Trà Vinh
            
            1311, // Cửa hàng 1
            1312, // Cửa hàng 2
            1313, // Cửa hàng 3
            1314, // Cửa hàng 4
            28941, // Cửa hàng Thi Sách
            268835, // Cửa hàng Chư Sê
            
            475830, // Cửa Hàng Long Xuyên 1
            596026, // Cửa Hàng Long Xuyên 2
            693615, // Kho Chợ Mới
            863146, // Cửa Hàng Chợ Mới 2
        );
        
        $criteria = new CDbCriteria();
        $criteria->addInCondition("agent_id", $aAgent);
        $count = Sell::model()->count($criteria);
        echo '<pre>';
        print_r($count);
        echo '</pre>';
        die;
        Sell::model()->deleteAll($criteria);
        SellDetail::model()->deleteAll($criteria);
        die;
    }
    
    /**
     * @Author: ANH DUNG Oct 16, 2016
     * @Todo: update lại province_id_agent cho thẻ kho và detail thẻ kho của những phiếu do điều phối tạo
     */
    public static function updateProvinceIdAgentStorecard() {
        set_time_limit(72000);
        $from = time();

        $aAllAgent      = Users::getArrObjectUserByRole(ROLE_AGENT);
        foreach($aAllAgent as $agent_id => $mAgent){
            $criteria = new CDbCriteria();
            $criteria->compare('user_id_create', $agent_id);
            $criteria->addCondition("date_delivery>='2016-10-01'");
            $aUpdate = array('province_id_agent' => $mAgent->province_id);
            
            GasStoreCard::model()->updateAll($aUpdate, $criteria);
            GasStoreCardDetail::model()->updateAll($aUpdate, $criteria);
        }
        $to = time();
        $second = $to-$from;
        echo $ResultRun = "".count($aAllAgent).' done in:+ '.($second).'  Second  <=> '.($second/60).' Minutes';
        die;
    }
    
    /**
     * @Author: ANH DUNG Now 01 , 2016
     * @Todo: update lại bảo trì định kỳ - change nhân viên
     */
    public static function updateUpholdScheduleEmployee() {
//        $employee_id_from  = 124;// 1. trong nghia
//        $employee_id_to    = 195;//Đặng Ngọc Huấn ok
//        $employee_id_from  = 903951; // 2. Trần Phi Thanh
//        $employee_id_to    = 124; //trong nghia
//        die(' need check before run');
        $employee_id_from  = 49175;
        $employee_id_to    = 1976782;// Feb0219 Trần Văn Cương
        
        set_time_limit(72000);
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->compare( 'type', GasUphold::TYPE_DINH_KY);
//        $criteria->compare( 'type_customer', STORE_CARD_KH_BINH_BO);
//        $criteria->compare( 'type_customer', STORE_CARD_KH_MOI);
        $criteria->compare( 'employee_id', $employee_id_from);
//        $mRes = GasUphold::model()->count($criteria);    
//        echo $mRes; die;
        $aUpdate = array('employee_id' => $employee_id_to);
        GasUphold::model()->updateAll($aUpdate, $criteria);

        $to = time();
        $second = $to-$from;
        echo ' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;          
    }
    
    /**
     * @Author: ANH DUNG Now 20, 2016
     * @Todo: update lại road_route cho storecard detail, do lỗi lập trình bị sai từ tháng 10
     * @note: sản lượng của ĐP sẽ dc tính từ tháng 10/2016
     */
    public static function updateStorecardDetailRoadRoute() {
        set_time_limit(72000);
        ini_set('memory_limit','500M');
        $from = time();

        $criteria = new CDbCriteria();
//        $criteria->compare('road_route', GasStoreCard::ROAD_ROUTE_FIX);
//        $criteria->addCondition("date_delivery>='2016-10-01'");
        // t09: 7524 done in:+ 12 Second <=> 0.2 Minutes -- t10: 17096 done in:+ 28 Second -- 
        // Now 28, 2016 fix uid_login dieu phoi trong detail
//        $criteria->addCondition("date_delivery>='2016-08-01'");
        $criteria->addCondition("month(date_delivery)='10'");
        $criteria->addCondition("year(date_delivery)='2016'");
        $sParamsIn = implode(',', GasTickets::$UID_DIEU_PHOI);
        $criteria->addCondition("uid_login IN ($sParamsIn)");
        // Now 28, 2016 fix uid_login dieu phoi trong detail
        
        $count = GasStoreCard::model()->count($criteria); echo $count;die;
        $models = GasStoreCard::model()->findAll($criteria);
        
        foreach($models as $mStorecard){
            $criteria = new CDbCriteria();
            $criteria->compare('store_card_id', $mStorecard->id);
//            $aUpdate = array('road_route' => $mStorecard->road_route);
            $aUpdate = array('uid_login' => $mStorecard->uid_login);
//            GasStoreCard::model()->updateAll($aUpdate, $criteria);
            GasStoreCardDetail::model()->updateAll($aUpdate, $criteria);
        }
        $to = time();
        $second = $to-$from;
        echo $ResultRun = "".count($models).' done in:+ '.($second).'  Second  <=> '.($second/60).' Minutes';
        die;
    }
    
    
    /**
     * @Author: ANH DUNG Dec 06, 2016
     * @Todo: cập nhật lại điều phối tạo
     */
    public static function updateCallHistory() {
        $from = time();
        foreach(GasTickets::$UID_DIEU_PHOI as $uid){
            $criteria = new CDbCriteria();
            $criteria->compare('user_id', $uid);
            $aUpdate = array('agent_id' => 0);
//            GasStoreCard::model()->updateAll($aUpdate, $criteria);
            CallHistory::model()->updateAll($aUpdate, $criteria);
        }
        $to = time();
        $second = $to-$from;
        echo $ResultRun = "".count($models).' done in:+ '.($second).'  Second  <=> '.($second/60).' Minutes';
        die;
    }
    
    /**
     * @Author: ANH DUNG Feb 06, 2017
     * @Todo: set thương hiệu gas cho những KH đã có của hệ thống
     * @Param: chia làm 2 phần, những KH còn lấy hàng và KH không còn lấy hàng
     */
    public static function setGasForCustomer() {
        $from = time();
        $criteria = new CDbCriteria();
//        $criteria->addCondition('t.customer_id=1');// only for dev test
        
        $sParamsIn = implode(',', CmsFormatter::$MATERIAL_TYPE_BINHBO_12);
        $criteria->addCondition("t.materials_type_id IN ($sParamsIn)");
        $criteria->addCondition('t.type_in_out='.STORE_CARD_TYPE_3. ' OR t.type_in_out='.STORE_CARD_TYPE_5 );
        $criteria->addCondition('YEAR(t.date_delivery)>2015');
        $sParamsIn = implode(',', CmsFormatter::$ARR_TYPE_CUSTOMER_STORECARD);
        $criteria->addCondition("t.type_customer IN ($sParamsIn)");
        UpdateSql::getLimitCustomer($criteria);
        $criteria->select   = 'sum(t.qty) as qty, t.customer_id, t.materials_type_id, t.materials_id, t.type_in_out';
        $criteria->group    = 't.customer_id, t.type_in_out, t.materials_type_id, t.materials_id';
        $models = GasStoreCardDetail::model()->findAll($criteria);
        $aCustomer  = [];// find ra 1 lần customer để update
        $aRes       = [];
        foreach($models as $item){
            $aRes[$item->customer_id][$item->type_in_out][$item->materials_type_id][$item->materials_id] = $item->qty*1;
            $aCustomer[$item->customer_id]  = $item->customer_id;
        }
        // thử sắp xếp mảng qty giảm dần arsort High to low
        foreach($aRes as $customer_id => $aInfo){
            foreach($aInfo as $type_in_out => $aInfo1){
                foreach($aInfo1 as $materials_type_id => $aInfo2){
                    arsort($aRes[$customer_id][$type_in_out][$materials_type_id]);
                }
            }
        }
        
        
        $aModelUsersRef = UpdateSql::getUsersRef($aCustomer);
        
        echo '<pre>';
        echo "Total Customer Find: ".count($aCustomer)." --";
        echo "Total Customer UsersRef: ".count($aModelUsersRef)." --";
        echo '</pre>';
//        die;
        $countAddNew = 0;
        foreach($aCustomer as $customer_id){
//        foreach($aModelUsersRef as $mUsersRef){
            if(!isset($aRes[$customer_id])){
                continue;
            }
            if(isset($aModelUsersRef[$customer_id])){
                $mUsersRef = $aModelUsersRef[$customer_id];
            }else{
                $mUsersRef = new UsersRef();
                $mUsersRef->user_id = $customer_id;
            }

            /** 1. map field từ json vào
             *  2. init biến post giống như web post
             *  3. gọi hàm build json gas_brand
             *  4. đưa vào sql để build 1 query
             */
            $mUsersRef->getMapFieldContact();
            UpdateSql::mapPostUsersRef($aRes[$mUsersRef->user_id]);
            $mUsersRef->setGasBrand();
            $mUsersRef->setFieldContact();
            if(empty($mUsersRef->id)){
                $countAddNew ++;
                $mUsersRef->save();
            }else{
                $mUsersRef->update(array('contact_person'));
            }

        }
        echo '<pre>';
        echo "countAddNew: $countAddNew";
        echo '</pre>';
//        login_attemp
        /* Feb 06, 2017 với KH không lấy hàng channel_id=2
         * channel_id=2 Customer Find: 2189 --Total Customer UsersRef: 1943 -- countAddNew: 246 -- 2189 done in:+ 20 Second <=> 0.3 Minutes
         * channel_id=1 Customer Find: 2941 --Total Customer UsersRef: 2676 -- countAddNew: 265 -- 2941 done in:+ 25 Second <=> 0.41 Minutes
         * channel_id=1 2883 done in:+ 27 Second <=> 0.45 Minutes
         */

        $to = time();
        $second = $to-$from;
        echo $ResultRun = "".count($aCustomer).' done in:+ '.($second).'  Second  <=> '.($second/60).' Minutes';
        die;
    }
    
    /**
     * @Author: ANH DUNG Feb 06, 2017
     * @Todo: khởi tạo biến post cho mỗi user ref array userRef của customer
     */
    public static function mapPostUsersRef($aInfo) {
        $_POST['gas']   = [];
        $_POST['vo']    = [];
        $aGas   = isset($aInfo[STORE_CARD_TYPE_3]) ? $aInfo[STORE_CARD_TYPE_3] : [];
        $aVo    = isset($aInfo[STORE_CARD_TYPE_5]) ? $aInfo[STORE_CARD_TYPE_5] : [];
        foreach(UpdateSql::getMaterialsTypePair() as $materials_type_id_gas => $materials_type_id_vo){
            if(!isset($aGas[$materials_type_id_gas])){
                continue;
            }
            $index = 0;
            foreach($aGas[$materials_type_id_gas] as $materials_id => $qty){
                if($index > 1){// chỉ lấy 2 loại gas hoặc vỏ có sl nhiều nhất để set cho customer
                    break;
                }
                $_POST['gas'][$materials_type_id_gas][$index] = $materials_id;
                $index++;
            }
            
            // for array Post vỏ
            $index = 0;
            foreach($aVo[$materials_type_id_vo] as $materials_id => $qty){
                if($index > 1){// chỉ lấy 2 loại gas hoặc vỏ có sl nhiều nhất để set cho customer
                    break;
                }
                $_POST['vo'][$materials_type_id_gas][$index] = $materials_id;
                $index++;
            }
        }
    }
    
    /**
     * @Author: ANH DUNG Feb 06, 2017
     * @Todo: get CẶP GIÁ TRỊ GAS-VỎ 
     */
    public static function getMaterialsTypePair() {
        return $MATERIAL_TYPE_PAIR = array( // CẶP GIÁ TRỊ GAS-VỎ ĐỂ THEO DÕI CÔNG NỢ VỎ KH THEO LOẠI BÌNH
            MATERIAL_TYPE_BINHBO_50=>MATERIAL_TYPE_VO_50,// VT GAS=> VT VỎ 11-12
            MATERIAL_TYPE_BINHBO_45=>MATERIAL_TYPE_VO_45,// 7 - 10
            MATERIAL_TYPE_BINH_12=>MATERIAL_TYPE_VO_12,// 4-1
            MATERIAL_TYPE_BINH_6=>MATERIAL_TYPE_VO_6,// 9- 14
        );
    }
    
    /**
     * @Author: ANH DUNG Feb 06, 2017
     * @Todo: get array userRef của customer
     */
    public static function getUsersRef($aUid) {
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',', $aUid);
        $criteria->addCondition("t.user_id IN ($sParamsIn)");
        $models = UsersRef::model()->findAll($criteria);
        $aRes = [];
        foreach($models as $item){
            $aRes[$item->user_id] = $item;
        }
        return $aRes;
    }
    
    /**
     * @Author: ANH DUNG Feb 06, 2017
     * @Todo: giới hạn điều kiện search KH lại
     */
    public static function getLimitCustomer(&$criteria) {
        $sParamsIn = implode(',', CmsFormatter::$ARR_TYPE_CUSTOMER_STORECARD);
        $w1 = "SubUser.is_maintain IN ($sParamsIn) AND SubUser.type=".CUSTOMER_TYPE_STORE_CARD." AND SubUser.role_id=".ROLE_CUSTOMER;
        $LIMIT = 100;
        $sql = "SELECT id FROM `gas_users` as SubUser WHERE $w1 AND channel_id=".Users::CON_LAY_HANG;
        $criteria->addCondition('t.customer_id IN ('.$sql.')');
        return ;
        $criteria = new CDbCriteria();
        $criteria->compare('role_id', ROLE_CUSTOMER);
        $criteria->compare('type', CUSTOMER_TYPE_STORE_CARD);
        $aTypeBoMoi = array(STORE_CARD_KH_BINH_BO, STORE_CARD_KH_MOI);
        $criteria->addInCondition( 'is_maintain', $aTypeBoMoi); // Aug 19, 2014 chỉ update KH bò mối thôi, vì cái 
        $criteria->compare('channel_id', Users::KHONG_LAY_HANG);
    }
    
    /**
     * @Author: ANH DUNG Feb 18, 2017
     * @Todo: copy email của KH from email sang biến contact
     * @Param: chia làm 2 phần, những KH còn lấy hàng và KH không còn lấy hàng
     */
    public static function setEmailForCustomer() {
        $from = time();
        $criteria = new CDbCriteria();
        self::getCriteriaCustomerStorecard($criteria);
        $criteria->compare('channel_id', Users::CON_LAY_HANG);
        $models = Users::model()->findAll($criteria);
//        echo '<pre>';
//        print_r(count($models));
//        echo '</pre>';
//        die;
        
        foreach($models as $mCustomer){
            $mUsersRef = $mCustomer->rUsersRef;
            if($mUsersRef){
                $mUsersRef->getMapFieldContact();
                $mUsersRef->contact_boss_mail = $mCustomer->email;
                $mUsersRef->setFieldContact();
                $mUsersRef->update(array('contact_person'));
            }
        }
        // Feb 18, 2017 3331 done in:+ 53 Second <=> 0.83 Minutes
        $to = time();
        $second = $to-$from;
        echo $ResultRun = "".count($models).' done in:+ '.($second).'  Second  <=> '.($second/60).' Minutes';
        die;
    }
    
    /**
     * @Author: ANH DUNG Feb 26, 2017
     * @Todo: copy email của KH from contact_person sang biến email để xác định user đó có mail
     */
    public static function setEmailForCustomerV1() {
        $from = time();
        $criteria = new CDbCriteria();
        self::getCriteriaCustomerStorecard($criteria);
        $criteria->compare('channel_id', Users::CON_LAY_HANG);
        $models = Users::model()->findAll($criteria);
//        echo '<pre>';
//        print_r(count($models));;
//        echo '</pre>';
//        die;
        
        foreach($models as $mCustomer){
            $mCustomer->mUsersRef = $mCustomer->rUsersRef;
            if($mCustomer->mUsersRef){
                $mCustomer->mUsersRef->getMapFieldContact();
                if(empty($mCustomer->mUsersRef->contact_boss_mail)){
                   continue ; 
                }
                $mCustomer->setEmail();
                $mCustomer->update(array('email'));
                echo $mCustomer->id.'--- '.$mCustomer->email.' == ';
            }
        }
        // Feb 18, 2017 3331 done in:+ 53 Second <=> 0.83 Minutes
        $to = time();
        $second = $to-$from;
        echo $ResultRun = "".count($models).' done in:+ '.($second).'  Second  <=> '.($second/60).' Minutes';
        die;
    }
    
    /**
     * @Author: ANH DUNG Mar 02, 2017
     * @Todo: resend lại SMS KH anh Hùng mới nhập KH cty Bò
     */
    public static function fixResendSmsCustomer() {
        die('Can review fix lai code');
        $roleCustomer   = ROLE_CUSTOMER;
        $statusLayHang  = Users::CON_LAY_HANG;
        $sParamsIn  = implode(',', CmsFormatter::$aTypeIdQuotes);
        $smsType  = GasScheduleSms::TYPE_QUOTES_BO_MOI;
        $smsMonth = '03';
        $sqlLimit = "SELECT SubQ.user_id FROM `gas_gas_schedule_sms_history` as SubQ WHERE SubQ.type=$smsType AND MONTH(SubQ.time_send)=$smsMonth";
        
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.is_maintain IN ($sParamsIn) AND t.channel_id=$statusLayHang");
        $criteria->addCondition("t.role_id=$roleCustomer AND t.sale_id=49184");
        $criteria->addCondition('t.id  NOT IN ('.$sqlLimit.')');
        $models = Users::model()->findAll($criteria);
        $aUid = CHtml::listData($models, 'id', 'id');

        $sParamsIn  = implode(',', $aUid);
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.user_id IN ($sParamsIn)");
        
        $aModelPhone = UsersPhoneExt2::model()->findAll($criteria);
        
        $mGasScheduleSms = new GasScheduleSms();
        $mGasScheduleSms->doInsertMultiQuotes($aModelPhone);
        
        // Anh Hùng cập nhật 28 số điện thoại của KH bò, sẽ xử lý send lại SMS
        echo '<pre>';
        print_r(count($aModelPhone));
        echo '</pre>';
        die;
    }
    
    /**
     * @Author: ANH DUNG Mar 03, 2017
     * @Todo: change người tạo phần setup giá, để Sale có thể chỉnh được giá
     * vì người tạo cũ đã nghỉ làm nên phải đưa người tạo mới làm
     */
    public static function changeSaleOnSetupPriceNotRun() {
        $sale_id_from  = 906662; //
        $sale_id_to    = 360792; //
        set_time_limit(72000);
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->compare( 'type', UsersPrice::TYPE_BO_MOI);
        $criteria->compare( 'uid_login', $sale_id_from);
        $mRes = UsersPrice::model()->count($criteria);    
//        echo $mRes; die;
        $aUpdate = array('uid_login' => $sale_id_to);
        UsersPrice::model()->updateAll($aUpdate, $criteria);

        $to = time();
        $second = $to-$from;
        echo count($mRes).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;
    }
    
    /**
     * @Author: ANH DUNG Mar 03, 2017
     * @Todo: change người tạo phần setup giá, để Sale có thể chỉnh được giá
     * vì người tạo cũ đã nghỉ làm nên phải đưa người tạo mới làm
     * -- tran binh Trong
SELECT * FROM `gas_users_price` where uid_login=257071 and c_month=3 and c_year=2017 ORDER BY `id` DESC ;
UPDATE `c1gas35`.`gas_users_price` SET `uid_login` = '1011646' WHERE  uid_login=257071 and c_month=3 and c_year=2017;
-- Tran Van Huy
UPDATE `c1gas35`.`gas_users_price` SET `uid_login` = '1011646' WHERE  uid_login=471328 and c_month=3 and c_year=2017; 
     */
    public static function changeSaleOnSetupPriceByAgent($month, $year) {
        $month = $month *1;
//        $sale_id    = 257071; // Trần Bình Trọng
//        $sale_id    = 489412; // Nguyễn Văn Nhật Linh
        $sale_id    = 360792; // Tống Thành Vũ Linh
        $aAgent = [
            652695,
            30751, 
            125798,
            197436,
        ];

        set_time_limit(72000);
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->addCondition('c_month='.$month.' AND c_year='.$year.'');
        $criteria->addCondition('type_customer='.STORE_CARD_KH_MOI);
        $sParamsIn = implode(',', $aAgent);
        $criteria->addCondition("agent_id IN ($sParamsIn)");
        $aUpdate = array('uid_login' => $sale_id);
        UsersPrice::model()->updateAll($aUpdate, $criteria);
            
        $to = time();
        $second = $to-$from;
        echo ' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;
    }
    
    /**
     * @Author: ANH DUNG Mar 03, 2017
     * @Todo: update agent_id của customer phục vụ đổi sale cho KH đó để setup giá
     * vì người tạo cũ đã nghỉ làm nên phải đưa người tạo mới làm
     */
    public static function updateAgentForSetupPrice($month, $year) {
        $month = $month *1;
        set_time_limit(72000);
        $from = time();
//        $criteria = new CDbCriteria();
//        $criteria->addCondition('t.c_month='.$month.' AND t.c_year='.$year.' AND t.type_customer='.STORE_CARD_KH_MOI);
//        $models = UsersPrice::model()->findAll($criteria);
        
        $criteria = new CDbCriteria();
//        $sqlLimit = "SELECT SubQ.customer_id FROM `gas_users_price` as SubQ WHERE SubQ.c_month=$month AND SubQ.c_year=$year AND SubQ.type_customer=".STORE_CARD_KH_MOI;
        $sqlLimit = "SELECT SubQ.customer_id FROM `gas_users_price` as SubQ WHERE SubQ.c_month=$month AND SubQ.c_year=$year AND SubQ.type_customer=".STORE_CARD_KH_BINH_BO;
        $criteria->addCondition('t.id IN ('.$sqlLimit.')');
        $models = Users::model()->findAll($criteria);
        $aDataAgent = [];
        foreach($models as $item){// Format gom các customer về 1 đại lý
            if(empty($item->area_code_id)){
                continue ;
            }
            $aDataAgent[$item->area_code_id][$item->id] = $item->id;
        }
        foreach($aDataAgent as $agent_id => $aCustomerId){
            $criteria = new CDbCriteria();
            $criteria->addCondition('c_month='.$month.' AND c_year='.$year.'');
            $sParamsIn = implode(',', $aCustomerId);
            $criteria->addCondition("customer_id IN ($sParamsIn)");
            $aUpdate = array('agent_id' => $agent_id);
            UsersPrice::model()->updateAll($aUpdate, $criteria);
        }
        
        $to = time();
        $second = $to-$from;
        echo count($mRes).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;
    }
    
    
    /**
     * @Author: ANH DUNG Apr 11, 2017
     * @Todo: set ngưng bảo trì với những KH không lấy hàng nữa
     */
    public static function setStopMaintain() {
        $from = time();
        $criteria = new CDbCriteria();
        self::getCriteriaCustomerStorecard($criteria);
        $aStatusLayHang = [Users::KHONG_LAY_HANG, Users::CHAN_HANG];
        $criteria->addInCondition('channel_id', $aStatusLayHang);
        $models = Users::model()->findAll($criteria);
//        echo '<pre>';
//        print_r(count($models));
//        echo '</pre>';
//        die;
        
        foreach($models as $mCustomer){
            $criteria = new CDbCriteria();
            $criteria->addCondition('type=1 AND customer_id='.$mCustomer->id.'');
            $aUpdate = array('status_uphold_schedule' => GasUphold::STATUS_UPHOLD_OFF);
            GasUphold::model()->updateAll($aUpdate, $criteria);
            
        }
        // Feb 18, 2017 3331 done in:+ 53 Second <=> 0.83 Minutes
        $to = time();
        $second = $to-$from;
        echo $ResultRun = "".count($models).' done in:+ '.($second).'  Second  <=> '.($second/60).' Minutes';
        die;
    }
    
    /**
     * @Author: ANH DUNG May 10, 2017
     * @Todo: xóa sổ quỹ của các đại lý cũ không chạy
     */
    public static function cleanCashbookOld() {
        $from = time();
        $aAgent = [
            1311,// Cửa hàng 1
            1312, // 2
            1313, // 3
            1314, // 4
            28941, // Cửa hàng Thi Sách
            268835, // Cửa hàng Chư Sê 
        ];
        
        $criteria = new CDbCriteria();
        $criteria->addNotInCondition('t.agent_id', $aAgent);
        $models = GasCashBookDetail::model()->findAll($criteria);
        echo '<pre>';
        print_r(count($models));
        echo '</pre>';
        die;
        foreach($models as $model){
            $model->delete();
        }
        $to = time();
        $second = $to-$from;
        echo $ResultRun = "".count($models).' done in:+ '.($second).'  Second  <=> '.($second/60).' Minutes';
        die;
    }
    
    /**
     * @Author: ANH DUNG May 12, 2017
     * @Todo: xóa record sổ quỹ của đại lý để run live đưa quỹ về = 0
SELECT * FROM `gas_gas_cash_book_detail` `t` WHERE (t.agent_id IN (658920,242369,945418,868845,768408,768409,103)) AND (t.release_date < '2017-05-12');
     */
    public static function cleanCashbookRunLive() {
//        return ;// không thể xóa hàm này, sẽ làm sai công nợ KH bò mối. ĐL chưa chạy app thì vẫn xóa bình thường
        $from = time();
        $aAgent = [
//            1127081, // Đại Lý Dương Quảng Hàm
//            919016, // Đại Lý Lê Phụng Hiểu - Jun1318
//            952187, // Đại Lý Bến Nôm  - Jun1318
//            1011084, // Đại Lý Trần Phú  - Jun1318
//            1179069, // Đại Lý Diên Khánh  - Jun2618
//            1205656, // Đại Lý Cam Ranh
            1176208, // Đại Lý Vĩnh Thạnh - Nha Trang
        ];
        
        if(count($aAgent) < 1){
            die('khong co dai ly') ;
        }
        $today = '2018-08-02';
//        $today = date('Y-m-d');
//        $prevDay = MyFormat::modifyDays($today, 1, '-');
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',',  $aAgent);
        $criteria->addCondition("t.agent_id IN ($sParamsIn)");
        $criteria->addCondition("t.release_date < '$today'");
        
        $models = GasCashBookDetail::model()->findAll($criteria);
//        echo '<pre>';
//        print_r(count($models));
//        echo '</pre>';
//        die;
        foreach($models as $model){
            $model->delete();
        }
        $to = time();
        $second = $to-$from;
        echo $ResultRun = "".count($models).' done in:+ '.($second).'  Second  <=> '.($second/60).' Minutes';
        die;
    }
    
    /**
     * @Author: ANH DUNG May 15, 2017
     * @Todo: sua so quy ho GD cua 1 dai ly ở 1 ngày cố định 
     */
    public static function fixCashbook() {
        $mCashbookCheck = new GasCashBookDetail();
        $mCashbookCheck->agent_id           = 658920;// Đại Lý Bình Thạnh 2
        $mCashbookCheck->release_date       = '23-05-2017';
        $model = new GasCashBookDetail();
        $model->makeRecordHgdAllAgent([$mCashbookCheck->agent_id],  $mCashbookCheck->release_date);
        die('ok run');
    }
    // sửa chạy insert lại data sổ quỹ thu bò mối trong 1 khoảng ngày của đại lý
    public static function fixCashbookBoMoiInRangeDate() {
        $mEmployeeCashbook = new EmployeeCashbook();
        $mEmployeeCashbook->date_from  = '2017-05-10';
        $mEmployeeCashbook->date_to    = '2017-05-12';
        $mEmployeeCashbook->agent_id   = 658920; // BT 2
        $mEmployeeCashbook->fixCashbookBoMoiInRangeDate();
        die('done ok');
    }
    
    
    /**
     * @Author: ANH DUNG Jun 03, 2017
     * @Todo: set chặn hàng với KH chưa lấy hàng lần nào
     */
    public static function setCustomerNotBuy() {
        die('need check trc khi run');
        $mUserPrice = new UsersPrice();// May 31, 2017 chạy mail hàng ngày các KH chưa setup giá cho các cấp quản lý
//        $models = $mUserPrice->checkSetupPrice();die;
        $models = $mUserPrice->getBoMoiNotSetPrice();
        $index = 0;
        foreach($models as $model){
            if(is_null($model->last_purchase)){
                $index++;
                $model->channel_id = Users::CHAN_HANG;
                $model->update(['channel_id']);
            }
        }
        echo '<pre>';
        print_r($index);
        echo '</pre>';
        die;
    }
    
    /**
     * @Author: ANH DUNG Jun 05, 2017
     * @Todo: cập nhật biến status_cancel tu ben transaction history sang table Sell
     */
    public static function fixUpdateSellStatusCancel() {
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->compare("t.status", Sell::STATUS_CANCEL);
        $criteria->addCondition("t.created_date_only >'2017-05-01'");
        $models = Sell::model()->findAll($criteria);
        echo '<pre>';
        print_r(count($models));
        echo '</pre>';
        die;
        foreach($models as $mSell){
            $c1= new CDbCriteria();
            $c1->compare('t.sell_id', $mSell->id);
            $mTransactionHistory = TransactionHistory::model()->find($c1);
            if($mTransactionHistory){
                $mSell->status_cancel = $mTransactionHistory->status_cancel;
                $mSell->update(['status_cancel']);
            }
        }
        $to = time();
        $second = $to-$from;
        echo $ResultRun = "".count($models).' done in:+ '.($second).'  Second  <=> '.($second/60).' Minutes';
        die;
    }
    
    /**
     * @Author: ANH DUNG Jun 10, 2017
     * @Todo: Xóa bớt KH 240 và dân dụng không lấy hàng đi 
     */
    public static function removeKHVip240() {
        set_time_limit(300);
        $from = time();
//        $aHgdAllow = array(STORE_CARD_VIP_HGD, STORE_CARD_HGD_CCS);
        for($i=0; $i < 10; $i++){
            $criteria = new CDbCriteria();
            $criteria->compare("t.role_id", ROLE_CUSTOMER);
            $criteria->compare("t.type", CUSTOMER_TYPE_STORE_CARD);
            $criteria->compare("t.is_maintain", STORE_CARD_HGD_CCS);
            $criteria->addCondition("DATE(t.created_date) < '2017-01-01'");
            $criteria->addCondition("t.id NOT IN (SELECT DISTINCT customer_id FROM `gas_sell` as SubReply) ");
            $criteria->limit = 2000;
            $models = Users::model()->findAll($criteria);
    //        echo '<pre>';
    //        print_r(Users::model()->count($criteria));
    //        echo '</pre>';
    //        die;

            foreach($models as $mUser){
                $criteriaSell = new CDbCriteria();
                $criteriaSell->addCondition('t.customer_id='. $mUser->id);
                $countSell = Sell::model()->count($criteriaSell);
                if($countSell < 1 ){
                    $mUser->delete();
                }
            }
            $models = null;
            $criteria = null;
        }
        
        
        
        $to = time();
        $second = $to-$from;
//        echo $ResultRun = "".count($models).' done in:+ '.($second).'  Second  <=> '.($second/60).' Minutes';
        echo $ResultRun = ' done in:+ '.($second).'  Second  <=> '.($second/60).' Minutes';
        die;
    }
    
    /** @Author: ANH DUNG Jun 17, 2017
     * @Todo: cập nhật đổi đầu số cho các khu vực 
     * print_r(substr($oldstr, 0, 4). ' --- ') ; // get 4 character
       print_r(substr($oldstr, 4));// remove 4 characer
     * @step_1: change $prefix_old, $prefix_new
     * @step_2: change call function getCriteriaCustomerStorecard OR getCriteriaCustomerHgd
     * @step_3: change "t.province_id => GasProvince::TINH_CA_MAU
     */
    public static function fixChangePrefixNumber() {
        ini_set('memory_limit','500M');
        $prefix_old = '060'; // TINH_KONTUM
        $prefix_new = '0260';
        $charRemove = strlen($prefix_old); $hasRunUpdateHgd = -1; $totalChange = 0;
        for($i=0; $i<20; $i++):
            
        $criteria = new CDbCriteria();
        self::getCriteriaCustomerStorecard($criteria); $isBomoi = true;
//        self::getCriteriaCustomerHgd($criteria); $isBomoi = false;
        $criteria->addCondition("t.password_hash<>$hasRunUpdateHgd");
        $criteria->addCondition("t.province_id=".GasProvince::TINH_KONTUM);
        $criteria->limit = 1000;
        $models = Users::model()->findAll($criteria);
        $aPhoneChange = []; $aPhoneNot2 = []; $aPhoneNot3 = [];$aUid = [];
        if(count($models) < 1){
            die('done run ok hgd: '.$totalChange);
        }
        
        foreach($models as $mCustomer){
            if(empty($mCustomer->phone)){
                continue;
            }
            $aPhone = explode('-', $mCustomer->phone);
            $aPhoneNew = []; $needUpdate = false;
            foreach($aPhone as $phone){
                // check 3 số đầu của phone
                $prefix_code = substr($phone, 0, 3);
                if(in_array($prefix_code, GasScheduleSms::$aCodePhoneValidThree)){
                    $aPhoneNot3[]       = $phone;
                    $aPhoneNew[$phone]  = $phone;
                    continue;
                }
                // check 2 số đầu của mạng 86 - viettel, 88 - Vinaphone, 89 - MobiFone
                $prefix_code = substr($phone, 0, 2);
                if(in_array($prefix_code, GasScheduleSms::$aCodePhoneValidSecond)){
                    $aPhoneNot2[]       = $phone;
                    $aPhoneNew[$phone]  = $phone;
                    continue;
                }
                
                $prefix_code = substr($phone, 0, $charRemove);
                if($prefix_code == $prefix_old){
//                    $oldstr = $phone;
//                    $str_to_insert = '2';
//                    $pos = 1;
//                    $newPhone = substr_replace($oldstr, $str_to_insert, $pos, 0); // check for phone máy bàn HCM
                    $newPhone = $prefix_new.substr($phone, $charRemove);
                    
                    $aPhoneChange[] = $phone.' -- '.$newPhone;
                    $aPhoneNew[$newPhone]    = $newPhone;
                    $needUpdate = true;
                }else{// để lại số di động vào chuỗi
                    $aPhoneNew[$phone]    = $phone;
                }
            }// end foreach($aPhone as $phone)
            
            
            if($needUpdate){ $totalChange++ ;
                $mCustomer->phone = implode('-', $aPhoneNew);
                $mCustomer->update(['phone']);
                UsersPhone::savePhone($mCustomer);
//                echo $mCustomer->id.' ==> '.$mCustomer->phone.'<br>';
            }
            $aUid[] = $mCustomer->id;// for update all id;
            
        }// end foreach($models as $mCustomer
        $models = null;
        if(count($aUid) && !$isBomoi){
            $criteria123 = new CDbCriteria();
            $sParamsIn = implode(',', $aUid);
            $criteria123->addCondition("id IN ($sParamsIn)");
            $aUpdate = array('password_hash' => $hasRunUpdateHgd);
            Users::model()->updateAll($aUpdate, $criteria123);
        }
        
//        die('done run ok hgd: '.$totalChange);
//        echo '<pre>';
//        print_r($aPhoneNot3);
//        echo '<hr>';
//        print_r($aPhoneNot2);
//        echo '<hr>';
//        print_r($aPhoneChange);
//        echo '</pre>';
//        die;
        endfor;// end for($i=1; $i<5; $i++):
        
        die('done run ok hgd: '.$totalChange);
    }
    
    
    
    
    
    /**
     * @Author: ANH DUNG Jun 17, 2017
     * @Todo: fix copy lại số phone của bò mối vào như cũ. Vì cập nhật sai bị xóa hết số phone
     */
    public static function copyPhoneFix() {
//        die('run fix ok Jun 17, 2017');
        $models = TableTemp::model()->findAll();
        $count = 0;
        foreach($models as $model){
            $mCustomer = Users::model()->findByPk($model->id);
            if($mCustomer && $mCustomer->role_id != ROLE_AGENT){
                $mCustomer->last_purchase = $model->last_purchase;
                $mCustomer->update(['last_purchase']);
                $count++;
            }
        }
        die('done: '.$count);
    }
    
    /** @Author: ANH DUNG Jun 18, 2017
     * @Todo: cập nhật mã cho KH trên hệ thống
     * WITH $is_maintain = 8;
     * run 1: 30000 done in:+ 7177  Second  <=> 119.61 Minutes
     * run 2: 30000 done in:+ 7927  Second  <=> 132.11
     * run 3: 60000 done in:+ 15117  Second  <=> 251.95 Minutes
     * run 4: 29754 done in:+ 8449  Second  <=> 140.81666
     * WITH $is_maintain = 9;
     * 10020 done in:+ 3761  Second  <=> 62.6833
     * 
     */
    public static function fixCodeBussiness() {
//        set_time_limit(1000);
        $from = time();
        try{// $is_maintain = 7,8,9,11 ;
        $is_maintain = 9 ;$count = 0;
        $needMore=array('type'=>CUSTOMER_TYPE_STORE_CARD, 'customer_all_system'=>1);
        for($i=0; $i<30; $i++):
            $criteria = new CDbCriteria();
            $criteria->addCondition("t.is_maintain=$is_maintain AND t.role_id=4 and t.code_bussiness='' " ); 
            $criteria->limit = 1000;
            $models = Users::model()->findAll($criteria);
            foreach($models as $model){ $count++;
                try{
                    $model->code_bussiness = MyFunctionCustom::genCodeBusinessCustomerAgent($model->first_name, $needMore);
                } catch (Exception $ex) {
                    continue;
                }
                $model->update(['code_bussiness', 'address_vi']);
            }
             
            $models = null;
        endfor;
        $to = time();
        $second = $to-$from;
        echo $ResultRun = $count.' done in:+ '.($second).'  Second  <=> '.($second/60).' Minutes '.date('Y-m-d H:i:s');
        die;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Jun 20, 2017
     * @Todo: cập nhật PTTT code
     */
    public static function fixUpdatePtttCode() {
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.pttt_code<>''");
        $models = TransactionHistory::model()->findAll($criteria);
        foreach($models as $mTransaction){
            $mSell = Sell::model()->findByPk($mTransaction->sell_id);
            if($mSell){
                $mSell->pttt_code = $mTransaction->pttt_code;
                $mSell->update(['pttt_code']);
            }
        }
        $to = time();
        $second = $to-$from;
        echo $ResultRun = "".count($models).' done in:+ '.($second).'  Second  <=> '.($second/60).' Minutes';
        die;
    }
    
    /** @Author: ANH DUNG Jun 23, 2017
     * @Todo: fix update employee_maintain_id từ GasAppOrder sang StorecarDetail
     */
    public static function fixEmployeeStorecardDetail() {
        $from = time();
        $status = GasAppOrder::STATUS_COMPPLETE;
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.date_delivery > '2017-05-30' AND t.status=$status" );
        $models = GasAppOrder::model()->findAll($criteria);
        foreach($models as $model){
            $criteria = new CDbCriteria();
            $criteria->addCondition('store_card_id='.$model->store_card_id_gas . ' OR store_card_id='.$model->store_card_id_vo);
            $aUpdate = array('employee_maintain_id' => $model->employee_maintain_id);
            GasStoreCardDetail::model()->updateAll($aUpdate, $criteria);
        }
        $to = time();
        $second = $to-$from;
        echo $ResultRun = "".count($models).' done in:+ '.($second).'  Second  <=> '.($second/60).' Minutes';
        die;
    }
    
    
    public static function fixUsersPriceSaleId() {
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.c_month=12 AND t.c_year=2018" );
        $models = UsersPrice::model()->findAll($criteria);
        echo '<pre>';
        print_r(count($models));
        echo '</pre>';
        die;
        $count = 0;
        foreach($models as $model){
            $mCustomer = $model->rCustomer;
//            if($mCustomer->area_code_id != 572832){
//                continue ;
//            }
            if($mCustomer){
                $model->sale_id = $mCustomer->sale_id;
                $model->agent_id = $mCustomer->area_code_id;
                $model->update(['sale_id','agent_id']);
            }else{
                $count++;
//                $model->delete();
            }
        }
        $to = time();
        $second = $to-$from;
        echo $ResultRun = "$count Null mCustomer => ".count($models).' done in:+ '.($second).'  Second  <=> '.($second/60).' Minutes';
        die;
    }
    
    public static function fixSpjCodeAgent() {
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.status='.SpjCode::STATUS_GO_BACK.' AND t.type='. SpjCode::TYPE_PTTT);
        $models = SpjCode::model()->findAll($criteria);
//        echo '<pre>';
//        print_r(count($models));
//        echo '</pre>';
//        die;
        $count = 0;
        foreach($models as $model){
            $mSell = Sell::model()->findByPk($model->sell_id);
            if($mSell){
                $model->agent_id = $mSell->agent_id;
                $model->update(['agent_id']);
            }else{
                $count++;
//                $model->delete();
            }
        }
        $to = time();
        $second = $to-$from;
        echo $ResultRun = "$count Null mCustomer => ".count($models).' done in:+ '.($second).'  Second  <=> '.($second/60).' Minutes';
        die;
    }
    
    /** @Author: ANH DUNG Aug 19, 2017
     *  @Todo: set required login App 
     */
    public static function setUserRequiredLoginApp() {
        $criteria = new CDbCriteria();
        $criteria->compare("role_id", ROLE_EMPLOYEE_MAINTAIN);
        $criteria->compare("status", STATUS_ACTIVE);
        $models = Users::model()->findAll($criteria);
        foreach($models as $mUser){
            MonitorUpdate::makeRecordParams(0, MonitorUpdate::TYPE_MOBILE_REQUIRED_LOGOUT, $mUser->id, 0, 0, '');
        }
        die;
    }
    /** @Author: ANH DUNG Aug 19, 2017
     *  @Todo: update call id to sell
     */
    public static function updateCallIdToSell() {
        $criteria = new CDbCriteria();
        $criteria->addCondition("sell_id>0 AND created_date_only>'2017-07-31'");
        $models = Call::model()->findAll($criteria);
        foreach($models as $mCall){
            $criteria = new CDbCriteria();
            $criteria->addCondition('id='.$mCall->sell_id);
            $aUpdate = array('call_id' => $mCall->id);
            Sell::model()->updateAll($aUpdate, $criteria);
        }
        die;
    }
    
    /** @Author: ANH DUNG Aug 25, 2017
     *  @Todo: update fix agent id to sell
     */
    public static function updateAgentIdCallId() {
        $criteria = new CDbCriteria();
        $criteria->addCondition("customer_id>0 AND agent_id=0 AND created_date_only>='2017-08-01'");
        $models = Call::model()->findAll($criteria);
//        echo '<pre>';
//        print_r(count($models));
//        echo '</pre>';
//        die;
        foreach($models as $mCall){
            $mCustomer = Users::model()->findByPk($mCall->customer_id);
            if($mCustomer){
                $mCall->agent_id = $mCustomer->area_code_id;
                $mCall->update(['agent_id']);
            }
        }
        echo count($models).' -- done';die;
    }
    
    /** @Author: ANH DUNG Sep0817
     *  @Todo: update fix role_id for active app customer
     */
    public static function updateRoleActiveApp() {
        $criteria = new CDbCriteria();
        $criteria->addCondition("type=".MonitorUpdate::TYPE_ACTIVE_USER);
        $models = MonitorUpdate::model()->findAll($criteria);
//        echo '<pre>';
//        print_r(count($models));
//        echo '</pre>';
//        die;
        foreach($models as $mMonitorUpdate){
            if($mMonitorUpdate->rUser){
                $mMonitorUpdate->status_obj     = $mMonitorUpdate->rUser->role_id; // need save role_id
                $mMonitorUpdate->update(['status_obj']);
            }
        }
        echo count($models).' -- done';die;
    }
    
    /** @Author: ANH DUNG Sep2917
     *  @Todo: update add App order detail
     */
    public static function updateAppOrderDetail() {
        return ;// Sep2917 run ok about 5s for 10 days
        $date_from  = '2017-09-01';
        $date_to    = '2017-09-10';
//        $date_from  = '2017-09-11';
//        $date_to    = '2017-09-20';
//        $date_from  = '2017-09-26';
//        $date_to    = '2017-09-30';
//        $date_from  = '2017-10-01';
//        $date_to    = '2017-10-11';
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition('date_delivery', $date_from, $date_to);
        GasAppOrderCDetailReal::model()->deleteAll($criteria);
        sleep(2);
        
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition('date_delivery', $date_from, $date_to);
        $criteria->addCondition('status='.GasAppOrder::STATUS_COMPPLETE);
        $models = GasAppOrder::model()->findAll($criteria);
        $aRowInsert = [];
        foreach($models as $mAppOrder){
            $mAppOrderDetail = new GasAppOrderCDetailReal();
            $mAppOrderDetail->detailMakeNewBuildSql($aRowInsert, $mAppOrder);
        }
        
        $tableName = GasAppOrderCDetailReal::model()->tableName();
        $sql = "insert into $tableName (app_order_id,
                    status_debit,
                    paid_gas_remain,
                    agent_id,
                    type,
                    type_of_order_car,
                    date_delivery,
                    customer_id,
                    type_customer,
                    sale_id,
                    province_id_agent,
                    status,
                    parent_chain_store,
                    uid_login,
                    uid_login_role,
                    uid_confirm,
                    employee_maintain_id,
                    user_id_executive,
                    driver_id,
                    car_id,
                    reason_false,
                    pay_direct,
                    pay_back,
                    phu_xe_1,
                    phu_xe_2,
                    road_route,
                    pay_back_amount,
                    discount,
                    total_gas,
                    total_gas_du,
                    materials_type_id,
                    materials_id,
                    qty,
                    price,
                    amount,
                    seri,
                    kg_empty,
                    kg_has_gas
                    ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert)>0)
            Yii::app()->db->createCommand($sql)->execute();
        
    }
    
    
    /** @Author: ANH DUNG Oct 20, 2017
     *  @Todo: get số Order app 
     */
    public static function getOrderApp($date) {
        if(empty($date)){
            $date = date('Y-m-d');
        }
        $criteria = new CDbCriteria();
        $criteria->compare('t.created_date_only', $date);
        $criteria->compare('t.source', Sell::SOURCE_APP);
        $criteria->order = 't.id';
        $models = Sell::model()->findAll($criteria);
        $aRes = [];
        foreach($models as $item){
            if(empty($item->customer_id)){
                continue ;
            }
            if(!isset($aRes[$item->customer_id])){
                $aRes['DATA'][$item->customer_id]   = $item;
                $aRes['UID'][$item->customer_id]    = $item->customer_id;
            }
        }
        return $aRes;
    }
    
    public static function renderHtmlTopOrder() {
        $date = date('Y-m-d');
//        $date = date('2017-10-19');
        $mSignup = new AppSignup();
        /* 1. get all record first những KH login trong AppSignup
         * 2. get all record đơn hàng đầu tiên trong model Sell
         * 3. tính khoảng time giữa đơn hàng và login
         */
        $aLogin = $mSignup->getFirstLoginByDate($date);
        $aOrder = UpdateSql::getOrderApp($date);
        $aUid1 = isset($aLogin['UID']) ? $aLogin['UID'] : [];
        $aUid2 = isset($aOrder['UID']) ? $aOrder['UID'] : [];
        $aUid1 = array_merge($aUid1, $aUid2);
        $aTime = $aTimeSort = [];
        $cTime = '2017-10-22 13:10:01';
        foreach($aUid1 as $uid):
            if(!isset($aLogin['DATA'][$uid]) || !isset($aOrder['DATA'][$uid])):
                continue ;
            endif;
            $aTime[$uid]['phone']       = $aLogin['DATA'][$uid]->phone;
            $aTime[$uid]['from']        = $aLogin['DATA'][$uid]->created_date;
            $aTime[$uid]['to']          = $aOrder['DATA'][$uid]->created_date;
//            $aTime[$uid]['duration']    = MyFormat::getSecondTwoDate($aTime[$uid]['from'], $aTime[$uid]['to']);
            $aTime[$uid]['duration']    = MyFormat::getSecondTwoDate($cTime, $aTime[$uid]['to']);
            $aTime[$uid]['text']        = gmdate("i:s", $aTime[$uid]['duration']);
            $aTimeSort[$uid]            = $aTime[$uid]['duration'];
            
        endforeach;
        asort($aTimeSort);
        $str = '<ol>';
        
        foreach($aTimeSort as $uid => $second):
            $phone = $aTime[$uid]['phone'];
            $phone = UsersPhone::formatPhoneView($phone);
            $text  = $aTime[$uid]['text'];
            $str .= "<li>";
                $str .= "<p class='phone'>$phone <span class='TextTime'>$text</span></p>";
            $str .= "</li>";
        endforeach;
        
        $str .= '</ol>';
        return $str;
    }
    
    /**
     * @Author: ANH DUNG Now 03, 2017
     * @Todo: Cập nhật thông tin search của Agent
     */
    public static function updateAddressAgent() {
        $criteria = new CDbCriteria();
        $criteria->compare("t.role_id", ROLE_AGENT);
        $models = Users::model()->findAll($criteria);
        foreach($models as $item){
            $item->update(['address_vi']);
        }
        die;
    }
    
    /** @Author: ANH DUNG Now 12, 2017
     * /var/www/spj.daukhimiennam.com/web/upload/all_file/2017
        zip -r all_file_08.zip 08/
     * root@huongminhgroup:/mnt/volume-android-huongminhgroup/backup/images/2017# wget spj.daukhimiennam.com/upload/all_file/2017/all_file_08.zip
     * wget spj.daukhimiennam.com/upload/all_file/2017/all_file_09.zip
     *  @Todo: xóa file thẻ kho + đơn hàng bò mối của các tháng cũ, để giảm dung lượng ổ đĩa, nên chạy = cron
     * @log: Now1217 đã xóa tháng 06,07 - 2017
     *  07 - 2017 - 3710 removeImageFreeDisk done in: 2 Second
        06 - 2017 - 2924 removeImageFreeDisk done in: 1 Second
     * 	08 - 2017 --> 5737 removeImageFreeDisk done in: 2 Second
     *  09 - 2017 --> 6534 removeImageFreeDisk done in: 3 Second
     **/
    public static function removeImageFreeDisk() {
        $from = time();
        $month  = '10';
        $year   = '2017';
        $typeDelete = [GasFile::TYPE_9_STORECARD, GasFile::TYPE_10_APP_BO_MOI];
        $criteria = new CDbCriteria();
        $criteria->compare('month(t.created_date)', $month);
        $criteria->compare('year(t.created_date)', $year);
        $sParamsIn = implode(',', $typeDelete);
        $criteria->addCondition("t.type IN ($sParamsIn)");
        $models = GasFile::model()->findAll($criteria);
        $total = count($models);
        
        foreach($models as $item){
//            echo "$item->type ** $item->belong_id  ---- ";
            GasFile::RemoveFileOnly($item, 'file_name');
        }
        $to = time();
        $second = $to-$from;
        $info = "$month - $year --> $total removeImageFreeDisk done in: ".($second).'  Second  <=> '.($second/60).' Minutes';
        Logger::WriteLog($info);
        
        die;
    }
    
}
?>
