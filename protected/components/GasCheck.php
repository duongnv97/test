<?php
/**
 * Class for check some role access to action custom
 */
class GasCheck
{
    // SỐ NGÀY CHO PHÉP CẬP NHẬT PTTT
    public static $DAY_UPDATE_PTTT = 3;
    public static $DAY_ALLOW_DELETE = 5;// cái này có thể dùng để check toàn hệ thống
    
    public static $ARR_ROLE_LIMIT_AGENT = array(
        ROLE_ACCOUNTING_AGENT_PRIMARY,
        ROLE_ACCOUNTING_ZONE,
        ROLE_MONITORING,
        ROLE_EMPLOYEE_MARKET_DEVELOPMENT,
        ROLE_MONITORING_MARKET_DEVELOPMENT,
        ROLE_SALE,
        ROLE_MONITOR_AGENT,
//        ROLE_ACCOUNTING, // Dec 29, 2014
        ROLE_EXECUTIVE_OFFICER, // Apr 12, 2017
        ROLE_DLLK_MANAGE, // Dec2518
        ROLE_EMPLOYEE_MAINTAIN,
    );
    
    const SESSION_USER_STORE_CARD = "SESSUSC";// JUL 25, 2016 name of sesion user khi view list store card vì không thể load theo kiểu relation dc, nó sẽ query vào db rất nhiều
    
    const DL_XUONG_DONG_NAI     = 132678; // Xưởng Đồng Nai
    const DL_BINH_THANH_1       = 106;
    const DL_KHO_MINH_HANH      = 700575;
    const DL_KHO_CHO_MOI        = 693615;
    const DL_KHO_VINH_LONG      = 30754;
    const DL_KHO_BEN_CAT        = 26677;
    const KHO_TAN_SON           = 26678; // Kho Tân Sơn Feb 26, 2016
    const KHO_PHUOC_TAN         = 25785; // Kho Phước Tân Feb 26, 2016
    const KHO_LY_CHINH_THANG    = 1171614; // Kho Lý Chính Thắng - Gia Lai Sep1917
    const KHO_90_NCV            = 1175138; // Kho 90 Nguyễn Cửu Vân - Sep2117
    const KHO_86_NCV_DKMN       = 768411; // Công Ty CP Dầu Khí Miền Nam - Sep2217
    const KHO_DAU_KHI_BINH_DINH = 862516; // Dầu Khí Bình Định  - Oct1217
    const KHO_NHA_TRANG         = 1463939; //Kho Nhập Xuất Nha Trang - Sep1318
    const KHO_BINH_THANH        = 118239; // Kho Binh Thanh - Now0818
    const KHO_VUNG_TAU          = 1117652; // Kho Binh Thanh - Apr1319
    const KHO_TRAM_BAC_LIEU     = 1654200; // Trạm Bạc Liêu - Apr0919
    
//    const DL_XUONG_DONG_NAI = 132678; // Xưởng Đồng Nai
    public static $arrAgentInputCar = array( // Jul 28, 2016 danh sách đại lý dc phép nhập car
        GasCheck::DL_XUONG_DONG_NAI,// Xưởng Đồng Nai
        GasCheck::DL_BINH_THANH_1,
        GasCheck::DL_KHO_MINH_HANH,
        GasCheck::DL_KHO_CHO_MOI,
        GasCheck::DL_KHO_VINH_LONG,
        GasCheck::DL_KHO_BEN_CAT,
        GasCheck::KHO_PHUOC_TAN,
        GasCheck::KHO_TRAM_BAC_LIEU,
    ); // những đại lý có thể tạo mới KH thẻ kho

    /** @Author: ANH DUNG Aug 03, 2017
     * @Todo: Các Kho không sinh tự động thẻ kho khi tạo hoặc cập nhật 
     * @note: chỗ này đang sử dụng cho report admin/gasAppOrder/daily
     * không thể close 1 row nào cả, phải tách ra nếu muốn sử dụng mục đích khác
     */
    public static function getAgentNotGentAuto() {
        return array( // Jul 28, 2016 danh sách đại lý dc phép nhập car
            GasCheck::DL_XUONG_DONG_NAI,// Xưởng Đồng Nai
            GasCheck::DL_KHO_MINH_HANH,
            GasCheck::DL_KHO_CHO_MOI,
            GasCheck::DL_KHO_VINH_LONG,
            GasCheck::DL_KHO_BEN_CAT,
            GasCheck::KHO_TAN_SON,
            GasCheck::KHO_PHUOC_TAN,
            GasCheck::KHO_LY_CHINH_THANG,
            GasCheck::KHO_90_NCV,
            GasCheck::KHO_86_NCV_DKMN,
            GasCheck::KHO_DAU_KHI_BINH_DINH,
            MyFormat::KHO_VUNG_TAU,
            GasCheck::KHO_NHA_TRANG,
            GasCheck::KHO_BINH_THANH,
            GasCheck::KHO_TRAM_BAC_LIEU,
        );
    }

    // hiện tại là những đại lý có thể tạo mới KH thẻ kho
        public static $arrAgentAllow = array(
//            100,// Kho Bến Cát
//            26678,// Kho Tân Sơn
//            25785,// Kho Phước Tân
//            106,// Đại lý Bình Thạnh
//            30754,// Kho Vĩnh Long
        ); // những đại lý có thể tạo mới KH thẻ kho
    
    /** ANH DUNG Feb 17, 2014
     * to do: check agent can create new customer store card
     */
    public static function AgentCreateCustomerStoreCard()
    {        
        // Close on Jun 08, 2016
//        if(Yii::app()->user->role_id==ROLE_SUB_USER_AGENT){
//            if(!in_array(MyFormat::getAgentId(), GasCheck::$arrAgentAllow))
//                    return false;
//        }
        return true; // for admin và điều phối create customer store card
    }
    
    /** ANH DUNG Feb 26, 2014
     * to do: check agent can update gas dư
     */
    public static function AgentCanUpdateRemain($model)
    {
        // May 21, 2015 dùng chung ngày update gas dư với thẻ kho
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, Yii::app()->params['storecard_admin_update'], '-');
        if(Yii::app()->user->role_id==ROLE_SUB_USER_AGENT ){
            if(Yii::app()->user->id != $model->uid_login && $model->uid_login!=0){
                return false;
            }
            $dayAllow = date('Y-m-d');
            $dayAllow = MyFormat::modifyDays($dayAllow, GasCashBook::getAgentDaysAllowUpdate(), '-');
//            $dayAllow = MyFormat::modifyDays($dayAllow, Yii::app()->params['gas_remain_agent_update'], '-');
        }
        // May 21, 2015 dùng chung ngày update gas dư với thẻ kho
//        if(MyFormat::getAgentId() != $model->agent_id)
//        if(Yii::app()->user->id != $model->uid_login && $model->uid_login!=0)
//            return false;
        return MyFormat::compareTwoDate($model->created_date, $dayAllow);
    }
    
    /** ANH DUNG May 08, 2014
     * to do: check agent  can update đặt hàng - order
     */
    public static function AgentCanUpdateOrder($model)
    {
        $mAppCache = new AppCache();
        $today = date('Y-m-d');
        $prev_date = MyFormat::modifyDays($today, (GasOrders::$days_allow_update+1), '-');
//        $isValidDateBetween = MyFormat::compareDateBetween($prev_date, $today, $model->date_delivery);
        // $model->date_delivery ngày giao hàng cho phép update, không check ngày tạo
        // cho phép cập nhật đơn hàng có ngày giao là 1 ngày trước
        $valid = MyFormat::compareTwoDate($model->date_delivery, $prev_date);
        $cUid = Yii::app()->user->id ;
        $aIdDieuPhoiBoMoi = $mAppCache->getArrayIdRole(ROLE_DIEU_PHOI);
        if(in_array($cUid, $aIdDieuPhoiBoMoi) 
            && in_array($model->user_id_create, $aIdDieuPhoiBoMoi)
            && $valid
        ){
            return true;
        }
        if($model->user_id_create != $cUid
                || !$valid
        ){
            return false;
        }

        return true;
    }
    
    /** ANH DUNG May 18, 2014
     * to do: check agent can update gas file scan
     */
    public static function AgentCanUpdateFileScan($model)
    {
//        return true;        
//        if(MyFormat::getAgentId() != $model->agent_id)
        if(Yii::app()->user->id != $model->uid_login)
            return false;
        // Dec 15, 2014 bỏ đoạn check này đi, nếu có bán hàng cũng cho upfile excel lên
//        if(count($model->rMaintainSell)) return false;
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, GasFileScan::getDayAllowUpdate(), '-');
        return MyFormat::compareTwoDate($model->created_date, $dayAllow);
    }
    
    /** ANH DUNG May 18, 2014
     * to do: check agent can update gas file scan
     */
    public static function AgentCanUpdateTextFile($model)
    {
        if(Yii::app()->user->role_id != ROLE_ADMIN && Yii::app()->user->id != $model->uid_login){
            return false; // cho phép admin sửa hết, còn những user khác thì ai tạo thì dc sửa của người đó
        }
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, GasText::getDayAllowUpdate(), '-');
        return MyFormat::compareTwoDate($model->created_date, $dayAllow);
    }
    
    public static function AgentCanUpdateMaintainSell($model)
    {
        if(Yii::app()->user->id != $model->user_id_create){
            return false; // cho phép admin sửa hết, còn những user khác thì ai tạo thì dc sửa của người đó
        }
        if(Yii::app()->user->role_id == ROLE_SUB_USER_AGENT && $model->agent_id != MyFormat::getAgentId())
            return false;
        
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, GasMaintainSell::getDayAllowUpdate(), '-');
        return MyFormat::compareTwoDate($model->created_date, $dayAllow);
    }
    
    /** ANH DUNG May 18, 2014
     * to do: check agent can update gas file scan
     */
    public static function AgentCanUpdateMeetingMinutes($model)
    {
        if(Yii::app()->user->role_id != ROLE_ADMIN && Yii::app()->user->id != $model->uid_login){
            return false; // cho phép admin sửa hết, còn những user khác thì ai tạo thì dc sửa của người đó
        }
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, GasMeetingMinutes::getDayAllowUpdate(), '-');
        return MyFormat::compareTwoDate($model->created_date, $dayAllow);
    }
    
    /** ANH DUNG Mar 15, 2015
     * to do: check agent can update gas PTTT daily goback
     */
    public static function AgentCanUpdatePTTTDailyGoback($model)
    {
//        if(Yii::app()->user->role_id != ROLE_ADMIN && Yii::app()->user->id != $model->uid_login){
        if(Yii::app()->user->id != $model->uid_login){
            return false; // cho phép admin sửa hết, còn những user khác thì ai tạo thì dc sửa của người đó
        }
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, GasPtttDailyGoback::getDayAllowUpdate(), '-');
        return MyFormat::compareTwoDate($model->created_date, $dayAllow);
    }
    
    /** ANH DUNG Dec 13, 2014
     * to do: check can update họp pháp lý
     */
    public static function CanUpdateMeetingLaw($model)
    {
        if(Yii::app()->user->role_id != ROLE_ADMIN && Yii::app()->user->id != $model->uid_login){
            return false; // cho phép admin sửa hết, còn những user khác thì ai tạo thì dc sửa của người đó
        }
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, GasMeetingMinutes::getDayAllowUpdate(), '-');
        return MyFormat::compareTwoDate($model->created_date, $dayAllow);
    }
    
    /** ANH DUNG Dec 13, 2014
     * to do: check can update họp pháp lý
     */
    public static function CanUpdateUphold($model)
    {
//        $cRole = Yii::app()->user->role_id;
        if(Yii::app()->user->role_id != ROLE_DIEU_PHOI && Yii::app()->user->id != $model->uid_login){
            return false; // cho phép admin sửa hết, còn những user khác thì ai tạo thì dc sửa của người đó
        }// Jan 22, 2016 cho phép điều phối sửa của nhau
        
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, $model->getDayAllowUpdate(), '-');
        return MyFormat::compareTwoDate($model->created_date, $dayAllow);
    }
    
    /** ANH DUNG Now 02, 2015
     * to do: check can update đại lý đề xuất sửa chữa
     */
    public static function CanUpdateSupportAgent($model)
    {
        if(Yii::app()->user->role_id != ROLE_ADMIN && Yii::app()->user->id != $model->uid_login){
            return false; // cho phép admin sửa hết, còn những user khác thì ai tạo thì dc sửa của người đó
        }elseif($model->status != GasSupportAgent::STATUS_NEW){
            return false;
        }
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, $model->getDayAllowUpdate(), '-');
        return MyFormat::compareTwoDate($model->created_date, $dayAllow);
    }
    
    /** ANH DUNG Dec 13, 2014
     * to do: check can update họp pháp lý
     */
    public static function CanUpdateBreakTask($model)
    {
        if(Yii::app()->user->role_id != ROLE_ADMIN && Yii::app()->user->id != $model->uid_login){
            return false; // cho phép admin sửa hết, còn những user khác thì ai tạo thì dc sửa của người đó
        }
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, GasBreakTask::getDayAllowUpdate(), '-');
        return MyFormat::compareTwoDate($model->created_date, $dayAllow);
    }
    
    /** ANH DUNG Now 23, 2014
     * to do: check head gas bò can update customer check
     */
    public static function CanUpdateCustomerCheck($model)
    {
        $cRole = Yii::app()->user->role_id;
//        if(Yii::app()->user->role_id != ROLE_ADMIN && Yii::app()->user->id != $model->uid_login){
//        if(Yii::app()->user->id != $model->uid_login){// Close Jun 09, 2015
//            return false; // cho phép admin sửa hết, còn những user khác thì ai tạo thì dc sửa của người đó
//        }
        if($cRole==ROLE_SUB_USER_AGENT && $model->status_check == GasCustomerCheck::STATUS_CHECKED ){
            return false; // Nếu kế toán văn phòng đã duyệt thì ko cho đại lý edit nữa
        }
        
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, GasCustomerCheck::getDayAllowUpdate(), '-');
        return MyFormat::compareTwoDate($model->created_date, $dayAllow);
    }
    
    /** ANH DUNG Dec 27, 2014
     * to do: check head gas bò can update gasSupportCustomer
     */
    public static function CanUpdateSupportCustomer($model)
    {
        if($model->CanUpdateTimeDoingReal()){
            return true;// Cho tổ trưởng bảo trì vào update cột ngày giờ thi công
        }
        
        if( !$model->canUpdateGrid() ){
            return false; // ai tạo thì dc sửa của người đó, những sale của KH đó cũng dc update
        }
        if(!empty($model->approved_uid_level_1) || !empty($model->approved_uid_level_2) || !empty($model->approved_uid_level_3) ){
            return false;// nếu đã có 1 cấp trên duyệt thì không cho update nữa
//            return $model->canUpdateToPrint();
        } // close on Sep 04, 2015, không cần tách ra 2 bước update nữa, chỉ cho nhập 1 lần
        
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, GasSupportCustomer::getDayAllowUpdate(), '-');
        return MyFormat::compareTwoDate($model->created_date, $dayAllow);
    }
    
    /** ANH DUNG Now 23, 2014
     * to do: check giám sát đại lý can update report
     */
    public static function CanUpdateCustomerCheckReport($model)
    {
        if(Yii::app()->user->role_id != ROLE_ADMIN && Yii::app()->user->id != $model->uid_login){
//        if(Yii::app()->user->id != $model->monitor_agent_id){
            return false; // cho phép admin sửa hết, còn những user khác thì ai tạo thì dc sửa của người đó
        }
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, GasCustomerCheck::getDayAllowUpdateReport(), '-');
        return MyFormat::compareTwoDate($model->created_date, $dayAllow);
    }
    
    public static function AgentCanUpdateManageTool($model)
    {        
        if(Yii::app()->user->role_id != ROLE_ADMIN && Yii::app()->user->id != $model->uid_login){
            return false; // cho phép admin sửa hết, còn những user khác thì ai tạo thì dc sửa của người đó
        }
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, GasManageTool::getDayAllowUpdate(), '-');
        return MyFormat::compareTwoDate($model->created_date, $dayAllow);
    }
    
    /** ANH DUNG May 18, 2014
     * to do: check agent can update gas profile, cập nhật hồ sơ pháp lý
     */
    public static function AgentCanUpdateProfileScan($model)
    {
        // Dec 04, 2014 tam close doan check nay lai
//        if(Yii::app()->user->role_id != ROLE_ADMIN && Yii::app()->user->id != $model->uid_login){
//            return false; // cho phép admin sửa hết, còn những user khác thì ai tạo thì dc sửa của người đó
//        }
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, GasProfile::getDayAllowUpdate(), '-');
        return MyFormat::compareTwoDate($model->created_date, $dayAllow);
    }
    
    public static function AgentCanDeleteFileScan($model)
    {
//        if(count($model->rMaintainSell)) return false;
        // Close Mar 23, 2015 có bán hàng cũng xóa hết luôn
        return true;
    }
    
    public static function AgentCanUpdateBussinessContract($model)
    {
//        return true;        
        $dayAllow = date('Y-m-d');
        if(Yii::app()->user->id != $model->uid_login)
            return false;
        // Now 18, 2014 với status thương lượng sẽ cho edit dòng mới nhất, còn lại không cho
        if( $model->status == GasBussinessContract::STATUS_THUONG_LUONG )
        {
            $session=Yii::app()->session;
            if(isset($session['ARR_LAST_ID_THUONG_LUONG'][$model->belong_to_id])){
                if( $session['ARR_LAST_ID_THUONG_LUONG'][$model->belong_to_id] == $model->id )
                {
//                    return true;// Close Jun 13, 2015 nên đóng lại, vì chỉ phục vụ sửa tạm thời tại 1 thời điểm
                }
            }
//            return false;// Close Jun 20, 2015 dong theo cai Jun 13, 2015, vi quen dong dong nay
        } // chỗ này có thể đóng lại sau khi sửa xong, đóng cả hàm init session ở index của Spancop
        // Now 18, 2014 với status thương lượng sẽ cho edit dòng mới nhất, còn lại không cho
        
        // Dec 13, 2014 với những STATUS_DATA thì cho edit không giới hạn
        if( in_array($model->status, $model->STATUS_ALLOW_UPDATE) )
        {
            return true;
        }
        // Dec 13, 2014 với những STATUS_DATA thì cho edit không giới hạn
        
        // Xử lý chỗ cập nhật row thương lượng của tuần tiếp theo
        // chỉ cho update row này khi có ngày hiện tại >= date_load, vì nó liên quan đến phần auto sinh row thương lượng cho tuần kê tiếp
//        $DateAdd = MyFormat::modifyDays($dayAllow, 1, '+');
//        $AllowUpdate = MyFormat::compareTwoDate($DateAdd, $model->date_load);
//        if($model->status==GasBussinessContract::STATUS_THUONG_LUONG && $model->row_auto==1)
//        {
//            if(!$AllowUpdate || $model->still_thuong_luong==1)
//                return false;
//        }
        // end Xử lý chỗ cập nhật row thương lượng của tuần tiếp theo
        
        // kiểm tra, nếu model nào dc load lên trong tuần hiện tại thì cho update 
        $session=Yii::app()->session;
        if(!isset($session['MODEL_CURRENT_WEEK_BC'])){
            $session['MODEL_CURRENT_WEEK_BC'] = GasWeekSetup::GetModelCurrentWeekNumber();
        }
        if($session['MODEL_CURRENT_WEEK_BC']){
            // scenario date_from < today < date_to
            $NewDateFrom = MyFormat::modifyDays($session['MODEL_CURRENT_WEEK_BC']->date_from, 1, '-');
            $NewDateTo = MyFormat::modifyDays($session['MODEL_CURRENT_WEEK_BC']->date_to, 1, '+');
            $BigThanDateFrom = MyFormat::compareTwoDate($model->date_load, $NewDateFrom);
            $SmallThanDateTo = MyFormat::compareTwoDate($NewDateTo, $model->date_load);
            return $BigThanDateFrom && $SmallThanDateTo;
        }
        // kiểm tra, nếu model nào dc load lên trong tuần hiện tại thì cho update 
        return false;
        // không check đoạn dưới này nữa
        $dayAllow = MyFormat::modifyDays($dayAllow, GasBussinessContract::getDayAllowUpdate(), '-');
        return MyFormat::compareTwoDate($model->created_date, $dayAllow);        
    }   
    
    /** ANH DUNG May 18, 2014
     * to do: check agent can update gas file scan
     */
    public static function AgentCanUpdateSalesFileScan($model)
    {
        if(Yii::app()->user->id != $model->uid_login)
            return false;
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, GasSalesFileScan::getDayAllowUpdate(), '-');
        return MyFormat::compareTwoDate($model->created_date, $dayAllow);
    }    
    
    /** ANH DUNG May 18, 2014
     * to do: check agent can update gas RemainExport
     */
    public static function AgentCanUpdateRemainExport($model)
    {
        if(Yii::app()->user->id != $model->uid_login)
            return false;
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, GasRemainExport::getDayAllowUpdate(), '-');
        return MyFormat::compareTwoDate($model->created_date, $dayAllow);
    }
    
    /** ANH DUNG Sep 27, 2014
     * to do: check agent can update nghi phep
     */
    public static function AgentCanUpdateLeaveUser($model)
    {
        if(Yii::app()->user->role_id != ROLE_ADMIN && Yii::app()->user->id != $model->uid_login){
            return false; // cho phép admin sửa hết, còn những user khác thì ai tạo thì dc sửa của người đó
        }
        
        // nếu đã được duyệt bởi quản lý thì sẽ không cho phép sửa nữa
//        if( ($model->status == GasLeave::STA_APPROVED_BY_MANAGE && $model->need_manage_approved==1) 
//                || $model->status == GasLeave::STA_APPROVED_BY_DIRECTOR
//                || $model->status == GasLeave::STA_REJECT
//                ){ // close on Jun 23, 2015
        if( !empty($model->manage_approved_uid)
                || !empty($model->approved_director_id)
                ){
            
            return false;
        }
        
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, GasLeave::getDayAllowUpdate(), '-');
        return MyFormat::compareTwoDate($model->created_date, $dayAllow);
    }    
    
    
    /**
     * @Author: ANH DUNG Apr 19, 2014
     * @Todo: check khi login sẽ đẩy user (ROLE_SUB_USER_AGENT) ra nếu chưa được set thuộc đại lý nào
     */    
    public static function checkSubUserAgent(){
        $ok = true;
        $cRole = MyFormat::getCurrentRoleId();
        if(isset(Yii::app()->user->id) && in_array($cRole, GasSocketNotify::getRoleUpdateNotify()) && empty(Yii::app()->user->parent_id)){
            $ok=false;
        }
        
        if( $cRole == ROLE_AGENT ){
            $ok=false;
        }
        
        if(!$ok){
            $session=Yii::app()->session;
            $session['TEXT_ACCESS_NOT_ALLOW'] = "Tài khoản chưa thuộc đại lý nào. Vui lòng liên hệ người quản trị để tiếp tục.";
            Yii::app()->controller->redirect(Yii::app()->createAbsoluteUrl('site/underConstruction'));
        }
    }
    
    
    /********** Apr 19, 2014 BEGIN FOR UPDATE MULTI USER AGENT ****************/
    /**
     * @Author: ANH DUNG Apr 19, 2014
     * @Todo: tự động sinh ra user sub agent với 1 agent tương ứng.
     * dùng tạm khi bắt đầu chuyển qua agent login multi user, có thể tạo = tay trên giao diện
     * nhưng mà tạo thì lâu, nên viết code gen ra cho nhanh
     */    
    public static function AddSubUserAgent(){
        $criteria = new CDbCriteria();
        $criteria->compare('t.role_id', ROLE_AGENT);
        $mRes = Users::model()->findAll($criteria);
        $count=0;
        foreach($mRes as $item){
            $model = new Users();
            $model->parent_id = $item->id;
            $model->username = $item->username."_sub";
            $model->password_hash = $item->password_hash;
            $model->temp_password = $item->temp_password;
            $model->first_name = $item->first_name;
            $model->code_account = $item->code_account."_sub"; //LONG001_sub_30754
            $model->code_bussiness = $item->code_bussiness."_sub_$item->id";
            $model->role_id = ROLE_SUB_USER_AGENT;
            $model->application_id = $item->application_id;
            $model->status = $item->status;
            $model->save();
            $count++;
        }
        echo $count;die;
    }
    
    // Xóa những ROLE_SUB_USER_AGENT test
    public static function DeleteSubUserAgent(){
        $criteria = new CDbCriteria();
        $criteria->compare('role_id', ROLE_SUB_USER_AGENT);
        $mRes = Users::model()->findAll($criteria);
        echo count($mRes);die;
        Users::model()->deleteAll($criteria);
    }
    
    /**
     * @Author: ANH DUNG Apr 19, 2014
     * @Todo: change all username of current agent, về sau đại lý sẽ không login dc mà chỉ có 
     * sub user đại lý mới login
     */    
    public static function ChangeUsernameAgent(){
        $criteria = new CDbCriteria();
        $criteria->compare('t.role_id', ROLE_AGENT);
        $mRes = Users::model()->findAll($criteria);
        $count=0;
        foreach($mRes as $item){
            $item->username = $item->username."_not_login";
            $item->update(array('username'));
            $count++;
        }
        echo $count;die;
    }    
    
    public static function UpdateUsernameSubAgent(){
        $criteria = new CDbCriteria();
        $criteria->compare('t.role_id', ROLE_SUB_USER_AGENT);
        $mRes = Users::model()->findAll($criteria);
        $count=0;
        foreach($mRes as $item){
            $item->username = str_replace('_sub', '', $item->username);
            $item->update(array('username'));
            $count++;
        }
        echo $count;die;
    } 
    
    /********** Apr 19, 2014 END FOR UPDATE MULTI USER AGENT ****************/

    // dùng để check xem 1 user có dc login tiếp hay không Aspr 19, 2014
    public static function checkLogoutUser($mUser){
        $session=Yii::app()->session;
//        $mUser = Users::model()->findByPk(Yii::app()->user->id);// Close Jan 28, 2015
        if(is_null($mUser)
                    || ( isset($session['CURRENT_SESSION_USER']) 
                    && $mUser->verify_code != $session['CURRENT_SESSION_USER'] 
                    && $mUser->role_id!=ROLE_ADMIN )
                ){
            GasTrackLogin::SaveTrackLogin(GasTrackLogin::TYPE_TWO_LOGIN_SAME_TIME); // Aug 22, 2014
            Yii::app()->user->logout();
            $RE_LOGIN_USER = "Tài khoản của bạn đã được đăng nhập ở một máy tính khác. Vui lòng đăng nhập lại";
//            $RE_LOGIN_USER = "Tài khoản của bạn đã bị block";
//            $RE_LOGIN_USER = "Hệ thống cập nhật, vui lòng đăng nhập lại";
            Yii::app()->controller->redirect(Yii::app()->createAbsoluteUrl('admin/site/login', array('RE_LOGIN_USER'=>$RE_LOGIN_USER)));
        }
    }
    
    
    /**
     * @Author: ANH DUNG Apr 27, 2014
     * @Todo: check allow access to controller and action
     * @param string  $controllerName ex: rolesAuth
     * @param string  $action : Group, CommissionConsultant
     * @return: true if allow, false if not allow
     * SpaCheck::isAllowAccess('employees', 'Index');
     */
    public static function isAllowAccess($controllerName, $action){
        $aActionAllowed = ActionsUsers::getActionArrayAllowForCurrentUserByControllerName($controllerName);
        $aActionAllowed = array_map('strtolower',$aActionAllowed);
        return in_array(strtolower($action), $aActionAllowed);         
    }
    
    /** ANH DUNG May 20, 2014
     * to do: check điều xe can update car number không
     */
    public static function allowUpdateCarNumber($model)
    {
        $today = date('Y-m-d');
//        $today = MyFormat::modifyDays($today, GasOrders::$days_allow_update, '-');
        $today = MyFormat::modifyDays($today, Yii::app()->params['days_update_support_agent'], '-'); // ngày giao phải lớn hơn ngày hiện tại,
//        $today = MyFormat::modifyDays($today, 15, '-'); // ngày giao phải lớn hơn ngày hiện tại,
        // nếu = thì hàm compareTwoDate sẽ trả về false thì sai, nên ta trừ đi 1 ngày nữa cho $today,
        // để dữ liệu date_delivery là ngày hôm nay thì hợp lên cho cập nhật số xe 
        $valid =  MyFormat::compareTwoDate($model->date_delivery, $today);
        if( ( Yii::app()->user->role_id!=ROLE_ADMIN && $model->user_id_executive!=Yii::app()->user->id )
                || !$valid
        ){
            return false;
        }
        if(count($model->rOrderDetail)<1) { // nếu không có detail thì ko cho cập nhật
            return false;
        }
        return true;
    }    
    
    /** ANH DUNG May 21, 2014
     * to do: check agent can update bán hàng PTTT
     */
    public static function AgentCanUpdatePTTTSell($model)
    {
        $today = date('Y-m-d');
        $prev_date = MyFormat::modifyDays($today, (GasOrders::$days_allow_update+1), '-');
//        $isValidDateBetween = MyFormat::compareDateBetween($prev_date, $today, $model->date_delivery);
        // $model->date_delivery ngày giao hàng cho phép update, không check ngày tạo
        // cho phép cập nhật đơn hàng có ngày giao là 1 ngày trước
        $valid = MyFormat::compareTwoDate($model->date_delivery, $prev_date);
        if($model->user_id_create!=Yii::app()->user->id 
                || !$valid
        )
            return false;
        return true;
    }    
    
    // chỉ cho phép Delete record dc tạo trong ngày, true if dc tao trong ngày, false nếu tạo hôm trc
    // chắc dùng check cho toàn hệ thống
    public static function canDeleteData($model){
//        return true;
        $cRole = MyFormat::getCurrentRoleId();
        if(Yii::app()->params['enable_delete'] == 'no' || $cRole != ROLE_ADMIN){
            return false;
        }
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, Yii::app()->params['delete_global_days'], '-');
        return MyFormat::compareTwoDate($model->created_date, $dayAllow);
    }
    
    /**
     * @Author: ANH DUNG Nov 22, 2016
     * @Todo: fix can delete 
     */
    public static function canDeleteDataGlobal(){
        $cRole = MyFormat::getCurrentRoleId();
        if(Yii::app()->params['enable_delete'] == 'no' || $cRole != ROLE_ADMIN){
            return false;
        }
        return true;
    }
    
    /**
     * @Author: ANH DUNG Aug 27, 2014
     * @Todo: không cho ip nước ngoài access
     */
    public static function CheckIpOtherCountry(){
        try {
            $session=Yii::app()->session;
            if(!isset($session['IP_VIET_NAM_LOGIN'])){
                if(isset($_COOKIE[VERZ_COOKIE_ADMIN])){
                    return ;// Now 08, 2016 cho phép login = cookie ko check ip
                }

                $ip_address = MyFormat::getIpUser();
                $country = '';
                if( $ip_address != '::1' && $ip_address != '127.0.0.1'){
                    $location = Yii::app()->geoip->lookupLocation($ip_address);
                    if(!is_null($location)){
                        $country =  $location->countryName;
//                        if(!empty($country) && strtolower($country) == 'vietnam'){ // nếu vn thì khởi tạo session
                        if(empty($country) || strtolower($country) == 'vietnam'){ // Jan0718 mở cho mạng 3G access => empty($country)
                            $session['IP_VIET_NAM_LOGIN'] = 1;
                        }else{
                            $description =  $location->region." - $location->regionName - $location->city - PostalCode: $location->postalCode";
        //                    Yii::log("COUNTRY: $country cố  gắng truy cập. $description ", 'error');
                            die;
                        }
                    }// end if(!is_null($location)){
                    else{
                        die;
                    }
                }
            }
        } catch (Exception $exc) {
            throw new Exception($exc->getMessage());
        }
    }
    
    /**
     * @Author: ANH DUNG Aug 29, 2014
     * @Todo: không cho ip nước ngoài access kiểm tra ở site ngoài cùng
     */
    public static function CheckIpOtherCountryV2FeOnlyDie($needMore=array()){
        $ip_address = MyFormat::getIpUser();
        $country = '';
        if( $ip_address != '::1' && $ip_address != '127.0.0.1'){
            $location = Yii::app()->geoip->lookupLocation($ip_address);
            $country =  $location->countryName;
            if(!is_null($location)){
                if(!empty($country) && strtolower($country) == 'vietnam'){ // nếu vn thì khởi tạo session                
                }else{
                    $catchFromAdmin = '';
                    if(isset($needMore['catchFromAdmin']))
                    {
                        $catchFromAdmin = 'catchFromAdmin ** ';
                    }
                    $description =  $catchFromAdmin.$location->region." - $location->regionName - $location->city - PostalCode: $location->postalCode";
                    Yii::log("COUNTRY AT Site Ngoài Cùng: $country cố  gắng truy cập. $description ", 'error');
                    die;
                }
            }// end if(!is_null($location)){
            else{
                die;
            }
        }
    }

    /**
     * @Author: ANH DUNG Sep 28, 2014
     * @Todo: catch all exception at controller - module admin
     * @Param: $model
     */
    public static function CatchAllExeptiong($exc) {
        $cUid = isset(Yii::app()->user) ? Yii::app()->user->id :"";
        $ResultRun = "Uid: $cUid Exception ".  $exc->getMessage();
        Logger::WriteLog($ResultRun, 'error', 'application');
        $code = 404;
        if(isset($exc->statusCode))
            $code=$exc->statusCode;
        if($exc->getCode())
            $code=$exc->getCode();
        throw new CHttpException($code, $exc->getMessage());
    }

    /**
     * @Author: ANH DUNG Oct 09, 2015
     * @Todo: App get name country
     * vẫn là kiểm tra chỉ cho ip của việt nam truy cập
     * !important nhớ ko lại quên
     */
    public static function ApiGetCountryUser($ip_address) {
        $country = '';
        if( $ip_address != '::1' && $ip_address != '127.0.0.1'){
            $location = Yii::app()->geoip->lookupLocation($ip_address);
            if(!is_null($location)){
                $country =  $location->countryName;
                $country =  $country." - ".$location->region." - $location->regionName - $location->city - PostalCode: $location->postalCode";
                if(!empty($country) && strtolower($country) == 'vietnam'){ // nếu vn thì khởi tạo session
//                    $country =  $country." - ".$location->region." - $location->regionName - $location->city - PostalCode: $location->postalCode";
                }else{
                    $result = ApiModule::$defaultSuccessResponse;
//                    ApiModule::sendResponse($result);
                }
            }// end if(!is_null($location)){
            else{
                $result = ApiModule::$defaultSuccessResponse;
//                ApiModule::sendResponse($result);
            }
        }
        return $country;
    }

    /**
     * @Author: ANH DUNG Now 29, 2016
     * @Todo: catch all exception request ajax json
     * @Param: $model
     */
    public static function CatchAllExceptionJson($exc) {
        $cUid = isset(Yii::app()->user) ? Yii::app()->user->id :"";
        $ResultRun = "Uid: $cUid Exception ".  $exc->getMessage();
        Logger::WriteLog($ResultRun, 'error', 'application');
//        $json = CJavaScript::jsonEncode(array('success'=>false,'msg'=>'invAlid request'.$exc->getMessage()));
        $json = CJavaScript::jsonEncode(array('success'=>false,'msg'=>'invAlid request'));
        echo $json;die;
    }
    
    /**
     * @Author: ANH DUNG Sep 28, 2014
     * @Todo: catch all exception at controller - module admin
     * @Param: $model
     */
    public static function CanUpdateGuideHelpSpancop($model)
    {
        if( empty($model->guide_help_date) ){
            return true; // nếu chưa cập nhật lần nào thì OK, cho phép cập nhật
        }
//        if( !empty($model->guide_help_uid) && Yii::app()->user->id != $model->guide_help_uid && Yii::app()->user->role_id != ROLE_ADMIN )
        if( !empty($model->guide_help_uid) && Yii::app()->user->id != $model->guide_help_uid )
            return false; // không cho người khác cập nhật, người nào tạo thì người đó cập nhật, có thể cho admin cập nhật thì mở đoạn trên ra        
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, GasBussinessContract::getDayAllowUpdateGuideHelp(), '-');
        return MyFormat::compareTwoDate($model->guide_help_date, $dayAllow);
    }
    
    /**
     * @Author: ANH DUNG Nov 26, 2014
     * @Todo: kiểm tra có cho admin login không
     */
    public static function CheckAllowAdmin() {
        if( Yii::app()->user->role_id == ROLE_ADMIN ){
            if( Yii::app()->params['allow_admin_login'] == "no" ){
                Yii::log("HACK KEYLOG DETECT - ADMIN bị mất pass, và user đã thử login ", 'error');
                GasCheck::LogoutUser();// will redirect to login page
            }
        }
    }
    
    /**
     * @Author: ANH DUNG Dec 13, 2014
     * @Todo: kiểm tra có cho admin login = cookie không
     * // sẽ cho 1 user chỉ có quyền được cập nhật cái này
     */
    public static function CheckAllowAdminCookie() {
        if( Yii::app()->params['allow_use_admin_cookie'] == "no" ){
            $data = json_decode($_COOKIE[VERZ_COOKIE_ADMIN],true);
            $info = "INFO Username: ".$data[VERZLOGIN]." Pass: ".$data[VERZLPASS];
            Yii::log("HACK KEYLOG DETECT - ADMIN bị mất COOKIE, và user đã thử login = COOKIE $info", 'error');
            echo $msg = "Can not login with cookie"; die;
            // không thể đưa redirect vào page login dc sẽ bị treo vì lặp vô hạn
//            Yii::app()->controller->redirect(Yii::app()->createAbsoluteUrl('admin/site/login', array('RE_LOGIN_USER'=>$msg)));
        }
    }
    
    /**
     * @Author: ANH DUNG Nov 19, 2014
     * @Todo: something
     * @Param: $model
     */
    public static function LogoutUser() {
        GasCheck::setCallCenterExtLogout();
        $session=Yii::app()->session;
        if(isset($session['CURRENT_SESSION_USER'])){
            UsersTokens::logout($session['CURRENT_SESSION_USER']);
        }
        if(isset($session['CUSTOMER_OF_AGENT'])){
            unset($session['CUSTOMER_OF_AGENT']);
        }
        Yii::app()->user->logout();
        //xoa cookie
        if(isset($_COOKIE[VERZ_COOKIE_ADMIN])){
            setcookie(VERZ_COOKIE_ADMIN, '', 1);
            setcookie(VERZ_COOKIE_ADMIN, '', 1, '/');
        }
        Yii::app()->controller->redirect(Yii::app()->createAbsoluteUrl('admin/login/'));
    }
    
    /**
     * @Author: ANH DUNG Jan 02, 2014
     * @Todo: chekc 
     * @Param: $model
     */
    public static function FunctionMaintenance() {
        Yii::app()->controller->redirect( Yii::app()->createAbsoluteUrl('admin/site/maintenance') );
    }
    
    /**
     * @Author: ANH DUNG Aug 03, 2015
     * @Todo: something
     * @Param: $model
     */
    public static function getCurl() {
        return "http://".$_SERVER['HTTP_HOST'].Yii::app()->request->requestUri;
    }
    
    /**
    * @Author: ANH DUNG Apr 24, 2015
    * @Todo: replace link cms to live site
    * GasCheck::ReplaceCmsLinkLiveSite();die;
    */
    public static function ReplaceCmsLinkLiveSite(){
        set_time_limit(7200);
        $find = 'daukhimiennam.com';
        $replace = 'spj.daukhimiennam.com';
//        $replace = 'localhost/verz/ownhome';
        $models = Cms::model()->findAll(); 
        foreach($models as $item){
            $item->cms_content = str_replace($find, $replace, $item->cms_content);
            $item->update(array('cms_content'));
        }
        
        echo 'Done: '.count($models);die;
    }
    
    /**
     * @Author: ANH DUNG Jul 05, 2016
     * @Todo: check current is localhost OR no
     */
    public static function isLocalhost() {
        if($_SERVER['HTTP_HOST'] == 'localhost'){
            return true;
        }
        return false;
    }
    public static function isServerLive() {
        if(in_array($_SERVER['HTTP_HOST'], SyncData::$aLiveUrl)){
            return true;
        }
        return false;
    }
    public static function isServerAndroid() {
        if($_SERVER['HTTP_HOST'] == 'dev.spj.vn'){
            return true;
        }
        return false;
    }
    public static function randomSleep() {
        usleep(rand(100000, 2000000));// 0.5->2s nên test kỹ lại với 3 đến 4 máy cùng 1 user submit 1 lúc random sleep khoảng 0.5s -> 3 second 
    }
    
    /**
     * @Author: ANH DUNG Jul 25, 2016
     * @Todo: khởi tạo sesion cho user 
     * @Param: $nameSession name of session
     * @Param: $aRole array role user
     */
    public static function sessionUserStart($nameSession, $aRole) {
        $session=Yii::app()->session;
        if(!isset($session[$nameSession])){
            $session[$nameSession] = Users::getArrDropdown($aRole);
        }
    }
    
    /**
     * @Author: ANH DUNG Jul 25, 2016
     * @Todo: get name user from sesion 
     * @Param: $nameSession name of session
     * @Param: $uid user id
     */
    public static function sessionUserGetName($nameSession, $uid) {
        $session=Yii::app()->session;
        return isset($session[$nameSession][$uid]) ? $session[$nameSession][$uid] : "";
    }
    
    
    /** @Author: ANH DUNG May 22, 2018
    *  @Todo: get array id user required logout EXT
    *  khong cho phep 1 user nao do login may o nha nua
    **/
    public function getListUserIdLock() {
        return [117079];
    }

    public function checkUserIdLock($ext) {
        $cUid = MyFormat::getCurrentUid();
        if(in_array($cUid, $this->getListUserIdLock())){
            $mCall = new Call();
            if(in_array($ext, $mCall->getListExtHomeSetup())){
                CacheSession::setCookie(CacheSession::CURRENT_USER_EXT, '');
                Yii::log("HOME SETUP LOGOUT $cUid CallCenter EXT $ext", 'error');
                GasCheck::LogoutUser();// will redirect to login page
            }
        }
    }

    
    
    /** @Author: ANH DUNG Mar 12, 2017
    * @Todo: set cache EXT for Employee 
    */
    public function setCallCenterExt($cRole) {
        $aRoleCallCenter    = [ROLE_TELESALE, ROLE_CALL_CENTER, ROLE_DIEU_PHOI];
        // Aug0918 xử lý 1 role limit login phải thêm ở 2 biến: $aRoleLimit, $aRoleLimitLogin
        $aRoleLimit         = [ROLE_BRANCH_DIRECTOR, ROLE_HEAD_OF_LEGAL, ROLE_AUDIT, ROLE_TEST_CALLL, ROLE_IT_EMPLOYEE, ROLE_TELESALE, ROLE_CALL_CENTER, ROLE_DIEU_PHOI, ROLE_MONITORING, ROLE_ACCOUNTING, ROLE_ACCOUNTING_ZONE, ROLE_RECEPTION, ROLE_EMPLOYEE_OF_LEGAL, ROLE_DIRECTOR_BUSSINESS, ROLE_HEAD_GAS_BO, ROLE_SALE, ROLE_CASHIER, ROLE_SALE_ADMIN];
        $aRoleLimitLogin    = [ROLE_BRANCH_DIRECTOR, ROLE_HEAD_OF_LEGAL, ROLE_AUDIT, ROLE_TEST_CALLL, ROLE_IT_EMPLOYEE, ROLE_MONITORING, ROLE_ACCOUNTING, ROLE_ACCOUNTING_ZONE, ROLE_RECEPTION, ROLE_EMPLOYEE_OF_LEGAL, ROLE_DIRECTOR_BUSSINESS, ROLE_HEAD_GAS_BO, ROLE_SALE, ROLE_CASHIER, ROLE_SALE_ADMIN];
        $ext                = CacheSession::getCookie(CacheSession::CURRENT_USER_EXT);
        $mCall              = new Call();
        $mAppCache          = new AppCache();
        if(!empty($ext) && in_array($cRole, $aRoleCallCenter)){// cập nhật User nào đang ngồi ở EXT nào vào cache
            $this->checkUserIdLock($ext);
//            usleep(rand(100000, 3000000));// Sep0319 DungNT close - old => 0.5->3s nên test kỹ lại với 3 đến 4 máy cùng 1 user submit 1 lúc random sleep khoảng 0.5s -> 3 second 
            if(in_array($ext, $mCall->getListExtHgd()) || in_array($ext, $mCall->getListExtBoMoi())){
                $mAppCache->setCallCenterExt($ext, MyFormat::getCurrentUid());
            }
        }elseif(in_array($cRole, $aRoleLimit) && empty($ext)){
            if(Yii::app()->params['EnableChangeExt'] == 'no'):
                Yii::log(MyFormat::getCurrentUid()." CallCenter cố gắng login ở 1 máy không cho phép", 'error');
                GasCheck::LogoutUser();// will redirect to login page
            endif;
        }
        if(Yii::app()->params['EnableChangeExt'] == 'yes' && in_array($cRole, $aRoleLimitLogin)):// Sep0217 xử lý limit login by role
            $cUid = 'uid_'.MyFormat::getCurrentUid();
            CacheSession::setCookie(CacheSession::CURRENT_USER_EXT, $cUid);
        endif;
    }
    
    public static function setCallCenterExtLogout() {
        $cRole          = MyFormat::getCurrentRoleId();
        $mAppCache      = new AppCache();
        $aCurrentExt    = $mAppCache->getCacheDecode(AppCache::EXT_EMPLOYEE);
        if(in_array($cRole, Call::getArrayRoleExt()) && is_array($aCurrentExt)){
            $cUid = MyFormat::getCurrentUid();
            foreach($aCurrentExt as $key => $user_id){
                if($cUid == $user_id){
                    unset($aCurrentExt[$key]);
                }
            }
            $mAppCache->setCallCenterExtAll($aCurrentExt);
        }
    }
    
    /** NguyenPT
     * Check if a user is admin user.
     * @return True if role id belong role restrict list, False otherwise
     */
    public static function isUserAdmin() {
        $cRole = MyFormat::getCurrentRoleId();
        return in_array($cRole, Roles::$aRoleRestrict);
    }


}
