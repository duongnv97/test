<?php
/**
 * @Author: ANH DUNG Dec 05, 2015
 * @Todo: dùng để cut data của năm cũ, không quan tâm ràng buộc dữ liệu
 * hệ thống sẽ giữ lại data của 1 năm hiện tại (2015), vd hôm nay là 05-12-2015 sẽ cut data của năm 2014
 * phần cut data dùng php xử lý, còn phần restore data thì chắc là phải sử dụng sql để import lại
 * vì chỉ cut những table lớn, những table nhỏ sẽ để nguyên
 * @Description_cut_table Dec 02, 2015 hiện tại có 4,824,101 Row: Sql khoảng 650 Mb, database index là 1.8 Gb
 * 1.   gas_users
 * 2	gas_gas_file_scan
 * 3  gas_gas_file_scan_detail
 * 4 	gas_gas_file_scan_info
 * 5.	gas_gas_maintain
 * 6.	gas_gas_maintain_sell
 * 7.   gas_gas_remain
 * 8.	gas_gas_store_card
 * 9.	gas_gas_store_card_detail
 * 10.
 * @Step: lần 1 (Dec 05, 2015) chạy gas_gas_store_card_detail bị lỗi timeout -> lần sau chạy = CRON 
 * 1. Close site
 * 2. backup data
 * 3. run cut data và so sánh dữ liệu count của KH bò mối so với cũ có bị mất không?
 * 4: chú ý lân sau nên chạy = cron cho đỡ bị lỗi timeout
 */
class UpdateCutData
{
    const CUT_YEAR = 2014;
    /**
     * @Author: ANH DUNG Dec 05, 2015
     * @Todo: run function cut data
     */
    public static function run(){
        $from = time();
        // 1. table gas_users
//        self::cutUsers();
        
        // 2. gas_gas_file_scan
//        self::cutByModel('GasFileScan', "maintain_date");
//        
//        // 3. gas_gas_file_scan_detail
//        self::cutByModel('GasFileScanDetail', "maintain_date");
//        // 4. gas_gas_file_scan_info
//        self::cutByModel('GasFileScanInfo', "maintain_date");
//        // 5. gas_gas_maintain
//        self::cutByModel('GasMaintain', "maintain_date");
//        // 6. gas_gas_maintain_sell
//        self::cutByModel('GasMaintainSell', "date_sell");
//        // 7. gas_gas_remain
//        self::cutByModel('GasRemain', "date_input");
//        // 8. gas_gas_store_card
//        self::cutByModel('GasStoreCard', "date_delivery");
//        // 9. gas_gas_store_card_detail
//        self::cutByModel('GasStoreCardDetail', "date_delivery");
        
        $to = time();
        $second = $to-$from;
        echo 'All done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;  	
    }
    
    /** table user sẽ chỉ cut khách hàng của Bảo Trì và PTTT
     */
    public static function cutUsers(){
         $from = time();
         
        // 1. table gas_users
        $criteria = new CDbCriteria();
        $criteria->compare("year(created_date)", self::CUT_YEAR);
        $criteria->compare("role_id", ROLE_CUSTOMER);
        // xóa những kh Bảo Trì và KH PTTT
        $aTypeDelete = array(CUSTOMER_TYPE_MAINTAIN, CUSTOMER_TYPE_MARKET_DEVELOPMENT);
        $criteria->addInCondition( 'type', $aTypeDelete);        
        $count = Users::model()->count($criteria); 
//        echo $count;die; // = 315647 record user đếm ngày Dec 06, 2015
        Users::model()->deleteAll($criteria);
        // Run 1 xóa 2014: Dec 06, 2015 done in: 364 Second <=> 6.066 Minutes server Android test
        
        // xoa nam 2013
        $criteria = new CDbCriteria();
        $criteria->compare("year(created_date)", 2013);
        $criteria->compare("role_id", ROLE_CUSTOMER);
        // xóa những kh Bảo Trì và KH PTTT
        $aTypeDelete = array(CUSTOMER_TYPE_MAINTAIN, CUSTOMER_TYPE_MARKET_DEVELOPMENT);
        $criteria->addInCondition( 'type', $aTypeDelete);        
//        $count = Users::model()->count($criteria); 
//        echo $count;die; // = 26108 record user (2013) - đếm ngày Dec 06, 2015
        Users::model()->deleteAll($criteria);
        // Run 1 xóa 2013: Dec 06, 2015 done in: 24 Second <=> 0.4 Minutes server Android test
        
        $to = time();
        $second = $to-$from;
        echo "Users: $count done in: ".($second).'  Second  <=> '.($second/60).' Minutes';die;
        
        
    }
    
    /** table gas_gas_file_scan
     * xóa theo model và $field_date
     */
    public static function cutByModel($ClassName, $field_date){
        $model_ = call_user_func(array($ClassName, 'model'));
        $from = time();
        // 1. table gas_users
        $criteria = new CDbCriteria();
        $criteria->compare("year($field_date)", self::CUT_YEAR);
//        $models = $model_->findAll($criteria); // không dùng findAll dc vì bị Fatal error: Allowed memory size of 367001600 bytes exhausted (tried to allocate 32 bytes)
        $count = $model_->count($criteria); 
//        echo $count;die; // count 2014 => 7936 Record Android Test count ngày Dec 06, 2015
        $model_->deleteAll($criteria);
        
        if($ClassName == "GasMaintain" || $ClassName == "GasMaintainSell"){
            $criteria = new CDbCriteria();
            $criteria->compare("year($field_date)", 2013);
            $model_->deleteAll($criteria);
        }
        
        $to = time();
        $second = $to-$from;
        echo "$ClassName: $count done in: ".($second)."  Second  <=> ".($second/60)." Minutes";
        die;
        // 1. GasFileScan:  done in: 0 Second <=> 0 Minutes server Android(10$)
        
    }
    
    /** @Created_date: Dec 06, 2015 
     * *** 2014 Count and Delete Server Android(10$) Test ***********
     * * ######### 26108 record user (2013)  24 Second <=> 0.4 Minutes
     * Users: 315647 Record 364 Second <=> 6.066 Minutes 
     * GasFileScan: 7936 Record done in: 0 Second 
     * GasFileScanDetail: 15137 Record done in: 1 Second <=> 0.0166 Minutes
     * GasFileScanInfo: 266853 Record done in: 15 Second <=> 0.25 Minutes
     * GasMaintain: 342763 Record done in: 156 Second <=> 2.6 Minutes
     * GasMaintainSell: 24518 done in: 7 Second <=> 0.11666 Minutes
     * GasRemain: 62565 done in: 24 Second <=> 0.4 Minutes
     * GasStoreCard: 221047 done in: 91 Second <=> 1.51666 Minutes
     * GasStoreCardDetail: 616101 done in: 468 Second <=> 7.8 Minutes
     *                     844654
     * 
     * 
     * *** 2014 Count and Delete Server Live daukhimiennam.com (20$) ***********
     * * ######### 26108 record user (2013)  24 Second <=> 0.4 Minutes
     * Server10: Users: 315647 Record 364 Second <=> 6.066 Minutes 
     * Server20: Users: 315647 done in: 581 Second <=> 9.683 Minutes
     * Server10: GasFileScan: 7936 Record done in: 0 Second 
     * Server20: GasFileScan: 7936 done in: 0 Second <=> 0 Minutes
     * Server10: GasFileScanDetail: 15137 Record done in: 1 Second <=> 0.0166 Minutes
     * Server20: GasFileScanDetail: 15137 done in: 1 Second <=> 0.0166 Minutes
     * Server10: GasFileScanInfo: 266853 Record done in: 15 Second <=> 0.25 Minutes
     * Server20: GasFileScanInfo: 266853 done in: 18 Second <=> 0.3 Minutes
     * Server10: GasMaintain: 342763 Record done in: 156 Second <=> 2.6 Minutes
     * Server20: GasMaintain: 342763 done in: 210 Second <=> 3.5 Minutes
     * Server10: GasMaintainSell: 24518 done in: 7 Second <=> 0.11666 Minutes
     * Server20: GasMaintainSell: 24518 done in: 13 Second <=> 0.216 Minutes
     * Server10: GasRemain: 62565 done in: 24 Second <=> 0.4 Minutes
     * Server20: GasRemain: 62565 done in: 35 Second <=> 0.583 Minutes
     * Server10: GasStoreCard: 221047 done in: 91 Second <=> 1.51666 Minutes
     * Server20: GasStoreCard: 221047 done in: 126 Second <=> 2.1 Minutes
     * Server10: GasStoreCardDetail: 616101 done in: 468 Second <=> 7.8 Minutes
     * Server20: 
     * 
     */
    
    /** @Delete_data_PTTT Sep 23, 2016
     * @Description_cut_table: xóa dữ liệu PTTT của table User đi để tăng speed cho autocomplete
     * 1/ delete table gas_gas_maintain tren  PHP myadmin và set lại id tăng từ 846628
     * 2/ delete gas_gas_maintain_sell trên php code để xóa ràng buộc promotion
     * 3/ xóa PTTT bên table user = php code
     * @Author: ANH DUNG Sep 23, 2016
     * @table_user: Showing rows 0 - 29 (~524038 total , Query took 0.0014 sec)
     */
    public static function deletePTTT() {
        die('need review');
        $from = time();
        $mMaintainSell = GasMaintainSell::model()->findAll();
        $count = count($mMaintainSell);
        foreach($mMaintainSell as $model){
            $model->delete();
        }
        $to = time();
        $second = $to-$from;
        $info = "Delete gas_gas_maintain_sell ".$count.' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';
        Logger::WriteLog($info);
        /* run 2 delete gas_gas_maintain_sell
         * 
         */
        
        /* run 3. xóa PTTT bên table user = sql
         * SELECT * FROM `gas_users` where role_id=4 and type=2 ORDER BY `id` ASC
         *  DELETE FROM `c1gas35`.`gas_users` WHERE role_id=4 and type=2 
         * nếu muốn restore lại thì chỉ cần export dữ liệu từ file backup về
         * Sep 23, 2016 => run from phpmyadmin 483114 rows deleted. (Query took 728.5656 sec)
         */
        
    }
	  
}
?>
