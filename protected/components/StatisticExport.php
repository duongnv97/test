<?php
class StatisticExport
{
        // Nguyen Dung 10-29-2013
	public static function Statistic_maintain_export_excel(){
		Yii::import('application.extensions.vendors.PHPExcel',true);
		$objPHPExcel = new PHPExcel();
		// Set properties
		$objPHPExcel->getProperties()->setCreator("NguyenDung")
						->setLastModifiedBy("NguyenDung")
						->setTitle('Thống Kê Bảo Trì')
						->setSubject("Office 2007 XLSX Document")
						->setDescription("Statistic Maintain")
						->setKeywords("office 2007 openxml php")
						->setCategory("Gas");
		$row=1;
		$data = $_SESSION['data-excel'];
		$date_from = $data['DATE_FROM'];
		$date_to = $data['DATE_TO'];
		$STATISTIC_TITLE = 'Thống kê bảo trì';
		if(!empty($date_from) && empty($date_to))
			$STATISTIC_TITLE = "Thống kê bảo trì từ ngày: $date_from";
		elseif(empty($date_from) && !empty($date_to))
			$STATISTIC_TITLE = "Thống kê bảo trì đến ngày: $date_to";
		elseif(!empty($date_from) && !empty($date_to))
			$STATISTIC_TITLE = "Thống kê bảo trì từ ngày $date_from đến ngày $date_to";				
		$cmsFormatter = new CmsFormatter();	
	
		$AGENT_DATA = $data['AGENT'];
		$AGENT_MODEL = $data['AGENT_MODEL'];
		$MAINTAIN_EMPLOYEE_DATA = $data['MAINTAIN_EMPLOYEE']; // Nhân viên bảo trì,(role nv chăm sóc kh)
		$MAINTAIN_EMPLOYEE_ACCOUNTING_DATA = $data['MAINTAIN_EMPLOYEE_ACCOUNTING'];// nhân viên kế toán bán hàng			
		$MAINTAIN_EMPLOYEE_MODEL = $data['MAINTAIN_EMPLOYEE_MODEL'];
		$MAINTAIN_EMPLOYEE_ACCOUNTING_MODEL = $data['MAINTAIN_EMPLOYEE_ACCOUNTING_MODEL'];
		
		// 1.sheet 1 Đại Lý
		$objPHPExcel->setActiveSheetIndex(0);								
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);                            
		$objPHPExcel->getActiveSheet()->setTitle('Đại Lý'); 
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", $STATISTIC_TITLE);
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                                    ->setBold(true);    			
		$objPHPExcel->getActiveSheet()->mergeCells("A$row:C$row");
		$row++;
		$beginBorder = $row;
                $index=1;
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại Lý');
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Khách Hàng');
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đã Bảo Trì');
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Cuộc Gọi Tốt');
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Cuộc Gọi Xấu');
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Bán Hàng BT');
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Bán Hàng Cuộc Gọi Tốt');
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Bán Hàng Cuộc Gọi Xấu');		
                $index--;
		$objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);		
		$objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                                    ->setBold(true);    	
		$row++;
                $index=1;
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng Cộng');
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $AGENT_DATA['AgentCustomer']['total_count']);
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $AGENT_DATA['CountIsMaintain']['total_count']);
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $AGENT_DATA['CountIsMaintainCallGood']['total_count']);
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $AGENT_DATA['CountIsMaintainCallBad']['total_count']);
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $AGENT_DATA['CountSellMaintain']['total_count']);
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $AGENT_DATA['CountSellMaintainCallGood']['total_count']);
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $AGENT_DATA['CountSellMaintainCallBad']['total_count']);
                $index--;
                                
		$objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                                    ->setBold(true);    		
                // setAutoSize
                $index=2;
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);                
                $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index++))->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index++))->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index++))->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index++))->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index++))->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index++))->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index++))->setAutoSize(true);
                // setAutoSize
                
		$row++;
		//foreach($AGENT_MODEL as $agent_id=>$mUser):	
		foreach($AGENT_DATA['AgentCustomer'] as $agent_id=>$total):	
			if(isset($AGENT_MODEL[$agent_id])):
                                $index=1;
				$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $AGENT_MODEL[$agent_id]->first_name);
				$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($AGENT_DATA['AgentCustomer'][$agent_id])?$AGENT_DATA['AgentCustomer'][$agent_id]:'');
				$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($AGENT_DATA['CountIsMaintain'][$agent_id])?$AGENT_DATA['CountIsMaintain'][$agent_id]:'');
				$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($AGENT_DATA['CountIsMaintainCallGood'][$agent_id])?$AGENT_DATA['CountIsMaintainCallGood'][$agent_id]:'');
				$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($AGENT_DATA['CountIsMaintainCallBad'][$agent_id])?$AGENT_DATA['CountIsMaintainCallBad'][$agent_id]:'');
				$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($AGENT_DATA['CountSellMaintain'][$agent_id])?$AGENT_DATA['CountSellMaintain'][$agent_id]:'');
				$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($AGENT_DATA['CountSellMaintainCallGood'][$agent_id])?$AGENT_DATA['CountSellMaintainCallGood'][$agent_id]:'');
				$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($AGENT_DATA['CountSellMaintainCallBad'][$agent_id])?$AGENT_DATA['CountSellMaintainCallBad'][$agent_id]:'');
				$row++;
			endif;	
		endforeach;	
		$row--;	
                $index--;
                
		$objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);		
		$objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
				->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);		
		
		// 2.sheet 2 Nhân Viên Phục Vụ Khách Hàng
		$row=1;
		$objPHPExcel->createSheet();
		$objPHPExcel->setActiveSheetIndex(1);
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);                                            
		$objPHPExcel->getActiveSheet()->setTitle('NV Phục Vụ Khách Hàng'); 
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", $STATISTIC_TITLE);
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                                    ->setBold(true);    			
		$objPHPExcel->getActiveSheet()->mergeCells("A$row:C$row");		
		$row++;
		$beginBorder = $row;
                $index=1;
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại Lý');
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Nhân Viên Phục Vụ Khách Hàng');
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đã Bảo Trì');
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Cuộc Gọi Tốt');
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Cuộc Gọi Xấu');
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Bình Quay Về');
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Bán Hàng Gọi Tốt');
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Bán Hàng Gọi Xấu');
                $index--;
                
		$objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);		
		$objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                                    ->setBold(true);    			
                // setAutoSize
                $index=2;
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);                
                $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index++))->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index++))->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index++))->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index++))->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index++))->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index++))->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index++))->setAutoSize(true);
                // setAutoSize               
		
		$row++;
                $index=1;
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng Cộng');
                $index++;
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $MAINTAIN_EMPLOYEE_DATA['CountIsMaintain']['total_count']);
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $MAINTAIN_EMPLOYEE_DATA['CountIsMaintainCallGood']['total_count']);
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $MAINTAIN_EMPLOYEE_DATA['CountIsMaintainCallBad']['total_count']);
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $MAINTAIN_EMPLOYEE_DATA['CountSellMaintain']['total_count']);
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $MAINTAIN_EMPLOYEE_DATA['CountSellMaintainCallGood']['total_count']);
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $MAINTAIN_EMPLOYEE_DATA['CountSellMaintainCallBad']['total_count']);
                $index--;
		$objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                                    ->setBold(true);    		
		$row++;
		foreach($MAINTAIN_EMPLOYEE_MODEL as $agent_id=>$arrModel):	
			$mAgent = $arrModel['mAgent'];
			$mEmployee = $arrModel['mEmployee'];	
			if(count($mEmployee)>0){ 
				foreach($mEmployee as $mUser){	
					if(isset($MAINTAIN_EMPLOYEE_DATA['CountIsMaintain'][$agent_id][$mUser->id])){
                                                $index=1;
						$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $mAgent->first_name);
						$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $mUser->first_name);
						$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($MAINTAIN_EMPLOYEE_DATA['CountIsMaintain'][$agent_id][$mUser->id])?$MAINTAIN_EMPLOYEE_DATA['CountIsMaintain'][$agent_id][$mUser->id]:'');
						$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($MAINTAIN_EMPLOYEE_DATA['CountIsMaintainCallGood'][$agent_id][$mUser->id])?$MAINTAIN_EMPLOYEE_DATA['CountIsMaintainCallGood'][$agent_id][$mUser->id]:'');
						$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($MAINTAIN_EMPLOYEE_DATA['CountIsMaintainCallBad'][$agent_id][$mUser->id])?$MAINTAIN_EMPLOYEE_DATA['CountIsMaintainCallBad'][$agent_id][$mUser->id]:'');
						$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($MAINTAIN_EMPLOYEE_DATA['CountSellMaintain'][$agent_id][$mUser->id])?$MAINTAIN_EMPLOYEE_DATA['CountSellMaintain'][$agent_id][$mUser->id]:'');
						$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($MAINTAIN_EMPLOYEE_DATA['CountSellMaintainCallGood'][$agent_id][$mUser->id])?$MAINTAIN_EMPLOYEE_DATA['CountSellMaintainCallGood'][$agent_id][$mUser->id]:'');
						$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($MAINTAIN_EMPLOYEE_DATA['CountSellMaintainCallBad'][$agent_id][$mUser->id])?$MAINTAIN_EMPLOYEE_DATA['CountSellMaintainCallBad'][$agent_id][$mUser->id]:'');
						$row++;
					}
				}
			}
		endforeach;	
		$row--;	
                $index--;
		$objPHPExcel->getActiveSheet()->getStyle("C$beginBorder:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);		
		$objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
				->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);			
		
		// 3.sheet 3 Nhân Viên Kế Toán Bán Hàng
		$row=1;
		$objPHPExcel->createSheet();
		$objPHPExcel->setActiveSheetIndex(2);								
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);                            
		$objPHPExcel->getActiveSheet()->setTitle('NV Kế Toán Bán Hàng'); 
		$objPHPExcel->getActiveSheet()->setCellValue("A$row", $STATISTIC_TITLE);
		$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                                    ->setBold(true);    			
		$objPHPExcel->getActiveSheet()->mergeCells("A$row:C$row");		
		$row++;
		$beginBorder = $row;
                $index=1;
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại Lý');
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Nhân Viên Kế Toán Bán Hàng');
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đã Bảo Trì');
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Cuộc Gọi Tốt');
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Cuộc Gọi Xấu');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Bình Quay Về');
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Bán Hàng Gọi Tốt');
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Bán Hàng Gọi Xấu');
                
                $index--;
                
		$objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);		
		$objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                                    ->setBold(true);    			
                // setAutoSize
                $index=2;
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);                
                $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index++))->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index++))->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index++))->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index++))->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index++))->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index++))->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index++))->setAutoSize(true);
                // setAutoSize
		
		$row++;
                $index=1;
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng Cộng');
                $index++;
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $MAINTAIN_EMPLOYEE_ACCOUNTING_DATA['CountIsMaintain']['total_count']);
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $MAINTAIN_EMPLOYEE_ACCOUNTING_DATA['CountIsMaintainCallGood']['total_count']);
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $MAINTAIN_EMPLOYEE_ACCOUNTING_DATA['CountIsMaintainCallBad']['total_count']);
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $MAINTAIN_EMPLOYEE_ACCOUNTING_DATA['CountSellMaintain']['total_count']);
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $MAINTAIN_EMPLOYEE_ACCOUNTING_DATA['CountSellMaintainCallGood']['total_count']);
		$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $MAINTAIN_EMPLOYEE_ACCOUNTING_DATA['CountSellMaintainCallBad']['total_count']);
                $index--;
		$objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                                    ->setBold(true);    		
		$row++;
		foreach($MAINTAIN_EMPLOYEE_ACCOUNTING_MODEL as $agent_id=>$arrModel):	
			$mAgent = $arrModel['mAgent'];
			$mEmployee = $arrModel['mEmployee'];	
			if(count($mEmployee)>0){ 
				foreach($mEmployee as $mUser){	
					if(isset($MAINTAIN_EMPLOYEE_ACCOUNTING_DATA['CountIsMaintain'][$agent_id][$mUser->id])){
                                                $index=1;
						$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $mAgent->first_name);
						$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $mUser->first_name);
						$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($MAINTAIN_EMPLOYEE_ACCOUNTING_DATA['CountIsMaintain'][$agent_id][$mUser->id])?$MAINTAIN_EMPLOYEE_ACCOUNTING_DATA['CountIsMaintain'][$agent_id][$mUser->id]:'');
						$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($MAINTAIN_EMPLOYEE_ACCOUNTING_DATA['CountIsMaintainCallGood'][$agent_id][$mUser->id])?$MAINTAIN_EMPLOYEE_ACCOUNTING_DATA['CountIsMaintainCallGood'][$agent_id][$mUser->id]:'');
						$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($MAINTAIN_EMPLOYEE_ACCOUNTING_DATA['CountIsMaintainCallBad'][$agent_id][$mUser->id])?$MAINTAIN_EMPLOYEE_ACCOUNTING_DATA['CountIsMaintainCallBad'][$agent_id][$mUser->id]:'');
						$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($MAINTAIN_EMPLOYEE_ACCOUNTING_DATA['CountSellMaintain'][$agent_id][$mUser->id])?$MAINTAIN_EMPLOYEE_ACCOUNTING_DATA['CountSellMaintain'][$agent_id][$mUser->id]:'');
						$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($MAINTAIN_EMPLOYEE_ACCOUNTING_DATA['CountSellMaintainCallGood'][$agent_id][$mUser->id])?$MAINTAIN_EMPLOYEE_ACCOUNTING_DATA['CountSellMaintainCallGood'][$agent_id][$mUser->id]:'');
						$objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($MAINTAIN_EMPLOYEE_ACCOUNTING_DATA['CountSellMaintainCallBad'][$agent_id][$mUser->id])?$MAINTAIN_EMPLOYEE_ACCOUNTING_DATA['CountSellMaintainCallBad'][$agent_id][$mUser->id]:'');
						$row++;
					}
				}
			}
		endforeach;	
		$row--;	
                $index--;
		$objPHPExcel->getActiveSheet()->getStyle("C$beginBorder:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);		
		$objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
				->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);			
		
		//save file
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		for($level=ob_get_level();$level>0;--$level)
		{
			@ob_end_clean();
		}
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="'.'Thống Kê Bảo Trì'.'.'.'xlsx'.'"');

		header('Cache-Control: max-age=0');				
		$objWriter->save('php://output');			
		Yii::app()->end();      		
		 
    }
	  
	public static function Statistic_maintain_by_month_export_excel(){
		Yii::import('application.extensions.vendors.PHPExcel',true);
		$objPHPExcel = new PHPExcel();
		// Set properties
		$objPHPExcel->getProperties()->setCreator("NguyenDung")
						->setLastModifiedBy("NguyenDung")
						->setTitle('Thống Kê Tiến Độ Bảo Trì')
						->setSubject("Office 2007 XLSX Document")
						->setDescription("Statistic Maintain")
						->setKeywords("office 2007 openxml php")
						->setCategory("Gas");
		$row=1;
                $i=0;
		$data = $_SESSION['data-excel'];
                $MONTHS = $data['month'];
                $AGENT_MODEL = $data['AGENT_MODEL'];
                $YEARS = $data['YEARS'];
		$STATISTIC_TITLE = 'Thống kê bảo trì';
		$cmsFormatter = new CmsFormatter();	
                
                $objPHPExcel->removeSheetByIndex(0); // may be need
                foreach($MONTHS as $month=>$tempM):   
                    $days_of_month = $data['days'][$month];
                    $objPHPExcel->createSheet();
                    $objPHPExcel->setActiveSheetIndex($i);       
                    $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
                    $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);                            
                    $objPHPExcel->getActiveSheet()->setTitle("Tháng $month"); 
                    $objPHPExcel->getActiveSheet()->setCellValue("A$row", "Thống Kê Tiến Độ Bảo Trì Tháng $month - $YEARS");
                    $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                                        ->setBold(true);    			
                    $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
                    $row++;
                    // render header
                    $objPHPExcel->getActiveSheet()->setCellValue("A$row", "Đại Lý / Ngày");
                    $objPHPExcel->getActiveSheet()->setCellValue("B$row", "Tổng Cộng");
                    $beginBorder = $row;
                    $iAgent=3;
                    foreach($days_of_month as $day):
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($iAgent).$row, $day);
                        $iAgent++;
                    endforeach;
                    $objPHPExcel->getActiveSheet()->getStyle("A".$row.":".MyFunctionCustom::columnName(count($days_of_month)+2)."$row")
                            ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $objPHPExcel->getActiveSheet()->getStyle("A".$row.":".MyFunctionCustom::columnName(count($days_of_month)+2)."$row")
                            ->getFont()->setBold(true);
                    
                    // render header
                    $row++;
                    // begin body
                    foreach($data['total_month'][$YEARS][$month] as $agent_id=>$total): 
                        //$total = isset($data['total_month'][$YEARS][$month][$mAgent->id])?$data['total_month'][$YEARS][$month][$mAgent->id]:'';
                        $objPHPExcel->getActiveSheet()->setCellValue("A$row", $AGENT_MODEL[$agent_id]->first_name);
                        // begin body total
                        $objPHPExcel->getActiveSheet()->setCellValue("B$row", $total);
                        $iAgent=3;
                        foreach($days_of_month as $day):
                            $total_day = isset($data[$YEARS][$month][$day][$agent_id])?$data[$YEARS][$month][$day][$agent_id]:'';
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($iAgent).$row, $total_day);
                            $iAgent++;
                        endforeach;
                        $row++;
                    endforeach;
                    // begin body
                    $row--;
                    $objPHPExcel->getActiveSheet()->getStyle("B".$beginBorder.":".MyFunctionCustom::columnName(count($days_of_month)+2)."$row")
                            ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $objPHPExcel->getActiveSheet()->getStyle("A2:".MyFunctionCustom::columnName(count($days_of_month)+2)."$row")
                    	->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);			
                    $objPHPExcel->getActiveSheet()->getStyle("A2:B$row")
                    	->getFont()->setBold(true);
                    
                    
                    
                    $row=1; // end one month, need reset row
                    $i++; // next month, new sheet
                endforeach;
		//save file
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		for($level=ob_get_level();$level>0;--$level)
		{
			@ob_end_clean();
		}
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="'.'Thống Kê Tiến Độ Bảo Trì'.'.'.'xlsx'.'"');

		header('Cache-Control: max-age=0');				
		$objWriter->save('php://output');			
		Yii::app()->end();      		
		 
    }
	
	public static function Statistic_market_development_by_month_export_excel(){
		Yii::import('application.extensions.vendors.PHPExcel',true);
		$objPHPExcel = new PHPExcel();
		// Set properties
		$objPHPExcel->getProperties()->setCreator("NguyenDung")
						->setLastModifiedBy("NguyenDung")
						->setTitle('Thống Kê Phát Triển Thị Trường')
						->setSubject("Office 2007 XLSX Document")
						->setDescription("Statistic market development")
						->setKeywords("office 2007 openxml php")
						->setCategory("Gas");
		$row=1;
		$i=0;
		$data = $_SESSION['data-excel'];
                if( !is_array($data) || !isset($data['month'])) return ;
		$MONTHS = $data['month'];
		$MONITOR_EMPLOYEE = isset($data['MONITOR_EMPLOYEE'])?$data['MONITOR_EMPLOYEE']:array();
		$YEARS = $data['YEARS'];
		$STATISTIC_TITLE = 'Thống Kê Phát Triển Thị Trường';
		$cmsFormatter = new CmsFormatter();	
                
                $objPHPExcel->removeSheetByIndex(0); // may be need
                foreach($MONTHS as $month=>$tempM):   
                    $days_of_month = $data['days'][$month];
                    $objPHPExcel->createSheet();
                    $objPHPExcel->setActiveSheetIndex($i);       
                    $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
                    $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);                            
                    $objPHPExcel->getActiveSheet()->setTitle("Tháng $month"); 
                    $objPHPExcel->getActiveSheet()->setCellValue("A$row", "Thống Kê Phát Triển Thị Trường Tháng $month - $YEARS");
                    $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                                        ->setBold(true);    			
                    $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
                    $row++;
                    // render header
                    $objPHPExcel->getActiveSheet()->setCellValue("A$row", "Phát Triển Thị Trường");
                    $objPHPExcel->getActiveSheet()->setCellValue("B$row", "Tổng Cộng");
                    $beginBorder = $row;
                    $iAgent=3;
                    foreach($days_of_month as $day):
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($iAgent).$row, $day);
                        $iAgent++;
                    endforeach;
                    $objPHPExcel->getActiveSheet()->getStyle("A".$row.":".MyFunctionCustom::columnName(count($days_of_month)+2)."$row")
                            ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $objPHPExcel->getActiveSheet()->getStyle("A".$row.":".MyFunctionCustom::columnName(count($days_of_month)+2)."$row")
                            ->getFont()->setBold(true);
                    
                    // render header
                    $row++;
                    // begin body
                    if(isset($data['total_monitor'][$YEARS][$month])):
                        //foreach($AGENT_MODEL as $mAgent): 
                        foreach($data['total_monitor'][$YEARS][$month] as $monitor_id=>$total_monitor):
                                                    $arrIdAndTotalOfEmployee = $data['total_month'][$YEARS][$month][$monitor_id];
                                                    // begin row sum one of monitor
                                                    if(     isset($MONITOR_EMPLOYEE[$monitor_id]) &&
                                                            (isset($data['total_monitor'][$YEARS][$month][$monitor_id]) ||
                                                            isset($data['total_monitor_sell'][$YEARS][$month][$monitor_id]) )
                                                        ):
                                                            $text_total_monitor='';
                                                            $market_dev = isset($data['total_monitor'][$YEARS][$month][$monitor_id])?$data['total_monitor'][$YEARS][$month][$monitor_id]:0;
                                                            $market_dev_sell = isset($data['total_monitor_sell'][$YEARS][$month][$monitor_id])?$data['total_monitor_sell'][$YEARS][$month][$monitor_id]:0;
                                                            if($market_dev!=0 || $market_dev_sell!=0){
                                                                    if($market_dev_sell!=0)
                                                                            $text_total_monitor= $market_dev.'/'.$market_dev_sell;
                                                                    elseif($market_dev!=0)
                                                                            $text_total_monitor= $market_dev;
                                                                    elseif($market_dev!=0 && $market_dev_sell!=0)
                                                                            $text_total_monitor= $market_dev.'/'.$market_dev_sell.': '.(($market_dev_sell/$market_dev)*100).' %';
                                                                    elseif($market_dev==0 && $market_dev_sell!=0)
                                                                            $text_total_monitor= $market_dev.'/'.$market_dev_sell.': BH '.($market_dev_sell).' %';
                                                            }
                                                            $objPHPExcel->getActiveSheet()->setCellValue("A$row", $MONITOR_EMPLOYEE[$monitor_id]->first_name);
                                                            $objPHPExcel->getActiveSheet()->setCellValue("B$row", $text_total_monitor);
                                                            $objPHPExcel->getActiveSheet()->getStyle("A$row:B$row")
                                                                    ->getFont()->setBold(true);
                                                            $row++;
                                                    endif;
                                                    // end row sum one of monitor
                                                    // begin multi row sum one of employee
                                                    foreach($arrIdAndTotalOfEmployee as $employee_id=>$total):
                                                            $text_total_month='';
                                                            //$market_dev = isset($data['total_month'][$YEARS][$month][$employee->id])?$data['total_month'][$YEARS][$month][$employee->id]:0;
                                                            $market_dev = $total;
                                                            $market_dev_sell = isset($data['total_month_sell'][$YEARS][$month][$monitor_id][$employee_id])?$data['total_month_sell'][$YEARS][$month][$monitor_id][$employee_id]:0;
                                                            if($market_dev!=0 || $market_dev_sell!=0){
                                                                    if($market_dev_sell!=0)
                                                                            $text_total_month = $market_dev.'/'.$market_dev_sell;
                                                                    elseif($market_dev!=0)
                                                                            $text_total_month = $market_dev;
                                                                    elseif($market_dev!=0 && $market_dev_sell!=0)
                                                                            $text_total_month = $market_dev.'/'.$market_dev_sell.': '.(($market_dev_sell/$market_dev)*100).' %';
                                                                    elseif($market_dev==0 && $market_dev_sell!=0)
                                                                            $text_total_month = $market_dev.'/'.$market_dev_sell.': BH '.($market_dev_sell).' %';
                                                            }
                                                            if( isset($MONITOR_EMPLOYEE[$employee_id]) && ($market_dev!=0 || $market_dev_sell!=0)):
                                                                    $objPHPExcel->getActiveSheet()->setCellValue("A$row", $MONITOR_EMPLOYEE[$employee_id]->first_name);
                                                                    $objPHPExcel->getActiveSheet()->setCellValue("B$row", $text_total_month);
                                                                    $iDays=3;
                                                                    foreach($days_of_month as $day):
                                                                            $text_total_day='';
                                                                            $market_dev = isset($data['market_dev'][$YEARS][$month][$day][$monitor_id][$employee_id])?$data['market_dev'][$YEARS][$month][$day][$monitor_id][$employee_id]:0;
                                                                            $market_dev_sell = isset($data['market_dev_sell'][$YEARS][$month][$day][$monitor_id][$employee_id])?$data['market_dev_sell'][$YEARS][$month][$day][$monitor_id][$employee_id]:0;
                                                                            if($market_dev!=0 || $market_dev_sell!=0){
                                                                                    if($market_dev_sell!=0)
                                                                                            $text_total_day= $market_dev.'/'.$market_dev_sell;
                                                                                    elseif($market_dev!=0)
                                                                                            $text_total_day= $market_dev;
                                                                            }
                                                                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($iDays).$row, $text_total_day);
                                                                            $iDays++;									

                                                                    endforeach; // end <?php foreach($days_of_month as $day)
                                                                    $row++;
                                                            endif; // end if($market_dev!=0 || $market_dev_sell!=0):

                                                    endforeach;
                                                    // end multi row sum one of employee
                        endforeach; // end foreach($data['total_monitor'][$YEARS][$month] 
                    endif; // end if(isset($resultSta['total_monitor'][$YEARS][$month])):
                    // end body
                    $row--;
                    $objPHPExcel->getActiveSheet()->getStyle("B".$beginBorder.":".MyFunctionCustom::columnName(count($days_of_month)+2)."$row")
                            ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $objPHPExcel->getActiveSheet()->getStyle("A2:".MyFunctionCustom::columnName(count($days_of_month)+2)."$row")
                    	->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);			
                    //$objPHPExcel->getActiveSheet()->getStyle("A2:B$row")
                    	//->getFont()->setBold(true);
                    $row=1; // end one month, need reset row
                    $i++; // next month, new sheet
                endforeach; //  end foreach($MONTHS as $month=>$tempM):   
		//save file
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		for($level=ob_get_level();$level>0;--$level)
		{
			@ob_end_clean();
		}
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="'.'Thống Kê Phát Triển Thị Trường'.'.'.'xlsx'.'"');

		header('Cache-Control: max-age=0');				
		$objWriter->save('php://output');			
		Yii::app()->end();      		
		 
    }
	  
}
?>