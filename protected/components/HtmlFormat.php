<?php
/** Class for HtmlFormat*/
class HtmlFormat
{
    public $customer_id;
    public $mCashBookDetail, $html= '', $title='',$alertOnly = false; // Jun1118 field same model MonitorUpdate, for handleErrorBankTransfer cảnh báo lúc 12h trưa ko tính lỗi PVKH
    
        
    /** @Author: DungNT Apr 01, 2019
     *  @Todo: DLLK chỉ scan T2 + T6 trong tuần
     **/
    public function getListAgentDllk() {
        return [
            1294352,// ĐLLK Gas Minh Hoàng
            1584211,// ĐLLK Đại Lý Phú Xuân
            1613881,// ĐLLK Phú Thanh - Đồng Nai
            1755139,// ĐLLK Quang Đại - Cẩm Mỹ - Đồng Nai
            1763817,// ĐLLK Hoàng Gia 3 - Bình Thuận
            
            1364588,// Jun0419 - Đại Lý Chơn Thành - Bình Phước
            2076752,// Jun0419 - ĐLLK Thiện Phương Bù Đăng - Bình Phước
        ];
    }
    
    /**
     * @Author: ANH DUNG Mar 13, 2017
     * @Todo: format html reponse array customer
     */
    public static function callCenterListCustomer($phone_number, $aUserPhone, $CallCurrentTab) {
        $html = '';
        $html .= '<h1 class="item_c">Cuộc gọi đến: <span class="BoxSelectCustomerPhoneNumber">'.$phone_number.'</span></h1>';
        $html .= '<ul class="">';
            foreach ($aUserPhone as $mUserPhone):
                $mUser = $mUserPhone->rUser;
                $html .= '<li class="w-600 float_l f_size_15">';
                        $html .= '<a class="item_b f_size_20 RowCustomerName" customer_id="'.$mUser->id.'" CallCurrentTab="'.$CallCurrentTab.'" href="javascript:;" style="color: #2962FF">'.$mUser->code_bussiness.'-'.$mUser->getFullName(). ' - '.$mUser->getTypeCustomerText().'</a><br>';
                        $html .= '<p><span class="RowCustomerPhone">'.$mUser->phone.'</span><br><span class="RowCustomerAddress">'.$mUser->address.'</span></p>';
                $html .= '</li>';
            endforeach;
        $html .= '</ul>';
        return $html;
    }
    /**
     * @Author: ANH DUNG Mar 15, 2017
     * @Todo: format html reponse array history customer
     */
    public function callCenterCustomerHistory($aModelSell, $CallCurrentTab) {
        $html = ''; $emptyHistory = true; $today = date('Y-m-d'); $mFollowCustomer =new FollowCustomer();
        $mAppCache = new AppCache();
        $aMaterial = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
        $mFollowCustomer->customer_id = $this->customer_id;
        $html .= $mFollowCustomer->getCallOutHtml();
        $html .= '<table class="call-tbl-list">';
            $html .= '<tr>';
                $html .= '<th class="item_c" colspan="2">Lịch sử lấy hàng</th>';
            $html .= '</tr>';
            foreach($aModelSell as $k => $mSell):
                $background = $eventLog = $callLog = '';
                if($mSell->created_date_only == $today){
                    $background = 'background: #8BC34A;';
                    $mTransactionEvent = new TransactionEvent();
                    $mTransactionEvent->transaction_history_id = $mSell->transaction_history_id;
                    $mTransactionEvent->type = TransactionEvent::TYPE_SELL;
                    $eventLog   = $mTransactionEvent->getToday();
                    if($k == 0){
                        $callLog    = HtmlFormat::getCallInHistory($mSell->customer_id, $today);
                    }
                }
                $emptyHistory = false;
                $employeeCallCenter = $mSell->getUidLogin();
                $note = $mSell->getAgent(); $sGas='';
                if(!empty($mSell->note)){
                    $note .= "<br>Ghi chú: $mSell->note";
                }
                foreach($mSell->rDetail as $key => $item):
                    $break = ($key==0) ? '<br>' : ', ';
                    $price = $item->amount > 0 ? ActiveRecord::formatCurrency($item->amount) : '';
                    $sGas .= isset($aMaterial[$item->materials_id]) ? $break.$price.' - '.$aMaterial[$item->materials_id]['name'] : '';
                endforeach;
                $html .= '<tr style="'.$background.'">';
                    $html .= '<td>'.$mSell->getCreatedDate().'</td>';
                    $html .= '<td>'."<b>NV: $employeeCallCenter</b><br>".trim($note.$sGas, '<br>').$eventLog.$callLog.'</td>';
                $html .= '</tr>';
            endforeach;
            if($emptyHistory):
                $html .= '<tr>';
                    $html .= '<td class="item_c" colspan="2">Không có dữ liệu</td>';
                $html .= '</tr>';
            endif;
            
        $html .= '</table>';
        
        return $html;
    }
    
    /**
     * @Author: ANH DUNG May 21, 2017
     * @Todo: format html reponse array history customer Bo Moi
     */
    public static function callCenterCustomerHistoryBoMoi($modelsStorecard, $CallCurrentTab) {
        $html = ''; $emptyHistory = true; $today = date('Y-m-d');
        $mAppCache = new AppCache();
        $aMaterial = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
        $html .= '<table class="call-tbl-list">';
            $html .= '<tr>';
                $html .= '<th class="item_c" colspan="2">Lịch sử lấy hàng</th>';
            $html .= '</tr>';
            foreach($modelsStorecard as $k => $mStorecard):
                $background = $eventLog = $callLog = '';
                if($mStorecard->date_delivery == $today){
                    $background = 'background: #8BC34A;';
                    $mTransactionEvent = new TransactionEvent();
                    $mTransactionEvent->transaction_history_id = $mStorecard->rAppOrder->id;
                    $mTransactionEvent->type = TransactionEvent::TYPE_BO_MOI;
                    $eventLog   = $mTransactionEvent->getToday();
                    $callLog    = HtmlFormat::getCallInHistory($mStorecard->customer_id, $today);
                }
                $emptyHistory = false;
                $employeeCallCenter = $mStorecard->getUidLoginByCache();
                $note = ''; $sGas='';
                $note .= '<br>Ghi chú: '.$mStorecard->note.'<br>';
                foreach($mStorecard->rStoreCardDetail as $key => $item):
                    $break = ($key==0) ? '' : '<br>';
                    $qty = '<b>SL: '.ActiveRecord::formatCurrency($item->qty).'</b> - ';
                    $sGas .= isset($aMaterial[$item->materials_id]) ? $break.$qty.$aMaterial[$item->materials_id]['name'] : '';
                endforeach;
                $html .= '<tr style="'.$background.'">';
                    $html .= '<td>'.$mStorecard->getCreatedDateApp().'</td>';
                    $html .= '<td>'."<b>$employeeCallCenter</b> - {$mStorecard->getAgentName()}<br>".trim($note.$sGas, '<br>'). $eventLog. $callLog. '</td>';
                $html .= '</tr>';
            endforeach;
            if($emptyHistory):
                $html .= '<tr>';
                    $html .= '<td class="item_c" colspan="2">Không có dữ liệu</td>';
                $html .= '</tr>';
            endif;
            
        $html .= '</table>';
        
        return $html;
    }
    
    /**
     * @Author: ANH DUNG Jul 22, 2017
     * @Todo: format html reponse array history customer Bo Moi
     */
    public function callCenterCustomerHistoryBoMoiV1($aAppOrder, $CallCurrentTab) {
        $html = ''; $emptyHistory = true; $today = date('Y-m-d');
        $nextDay = MyFormat::modifyDays($today, 1);
        $mAppCache = new AppCache();
        $aMaterial = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
        $html .= '<table class="call-tbl-list">';
            $html .= '<tr>';
                $html .= '<th class="item_c" colspan="2">Lịch sử lấy hàng</th>';
            $html .= '</tr>';
            foreach($aAppOrder as $k => $mAppOrder):
                $background = $eventLog = $callLog = '';
                if($mAppOrder->date_delivery == $today || $mAppOrder->date_delivery == $nextDay){
                    $background = 'background: #8BC34A;';
                    $mTransactionEvent = new TransactionEvent();
                    $mTransactionEvent->transaction_history_id = $mAppOrder->id;
                    $mTransactionEvent->type = TransactionEvent::TYPE_BO_MOI;
                    $eventLog   = $mTransactionEvent->getToday();
                    $callLog    = HtmlFormat::getCallInHistory($mAppOrder->customer_id, $today);
                }
                $emptyHistory = false;
                $employeeCallCenter = $mAppOrder->getUidLoginByCache();
                $note = ''; $sGas='';
                $note .= '<br>Ghi chú: '.$mAppOrder->note_employee.'<br>';
                
                $mAppOrder->mapJsonFieldOneDecode('JSON_FIELD', 'json', 'baseArrayJsonDecode');
                foreach($mAppOrder->info_gas as $key => $item):
                    $break = ($key==0) ? '' : '<br>';
                    $qty = '<b>SL: '.ActiveRecord::formatCurrency($item['qty_real']).'</b> - ';
                    $sGas .= $break.$qty.$item['materials_name'];
                endforeach;
                $html .= '<tr style="'.$background.'">';
                    $html .= '<td>'.$mAppOrder->getCreatedDate().'</td>';
                    $html .= '<td>'."<b>$employeeCallCenter</b> - {$mAppOrder->getAgentName()}<br>".trim($note.$sGas, '<br>'). $eventLog. $callLog. '</td>';
                $html .= '</tr>';
            endforeach;
            if($emptyHistory):
                $html .= '<tr>';
                    $html .= '<td class="item_c" colspan="2">Không có dữ liệu</td>';
                $html .= '</tr>';
            endif;
            
        $html .= '</table>';
        
        return $html;
    }
    
    /** @Author: ANH DUNG Jul 19, 2017
     * @Todo: lấy danh sách cuộc gọi trong ngày của 1 KH
     */
    public static function getCallInHistory($customer_id, $date) {
        $model = new Call();
        $model->customer_id         = $customer_id;
        $model->created_date_only   = $date;
        return $model->getCallInHtml();
    }
    
    /**
     * @Author: ANH DUNG May 31, 2017
     * @Todo: format email send customer not have price
     * @param: $models array User
     */
    public function getContentBoMoiNotSetPrice($models) {
        $this->html .= "<table  cellpadding='0' cellspacing='0'> ";
            $this->html .= '<thead>';
            $this->html .= '<tr>';
                $this->html .= '<th style="border:1px solid #000;font-size: 15px;font-weight: bold;text-align: center;">STT</th>';
                $this->html .= '<th style="border:1px solid #000;font-size: 15px;font-weight: bold;text-align: center;">Khách hàng</th>';
                $this->html .= '<th style="border:1px solid #000;font-size: 15px;font-weight: bold;text-align: center;">Loại KH</th>';
                $this->html .= '<th style="border:1px solid #000;font-size: 15px;font-weight: bold;text-align: center;">Sale</th>';
                $this->html .= '<th style="border:1px solid #000;font-size: 15px;font-weight: bold;text-align: center;">Lấy hàng mới nhất</th>';
//                $this->html .= '<th style="border:1px solid #000;font-size: 15px;font-weight: bold;text-align: center;">Ngày tạo</th>';
            $this->html .= '</tr>';
            $this->html .= '</thead>';
            $this->html .= '<tbody>';
        foreach($models as $key=>$model){
            $customerName   = $model->code_bussiness.' - '.$model->first_name;
            $customerAdd    = $model->address;
            $customerType   = $model->getTypeCustomerText();
            $saleName       = $model->getSaleName();
            $lastPurchase   = !is_null($model->last_purchase) ? MyFormat::dateConverYmdToDmy($model->last_purchase) : '';
            $createdDate   = MyFormat::dateConverYmdToDmy($model->created_date, 'd/m/Y H:i');
            $this->html .= '<tr>';
                $this->html .= "<td style='padding: 3px; border:1px solid #000;text-align: center;'>".($key+1)."</td>";
                $this->html .= "<td style='padding: 3px; border:1px solid #000;'><p style='font-weight: bold;'>$customerName</p>$customerAdd</td>";
                $this->html .= "<td style='padding: 3px; border:1px solid #000;text-align: center;'>$customerType</td>";
                $this->html .= "<td style='padding: 3px; border:1px solid #000;'>$saleName</td>";
                $this->html .= "<td style='padding: 3px; border:1px solid #000;text-align: center;'>$lastPurchase</td>";
//                $this->html .= "<td style='padding: 3px; border:1px solid #000;text-align: center;'>$createdDate</td>";
            $this->html .= '</tr>';
        }
        $this->html .= '</tbody>';
        $this->html .= '</table>';
        return $this->html;
    }
    
    /** @Author: ANH DUNG Dec 07, 2017
     * @Todo: send mail alert cho KTKV, KTVP các NV tồn quỹ trên 3 tr
     */
    public function emailInventoryCash($mCashBookDetail) {
        $this->mCashBookDetail = $mCashBookDetail;
        $info   = $this->emailInventoryCashGetContent($mCashBookDetail);
        $needMore['title']      = 'Cảnh báo tồn quỹ '.date('d/m/Y H:i');
//        $needMore['list_mail']  = ['dungnt@spj.vn'];
        $needMore['list_mail'] = ['dungnt@spj.vn', 'ketoan.kv@spj.vn', 'giamsat@spj.vn',
            'ketoan@spj.vn', 'ngocpt@spj.vn'];
        SendEmail::bugToDev($info, $needMore);
    }
    
    public function emailInventoryCashGetContent($mCashBookDetail) {
        $aDataInventory         = $mCashBookDetail->aDataAlert;
        $aDataSort              = $mCashBookDetail->aDataSort;
        $mAppCache              = new AppCache();
        $listdataProvince       = $mAppCache->getListdata('GasProvince', AppCache::LISTDATA_PROVINCE);

        $today = date('d/m/Y H:i'); $noData = true;
        $html = '<p>Danh sách nhân viên chưa nộp ngân hàng ngày: '.$today.'</p><br><br>';
        foreach($aDataSort as $province_id => $aInfoUid){
            $nameProvince = isset($listdataProvince[$province_id]) ? $listdataProvince[$province_id] : '';
            $html .= "<h3>$nameProvince</h3>";
            $this->renderOneProvince($html, $aInfoUid, $aDataInventory, $mCashBookDetail);
            $noData = false;
        }
        if($noData){
            $html .= '<h3>Không có dữ liệu</h3>';
        }
        return $html;
    }
    /** @Author: ANH DUNG Oct 31, 2018
     *  @Todo: get list agent highlight color
     **/
    public function getAgentHighlight() {
        return [
            970958, // Đại Lý Mỹ Luông
            898894, // Cửa Hàng Gas Minh Hạnh
            1161356, // ĐLLK - Đại Lý An Hiệp - Đồng Tháp 
        ];
    }
    
    public function renderOneProvince(&$html, $aInfoUid, $aDataInventory, $mCashBookDetail) {
        $key = 1;
        
        $html .= "<table  cellpadding='0' cellspacing='0'> ";
            $html .= '<thead>';
            $html .= '<tr>';
                $html .= '<th style="padding: 3px; border:1px solid #000;font-size: 15px;font-weight: bold;text-align: center;">STT</th>';
                $html .= '<th style="padding: 3px; border:1px solid #000;font-size: 15px;font-weight: bold;text-align: center;">Đại lý</th>';
                $html .= '<th style="padding: 3px; border:1px solid #000;font-size: 15px;font-weight: bold;text-align: center;">Nhân viên</th>';
                $html .= '<th style="padding: 3px; border:1px solid #000;font-size: 15px;font-weight: bold;text-align: center;">Tồn đầu</th>';
                $html .= '<th style="padding: 3px; border:1px solid #000;font-size: 15px;font-weight: bold;text-align: center;">Thu</th>';
                $html .= '<th style="padding: 3px; border:1px solid #000;font-size: 15px;font-weight: bold;text-align: center;">Chi</th>';
                $html .= '<th style="padding: 3px; border:1px solid #000;font-size: 15px;font-weight: bold;text-align: center;">Tồn cuối</th>';
            $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';
        foreach($aInfoUid as $employee_id => $closingBalance){
            $aInfo      = $aDataInventory[$employee_id];
            $agent_id   = isset($aInfo['agent_id']) ? $aInfo['agent_id'] : 0;
            $this->saveErrorsBankTransfer($agent_id, $employee_id);
            $background = '';
            if(in_array($agent_id, $this->getAgentHighlight())){
                $background = 'background: yellow;';
            }
            $html .= "<tr style='$background'>";
                $html .= "<td style='padding: 3px; border:1px solid #000;text-align: center;'>".($key++)."</td>";
                $html .= "<td style='padding: 3px; border:1px solid #000;'><p>{$aInfo['agentName']}</p></td>";
                $html .= "<td style='padding: 3px; border:1px solid #000;'><p>{$aInfo['employeeName']}</p></td>";
                $html .= "<td style='padding: 3px; border:1px solid #000;text-align: right;'>{$aInfo['opening']}</td>";
                $html .= "<td style='padding: 3px; border:1px solid #000;text-align: right;'>{$aInfo['in']}</td>";
                $html .= "<td style='padding: 3px; border:1px solid #000;text-align: right;'>{$aInfo['out']}</td>";
                $html .= "<td style='padding: 3px; border:1px solid #000;text-align: right; font-weight: bold;'>{$aInfo['closing']}</td>";
            $html .= '</tr>';
        }
        $html .= '</tbody>';
        $html .= '</table>';
    }
    public function saveErrorsBankTransfer($agent_id, $employee_id) {
        if($this->alertOnly || $this->isExceptionDLLK($agent_id, $employee_id)){
            return ;// Jun1118
        }
        // 1. make record 
        $model = new MonitorUpdate();
        $model->user_id = $employee_id;
        $model->saveErrorsBankTransfer();
        
        // 2. make notify
        $message    = 'Bạn đã bị phạt 1 lỗi chưa nộp tiền ngân hàng trước 16h30';
        $aUid       = [$employee_id];
        foreach($aUid as $uid){
            GasScheduleNotify::InsertRecord($uid, GasScheduleNotify::ANNOUNCE_TO_ALL, 0, '', $message, []);
        }
        // 3. Now2518 save to table EmployeeProblems, save toàn bộ lỗi vào 1 chỗ để trừ lương
        // viết chung trong hàm $model->saveErrorsBankTransfer();
    }
    
    /** @Author: DungNT Apr 01, 2019
     *  @Todo: DLLK chỉ scan T2,T6
     **/
    public function isExceptionDLLK($agent_id, $employee_id) {
        $isException = false;
        $day_of_week = strtolower(date('l'));
        $aDateCheck = ['monday', 'friday'];
        if(in_array($agent_id, $this->getListAgentDllk()) && !in_array($day_of_week, $aDateCheck) ){
            Logger::WriteLog("day_of_week = $day_of_week => isExceptionDLLK $agent_id - employee_id = $employee_id");
            $isException = true;
        }
        return  $isException;
    }
    
    
    
    /** @Author: HOANG NAM 22/12/2017
     *  @CODE: NAM001
     *  @Todo: Định dang email gửi đi (gasProfile Expired)
     *  @Param: $aExpired: danh sách profile hết hạn theo vùng: zone->province->profile
     **/
    public function renderEmailProfileExpired($aExpired){
        $aZone              = GasOrders::getZoneNew();
        $mAppCache          = new AppCache();
        $listdataProvince   = $mAppCache->getListdata('GasProvince', AppCache::LISTDATA_PROVINCE);
        $html='';
        foreach ($aExpired as $idZone => $aProvince) {
            $nameZone = isset($aZone[$idZone])  ? $aZone[$idZone] : $idZone;
            $html.='<h2>'. $nameZone .'</h2>';
            foreach ($aProvince as $idProvince => $aProfile) {
                $html.='<span style="font-size:1.3em">'.$listdataProvince[$idProvince].'</span>';
                $html.= $this->renderOneProvinceExpired($aProfile);
            }
        }
        return $html;
    }
    /** @Author: HOANG NAM
     *  @CODE: NAM001
     *  @Todo: tao danh sách hồ sơ pháp lý hết hạn
     **/
    public function renderOneProvinceExpired($aProfile){
//        @usort($aProfile, function ($item1, $item2) {
//            return $item2->agent_id <= $item1->agent_id;
//        });
        $key=1;
        $html = "<table  cellpadding='0' cellspacing='0'> ";
            $html .= '<thead>';
                $html .= '<tr>';
    //          danh sách td
                    $html .= '<th style="padding: 3px; border:1px solid #000;font-size: 15px;font-weight: bold;text-align: center;">#</th>';
                                        $html .= '<th style="padding: 3px; border:1px solid #000;font-size: 15px;font-weight: bold;text-align: center;">Ngày hết hạn</th>';
                    $html .= '<th style="padding: 3px; border:1px solid #000;font-size: 15px;font-weight: bold;text-align: center;">Đại lý</th>';
                    $html .= '<th style="padding: 3px; border:1px solid #000;font-size: 15px;font-weight: bold;text-align: center;">Tên hồ sơ</th>';
                $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';
            foreach ($aProfile as $itemProvince) {
                $html .= '<tr>';
                    $html .= "<td style='padding: 3px; border:1px solid #000;text-align: center;'>".($key++)."</td>";
                    $html .= "<td style='padding: 3px; border:1px solid #000;text-align: center;'>". MyFormat::dateConverYmdToDmy($itemProvince->date_expired)."</td>";
                    $html .= "<td style='padding: 3px; border:1px solid #000;'>{$itemProvince->getAgent()}</td>";
                    $html .= "<td style='padding: 3px; border:1px solid #000;'>{$itemProvince->name}</td>";
                $html .= '</tr>';
            }
            $html .= '</tbody>';
        $html .= '</table>';
        return $html;
    }
    
    
    /** @Author: ANH DUNG Feb 14, 2019
     * @Todo: send mail alert PVKH âm quỹ cho KTKV, KTVP
     */
    public function emailNegativeCashbook($mCashBookDetail) {
        $info   = $this->emailNegativeCashbookGetContent($mCashBookDetail);
        $needMore['title']      = '[Âm Quỹ] '.date('d/m/Y H:i');
        $needMore['list_mail'] = ['dungnt@spj.vn', 'ketoan.kv@spj.vn', 'giamsat@spj.vn',
            'ngocpt@spj.vn'];
//        $needMore['list_mail']  = ['dungnt@spj.vn'];
//        $needMore['SendNow']  = 1;
        SendEmail::bugToDev($info, $needMore);
    }
    // belong function emailNegativeCashbook
    public function emailNegativeCashbookGetContent($mCashBookDetail) {
        $aDataInventory         = $mCashBookDetail->aDataNegativeCashbook;
        $aDataSort              = $mCashBookDetail->aDataNegativeCashbookSort;
        $mAppCache              = new AppCache();
        $listdataProvince       = $mAppCache->getListdata('GasProvince', AppCache::LISTDATA_PROVINCE);

        $today = date('d/m/Y H:i'); $noData = true;
        $html  = '<p>Danh sách nhân viên âm quỹ ngày: '.$today.'</p><br><br>';
        foreach($aDataSort as $province_id => $aInfoUid){
            $nameProvince = isset($listdataProvince[$province_id]) ? $listdataProvince[$province_id] : '';
            $html .= "<h3>$nameProvince</h3>";
            $this->renderOneProvince($html, $aInfoUid, $aDataInventory, $mCashBookDetail);
            $noData = false;
        }
        if($noData){
            $html .= '<h3>Không có dữ liệu</h3>';
        }
        return $html;
    }
    
}
