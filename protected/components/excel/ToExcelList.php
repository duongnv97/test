<?php
class ToExcelList
{
    const STORE_CLIENT      = 1; // Lưu vào máy client (hiện popup download)
    const STORE_SERVER      = 2; // Lưu trên server
    public $output          = 1;
    // Anh Dung Oct0617
    public function listStorecard($aData, $mStoreCard){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                        ->setLastModifiedBy("NguyenDung")
                                        ->setTitle('TheKho')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("TheKho")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1; $i=1;
//        $aData = $_SESSION['data-excel']->data;
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Danh sách thẻ kho'); 
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã thẻ kho');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại Lý');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'khách hàng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Loại KH');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Loại Nhập Xuất');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Vật tư');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Số lượng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đơn giá');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Thành tiền');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày tạo');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Người giao');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Số xe');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ghi chú');
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        $this->listStorecardBody($aData, $objPHPExcel, $row);
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10); // SL
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20); // don gia
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20); // thanh tien
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(55);

        $row--;
//        $objPHPExcel->getActiveSheet()->getStyle("F$beginBorder:F".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("H$beginBorder:H".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle("I".($beginBorder+1).":J".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle("K$beginBorder:K".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()
                    ->getStyle("I$beginBorder:J".$row)->getNumberFormat()
                    ->setFormatCode('#,##0'); 
//        $objPHPExcel->getActiveSheet()
//                                ->getStyle("H$beginBorder:H".$row)->getNumberFormat()
//                                ->setFormatCode('#,##0.00'); 
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

//        $_SESSION['data-excel'] = null;
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        if($this->output == ExportList::STORE_SERVER){
            $mCronExcel  = new CronExcel();
            $file_name   = $mCronExcel->getFileFinalPath($mStoreCard);
            $objWriter->save($file_name);
        } else {
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="'."TheKho".'.'.'xlsx'.'"');

            header('Cache-Control: max-age=0');				
            $objWriter->save('php://output');			
            Yii::app()->end();
        }
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public function listStorecardBody($aData, &$objPHPExcel, &$row) {
        $mAppCache      = new AppCache();
        $aAgent         = $mAppCache->getAgentListdata();
        $aCustomer      = $mAppCache->getCustomerListdata();
        $aDriver        = $mAppCache->getListdataUserByRole(ROLE_DRIVER);
        $aCar           = $mAppCache->getListdataUserByRole(ROLE_CAR);
        
        $aCustomerType  = CmsFormatter::$CUSTOMER_BO_MOI;
        $aStorecardType  = CmsFormatter::$STORE_CARD_ALL_TYPE;
        $aMaterials     = $mAppCache->getListdata('GasMaterials', AppCache::LISTDATA_MATERIAL);
        $aCustomer      = $aCustomer + $aAgent;
        
        //get json store card detail
        $aStoreCardId = [];
        foreach($aData as $data){
            $aStoreCardId[$data->id] = $data->id;
        }
        $criteria = new CDbCriteria;
        $criteria->addInCondition('t.relate_id', $aStoreCardId);
        $aModelGasDebts = GasDebts::model()->findAll($criteria);
        $aDetail = [];
        foreach ($aModelGasDebts as $value) {
            $aDetail[$value->relate_id] = empty($value->json) ? '' : json_decode($value->json, true);
        }
        
        foreach($aData as $data){
            $agentName      = isset($aAgent[$data->user_id_create]) ? $aAgent[$data->user_id_create] : '';
            $customerName   = isset($aCustomer[$data->customer_id]) ? $aCustomer[$data->customer_id] : '';
            $customerType   = isset($aCustomerType[$data->type_user]) ? $aCustomerType[$data->type_user] : '';
            $storecardType  = isset($aStorecardType[$data->type_in_out]) ? $aStorecardType[$data->type_in_out] : '';
            $dateDelivery   = $data->getDateDelivery();
            $store_card_no  = $data->store_card_no;
            if(empty($customerName) && !empty($data->customer_id)){
                $mUser = Users::model()->findByPk($data->customer_id);
                $customerName = $mUser ? $mUser->first_name : '';
            }
            if(empty($data->customer_id)){
                $customerName = 'HGĐ';
            }
            $driverName     = isset($aDriver[$data->driver_id_1]) ? $aDriver[$data->driver_id_1] : $data->driver_id_1;
            $carName        = isset($aCar[$data->car_id]) ? $aCar[$data->car_id] : $data->car_id;
            
            foreach($data->rStoreCardDetail as $key => $item):
                $materialName   = isset($aMaterials[$item->materials_id]) ? $aMaterials[$item->materials_id] : '';
                $aDetailJson    = isset($aDetail[$data->id][$item->id]) ? $aDetail[$data->id][$item->id] : [];
                $price          = isset($aDetailJson['price']) ? $aDetailJson['price'] : 0;
                $amount         = isset($aDetailJson['amount']) ? $aDetailJson['amount'] : 0;
                
                $index=1;
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $dateDelivery);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $store_card_no);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $agentName);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $customerName);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $customerType);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $storecardType);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $materialName);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $item->qty); // SL
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $price); // don gia
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $amount); // thanh tien
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->created_date);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $driverName);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $carName);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getNote());
                $row++;
            endforeach;
        }
    }
    
    /** @Author: Pham Thanh Nghia Aug 6, 2018
     *  @Todo:
     *  @Param: admin/gasUphold/index
     **/
    public function exportGasUphold($model){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("PhamThanhNghia")
                                ->setLastModifiedBy("PhamThanhNghia")
                                ->setTitle('Ho Tro KH')
                                ->setSubject("Office 2007 XLSX Document")
                                ->setDescription("Ho Tro KH")
                                ->setKeywords("office 2007 openxml php")
                                ->setCategory("Gas");
        $row=1;
        $i=1;
        $cmsFormatter = new CmsFormatter();
        $aData = $_SESSION['data-excel']->data;
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Danh sách'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "BÁO CÁO BẢO TRÌ SỰ CỐ");
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:F$row");
        $objPHPExcel->getActiveSheet()->getStyle("A$row:F$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã sô');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Khách hàng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'NV Kinh doanh');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Loại sự cố');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Người liên hệ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Điện thoại');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'NV Bảo trì');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Nội dung');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Xác nhận(view)');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Trạng thái');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Người tạo');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày tạo');
        
        
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        
        $this->exportGasUpholdBody($aData, $objPHPExcel, $row, $model);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(60);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
        
        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:B".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
//        $objPHPExcel->getActiveSheet()->getStyle("C$beginBorder:O".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $_SESSION['data-excel'] = null;
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."BaoCaoBaoTriSuCo".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    public function exportGasUpholdBody($aData,&$objPHPExcel, &$row, $model){
        $stt = 1;
        foreach ($aData as $data) {
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->code_no);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCustomer()."\n".$data->getCustomer("address"));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getSale());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getTextUpholdType());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getContactPerson());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getContactTel());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getEmployee());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getContentOnly());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getHasReadText());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getTextStatus());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getUidLogin());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCreatedDate());
            $row++;
            $stt++;
        }
    }
    
    
    /** @Author: NGUYEN KHANH TOAN 2018
     *  @Todo:
     *  @Param:
     **/
    public function exportGasScheduleNotifyHistory(){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenKhanhToan")
                                ->setLastModifiedBy("NguyenKhanhToan")
                                ->setTitle('Thong Bao KH')
                                ->setSubject("Office 2007 XLSX Document")
                                ->setDescription("Thong Bao KH")
                                ->setKeywords("office 2007 openxml php")
                                ->setCategory("Gas");
        $row=1;
        $i=1;
        $aData = $_SESSION['dataScheduleNotifyHistory']->data;
        $model = $_SESSION['modelScheduleNotifyHistory'];
        
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Danh sách'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Danh sách notify app');
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:D$row");
        $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Khách hàng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Nội dung');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày tạo');
        
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        
        $this->exportGasScheduleNotifyHistoryBody($aData, $objPHPExcel, $row, $model);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(100);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        
        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:B".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle("D$beginBorder:D".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $_SESSION['dataScheduleNotifyHistory'] = null;
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."ListNotifyApp".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
            
            
    } 
    public function exportGasScheduleNotifyHistoryBody($aData, $objPHPExcel, &$row, $model){
        $stt = 1;
        foreach ($aData as $data) {
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getUserInfo());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getTitle());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", MyFormat::dateConverYmdToDmy($data->created_date));
            $row++;
            $stt++;
        }
    } 
    
    /** @Author: NGUYEN KHANH TOAN 2018
     *  @Todo: xuất sữ liệu nhân sự
     *  @Param:
     **/
    public function exportGasMember(){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenKhanhToan")
                                ->setLastModifiedBy("NguyenKhanhToan")
                                ->setTitle('DanhSachNhanSu')
                                ->setSubject("Office 2007 XLSX Document")
                                ->setDescription('DanhSachNhanSu')
                                ->setKeywords("office 2007 openxml php")
                                ->setCategory("Gas");
        $row=1;
        $i=1;
        $aData = $_SESSION['dataEmployees']->data;
        $model = $_SESSION['modelEmployees'];
        
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Thông tin nhân sự'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Thông tin nhân sự');
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:T$row");
        $objPHPExcel->getActiveSheet()->getStyle("A$row:M$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã nhân viên');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tên nhân viên');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Chức vụ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Bộ Phận');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tỉnh');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tình trạng hồ sơ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Công ty');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày tham gia(BHXH)');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày ký hợp đồng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày kết thúc HĐ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày vào làm');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày nghỉ việc');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'CMND');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'MST');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Số tài khoản NH');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Chi nhánh NH');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tỉnh làm thẻ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ghi chú');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Người tạo');
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        
        $this->exportGasMemberBody($aData, $objPHPExcel, $row, $model);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);  // STT
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15); // Mã nhân viên
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40); // Tên nhân viên
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12); // Chức vụ
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20); // Bộ Phận
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20); // Tỉnh
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20); // Tình trạng hồ sơ
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(40); // Công ty
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20); // Ngày tham gia(BHXH)
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20); // Ngày ký hợp đồng
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20); // Ngày kết thúc HĐ
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20); // Ngày vào làm
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20); // Ngày nghỉ việc
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20); // CMND
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20); // MST
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(25); // Số tài khoản NH
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20); // Chi nhánh NH
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(20); // Tỉnh làm thẻ
        $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(20); // Ghi chú
        $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(20); // Người tạo
        
        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:B".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $_SESSION['dataEmployees'] = null;
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."DanhSachNhanSu".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
            
            
    } 
    public function exportGasMemberBody($aData, $objPHPExcel, &$row, $model){
        $mAppCache = new AppCache();
        $aProvince = $mAppCache->getListdata('GasProvince', AppCache::LISTDATA_PROVINCE);
        $stt = 1;
        foreach ($aData as $data) {
            $index=1;
            $salary = 'Không';
            if($data->payment_day == UsersProfile::SALARY_YES){
                $salary = 'Có';
            }
            $provinceName = isset($aProvince[$data->province_id]) ? $aProvince[$data->province_id] : '';
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->code_account);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->first_name);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getPositionRoom'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getPositionWork'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $provinceName);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getProfileStatus'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCompany());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getInsuranceStart'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getContractBegin'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getContractEnd'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getDateBeginJob'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getLeaveDate'));
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getIdNumber'), PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getTaxNo'), PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getBankNo'), PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getBankBranch'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getBankProvince'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getNote'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCreatedBy());
            $row++;
            $stt++;
        }
    } 
    /** @Author: NGUYEN KHANH TOAN 2018
     *  @Todo: xuất sữ liệu nhân sự
     *  @Param:
     **/
    public function exportGasMemberBackUp(){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenKhanhToan")
                                ->setLastModifiedBy("NguyenKhanhToan")
                                ->setTitle('DanhSachNhanSu')
                                ->setSubject("Office 2007 XLSX Document")
                                ->setDescription('DanhSachNhanSu')
                                ->setKeywords("office 2007 openxml php")
                                ->setCategory("Gas");
        $row=1;
        $i=1;
        $aData = $_SESSION['dataEmployees']->data;
        $model = $_SESSION['modelEmployees'];
        
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Danh sách nhân sự'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Danh sách nhân sự');
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:M$row");
        $objPHPExcel->getActiveSheet()->getStyle("A$row:M$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Nhận lương');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã nhân viên');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tên nhân viên');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Giới tính');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày sinh');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Địa chỉ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Trình độ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'CMND');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày cấp');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Nơi cấp');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Điện thoại');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Email cty');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Nơi công tác');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Bộ Phận công tác');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Chức vụ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày vào làm');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Lương thời gian');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tài khoản');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngân hàng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tỉnh làm thẻ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Chi nhánh ngân hàng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Cty đóng BHXH');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Lương BHXH');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'MST');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày ký hđ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ký quỹ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'UserName');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Người tạo');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Trạng thái');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày tạo');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tỉnh');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ghi chú');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày nghỉ việc');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tình trạng hồ sơ');
        
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        
        $this->exportGasMemberBodyBackUp($aData, $objPHPExcel, $row, $model);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(60);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(60);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth(20);
        
        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:AD".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()
                                ->getStyle("R$beginBorder:R".$row)->getNumberFormat()
                                ->setFormatCode('#,##0'); 
        $objPHPExcel->getActiveSheet()
                                ->getStyle("X$beginBorder:X".$row)->getNumberFormat()
                                ->setFormatCode('#,##0'); 

        $objPHPExcel->getActiveSheet()->getStyle("R$beginBorder:R".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle("X$beginBorder:X".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        
        $_SESSION['dataEmployees'] = null;
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."DanhSachNhanSu".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
            
            
    } 
    public function exportGasMemberBodyBackUp($aData, $objPHPExcel, &$row, $model){
        $mAppCache = new AppCache();
        $aProvince = $mAppCache->getListdata('GasProvince', AppCache::LISTDATA_PROVINCE);
        $stt = 1;
        foreach ($aData as $data) {
            $index=1;
            $salary = 'Không';
            if($data->payment_day == UsersProfile::SALARY_YES){
                $salary = 'Trả lương';
            }
            $provinceName = isset($aProvince[$data->province_id]) ? $aProvince[$data->province_id] : '';
            $mUsersProfile = $data->rUsersProfile;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $salary);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->code_account);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->first_name);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getGender());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getLastPurchase());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->address);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getTypeEducation'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getIdNumber'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getIdCreatedDate'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getIdProvince'));
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row", $data->phone, PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->email);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getParentCacheSession());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getPositionWork'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getRoleName());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getDateBeginJob());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getBaseSalaryNormal'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getBankNo'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getBankName'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getBankProvince'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getBankBranch'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCompany());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getSalaryInsuranceNormal'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getTaxNo'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getContractBegin'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCredit());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->username);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCreatedBy());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getProfileStatus());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCreatedDate());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $provinceName);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFunctionProfile('getNote'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($mUsersProfile) ? $mUsersProfile->getLeaveDate() : '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($mUsersProfile) ? $mUsersProfile->getProfileStatus() : '');
            $row++;
            $stt++;
        }
    } 
    /** @Author: Pham Thanh Nghia Sep 7, 2018
     *  @Todo: Xuất excel, gasSettle/index
     **/
    public function exportGasSettle(){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("PhamThanhNghia")
                                ->setLastModifiedBy("PhamThanhNghia")
                                ->setTitle('TTTIENNHA')
                                ->setSubject("Office 2007 XLSX Document")
                                ->setDescription('TTTIENNHA')
                                ->setKeywords("office 2007 openxml php")
                                ->setCategory("Gas");
        $row=1;
        $i=1;
        $aData = $_SESSION['dataGasSettleExcel']->data;
        $model = $_SESSION['modelGasSettle'];
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('TT Tiền nhà'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Đề nghị chuyển khoản hợp đồng thuê nhà');
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:E$row");
        $objPHPExcel->getActiveSheet()->getStyle("A$row:E$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tên tài khoản');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Số tài khoản');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tên ngân hàng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tiền');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Nội dung');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'HO TEN');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'NGAN HANG');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'NOI DUNG');
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        
        $this->exportGasSettleBody($aData, $objPHPExcel, $row, $model);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(60);
        
        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->getActiveSheet()->getStyle("E$beginBorder:E".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()
                        ->getStyle("E$beginBorder:E".$row)->getNumberFormat()
                        ->setFormatCode('#,##0');
//        $objPHPExcel->getActiveSheet()->getStyle("D$beginBorder:D".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $_SESSION['dataGasSettleExcel'] = null;
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."TT TIỀN NHÀ".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public function exportGasSettleBody($aData, $objPHPExcel, &$row, $model){
        $stt = 1;
        foreach ($aData as $data) {
            $model = GasSettle::model()->findByPk( $data->id);
            $recordList = $model->rHomeContractDetail;
            foreach ($recordList as $record) {
                $mHomeContract = $record->rHomeContract;
                $mHomeContract->mapJsonFieldOneDecode('JSON_FIELD', 'type_pay_value', 'baseArrayJsonDecode');
                $dateEndPay = MyFormat::modifyDays($record->date_pay, $record->amount_month_pay, '+', 'month', 'd/m/Y');
                $nameAcc    = $mHomeContract->getBeneficiary();
                $nameBank   = $mHomeContract->getBank();
                $note       = $record->getNote();
                $index=1;
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $mHomeContract->getBeneficiary());
                $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row", $mHomeContract->getBankNumber(), PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $mHomeContract->getBank());
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $record->getAmount(false));
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $note);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", MyFunctionCustom::remove_vietnamese_accents($nameAcc));
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", MyFunctionCustom::remove_vietnamese_accents($nameBank));
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", MyFunctionCustom::remove_vietnamese_accents($note));
                
                $row++;
                $stt++;
            }
        }
    } 
    
    /** @Author: NGUYEN KHANH TOAN Sep 14 2018
     *  @Todo: xuất dữ liệu revenue_output
     *  @Param:
     **/
    public function exportRevenueOutput(){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenKhanhToan")
                                ->setLastModifiedBy("NguyenKhanhToan")
                                ->setTitle('BaoCaoDoanhThuSanLuong')
                                ->setSubject("Office 2007 XLSX Document")
                                ->setDescription('BaoCaoDoanhThuSanLuong')
                                ->setKeywords("office 2007 openxml php")
                                ->setCategory("Gas");
       
        $aData = $_SESSION['data-excel'];
        $model = $_SESSION['modelRevenueOutput'];
        
        $this->exportRevenueOutputBody($aData, $objPHPExcel, $row, $model);
        
//        $row--;
//        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

//        $objPHPExcel->getActiveSheet()->getStyle("K$beginBorder:K".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
//        $objPHPExcel->getActiveSheet()->getStyle("D$beginBorder:D".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $_SESSION['data-excel'] = null;
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."BaoCaoDoanhThuSanLuong".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
            
            
    } 
    public function exportRevenueOutputBody($aData, $objPHPExcel, &$row, $model){
        $stt = 1;
        $AGENT_MODEL = $_SESSION['data-excel']['AGENT_MODEL'];
        $OUTPUT = $aData['OUTPUT'];
        $DETAIL_12KG = isset($aData['DETAIL_12KG'])?$aData['DETAIL_12KG']:array();
        $MONEY_BANK = isset($aData['MONEY_BANK'])?$aData['MONEY_BANK']:array();
        $TOTAL_REVENUE = isset($aData['TOTAL_REVENUE'])?$aData['TOTAL_REVENUE']:array();
        $MONTHS = $aData['month'];
        $month = isset($_SESSION['data-excel']['MONTH']) ? $_SESSION['data-excel']['MONTH'] : null;
        $YEARS = $_SESSION['data-excel']['YEARS'];
        
        if(!empty($month)):
//          include 'Revenue_output_form_detail_12kg.php';
            $i =-1;
            foreach(GasStoreCard::$TYPE_CUSTOMER as $key=>$type_customer_code):
                $i++; // sheet
                $row=1;
                $code = $type_customer_code; 
                if(!isset($DETAIL_12KG[$type_customer_code]['days'][$month])){
                    continue;
                }
                $text12 = 'Hộ Gia Đình';
                if(in_array($key, CmsFormatter::$aTypeIdBoMoi)){
                    $text12 = CmsFormatter::$CUSTOMER_BO_MOI[$key];
                }
                // sheet
                $objPHPExcel->createSheet($i);
                $objPHPExcel->setActiveSheetIndex($i);	
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
                $objPHPExcel->getActiveSheet()->setTitle($text12.' 12KG '); 
                $strText  =  'TỔNG SẢN LƯỢNG GAS BÌNH 12KG KH '.$text12.' THÁNG '.$month.' NĂM '.$YEARS;
                $objPHPExcel->getActiveSheet()->setCellValue("A$row", $strText);
                $objPHPExcel->getActiveSheet()->getStyle("A$row:AI$row")->getFont()
                                    ->setBold(true);
                $objPHPExcel->getActiveSheet()->mergeCells("A$row:M$row");
                $objPHPExcel->getActiveSheet()->getStyle("A$row:M$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
                $row++;
                $index=1;
                $beginBorder = $row;

                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
                $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
                $index--;

                $objPHPExcel->getActiveSheet()->getStyle("A$row:AI$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle("A$row:AI$row")->getFont()
                                    ->setBold(true);  
                $objPHPExcel->getActiveSheet()
                                    ->getStyle("C:AI")->getNumberFormat()->setFormatCode('#,##0');                
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
                // end sheet 
                
                $days_of_month = $DETAIL_12KG[$code]['days'][$month];
                $STT = 0; 
                $index = 1;
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tên đại lý');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'BQ');
                foreach($days_of_month as $day):
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $day);
                endforeach;
                
                $index = 1;
                $row++;
                foreach($DETAIL_12KG[$code]['sum_row_total_month'][$month] as $agent_id=>$total_month_agent):
                    $STT++;
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $STT);
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $AGENT_MODEL[$agent_id]->first_name);
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($total_month_agent));
                    $average = round($total_month_agent/count($days_of_month)); 
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($average));
                    foreach($days_of_month as $day):
                        $output_day = isset($DETAIL_12KG[$code][$month][$day][$agent_id])?$DETAIL_12KG[$code][$month][$day][$agent_id]:"";
                        $output_day = $output_day>0? ($output_day):'';
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $output_day);
                    endforeach;
                    $index = 1;
                    $row++;
                endforeach;
                // tong cong binh
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng Cộng ( bình )');
                $sum_col_total_month_all_agent = $DETAIL_12KG[$code]['sum_col_total_month_all_agent'][$month];
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($sum_col_total_month_all_agent));
                $average = round($sum_col_total_month_all_agent/count($days_of_month));
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",($average));
                foreach($days_of_month as $day):																	
                    $sum_day = isset($DETAIL_12KG[$code]['sum_col'][$month][$day])?$DETAIL_12KG[$code]['sum_col'][$month][$day]:"";
                    $sum_day = $sum_day>0? ($sum_day):'';
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",$sum_day);
                endforeach;
                $index = 1;
                $row++;
                // tong cong kg
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng Cộng ( KG )');
                $BINH_KG = KL_BINH_12;
                if($code=='45KG'){
                    $BINH_KG = KL_BINH_45;
                }
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",$sum_col_total_month_all_agent*$BINH_KG);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",($average*$BINH_KG));
                foreach($days_of_month as $day):																
                    $sum_day = isset($DETAIL_12KG[$code]['sum_col'][$month][$day])?$DETAIL_12KG[$code]['sum_col'][$month][$day]:"";
                    $sum_day = ($sum_day*$BINH_KG);
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",$sum_day);
                endforeach;
                $index = 1;
                $row++;
            endforeach; //  foreach(GasStoreCard::$TYPE_CUSTOMER as $key=>$type_customer_code)
            
            foreach(CmsFormatter::$MATERIAL_TYPE_OUTPUT_STATISTIC as $type_output):  // lặp 2 loại bình thống kê là 12 và 45kg   
                $i++;
                $row=1;
                $code = $type_output['code']; 
                if(!isset($OUTPUT[$code]['days'][$month]))
                    continue;
                $days_of_month = $OUTPUT[$code]['days'][$month];
                $STT = 0;
                 $textcode = $code;
                if($code=='45KG'){
                    $textcode = "Bình Bò";
                }
                // sheet
                $objPHPExcel->createSheet($i);
                $objPHPExcel->setActiveSheetIndex($i);	
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
                $objPHPExcel->getActiveSheet()->setTitle(' Tổng '.$textcode); 
                $strText  =  'TỔNG SẢN LƯỢNG '.$textcode.' THÁNG '.$month.' NĂM '.$YEARS;
                $objPHPExcel->getActiveSheet()->setCellValue("A$row", $strText);
                $objPHPExcel->getActiveSheet()->getStyle("A$row:AI$row")->getFont()
                                    ->setBold(true);
                $objPHPExcel->getActiveSheet()->mergeCells("A$row:M$row");
                $objPHPExcel->getActiveSheet()->getStyle("A$row:M$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
                $row++;
                $index=1;
                $beginBorder = $row;
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
                $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
                $index--;
                $objPHPExcel->getActiveSheet()->getStyle("A$row:AI$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle("A$row:AI$row")->getFont()
                                    ->setBold(true);  
                $objPHPExcel->getActiveSheet()
                                    ->getStyle("C:AI")->getNumberFormat()->setFormatCode('#,##0');                   
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
                // end sheet 
                $STT = 0; 
                $index = 1;
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tên đại lý');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'BQ');
                foreach($days_of_month as $day):
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $day);
                endforeach;
                $index = 1;
                $row++;
                foreach($OUTPUT[$code]['sum_row_total_month'][$month] as $agent_id=>$total_month_agent):
                    $STT++;
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $STT);
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $AGENT_MODEL[$agent_id]->first_name);
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($total_month_agent));
                    $average = round($total_month_agent/count($days_of_month));  
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($average));
                    
                    foreach($days_of_month as $day):																
                            $output_day = isset($OUTPUT[$code][$month][$day][$agent_id])?$OUTPUT[$code][$month][$day][$agent_id]:"";
                            $output_day = $output_day>0? ($output_day):'';
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $output_day);
                    endforeach; // end <?php foreach($days_of_month as $day) 
                    $index = 1;
                    $row++;
                endforeach; // end foreach($OUTPUT[$code]['sum_row_total_month'][$month]  
                // tổng cộng
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng Cộng ( bình )');
                $sum_col_total_month_all_agent = $OUTPUT[$code]['sum_col_total_month_all_agent'][$month];
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",($sum_col_total_month_all_agent));
                $average = round($sum_col_total_month_all_agent/count($days_of_month));
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($average));
                foreach($days_of_month as $day): 																	
                    $sum_day = isset($OUTPUT[$code]['sum_col'][$month][$day])?$OUTPUT[$code]['sum_col'][$month][$day]:"";
                    $sum_day = $sum_day>0? ($sum_day):'';
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",$sum_day);
                endforeach; // end <?php foreach($days_of_month as $day)    
                $index = 1;
                $row++;
                // tổng kg
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng Cộng ( KG )');
                $BINH_KG = KL_BINH_12;
                if($code=='45KG'){
                    $BINH_KG = KL_BINH_45;
                }
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($sum_col_total_month_all_agent*$BINH_KG));
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($average*$BINH_KG));
                foreach($days_of_month as $day): 																	
                    $sum_day = isset($OUTPUT[$code]['sum_col'][$month][$day])?$OUTPUT[$code]['sum_col'][$month][$day]:"";
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($sum_day*$BINH_KG));
                endforeach; // end <?php foreach($days_of_month as $day)     
                $index = 1;
                $row++;
            endforeach;
        else : 
            // include 'Revenue_output_year/form_year.php';
            $this->exportRevenueOutputBodyForMonth($aData, $objPHPExcel, $row, $model);
        endif;
    }
    public function exportRevenueOutputBodyForMonth($aData, $objPHPExcel, &$row, $model){
        $AGENT_MODEL = $_SESSION['data-excel']['AGENT_MODEL'];
        $OUTPUT = $aData['OUTPUT'];
        $DETAIL_12KG = isset($aData['DETAIL_12KG'])?$aData['DETAIL_12KG']:array();
        $MONEY_BANK = isset($aData['MONEY_BANK'])?$aData['MONEY_BANK']:array();
        $TOTAL_REVENUE = isset($aData['TOTAL_REVENUE'])?$aData['TOTAL_REVENUE']:array();
        $MONTHS = $aData['month'];
        $YEARS = $_SESSION['data-excel']['YEARS'];
        $i =-1;
        foreach(GasStoreCard::$TYPE_CUSTOMER as $key=>$type_customer_code):
            $i++;
            $row=1;
            $code = $type_customer_code; 
            if(!isset($DETAIL_12KG[$type_customer_code]['total_year']))
                continue;
                $text12 = 'Hộ Gia Đình';
                if(in_array($key, CmsFormatter::$aTypeIdBoMoi)){
                    $text12 = CmsFormatter::$CUSTOMER_BO_MOI[$key];
                }
            // sheet
            $objPHPExcel->createSheet($i);
            $objPHPExcel->setActiveSheetIndex($i);	
            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
            $objPHPExcel->getActiveSheet()->setTitle($text12.' 12KG'); 
            $strText  =  'TỔNG SẢN LƯỢNG GAS BÌNH '.$text12.'  NĂM '.$YEARS;
            $objPHPExcel->getActiveSheet()->setCellValue("A$row", $strText);
            $objPHPExcel->getActiveSheet()->getStyle("A$row:AI$row")->getFont()
                                ->setBold(true);
            $objPHPExcel->getActiveSheet()->mergeCells("A$row:M$row");
            $objPHPExcel->getActiveSheet()->getStyle("A$row:M$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
            $row++;
            $index=1;
            $beginBorder = $row;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
            $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
            $index--;
            $objPHPExcel->getActiveSheet()->getStyle("A$row:AI$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle("A$row:AI$row")->getFont()
                                ->setBold(true);  
            $objPHPExcel->getActiveSheet()
                                ->getStyle("C:AI")->getNumberFormat()->setFormatCode('#,##0');                   
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
            // end sheet 
            $STT = 0;  
            $index = 1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tên đại lý');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'BQ');
            foreach($MONTHS as $month=>$tempM):
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "T".$month);
            endforeach;
            $index = 1;
            $row++;
            foreach($DETAIL_12KG[$code]['total_year']['sum_row'] as $agent_id=>$total_year_agent):
                $STT++;
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $STT);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $AGENT_MODEL[$agent_id]->first_name);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $total_year_agent);
                $average = round($total_year_agent/count($MONTHS));
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $average);
                foreach($MONTHS as $month=>$tempM):
                    $output_month = isset($DETAIL_12KG[$code]['total_year'][$month][$agent_id])?$DETAIL_12KG[$code]['total_year'][$month][$agent_id]:"";
                    $output_month = $output_month>0? ($output_month):'';
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $output_month);
                endforeach;
                $index = 1;
                $row++;
            endforeach; // $DETAIL_12KG[$code]['total_year']['sum_row'] as $agent_id=>$total_year_agent
            //
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",'');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng Cộng ( bình )');
            $sum_col_total_year_all_agent = $DETAIL_12KG[$code]['sum_col_total_year_all_agent'];
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",$sum_col_total_year_all_agent);
            $average = round($sum_col_total_year_all_agent/count($MONTHS));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",$average );
            foreach($MONTHS as $month=>$tempM):
                $sum_month = isset($DETAIL_12KG[$code]['total_year']['sum_col'][$month])?$DETAIL_12KG[$code]['total_year']['sum_col'][$month]:"";
                $sum_month = $sum_month>0 ? ($sum_month):'';
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sum_month);
            endforeach;
            $index = 1;
            $row++;
            //
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",'');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng Cộng ( KG )');
            $BINH_KG = KL_BINH_12;
            if($code=='45KG'){
                $BINH_KG = KL_BINH_45;
            }
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",$sum_col_total_year_all_agent*$BINH_KG );
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",$average*$BINH_KG );
            foreach($MONTHS as $month=>$tempM):
                $sum_month = isset($DETAIL_12KG[$code]['total_year']['sum_col'][$month])?$DETAIL_12KG[$code]['total_year']['sum_col'][$month]:"";
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sum_month*$BINH_KG);
            endforeach;
            $index = 1;
            $row++;
        endforeach; // GasStoreCard::$TYPE_CUSTOMER as $key=>$type_customer_code
        
        foreach(CmsFormatter::$MATERIAL_TYPE_OUTPUT_STATISTIC as $type_output):  // lặp 2 loại bình thống kê là 12 và 45kg        
            $i++;
            $row=1;
            $code = $type_output['code']; 
            $STT = 0;
            $textcode = $code;
            // sheet
            $objPHPExcel->createSheet($i);
            $objPHPExcel->setActiveSheetIndex($i);	
            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
            $objPHPExcel->getActiveSheet()->setTitle(' Tổng '.$textcode); 
            $strText  =  'TỔNG SẢN LƯỢNG GAS BÌNH '.$textcode.' NĂM '.$YEARS;
            $objPHPExcel->getActiveSheet()->setCellValue("A$row", $strText);
            $objPHPExcel->getActiveSheet()->getStyle("A$row:AI$row")->getFont()
                                ->setBold(true);
            $objPHPExcel->getActiveSheet()->mergeCells("A$row:M$row");
            $objPHPExcel->getActiveSheet()->getStyle("A$row:M$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
            $row++;
            $index=1;
            $beginBorder = $row;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
            $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
            $index--;
            $objPHPExcel->getActiveSheet()->getStyle("A$row:AI$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle("A$row:AI$row")->getFont()
                                ->setBold(true);  
            $objPHPExcel->getActiveSheet()
                                ->getStyle("C:AI")->getNumberFormat()->setFormatCode('#,##0');                   
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
            // end sheet 
            $STT = 0; 
            $index = 1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tên đại lý');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'BQ');
            foreach($MONTHS as $month=>$tempM):
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "T".$month);
            endforeach;
            $index = 1;
            $row++;
            foreach($OUTPUT[$code]['total_year']['sum_row'] as $agent_id=>$total_year_agent):
                $STT++;
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $STT);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $AGENT_MODEL[$agent_id]->first_name);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($total_year_agent));
                $average = round($total_year_agent/count($MONTHS));
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($average));

                foreach($MONTHS as $month=>$tempM):
                    $output_month = isset($OUTPUT[$code]['total_year'][$month][$agent_id])?$OUTPUT[$code]['total_year'][$month][$agent_id]:"";
                    $output_month = $output_month>0? ($output_month):'';
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $output_month);
                endforeach;
                $index = 1;
                $row++;
            endforeach; // end foreach($OUTPUT[$code]['sum_row_total_month'][$month]
            // tổng cộng
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng Cộng ( bình )');
            $sum_col_total_year_all_agent = $OUTPUT[$code]['sum_col_total_year_all_agent'];
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",($sum_col_total_year_all_agent));
            $average = round($sum_col_total_year_all_agent/count($MONTHS));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($average));
            foreach($MONTHS as $month=>$tempM):
                $sum_month = isset($OUTPUT[$code]['total_year']['sum_col'][$month])?$OUTPUT[$code]['total_year']['sum_col'][$month]:"";
                $sum_month = $sum_month>0? ($sum_month):'';
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sum_month);
            endforeach;  
            $index = 1;
            $row++;
            // tổng kg
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng Cộng ( KG )');
            $BINH_KG = KL_BINH_12;
            if($code=='45KG'){
                $BINH_KG = KL_BINH_45;
            }
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($sum_col_total_year_all_agent*$BINH_KG));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($average*$BINH_KG));
            foreach($MONTHS as $month=>$tempM):
                $sum_month = isset($OUTPUT[$code]['total_year']['sum_col'][$month])?$OUTPUT[$code]['total_year']['sum_col'][$month]:"";
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sum_month*$BINH_KG);
            endforeach;      
            $index = 1;
            $row++;
        endforeach;
    }
    
    /** @Author: NGUYEN KHANH TOAN 25/09/2018
     *  @Todo: xuất dữ liệu báo cáo code/BQV
     *  @Param:
     **/
    public function exportReportTeleSale(){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenKhanhToan")
                                ->setLastModifiedBy("NguyenKhanhToan")
                                ->setTitle('BaoCaoTelesale')
                                ->setSubject("Office 2007 XLSX Document")
                                ->setDescription('BaoCaoTelesale')
                                ->setKeywords("office 2007 openxml php")
                                ->setCategory("Gas");
        $row=1;
        $i=1;
        $aData = $_SESSION['data-report-telesale'];
        $model = $_SESSION['model-gas-back'];
        $EMPLOYEE_FULLNAME      = isset($aData['EMPLOYEE_FULLNAME']) ? $aData['EMPLOYEE_FULLNAME'] : [];// full name 
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
        $objPHPExcel->getActiveSheet()->setTitle('BÁO CÁO TELESALE');
        $titleCell = 'Báo cáo Code/BQV từ ngày '.$model->date_from.' đến ngày '. $model->date_to;
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", $titleCell);
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:E$row");
        $objPHPExcel->getActiveSheet()->getStyle("A$row:M$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        foreach ($aData['OUT_SELL_EMPLOYEE'] as $user_id => $arrayValue) {
            $trackingValueSumEmployee       = isset($aData['SUM_OUT_TRACKING']['EMPLOYEE'][$user_id]) ? $aData['SUM_OUT_TRACKING']['EMPLOYEE'][$user_id] : '0';
            $saleValueSumEmployee   = isset($arrayValue['value']) ? $arrayValue['value'] : '0';
            $textSum                = $trackingValueSumEmployee.'/'.$saleValueSumEmployee;
            if ($textSum == '0/0'){
                continue ;
            }
            $employeeName       = isset($arrayValue['name']) ? $arrayValue['name']: '';
            $temp               = explode(' ', $employeeName);
            $employeeName   = end($temp);
            $employeeName   .= isset($EMPLOYEE_FULLNAME[$user_id]) ? ExportList::$replaceNewLine.$EMPLOYEE_FULLNAME[$user_id] : '';
                
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $employeeName);
        }
        
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
//        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
//                            ->setBold(true);    	
        $row++;
        
        $this->exportReportTeleSaleBody($aData, $objPHPExcel, $row, $model);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);

        for($i=3; $i <= $index ; $i++):
            $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($i))->setWidth(25);
        endfor;
        
        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

//        $objPHPExcel->getActiveSheet()->getStyle("K$beginBorder:K".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
//        $objPHPExcel->getActiveSheet()->getStyle("D$beginBorder:D".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $_SESSION['data-report-telesale'] = null;
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."BaoCaoTeleSale".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
            
            
    } 
    public function exportReportTeleSaleBody($aData, $objPHPExcel, &$row, $model){
        $LIST_EMPLOYEE          = $aData['EMPLOYEE'];
        $AGENT                  = $aData['AGENT'];
        $OUT_SELL_EMPLOYEE      = $aData['OUT_SELL_EMPLOYEE'];
        $OUT_SELL_SUM_EMPLOYEE  = $aData['OUT_SELL_SUM_EMPLOYEE'];    //tổng sell từng NV
        $OUT_SELL               = $aData['OUT_SELL'];                 //sell theo từng đại lí của từng NV
        $OUT_TRACKING_EMPLOYEE  = $aData['OUT_TRACKING_EMPLOYEE'];    //tổng code từng NV
        $SUM_OUT_TRACKING       = $aData['SUM_OUT_TRACKING'];         //code theo từng đại lí của từng NV
        $OUTPUT_AGENT           = $aData['OUTPUT_AGENT'];

        $stt = 1;
        $index=1;
        $trackingValueSumAll   = isset($SUM_OUT_TRACKING['SUM']) ? $SUM_OUT_TRACKING['SUM'] : '0';
        $saleValueSumAll       = isset($OUT_SELL_SUM_EMPLOYEE['SUM']) ? $OUT_SELL_SUM_EMPLOYEE['SUM'] : '0';
        $sumAll = $trackingValueSumAll.'/'.$saleValueSumAll;
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng: '.$sumAll);
        foreach ($OUT_SELL_EMPLOYEE as $user_id => $arrayValue){
            $trackingValueSumEmployee       = isset($SUM_OUT_TRACKING['EMPLOYEE'][$user_id]) ? $SUM_OUT_TRACKING['EMPLOYEE'][$user_id] : '0';
            $saleValueSumEmployee   = isset($arrayValue['value']) ? $arrayValue['value'] : '0';
            $textSum                = $trackingValueSumEmployee.'/'.$saleValueSumEmployee;
            if ($textSum == '0/0'){
                 continue ;
            }
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $textSum);
        }
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);
        $row++;
        foreach ($OUTPUT_AGENT as $agent_id => $arrayValue){
            $index=1;
            $name           = isset($AGENT[$agent_id]) ? $AGENT[$agent_id] : '';
            $nRef           = isset($arrayValue['tracking']) ? (int)$arrayValue['tracking'] : 0;
            $nSale          = isset($arrayValue['sell']) ? $arrayValue['sell'] : '0';
            $textSumRow     = $nRef. '/' . $nSale;
            if ($textSumRow == '0/0'):
                continue ;
            endif;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $name);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $textSumRow);
            foreach ($OUT_SELL_EMPLOYEE as $user_id => $arrayValue){
                $trackingValueSumEmployee       = isset($SUM_OUT_TRACKING['EMPLOYEE'][$user_id]) ? $SUM_OUT_TRACKING['EMPLOYEE'][$user_id] : '0';
                $saleValueSumEmployee   = isset($arrayValue['value']) ? $arrayValue['value'] : '0';
                $textSum                = $trackingValueSumEmployee.'/'.$saleValueSumEmployee;
                if ($textSum == '0/0'){
                     continue ;
                }
                $trackingvalue  = isset($OUT_TRACKING_EMPLOYEE[$agent_id][$user_id]) ? $OUT_TRACKING_EMPLOYEE[$agent_id][$user_id] : '0';
                $saleValue      = isset($OUT_SELL[$agent_id][$user_id]) ? $OUT_SELL[$agent_id][$user_id] : '0';
                $text = $trackingvalue. '/' .$saleValue;
                if($text == '0/0'){
                    $text = '';
                }
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $text);
            }
            $row++;
            $stt++;
        }
    }
    
    /** @Author: Pham Thanh Nghia Nov 2, 2018
     *  @Todo: Xuất excel, gasSettle/index
     **/
    public function exportGasSettleAll(){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("PhamThanhNghia")
                                ->setLastModifiedBy("PhamThanhNghia")
                                ->setTitle('TTTIENNHA')
                                ->setSubject("Office 2007 XLSX Document")
                                ->setDescription('TTTIENNHA')
                                ->setKeywords("office 2007 openxml php")
                                ->setCategory("Gas");
        $row=1;
        $i=1;
        $aData = $_SESSION['dataGasSettleExcelAll']->data;
        $model = $_SESSION['modelGasSettle'];
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('DS QUYẾT TOÁN'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Danh sách quyết toán');
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:E$row");
        $objPHPExcel->getActiveSheet()->getStyle("A$row:E$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã Số');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Loại');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Người đề nghị');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Số tiền');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Quản lý');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Kế toán trưởng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Giám đốc');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Công ty');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Lý do');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày tạo');
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        
        $this->exportGasSettleAllBody($aData, $objPHPExcel, $row, $model);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->getActiveSheet()->getStyle("E$beginBorder:E".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()
                        ->getStyle("E$beginBorder:E".$row)->getNumberFormat()
                        ->setFormatCode('#,##0');
//        $objPHPExcel->getActiveSheet()->getStyle("D$beginBorder:D".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $_SESSION['dataGasSettleExcelAll'] = null;
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."DS QUYẾT TOÁN".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public function exportGasSettleAllBody($aData, $objPHPExcel, &$row, $model){
        $stt = 1;
        foreach ($aData as $data) {
            $model = GasSettle::model()->findByPk( $data->id);
            $recordList = $model->rHomeContractDetail;
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->code_no);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getTypeText());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getNameUserCreate());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getAmount(false));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getLeaderName());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getChiefAccountantName());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getDirectorName());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCompany());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getReasonText());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCreatedDate());
            $row++;
            $stt++;
        }
    } 
    
    
    /** @Author: NGUYEN KHANH TOAN 25/09/2018
     *  @Todo: xuất dữ liệu báo cáo code/BQV
     *  @Param:
     **/
    public function exportTelesaleSource(){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenKhanhToan")
                                ->setLastModifiedBy("NguyenKhanhToan")
                                ->setTitle('BaoCaoTelesale')
                                ->setSubject("Office 2007 XLSX Document")
                                ->setDescription('BaoCaoTelesale')
                                ->setKeywords("office 2007 openxml php")
                                ->setCategory("Gas");
        $row=1;
        $i=1;
        $aData = $_SESSION['data-telesale-source'];
        $model = $_SESSION['model-customer-draft'];
        $SUM_TELESALE          = $aData['SUM_TELESALE'];
        $mAppCache             = new AppCache();
        $aAgentName            = $mAppCache->getAgentListdata();
        $LIST_TELESALE         = $aData['LIST_TELESALE'];
        $name= Users::getArrObjectUserByRole('', $LIST_TELESALE);
        $aUid = CHtml::listData($name, 'id', 'first_name');
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
        $objPHPExcel->getActiveSheet()->setTitle('BÁO CÁO TELESALE');
        $titleCell = 'Báo cáo Số Đơn hàng/ App từ ngày '.$model->date_from.' đến ngày '. $model->date_to;
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", $titleCell);
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:E$row");
        $objPHPExcel->getActiveSheet()->getStyle("A$row:M$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại lí');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        foreach ($LIST_TELESALE as $key_telesale) {
            $sumSell       = isset($SUM_TELESALE[$key_telesale]['NUMBER_SELL']) ? $SUM_TELESALE[$key_telesale]['NUMBER_SELL'] : '0';
            $sumApp       = isset($SUM_TELESALE[$key_telesale]['NUMBER_APP']) ? $SUM_TELESALE[$key_telesale]['NUMBER_APP'] : '0';
            $textSum                = $sumSell.'/'.$sumApp;
            if ($textSum == '0/0'){
                 continue ;
            }
            $telesaleName   = isset($aUid[$key_telesale]) ? $aUid[$key_telesale]: '';
            $temp           = explode(' ', $telesaleName);
            $telesaleName   = end($temp);
                
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $telesaleName);
        }
        
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
//        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
//                            ->setBold(true);    	
        $row++;
        
        $this->exportTelesaleSourceBody($aData, $objPHPExcel, $row, $model);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);

        for($i=3; $i <= $index ; $i++):
            $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($i))->setWidth(25);
        endfor;
        
        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

//        $objPHPExcel->getActiveSheet()->getStyle("K$beginBorder:K".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
//        $objPHPExcel->getActiveSheet()->getStyle("D$beginBorder:D".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $_SESSION['data-telesale-source'] = null;
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."BaoCaoTeleSale".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
            
            
    } 
    public function exportTelesaleSourceBody($aData, $objPHPExcel, &$row, $model){
        $OUT_PUT = $aData['OUT_PUT'];
        $SUM_TELESALE           = $aData['SUM_TELESALE'];
        $SUM_AGENT              = $aData['SUM_AGENT'];
        $SUM_ALL                = $aData['SUM_ALL'];
        $mAppCache              = new AppCache();
        $aAgentName             = $mAppCache->getAgentListdata();
        $LIST_TELESALE          = $aData['LIST_TELESALE'];
        $LIST_AGENT              = $aData['LIST_AGENT'];
        $name= Users::getArrObjectUserByRole('', $LIST_TELESALE);
        $stt = 1;
        $index=1;
        $numberSellAll   = isset($SUM_ALL['NUMBER_SELL']) ? $SUM_ALL['NUMBER_SELL'] : '0';
        $numberAppAll   = isset($SUM_ALL['NUMBER_APP']) ? $SUM_ALL['NUMBER_APP'] : '0';
        $sumAll = $numberSellAll.'/'.$numberAppAll;
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng: '.$sumAll);
        foreach ($LIST_TELESALE as $key_telesale){
            $sumSell       = isset($SUM_TELESALE[$key_telesale]['NUMBER_SELL']) ? $SUM_TELESALE[$key_telesale]['NUMBER_SELL'] : '0';
            $sumApp       = isset($SUM_TELESALE[$key_telesale]['NUMBER_APP']) ? $SUM_TELESALE[$key_telesale]['NUMBER_APP'] : '0';
            $textSum                = $sumSell.'/'.$sumApp;
            if ($textSum == '0/0'){
                 continue ;
            }
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $textSum);
        }
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);
        $row++;
        foreach ($LIST_AGENT as $key_agent_id ){
            $index=1;
            $nameAgent              = isset($aAgentName[$key_agent_id]) ? $aAgentName[$key_agent_id] : '';
            $numberSellAgent         = isset($SUM_AGENT[$key_agent_id]['NUMBER_SELL']) ? $SUM_AGENT[$key_agent_id]['NUMBER_SELL'] : 0;
            $numberAppAgent          = isset($SUM_AGENT[$key_agent_id]['NUMBER_APP']) ? $SUM_AGENT[$key_agent_id]['NUMBER_APP'] : 0;
            $textSumRow     = $numberSellAgent. '/' . $numberAppAgent;
            if ($textSumRow == '0/0'):
                continue ;
            endif;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $nameAgent);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $textSumRow);
            foreach ($LIST_TELESALE as $key_telesale){
                $numberSell           = isset($OUT_PUT[$key_agent_id][$key_telesale]['NUMBER_SELL']) ? $OUT_PUT[$key_agent_id][$key_telesale]['NUMBER_SELL'] : 0;
                $numberApp           = isset($OUT_PUT[$key_agent_id][$key_telesale]['NUMBER_APP']) ? $OUT_PUT[$key_agent_id][$key_telesale]['NUMBER_APP'] : 0;
                $text                = $numberSell.'/'.$numberApp;
                if ($text == '0/0'){
                    $text = '' ;
                }
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $text);
            }
            $row++;
            $stt++;
        }
    }
    
    public static function exportMonitorUpdate(){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenKhanhToan")
                                        ->setLastModifiedBy("NguyenKhanhToan")
                                        ->setTitle('DANH SÁCH CHI TIẾT LỖI PVKH')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("ListMonitorUpdate")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1; $i=1;
        $aData = $_SESSION['data-excel-monitorUpdate']->data;
        

        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('DANH SÁCH CHI TIẾT LỖI PVKH'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", 'DANH SÁCH CHI TIẾT LỖI PVKH');
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:E$row");
        $objPHPExcel->getActiveSheet()->getStyle("A$row:G$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'S/N');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Nhân viên');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Loại');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Chi tiết');
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:J".$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        self::exportMonitorUpdateBody($aData, $objPHPExcel, $row);
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(50);
       
        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:E".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $_SESSION['data-excel'] = null;
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."MonitorUpdate".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public static function exportMonitorUpdateBody($aData, &$objPHPExcel, &$row) {
        $stt = 1;
        $find = array("<b>","</b>");
        foreach($aData as $data){
            $index=1;
            $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
            $date = $data->getDate()."\n".$data->id;
            $detail = str_replace($find, '', $data->getDetail());
            $detail = str_replace("<br>", "\n", $detail);
            
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getUser());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getType());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $date);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $detail);
            $stt++;
            $row++;
        }
    }
    
    // KHANH Nov 11, 2018
    public static function exportReportMaintenance(){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenKhanhToan")
                                        ->setLastModifiedBy("NguyenKhanhToan")
                                        ->setTitle('BCSL')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("BCSL")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1; $i=1; $endRowFormat = 1;
        $stt =1;
        $aData          = $_SESSION['dataReportMaintenance'];
        $model          = $_SESSION['modelReportMaintenance'];
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Báo cáo đơn hàng bảo trì');
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "Báo cáo đơn hàng bảo trì từ $model->date_from đến $model->date_to");
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '#');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Họ tên NV');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng đơn');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đơn hoàn thành');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        $objPHPExcel->getActiveSheet()->mergeCells("C$row:D$row");
        $objPHPExcel->getActiveSheet()->mergeCells("E$row:F$row");
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:J".$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    
        $row++;
        $index=1;
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mối');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Hộ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mối');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Hộ');
        $row++;
       
        self::exportReportMaintenanceBody($aData, $objPHPExcel, $row, $model);
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $row--;
        
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $beginBorder++;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:B".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle("C$beginBorder:Z".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()
                                ->getStyle("E$beginBorder:J".$row)->getNumberFormat()
                                ->setFormatCode('#,##0');
        $objPHPExcel->getActiveSheet()->getStyle("A1:J".$row)
            ->getAlignment()->setWrapText(true);
        
        $_SESSION['dataReportMaintenance'] = null;
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."BCSL".'.'.'xlsx'.'"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public static function exportReportMaintenanceBody($aData, &$objPHPExcel, &$row, $model) {
        $OUTPUT_UPHOLD              = $aData['aUphold']['OUTPUT']; 
        $SUMALl_UPHOLD              = $aData['aUphold']['SUMALL'];           //tổng đơn mối
        $SUM_COMPLETE_UPHOLD        = $aData['aUphold']['SUM_COMPLETE'];     //tổng đơn mối hoàn thành
        //Uphold HGD
        $OUTPUT_UPHOLD_HGD          = $aData['aUpholdHGD']['OUTPUT'];            
        $SUMALl_UPHOLD_HGD          = $aData['aUpholdHGD']['SUMALL'];        //tổng đơn hộ GD
        $SUM_COMPLETE_UPHOLD_HGD    = $aData['aUpholdHGD']['SUM_COMPLETE'];  //tổng đơn hộ GD hoàn thành

        $Uid = [];
        foreach ($OUTPUT_UPHOLD as $key => $value) {
            $Uid[$key] = $key;
        }
        foreach ($OUTPUT_UPHOLD_HGD as $key => $value) {
            $Uid[$key] = $key;
        }
        $stt = 1;
        $index=1;
        $sumAllUphold             = isset($SUMALl_UPHOLD) ? $SUMALl_UPHOLD : "0";
        $sumAllUpholdHGD          = isset($SUMALl_UPHOLD_HGD) ? $SUMALl_UPHOLD_HGD : "0";
        $sumCompleteUphold        = isset($SUM_COMPLETE_UPHOLD) ? $SUM_COMPLETE_UPHOLD : "0";
        $sumCompleteUpholdHGD     = isset($SUM_COMPLETE_UPHOLD_HGD) ? $SUM_COMPLETE_UPHOLD_HGD : "0";
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sumAllUphold);
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sumAllUpholdHGD);
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sumCompleteUphold);
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sumCompleteUpholdHGD);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);  
        $row++;
        foreach ($Uid as $key_uid_login => $uid_login){
            $index = 1;
            $model = Users::model()->findByPk($key_uid_login);
            $name = isset($model) ? $model->first_name :'';
            $sumMoi           = isset($OUTPUT_UPHOLD[$key_uid_login]['SUM']) ? $OUTPUT_UPHOLD[$key_uid_login]['SUM'] : "0";
            $sumHGD           = isset($OUTPUT_UPHOLD_HGD[$key_uid_login]['SUM']) ? $OUTPUT_UPHOLD_HGD[$key_uid_login]['SUM'] : "0";
            $sumMoiComplete   = isset($OUTPUT_UPHOLD[$key_uid_login]['COMPLETE']) ? $OUTPUT_UPHOLD[$key_uid_login]['COMPLETE'] : "0";
            $sumHGDComplete   = isset($OUTPUT_UPHOLD_HGD[$key_uid_login]['COMPLETE']) ? $OUTPUT_UPHOLD_HGD[$key_uid_login]['COMPLETE'] : "0";
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $name);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sumMoi);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sumHGD);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sumMoiComplete);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sumHGDComplete);
            $stt++;
            $row++;
        }
        $objPHPExcel->getActiveSheet()->getStyle("A$row:Z".$row)->getFont()
                        ->setBold(true);
        $row++;
    }
    
    /** @Author: KHANH TOAN 2018
     *  @Todo:
     *  @Param:
     **/
    public static function listMaterial(){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenKhanhToan")
                                        ->setLastModifiedBy("NguyenKhanhToan")
                                        ->setTitle('VatTu')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("VatTu")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1; $i=1;
        $aData = $_SESSION['data-excel-materials']->data;
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Danh sách vật tư'); 
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Loại vật tư');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Nhóm');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'ID');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã vật tư');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tên vật tư');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đơn vị');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Quy cách vật tư');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Vỏ tương ứng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tên ngắn cho App 24h');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Cẫn xóa');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'URL hình ảnh');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Giá tiền');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Trạng thái');
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("B$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle("C$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        self::listMaterialBody($aData, $objPHPExcel, $row);
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(55);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(25);

        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:N".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("M$beginBorder:M".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:B".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle("J$beginBorder:J".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()
                                ->getStyle("M$beginBorder:M".$row)->getNumberFormat()
                                ->setFormatCode('#,##0'); 
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

//        $_SESSION['data-excel'] = null;
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."VatTu".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public static function listMaterialBody($aData, &$objPHPExcel, &$row) {
        $stt = 1;
        foreach($aData as $data){
            $index=1;
            $type = $data->materials_type?$data->materials_type->name:"";
            $parent_id = $data->parent?$data->parent->name:"";
//            $price = $data->price;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $type);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $parent_id);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->id);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->materials_no);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->name);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->unit);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getDescription());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getVo());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->name_store_card);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getNeedDelete());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getImage());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->price);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getStatus());
            $stt++;
            $row++;
        }
    }
    
    /** @Author: DuongNV May 11,2019
     *  @Todo: export excel Funds
     **/
    public static function listFunds(){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("DuongNV")
                                        ->setLastModifiedBy("DuongNV")
                                        ->setTitle('funds')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("funds")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1; $i=1;
        $aData = $_SESSION['data-excel-funds']->getData();
        
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('DANH SÁCH TIỀN VỐN'); 
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại lý');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Giá vốn');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tháng áp Dụng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Năm áp Dụng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Người tạo');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày tạo');
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        self::listFundsBody($aData, $objPHPExcel, $row);
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);

        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("D$beginBorder:D".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("E$beginBorder:E".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()
                                ->getStyle("C$beginBorder:C".$row)->getNumberFormat()
                                ->setFormatCode('#,##0'); 
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."DSTienVon".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public static function listFundsBody($aData, &$objPHPExcel, &$row) {
        $stt = 1;
        foreach($aData as $data){
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getAgent());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->funds);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getMonthApply());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getYearApply());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCreatedBy());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCreatedDate());
            $stt++;
            $row++;
        }
    }
    
    /** @Author: DuongNV Jun 10,2019
     *  @Todo: export excel gasDebts
     **/
    public static function listGasDebts(){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("DuongNV")
                                        ->setLastModifiedBy("DuongNV")
                                        ->setTitle('Debit')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("Gas Debit")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row    = 1;
        $aData  = $_SESSION['data-excel-debts']->getData();
        
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('DANH SÁCH CÔNG NỢ'); 
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã NV');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Người thực hiện');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'BP');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tỉnh');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày vào làm');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày nghỉ việc');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Số tiền');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Lý do/Diễn giải');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tháng thực hiện');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Loại');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Trạng thái');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày tạo');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Người tạo');
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:A$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("B$row:B$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("J$row:J$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // reason
        $objPHPExcel->getActiveSheet()->getStyle("M$row:M$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // created_date
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        self::listGasDebtsBody($aData, $objPHPExcel, $row);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);

        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:B".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("J$beginBorder:J".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("M$beginBorder:M".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."CongNo".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public static function listGasDebtsBody($aData, &$objPHPExcel, &$row) {
        $aUid   = [];
        foreach ($aData as $mDeb) {
            $aUid[$mDeb->user_id] = $mDeb->user_id;
        }
        $mHr        = new HrSalaryReports();
        $aProfile   = $mHr->getArrayProfile($aUid, true);
        $aUser      = $mHr->getArrayUsersById($aUid);
        $aProvince  = GasProvince::getArrAll();
        $aPosition  = UsersProfile::model()->getArrWorkRoom();
        
        
        $stt = 1;
        $sum = 0;
        foreach($aData as $data){
            $index   = 1;
            $sum    += $data->amount;
            $codeAccount    = isset($aUser[$data->user_id]) ? $aUser[$data->user_id]->code_account : '';
            $position       = isset($aProfile[$data->user_id]) ? $aPosition[$aProfile[$data->user_id]->position_work] : '';
            $province       = !empty($aUser[$data->user_id]->province_id) ? $aProvince[$aUser[$data->user_id]->province_id] : '';
            $beginJob       = isset($aProfile[$data->user_id]) ? MyFormat::dateConverYmdToDmy($aProfile[$data->user_id]->date_begin_job) : '';
            $endJob         = isset($aProfile[$data->user_id]) ? $aProfile[$data->user_id]->leave_date: '';
            if($endJob == '0000-00-00'){
                $endJob = '';
            }
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $codeAccount);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getUserName());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $position);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $province);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $beginJob);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $endJob);
            $objPHPExcel->getActiveSheet()
                                ->getStyle(MyFunctionCustom::columnName($index)."$row")->getNumberFormat()
                                ->setFormatCode('#,##0'); 
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->amount);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->reason);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getMonth());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getTypesText());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getStatusText());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCreatedDate());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCreatedBy());
            $stt++;
            $row++;
        }
        $objPHPExcel->getActiveSheet()->setCellValue("G$row", 'Tổng');
        $objPHPExcel->getActiveSheet()
                    ->getStyle("H$row")->getNumberFormat()
                    ->setFormatCode('#,##0'); 
        $objPHPExcel->getActiveSheet()->setCellValue("H$row", $sum);
        $objPHPExcel->getActiveSheet()
                    ->getStyle("G$row:H$row")
                    ->getFont()
                            ->setBold(true);    
        $row++;
    }
    /** @Author: HaoNH August 15, 2019
     *  @Todo: export branch company
     **/
    public function exportBranchCompany(){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("HaoNH")
                                        ->setLastModifiedBy("HaoNH")
                                        ->setTitle('Branch')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("Branch company")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row    = 1;
        $aData  = $_SESSION['data-excel-branch']->getData();
        
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('DANH SÁCH CÁC CHI NHÁNH'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", 'DANH SÁCH CÁC CHI NHÁNH');
         $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:k$row");
        $objPHPExcel->getActiveSheet()->getStyle("A$row:J$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:J$row")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $index=1;
        $beginBorder = $row++;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Loại');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tên Chi Nhánh/ Địa Điểm');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Công Ty');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tỉnh');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Quận Huyện');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày Tạo');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại lý');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Người Tạo');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Trạng Thái');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Thực tế');
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:A$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("B$row:B$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("J$row:J$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // reason
        $objPHPExcel->getActiveSheet()->getStyle("M$row:M$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // created_date
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        self::setValueBranch($aData, $objPHPExcel, $row);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25);

        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:B".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("J$beginBorder:J".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("M$beginBorder:M".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."Chinhanhcongty".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }  
    }
    /** @Author: HaoNH August 15, 2019
     *  @Todo: set body export branch company
     **/
    public function setValueBranch($aData,$objPHPExcel,&$row){
        
        $stt = 1;
        foreach($aData as $data){        
            $data->to_excel = true;
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getArrayManageBranch()[$data->gender]);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", strip_tags($data->getInfoBranch()));
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row", $data->getCodeAccountBranch(),PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getParent());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getProvince());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getDistrict());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCreatedDate());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", strip_tags($data->getAgentList()));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCreatedBy());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getStatusText());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getRealStatusAgentOfBranch());
            $stt++;
            $row++;
        }
    }
    
}
?>