<?php
class ToExcel1
{
     /** @Author: Pham Thanh Nghia Sep 26, 2018
     *  @Param: admin/callReview/reportCallReview
     **/
    public function exportCallReviewForUserCheck($model){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("PhamThanhNghia")
                                ->setLastModifiedBy("PhamThanhNghia")
                                ->setTitle('Ho Tro KH')
                                ->setSubject("Office 2007 XLSX Document")
                                ->setDescription("Ho Tro KH")
                                ->setKeywords("office 2007 openxml php")
                                ->setCategory("Gas");
        $row=1;
        $i=1;
        $aData = $_SESSION['dataReportForUserCheck'];
        $aDay = $aData['aDay'];
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Danh sách'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "BÁO CÁO KIỂM TRA CUỘC GỌI THEO NV KIỂM TRA");
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:C$row");
        $objPHPExcel->getActiveSheet()->getStyle("A$row:C$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Họ tên người kiểm tra');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng ghi âm');
        foreach ($aDay as $date):
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", substr($date, -2));
        endforeach;
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        
        $this->exportCallReviewForUserCheckBody($aData, $objPHPExcel, $row, $model);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        
        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:B".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
//        $objPHPExcel->getActiveSheet()->getStyle("C$beginBorder:O".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $_SESSION['dataReportForUserCheck'] = null;
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."KiemTraCuocGoiNVKiemTra".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    public function exportCallReviewForUserCheckBody($aData,&$objPHPExcel, &$row, $model){
        $aRes = $aData['data']; 
        $toTalSum = $aData['toTalSum'];
        $aDay = $aData['aDay'];
        $aSumCol = $aData['aSumCol'];
        $aSumRow = $aData['aSumRow'];
        $stt = 1;
        $index=1;
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",$toTalSum);
        foreach ($aDay as $date):
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($aSumCol[$date]) ? $aSumCol[$date] : ' ');
        endforeach;
        $row++;
        foreach ($aSumRow as $key => $sumRow) {
            $model = Users::model()->findByPk($key);
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($model) ? $model->first_name :'');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sumRow);
            foreach ($aDay as $date):
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", (isset($aRes[$key][$date])) ? $aRes[$key][$date] : ' ');
            endforeach;
            $row++;
            $stt++;
        }
    }
    /** @Author: Pham Thanh Nghia Oct 3, 2018
     *  @Todo: Export excel on admin/callReview/reportCallReview/type/2
     **/
    public function exportCallReviewForCallCenter($model){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("PhamThanhNghia")
                                ->setLastModifiedBy("PhamThanhNghia")
                                ->setTitle('Ho Tro KH')
                                ->setSubject("Office 2007 XLSX Document")
                                ->setDescription("Ho Tro KH")
                                ->setKeywords("office 2007 openxml php")
                                ->setCategory("Gas");
        $row=1;
        $i=1;
        $aData = $_SESSION['dataReportForCallCenter'];
        $aReason = $model->getArrayReasonForView();
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Danh sách'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "BÁO CÁO KIỂM TRA CUỘC GỌI THEO NV TỔNG ĐÀI");
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:G$row");
        $objPHPExcel->getActiveSheet()->getStyle("A$row:G$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Họ tên NVTĐ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng ghi âm');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng lỗi');
        foreach ($aReason as $key => $value) {
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $value);
        }
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Điểm TB');
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        
        $this->exportCallReviewForCallCenterBody($aData, $objPHPExcel, $row, $model);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(25);
        
        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("C$beginBorder:N".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:B".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
//        $objPHPExcel->getActiveSheet()->getStyle("C$beginBorder:O".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $_SESSION['dataReportForCallCenter'] = null;
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."KiemTraCuocGoiNVTongDai".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    public function exportCallReviewForCallCenterBody($aData,&$objPHPExcel, &$row, $model){
        $list = $aData['data']; 
        $aSumColReason = $aData['aSumColReason'];
        $aSumRowReason = $aData['aSumRowReason'];
        $aSumRowApp    = $aData['aSumRowApp'];
        $aScore        = $aData['aScore'];
        $aReason = $model->getArrayReasonForView();
        $TotalSumRowApp = array_sum($aSumRowApp);
        $TotalSumRowReason = array_sum($aSumRowReason);
        $stt = 1;
        $index=1;
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $TotalSumRowApp);
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $TotalSumRowReason);
        foreach ($aReason as $key => $value) {
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($aSumColReason[$key]) ? $aSumColReason[$key] : '');
        }
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",'');
        $row++;
        foreach ($list as $key => $value) {
            $model = Users::model()->findByPk($key);
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($model) ? $model->first_name :'');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $aSumRowApp[$key]);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $aSumRowReason[$key]);
            foreach ($aReason as $id => $reasonName): 
                if(isset($value[$id])){
                    $pREASON = number_format($value[$id]/$aSumRowReason[$key]*100, 0, '.', '');
                    $pAPP = number_format($value[$id]/$aSumRowApp[$key]*100, 0, '.', '');
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $value[$id].' / '. $pREASON.'% / '.$pAPP.'% ');
                }else{
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
                }
            endforeach; 
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($aScore[$key]) ? number_format($aScore[$key] , 2) : '');
            $row++;
            $stt++;
        }
    }
    
    /** @Author: Pham Thanh Nghia Oct 3, 2018
     *  @Todo: Export excel on admin/callReview/listDetail
     **/
    public function exportCallReviewForListDetail($model){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("PhamThanhNghia")
                                ->setLastModifiedBy("PhamThanhNghia")
                                ->setTitle('Ho Tro KH')
                                ->setSubject("Office 2007 XLSX Document")
                                ->setDescription("Ho Tro KH")
                                ->setKeywords("office 2007 openxml php")
                                ->setCategory("Gas");
        $row=1;
        $i=1;
        $aData = $_SESSION['dataCallReview']->data;
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Danh sách'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "DANH SÁCH CHI TIẾT KIỂM TRA CUỘC GỌI");
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:G$row");
        $objPHPExcel->getActiveSheet()->getStyle("A$row:G$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại lý');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tỉnh');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'NV gọi');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Lý do');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày kiểm tra');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Họ tên NV kiểm tra');
        
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);  
              
        $row++;
        
        $this->exportCallReviewForListDetailBody($aData, $objPHPExcel, $row, $model);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        
        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle("C$beginBorder:C".$row)->getFont()
                            ->setBold(true);  
        $_SESSION['dataCallReview'] = null;
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."DSChiTietKiemTraCuocGoi".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    public function exportCallReviewForListDetailBody($aData,&$objPHPExcel, &$row, $model){
        $stt = 1;
        foreach ($aData as $data) :
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getAgent());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getProvince());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCallCenter());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getReaSon());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCreatedDate());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCreatesBy());
            $stt++;
            $row++;
        endforeach; 
        
    }
    /** @Author: Pham Thanh Nghia Oct 5, 2018
     *  @Todo: Export excel on gasAppOrder/soldOut
     **/
    public function exportReportSoldOut($model){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("PhamThanhNghia")
                                ->setLastModifiedBy("PhamThanhNghia")
                                ->setTitle('Ho Tro KH')
                                ->setSubject("Office 2007 XLSX Document")
                                ->setDescription("Ho Tro KH")
                                ->setKeywords("office 2007 openxml php")
                                ->setCategory("Gas");
        $row=1;
        $i=1;
        $aData = Yii::app()->session['dataSoldOut'];
        
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Danh sách'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "DANH SÁCH CHI TIẾT KIỂM TRA CUỘC GỌI");
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:G$row");
        $objPHPExcel->getActiveSheet()->getStyle("A$row:G$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Vật tư');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Số lượng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Doanh thu');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Gas dư');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tiền gas dư');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Giá TB - doanh thu chia số lượng');
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);  
              
        $row++;
        
        $this->exportReportSoldOutBody($aData, $objPHPExcel, $row, $model);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        
        $row--;
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
//        $objPHPExcel->getActiveSheet()->getStyle("C$beginBorder:C".$row)->getFont()
//                            ->setBold(true);  
//        $_SESSION['dataSoldOut'] = null;
        $objPHPExcel->getActiveSheet()
                    ->getStyle("B$beginBorder:F".$row)->getNumberFormat()
                    ->setFormatCode('#,##0'); 
        unset(Yii::app()->session['dataSoldOut']); 
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."BaoCaoXuatBan".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    public function exportReportSoldOutBody($rData,&$objPHPExcel, &$row, $model){
        $mAppCache      = new AppCache();
        $aMaterial      = $mAppCache->getListdata('GasMaterials', AppCache::LISTDATA_MATERIAL);
        $aMaterialType  = $mAppCache->getListdata('GasMaterialsType', AppCache::LISTDATA_MATERIAL_TYPE);
        $sumAllQty = $sumAllRevenue = $sumAllRemain = $sumAllRemainAmount = 0;
        foreach ($rData as $materials_type_id => $aInfo):
            $index=1;
            $nameMaterialType = isset($aMaterialType[$materials_type_id]) ? $aMaterialType[$materials_type_id] : '';
            $sumQty = $sumRevenue = $sumRemain = $sumRemainAmount = 0;
            foreach ($aInfo as $materials_id => $aInfo1):
                $nameMaterial = isset($aMaterial[$materials_id]) ? $aMaterial[$materials_id] : $materials_id;
                $remain = $aInfo1['kg_has_gas'] - $aInfo1['kg_empty'] + $aInfo1['pay_back'];
                $sumQty             += $aInfo1['qty'];
                $sumRevenue         += $aInfo1['amount'];
                $sumRemain          += $remain;
                $sumRemainAmount    += $aInfo1['total_gas_du'];
                
                $sumAllQty             += $aInfo1['qty'];
                $sumAllRevenue         += $aInfo1['amount'];
                $sumAllRemain          += $remain;
                $sumAllRemainAmount    += $aInfo1['total_gas_du'];
                
                $priceAverage = 0;
                if($aInfo1['qty'] !=0){
                    $priceAverage = $aInfo1['amount'] / $aInfo1['qty'];
                }
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $nameMaterial);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($aInfo1['qty']));
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $aInfo1['amount'] !=0 ? ($aInfo1['amount']) : '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $remain != 0 ? ($remain) : '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $aInfo1['total_gas_du'] != 0 ? ($aInfo1['total_gas_du']) : '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $priceAverage != 0 ? ($priceAverage) : '');
                $row++;
                $index=1;
            endforeach;
            $priceAverage = 0;
            if($sumQty !=0){
                $priceAverage = $sumRevenue / $sumQty;
            }
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $nameMaterialType);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($sumQty));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sumRevenue != 0 ? ($sumRevenue) : '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sumRemain != 0 ? ($sumRemain) : '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sumRemainAmount != 0 ? ($sumRemainAmount) : '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $priceAverage != 0 ? ($priceAverage) : '');
            $objPHPExcel->getActiveSheet()->getStyle("A$row:F".$row)->getFont()
                    ->setBold(true);  
            $objPHPExcel->getActiveSheet()->getStyle("A$row:F".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $row++;
        endforeach;
        $index=1;
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Sum All');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($sumAllQty));
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($sumAllRevenue));
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($sumAllRemain));
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($sumAllRemainAmount));
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        $objPHPExcel->getActiveSheet()->getStyle("A$row:F".$row)->getFont()
                    ->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:F".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $row++;
    }
    
    
    // Anh Dung Oct 10, 2018
    public function inventoryAllAgent(){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                        ->setLastModifiedBy("NguyenDung")
                                        ->setTitle('inventoryAllAgent')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("inventoryAllAgent")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1; $i=1;
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('BC NXT'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", " Nhập xuất tồn hệ thống đến ".date('d/m/Y'));
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại lý');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Loại');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã Vật Tư');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tên Vật Tư');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tồn Đầu');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Nhập Kho');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Xuất Kho');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tồn Cuối');
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        $this->inventoryAllAgentBody($objPHPExcel, $row);
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);

        $row--;
//        $objPHPExcel->getActiveSheet()->getStyle("F$beginBorder:F".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
//        $objPHPExcel->getActiveSheet()->getStyle("D$beginBorder:G".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()
                                ->getStyle("E$beginBorder:H".$row)->getNumberFormat()
                                ->setFormatCode('#,##0'); 
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."InventoryAllAgent".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    
    public function inventoryAllAgentBody(&$objPHPExcel, &$row) {
        $sta2               = new Sta2();
        $mAppCache          = new AppCache();
        $aRes               = $sta2->getInventoryAllAgent();
        $aMaterials         = $sta2->aMaterials;
        $aMaterialsType     = $mAppCache->getMasterModel('GasMaterialsType', AppCache::ARR_MODEL_MATERIAL_TYPE);
        $aAgent             = $mAppCache->getAgentListdata();
                
        foreach($aAgent as $agent_id => $agentName):
            if(!isset($aRes[$agent_id]) || in_array($agent_id, GasCheck::getAgentNotGentAuto())){
                continue ;
            }
            foreach($aRes[$agent_id] as $materials_type_id => $aInfo1):
                $materialsTypeName = isset($aMaterialsType[$materials_type_id]) ? $aMaterialsType[$materials_type_id]['name'] : $materials_type_id;
                foreach($aInfo1 as $materials_id => $aInfoInventory):
                    $materialsName = isset($aMaterials[$materials_id]) ? $aMaterials[$materials_id]['name'] : $materials_id;
                    $materialsCode = isset($aMaterials[$materials_id]) ? $aMaterials[$materials_id]['materials_no'] : $materials_id;
                    $index=1;
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $agentName);
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $materialsTypeName);
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $materialsCode);
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $materialsName);
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $aInfoInventory['open']);
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $aInfoInventory['import']);
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $aInfoInventory['export']);
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $aInfoInventory['end']);
                    $row++;
                endforeach;// end foreach($aInfo1 as $materials_id
            endforeach;// end foreach($aRes[$agent_id]
        endforeach;
    }
     /** @Author: Pham Thanh Nghia Oct 17, 2018
     *  @Param: admin/callReview/reportCallReview
     **/
    public function exportCallReviewForCallCenterOnDay($model){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("PhamThanhNghia")
                                ->setLastModifiedBy("PhamThanhNghia")
                                ->setTitle('Ho Tro KH')
                                ->setSubject("Office 2007 XLSX Document")
                                ->setDescription("Ho Tro KH")
                                ->setKeywords("office 2007 openxml php")
                                ->setCategory("Gas");
        $row=1;
        $i=1;
        $aData = $_SESSION['dataReportForCallCenterOnDay'];
        $aDay = $aData['aDay'];
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Danh sách'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "BÁO CÁO CUỘC GỌI NV TỔNG ĐÀI ĐƯỢC KIỂM TRA THEO NGÀY");
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:C$row");
        $objPHPExcel->getActiveSheet()->getStyle("A$row:C$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Họ tên NV tổng đài');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng ghi âm');
        foreach ($aDay as $date):
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", substr($date, -2));
        endforeach;
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        
        $this->exportCallReviewForUserCheckBody($aData, $objPHPExcel, $row, $model);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        
        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:B".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
//        $objPHPExcel->getActiveSheet()->getStyle("C$beginBorder:O".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $_SESSION['dataReportForCallCenterOnDay'] = null;
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."KiemTraCuocGoiNVKiemTra".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    public function exportCallReviewForCallCenterOnDayBody($aData,&$objPHPExcel, &$row, $model){
        $aRes = $aData['data']; 
        $toTalSum = $aData['toTalSum'];
        $aDay = $aData['aDay'];
        $aSumCol = $aData['aSumCol'];
        $aSumRow = $aData['aSumRow'];
        $stt = 1;
        $index=1;
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",$toTalSum);
        foreach ($aDay as $date):
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($aSumCol[$date]) ? $aSumCol[$date] : ' ');
        endforeach;
        $row++;
        foreach ($aSumRow as $key => $sumRow) {
            $model = Users::model()->findByPk($key);
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($model) ? $model->first_name :'');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sumRow);
            foreach ($aDay as $date):
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", (isset($aRes[$key][$date])) ? $aRes[$key][$date] : ' ');
            endforeach;
            $row++;
            $stt++;
        }
    }
    /** @Author: Pham Thanh Nghia Oct,31 2018
     *  @Todo: export excel for gasAppOrder/reportRevenue
     **/
    public function exportReportRevenue($model){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("PhamThanhNghia")
                                ->setLastModifiedBy("PhamThanhNghia")
                                ->setTitle('Ho Tro KH')
                                ->setSubject("Office 2007 XLSX Document")
                                ->setDescription("Ho Tro KH")
                                ->setKeywords("office 2007 openxml php")
                                ->setCategory("Gas");
        $row=1;
        $i=1;
        $aData = Yii::app()->session['dataRevenue'];
        
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Danh sách'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "BÁO CÁO DOANH THU BỒ MỐI");
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:I$row");
        $objPHPExcel->getActiveSheet()->getStyle("A$row:I$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại lý');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Doanh thu');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Bình 6');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Bình 12');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Bình 45');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Bình 50');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Gas dư (kg)');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tiền Gas dư');
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);  
              
        $row++;
        
        $this->exportReportRevenueBody($aData, $objPHPExcel, $row, $model);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        $row--;
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
//        $objPHPExcel->getActiveSheet()->getStyle("C$beginBorder:C".$row)->getFont()
//                            ->setBold(true);  
//        $_SESSION['dataSoldOut'] = null;
        $objPHPExcel->getActiveSheet()
                    ->getStyle("B$beginBorder:I".$row)->getNumberFormat()
                    ->setFormatCode('#,##0'); 
        unset(Yii::app()->session['dataRevenue']); 
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."BaoCaoDoanhThuBoMoi".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    public function exportReportRevenueBody($aData,&$objPHPExcel, &$row, $model){
        $BINH_6 = GasMaterialsType::MATERIAL_BINH_6KG;
        $BINH_12 = GasMaterialsType::MATERIAL_BINH_12KG;
        $BINH_45 = GasMaterialsType::MATERIAL_BINH_45KG;
        $BINH_50 = GasMaterialsType::MATERIAL_BINH_50KG;
        $mAppCache = new AppCache();
        $aAgent = $mAppCache->getAgent();
        $stt = 0;
        $sumBinh6 = $sumBinh12 = $sumBinh45 =$sumBinh50 = 0;
        $sumQtyFinal = 0;
        if(!empty($aData)):
        $aRes =$aData['aRes'];
        $aAgentAmount =$aData['aAgentAmount'];
        $aAgentRemain =$aData['aAgentRemain'];
        $aAgentRemainAmount = $aData['aAgentRemainAmount'];
            foreach ($aAgentAmount as $agent_id => $amount): 
            $stt++;
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $aAgent[$agent_id] ? $aAgent[$agent_id]['first_name'] : '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $amount != 0 ? ($amount) : '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($aRes[$agent_id][$BINH_6]) ? $aRes[$agent_id][$BINH_6]['qty'] : '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($aRes[$agent_id][$BINH_12]) ? $aRes[$agent_id][$BINH_12]['qty'] : '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($aRes[$agent_id][$BINH_45]) ? $aRes[$agent_id][$BINH_45]['qty'] : '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($aRes[$agent_id][$BINH_50]) ? $aRes[$agent_id][$BINH_50]['qty'] : '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($aAgentRemain[$agent_id]) && $aAgentRemain[$agent_id] != 0 ? ($aAgentRemain[$agent_id]) : '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($aAgentRemainAmount[$agent_id]) ? ($aAgentRemainAmount[$agent_id]) : '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
//            $objPHPExcel->getActiveSheet()->getStyle("A$row:F".$row)->getFont()
//                        ->setBold(true);
//            $objPHPExcel->getActiveSheet()->getStyle("A$row:F".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sumBinh6 += isset($aRes[$agent_id][$BINH_6]) ? $aRes[$agent_id][$BINH_6]['qty']: 0;
            $sumBinh12 += isset($aRes[$agent_id][$BINH_12]) ? $aRes[$agent_id][$BINH_12]['qty']: 0;
            $sumBinh45 += isset($aRes[$agent_id][$BINH_45]) ? $aRes[$agent_id][$BINH_45]['qty']: 0;
            $sumBinh50 += isset($aRes[$agent_id][$BINH_50]) ? $aRes[$agent_id][$BINH_50]['qty']: 0;
            $row++;
            endforeach;
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", is_array($aAgentAmount) != 0 ? (array_sum($aAgentAmount)) : '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sumBinh6 != 0 ? ($sumBinh6): '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sumBinh12 != 0 ? ($sumBinh12): '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sumBinh45 != 0 ? ($sumBinh45): '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sumBinh50 != 0 ? ($sumBinh50): '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", is_array($aAgentRemain) != 0 ? (array_sum($aAgentRemain)) : '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", is_array($aAgentRemainAmount) != 0 ? (array_sum($aAgentRemainAmount)) : '');
            $objPHPExcel->getActiveSheet()->getStyle("A$row:I".$row)->getFont()
                        ->setBold(true);
//            $objPHPExcel->getActiveSheet()->getStyle("A$row:F".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $row++;
        
        endif;
        
    }
  
    // KHANH Nov 02, 2018
    public static function exportReportUidLogin(){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenKhanhToan")
                                        ->setLastModifiedBy("NguyenKhanhToan")
                                        ->setTitle('BHHGD')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("BHHGD")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1; $i=1; $endRowFormat = 1;
        $aData          = $_SESSION['data-excel'];
        $model          = $_SESSION['data-model'];
        $Uid            = $aData['Uid'];
        $COLUMN         = $aData['COLUMN'];
        $OUTPUT         = $aData['OUTPUT'];
        $name = array();
        $name= Users::getArrObjectUserByRole('', $Uid);
        $aUid = CHtml::listData($name, 'id', 'first_name');
        $stt =1;
        $mAppCache      = new AppCache();
        $aAgent         = $mAppCache->getAgent();
        $agentCode      = isset($aAgent[$model->agent_id]) ? $aAgent[$model->agent_id]['username'] : '';
        
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Báo cáo bán hàng hộ gia đình');
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "Báo cáo bán hàng hộ gia đình từ $model->date_from đến $model->date_to");
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '#');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Họ tên');
        foreach($Uid as $key_uid_login => $uid_login){
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $aUid[$uid_login]);
        }
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:J".$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        self::exportReportUidLoginBody($aData, $objPHPExcel, $row, $model);
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(30);
        $row--;
        
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $beginBorder++;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:B".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle("C$beginBorder:Z".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()
                                ->getStyle("E$beginBorder:J".$row)->getNumberFormat()
                                ->setFormatCode('#,##0');
        $objPHPExcel->getActiveSheet()->getStyle("A1:J".$row)
            ->getAlignment()->setWrapText(true);

        //save file
        $_SESSION['data-excel'] = null;
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."BC_HGD_$agentCode".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public static function exportReportUidLoginBody($aData, &$objPHPExcel, &$row, $model) {
        $Uid            = $aData['Uid'];
        $COLUMN         = $aData['COLUMN'];
        $OUTPUT         = $aData['OUTPUT'];
        $SUM_AMOUNT     = $aData['SUM_AMOUNT'];
        $SUMTYPE        = $aData['SUM_TYPE'];
        $COLUMN_BINH_12 =  $COLUMN['BINH_12'];
        $COLUMN_VAT_TU  =  $COLUMN['VAT_TU'];
        $mAppCache      = new AppCache();
        $aMaterial      = $mAppCache->getListdata('GasMaterials', AppCache::LISTDATA_MATERIAL);
        $index=1;
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng sản lượng (bình 12kg)');
        foreach ($Uid as $key_uid_login => $uid_login) {
            $qty_sum = isset($SUMTYPE[$uid_login]['BINH_12']['QTY']) ? $SUMTYPE[$uid_login]['BINH_12']['QTY'] : '0';
            $amount_sum = isset($SUMTYPE[$uid_login]['BINH_12']['AMOUNT']) ?  ActiveRecord::formatCurrency($SUMTYPE[$uid_login]['BINH_12']['AMOUNT']) : '0';
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $qty_sum.' / '.$amount_sum);
        }
        $objPHPExcel->getActiveSheet()->getStyle("A$row:Z".$row)->getFont()
                        ->setBold(true);
        $row++;
        $stt = 1;
        foreach ($COLUMN_BINH_12 as $key_materials_id => $materials_id){
            $index = 1;
            $nameMaterials      = isset($aMaterial[$materials_id]) ? $aMaterial[$materials_id] : '';
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $nameMaterials);
            foreach ($Uid as $key_uid_login => $uid_login){
                $qty = isset($OUTPUT[$uid_login][$materials_id]) ? ActiveRecord::formatCurrency($OUTPUT[$uid_login][$materials_id]['qty'])  : '0';
                $amount = isset($OUTPUT[$uid_login][$materials_id]) ?  ActiveRecord::formatCurrency($OUTPUT[$uid_login][$materials_id]['amount']) : '0';
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $qty.' / '.$amount);
            }
            $stt++;
            $row++;
        }
        $index = 1;
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Vật tư');
        foreach ($Uid as $key_uid_login => $uid_login) {
            $qty_sum = isset($SUMTYPE[$uid_login]['VAT_TU']['QTY']) ? $SUMTYPE[$uid_login]['VAT_TU']['QTY'] : '0';
            $amount_sum = isset($SUMTYPE[$uid_login]['VAT_TU']['AMOUNT']) ?  $SUMTYPE[$uid_login]['VAT_TU']['AMOUNT'] : '0';;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $qty_sum.' / '.$amount_sum);
        }
        $objPHPExcel->getActiveSheet()->getStyle("A$row:Z".$row)->getFont()
                        ->setBold(true);
        $row++;
        foreach ($COLUMN_VAT_TU as $key_materials_id => $materials_id){
            $index = 1;
            $nameMaterials      = isset($aMaterial[$materials_id]) ? $aMaterial[$materials_id] : '';
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $nameMaterials);
            foreach ($Uid as $key_uid_login => $uid_login){
                $qty_vattu = isset($OUTPUT[$uid_login][$materials_id]) ? ActiveRecord::formatCurrency($OUTPUT[$uid_login][$materials_id]['qty']) : '0';
                $amount_vattu = isset($OUTPUT[$uid_login][$materials_id]) ?  ActiveRecord::formatCurrency($OUTPUT[$uid_login][$materials_id]['amount']) : '0';
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $qty_vattu.' / '.$amount_vattu);
            }
            $stt++;
            $row++;
        }
        $index = 1;
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng Doanh Thu Từng NV');
        foreach ($Uid as $key_uid_login => $uid_login) {
            $sum_uid = isset($SUM_AMOUNT[$uid_login]) ? ActiveRecord::formatCurrency($SUM_AMOUNT[$uid_login]) : "" ;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sum_uid);
        }
        $objPHPExcel->getActiveSheet()->getStyle("A$row:Z".$row)->getFont()
                        ->setBold(true);
        $row++;
        $index = 1;
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng Doanh Thu');
        $sum_qty = isset($OUTPUT['SUM_ALL_QTY']) ? $OUTPUT['SUM_ALL_QTY'] : '';
        $sum_all = isset($OUTPUT['SUM_ALL_AMOUNT']) ? ActiveRecord::formatCurrency($OUTPUT['SUM_ALL_AMOUNT']) : '';
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sum_qty.' / '.$sum_all);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:Z".$row)->getFont()
                        ->setBold(true);
        $row++;
    }
     /** @Author: Pham Thanh Nghia Nov 9, 2018
     *  @Todo: export excel for homeContract/report
     **/
    public function exportReportHomeContract($model){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("PhamThanhNghia")
                                ->setLastModifiedBy("PhamThanhNghia")
                                ->setTitle('Ho Tro KH')
                                ->setSubject("Office 2007 XLSX Document")
                                ->setDescription("Ho Tro KH")
                                ->setKeywords("office 2007 openxml php")
                                ->setCategory("Gas");
        $row=1;
        $i=1;
        $aData = Yii::app()->session['dataReportHomeContract'];
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Danh sách'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "BÁO CÁO THUÊ NHÀ");
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:I$row");
        $objPHPExcel->getActiveSheet()->getStyle("A$row:I$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tỉnh');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại lý');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tình trạng ĐL');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Trạng thái HĐ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Kỳ hạn thanh toán');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày hết hạn');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tiền đặt cọc');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Giá thuê');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Thời hạn (năm)');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng');
        for($i = 1; $i <= 12 ; $i++):
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "T/".$i);
        endfor;
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);  
        $row++;
        $this->exportReportHomeContractBody($aData, $objPHPExcel, $row, $model);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(15);
        $row--;
        $objPHPExcel->getActiveSheet()
                    ->getStyle("B$beginBorder:V".$row)->getNumberFormat()
                    ->setFormatCode('#,##0'); 
        unset(Yii::app()->session['dataReportHomeContract']); 
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."BaoCaoDoanhThuBoMoi".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    public function exportReportHomeContractBody($aData,&$objPHPExcel, &$row, $model){
        $stt = 0;
        $mAppCache = new AppCache();
        $aAgent = $mAppCache->getAgent();
        $aProvince = GasProvince::getArrAll();
        if(!empty($aData)):
        $aRes =$aData['aRes'];
        $aModel =$aData['aModel'];
        $aSubDate = $aData['aSubDate'];
        $aSumMonth = $aData['aSumMonth'];
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');

            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", array_sum($aSumMonth));
            for($i = 1; $i <= 12 ; $i++):
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($aSumMonth[$i]) ? $aSumMonth[$i]: "");
            endfor;
            $objPHPExcel->getActiveSheet()->getStyle("A$row:V".$row)->getFont()
                        ->setBold(true);
            $row++;
            //
            foreach ($aModel as $item): 
            $stt++;
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $aProvince[$item->province_id]);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $aAgent[$item->agent_id] ? $aAgent[$item->agent_id]['first_name'] : '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $aAgent[$item->agent_id] ? (($aAgent[$item->agent_id]['status'] == 1) ? 'Active': 'Inactice') : '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $item->getStatus());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $item->getAmountPay());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $item->getsEndDateContract());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $item->getDeposit(false));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $item->getAmountContract(false));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($aSubDate[$item->id]) ? $aSubDate[$item->id]: "");
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($aRes[$item->id]) ? (array_sum($aRes[$item->id])): "");
            for($i = 1; $i <= 12 ; $i++):
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($aRes[$item->id][$i]) ? ($aRes[$item->id][$i]) : "");
            endfor;
            $row++;
            endforeach;
        endif;
    }
    
    // KHANH Nov 11, 2018
    public static function exportReportQuantity(){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenKhanhToan")
                                        ->setLastModifiedBy("NguyenKhanhToan")
                                        ->setTitle('BCSL')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("BCSL")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1; $i=1; $endRowFormat = 1;
        $stt =1;
        $aData          = $_SESSION['dataReportQuantity'];
        $model          = $_SESSION['data-model'];
        $sum = $aData['sumAll'];
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Báo cáo sản lượng');
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "Báo cáo sản lượng bình từ $model->date_from đến $model->date_to");
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '#');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Họ tên');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Số bình cam bán mới');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Số bình xanh bán mới');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Số bình vàng bán mới');
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:J".$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    
        $row++;
        $index=1;
        $sumCam      = isset($sum['CAM']) ? $sum['CAM'] : "0"; 
        $sumXanh     = isset($sum['XANH']) ? $sum['XANH'] : "0";
        $sumVang     = isset($sum['VANG']) ? $sum['VANG'] : "0";
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sumCam);
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sumXanh);
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sumVang);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);  
        $row++;
        self::exportReportQuantityBody($aData, $objPHPExcel, $row, $model);
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $row--;
        
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $beginBorder++;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:B".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle("C$beginBorder:Z".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()
                                ->getStyle("E$beginBorder:J".$row)->getNumberFormat()
                                ->setFormatCode('#,##0');
        $objPHPExcel->getActiveSheet()->getStyle("A1:J".$row)
            ->getAlignment()->setWrapText(true);
        
        $_SESSION['dataReportQuantity'] = null;
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."BCSL".'.'.'xlsx'.'"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public static function exportReportQuantityBody($aData, &$objPHPExcel, &$row, $model) {
        $Uid  = $aData['aUid'];
        $data = $aData['data'];
        $name = array();
        $name = Users::getArrObjectUserByRole('', $Uid);
        $aUid = CHtml::listData($name, 'id', 'first_name');
        $stt = 1;
        foreach ($data as $key_uid_login => $uid_login){
            $index = 1;
            $name     = isset($aUid[$key_uid_login]) ? $aUid[$key_uid_login] : "";
            $cam      = isset($uid_login['CAM']) ? $uid_login['CAM'] : "0";
            $xanh     = isset($uid_login['XANH']) ? $uid_login['XANH'] : "0"; 
            $vang     = isset($uid_login['VANG']) ? $uid_login['VANG'] : "0"; 
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $name);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $cam);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $xanh);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $vang);
            $stt++;
            $row++;
        }
        $objPHPExcel->getActiveSheet()->getStyle("A$row:Z".$row)->getFont()
                        ->setBold(true);
        $row++;
    }
    
    //Khanh Toan Nov 15
    public static function CashbookAgentForProvinceID($aData){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenKhanhToan")
                                        ->setLastModifiedBy("NguyenKhanhToan")
                                        ->setTitle('SoQuyTienMat')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("SoQuyTienMat")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1;
        $i=1;
        $cmsFormatter = new CmsFormatter();
        $aAgent = $aData['Agent'];
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Sổ quỹ tiền mặt'); 
        foreach ($aAgent as $key => $agent_id){
            $objPHPExcel->getActiveSheet()->setCellValue("A$row", $aData[$agent_id]['title']);
            $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                                ->setBold(true);
            $objPHPExcel->getActiveSheet()->mergeCells("A$row:E$row");
            $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
            $row++;
            $index=1;
            $beginBorder = $row;

            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Diễn Giải');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Loại');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Nhân Viên');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Khách Hàng');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'SL');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Thu');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Chi');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tồn');

            $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
            $index--;

            $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                                ->setBold(true);    	
            $row++;
            self::CashbookAgentForProvinceIDBody($aData[$agent_id]['openingBalance'], $aData[$agent_id]['aModelCashBookDetail'], $objPHPExcel, $row, $i);
//            break;
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(35);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);

            $row--;
            $objPHPExcel->getActiveSheet()->getStyle("F$beginBorder:F".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle("G$beginBorder:I".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    //        $objPHPExcel->getActiveSheet()->getStyle("M$beginBorder:O".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()
                                    ->getStyle("G$beginBorder:I".$row)->getNumberFormat()
                                    ->setFormatCode('#,##0'); 
            $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                            ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $row++;
        }
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."SoQuyTienMat".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public static function CashbookAgentForProvinceIDBody($openingBalance, $aModelCashBookDetail, &$objPHPExcel, &$row, &$i) {
        $mAppCache      = new AppCache();
        $aEmployee      = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MAINTAIN);
        $aCashbookType  = $mAppCache->getMasterModel('GasMasterLookup', AppCache::ARR_MODEL_LOOKUP);
        
        foreach($aModelCashBookDetail as $data){
            $index=1;
            $date           = MyFormat::dateConverYmdToDmy($data->release_date);
            $employeeName   = isset($aEmployee[$data->employee_id]) ? $aEmployee[$data->employee_id] : $data->name_employee;
            $typeName       = isset($aCashbookType[$data->master_lookup_id]) ? $aCashbookType[$data->master_lookup_id]['name'] : '';
            $amount         = $data->amount > 0 ? $data->amount : '';
            $qty            = $data->qty > 0 ? $data->qty : '';
            $in_text = $out_text = '';
            if($data->type==MASTER_TYPE_REVENUE){ // thu
                $openingBalance += $data->amount;
                $in_text = $amount;
            }elseif($data->type==MASTER_TYPE_COST){ // chi
                $openingBalance -= $data->amount;
                $out_text = $amount;
            }
            
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $date);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->description);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $typeName);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $employeeName);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCustomer('first_name'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $qty);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $in_text);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $out_text);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $openingBalance);
//            $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
            $row++;
        }
    }
    
    // KHANH Nov 02, 2018
    public static function exportReportCodePartner(){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenKhanhToan")
                                        ->setLastModifiedBy("NguyenKhanhToan")
                                        ->setTitle('BHHGD')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("BHHGD")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1; $i=1; $endRowFormat = 1;
        $aData          = $_SESSION['dataReportCodePartner'];
        $model          = $_SESSION['data-model'];
        $aDay = $aData['aDay'];

       

        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Báo cáo');
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "Báo cáo từ $model->date_from đến $model->date_to");
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '#');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại lý / Nhân viên');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng');
        foreach ($aDay as $day){
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", substr($day, -2));
        }
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:J".$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        self::exportReportCodePartnerBody($aData, $objPHPExcel, $row, $model);

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        
        $row--;
        
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $beginBorder++;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:B".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle("C$beginBorder:Z".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
       
        $objPHPExcel->getActiveSheet()->getStyle("A1:J".$row)
            ->getAlignment()->setWrapText(true);

        //save file
        $_SESSION['dataReportCodePartner'] = null;
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."BaoCao".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public static function exportReportCodePartnerBody($aData, &$objPHPExcel, &$row, $model) {
        $aAgent     = $aData['aAgent'];
        $aDay       = $aData['aDay'];
        $mAppCache  = new AppCache();
        $aAgentName = $mAppCache->getAgentListdata();
        $SUM_AGENT  = $aData['SUM_AGENT'];
        $SUM_DAY    =  $aData['SUM_DAY'];
        $sumAll     = $aData['SUM_ALL'];
        $OUT_PUT    = $aData['OUTPUT'];
        $index=1;
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sumAll);
        foreach ($aDay as $day) {
            $sumDay = isset($SUM_DAY[$day]) ? $SUM_DAY[$day] : 0;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sumDay);
        }
        $objPHPExcel->getActiveSheet()->getStyle("A$row:Z".$row)->getFont()
                        ->setBold(true);
        $row++;
        $stt = 1;
        foreach ($OUT_PUT as $agent_id => $aReport){
            $index = 1;
            $nameAgent =  isset($aAgentName[$agent_id]) ? $aAgentName[$agent_id] : $agent_id;
            $sumAgent =  isset($SUM_AGENT[$agent_id]) ? $SUM_AGENT[$agent_id] : '';
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $nameAgent);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sumAgent);
            foreach ($aDay as $day){
                $value = isset($OUT_PUT[$agent_id][$day]) ? $OUT_PUT[$agent_id][$day] : '';
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $value);
            }
            $stt++;
            $row++;
        }
        $row++;
    }
    
    public static function CodePartner(){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                        ->setLastModifiedBy("NguyenDung")
                                        ->setTitle('ListCall')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("ListCall")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1; $i=1;
        $aData = $_SESSION['data-excel']->data;
        

        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Danh sách CodePartner'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Danh sách CodePartner');
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $objPHPExcel->getActiveSheet()->getStyle("A$row:G$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'S/N');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Loại');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại lí');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã code');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Trạng thái');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày sử dụng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Khách hàng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'SMS Phone');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Người nhập code');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày tạo');
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:J".$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        self::CodePartnerBody($aData, $objPHPExcel, $row);
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);

        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:B".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("C$beginBorder:C".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle("D$beginBorder:F".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("G$beginBorder:I".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle("J$beginBorder:J".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $_SESSION['data-excel'] = null;
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."ListCall".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public static function CodePartnerBody($aData, &$objPHPExcel, &$row) {
        $stt = 1;
        foreach($aData as $data){
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getType());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getAgent());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCodeNo());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getStatus());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getApplyDate());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCustomer());
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row", $data->getSmsPhone(), PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getApplyByUser());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCreatedDate());
            $stt++;
            $row++;
        }
    }
     /** @Author: Pham Thanh Nghia Dec 2, 2018
     *  @Param: admin/upholdHgd/ReportSum
     **/
    public function exportUpholdHgdReportSum($model){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("PhamThanhNghia")
                                ->setLastModifiedBy("PhamThanhNghia")
                                ->setTitle('Ho Tro KH')
                                ->setSubject("Office 2007 XLSX Document")
                                ->setDescription("Ho Tro KH")
                                ->setKeywords("office 2007 openxml php")
                                ->setCategory("Gas");
        $row=1;
        $i=1;
        $aData = Yii::app()->session['dataUpholdReportSum'];
        $aDay = $aData['aUphold']['aDay'];
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Danh sách'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "BÁO CÁO ĐƠN BẢO TRÌ BÒ MỐI VÀ HỘ GĐ");
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:C$row");
        $objPHPExcel->getActiveSheet()->getStyle("A$row:C$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại lý / Nhân viên');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng');
        foreach ($aDay as $date):
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", substr($date, -2));
        endforeach;
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        
        $this->exportUpholdHgdReportSumBody($aData, $objPHPExcel, $row, $model);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        
        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:B".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
//        $objPHPExcel->getActiveSheet()->getStyle("C$beginBorder:O".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        unset(Yii::app()->session['dataUpholdReportSum']); 
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."KiemTraCuocGoiNVKiemTra".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    public function exportUpholdHgdReportSumBody($data,&$objPHPExcel, &$row, $model){
        $aRes = $data['aUphold']['data']; 
        $toTalSum = $data['aUphold']['toTalSum'];
        $aDay = $data['aUphold']['aDay'];
        $aSumCol = $data['aUphold']['aSumCol'];
        $aSumRow = $data['aUphold']['aSumRow'];
        $aResHGD = $data['aUpholdHGD']['data']; 
        $toTalSumHGD = $data['aUpholdHGD']['toTalSum'];
        $aSumColHGD = $data['aUpholdHGD']['aSumCol'];
        $aSumRowHGD = $data['aUpholdHGD']['aSumRow'];
        $aRow = [];
        foreach ($aSumRow as $key => $value) :
            $aRow[$key] = $key;
        endforeach;
        foreach ($aSumRowHGD as $key => $value) :
            $aRow[$key] = $key;
        endforeach;
        $stt = 1;
        $index=1;
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",$toTalSumHGD.'/'.$toTalSum);
        foreach ($aDay as $date):
            $day = (isset($aSumColHGD[$date])) ? $aSumColHGD[$date] : '0';
            $day .= '/';
            $day .= (isset($aSumCol[$date])) ? $aSumCol[$date] : '0';
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $day);
        endforeach;
        $row++;
        foreach ($aRow as $value):
            $model = Users::model()->findByPk($value);
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($model) ? $model->first_name :'');
            $sumRow = (isset($aSumRowHGD[$value])) ? $aSumRowHGD[$value] : '0';
            $sumRow .= '/';
            $sumRow .= (isset($aSumRow[$value])) ? $aSumRow[$value] : '0';
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sumRow);
            foreach ($aDay as $date):
                $sumDay = (isset($aResHGD[$value][$date])) ? $aResHGD[$value][$date] : '0';
                $sumDay .= '/';
                $sumDay .= (isset($aRes[$value][$date])) ? $aRes[$value][$date] : '0';
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $sumDay);
            endforeach;
            $row++;
            $stt++;
        endforeach;
    }
    /** @Author: Pham Thanh Nghia Dec 10, 2018
     *  @Todo: admin/upholdHgd/index
     **/
    public function exportUpholdHgdIndex($model){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("PhamThanhNghia")
                                        ->setLastModifiedBy("PhamThanhNghia")
                                        ->setTitle('ListCall')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("ListCall")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1; $i=1;
        $aData = $_SESSION['dataUpholdHgdIndex']->data;
        
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('DANH SÁCH BẢO TRÌ HỘ GIA ĐÌNH'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", 'DANH SÁCH BẢO TRÌ HỘ GIA ĐÌNH');
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $objPHPExcel->getActiveSheet()->getStyle("A$row:G$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'S/N');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã số');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Khách hàng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Điện thoại');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại lý');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Nhân viên');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ghi chú');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Người tạo');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Trạng thái');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày tạo');
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:J".$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        $this->exportUpholdHgdIndexBody($aData, $objPHPExcel, $row);
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(60);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);

        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:C".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("E$beginBorder:E".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle("J$beginBorder:K".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $_SESSION['dataUpholdHgdIndex'] = null;
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."DANH SÁCH BẢO TRÌ HỘ GIA ĐÌNH".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    public function exportUpholdHgdIndexBody($aData, &$objPHPExcel, &$row) {
        $stt = 1;
        foreach($aData as $data){
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getDateOnly());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->code_no);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCustomer()." - ".$data->getCustomer("phone")." - ".$data->getCustomer("address"));
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row", $data->getPhone(), PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getAgent());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getEmployee());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getNote(false));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getUidLogin());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getStatus());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCreatedDate());
            $stt++;
            $row++;
        }
    }
    /** @Author: Pham Thanh Nghia Dec 14, 2018
     *  @Todo: admin/gasstorecard/view_store_movement_summary/excel/1
     **/
    public function inventoryByProvince($model){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("PhamThanhNghia")
                                        ->setLastModifiedBy("PhamThanhNghia")
                                        ->setTitle('inventoryAllAgent')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("inventoryAllAgent")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1; $i=1;
        
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('BC NXT'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", " Nhập xuất tồn hệ thống từ ".MyFormat::dateConverYmdToDmy($model->date_from)." đến ".MyFormat::dateConverYmdToDmy($model->date_to));
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại lý');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Loại');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã Vật Tư');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tên Vật Tư');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tồn Đầu');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Nhập Kho');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Xuất Kho');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tồn Cuối');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Vỏ tương ứng');
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        $this->inventoryByProvinceBody($model,$objPHPExcel, $row);
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);

        $row--;
//        $objPHPExcel->getActiveSheet()->getStyle("F$beginBorder:F".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
//        $objPHPExcel->getActiveSheet()->getStyle("D$beginBorder:G".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()
                                ->getStyle("E$beginBorder:H".$row)->getNumberFormat()
                                ->setFormatCode('#,##0'); 
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."NhapXuatTon".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public function inventoryByProvinceBody($model, &$objPHPExcel, &$row) {
        $sta2               = new Sta2();
        $mEmployeeCashbook  = new EmployeeCashbook();
        $aAgent             = $mEmployeeCashbook->getAgentOfProvince($model->province_id_agent);
        
        $aRes               = $sta2->getInventoryByProvince($model, $aAgent);
        $aMaterials         = $sta2->aMaterials;
        $mAppCache          = new AppCache();
        $aMaterialsType     = $mAppCache->getMasterModel('GasMaterialsType', AppCache::ARR_MODEL_MATERIAL_TYPE);
        $aAgentName         = $mAppCache->getAgentListdata();
        foreach($aAgent as $agent_id ):
            if(!isset($aRes[$agent_id]) || 
                ($model->update_note == 1 && in_array($agent_id, GasCheck::getAgentNotGentAuto()))
            ){
                continue ;
            }
            $nameAgent = isset($aAgentName[$agent_id]) ? $aAgentName[$agent_id] : $agent_id;
            foreach($aRes[$agent_id] as $materials_type_id => $aInfo1):
                $materialsTypeName = isset($aMaterialsType[$materials_type_id]) ? $aMaterialsType[$materials_type_id]['name'] : $materials_type_id;
                foreach($aInfo1 as $materials_id => $aInfoInventory):
                    if($aInfoInventory['open']==0 && $aInfoInventory['import'] == 0 && $aInfoInventory['export'] == 0 && $aInfoInventory['end'] == 0){
                        continue ;
                    }
                    $materialsName = $materialsCode = $materialsVoTuongUng = '';
                    if(isset($aMaterials[$materials_id])){
                        $materialsName          =  $aMaterials[$materials_id]['name'];
                        $materialsCode          =  $aMaterials[$materials_id]['materials_no'];
                        $materialsVoId          =  $aMaterials[$materials_id]['materials_id_vo'];
                        $materialsVoTuongUng    =  isset($aMaterials[$materialsVoId]) ? $aMaterials[$materialsVoId]['name'] : $materialsVoId;
                    }
                    $index=1;
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $nameAgent);
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $materialsTypeName);
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $materialsCode);
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $materialsName);
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $aInfoInventory['open']);
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $aInfoInventory['import']);
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $aInfoInventory['export']);
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $aInfoInventory['end']);
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $materialsVoTuongUng);
                    $row++;
                endforeach;// end foreach($aInfo1 as $materials_id
            endforeach;// end foreach($aRes[$agent_id]
        endforeach;
    }
    
    /**
     * @Author: PHAM THANH NGHIA Jan 5 2019
     * @Todo:  admin/usersPrice/reportManage
     */
    public function exportReportManage($model){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("PhamThanhNghia")
                                        ->setLastModifiedBy("PhamThanhNghia")
                                        ->setTitle('ListCall')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("ListCall")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1; $i=1;
        $aData = Yii::app()->session['dataManageCustomer'];
        
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('BÁO CÁO '); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", 'BÁO CÁO QUẢN TRỊ ĐẦU TƯ KHÁCH HÀNG');
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $objPHPExcel->getActiveSheet()->getStyle("A$row:G$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'S/N');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tên KH');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Địa chỉ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Sale');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Giá G');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Giá');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tiền đầu tư');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'SL Bình quân tháng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'SL tháng gần nhất');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Lấy hàng mới nhất');
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:J".$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        $this->exportReportManageBody($aData, $objPHPExcel, $row);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(70);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('k')->setWidth(20);

        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("E$beginBorder:E".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("I$beginBorder:I".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
//        $objPHPExcel->getActiveSheet()->getStyle("J$beginBorder:K".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()
                                ->getStyle("F$beginBorder:I".$row)->getNumberFormat()
                                ->setFormatCode('#,##0');
        unset(Yii::app()->session['dataManageCustomer']);
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."BÁO CÁO QUẢN TRỊ ĐẦU TƯ KHÁCH HÀNG".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    public function exportReportManageBody($aData, &$objPHPExcel, &$row) {
        $sta2 = new Sta2();
        $mGasSupportCustomer = new GasSupportCustomer();
        $stt = 1;
        $aUsersPrice = $aData['aUsersPrice'];
        $aUsers = $aData['aUsers'];
        $aCustomerMonth = $aData['aCustomerMonth']; // SL bình quân
        $aOneMonth = $aData['aOneMonth'];
        $aCustomerInvest = $mGasSupportCustomer->getCacheArraytCustomerInvert(); // cache
        foreach ($aUsersPrice as $item): 
            if(!isset($aCustomerMonth[$item->customer_id]) || !isset($aOneMonth[$item->customer_id])) continue;
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $stt);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($aUsers[$item->customer_id]) ? $aUsers[$item->customer_id]->code_bussiness.' - '.$aUsers[$item->customer_id]->first_name : '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($aUsers[$item->customer_id]) ? $aUsers[$item->customer_id]->address : '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($aUsers[$item->sale_id]) ? $aUsers[$item->sale_id]->first_name : '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($item->price_code != -1) ? $item->price_code : ' ');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $item->price);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $aCustomerInvest[$item->customer_id]);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($aCustomerMonth[$item->customer_id]) ? $sta2->convertOutputCustomerForView($aCustomerMonth[$item->customer_id],ExportList::$replaceNewLine) : '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($aCustomerMonth[$item->customer_id]) ? $aOneMonth[$item->customer_id] : '');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($aUsers[$item->customer_id]) ? MyFormat::dateDmyToYmdForAllIndexSearch($aUsers[$item->customer_id]->last_purchase) : '');
            $stt++;
            $row++;
        endforeach;
    }
    
    /** @Author: KhueNM Sep 27,2019
     *  @Todo: 
     *  @Param:
     **/
    public function mortageReportExcel() {
        try {
            Yii::import('application.extensions.vendors.PHPExcel', true);
            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("NguyenMinhKhue")
                    ->setLastModifiedBy("NguyenMinhKhue")
                    ->setTitle('AppPromotionUser')
                    ->setSubject("Office 2007 XLSX Document")
                    ->setDescription("AppPromotionUser")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Gas");
            $row = 1;
            $model = $_SESSION['mortgage-excel-report'];
            $session = Yii::app()->session;
            $sessSumColBank = isset($session['SESS_SUM_BANK']) ? $session['SESS_SUM_BANK'] : [];

            // 1.sheet 1 borrow report
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
            $titleCell = "BÁO CÁO TÀI SẢN THẾ CHẤP";
            $objPHPExcel->getActiveSheet()->setTitle($titleCell);
            $objPHPExcel->getActiveSheet()->setCellValue("A$row", $titleCell);
            $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                    ->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle("A$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
            $row++;
            $index = 1;
            $beginBorder = $row;

            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++) . "$row", 'STT');
            $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index))->setWidth(40);
            $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index))->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++) . "$row", "Công ty");
            $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index))->setWidth(20);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++) . "$row", 'Ngân hàng');
            $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index))->setWidth(20);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++) . "$row", 'Tài sản thế chấp');
            $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index))->setWidth(10);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++) . "$row", 'Mục');
            $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index))->setWidth(20);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++) . "$row", 'Định giá');
            $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index))->setWidth(10);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++) . "$row", 'Hệ số TSĐB');
            $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index))->setWidth(20);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++) . "$row", 'Giá trị sau quy đổi');
            $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index))->setWidth(20);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++) . "$row", 'Hạn mức đã sử dụng');
            $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index))->setWidth(20);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++) . "$row", 'Ghi chú');
            $objPHPExcel->getActiveSheet()->getColumnDimension(MyFunctionCustom::columnName($index))->setWidth(20);
            $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
            $rowMerge = $row - 1;
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $rowMerge . ':' . MyFunctionCustom::columnName($index - 1) . $rowMerge);
            $objPHPExcel->getActiveSheet()->getStyle("A$row:" . MyFunctionCustom::columnName($index - 1) . $row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle("A1:" . MyFunctionCustom::columnName($index - 1) . $row)->getAlignment()->setWrapText(true);
            $index--;
            $objPHPExcel->getActiveSheet()->getStyle("A$row:" . MyFunctionCustom::columnName($index) . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle("A$row:" . MyFunctionCustom::columnName($index) . $row)->getFont()
                    ->setBold(true);
            $row++;
            $this->mortageReportBodyExcel($objPHPExcel, $row, $model);
            $row--;

            $objPHPExcel->getActiveSheet()
                    ->getStyle("D$row:" . MyFunctionCustom::columnName($index) . $row)->getNumberFormat()
                    ->setFormatCode('#,##0');
            $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:" . MyFunctionCustom::columnName($index) . $row)
                    ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $beginBorder++;

            //save file
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            for ($level = ob_get_level(); $level > 0;  --$level) {
                @ob_end_clean();
            }
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-type: ' . 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="' . 'Mortgage Report' . '.' . 'xlsx' . '"');

            header('Cache-Control: max-age=0');
            $objWriter->save('php://output');
            Yii::app()->end();
        } catch (Exception $e) {
            MyFormat::catchAllException($e);
        }
    }
    /** @Author: KhueNM Sep 27,2019
     *  @Todo:
     *  @Param:
     **/

    public static function mortageReportBodyExcel(&$objPHPExcel, &$row, $model) {
        
        $aData = $model->report();
        $mBorrow        = new Borrow();
        $dataRowspan    = $model->calcRowSpan($aData);
        $dataBorrow     = $mBorrow->report();
        $DETAIL         = $dataBorrow['DETAIL'];
        
        $STT = 1;
        $cCompany = 0;
        $cBank = 0;
        $rowSpanCompany = false;
        $rowSpanBank = false;
        $session = Yii::app()->session;
        $sessSumAll = isset($session['SESS_SUM_ALL']) ? $session['SESS_SUM_ALL'] : [];
        
        foreach ($dataRowspan['ASSETS_SUM'] as $company_id => $aInfo){
            $startRow = $row;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName(1) . "$row", $STT++);
            //
            $aSumCompany['DINH_GIA'] = 0; 
            $aSumCompany['BORROW_AMOUNT'] = 0;
            $aSumCompany['EXCHANGE'] = 0;
            foreach ($aInfo as $bank_id => $aInfo1){
                $startBankRow = $row;
                $rowOtherInfo = 0;
                $amount = isset($DETAIL[$company_id][$bank_id]) ? $DETAIL[$company_id][$bank_id] : 0;
                $aSumBank['DINH_GIA'] = 0; $aSumBank['EXCHANGE'] = 0;
                $aSumCompany['BORROW_AMOUNT']   += $amount;
                
                foreach ($aInfo1 as $asset_id => $aInfo2){
                    $index = 2;
                    
                    if($cCompany != $company_id){
                        $rowSpanCompany = true;
                    }
                    
                    if($cBank != $bank_id){
                        $rowSpanBank = true;
                    }
                    
                    $cCompany = $company_id;
                    $cBank = $bank_id;
                    $item = $aInfo2['data'];
                    $assetSum = $dataRowspan['ASSETS_SUM'][$company_id][$bank_id][$item->asset_id]['sum'];
                    $assetAfterExchange = $assetSum*$item->borrow_ration/100;
                    //calculate total each value
                    $aSumBank['DINH_GIA']       += $assetSum;
                    $aSumBank['EXCHANGE']       += $assetAfterExchange;
                    
                    $aSumCompany['DINH_GIA']    += $assetSum;
                    $aSumCompany['EXCHANGE']    += $assetAfterExchange;
                    if($rowSpanCompany){
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++) . "$row", $item->getCompany());
                        $rowSpanCompany = false;
                    }else{
                        $index++;
                    }
                    $rowBankInfo = $row;
                    $indexBankInfo = $index;
                    
                    if($rowOtherInfo == 0){
                        $rowOtherInfo = $row;
                    } 
                    if($rowSpanBank){
                        //Add more rows if number of assets less than 7. 
                        $numOfAsset = count($aInfo1);
                        if(count($aInfo1) < 7){
                            $row+=(7 - $numOfAsset );
                            $rowOtherInfo = $row - (7 - $numOfAsset );
                        }
                        //Fill all bank info to each row
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($indexBankInfo) . "$rowBankInfo",'Ngân hàng:'.$item->getBank());$rowBankInfo++;
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($indexBankInfo) . "$rowBankInfo",'Hệ số tín chấp:'.$item->getBankRatio());$rowBankInfo++;
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($indexBankInfo) . "$rowBankInfo",'Hạn mức cho vay:'.$item->getBankCredit(true));$rowBankInfo++;
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($indexBankInfo) . "$rowBankInfo",'Hạn mức BLTT:'.$item->getLimitPaymentGuarantee());$rowBankInfo++;
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($indexBankInfo) . "$rowBankInfo",'Ngày cấp HM:'.$item->getLimitDate());$rowBankInfo++;
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($indexBankInfo) . "$rowBankInfo",'Thời gian vay:'.$item->getBorrowTime(true));$rowBankInfo++;
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($indexBankInfo) . "$rowBankInfo",'Lãi suất năm:'.$item->getInterestRate());$rowBankInfo++; 
                        $rowSpanBank = false;
                    }
                    $index++;
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++) . "$rowOtherInfo", $item->getAsset());
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++) . "$rowOtherInfo", $item->showType());
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++) . "$rowOtherInfo", ActiveRecord::formatCurrency($assetSum));
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++) . "$rowOtherInfo", $item->getBorrowRation());
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++) . "$rowOtherInfo", ActiveRecord::formatCurrencyRound($assetAfterExchange));
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName(++$index) . "$rowOtherInfo", $item->getNote());
                    $rowOtherInfo++;
                    $row++;
                }
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName(--$index) . "$startBankRow", ActiveRecord::formatCurrencyRound($amount));
                $objPHPExcel->getActiveSheet()->mergeCells('I' . $startBankRow . ':I' . ($row - 1));
                $row++;
            }
            $objPHPExcel->getActiveSheet()->mergeCells('C' . ($row - 1) . ':D' . ($row - 1));
            $objPHPExcel->getActiveSheet()->getStyle('C' . ($row - 1) . ':D' . ($row - 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->mergeCells('A' . ($row) . ':D' . ($row));
            $objPHPExcel->getActiveSheet()->getStyle('A' . ($row) . ':D' . ($row))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $rowColSpan = $row - 1;
            // Fill total value to each bank 
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName(3) . "$rowColSpan", 'Tổng cộng');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName(6) . "$rowColSpan", ActiveRecord::formatCurrencyRound($aSumBank['DINH_GIA']));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName(8) . "$rowColSpan", ActiveRecord::formatCurrencyRound($aSumBank['EXCHANGE']));
            $objPHPExcel->getActiveSheet()->getStyle('C' . ($row-1) . ':J' . ($row-1))->getFont()
                    ->setBold(true);

            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName(1) . "$row", 'Tổng CTY');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName(6) . "$row", ActiveRecord::formatCurrencyRound($aSumCompany['DINH_GIA']));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName(8) . "$row", ActiveRecord::formatCurrencyRound($aSumCompany['EXCHANGE']));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName(9) . "$row", ActiveRecord::formatCurrencyRound($aSumCompany['BORROW_AMOUNT']));
            $objPHPExcel->getActiveSheet()->getStyle('A' .$row . ':J' . $row)->getFont()
                    ->setBold(true);
            
            $objPHPExcel->getActiveSheet()->mergeCells('A' . $startRow . ':A' . ($row - 1));
            $objPHPExcel->getActiveSheet()->getStyle('A' . $startRow . ':A' . ($row - 1))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $startRow . ':A' . ($row - 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $objPHPExcel->getActiveSheet()->mergeCells('B' . $startRow . ':B' . ($row - 1));
            $objPHPExcel->getActiveSheet()->getStyle('B' . $startRow . ':B' . ($row - 1))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $startRow . ':B' . ($row - 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $row++;
            
        }
        $sumAll['DINH_GIA'] = isset($sessSumAll['DINH_GIA']) ? $sessSumAll['DINH_GIA'] : 0;
        $sumAll['EXCHANGE'] = isset($sessSumAll['EXCHANGE']) ? $sessSumAll['EXCHANGE'] : 0;
        $sumAll['BORROW_AMOUNT'] = isset($sessSumAll['BORROW_AMOUNT']) ? $sessSumAll['BORROW_AMOUNT'] : 0;
        
        $objPHPExcel->getActiveSheet()->mergeCells('A' . ($row) . ':D' . ($row));
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName(1) . "$row", 'Tổng Cộng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName(6) . "$row", ActiveRecord::formatCurrencyRound($sumAll['DINH_GIA']));
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName(8) . "$row", ActiveRecord::formatCurrencyRound($sumAll['EXCHANGE']));
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName(9) . "$row", ActiveRecord::formatCurrencyRound($sumAll['BORROW_AMOUNT']));
        $objPHPExcel->getActiveSheet()->getStyle('A' . ($row) . ':J' . ($row))->getFont()
                    ->setBold(true);
        $row++;
        $startRow = 3;
        $objPHPExcel->getActiveSheet()->getStyle('F' . $startRow . ':F' . ($row))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle('H' . $startRow . ':H' . ($row))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle('I' . $startRow . ':I' . ($row))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle('E' . $startRow . ':E' . ($row))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('G' . $startRow . ':G' . ($row))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    }
    
}?>