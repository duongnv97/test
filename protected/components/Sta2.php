<?php
class Sta2
{
    public $gas24hBegin = '2017-11-01';
    public $aMaterials, $aAgent = [];
    
    public static $TYPE = array(
        TYPE_STORE_CARD_IMPORT => 'IMPORT',
        TYPE_STORE_CARD_EXPORT => 'EXPORT',
    );
    public static function getUidBlock(){
        return [
            138142, // Trương Nguyễn Trãi
//            992793, // Dìu Thế Minh
            1096876, // Phạm Văn Giang
            188, // Dương Chí Quyền
        ];
    }
    
    const REPORT_OUTPUT = 1;
    const REPORT_REMAIN = 2;
    
    const SALARY_TYPE_DEFAULT   = 1;
    const SALARY_TYPE_BOBINH    = 2;
    const SALARY_TYPE_UPHOLD    = 3;
    
    // MAY 05, 2016 define dai ly khogn co ton 60 ngay trong bc ton vo gasreports/inventoryVo
    public static $AGENT_VO = array(
        "2016-04-01" => MyFormat::DAU_KHI_BINH_DINH,
    );
    const InventoryVo60 = 30;
    
    public static function calcStoreMovement($model, $needMore=array()){
    try{// $model is GasStoreCard
        $agent_id   = $model->agent_id;
        $date_from  = $model->date_from_ymd;
        $date_to    = $model->date_to_ymd;
        
        $aRes = [];
        $needMore['model'] = $model;
        // 2. Lấy tồn đầu kỳ cho mỗi vật tư nhập ban đầu bởi đại lý
        $aRes['OPENING_BALANCE_YEAR_BEFORE'] = MyFunctionCustom::getOpeningBalanceYearBefore($agent_id, $needMore);
//        $aRes['OPENING_IMPORT'] = MyFunctionCustom::calcTotalExportImportFixQuery($agent_id, $date_from, TYPE_STORE_CARD_IMPORT);
//        $aRes['OPENING_EXPORT'] = MyFunctionCustom::calcTotalExportImportFixQuery($agent_id, $date_from, TYPE_STORE_CARD_EXPORT);
//        // XONG ĐOẠN CHO calcOpeningStockOnly        
//        // 6. Tính tổng nhập trong khoảng ngày $date_from, $date_to cho từng cấp con 
//        $aRes['IMPORT'] = MyFunctionCustom::calcTotalExportImportInRangeDate($agent_id, '', $date_from, $date_to, TYPE_STORE_CARD_IMPORT);
//        // 7. Tính tổng xuất trong khoảng ngày $date_from, $date_to cho từng cấp con
//        $aRes['EXPORT'] = MyFunctionCustom::calcTotalExportImportInRangeDate($agent_id, '', $date_from, $date_to, TYPE_STORE_CARD_EXPORT);
        MyFunctionCustom::calcTotalExportImportFixQueryV1($aRes, $agent_id, $date_from, $needMore);
        MyFunctionCustom::calcTotalExportImportInRangeDateV1($aRes, $agent_id, '', $date_from, $date_to, $needMore);
        
//        $needMore = array('GetAll'=>1);
        $needMore = [];
        $aRes['aMaterials']     = GasMaterials::getArrayModelParentAndListSubId($needMore);
        $_SESSION['data-excel'] = $aRes;
        $_SESSION['data-model'] = $model;
        return $aRes;
        
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }
    }
    
    /**
     * @Author: DungNT Nov 12, 2015 - GIỐNG HÀM BÊN TRÊN, DÙNG CHUNG CODE
     * @Todo: tính tồn kho tại thời điểm hiện tại cho all vật tư của từng đại lý, 
     * vì không thể tách ra cho từng loại vật tư nên sẽ để get all
     * @Using: sử dụng cho phần đặt hàng Khuyến Mãi của đại lý
     * admin/gasOrderPromotion/create
     */
    public static function getInventoryOfAgent($agent_id, $date_from, $date_to, $aModelMaterial, $needMore=array()){
        try{
        $aRes = array();
        // 2. Lấy tồn đầu kỳ cho mỗi vật tư nhập ban đầu bởi đại lý
        $aRes['OPENING_BALANCE_YEAR_BEFORE'] = MyFunctionCustom::getOpeningBalanceYearBefore($agent_id);
        $aRes['OPENING_IMPORT'] = MyFunctionCustom::calcTotalExportImportFixQuery($agent_id, $date_from, TYPE_STORE_CARD_IMPORT);
        $aRes['OPENING_EXPORT'] = MyFunctionCustom::calcTotalExportImportFixQuery($agent_id, $date_from, TYPE_STORE_CARD_EXPORT);
        // XONG ĐOẠN CHO calcOpeningStockOnly        
        // 6. Tính tổng nhập trong khoảng ngày $date_from, $date_to cho từng cấp con 
        $aRes['IMPORT'] = MyFunctionCustom::calcTotalExportImportInRangeDate($agent_id, '', $date_from, $date_to, TYPE_STORE_CARD_IMPORT);
        // 7. Tính tổng xuất trong khoảng ngày $date_from, $date_to cho từng cấp con
        $aRes['EXPORT'] = MyFunctionCustom::calcTotalExportImportInRangeDate($agent_id, '', $date_from, $date_to, TYPE_STORE_CARD_EXPORT);        
        return Sta2::getInventoryOfAgentFormatData($aRes, $aModelMaterial);
        
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }
    }
    
    /**
     * @Author: DungNT Nov 12, 2015
     * @Todo: format array inventory dạng materials_id => qty
     * để đưa vào array trả về json của autocomplete dưới view
     */
    public static function getInventoryOfAgentFormatData($aRes, $aModelMaterial) {
        $aInventory = array();
        foreach($aModelMaterial as $mMaterial){
            $OpeningYear = isset($aRes['OPENING_BALANCE_YEAR_BEFORE'][$mMaterial->id])?$aRes['OPENING_BALANCE_YEAR_BEFORE'][$mMaterial->id]:0;
            $OpeningYearImport = isset($aRes['OPENING_IMPORT'][$mMaterial->id])?$aRes['OPENING_IMPORT'][$mMaterial->id]:0;
            $OpeningYearExport = isset($aRes['OPENING_EXPORT'][$mMaterial->id])?$aRes['OPENING_EXPORT'][$mMaterial->id]:0;
            $OpeningStock = $OpeningYear+$OpeningYearImport-$OpeningYearExport;
            // Nhập xuất kho
            $totalImport = isset($aRes['IMPORT'][$mMaterial->id])?$aRes['IMPORT'][$mMaterial->id]:0;
            $totalExport = isset($aRes['EXPORT'][$mMaterial->id])?$aRes['EXPORT'][$mMaterial->id]:0;
            // Tồn cuối $remain
            $aInventory[$mMaterial->id] = $OpeningStock+$totalImport-$totalExport;
        }
        return $aInventory;
    }
    
    public static function outputCustomerAddCriteria($model, &$criteria){
//        $criteria->addInCondition("t.type_in_out", CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC); //xuất bán
        $sParamsIn = implode(',', CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC);
        $criteria->addCondition("t.type_in_out IN ($sParamsIn)");
        
        if(!empty($model->agent_id)){
            $criteria->addCondition("t.user_id_create=$model->agent_id");
        }
        if(!empty($model->ext_sale_id)){
            $criteria->addCondition("t.sale_id=$model->ext_sale_id");
        }
        if(!empty($model->customer_id)){
            $criteria->addCondition("t.customer_id=$model->customer_id");
        }
        if(!empty($model->ext_is_maintain)){
            $criteria->addCondition("t.type_customer=$model->ext_is_maintain");
        }
        if(!empty($model->province_id_agent)){
            $aZoneProvince = GasOrders::getZoneNewProvinceId();
            $sParamsIn = implode(',', $aZoneProvince[$model->province_id_agent]);
            $criteria->addCondition("t.province_id_agent IN ($sParamsIn)");
        }
        if(!empty($model->province_id) && is_array($model->province_id)){
            $sParamsIn = implode(',', $model->province_id);
            $criteria->addCondition("t.province_id_agent IN ($sParamsIn)");
        }
        
    }
    
    // báo cáo  sản lượng khách hàng admin/gasreports/Output_customer
    // Ý nghĩa là thống kê để biết tần suất lấy hàng và số lượng của KH này lấy có đúng như trong hợp đồng ko
    public static function Output_customer($model){
    try{
        /* 1. khởi tạo session model KH bò mối id=>model - đã làm khi load page
         * 2. lấy sản lượng thực tế foreach các $MATERIAL_TYPE_BINHBO_OUTPUT, chắc là ko lấy vỏ bình
         * 3. lấy gas dư
         */        
        $aRes = array();
        if(!MyFormat::validDateInput($model->date_from, '-') || !MyFormat::validDateInput($model->date_to, '-') )
            return $aRes;
        $aRes['customer_id'] = array();
        $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
        $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);
        foreach( CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT as $material_type_id){
            // $material_type_id là loại vật tư MATERIAL_TYPE_BINHBO_50,// Gas Bình Bò 50 Kg ....
            // get array id những vật tư có loại là $material_type_id: Gas 12 Kg OR 45kg OR 50
//           Close May 16, 2015 giam dc 3s load page
//            $aMaterialId = GasMaterials::getArrayIdByMaterialTypeId($material_type_id);
//           Close May 16, 2015 giam dc 3s load page
            $criteria = new CDBcriteria();
            self::outputCustomerAddCriteria($model, $criteria);
            DateHelper::searchBetween($date_from, $date_to, 'date_delivery_bigint', $criteria, false);
//           Close May 16, 2015 giam dc 3s load page
//            if(count($aMaterialId)){
//                $criteria->addInCondition("t.materials_id", $aMaterialId);
//            }
//           Close May 16, 2015 giam dc 3s load page
            $criteria->addCondition("t.materials_type_id=$material_type_id");// Add May 16, 2015
            
            $criteria->select = "t.user_id_create, t.date_delivery, t.customer_id, t.sale_id, t.type_customer, "
                    . " sum(qty) as qty ";
            $criteria->group = "t.customer_id, t.date_delivery, t.user_id_create"; 
            $mRes = GasStoreCardDetail::model()->findAll($criteria);   
            self::Output_customer_process_result($aRes, $mRes, $material_type_id, $model);
            //**** begin gas dư
        } // end foreach( CmsFormatter:
        // lấy gas dư
        $needMore['groupDay'] = 1; //group them date_input
        self::getGasRemain($model, $aRes, $date_from, $date_to, $needMore);
        // lấy gas dư
        $aRes['ARR_DAYS'] = MyFormat::getArrayDay($date_from, $date_to);
        
        krsort($aRes['customer_id']);

        return $aRes;
    }catch (Exception $e)
    {
        MyFormat::catchAllException($e);
    }
    }
    
    public static function Output_customer_process_result(&$aRes, $mRes, $material_type_id, $model){
        foreach($mRes as $item){
            // Mar 18, 2015 - sửa không find all customer storecard nữa mà sẽ find incondition
            // item ref: MAR182015 tạm close ý tưởng xử lý kiểu này
//            $aRes['session_customer_id'][$item->customer_id] = $item->customer_id;
            // biến $aRes['session_customer_id'] dùng để giới hạn lại data trong session
            // Mar 18, 2015 - sửa không find all customer storecard nữa mà sẽ find incondition
            
            $aRes['customer_id'][$item->customer_id][$item->user_id_create] = $item->user_id_create; // lưu id customer duy nhất để foreach
            $aRes['customer_id_only'][$item->customer_id] = $item->customer_id;
//            $aRes['data'][$item->customer_id][$item->date_delivery][$item->user_id_create] = $item; // dùng để lấy thông tin của user_id_create, t.date_delivery, t.sale_id
            $aRes['data'][$item->customer_id][$item->user_id_create] = $item; // dùng để lấy thông tin của user_id_create, t.date_delivery, t.sale_id, CÓ LẼ KHÔNG CẦN LẤY NGÀY
            if($model->ext_is_maintain==STORE_CARD_KH_MOI && $material_type_id==MATERIAL_TYPE_BINH_12){
                // Nếu là thống kê KH mối: bình bò hiển thị Kg (chỗ else bên dưới) và bình 12 thì hiển thị bình
                $aRes['OUTPUT_MOI_12'][$item->customer_id][$item->date_delivery][$item->user_id_create] = $item->qty;
            }else{
                if(isset($aRes['OUTPUT'][$item->customer_id][$item->date_delivery][$item->user_id_create]))
                    $aRes['OUTPUT'][$item->customer_id][$item->date_delivery][$item->user_id_create] += $item->qty*CmsFormatter::$MATERIAL_VALUE_KG[$material_type_id];
                else
                    $aRes['OUTPUT'][$item->customer_id][$item->date_delivery][$item->user_id_create] = $item->qty*CmsFormatter::$MATERIAL_VALUE_KG[$material_type_id];
            }
        }
    }
    
    /**
     * @Author: DungNT Aug 16, 2014
     * @Todo: lấy gas dư của KH trong ngày đó hoặc tháng
     * @param: $model,  &$aRes, $date_from, $date_to
     * @Return: $aRes: is final array return
     */      
    public static function getGasRemain($model, &$aRes, $date_from, $date_to, $needMore = []){
        $groupMore = '';
        if(isset($needMore['groupDay'])){
            $groupMore = 't.date_input ,';
        }
        $criteria = new CDBcriteria();
        $criteria->addBetweenCondition("t.date_input",$date_from,$date_to);
        if(isset($needMore['aCustomerId'])){
            $sParamsIn = implode(',', $needMore['aCustomerId']);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.customer_id IN ($sParamsIn)");
            }
        }
        $criteria->addCondition("t.only_gas_remain=0");
        $criteria->select = $groupMore."t.customer_id,t.agent_id, "
                . "sum(amount_gas) as amount_gas, sum(amount) as amount";
        $criteria->group = $groupMore."t.customer_id, t.agent_id"; 
        $mRes = GasRemain::model()->findAll($criteria);
        if(count($mRes)){
            if(isset($needMore['groupDay'])){
                foreach($mRes as $item){
                    // item ref: MAR182015 tạm close ý tưởng xử lý kiểu này
    //                $aRes['session_customer_id'][$item->customer_id] = $item->customer_id;
                    $aRes['GAS_REMAIN'][$item->customer_id][$item->date_input][$item->agent_id] = $item->amount_gas;
                }
            }else{
                foreach($mRes as $item){
                    $aRes['GAS_REMAIN'][$item->customer_id][$item->agent_id] = $item->amount_gas;
            }
        }}
    }
    
    /**
     * @Author: DungNT Dec 28, 2014
     * @Todo: thống kê sản lượng 1 khách hàng theo tháng admin/gasSupportCustomer/view/id/5
     * binh 45, 50, 12
     * @Param: $customer_id
     */
    public static function GetOutputOneCustomer($customer_id, $month_statistic) {
        /* 1. lấy số tháng tính thống kê trong config, tính toán lùi lại tháng theo con số đó 5 hay 6 tháng
         * 2. tính toán
         */
        $date_from = MyFormat::GetDateFromForSupportCustomer($month_statistic);
        $date_to = date("Y-m-d");
        try{
            /* 1. lấy sản lượng thực tế foreach các $MATERIAL_TYPE_BINHBO_OUTPUT, chắc là ko lấy vỏ bình
            *  2. lấy gas dư
            */        
           $aRes = array();
           foreach( CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT as $material_type_id){
               // $material_type_id là loại vật tư MATERIAL_TYPE_BINHBO_50,// Gas Bình Bò 50 Kg ....
               // get array id những vật tư có loại là $material_type_id: Gas 12 Kg OR 45kg OR 50
//           Close May 16, 2015 giam dc 3s load page
//               $aMaterialId = GasMaterials::getArrayIdByMaterialTypeId($material_type_id);
               $criteria = new CDBcriteria();
               $criteria->addBetweenCondition("t.date_delivery", $date_from, $date_to);
               
//               if(count($aMaterialId)){
//                   $criteria->addInCondition("t.materials_id", $aMaterialId);
//               }
               $criteria->compare("t.materials_type_id", $material_type_id);// Add May 16, 2015
               
               $criteria->compare('t.customer_id', $customer_id);
               $criteria->select = "year(t.date_delivery) as statistic_year, month(t.date_delivery) as statistic_month, t.user_id_create, t.date_delivery, t.customer_id, t.sale_id, t.type_customer, "
                       . " sum(qty) as qty ";
               $criteria->group = "year(t.date_delivery), month(t.date_delivery) ";               
               $mRes = GasStoreCardDetail::model()->findAll($criteria);               
               self::GetOutputOneCustomerHanleResult($aRes, $mRes, $material_type_id);
               //**** begin gas dư
           } // end foreach( CmsFormatter:
           // lấy gas dư
           self::GetOutputOneCustomerGetGasRemain($aRes, $date_from, $date_to, $customer_id);
           // end lấy gas dư
           if(isset($aRes['YEAR_MONTH'])){
               // tăng dần asort low to high || Giam dần  arsort() high to low 
               foreach( $aRes['YEAR_MONTH'] as $year => $aMonth){
                   asort($aRes['YEAR_MONTH'][$year]);
               }
           }
           return $aRes;
           
       }catch (Exception $e)
       {
           MyFormat::catchAllException($e);
       }
    }
    
    /** @Author: DungNT Dec 28, 2014 belong to GetOutputOneCustomer */
    public static function GetOutputOneCustomerHanleResult(&$aRes, $mRes, $material_type_id) {
        foreach($mRes as $item){            
            $aRes['YEAR_MONTH'][$item->statistic_year][$item->statistic_month] = $item->statistic_month; // dùng để FOREACH
            if(isset($aRes['OUTPUT'][$material_type_id][$item->statistic_year][$item->statistic_month])){
                $aRes['OUTPUT'][$material_type_id][$item->statistic_year][$item->statistic_month] += $item->qty;
            }else{
                $aRes['OUTPUT'][$material_type_id][$item->statistic_year][$item->statistic_month] = $item->qty;
            }
        }
    }
    
    /** @Author: DungNT Dec 28, 2014 belong to GetOutputOneCustomer */
    public static function GetOutputOneCustomerGetGasRemain(&$aRes, $date_from, $date_to, $customer_id) {
        $criteria = new CDBcriteria();
        $criteria->compare('t.customer_id', $customer_id);
        $criteria->addBetweenCondition("t.date_input",$date_from,$date_to);
        $criteria->addCondition("t.only_gas_remain=0");
        $criteria->select = "year(t.date_input) as statistic_year, month(t.date_input) as statistic_month, t.date_input,t.agent_id, "
                . "sum(amount_gas) as amount_gas";
        $criteria->group = "year(t.date_input), month(t.date_input) "; 
        $mRes = GasRemain::model()->findAll($criteria);
        if(count($mRes)){
            foreach($mRes as $item){
                $aRes['GAS_REMAIN'][$item->statistic_year][$item->statistic_month] = $item->amount_gas;
            }
        }
    }
    
    /**
     * @Author: DungNT Mar 18, 2015
     * @Todo: xuất báo cáo target ho gia dinh: gs pttt,
     * @Param: $model
     */
    public static function Target_monitor_pttt($model) {
        $aRes = array();
        $aMonitoringId = Users::getArrIdUserByArrayRole(array(ROLE_MONITORING_MARKET_DEVELOPMENT));
        // lấy target setup by admin
        GasTargetMonthly::getTargetEachMonth($aMonitoringId, $model->statistic_year, $model->statistic_month,$aRes);
        // get sản lượng thực tế HỘ GIA ĐÌNH
        Statistic::getOutputDailyForTarget($model, GasTargetMonthly::HGD_REAL ,$aRes);
        // lấy model user monitor
        $aRes['MONITOR_MODEL'] = Users::getListOptions($aMonitoringId, array('get_all'=>1));
        // get list agent của monitor
        $aRes['AGENT_OF_MONITOR'] = GasAgentCustomer::GetListAgentIdOfListMonitorId($aMonitoringId);
        return $aRes;
    }
    
    /**
     * @Author: DungNT Apr 04, 2015
     * @Todo: báo cáo target của chuyên viên kinh doanh khu vực - PTTT
     * 1/ bc sản lượng bò
     * 2/ sản lượng mối
     * 3/ sản lượng PTTT
     * @Param: $model
     */
    public static function Target_sale_cvkv($model) {
        /* 1. lấy array id của monitor + id nhân viên của monitor đó
         * 2. get target cho từng nhân viên
         * 3. tính sản lượng bò, mối
         * 4. tính sản lượng PTTT
         * 5. get model của các id đó
         */
        $aRes = array(); // 1,2 here
        $aUidEmployeeId = array(); // 1,2 here
        $aUidInfoName = array(); // 1,2 here
        $aMonitoringId = Users::getArrIdUserByArrayRole(array(ROLE_MONITORING_MARKET_DEVELOPMENT), array('monitor_chuyenvien_kinhdoanh_khuvuc'=>1, 'order'=>'t.province_id asc'));
        $aMonitorEmployee = GasOneMany::GetArrEmployeeOfMonitor($aMonitoringId, $aUidEmployeeId);
        $aUidInfoName = array_merge($aMonitoringId, $aUidEmployeeId);
        $aRes['MONITOR_EMPLOYEE'] =  $aMonitorEmployee;
        // 2. get target cho từng nhân viên - target setup by admin
        GasTargetMonthly::getTargetEachMonth($aUidEmployeeId, $model->statistic_year, $model->statistic_month, $aRes);

        // 3. tính sản lượng bò, mối
        Sta2::getOutputDailyForTarget($model, 'sale_id',$aUidEmployeeId, $aRes);
        // 4. tính sản lượng PTTT
        // 4.1 sản lượng PTTT ở bán hàng PTTT
        $aRes['PTTT_BAN_HANG'] =  GasPtttDailyGoback::GetOutputPttt('GasMaintainSell', 'date_sell', 'quantity_vo_back', $model->statistic_month, $model->statistic_year);
        // 4.2 sản lượng PTTT ở PTTT bình quay về hàng ngày
        $aRes['PTTT_BINH_QUAY_VE'] =  GasPtttDailyGoback::GetOutputPttt('GasPtttDailyGobackDetail', 'maintain_date', 'qty', $model->statistic_month, $model->statistic_year);
        // 5. get model của các id đó $aUidInfoName
//        $aRes['USER_FULL_NAME'] = Users::getListOptions($aUidInfoName, array('get_all'=>1)); // Oct 03, 2015 bỏ dk get_all đi chỉ lấy những thằng nào active thôi
        $aRes['USER_FULL_NAME'] = Users::getListOptions($aUidInfoName, array());
        return $aRes;
    }
    
    
    /**
     * @Author: DungNT Apr 04, 2015
     * @Todo: thống kê daily sản lượng KH bình bò, mối theo từng đại lý vs từng sale, theo 1 tháng submit từ dưới lên
     * @param: $model
     * copy from Statistic::getOutputDailyForTarget
     * @Return: $aRes: is final array return
     */       
    public static function getOutputDailyForTarget($model, $fieldName, $aUidEmployeeId, &$aRes, $needMore=array()){
        /* 1. findAll and group by sale id, type customer id là Loại KH bò hoặc mối của từng sale hoặc từng đại lý
         * sau đó tính toán sum sản lượng cho từng sale 
         * 2. tương tự cho user_id_create là agent id
         * 3. lấy id các loại bình từ loại vật tư
         */        
        //  1/ tính toán lượng bình bán thực tế
        foreach( CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT as $material_type_id){
            // $material_type_id là loại vật tư MATERIAL_TYPE_BINHBO_50,// Gas Bình Bò 50 Kg ....
            // $aMaterialId <=> get array id những vật tư có loại là $material_type_id: Gas 12 Kg OR 45kg OR 50
            //           Close May 16, 2015 giam dc 3s load page
//            $aMaterialId = GasMaterials::getArrayIdByMaterialTypeId($material_type_id);
            $criteria = new CDBcriteria();
//            $criteria->addInCondition("t.type_in_out", CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC); // xuất bán, bán bộ bình, thế chân
            $sParamsIn = implode(',', CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC);
            $criteria->addCondition("t.type_in_out IN ($sParamsIn)");
//            if(count($aMaterialId)){
//                $criteria->addInCondition("t.materials_id", $aMaterialId);
//            }
            $criteria->compare("t.materials_type_id", $material_type_id);// Add May 16, 2015
            
//            $criteria->addInCondition("t.$fieldName", $aUidEmployeeId);
            $sParamsIn = implode(',', $aUidEmployeeId);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.$fieldName IN ($sParamsIn)");
            }
            
            self::TargetAddCriteria($model, $criteria);
            $criteria->addCondition("t.$fieldName<>''");
//            $criteria->addInCondition("t.type_customer", CmsFormatter::$aTypeIdBoMoi);
            $sParamsIn = implode(',', CmsFormatter::$aTypeIdBoMoi);
            $criteria->addCondition("t.type_customer IN ($sParamsIn)");

            $criteria->select = "sum(qty) as qty, t.type_customer, t.$fieldName";
            $criteria->group = "t.$fieldName, t.type_customer"; 
            self::FormatTargetOutput($model, $fieldName, $criteria, $material_type_id, $aRes, $needMore);
        } // end foreach( CmsFormatter::$MATERIAL_TYPE_BINHBO_12          
        self::GetTargetRemain($model, $fieldName, $aUidEmployeeId, $aRes, $needMore);
    }
    
    // belong to getOutputDailyForTarget
    public static function TargetAddCriteria($model, &$criteria){
        $criteria->compare('month(t.date_delivery)', $model->statistic_month);
        $criteria->compare('year(t.date_delivery)', $model->statistic_year);
        $criteria->compare("t.warehouse_new", Users::IS_AGENT_ONLY);
    }
    
    /**
     * @Author: DungNT Apr 04, 2015
     * @Todo: getOutputDailyForTarget
     * @Param: $criteria, $material_type_id, &$aRes, $needMore=array()
     */
    public static function FormatTargetOutput($model, $fieldName, $criteria, $material_type_id, &$aRes, $needMore) {
        $mRes = GasStoreCardDetail::model()->findAll($criteria);                 
        foreach($mRes as $item){ // nhân luôn với số KG của loại bình đó
            if(!isset($aRes['OUTPUT'][$item->$fieldName][$item->type_customer])){
                $aRes['OUTPUT'][$item->$fieldName][$item->type_customer] = $item->qty*CmsFormatter::$MATERIAL_VALUE_KG[$material_type_id]; 
            }else{
                $aRes['OUTPUT'][$item->$fieldName][$item->type_customer] += $item->qty*CmsFormatter::$MATERIAL_VALUE_KG[$material_type_id]; 
            }
            
            // Dec 18, 2015 - fix cho đội CCS Thanh Minh
//            if(!isset($aRes['OUTPUT_QTY'][$item->$fieldName][$item->type_customer][$material_type_id])){
//                $aRes['OUTPUT_QTY'][$item->$fieldName][$item->type_customer][$material_type_id] = $item->qty;
//            }else{
//                $aRes['OUTPUT_QTY'][$item->$fieldName][$item->type_customer][$material_type_id] += $item->qty;
//            }
            // Dec 18, 2015 - fix cho đội CCS Thanh Minh
            
        }// end foreach($mRes as $item){
    }
    
    /**
     * @Author: DungNT Apr 04, 2015
     * @Todo: getOutputDailyForTarget
     * @Param: $criteria, $material_type_id, &$aRes, $needMore=array()
     */
    public static function GetTargetRemain($model, $fieldName, $aUidEmployeeId, &$aRes, $needMore) {
        //  2/ tính toán lượng gas dư ***********
        $criteria = new CDBcriteria();
        $criteria->compare('month(t.date_input)', $model->statistic_month);
        $criteria->compare('year(t.date_input)', $model->statistic_year);
        $criteria->addCondition("t.only_gas_remain=0");
        $criteria->addCondition("t.customer_id<>0");
//        $criteria->addInCondition("t.$fieldName", $aUidEmployeeId);
        $sParamsIn = implode(',', $aUidEmployeeId);
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.$fieldName IN ($sParamsIn)");
        }
        
        $criteria->select = "sum(amount_gas) as amount_gas, t.type_customer, t.$fieldName";
        $criteria->group = "t.$fieldName, t.type_customer"; 
        $mRes = GasRemain::model()->findAll($criteria);
        if(count($mRes)){
            foreach($mRes as $item){
                if(!isset($aRes['REMAIN'][$item->$fieldName][$item->type_customer])){
                    $aRes['REMAIN'][$item->$fieldName][$item->type_customer] = $item->amount_gas;
                }else{
                    $aRes['REMAIN'][$item->$fieldName][$item->type_customer] += $item->amount_gas;
                }
            }
        }
        //  end 2/ tính toán lượng gas dư *************
    }
    
    /**
     * @Author: DungNT Apr 20, 2015
     * @Todo: BC san luong tang giam theo thang
     */
    public function outputChange($model) {
        try {
        /* 1. lấy sản lượng cho từng loại  3: Hộ gia đình 12, 2: mối 12, 1:bình bò
         * gom theo năm tháng
         */
        $aRes = [];
        $this->handleProvince($model);
        $this->getOutputDailyForOutputChange($model, $model->type_user, $aRes);
        if(isset($aRes['AGENT_ID'])){
            $aRes['ROLE_AGENT_MODEL'] = Users::getArrObjecByRoleGroupByProvince(ROLE_AGENT, array('only_id_model'=>1, 'aUid'=>$aRes['AGENT_ID'] ));
        }
        
        if(isset($aRes['OUTPUT_MONTH'])){
            asort($aRes['OUTPUT_MONTH']);
        }
        if(isset($aRes['PROVINCE_ID_FOR'])){
            asort($aRes['PROVINCE_ID_FOR']);
        }
        
        // xu ly san luong giam dan
        // Apr 22, 2015 thêm province_id_agent xử lý sắp xếp tăng giảm theo tỉnh
        if(isset($aRes['OUTPUT_SUM'])){
            // tăng dần asort low to high || Giam dần  arsort() high to low , sort theo value
            foreach($aRes['OUTPUT_SUM'] as $province_id => $aTmp){
                arsort($aRes['OUTPUT_SUM'][$province_id]);
            }
        }
        // xu ly san luong giam dan
        
        return $aRes;
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }
    }

    /** @Author: DungNT Apr 25, 2019
     *  @Todo: handle limit agent by province
     */
    public function handleProvince($model) {
        $mInventoryCustomer = new InventoryCustomer();
        $this->aAgent           = $mInventoryCustomer->getAgentOfProvince($model->province_id_agent);
    }
    
    /**
     * @Author: DungNT Apr 20, 2015
     * @Todo: thống kê theo tháng sản lượng KH bình bò, mối 12kg, hộ GD 12kg theo từng đại lý
     * @Return: $aRes: is final array return
     */       
    public function getOutputDailyForOutputChange($model, $TypeGet, &$aRes, $needMore=array()){
        /* 1. findAll and group by sale id, type customer id là Loại KH bò hoặc mối của từng sale hoặc từng đại lý
         * sau đó tính toán sum sản lượng cho từng sale 
         * 2. tương tự cho user_id_create là agent id
         * 3. lấy id các loại bình từ loại vật tư
         */        
        //  1/ tính toán lượng bình bán thực tế
        foreach( CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT as $material_type_id){
            // $material_type_id là loại vật tư MATERIAL_TYPE_BINHBO_50,// Gas Bình Bò 50 Kg ....
            // $aMaterialId <=> get array id những vật tư có loại là $material_type_id: Gas 12 Kg OR 45kg OR 50
            if($TypeGet == STORE_CARD_KH_BINH_BO && $material_type_id!=MATERIAL_TYPE_BINHBO_50 && $material_type_id!=MATERIAL_TYPE_BINHBO_45 ){
                continue;
            }elseif($TypeGet != STORE_CARD_KH_BINH_BO && $material_type_id!=MATERIAL_TYPE_BINH_12){
                continue;
            }
            
            
            $criteria = new CDBcriteria();
//            $criteria->addInCondition("t.type_in_out", CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC); // xuất bán, bán bộ bình, thế chân
            $sParamsIn = implode(',', CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC);
            $criteria->addCondition("t.type_in_out IN ($sParamsIn)");

//            $aMaterialId = GasMaterials::getArrayIdByMaterialTypeId($material_type_id);
//            if(count($aMaterialId)){
//                $criteria->addInCondition("t.materials_id", $aMaterialId);
//            }
            $criteria->compare("t.materials_type_id", $material_type_id);// Add May 16, 2015            
            
            $this->addCriteriaOutputChange($model, $criteria);
            if($TypeGet == GasStoreCard::TYPE_MOI_12){
                $criteria->compare("t.type_customer", STORE_CARD_KH_MOI);
            }elseif($TypeGet == GasStoreCard::TYPE_HO_GD_12){
                $criteria->compare("t.type_customer", CUSTOMER_HO_GIA_DINH);
            }
            
            $criteria->select = "sum(qty) as qty, t.user_id_create, t.province_id_agent, year(t.date_delivery) as statistic_year, month(t.date_delivery) as statistic_month";
            $criteria->group = "year(t.date_delivery), month(t.date_delivery), t.user_id_create"; 
            $this->formatOutputChange($model, $criteria, $material_type_id, $aRes, $needMore);
        } // end foreach( CmsFormatter::$MATERIAL_TYPE_BINHBO_12
    }
    
    // belong to Output_change
    public function addCriteriaOutputChange($model, &$criteria){
        $criteria->compare('year(t.date_delivery)', $model->statistic_year);
        $criteria->compare("t.warehouse_new", Users::IS_AGENT_ONLY);
        if(count($this->aAgent)){
            $sParamsIn = implode(',', $this->aAgent);
            $criteria->addCondition("t.user_id_create IN ($sParamsIn)");
        }
    }

    /** @Author: DungNT Apr 20, 2015
     *  @Todo: belong to Output_change
     */
    public function formatOutputChange($model, $criteria, $material_type_id, &$aRes, $needMore) {
        $mRes = GasStoreCardDetail::model()->findAll($criteria);                 
        foreach($mRes as $item){ // nhân luôn với số KG của loại bình đó
            if(!isset($aRes['OUTPUT'][$item->user_id_create][$item->statistic_year][$item->statistic_month])){
                $aRes['OUTPUT'][$item->user_id_create][$item->statistic_year][$item->statistic_month] = $item->qty; 
            }else{
                $aRes['OUTPUT'][$item->user_id_create][$item->statistic_year][$item->statistic_month] += $item->qty; 
            }
            
            if(!isset($aRes['OUTPUT_SUM'][$item->province_id_agent][$item->user_id_create])){
                $aRes['OUTPUT_SUM'][$item->province_id_agent][$item->user_id_create] = $item->qty*1; 
            }else{
                $aRes['OUTPUT_SUM'][$item->province_id_agent][$item->user_id_create] += $item->qty; 
            }
            
            if(!isset($aRes['SUM_MONTH'][$item->statistic_month])){
                $aRes['SUM_MONTH'][$item->statistic_month] = $item->qty; 
            }else{
                $aRes['SUM_MONTH'][$item->statistic_month] += $item->qty; 
            }
            if(!isset($aRes['SUM_ALL'])){
                $aRes['SUM_ALL'] = $item->qty; 
            }else{
                $aRes['SUM_ALL'] += $item->qty; 
            }
            
            $aRes['OUTPUT_MONTH'][$item->statistic_month]       = $item->statistic_month;
            $aRes['PROVINCE_ID_FOR'][$item->province_id_agent]  = $item->province_id_agent;
            $aRes['AGENT_ID'][$item->user_id_create]            = $item->user_id_create;
        }// end foreach($mRes as $item){
    }
    
    /**
     * @Author: DungNT Feb 19, 2016 -- only xuất excel
     * @Todo: BC san luong tang giam theo thang
     * admin/gasreports/outputAgent
     */
    public static function OutputAgent($model) {
        try {
        /* 1. lấy sản lượng cho từng loại  3: Hộ gia đình 12, 2: mối 12, 1:bình bò
         * gom theo năm tháng
         */
        $aRes = array();
        self::OutputAgentData($model, $aRes);
        if(isset($aRes['AGENT_ID'])){
            $aRes['ROLE_AGENT_MODEL'] = Users::getArrObjecByRoleGroupByProvince(ROLE_AGENT, array('aUid'=>$aRes['AGENT_ID'] ));
        }
        if(isset($aRes['CUSTOMER_ID'])){
            $aRes['CUSTOMER_MODEL'] = Users::getArrObjectUserByRole(ROLE_CUSTOMER, $aRes['CUSTOMER_ID']);
        }
        return $aRes;
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }
    }
    
    /**
     * @Author: DungNT Feb 19, 2016
     * @Todo: thống kê theo tháng sản lượng KH bình bò, mối 12kg, hộ GD 12kg theo từng đại lý
     * @Return: $aRes: is final array return
     */       
    public static function OutputAgentData($model, &$aRes, $needMore=array()){
        /* 1. findAll and group by agent_id,materials_id  với type customer id là Loại KH bò hoặc mối của từng đại lý
         * sau đó tính toán sum sản lượng cho từng sale 
         * 2. tương tự cho user_id_create là agent id
         * 3. lấy id các loại bình từ loại vật tư
         */
        $TypeCustomer = $model->ext_is_maintain;
        $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
        $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);
//        $aRes['PRICE_SETUP'] = UsersPrice::getPriceHgd($date_from);// sử dụng cho xuất Excel ExportExcel::OutputAgentBody
        $aRes['PRICE_SETUP'] = UsersPriceHgd::getPriceHgd($date_from);// Jan 01, 2016; // sử dụng cho xuất Excel ExportExcel::OutputAgentBody
        //  1/ tính toán lượng bình bán thực tế
//        foreach( CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT as $material_type_id){
            // $material_type_id là loại vật tư MATERIAL_TYPE_BINHBO_50,// Gas Bình Bò 50 Kg ....
            $criteria = new CDBcriteria();
//            $criteria->addInCondition("t.type_in_out", CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC); // xuất bán, bán bộ bình, thế chân
            $sParamsIn = implode(',', CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC);
            $criteria->addCondition("t.type_in_out IN ($sParamsIn)");
            
            $criteria->addBetweenCondition("t.date_delivery",$date_from,$date_to);
//            $criteria->addInCondition("t.materials_type_id", CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT);// Add May 16, 2015
            $sParamsIn = implode(',', CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT);
            $criteria->addCondition("t.materials_type_id IN ($sParamsIn)");
            $criteria->compare("t.type_customer", $TypeCustomer);
            
            $criteria->select = "sum(qty) as qty, t.customer_id, t.user_id_create, t.province_id_agent, t.materials_type_id, t.materials_id";
            $criteria->group = "t.user_id_create, t.customer_id, t.materials_id"; 
            if($TypeCustomer == CUSTOMER_HO_GIA_DINH){
                $criteria->group = "t.user_id_create, t.materials_id"; 
            }
            
            self::OutputAgentFormat($model, $TypeCustomer, $criteria, $aRes, $needMore);
//        } // end foreach( CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT
        self::OutputAgentGetRemain($date_from, $date_to, $aRes, $needMore);
    }
    
    public static function OutputAgentFormat($model, $TypeCustomer, $criteria, &$aRes, $needMore) {
        $mRes = GasStoreCardDetail::model()->findAll($criteria);
        if($TypeCustomer == CUSTOMER_HO_GIA_DINH){
            foreach($mRes as $item){
                $aRes['OUTPUT'][$item->user_id_create][$item->materials_id] = $item->qty; 
                $aRes['AGENT_ID'][$item->user_id_create] = $item->user_id_create;
            }// end foreach($mRes as $item){
        }else{
            foreach($mRes as $item){
                $aRes['OUTPUT'][$item->user_id_create][$item->customer_id][$item->materials_type_id][$item->materials_id] = $item->qty; 
                $aRes['AGENT_ID'][$item->user_id_create] = $item->user_id_create;
                $aRes['CUSTOMER_ID'][$item->customer_id] = $item->customer_id;
            }// end foreach($mRes as $item){
        }
    }
    
    /**
     * @Author: DungNT Feb 19, 2016
     * @Todo: get remain of customer for OutputAgent
     * @Param: $criteria, &$aRes, $needMore=array()
     */
    public static function OutputAgentGetRemain($date_from,$date_to, &$aRes, $needMore) {
        if(!isset($aRes['CUSTOMER_ID'])){
            return ;
        }
        $fieldName = "agent_id";
        
        //  2/ tính toán lượng gas dư ***********
        $criteria = new CDBcriteria();
        $criteria->addBetweenCondition("t.date_input",$date_from, $date_to);
//        $criteria->addInCondition("t.customer_id", $aRes['CUSTOMER_ID']);
        
        $criteria->select = "sum(amount_gas) as amount_gas, t.$fieldName, t.customer_id, t.materials_type_id, t.materials_id";
        $criteria->group = "t.$fieldName, t.customer_id"; 
        $mRes = GasRemain::model()->findAll($criteria);
        if(count($mRes)){
            foreach($mRes as $item){
                $aRes['REMAIN'][$item->$fieldName][$item->customer_id] = $item->amount_gas;
                $aRes['CUSTOMER_ID'][$item->customer_id] = $item->customer_id;
//                if(!isset($aRes['OUTPUT'][$item->agent_id][$item->customer_id])){// bi sai, chua tim dc nguyen nhan Feb 19, 2016
//                    $aRes['OUTPUT'][$item->agent_id][$item->customer_id][$item->materials_type_id][$item->materials_id] = 0;
//                }
            }
        }
        //  end 2/ tính toán lượng gas dư *************
    }
    
     /**
     * @Author: DungNT Mar 02, 2016
     * @Todo: BC target new
     */
    public function targetNew($model) {
        try {
        /* 1. lấy sản lượng cho từng loại  3: Hộ gia đình 12, 2: mối 12, 1:bình bò
         * gom theo năm tháng
         */
        $aRes = array();
        $model->date_from_ymd = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
        $model->date_to_ymd = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);
        GasTargetMonthly::getTargetByDate($model, $aRes);
        
        $this->targetNewData($model, $aRes);

        $aRes['ROLE_AGENT_MODEL'] = Users::getArrObjecByRoleGroupByProvince(ROLE_AGENT, array('only_id_model'=>1 ));
        
        return $aRes;
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }
    }
    
    /**
     * @Author: DungNT Mar 19, 2016
     */
    public function getSameCriteriaTartgetNew($model, &$criteria, $aZoneProvince, $aChuyenVienCCS = array()) {
//        $criteria->addInCondition("t.type_in_out", CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC); // xuất bán, bán bộ bình, thế chân
        $sParamsIn = implode(',', CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC);
        $criteria->addCondition("t.type_in_out IN ($sParamsIn)");
        
        $sProvince = !empty($model->agent_id) ? implode(',', $aZoneProvince[$model->agent_id]) : '';
        if($model->agent_id == GasOrders::ZONE_TARGET_HCM){
            $KhoPhuocTan = MyFormat::KHO_PHUOC_TAN;
//            $criteria->addInCondition("t.province_id_agent", $aZoneProvince[$model->agent_id]);
            // Mar 31, 2016 xử lý vì kho phước tân thuộc Đồng Nai mà muốn thống kê bên TP HCM
            $criteria->addCondition("t.user_id_create=$KhoPhuocTan OR t.province_id_agent IN ($sProvince) ");
        }elseif($model->agent_id == GasOrders::ZONE_TARGET_MIENDONG){
            $sAgentAdd = 105; // - Đại lý Quận 9 
            $criteria->addCondition("t.user_id_create IN ($sAgentAdd) OR t.province_id_agent IN ($sProvince) ");
        }elseif(!empty($sProvince)){
            $criteria->addCondition("t.province_id_agent IN ($sProvince) ");
        }
        
//        $criteria->addBetweenCondition("t.date_delivery", $model->date_from_ymd, $model->date_to_ymd);
        DateHelper::searchBetween($model->date_from_ymd, $model->date_to_ymd, 'date_delivery_bigint', $criteria, false);
        
//       Jan 18, 2019 NamNH  thay đổi lấy danh sách sale Bo Moi
//        if(count($aChuyenVienCCS)){// vì hộ gia đình và Xe Rao không gắn Chuyên Viên CCS nên sẽ không đưa vào
////            $criteria->addInCondition("t.sale_id", $aChuyenVienCCS);
//            $sParamsIn = implode(',', $aChuyenVienCCS);
//            $criteria->addCondition("t.sale_id IN ($sParamsIn)");
//        }
        $criteria->addNotInCondition('t.sale_id', GasLeave::getArraySaleIdCompany());
    }
    // Belong to TargetNew
    public function targetNewData($model, &$aRes) {
        /* 1. find ra Binh BO va Moi theo material type khong can phan biet KH bo hay KH Moi
         * 2. find rieng KH ho GD + Xe Rao
         */
        $aZoneProvince          = GasOrders::getTargetZoneProvinceId();
        if(empty($model->agent_id)){
            $aModelChuyenVienCCS    = Users::getArrObjecByRoleGroupByProvince(ROLE_MONITORING_MARKET_DEVELOPMENT, array('only_id_model'=>1));
        }else{
            $aModelChuyenVienCCS    = Users::getArrObjecByRoleGroupByProvince(ROLE_MONITORING_MARKET_DEVELOPMENT, array('only_id_model'=>1, "aProvinceId"=>$aZoneProvince[$model->agent_id]));
        }
        $aChuyenVienCCS         = array_keys($aModelChuyenVienCCS);
        $aRes['MODEL_CCS']      = $aModelChuyenVienCCS;

        // 1.1 find ra Binh BO
        $criteria = new CDBcriteria();
        $this->getSameCriteriaTartgetNew($model, $criteria, $aZoneProvince, $aChuyenVienCCS);
//        $criteria->addInCondition("t.materials_type_id", CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT);// Add May 16, 2015
        $sParamsIn = implode(',', CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT);
        $criteria->addCondition("t.materials_type_id IN ($sParamsIn)");
        
//        $criteria->addInCondition("t.type_customer", array(STORE_CARD_KH_BINH_BO));
        $sParamsIn = implode(',', array(STORE_CARD_KH_BINH_BO));
        $criteria->addCondition("t.type_customer IN ($sParamsIn)");
//        NamNH remove May 26,2019
//        if($model->agent_id != GasOrders::ZONE_TARGET_MIENTAY){
//            $criteria->addCondition("t.customer_id >=850484 ");// Chỉ lấy KH phát sinh từ tháng 3-2016
//        }

        $criteria->select = "sum(qty) as qty, t.user_id_create, t.materials_type_id, t.sale_id";
//        $criteria->group = "t.user_id_create, t.sale_id, t.materials_type_id";
        $criteria->group = "t.user_id_create, t.materials_type_id";// Jun0219 bỏ group theo sale, chỉ theo đại lý - Kiên yc
        $mRes = GasStoreCardDetail::model()->findAll($criteria);
        foreach($mRes as $item){// Chỉ lấy dữ liệu đại lý và loại vật tư, để xuống view nhân ra kg với Bò 
//            $aRes['OUTPUT_BO'][$item->user_id_create][$item->sale_id][$item->materials_type_id] = $item->qty;
            $aRes['OUTPUT_BO'][$item->user_id_create][$item->materials_type_id] = $item->qty;// Jun0219
        }// Chỗ này sẽ chia ra bình bò và bình 12 kg của KH bò
        
        // 1.2 find ra Binh Moi
        $criteria = new CDBcriteria();
        $this->getSameCriteriaTartgetNew($model, $criteria, $aZoneProvince, $aChuyenVienCCS);
//        $criteria->addInCondition("t.materials_type_id", CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT);// Add May 16, 2015
        $sParamsIn = implode(',', CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT);
        $criteria->addCondition("t.materials_type_id IN ($sParamsIn)");
        
//        $criteria->addInCondition("t.type_customer", array(STORE_CARD_KH_MOI));
        $sParamsIn = implode(',', array(STORE_CARD_KH_MOI));
        $criteria->addCondition("t.type_customer IN ($sParamsIn)");
        
        $criteria->select = "sum(qty) as qty, t.user_id_create, t.materials_type_id, t.sale_id";
//        $criteria->group = "t.user_id_create, t.sale_id, t.materials_type_id";
        $criteria->group = "t.user_id_create, t.materials_type_id";// Jun0219
        $mRes = GasStoreCardDetail::model()->findAll($criteria);
        foreach($mRes as $item){// Chỉ lấy dữ liệu đại lý và loại vật tư, để xuống view nhân ra kg với Bò 
//            $aRes['OUTPUT_MOI'][$item->user_id_create][$item->sale_id][$item->materials_type_id] = $item->qty;
            $aRes['OUTPUT_MOI'][$item->user_id_create][$item->materials_type_id] = $item->qty;
        }

        // 2. find rieng KH ho GD + Xe Rao
        $criteria = new CDBcriteria();
        $this->getSameCriteriaTartgetNew($model, $criteria, $aZoneProvince, array() );
//        $criteria->addInCondition("t.materials_type_id", array(MATERIAL_TYPE_BINH_12));// Add May 16, 2015
//        $criteria->addInCondition("t.materials_type_id", CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT);//fix Mar 09, 2016 Xe Rao lấy cả Bình Bò
        $sParamsIn = implode(',', CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT);
        $criteria->addCondition("t.materials_type_id IN ($sParamsIn)");
        
//        $criteria->addInCondition("t.type_customer", array(CUSTOMER_HO_GIA_DINH, STORE_CARD_XE_RAO));
        $sParamsIn = implode(',', array(CUSTOMER_HO_GIA_DINH, STORE_CARD_XE_RAO));
        $criteria->addCondition("t.type_customer IN ($sParamsIn)");
        
        $criteria->select = "sum(qty) as qty, t.user_id_create, t.materials_type_id, t.type_customer";
        $criteria->group = "t.user_id_create, t.type_customer, t.materials_type_id";
        $mRes = GasStoreCardDetail::model()->findAll($criteria);
        foreach($mRes as $item){// Chỉ lấy dữ liệu đại lý và loại vật tư, để xuống view nhân ra kg với Bò 
            $aRes['OUTPUT_HO_GD'][$item->user_id_create][$item->type_customer][$item->materials_type_id] = $item->qty;
        }
        
        // 3. gas du - remain
        $criteria = new CDBcriteria();
//        $criteria->addBetweenCondition("t.date_input", $model->date_from_ymd, $model->date_to_ymd);
        DateHelper::searchBetween($model->date_from_ymd, $model->date_to_ymd, 'date_input_bigint', $criteria, false);
//        $criteria->addInCondition("t.sale_id", $aChuyenVienCCS);
        $sParamsIn = implode(',', $aChuyenVienCCS);
        if(!empty($sParamsIn)){
//            $criteria->addCondition("t.sale_id IN ($sParamsIn)");
        }
        
        $criteria->select = "sum(amount_gas) as amount_gas, t.type_customer, t.agent_id, t.sale_id";
//        $criteria->group = "t.agent_id, t.sale_id, t.type_customer"; 
        $criteria->group = "t.agent_id, t.type_customer"; // Jun0219 fix
        $mRes = GasRemain::model()->findAll($criteria);
        foreach($mRes as $item){// Chỉ lấy dữ liệu đại lý và loại vật tư, để xuống view nhân ra kg với Bò 
//            $aRes['GAS_REMAIN'][$item->agent_id][$item->sale_id][$item->type_customer] = $item->amount_gas; 
            $aRes['GAS_REMAIN'][$item->agent_id][$item->type_customer] = $item->amount_gas; // Jun0219 fix
        }
        }

     /**
     * @Author: DungNT Apr 03, 2016
     * @Todo: get sản lượng của 1 array customer id không quan tâm là KH bò hay mối, sẽ group theo loại vật tư
      * hàm này có thể sử dụng cho nhiều chỗ, để get ra sản lượng của 1 hay 1 array KH
      * @param: OutputCustomer is $mStoreCard
     */
    public static function OutputCustomer($model) {
        try {
        $aRes = array();
        $model->date_from_ymd = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
        $model->date_to_ymd = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);
        self::OutputCustomerData($model, $aRes);
        return $aRes;
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }
    }
    
    // Belong to OutputCustomer
    public static function OutputCustomerData($model, &$aRes) {
        /* 1. find output theo material type khong can phan biet KH bo hay KH Moi
         * 2. gas du - remain
         */
        // sản lượng all của KH
        $criteria = new CDBcriteria();        
//        $criteria->addInCondition("t.materials_type_id", CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT);// Add May 16, 2015
        $sParamsIn = implode(',', CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT);
        $criteria->addCondition("t.materials_type_id IN ($sParamsIn)");
        // $model->customer_id là 1 array Sep 22, 2016
//        $criteria->addInCondition("t.customer_id", $model->customer_id);
        $sParamsIn = implode(',', $model->customer_id);
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.customer_id IN ($sParamsIn)");
        }
//        $criteria->addInCondition("t.type_in_out", CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC); // xuất bán, bán bộ bình, thế chân
        $sParamsIn = implode(',', CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC);
        $criteria->addCondition("t.type_in_out IN ($sParamsIn)");
        
        $criteria->addBetweenCondition("t.date_delivery", $model->date_from_ymd, $model->date_to_ymd);
        $criteria->select = 'sum(qty) as qty, t.materials_type_id, t.customer_id';
        $criteria->group = 't.customer_id, t.materials_type_id';
//        $criteria->group = "t.customer_id";// add thêm điều kiện t.materials_type_id ngày Feb 24, 2017, vậy BC này bị sai cho đến thời điểm hiện tại
        $mRes = GasStoreCardDetail::model()->findAll($criteria);
        foreach($mRes as $item){// Chỉ lấy dữ liệu đại lý và loại vật tư, để xuống view nhân ra kg với Bò 
            $aRes['OUTPUT_ALL'][$item->customer_id][$item->materials_type_id] = $item->qty;
        }// Chỗ này sẽ chia ra bình bò và bình 12 kg của KH bò
        
        // 2. gas du - remain
        $criteria = new CDBcriteria();
        $criteria->addBetweenCondition("t.date_input", $model->date_from_ymd, $model->date_to_ymd);
//        $criteria->addInCondition("t.customer_id", $model->customer_id);
        $sParamsIn = implode(',', $model->customer_id);
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.customer_id IN ($sParamsIn)");
        }
        
        $criteria->select = "sum(amount_gas) as amount_gas, t.customer_id";
        $criteria->group = "t.customer_id"; 
        $mRes = GasRemain::model()->findAll($criteria);
        foreach($mRes as $item){// Chỉ lấy dữ liệu đại lý và loại vật tư, để xuống view nhân ra kg với Bò 
            $aRes['GAS_REMAIN'][$item->customer_id] = $item->amount_gas;
        }
    }
    
    
    /**
     * @Author: DungNT May 05, 2016
     * @Todo: tính tồn kho Vỏ theo 1 số kho, và tồn Vỏ quá 60 ngày
     * @Param: $model model store card chứa thông tin điều kiện search
     */
    public static function InventoryVo($model){
        try{
        /* 1. tính tồn đầu cho tất cả các loại vật tư
         * 2. tính tổng nhập, xuất của các loại vật tư theo từng đại lý
         * 3. tính tồn đầu cho vỏ tồn quá 60 ngày - có sẵn dùng chung OPENING_BALANCE_YEAR_BEFORE
         * 4. tính tổng nhập của vỏ tồn quá 60 ngày
         * 5. số I60 = tổng xuất - tổng nhập của I60
         */
        /* 1. */
        $aRes['OPENING_BALANCE_YEAR_BEFORE'] = MyFunctionCustom::getOpeningBalanceGroupByAgent($model->agent_id, '', $model->ext_materials_type_id);
        
//        $date_from = date('Y-').'01-01';// từ đầu năm, hiện tại hỗ trợ xem ngày trong năm, không cho chọn ngày của năm trước
        $date_from = MyFunctionCustom::GetYearForReport().'-01-01';// từ đầu năm, hiện tại hỗ trợ xem ngày trong năm, không cho chọn ngày của năm trước
        /** Jan 02, 2014 xem ghi chú ở function MyFunctionCustom::GetYearForReport()
         */
        $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);
        $date_to_60 = MyFormat::modifyDays($date_to, Sta2::InventoryVo60, "-");// sử dụng để tính tổng xuất của 60 ngày trước
        /* 2. */
        MyFunctionCustom::fixSumExportImport($aRes, $model->agent_id, $date_from, $date_to);
//        $aRes['IMPORT'] = MyFunctionCustom::sumTotalExportImportGroupByAgent($model->agent_id, $date_from, $date_to, TYPE_STORE_CARD_IMPORT);
//        $aRes['EXPORT'] = MyFunctionCustom::sumTotalExportImportGroupByAgent($model->agent_id, $date_from, $date_to, TYPE_STORE_CARD_EXPORT);
        $aRes['IMPORT_60'] = MyFunctionCustom::sumTotalExportImportGroupByAgent($model->agent_id, $date_from, $date_to_60, TYPE_STORE_CARD_IMPORT);
        $aRes['MODEL_MATERIAL_TYPE'] = GasMaterials::getArrayModelMaterialType($model->ext_materials_type_id);        
        
        return $aRes;
        
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }

    /**
     * @Author: DungNT Jul 04, 2016
     * @Todo: thống kê gom nhóm số lượng KH theo chuyên viên CCS tạo
     */
    public static function groupVip240() {
        $criteria = new CDbCriteria();
        $criteria->compare("t.role_id", ROLE_CUSTOMER);
        $criteria->compare("t.type", CUSTOMER_TYPE_STORE_CARD);
        $criteria->compare("t.is_maintain", STORE_CARD_VIP_HGD);
        $criteria->select = "t.created_by, count(t.id) as id";
        $criteria->group = "t.created_by";
        $criteria->order = "t.province_id ASC, count(t.id) DESC" ;
        $models = Users::model()->findAll($criteria);
        $aRes = array();
        foreach($models as $model){
            $aRes['DATA'][$model->created_by] = $model->id;
            $aRes['UID'][$model->created_by] = $model->created_by;
        }
        if(isset($aRes['UID'])){
            $aRes['MODEL_USER'] = Users::getListOptions($aRes['UID']);
        }
        return $aRes;
    }
    
    /**
     * @Author: DungNT Jul 29, 2016
     * @Todo: BC san luong xe Phước Tân
     */
    public static function OutputCar($model) {
        try {
        /* 1. lấy sản lượng cho của từng thẻ kho
         * gom theo năm tháng
         */
        $aRes = array();
        $aRes['DRIVER'] = array();
        $aRes['PHU_XE'] = array();
        $model->agent_id = empty($model->agent_id) ? 0 : $model->agent_id;
        self::OutputCarData($model, $aRes);
        if(isset($aRes['UID'])){
            $aRes['MODEL_USER'] = UsersS1::getArrObjectUserByRole('', $aRes['UID']);
        }
        if(isset($aRes['STORECARD_ID'])){
            $aRes['STORECARD_NO'] = GasStoreCard::getArrObjectByArrId($aRes['STORECARD_ID'], array('listdata'=>1));
        }
        if($model->MAX_ID == 2){// for web view sum by EMPLOYEE
            self::OutputCarEmployee($aRes);
        }
        return $aRes;
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }

    /**
     * @Author: DungNT Jul 29, 2016
     */       
    public static function OutputCarData($model, &$aRes, $needMore=array()){
        /* 1. findAll and group by materials_type_id */
        $criteria = new CDBcriteria();
        $criteriaDaily = new CDBcriteria();
        self::OutputCarSameCriteria($model, $criteria);
        self::OutputCarSameCriteria($model, $criteriaDaily);

//        $criteria->group = "t.user_id_create, t.date_delivery, t.car_id, t.road_route, t.materials_type_id";
        if(empty($model->MAX_ID)){// for export excel
            $criteria->group    = "t.date_delivery, t.car_id, t.road_route, t.store_card_id, t.materials_type_id";
        }elseif($model->MAX_ID == 1){// Xem sum theo xe tải
            $criteria->group        = "t.car_id, t.road_route, t.materials_type_id";
            $criteriaDaily->group   = "t.date_delivery, t.road_route, t.materials_type_id";
            
        }elseif($model->MAX_ID == 2){// Xem sum theo nhân viên
            $criteria->group    = "t.road_route, t.store_card_id, t.materials_type_id";
        }
        $mRes = GasStoreCardDetail::model()->findAll($criteria);
        foreach($mRes as $item){
            $aRes['UID'][$item->car_id] = $item->car_id;
            $aRes['PROVINCE_ID'][$item->store_card_id] = $item->province_id_agent;
            if(empty($model->MAX_ID)){// for export excel
                $aRes['STORECARD_ID'][$item->store_card_id] = $item->store_card_id;
                $aRes['OUTPUT_CAR'][$item->road_route][$item->date_delivery][$item->car_id][$item->store_card_id]['qty'][$item->materials_type_id]      = $item->qty;
                if(!isset($aRes['OUTPUT_CAR'][$item->road_route][$item->date_delivery][$item->car_id][$item->store_card_id]['employee'])){
                    $aRes['OUTPUT_CAR'][$item->road_route][$item->date_delivery][$item->car_id][$item->store_card_id]['employee'] = self::getArrayEmployee($aRes, $item);
                }
            }elseif($model->MAX_ID == 1){// for web view sum by car
                $aRes['OUTPUT_CAR'][$item->road_route][$item->car_id]['qty'][$item->materials_type_id]      = $item->qty;
            }elseif($model->MAX_ID == 2){// for web view sum by EMPLOYEE
                $aRes['OUTPUT_CAR'][$item->road_route][$item->car_id][$item->store_card_id]['qty'][$item->materials_type_id]  = $item->qty;
                $aRes['OUTPUT_CAR'][$item->road_route][$item->car_id][$item->store_card_id]['employee'] = self::getArrayEmployee($aRes, $item);
            }
        }
        
        if(!empty($model->uid_login)){// lấy tạm biến uid_login là car_id
            self::OutputCarDetaiOne($aRes, $criteriaDaily);
        }
    }
    
    /**
     * @Author: DungNT Dec 30, 2016
     * @Todo: get same condition
     */
    public static function OutputCarSameCriteria($model, &$criteria) {
        $date_from  = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
        $date_to    = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);
        $model->date_from_ymd   = $date_from;
        $model->date_to_ymd     = $date_to;
        $criteria->addBetweenCondition("t.date_delivery", $date_from, $date_to);
//        $criteria->compare("t.user_id_create", $model->agent_id);
        $sParamsIn = implode(',', CmsFormatter::$MATERIAL_TYPE_BINHBO_12);
        $criteria->addCondition("t.user_id_create=$model->agent_id AND t.materials_type_id IN ($sParamsIn)");
        $criteria->addCondition("t.car_id IS NOT NULL AND t.car_id<>'' ");
        $criteria->select = "sum(qty) as qty, t.materials_type_id, t.user_id_create, "
                . "t.date_delivery, t.car_id, t.driver_id_1, t.driver_id_2, "
                . "t.phu_xe_1, t.phu_xe_2, t.road_route, t.store_card_id, t.province_id_agent";
        // Dec 30, 2016 xử lý xem chi tiết từ ngày 01->30 của 1 xe
        if(!empty($model->uid_login)){// lấy tạm biến uid_login là car_id
            $criteria->addCondition("t.car_id=$model->uid_login");
        }
    }
    
    /**
     * @Author: DungNT Dec 30, 2016
     * @Todo: tính sl chi tiết từng xe cũng theo cố định và thông thường
     */
    public static function OutputCarDetaiOne(&$aRes, $criteria) {
        $mRes = GasStoreCardDetail::model()->findAll($criteria);
        foreach($mRes as $item){
            $aRes['ONE_CAR'][$item->road_route][$item->date_delivery][$item->materials_type_id] = $item->qty;
//            $aRes['ONE_CAR_DATE'][$item->date_delivery] = ;
        }
    }   
    
    public static function getArrayEmployee(&$aRes, $item) {
        $aEmployee = array();
        $aRes['UID'][$item->car_id]                 = $item->car_id;
        if(!empty($item->driver_id_1) && $item->driver_id_1 > 100){
            $aEmployee[]                            = $item->driver_id_1;
            $aRes['UID'][$item->driver_id_1]        = $item->driver_id_1;
            $aRes['EMPLOYEE'][$item->driver_id_1]   = $item->driver_id_1;
            $aRes['DRIVER'][$item->driver_id_1]     = $item->driver_id_1;
            $aRes['CAR'][$item->driver_id_1][$item->car_id] = $item->car_id;
        }
        if(!empty($item->driver_id_2) && $item->driver_id_2 > 100){
            $aEmployee[]                            = $item->driver_id_2;
            $aRes['UID'][$item->driver_id_2]        = $item->driver_id_2;
            $aRes['EMPLOYEE'][$item->driver_id_2]   = $item->driver_id_2;
            $aRes['DRIVER'][$item->driver_id_2]     = $item->driver_id_2;
            $aRes['CAR'][$item->driver_id_2][$item->car_id] = $item->car_id;
        }
        if(!empty($item->phu_xe_1) && $item->phu_xe_1 > 100){
            $aEmployee[]                            = $item->phu_xe_1;
            $aRes['UID'][$item->phu_xe_1]           = $item->phu_xe_1;
            $aRes['EMPLOYEE'][$item->phu_xe_1]      = $item->phu_xe_1;
            $aRes['PHU_XE'][$item->phu_xe_1]        = $item->phu_xe_1;
            $aRes['CAR'][$item->phu_xe_1][$item->car_id] = $item->car_id;
        }// Oct0917: vì biến phu_xe_1 và phu_xe_2 sử dụng giá trị 1,2,3 để làm cờ trong một số trường hợp, nên phải kiểm tra > 100
        if(!empty($item->phu_xe_2) && $item->phu_xe_2 > 100){
            $aEmployee[]                            = $item->phu_xe_2;
            $aRes['UID'][$item->phu_xe_2]           = $item->phu_xe_2;
            $aRes['EMPLOYEE'][$item->phu_xe_2]      = $item->phu_xe_2;
            $aRes['PHU_XE'][$item->phu_xe_2]        = $item->phu_xe_2;
            $aRes['CAR'][$item->phu_xe_2][$item->car_id] = $item->car_id;
        }
        return $aEmployee;
    }
    
    /**
     * @Author: DungNT Jul 31, 2016
     * @todo: cộng cho từng nhân viên
     * @cong_thuc: 1; tuyến cố định chia 2
     * 2/ quy đổi 1 bình 50,45 = 2 bình 12
     * 3/ CỘNG TỔNG BÌNH CHIA HỆ SỐ ĐI KÈM VỚI LOẠI XE
     * 4/ CHIA TỔNG SỐ BÌNH SAU KHI CHIA HE SO CHO CÁC NHÂN VIÊN CỦA CHUYẾN ĐÓ
     * 5/ cộng dồn Gắn sản lượng cho nv
     */
    public static function OutputCarEmployee(&$aRes) {
        if(!isset($aRes['OUTPUT_CAR'])){
            return ;
        }
        $session = Yii::app()->session;
        $OUTPUT = $aRes['OUTPUT_CAR']; 
        $aDataNew = array();
        foreach($OUTPUT as $road_route => $aData){
            foreach($aData as $car_id => $aDataStoreCard){
                foreach($aDataStoreCard as $store_card_id => $aDataCar){
                    $sum12 = 0;// 1. tính tổng số binh của chuyến + quy đổi 45, 6KG
                    foreach($aDataCar['qty'] as $materials_type_id => $qty){
                        if(in_array($materials_type_id, GasMaterialsType::$ARR_GAS_VO_45KG)){
                            $sum12 += ($qty*2);// bình 45 x 2
                        }elseif(in_array($materials_type_id, GasMaterialsType::$ARR_GAS_VO_6KG)){
                            $sum12 += ($qty/2);// bình 6kg chia 2
                        }else{
                            $sum12 += $qty;
                        }
                    }
                    // 1; tuyến cố định chia 2
                    if($road_route == GasStoreCard::ROAD_ROUTE_FIX){
                        $sum12 = $sum12/2;
                    }
                    $totalEmployeeInCar = count($aDataCar['employee']);
                    // Sep 29, 2016 lấy sản lượng trc khi chi hệ số
                    $qtyBeforeRatio = $sum12;
                    if($totalEmployeeInCar != 0){
                        $qtyBeforeRatio = $sum12/$totalEmployeeInCar;
                    }else{
                        $totalEmployeeInCar = 1;
                    }
                    
                    $ratio = 1;// 3. CỘNG TỔNG BÌNH CHIA HỆ SỐ ĐI KÈM VỚI LOẠI XE
                    if(isset($session['WEB_MODEL_BY_ROLE'][$car_id])){
                        $ratio = $session['WEB_MODEL_BY_ROLE'][$car_id]->last_name;
                    }
                    $ratio = empty($ratio) ? 1 : $ratio;
                    $sum12 = $sum12/$ratio;
                    $qtyEachOne = $sum12/$totalEmployeeInCar;
                    // 5/ cộng dồn Gắn sản lượng cho nv
                    foreach($aDataCar['employee'] as $employee_id){
                        if(in_array($employee_id, $aRes['DRIVER'])){
                            if(isset($aDataNew['DRIVER'][$employee_id])){
                                $aDataNew['DRIVER'][$employee_id] += $qtyEachOne;
                            }else{
                                $aDataNew['DRIVER'][$employee_id] = $qtyEachOne;
                            }
                            // for qty before ratio
                            if(isset($aDataNew['DRIVER_BEFORE_RATIO'][$employee_id][$car_id])){
                                $aDataNew['DRIVER_BEFORE_RATIO'][$employee_id][$car_id] += $qtyBeforeRatio;
                            }else{
                                $aDataNew['DRIVER_BEFORE_RATIO'][$employee_id][$car_id] = $qtyBeforeRatio;
                            }
                            
                        }elseif(in_array($employee_id, $aRes['PHU_XE'])){
                            if(isset($aDataNew['PHU_XE'][$employee_id])){
                                $aDataNew['PHU_XE'][$employee_id] += $qtyEachOne;
                            }else{
                                $aDataNew['PHU_XE'][$employee_id] = $qtyEachOne;
                            }
                            // for qty before ratio
                            if(isset($aDataNew['PHU_XE_BEFORE_RATIO'][$employee_id][$car_id])){
                                $aDataNew['PHU_XE_BEFORE_RATIO'][$employee_id][$car_id] += $qtyBeforeRatio;
                            }else{
                                $aDataNew['PHU_XE_BEFORE_RATIO'][$employee_id][$car_id] = $qtyBeforeRatio;
                            }
                        }
                        
                        // Sep 28, 2016 fix thêm thống kê qty từng xe của từng người nữa
                        if(!isset($aDataNew['EMPLOYEE_BY_CAR'][$employee_id][$car_id])){
                            $aDataNew['EMPLOYEE_BY_CAR'][$employee_id][$car_id] = $qtyEachOne;
                        }else{
                            $aDataNew['EMPLOYEE_BY_CAR'][$employee_id][$car_id] += $qtyEachOne;
                        }
                        
                    }
                }
            }
        }
        $aRes['OUTPUT_CAR'] = $aDataNew;
    }
    
    /**
     * @Author: DungNT Sep 18, 2016
     * @Todo: Báo cáo số điểm đã đi của ccs
     */
    public static function TargetCcs() {
        $criteria = new CDbCriteria();
        $criteria->compare("t.role_id", ROLE_CUSTOMER);
        $criteria->compare("t.type", CUSTOMER_TYPE_STORE_CARD);
        $criteria->compare("t.is_maintain", STORE_CARD_HGD_CCS);
//        $criteria->select   = "t.created_by, count(t.id) as id";
//        $criteria->group    = "t.created_by, t.parent_id";
        // 1 Tiếp xúc, 2: Bảo trì
        $criteria->group = "month(t.created_date), year(t.created_date), t.created_by, t.parent_id";
        $criteria->select = "t.created_by, t.parent_id, count(t.id) as id, month(t.created_date) as date_month, year(t.created_date) as date_year ";
        $models = Users::model()->findAll($criteria);
        $aRes = array();
        foreach($models as $model){
            $aRes['DATA'][$model->date_year][$model->date_month][$model->created_by][$model->parent_id]   = $model->id;
            $aRes['UID'][$model->created_by]    = $model->created_by;
        }
        if(isset($aRes['UID'])){
            $aRes['MODEL_USER'] = Users::getListOptions($aRes['UID']);
        }
        return $aRes;
    }
    
    /**
     * @Author: DungNT Oct 07, 2016
     * @Todo: BC san luong điếu phối - tổng sl kh bò, mối, số bình, gas dư, số cuộc gọi
     */
    public static function CallCenterOutput($model) {
        try {
        /* 1. lấy sản lượng cho của thẻ kho
         * 2/ lấy sản lượng bên đặt hàng
         */
        $aRes = array();
        self::CallCenterOutputData($model, $aRes);
        self::CallCenterOutputFormatData($aRes);
        return $aRes;
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public static function CallCenterOutputData($model, &$aRes, $needMore=array()){
        /* 1. findAll and group by materials_type_id thẻ kho 
         * đã chỉnh cho tất cả vào thẻ kho, những đơn hàng Điều Xe Kho Bến Cát => Kho Bến Cát, Điều Xe Kho Phước Tân => Kho Phước Tân
         * được sinh tự động
         */
        $date_from  = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
        $date_to    = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);
        $model->date_from_ymd = $date_from;
        $model->date_to_ymd = $date_to;
        $criteria = new CDBcriteria();
        $criteria->addBetweenCondition("t.date_delivery", $date_from, $date_to);
        $sParamsIn = implode(',', GasOrders::$ZONE_HCM);
        $criteria->addCondition("t.uid_login IN ($sParamsIn) AND t.uid_login IS NOT NULL");
        if(!empty($model->agent_id)){
            $criteria->addCondition("t.user_id_create=$model->agent_id");
        }
        $sParamsIn = implode(',', CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT);
        $criteria->addCondition("t.materials_type_id IN ($sParamsIn)");
        $sParamsIn = implode(',', CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC);
        $criteria->addCondition("t.type_in_out IN ($sParamsIn)");
        
        $criteria->select = "sum(qty) as qty, t.uid_login, t.materials_type_id, t.type_customer, t.customer_id";
        $criteria->group = "t.uid_login, t.materials_type_id, t.customer_id";
        
        $mRes = GasStoreCardDetail::model()->findAll($criteria);
        foreach($mRes as $item){
            $aRes['OUTPUT'][$item->uid_login][$item->materials_type_id][$item->type_customer][$item->customer_id] = $item->qty;
//            $aRes['CUSTOMER_ID'][$item->uid_login][$item->customer_id] = $item->customer_id;// dùng để tính gas dư của KH cho mỗi điếu phối
            // Gas dư không thể tính riêng cho từng điều phối được
            $aRes['CUSTOMER_UNIQUE'][$item->customer_id] = $item->customer_id;// ma KH khong trung, de tinh gas du
        }
        if(!isset($aRes['OUTPUT'])){
            return ;
        }
        // 2. gas dư, chắc là lấy all gas dư của tất cả customer, ra ngoài PHP xử lý sum theo từng điếu phối
        self::CallCenterOutputGasRemain($model, $aRes, $date_from, $date_to);
        
    }
    
    /**
     * @Author: DungNT Oct 19, 2016
     * @Todo: lấy gas dư của KH trong ngày đó hoặc tháng
     * @param: $model,  &$aRes, $date_from, $date_to
     * @Return: $aRes: is final array return
     */      
    public static function CallCenterOutputGasRemain($model, &$aRes, $date_from, $date_to){
        $criteria = new CDBcriteria();
        $criteria->addBetweenCondition("t.date_input", $date_from, $date_to);
        if(!empty($model->agent_id)){
            $criteria->addCondition("t.agent_id=$model->agent_id");
        }
        if(isset($aRes['CUSTOMER_UNIQUE'])){
            $sParamsIn = implode(',', $aRes['CUSTOMER_UNIQUE']);
            $criteria->addCondition("t.customer_id IN ($sParamsIn)");
        }

        $criteria->select   = "sum(amount_gas) as amount_gas, sum(amount) as amount";
//        $criteria->group    = "t.customer_id"; 
        $mRes = GasRemain::model()->find($criteria);
        $aRes['GAS_REMAIN'] = $mRes->amount_gas;
    }
    
    /**
     * @Author: DungNT Oct 19, 2016
     * @Todo: format data show
     */      
    public static function CallCenterOutputFormatData(&$aRes){
        /* 1. tổng sl kh bò, mối, số bình,
         *  2.  gas dư
         */
        $aTmp = array();
        foreach($aRes['OUTPUT'] as $uid_login => $aInfo1){
            foreach($aInfo1 as $materials_type_id => $aInfo2){
                foreach($aInfo2 as $type_customer => $aInfo3){
                    foreach($aInfo3 as $customer_id => $qty){
                        // sum loại kh bò mối cho từng NV
                        if(!isset($aTmp['TYPE_CUSTOMER_COUNT'][$uid_login][$type_customer])){
                            $aTmp['TYPE_CUSTOMER_COUNT'][$uid_login][$type_customer] = 1;
                        }else{
                            $aTmp['TYPE_CUSTOMER_COUNT'][$uid_login][$type_customer] += 1;
                        }
                        // sum từng loại bình cho loại KH đó
                        if(!isset($aTmp['TYPE_CUSTOMER_QTY'][$uid_login][$type_customer][$materials_type_id])){
                            $aTmp['TYPE_CUSTOMER_QTY'][$uid_login][$type_customer][$materials_type_id] = $qty;
                        }else{
                            $aTmp['TYPE_CUSTOMER_QTY'][$uid_login][$type_customer][$materials_type_id] += $qty;
                        }
                    }
                }
            }
        }
        $aRes['OUTPUT'] = $aTmp;
        
        return ; // Now 29, 2016, không tính gas dư theo từng điều phối dc
        
        // 2.  gas dư
        if(!isset($aRes['GAS_REMAIN'])){
            return ;
        }
        $aTmp = array();
        foreach($aRes['CUSTOMER_ID'] as $uid_login => $aInfo1){
            foreach($aInfo1 as $customer_id){
                $remain = isset($aRes['GAS_REMAIN'][$customer_id]) ? $aRes['GAS_REMAIN'][$customer_id] : 0;
                if(!isset($aTmp[$uid_login])){
                    $aTmp[$uid_login] = $remain;
                }else{
                    $aTmp[$uid_login] += $remain;
                }
            }
        }
        $aRes['GAS_REMAIN'] = $aTmp;
    }
    
    /**
     * @Author: DungNT Oct 30, 2016 
     * @Todo: get same criteria của sản lg và gas dư
     * Oct2318 fix 
     * # Query_time: 18.122132  Lock_time: 0.000147 Rows_sent: 30  Rows_examined: 3625294
        SET timestamp=1540200334;
        SELECT sum(qty) as qty, t.sale_id, t.materials_type_id FROM `gas_gas_store_card_detail` `t` WHERE ((((month(t.date_delivery)=10) AND (year(t.date_delivery)=2018)) AND (t.warehouse_new=1)) AND (t.sale_id IN (49168,49175,49184,137223,874502,899502,974623,1110535,1180960,1207466,1398942,1619636,1619638,1631086,1654179,1672579,1672591))) AND (t.materials_type_id IN (11,7,4,9,19)) GROUP BY t.sale_id, t.materials_type_id;
     */
    public static function TargetFixSameCriteria(&$criteria, $model, $fieldName) {
        $fieldNameBigInt = 'date_input_bigint';
        if($fieldName == 'date_delivery'){
            $fieldNameBigInt = 'date_delivery_bigint';
        }
        if(!empty($model->date_delivery)){
            // May 19, 2014 fix
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_delivery);
            $tmpDate = explode('-', $date_to);
            if(count($tmpDate)!=3){
                $tmpDate[0] = date('Y');
                $tmpDate[1] = date('m');
            }
            $date_from = "$tmpDate[0]-$tmpDate[1]-01" ;
//            $criteria->addBetweenCondition("t.$fieldName", $date_from, $date_to);
            DateHelper::searchBetween($date_from, $date_to, $fieldNameBigInt, $criteria, false);
            
        }else{
            if(!empty($model->statistic_month)){
                $date_from  = "$model->statistic_year-$model->statistic_month-01";
                $date_to    = MyFormat::dateConverYmdToDmy($date_from, 'Y-m-t');
//                $criteria->addCondition("month(t.$fieldName)=".$model->statistic_month);
                DateHelper::searchBetween($date_from, $date_to, $fieldNameBigInt, $criteria, false);
            }
            $criteria->addCondition("year(t.$fieldName)=".$model->statistic_year);
        }
    }
    
    
    /**
     * @Author: DungNT Oct 30, 2016
     * @Todo: sửa lại bc target - chỉ có sale bò
     */
    public static function TargetFix($model) {
        try {
        $aRes = array(); $sta2 = new Sta2();
        /* 1. tính sản lượng thực  tế rồi group theo sale
         * 2. tinh gas du
         * 3. lay target theo sale id
         */
        $criteria = new CDBcriteria();
        self::TargetFixSameCriteria($criteria, $model, 'date_delivery');
        $criteria->addCondition('t.warehouse_new='.Users::IS_AGENT_ONLY);
//        $criteria->addCondition('t.sale_type='.STORE_CARD_KH_BINH_BO);// Close May1618
        $sParamsIn = implode(',', $sta2->getListSaleBo());
        $criteria->addCondition("t.sale_id IN ($sParamsIn)");
        
//        $criteria->addCondition('t.type_customer='.$model->ext_is_maintain);//  Close May1618 để tính cả sản lượng KH bò + mói cho NV
        $sParamsIn = implode(',', CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT);
        $criteria->addCondition("t.materials_type_id IN ($sParamsIn)");
        $criteria->select = "sum(qty) as qty, t.sale_id, t.materials_type_id";
        $criteria->group = "t.sale_id, t.materials_type_id";
        
        $mRes = GasStoreCardDetail::model()->findAll($criteria);
        foreach($mRes as $item){
            $aRes['OUTPUT'][$item->sale_id][$item->materials_type_id] = $item->qty;
            $aRes['SALE_ID'][$item->sale_id] = $item->sale_id;
            $aRes['PROVINCE_ID'][$item->province_id_agent] = $item->province_id_agent;
        }
        if(isset($aRes['SALE_ID'])){
            self::TargetFixGasRemain($model, $aRes, $aRes['SALE_ID']);
            // 3. lay target theo sale id
            GasTargetMonthly::getTargetEachMonth($aRes['SALE_ID'], $model->statistic_year, $model->statistic_month, $aRes);
            $aRes['SALE_MODEL'] = Users::getArrObjecByRoleGroupByProvince("", array('aUid'=> $aRes['SALE_ID']));
        }
        return $aRes;
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public static function TargetFixGasRemain($model, &$aRes, $aSale){
        $criteria = new CDBcriteria();
        self::TargetFixSameCriteria($criteria, $model, 'date_input');
        $sParamsIn = implode(',', $aSale);
        $criteria->addCondition("t.sale_id IN ($sParamsIn) AND t.only_gas_remain=0");
        $criteria->select   = "t.sale_id, sum(amount_gas) as amount_gas, sum(amount) as amount";
        $criteria->group    = "t.sale_id"; 
        $mRes = GasRemain::model()->findAll($criteria);
        foreach($mRes as $item){
            $aRes['REMAIN'][$item->sale_id] = $item->amount_gas;
        }
    }
    
    /** @Author: DungNT May 16, 2018
     *  @Todo: get array sale bo
     **/
    public function getListSaleBo() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.role_id=' . ROLE_SALE . ' AND t.status=1 AND t.gender='.Users::SALE_BO);
        $models = Users::model()->findAll($criteria);
        $aUid = CHtml::listData($models, 'id', 'id');
        // Mar0719 cập nhật thêm target cho giám sát
        $aUid[GasConst::UID_SU_BH]      = GasConst::UID_SU_BH;
        $aUid[GasConst::UID_LOC_PV]     = GasConst::UID_LOC_PV;
        $aUid[GasConst::UID_NHAT_NT]    = GasConst::UID_NHAT_NT;
        $aUid[GasConst::UID_BINH_TV]    = GasConst::UID_BINH_TV;
        $aUid[GasConst::UID_LAM_KD]     = GasConst::UID_LAM_KD;
        $aUid[GasConst::UID_TUAN_DH]    = GasConst::UID_TUAN_DH;
        $aUid[GasConst::UID_SU_BH]      = GasConst::UID_SU_BH;
        $aUid[974623] = 974623; // Nguyễn Minh Nhật
        $aUid[976187] = 976187; // Sale Nguyễn Văn Đông
        $aUid[1241317] = 1241317; // Vũ Thành Sơn
        $aUid[GasConst::UID_TRUNG_HV]   = GasConst::UID_TRUNG_HV;
        $aUid[GasConst::UID_EM_MP]      = GasConst::UID_EM_MP;
        $aUid[GasConst::UID_ANH_VH]     = GasConst::UID_ANH_VH;
        $aUid[GasConst::UID_LINH_TTV]   = GasConst::UID_LINH_TTV;
        
        $aUid[GasConst::UID_PHONG_NU]       = GasConst::UID_PHONG_NU;
        $aUid[2145610]                      = 2145610; // Trần Tuấn
        $aUid[GasConst::UID_AN_LCT]         = GasConst::UID_AN_LCT;
        $aUid[GasConst::UID_THIEN_NM]       = GasConst::UID_THIEN_NM;
        $aUid[GasConst::UID_TRUONG_NN]      = GasConst::UID_TRUONG_NN;
        $aUid[1044912]                      = 1044912; // Nguyễn Tấn Đức
        $aUid[GasConst::UID_TROC_VV]        = GasConst::UID_TROC_VV;
        return $aUid;
    }
    
    /**
     * @Author: DungNT Nov 06, 2016
     * @Todo: tính tồn kho all Agent tại thời điểm hiện tại cho all vật tư
     * sử dụng cho load vật tư tồn kho từng đại lý xuống app đặt hàng 
     * @Using: sử dụng cho phần đặt hàng qua app của customer
     */
    public static function setCacheInventoryAllAgent($needMore=array()){
        try{
        $from = time();$aRes = [];
        $date_to = date('Y-m-d');
        $aMateriaType = array_merge(GasMaterialsType::$ARR_WINDOW_GAS,  GasMaterialsType::$ARR_WINDOW_PROMOTION);
        // 1. Lấy tồn đầu kỳ cho mỗi vật tư nhập ban đầu bởi đại lý
        $OPENING_BALANCE = MyFunctionCustom::getAllAgentOpeningBalance($aMateriaType);
        // 2. tính tổng nhập xuất đến thời điểm hiện tại
        $IMPORT_EXPORT = MyFunctionCustom::calcExportImport($aMateriaType, $date_to);
        foreach($IMPORT_EXPORT as $agent_id => $aInfo1){
            if(empty($agent_id)){
                continue;
            }
            foreach($aInfo1 as $materials_type_id => $aInfo2){
                foreach($aInfo2 as $materials_id => $aInfo3){
                    $qtyOpen    = isset($OPENING_BALANCE[$agent_id][$materials_id]) ? $OPENING_BALANCE[$agent_id][$materials_id] : 0;
                    $qtyImport  = isset($aInfo3[TYPE_STORE_CARD_IMPORT]) ? $aInfo3[TYPE_STORE_CARD_IMPORT] : 0;
                    $qtyExport  = isset($aInfo3[TYPE_STORE_CARD_EXPORT]) ? $aInfo3[TYPE_STORE_CARD_EXPORT] : 0;
                    $inventory  = $qtyOpen + $qtyImport - $qtyExport;
                    if($inventory > 0){
                        $aRes[$agent_id][$materials_type_id][$materials_id] = $inventory;
                    }
                }
            }
        }
        $mAppCache = new AppCache();
        if(!isset($_SERVER['SERVER_ADDR'])){
            $mAppCache->isSyncCacheFile = false;
        }
        $mAppCache->setCache(AppCache::APP_AGENT_INVENTORY, json_encode($aRes, JSON_UNESCAPED_UNICODE), 0); // Jun 30, 2017 không chạy tự động bằng time expiry của Cache nữa, sẽ chạy = cron để tránh tình trạng chạy hơn 2 cái cùng 1 lúc
        // Note sử dụng cái update tự động của cache này cũng Nguy Hiểm. Khi có nhiều request cùng chạy 1 lúc, vd mỗi request cần hơn 10s để xong
//        $mAppCache->setCache(AppCache::APP_AGENT_INVENTORY, json_encode($aRes, JSON_UNESCAPED_UNICODE), 3600); // 1h phút chạy 1 lần
        $model = new MonitorUpdate();
        $model->setTimeUpdateInventory();
        
        $to = time();
        $second = $to-$from;
        $info = MyFormat::getIpServer() . " Cron Set Inventory All Agent for App Order: done in: $second  Second  <=> ".($second/60)." Minutes Sta2::setCacheInventoryAllAgent()";
        Logger::WriteLog($info);
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    /**
     * @Author: DungNT Oct 10, 2018
     * @Todo: tính tồn kho all Agent tại thời điểm hiện tại cho all vật tư. BC này không xem đc khoảng ngày
     * sử dụng xuất excel all vật tư tồn kho từng đại lý 
     * @Using: sử dụng cho admin/gasstorecard/view_store_movement_summary
     */
    public function getInventoryAllAgent(){
        $mAppCache = new AppCache();
        $aRes = []; $date_to = date('Y-m-d');
        $this->aMaterials = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
        // 1. Lấy tồn đầu kỳ cho mỗi vật tư nhập ban đầu bởi đại lý
        $OPENING_BALANCE    = MyFunctionCustom::getAllAgentOpeningBalance(''); // A
        $aTempOpening       = $OPENING_BALANCE;// xử lý lọc lấy những vật tư ko có nhập xuất trong 1 năm
        // 2. tính tổng nhập xuất đến thời điểm hiện tại
        $IMPORT_EXPORT = MyFunctionCustom::calcExportImport('', $date_to); // B
        foreach($IMPORT_EXPORT as $agent_id => $aInfo1){
            if(empty($agent_id)){
                continue;
            }
            foreach($aInfo1 as $materials_type_id => $aInfo2){
                foreach($aInfo2 as $materials_id => $aInfo3){
                    unset($aTempOpening[$agent_id][$materials_id]);
                    $this->getInventoryFormatData($aRes, $OPENING_BALANCE, $IMPORT_EXPORT, $agent_id, $materials_type_id, $materials_id);
                }
            }
        }
        
        // xử lý những vật tư ko có nhập xuất trong năm 
        foreach($aTempOpening as $agent_id => $aInfo):
            foreach($aInfo as $materials_id => $qty):
                if(!isset($this->aMaterials[$materials_id])){
                    continue ;
                }
                $mMaterial          = $this->aMaterials[$materials_id];
                $materials_type_id  = $mMaterial['materials_type_id'];
                $this->getInventoryFormatData($aRes, $OPENING_BALANCE, $IMPORT_EXPORT, $agent_id, $materials_type_id, $materials_id);
            endforeach;
        endforeach;
        return $aRes;
    }
    
    /** @Author: DungNT Oct 10, 2018
     *  @Todo: format array return 
     **/
    public function getInventoryFormatData(&$aRes, $OPENING_BALANCE, $IMPORT_EXPORT, $agent_id, $materials_type_id, $materials_id) {
        $temp = [];
        $qtyOpen    = isset($OPENING_BALANCE[$agent_id][$materials_id]) ? $OPENING_BALANCE[$agent_id][$materials_id] : 0;
        $qtyImport  = isset($IMPORT_EXPORT[$agent_id][$materials_type_id][$materials_id][TYPE_STORE_CARD_IMPORT]) ? $IMPORT_EXPORT[$agent_id][$materials_type_id][$materials_id][TYPE_STORE_CARD_IMPORT] : 0;
        $qtyExport  = isset($IMPORT_EXPORT[$agent_id][$materials_type_id][$materials_id][TYPE_STORE_CARD_EXPORT]) ? $IMPORT_EXPORT[$agent_id][$materials_type_id][$materials_id][TYPE_STORE_CARD_EXPORT] : 0;
        $inventory  = $qtyOpen + $qtyImport - $qtyExport;
        $temp['open']   = $qtyOpen;
        $temp['import'] = $qtyImport;
        $temp['export'] = $qtyExport;
        $temp['end']    = $inventory;
        if($qtyOpen == 0 && $qtyImport == 0 && $qtyExport == 0 && $inventory == 0){
            return ;
        }
        $aRes[$agent_id][$materials_type_id][$materials_id] = $temp;
    }
    
    
    /** @Author: DungNT Apr 13, 2017 @Todo: get some criteria */
    public function businessCriteria(&$model, &$criteria, $type) {
        if($type == Sta2::REPORT_OUTPUT){// tính sản lượng bên thẻ kho
            $criteria->addBetweenCondition('t.date_delivery', $model->date_from_ymd, $model->date_to_ymd);
            if(!empty($model->agent_id)){
                $criteria->addCondition('t.user_id_create=' . $model->agent_id);
            }
            if(!empty($model->ext_is_maintain)){
                $criteria->addCondition("t.type_customer=$model->ext_is_maintain");
            }
        }elseif($type == Sta2::REPORT_REMAIN){// tính sản lượng gas dư
            $criteria->addBetweenCondition('t.date_input', $model->date_from_ymd, $model->date_to_ymd);
            if(!empty($model->agent_id)){
                $criteria->addCondition('t.agent_id=' . $model->agent_id);
            }
        }
        
        if(!empty($model->sale_id)){
                $criteria->addCondition('t.sale_id=' . $model->sale_id);
        }
        if(!empty($model->customer_id)){
            $criteria->addCondition('t.customer_id=' . $model->customer_id);
        }

    }
    
    /** @Author: DungNT Apr 13, 2017 - get output */
    public function businessGetOutput(&$model, &$aRes) {
        $criteria = new CDBcriteria();
        $this->businessCriteria($model, $criteria, Sta2::REPORT_OUTPUT);
        $sParamsIn = implode(',', CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT);
        $criteria->addCondition("t.materials_type_id IN ($sParamsIn)");
        $sParamsIn = implode(',', CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC);
        $criteria->addCondition("t.type_in_out IN ($sParamsIn)");
        
        $criteria->select   = 'sum(qty) as qty, t.user_id_create, t.materials_type_id, t.type_customer, t.customer_id, t.sale_id';
        $criteria->group    = 't.customer_id, t.materials_type_id';
        
        $mRes = GasStoreCardDetail::model()->findAll($criteria);
        foreach($mRes as $item){
            $aRes['OUTPUT'][$item->customer_id][$item->materials_type_id] = $item->qty;
            $aRes['SALE_ID'][$item->sale_id]                = $item->sale_id;
            $aRes['SALE_OF_CUSTOMER'][$item->customer_id]   = $item->sale_id;
            $aRes['CUSTOMER_ID'][$item->customer_id]        = $item->customer_id;
        }
    }
    public function businessGetRemain($model, &$aRes){
        $criteria = new CDBcriteria();
        $criteria->addCondition("t.only_gas_remain=0");
        $this->businessCriteria($model, $criteria, Sta2::REPORT_REMAIN);
        $sParamsIn = implode(',', $aRes['CUSTOMER_ID']);
        $criteria->addCondition("t.customer_id IN ($sParamsIn)");
        $criteria->select   = "t.customer_id, sum(amount_gas) as amount_gas, sum(amount) as amount";
        $criteria->group    = "t.customer_id"; 
        $mRes = GasRemain::model()->findAll($criteria);
        foreach($mRes as $item){
            $aRes['REMAIN'][$item->customer_id] = $item->amount_gas;
        }
    }
    
    /** @Author: DungNT Apr 13, 2017
     * @Todo: BC kết quả kinh doanh
     */
    public function business(&$model) {
        try {
        $aRes = [];
        /* 1. tính sản lượng thực  tế rồi group theo customer
         * 2. tinh gas du
         */
        $model->date_from_ymd  = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
        $model->date_to_ymd    = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);
        $this->businessGetOutput($model, $aRes);
        
        if(isset($aRes['CUSTOMER_ID'])){
            $this->businessGetRemain($model, $aRes);
            // 3. lay target theo sale id
            $aUid = array_merge($aRes['SALE_ID'], $aRes['CUSTOMER_ID']);
            $aRes['CUSTOMER_MODEL'] = Users::getArrObjecByRoleGroupByProvince('', array('aUid'=> $aUid, 'only_id_model'=>1));
            $temp = explode('-', $model->date_from_ymd);
            $aRes['PRICE']          = UsersPrice::getPriceOfListCustomer($temp[1], $temp[0], $aRes['CUSTOMER_ID']);
        }
        
        return $aRes;
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    /** @Author: DungNT Jun 23, 2017
     * @Todo: BC công thực tế NVGN
     */
    public function employeeMaintain(&$model) {
        try {
        $aRes = [];
        /* 1. sum + group by material_type bên thẻ kho, KH bò mối trừ KH hộ GĐ ra rồi group theo employee
        /* 2. sum + group by material_type bên table Sell rồi group theo employee
         * 3. foreach cả 2 để gộp lại 1 mảng 
         */
        $model->date_from_ymd  = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
        $model->date_to_ymd    = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);
        $this->employeeMaintainGetOutputStorecard($model, $aRes);
        $this->employeeMaintainGetOutputSell($model, $aRes);
        $this->employeeMaintainGetOutputSellBoBinh($model, $aRes);
        $this->employeeMaintainGetUpholdHgd($model, $aRes);
//        $this->employeeMaintainGetErrors($model, $aRes);
        $this->getReportAppGas24h($model, $aRes);
        $newArr = [];// đưa ra mảng mới bỏ biến $materials_type_id đi đưa vào 1 mảng chỉ có materials_id để foreach
        foreach($aRes['MATERIALS_ID'] as $materials_type_id=>$aMaterial){
            $newArr += $aMaterial; 
        }
        $aRes['MATERIALS_ID'] = $newArr;
        $_SESSION['data-excel'] = $aRes;
        $_SESSION['data-model'] = $model;

        return $aRes;
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public function employeeMaintainSameCondition(&$criteria, $field_name) {
//        $sParamsIn = '118,1315';// id Agent - bỏ khỏi chấm công Đại lý An Thạnh + Dầu Khí Bình Định
        $sParamsIn = '1315';// id Agent - bỏ khỏi chấm công Đại lý An Thạnh + Dầu Khí Bình Định
        $criteria->addCondition("t.$field_name NOT IN ($sParamsIn)");
    }
    
    /** @Author: DungNT Apr 13, 2017 -  1. sum + group by material_type bên thẻ kho, KH bò mối trừ KH hộ GĐ ra rồi group theo employee */
    public function employeeMaintainGetOutputStorecard($model, &$aRes) {
        $criteria = new CDBcriteria();
        $this->employeeMaintainSameCondition($criteria, 'user_id_create');
//        $criteria->addBetweenCondition('t.date_delivery', $model->date_from_ymd, $model->date_to_ymd);
        DateHelper::searchBetween($model->date_from_ymd, $model->date_to_ymd, 'date_delivery_bigint', $criteria, false);
        $sParamsIn = implode(',', CmsFormatter::$aTypeIdMakeUsername);
        $criteria->addCondition("t.type_customer IN ($sParamsIn)");
        $sParamsIn = implode(',', CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC);
        $criteria->addCondition("t.type_in_out IN ($sParamsIn)");
        if(!empty($model->agent_id)){
            $criteria->addCondition('t.user_id_create='.$model->agent_id);
        }
        if(!empty($model->employee_maintain_id)){
            $criteria->addCondition('t.employee_maintain_id='.$model->employee_maintain_id);
        }
        
        if(count($model->province_id_agent)){
            $sParamsIn = implode(',', $model->province_id_agent);
            $criteria->addCondition("t.province_id_agent IN ($sParamsIn)");
        }
        $criteria->select   = 'sum(qty) as qty, t.employee_maintain_id, t.user_id_create, t.materials_id, t.materials_type_id';
        if(!isset($_GET['detail'])):
            $criteria->group    = 't.employee_maintain_id, t.materials_id';
        else:
            $criteria->group    = 't.employee_maintain_id, t.user_id_create, t.materials_id';
        endif;
        
//        $criteria->order    = 't.materials_type_id';;// Close Oct2418
        $aRes['MATERIALS_ID'] = [];
        $mRes = GasStoreCardDetail::model()->findAll($criteria);
        $this->employeeMaintainStorecardFormat($aRes, $mRes);
        $mRes = null;
    }
    
    /** @Author: DungNT Sep 05, 2017
     * @Todo: format data detail
     * @Jul0618_note: $aRes['OUTPUT_GAS'] is sản lượng bò mối 
     */
    public function employeeMaintainStorecardFormat(&$aRes, $mRes) {
        if(!isset($_GET['detail'])):
            foreach($mRes as $item){
                $aRes['EMPLOYEE_ID'][$item->employee_maintain_id]    = $item->user_id_create;
                if(!in_array($item->materials_type_id, CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT) ){
                    // Cộng riêng vật tư bếp van dây
                    $aRes['MATERIALS_ID'][$item->materials_type_id][$item->materials_id]    = $item->materials_id;
                    $aRes['OUTPUT'][$item->employee_maintain_id][$item->materials_id]       = $item->qty;
                }else{// gas12,45,50
                    if(!isset($aRes['OUTPUT_GAS'][$item->employee_maintain_id][$item->materials_type_id])){
                        $aRes['OUTPUT_GAS'][$item->employee_maintain_id][$item->materials_type_id]      = $item->qty;
                    }else{
                        $aRes['OUTPUT_GAS'][$item->employee_maintain_id][$item->materials_type_id]      += $item->qty;
                    }
                }
            }
        else:
            foreach($mRes as $item){
                $aRes['EMPLOYEE_ID'][$item->employee_maintain_id][$item->user_id_create]    = $item->user_id_create;
                if(!in_array($item->materials_type_id, CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT) ){
                    // Cộng riêng vật tư bếp van dây
                    $aRes['MATERIALS_ID'][$item->materials_type_id][$item->materials_id]    = $item->materials_id;
                    $aRes['OUTPUT'][$item->employee_maintain_id][$item->user_id_create][$item->materials_id]       = $item->qty;
                }else{// gas12,45,50. Jul0618 tach rieng B12 HGD va b12 Bo Moi - OUTPUT_GAS is B12,45 bo moi
                    if(!isset($aRes['OUTPUT_GAS'][$item->employee_maintain_id][$item->user_id_create][$item->materials_type_id])){
                        $aRes['OUTPUT_GAS'][$item->employee_maintain_id][$item->user_id_create][$item->materials_type_id]      = $item->qty;
                    }else{
                        $aRes['OUTPUT_GAS'][$item->employee_maintain_id][$item->user_id_create][$item->materials_type_id]      += $item->qty;
                    }
                }
            }
        endif;
    }
    
    /** @Author: DungNT Apr 13, 2017 - get output */
    public function employeeMaintainGetOutputSell($model, &$aRes) {
        $criteria = new CDBcriteria();
        $this->employeeMaintainSameCondition($criteria, 'agent_id');
//        $criteria->addBetweenCondition('t.created_date_only', $model->date_from_ymd, $model->date_to_ymd);
        DateHelper::searchBetween($model->date_from_ymd, $model->date_to_ymd, 'created_date_only_bigint', $criteria, false);
        $criteria->addCondition('t.status=' . Sell::STATUS_PAID);
        if(!empty($model->agent_id)){
            $criteria->addCondition('t.agent_id='.$model->agent_id);
        }
        if(!empty($model->employee_maintain_id)){
            $criteria->addCondition('t.employee_maintain_id='.$model->employee_maintain_id);
        }
        if(count($model->province_id_agent)){
            $sParamsIn = implode(',', $model->province_id_agent);
            $criteria->addCondition("t.province_id IN ($sParamsIn)");
        }
        $sParamsIn = implode(',', GasMaterialsType::getTypeVoNot12kg()).','.GasMaterialsType::MATERIAL_TYPE_PROMOTION;
        $criteria->addCondition("t.materials_type_id NOT IN ($sParamsIn)");
        $criteria->addCondition("t.order_type<>".Sell::ORDER_TYPE_BO_BINH);
        $sParamsIn = implode(',', GasMaterials::getIdNotReportPay());// khong lay vat tu phu
        $criteria->addCondition("t.materials_id NOT IN ($sParamsIn)");
            
        $criteria->select   = 'sum(qty) as qty, t.employee_maintain_id, t.agent_id, t.materials_id, t.materials_type_id';
        if(!isset($_GET['detail'])):
            $criteria->group    = 't.employee_maintain_id, t.materials_id';
        else:
            $criteria->group    = 't.employee_maintain_id, t.agent_id, t.materials_id';
        endif;
//        $criteria->order    = 't.materials_type_id';// Close Oct2418
        
        $mRes = SellDetail::model()->findAll($criteria);
        $this->employeeMaintainSelldFormat($aRes, $mRes);
        $mRes = null;
    }
    /** @Author: DungNT Sep 05, 2017
     * @Todo: format data detail
     */
    public function employeeMaintainSelldFormat(&$aRes, $mRes) {
        if(!isset($_GET['detail'])):
            foreach($mRes as $item):
                $aRes['EMPLOYEE_ID'][$item->employee_maintain_id]   = $item->agent_id;
                if(!in_array($item->materials_type_id, CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT)){
                    // Xử lý cộng riêng vỏ 12 de so sanh voi Gas
                    if($item->materials_type_id == MATERIAL_TYPE_VO_12){
                        if(!isset($aRes['OUTPUT_VO_HGD'][$item->employee_maintain_id][$item->materials_type_id])){
                            $aRes['OUTPUT_VO_HGD'][$item->employee_maintain_id][$item->materials_type_id]      = $item->qty;
                        }else{
                            $aRes['OUTPUT_VO_HGD'][$item->employee_maintain_id][$item->materials_type_id]      += $item->qty;
                        }
                        continue ;
                    }
                    // Cộng riêng vật tư bếp van dây
                    $aRes['MATERIALS_ID'][$item->materials_type_id][$item->materials_id] = $item->materials_id;
                    if(!isset($aRes['OUTPUT'][$item->employee_maintain_id][$item->materials_id])){
                        $aRes['OUTPUT'][$item->employee_maintain_id][$item->materials_id] = $item->qty;
                    }else{
                        $aRes['OUTPUT'][$item->employee_maintain_id][$item->materials_id] += $item->qty;
                    }
                }else{// gas12,45,50. Jul0618 tach rieng B12 HGD va b12 Bo Moi
                    if(!isset($aRes['OUTPUT_GAS_HGD'][$item->employee_maintain_id][$item->materials_type_id])){
                        $aRes['OUTPUT_GAS_HGD'][$item->employee_maintain_id][$item->materials_type_id]      = $item->qty;
                    }else{
                        $aRes['OUTPUT_GAS_HGD'][$item->employee_maintain_id][$item->materials_type_id]      += $item->qty;
                    }
                }
            endforeach;
        else:
            foreach($mRes as $item):
                $aRes['EMPLOYEE_ID'][$item->employee_maintain_id][$item->agent_id]   = $item->agent_id;
                if(!in_array($item->materials_type_id, CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT)){
                    if($item->materials_type_id == MATERIAL_TYPE_VO_12){
                        continue;// trong detail thì ko cộng vỏ 12 vào
                    }
                    // Cộng riêng vật tư bếp van dây
                    $aRes['MATERIALS_ID'][$item->materials_type_id][$item->materials_id] = $item->materials_id;
                    if(!isset($aRes['OUTPUT'][$item->employee_maintain_id][$item->agent_id][$item->materials_id])){
                        $aRes['OUTPUT'][$item->employee_maintain_id][$item->agent_id][$item->materials_id] = $item->qty;
                    }else{
                        $aRes['OUTPUT'][$item->employee_maintain_id][$item->agent_id][$item->materials_id] += $item->qty;
                    }
                }else{
                    if(!isset($aRes['OUTPUT_GAS'][$item->employee_maintain_id][$item->agent_id][$item->materials_type_id])){
                        $aRes['OUTPUT_GAS'][$item->employee_maintain_id][$item->agent_id][$item->materials_type_id]      = $item->qty;
                    }else{
                        $aRes['OUTPUT_GAS'][$item->employee_maintain_id][$item->agent_id][$item->materials_type_id]      += $item->qty;
                    }
                }
            endforeach;
        endif;
    }
    
    /** @Author: DungNT Apr 13, 2017 - get output Sell Bộ Bình*/
    public function employeeMaintainGetOutputSellBoBinh($model, &$aRes) {
        $criteria = new CDBcriteria();
        $this->employeeMaintainSameCondition($criteria, 'agent_id');
//        $criteria->addBetweenCondition('t.created_date_only', $model->date_from_ymd, $model->date_to_ymd);
        DateHelper::searchBetween($model->date_from_ymd, $model->date_to_ymd, 'created_date_only_bigint', $criteria, false);
        $criteria->addCondition('t.status=' . Sell::STATUS_PAID);
        if(count($model->province_id_agent)){
            $sParamsIn = implode(',', $model->province_id_agent);
            $criteria->addCondition("t.province_id IN ($sParamsIn)");
        }
        if(!empty($model->agent_id)){
            $criteria->addCondition('t.agent_id='.$model->agent_id);
        }
        if(!empty($model->employee_maintain_id)){
            $criteria->addCondition('t.employee_maintain_id='.$model->employee_maintain_id);
        }
        $sDataNotGet = GasMaterialsType::MATERIAL_TYPE_BEP;// ?? tai sao khong lay bep o day
//        $sParamsIn = implode(',', CmsFormatter::$MATERIAL_TYPE_BINHBO_INPUT).','.GasMaterialsType::MATERIAL_TYPE_PROMOTION.','.$sDataNotGet;
        $sParamsIn = implode(',', CmsFormatter::$MATERIAL_TYPE_BINHBO_INPUT).','.GasMaterialsType::MATERIAL_TYPE_PROMOTION;
        $criteria->addCondition("t.materials_type_id NOT IN ($sParamsIn)");
        $criteria->addCondition("t.order_type=".Sell::ORDER_TYPE_BO_BINH);
        $criteria->select   = 't.qty, t.employee_maintain_id, t.sell_id, t.agent_id, t.materials_id, t.materials_type_id';
//        $criteria->group    = 't.employee_maintain_id, t.materials_id';
        
        $mRes = SellDetail::model()->findAll($criteria);
        $this->employeeMaintainSellBoBinhFormat($aRes, $mRes);
        $mRes = null;
    }
    
    /** @Author: DungNT Sep 05, 2017
     * @Todo: format data detail
     */
    public function employeeMaintainSellBoBinhFormat(&$aRes, $mRes) {
        $aTypeDay = [GasMaterials::ID_DAY_DEN, GasMaterials::ID_DAY_INOX];
        if(!isset($_GET['detail'])):
            foreach($mRes as $item){
                $aRes['EMPLOYEE_ID'][$item->employee_maintain_id]   = $item->agent_id;
                if($item->materials_type_id == GasMaterialsType::MATERIAL_BINH_12KG){
                    if(!isset($aRes['QTY_BOBINH'][$item->employee_maintain_id])){
                        $aRes['QTY_BOBINH'][$item->employee_maintain_id] = 1;
                    }else{
                        $aRes['QTY_BOBINH'][$item->employee_maintain_id] +=1;
                    }
                    continue ;
                }
                
//                Sep0917 không cộng dây đen và dây inox với bộ bình 
                if(in_array($item->materials_id, $aTypeDay)){
                    continue ;
                }
                // Cộng riêng vật tư bếp van dây
                $aRes['MATERIALS_ID'][$item->materials_type_id][$item->materials_id] = $item->materials_id;
    //            $aRes['OUTPUT_BOBINH'][$item->employee_maintain_id][$item->materials_id] = $item->qty;
                // có thể không cần đoạn dưới, vì đã sum qty và group rồi: 't.employee_maintain_id, t.materials_id'
                if(!isset($aRes['OUTPUT_BOBINH'][$item->employee_maintain_id][$item->materials_id])){
                    $aRes['OUTPUT_BOBINH'][$item->employee_maintain_id][$item->materials_id] = $item->qty;
                }else{
                    $aRes['OUTPUT_BOBINH'][$item->employee_maintain_id][$item->materials_id] += $item->qty;
                }
            }
        else:
            foreach($mRes as $item){
                $aRes['EMPLOYEE_ID'][$item->employee_maintain_id][$item->agent_id]   = $item->agent_id;
                if($item->materials_type_id == GasMaterialsType::MATERIAL_BINH_12KG){
                    if(!isset($aRes['QTY_BOBINH'][$item->employee_maintain_id][$item->agent_id])){
                        $aRes['QTY_BOBINH'][$item->employee_maintain_id][$item->agent_id] = 1;
                    }else{
                        $aRes['QTY_BOBINH'][$item->employee_maintain_id][$item->agent_id] +=1;
                    }
                    continue ;
                }
//                Sep0917 không cộng dây đen và dây inox với bộ bình 
                if(in_array($item->materials_id, $aTypeDay)){
                    continue ;
                }
                
                // Cộng riêng vật tư bếp van dây
                $aRes['MATERIALS_ID'][$item->materials_type_id][$item->materials_id] = $item->materials_id;
    //            $aRes['OUTPUT_BOBINH'][$item->employee_maintain_id][$item->materials_id] = $item->qty;
                // có thể không cần đoạn dưới, vì đã sum qty và group rồi: 't.employee_maintain_id, t.materials_id'
                if(!isset($aRes['OUTPUT_BOBINH'][$item->employee_maintain_id][$item->agent_id][$item->materials_id])){
                    $aRes['OUTPUT_BOBINH'][$item->employee_maintain_id][$item->agent_id][$item->materials_id] = $item->qty;
                }else{
                    $aRes['OUTPUT_BOBINH'][$item->employee_maintain_id][$item->agent_id][$item->materials_id] += $item->qty;
                }
            }
        endif;
    }
    /** @Author: DungNT Apr 24, 2017 - get output Uphold HGD*/
    public function employeeMaintainGetUpholdHgd($model, &$aRes) {
        $criteria = new CDBcriteria();
        $this->employeeMaintainSameCondition($criteria, 'agent_id');
        $criteria->addBetweenCondition('t.created_date_only', $model->date_from_ymd, $model->date_to_ymd);
        $criteria->addCondition('t.status=' . UpholdHgd::STATUS_COMPLETE);
        if(!empty($model->agent_id)){
            $criteria->addCondition('t.agent_id='.$model->agent_id);
        }
        if(!empty($model->employee_maintain_id)){
            $criteria->addCondition('t.employee_id='.$model->employee_maintain_id);
        }
        $criteria->select   = 'count(t.id) as id, t.employee_id, t.agent_id';
        $criteria->group    = 't.employee_id, t.agent_id';
        if(!isset($_GET['detail'])):
            $criteria->group    = 't.employee_id';
        else:
            $criteria->group    = 't.employee_id, t.agent_id';
        endif;
        $mRes = UpholdHgd::model()->findAll($criteria);
        $this->employeeMaintainUpholdHgdFormat($aRes, $mRes);
    }
    
    /** @Author: DungNT Sep 05, 2017
     * @Todo: format data detail
     */
    public function employeeMaintainUpholdHgdFormat(&$aRes, $mRes) {
        if(!isset($_GET['detail'])):
            foreach($mRes as $item){
                $aRes['UPHOLD_HGD'][$item->employee_id] = $item->id;
            }
        else:
            foreach($mRes as $item){
                $aRes['UPHOLD_HGD'][$item->employee_id][$item->agent_id] = $item->id;
            }
        endif;
    }
    
    /** @Author: DungNT Now 23, 2017 - get output các lỗi phạt GN*/
    public function employeeMaintainGetErrors($model, &$aRes) {
        $criteria = new CDBcriteria();
        $criteria->addBetweenCondition('DATE(t.last_update)', $model->date_from_ymd, $model->date_to_ymd);
        if(!empty($model->employee_maintain_id)){
            $criteria->addCondition('t.user_id='.$model->employee_maintain_id);
        }
        $mMonitorUpdate = new MonitorUpdate();
        $aIdErrors = array_keys($mMonitorUpdate->getArrayErrors());
        $sParamsIn = implode(',', $aIdErrors);
        $criteria->addCondition("t.type IN ($sParamsIn)");
        $criteria->select   = 'COUNT(t.id) as id, t.user_id, t.type';
        $criteria->group    = 't.type, t.user_id';

        $mRes = MonitorUpdate::model()->findAll($criteria);
        $this->employeeMaintainGetErrorsFormat($aRes, $mRes);
        $mRes = null;
    }
    public function employeeMaintainGetErrorsFormat(&$aRes, $mRes) {
        foreach($mRes as $item){
            $aRes['ERRORS'][$item->type][$item->user_id] = $item->id;
        }
    }
    
    /** @Author: DungNT Jun 13, 2018
     *  @Todo: thong ke SL nhap ma va BQV cua PVKH App Gas24h
     **/
    public function getReportAppGas24h($model, &$aRes){
        $mPromotionUser     = new AppPromotionUser();
        $mAppCache          = new AppCache();
        $mPromotionUser->unsetAttributes();
        $mPromotionUser->aEmployee  = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MAINTAIN);
        $mPromotionUser->date_from  = $model->date_from;
        $mPromotionUser->date_to    = $model->date_to;
        $aData = $mPromotionUser->getReportMarketing();
        if(isset($aData['OUT_TRACKING'])){
            $aRes['GAS24H_INPUT_CODE']  = $aData['OUT_TRACKING'];
        }
        if(isset($aData['OUT_SELL_BY_USER'])){
            $aRes['GAS24H_SELL_BQV']    = $aData['OUT_SELL_BY_USER'];
        }
    }
    

    /** @Author: DungNT Apr 24, 2017 - tính toán price từng vật tư cho GN
     * @param: $type : 1: vật tư, 2 bộ bình, 3: uphold bảo trì
     */
    public static function employeeMaintainCalcPrice(&$price, $type, $qty, $employee_maintain_id, $materials_type_id, $materials_id) {
        if($qty < 0){
            return 0;
        }
        if($type == Sta2::SALARY_TYPE_BOBINH){
            $price = 50000;
            return $price*$qty;// 50K
        }elseif($type == Sta2::SALARY_TYPE_UPHOLD){
            $price = 8000;
            return $price*$qty;// 8K
        }
        
        $amount = 0;
        switch ($materials_type_id) {
            case MATERIAL_TYPE_BINH_12:
                $price = 9000;
                break;
            case MATERIAL_TYPE_BINHBO_50:
            case MATERIAL_TYPE_BINHBO_45:
                $price = 25000;
                break;
            case MATERIAL_TYPE_BINH_6:
            case GasMaterialsType::MATERIAL_BINH_4KG:
                $price = 4500;
                break;
            case GasMaterialsType::MATERIAL_TYPE_VAL:
                $price = 20000;
                break;
            case GasMaterialsType::MATERIAL_TYPE_BEP:
                $price = 20000;
                break;
        }
        $amount = $price*$qty;
        if(!empty($amount)){
            return $amount;
        }
        $amount = 0; $unitDayDen = 1.5;
        switch ($materials_id) {
            case GasMaterials::ID_DAY_DEN:
                $price = 20000;
                $amount = $price*(round($qty/$unitDayDen));
                break;
            case GasMaterials::ID_DAY_INOX:
            case GasMaterials::ID_DAY_MEM_CN:
                $price = 20000;
                $amount = $price*$qty;
                break;
        }
        return $amount;
    }
    
    /** @Author: DungNT Sep 05, 2017
     * @Todo: BC chi tiết công thực tế NVGN
     */
    public function employeeMaintainDetail(&$model) {
        try {
        $aRes = [];
        /* 1. sum + group by material_type bên thẻ kho, trừ KH hộ GĐ ra rồi group theo employee
        /* 2. sum + group by material_type bên table Sell rồi group theo employee
         * 3. foreach cả 2 để gộp lại 1 mảng
         */
        $model->date_from_ymd  = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
        $model->date_to_ymd    = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);
        $this->employeeMaintainGetOutputStorecard($model, $aRes);
        $this->employeeMaintainGetOutputSell($model, $aRes);
        $this->employeeMaintainGetOutputSellBoBinh($model, $aRes);
        $this->employeeMaintainGetUpholdHgd($model, $aRes);
        $newArr = [];
        foreach($aRes['MATERIALS_ID'] as $materials_type_id=>$aMaterial){
            $newArr += $aMaterial; 
        }
        $aRes['MATERIALS_ID'] = $newArr;
        $_SESSION['data-excel'] = $aRes;
        $_SESSION['data-model'] = $model;

        return $aRes;
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    /** @Author: DungNT Sep 08, 2017
     *  @Todo: BC app triển khai $model is model GasAppOrder
     */
    public function appReport($model) {
        $aRes = [];
        // 1. tổng số KH kích hoạt từng tháng theo từng sale
        // 2. Tổng số KH đặt app từng tháng theo từng sale
        $this->appReportActive($model, $aRes);
        $this->appReportOrders($model, $aRes);
        return $aRes;
    }
    
    public function appReportActive($model, &$aRes) {
        // 1. tổng số KH kích hoạt từng tháng theo từng sale
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.type='.MonitorUpdate::TYPE_ACTIVE_USER);
        $criteria->addCondition('t.status_obj='.ROLE_CUSTOMER);
        $criteria->select   = 'count(t.id) as id, t.agent_id, MONTH(t.last_update) as onlyMonth, YEAR(t.last_update) as onlyYear';
        $criteria->group    = 't.agent_id, MONTH(t.last_update), YEAR(t.last_update)';
//        $criteria->order    = 't.last_update';
        $mRes = MonitorUpdate::model()->findAll($criteria);
        foreach($mRes as $item){
            $key = $item->onlyMonth.'-'.$item->onlyYear;
            $aRes['ACTIVE'][$item->agent_id][$key]  = $item->id;
            $aRes['SALE_ID'][$item->agent_id]       = $item->agent_id;// agent_id is sale_id
//            $aRes['KEY'][$key] = $key;
        }
    }
    public function appReportOrders($model, &$aRes) {
        // 1. Tổng số KH đặt app từng tháng theo từng sale
        $criteria = new CDbCriteria();
        $this->appReportOrdersCriteria($model, $criteria);
        $criteria->select   = 'count(DISTINCT t.customer_id) as customer_id, t.sale_id, MONTH(t.date_delivery) as onlyMonth, YEAR(t.date_delivery) as onlyYear';
        $criteria->group    = 't.sale_id, MONTH(t.date_delivery), YEAR(t.date_delivery)';
        $criteria->order    = 't.date_delivery';
        $mRes = GasAppOrder::model()->findAll($criteria);
        foreach($mRes as $item){
            $key = $item->onlyMonth.'-'.$item->onlyYear;
            $aRes['ORDERS'][$item->sale_id][$key]  = $item->customer_id;
            $aRes['SALE_ID'][$item->sale_id]       = $item->sale_id;
            $aRes['KEY'][$key] = $key;
        }
        
        // For sum all DISTINCT customer
        $criteria = new CDbCriteria();
        $this->appReportOrdersCriteria($model, $criteria);
        $criteria->select   = 'count(DISTINCT t.customer_id) as customer_id, t.sale_id';
        $criteria->group    = 't.sale_id';
        $mRes = GasAppOrder::model()->findAll($criteria);
        foreach($mRes as $item){
            $aRes['SUM_ORDERS'][$item->sale_id]  = $item->customer_id;
        }
    }
    
    public function appReportOrdersCriteria($model, &$criteria) {
        $criteria->addCondition("t.date_delivery>='2017-06-01'");
        $criteria->addCondition('t.status='.  GasAppOrder::STATUS_COMPPLETE);
        $criteria->addCondition('t.uid_login_role='.ROLE_CUSTOMER);
    }

    /** @Author: DungNT Sep 17, 2017
     *  @Todo: report loại KH cũ mới trong HGĐ
     */
    public function hgdSource($model) {
        $criteria = new CDbCriteria();
        $model->date_from_ymd  = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
        $model->date_to_ymd    = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);
        $criteria->addBetweenCondition('t.created_date_only', $model->date_from_ymd, $model->date_to_ymd);
        if(!empty($model->agent_id)){
            $criteria->addCondition('t.agent_id='.$model->agent_id);
        }
        if(!empty($model->status)){
            $criteria->addCondition('t.status='.$model->status);
        }
        if(!empty($model->status_cancel)){
            $criteria->addCondition('t.status_cancel='.$model->status_cancel);
        }
        if(count($model->province_id)){
            $sParamsIn = implode(',', $model->province_id);
            $criteria->addCondition("t.province_id IN ($sParamsIn)");
        }
        $criteria->addCondition('t.customer_new > 0');
        $criteria->select   = 'count(t.id) as id, t.customer_new, t.created_date_only, t.agent_id';
        $criteria->group    = 't.agent_id, t.created_date_only, t.customer_new';
        $criteria->order    = 't.agent_id';
        $mRes = Sell::model()->findAll($criteria);
        foreach($mRes as $item){
            $aRes['CUSTOMERS'][$item->agent_id][$item->created_date_only][$item->customer_new]  = $item->id;
            $aRes['AGENT_ID'][$item->agent_id]     = $item->agent_id;
            if(!isset($aRes['AGENT_SUM'][$item->agent_id][$item->customer_new])){
                $aRes['AGENT_SUM'][$item->agent_id][$item->customer_new] = $item->id;
            }else{
                $aRes['AGENT_SUM'][$item->agent_id][$item->customer_new] += $item->id;
            }
            
            if(!isset($aRes['DAY_SUM'][$item->created_date_only][$item->customer_new])){
                $aRes['DAY_SUM'][$item->created_date_only][$item->customer_new] = $item->id;
            }else{
                $aRes['DAY_SUM'][$item->created_date_only][$item->customer_new] += $item->id;
            }
        }
        $aRes['ARR_DAYS'] = MyFormat::getArrayDay($model->date_from_ymd, $model->date_to_ymd);
        return $aRes;
    }
    
    public function getStringAgentId() {
        $mAppCache = new AppCache();
        $aAgent = $mAppCache->getAgentListdata();
        $sAgent = '';
        foreach($aAgent as $agentId => $agentName){
            $sAgent .= ",$agentId";
        }
        return trim($sAgent, ',');
    }
    
     /** @Author: DungNT Oct 09, 2017
     *  @Todo: BC app triển khai $model is model GasAppOrder
      * 1. tính sản lượng - doanh thu bò mối
      * 2. tính sản lượng - doanh thu HGĐ
     */
    public function soldOut($mAppOrder) {
        $aRes = $mRes = [];
        $sParamsAgent = $this->getStringAgentId();
                
        $mAppOrder->date_from_ymd  = MyFormat::dateDmyToYmdForAllIndexSearch($mAppOrder->date_from);
        $mAppOrder->date_to_ymd    = MyFormat::dateDmyToYmdForAllIndexSearch($mAppOrder->date_to);
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.customer_id NOT IN ($sParamsAgent)");//không lấy xuất, nhập nội bộ
        $this->soldOutCriteria($mAppOrder, $criteria);
        $criteria->select   = 'sum(t.qty) as qty, sum(t.amount) as amount, sum(t.kg_empty) as kg_empty, sum(t.kg_has_gas) as kg_has_gas, '
                . 'sum(t.pay_back) as pay_back, sum(t.total_gas) as total_gas, sum(t.total_gas_du) as total_gas_du,'
                . 't.materials_id, t.materials_type_id';
        $criteria->group    = 't.materials_type_id, t.materials_id';
        if(!$this->isViewOnlyHgd($mAppOrder)) {
            $mRes = GasAppOrderCDetailReal::model()->findAll($criteria);
        }
        foreach($mRes as $item){
            $aRes[$item->materials_type_id][$item->materials_id]['qty']         = $item->qty;
            $aRes[$item->materials_type_id][$item->materials_id]['amount']      = $item->amount;
            $aRes[$item->materials_type_id][$item->materials_id]['kg_empty']    = $item->kg_empty;
            $aRes[$item->materials_type_id][$item->materials_id]['kg_has_gas']  = $item->kg_has_gas;
            $aRes[$item->materials_type_id][$item->materials_id]['total_gas_du']  = $item->total_gas_du;
            $aRes[$item->materials_type_id][$item->materials_id]['pay_back']    = $item->pay_back;
        }
        $this->soldOutHgd($mAppOrder, $aRes);
        return $aRes;
    }
    public function soldOutCriteria($mAppOrder, &$criteria) {
        $cRole = MyFormat::getCurrentRoleId();
        $criteria->addBetweenCondition('t.date_delivery', $mAppOrder->date_from_ymd, $mAppOrder->date_to_ymd);
        $criteria->addCondition('t.status='.  GasAppOrder::STATUS_COMPPLETE);
        $mAppOrder->handleRole($cRole, $criteria);
        $mAppOrder->handleProvince($criteria);
        $mAppOrder->handleLimitCustomer($criteria);
        $this->soldOutCriteriaSame($criteria);
        GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');
    }
    public function soldOutHgd($mAppOrder, &$aRes) {
        if( !empty($mAppOrder->customer_contact)
//            || (!empty($mAppOrder->type_customer) && !in_array($mAppOrder->type_customer, $aTypeAllow))
            || !$this->isViewOnlyHgd($mAppOrder)
        ) {
            return ;
        }
        
        $criteria = new CDbCriteria();
        $this->soldOutCriteriaSame($criteria);
        $criteria->addBetweenCondition('t.created_date_only', $mAppOrder->date_from_ymd, $mAppOrder->date_to_ymd);
        if(is_array($mAppOrder->agent_id)){
            $sParamsIn = implode(',', $mAppOrder->agent_id);
            $criteria->addCondition("t.agent_id IN ($sParamsIn)");
        }elseif(!empty($mAppOrder->agent_id)){
            $criteria->addCondition('t.agent_id=' . $mAppOrder->agent_id);
        }
        $criteria->addCondition('t.status=' . Sell::STATUS_PAID);
        if(!empty($mAppOrder->customer_id)){
            $criteria->addCondition('t.customer_id=' . $mAppOrder->customer_id);
        }
        $mAppOrder->handleProvince($criteria);
        $criteria->select = 'sum(t.promotion_amount) as promotion_amount, sum(t.amount_bu_vo) as amount_bu_vo, sum(t.qty) as qty, sum(t.amount) as amount, sum(t.qty_discount) as qty_discount, sum(t.amount_discount) as amount_discount, t.agent_id, t.employee_maintain_id, t.materials_type_id, t.materials_id, t.price';
        $criteria->group  = 't.agent_id, t.materials_id';
        $mRes = SellDetail::model()->findAll($criteria);
        foreach($mRes as $item){
            if(!isset($aRes[$item->materials_type_id][$item->materials_id]['qty'])){
                $aRes[$item->materials_type_id][$item->materials_id]['qty']     = $item->qty;
            }else{
                $aRes[$item->materials_type_id][$item->materials_id]['qty']     += $item->qty;
            }
            if(!isset($aRes[$item->materials_type_id][$item->materials_id]['amount'])){
                $aRes[$item->materials_type_id][$item->materials_id]['amount']     = $item->amount;
            }else{
                $aRes[$item->materials_type_id][$item->materials_id]['amount']     += $item->amount;
            }
            
            if(!isset($aRes[$item->materials_type_id][$item->materials_id]['kg_empty'])){
                $aRes[$item->materials_type_id][$item->materials_id]['kg_empty'] = 0;
            }
            if(!isset($aRes[$item->materials_type_id][$item->materials_id]['kg_has_gas'])){
                $aRes[$item->materials_type_id][$item->materials_id]['kg_has_gas'] = 0;
            }
            if(!isset($aRes[$item->materials_type_id][$item->materials_id]['total_gas_du'])){
                $aRes[$item->materials_type_id][$item->materials_id]['total_gas_du'] = 0;
            }
            if(!isset($aRes[$item->materials_type_id][$item->materials_id]['pay_back'])){
                $aRes[$item->materials_type_id][$item->materials_id]['pay_back'] = 0;
            }
        }
    }
    public function isViewOnlyHgd($mAppOrder) {
        if(is_array($mAppOrder->type_customer) && count($mAppOrder->type_customer) == 1){
            if(in_array(STORE_CARD_HGD, $mAppOrder->type_customer)){
                return true;
            }
        }
        return false;
    }
    public function soldOutCriteriaSame(&$criteria) {
        $sta2 = new Sta2();
        $sta2->employeeMaintainSameCondition($criteria, 'agent_id');
    }
    
    
    /** @Author: DungNT Jan 25, 2018
     *  @Todo: Bc KH app: tính từ ngày ??? => tổng số kích hoạt, tổng đã đặt thành công, hủy,
        tổng số đặt trên 1 lần, trên 2 lần, 
        Tổng số KM 1 bình,
        Tổng số KH sử dụng mã KM
     **/
    public function reportGas24h() {
        /* 1. Tổng số KH kích hoạt (tạo trên hệ thống) - group by month
         * 2. Tổng số đặt thành công  - group by month
         * 3. tổng số KH đặt trên 1 lần
         * 4. sử dụng KM 1 bình
         * 5. sử dụng mã giới thiệu (100k)
         * 6. thống kê KH đặt theo từng ngày
         */
        $aRes = [];
        $this->gas24hGetActiveUser($aRes);
        $this->gas24hGetOrder($aRes);
        $this->gas24hGetMoreThanOneOrder($aRes);
        $this->gas24hGetPromotionRef($aRes);
        $this->gas24hGetPromotionOther($aRes);
        $this->gas24hDaily($aRes);
        $aRes['aNameParam']['aActiveUser']      = 'KH kích hoạt';
        $aRes['aNameParam']['aTotalOrder']      = 'Tổng số đơn hàng';
        $aRes['aNameParam']['aTotalOrderCancel']= 'Đơn hàng hủy';
        $aRes['aNameParam']['aTotalMoreThanOne']= 'KH đặt trên 1 lần';
//        $aRes['aNameParam']['aTotalFree']           = 'KH miễn phí 1 bình';
        $aRes['aNameParam']['aTotalPromotionRef']   = 'KH dùng mã giới thiệu';
        $mAppPromotion = new AppPromotion();
        $aRes['aPromotion']= $mAppPromotion->getListdataAll(['GetModel'=>1]);
        arsort($aRes['KEY']);

        return $aRes;
    }
    // 1. Tổng số KH kích hoạt (tạo trên hệ thống) - group by month
    public function gas24hGetActiveUser(&$aRes) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.role_id='.ROLE_CUSTOMER.' AND t.type='.CUSTOMER_TYPE_STORE_CARD.' AND t.is_maintain='.UsersExtend::STORE_CARD_HGD_APP );
//        $criteria->addCondition("DATE(t.created_date) > '{$this->gas24hBegin}'");
        DateHelper::searchGreater($this->gas24hBegin, 'created_date_bigint', $criteria);
//        $criteria->addCondition("DATE(t.created_date) > '{$this->gas24hBegin}'");
        $criteria->select   = 'count(t.id) as id, MONTH(t.created_date) as date_month, YEAR(t.created_date) as date_year';
        $criteria->group    = 'MONTH(t.created_date), YEAR(t.created_date)';
        $criteria->order    = 't.id ASC';// Oct2218 ko thể bỏ đc, nếu bỏ thì khi render ra bị sai
        $models = Users::model()->findAll($criteria);
        foreach($models as $item){
            $item->date_month = $item->date_month < 10 ? "0$item->date_month" : $item->date_month;
            $key = "$item->date_year-$item->date_month-01";
            $aRes['KEY'][$key] = strtotime($key);
            $aRes['aActiveUser'][$key]   = $item->id;
        }
    }
    // 2. Tổng số đặt thành công  - group by month
    public function gas24hGetOrder(&$aRes) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.source='.Sell::SOURCE_APP);
        $criteria->addCondition('t.materials_type_id = ' . GasMaterialsType::MATERIAL_BINH_12KG);
        $criteria->select   = 'count(t.id) as id, MONTH(t.created_date_only) as date_month, YEAR(t.created_date_only) as date_year, t.status';
        $criteria->group    = 't.status, MONTH(t.created_date_only), YEAR(t.created_date_only)';
        $models = SellDetail::model()->findAll($criteria);
        foreach($models as $item){
            $item->date_month = $item->date_month < 10 ? "0$item->date_month" : $item->date_month;
            $key = "$item->date_year-$item->date_month-01";
            $aRes['KEY'][$key] = strtotime($key);
            if($item->status == Sell::STATUS_PAID){
                if(!isset($aRes['aTotalOrder'][$key])){
                    $aRes['aTotalOrder'][$key]       = $item->id;
                }else{
                    $aRes['aTotalOrder'][$key]       += $item->id;
                }
            }
            if($item->status == Sell::STATUS_CANCEL){
                if(!isset($aRes['aTotalOrderCancel'][$key])){
                    $aRes['aTotalOrderCancel'][$key] = $item->id;
                }else{
                    $aRes['aTotalOrderCancel'][$key] += $item->id;
                }
            }
        }
    }
    // 3. tổng số KH đặt trên 1 lần
    public function gas24hGetMoreThanOneOrder(&$aRes) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.source='.Sell::SOURCE_APP);
        $criteria->addCondition('t.status='.Sell::STATUS_PAID. ' AND t.customer_new='.Sell::CUSTOMER_OLD);
        $criteria->select   = 'count(t.id) as id, MONTH(t.created_date_only) as date_month, YEAR(t.created_date_only) as date_year, t.customer_id';
        $criteria->group    = 't.customer_id, MONTH(t.created_date_only), YEAR(t.created_date_only)';
//        $criteria->having   = 'count(t.id) > 1';
        $models = Sell::model()->findAll($criteria);
        foreach($models as $item){
            $item->date_month = $item->date_month < 10 ? "0$item->date_month" : $item->date_month;
            $key = "$item->date_year-$item->date_month-01";
            $aRes['KEY'][$key] = strtotime($key);
            if(!isset($aRes['aTotalMoreThanOne'][$key])){
                $aRes['aTotalMoreThanOne'][$key] = 1;
            }else{
                $aRes['aTotalMoreThanOne'][$key]++;
            }
            $aRes['aCustomerMoreThanOne'][$key][$item->customer_id]   = $item->customer_id;
        }
    }
    // 4. sử dụng KM 1 bình + 5. sử dụng mã giới thiệu (100k)
    public function gas24hGetPromotionRef(&$aRes) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.status_use='.  AppPromotionUser::STATUS_COMPLETE. ' AND t.date_apply IS NOT NULL');
        $sPromotionType = AppPromotion::TYPE_FIRST_USER;
        $criteria->addCondition("t.promotion_type IN ($sPromotionType)");
        $criteria->select   = 't.promotion_type, count(t.id) as id, MONTH(t.date_apply) as onlyMonth, YEAR(t.date_apply) as onlyYear';
        $criteria->group    = 't.promotion_type, MONTH(t.date_apply), YEAR(t.date_apply)';
        $models = AppPromotionUser::model()->findAll($criteria);
        foreach($models as $item){
            if(empty($item->onlyMonth)){
                continue ;
            }
            $item->onlyMonth = $item->onlyMonth < 10 ? "0$item->onlyMonth" : $item->onlyMonth;
            $key = "$item->onlyYear-$item->onlyMonth-01";
            $aRes['KEY'][$key] = strtotime($key);
            $nameParam = 'aTotalFree';
            if($item->promotion_type == AppPromotion::TYPE_FIRST_USER){
                $nameParam = 'aTotalPromotionRef';
            }
            if(!isset($aRes[$nameParam][$key])){
                $aRes[$nameParam][$key] = $item->id;
            }else{
                $aRes[$nameParam][$key] += $item->id;
            }
        }
    }
    
    // 5. sử dụng các loại KM trừ KM sử dụng mã giới thiệu (100k)
    public function gas24hGetPromotionOther(&$aRes) {
        $mAppPromotion = new AppPromotion();
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.status_use='.  AppPromotionUser::STATUS_COMPLETE. ' AND t.date_apply IS NOT NULL');
        $sPromotionType = implode(',', $mAppPromotion->getTypeReport());
        $criteria->addCondition("t.promotion_type IN ($sPromotionType)");
        $criteria->select   = 't.promotion_id, t.promotion_type, count(t.id) as id, MONTH(t.date_apply) as onlyMonth, YEAR(t.date_apply) as onlyYear';
//        $criteria->group    = 't.promotion_type, MONTH(t.date_apply), YEAR(t.date_apply)';
        $criteria->group    = 't.promotion_id, MONTH(t.date_apply), YEAR(t.date_apply)';
        $models = AppPromotionUser::model()->findAll($criteria);
        foreach($models as $item){
            if(empty($item->onlyMonth)){
                continue ;
            }
            $item->onlyMonth = $item->onlyMonth < 10 ? "0$item->onlyMonth" : $item->onlyMonth;
            $key = "$item->onlyYear-$item->onlyMonth-01";
            $aRes['KEY'][$key] = strtotime($key);
            if(!isset($aRes['PROMOTION_DONE'][$item->promotion_id][$key])){
                $aRes['PROMOTION_DONE'][$item->promotion_id][$key] = $item->id;
            }else{
                $aRes['PROMOTION_DONE'][$item->promotion_id][$key] += $item->id;
            }
            
            if(!isset($aRes['PROMOTION_SUM'][$item->promotion_id])){
                $aRes['PROMOTION_SUM'][$item->promotion_id] = $item->id;
            }else{
                $aRes['PROMOTION_SUM'][$item->promotion_id] += $item->id;
            }
        }
    }
    
    // 6. thống kê KH đặt theo từng ngày
    public function gas24hDaily(&$aRes) {
        $date_to    = date('Y-m-d');
        $date_from  = MyFormat::modifyDays($date_to, 30, '-');
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.source='.Sell::SOURCE_APP);
        $criteria->addCondition('t.status='.Sell::STATUS_PAID);
        $criteria->addCondition('t.materials_type_id=' . GasMaterialsType::MATERIAL_BINH_12KG);
        $criteria->addBetweenCondition('t.created_date_only', $date_from, $date_to); 
        $criteria->select   = 'count(t.id) as id, t.agent_id, t.created_date_only';
        $criteria->group    = 't.agent_id, t.created_date_only';
        $criteria->order    = 't.id';// Oct2218 ko thể bỏ đc 
        $models = SellDetail::model()->findAll($criteria);
        $aRes['DailySellDate'] = MyFormat::getArrayDay($date_from, $date_to);
        $aRes['DailySellSumAgent'] = [];
        foreach($models as $item){
            if(!isset($aRes['DailySellSumDay'][$item->created_date_only])){
                $aRes['DailySellSumDay'][$item->created_date_only] = $item->id;
            }else{
                $aRes['DailySellSumDay'][$item->created_date_only] += $item->id;
            }
            if(!isset($aRes['DailySellSumAgent'][$item->agent_id])){
                $aRes['DailySellSumAgent'][$item->agent_id] = $item->id;
            }else{
                $aRes['DailySellSumAgent'][$item->agent_id] += $item->id;
            }
        }
        
        $limit = 10; $temp = []; $i = 0;
        arsort($aRes['DailySellSumAgent']);
        foreach($aRes['DailySellSumAgent'] as $agent_id => $total){
            $temp[$agent_id] = $total; $i++ ;
            if($i > $limit){
                break;
            }
        }
        $aRes['DailySellSumAgent'] = $temp;
    }
    
    /** @Author: DungNT Mar 29, 2018
     *  @Todo: bc ước lượng số tiền sms
     **/
    public function reportSmsHgd($mUsers) {
        $criteria = new CDbCriteria();
        $this->reportSmsHgdGetCondition($criteria, $mUsers);
//        $criteria->select = 'count(DISTINCT t.user_id) as user_id, t.network';
        $criteria->select = 'user_id, t.network';
//        $criteria->group = 't.network';
        $criteria->group = 't.user_id';
        $models = UsersPhone::model()->findAll($criteria);
        $aRes = [];
        foreach($models as $item):
            if(!isset($aRes['OUTPUT'][$item->network])){
                $aRes['OUTPUT'][$item->network] = 1;
            }else{
                $aRes['OUTPUT'][$item->network] += 1;
            }
        endforeach;
        return $aRes;
    }
        
    /** @Author: DungNT Mar 30, 2018
     *  @Todo: gộp những điều kiện chung lại 1 hàm
     **/
    public function reportSmsHgdGetCondition(&$criteria, $mUsers) {
        if(!empty($mUsers->province_id)){
            $criteria->addCondition('t.province_id='.$mUsers->province_id);
        }
        if(!empty($mUsers->district_id)){
            $criteria->addCondition('t.district_id='.$mUsers->district_id);
        }
        $date_from  = MyFormat::dateDmyToYmdForAllIndexSearch($mUsers->date_from);
        $date_to    = MyFormat::dateDmyToYmdForAllIndexSearch($mUsers->date_to);
//        $criteria->addCondition("t.last_purchase<='$date_from' AND last_purchase IS NOT NULL");
        $criteria->addBetweenCondition("t.last_purchase", $date_from, $date_to);
        $criteria->addCondition('t.network<>'.GasScheduleSms::NETWORK_OTHER);
        $criteria->compare('t.role_id', ROLE_CUSTOMER);
        $aTypeCustomer = [STORE_CARD_HGD, STORE_CARD_HGD_CCS];
        $sParamsIn = implode(',',  $aTypeCustomer);
        $criteria->addCondition("t.is_maintain IN ($sParamsIn) AND t.is_customer_app=0");
    }
    
    /** @Author: Pham Thanh Nghia Oct 31, 2018
     *  @Todo: Báo cáo doanh thu bò mối
     **/
    public function getReportRevenue($mAppOrder){
        $aRes = $aAgentAmount = [];
        $aAgentRemain = []; // gas dư
        $aAgentRemainAmount = []; // gas dư
        $mInventoryCustomer = new InventoryCustomer();
        $criteria = new CDbCriteria;
        $criteria->compare('t.status', $mAppOrder->status);
        $criteria->addInCondition('t.type_customer', CmsFormatter::$aTypeIdBoMoi );
        if(!empty($mAppOrder->province_id_agent) && is_array($mAppOrder->province_id_agent)){
            $aAgent = $mInventoryCustomer->getAgentOfProvince($mAppOrder->province_id_agent);
            $criteria->addInCondition('t.agent_id',$aAgent);
        }
        if(!empty($mAppOrder->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($mAppOrder->date_from);
            $criteria->addCondition("DATE(t.date_delivery) >= '$date_from'");
        }
        if(!empty($mAppOrder->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($mAppOrder->date_to);
            $criteria->addCondition("DATE(t.date_delivery) <= '$date_to'");
        }
//        DateHelper::searchBetween($date_from, $date_to, 'date_delivery_bigint', $criteria, false);
        
        $criteria->select   = 'sum(t.qty) as qty, sum(t.amount) as amount, sum(t.kg_empty) as kg_empty, sum(t.kg_has_gas) as kg_has_gas, '
                . 'sum(t.pay_back) as pay_back, sum(t.total_gas) as total_gas, sum(t.total_gas_du) as total_gas_du,'
                . 't.agent_id, t.materials_type_id';
        $criteria->group    = 't.agent_id, t.materials_type_id';
        $aModelAppOrder = GasAppOrderCDetailReal::model()->findAll($criteria);
        foreach ($aModelAppOrder as $item) {
            $aRes[$item->agent_id][$item->materials_type_id]['qty'] = $item->qty;
            
            if(!isset($aAgentAmount[$item->agent_id])){
                $aAgentAmount[$item->agent_id] = $item->amount;
            }else{
                $aAgentAmount[$item->agent_id] += $item->amount;
            }
            // tính gas dư
            if(!isset($aAgentRemain[$item->agent_id])){
                $aAgentRemain[$item->agent_id] = $item->kg_has_gas - $item->kg_empty + $item->pay_back;  // line 35 /views/gasAppOrder/report/SoldOut.php
            }else{
                $aAgentRemain[$item->agent_id] += $item->kg_has_gas - $item->kg_empty + $item->pay_back;  // line 35 /views/gasAppOrder/report/SoldOut.php
            }
            if(!isset($aAgentRemainAmount[$item->agent_id])){
                $aAgentRemainAmount[$item->agent_id] = $item->total_gas_du;
            }else{
                $aAgentRemainAmount[$item->agent_id] += $item->total_gas_du;
            }
        }
        arsort($aAgentAmount); // sort associative arrays in descending order, according to the value
        return [
            'aRes'=> $aRes,
            'aAgentAmount' => $aAgentAmount,
            'aAgentRemain' => $aAgentRemain,
            'aAgentRemainAmount' => $aAgentRemainAmount,
        ];
    }
    /** @Author: Pham Thanh Nghia Nov 2, 2018
     *  @Todo: Báo cáo thue nhà
     **/
    public function getReportHomeContract($mHomeContract){
        $aRes = $aModel = $aSubDate = $aSumMonth = [];
        $criteria = new CDbCriteria;
        $criteria->compare('t.status', $mHomeContract->status);
        $criteria->compare('t.type_pay', $mHomeContract->type_pay);
        $criteria->compare('t.agent_id', $mHomeContract->agent_id);
        if(!empty($mHomeContract->province_id) && is_array($mHomeContract->province_id)){
            $criteria->addInCondition('t.province_id',$mHomeContract->province_id);
        }
        if(!empty($mHomeContract->statistic_year)){
//            $criteria->compare('YEAR(t.created_date)', $mHomeContract->statistic_year);
            $criteria->with         = ['rHomeContractDetail'];
            $criteria->together     = true;
            $criteria->compare('YEAR(rHomeContractDetail.date_pay)', $mHomeContract->statistic_year);
        }
        
        $aModelHome = HomeContract::model()->findAll($criteria);
        foreach ($aModelHome as $item) {
            $aMonth = $this->getDetailForReportHomeContract($item->id,$item->amount_month_pay,$mHomeContract->statistic_year);
            $aRes[$item->id] = $aMonth;
            $aModel[$item->id] = $item;
            $aSubDate[$item->id] = $item->end_contract - $item->start_contract; // sô chẳn
            $this->mergeArraySumValue($aSumMonth,$aMonth);
        }
        $result = [
            'aRes'=> $aRes,
            'aModel' => $aModel,
            'aSubDate' => $aSubDate,
            'aSumMonth' => $aSumMonth,
        ];
        Yii::app()->session['dataReportHomeContract']= $result;
        return $result;
    }
    /** @Author: Pham Thanh Nghia Nov 2, 2018
    *  @Todo: lấy ra danh sách tiền trả theo từng tháng
    *  @Param: điều kiện đổ lên view khi tiền trả cho kì hạn nhiều tháng thì chia nhỏ số tiền đó ra cho điều các tháng
     * $amount_month_pay : kì hạn thanh toán vd : 3 tháng , 6 tháng
    **/
    public function getDetailForReportHomeContract($home_contract_id,$amount_month_pay,$statistic_year){
        $criteria = new CDbCriteria;
        $aRes = [];
        $criteria->compare('t.home_contract_id', $home_contract_id);
        $criteria->select   = 'MONTH(date_pay) as date_pay, amount';
        $criteria->compare('YEAR(t.date_pay)', $statistic_year);
        $aModelHomeDetail = HomeContractDetail::model()->findAll($criteria);
        $date_pay = 0;
        $count = count($aModelHomeDetail);
        if($amount_month_pay > 1){ // trường hợp phải chia
            foreach ($aModelHomeDetail as $item) {
                $date_pay = $item->date_pay;
                $value = $item->amount/$amount_month_pay;
                // cho đều tổng số tiền cho các tháng
                for ($index = 0; $index < $amount_month_pay; $index++) { // từng tháng gắn tiền trung bình
                    $month = $date_pay+$index;
                    $aRes[$month] = $value;
                }
            }
        }else{ // làm như bình thường gắn tháng cho số tiền
            foreach ($aModelHomeDetail as $item) {
                $aRes[$item->date_pay] = $item->amount; // [tháng thanh toán] => giá trị
                $date_pay = $item->date_pay;
            }
        }
        return $aRes;
    }
    public function mergeArraySumValue(&$aRes,$aList){
        if(is_array($aList)){
            foreach ($aList as $key => $value) {
                if(!isset($aRes[$key])){
                    $aRes[$key] = $value;
                }else{
                    $aRes[$key] += $value;
                }
            }
        }
    }
    /** @Author: Pham Thanh Nghia Nov 16, 2018
     *  @Todo: get data Báo cáo quản trị khách hàng
     *  admin/customerSpecial/reportManage
     **/
    public function getReportManageCustomer($model){
        $criteria = new CDbCriteria;
        $mGasPrice = new GasPrice();
        $aCondition = $this->getArrayCustomerByInvest($model->money_invest,$model->money_invest_max); // get list array customer_id
        $criteria->addInCondition('t.customer_id',$aCondition);
        $date = MyFormat::dateDmyToYmdForAllIndexSearch($model->time);
        $strtotime = strtotime($date);
        $month = date("m",$strtotime);
        $year = date("Y",$strtotime);
        $criteria->compare('t.c_month',$month); //hỏi anh dũng
        $criteria->compare('t.c_year',$year);
        $price = GasPrice::getPriceOfCode($model->price_code, $month, $year);
        if( $price > 0){
            $criteria->addCondition("t.price <= '$price'"); 
        }
        if(!empty($model->payment_day)){
            $criteria->with         = ['rCustomer'];
            $criteria->together     = true;
            $criteria->addCondition('rCustomer.payment_day ='.$model->payment_day);
        }
        $criteria->select = "t.id, t.customer_id, t.sale_id,t.price ,t.price_code"; 
        $aUsersPrice = UsersPrice::model()->findAll($criteria);
        $aCustomer = CHtml::listData($aUsersPrice,'customer_id','customer_id'); 
        $aSale = CHtml::listData($aUsersPrice,'sale_id','sale_id'); 
        $aMember = array_merge($aCustomer,$aSale);
        $aUsers = array(); 
        if(!empty($aMember)){ // ERROR 500
            $needMore = [];
            if(!empty($model->payment_day)){
                $needMore['condition'] =  't.payment_day ='.$model->payment_day;
            }
            $aUsers    = Users::getArrObjectUserByRole('',$aMember,$needMore);
        }
        $aCustomerMonth = $this->getArrayOutputCustomer($aCustomer,$date); // SL bình quân  3 tháng
        
        $aOneMonth = $this->getArrayOneMonth($aCustomerMonth); // SL bình quân tháng gần nhất
        
        if(!empty($model->qty)){ // SL lớn hơn SL chọn sẽ bị xóa key 
            foreach ($aOneMonth as $key => $value) {
                if($value > $model->qty){
                    unset($aOneMonth[$key]);
                }
            }
        }
        
        $aResult = [
            'aUsersPrice' => $aUsersPrice, 
            'aUsers' => $aUsers,
            'aCustomerMonth' => $aCustomerMonth,
            'aOneMonth' => $aOneMonth,
        ];
        Yii::app()->session['dataManageCustomer'] = $aResult;
        return $aResult;
    }
    /** @Author: Pham Thanh Nghia Nov 16, 2018
     *  @Todo: lấy danh sách khách hàng có tiền đầu tư 
     * lớn hơn $money_invest
     * bé hơn $money_invest_max
     **/
    public function getArrayCustomerByInvest($money_invest =0, $money_invest_max =0){
        $aResult = [];
        $mGasSupportCustomer = new GasSupportCustomer();
        $aData = $mGasSupportCustomer->getCacheArraytCustomerInvert();
        if($money_invest !=0  && $money_invest_max!=0 ){
            foreach ($aData as $key => $value) {
                if($value >= $money_invest && $value <= $money_invest_max ){
                    $aResult[$key] = $key;
                }
            }
        }elseif ($money_invest !=0  && $money_invest_max ==0) {
            foreach ($aData as $key => $value) {
                if($value >= $money_invest ){
                    $aResult[$key] = $key;
                }
            }
        }elseif ($money_invest ==0  && $money_invest_max !=0) {
            foreach ($aData as $key => $value) {
                if($value <= $money_invest_max ){
                    $aResult[$key] = $key;
                }
            }
        }else{
            $aResult = array_keys($aData);
        }
        return $aResult;
    }
    /** @Author: Pham Thanh Nghia 2018
     *  @Todo: lấy sản lượng theo danh sách khách hàng và thời gian 
     *  @Param: $date : Y-m-d
     **/
    public function getArrayOutputCustomer($aCustomer, $date) { // edit from 252
        $date_from = $this->getFirstDayOfMonthBySubtract($date,3);  // lấy trước đó 3 tháng
        $date_to = $this->getEndOfMonthNearest($date);// lấy cuối tháng trước tháng hiện tại
        try{
            $aRes = $aSearch= array();
            $criteria = new CDBcriteria();
            DateHelper::searchBetween($date_from, $date_to, 'date_delivery_bigint', $criteria, false);
            $criteria->addInCondition("t.materials_type_id",  CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT);
            $criteria->addInCondition('t.customer_id', $aCustomer);
            $criteria->select = "year(t.date_delivery) as statistic_year, month(t.date_delivery) as statistic_month,t.customer_id, "
                    . " sum(qty) as qty , materials_type_id";
            $criteria->group = "year(t.date_delivery), month(t.date_delivery) , customer_id , materials_type_id";
            $mRes = GasStoreCardDetail::model()->findAll($criteria);               
            foreach ($mRes as $item) {
                $amount = $item->qty * CmsFormatter::$MATERIAL_VALUE_KG[$item->materials_type_id]; // qty * kg
                if(isset($aRes[$item->customer_id][$item->statistic_year][$item->statistic_month])){
                    $aRes[$item->customer_id][$item->statistic_year][$item->statistic_month] += $amount;
                }else{
                    $aRes[$item->customer_id][$item->statistic_year][$item->statistic_month] = $amount;
                    $aSearch[] = [
                        'customer_id'       => $item->customer_id,
                        'statistic_year'    => $item->statistic_year,
                        'statistic_month'   => $item->statistic_month,
                    ];
                }
            }
            $aRemain = $this->getArrayOutputCustomerGetGasRemain($date_from, $date_to, $aCustomer);
            
            foreach ($aSearch as $key => $value) {
                $customer_id = $value['customer_id'];
                $statistic_year = $value['statistic_year'];
                $statistic_month = $value['statistic_month'];
                if(isset($aRemain[$customer_id][$statistic_year][$statistic_month])){
                    $remain = $aRemain[$customer_id][$statistic_year][$statistic_month];
                    $amount = $aRes[$customer_id][$statistic_year][$statistic_month];
                    $aRes[$customer_id][$statistic_year][$statistic_month] = $amount - $remain;
                }
            }

           return $aRes;
       }catch (Exception $e)
       {
           MyFormat::catchAllException($e);
       }
    }
    /**
     * @Author: PHAM THANH NGHIA 2019
     * @Todo:   lấy sản lượng của tháng gần nhất trong 3 tháng của hàm getArrayOutputCustomer
     * admin/usersPrice/reportManage
     */
    public function getArrayOneMonth($aCustomerMonth){
        $aResult = [];
        foreach ($aCustomerMonth as $key => $aTime) {
            $year  = max(array_keys($aTime));
            $month =  max(array_keys($aTime[$year]));
            $aResult[$key] = $aTime[$year][$month];
        }
        return $aResult;
       
    }
     /** @Author: Pham Thanh Nghia Dec 2 ,2018
     *  @Todo: lấy danh sach gas dư theo danh sách khách hàng và thời gian 
     *  @Param: $date : Y-m-d
     **/
    public function getArrayOutputCustomerGetGasRemain( $date_from, $date_to, $aCustomer) {
        $aRes = array();
        $criteria = new CDBcriteria();
        $criteria->addInCondition('t.customer_id', $aCustomer);
        $criteria->addBetweenCondition("t.date_input",$date_from,$date_to);
        $criteria->addCondition("t.only_gas_remain=0");
        $criteria->select = "year(t.date_input) as statistic_year, month(t.date_input) as statistic_month,"
                . "sum(amount_gas) as amount_gas, customer_id";
        $criteria->group = "year(t.date_input), month(t.date_input) , customer_id "; 
        $mRes = GasRemain::model()->findAll($criteria);
        if(count($mRes)){
            foreach($mRes as $item){
                $aRes[$item->customer_id][$item->statistic_year][$item->statistic_month] = $item->amount_gas;
            }
        }
        return $aRes;
    }
    /** @Author: Pham Thanh Nghia Nov 23, 2018
     *  @Todo: trả về cuối tháng gần nhất 
     *  @Param: $date Y-m-d 
     *  Ex :  2018-10-15 => 2018-09-30
     *        2018-08-03 => 2018-07-31
     **/
    public function getEndOfMonthNearest($date){
        $date = date ( 'Y-m' , strtotime($date) ).'-1';
        $newdate = strtotime ( '-1 day' , strtotime ( $date ) ) ;
        $newdate = date('Y-m-d',$newdate);
        return $newdate;
    }
    /** @Author: Pham Thanh Nghia  Nov 23, 2018
     *  @Todo: trả về ngày 1 của tháng trừ đi số tháng trong ngày được chọn 
     *  @Param: $date Y-m-d , $subtract > 0
     *  Ex : 1998-08-14 => 1998-04-01 (subtract = 4)
     **/
    public function getFirstDayOfMonthBySubtract($date,$subtract = 1){
        if($subtract > 0){
            $newdate = strtotime ( '-'.$subtract.' months' , strtotime ( $date ) ) ;
            $newdate = date('Y-m',$newdate).'-01';
            return $newdate;
        }
        return $date;
    }
    public function convertOutputCustomerForView($aDetail = array(), $format ='<br>'){
        $result ='';
        if(!empty($aDetail)){
            foreach ($aDetail as $year => $aMonth) {
                foreach ($aMonth as $month => $value) {
                    $result .= 'T'.$month.' : '.ActiveRecord::formatCurrencyRound($value).$format; 
                }
            }
        }
        return $result;
    }
    // tình tổng sản lượng từ danh sách con của getArrayOutputCustomer
    public function sumOutputCustomerForView($aDetail = array()){
        $result =0;
        if(!empty($aDetail)){
            foreach ($aDetail as $year => $aMonth) {
                foreach ($aMonth as $month => $value) {
                    $result += $value;
                }
            }
        }
        return $result;
    }
    /**
     * @Author: Pham Thanh Nghia Dec 14, 2018
     * @Todo: tính tồn kho list Agent theo Province cho all vật tư. BC này xem đc khoảng ngày
     * sử dụng xuất excel all vật tư tồn kho từng đại lý 
     * @Using: sử dụng cho gasstorecard/view_store_movement_summary/excel/1
     * $model = GasStoreCard
     */
    public function getInventoryByProvince($model, $aAgent ){
        $aRes = []; 
        $mAppCache = new AppCache();
        $this->aMaterials = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
        // 1. Lấy tồn đầu kỳ cho mỗi vật tư nhập ban đầu bởi đại lý
//        $OPENING_BALANCE    = MyFunctionCustom::getAllAgentOpeningBalanceByProvince($aAgent,''); // nhợ hơn 
        $OPENING_BALANCE    = MyFunctionCustom::getAllAgentOpeningBalance(''); // A
        $aTempOpening       = $OPENING_BALANCE;// xử lý lọc lấy những vật tư ko có nhập xuất trong 1 năm
        // 2. tính tổng nhập xuất đến thời điểm hiện tại
        $mRpInventory                   = new RpInventory();
        $mRpInventory->notGetWarehouse  = $model->update_note;// lấy tạm biến update_note làm cờ check kho hay không
        $date_to = MyFormat::modifyDays($model->date_from_ymd, 1, '-');// trừ 1 ngày
        $IMPORT_EXPORT = $mRpInventory->calcAgentExportImport($date_to); // B
        // nhập xuất trong khoản thời gian
        $aImportExport = $this->getImportExport($aAgent, $model->date_from_ymd, $model->date_to_ymd,$model);
        
        foreach($IMPORT_EXPORT as $agent_id => $aInfo1){
            if(empty($agent_id)){
                continue;
            }
            foreach($aInfo1 as $materials_type_id => $aInfo2){
                foreach($aInfo2 as $materials_id => $aInfo3){
                    unset($aTempOpening[$agent_id][$materials_id]);
                    $this->getInventoryFormatData($aRes, $OPENING_BALANCE, $IMPORT_EXPORT, $agent_id, $materials_type_id, $materials_id);
                }
            }
        }
        
        // xử lý những vật tư ko có nhập xuất trong năm 
        foreach($aTempOpening as $agent_id => $aInfo):
            foreach($aInfo as $materials_id => $qty):
                if(!isset($this->aMaterials[$materials_id])){
                    continue ;
                }
                $mMaterial          = $this->aMaterials[$materials_id];
                $materials_type_id  = $mMaterial['materials_type_id'];
                $this->getInventoryFormatData($aRes, $OPENING_BALANCE, $IMPORT_EXPORT, $agent_id, $materials_type_id, $materials_id);
            endforeach;
        endforeach;
        // 
        $aImport = $aImportExport['aImport'];
        $aExport = $aImportExport['aExport'];
        foreach($IMPORT_EXPORT as $agent_id => $aInfo1){
            if(empty($agent_id)){
                continue;
            }
            foreach($aInfo1 as $materials_type_id => $aInfo2){
                foreach($aInfo2 as $materials_id => $aInfo3){
                    $this->getInventoryFormatArrayImportExport($aRes, $aImport, $aExport, $agent_id, $materials_type_id, $materials_id);
                }
            }
        } // 
        
        return $aRes;
    }
     /** 
      * @Author: Pham Thanh Nghia Dec 21, 2018
     * @Todo:  import  trừ export 
     */         
     public function getImportExport($agent_id, $date_from, $date_to,$model){
         $aRes = [];
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',', $agent_id);
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.user_id_create IN ($sParamsIn)");
        }
        if($model->update_note == 1){
            $sParamsInAgent = implode(',', GasCheck::getAgentNotGentAuto());
            $criteria->addCondition("t.user_id_create NOT IN ($sParamsInAgent)");
        }
        
        DateHelper::searchBetween($date_from, $date_to, 'date_delivery_bigint', $criteria, false);
        $criteria->select = "t.type_store_card, t.user_id_create, t.materials_id,t.materials_type_id, sum(qty) as qty";
        $criteria->group = "t.type_store_card, t.user_id_create, t.materials_id, t.materials_type_id";
        $models = GasStoreCardDetail::model()->findAll($criteria);
        $aImport = $aExport = [];
        foreach($models as $item){
            if($item->type_store_card  ==  TYPE_STORE_CARD_IMPORT){
                if(!isset($aImport[$item->user_id_create][$item->materials_type_id][$item->materials_id])){
                    $aImport[$item->user_id_create][$item->materials_type_id][$item->materials_id] = $item->qty;
                }else{
                    $aImport[$item->user_id_create][$item->materials_type_id][$item->materials_id] += $item->qty;
                }
            }
            if($item->type_store_card  ==  TYPE_STORE_CARD_EXPORT){
                if(!isset($aExport[$item->user_id_create][$item->materials_type_id][$item->materials_id])){
                    $aExport[$item->user_id_create][$item->materials_type_id][$item->materials_id] = $item->qty;
                }else{
                    $aExport[$item->user_id_create][$item->materials_type_id][$item->materials_id] += $item->qty;
                }
            }
        }
        $aRes['aImport'] = $aImport;
        $aRes['aExport'] = $aExport;
        return $aRes;
     }
     // với mỗi nhập kho vs xuất kho mình từ thêm nhập kho vs xuất theo date_form , date_to
    public function getInventoryFormatArrayImportExport(&$aRes, $aImport, $aExport, $agent_id, $materials_type_id, $materials_id) {
        $temp = $aRes[$agent_id][$materials_type_id][$materials_id];
        $qtyImport  = isset($aImport[$agent_id][$materials_type_id][$materials_id]) ? $aImport[$agent_id][$materials_type_id][$materials_id] : 0;
        $qtyExport  = isset($aExport[$agent_id][$materials_type_id][$materials_id]) ? $aExport[$agent_id][$materials_type_id][$materials_id] : 0;
        $inventory  =   $qtyImport - $qtyExport;
        $temp['open']       = $temp['end'];
        $temp['import']     = $qtyImport;
        $temp['export']     = $qtyExport;
        $temp['end']        += $inventory;
        $aRes[$agent_id][$materials_type_id][$materials_id] = $temp;
    }
    
    //KHANH TOAN Dec 12 2018
    // báo cáo  sản lượng khách hàng admin/gasreports/Output_customer
    // Ý nghĩa là thống kê để biết tần suất lấy hàng và số lượng của KH này lấy có đúng như trong hợp đồng ko
    public function outputCustomerGenerality($model){
    try{
        /* 1. khởi tạo session model KH bò mối id=>model - đã làm khi load page
         * 2. lấy sản lượng thực tế foreach các $MATERIAL_TYPE_BINHBO_OUTPUT, chắc là ko lấy vỏ bình
         * 3. lấy gas dư
         */        
        $aRes = array();
        if(!MyFormat::validDateInput($model->date_from, '-') || !MyFormat::validDateInput($model->date_to, '-') )
            return $aRes;
        $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
        $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);
        $needMore['groupDay'] = 1; //group them date_delivery
        $this->getQuantityCustomer($model, $aRes, $date_from, $date_to, $needMore);
        $aRes['ARR_DAYS'] = MyFormat::getArrayDay($date_from, $date_to);
        return $aRes;
    }catch (Exception $e){
        MyFormat::catchAllException($e);
    }
    }
    
    public static function Output_customer_process_result_generality(&$aRes, $mRes, $model, $needMore=[]){
        if(isset($needMore['groupDay'])){
            foreach($mRes as $item){
                $aRes['customer_id'][$item->customer_id][$item->user_id_create] = $item->user_id_create; // lưu id customer duy nhất để foreach
                $aRes['customer_id_only'][$item->customer_id] = $item->customer_id;

                $aRes['data'][$item->customer_id][$item->user_id_create] = $item;
    //            $aRes['data'][$item->customer_id] = $item; // dùng để lấy thông tin của user_id_create, t.date_delivery, t.sale_id, CÓ LẼ KHÔNG CẦN LẤY NGÀY
                if($model->ext_is_maintain==STORE_CARD_KH_MOI && $item->materials_type_id==MATERIAL_TYPE_BINH_12){
                    // Nếu là thống kê KH mối: bình bò hiển thị Kg (chỗ else bên dưới) và bình 12 thì hiển thị bình
                    $aRes['OUTPUT_MOI_12'][$item->customer_id][$item->date_delivery] = $item->qty;
                }else{
                    if(isset($aRes['OUTPUT'][$item->customer_id][$item->date_delivery]))
                        $aRes['OUTPUT'][$item->customer_id][$item->date_delivery] += $item->qty*CmsFormatter::$MATERIAL_VALUE_KG[$item->materials_type_id];
                    else
                        $aRes['OUTPUT'][$item->customer_id][$item->date_delivery] = $item->qty*CmsFormatter::$MATERIAL_VALUE_KG[$item->materials_type_id];
                }
            }
        }else{
            foreach($mRes as $item){
            $aRes['customer_id'][$item->customer_id][$item->user_id_create] = $item->user_id_create; // lưu id customer duy nhất để foreach
            $aRes['customer_id_only'][$item->customer_id] = $item->customer_id;
            if($model->ext_is_maintain==STORE_CARD_KH_MOI && $item->materials_type_id==MATERIAL_TYPE_BINH_12){
                // Nếu là thống kê KH mối: bình bò hiển thị Kg (chỗ else bên dưới) và bình 12 thì hiển thị bình
                $aRes['OUTPUT_MOI_12'][$item->customer_id] = $item->qty;
            }else{
                if(isset($aRes['OUTPUT'][$item->customer_id]))
                    $aRes['OUTPUT'][$item->customer_id] += $item->qty*CmsFormatter::$MATERIAL_VALUE_KG[$item->materials_type_id];
                else
                    $aRes['OUTPUT'][$item->customer_id] = $item->qty*CmsFormatter::$MATERIAL_VALUE_KG[$item->materials_type_id];
            }
        }
        }
    }
    
    public static function getGasRemainGenerality($model, &$aRes, $date_from, $date_to){
        $criteria = new CDBcriteria();
        $criteria->addBetweenCondition("t.date_input",$date_from,$date_to);
        $criteria->addCondition("t.only_gas_remain=0");
        $criteria->select = "t.date_input,t.customer_id,"
                . "sum(amount_gas) as amount_gas, sum(amount) as amount";
        $criteria->group = "t.customer_id, t.date_input"; 
        $mRes = GasRemain::model()->findAll($criteria);
        if(count($mRes)){
            foreach($mRes as $item){
                // item ref: MAR182015 tạm close ý tưởng xử lý kiểu này
//                $aRes['session_customer_id'][$item->customer_id] = $item->customer_id;
                $aRes['GAS_REMAIN'][$item->customer_id][$item->date_input] = $item->amount_gas;
                if(isset($aRes['OUTPUT']['SUMGASREMAIN']))
                    $aRes['OUTPUT']['SUMGASREMAIN'] += $item->amount_gas;
                else
                    $aRes['OUTPUT']['SUMGASREMAIN'] = $item->amount_gas;
            }
        }
    }
    
    /** @Author: KHANH TOAN 2018
     *  @Todo: lấy sẳn lượng  theo custeomr_id
     *  @Param:
     **/
    public function getQuantityCustomer($model, &$aRes, $date_from, $date_to, $needMore=[]) {
        $aRes['customer_id'] = array();
        $groupMore = '';
        if(isset($needMore['groupDay'])){
            $groupMore = "t.date_delivery ,";
        }
        $criteria = new CDBcriteria();
        if(isset($needMore['aCustomerId'])){
            $sParamsIn = implode(',', $needMore['aCustomerId']);
            if(!empty($sParamsIn)){
                $criteria->addCondition("t.customer_id IN ($sParamsIn)");
            }
        }
        self::outputCustomerAddCriteria($model, $criteria);
        DateHelper::searchBetween($date_from, $date_to, 'date_delivery_bigint', $criteria, false);
        $criteria->addInCondition("t.materials_type_id",CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT);
        $criteria->select = $groupMore."t.user_id_create, t.customer_id, t.sale_id, t.type_customer,t.materials_type_id, "
                . " sum(qty) as qty ";
        $criteria->group = $groupMore."t.customer_id, t.materials_type_id, t.user_id_create"; 
        $mRes = GasStoreCardDetail::model()->findAll($criteria);
        self::Output_customer_process_result_generality($aRes, $mRes, $model, $needMore);
            //**** begin gas dư
        // lấy gas dư
        self::getGasRemain($model, $aRes, $date_from, $date_to, $needMore);
    }
}
