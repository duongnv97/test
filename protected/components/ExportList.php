<?php
class ExportList
{
    const STORE_CLIENT      = 1; // Lưu vào máy client (hiện popup download)
    const STORE_SERVER      = 2; // Lưu trên server
    public $output          = 1;
     public static $replace = array("<br>","<BR>");
     public static $replaceNewLine = "\r\n";
//     public static $replaceNewLine = "\n";
     public static $remove = array( "&nbsp;", "<p>", "</p>", "<div>", "</div>","<b>","</b>");
      // Nguyen Dung 10-29-2013
      public static function Export_list_maintain(){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                        ->setLastModifiedBy("NguyenDung")
                                        ->setTitle('Danh Sách Bảo Trì')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("List Maintain")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1;
        $i=1;
        $dataAll = $_SESSION['data-excel']->data;
//        $cmsFormatter = new CmsFormatter();	

        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);            
        $objPHPExcel->getActiveSheet()->setTitle('DS Bảo Trì'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Danh Sách Bảo Trì');
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);    			
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $row++;
        $index=1;
        $beginBorder = $row;
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'SN');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại Lý');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày Bảo Trì');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã Khách Hàng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Khách Hàng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Địa Chỉ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Điện Thoại');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Nhân Viên Bảo Trì');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Thương Hiệu Gas');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Số Seri');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đang Dùng Gas Hướng Minh');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ghi Chú');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ghi Chú Sau Cuộc Gọi');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Trạng Thái');
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;

        foreach($dataAll as $data):
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $i);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->agent?$data->agent->first_name:'');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $cmsFormatter->formatDate($data->maintain_date));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->customer?$data->customer->code_bussiness:'');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->customer?$cmsFormatter->formatNameUser($data->customer):'');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->customer?$data->customer->address:'');
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row",$data->customer?$data->customer->phone:'', PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->maintain_employee?$data->maintain_employee->first_name:'');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->materials?$data->materials->name:'');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->seri_no);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", CmsFormatter::$yesNoFormat[$data->using_gas_huongminh]);
            $note = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $data->note);
            $note = str_replace(ExportList::$remove, " ", $note);            
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $note);

            $note = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $data->note_update_status);
            $note = str_replace(ExportList::$remove, " ", $note);                            
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $note);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", CmsFormatter::$STATUS_MAINTAIN[$data->status]);
            $row++;
            $i++;
        endforeach;	// end body

        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(18);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);

        $row--;		
        $index--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);		
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);		

        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.'Danh Sách Bảo Trì'.'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }     
      }
	  
      // Nguyen Dung 10-29-2013
      public static function Export_list_maintain_sell(){
          try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                        ->setLastModifiedBy("NguyenDung")
                                        ->setTitle('Danh Sách Bán Hàng Bảo Trì')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("List Maintain Sell")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1;
        $i=1;
        $dataAll = $_SESSION['data-excel']->data;
        $cmsFormatter = new CmsFormatter();	

        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);            

        $objPHPExcel->getActiveSheet()->setTitle('Bán Hàng Bảo Trì'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Danh Sách Bán Hàng Bảo Trì');
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);    			
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $row++;
        $index=1;
        $beginBorder = $row;
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'SN');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại Lý');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày Bán');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'NV PTTT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã Khách Hàng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Khách Hàng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Địa Chỉ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Điện Thoại');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'PTTT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Thương Hiệu Gas Bán');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'SL Vỏ Thu Về');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Thương Hiệu Gas Vỏ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Seri Thu Về');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Trùng Seri Bảo Trì');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ghi Chú Sau Cuộc Gọi');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Trạng Thái');
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        foreach($dataAll as $data):	
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $i);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->agent?$data->agent->first_name:'');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $cmsFormatter->formatDate($data->date_sell));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->maintain_employee?$data->maintain_employee->first_name:'');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->customer?$data->customer->code_bussiness:'');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->customer?$cmsFormatter->formatNameUser($data->customer):'');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->customer?$data->customer->address:'');
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row",$data->customer?$data->customer->phone:'', PHPExcel_Cell_DataType::TYPE_STRING);
            $MaintainHistory=$cmsFormatter->formatMaintainHistory($data);
            $MaintainHistory = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $MaintainHistory);
            $MaintainHistory = str_replace(ExportList::$remove, " ", $MaintainHistory);                            
            $MaintainHistory = strip_tags($MaintainHistory);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $MaintainHistory);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->materials_sell?$data->materials_sell->name:'');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->quantity_vo_back);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->materials_back?$data->materials_back->name:'');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->seri_back);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", CmsFormatter::$STATUS_SAME_SERI[$data->is_same_seri_maintain]);

            $note = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $data->note_update_status);
            $note = str_replace(ExportList::$remove, " ", $note);            
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $note);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", CmsFormatter::$STATUS_MAINTAIN[$data->status]);
//                $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
            $row++;
            $i++;
        endforeach;	
        $row--;		
        $index--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);		
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);		
        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:".MyFunctionCustom::columnName($index).$row)
            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(10);


        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.'Bán Hàng Bảo Trì'.'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }     
      }
	  
      // 10-31-2013 ANH DUNG
      public static function Export_list_for_monitoring_maintain(){
          try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                        ->setLastModifiedBy("NguyenDung")
                                        ->setTitle('Bảo Trì Xuống Lại')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("List Maintain")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1;
        $i=1;
        $dataAll = $_SESSION['data-excel']->data;
        $cmsFormatter = new CmsFormatter();	

        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);            
        $objPHPExcel->getActiveSheet()->setTitle('Bảo Trì Xuống Lại'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Danh Sách Bảo Trì Cần Xuống Lại');
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);    			
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $row++;
        $index=1;
        $beginBorder = $row;
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'SN');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại Lý');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày Bảo Trì');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã Khách Hàng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Khách Hàng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Địa Chỉ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Điện Thoại');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ghi Chú');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ghi Chú Sau Cuộc Gọi');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Trạng Thái');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Kết Quả Xử Lý');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ghi Chú Sau Xử Lý');
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        foreach($dataAll as $data):	
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $i);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->agent?$data->agent->first_name:'');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $cmsFormatter->formatDate($data->maintain_date));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->customer?$data->customer->code_bussiness:'');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->customer?$cmsFormatter->formatNameUser($data->customer):'');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->customer?$data->customer->address:'');                
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row",$data->customer?$data->customer->phone:'', PHPExcel_Cell_DataType::TYPE_STRING);
            $note = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $data->note);
            $note = str_replace(ExportList::$remove, " ", $note);            
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $note);
            $note = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $data->note_update_status);
            $note = str_replace(ExportList::$remove, " ", $note);                            
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $note);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", CmsFormatter::$STATUS_MAINTAIN[$data->status]);
            // Kết Quả Xử Lý
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", CmsFormatter::$STATUS_MAINTAIN_BACK[$data->status_maintain_back]);
            $note = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $data->note_maintain_back);
            $note = str_replace(ExportList::$remove, " ", $note);                            
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $note);
            $row++;
            $i++;
        endforeach;	
        $row--;		
        $index--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);		
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);		
        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:L".$row)
            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);

        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.'Bảo Trì Cần Xuống Lại'.'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }     
      }	        
      
      // 10-31-2013 ANH DUNG
      public static function Export_supervision_list_maintain_call_bad(){
          try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                        ->setLastModifiedBy("NguyenDung")
                                        ->setTitle('Bảo Trì Gọi Xấu')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("List Maintain")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1;
        $i=1;
        $dataAll = $_SESSION['data-excel']->data;
        $cmsFormatter = new CmsFormatter();	

        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);            
        $objPHPExcel->getActiveSheet()->setTitle('Bảo Trì Gọi Xấu'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Danh Sách Bảo Trì Có Cuộc Gọi Xấu');
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);    			
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $row++;
        $index=1;
        $beginBorder = $row;
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'SN');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại Lý');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày Bảo Trì');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã Khách Hàng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Khách Hàng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Địa Chỉ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Điện Thoại');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ghi Chú');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ghi Chú Sau Cuộc Gọi');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Trạng Thái');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Kết Quả Xử Lý');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ghi Chú Sau Xử Lý');
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        foreach($dataAll as $data):	
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $i);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->agent?$data->agent->first_name:'');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $cmsFormatter->formatDate($data->maintain_date));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->customer?$data->customer->code_bussiness:'');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->customer?$cmsFormatter->formatNameUser($data->customer):'');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->customer?$data->customer->address:'');                
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row",$data->customer?$data->customer->phone:'', PHPExcel_Cell_DataType::TYPE_STRING);
            $note = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $data->note);
            $note = str_replace(ExportList::$remove, " ", $note);            
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $note);
            $note = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $data->note_update_status);
            $note = str_replace(ExportList::$remove, " ", $note);                            
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $note);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", CmsFormatter::$STATUS_MAINTAIN[$data->status]);
            // Kết Quả Xử Lý bảo trì có cuộc gọi xấu
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", CmsFormatter::$STATUS_MAINTAIN_BACK[$data->status_call_bad]);
            $note = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $data->note_call_bad);
            $note = str_replace(ExportList::$remove, " ", $note);                            
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $note);
            $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
            $row++;
            $i++;
        endforeach;	
        $row--;		
        $index--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);		
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);		
        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:L".$row)
            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);

        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.'Bảo Trì Gọi Xấu'.'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }     
      }
	  
      // 10-31-2013 ANH DUNG
      public static function Export_supervision_list_maintain_sell_call_bad(){
          try{
            Yii::import('application.extensions.vendors.PHPExcel',true);
            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                            ->setLastModifiedBy("NguyenDung")
                                            ->setTitle('Bán Hàng Gọi Xấu')
                                            ->setSubject("Office 2007 XLSX Document")
                                            ->setDescription("List Maintain")
                                            ->setKeywords("office 2007 openxml php")
                                            ->setCategory("Gas");
            $row=1;
            $i=1;
            $dataAll = $_SESSION['data-excel']->data;
            $cmsFormatter = new CmsFormatter();	
            
            // 1.sheet 1 Đại Lý
            $objPHPExcel->setActiveSheetIndex(0);		
            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);            
            $objPHPExcel->getActiveSheet()->setTitle('Bán Hàng Gọi Xấu'); 
            $objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Danh Sách Bán Hàng Có Cuộc Gọi Xấu');
            $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                                ->setBold(true);    			
            $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
            $row++;
            $index=1;
            $beginBorder = $row;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'SN');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại Lý');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày Bán');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã Khách Hàng');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Khách Hàng');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Địa Chỉ');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Điện Thoại');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Bảo Trì');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Trạng Thái');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ghi Chú Sau Cuộc Gọi');            
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Kết Quả Xử Lý');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ghi Chú Sau Xử Lý');
            $index--;
            
            $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                                ->setBold(true);    	
            $row++;
            foreach($dataAll as $data):	
                $index=1;
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $i);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->agent?$data->agent->first_name:'');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $cmsFormatter->formatDate($data->date_sell));
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->customer?$data->customer->code_bussiness:'');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->customer?$cmsFormatter->formatNameUser($data->customer):'');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->customer?$data->customer->address:'');                
                $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row",$data->customer?$data->customer->phone:'', PHPExcel_Cell_DataType::TYPE_STRING);
                $MaintainHistory=$cmsFormatter->formatMaintainHistory($data);
                $MaintainHistory = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $MaintainHistory);
                $MaintainHistory = str_replace(ExportList::$remove, " ", $MaintainHistory);                            
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $MaintainHistory);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", CmsFormatter::$STATUS_MAINTAIN[$data->status]);
                $note = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $data->note_update_status);
                $note = str_replace(ExportList::$remove, " ", $note);                            
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $note);
                
                // Kết Quả Xử Lý bảo trì có cuộc gọi xấu
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", CmsFormatter::$STATUS_MAINTAIN_BACK[$data->status_call_bad]);
                $note = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $data->note_call_bad);
                $note = str_replace(ExportList::$remove, " ", $note);                            
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $note);
                $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
                $row++;
                $i++;
            endforeach;	
            $row--;		
            $index--;
            $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);		
            $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                            ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);		
            $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:L".$row)
                ->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);

            //save file
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            for($level=ob_get_level();$level>0;--$level)
            {
                    @ob_end_clean();
            }
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="'.'Bán Hàng Gọi Xấu'.'.'.'xlsx'.'"');

            header('Cache-Control: max-age=0');				
            $objWriter->save('php://output');			
            Yii::app()->end();
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }                 
      }      
      
      // 10-31-2013 ANH DUNG
      public static function Export_supervision_list_maintain_sell_seri_diff(){
          try{
            Yii::import('application.extensions.vendors.PHPExcel',true);
            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                            ->setLastModifiedBy("NguyenDung")
                                            ->setTitle('Seri Không Trùng')
                                            ->setSubject("Office 2007 XLSX Document")
                                            ->setDescription("List Maintain Sell")
                                            ->setKeywords("office 2007 openxml php")
                                            ->setCategory("Gas");
            $row=1;
            $i=1;
            $dataAll = $_SESSION['data-excel']->data;
            $cmsFormatter = new CmsFormatter();	
            
            // 1.sheet 1 Đại Lý
            $objPHPExcel->setActiveSheetIndex(0);		
            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);            
            $objPHPExcel->getActiveSheet()->setTitle('Seri Không Trùng'); 
            $objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Danh Sách Bán Hàng Có Seri Không Trùng');
            $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                                ->setBold(true);    			
            $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
            $row++;
            $index=1;
            $beginBorder = $row;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'SN');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại Lý');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày Bán');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã Khách Hàng');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Khách Hàng');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Địa Chỉ');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Điện Thoại');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Bảo Trì');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Seri Thu Về');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Trạng Thái');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ghi Chú Sau Cuộc Gọi');            
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Kết Quả Xử Lý');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ghi Chú Sau Xử Lý');
            $index--;
            
            $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                                ->setBold(true);    	
            $row++;
            foreach($dataAll as $data):	
                $index=1;
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $i);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->agent?$data->agent->first_name:'');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $cmsFormatter->formatDate($data->date_sell));
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->customer?$data->customer->code_bussiness:'');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->customer?$cmsFormatter->formatNameUser($data->customer):'');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->customer?$data->customer->address:'');                
                $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row",$data->customer?$data->customer->phone:'', PHPExcel_Cell_DataType::TYPE_STRING);
                $MaintainHistory=$cmsFormatter->formatMaintainHistory($data);
                $MaintainHistory = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $MaintainHistory);
                $MaintainHistory = str_replace(ExportList::$remove, " ", $MaintainHistory);                            
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $MaintainHistory);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->seri_back);                
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", CmsFormatter::$STATUS_MAINTAIN[$data->status]);
                $note = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $data->note_update_status);
                $note = str_replace(ExportList::$remove, " ", $note);                            
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $note);
                
                // Kết Quả Xử Lý bảo trì có cuộc gọi xấu
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", CmsFormatter::$STATUS_MAINTAIN_BACK[$data->status_call_bad]);
                $note = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $data->note_call_bad);
                $note = str_replace(ExportList::$remove, " ", $note);                            
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $note);
                $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
                $row++;
                $i++;
            endforeach;	
            $row--;		
            $index--;
            $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);		
            $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                            ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);		
            $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:M".$row)
                ->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);

            //save file
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            for($level=ob_get_level();$level>0;--$level)
            {
                    @ob_end_clean();
            }
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="'.'Seri Không Trùng'.'.'.'xlsx'.'"');

            header('Cache-Control: max-age=0');				
            $objWriter->save('php://output');			
            Yii::app()->end();                     		          
            }catch (Exception $e)
            {
                MyFormat::catchAllException($e);
            }     
      }      
      
      // 10-31-2013 ANH DUNG
      public static function Export_list_customer_maintain(){
          try{
            Yii::import('application.extensions.vendors.PHPExcel',true);
            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                            ->setLastModifiedBy("NguyenDung")
                                            ->setTitle('Khách Hàng Bảo Trì')
                                            ->setSubject("Office 2007 XLSX Document")
                                            ->setDescription("List Customer Maintain")
                                            ->setKeywords("office 2007 openxml php")
                                            ->setCategory("Gas");
            $row=1;
            $i=1;
            $dataAll = $_SESSION['data-excel']->data;
            $cmsFormatter = new CmsFormatter();	
            
            // 1.sheet 1 Đại Lý
            $objPHPExcel->setActiveSheetIndex(0);		
            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
            $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);            
            $objPHPExcel->getActiveSheet()->setTitle('Khách Hàng Bảo Trì'); 
            $objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Danh Sách Khách Hàng Bảo Trì');
            $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                                ->setBold(true);    			
            $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
            $row++;
            $index=1;
            $beginBorder = $row;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'SN');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã Hệ Thống');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã Khách Hàng');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tên Khách Hàng');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Địa Chỉ');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Điện Thoại');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Trạng Thái Bảo Trì');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày Tạo');
            $index--;
            
            $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                                ->setBold(true);    	
            $row++;
            foreach($dataAll as $data):	
                $index=1;
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $i);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->code_account);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->code_bussiness);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->first_name);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->address);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row",$data->phone, PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", CmsFormatter::$STATUS_IS_MAINTAIN[$data->is_maintain]);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $cmsFormatter->formatDatetime($data->created_date));
                $row++;
                $i++;
            endforeach;	
            $row--;		
            $index--;
            $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);		
            $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                            ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);		
            $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:H".$row)
                ->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);

            //save file
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            for($level=ob_get_level();$level>0;--$level)
            {
                    @ob_end_clean();
            }
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="'.'Khách Hàng Bảo Trì'.'.'.'xlsx'.'"');

            header('Cache-Control: max-age=0');				
            $objWriter->save('php://output');			
            Yii::app()->end();                     		          
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }                 
      }
	        
      
      public static function Export_list_employees(){
          
      }
      
      
 /** @Author: ANH DUNG Apr 04, 2014 */
    public static function DetermineGasGoBack_export_excel(){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                        ->setLastModifiedBy("NguyenDung")
                                        ->setTitle('Xác định bình quay về của PTTT')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("gas go back pttt")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1;
        $i=1;

          $TOTAL_ALL = isset($_SESSION['data-excel-gas-go-back']['TOTAL_ALL'])?$_SESSION['data-excel-gas-go-back']['TOTAL_ALL']:array();
          $TOTAL_USING_GAS_HM = isset($_SESSION['data-excel-gas-go-back']['TOTAL_USING_GAS_HM'])?$_SESSION['data-excel-gas-go-back']['TOTAL_USING_GAS_HM']:array();
          $TOTAL_QTY_BIGGER_2 = isset($_SESSION['data-excel-gas-go-back']['TOTAL_QTY_BIGGER_2'])?$_SESSION['data-excel-gas-go-back']['TOTAL_QTY_BIGGER_2']:array();
          $TOTAL_QTY_BIGGER_2_COUNT_CUSTOMER = isset($_SESSION['data-excel-gas-go-back']['TOTAL_QTY_BIGGER_2_COUNT_CUSTOMER'])?$_SESSION['data-excel-gas-go-back']['TOTAL_QTY_BIGGER_2_COUNT_CUSTOMER']:array();
          $ARR_CUSTOMER_ID = isset($_SESSION['data-excel-gas-go-back']['ARR_CUSTOMER_ID'])?$_SESSION['data-excel-gas-go-back']['ARR_CUSTOMER_ID']:array();

          $MONITOR_EMPLOYEE = $_SESSION['data-excel-gas-go-back']['MONITOR_EMPLOYEE'];
          $AGENT_MODEL = $_SESSION['data-excel-gas-go-back']['AGENT_MODEL'];
          $MODEL_CUSTOMER = $_SESSION['data-excel-gas-go-back']['MODEL_CUSTOMER'];

        $cmsFormatter = new CmsFormatter();	

        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);            
        $model = $_SESSION['data-excel-gas-go-back']['MODEL'];
        $textHead = "Xác định bình quay về của PTTT Từ Ngày $model->date_from đến ngày $model->date_to";
        $objPHPExcel->getActiveSheet()->setTitle('Bình quay về của PTTT'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", $textHead);
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);    			
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $row++;
        $index=1;
        $beginBorder = $row;
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'NV PTTT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại Lý');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng BQV');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Dùng Gas HM');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Số Bình Mua >= 2');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Số KH MUA >= 2');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Số Còn Lại');
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    
          $sum_total_all = 0;
          $sum_using_gas_hm = 0;
          $sum_qty_bigger_2 = 0;
          $sum_qty_bigger_2_count_customer = 0;
          $sum_remain = 0;          

        $row++;
          foreach($TOTAL_ALL as $maintain_employee_id=>$item_agent):
              foreach($item_agent as $agent_id=>$item_total_all):
                  $index=1;
                  $name_employer = isset($MONITOR_EMPLOYEE[$maintain_employee_id])?$MONITOR_EMPLOYEE[$maintain_employee_id]->first_name:'';
                  $name_agent = isset($AGENT_MODEL[$agent_id])?$AGENT_MODEL[$agent_id]->first_name:'';
                  $total_using_gas_hm = isset($TOTAL_USING_GAS_HM[$maintain_employee_id][$agent_id])?$TOTAL_USING_GAS_HM[$maintain_employee_id][$agent_id]:0;
                  $qty_bigger_2 = isset($TOTAL_QTY_BIGGER_2[$maintain_employee_id][$agent_id])?$TOTAL_QTY_BIGGER_2[$maintain_employee_id][$agent_id]:0;
                  $qty_bigger_2_customer = isset($TOTAL_QTY_BIGGER_2_COUNT_CUSTOMER[$maintain_employee_id][$agent_id])?$TOTAL_QTY_BIGGER_2_COUNT_CUSTOMER[$maintain_employee_id][$agent_id]:0;
                  $remain = $item_total_all-$total_using_gas_hm-$qty_bigger_2;
                  $infoC = '';
                  if(isset($ARR_CUSTOMER_ID[$maintain_employee_id][$agent_id])){
                      foreach($ARR_CUSTOMER_ID[$maintain_employee_id][$agent_id] as $customer_id){
                          $infoC .= ExportList::$replaceNewLine.$MODEL_CUSTOMER[$customer_id]->first_name." - ".$MODEL_CUSTOMER[$customer_id]->phone;
                      }
                  } 
                  $sum_total_all+=$item_total_all;
                  $sum_using_gas_hm+=$total_using_gas_hm;
                  $sum_qty_bigger_2+=$qty_bigger_2;
                  $sum_qty_bigger_2_count_customer+=$qty_bigger_2_customer;
                  $sum_remain+=$remain;

                  $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $name_employer);
                  $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $name_agent);
                  $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($item_total_all!=0)?ActiveRecord::formatCurrency($item_total_all):'');
                  $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($total_using_gas_hm!=0)?ActiveRecord::formatCurrency($total_using_gas_hm):'');
                  $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($qty_bigger_2!=0)?ActiveRecord::formatCurrency($qty_bigger_2):'');
                  $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", (($qty_bigger_2_customer!=0)?ActiveRecord::formatCurrency($qty_bigger_2_customer):'').$infoC);
                  $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($remain!=0)?ActiveRecord::formatCurrency($remain):'');
                  $row++;
                  $i++;
              endforeach;
          endforeach;
          // for sum row total
          $index=1;
          $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", '');
          $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Tổng Cộng");
          $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($sum_total_all!=0)?ActiveRecord::formatCurrency($sum_total_all):'');
          $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($sum_using_gas_hm!=0)?ActiveRecord::formatCurrency($sum_using_gas_hm):'');
          $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($sum_qty_bigger_2!=0)?ActiveRecord::formatCurrency($sum_qty_bigger_2):'');
          $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", (($sum_qty_bigger_2_count_customer!=0)?ActiveRecord::formatCurrency($sum_qty_bigger_2_count_customer):'').$infoC);
          $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($sum_remain!=0)?ActiveRecord::formatCurrency($sum_remain):'');
          // for sum row total  

//          $row--;		
        $index--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);		
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);		
        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:".MyFunctionCustom::columnName($index).$row)
            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);

        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.' Xác định bình quay về của PTTT'.'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }     
    }
    
    
    /**
     * @Author: ANH DUNG Aug 24, 2015
     * @Todo: customer bò, mối
     */
    public static function CustomerStoreCard(){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                        ->setLastModifiedBy("NguyenDung")
                                        ->setTitle('DS Khách Hàng')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("List Customer")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1;
        $i=1;
        $dataAll = $_SESSION['data-excel']->data;
        $aAgent = Users::getSelectByRoleNotRoleAgent(ROLE_AGENT);
        $aTypePay = GasTypePay::getArrAll();
        $mStoreCard = new GasStoreCard();
        $mStoreCard->date_from  = '01-12-2016';
        $mStoreCard->date_to    = '31-12-2016';
        $mStoreCard->customer_id = [];
        $aResult = Sta2::OutputCustomer($mStoreCard);
        $OUTPUT     = isset($aResult['OUTPUT_ALL']) ? $aResult['OUTPUT_ALL'] : [];
        $GAS_REMAIN = isset($aResult['GAS_REMAIN']) ? $aResult['GAS_REMAIN'] : [];

        $cmsFormatter = new CmsFormatter();

        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);            
        $objPHPExcel->getActiveSheet()->setTitle('DS Khách Hàng'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Danh Sách Khách Hàng ');
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);    			
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $row++;
        $index=1;
        $beginBorder = $row;
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'SN');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại Lý');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã Khách Hàng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Loại');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tên Khách Hàng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Địa Chỉ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Điện Thoại');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'NV Sale');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Hạn Thanh Toán');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Trạng Thái');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày Tạo');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Bình Bò');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Bình 12');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tổng cộng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Username');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Số báo giá');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Số bảo trì');
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        foreach($dataAll as $data):
            $data->loadExtPhone2();
            $username = $data->username;
            // DuongNV feb 27,2019 array id khách hàng có đặt hàng qua app trong 3 tháng
            if(isset($_SESSION['data-excel-app'])){
                $aUserUseApp = $_SESSION['data-excel-app'];
                if(in_array($data->id, $aUserUseApp)) {
                    continue;
                }
            }
            if($data->apiCheckChangePass()){
                $username = "$username - $data->temp_password";
            }
            $index=1;
            $AgentName = isset($aAgent[$data->area_code_id]) ? $aAgent[$data->area_code_id]:'';
            $TypePay = isset($aTypePay[$data->payment_day]) ? $aTypePay[$data->payment_day]:'';
            $TypeBoMoi = $data->is_maintain?CmsFormatter::$CUSTOMER_BO_MOI[$data->is_maintain]:'';
            $data->md5pass = 'for_export';
            $Sale = $cmsFormatter->formatSaleAndLevel($data);
            $customer_id = $data->id;
            
            // for sản lượng BB và B12
            $qty_b50 = isset($OUTPUT[$customer_id][MATERIAL_TYPE_BINHBO_50]) ? $OUTPUT[$customer_id][MATERIAL_TYPE_BINHBO_50]:0;
            $qty_b45 = isset($OUTPUT[$customer_id][MATERIAL_TYPE_BINHBO_45]) ? $OUTPUT[$customer_id][MATERIAL_TYPE_BINHBO_45]:0;
            $qty_b12 = isset($OUTPUT[$customer_id][MATERIAL_TYPE_BINH_12]) ? $OUTPUT[$customer_id][MATERIAL_TYPE_BINH_12]:0;
            $amount_50 = $qty_b50*CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINHBO_50];
            $amount_45 = $qty_b45*CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINHBO_45];
            $amount_12 = $qty_b12*CmsFormatter::$MATERIAL_VALUE_KG[MATERIAL_TYPE_BINH_12];
            $remain = isset($GAS_REMAIN[$customer_id])?$GAS_REMAIN[$customer_id]:0;
//            echo $qty_b50." = $qty_b45 = $qty_b12 = $remain";
            $outputBB   = $amount_50 + $amount_45 - $remain;
            $outputB12  = $amount_12;
            // for sản lượng BB và B12

            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $i);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $AgentName);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->code_bussiness);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $TypeBoMoi);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->first_name);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->address);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row",$data->phone, PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $cmsFormatter->formatSaleAndLevel($data));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $TypePay);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $cmsFormatter->formatStatusLayHang($data));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $cmsFormatter->formatDatetime($data->created_date));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $outputBB);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $outputB12);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", ($outputBB + $outputB12));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $username);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row",$data->phone_ext2, PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row",$data->phone_ext3, PHPExcel_Cell_DataType::TYPE_STRING);
            $row++;
            $i++;
        endforeach;	
        $row--;		
        $index--;
//        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("C$beginBorder:C".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("D$beginBorder:D".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getStyle("L$beginBorder:N".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()
                                ->getStyle("L$beginBorder:N".$row)->getNumberFormat()
                                ->setFormatCode('#,##0');
        $objPHPExcel->getActiveSheet()->getStyle("N$beginBorder:N".$row)->getFont()
                            ->setBold(true);  
        
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:H".$row)
            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(18);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(80);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);

        // DuongNV feb 27,2019 hủy session
        if(isset($_SESSION['data-excel-app'])){
            unset($_SESSION['data-excel-app']);
        }
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.'customer-list-'.date("Y-m-d").'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();                     		          
      }catch (Exception $e)
      {
          MyFormat::catchAllException($e);
      }                 
    }
    
    // Anh Dung Aug 05, 2016
    public static function Borrow(){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                        ->setLastModifiedBy("NguyenDung")
                                        ->setTitle('DanhSachVayDaoHan')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("DanhSachVayDaoHan")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1;
        $i=1;
        $cmsFormatter = new CmsFormatter();
        $model = $_SESSION['ModelForExcel'];
        $aData = $_SESSION['data-excel']->data;
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Danh sách khoản vay cần đáo hạn'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "Danh sách khoản vay cần đáo hạn từ $model->expiry_date_from đến $model->expiry_date_to");
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);    			
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã khế ước');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Hình thức Vay');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Công ty vay');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Ngân hàng");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Chi Tiết");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Ngày vay");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Ngày đáo hạn");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Ngày chờ đáo hạn");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Tiền đã vay");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Tiền đã đáo hạn");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Tiền còn vay");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Ngày tạo");
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        self::BorrowBody($model, $aData, $objPHPExcel, $row, $i);
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(30);

        $row--;
//        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("H$beginBorder:K".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()
                                ->getStyle("H$beginBorder:K".$row)->getNumberFormat()
                                ->setFormatCode('#,##0'); 
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."DanhSachDaoHan__{$model->expiry_date_from}__{$model->expiry_date_to}".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public static function BorrowBody($model, $aData, &$objPHPExcel, &$row, &$i) {
        $sumBorrow      = 0;
        $sumPayAmount   = 0;
        foreach($aData as $data){
            $sumBorrow      += $data->getBorrowAmount();
            $sumPayAmount   += $data->getPayAmount();
            $index=1;
            $detail = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $data->getDetail());
            $detail = str_replace(ExportList::$remove, " ", $detail);
            
            $createdDate = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $data->getCreatedInfo());
            $createdDate = str_replace(ExportList::$remove, " ", $createdDate);
            
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getContractNo());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getBorrowType());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCompany());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getBank());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $detail);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getBorrowDate());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getExpiryDate());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getDayWaitExpiryDate());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getBorrowAmount());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getPayAmount());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getRemainAmount());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $createdDate);
//            $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
            $row++;
        }
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "Tổng Cộng");
        $objPHPExcel->getActiveSheet()->getStyle("A$row:L$row")->getFont()
                            ->setBold(true);    			
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $objPHPExcel->getActiveSheet()->setCellValue("I$row", $sumBorrow);
        $objPHPExcel->getActiveSheet()->setCellValue("J$row", $sumPayAmount);
        $objPHPExcel->getActiveSheet()->setCellValue("K$row", ($sumBorrow-$sumPayAmount));
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $row++;
    }

    // Anh Dung Aug 24, 2016
    public function sellDaily($aData, $mSell){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                        ->setLastModifiedBy("NguyenDung")
                                        ->setTitle('DailyBanHangHoGiaDinh')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("DailyBanHangHoGiaDinh")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1;
        $i=1;
        $cmsFormatter = new CmsFormatter();
//        $aData = $_SESSION['data-excel']->data;
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Daily Bán Hàng Hộ Gia Đình'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "Daily Bán Hàng Hộ Gia Đình");
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        $isTayNguyen = false;
        
        if(isset($_GET['type']) && $_GET['type'] == 1){
            $isTayNguyen = true;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày bán');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại lý');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Khách hàng");
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Số điện thoại");
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Địa chỉ");
        }else{
        
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày bán');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã số');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Trạng thái');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại lý');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Khách hàng');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Địa chỉ');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Loại KH');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Số điện thoại');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Kế toán bán hàng');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'NV giao nhận');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Gas + Vật tư');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'SL');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Giá');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Thành tiền');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Chiết khấu');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Vỏ');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'SL');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Khuyến mãi');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'SL');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Khuyến mãi 2');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'SL 2');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Lý do hủy');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày tạo');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Giảm giá');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Seri');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'SL Gas dư');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tiền Gas dư');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Loại đơn hàng');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Bán vỏ');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Bù vỏ');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Phút hoàn thành');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Phút hoàn thành trễ');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Giám sát');
            // Phạm Thành NGhĩa 19/7/2018 (chị Ngọc thêm nội dung hủy , audit cancel = order_type_status , audit note )
            
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Nội dung hủy');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Audit Cancel');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Audit Cancel Note');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Telesale');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'App count');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'KH cũ/mới');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Bù vỏ chính sách');
        }
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        if($isTayNguyen){
            $this->SellDailyBodyTayNguyen($aData, $objPHPExcel, $row, $i);
        }else{
            $this->sellDailyBody($aData, $objPHPExcel, $row, $i);
        }
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setWidth(25);

        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:C".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("L$beginBorder:L".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("Q$beginBorder:Q".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("S$beginBorder:S".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("M$beginBorder:O".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle("AE$beginBorder:AF".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()
                                ->getStyle("M$beginBorder:O".$row)->getNumberFormat()
                                ->setFormatCode('#,##0'); 
        $objPHPExcel->getActiveSheet()
                                ->getStyle("X$beginBorder:X".$row)->getNumberFormat()
                                ->setFormatCode('#,##0'); 
        $objPHPExcel->getActiveSheet()
                                ->getStyle("AA$beginBorder:AA".$row)->getNumberFormat()
                                ->setFormatCode('#,##0'); 
        $objPHPExcel->getActiveSheet()
                                ->getStyle("AC$beginBorder:AD".$row)->getNumberFormat()
                                ->setFormatCode('#,##0'); 
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        if(isset($_SESSION['data-excel'])){
            unset($_SESSION['data-excel']);
        }
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        if($this->output == ExportList::STORE_SERVER){
            $mCronExcel  = new CronExcel();
            $file_name   = $mCronExcel->getFileFinalPath($mSell);
            $objWriter->save($file_name);
        } else {
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="'."DailyBanHangHoGiaDinh".'.'.'xlsx'.'"');

            header('Cache-Control: max-age=0');				
            $objWriter->save('php://output');			
            Yii::app()->end();
        }
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public function sellDailyBody($aData, &$objPHPExcel, &$row, &$i) {
        $mAppCache      = new AppCache();
        $aDieuPhoi      = $mAppCache->getListdataUserByRole(ROLE_DIEU_PHOI);
        $aCallCenter    = $mAppCache->getListdataUserByRole(ROLE_CALL_CENTER);
        $aPvkh          = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MAINTAIN);
        $aCcs           = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MARKET_DEVELOPMENT);
        $aAgent         = $mAppCache->getListdataUserByRole(ROLE_AGENT);
        $aCallCenter    = $aDieuPhoi + $aCallCenter;
        $aPvkh          = $aPvkh + $aCcs;
        $aTelesale      = $mAppCache->getListdataUserByRole(ROLE_TELESALE);
        
        $aAgentMonitor = GasOneMany::getManyIdByType(GasOneMany::TYPE_AGENT_OF_MONITOR);
        $aMonitorId = [];
        foreach($aAgentMonitor as $agent_id => $monitor_id){
            $aMonitorId[] = $monitor_id;
        }
        $listdataMonitor = Users::getListOptions($aMonitorId, ['get_all'=>1]);

        $aCustomerId = $aCustomerAtt = [];
        // 1, get list customer
        foreach($aData as $data){
            $aCustomerId[] = $data->customer_id;
        }
        $aModelCustomer = Users::getArrayModelByArrayId($aCustomerId);
        foreach($aModelCustomer as $mCustomer){
            $aCustomerAtt[$mCustomer->id] = $mCustomer->getAttributes();
        }
        $aModelCustomer = $aCustomerId = null;
        
        if($this->output == ExportList::STORE_SERVER){
            $aMaterial   = array_merge(GasMaterialsType::$ARR_WINDOW_GAS, GasMaterialsType::$ARR_WINDOW_VO, GasMaterialsType::$ARR_WINDOW_PROMOTION);
            $needMore    = array('GetAll'=>1);
            $aMaterials  = MyFunctionCustom::getMaterialsModel($aMaterial, $needMore);
        }
        foreach($aData as $data){
            $appCount = '';
            $data->isExcelExport = 1;
//            $detail = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $data->getDetail());
//            $detail = str_replace(ExportList::$remove, " ", $detail);
            $note = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $data->note);
//            $viewGasName    = $data->getInfoGas();
            $viewGasName        = null;
            if($this->output == ExportList::STORE_SERVER){
                $viewGasName    = $data->getInfoGasCron($aMaterials);
            } else {
                $viewGasName    = $data->getInfoGas();
            }
            $aFor = count($viewGasName) > count($data->viewVoName) ? $viewGasName :  $data->viewVoName;
            
            $aInfoCustomer = isset($aCustomerAtt[$data->customer_id]) ? $aCustomerAtt[$data->customer_id] : '';
            $last_purchase = $telesale = $code_bussiness = $code_account = $customer_name = $customer_add = $customer_type = $customer_group = '';
            $phone = $data->getCustomerPhone();
            if(!empty($aInfoCustomer)){
                $mUser = new Users();
                $mUser->is_maintain = $aInfoCustomer['is_maintain'];
                $code_account   = $aInfoCustomer['code_account'];
                $code_bussiness = $aInfoCustomer['code_bussiness'];
                $customer_name  = $aInfoCustomer['first_name'];
                $customer_add   = $aInfoCustomer['address'];
                $customer_type  = $mUser->getTypeCustomerText();
                $last_purchase  = $aInfoCustomer['last_purchase'];

                if(empty($phone)){
                    $phone   = $aInfoCustomer['phone'];
                }
                $temp = explode('-', $phone);
                $phone = isset($temp[0]) ? $temp[0] : '';
            }
//            if($data->first_order == Sell::IS_FIRST_ORDER && !empty($data->sale_id)){
//                $telesale = $data->getTextBqv($aTelesale);// Sep0319 Close to test giảm time export
                $telesale = '';
            if($data->source == Sell::SOURCE_APP){
//                $appCount = ($data->created_date_only == $last_purchase ? 'App lần 1' : 'App lần 2');
                $appCount = ($data->first_order == Sell::IS_FIRST_ORDER ? 'App lần 1' : 'App lần 2');
            }
//            if($data->source == Sell::SOURCE_APP && $data->uid_login == GasConst::UID_ADMIN){
//                $appCount = 'App lần 2';
//            }
            
            $nameUserCreated    = isset($aCallCenter[$data->uid_login]) ? $aCallCenter[$data->uid_login] : '';
            $namePVKH           = isset($aPvkh[$data->employee_maintain_id]) ? $aPvkh[$data->employee_maintain_id] : '';
            $nameAgent          = isset($aAgent[$data->agent_id]) ? $aAgent[$data->agent_id] : '';
            $nameMonitor    = '';
            if(isset($aAgentMonitor[$data->agent_id])){
                $nameMonitor    = $listdataMonitor[$aAgentMonitor[$data->agent_id]];
            }
            
            $data->setBuVo($data->materials_parent_id_gas, $data->materials_id_vo);
            
            foreach($aFor as $key => $nothing){
                $index=1;
                $timeComplete   = $data->getTimeOverOnlyNumber();
                $timeOver       = '';
                if($timeComplete > MonitorUpdate::TIME_COMPLETE_HGD){
                    $timeOver       = $timeComplete - MonitorUpdate::TIME_COMPLETE_HGD;
                }
                
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCreatedDateOnly());
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCodeNo());
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getStatus());
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $nameAgent);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $customer_name);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $customer_add);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $customer_type);
//                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCustomerPhone());
                $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row", $phone, PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $nameUserCreated);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $namePVKH);
                
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($data->viewGasName[$key]) ? $data->viewGasName[$key] : '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($data->viewGasQty[$key]) ? $data->viewGasQty[$key] : '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($data->viewGasPrice[$key]) ? $data->viewGasPrice[$key] : '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($data->viewGasAmount[$key]) ? $data->viewGasAmount[$key] : '');
                
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($data->amountDiscount[$key]) ? $data->amountDiscount[$key] : '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($data->viewVoName[$key]) ? $data->viewVoName[$key] : '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($data->viewVoQty[$key]) ? $data->viewVoQty[$key] : '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($data->viewKmName[$key]) ? $data->viewKmName[$key] : '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($data->viewKmQty[$key]) ? $data->viewKmQty[$key] : '');
                unset($data->viewKmName[$key]);unset($data->viewKmQty[$key]);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($data->viewKmName[$key+1]) ? $data->viewKmName[$key+1] : '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($data->viewKmQty[$key+1]) ? $data->viewKmQty[$key+1] : '');
                unset($data->viewKmName[$key+1]);unset($data->viewKmQty[$key+1]);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $note);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCreatedDate());
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($data->promotionAmount[$key]) ? $data->promotionAmount[$key] : '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($data->gasRemainSeri[$key]) ? $data->gasRemainSeri[$key] : '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($data->gasRemainQty[$key]) ? $data->gasRemainQty[$key] : '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($data->gasRemainAmount[$key]) ? $data->gasRemainAmount[$key] : '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getOrderType());
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($data->amountBanVo[$key]) ? $data->amountBanVo[$key] : '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($data->amountBuVo[$key]) ? $data->amountBuVo[$key] : '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $timeComplete);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $timeOver);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $nameMonitor);
                // thêm ngày 19/7 Phạm Thành NGhĩa theo yêu cầu chị ngọc
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getNote());
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getStatusAuditCancelText());
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getNoteAudit());
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $telesale);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $appCount);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCustomerNewOld());
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->amount_bu_vo);
                
    //            $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
                $row++;
            }
        }
//        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $row++;
    }
    
    public function SellDailyBodyTayNguyen($aData, &$objPHPExcel, &$row, &$i) {
        foreach($aData as $data){
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCreatedDateOnly());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getAgent());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCustomer('first_name'));
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row", $data->getCustomerPhone(), PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCustomer('address'));
            $row++;
        }
        $row++;
    }

     // Anh Dung Sep 19, 2016
    public function hgd($aData, $mUser){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                        ->setLastModifiedBy("NguyenDung")
                                        ->setTitle('KhachHangHoGiaDinh')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("KhachHangHoGiaDinh")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1;
        $i=1;
        $cmsFormatter = new CmsFormatter();
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Khách Hàng Hộ Gia Đình'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "Khách Hàng Hộ Gia Đình");
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại lý');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Khách hàng");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Địa chỉ");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Số điện thoại");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Loại KH");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Ngày tạo");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "ĐH mới nhất");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Username");
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        $this->hgdBody($aData, $objPHPExcel, $row, $i);
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(80);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);

        $row--;
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:C".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        if($this->output == ExportList::STORE_SERVER){
            $mCronExcel  = new CronExcel();
            $file_name   = $mCronExcel->getFileFinalPath($mUser);
            $objWriter->save($file_name);
        } else {
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="'."KhachHangHoGiaDinh".'.'.'xlsx'.'"');

            header('Cache-Control: max-age=0');				
            $objWriter->save('php://output');			
            Yii::app()->end();
        }
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public function hgdBody($aData, &$objPHPExcel, &$row, &$i) {
        foreach($aData as $data){
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getAgentOfCustomer());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getFullName());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getAddress());
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row", $data->getPhone(), PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getTypeCustomerText());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCreatedDate());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getLastPurchase());
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row", $data->username, PHPExcel_Cell_DataType::TYPE_STRING);
            $row++;
        }
        $row++;
    }
    
     // Anh Dung Sep 19, 2016
    public static function SettleView($model){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                        ->setLastModifiedBy("NguyenDung")
                                        ->setTitle('QuyetToan')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("QuyetToan")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1;
        $i=1;
        $cmsFormatter = new CmsFormatter();
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Quyết toán'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", $model->getCompany()." - ". $model->getTypeText());
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Dự toán');
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:F$row");
        $objPHPExcel->getActiveSheet()->setCellValue("G$row", "Quyết toán");
        $objPHPExcel->getActiveSheet()->mergeCells("G$row:L$row");
        $row++;
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", '#');
        $objPHPExcel->getActiveSheet()->setCellValue("B$row", 'Diễn giải');
        $objPHPExcel->getActiveSheet()->setCellValue("C$row", 'Đơn vị');
        $objPHPExcel->getActiveSheet()->setCellValue("D$row", 'Số lượng');
        $objPHPExcel->getActiveSheet()->setCellValue("E$row", 'Đơn giá');
        $objPHPExcel->getActiveSheet()->setCellValue("F$row", 'Thành tiền');
        $objPHPExcel->getActiveSheet()->setCellValue("G$row", 'Số hóa đơn');
        $objPHPExcel->getActiveSheet()->setCellValue("H$row", 'Đơn vị');
        $objPHPExcel->getActiveSheet()->setCellValue("I$row", 'Số lượng');
        $objPHPExcel->getActiveSheet()->setCellValue("J$row", 'Đơn giá');
        $objPHPExcel->getActiveSheet()->setCellValue("K$row", 'Thành tiền');
        $objPHPExcel->getActiveSheet()->setCellValue("L$row", 'Ghi chú');
        $row++;
        $sum1=0;
        $sum2=0;
        foreach($model->rDetail as $record){
            $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
            $objPHPExcel->getActiveSheet()->setCellValue("A$row", $index++);
            $objPHPExcel->getActiveSheet()->setCellValue("B$row", $record->getItemName());
            $objPHPExcel->getActiveSheet()->setCellValue("C$row", $record->getPlanUnit());
            $objPHPExcel->getActiveSheet()->setCellValue("D$row", $record->getPlanQty());
            $objPHPExcel->getActiveSheet()->setCellValue("E$row", $record->getPlanPrice());
            $objPHPExcel->getActiveSheet()->setCellValue("F$row", $record->getPlanTotal());
            $objPHPExcel->getActiveSheet()->setCellValue("G$row", $record->payment_no);
            $objPHPExcel->getActiveSheet()->setCellValue("H$row", $record->unit);
            $objPHPExcel->getActiveSheet()->setCellValue("I$row", $record->qty);
            $objPHPExcel->getActiveSheet()->setCellValue("J$row", $record->price);
            $objPHPExcel->getActiveSheet()->setCellValue("K$row", $record->price * $record->qty);
            $objPHPExcel->getActiveSheet()->setCellValue("L$row", $record->note);
            $row++;
            $sum1 += $record->getPlanTotal();
            $sum2 += $record->price * $record->qty;
            
        }
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Tổng cộng');
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:E$row");
        $objPHPExcel->getActiveSheet()->setCellValue("F$row", $sum1);
        $objPHPExcel->getActiveSheet()->setCellValue("G$row", $sum2);
        $objPHPExcel->getActiveSheet()->mergeCells("G$row:K$row");
        
        $objPHPExcel->getActiveSheet()
                                ->getStyle("D4:K".$row)->getNumberFormat()
                                ->setFormatCode('#,##0'); 
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A1:L3")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A2:L2")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:L$row")->getFont()
                            ->setBold(true);
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(30);

        $objPHPExcel->getActiveSheet()->getStyle("C3:C".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("H3:H".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A2:L".$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."QuyetToan".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 21, 2016
     * @Todo: get list customer cua BT dinh ky
     */
    public static function getListCustomer($aData) {
        $aRes = array();
        $aCustomerId = array();
        $ok = false;
        foreach($aData as $model){
            $aCustomerId[$model->customer_id]   = $model->customer_id;
            if(!empty($model->sale_id)){
                $aCustomerId[$model->sale_id]       = $model->sale_id;
            }
            if(!empty($model->manage_id)){
                $aCustomerId[$model->manage_id]     = $model->manage_id;
            }
            if(!empty($model->employee_id)){
                $aCustomerId[$model->employee_id]   = $model->employee_id;
            }
            $ok = true;
        }
        if($ok){
            $aRes = Users::getArrObjectUserByRole('', $aCustomerId);
        }
        return $aRes;
    }
    
    // Anh Dung Oct 21, 2016
    public static function UpholdSchedule(){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                        ->setLastModifiedBy("NguyenDung")
                                        ->setTitle('DanhSachBaoTriDinhKy')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("DanhSachBaoTriDinhKy")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1;
        $i=1;
        $cmsFormatter   = new CmsFormatter();
        $aData          = $_SESSION['data-excel']->data;
        $aCustomer      = self::getListCustomer($aData);
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Danh sách bảo trì định kỳ'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "Danh sách bảo trì định kỳ");
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);    			
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Người quản lý bảo trì');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Khách Hàng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Địa Chỉ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Loại KH');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "NV Kinh Doanh");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Người Liên Hệ");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Điện Thoại");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Nhân Viên");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Lịch bảo trì");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Trạng Thái");
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        self::UpholdScheduleBody($aData, $aCustomer, $objPHPExcel, $row);
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(45);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);

        $row--;
//        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
//        $objPHPExcel->getActiveSheet()->getStyle("H$beginBorder:K".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()
                                ->getStyle("H$beginBorder:K".$row)->getNumberFormat()
                                ->setFormatCode('#,##0'); 
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."DanhSachBaoTriDinhKy".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public static function UpholdScheduleBody($aData, $aCustomer, &$objPHPExcel, &$row) {
        foreach($aData as $data){
            if(!isset($aCustomer[$data->customer_id])){
                continue;
            }
            $index=1;
            $manageName     = '';
            $saleName       = '';
            $employeeName   = '';
            $mCustomer  = $aCustomer[$data->customer_id];
            if(isset($aCustomer[$data->sale_id])){
                $mSale      = $aCustomer[$data->sale_id];
                $saleName   = $mSale->getFullName();
            }
            if(isset($aCustomer[$data->manage_id])){
                $mManage      = $aCustomer[$data->manage_id];
                $manageName   = $mManage->getFullName();
            }
            if(isset($aCustomer[$data->employee_id])){
                $mEmployee      = $aCustomer[$data->employee_id];
                $employeeName     = $mEmployee->getFullName();
            }
            
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $manageName);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $mCustomer->getFullName());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $mCustomer->address);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $mCustomer->getTypeCustomerText());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $saleName);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getContactPerson());
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row", $data->getContactTel(), PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $employeeName);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getScheduleText());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getStatusUpholdSchedule());
            $row++;
        }
        $row++;
    }
    
    // Anh Dung Now 26, 2016
    /** @Author: DungNT Jan 07, 2019
     *  @param: $aData is array model 
     *  @param: $mGasRemain is model GasRemain
     **/
    public function gasRemain($aData, $mGasRemain){ // DuongNV bỏ static để xuất excel cron
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                        ->setLastModifiedBy("NguyenDung")
                                        ->setTitle('GasRemain')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("GasRemain")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1;
        $i=1;
        $cmsFormatter = new CmsFormatter();
//        $aData = $_SESSION['data-excel']->data;

        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Gas Dư'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "Gas Dư");
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại lý');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Khách hàng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Địa chỉ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Điện thoại');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày Cân');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Loại KH');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Vật Tư');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Seri');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'KL Vỏ Bình');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'KL Gas+Vỏ Bình');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Gas Dư');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Cân lần 2');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Giá');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Thành tiền');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày tạo');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Trạng thái');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Số xe');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tài xế');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Phụ xe 1');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Phụ xe 2');
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        if($this->output == ExportList::STORE_SERVER){
            $this->gasRemainBodyCron($aData, $objPHPExcel, $row, $i);
        }else{
            $this->gasRemainBody($aData, $objPHPExcel, $row, $i);
        }
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);  
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('k')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(20);

        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("J$beginBorder:J".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $objPHPExcel->getActiveSheet()
                                ->getStyle("M$beginBorder:N".$row)->getNumberFormat()
                                ->setFormatCode('#,##0'); 
        if(isset($_SESSION['data-excel'])){
            unset($_SESSION['data-excel']);
        }
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        if($this->output == ExportList::STORE_SERVER){
            $mCronExcel  = new CronExcel();
            $file_name   = $mCronExcel->getFileFinalPath($mGasRemain);
            $objWriter->save($file_name);
        } else {
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="'.'GasRemain'.'.'.'xlsx'.'"');
            header('Cache-Control: max-age=0');				
            $objWriter->save('php://output');
            Yii::app()->end();
        }
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public function gasRemainBody($aData, &$objPHPExcel, &$row, &$i) {
        if(count($aData) < 1){
            return ;
        }
        $aCustomer = self::getModelCustomer($aData);
        foreach($aData as $data){
            $customerName = $customerAdd = $customerPhone = '';
            if(isset($aCustomer[$data->customer_id])){
                $customerName   =  $aCustomer[$data->customer_id]->first_name;
                $customerAdd    =  $aCustomer[$data->customer_id]->address;
                $customerPhone  =  $aCustomer[$data->customer_id]->phone;
            }
            if(in_array($data->agent_id, GasCheck::getAgentNotGentAuto()) && array_key_exists($data->agent_id_old, $data->getArrayAgentCar())){
                $mRemainTemp            = new GasRemain();
                $mRemainTemp->agent_id  = $data->agent_id_old;
                $customerName           =  $mRemainTemp->getAgent();
                $customerAdd = $customerPhone = '';// Oct1818 Sửa thành tên xe lưu động hcm1,2 theo NganDtm
            }
            
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getAgent());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $customerName);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $customerAdd);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row", $customerPhone, PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getDateInput());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getTypeCustomer());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getMaterials());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->seri);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->amount_empty);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->amount_has_gas);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->amount_gas);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->amount_gas_2);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->price);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->amount);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCreatedDate());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", strip_tags($data->getTextHasExport()));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCar());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getDriver());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getPhuXe1());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getPhuXe2());
            $row++;
        }
        $row++;
    }
    
    /** @Author: DungNT Jan 07, 2019
     *  @Todo: write new body for cron export
     **/
    public function gasRemainBodyCron($aData, &$objPHPExcel, &$row, &$i) {
        if(count($aData) < 1){
            return ;
        }
        $mAppCache      = new AppCache();
        $aCustomer      = self::getModelCustomer($aData);
        $aAgent         = $mAppCache->getAgentListdata();
        $aCar           = $mAppCache->getListdataUserByRole(ROLE_CAR);
        $aDriver        = $mAppCache->getListdataUserByRole(ROLE_DRIVER);
        $aPhuXe         = $mAppCache->getListdataUserByRole(ROLE_PHU_XE);
        $aMaterials     = $mAppCache->getListdata('GasMaterials', AppCache::LISTDATA_MATERIAL);


        foreach($aData as $data){
            $agentName      = isset($aAgent[$data->agent_id]) ? $aAgent[$data->agent_id] : '';
            $materialName   = isset($aMaterials[$data->materials_id]) ? $aMaterials[$data->materials_id] : '';
            $driverName     = isset($aDriver[$data->driver_id]) ? $aDriver[$data->driver_id] : '';
            $phuXe1         = isset($aPhuXe[$data->phu_xe_1]) ? $aPhuXe[$data->phu_xe_1] : '';
            $phuXe2         = isset($aPhuXe[$data->phu_xe_2]) ? $aPhuXe[$data->phu_xe_2] : '';
            
            $customerName = $customerAdd = $customerPhone = '';
            if(isset($aCustomer[$data->customer_id])){
                $customerName   =  $aCustomer[$data->customer_id]->first_name;
                $customerAdd    =  $aCustomer[$data->customer_id]->address;
                $customerPhone  =  $aCustomer[$data->customer_id]->phone;
            }
            if(in_array($data->agent_id, GasCheck::getAgentNotGentAuto()) && array_key_exists($data->agent_id_old, $data->getArrayAgentCar())){
                $mRemainTemp            = new GasRemain();
                $mRemainTemp->agent_id  = $data->agent_id_old;
                $customerName           =  $agentName;
                $customerAdd = $customerPhone = '';// Oct1818 Sửa thành tên xe lưu động hcm1,2 theo NganDtm
            }
            
            $carName  = isset($aCar[$data->car_id]) ? $aCar[$data->car_id] : '';
            $agentCar = !empty($data->agent_id_old) ? $data->agent_id_old : $data->agent_id;
            if(array_key_exists($agentCar, $data->getArrayAgentCar())){
                $aAgentCar = $data->getArrayAgentCar();
                $carName   = $aAgentCar[$agentCar];
            }
            
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $agentName);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $customerName);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $customerAdd);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row", $customerPhone, PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getDateInput());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getTypeCustomer());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $materialName);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->seri);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->amount_empty);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->amount_has_gas);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->amount_gas);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->amount_gas_2);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->price);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->amount);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCreatedDate());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", strip_tags($data->getTextHasExport()));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $carName);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $driverName);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $phuXe1);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $phuXe2);
            $row++;
        }
        $row++;
    }
    
    /**
     * @Author: ANH DUNG Now 26, 2016
     * @Todo: get list model customer
     */
    public static function getModelCustomer($aData) {
        $aCustomerId = CHtml::listData($aData, 'customer_id', 'customer_id');
        return Users::getArrObjectUserByRole('', $aCustomerId);
    }
    
    // Anh Dung Sep 19, 2016
    public static function PtttDetail(){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                        ->setLastModifiedBy("NguyenDung")
                                        ->setTitle('KhachHangPTTT')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("KhachHangPTTT")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1;
        $i=1;
        $cmsFormatter = new CmsFormatter();
        $aData = $_SESSION['data-excel']->data;
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Khách Hàng PTTT'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "Khách Hàng PTTT");
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại lý');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Giám sát');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'NV PTTT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Note");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Khách hàng");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Địa chỉ");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Số điện thoại");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Loại Gas");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Seri");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Ghi chú");        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Ngày tạo");
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        self::PtttDetailBody($aData, $objPHPExcel, $row, $i);
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);

        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."KhachHangPTTT".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public static function PtttDetailBody($aData, &$objPHPExcel, &$row, &$i) {
        foreach($aData as $data){
            $agentName      = $data->getAgent();
            $employeeName   = $data->getEmployee();
            $monitoringName = $data->getMonitoring();
            $maintainDate   = $data->getMaintainDate();
            
            foreach($data->rDetailInfo as $key=>$item){
                $index=1;
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $maintainDate);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $agentName);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $monitoringName);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $employeeName);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->note);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $item->customer_name);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $item->customer_address);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row", $item->customer_phone, PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $item->materials_name);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $item->seri);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $item->note);                
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCreatedDate());
                $row++;
            }
        }
        $row++;
    }
    
    // Anh Dung May 12, 2017
    public function appOrderDaily($aData, $mGasAppOrder){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                        ->setLastModifiedBy("NguyenDung")
                                        ->setTitle('DailyBoMoi')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("DailyBoMoi")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1;
        $i=1;
        $cmsFormatter = new CmsFormatter();
//        $aData = $_SESSION['data-excel']->data;
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Daily Bán Hàng Bò Mối'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "Daily Bán Hàng Bò Mối");
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày bán');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại lý');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã KH');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Khách hàng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Địa chỉ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Loại KH');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Gas đi');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'SL');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Giá bán');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Gas dư');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Gas dư tiền');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Qty');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Giảm giá');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Thành tiền');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Vỏ về');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'SL Vỏ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Người giao');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Số xe');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Nhóm KH');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Sale');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'ĐVT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Trạng thái');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Số BBGN');
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        $this->appOrderDailyBody($aData, $objPHPExcel, $row, $i);
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(20);

        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("C$beginBorder:C".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle("I$beginBorder:N".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
//        $objPHPExcel->getActiveSheet()->getStyle("L$beginBorder:L".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle("H$beginBorder:H".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
//        $objPHPExcel->getActiveSheet()->getStyle("K$beginBorder:K".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle("P$beginBorder:P".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()
                                ->getStyle("H$beginBorder:I".$row)->getNumberFormat()
                                ->setFormatCode('#,##0'); 
        $objPHPExcel->getActiveSheet()
                                ->getStyle("K$beginBorder:N".$row)->getNumberFormat()
                                ->setFormatCode('#,##0'); 
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        if(isset($_SESSION['data-excel'])){
            unset($_SESSION['data-excel']);
        }
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        if($this->output == self::STORE_SERVER){
            $mCronExcel             = new CronExcel();
            $mCronExcel->nameDir    = CronExcel::DIR_BOMOI_DAILY;
            $file_name              = $mCronExcel->getFileFinalPath($mGasAppOrder);
            $objWriter->save($file_name);
        } else {
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="'."DailyBoMoi".'.'.'xlsx'.'"');

            header('Cache-Control: max-age=0');				
            $objWriter->save('php://output');			
            Yii::app()->end();
        }
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public function appOrderDailyBody($aData, &$objPHPExcel, &$row, &$i) {
        $mAppCache          = new AppCache();
        $aSale              = $mAppCache->getListdataUserByRole(ROLE_SALE);
        $aChuyenVien        = $mAppCache->getListdataUserByRole(ROLE_MONITORING_MARKET_DEVELOPMENT);
        $aCcs               = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MARKET_DEVELOPMENT);
        $aEmployeeMaintain  = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MAINTAIN);
        $aMonitor   = $mAppCache->getListdataUserByRole(ROLE_MONITOR_AGENT);
        $aSaleMerge = $aSale + $aChuyenVien + $aCcs + $aMonitor + $aEmployeeMaintain;
        $aCustomerId = [];
        // 1, get list customer
        foreach($aData as $data){
            $aCustomerId[$data->customer_id] = $data->customer_id;
        }
        $aModelCustomer = Users::getArrayModelByArrayId($aCustomerId);

        foreach($aData as $data){
//            $detail = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $data->getDetail());
//            $detail = str_replace(ExportList::$remove, " ", $detail);
            $data->getStringGasReportDaily();
            $showValue = true;
            $aFor = count($data->aListGas) > count($data->aListVo) ? $data->aListGas :  $data->aListVo;
            $oldGasId = 0;
            $nameSale   = isset($aSaleMerge[$data->sale_id]) ? $aSaleMerge[$data->sale_id] : $data->sale_id;

            $mCustomer = isset($aModelCustomer[$data->customer_id]) ? $aModelCustomer[$data->customer_id] : '';
            $code_bussiness = $code_account = $customer_name = $customer_add = $customer_type = $customer_group = '';
            if(!empty($mCustomer)){
                $code_account   = $mCustomer->code_account;
                $code_bussiness = $mCustomer->code_bussiness;
                $customer_name  = $mCustomer->first_name;
                $customer_add   = $mCustomer->address;
                $customer_type  = $mCustomer->getTypeCustomerText();
                $customer_group = $mCustomer->getGroup();
            }

            foreach($aFor as $key => $nothing){
                $typeCustomerText = isset($data->rCustomer) ? $data->rCustomer->getTypeCustomerText() : '';
                $index=1;
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getDateDelivery());
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getAgent());
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $code_bussiness);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $customer_name);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $customer_add);
//                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->rCustomer->getTypeCustomerText());
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $typeCustomerText);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($data->aListGas[$key]) ? $data->aListGas[$key] : '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($data->aListGasQty[$key]) ? $data->aListGasQty[$key] : '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($data->aListPrice[$key]) ? $data->aListPrice[$key] : '');
                
                $totalRemain    = $data->totalRemain;
                $totalWeightGas = $data->totalWeightGas;
                $totalGas       = $data->getTotalGas();
                $totalDiscount  = $data->discount;
                $cGasId         = isset($data->aListGasId[$key]) ? $data->aListGasId[$key] : 0;
                $totalRemainAmount = $data->total_gas_du;
//                if(!$showValue && empty($cGasId) && $cGasId == $oldGasId){
                if(!$showValue){
                    $totalRemain = $totalWeightGas = $totalGas = $totalRemainAmount = $totalDiscount = '';
                }
                $oldGasId = $cGasId;
                
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $totalRemain);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $totalRemainAmount);
//                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $totalWeightGas);
//                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $totalGas);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($data->aListGasWeight[$key]) ? $data->aListGasWeight[$key] : '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $totalDiscount);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($data->aListGasAmount[$key]) ? $data->aListGasAmount[$key] : '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($data->aListVo[$key]) ? $data->aListVo[$key] : '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($data->aListVoQty[$key]) ? $data->aListVoQty[$key] : '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getNameDriverApp());
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getNameCar());
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $customer_group);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $nameSale);
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", isset($data->aListUnit[$key]) ? $data->aListUnit[$key] : '');
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getStatusText());
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getReceiptsNo());

    //            $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
                $row++;
                $showValue = false;
            }
        }
//        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
    }
    
    // Anh Dung May 16, 2017
    public static function CashbookAgent(){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                        ->setLastModifiedBy("NguyenDung")
                                        ->setTitle('SoQuyTienMat')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("SoQuyTienMat")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1;
        $i=1;
        $cmsFormatter = new CmsFormatter();
        $title                  = $_SESSION['data-excel']['title'];
        $openingBalance         = $_SESSION['data-excel']['openingBalance'];
        $aModelCashBookDetail   = $_SESSION['data-excel']['aModelCashBookDetail'];
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Sổ quỹ tiền mặt'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", $title);
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Diễn Giải');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Loại');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Nhân Viên');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Khách Hàng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'SL');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Thu');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Chi');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tồn');
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        self::CashbookAgentBody($openingBalance, $aModelCashBookDetail, $objPHPExcel, $row, $i);
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);

        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("F$beginBorder:F".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("G$beginBorder:I".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
//        $objPHPExcel->getActiveSheet()->getStyle("M$beginBorder:O".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()
                                ->getStyle("G$beginBorder:I".$row)->getNumberFormat()
                                ->setFormatCode('#,##0'); 
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."SoQuyTienMat".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public static function CashbookAgentBody($openingBalance, $aModelCashBookDetail, &$objPHPExcel, &$row, &$i) {
        $mAppCache      = new AppCache();
        $aEmployee      = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MAINTAIN);
        $aCashbookType  = $mAppCache->getMasterModel('GasMasterLookup', AppCache::ARR_MODEL_LOOKUP);
        
        foreach($aModelCashBookDetail as $data){
            $index=1;
            $date           = MyFormat::dateConverYmdToDmy($data->release_date);
            $employeeName   = isset($aEmployee[$data->employee_id]) ? $aEmployee[$data->employee_id] : $item->name_employee;
            $typeName       = isset($aCashbookType[$data->master_lookup_id]) ? $aCashbookType[$data->master_lookup_id]['name'] : '';
            $amount         = $data->amount > 0 ? $data->amount : '';
            $qty            = $data->qty > 0 ? $data->qty : '';
            $in_text = $out_text = '';
            if($data->type==MASTER_TYPE_REVENUE){ // thu
                $openingBalance += $data->amount;
                $in_text = $amount;
            }elseif($data->type==MASTER_TYPE_COST){ // chi
                $openingBalance -= $data->amount;
                $out_text = $amount;
            }
            
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $date);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->description);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $typeName);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $employeeName);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCustomer('first_name'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $qty);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $in_text);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $out_text);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $openingBalance);
//            $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
            $row++;
        }
    }
    
    // Anh Dung Jun 19, 2017
    public function employeeCashbook($aData, $mEmployeeCashbook){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                        ->setLastModifiedBy("NguyenDung")
                                        ->setTitle('ChiTietSoQuyTienMat')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("ChiTietSoQuyTienMat")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1;
        $i=1;
        $cmsFormatter = new CmsFormatter();
//        $aData = $_SESSION['data-excel']->data;
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Chi tiết sổ quỹ tiền mặt'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Chi tiết sổ quỹ tiền mặt');
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Người thu');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Khách Hàng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Địa chỉ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Loại Thu Chi');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Thu');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Chi');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ghi Chú');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã nhân viên');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Nhân viên');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày Tạo');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày sửa');
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        $this->employeeCashbookBody($aData, $objPHPExcel, $row, $i);
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);

        $row--;
//        $objPHPExcel->getActiveSheet()->getStyle("F$beginBorder:F".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("F$beginBorder:G".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle("F$beginBorder:G".$beginBorder)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
//        $objPHPExcel->getActiveSheet()->getStyle("M$beginBorder:O".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()
                                ->getStyle("F$beginBorder:G".$row)->getNumberFormat()
                                ->setFormatCode('#,##0'); 
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        if(isset($_SESSION['data-excel'])){
            unset($_SESSION['data-excel']);
        }
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        if($this->output == ExportList::STORE_SERVER){
            $mCronExcel  = new CronExcel();
            $file_name   = $mCronExcel->getFileFinalPath($mEmployeeCashbook);
            $objWriter->save($file_name);
        } else {
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="'."ChiTietSoQuyTienMat".'.'.'xlsx'.'"');

            header('Cache-Control: max-age=0');				
            $objWriter->save('php://output');			
            Yii::app()->end();
        }
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public function employeeCashbookBody($aData, &$objPHPExcel, &$row, &$i) {
        $mAppCache              = new AppCache();
        $aAgent                 = $mAppCache->getAgentListdata();
        $aUidLogin = $aCustomerId = [];
        foreach($aData as $data){
            $aUidLogin[$data->uid_login]        = $data->uid_login;
            $aCustomerId[$data->customer_id]    = $data->customer_id;
        }
        $aCustomer              = Users::getArrayModelByArrayId($aCustomerId);
        $aLoginName             = Users::getArrayModelByArrayId($aUidLogin);
        foreach($aData as $data){
            $agentName          = isset($aAgent[$data->agent_id]) ? $aAgent[$data->agent_id] : '';
            $customerName       = isset($aCustomer[$data->customer_id]) ? $aCustomer[$data->customer_id]->first_name : '';
            $customerAddr       = isset($aCustomer[$data->customer_id]) ? $aCustomer[$data->customer_id]->address : '';
            $loginName          = isset($aLoginName[$data->uid_login]) ? $aLoginName[$data->uid_login]->first_name : '';
            $loginCodeAccount   = isset($aLoginName[$data->uid_login]) ? $aLoginName[$data->uid_login]->code_account : '';
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getDateInput());
//            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getAgent());
//            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCustomer());
//            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCustomer('address'));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $agentName);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $customerName);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $customerAddr);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getMasterLookupText());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getAmountThu(false));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getAmountChi(false));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getNoteWebShow());
//            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getUidLogin());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $loginCodeAccount);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $loginName);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCreatedDate());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getLastUpdateTime());
            $row++;
        }
        $aCustomer = $aLoginName = null;
    }
    
    // Anh Dung Jun 19, 2017
    public static function CashbookAgentAll(){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                        ->setLastModifiedBy("NguyenDung")
                                        ->setTitle('TongHopSoQuyTienMat')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("TongHopSoQuyTienMat")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1; $i=1;
        $aData = $_SESSION['data-excel'];
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Tổng hợp sổ quỹ tiền mặt'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Tổng hợp sổ quỹ tiền mặt');
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại lý');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tồn đầu');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Thu');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Chi');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tồn cuối');
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        self::CashbookAgentAllBody($aData, $objPHPExcel, $row, $i);
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);

        $row--;
//        $objPHPExcel->getActiveSheet()->getStyle("F$beginBorder:F".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("C$beginBorder:F".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle("C$beginBorder:F".$beginBorder)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
//        $objPHPExcel->getActiveSheet()->getStyle("M$beginBorder:O".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()
                                ->getStyle("C$beginBorder:F".$row)->getNumberFormat()
                                ->setFormatCode('#,##0'); 
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."TongHopSoQuyTienMat".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public static function CashbookAgentAllBody($aData, &$objPHPExcel, &$row, &$i) {
        $model          = $_SESSION['data-model'];
        $mAppCache      = new AppCache();
        $ARR_OPENING    = $aData[GasCashBookDetail::P_OPENING_BALANCE];
        $INCURRED       = isset($aData[GasCashBookDetail::P_INCURRED]) ? $aData[GasCashBookDetail::P_INCURRED] : [];
        $index = 1; $aAgent = $mAppCache->getAgent(); $sumEnd = 0;
        
        foreach($model->agent_id as $agent_id){
            if(!$model->allowAccessAgent($agent_id)){
                continue ;// Add check Dec2518
            }
            $aInfo          = isset($ARR_OPENING[$agent_id]) ? $ARR_OPENING[$agent_id] : ['revenue'=>0, 'cost'=>0];
            $open           = $aInfo['revenue'] - $aInfo['cost'];
            $amountRevenue  = isset($INCURRED[$agent_id]['revenue']) ? $INCURRED[$agent_id]['revenue'] : 0;
            $amountCost     = isset($INCURRED[$agent_id]['cost']) ? $INCURRED[$agent_id]['cost'] : 0;
            $end            = $open + $amountRevenue - $amountCost;
            $sumEnd         += $end;
            
            $agentCode = isset($aAgent[$agent_id]) ? $aAgent[$agent_id]['code_account'] : '';
            $agentName = isset($aAgent[$agent_id]) ? $aAgent[$agent_id]['first_name'] : '';
            
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $agentCode);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $agentName);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $open);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $amountRevenue);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $amountCost);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $end);
            $row++;
        }
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Tổng cộng');
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:F$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:E$row");
        $objPHPExcel->getActiveSheet()->setCellValue("F$row", $sumEnd);
        $row++;
    }
    
    // Anh Dung Jun 19, 2017
    public static function CashbookEmployee(){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                        ->setLastModifiedBy("NguyenDung")
                                        ->setTitle('SoQuyNhanVien')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("SoQuyNhanVien")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1; $i=1;
        $aData = $_SESSION['data-excel'];
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Sổ quỹ nhân viên'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Tổng hợp sổ quỹ tiền mặt nhân viên');
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại lý');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Nhân viên');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tồn đầu');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Thu');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Chi');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tồn cuối');
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        self::CashbookEmployeeBody($aData, $objPHPExcel, $row);
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);

        $row--;
//        $objPHPExcel->getActiveSheet()->getStyle("F$beginBorder:F".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("D$beginBorder:G".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
//        $objPHPExcel->getActiveSheet()->getStyle("C$beginBorder:F".$beginBorder)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
//        $objPHPExcel->getActiveSheet()->getStyle("M$beginBorder:O".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()
                                ->getStyle("D$beginBorder:G".$row)->getNumberFormat()
                                ->setFormatCode('#,##0'); 
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."SoQuyNhanVien".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public static function CashbookEmployeeBody($aData, &$objPHPExcel, &$row) {
        $model          = $_SESSION['data-model'];
        $mAppCache      = new AppCache();
        $ARR_OPENING    = $aData[GasCashBookDetail::P_OPENING_BALANCE];
        $INCURRED       = isset($aData[GasCashBookDetail::P_INCURRED]) ? $aData[GasCashBookDetail::P_INCURRED] : [];
        $index = 1; $aAgent = $mAppCache->getAgent(); $sumEnd = 0;
        $ARR_USER = $aData['ARR_USER'];
        $aAgentId = $model->getAgentByProvince();
        
        foreach($ARR_USER as $employee_id => $mUser){
//            $agent_id       = $mUser->getAgentOfEmployee();
            $agent_id       = $mUser->parent_id;
            if(!$model->allowAccessAgent($agent_id) || !in_array($agent_id, $aAgentId)){
                continue ;
            }
            $aInfo          = isset($ARR_OPENING[$employee_id]) ? $ARR_OPENING[$employee_id] : ['revenue'=>0, 'cost'=>0];
            $open           = $aInfo['revenue'] - $aInfo['cost'];
            $amountRevenue  = isset($INCURRED[$employee_id]['revenue']) ? $INCURRED[$employee_id]['revenue'] : 0;
            $amountCost     = isset($INCURRED[$employee_id]['cost']) ? $INCURRED[$employee_id]['cost'] : 0;
            $end            = $open + $amountRevenue - $amountCost;
            $sumEnd         += $end;
            
            $agentCode = isset($aAgent[$agent_id]) ? $aAgent[$agent_id]['code_account'] : '';
            $agentName = isset($aAgent[$agent_id]) ? $aAgent[$agent_id]['first_name'] : '';
            
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $agentCode);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $agentName);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $mUser->first_name);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $open);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $amountRevenue);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $amountCost);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $end);
            $row++;
        }
        
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", 'Tổng cộng');
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:G$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:F$row");
        $objPHPExcel->getActiveSheet()->setCellValue("G$row", $sumEnd);
        $row++;
        
    }
    
    // Anh Dung Aug2617
    public function appOrderList($aData, $mGasAppOrder){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                        ->setLastModifiedBy("NguyenDung")
                                        ->setTitle('AppOrderList')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("AppOrderList")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1;
        $i=1;
        $cmsFormatter = new CmsFormatter();
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Đơn Hàng Bò Mối'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "Đơn Hàng Bò Mối");
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày giao');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã số');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "Loại");
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại lý/người thực hiện');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã Kế toán');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Khách hàng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Địa chỉ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Loại KH');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Chi tiết');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Trạng thái');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Lý do hủy');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Người tạo');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày tạo');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Phút hoàn thành');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Phút hoàn thành trễ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'NV kinh doanh');
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        $this->appOrderListBody($aData, $objPHPExcel, $row);
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);

        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("E$beginBorder:E".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle("N$beginBorder:O".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        if(isset($_SESSION['data-excel'])){
            unset($_SESSION['data-excel']);
        }
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        if($this->output == ExportList::STORE_SERVER){
            $mCronExcel  = new CronExcel();
            $file_name   = $mCronExcel->getFileFinalPath($mGasAppOrder);
            $objWriter->save($file_name);
        } else {
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="'."AppOrderList".'.'.'xlsx'.'"');

            header('Cache-Control: max-age=0');				
            $objWriter->save('php://output');			
            Yii::app()->end();
        }
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public function appOrderListBody($aData, &$objPHPExcel, &$row) {
        $aCustomerId = [];
        // 1, get list customer
        foreach($aData as $data){
            $aCustomerId[$data->customer_id] = $data->customer_id;
        }
        $aModelCustomer = Users::getArrayModelByArrayId($aCustomerId);
        $aSale          = ToExcel::getListdataSale();
        $mAppCache      = new AppCache();
        $aAgent         = $mAppCache->getListdataUserByRole(ROLE_AGENT);
              
        // 2. render excel
        foreach($aData as $data){
            $mCustomer = isset($aModelCustomer[$data->customer_id]) ? $aModelCustomer[$data->customer_id] : '';
            $code_account = $customer_name = $customer_add = $customer_type = $saleName = '';
            if(!empty($mCustomer)){
                $code_account   = $mCustomer->code_account;
                $customer_name  = $mCustomer->first_name;
                $customer_add   = $mCustomer->address;
                $customer_type  = $mCustomer->getTypeCustomerText();
            }
            $saleName       = isset($aSale[$data->sale_id]) ? $aSale[$data->sale_id] : '';
            
            $detail     = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $data->getViewDetail());
            $detail     = str_replace(ExportList::$remove, " ", $detail);
//            $agentName  = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $data->getAgentDisplayGrid());
//            $agentName  = str_replace(ExportList::$remove, " ", $agentName);
            $agentName  = isset($aAgent[$data->agent_id]) ? $aAgent[$data->agent_id] : '';
            $status     = trim($data->getStatusText());
            $timeComplete   = $data->getTimeOverOnlyNumber();
            $timeOver       = '';
            if($timeComplete > MonitorUpdate::TIME_COMPLETE_BO_MOI){
                $timeOver       = $timeComplete - MonitorUpdate::TIME_COMPLETE_BO_MOI;
            }
            
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getDateDelivery());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->code_no);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getArrayDeliveryText());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $agentName);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $code_account);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $customer_name);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $customer_add);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $customer_type);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $detail);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $status);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getReasonFalse());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", strip_tags($data->getInfoCreatedByOnGrid()));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCreatedDate("d/m/Y H:i:s"));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $timeComplete);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $timeOver);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $saleName);
            $row++;
        }
    }
    
    public static function AppOrderListBodyCron($aData, &$objPHPExcel, &$row) {
        $aCustomerId = [];
        // 1, get list customer
        foreach($aData as $data){
            $aCustomerId[] = $data->customer_id;
        }
        $mAppCache      = new AppCache();
        $aModelCustomer = Users::getArrayModelByArrayId($aCustomerId);
        $aSale          = $mAppCache->getListdataUserByRole(ROLE_SALE);
        $aAgent         = $mAppCache->getListdataUserByRole(ROLE_AGENT);
        // 2. render excel
        foreach($aData as $data){
            $mCustomer = isset($aModelCustomer[$data->customer_id]) ? $aModelCustomer[$data->customer_id] : '';
            $code_account = $customer_name = $customer_add = $customer_type = $saleName = '';
            if(!empty($mCustomer)){
                $code_account   = $mCustomer->code_account;
                $customer_name  = $mCustomer->first_name;
                $customer_add   = $mCustomer->address;
                $customer_type  = $mCustomer->getTypeCustomerText();
            }
            $saleName       = isset($aSale[$data->sale_id]) ? $aSale[$data->sale_id] : '';
            
            $detail     = str_replace(ExportList::$replace, ExportList::$replaceNewLine, $data->getViewDetail());
            $detail     = str_replace(ExportList::$remove, " ", $detail);
            $agentName  = isset($aAgent[$data->agent_id]) ? $aAgent[$data->agent_id] : '';
            $status     = trim($data->getStatusText());
            $timeComplete   = $data->getTimeOverOnlyNumber();
            $timeOver       = '';
            if($timeComplete > MonitorUpdate::TIME_COMPLETE_BO_MOI){
                $timeOver       = $timeComplete - MonitorUpdate::TIME_COMPLETE_BO_MOI;
            }
            
            $index=1;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getDateDelivery());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->code_no);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getArrayDeliveryText());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $agentName);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $code_account);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $customer_name);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $customer_add);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $customer_type);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $detail);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $status);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getReasonFalse());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", strip_tags($data->getInfoCreatedByOnGrid()));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCreatedDate("d/m/Y H:i:s"));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $timeComplete);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $timeOver);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $saleName);
            $row++;
        }
    }
    
    /** @Author: Pham Thanh Nghia 4/7/2018
     *  @Todo: Export Excel /admin/gasSupportCustomer/index
     **/
    public function exportGasSupportCustomer(){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("PhamThanhNghia")
                                        ->setLastModifiedBy("PhamThanhNghia")
                                        ->setTitle('Ho Tro KH')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("Ho Tro KH")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1;
        $i=1;
        $cmsFormatter = new CmsFormatter();
        $aData = $_SESSION['data-excel']->data;
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Phieu giao NV'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "BÁO CÁO PHIẾU GIAO NHIỆM VỤ");
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:J$row");
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        $isTayNguyen = false;
        
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày tạo');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày cập nhật');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Trạng thái');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tên khách hàng');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Loại KH');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tiền hỗ trợ');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tiền phải thu');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Người tạo/Sale/NVKD');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Người duyệt');
            
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        
        $this->exportGasSupportCustomerBody($aData, $objPHPExcel, $row);
        
//        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
//            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(45);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(35);

        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:C".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("D$beginBorder:D".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("F$beginBorder:F".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()
                                ->getStyle("G$beginBorder:H".$row)->getNumberFormat()
                                ->setFormatCode('#,##0'); 
        
        
        $_SESSION['data-excel'] = null;
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."PhieuGiaoNhiemVu".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    public function exportGasSupportCustomerBody($aData, &$objPHPExcel, &$row) {
//        $aData = $_SESSION['data-excel'];
        $gasSupport= new GasSupportCustomer();
        // 2. render excel
        $stt = 1;
//        $aData = $aData->data;
        foreach($aData as $data){
            $approved_uid = "";
            $approved_date = "";
            
            
            $data->formatGasSupportCustomerItemName();
            $aResult   =  explode("<br>",$data->getLastUpdateStatus());
            if(!empty($aResult[0])){
                $approved_uid = $aResult[0];
            }
            if(!empty($aResult[0])){
                $approved_date = $aResult[1];
            }
            $index=1;
            
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",$stt);
//            $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row",MyFormat::dateConverYmdToDmy($data->created_date) , PHPExcel_Cell_DataType::TYPE_FORMULA); 
//            $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row", substr($approved_date,0,10), PHPExcel_Cell_DataType::TYPE_FORMULA);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", MyFormat::dateConverYmdToDmy($data->created_date));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", substr($approved_date,0,10));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getStatus());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getCustomer());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getTypeCustomer());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->sumAmountHoTro);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->sumAmountDauTu);
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $data->getUidLogin(true));
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $approved_uid);

            $row++; $stt++;
        }
    }
    
    /** @Author: Pham Thanh Nghia 2018
     *  @Todo:
     *  @Param: admin/Gas24hReport/AgentMonthly
     **/
    public function exportAgentMonthlyGas24h(){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("PhamThanhNghia")
                                ->setLastModifiedBy("PhamThanhNghia")
                                ->setTitle('Ho Tro KH')
                                ->setSubject("Office 2007 XLSX Document")
                                ->setDescription("Ho Tro KH")
                                ->setKeywords("office 2007 openxml php")
                                ->setCategory("Gas");
        $row=1;
        $i=1;
        $cmsFormatter = new CmsFormatter();
        $aData = $_SESSION['data-excel'];
        
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Sản lượng'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "BÁO CÁO SẢN LƯỢNG GIỮA TỔNG ĐÀI VÀ ĐẶT APP");
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:C$row");
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $row++;
        $index=1;
        $beginBorder = $row;
        $isTayNguyen = false;
        
        if(isset($aData)){
            $aMonth = isset($aData['aMonth']) ? $aData['aMonth'] : array();
            $maxMonth = 1;
            if(!empty($aMonth)){
                $maxMonth =  max($aMonth);
            }
        }
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại lý');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Bình quân tháng (Cuộc gọi/App)');
        
         if(count($aMonth)): 
            for($i = 1 ; $i<= $maxMonth ; $i++ ): 
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tháng '.$i);
            endfor;
        endif; 
        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        
        $this->exportAgentMonthlyGas24hBody($aData, $objPHPExcel, $row);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
        
        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:B".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle("C$beginBorder:O".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $_SESSION['data-excel'] = null;
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."BaoCaoSanLuongGiuaTongDaiVaApp".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    public function exportAgentMonthlyGas24hBody($aData, &$objPHPExcel, &$row) {
        $sellReport = new SellReport();
        $mAppCache  = new AppCache();
        $aAgent     = $mAppCache->getAgent();
        if(isset($aData)){
            $aMonth = isset($aData['aMonth']) ? $aData['aMonth'] : array();
            $aAgentId = isset($aData['aAgentId']) ? $aData['aAgentId'] : array();
            $stt = 1;
            $maxMonth = 1;
            if(!empty($aMonth)){
                $maxMonth =  max($aMonth);
            }
        }
        $index = 1;
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",'');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",'');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",'');

        for($i = 1 ; $i<= $maxMonth ; $i++ ): 
            $total = isset($aData['TOTAL'][Sell::SOURCE_WEB][$i]) ? $aData['TOTAL'][Sell::SOURCE_WEB][$i] : '0';
            $total .= "/";
            $total .= isset($aData['TOTAL'][Sell::SOURCE_APP][$i]) ? $aData['TOTAL'][Sell::SOURCE_APP][$i] : '0';
            
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",$total);
        endfor; 
        $row++;
            
        $index = 1;
        if(isset($aData)):
            foreach ($aAgentId as $agent_id) :
                    $average = isset($aData['AVERAGE'][$agent_id][Sell::SOURCE_WEB]) ? number_format($aData['AVERAGE'][$agent_id][Sell::SOURCE_WEB]/$maxMonth) : '0';
                    $average .= "/";
                    $average .= isset($aData['AVERAGE'][$agent_id][Sell::SOURCE_APP]) ? number_format($aData['AVERAGE'][$agent_id][Sell::SOURCE_APP]/$maxMonth) : '0';
                    
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",$stt);
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",isset($aAgent[$agent_id]) ? $aAgent[$agent_id]['first_name'] : '');
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",$average);
                    for( $i = 1; $i <= max($aMonth); $i++) : 
                        $monthly = isset($aData['MONTHLY'][$agent_id][$i][Sell::SOURCE_WEB]) ? $aData['MONTHLY'][$agent_id][$i][Sell::SOURCE_WEB] : '0';
                        $monthly .= "/";
                        $monthly .= isset($aData['MONTHLY'][$agent_id][$i][Sell::SOURCE_APP]) ? $aData['MONTHLY'][$agent_id][$i][Sell::SOURCE_APP] : '0'; 
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",$monthly);
                    endfor;
            $stt++; 
            $row++;
            $index = 1;

            endforeach;
        endif;
    }
    /** @Author: Pham Thanh Nghia 2-8-2018
     *  @Todo: 
     *  @Param:
     **/
    public function exportHomeContract($model){ 
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("PhamThanhNghia")
                                ->setLastModifiedBy("PhamThanhNghia")
                                ->setTitle('Ho Tro KH')
                                ->setSubject("Office 2007 XLSX Document")
                                ->setDescription("Ho Tro KH")
                                ->setKeywords("office 2007 openxml php")
                                ->setCategory("Gas");
        $row=1;
        $i=1;
        $cmsFormatter = new CmsFormatter();
        $ObjCreatedDate = new DateTime($model->created_date);
        $strDate = "Ngày ".$ObjCreatedDate->format('d')." tháng ".$ObjCreatedDate->format('m')." năm ".$ObjCreatedDate->format('Y');
        //$aData = $_SESSION['data-excel'];
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('QUYẾT TOÁN'); 
        
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", $model->getTypeText()." \n ".$strDate);
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:F$row");
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:I".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $row++;
        $index=1;
        $beginBorder = $row;
        $fakeHomeContract = new HomeContract();
        
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'STT');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tên tài khoản');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $fakeHomeContract->getAttributeLabel('pay_bankNumber'));
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $fakeHomeContract->getAttributeLabel('pay_bank'));
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tiền');
//        if ($model->type!=GasSettle::TYPE_REQUEST_MONEY_MULTI ) { }
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Nội dung');

        
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);  
        
        $row++;
        
        $this->exportHomeContractBody($model, $objPHPExcel, $row);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(50);


        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("D$beginBorder:D".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $row++;
        $objPHPExcel->getActiveSheet()
                                ->getStyle("E$beginBorder:E".$row)->getNumberFormat()
                                ->setFormatCode('#,##0');
       $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $_SESSION['data-excel'] = null;
        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'."BaoCaoSanLuongGiuaTongDaiVaApp".'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    public function exportHomeContractBody($model,$objPHPExcel, &$row){
        $recordList = $model->rHomeContractDetail;
        $fakeHomeContractDetail = new HomeContractDetail();
        $fakeHomeContract = new HomeContract();
        $current = 1;
        $index =1;
        foreach ($recordList as $record) :
            $mHomeContract = $record->rHomeContract;
            $mHomeContract->mapJsonFieldOneDecode('JSON_FIELD', 'type_pay_value', 'baseArrayJsonDecode');
            $dateEndPay = MyFormat::modifyDays($record->date_pay, $record->amount_month_pay, '+', 'month', 'd/m/Y');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $current);
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", "--- 999 ".$mHomeContract->getBeneficiary());
                $objPHPExcel->getActiveSheet()->setCellValueExplicit(MyFunctionCustom::columnName($index++)."$row", $mHomeContract->getBankNumber(), PHPExcel_Cell_DataType::TYPE_STRING);
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",$mHomeContract->getBank());
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",$record->getAmount(false));
//            if ($model->type!=GasSettle::TYPE_REQUEST_MONEY_MULTI ) :
//            endif;
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row",$record->getNote());

            $row++;
            $index =1;
            $current++;
        endforeach;
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row","TỔNG CỘNG");
        $index = $index +3;
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $model->amount);

    }
    
}
?>