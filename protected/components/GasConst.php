<?php
/** Class for define const of system */
class GasConst
{
    const PHONE_BOMOI   = '1900 1521';
    const PHONE_HGD     = '1900 1565';
    const UID_ADMIN             = 2;
    const UID_ADMIN_NAM         = 1979725;
    const UID_ADMIN_DUONG       = 1979734;
    const STATUS_YES    = 1;
    const STATUS_NO     = 0;
    const VALUE_YES     = 1;
    const VALUE_NO      = 2;
    const DEFAULT_PASS  = 'Qwe456789';
    const PASS_TYPE_1   = 1;// Now0317 type same username for customer
    const CODE_CHAR     = 'ABCDEFGHIJKLMNPQRSTUVWXYZ123456789';
    const HGD_TRA_THE   = 1211571;
    
    const ACTION_LIST       = 1;
    const ACTION_ADD        = 2;
    const ACTION_UPDATE     = 3;
    const ACTION_VIEW       = 4;
    
    public static function getIdHgdNotUpdate(){
        return [GasConst::HGD_TRA_THE];
    } 
    public static function getListdataYesNo(){
        return [GasConst::VALUE_YES => 'Có', GasConst::VALUE_NO => 'Không'];
    } 
    public static function getAdminArray(){
        return [
            GasConst::UID_ADMIN             => GasConst::UID_ADMIN,
            GasConst::UID_ADMIN_NAM         => GasConst::UID_ADMIN_NAM,
            GasConst::UID_ADMIN_DUONG       => GasConst::UID_ADMIN_DUONG,
        ];
    }
    public static $arrAdminUserReal = [// Jul1819 define user real
            GasLeave::UID_DUONG_NV          => GasLeave::UID_DUONG_NV,
            GasLeave::UID_NAM_NH            => GasLeave::UID_NAM_NH,
            GasLeave::UID_DUNG_NT           => GasLeave::UID_DUNG_NT,
    ];
    
    /*  Jul 31, 2016 for ROLE xe tải sử dụng chung cột is_maintain của KH Bình Bò
     * define('STORE_CARD_KH_BINH_BO', 1); // khách hàng bình bò là cột is_maintain trong table user
     */
    const CAR_750KG = 1;
    const CAR_1T    = 2;
    const CAR_1T2   = 3;
    const CAR_1T8   = 4;
    const CAR_2T    = 5;
    const CAR_5T    = 6;
    const CAR_15T   = 7;
    const CAR_2T4   = 8;
    const CAR_3T5   = 9;
    public static $ARR_CAR_WEIGHT = array(
        GasConst::CAR_750KG => '750 KG',
        GasConst::CAR_1T    => '1 Tấn',
        GasConst::CAR_1T2   => '1 Tấn 2',
        GasConst::CAR_1T8   => '1 Tấn 8',
        GasConst::CAR_2T    => '2 Tấn',
        GasConst::CAR_2T4   => '2 Tấn 4',
        GasConst::CAR_3T5   => '3 Tấn 5',
        GasConst::CAR_5T    => '5 Tấn',
        GasConst::CAR_15T   => '15 Tấn',
    );
    public static function getCarWeight($v) {
        return isset(GasConst::$ARR_CAR_WEIGHT[$v]) ? GasConst::$ARR_CAR_WEIGHT[$v] : '';
    }
    public static function getInvestHgd() {
        return array(// Sep 23, 2016
            199   => 'Cổ dê',
            114   => 'Dây đen',
            395   => 'Dây đánh lửa CN',
            591   => 'Búa đánh lửa',
            597   => 'Nút vặn bếp',
            592   => 'Cục Sứ',
        );
    }
    public static function getInvestHgdText($sId) {
        $sId = trim($sId, ',');
        if(empty($sId)){
            return '';
        }
        $masterMaterial = GasConst::getInvestHgd();
        $aId = explode(',', $sId);
        $res = array();
        foreach($aId as $id){
            $res[] = isset($masterMaterial[$id]) ? $masterMaterial[$id] : '';
        }
        return implode(', ', $res);
    }
    
    const UID_LUYCH         = 22884;// Lại Như Luých
    const UID_NHUNG_TTT     = 485407;// Trần Thị Tuyết Nhung
    const UID_AGENT_CH1     = 1311;// Cửa hàng 1
    
    public static function getPageSize() {
        return array(
            10=>10,
            20=>20,
            50=>50,
            100=>100,
            200=>200,
        );
    }
    
    const UID_SANG_TH   = 818113; // NV Kỹ Thuật Trần Hải Sang - Aug 24, 2016
    const UID_VANG_LAI  = 874884; // HGD Vãng Lai
    const UID_ANH_LD    = 868849; // Lê Đức Anh
    const UID_SON_HV    = 330021; // Hoàng Vũ Sơn
    const UID_CANH_NV   = 193; // Nguyễn Văn Cảnh
    const UID_TUYET_LT  = 863098; // Lê Thị Tuyết
    const UID_THUY_HT   = 260420; // Nv kế toán - Huỳnh Thanh Thùy
    const UID_PHUOC_HT  = 874503; // Nv kế toán - Huỳnh Thanh Phước
    const UID_NHAN_NTT  = 131156; // Nv kế toán - Nguyễn Thị Thanh Nhàn
    const UID_LY_HT     = 117094; // Nv kế toán KV - Hồ Thị Lý
    const UID_QUY_PT    = 136815; // Nv kế toán KV - Phạm Thị Quý
    const UID_HAN_TH    = 705615; // Nv kế toán KV - Trần Hoài Hận
    const UID_THIEN_DT  = 726576; // Nv kế toán KV - Đặng Thị Thiện	
    
    const UID_TU_Y      = 281997; // Y Tú
    const UID_QUY_PQ    = 137996; // Phạm Quốc Quy
    const UID_TIN_TB    = 217787; // Trần Bá Tín
    const UID_HA_VT     = 866992; // Vương Thanh Hà
    const UID_DUNG_NT   = 862506; // Nguyễn Tiến Dũng - Trưởng Phòng Công Nghệ
    const UID_NGAN_LT   = 740837; // Lại Thu Ngân
    const UID_DIEUXE_KHO_PHUOCTAN   = 107111; // Điều Xe Kho Phước Tân
    const UID_DIEUXE_KHO_BENCAT     = 109107; // Điều Xe Kho Bến Cát
    const UID_DIEUXE_BINH_DINH      = 924591; // Điều Xe Trạm Bình Định
    const UID_DIEUXE_VINH_LONG      = 114259; // Điều Xe Kho Vĩnh long
    const UID_DIEUXE_CHO_MOI        = 863150; // Điều Xe Kho Chợ Mới
    const UID_DIEUXE_GIA_LAI        = 1199406; // Điều Xe Gia Lai
    const UID_DIEUXE_VUNG_TAU       = 1536340; // Điều Xe Kho Vũng Tàu
    const UID_DIEUXE_BAC_LIEU       = 1991451; // Apr1319Điều Xe Trạm Bạc Liêu
    // Apr1319 khi tạo điều xe nhớ set luôn biến $DIEUXE_MAP_TO_AGENT ở cùng Class GasConst
    
    const UID_HA_NTT    = 852294; //Nguyễn Thị Thu Hà
    const UID_DANG_TH   = 766823; // Trần Hải Đăng
    const UID_HIEP_TV   = 289272; // Thân Văn Hiệp
    const UID_HANH_NT   = 163598; // Lễ Tân - Nguyễn Thị Hạnh
    const UID_HUNG_NT   = 705612; // Nguyễn Thế Hùng
    const UID_PHONG_HT  = 874177; // Huỳnh Thanh Phong
    const UID_PHUOC_ND  = 874243; // Nguyễn Đức Phước
    const UID_THAO_LT   = 27200; // NV Kế Toán - Lê Thị Thảo
    const UID_PHUONG_TT = 948495; // NV Điều Phối - Trần Thị Phượng
    const UID_LIEU_CTM  =  257308; // NV KTKV- Châu Thị Mỹ Liễu
    const UID_TRUONG_NN =  435846; // Giám Sát Đại Lý -Nguyễn Nhật Trường
    const UID_DOT_NV    =  217782; // Giám Sát Đại Lý -Nguyễn Văn Đợt
    const UID_THAM_LT   =  973068; // NV Kế Toán - Lê Thị Thẩm
    const UID_HUE_LT    =  874074; // NV Kế Toán - Lê Thị Huệ
    const UID_VINH_NH   = 710310; // Kế Toán- VLong - Nguyễn Hoàng Vinh - FEB 03, 2017
    const UID_NHI_DT    = 623193; // NV Kế Toán VP - Bình Định
    const UID_NGUYET_PT = 710307; // Phạm Thị Nguyệt - NV Call Center
    const UID_KH_HO_GD  = 48694; // KH trả tiền ngay - Hộ gia đình
    const UID_NGUYEN_DTM    = 850523; // Đặng Thị Mỹ Nguyên - NV Kế Toán VP
    const UID_NGAN_DTM      = 153; // Đặng Thị Mỹ Ngân - NV Kế Toán VP
    const UID_SU_BH         = 49175; // Bùi Hữu Sự - Nhân Viên Kinh Doanh
    const UID_NGOC_PT       = 160; // Phạm Thị Ngọc - Call Center
    const UID_TRUNG_HV      = 766819; // Hà Văn Trung - Chuyên Viên CCS
    const UID_HA_PT         = 936982; // Phạm Thị Hà - Call Center
    const UID_ANH_HTN       = 129122; // Hoàng Thị Ngọc Ánh - User Đại Lý
    const UID_KIEN_NT       = 136962; // Nguyễn Trung Kiên - NV Phục Vụ Khách Hàng
    const UID_HUY_VT        = 863284; // Võ Tấn Huy - Chuyên Viên CCS
    const UID_BINH_TQ       = 217786; // Trần Quang Bình - NV Thu Nợ
    const UID_VAN_TV        = 146; // Trần Thị Vân - Điều Phối
    const UID_DUYEN_NT      = 516154; // Nguyễn Thị Duyên - NV Kế Toán VP - Ben Cat
    const UID_PHUONG_NT     = 281994; // Nguyễn Thị Phương - Điều Phối Ninh Bình
    const UID_TUYET_LT_CALL = 806515; // Lê Thị Tuyết - 806515
    const UID_DAT_LX        = 868857; // Lê Xuân Đạt - Bien Dong VT
    const UID_THANG_LX      = 1149929; // Lê Xuân Thắng - Bien Dong VT
    const UID_DONG_NV       = 908108; // Nguyễn Văn Đông - NV Bảo Trì
    const UID_VINH_VT       = 49173; // Vũ Thái Vinh - Sale Admin
    const UID_LAM_KD        = 257122; // Kiều Duy Lâm
    const UID_TRAM_PTN      = 1183921; // Phạm Thị Ngọc Trâm
    const UID_TINH_P        = 4221; // Phan Tịnh
    const UID_CHAU_LNM      = 867768; // Lê Nguyễn Mỹ Châu
    const UID_THUY_TT       = 1162981; // Trần Thi Thúy - BDinh
    const UID_VEN_NTB       = 1014257;
    const UID_THUAN_NH      = 599916; // Nguyễn Hòa Thuận
    const UID_PHUONG_ND     = 863038;// Nguyễn Đình Phương
    const UID_LONG_PN       = 795790;// Phạm Ngọc Long
    const UID_LOC_PV        = 888989;// Phan văn lộc
    const UID_NHAT_NT       = 1298168;// Nguyễn Tấn Nhật
    const UID_NGAN_TNP      = 1304439;// Trần Nguyễn Phát Ngân
    const UID_LOI_TV        = 1521059;// Trần Văn Lợi
    const UID_VY_NTT        = 951689;// Nguyễn Thị Tường Vy
    const UID_LINH_TTV      = 360792;// Tống Thành Vũ Linh
    const UID_TUAN_DH       = 1207466;// Đặng Hoàng Tuấn
    const UID_PHONG_NU      = 866210;// Nguyễn Uy Phong
    const UID_ANH_VH        = 1110535;// Võ Hoàng Anh
    const UID_HUY_DD        = 516143;// Đào Đức Huy
    const UID_AN_LCT        = 1615009;// Lê Châu Thuận An
    const UID_BINH_TV       = 865752;// Trần Văn Bình
    const UID_CHIEN_BQ      = 485406; // NV Kế Toán Bùi Quyết Chiến
    const UID_HIEU_NT       = 257116; // Nguyễn Trung Hiếu PTTT
    const UID_DAT_NV        = 740841; // Nguyễn Văn Đạt
    const UID_HIEU_PT       = 1430221; // Phan Thiện Hiếu
    const UID_HUNG_NM       = 1737318; // Nguyễn Minh Hùng
    const UID_TAM_NM        = 1574870; // Ngô Minh Tâm
    const UID_LAM_KS        = 2085496; // Kiều Sơn Lâm
    const UID_KIEN_DNT      = 2085434; // Đào Nguyễn Thành Kiên
    const UID_QUYEN_PTL     = 627183; // Phan Thị Lệ Quyên
    const UID_KIEN_DT       = 1392015; // Dương Trung Kiên
    const UID_THIEN_NM      = 1246418; // Nguyễn Minh Thiện
    const UID_TROC_VV       = 1832440; // Võ Văn Trọc
    const UID_DUC_NT        = 1044912; // Nguyễn Tấn Đức - Vinh Long

    
    /**** Sep1517 UId Chuyên Viên CCS Agent Khoan *****/
    const UID_HOAN_NH       = 976187; // Nguyễn Huy Hoàn - Chuyên Viên CCS
    const UID_EM_MP         = 863107; // Mạch Phương Em - Chuyên Viên CCS
    const UID_MANH_TV       = 866953; // Trần Văn Mạnh - NV Phục Vụ Khách Hàng Cửa hàng Chư Sê
    const UID_THANH_NC      = 137689; // Nguyễn Chí Thanh - NV Phục Vụ Khách Hàng
    const UID_SUOL_LV       = 260491; // Le Văn Suol - NV Phục Vụ Khách Hàng
    const UID_SANG_NT       = 652701; // Nguyễn Thanh Sang - NV Phục Vụ Khách Hàng
    const UID_LIEM_LT       = 893613; // Lê Thanh Liêm
    const UID_ANH_LQ        = 1373422; // Lê Quốc Anh
    const UID_MINH_NT       = 952191; // Nguyễn Thị Minh
    const UID_THUY_NTT      = 1242592; // Nguyễn Thị Thanh Thủy - Binh Dinh
    const UID_HUYEN_CT      = 461767; // Cao Thị Huyền - Binh Dinh
    const UID_DEMO_PVKH     = 1042346; // Demo Dũng Test App

    
    const UID_AGENT_XE_TAI_1        = 1429436; // HCM Xe Tải 1
    const UID_AGENT_XE_TAI_2        = 1690641; // HCM Xe Tải 2
    const UID_AGENT_XE_TAI_3        = 1711692; // HCM Xe Tải 3
    const UID_AGENT_BINH_THANH_1    = 106; // Đại lý Bình Thạnh 1
    const UID_AGENT_BINH_THANH_2    = 658920; // Đại lý Bình Thạnh 2

    public static $DIEUXE_MAP_TO_AGENT = array(// Oct 07, 2016 điều xe map với kho giao tương ứng
        GasConst::UID_DIEUXE_KHO_PHUOCTAN   => MyFormat::KHO_PHUOC_TAN,
        GasConst::UID_DIEUXE_KHO_BENCAT     => MyFormat::KHO_BEN_CAT,
        GasConst::UID_DIEUXE_BINH_DINH      => MyFormat::DAU_KHI_BINH_DINH,
        GasConst::UID_DIEUXE_CHO_MOI        => MyFormat::KHO_CHO_MOI,
        GasConst::UID_DIEUXE_VINH_LONG      => MyFormat::KHO_VINH_LONG,
        GasConst::UID_DIEUXE_GIA_LAI        => MyFormat::KHO_LY_CHINH_THANG,
        GasConst::UID_DIEUXE_VUNG_TAU       => GasCheck::KHO_VUNG_TAU,
        GasConst::UID_DIEUXE_BAC_LIEU       => GasCheck::KHO_TRAM_BAC_LIEU,
    );

    public static $ARR_VIEW_SETTLE_MDXT = array(// Aug 26, 2016 Kế toán view phiếu của chị Thủy
        GasLeave::PHUONG_PTK,
        GasLeave::DUNG_NTT,
        GasConst::UID_ANH_LD,
        GasConst::UID_SON_HV,
        GasConst::UID_NGAN_LT,
    );
    public static $ARR_VIEW_SETTLE_MIEN_DONG = array(// Oct 05, 2016 Kế toán khu vuc view quyet toan KV Dong Nai, Binh Duong
        GasConst::UID_LY_HT,
        GasConst::UID_QUY_PT,
        GasConst::UID_HAN_TH,
        GasConst::UID_THIEN_DT,
    );
    
    // Aug 29, 2016 for define Agent id
    const AGENT_CH3 = 1313;
    const AGENT_BINH_THANH_3    = 768408;
    const AGENT_TAN_BINH_1      = 235;
    const AGENT_TAN_BINH_2      = 27740;
    const AGENT_HOC_MON         = 108;
    const AGENT_QUAN_10         = 242369;
    const AGENT_QUAN_6          = 211393;
    const AGENT_BINH_TAN        = 122;
    const AGENT_BINH_THANH_1    = 106;
    const AGENT_QUAN_2          = 100;
    const AGENT_QUAN_4          = 101;
    const AGENT_DKMN            = 768411;
    public static $AGENT_NOT_HGD = array(// Sep 08, 2016 list agent không tính sản lượng HỘ chỉ tính mối cho CV
        GasConst::AGENT_BINH_THANH_3,
        GasConst::AGENT_TAN_BINH_1,
        GasConst::AGENT_TAN_BINH_2,
        GasConst::AGENT_HOC_MON,
        GasConst::AGENT_QUAN_10,
        GasConst::AGENT_QUAN_6,
        GasConst::AGENT_BINH_TAN,
    );
    // Aug 29, 2016 for define Agent id
    
    const INVEST_YES    = 1;// SEP 25, 2016 FOR SEARCH vip240
    const INVEST_NO     = 2;
    public static $ARR_INVERS_HGD = array(
        GasConst::INVEST_YES    => 'Có đầu tư',
        GasConst::INVEST_NO     => 'Không có đầu tư',
    );
    
    public static function getBossAgent() {
        $aModelUser = GasOneManyBig::getArrayModelUser(GasOneManyBig::TYPE_AGENT_BOSS, GasOneManyBig::TYPE_AGENT_BOSS);
        $aRes = array();
        foreach ($aModelUser as $item) {
            $aRes[$item->id] = $item->getNameWithRole();
        }
        return $aRes;
        
        
        return array(// Oct 25, 2016 Chủ Cơ Sở
            GasConst::UID_HIEP_TV               => 'Thân Văn Hiệp',
            GasLeave::UID_DIRECTOR_BUSSINESS    => 'Bùi Đức Hoàn',
            GasConst::UID_HANH_NT               => 'Nguyễn Thị Hạnh',
            GasLeave::UID_HEAD_OF_LEGAL         => 'Nguyễn Ngọc Kiên',
            GasLeave::UID_DIRECTOR              => 'Vũ Thái Long',
            GasConst::UID_HUNG_NT               => 'Nguyễn Thế Hùng',
            GasConst::UID_NHAN_NTT              => 'Nguyễn Thị Thanh Nhàn',
            GasLeave::UID_MANAGER_BUSSINESS     => 'Nguyễn Thanh Hải',
            GasLeave::DUNG_NTT                  => 'Nguyễn Thị Thùy Dung',
            GasLeave::UID_HEAD_GAS_BO           => 'Phạm Văn Đức',
            GasLeave::UID_DUNG_NT               => 'Nguyễn Tiến Dũng',
        );
    }
    
    const SELL_COMPLETE     = 1;
    const SELL_HOI_GIA      = 2;
    const SELL_PHAN_NAN     = 3;
    const SELL_KHONG_CO_QUA = 4;
    const SELL_GIA_CAO      = 5;
    const SELL_OTHER        = 6;
    const SELL_INTERNAL     = 7;
    const SELL_NOT_SUCCESS  = 8;
    const SELL_MISS_CALL    = 9;
    const SELL_HOI_GAS          = 10;
    const SELL_CHUYEN_DAI_LY    = 11;
    const SELL_NHAM_SO          = 12;
    const SELL_CHANGE_ADDRESS   = 13;
    const SELL_NETWORK_SLOW     = 14;
    const SELL_NOT_RATE         = 15;
    const SELL_UPHOLD           = 16;
    
    const SELL_KHM              = 17;
    const SELL_CHANGE_GIFT      = 18;
    const SELL_MAKE_ORDER       = 19;
    const SELL_USE_BRAND_OTHER  = 20;
    const SELL_CANCEL_ORDER     = 21;
    const SELL_AUDIT_TEST       = 22;
    const SELL_CALL_APP         = 23;
    const SELL_CUSTOMER_REJECT  = 24;
    const SELL_CALL_TELESALE    = 25;
    const SELL_FIND_INFO            = 26;
    const SELL_ADVISORY_PTTT        = 27;
    const SELL_CANNOT_GET_ADDRESS   = 28;
    const SELL_CHEAT_COMPLETE   = 29;// hoàn thành trc
    
    public static function getSellReason($getComplete = false) {// các trạng thái của cuộc gọi KH
        if($getComplete){// sử dụng cho search
            return array(
//                GasConst::SELL_COMPLETE     => 'Thành công',
                GasConst::SELL_HOI_GIA      => 'Hỏi giá',
                GasConst::SELL_PHAN_NAN     => 'Phàn nàn',
//                GasConst::SELL_KHONG_CO_QUA => 'Không có quà',
//                GasConst::SELL_GIA_CAO      => 'Giá cao',
                GasConst::SELL_INTERNAL     => 'KH ngoài hệ thống',
                
                GasConst::SELL_UPHOLD               => 'Bảo trì',
                GasConst::SELL_HOI_GAS              => 'Hối gas',
//                GasConst::SELL_CHUYEN_DAI_LY        => 'Chuyển ĐL khác giao',
//                GasConst::SELL_MISS_CALL            => 'Miss call',
//                GasConst::SELL_NHAM_SO              => 'Nhầm số',
//                GasConst::SELL_CHANGE_ADDRESS       => 'KH báo lại địa chỉ',
                GasConst::SELL_NETWORK_SLOW         => 'Đường mạng kém',
//                GasConst::SELL_KHM                  => 'KHM',
//                GasConst::SELL_CHANGE_GIFT          => 'Đổi quà',
                GasConst::SELL_MAKE_ORDER           => 'KH lấy gas',
                GasConst::SELL_CANCEL_ORDER         => 'KH hủy gas',
//                GasConst::SELL_USE_BRAND_OTHER      => 'KH sử dụng thương hiệu khác',
//                GasConst::SELL_OTHER              => 'Khác',// Jun 05, 2017 close
//                GasConst::SELL_NOT_SUCCESS          => 'Cuộc gọi không thành công',
                GasConst::SELL_NOT_RATE             => 'Cuộc gọi chưa phân loại',
//                GasConst::SELL_AUDIT_TEST           => 'Audit test',
//                GasConst::SELL_CALL_APP             => 'Hỏi App Gas24h',
//                GasConst::SELL_CUSTOMER_REJECT      => 'Không thuyết phục được KH',
                GasConst::SELL_CALL_TELESALE        => 'KH Telesale',
                GasConst::SELL_FIND_INFO            => 'Tìm hiểu và Cung cấp TT',
                GasConst::SELL_ADVISORY_PTTT        => 'Tư vấn PTTT',
                GasConst::SELL_CANNOT_GET_ADDRESS   => 'Không xin được địa chỉ',
                GasConst::SELL_CHEAT_COMPLETE       => 'Hoàn thành trước',
            );
        }
        return array(
//            GasConst::SELL_COMPLETE     => 'Thành công', 
            GasConst::SELL_HOI_GIA      => 'Hỏi giá',
            GasConst::SELL_PHAN_NAN     => 'Phàn nàn',
//            GasConst::SELL_KHONG_CO_QUA => 'Không có quà',
//            GasConst::SELL_GIA_CAO      => 'Giá cao',
            GasConst::SELL_INTERNAL     => 'KH ngoài hệ thống',
            GasConst::SELL_UPHOLD       => 'Bảo trì',
            GasConst::SELL_HOI_GAS      => 'Hối gas',
//            GasConst::SELL_CHUYEN_DAI_LY  => 'Chuyển ĐL khác giao',
//            GasConst::SELL_MISS_CALL    => 'Miss call',
//            GasConst::SELL_NHAM_SO      => 'Nhầm số',
//            GasConst::SELL_CHANGE_ADDRESS       => 'KH báo lại địa chỉ',
            GasConst::SELL_NETWORK_SLOW         => 'Đường mạng kém',
//            GasConst::SELL_KHM                  => 'KHM',
//            GasConst::SELL_CHANGE_GIFT          => 'Đổi quà',
            GasConst::SELL_MAKE_ORDER           => 'KH lấy gas',
            GasConst::SELL_CANCEL_ORDER         => 'KH hủy gas',
//            GasConst::SELL_CALL_APP             => 'Hỏi App Gas24h',
//            GasConst::SELL_CUSTOMER_REJECT      => 'Không thuyết phục được KH',
            GasConst::SELL_CALL_TELESALE        => 'KH Telesale',
//            GasConst::SELL_USE_BRAND_OTHER      => 'KH sử dụng thương hiệu khác',
//            GasConst::SELL_OTHER        => 'Khác',// Jun 05, 2017 close
            GasConst::SELL_FIND_INFO            => 'Tìm hiểu và Cung cấp TT',
            GasConst::SELL_ADVISORY_PTTT        => 'Tư vấn PTTT',
            GasConst::SELL_CANNOT_GET_ADDRESS   => 'Không xin được địa chỉ',
            GasConst::SELL_CHEAT_COMPLETE       => 'Hoàn thành trước',
        );
    }
    
    const COMPANY_BIEN_DONG     = 868629;
    const COMPANY_DKMN          = 868626;
    const COMPANY_HUONGMINH     = 868624;
    const COMPANY_KHL_ANH_DUONG = 868630;
    const COMPANY_DK_BINH_DINH  = 868632;
    const COMPANY_KIEN_LONG     = 868625;
    
    /**
     * @Author: ANH DUNG Jan 08, 2017
     * @Todo: get material type for setup Thương Hiệu bán của KH
     */
    public static function getMaterialsTypeGasOfCustomer() {
        return array(
            MATERIAL_TYPE_BINHBO_50     => 'Bình 50',
            MATERIAL_TYPE_BINHBO_45     => 'Bình 45',
            MATERIAL_TYPE_BINH_12       => 'Bình 12',
            MATERIAL_TYPE_BINH_6        => 'Bình 6',
        );
    }
    public static function getMaterialsTypeVoOfCustomer() {
        return array(
            MATERIAL_TYPE_BINHBO_50     => MATERIAL_TYPE_VO_50,
            MATERIAL_TYPE_BINHBO_45     => MATERIAL_TYPE_VO_45,
            MATERIAL_TYPE_BINH_12       => MATERIAL_TYPE_VO_12,
            MATERIAL_TYPE_BINH_6        => MATERIAL_TYPE_VO_6,
        );
    }
    
    public static function getArrRoleLikeSale() {
        $mUser = new Users();
        return $mUser->ARR_ROLE_SALE_LIMIT;// Fix Dec2618 
//        return [ROLE_SALE, ROLE_EMPLOYEE_MARKET_DEVELOPMENT,
//            ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_EMPLOYEE_MAINTAIN];
    }
    public static function getRoleSaleOfCustomer() {
        return [ROLE_SALE, ROLE_MONITORING_MARKET_DEVELOPMENT];
    }
    // May 07, 2017 for callcenter update fix data system  
    public static function getArrUidUpdateAll() {// uid cap quyen sửa toàn bộ hệ thống
        return [GasConst::UID_NGUYEN_DTM, GasConst::UID_VEN_NTB, GasConst::UID_ANH_LQ, GasConst::UID_HIEU_PT, GasConst::UID_NGOC_PT, GasConst::UID_PHUONG_NT, GasLeave::PHUONG_PTK];
//        return [GasConst::UID_NGOC_PT, GasConst::UID_PHUONG_NT, GasLeave::PHUONG_PTK];
    }
    public static function canUpdateAll($created_date) {// uid cap quyen sửa toàn bộ hệ thống
        $dayAllow = date('Y-m-d');// chỉ cho phép nhân viên CallCenter cập nhật trong ngày
        $dayAllow = MyFormat::modifyDays($dayAllow, Yii::app()->params['DaysUpdateFixAll'], '-');
        return MyFormat::compareTwoDate($created_date, $dayAllow);
    }
    // May 07, 2017 for callcenter update fix data system 
    public static function getSimplePassword() {
        return ['Qwe456789','123456','111111'];
    }
    public static function canUpdateLocation() {
        $cUid = MyFormat::getCurrentUid();
        $aAllow = [GasConst::UID_NGOC_PT, GasConst::UID_PHUONG_NT, GasConst::UID_ADMIN];
        return in_array($cUid, $aAllow);
    }
    
    // Feb 24, 2017 mã lỗi
    const E2247 = 2247;
    
    // Mar 02, 2017 loại search autocomplete app
    const SEARCH_AGENT      = 1;
    const SEARCH_CUSTOMER   = 2;
    const APP_LIST_NEW          = 1;
    const APP_LIST_COMPLETE     = 2;
    
    public static function getRoleViewUsername(){
        return [
            ROLE_ADMIN,
            ROLE_SALE,
            ROLE_HEAD_GAS_BO,
            ROLE_HEAD_GAS_MOI,
            ROLE_DIRECTOR_BUSSINESS,
            ROLE_SALE_ADMIN,
            ROLE_MONITORING_MARKET_DEVELOPMENT,
            ROLE_MONITOR_AGENT,
            ROLE_E_MAINTAIN,
            ROLE_DIEU_PHOI,
            ROLE_TELESALE,
        ];
    }
    public static function getRoleChangeCustomerSystem(){
        return [
            ROLE_ADMIN,
            ROLE_HEAD_GAS_BO,
            ROLE_SALE_ADMIN,
            ROLE_DIRECTOR_BUSSINESS,
            ROLE_SALE,
        ];
    }
    
    const CREDIT1      = 1;// hệ số ký quỹ 30tr
    const CREDIT2      = 2;// 60tr
    const CREDIT3      = 3;// 90tr
    
    const PARTNER_1_RAC         = 10;// partner Rác
    const PARTNER_2_GA0         = 11;// partner gạo
    const PARTNER_3_GRAB        = 12;// partner grab bike
    
    public static function getArrCredit(){
        return [
            GasConst::CREDIT1 => 'Ký 30 triệu',
            GasConst::CREDIT2 => 'Ký 60 triệu',
            GasConst::CREDIT3 => 'Ký 90 triệu',
        ];
    }
    public static function getArrPartner(){
        return [
            GasConst::PARTNER_1_RAC     => 'Partner rác',
            GasConst::PARTNER_2_GA0     => 'Partner gạo',
            GasConst::PARTNER_3_GRAB    => 'Partner grab bike',
        ];
    }
    public static function getArrCreditMapPay(){
        return [
            GasConst::CREDIT1 => 12000,
            GasConst::CREDIT2 => 13000,
            GasConst::CREDIT3 => 14000,
        ];
    }
    
    const FAMILY_WIFE       = 1;// loại người thân của nhân viên
    const FAMILY_HUSBAND    = 2;
    const FAMILY_BROTHER    = 3;
    const FAMILY_PARENT     = 4;
    const FAMILY_SON        = 5;
    public static function getArrFamily(){
        return [
            GasConst::FAMILY_WIFE       => 'Vợ',
            GasConst::FAMILY_HUSBAND    => 'Chồng',
            GasConst::FAMILY_BROTHER    => 'Anh/Em',
            GasConst::FAMILY_PARENT     => 'Cha mẹ',
            GasConst::FAMILY_SON        => 'Con',
        ];
    }
    
    public static function getArrUidZoneManager(){
        return [
//            GasConst::UID_THUY_HT,
//            GasLeave::UID_VIEW_ALL_TAY_NGUYEN_GIA_LAI,
//            GasConst::UID_NHI_DT,
            GasLeave::PHUC_HV,
//            GasConst::UID_NGAN_LT,
        ];
    }

    /** @Author: DungNT 2019
     *  @Todo: các đại lý mà GS, chuyên viên đc bấm giảm giá
     **/
    public static function getUidMonitorKhoan() {
        $aRes = [
            // agent_id => user_id
            1315    => 1042346,// Cửa hàng 5 - Demo Giao Nhận
//            268835 => GasConst::UID_MANH_TV,// Cửa hàng Chư Sê
            
            30751   => GasConst::UID_TRUONG_NN, // Cửa hàng Vĩnh Long 1
            197436  => GasConst::UID_TRUONG_NN,// Cửa Hàng Phú Thuận
            125798  => GasConst::UID_TRUONG_NN,//  	Đại Lý Trạm Vĩnh Long 
            148404  => GasConst::UID_TRUONG_NN,// Cửa hàng Vũng Liêm
            652695  => GasConst::UID_TRUONG_NN,// Cửa Hàng Bình Minh 
            171449  => GasConst::UID_EM_MP,// Cửa hàng Trà Vinh
            1525846 => GasConst::UID_THIEN_NM,// Đại Lý Sơn Đông - Bến Tre
            1210050 => GasConst::UID_THIEN_NM,// Đại Lý Tp Bến Tre
            
            1966690 => GasConst::UID_EM_MP,// Đại Lý Mỹ Tho 1 May1019
            1600402 => GasConst::UID_THIEN_NM,//  	Đại Lý Chợ Lách - Bến Tre -
            
            30753 => GasConst::UID_LINH_TTV,// Cửa hàng Cần Thơ 1
            138544 => GasConst::UID_LINH_TTV,// Cửa hàng Cần Thơ 2
            262526 => GasConst::UID_LINH_TTV,// Cửa Hàng Ô Môn
            1317820 => GasConst::UID_LINH_TTV,// Đại Lý Trà Nóc - Cần Thơ 
            
            1048571 => GasConst::UID_TROC_VV,// Đại Lý Cà Mau Petro - Phường 8 
            1161320 => GasConst::UID_TROC_VV,// Đại Lý Cà Mau Petro - Phường 9
            1079730 => GasConst::UID_ANH_VH,// Đại Lý Bạc Liêu 
            1120439 => GasConst::UID_ANH_VH,// Đại Lý Sóc Trăng

            475830  => GasConst::UID_DUC_NT,// Cửa Hàng Long Xuyên 1
            596026  => GasConst::UID_DUC_NT,// Cửa Hàng Long Xuyên 2
            1176354 => GasConst::UID_DUC_NT,// Đại Lý Châu Thành - An Giang
            1317794 => GasConst::UID_PHONG_NU,// Đại Lý Núi Sam - An Giang
            1317767 => GasConst::UID_PHONG_NU,// Đại Lý Châu Đốc - An Giang 
            1214444 => GasConst::UID_PHONG_NU,// Đại Lý Châu Phú - An Giang
            898894  => GasConst::UID_TRUNG_HV,// Cửa Hàng Gas Minh Hạnh 
            863146  => GasConst::UID_TRUNG_HV,// Cửa Hàng Chợ Mới 2
            970958  => GasConst::UID_TRUNG_HV,// Đại Lý Mỹ Luông
            1317841 => GasConst::UID_TRUNG_HV,// Đại Lý Cao Lãnh - Đồng Tháp
            
            1486944 => GasConst::UID_AN_LCT,// ĐLLK - Đại Lý Thanh Minh - Long An 
            2068429 => GasConst::UID_LIEM_LT,// Đại Lý Long Mỹ - Hậu Giang

        ];
        foreach(UsersExtend::getAgentAnhHieu() as $agent_id){
            $aRes[$agent_id] = GasLeave::UID_CHIEF_MONITOR;
        }
        foreach(UsersExtend::getAgentGsPhuc() as $agent_id){
            $aRes[$agent_id] = GasLeave::PHUC_HV;
        }
        return $aRes;
    }
    public static function getAgentKhoanAllowUpdate() {
        $aRes = [
            693615, // Kho Chợ Mới
            863146,// Chợ Mới 2
            898894,// Minh Hạnh
            971227,// Mỹ An
            970958,// Mỹ Luông
            
            946670,// Long Hồ
            125798,// Trạm Vĩnh Long
            652695,// Bình Minh
        ];
        return $aRes;
    }
    public static function getUidKhoanOnlyHighPrice() {
        $aRes = [
            GasLeave::UID_CHIEF_MONITOR
        ];
        return $aRes;
    }
    /** @Todo: ApiController role Allow view some function on app **/
    public static function getRoleAppReport() {
        return [ROLE_ACCOUNTING, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_ACCOUNTING_ZONE, ROLE_HEAD_GAS_BO, ROLE_SECURITY, ROLE_SALE, ROLE_CHIEF_MONITOR, ROLE_CRAFT_WAREHOUSE, ROLE_EMPLOYEE_MAINTAIN, ROLE_DRIVER, ROLE_DEBT_COLLECTION, ROLE_EMPLOYEE_MARKET_DEVELOPMENT];
    }
    /** @Todo: ApiController role get agen_id để xác định đại lý của NV trên app **/
    public static function getRoleLikeDriver() {
        return [ROLE_ACCOUNTING, ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_ACCOUNTING_ZONE, ROLE_HEAD_GAS_BO, ROLE_SECURITY, ROLE_SALE, ROLE_DRIVER, ROLE_DEBT_COLLECTION, ROLE_CRAFT_WAREHOUSE];
    }
    public static function getUidAlertOrderGas24h() {
        $aRes = [
            981743, // Huỳnh Gia Huy
            893613,// Lê Thanh Liêm
            852407,// Huỳnh Thanh Tuấn
            471328,// Trần Văn Huy
            257122,// Kiều Duy Lâm
            GasConst::UID_TRUONG_NN,
            GasConst::UID_DOT_NV,
        ];
        return $aRes;
    }
    
    /** @Author: ANH DUNG Sep 30, 2018
     *  @Todo: get list GDKV + CV 
     *  id_cv => id_gdkv
     **/
    public static function getArrayGdkv() {
        return [
            // TP HCM
//            471328    => GasLeave::UID_CHIEF_MONITOR,// Trần Văn Huy
            
            GasConst::UID_HIEU_NT       => GasLeave::UID_DIRECTOR,
            1001652                     => GasLeave::UID_DIRECTOR, // Nguyễn Thanh Hậu
            30760                       => GasLeave::UID_DIRECTOR,// Lê Xuân Sang
            GasConst::UID_BINH_TV       => GasLeave::UID_DIRECTOR,// Trần Văn Bình
            
            1810265                     => GasLeave::UID_DIRECTOR,// Dương Hùng Vĩ
            1176                        => GasLeave::UID_DIRECTOR,// Đặng Trung Hiếu Jan2519
//            1784420                     => GasLeave::UID_DIRECTOR,// ĐLLK Phạm Đình Thọ Mar0619
//            1815873                     => GasLeave::UID_DIRECTOR,// Nguyễn Thanh Hoang ĐN
            1657571                     => GasLeave::UID_DIRECTOR,// Tô Bá Vương
//            1740207                     => GasLeave::UID_DIRECTOR,// Trần Viết Thái
            1616722                     => GasLeave::UID_DIRECTOR,// Lê Nhật Trường
//            GasConst::UID_TINH_P        => GasLeave::UID_DIRECTOR,// Phan Tịnh
//            GasConst::UID_DOT_NV        => GasLeave::UID_DIRECTOR,
//            GasConst::UID_TAM_NM        => GasLeave::UID_DIRECTOR,
            974623                      => GasLeave::UID_DIRECTOR, // Nguyễn Minh Nhật
            1393798                     => GasLeave::UID_DIRECTOR, // Nguyễn Đình Đại Nghĩa
            1714261                     => GasLeave::UID_DIRECTOR, // Nguyễn Hữu Toàn
            863291                      => GasLeave::UID_DIRECTOR, // Trần Tuấn Anh
            981743                      => GasLeave::UID_DIRECTOR, // Huỳnh Gia Huy
            2018967                     => GasLeave::UID_DIRECTOR, // Nguyễn Hoàng Mỹ
            1036609                     => GasLeave::UID_DIRECTOR, // Nguyễn Thanh Phúc
            1039882                     => GasLeave::UID_DIRECTOR, // Nguyễn Đình Trung
            1986067                     => GasLeave::UID_DIRECTOR, // Huỳnh Thanh Phú
            1503658                     => GasLeave::UID_DIRECTOR, // Trịnh Văn Tùng
            886663                      => GasLeave::UID_DIRECTOR, // Võ Xuân Hiệp
            1241317                     => GasLeave::UID_DIRECTOR, // Vũ Thành Sơn
            893613                      => GasLeave::UID_DIRECTOR, // Lê Thanh Liêm
            1841311                     => GasLeave::UID_DIRECTOR, // Nguyễn Hoàng Minh
            self::UID_BINH_TV           => GasLeave::UID_DIRECTOR, // Trần Văn Bình

            // Mien Dong
            GasConst::UID_LAM_KD        => GasLeave::UID_DIRECTOR,
            GasConst::UID_LOC_PV        => GasLeave::UID_DIRECTOR,
            GasConst::UID_NHAT_NT       => GasLeave::UID_DIRECTOR,
            GasConst::UID_NGAN_TNP      => GasLeave::UID_DIRECTOR,
            1713284         => GasLeave::UID_DIRECTOR,// Bùi Xuân Ngọc Jun0119
            850212          => GasLeave::UID_DIRECTOR,// Phan Thành Nhân Jun0119
//            GasConst::UID_LOI_TV        => GasLeave::UID_DIRECTOR,// Close Dec1318
            874498                      => GasLeave::UID_DIRECTOR,// Nguyễn Duy Phương

            // Mien Tay
            937389                      => GasLeave::UID_DIRECTOR,//  Lê Trung Hiển May0219
            GasConst::UID_TUAN_DH       => GasLeave::UID_DIRECTOR,
            GasConst::UID_TRUNG_HV      => GasLeave::UID_HEAD_OF_LEGAL,
            GasConst::UID_EM_MP         => GasLeave::UID_HEAD_OF_LEGAL,
            GasConst::UID_LIEM_LT       => GasLeave::UID_HEAD_OF_LEGAL,
            GasConst::UID_LINH_TTV      => GasLeave::UID_HEAD_OF_LEGAL,
            GasConst::UID_ANH_VH        => GasLeave::UID_HEAD_OF_LEGAL,
            GasConst::UID_PHONG_NU      => GasLeave::UID_HEAD_OF_LEGAL,
            
            GasConst::UID_THIEN_NM      => GasLeave::UID_HEAD_OF_LEGAL,// Nguyễn Minh Thiện
            GasConst::UID_AN_LCT        => GasLeave::UID_HEAD_OF_LEGAL,
            GasConst::UID_TRUONG_NN     => GasLeave::UID_HEAD_OF_LEGAL,
            GasConst::UID_TROC_VV       => GasLeave::UID_HEAD_OF_LEGAL,
            GasConst::UID_DUC_NT        => GasLeave::UID_HEAD_OF_LEGAL,// Nguyễn Tấn Đức

            // Mien Trung - Tay Nguyen
            GasConst::UID_HA_VT         => GasLeave::UID_DIRECTOR,
            GasConst::UID_SU_BH         => GasLeave::UID_DIRECTOR,
//            893459                      => GasConst::UID_PHUONG_ND,// Trần Uy Lực
        ];
    }
    
    // Oct0118 TransactionHistory + Sell
    const TIME_5_MIN  = 5; // AnhDung define số phút để check hoàn thành app
    const TIME_10_MIN = 10; // AnhDung define số phút để check hoàn thành app
    const TIME_15_MIN = 15; // AnhDung define số phút để check hoàn thành app
    const TIME_20_MIN = 20; // AnhDung define số phút để check hoàn thành app
    const TIME_25_MIN = 25; // AnhDung define số phút để check hoàn thành app
    const TIME_30_MIN = 30; // sử dụng trong TransactionHistory + Sell
    const TIME_40_MIN = 40;
    const TIME_50_MIN = 50;
    const TIME_60_MIN = 60;

    const PUNISH_5K     = 5000;
    const PUNISH_10K    = 10000;
    const PUNISH_15K    = 15000;
    const PUNISH_20K    = 20000;
    
    const PUNISH_100K    = 100000;
    const PUNISH_200K    = 200000;
    const PUNISH_500K    = 500000;
    
    const PUNISH_1000K    = 1000000; //HaoNH
    // Oct0118 TransactionHistory + Sell
    
    // Oct1218 move from to here, để sử dụng đc trong module admin 
    public static $defaultResponse = array('status'=>0,
                                    'code'=>400,
                                    'message'=>'Invalid request. Please check your http verb, action url and param'
                                    );
    public static $defaultSuccessResponse = array('status'=>1,
                                    'code'=>5335,
                                    'message'=>'Success'
                                    );
    // Oct1218 move from to here, để sử dụng đc trong module admin 
    
}
