<?php
/** Gas
 * The extend of CFormatter, using in type of CGridView columns and attributes of CDetailView
 */
class CmsFormatter extends CFormatter
{
    protected $statusFormat = array('1'=>'Active', '0'=>'Inactive');
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;    
    protected $publishFormat = array('1' => 'Published', '0' => 'Unpublished');
    protected $approveFormat = array('1' => 'Approved', '0' => 'Unapproved');
    public static $statusVar = array('1' => 'Active', '0' => 'Inactive');
    public static $yesNoFormat = array(1 => 'Có', 0 => 'Không');
    public static $yesNoCharFormat = array('yes' => 'Có', 'no' => 'Không');
    public static $allModule = array(null => 'Front End',
            'admin' => 'Admin',
            'member' => 'Member',
            'product' => 'Product',
            'auditTrail' => 'Audit Trail');

    public static $PAGE_MAX_BUTTON = 20;
    
    public static $aTypePay = array(PAY_TYPE_1,PAY_TYPE_2,PAY_TYPE_3,PAY_TYPE_4);
    public static $aRoleRestrict = array(ROLE_MANAGER, ROLE_ADMIN,ROLE_SALE,ROLE_CUSTOMER,ROLE_AGENT, ROLE_SUB_USER_AGENT);
    public static $aRoleRestrictCode = array(ROLE_MANAGER, ROLE_ADMIN,ROLE_CUSTOMER,ROLE_AGENT, ROLE_SUB_USER_AGENT);
    
//    public static $aRoleMemLogin = array(ROLE_MEMBER,ROLE_CHECK_MAINTAIN,ROLE_ACCOUNTING_AGENT_PRIMARY,
//	ROLE_ACCOUNTING_ZONE,ROLE_MONITORING, ROLE_MONITORING_MAINTAIN,
//	ROLE_MONITORING_MARKET_DEVELOPMENT, ROLE_EMPLOYEE_MARKET_DEVELOPMENT, 
//        ROLE_SALE,ROLE_MONITORING_STORE_CARD, ROLE_DIEU_PHOI,ROLE_SCHEDULE_CAR,
//        ROLE_DIRECTOR,ROLE_SUB_USER_AGENT,ROLE_ACCOUNT_RECEIVABLE,
//        ROLE_HEAD_GAS_BO, ROLE_HEAD_GAS_MOI, ROLE_DIRECTOR_BUSSINESS,ROLE_RECEPTION,
//        ROLE_CHIEF_ACCOUNTANT, ROLE_CHIEF_MONITOR,ROLE_MONITOR_AGENT,ROLE_SECRETARY_OF_THE_MEETING,
//        ROLE_HEAD_OF_LEGAL, ROLE_EMPLOYEE_OF_LEGAL, ROLE_ACCOUNTING, ROLE_DEBT_COLLECTION,
//        ROLE_HEAD_TECHNICAL,ROLE_HEAD_OF_MAINTAIN,ROLE_E_MAINTAIN,
//        ROLE_SECURITY_SYSTEM,ROLE_BUSINESS_PROJECT,ROLE_HEAD_GAS_FAMILY,
//        ROLE_CHIET_NAP
//	);
    public static $aRoleMemLogin = array();
    public static $aRoleMemNotLogin = array(
//        ROLE_EMPLOYEE_MAINTAIN,ROLE_ACCOUNTING_AGENT,
        ROLE_MANAGER, ROLE_ADMIN,ROLE_SECURITY_SYSTEM,
        ROLE_MEMBER,ROLE_CUSTOMER,ROLE_AGENT, ROLE_COMPANY, ROLE_CAR, ROLE_BANK, ROLE_ASSETS
        );
    public static $aRoleAgentUserLogin = array(
//        ROLE_EMPLOYEE_MAINTAIN,ROLE_ACCOUNTING_AGENT,
        ROLE_MANAGER, ROLE_ADMIN,ROLE_SECURITY_SYSTEM,
        ROLE_MEMBER,ROLE_CUSTOMER, ROLE_COMPANY, ROLE_SUB_USER_AGENT
    );
    public static  $days = array(
        2=>'Monday',
        3=>'Tuesday',
        4=>'Wednesday',
        5=>'Thursday',
        6=>'Friday',
        7=>'Saturday',
        8=>'Sunday',
    );    
    
    public static  $STATUS_MAINTAIN = array(
        STATUS_NOT_YET_CALL=>'Chưa Gọi',
        STATUS_HAS_CALL_OK=>'Đã Gọi Rất Tốt',
        STATUS_HAS_CALL_OK_LEVEL_1=>'Đã Gọi Tốt',
        STATUS_HAS_CALL_OK_LEVEL_2=>'Đã Gọi Bình Thường',
        STATUS_HAS_CALL_OK_LEVEL_3=>'Đã Gọi Không Được Tốt',
        STATUS_HAS_CALL_FAILED=>'Đã Gọi Xấu',
    );    
    
    public static  $STATUS_MAINTAIN_GOOD = array(
        STATUS_HAS_CALL_OK,
        STATUS_HAS_CALL_OK_LEVEL_1,
        STATUS_HAS_CALL_OK_LEVEL_2,        
    );     
    public static  $STATUS_MAINTAIN_BAD = array(
        STATUS_HAS_CALL_OK_LEVEL_3,
        STATUS_HAS_CALL_FAILED
    );    
    
    public static  $STATUS_MAINTAIN_BACK = array(
        1=>'Chưa Xử Lý',
        2=>'Xử Lý Xong',
    );    

    public static  $STATUS_SAME_SERI = array(
        0=>'Không',
        1=>'Trùng Seri Bảo Trì',
    );    

    public static  $STATUS_IS_MAINTAIN = array(
        0=>'Chưa Bảo Trì',
        1=>'Đã Bảo Trì',
    );    

    public static  $SALUTATION = array(
        1=>'Anh',
        2=>'Chị',
        3=>'Cô',
        4=>'Chú',
        8=>'Em',
        5=>'Bác',
        6=>'Gì',
        7=>'Ông',
    );   
	
    public static  $TYPE_MAINTAIN = array(
        TYPE_MAINTAIN=>'Bảo Trì',
        TYPE_MARKET_DEVELOPMENT=>'Phát Triển Thị Trường',
    );    		
    public static  $CUSTOMER_NEW_OLD = array(CUSTOMER_NEW=>'Khách Hàng Mới', CUSTOMER_OLD=>'Khách Hàng Cũ');
    public static  $CUSTOMER_NEW_OLD_STORE_CARD = array( CUSTOMER_PAY_NOW=>'KH Trả Tiền Ngay (Hộ GĐ)', CUSTOMER_NEW=>'KH Mới ( Bò+Mối )', CUSTOMER_OLD=>'KH Cũ ( Bò+Mối )');
    public static $SortByFormat = array('desc' => 'Giảm Dần', 'asc' => 'Tăng Dần');
    public static  $STORE_CARD_TYPE = array(
        TYPE_STORE_CARD_IMPORT=>'Nhập Kho',
        TYPE_STORE_CARD_EXPORT=>'Xuất Kho',
    );    
    
    /******** FOR SELECT CỦA HỆ THỐNG - KHI THÊM MỚI 1 LOẠI LÀ PHẢI THÊM VÀO 3 MẢNG NÀY ****/
    public static  $STORE_CARD_ALL_TYPE = array(
        STORE_CARD_TYPE_1=>'Nhập Nội Bộ',
        STORE_CARD_TYPE_2=>'Nhập Mua',
        STORE_CARD_TYPE_5=>'Nhập Vỏ',
        STORE_CARD_TYPE_6=>'Nhập Khác',
        STORE_CARD_TYPE_14=>'Nhập Vỏ Thế Chân',
        STORE_CARD_TYPE_15=>'Nhập Thu Hồi Và Đổi',
        STORE_CARD_TYPE_16=>'Nhập Chiết',
        STORE_CARD_TYPE_18=>'Nhập đổi vỏ',
        STORE_CARD_TYPE_21=>'Nhập mua vỏ',
        STORE_CARD_TYPE_22=>'Nhập chiết thuê',
        
        STORE_CARD_TYPE_3=>'Xuất Bán',
        STORE_CARD_TYPE_4=>'Xuất Nội Bộ',
        STORE_CARD_TYPE_7=>'Xuất Khác',
        STORE_CARD_TYPE_8=>'Xuất Trả',
        STORE_CARD_TYPE_9=>'Xuất Tặng',
        STORE_CARD_TYPE_10=>'Xuất Bán Bộ Bình',
        STORE_CARD_TYPE_11=>'Xuất Bán Thế Chân',
        STORE_CARD_TYPE_12=>'Xuất Bán Vỏ',
        STORE_CARD_TYPE_13=>'Xuất Cho Bảo Trì',        
        STORE_CARD_TYPE_17=>'Xuất Chiết',        
        STORE_CARD_TYPE_19=>'Xuất Đổi Vỏ',
        STORE_CARD_TYPE_20=>'Xuất Dùm',
        STORE_CARD_TYPE_23=>'Xuất chiết thuê',
        STORE_CARD_TYPE_24=>'Xuất kho treo CN trừ lương',
    );
    
    public static  $STORE_CARD_TYPE_IMPORT = array(
        STORE_CARD_TYPE_1=>'Nhập Nội Bộ',
        STORE_CARD_TYPE_2=>'Nhập Mua',
        STORE_CARD_TYPE_5=>'Nhập Vỏ',
        STORE_CARD_TYPE_6=>'Nhập Khác',
        STORE_CARD_TYPE_14=>'Nhập Vỏ Thế Chân',
        STORE_CARD_TYPE_15=>'Nhập Thu Hồi Và Đổi',
        STORE_CARD_TYPE_16=>'Nhập Chiết',
        STORE_CARD_TYPE_18=>'Nhập Đổi Vỏ',
        STORE_CARD_TYPE_21=>'Nhập mua vỏ',
        STORE_CARD_TYPE_22=>'Nhập chiết thuê',
    );    
    public static  $STORE_CARD_TYPE_EXPORT = array(
        STORE_CARD_TYPE_3=>'Xuất Bán',
        STORE_CARD_TYPE_4=>'Xuất Nội Bộ',
        STORE_CARD_TYPE_7=>'Xuất Khác',
        STORE_CARD_TYPE_8=>'Xuất Trả',
        STORE_CARD_TYPE_9=>'Xuất Tặng',
        STORE_CARD_TYPE_10=>'Xuất Bán Bộ Bình',
        STORE_CARD_TYPE_11=>'Xuất Bán Thế Chân',
        STORE_CARD_TYPE_12=>'Xuất Bán Vỏ',
        STORE_CARD_TYPE_13=>'Xuất Cho Bảo Trì',
        STORE_CARD_TYPE_17=>'Xuất Chiết',
        STORE_CARD_TYPE_19=>'Xuất Đổi Vỏ',
        STORE_CARD_TYPE_20=>'Xuất Dùm',
        STORE_CARD_TYPE_23=>'Xuất chiết thuê',
        STORE_CARD_TYPE_24=>'Xuất kho treo CN trừ lương',
    );
    
    public static function getTypeInOutApp(){
        return array(
            STORE_CARD_TYPE_5   => 'Nhập Vỏ',
            STORE_CARD_TYPE_1   => 'Nhập Nội Bộ',
            STORE_CARD_TYPE_2   => 'Nhập Mua',
            STORE_CARD_TYPE_6   => 'Nhập Khác',
            STORE_CARD_TYPE_21  => 'Nhập mua vỏ',
            STORE_CARD_TYPE_4   => 'Xuất Nội Bộ',
            STORE_CARD_TYPE_7   => 'Xuất Khác',
            STORE_CARD_TYPE_3   => 'Xuất Bán',
            STORE_CARD_TYPE_13  => 'Xuất Cho Bảo Trì',
            STORE_CARD_TYPE_9   => 'Xuất tặng',
            STORE_CARD_TYPE_8   => 'Xuất Trả',
            STORE_CARD_TYPE_12  => 'Xuất Bán Vỏ',
            STORE_CARD_TYPE_24  => 'Xuất kho treo CN trừ lương',
        );
    }
    
    /******** FOR SELECT CỦA HỆ THỐNG - KHI THÊM MỚI 1 LOẠI LÀ PHẢI THÊM VÀO 3 MẢNG NÀY ****/
    
    public static  $MASTER_TYPE = array(
        MASTER_TYPE_REVENUE=>'Thu',
        MASTER_TYPE_COST=>'Chi',
    );    
    
    public static  $MASTER_TYPE_CASHBOOK = array(
        MASTER_TYPE_REVENUE=>'Thu',
        MASTER_TYPE_COST=>'Chi',
    );
    public static  $MASTER_TYPE_LOOKUP = array(
        MASTER_TYPE_LOOKUP_CASH_BOOK=>'Sổ Quỹ Tiền Mặt',
    );
    
    public static  $CUSTOMER_BO_MOI = array(
        STORE_CARD_KH_BINH_BO   => 'Bình Bò',
        STORE_CARD_KH_MOI       => 'Mối',
        STORE_CARD_XE_RAO       => 'Xe Rao',
        STORE_CARD_VIP_HGD      => '240 VIP Hộ GĐ',
        STORE_CARD_HGD          => 'Hộ Gia Đình',
        STORE_CARD_HGD_CCS      => 'Dân Dụng',
        UsersExtend::STORE_CARD_HGD_APP             => 'APP',
        UsersExtend::STORE_CARD_HEAD_QUATER         => 'Trụ Sở Chính',
        UsersExtend::STORE_CARD_NCC                 => 'Nhà cung cấp',
        UsersExtend::STORE_CARD_FOR_QUOTE           => 'KH tiềm năng',
        UsersExtend::STORE_CARD_SUPPLIER            => 'Gas bồn - Nhà cung cấp',
        UsersExtend::STORE_CARD_SUPPLIER_WAREHOUSE  => 'Gas bồn - Kho nhà cung cấp',
        UsersExtend::STORE_CARD_GAS_TANK            => 'Gas bồn - khách hàng',
        UsersExtend::STORE_CARD_TRANSPORT           => 'Gas bồn - Đơn vị vận chuyển',
    );
    public static  $CUSTOMER_BO_MOI_ONLY = array(
        STORE_CARD_KH_BINH_BO   => 'Bình Bò',
        STORE_CARD_KH_MOI       => 'Mối',
        STORE_CARD_XE_RAO       => 'Xe Rao',
    );
    
    // Apr 14, 2015 mảng các loại KH thẻ kho cần theo dõi, và dc chọn trong select khi tạo KH
    public static  $ARR_TYPE_CUSTOMER_STORECARD = array(
        STORE_CARD_KH_BINH_BO,
        STORE_CARD_KH_MOI,
        STORE_CARD_XE_RAO,
    );
    // Apr 14, 2015 mảng các loại KH thẻ kho cần theo dõi, và dc chọn trong select khi tạo KH
    
    // FOR Xem báo cáo doanh thu - sản lượng http://localhost/gas/admin/gasreports/Revenue_output
    // TỔNG DOANH THU ĐẠI LÝ THÁNG 12 NĂM 2013
    public static $REVENUE_ITEM = array(
        1, // Thu bán gas 45kg
        2, // Thu bán gas 12kg
        3, // Thu bán gas 50kg
        6, // Thu tiền bán bếp
        7, // Thu tiền bán vỏ
        8, // Thu tiền bán van
        9, // Thu tiền bán dây
        27 // 	Thu tiền bán bộ bình
    );
    public static $REVENUE_ITEM_EXCEPT_CUSTOMER = array( // sử dụng ở Statistic::getMoneyBankAndTotalRevenue()
        STORE_CARD_KH_BINH_BO, // tính doanh thu trừ KH bình bò ra
    );
    // FOR Xem báo cáo doanh thu - sản lượng http://localhost/gas/admin/gasreports/Revenue_output
    
    // fix lại cho phép theo đại lý cho nhanh -- dùng để cho phép mở sửa tồn đầu kỳ
    public static $PROVINCE_ALLOW = array(
//        8, // CẦN THƠ
//        122, // Đại lý Bình Tân
//        26678, // Kho Tân Sơn
//        108, // Đại lý Hóc Môn
//        110, // Đại lý Thủ Dầu Một
//        112, // Đại lý Thủ Đức
//        26677, // Kho Bến Cát
//        132678, // Xưởng Đồng Nai
//        862516, // Dầu Khí Bình Định Apr 11, 2016
        );
    
    // FOR Xem báo cáo - sản lượng http://localhost/gas/admin/gasreports/Revenue_output
    // use at function Statistic::getTotalOutput($model)
    public static $MATERIAL_TYPE_OUTPUT_STATISTIC = array(
        array(
            'code'=>'12KG',
            'name'=>'BÌNH 12KG',
            'list_id'=>array(4), // Loại Vật Tư Gas 12 Kg, hiện tại chỉ có 1, sẽ dc tách ra nhiều sau này
        ), 
        array(
            'code'=>'45KG',
            'name'=>'BÌNH 45KG',
            'list_id'=>array(7, 11), // Loại Vật Tư Gas Bình Bò, hiện tại chỉ có 1, sẽ dc tách ra nhiều sau này
            // 7 là id loại vật tư bình bò 45 kg
            // 11 là id loại vật tư bình bò 50 kg
        ), 
    );
    
    public static $TYPE_IN_OUT_OUTPUT_STATISTIC = array(
        STORE_CARD_TYPE_3,// xuất bán
        STORE_CARD_TYPE_10,// Xuất Bán Bộ Bình
        STORE_CARD_TYPE_11,// Xuất Bán Thế Chân
//        STORE_CARD_TYPE_12, // Xuất Bán Gas + Vỏ -- DungNT Remove Jan0819 bỏ không tính vào công của giao nhận
    );
    
    public static $TYPE_MONEY_FROM_BANK = array(
        CHI_NOP_NGAN_HANG,// chi nộp ngân hàng
    );
    
    public static  $TYPE_VIEW_OUTPUT_STATISTIC = array(
        'OUTPUT_12'=>'Sản Lượng Gas 12KG',
        'OUTPUT_45'=>'Sản Lượng Gas 45KG',
        'MONEY_BANK'=>'Đại Lý Nộp Công Ty',
        'TOTAL_REVENUE'=>'Doanh Thu Đại Lý',
    );     
    
    // FOR Xem báo cáo - sản lượng http://localhost/gas/admin/gasreports/Revenue_output	
    
    // FOR Xem báo cáo - DAILY sản lượng http://localhost/gas/admin/gasreports/Output_daily
    public static $MATERIAL_TYPE_BINHBO_12 = array(
        MATERIAL_TYPE_BINHBO_50,// Gas Bình Bò 50 Kg
        MATERIAL_TYPE_BINHBO_45,//
        MATERIAL_TYPE_BINH_12,//
        MATERIAL_TYPE_BINH_6,//
        GasMaterialsType::MATERIAL_BINH_4KG,// Bình 4 Kg Sep 22, 2016
        MATERIAL_TYPE_VO_50,// Vỏ Bình 50 Kg
        MATERIAL_TYPE_VO_45,// 
        MATERIAL_TYPE_VO_12,//  Vỏ Bình 12 Kg
        MATERIAL_TYPE_VO_6,//  Vỏ Bình 6 Kg
        GasMaterialsType::MATERIAL_VO_4,
    );

    public static $MATERIAL_TYPE_BINHBO_OUTPUT = array( // xuất bán
        MATERIAL_TYPE_BINHBO_50,// Gas Bình Bò 50 Kg
        MATERIAL_TYPE_BINHBO_45,//
        MATERIAL_TYPE_BINH_12,//
        MATERIAL_TYPE_BINH_6,//
        GasMaterialsType::MATERIAL_BINH_4KG,// Bình 4 Kg
    );
    
    public static $MATERIAL_TYPE_BINHBO_12_45 = array( // tính toán
        MATERIAL_TYPE_BINHBO_45,//
        MATERIAL_TYPE_BINH_12,//
    );

    public static $MATERIAL_VALUE_KG= array( // xuất bán
        MATERIAL_TYPE_BINHBO_50=>50,// Gas Bình Bò 50 Kg
        MATERIAL_TYPE_BINHBO_45=>45,//
        MATERIAL_TYPE_BINH_12=>12,//
        MATERIAL_TYPE_BINH_6=>6,// close Sep 14, 2015 sửa lại bình 4kg
        GasMaterialsType::MATERIAL_BINH_4KG => 4,
    );
    public static $MATERIAL_TYPE_BINH_BO_VALUE_KG= array( // dùng để xác định giá bình bò ở save storecard detail
        MATERIAL_TYPE_BINHBO_50=>50,// Gas Bình Bò 50 Kg
        MATERIAL_TYPE_BINHBO_45=>45,//
    );    
    
    public static $MATERIAL_TYPE_BINHBO_INPUT = array( // nhập vỏ
        MATERIAL_TYPE_VO_50,// 
        MATERIAL_TYPE_VO_45,// 
        MATERIAL_TYPE_VO_12,//  Vỏ Bình 12 Kg
        MATERIAL_TYPE_VO_6,//  Vỏ Bình 6 Kg
        GasMaterialsType::MATERIAL_VO_4,
    );

    public static $MATERIAL_TYPE_PAIR = array( // CẶP GIÁ TRỊ GAS-VỎ ĐỂ THEO DÕI CÔNG NỢ VỎ KH THEO LOẠI BÌNH
        MATERIAL_TYPE_BINHBO_50 => MATERIAL_TYPE_VO_50,// VT GAS=> VT VỎ 11-12
        MATERIAL_TYPE_BINHBO_45 => MATERIAL_TYPE_VO_45,// 7 - 10
        MATERIAL_TYPE_BINH_12 => MATERIAL_TYPE_VO_12,// 4-1
        MATERIAL_TYPE_BINH_6 => MATERIAL_TYPE_VO_6,// 9- 14
        GasMaterialsType::MATERIAL_BINH_4KG => GasMaterialsType::MATERIAL_VO_4,// 9- 14
        
        MATERIAL_TYPE_VO_50 => MATERIAL_TYPE_BINHBO_50,//  VT VỎ=> VT GAS
        MATERIAL_TYPE_VO_45 => MATERIAL_TYPE_BINHBO_45,
        MATERIAL_TYPE_VO_12 => MATERIAL_TYPE_BINH_12,
        MATERIAL_TYPE_VO_6 => MATERIAL_TYPE_BINH_6,
        GasMaterialsType::MATERIAL_VO_4 => GasMaterialsType::MATERIAL_BINH_4KG,
    );
    
    public static $MATERIAL_TYPE_CLOSING_BALANCE = array( // for gasClosingBalanceCustomerMaterials
        MATERIAL_TYPE_BINHBO_50,// Gas Bình Bò 50 Kg
        MATERIAL_TYPE_BINHBO_45,
        MATERIAL_TYPE_BINH_12,
        MATERIAL_TYPE_BINH_6,
        GasMaterialsType::MATERIAL_BINH_4KG,
    );

    // FOR Xem báo cáo - DAILY sản lượng http://localhost/gas/admin/gasreports/Output_daily
    
    public static $ROLE_VIEW_KH_BINHBO = array( // at admin/gascustomer/customer_store_card 
        //chỉ admin và điều phối có thể thấy
        ROLE_ADMIN,// 
        ROLE_DIEU_PHOI,// 
        ROLE_HEAD_GAS_BO,// 
        ROLE_HEAD_GAS_MOI,// 
        ROLE_DIRECTOR_BUSSINESS,// 
    ); 
    
    // FOR BÁO CÁO NHẬP XUẤT TỒN TRONG NÀY CỦA ĐẠI LÝ - function Statistic::AgentNXT()
    public static $LIST_WAREHOUSE_ID = array( // at admin/gascustomer/customer_store_card 
//        MyFormat::KHO_TAN_SON,// Kho Tân Sơn
        MyFormat::KHO_BEN_CAT,// Kho Bến Cát
        MyFormat::KHO_PHUOC_TAN,// Kho Phước Tân
        MyFormat::KHO_VUNG_TAU,
    );    
    
    public static $NXT_VT_GIFT_INPUT = array( // at admin/gascustomer/customer_store_card 
        STORE_CARD_TYPE_1, // 'Nhập Nội Bộ',
        STORE_CARD_TYPE_2, // 'Nhập Mua',
        STORE_CARD_TYPE_15, // 'Nhập Thu Hồi Và Đổi',
    );
    public static $NXT_VT_GIFT_OUTPUT = array( // at admin/gascustomer/customer_store_card 
        STORE_CARD_TYPE_3, // =>'Xuất Bán',
        STORE_CARD_TYPE_4, // =>'Xuất Nội Bộ',
        STORE_CARD_TYPE_7, // =>'Xuất Khác',
        STORE_CARD_TYPE_9, // =>'Xuất Tặng',
        STORE_CARD_TYPE_10, // =>'Xuất Bán Bộ Bình',
        STORE_CARD_TYPE_13, // =>'Xuất Cho Bảo Trì',
    );
    
    public static $LIST_WAREHOUSE_ONLY_NHAP_XUAT = array( // 
        132678,// Xưởng Đồng Nai
    );    
    public static $LIST_WAREHOUSE_ACCOUNT = array( // 
        25785,// Kho Phước Tân
        122546,// Kho Lê Duẩn
        26678,// Kho Tân Sơn
        26677,// Kho Bến Cát
        30754,// Kho Vĩnh Long
        118239,// Kho Bình Thạnh
        862516,// Dầu Khí Bình Định
        693615,// Kho Chợ Mới
        700575,// Kho Minh Hạnh
        
        132678,// Xưởng Đồng Nai
    );
    // FOR BÁO CÁO NHẬP XUẤT TỒN TRONG NÀY CỦA ĐẠI LÝ - function Statistic::AgentNXT()
    
    public static $aTypeIdBoMoi     = [STORE_CARD_KH_BINH_BO, STORE_CARD_KH_MOI];
    public static $aTypeIdQuotes    = [UsersExtend::STORE_CARD_FOR_QUOTE, STORE_CARD_KH_BINH_BO, STORE_CARD_KH_MOI, UsersExtend::STORE_CARD_HEAD_QUATER];// những loại sẽ lấy để báo giá, bao gôm cả trụ sở chính
    public static $aTypeIdMakeUsername = [UsersExtend::STORE_CARD_FOR_QUOTE, STORE_CARD_KH_BINH_BO, STORE_CARD_KH_MOI, UsersExtend::STORE_CARD_HEAD_QUATER, STORE_CARD_XE_RAO];// những loại sẽ lấy sinh tự động username
    public static $aTypeIdListBoMoi = [UsersExtend::STORE_CARD_FOR_QUOTE, STORE_CARD_KH_BINH_BO, STORE_CARD_KH_MOI, UsersExtend::STORE_CARD_HEAD_QUATER, STORE_CARD_XE_RAO, 
        UsersExtend::STORE_CARD_NCC,
        UsersExtend::STORE_CARD_SUPPLIER,
        UsersExtend::STORE_CARD_SUPPLIER_WAREHOUSE,
        UsersExtend::STORE_CARD_GAS_TANK,
        UsersExtend::STORE_CARD_TRANSPORT,
        ];// dùng để limit loại KH bò mối trong admin/gascustomer/customer_store_card
    public static $aTypeIdHgd   = [ // is column is_maintain
        STORE_CARD_VIP_HGD, 
        STORE_CARD_HGD, 
        STORE_CARD_HGD_CCS,
        UsersExtend::STORE_CARD_HGD_APP,
    ];
    public static $ARR_TYPE_HGD_APP   = array(// vì KH do KTBH tạo ở PMBH mới sẽ không cho trùng số đt nên sẽ check dc
//        STORE_CARD_HGD, // close on Now 22, 2016
        UsersExtend::STORE_CARD_HGD_APP,
    );
    public static $aTypeIdAgentAndOther = array(CUSTOMER_OTHER, CUSTOMER_IS_AGENT);
    
    // LOẠI kh KHI XUẤT BÁN GAS SỬ DỤNG ĐỂ FOREACH
    public static $NXT_OUTPUT_TYPE_CUSTOMER = array(
        CUSTOMER_HO_GIA_DINH, // =>'
        STORE_CARD_KH_MOI, // =>'
        STORE_CARD_KH_BINH_BO, // =>
        CUSTOMER_IS_AGENT, // =>'KH LÀ ĐẠI LÝ KHÁC TRONG HỆ THỐNG
        CUSTOMER_OTHER_OBJ, // => LÀ LOẠI kh KHÔNG PHẢI BÒ HAY MỐI
    );    
    
    public function getStatusFormat($hasEmpty = true) {
        if($hasEmpty)
            return array(''=>'', '1'=>'Active', '0'=>'Inactive');
        return $this->statusFormat;
    }    
    
    public function getApproveFormat($hasEmpty = true) {
        if($hasEmpty)
            return array(''=>'', '1'=>'Approved', '0'=>'Unapproved');
        return $this->approveFormat;
    }

    public function getPositionFormat($hasEmpty = true) {
        if($hasEmpty)
            return array(''=>'', 'Bottom'=>'Bottom');
        return $this->positionFormat;
    }
	
	/* formatYNStatus use for Yes/No*/
    
    public static function formatYNStatus($value)
    {
    	$return = array('1' =>	'Yes','0' =>	'No');
    	return isset($return[$value])?$return[$value]:""; 
    }
    

    public function formatStatus($value)
    {
        if(is_array($value))
        {
            return (($value['status'] == self::STATUS_INACTIVE) ?
                CHtml::link(
                    "Nghỉ Việc",
                    array("ajaxActivate", "id"=>$value['id']),
                    array(
                        "class"=>"ajaxupdate",
                        "title"=>"Click here to ".$this->publishFormat[1],
                    )
                )
                :
                CHtml::link(
                    "Active",
                    array("ajaxDeactivate", "id"=>$value['id']),
                    array(
                        "class"=>"ajaxupdate",
                        "title"=>"Click here to ".$this->publishFormat[0],
                    )
                )
            );
        }
        else
            return $value == 0 ? $this->statusFormat['0'] : $this->statusFormat['1'];
    }
    
    public function formatShowMenu($value)
    {
        if(is_array($value))
        {
            return ($value['show_menu'] == self::STATUS_INACTIVE) ?
                CHtml::link(
                    "No",
                    array("ajaxShow", "id"=>$value['id']),
                    array(
                        "class"=>"ajaxupdate",
                        "title"=>"Click here to turn on in menu",
                    )
                )
                :
                CHtml::link(
                    "Yes",
                    array("ajaxNotShow", "id"=>$value['id']),
                    array(
                        "class"=>"ajaxupdate",
                        "title"=>"Click here to turn off in menu",
                    )
                );

        }
        else
            return $value == 0 ? $this->statusFormat['0'] : $this->statusFormat['1'];
    }

    public function formatPage($value)
    {
        $value = EmbedController::parse($value);
        return $this->formatHtml($value);
    }

    public function formatDate($value)
    {	
        if($value=='0000-00-00' || $value=='0000-00-00 00:00:00' || is_null($value))
            return '';	
        if(is_string($value))
        {
            $date = new DateTime($value);
//            return $date->format(Yii::app()->params['dateFormat']);
            return $date->format( 'd/m/Y' );
        }
        return parent::formatDate($value);
    }

    public function formatTime($value)
    {
        if($value=='0000-00-00' || $value=='0000-00-00 00:00:00' || is_null($value))
            return '';	
        if(is_string($value))
        {
            $date = new DateTime($value);
            return $date->format( 'H:i');
        }
        return parent::formatDate($value);
    }

    public function formatDateTime($value)
    {
        if($value=='0000-00-00' || $value=='0000-00-00 00:00:00' || is_null($value))
            return '';	
        if(is_string($value))
        {
            $date = new DateTime($value);
//            return $date->format(Yii::app()->params['dateFormat'] . ' ' . Yii::app()->params['timeFormat']);
            return $date->format('d/m/Y' . ' ' . 'H:i');
        }
        return parent::formatDate($value);
    }

    public function formatFileSize($value)
    {
        static $KB = 1024;
        static $MB = 1048576;

        $size = intval($value);
        if($size < $KB)
        {
            return $value . ' B';
        }
        elseif($size < $MB)
        {
            return round($value / $KB, 3) . ' KB';
        }
        else
        {
            return round($value / $MB, 3) . ' MB';
        }
    }

    public function formatImage($value)
    {
        if(is_array($value))
        {
            $url = Yii::app()->createUrl($value['url']);
            $h = $value['h'];
            $w = $value['w'];
            return CHtml::image(Yii::app()->createAbsoluteUrl(
                "vendors/timthumb.php?src=$url&h=$h&w=$w&zc=1"
            ), '', isset($value['htmlOptions']) ? $value['htmlOptions'] : array());
        }
//        return CHtml::image(Yii::app()->createAbsoluteUrl($value));
        return $value;
    }

    public function formatPrice($value, $country = 'sg')
    {
        if($country == 'sg')
        {
            return 'S$'.number_format($value,2);
        }
        return $value;
    }

   public static function formatCurrency($price)
   {
        return ActiveRecord::formatCurrency($price);
   }
   
    public function formatNumberCurrency($value, $country = 'sg')
    {
        if(is_array($value))
        {
            if(empty($value['currencyType']))
                $currencyType = 'SGD';
            else
                $currencyType = $value['currencyType'];
            return number_format((float)$value['number'],2)." (".$currencyType.")";
        }
        else
            return $value = "";		
    }
    
    // Nguyen Dung 09-16-2013
    public function formatDeleteAgentCustomer($model) {
        $cUid = MyFormat::getCurrentUid();
        $aUidAllow = [GasConst::UID_PHUONG_TT, GasConst::UID_VAN_TV, GasTickets::DIEU_PHOI_TRANG, GasConst::UID_ADMIN];
        if(!in_array($cUid, $aUidAllow)){
            return '';
        }
        
        return "<a  title='Xóa Khách Hàng Khỏi Đại Lý' href='".Yii::app()->createAbsoluteUrl('admin/gasagent/delete_customer_agent',
                array('id'=>$model->id))."' onclick='return fnUpdateDeleteCustomer(this);'>Xóa</a>";
    }        
    
    // Nguyen Dung 09-16-2013
    public function formatHadSelling($model) {
        if(count($model->GasMaintainSell)>0){
            $html = "<div class='HadSelling'>";
            foreach($model->GasMaintainSell as $item){
                if($item->maintain_id==$model->id)
                $html.="<a class='view_selling' title='Xem Bán Hàng' href='".Yii::app()->createAbsoluteUrl('admin/gasmaintainsell/view',array('id'=>$item->id))."'>
                            <img alt='Xem Bán Hàng' src='".Yii::app()->theme->baseUrl."/admin/images/icon/selling.png'>    
                        </a>";
            }
            $html .= "</div>";
            return $html;
        }
        return "";
    }        
    
   public static function formatStatusMaintain($status)
   {
       $str='';
       if($status==STATUS_HAS_CALL_OK)
           $str="<font style='color:#0101DF'>".CmsFormatter::$STATUS_MAINTAIN[$status]."</font>";
       elseif($status==STATUS_HAS_CALL_FAILED)
           $str="<font style='color:#DF0101'>".CmsFormatter::$STATUS_MAINTAIN[$status]."</font>";
       elseif($status==STATUS_HAS_CALL_OK_LEVEL_1)
           $str="<font style='color:#0040FF'>".CmsFormatter::$STATUS_MAINTAIN[$status]."</font>";
       elseif($status==STATUS_HAS_CALL_OK_LEVEL_2)
           $str="<font style='color:#0080FF'>".CmsFormatter::$STATUS_MAINTAIN[$status]."</font>";
       elseif($status==STATUS_HAS_CALL_OK_LEVEL_3)
           $str="<font style='color:#F78181'>".CmsFormatter::$STATUS_MAINTAIN[$status]."</font>";
       else
           return CmsFormatter::$STATUS_MAINTAIN[$status];
        return $str;
   }
   
   public static function formatStatusSupervision($status)
   {
       $str='';
       if($status==2)
           return $str="<font style='color:#DF0101'>".CmsFormatter::$STATUS_MAINTAIN_BACK[$status]."</font>";
        return CmsFormatter::$STATUS_MAINTAIN_BACK[$status];
   }
       
   public static function formatMaintainHistory($model)
   {
       $str='';
       $cmsFormat = new CmsFormatter();
       $mMaintain = GasMaintain::getAllMaintainCustomerOfAgent($model->agent_id, $model->customer_id);
       if(count($mMaintain)>0)
        foreach($mMaintain as $key=>$item){
           $TEXT = 'Bảo trì';
           if($item->type == TYPE_MARKET_DEVELOPMENT)
               $TEXT = 'PTTT';
           if($key==0)
                $str.="<b>$TEXT lần: ".($key+1)." </b>";
           else
                $str.="<br><font style='color:red'><b>$TEXT lần: ".($key+1)." </b></font>";
           $str.="<br>Ngày : ".$cmsFormat->formatDate($item->maintain_date)." ";
           $str.="<br>Nhân Viên : ".($item->maintain_employee?$item->maintain_employee->first_name:'')." ";
           $str.="<br>Hiệu Gas : ".($item->materials?$item->materials->name:'')." ";
           $str.="<br>Seri : ".$item->seri_no."<br> ";
        }
        return $str;
   }
          
		  
    public static function formatRoleNameUser($role_id)
    {
        if(empty($role_id)) return '';
        $session=Yii::app()->session;
        if(!isset($session['ROLE_NAME_USER']) || !isset($session['ROLE_NAME_USER'][$role_id]))
                $session['ROLE_NAME_USER'] = Roles::getArrRoleName();
        return $session['ROLE_NAME_USER'][$role_id];
    }
		  
    public static function formatStatusCustomerMaintain($mUser)
   {
        return CmsFormatter::$STATUS_IS_MAINTAIN[$mUser->is_maintain];
   }
       
		  
    public static function formatCheckWithSeriMaintain($mMaintainSell)
   {
       $str='';
//       $mMaintain = GasMaintain::getLatestObjMaintainCustomerOfAgent($mMaintainSell->agent_id, $mMaintainSell->customer_id);
       if($mMaintainSell->is_same_seri_maintain)
           return 'Trùng Seri Bảo Trì';
       return "<font style='color:red;'>Không</font>";
   }
       
    public static function formatMaintainSellPromotion($mMaintainSell)
   {
       $str='';
       $models = GasOneManyBig::getArrModelOfManyId($mMaintainSell->id, ONE_SELL_MAINTAIN_PROMOTION);
//       $mMaintain = GasMaintain::getLatestObjMaintainCustomerOfAgent($mMaintainSell->agent_id, $mMaintainSell->customer_id);
       if(count($models)>0){
           foreach($models as $key=>$item){
               if($key==0)
                    $str.= $item->material?($key+1).'. '.$item->material->name:'';
               else
                   $str.= $item->material?'<br>'.($key+1).'. '.$item->material->name:'';
           }
       }
       return $str;
   }
   
    public function formatStatusField($value)
    {
        if(is_array($value))
        {
            return (($value['status'] == self::STATUS_INACTIVE) ?
                CHtml::link(
                    "Inactive",
                    array("ajaxActivateField", "id"=>$value['id'],'field_name'=>$value['field_name']),
                    array(
                        "class"=>"ajaxupdate",
                        "title"=>"Click here to ".$this->publishFormat[1],
                    )
                )
                :
                CHtml::link(
                    "Active",
                    array("ajaxDeactivateField", "id"=>$value['id'],'field_name'=>$value['field_name']),
                    array(
                        "class"=>"ajaxupdate",
                        "title"=>"Click here to ".$this->publishFormat[0],
                    )
                )
            );
        }
        else
            return $value == 0 ? $this->statusFormat['0'] : $this->statusFormat['1'];
    }
       
    /**
     * @Author: ANH DUNG 11-11-2013
     * @Todo: display full name of user
     * @Param: $mUser model user
     * @Return: full name with salution of user
     */    
   public function formatNameUser($mUser)
   {
       if(!$mUser) return '';
//        if(!empty($mUser->last_name)){ // Close on Apr 15, 2015 vi thay khong can thiet, he thong khong co dung last_name
//            if(isset(CmsFormatter::$SALUTATION[$mUser->last_name]))
//                return CmsFormatter::$SALUTATION[$mUser->last_name].' '.trim($mUser->first_name);
//        }
        return $mUser->code_bussiness."-".trim($mUser->first_name);
   }

   public function formatOnlyNameUser($mUser)
   {
        if(!$mUser) return '';
        return $mUser->first_name;
   }
   
    /**
     * @Author: ANH DUNG 11-11-2013
     * @Todo: display full name of customer and type Bo Moi
     * @Param: $mUser model user
     */        
   public function formatNameUserWithTypeBoMoi($mUser)
   {
        if(is_null($mUser)) return '';
        return $mUser->code_bussiness." - ".trim($mUser->first_name)." - ". (isset(CmsFormatter::$CUSTOMER_BO_MOI[$mUser->is_maintain])?CmsFormatter::$CUSTOMER_BO_MOI[$mUser->is_maintain]:"");
   }

   // 11-11-2013 ANH DUNG
   public function formatAddressTempUser($mUser){
       $str='';
       if($mUser)
           return $mUser->address;
       return '';
//       if(!empty($mUser->address_temp))
//           return $mUser->address_temp;
       
   }

   // 12-15-2013 ANH DUNG
   public function formatPrimaryAddress($mUser){
       if(!$mUser) return '';
       MyFunctionCustom::buildAddressUser($mUser);
       return $mUser->address;
   }

   // 12-17-2013 ANH DUNG
   public function formatStoreCardDetail($mStoreCard){
       $str = '';
       $sum_sl=0;
       $total_item = count($mStoreCard->StoreCardDetail);
       if($total_item){
           foreach($mStoreCard->StoreCardDetail as $item){
               if(empty($item->materials_id)){
                   continue ;
               }
               $sum_sl+=$item->qty;
               $str .= "<br> SL:<b>".  ActiveRecord::formatCurrency($item->qty)."</b> - ".$item->materials->name;
           }
       }
       if($total_item>1){
           $str .= "<br> <b>Tổng SL: $sum_sl</b>";
       }
//       $str .= "<br> <b>Người tạo: </b>".$mStoreCard->getUidLogin();// Jul 15, 2016 có thể không cần show cái này
       
       return trim($str,"<br>");
   }

   // 12-26-2013 ANH DUNG
   public function formatCashBookDetail($model){
       $str = '';
       if(count($model->CashBookDetail)){
           foreach($model->CashBookDetail as $key=>$item){
               $str .= "<br> <b>".($key+1).". </b>Diễn giải: $item->description - <b>Loại:</b> {$item->master_lookup->name} - SL: ".$item->qty ." Số Tiền: ". ActiveRecord::formatCurrency($item->amount);
           }
       }
       return trim($str,"<br>");
   }

   // 01-01-2014 ANH DUNG: format tên KH của thẻ kho, vì có tể không nhập customer
   public function formatNameCustomerStoreCard($model){
       $res = '';
       $cmsFormater = new CmsFormatter();
       if($model->customer){
           $res = $cmsFormater->formatNameUser($model->customer);
       }else{
           $res = CmsFormatter::$CUSTOMER_NEW_OLD_STORE_CARD[CUSTOMER_PAY_NOW];
       }
       return $res;
   }
   
   // 01-01-2014 ANH DUNG: format tên KH của thẻ kho, vì có tể không nhập customer
   public function formatNameCustomerOrder($model){
       $res = '';
       $cmsFormater = new CmsFormatter();
       if($model->customer){
           $type = isset(CmsFormatter::$CUSTOMER_BO_MOI[$model->customer->is_maintain])?CmsFormatter::$CUSTOMER_BO_MOI[$model->customer->is_maintain]:"";
           $res = $cmsFormater->formatNameUser($model->customer)." KH $type";
       }
       return $res;
   }
   
   // 01-04-2014 ANH DUNG: format tên KH của thẻ kho, vì có tể không nhập customer
   public function formatCustomerStoreCardType($model){
       $res = '';
       if($model->customer){
           $res = $model->customer->is_maintain?CmsFormatter::$CUSTOMER_BO_MOI[$model->customer->is_maintain]:"";
       }
       return $res;
   }
   
   // Apr 01, 2014 ANH DUNG: format cân gas lần 1 của gas dư
   public function formatGasRemain1($model){       
       $res = '';
       if($model->amount_gas){
           $res .= "".ActiveRecord::formatCurrency($model->amount_gas);
       }
        $res .= "<input class='amount_gas' type='hidden' value='$model->amount_gas'>";
       return $res;
   }   
   // Apr 01, 2014 ANH DUNG: format cân gas lần 1 của gas dư
   
   // Mar 01, 2014 ANH DUNG: format cân gas lần 2 của gas dư
   public function formatGasRemain2($model){       
       $res = '';
       $cRole = MyFormat::getCurrentRoleId();
       if( !empty($model->user_update_2) ){
//           $res .= "<span class='high_light_tr'></span>".ActiveRecord::formatCurrency($model->amount_gas_2)." Kg - ".($model->re_user_update_2?$model->re_user_update_2->first_name:" User đã bị xóa");
           $res .= "<span class=''></span>".ActiveRecord::formatCurrency($model->amount_gas_2)." Kg";
           if(GasRemain::CanUpdateGasRemain2Or3($model)){
               $res .= "<br><a class='update_gas_remain' href='".Yii::app()->createAbsoluteUrl('admin/ajax/update_gas_remain',array('id'=>$model->id,'type'=>'2'))."'>Cập NhậtLần 2</a>";
           }
           
           if($cRole == ROLE_ADMIN){
               $res .= "<br><a class='remove_update_gas_remain' href='javascript:void(0);' rel='".Yii::app()->createAbsoluteUrl('admin/ajax/remove_update_gas_remain',array('id'=>$model->id,'type'=>'2'))."'>Hủy Lần 2</a>";
           }
           
       }else{
           if(GasRemain::CanUpdateGasRemain2Or3($model)){
                $res = "<a class='update_gas_remain' href='".Yii::app()->createAbsoluteUrl('admin/ajax/update_gas_remain',array('id'=>$model->id,'type'=>'2'))."'>Cập NhậtLần 2</a>";
           }
       }
        $res .= "<input class='amount_gas_2' type='hidden' value='$model->amount_gas_2'>";
       return $res;
   }   
   // Mar 01, 2014 ANH DUNG: format cân gas lần 3 của gas dư
   public function formatGasRemain3($model){
       $res = '';
       if(empty($model->amount_gas_2))
           return $res;
       if($model->amount_gas_3 != 0){
           $res .= "<span class='high_light_tr'></span>".ActiveRecord::formatCurrency($model->amount_gas_3)." Kg - ".($model->re_user_update_3?$model->re_user_update_3->first_name:" User đã bị xóa");
           if(GasRemain::CanUpdateGasRemain2Or3($model)){
               $res .= "<br><a class='update_gas_remain' href='".Yii::app()->createAbsoluteUrl('admin/ajax/update_gas_remain',array('id'=>$model->id,'type'=>'3'))."'>Cập Nhật Lần 3</a>";
           }
       }else{
           if(GasRemain::CanUpdateGasRemain2Or3($model)){
                $res = "<a class='update_gas_remain' href='".Yii::app()->createAbsoluteUrl('admin/ajax/update_gas_remain',array('id'=>$model->id,'type'=>'3'))."'>Cập Nhật Lần 3</a>";
           }
       }       
       $res .= "<input class='amount_gas_3' type='hidden' value='$model->amount_gas_3'>";
       return $res;
   }   
   
   // May 20, 2014 ANH DUNG: 
   public function formatGasRemainAmount($model){       
       $res = '';
       if($model->amount != 0){
           $res .= "<span class='amount display_none'>$model->amount</span>".ActiveRecord::formatCurrency($model->amount);
       }
       return $res;
   }     
   
      // Apr 01, 2014 ANH DUNG: format row đã cập nhật số xe rồi
   public function formatGasOrderCar($model){
       $res = $model->orders_no;
       if($model->user_id_update_car_number){
           $res = "<span class='high_light_tr'>$model->orders_no</span>";
       }
       return $res;
   }   

   //// 1: sale bò, 2: sale Mối
   public function formatTypeSaleText($model){
       $res = '';
       if(in_array($model->role_id, Users::$ARR_ROLE_LIKE_SALE) && isset(Users::$aTypeSale[$model->gender])){
           $res = Users::$aTypeSale[$model->gender];
       }
       return $res;
   }
   
   
   // May 20, 2014 ANH DUNG: 
   public function formatCollectionCustomer($model){       
       $res = '';
       if($model->collection_customer>0){
           $res .= "<span class='collection_customer display_none'>$model->collection_customer</span>".ActiveRecord::formatCurrency($model->collection_customer);
       }
       return $res;
   }    
   // May 20, 2014 ANH DUNG: 
   public function formatReceivablesCustomer($model){       
       $res = '';
       if($model->receivables_customer>0){
           $res .= "<span class='receivables_customer display_none'>$model->receivables_customer</span>".ActiveRecord::formatCurrency($model->receivables_customer);
       }
       return $res;
   }    
   
   // May 20, 2014 ANH DUNG: 
   public function formatGasRemainHasExport($model){       
       $res = GasRemain::$TYPE_HAS_EXPORT[0];
       if($model->has_export){
           $res = "<span class='item_b hight_light'>".GasRemain::$TYPE_HAS_EXPORT[1]."</span>";
       }
       return $res;
   }    
 
   // Jun 30, 2014 ANH DUNG
   public function formatDetailFileScanInfo($model){
       $str = '';
       foreach($model->rFileScanDetail as $key=>$item){
            $aDate = explode('-', $item->maintain_date);
            $pathUpload = "upload/file_scan/$aDate[0]/$aDate[1]/$aDate[2]";            
            $path = '/' . $pathUpload . '/size2' . '/' . $item->file_name;
//            if (file_exists(Yii::getPathOfAlias("webroot") . $path)) {
            if (is_file(Yii::getPathOfAlias("webroot") . $path)) {
                $str.="<a class='gallery' href='".Yii::app()->createAbsoluteUrl('admin/ajax/viewImageProfileHs', array('id'=>$item->id, 'model'=>'GasFileScanDetail'))."'> ";
                    $str.="<img width='80' height='60' src='".ImageProcessing::bindImageByModel($item,'','',array('size'=>'size2'))."'>";
                $str.="</a>";
            }
        }
       foreach($model->rDetailInfo as $key=>$item){
            $str .= "<br> <b>".($key+1).". $item->customer_name </b> - {$item->customer_phone} - <b>Gas:$item->materials_name - Seri: $item->seri</b> - ".$item->customer_address ." - $item->note";
       }
       return $str;
//       return trim($str,"<br>");
   }
   
   // Jul 25, 2014 ANH DUNG: 
   public function formatSpancopB12($model){       
       $res = '';
       if($model->b12>0){
           $res .= "<span class='amount_b12 display_none'>$model->b12</span>".$model->b12;
       }
       return $res;
   }       
   // Jul 25, 2014 ANH DUNG: 
   public function formatSpancopB45($model){       
       $res = '';
       if($model->b45>0){
           $res .= "<span class='amount_b45 display_none'>$model->b45</span>".$model->b45;
       }
       return $res;
   }       
   
   // Jul 30, 2014 ANH DUNG: 
   public function formatSpancopCustomerName($model){
       $cmsFormater = new CmsFormatter();
       $newShow = $cmsFormater->formatSpancopCustomerSpecial($model);
       $res = $newShow;
       if($model->status == GasBussinessContract::STATUS_THUONG_LUONG){
           $OverDate = MyFormat::compareTwoDate(date('Y-m-d'), $model->date_plan);           
           if($model->still_thuong_luong || ($OverDate && $model->still_thuong_luong==0) ){
                $res = "<span class='high_light_tr'></span>".$newShow;
           }
       }
       return $res;
   }
   
   // Jul 30, 2014 ANH DUNG: 
    public function formatSpancopAddress($model){       
       $res = "<b>CF: ";
       $address = nl2br($model->address);
       if($model->belong_to_id == $model->id){
           $res .= "$model->code_no</b>";
       }elseif( $model->belong_to_id ){
           $mFirst = $model->rBelongToId;
           if($mFirst){
                $res .= "$mFirst->code_no</b>";
           }
       }
       return $res."<br>$address";
   }
   
    public function formatSpancopCustomerSpecial($model){
       $res = "$model->customer_name";
       if(!empty($model->phone)){
           $res .= "<br><b>ĐT:</b>$model->phone";
       }
       if(!empty($model->customer_contact)){
           $res .= "<br><b>Liên Hệ:</b>$model->customer_contact";
       }
       if(!empty($model->customer_zone)){
           $res .= "<br><b>Chuyên Đề: </b>".GasBussinessContract::GetCustomerZoneView($model->customer_zone);
       }
       
       return $res;
   }
   
    public function formatSpancopReport($model){
        $cmsFormater = new CmsFormatter();
       $res = "";
       foreach($model->rComment as $mComment){
//           $res .= "<b>{$mComment->getUidLogin()}</b> <i>{$cmsFormater->formatDateTime($mComment->created_date)}</i>: ".nl2br($mComment->getContent())."<br>";
           $res .= $mComment->ShowItem();
       }
       $note = "<b>Ghi chú: </b>".nl2br($model->note);
       $res = "$res".$note;
       return $res;
   }
   
    public function formatStorecardNote($model){
       $note = nl2br($model->note);
       $update_note = nl2br($model->update_note);
       $res = $note." $update_note";
       if(!empty($note)){
           $res = $note."<br> $update_note";
       }
       return $res;
   }   
   
   // Aug 22, 2014 ANH DUNG: 
    public function formatFullNameTrackLogin($model){       
       $res = '';
       if($model->rUidLogin){
           $res = $model->rUidLogin->first_name;
           if(in_array($model->type, GasTrackLogin::$TYPE_RISK) )
                $res = "<span class='high_light_tr'></span>".$res;
       }        
       return $res;
   }
   
    // Sep 05, 2014 ANH DUNG
    public function formatGasManageToolDetail($model){
       $str = '';
       
        foreach($model->rManageToolDetail as $key=>$item){
            $str .= "<br> <b>".($key+1).". </b>Diễn giải: {$item->rMaterial->name} - <b>SL:</b> ".ActiveRecord::formatCurrency($item->qty);
        }
       return trim($str,"<br>");
   }
   
   // Sep 06, 2014 ANH DUNG
    public function formatDetailGasProfile($model){
       $str = '';
        foreach($model->rProfileDetail as $key=>$item){
//            $str.="<a class='gallery' href='".ImageProcessing::bindImageByModel($item,'','',array('size'=>'size2'))."'> ";
//                $str.="<img width='100' height='70' src='".ImageProcessing::bindImageByModel($item,'','',array('size'=>'size1'))."'>";
//            $str.="</a>";            
            $str.="<a class='gallery' href='".Yii::app()->createAbsoluteUrl('admin/ajax/viewImageProfileHs', array('id'=>$item->id))."'> ";
                $str.="<img width='80' height='60' src='".ImageProcessing::bindImageByModel($item,'','',array('size'=>'size1'))."'>";
            $str.="</a>";
        }
       return $str;
   }
   
   // Sep 06, 2014 ANH DUNG
   public function formatGasProfileMaterial($model){
       $str = '';
       $aId = explode(',', $model->list_materials_id);
       $aRes = GasMaterials::getListOptionByArrId($aId);
       $i=1;
       foreach($aRes as $item){
           $str .= "<br> <b>".($i++).". </b>: {$item}";
       }
       return trim($str,"<br>");
   }
   
   // Sep 06, 2014 ANH DUNG
    public function formatNameAndRole($mUser){
       $session=Yii::app()->session;
       if(!empty($mUser) && !empty($mUser->role_id)){
           return $mUser->first_name.' - '. $session['ROLE_NAME_USER'][$mUser->role_id];
       }
       return '';
   }
   
   // Sep 27, 2014 ANH DUNG $model is model Leave
    public function formatLeaveDate($model){
       $cmsFormater = new CmsFormatter();
       if($model->leave_date_from == $model->leave_date_to){
           return $cmsFormater->formatDate($model->leave_date_to);
       }
       return "Từ ".$cmsFormater->formatDate($model->leave_date_from)." Đến ".$cmsFormater->formatDate($model->leave_date_to);
   }
   
   // Sep 27, 2014 ANH DUNG
   public function formatLeaveStatus($model){
//       if($model->status == GasLeave::STA_APPROVED_BY_MANAGE && Yii::app()->user->role_id==ROLE_DIRECTOR){
//           return GasLeave::$LIST_STATUS_TEXT[GasLeave::STA_NEW];
//       }
       return GasLeave::$LIST_STATUS_TEXT[$model->status];
   }
   
   // Sep 28, 2014 ANH DUNG
   public function formatLeaveStatusDateApproved($model){
       $cmsFormater = new CmsFormatter();
       if(!empty($model->approved_director_date)){
           return $cmsFormater->formatDateTime($model->approved_director_date);
       }
       return $cmsFormater->formatDateTime($model->manage_approved_date);
   }
   
   public function formatLeaveUserApproved($model){
       $cmsFormater = new CmsFormatter();
       $mUser = null;
       if($model->rApprovedDirectorId){
           return $cmsFormater->formatOnlyNameUser($model->rApprovedDirectorId);
       }elseif($model->rManageApprovedUid){
           return $cmsFormater->formatOnlyNameUser($model->rManageApprovedUid);
       }elseif($model->rToUidApproved){
           return $cmsFormater->formatOnlyNameUser($model->rToUidApproved);
       }
       return '';
   }
   
   
    // Nov 04, 2014 ANH DUNG
    public function formatSalesFileScanNote($model){
       $str = '';
       foreach($model->rFileScanDetail as $key=>$item){
            $aDate = explode('-', $item->date_sales);            
            $pathUpload = GasSalesFileScanDetai::$pathUpload."/$aDate[0]/$aDate[1]/$aDate[2]";
            $path = '/' . $pathUpload . '/size2' . '/' . $item->file_name;
//            if (file_exists(Yii::getPathOfAlias("webroot") . $path)) {
            if (is_file(Yii::getPathOfAlias("webroot") . $path)) {
                $str.="<a class='gallery' href='".Yii::app()->createAbsoluteUrl('admin/ajax/viewImageProfileHs', array('id'=>$item->id, 'model'=>'GasSalesFileScanDetai'))."'> ";
                    $str.="<img width='80' height='60' src='".ImageProcessing::bindImageByModel($item,'','',array('size'=>'size2'))."'>";
                $str.="</a>";
            }
        }
       $str.= "<br>$model->note";
       return $str;
//       return trim($str,"<br>");
   }
   
   // Nov 23, 2014 ANH DUNG
    public function formatCustomerCheckMonth($model){
        return $model->month."-$model->year";
    }
    
    // Nov 24, 2014 ANH DUNG
    public function formatCustomerCheckFileReport($model){
        $str = '';
        if(!empty($model->file_report)){
            $year = MyFormat::GetYearByDate($model->created_date);
            $month = MyFormat::GetYearByDate($model->created_date, array( 'format'=>'m') );
            $pathUpload = GasCustomerCheck::$pathUpload."/$year/$month";
            $path = '/' . $pathUpload . '/size2' . '/' . $model->file_report;
            if (is_file(Yii::getPathOfAlias("webroot") . $path)) {
                $str.="<a class='gallery' href='".Yii::app()->createAbsoluteUrl('admin/ajax/viewImageProfileHs', array('id'=>$model->id, 'model'=>'GasCustomerCheck'))."'> ";
                    $str.="View";
                $str.="</a>";
            }
        }
        return $str;
    }
   
    // Dec 02, 2014 ANH DUNG
    public function formatAgentProfileCheck($model){
        $str = "<span class='hight_light item_b'>Chưa Có</span>";
        if(count($model->rProfileAgent)){
            $str = "<span class=''>Có Rồi</span>";
        }
        return $str;
    }
 
    // Feb 25, 2015 ANH DUNG
    public function formatIssueFile($model){
        $aFile = $model->rFileDetail;
        $str = '';
        if(count($aFile))
            $str = 'File đính kèm: ';
        foreach($aFile as $key=>$item){
            $str.="<a class='gallery' href='".Yii::app()->createAbsoluteUrl('admin/ajax/viewImageProfileHs', array('id'=>$item->id, 'model'=>'GasIssueTicketsDetailFile'))."'> ";
                $str.="<img width='80' height='60' src='".ImageProcessing::bindImageByModel($item,'','',array('size'=>'size1'))."'>";
            $str.="</a>";
        }
       return $str;
   }
   
    // Mar 06, 2015 ANH DUNG
    public function formatBreakTaskDailyFile($model, $needMore = array()){
        $aFile = $model->rFile;
        $str = '';
        $width = isset($needMore['width']) ? $needMore['width'] : 80;
        $height = isset($needMore['height']) ? $needMore['height'] : 60;
        if(count($aFile)){
            $str = '<br>File: ';
        }
        foreach($aFile as $key=>$item){
            $str.="<a target='_blank' class='gallery' href='".Yii::app()->createAbsoluteUrl('admin/ajax/viewImageProfileHs', array('id'=>$item->id, 'model'=>'GasFile'))."'> ";
//                $str.="<img width='$width' height='$height' src='".ImageProcessing::bindImageByModel($item,'','',array('size'=>'size1'))."'>";
                $str.= 'Hình '.($key+1);
            $str.="</a>";
        }
       return $str;
   }
   
    /**
     * @Author: ANH DUNG Jul 23, 2016
     * @Todo: format image at gas file by relation
     * @param: $rFile is relation with model GasFile
     */
    public function getViewGasFile($aFile, $needMore = array()){
        $str = '';
        $width = isset($needMore['width']) ? $needMore['width'] : 80;
        $height = isset($needMore['height']) ? $needMore['height'] : 60;
        if(count($aFile)){
            $str = '<br>File: ';
        }
        foreach($aFile as $key=>$item){
            $str.="<a target='_blank' class='gallery' href='".Yii::app()->createAbsoluteUrl('admin/ajax/viewImageProfileHs', array('id'=>$item->id, 'model'=>'GasFile'))."'> ";
                $str.="<img width='$width' height='$height' src='".ImageProcessing::bindImageByModel($item,'','',array('size'=>'size1'))."'>";
            $str.="</a>";
        }
       return $str;
   }
   
   // Mar 15, 2015 ANH DUNG
   public function formatDetailDailyGobackInfo($model){
       // init session cua pttt
       $str = '';
       $session=Yii::app()->session;
       foreach($model->rDetail as $key=>$item){
            $name = isset($session['SESSION_LIST_NAME_BY_ROLE'][$item->maintain_employee_id])?$session['SESSION_LIST_NAME_BY_ROLE'][$item->maintain_employee_id]:'';
            $str .= "<br> <b>".($key+1).". $name </b> - SL: $item->qty";
       }
//       return $str;
       return trim($str,"<br>");
   }
   
   public function formatSaleAndLevel($model){
       $mSale = $model->sale;
       if($mSale){
           $typeSale = isset(Users::$aTypeSale[$mSale->gender])?Users::$aTypeSale[$mSale->gender]:'';
           if($model->md5pass == 'for_export'){// cờ xác định là get data cho export ExportList::CustomerStoreCard
               return "$mSale->first_name";
           }
           return "<b>$mSale->first_name</b><br>[$typeSale]";
       }
       return '';
   }
   
    // Feb 25, 2015 ANH DUNG
    public function formatCustomerCheckFile($model){
        $str = '';
        if(!empty($model->file_report)){
//            $str = 'File đính kèm hiện tại: ';
            $str.="<a class='gallery' href='".Yii::app()->createAbsoluteUrl('admin/ajax/viewImageProfileHs', array('id'=>$model->id, 'model'=>'GasCustomerCheck'))."'> ";
//                $str.="<img width='80' height='60' src='".ImageProcessing::bindImageByModel($model,'','',array('size'=>'size2'))."'>";
                $str.="Xem";
            $str.="</a>";
        }
       return $str;
   }
   
   // Apr 20, 2015 ANH DUNG: 
   public function formatCustomerCheckMoneyGet($model){       
       $res = '';
       if($model->money_get>0){
           $res .= "<span class='money_get display_none'>$model->money_get</span>".ActiveRecord::formatCurrency($model->money_get);
       }
       return $res;
   }
   
   // May 14, 2015 ANH DUNG
   public function formatStatusLayHang($model){
       if($model->status == STATUS_REJECT){
           return $model->getStatusText();
       }
       if(isset(Users::$STATUS_LAY_HANG[$model->channel_id])){
           return Users::$STATUS_LAY_HANG[$model->channel_id];
       }
       $aCheck = array(STATUS_ACTIVE, STATUS_WAIT_ACTIVE, STATUS_REJECT);
       if(in_array($model->status, $aCheck) ){
           return $model->getStatusText();
       }
       return '';
   }
   
   // May 14, 2015 ANH DUNG
   public function formatCustomerCheckNameUser($model){
       $cmsFormater = new CmsFormatter();
       $mUser = $model->rUidLogin;
       if($model->role_id == ROLE_SUB_USER_AGENT){
           $mUser = $model->rAgent;
       }
       return $cmsFormater->formatOnlyNameUser($mUser);
   }
   
   
   // Jun 13, 2015 ANH DUNG
   public function formatBussinessContractStatus($model){
       $res = GasBussinessContract::$ARR_STATUS[$model->status];
       if(!empty($model->old_status)){
           $res = "<span class='item_b'>".$model->old_status." => </span>$res";
       }
       return $res;
   }
   
   // Jun 19, 2015 ANH DUNG
   public function formatOrderViewCreateBy($model){
       $str = '';
        $str.="<a href='".Yii::app()->createAbsoluteUrl('admin/gasOrders/view', array('id'=>$model->id, 'view_create_by'=>1))."'> ";
            $str.="Xem";
        $str.="</a>";
       return $str;
   }
   
 
      
   
}