<?php

class SendEmail {
    const MAIL_REQUEST_RESET_PASS       = 1;
    const MAIL_RESET_PASS               = 3; // mail reset pass send luc 2h AM
    const MAIL_TEXT_ALERT               = 4; // gửi mail alert khi có văn bản mới
    const MAIL_LEAVE_ALERT              = 5; // gửi mail alert cho user quản lý khi có người thuộc cấp quản lý đó  nghỉ phép
    const MAIL_SUPPORT_CUSTOMER_CHANGE_STATUS = 6; // gửi mail notify cho user lien quan khi support customer change status
    const MAIL_ISSUE_TICKET_CHANGE      = 7; // gửi mail notify cho user lien quan khi issue ticket có reply
    const MAIL_ID_NEW_CUSTOMER          = 8; // gửi mail cho điều phối + 2 nv kế toán khi có customer mới dc tạo
    const MAIL_ID_SETTLE_ALERT          = 9; // Anh Dung Jul 10, 2016 gửi mail cho user lien quan khi co thay doi tren quyet toan
    const MAIL_ID_SHELL_RETURN_PLANING  = 10; // template send shell return - Anh Dung fix id = 10
    const MAIL_PHAP_LY_UPDATE           = 11; // template alert pháp lý có update
    const MAIL_DEV_ERRORS               = 12; // template báo bug cho dev
    const MAIL_STATUS_CUSTOMER_SPECIAL  = 13; // new or  change status KH đặc biệt
    const MAIL_STATUS_PRICE_REQUEST     = 14; //Đề xuất tăng giảm giá KH Bò Mối
    const MAIL_QUOTES                   = 15; // Báo giá gas Cty Dầu Khí Miền Nam
    const MAIL_MEMBER_NEW               = 16; // mail tài khoản nhân viên
    const MAIL_MEMBER_LEAVE             = 17; // mail nhân viên nghỉ việc
    const MAIL_EMPLOYEE_NEW             = 18; // mail nhân viên mới
    
    // reset pass of agent (hoặc các loại user khác) 1 ngày 1 lần = cron job
    public static function ResetPassByRole(){
        $from = time();
        $aRole = array(ROLE_SUB_USER_AGENT);
        $aUid = array(
            136839, // NV Giám Sát Admin Ngọc Kiên
            142134, // NV Giám Sát dungninhbinh
        );
        $criteria = new CDbCriteria();
//        $criteria->compare('t.status', STATUS_ACTIVE); // không nên có đk này,vì sẽ xử lý user login 2 chỗ bị đẩy ra, khi đó sẽ inactive user và send mail reset pass
        $criteria->addInCondition('t.role_id', $aRole);
        $criteria->addInCondition('t.id', $aUid, 'OR'); // for test send mail
        $models = Users::model()->findAll($criteria);
        foreach($models as $model){
            SendEmail::ResetPasswordModelAndSendMail($model);
        }
        
        $to = time();
        $second = $to-$from;
        $ResultRun = "Mail ResetPassByRole: ".count($models).' done in: '.($second).'  Second  <=> '.round($second/60, 2).' Minutes';
        Logger::WriteLog($ResultRun);        
    }
    
    // to do: reset pass + update model + send mail
    // @param: $model is model user
    public static function ResetPasswordModelAndSendMail($model){
        if(trim($model->email)!=''){
            Users::ResetPassword($model);
            SendEmail::ResetPassword($model);
        }
    }
    
    /**
     * @Author: ANH DUNG Now 20, 2014:
     * @Todo: send mail alert khi có văn bản mới cho user nào có mail
     * @Param: $mUser
     * @Param: $mText
     */
    public static function TextAlert($mUser, $mText){
        if(trim($mUser->email)!=''){
            SendEmail::TextAlertSend($mUser, $mText);
        }
    }

    public static function ResetPassword($model) {
         $aBody = array(
            '{NAME}' => $model->first_name,
            '{EMAIL}' => $model->email,
            '{DATE_APPLY}' => date('d-m-Y'),
            '{PASSWORD_NEW}' => $model->temp_password,
        );   
        $aSubject = array(
            '{NAME}' => $model->first_name,
            '{DATE_APPLY}' => date('d-m-Y'),
        );
        CmsEmail::sendmail(SendEmail::MAIL_RESET_PASS, $aSubject, $aBody, $model->email);
    }
    
    /**
     * @Author: ANH DUNG Nov 20, 2014
     * @Todo: belong to TextAlert
     */
    public static function TextAlertSend($mUser, $mText) {
        if(is_null($mText)){
            return ;
        }
        $cmsFormater = new CmsFormatter();
        $date_published = $cmsFormater->formatDate( $mText->date_published );
        $type           = $mText->getTypeShow();
        $Title          = "$mText->short_content - $type $mText->number_sign - Ngày Ban Hành: $date_published";
//        $Title = "Văn Bản Mới: $type $mText->number_sign - Ngày Ban Hành: $date_published";
//        $LinkView = Yii::app()->createAbsoluteUrl("admin/gasText/text_view", array('id' => $mText->id));
        $LinkView = SyncData::DOMAIN_LIVE_NOIBO. "/admin/gasText/text_view/id/{$mText->id}";
        $LinkView = "<a href='$LinkView' target='_blank'>Click Here To View</a>";
        $aBody = array(
            '{NAME}' => $mUser->first_name,
            '{EMAIL}' => $mUser->email,
            '{DATE_PUBLISHED}' => $date_published, // Ngày ban hành
            '{TITLE}' => $Title,
            '{SHORT_CONTENT}' => $mText->short_content, // trích yếu tóm tắt
            '{LINK_VIEW}' => $LinkView, // LINK VIEW DETAIL
        );   
        $aSubject = array(
            '{NAME}' => $mUser->first_name,
            '{TITLE}' => $Title,
        );
        CmsEmail::sendmail(SendEmail::MAIL_TEXT_ALERT, $aSubject, $aBody, $mUser->email);
    }
    
    /**
     * @Author: ANH DUNG Nov 21, 2014
     * @Todo: test send leave alert
     */
    public static function TestLeaveAlertSend() {
        $mUser = Users::model()->findByPk(142134);
        $mLeave = GasLeave::model()->findByPk(39);
        self::LeaveAlertSend($mUser, $mLeave);
    }
    
    /**
     * @Author: ANH DUNG Nov 21, 2014
     * @Todo: gửi mail alert cho user quản lý khi có người thuộc cấp quản lý đó nghỉ phép
     * @param model $mUser model user quản lý sẽ send mail notify
     * @param model $mLeave model leave - người nghỉ
     */
    public static function LeaveAlertSend($mUser, $mLeave) {
        if(trim($mUser->email) == '') return ;
        $cmsFormater = new CmsFormatter();
        $date  = $cmsFormater->formatLeaveDate( $mLeave );
        $LEAVE_NAME = '';
        $LEAVE_POSITION = '';
        $mUserLeave = $mLeave->rUidLeave;
        if( $mUserLeave ){
            $LEAVE_NAME = $mUserLeave->first_name;
            $LEAVE_POSITION = Roles::GetRoleNameById( $mUserLeave->role_id );
        }
        
        $aBody = array(
            '{NAME}' => $mUser->first_name,
            '{EMAIL}' => $mUser->email,
            '{LEAVE_NAME}' => $LEAVE_NAME, // ten nguoi nghi
            '{LEAVE_POSITION}' => $LEAVE_POSITION,// chuc vu
            '{LEAVE_DATE}' => $date, // ngay nghi
            '{LEAVE_CONTENT}' => $mLeave->leave_content, // ly do
            '{LEAVE_STATUS}' => $mLeave->getStatus(),
        );   
        $aSubject = array(
            '{NAME}' => $mUser->first_name,
            '{LEAVE_NAME}' => $LEAVE_NAME, // ten nguoi nghi
            '{LEAVE_POSITION}' => $LEAVE_POSITION,// chuc vu
        );
        CmsEmail::sendmail(SendEmail::MAIL_LEAVE_ALERT, $aSubject, $aBody, $mUser->email);
    }
    
    /**
     * @Author: ANH DUNG Dec 30, 2014
     * @Todo: mail cron khi SupportCustomer change status
     */
    public static function SupportCustomerChangeStatus($mUser, $mSupportCustomer) {
        $mSupportCustomer->from_cron_mail = 1;
        $cmsFormater = new CmsFormatter();
        $date_request  = $cmsFormater->formatDate( $mSupportCustomer->date_request );
        $customer_name = $cmsFormater->formatNameUser($mSupportCustomer->rCustomer);
        $status = GasSupportCustomer::GetStatusSupport($mSupportCustomer);
        $approved_name = $mSupportCustomer->getLastUpdateStatus();
        $approved_name = str_replace("<br>", " - ", $approved_name);
        $item_name = $mSupportCustomer->formatGasSupportCustomerItemName();
        $detail = $mSupportCustomer->SupportCustomerInfo();
        $Title = "Đề xuất hỗ trợ KH: $customer_name - [$status]";
        $time_doing_real = MyFormat::datetimeDbDatetimeUser($mSupportCustomer->time_doing_real);
        $last_note = GasSupportCustomer::GetLastUpdateNote($mSupportCustomer);
        $aBody = array(
            '{NAME}' => $mUser->first_name,
            '{EMAIL}' => $mUser->email,
            '{CUSTOMER_NAME}' => $customer_name,
            '{DATE_REQUEST}' => $date_request, // Ngày Đề xuất
            '{TIME_DOING_REAL}' => $time_doing_real, // Ngày THI CONG THUC TE
            '{STATUS}' => $status,
            '{ARPPROVED_NAME}' => $approved_name, // nguoi duyet va thoi gian
            '{ITEM_NAME}' => $item_name."<hr><b>Chi Tiết Khách Hàng: </b><br>$detail", // 
            '{TITLE}' => $Title, // 
            '{LAST_NOTE}' => $last_note, // GHI CHU MOI NHAT
            '{DELOY_BY}' => $mSupportCustomer->getDeloyHtml(), // Sep 23, 2015 Đơn vị thực hiện 
        );   
        $aSubject = array(
            '{NAME}' => $mUser->first_name,
            '{TITLE}' => $Title,
        );
        CmsEmail::sendmail(SendEmail::MAIL_SUPPORT_CUSTOMER_CHANGE_STATUS, $aSubject, $aBody, $mUser->email);
    }    
    
    /**
     * @Author: ANH DUNGFeb 25, 2015
     * @Todo: mail cron khi issue change reply 
     */
    public static function IssueTicketReply($mUser, $mGasIssueTicketsDetail) {
        $cmsFormater = new CmsFormatter();
        $mGasIssueTickets = GasIssueTickets::model()->findByPk($mGasIssueTicketsDetail->ticket_id);
        if(is_null($mGasIssueTickets)){
            return ;
        }
        $mCustomer = $mGasIssueTickets->rCustomer;
        $date_reply  = $cmsFormater->formatDate( $mGasIssueTicketsDetail->created_date );
        $customer_name = $cmsFormater->formatNameUserWithTypeBoMoi($mCustomer);
        $uIdPost = $mGasIssueTicketsDetail->rUidPost;
        $name_user_login = $cmsFormater->formatNameUser($uIdPost);
        $POSITION = '';
        if($uIdPost){
            $POSITION = Roles::GetRoleNameById( $uIdPost->role_id );
        }
        $DetailIssue = $mGasIssueTickets->getDetailIssue($mGasIssueTicketsDetail);
        
        $aBody = array(
            '{NAME}' => $mUser->first_name,
            '{EMAIL}' => $mUser->email,
            '{CUSTOMER_NAME}' => $customer_name,
            '{DATE_REPLY}' => $date_reply, // Ngày REPLY
            '{MESSAGE}' => $mGasIssueTicketsDetail->getMessage() . $DetailIssue, // MESSAGE REPLY
            '{NAME_USER_LOGIN}' => $name_user_login." - $POSITION", // MESSAGE REPLY
            '{TITLE}' => $mGasIssueTickets->title, // 
        );   
        $aSubject = array(
            '{NAME}' => $mUser->first_name,
            '{TITLE}' => $mGasIssueTickets->title, //
            '{CUSTOMER_NAME}' => $customer_name,
        );
        CmsEmail::sendmail(SendEmail::MAIL_ISSUE_TICKET_CHANGE, $aSubject, $aBody, $mUser->email);
    }
    
    /**
     * @Author: ANH DUNG Apr 12, 2016
     * @Todo: gửi mail alert cho user điều phối + 2 nv kế toán khi có KH mới dc tạo do sale or CCS
     * @param model $mUser model user điều phối + 2 nv kế toán 
     * @param model $mCustomer model new customer
     */
    public static function NewCustomer($mUser, $mCustomer) {
        if(trim($mUser->email) == '') return ;
        $aBody = array(
            '{NAME}' => $mUser->getFullName(),
            '{CUSTOMER_NAME}' => $mCustomer->getFullName(),
            '{CUSTOMER_ADD}' => $mCustomer->getAddress(), // dia chi KH
            '{CUSTOMER_PHONE}' => $mCustomer->getPhone(),
            '{CUSTOMER_TYPE}' => $mCustomer->getTypeCustomerText(),
            '{CUSTOMER_SALE}' => $mCustomer->getSaleName(),
            '{CUSTOMER_PRICE}' => $mCustomer->getPrice(),
            '{CUSTOMER_AGENT}' => $mCustomer->getAgentOfCustomer(),
            '{CUSTOMER_HAN_TT}' => $mCustomer->getMasterLookupName("payment"),
            '{CUSTOMER_CONTACT_PERSON_NAME}' => $mCustomer->getUserRefField("contact_person_name"),
            '{CREATED_BY}' => $mCustomer->getCreatedBy(),
            '{CREATED_DATE}' => $mCustomer->getCreatedDate(),
            '{LAST_PURCHASE}' => $mCustomer->getLastPurchase(),
            '{CUSTOMER_NOTE}' => $mCustomer->getUserRefField("contact_note"),// Đặc điểm KH
            
        );
        $aSubject = array(
            '{NAME}' => $mUser->getFullName(),
            '{CUSTOMER_NAME}' => $mCustomer->getFullName(),
            '{CUSTOMER_ADD}' => $mCustomer->getAddress(), // dia chi KH
            '{CUSTOMER_PHONE}' => $mCustomer->getPhone(),
            '{CUSTOMER_TYPE}' => $mCustomer->getTypeCustomerText(),
            '{CUSTOMER_SALE}' => $mCustomer->getSaleName(),
        );
        CmsEmail::sendmail(SendEmail::MAIL_ID_NEW_CUSTOMER, $aSubject, $aBody, $mUser->email);
    }
    
    /**
     * @Author: ANH DUNG Jul 10, 2016
     * @Todo: gửi mail alert cho user điều phối + 2 nv kế toán khi có KH mới dc tạo do sale or CCS
     * @param model $mUser model user điều phối + 2 nv kế toán 
     * @param model $mCustomer model new customer
     */
    public static function SettleAlert($mUser, $mSettle) {
        if(trim($mUser->email) == '') return ;
        $BANK_CONTENT = "";
        if ($mSettle->type == GasSettle::TYPE_BANK_TRANSFER){
            $BANK_CONTENT = "<b>- Đơn vị hưởng thụ: </b>{$mSettle->getBankUser()}. <b>Số tài khoản: </b>{$mSettle->getBankNumber()}. <b>Tại ngân hàng: </b>{$mSettle->getBankName()}";
        }
        $TITLE = "{$mSettle->getTypeText()} - {$mUser->getFullName()} - Trạng Thái: {$mSettle->getStatusText()}";
        
        $aBody = array(
            '{NAME}' => $mUser->getFullName(),
            '{SETTLE_TYPE}' => $mSettle->getTypeText(),
            '{STATUS}' => $mSettle->getStatusText(),
            '{REASON}' => $mSettle->getReason(),
            '{AMOUNT}' => $mSettle->getAmount(),
            '{BANK_CONTENT}' => $BANK_CONTENT,
            '{UID_LOGIN}' => $mSettle->getUidLoginName(),
            '{COMPANY_NAME}' => $mSettle->getCompany(),
            
        );
        $aSubject = array(
            '{NAME}' => $mUser->getFullName(),
            '{TITLE}' => $TITLE,
        );
        CmsEmail::sendmail(SendEmail::MAIL_ID_SETTLE_ALERT, $aSubject, $aBody, $mUser->email);
    }
    
    /**
     * @Author: ANH DUNG Aug 19, 2014
     * @Todo: dùng để test xem cron chạy đúng ko
     */
    public static function TestCron() {
//        $aMailTest = ['hanhnt@spj.vn', 'kiennn@spj.vn'];
        $aMailTest = ['dungnt@spj.vn'];
        $aBody = $aSubject = [];
        foreach($aMailTest as $emailTest){
            CmsEmail::sendmail(SendEmail::MAIL_REQUEST_RESET_PASS, $aSubject, $aBody, $emailTest);
        }
//        $MailTest1 = 'nguyendungww@gmail.com';
//        CmsEmail::sendmail(SendEmail::MAIL_REQUEST_RESET_PASS, $aSubject, $aBody, $MailTest1);
    }
    
    /** @Author: ANH DUNG Oct 02, 2016
     *  @Todo: dùng để send bug cho dev
     */
    public static function bugToDev($body, $needMore=[]) {
        $aMailDev   = ['dungnt@spj.vn'];
        $aBody      = ['{INFO}'=>$body];
        $aSubject   = [];
        if(isset($needMore['title'])){
            $aSubject['{TITLE}'] = $needMore['title'];
        }
        if(isset($needMore['list_mail'])){
            $aMailDev = $needMore['list_mail'];// list_mail is array
        }
        if(isset($needMore['SendNow'])){
            foreach($aMailDev as $email){
                CmsEmail::sendmail(SendEmail::MAIL_DEV_ERRORS, $aSubject, $aBody, $email);
            }
            return ;
        }
        
        GasScheduleEmail::buildNotifyDev($aMailDev, $aSubject, $aBody, $needMore);
//        foreach($aMailDev as $email){
//            CmsEmail::sendmail(SendEmail::MAIL_DEV_ERRORS, $aSubject, $aBody, $email);
//        }
//        $MailTest1 = 'nguyendungww@gmail.com';
//        CmsEmail::sendmail(SendEmail::MAIL_REQUEST_RESET_PASS, $aSubject, $aBody, $MailTest1);
    }
    
    /** @Author: ANH DUNG Dec 22, 2017
     *  @Todo: dùng để now
     */
    public static function testSendNow() {
        $aMailDev   = ['dungnt@spj.vn'];
        $aBody      = ['{INFO}'=>'test body'];
        $aSubject   = [];
        foreach($aMailDev as $email){
            CmsEmail::sendmail(SendEmail::MAIL_REQUEST_RESET_PASS, $aSubject, $aBody, $email);
        }
        $MailTest1 = 'nguyendungww@gmail.com';
    }

    /** @Author: ANH DUNG Mar 12, 2018
     *  @Todo: send mail cảnh báo hoàn thành trễ đơn hàng hộ GĐ và bò mối
     * @param: $model is model TransactionHistory or GasAppOrder
     **/
    public function alertCompleteOrderLate($model) {
        return ; // May2218 close không gửi nữa
        $codeError = '[E001]';
        $mAppCache = new AppCache();
        $ClassName = get_class($model);
        $province_id = 0;
        if($ClassName == 'TransactionHistory'){
            $title          = "$codeError HGĐ hoàn thành trễ ". $model->code_no_sell;
            $type           = 'HGĐ';
            $codeNo         = $model->code_no_sell;
            $province_id    = $model->province_id;
        }elseif($ClassName == 'GasAppOrder'){
            $title          = "$codeError Bò Mối hoàn thành trễ ". $model->code_no;
            $type           = 'Bò Mối';
            $codeNo         = $model->code_no;
            $province_id    = $model->province_id_agent;
        }else{
            throw new Exception('Model chưa được định nghĩa, cảnh báo hoàn thành trễ đơn hàng');
        }
        $aProvinceAllow = array_merge(GasOrders::$TARGET_ZONE_PROVINCE_TAY_NGUYEN, GasOrders::$PROVINCE_ZONE_HCM);
        
        if(!in_array($province_id, $aProvinceAllow)){
            return ;
        }
        
        $now = date('d/m/Y H:i');
        $minutes = MyFormat::getMinuteTwoDate($model->created_date, date('Y-m-d H:i:s'));
        $mEmployee  = $model->rEmployeeMaintain;
        $mCustomer  = $model->rCustomer;
        $aAgent     = $mAppCache->getAgentListdata();
        $needMore               = ['title'=> $title];
//        $needMore['SendNow']    = 1;// only dev debug
//        $needMore['list_mail']  = ['dungnt@spj.vn'];
        $needMore['list_mail']  = [];
        $this->limitMailByProvince($province_id, $needMore);
        $body = "Nhân viên {$mEmployee->first_name} hoàn thành đơn hàng trễ:<br>";
        $body .= "<br><br>Đại lý: ". (isset($aAgent[$model->agent_id]) ? $aAgent[$model->agent_id] : '' );
        $body .= "<br><br>Mã đơn hàng: $codeNo";
        $body .= "<br><br>Loại đơn hàng: $type";
        $body .= "<br><br>Tổng thời gian hoàn thành: $minutes phút";
        $body .= "<br><br>Khách hàng: $mCustomer->first_name";
        $body .= "<br><br>Địa chỉ: $mCustomer->address";
        $body .= "<br><br>Thời gian tạo đơn hàng: ". MyFormat::dateConverYmdToDmy($model->created_date, 'd/m/Y H:i');
        $body .= "<br><br>Thời gian hoàn thành: $now";
        $body .= "<br><br>Các bộ phận liên quan kiểm tra lại";
        SendEmail::bugToDev($body, $needMore);
    }
    
    /** @Author: ANH DUNG May 22, 2018
     *  @Todo: send mail cảnh báo bấm nợ đơn bò mối
     * @param: $mAppOrder is model GasAppOrder
     **/
    public function alertOrderDebit($mAppOrder) {
        if($mAppOrder->type != GasAppOrder::DELIVERY_NOW){
            return ;
        }
        $mAppCache      = new AppCache();
        $province_id    = $mAppOrder->province_id_agent;
        $codeError      = '[E002]';
        $title          = "$codeError Cảnh báo đơn hàng nợ: ". $mAppOrder->getCustomer('first_name');
        $mEmployee      = $mAppOrder->rEmployeeMaintain;
        $mCustomer      = $mAppOrder->rCustomer;
        $aAgent         = $mAppCache->getAgentListdata();
        $needMore               = ['title'=> $title];
        $needMore['list_mail']  = [];
        $this->orderGetSaleEmail($mAppOrder, $needMore);
//        $aProvinceAllow = array_merge(GasOrders::$TARGET_ZONE_PROVINCE_TAY_NGUYEN, GasOrders::$PROVINCE_ZONE_HCM, GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TRUNG);
//        if(!in_array($province_id, $aProvinceAllow)){
//            return ;
//        }
        $this->limitMailByProvince($province_id, $needMore);

//        $needMore['SendNow']    = 1;// only dev debug
//        $needMore['list_mail'][]        = 'dungnt@spj.vn';
        
        $body = "Nhân viên <b>{$mEmployee->first_name}</b> bấm đơn hàng nợ:<br>";
        $body .= "<br><br>Đại lý: ". (isset($aAgent[$mAppOrder->agent_id]) ? $aAgent[$mAppOrder->agent_id] : '' );
        $body .= "<br><br>Mã đơn hàng: $mAppOrder->code_no";
        $body .= "<br><br>Khách hàng: $mCustomer->first_name";
        $body .= "<br><br>NV kinh doanh: {$mCustomer->getSaleName()}";
        $body .= "<br><br>Địa chỉ: $mCustomer->address";
        $body .= "<br><br>Loại KH: {$mCustomer->getTypeCustomerText()}";
        $body .= "<br><br>Hạn thanh toán: {$mCustomer->getMasterLookupName("payment")}";
        $body .= "<br><br>Thời gian tạo đơn hàng: ". MyFormat::dateConverYmdToDmy($mAppOrder->created_date, 'd/m/Y H:i');
        $body .= "<br><br>Tổng tiền: ".$mAppOrder->getGrandTotal(true);
        $body .= "<br><br>Các bộ phận liên quan kiểm tra lại";
        $body .= $this->getHtmlOrderDetail($mAppOrder);
        SendEmail::bugToDev($body, $needMore);
    }
    
    /** @Author: ANH DUNG May 19, 2018
     *  @Todo: giới hạn mail gửi theo khu vực
     **/
    public function limitMailByProvince($province_id, &$needMore) {
        if(in_array($province_id, GasOrders::$PROVINCE_ZONE_HCM)){
//            $needMore['list_mail'][] = 'giamsat.dnb@spj.vn';// Aug1618 anh Dot noi bo ko gui
            $needMore['list_mail'][] = 'ketoan.kv.dnb@spj.vn';
            $needMore['list_mail'][] = 'tramptn@spj.vn';
//            $needMore['list_mail'][] = 'vinhvt@spj.vn';
        }elseif(in_array($province_id, GasOrders::$TARGET_ZONE_PROVINCE_TAY_NGUYEN)){
            $needMore['list_mail'][] = 'ketoan.kv.taynguyen@spj.vn';
        }elseif(in_array($province_id, GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TRUNG)){
            $needMore['list_mail'][] = 'ketoan.kv.mientrung@spj.vn';
        }elseif(in_array($province_id, GasOrders::$TARGET_ZONE_PROVINCE_MIEN_TAY)){
            $needMore['list_mail'][] = 'ketoan.kv.mientay@spj.vn';
        }
    }
    
    
    /** @Author: ANH DUNG May 26, 2018
     *  @Todo: get email of Sale
     *  @Param: $mAppOrder is model GasAppOrder
     **/
    public function orderGetSaleEmail($mAppOrder, &$needMore) {
        $saleEmail = '';
        if(in_array($mAppOrder->sale_id, GasLeave::getArraySaleIdCompany())){
            $saleEmail      = 'ducpv@spj.vn';
        }else{
            $mSale          = Users::model()->findByPk($mAppOrder->sale_id);
            if($mSale && !empty($mSale->email)){
                $saleEmail      = $mSale->email;
            }
        }
        if(!empty($saleEmail)){
            $needMore['list_mail'][]    = $saleEmail;
        }
    }
    
    /** @Author: ANH DUNG May 22, 2018
     *  @Todo: check send mail cảnh báo đơn bò mối: gas đi và vỏ về không bằng nhau
     * @param: $mAppOrder is model GasAppOrder
     **/
    public function checkOrderDebitVo($mAppOrder) {
        if($mAppOrder->isOrderAgent()){// Jul3019 DungNT close check ca DH xe tai va giao ngay
            return ;
        }
        $totalGas = $totalVo = $totalGasBinhBo = $totalGas12 = $totalVoBinhBo = $totalVo12 = 0;
        $aTypeGas = [
            GasMaterialsType::MATERIAL_BINH_12KG,
            GasMaterialsType::MATERIAL_BINH_50KG,
            GasMaterialsType::MATERIAL_BINH_45KG,
        ];
        $aTypeVo =  [
            GasMaterialsType::MATERIAL_VO_50,
            GasMaterialsType::MATERIAL_VO_45,
            GasMaterialsType::MATERIAL_VO_12,
        ];

        // check gas vỏ
        $aGas   = $mAppOrder->getJsonDataField('info_gas');
        $aVo    = $mAppOrder->getJsonDataField('info_vo');
        $aData  = array_merge($aGas, $aVo);
        foreach($aData as $item):
            $qty        = ($item['qty_real']*1);
            if(in_array($item['materials_type_id'], $aTypeGas)){
                $totalGas   += $qty;
            }elseif(in_array($item['materials_type_id'], $aTypeVo)){
                $totalVo    += $qty;
            }
            
            if(in_array($item['materials_type_id'], GasMaterialsType::getArrGasBB())){
                $totalGasBinhBo     += $qty;
            }elseif($item['materials_type_id'] == GasMaterialsType::MATERIAL_BINH_12KG){
                $totalGas12         += $qty;
            }
            
            if(in_array($item['materials_type_id'], GasMaterialsType::$ARR_GAS_VO_LON)){
                $totalVoBinhBo      += $qty;
            }elseif($item['materials_type_id'] == GasMaterialsType::MATERIAL_VO_12){
                $totalVo12          += $qty;
            }
        endforeach;
        // tính số vỏ lớn, vỏ nhỏ nợ của đơn hàng
        $qtyVoDebitBinhBo   = $totalGasBinhBo - $totalVoBinhBo;
        $qtyVoDebit12       = $totalGas12 - $totalVo12;
        
        $mAppOrder->sListGas = [];
//        if($totalGas > 0 && $totalGas != $totalVo){
        if($totalGas > 0 && $totalGas > $totalVo){// Dec3018 vỏ có thể về nhiều hơn Gas // Dec0318 check gas đi và vỏ về KH mối
            // May2019 KH mối check như KH bò bên dưới - do KH mối của BìnhTV có set hạn mức vỏ
//            if($mAppOrder->type_customer == STORE_CARD_KH_MOI){// Dec0818 KH bò vẫn cứ email bình thường
//                throw new Exception('Gas đi và vỏ về không bằng nhau. KH bò mối không cho nợ vỏ, vui lòng liên hệ nhân viên kinh doanh, chuyên viên hoặc kế toán khu vực để được hỗ trợ');
//            }// Jan0519 tạm mở ra, để làm xong phần set hạn mức vỏ KH bò --> old Open for KH Bo Moi
//            if($mAppOrder->type_customer == STORE_CARD_KH_BINH_BO){// Jun1019 open, ko rõ sao lại chỉ check KH bò
                $this->checkDebitVoBinhBo($mAppOrder, $qtyVoDebitBinhBo, $qtyVoDebit12);
//            }
            $mAppOrder->sListGas = $aData; 
            $this->alertOrderDebitVo($mAppOrder);
        }
    }
    
    /** @Author: DungNT Jan 16, 2019
     *  @Todo: check hạn mức nợ vỏ của KH bình bò 
     **/
    public function checkDebitVoBinhBo($mAppOrder, $qtyVoDebitBinhBo, $qtyVoDebit12) {
        $mInventoryCustomer = new InventoryCustomer();
        $mInventoryCustomer->customer_id = $mAppOrder->customer_id;
        $mSetupLimitVo = $mInventoryCustomer->getDebitVoOfCustomer();
        
        if(empty($mSetupLimitVo) && ($qtyVoDebitBinhBo > 0 || $qtyVoDebit12 > 0) ){
            throw new Exception("Gas đi và vỏ về không bằng nhau. KH bò mối không cho nợ vỏ, vui lòng liên hệ nhân viên kinh doanh, hoặc anh Đức - 0909 875 420 để hỗ trợ. E01 BB: $qtyVoDebitBinhBo v12: $qtyVoDebit12");
        }
        if($qtyVoDebitBinhBo < 1 && $qtyVoDebit12 < 1){
            return ;
        }
        if(empty($mSetupLimitVo)){
            SendEmail::bugToDev("Check case này empty(mSetupLimitVo), $mAppOrder->id qtyVoDebitBinhBo = $qtyVoDebitBinhBo +++ qtyVoDebit12 = $qtyVoDebit12 ");
            return ;
        }
        
        $json = json_decode($mSetupLimitVo->json_vo, true);
//         + $mSetupLimitVo->debit Binh 45
//         + $mSetupLimitVo->credit Binh 12
        $currentDebitBinhBo = $json['debit_vo_45'];
        $currentDebitBinh12 = $json['debit_vo_12'];
        $totalDebitBinhBo   = $currentDebitBinhBo + $qtyVoDebitBinhBo;
        $totalDebitBinh12   = $currentDebitBinh12 + $qtyVoDebit12;
        if($totalDebitBinhBo > $mSetupLimitVo->debit){
            SendEmail::bugToDev("Check case này empty totalDebitBinhBo > mSetupLimitVo->debit, $mAppOrder->id qtyVoDebitBinhBo = $qtyVoDebitBinhBo +++ qtyVoDebit12 = $qtyVoDebit12 ");
            throw new Exception("Gas đi và vỏ về không bằng nhau. KH này có hạn mức nợ vỏ lớn là {$mSetupLimitVo->debit}, vui lòng liên hệ nhân viên kinh doanh, hoặc anh Đức - 0909 875 420 để hỗ trợ");
        }
        if($totalDebitBinh12 > $mSetupLimitVo->credit){
            SendEmail::bugToDev("Check case này empty totalDebitBinh12 > mSetupLimitVo->credit, $mAppOrder->id qtyVoDebitBinhBo = $qtyVoDebitBinhBo +++ qtyVoDebit12 = $qtyVoDebit12 ");
            throw new Exception("Gas đi và vỏ về không bằng nhau. KH này có hạn mức nợ vỏ 12 là {$mSetupLimitVo->credit}, vui lòng liên hệ nhân viên kinh doanh, hoặc anh Đức - 0909 875 420 để hỗ trợ");
        }
        
    }
    
    /** @Author: ANH DUNG May 22, 2018
     *  @Todo: send mail cảnh báo đơn bò mối: gas đi và vỏ về không bằng nhau
     * @param: $mAppOrder is model GasAppOrder
     **/
    public function alertOrderDebitVo($mAppOrder) {
        $mAppCache      = new AppCache();
        $province_id    = $mAppOrder->province_id_agent;
        $codeError      = '[E003]';
        $title          = "$codeError Cảnh báo nợ vỏ: ". $mAppOrder->getCustomer('first_name');
        $mEmployee      = $mAppOrder->rEmployeeMaintain;
        $mCustomer      = $mAppOrder->rCustomer;
        $aAgent         = $mAppCache->getAgentListdata();
        $needMore               = ['title'=> $title];
        $needMore['list_mail']  = [];
        $this->orderGetSaleEmail($mAppOrder, $needMore);
        $this->limitMailByProvince($province_id, $needMore);
        
//        $needMore['SendNow']    = 1;// only dev debug
//        $needMore['list_mail'][]        = 'dungnt@spj.vn';
        
        $body = "Nhân viên <b>{$mEmployee->first_name}</b> hoàn thành đơn hàng lệch vỏ:<br>";
        $body .= "<br><br>Đại lý: ". (isset($aAgent[$mAppOrder->agent_id]) ? $aAgent[$mAppOrder->agent_id] : '' );
        $body .= "<br><br>Mã đơn hàng: $mAppOrder->code_no";
        $body .= "<br><br>Khách hàng: $mCustomer->first_name";
        $body .= "<br><br>NV kinh doanh: {$mCustomer->getSaleName()}";
        $body .= "<br><br>Địa chỉ: $mCustomer->address";
        $body .= "<br><br>Loại KH: {$mCustomer->getTypeCustomerText()}";
        $body .= "<br><br>Thời gian tạo đơn hàng: ". MyFormat::dateConverYmdToDmy($mAppOrder->created_date, 'd/m/Y H:i');
        $body .= "<br><br>Tổng tiền: ".$mAppOrder->getGrandTotal(true);
        $body .= "<br><br>Các bộ phận liên quan kiểm tra lại";
        $body .= $this->getHtmlOrderDetail($mAppOrder);
        SendEmail::bugToDev($body, $needMore);
        
    }
    
    /** @Author: ANH DUNG May 26, 2018
     *  @Todo: get html order bò mối detail
     **/
    public function getHtmlOrderDetail($mAppOrder) {
        $mAppCache      = new AppCache();
        $aMaterial      = $mAppCache->getMasterModel('GasMaterials', AppCache::ARR_MODEL_MATERIAL);
        $key=1;
        $html = "<table  cellpadding='0' cellspacing='0'> ";
            $html .= '<thead>';
                $html .= '<tr>';
    //          danh sách td
                    $html .= '<th style="padding: 3px; border:1px solid #000;font-size: 15px;font-weight: bold;text-align: center;">#</th>';
                    $html .= '<th style="padding: 3px; border:1px solid #000;font-size: 15px;font-weight: bold;text-align: center;">Vật tư</th>';
                    $html .= '<th style="padding: 3px; border:1px solid #000;font-size: 15px;font-weight: bold;text-align: center;">SL</th>';
                $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';
            if(is_array($mAppOrder->sListGas)){
                foreach ($mAppOrder->sListGas as $key => $item) {
                    $key++;
                    $materials_name = isset($aMaterial[$item['materials_id']]) ? $aMaterial[$item['materials_id']]['name'] : '';
                    $html .= '<tr>';
                        $html .= "<td style='padding: 3px; border:1px solid #000;text-align: center;'>".$key."</td>";
                        $html .= "<td style='padding: 3px; border:1px solid #000;'>". $materials_name."</td>";
                        $html .= "<td style='padding: 3px; border:1px solid #000;text-align: center;'>{$item['qty_real']}</td>";
                    $html .= '</tr>';
                }
            }
            $html .= '</tbody>';
        $html .= '</table>';
        return $html;
    }
    
    /** @Author: ANH DUNG Jul 12, 2018
     *  @Todo: xử lý mail notify Kinh doanh + Kế toán khi KH bị chặn hàng hoặc Ngược lại
     *  @Param: $mCustomer model User
     **/    
    public function notifyLockCustomer($mCustomer) {
//        error_reporting(1);
        if($mCustomer->modelOld->channel_id == $mCustomer->channel_id || !in_array($mCustomer->is_maintain, CmsFormatter::$aTypeIdMakeUsername)){
            return ;
        }
        $nameUserUpdate = Yii::app()->user->first_name;
        $cmsFormatter   = new CmsFormatter();
        $statusOld = $cmsFormatter->formatStatusLayHang($mCustomer->modelOld);
        $statusNew = $cmsFormatter->formatStatusLayHang($mCustomer);

        $needMore = ['title'=> "[Notify Chặn Hàng] chuyển [$statusOld -> $statusNew] - {$mCustomer->getFullName()}"];
        
//        $needMore['SendNow']            = 1;// only dev debug
//        $needMore['list_mail'][]        = 'dungnt@spj.vn';
        $needMore['list_mail']  = ['dieuphoi@spj.vn', 'ketoan.vanphong@spj.vn', 'kinhdoanh@spj.vn'];
        $body = "Nhân viên <b>{$nameUserUpdate}</b> chuyển trạng thái KH [$statusOld -> $statusNew]:<br>";
        $body .= $this->getHtmlCustomer($mCustomer);
        $body .= "<br><br>Trạng thái: {$statusNew}";
        $body .= "<br><br>Các bộ phận liên quan kiểm tra lại";
        SendEmail::bugToDev($body, $needMore);
        if($mCustomer->channel_id == Users::CHAN_HANG){
            $mCustomer->LoadUsersRef(); $today = date('d/m/Y H:i:s');
            $mCustomer->mUsersRef->reason_leave = "<br>$today $nameUserUpdate chặn hàng ".$mCustomer->mUsersRef->reason_leave;
            $mCustomer->SaveUsersRef();
        }
    }
    
    /** @Author: ANH DUNG Dec 10, 2018
     *  @Todo: get html info customer to send email 
     **/
    public function getHtmlCustomer($mCustomer) {
        $body = '';
        $body .= "<br><br><b>Khách hàng: $mCustomer->first_name </b>";
        $body .= "<br><br>NV kinh doanh: {$mCustomer->getSaleName()}";
        $body .= "<br><br>Địa chỉ: $mCustomer->address";
        $body .= "<br><br>Loại KH: {$mCustomer->getTypeCustomerText()}";
        $body .= "<br><br>Lấy hàng mới nhất: {$mCustomer->getLastPurchase()}";
        return $body;
    }
    
    /** @Author: DungNT Apr 13, 2019
     *  @Todo: mail alert KTKV setup giá cho B12 mới của ĐL
     **/
    public function alertSetupB12Hgd($mSell, $nameGas) {
        $agentName = $mSell->getAgent(); 
         $needMore = ['title'=> "[HGD Chưa Setup Giá B12] $agentName"];
//        $needMore['SendNow']            = 1;// only dev debug
//        $needMore['list_mail'][]        = 'dungnt@spj.vn';
        $needMore['list_mail']  = ['ketoan.vanphong@spj.vn', 'dungnt@spj.vn', 'duchoan@spj.vn', 'ketoan.kv@spj.vn'];
        $body = "$agentName chưa setup giá bán HGĐ cho loại Gas sau:<br>";
        $body .= "<br><br>Mã đơn hàng : {$mSell->getCodeNo()} - $nameGas";
        $body .= "<br><br>Các bộ phận liên quan kiểm tra báo cho Giám Đốc Hoàn setup giá. Sau đó ticket lên để kế toán Phan Thị Kim Phương cập nhật";
        SendEmail::bugToDev($body, $needMore);
    }
    
    /** @Author: DungNT May 20, 2019
     *  @Todo: agent chuyển trạng thái quy hoạch sang mở mới 
     * Mở mới: khi chuyển từ trạng thái quy hoạch sang mở mới thì email cho cả phòng kế toán và pháp lý nắm, hoàn thành xong thì chuyển active
     **/
    public function alertWhenChangeTypeAgent($mUser) {
        $mOldAgent      = $mUser->modelOld;
        $oldType        = $mOldAgent->getTypeAgent();
        $newType        = $mUser->getTypeAgent();
        $cUserFullname  = Yii::app()->user->first_name;
        
        $agentName = $mUser->getFullName(); 
         $needMore = ['title'=> "[Đại lý chuyển $oldType -> $newType]: $agentName"];
//        $needMore['SendNow']            = 1;// only dev debug
//        $needMore['list_mail'][]        = 'dungnt@spj.vn';
        $needMore['list_mail']  = ['dungnt@spj.vn', 'phaply@spj.vn', 'ketoan.vanphong@spj.vn'];
        $body = "$agentName chuyển trạng thái từ $oldType sang $newType:<br>";
        $body .= "<br><br>Người chuyển trạng thái: $cUserFullname";
        $body .= "<br><br>Các bộ phận liên quan cập nhật";
        SendEmail::bugToDev($body, $needMore);
    }
    
}

?>
