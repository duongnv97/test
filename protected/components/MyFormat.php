<?php

/**
 * @todo Class for date format
 * @author bb
 */
class MyFormat
{
    const MAX_LENGTH_8 = 8;
    const MAX_LENGTH_9 = 9;
    const MAX_LENGTH_10 = 10;
    const MAX_LENGTH_11 = 11;
    const MAX_LENGTH_12 = 12;
    const MAX_LENGTH_13 = 13;
    const MAX_LENGTH_14 = 14;
    const MAX_LENGTH_15 = 15;
    public static $sMyDateFormat = 'd-M-Y';
    public static $sMyCurrencyFormat = '#,##0';
    public static $sCurrencyType = 'S$';
    public static $sMyTimeFormat = 'H:i';
    public static $sMyTimeFormatAM_PM = 'h:i A';
    public static $dateFormatSearch = 'dd-mm-yy';

    const SUCCESS_UPDATE = "successUpdate";
    const ERROR_UPDATE = "ErrorUpdate";

    // Define cac kho hoac id dai ly o day
    const DL_TAN_DINH       = 119; // Đại lý Tân Định May 05, 2016
    const KHO_BEN_CAT       = 26677; // Kho Bến Cát Feb 26, 2016
    const KHO_TAN_SON       = 26678; // Kho Tân Sơn Feb 26, 2016
    const KHO_PHUOC_TAN     = 25785; // Kho Phước Tân Feb 26, 2016
    const DAU_KHI_BINH_DINH = 862516; // Dầu Khí Bình Định May 05, 2016
    const KHO_CHO_MOI       = 693615; // Kho Chợ Mới
    const KHO_VINH_LONG     = 30754; // Kho Vĩnh Long 
    const KHO_LY_CHINH_THANG    = 1171614; // Kho Lý Chính Thắng
    const KHO_VUNG_TAU      = 1117652; // Kho Nhập Xuất Vũng Tàu
    // Apr1319 không define KHO ở đây nữa, chuyển sang Class GasCheck

    public static $NOTIFY_CLASS = array(
        MyFormat::SUCCESS_UPDATE => 'notice',
        MyFormat::ERROR_UPDATE => 'notice_error',
    );

    public static $TheDaysOfTheWeek = array(
        'Monday' => 'Thứ Hai',
        'Tuesday' => 'Thứ Ba',
        'Wednesday' => 'Thứ Tư',
        'Thursday' => 'Thứ Năm',
        'Friday' => 'Thứ Sáu',
        'Saturday' => 'Thứ Bảy',
        'Sunday' => 'Chủ Nhật',
    );
    public static $BAD_CHAR = array('"', "'", "\\");

    public static function date($date)//date in database or timestamp
    {
        $date = self::isTimeStamp($date) ? $date : strtotime($date);
        return date(self::$sMyDateFormat, $date);
    }

    public static function  isTimeStamp($timestamp)
    {
        return ((string)(int)$timestamp === $timestamp)
        && ($timestamp <= PHP_INT_MAX)
        && ($timestamp >= ~PHP_INT_MAX);
    }

    public static function currency($decimal)
    {
        return self::$sCurrencyType . Yii::app()->numberFormatter->format(self::$sMyCurrencyFormat, $decimal);
    }

    public static function time($date)//time in database or timestamp
    {
        $date = self::isTimeStamp($date) ? $date : strtotime($date);
        return date(self::$sMyTimeFormat, $date);
    }

    public static function timeAM($date)//time in database or timestamp
    {
        $date = self::isTimeStamp($date) ? $date : strtotime($date);
        return date(self::$sMyTimeFormatAM_PM, $date);
    }

    /**
     * @param: date format: 20/05/2013
     * @return: date format: 2013-05-20
     */
    public static function dateConverDmyToYmd($date, $syntax = '/'){
        if (empty($date)) return '';
        $date = explode($syntax, $date);
        if (count($date) > 2)
            return $date[2] . '-' . $date[1] . '-' . $date[0];
        return '';
    }

    /**
     * @param: date format: 20-05-2013
     * @return: date format: 2013-05-20
     */
    public static function dateDmyToYmdForAllIndexSearch($date)
    {
        if (empty($date)) return '';
        $date = explode('-', $date);
        if (count($date) > 2)
            return $date[2] . '-' . $date[1] . '-' . $date[0];
        return '';
    }

    public static function dateConverYmdToDmy($date, $format = 'd/m/Y')
    {
        if ($date == '0000-00-00' || $date == '0000-00-00 00:00:00' || empty($date))
            return '';
        if (is_string($date)) {
            $date = new DateTime($date);
            return $date->format($format);
        }
    }

    /**
     * @Author: DungNT Apr 28, 2016
     * @Todo: PHP - get last week number in year
     */
    public static function getIsoWeeksInYear($year)
    {
        $date = new DateTime;
        $date->setISODate($year, 53);
        return ($date->format("W") === "53" ? 53 : 52);
    }

    /**
     * @Author: DungNT Apr 28, 2016
     * @Todo: get begin date and end date by given week number
     */
    public static function dateByWeekNumber($year, $week_no)
    {
        $date = new DateTime();
        $date->setISODate($year, $week_no);
        $aRes['date_from'] = $date->format('Y-m-d');
        $date_to_tmp = MyFormat::modifyDays($aRes['date_from'], 1, "+", 'week');
        $aRes['date_to'] = MyFormat::modifyDays($date_to_tmp, 1, "-", 'day');
        return $aRes;
    }

    /** Nguyen Dung 01-11-2013
     *  cộng thêm ngày
     * @param: $date: 2013-05-26
     * @param: $day_add: 16
     * @param: $operator: + or -
     * @param: $amount_of_days: days, months, years, hours, minutes and seconds
     */
    public static function addDays($date, $day_add, $operator = '+', $amount_of_days = 'day', $format = 'Y-m-d')
    {
        MyFormat::isValidDate($date);
        $date2 = new DateTime($date);
        $date2->modify($operator . $day_add . ' ' . $amount_of_days);
        return $date2->format($format);
    }

    /** Nguyen Dung 12-20-2013 fix tên hàm cho dễ hiểu
     *  cộng hoặc trừ thêm ngày
     * @param: $date: 2013-05-26
     * @param: $day_add: 16
     * @param: $operator: + or - default is +
     * @param: $amount_of_days: day, month, year, week default is "day"
     * @param: $format: default "Y-m-d"
     * @return: $format: default "Y-m-d"
     */
    public static function modifyDays($date, $day_add, $operator = '+', $amount_of_days = 'day', $format = 'Y-m-d')
    {
        MyFormat::isValidDate($date);
        if ($day_add == 0 || empty($day_add)) {// Fix on Jan 22, 2016
            return $date;
        }
        $date2 = new DateTime($date);
//        if($day_add==0)// Close Jan 22, 2016
//            $day_add=1;
        $date2->modify($operator . $day_add . ' ' . $amount_of_days);
        return $date2->format($format);
    }
    
    public static function getNextDay() {// Jan 23, 2017
        $cDate = date("Y-m-d");
        return MyFormat::modifyDays($cDate, 1);
    }

    /**
     * @Author: DungNT Feb 24, 2016
     * @Todo: get current week number of today
     */
    public static function getWeekNumber()
    {
        $ddate = date("Y-m-d");
        $date = new DateTime($ddate);
        return $date->format("W"); // $week
    }

    /**
     * @Author: DungNT Feb 24, 2016
     * @Todo: get start and end date of a week by weeknumber
     * http://stackoverflow.com/questions/4861384/php-get-start-and-end-date-of-a-week-by-weeknumber
     */
    public static function getStartAndEndDate($week, $year)
    {
        $dto = new DateTime();
        $ret['week_start'] = $dto->setISODate($year, $week)->format('Y-m-d');
        $ret['week_end'] = $dto->modify('+6 days')->format('Y-m-d');
        return $ret;
    }


    /* Nguyen Dung Jun 30, 2014
    *  check valid date
     * @param: $stringcheck: 2013-05-26
     * @return: true if valid date, else false
    */
    public static function isValidDate($someString)
    {
        $someString = trim($someString);
        if (empty($someString)) return;
        $date = date_parse($someString);
        if (checkdate($date["month"], $date["day"], $date["year"])) {
            return true;
        } else {
            Yii::log("function MyFormat::isValidDate();  Uid: " . Yii::app()->user->id . " Exception Datetime không hợp lệ", 'error');
            throw new CHttpException(404, 'Yêu cầu không hợp lệ, vui lòng thử lại');
        }
    }

    /**
     * @Author: DungNT Sep 02, 2015
     * @Todo: convert datetime to datetime db
     * @Param: $datetime: 20/05/2013 20:01:05
     */
    public static function datetimeToDbDatetime($datetime, $char = '/')
    {
        if (empty($datetime)) return null;
        $date_tmp = explode(' ', $datetime);
        $date = explode($char, $date_tmp[0]);
        if (count($date) < 2)
            return '';
        return "$date[2]-$date[1]-$date[0] $date_tmp[1]";
    }

    /**
     * @Author: DungNT Sep 02, 2015
     * @Todo: convert datetime db to datetime user
     * @Param: $datetime: 2013-05-20 20:01:05
     */
    public static function datetimeDbDatetimeUser($datetime, $char = '-')
    {
        if (empty($datetime) || $datetime == "0000-00-00 00:00:00") return '';
        $date_tmp = explode(' ', $datetime);
        $date = explode($char, $date_tmp[0]);
        if (count($date) < 2)
            return '';
        return "$date[2]/$date[1]/$date[0] $date_tmp[1]";
    }
    
    /**
     * NGUYEN DUNG - count total month between date
     * @param: string $date_from: Y-m-d
     * @param: string $date_to: Y-m-d
     * @return: number of month
     * @ex1: 2018-05-02 AND 2018-06-25 will return 2
     * @ex2: 2018-05-02 AND 2018-02-25 will return 3
     */
    public static function countMonthBetweenDate($date_from, $date_to){
        $objFrom             = new DateTime($date_from);
        $objTo               = new DateTime($date_to);
        $countMonth          = $objFrom->diff($objTo)->m + 1;
        return $countMonth;
    }

    /**
     * NGUYEN DUNG - TO COMPARE TWO DATE
     * @param: string $date1: 2013-10-25
     * @param: string $date2: 2013-10-20
     * @return: bool; true if date1>date2 else return flase
     */
    public static function compareTwoDate($date1, $date2)
    {
        $d1 = new DateTime($date1);
        $d2 = new DateTime($date2);
        if ($d1 > $d2) return true;
        return false;
    }

    /**
     * NGUYEN DUNG - TO COMPARE 3 DATE
     * @param: string $date_from: 2013-10-25
     * @param: string $date_to: 2013-10-20
     * @param: string $date_between: 2013-10-20
     * @return: bool; true if $date_from <= $date_between <= $date_to else return flase
     */
    public static function compareDateBetween($date_from, $date_to, $date_between)
    {
        $date_from = new DateTime($date_from);
        $date_to = new DateTime($date_to);
        $date_between = new DateTime($date_between);
        if ($date_from <= $date_between && $date_between <= $date_to) return true;
        return false;
    }

    /** Sep 27, 2014
     * NGUYEN DUNG - get days between 2 date
     * @param: string $date_from: 2013-10-25
     * @param: string $date_to: 2013-10-20
     * @return: number of day
     */
    public static function getNumberOfDayBetweenTwoDate($date1, $date2)
    {
        $date1 = new DateTime($date1);
        $date2 = new DateTime($date2);
        return $diff = $date2->diff($date1)->format("%a");
    }
    /** @Author: DungNT Jul 13, 2017
     * @Todo: calculate minute difference between two date-times. To get the total number of minutes
     * @param: string $date_from: 2008-12-13 10:42:00
     * @param: string $date_to: 2008-12-13 11:42:00
     */
    public static function getMinuteTwoDate($date1, $date2)
    {
        $date1 = new DateTime($date1);
        $date2 = new DateTime($date2);

        $since_start = $date2->diff($date1);
        
        $minutes = $since_start->days * 24 * 60;
        $minutes += $since_start->h * 60;
        $minutes += $since_start->i;
        return $minutes;
    }
    /** @Author: DungNT Jul 13, 2017
     * @Todo: calculate minute difference between two date-times. To get the total number of minutes
     * @param: string $date_from: 2008-12-13 10:42:00
     * @param: string $date_to: 2008-12-13 11:42:00
     */
    public static function getSecondTwoDate($date1, $date2)
    {
        $date1 = new DateTime($date1);
        $date2 = new DateTime($date2);

        return $diffInSeconds = $date2->getTimestamp() - $date1->getTimestamp();
    }
    public static function getDateTimeNow() {
        return date('Y-m-d H:i:s');
    }
    
    /**
     * @Author: DungNT May 22, 2014
     * @Todo: lấy mảng ngày từ 2 ngày đưa ra
     * @Param: $date_from format: Y-m-d
     * @Param: $date_to format: Y-m-d
     * @Return: array date array('2014-06-07','2014-06-08'....)
     */
    public static function getArrayDay($date_from, $date_to)
    {
        if ($date_from == $date_to)
            return array($date_from);
        $aRes = array();
        $date_from_obj = new DateTime($date_from);
        $date_to_obj = new DateTime($date_to);

        // Nếu ngày bắt đầu lớn hơn ngày kết thúc -=> sai
        if ($date_from_obj > $date_to_obj)
            return array($date_from);
        $aRes[] = $date_from;
        $ok = true;
        while ($ok) {
            $temp = MyFormat::modifyDays($date_from, 1);
            $aRes[] = $temp;
            if ($temp == $date_to)
                $ok = FALSE;
            $date_from = $temp;
        }
        return $aRes;
    }

    /**
     * @Author: DungNT Sep 27, 2014
     * @Todo: tính số ngày chủ nhật (có thể là t7 nếu có modify) có trong khoảng ngày đưa ra
     * @Param: $date_from format: Y-m-d
     * @Param: $date_to format: Y-m-d
     * @Return: array date array('2014-06-07','2014-06-08'....)
     */
    public static function getNumberOfSunday($date_from, $date_to)
    {
        if ($date_from == $date_to)
            return 0;
        $aRes = array();
        $date_from_obj = new DateTime($date_from);
        $date_to_obj = new DateTime($date_to);
//        $WeekendHolidays = array('Saturday', 'Sunday');
        $WeekendHolidays = array('Sunday');
        // Nếu ngày bắt đầu lớn hơn ngày kết thúc -=> sai
        if ($date_from_obj > $date_to_obj)
            return 0;
        $resDays = 0;
        $ok = true;
        while ($ok) {
            $temp = MyFormat::modifyDays($date_from, 1);
            $date_obj_tmp = new DateTime($temp);
            if (in_array($date_obj_tmp->format('l'), $WeekendHolidays)) {
                $resDays++;
            }
            $aRes[] = $temp;
            if ($temp == $date_to)
                $ok = FALSE;
            $date_from = $temp;
        }
        return $resDays;
    }

    /** Sep 27, 2014
     * NGUYEN DUNG - get days between 2 date
     * @param: string $date_from: 2013-10-25
     * @param: string $date_to: 2013-10-20
     * @return: number of day
     */
    public static function getNumberOfDayBetweenTwoDateForLeave($date1, $date2)
    {
        return self::getNumberOfDayBetweenTwoDate($date1, $date2) + 1;
    }

    /** 01-11-2014
     * NGUYEN DUNG - format tên khách hàng: mã KH - tên KH
     * @param: string $mUser model user
     * @return: string name
     */
    public static function formatNameCustomer($mUser)
    {
        if ($mUser) {
            return $mUser->code_bussiness . '-' . $mUser->first_name;
        }
        return '';
    }

    /**
     * @Author: DungNT Mar 12, 2014
     * @Todo: remove some user input sql injection
     * @Param: $string string input
     * @Param: $needMore  $needMore['RemoveScript']
     * @Return: string
     */
    public static function removeBadCharacters($string, $needMore = array())
    {
        $string = str_replace(MyFormat::$BAD_CHAR, '', $string);
        if (isset($needMore['RemoveScript'])) {
            $string = InputHelper::removeScriptTag($string);
        }
        return trim($string);
//         return str_replace(array('&','<','>','/','\\','"',"'",'?','+'), '', $string);
    }

    /**
     * @Author: DungNT Jul 24, 2014
     * @Todo: remove script and bad character
     * @Param: $model model . Call MyFormat::RemoveScriptBad($mDetail, $aAtt);
     * @Param: $aAtt array fieldName: $aAtt = array('customer_name','customer_contact', 'address', 'note');
     */
    public static function RemoveScriptBad($model, $aAtt)
    {
        foreach ($aAtt as $fieldName) {
            $model->$fieldName = MyFormat::removeBadCharacters($model->$fieldName, array('RemoveScript' => 1));
        }
    }

    /**
     * @Author: DungNT Apr 19, 2014
     * @Todo: get agent id cho new record là cột parent_id
     * lần này sửa cho 1 agent có thể có nhiều user login
     * @Return: id của agent
     */
    public static function getAgentId(){
        $session = Yii::app()->session;
        if (!isset($session['WEB_SESS_AGENT_ID'])) {// Sep 07, 2016
            $session['WEB_SESS_AGENT_ID'] = Yii::app()->user->parent_id;
        }
        return $session['WEB_SESS_AGENT_ID'];
    }
    
    public static function getCurrentUid(){
        $session = Yii::app()->session;
        if (!isset($session['WEB_SESS_UID'])) {// Sep 07, 2016
            $session['WEB_SESS_UID'] = Yii::app()->user->id;
        }
        return $session['WEB_SESS_UID'];
    }
    
    public static function getCurrentRoleId(){
        if(!isset(Yii::app()->session)){// Gây bug from CRON  exception 'CHttpException' with message 'Property "CConsoleApplication.session" is not defined.'
            return 0;
        }
        $session = Yii::app()->session;
        if (!isset($session['WEB_SESS_ROLE_ID'])) {// Sep 07, 2016
            $session['WEB_SESS_ROLE_ID'] = Yii::app()->user->role_id;
        }
        return $session['WEB_SESS_ROLE_ID'];
    }
            

    public static function getAgentCodeAccount()
    {
        $session = Yii::app()->session;
        if (isset($session['WEB_AGENT_MODEL'])) {// Jul 12, 2016
            return $session['WEB_AGENT_MODEL']['code_account'];
        }
        $mUser = Users::model()->findByPk(Yii::app()->user->parent_id);
        if ($mUser)
            return $mUser->code_account;
        return 'ADHM';
    }

    /**
     * @Author: DungNT Jul 12, 2016
     * @Todo: khởi tạo thông tin session cho user Agent, để get info
     */
    public static function initSessionModelAgent($agent_id)
    {
        $cRole = Yii::app()->user->role_id;
        if ($cRole != ROLE_SUB_USER_AGENT) {
            return;// chỉ khởi tạo cho user đại lý
        }
        $session = Yii::app()->session;
        if (!isset($session['WEB_AGENT_MODEL'])) {
            $mUser = Users::model()->findByPk($agent_id);
            $session['WEB_AGENT_MODEL'] = $mUser->getAttributes();
        }
    }
    
    /**
     * @Author: DungNT Aug 19, 2016
     * @Todo: from C# or Androi khởi tạo thông tin session cho user Agent, để get info
     * không sử dụng cho web
     */
    public static function initSessionModelAgentNotFromWeb($agent_id)
    {
        $session = Yii::app()->session;
//        if (!isset($session['WEB_AGENT_MODEL'])) {
            $mUser = Users::model()->findByPk($agent_id);
            $session['WEB_AGENT_MODEL'] = $mUser->getAttributes();
//        }
    }

    /**
     * @Author: DungNT Jul 31, 2016
     * @Todo: khởi tạo thông tin session By Role để get info
     */
    public static function initSessionModelByRole($role_id)
    {
        $session = Yii::app()->session; 
//        if (!isset($session['WEB_MODEL_BY_ROLE'])) {
            $session['WEB_MODEL_BY_ROLE'] = Users::getArrObjectUserByRole($role_id);
//        }
    }

    //  lấy gender: để xác định là đại lý hay kho Users::IS_AGENT = 1
    public static function getAgentGender()
    {
        $mUser = Users::model()->findByPk(Yii::app()->user->parent_id);
        if ($mUser)
            return $mUser->gender;
        return '';
    }

    //  lấy gender: để xác định là đại lý hay kho Users::IS_AGENT = 1
    public static function getAgentInfoFieldName($fieldName)
    {
        $mUser = Users::model()->findByPk(Yii::app()->user->parent_id);
        if ($mUser)
            return $mUser->$fieldName;
        return '';
    }

    // dùng để lấy mã KH hệ thống của kh, nếu có 1 thì sẽ trả về id của chính nó
    public static function getParentIdForCustomer($mUser)
    {
        if (is_null($mUser) || empty($mUser)) {
            return;
        }
        $customer_parent_id = $mUser->id;
        if (!empty($mUser->parent_id) && $mUser->parent_id > 0) {
            $customer_parent_id = $mUser->parent_id;
        }
        return $customer_parent_id;
    }

    /**
     * @Author: DungNT Jun 15, 2014
     * @Todo: kiểm tra ngày user đưa lên có hợp lệ không
     * @Param: $date required format d-m-Y. May be: 15/11/1987 OR 15-11-1987
     * @Param: $stringFormat may be / or -
     * @Return: true if OK, false if not valid
     * var_dump(MyFormat::validDateInput("055/06/2014", "/"));die;
     */
    public static function validDateInput($date, $stringFormat)
    {
        $test_arr = explode($stringFormat, $date);
        if (count($test_arr) == 3) {
            // bool checkdate ( int $month , int $day , int $year )
            return checkdate((int)$test_arr[1], (int)$test_arr[0], (int)$test_arr[2]);
        }
        return false;
    }

    // Jun 29, 2014 viết gọn đoạn bắt exception
    public static function catchAllException($e)
    {
        Yii::log("Statistic:: Uid: " . Yii::app()->user->id . "Exception " . $e->getMessage(), 'error');
        $code = 404;
        if (isset($e->statusCode))
            $code = $e->statusCode;
        if ($e->getCode())
            $code = $e->getCode();
        throw new CHttpException($code, $e->getMessage());
    }

    /**
     * @Author: DungNT Aug 15, 2014
     * @Todo: lấy ngày giới hạn search KH pttt và seri của pttt
     */
    public static function GetDateLimitSearchCustomerPttt()
    {
        $res = '';
        $session = Yii::app()->session;
        if (!isset($session['DateLimitSearchCustomerPttt'])) {
            // chỗ này có 2 cách tính, 1 là lấy trong last_logged_in của user, không thì lấy chung trong config của hệ thống
//            $DateLimit = MyFormat::getAgentInfoFieldName('last_logged_in');
//            if(empty($DateLimit)){
//            if(1){
            $today = date('Y-m-d');
            $TimeLimit = Yii::app()->params['month_limit_search_pttt'];
            $DateLimit = MyFormat::modifyDays($today, $TimeLimit, "-", 'month') . " 00:00:00";
//            }
            $session['DateLimitSearchCustomerPttt'] = $DateLimit;
        }
        return $session['DateLimitSearchCustomerPttt'];
    }

    public static function RemoveNumberOnly($string)
    {
        return trim(str_replace(range(0, 9), '', $string)); // to remove number on name
    }

    /**
     * @Author: DungNT Aug 06, 2014
     * @Todo: remove some field need remove script
     * @Param: $model model
     * @Param: $aAttributes array  attributes
     */
    public static function RemoveScriptOfModelField(&$model, $aAttributes)
    {
        foreach ($aAttributes as $FieldName) {
            $model->$FieldName = InputHelper::removeScriptTag($model->$FieldName);
        }
    }

    /**
     * @Author: DungNT Aug 12, 2014
     * @Todo: remove some field need remove script
     * @Param: $model model
     * @Param: $aAttributes array  attributes
     * @Param: $needMore array  array('RemoveScript'=>1)
     */
    public static function RemoveScriptAndBadCharOfModelField(&$model, $aAttributes, $needMore = array())
    {
        foreach ($aAttributes as $FieldName) {
            $model->$FieldName = MyFormat::removeBadCharacters($model->$FieldName, $needMore);
        }
    }

    public static function getIpUser()
    {
        return Yii::app()->request->getUserHostAddress();
    }
    /** @Author: DungNT Sep 01, 2019
     *  @Todo: get ip server
     **/
    public static function getIpServer(){
        return isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : ' ---Run Console--- ';
    }
            

    /**
     * @Author: DungNT Aug 30, 2014
     * @Todo: build mảng số thứ tự
     */
    public static function BuildNumberOrder($max)
    {
        $res = array();
        for ($i = 1; $i <= $max; $i++):
            $res[$i] = $i;
        endfor;
        return $res;
    }

    /**
     * @Author: DungNT Aug 30, 2014
     * @Todo: get year, MONTH, DAY by date
     * @Param: $date: 2014-05-05 OR 2014-05-05 05:25:00
     * @param: $needMore['format']: can be: m, d...
     * @Return: default only year.
     */
//    public static function addDays($date, $day_add, $operator='+', $amount_of_days='day', $format='Y-m-d'){
    public static function GetYearByDate($date, $needMore = array())
    {
        $date2 = new DateTime($date);
        if (isset($needMore['format'])) {
            return $date2->format($needMore['format']);
        }
        return $date2->format('Y');
    }

    /**
     * @Author: DungNT Aug 31, 2014
     * @Todo: load model loadModelByClass
     * @Param: ($id, $ClassName)
     * @Return: model
     */
    public static function loadModelByClass($id, $ClassName)
    {
        try {
            $model_ = MyFormat::CheckBeforeLoadModel($ClassName);
            $model = $model_->findByPk($id);
            if ($model === null) {
                $cUid = Yii::app()->user->id;
                Yii::log("Class $ClassName Uid : $cUid Model Bị NULL trong hàm loadModelByClass dùng hàm call_user_func.");
                throw new CHttpException(404, 'The requested page does not exist.');
            }
            return $model;
        } catch (Exception $e) {
            Yii::log("Exception " . print_r($e, true), 'error');
            throw new CHttpException("Exception " . print_r($e, true));
        }
    }

    /**
     * @Author: DungNT Mar 06, 2015
     * @Todo: check file tồn tại before load
     * @Param: $ClassName
     */
    public static function CheckBeforeLoadModel($ClassName)
    {
        $path = "/protected/models/$ClassName.php";
        $dirModel1 = Yii::getPathOfAlias("webroot") . "/protected/models/$ClassName.php";
        $dirModel2 = Yii::getPathOfAlias("webroot") . "/protected/modules/api/models/$ClassName.php";
        if (!is_file($dirModel1) && !is_file($dirModel2)) {
            $cUid = Yii::app()->user->id;
            Yii::log("Class $ClassName Uid : $cUid Lỗi model không tồn tại. Important to review this error. User có thể đã chỉnh URL");
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model = call_user_func(array($ClassName, 'model'));
    }

    /**
     * @Author: DungNT Nov 07, 2014
     * @Todo: delete list model detail belong to one model parent: ex: table GasText and GasTextComment
     * @Param: $ClassNameModelDelete name of model delete: ex: GasTextComment
     * @Param: $root_id_belong value id ref to parent table: ex 1,2,3...
     * @Param: $NameField: ex: text_id
     * will call at beforeDelete: MyFormat::deleteModelDetailByRootId('GasMeetingMinutesComment', $this->id, 'meeting_minutes_id');
     */
    public static function deleteModelDetailByRootId($ClassNameModelDelete, $root_id, $NameField)
    {
        $criteria = new CDbCriteria();
        $criteria->compare("t.$NameField", $root_id);
        $model_ = MyFormat::CheckBeforeLoadModel($ClassNameModelDelete);
        $models = $model_->findAll($criteria);
        Users::deleteArrModel($models);
    }
    
     
    /**
    * @Author: DungNT Oct 24, 2016 xử lý delete detail không qua before delete
    * @Todo: get delete all store card detail by store_card_id
    * @Param: $root_id ex is Transaction id
    * @Param: $DetailClassName ex: TransactionDetail
    * @Param: $field_name ex: transaction_id
     */
    public static function deleteAllByRootId($root_id, $DetailClassName, $field_name){
        if(empty($root_id)){
            return ;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition("$field_name=$root_id");
        $model = call_user_func(array($DetailClassName, 'model'));
        $model->deleteAll($criteria);
        // Với những model không xử lý beforedelete thì không phải delete từng item nữa, mà xóa hết luôn DeleteAll
        // Sep 16, 2016 không cần thiết phải xóa từng detail nữa,
    }

    /**  DungNT Sep 08, 2014
     * @to do: save file from internet
     * @param string $url : http://noibo.daukhimiennam.com/SpjCacheFile.zip
     * @param string $path : /upload/temp
     * @param string $fileName : 3-1374742421.jpg
     */
    public function downloadFileUsingCurl($url, $path, $fileName)
    {
        $local_file_name = ROOT . $path . '/' . $fileName;
        set_time_limit(0);
//        $fp = fopen (dirname(__FILE__) . '/localfile.tmp', 'w+');//This is the file where we save the    information
        $fp = fopen($local_file_name, 'w+');//This is the file where we save the information
        $ch = curl_init(str_replace(" ", "%20", $url));//Here is the file we are downloading, replace spaces with %20
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_FILE, $fp); // write curl response to file
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch); // get curl response
        curl_close($ch);
        fclose($fp);
    }
    
    /**  DungNT Aug 26, 2019
     * @to do: save file from internet
     * @param string $url : http://noibo.daukhimiennam.com/SpjCacheFile.zip
     * @param string $path : /upload/temp
     * @param string $fileName : 3-1374742421.jpg
     * @note: use Curl with CURLOPT_FOLLOWLOCATION = false
     */
    public function downloadFileUsingCurlV1($url, $path, $fileName)
    {
        $local_file_name = ROOT . $path . '/' . $fileName;
        set_time_limit(0);
//        $fp = fopen (dirname(__FILE__) . '/localfile.tmp', 'w+');//This is the file where we save the    information
        $fp = fopen($local_file_name, 'w+');//This is the file where we save the information
        $ch = curl_init(str_replace(" ", "%20", $url));//Here is the file we are downloading, replace spaces with %20
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_FILE, $fp); // write curl response to file
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_exec($ch); // get curl response
        curl_close($ch);
        fclose($fp);
    }

    /**
     * @Author: DungNT Sep 10, 2014
     * @Todo: check file upload có phải là image không
     * @Param: $file = $_FILES['file']['tmp_name'];
     * @Return: true if file is image, false if not
     * http://stackoverflow.com/questions/15595592/php-validating-the-file-upload
     */
    public static function IsImageFile($file)
    {
        if (file_exists($file)) {
            $imagesizedata = getimagesize($file);
            if ($imagesizedata === FALSE) {
                //not image
                throw new Exception('File không phải là ảnh, không hợp lệ, chỉ cho phép JPG, JPEG, PNG. FILE IMAGE not image Invalid request 1');
            } else {
                // http://stackoverflow.com/questions/1141227/php-checking-if-the-images-is-jpg
                // http://us2.php.net/manual/en/function.exif-imagetype.php // kiem tra file upload co hop le khong
                $imageFileType = exif_imagetype($file);
                if ($imageFileType != IMAGETYPE_JPEG && $imageFileType != IMAGETYPE_PNG) {
                    throw new Exception('File không phải là ảnh, không hợp lệ, chỉ cho phép JPG, JPEG, PNG. FILE IMAGE NOT IMAGE Invalid request 3');
                }
                return true;
                //image
                //use $imagesizedata to get extra info
            }
        } else {
            throw new Exception('File không phải là ảnh, không hợp lệ, chỉ cho phép JPG, JPEG, PNG. FILE IMAGE not exists file Invalid request 2');
            //not file
        }
    }

    /** DungNT MOVE Apr 10, 2015 */
    public static function setNotifyMessage($type, $message)
    {
        Yii::app()->user->setFlash($type, $message);
    }

    /**
     * @Author: DungNT Nov 07, 2014
     * @Todo: bind show notify sucess, error message
     */
    public static function BindNotifyMsg()
    {
        if (Yii::app()->user->hasFlash(MyFormat::SUCCESS_UPDATE)):
            return MyFormat::renderNotifySuccess(Yii::app()->user->getFlash(MyFormat::SUCCESS_UPDATE));
        elseif (Yii::app()->user->hasFlash(MyFormat::ERROR_UPDATE)):
            return MyFormat::renderNotifyError(Yii::app()->user->getFlash(MyFormat::ERROR_UPDATE));
        endif;
        return '';
    }
    
    /** @Author: DungNT Jul 11, 2017 */
    public static function renderNotifySuccess($msg) {
        return "<div class='flash notice'><a data-dismiss='alert' class='close' href='javascript:void(0)'>×</a>$msg</div>";
    }
    public static function renderNotifyError($msg) {
        return "<div class='flash notice_error'><a data-dismiss='alert' class='close' href='javascript:void(0)'>×</a>$msg</div>";
    }

    /**
     * @Author: DungNT Sep 19, 2015
     * @Todo: hiển thị bất cứ message nào trên hệ thống
     */
    public static function SystemNotifyMsg($type, $msg)
    {
        $class = MyFormat::$NOTIFY_CLASS[$type];
        echo "<div class='flash $class'>
                <a data-dismiss='alert' class='close' href='javascript:void(0)'>×</a>
                $msg
            </div>";
    }

    /**
     * @Author: DungNT Nov 21, 2014
     * @Todo: build name for sale, all system
     * @Param: $model model user
     */
    public static function BuildNameSaleSystem($model)
    {
        if (is_null($model)) return "";
        $typeSale = isset(Users::$aTypeSale[$model->gender]) ? Users::$aTypeSale[$model->gender] : '';
        $AgentBelongTo = $model->rParent ? $model->rParent->first_name : '';
        if ($model->role_id != ROLE_SALE) {
            return $model->code_bussiness . " - " . MyFormat::GetNameWithLevelProvince($model);
        }
        return $model->code_bussiness . " - " . $model->first_name . " - " . $typeSale . "-" . $AgentBelongTo;
    }

    /**
     * @Author: DungNT Nov 24, 2014
     * @Todo: format some number (decimal) show at input
     * @Param: $model model
     * @Param: $aAttribute array()
     */
    public static function FormatNumberDecimal($model, $aAttribute)
    {
        foreach ($aAttribute as $FieldName) {
            $model->$FieldName = ActiveRecord::formatNumberInput($model->$FieldName);
        }
    }

    /**
     * @Author: DungNT Dec 27, 2014
     * @Todo: tính toán ngày bắt đầu của tháng , dựa trên ngày hiện tại
     * vd: ngày hiện tại là 28-12-2014 và số tháng trong config là 6 tháng
     * thì ta sẽ tính như sau:
     * đầu tiên ta sẽ lấy ngày 01-12-2014 rồi trừ đi 6 tháng (config)
     * sẽ ra ngày 01-06-2014
     * và sẽ return ngày 01-06-2014, để chạy thống kê từ 01-06-2014 đến 28-12-2014
     * rồi group theo tháng
     * @Param: $model
     */
    public static function GetDateFromForSupportCustomer($month_statistic)
    {
        $date_begin_month = date("Y-m") . "-01";
        $date_from = MyFormat::modifyDays($date_begin_month, $month_statistic, '-', 'month');
        return $date_from;
    }

    /**
     * @Author: DungNT Jan 19, 2015
     * @Todo: get length code for storecard, cashbook
     * tính toán độ dài cho mã thẻ kho, mã sổ quỹ
     * @Param: $model
     */
    public static function GetLengthCode($length, $needMore = array())
    {
        $code_account = isset($needMore['code_account']) ? $needMore['code_account'] : MyFormat::getAgentCodeAccount();
        $code_lenth_root = 5; // length mã số của 1 đại lý hay cửa hàng chuẩn
        $need_add = strlen($code_account) - $code_lenth_root;
//        if( $need_add > 0){
        $length += $need_add; // chỗ này đảm bảo trong 1 năm sẽ có 99 ngàn phiếu được xuất cho 1 đại lý, nếu vượt thì phải xem lại chỗ này
//        } // xử lý cho cộng cả số âm cho mã này: CH215 00 00007
        return $length;
    }

    /**
     * @Author: DungNT Jan 28, 2015
     * @Todo: get last comment for this
     * @Param: $id_root is id model parent (root) of this detail model.
     * Ex GasMeetingMinutes có GasMeetingMinutesComment thì $id_root ở đây là id của model GasMeetingMinutes
     * @Param: $field_name tên khóa ngoại ref của GasMeetingMinutes ở table GasMeetingMinutesComment
     * @Param: $ClassName is tên model class GasMeetingMinutesComment
     * @Param: $rUidLogin is relation user post comment của GasMeetingMinutesComment
     * @Return: string
     * @example:  MyFormat::GetLastComment($model->id, 'meeting_minutes_id', 'GasMeetingMinutesComment', 'rUidLogin');
     */
    public static function GetLastComment($id_root, $field_name, $ClassName, $rUidLogin, $needMore = array())
    {
        $res = '';
        $criteria = new CDbCriteria();
        $criteria->compare("t.$field_name", $id_root);
        $criteria->order = "t.id DESC";
        $criteria->limit = 1;
        $model_ = call_user_func(array($ClassName, 'model'));
        $mComment = $model_->find($criteria);
        if ($mComment) {
            $cmsFormater = new CmsFormatter();
            if ($mComment->$rUidLogin) {
                $res = $mComment->$rUidLogin->first_name . "<br>" . $cmsFormater->formatDateTime($mComment->created_date);
            }
        }
        return $res;
    }

    /**
     * @Author: DungNT Jan 28, 2015
     * @Todo: count total comment for this topic
     * @Param: $id_root is id model parent (root) of this detail model.
     * Ex GasMeetingMinutes có GasMeetingMinutesComment thì $id_root ở đây là id của model GasMeetingMinutes
     * @Param: $field_name tên khóa ngoại ref của GasMeetingMinutes ở table GasMeetingMinutesComment
     * @Param: $ClassName is tên model class GasMeetingMinutesComment
     * @Param: $rUidLogin is relation user post comment của GasMeetingMinutesComment
     * @Return: number
     * @example:  MyFormat::CountComment($model->id, 'meeting_minutes_id', 'GasMeetingMinutesComment', 'rUidLogin');
     */
    public static function CountComment($id_root, $field_name, $ClassName, $needMore = array())
    {
        $res = '';
        $criteria = new CDbCriteria();
        $criteria->compare("t.$field_name", $id_root);
        $model_ = call_user_func(array($ClassName, 'model'));
        return $model_->count($criteria);
    }

    /**
     * @Author: DungNT Feb 24, 2015
     * @Todo: get name user with level
     * @Param: $mUser
     */
    public static function GetNameWithLevel($mUser)
    {
        if (is_null($mUser)) {
            return '';
        }
        // Apr 17,17 phải check isset(Yii::app()->session) khi run ở console
        if (isset(Yii::app()->session) && isset(Yii::app()->session['ROLE_NAME_USER'][$mUser->role_id])) {
            $session = Yii::app()->session;
            return $mUser->first_name . "<br>[" . $session['ROLE_NAME_USER'][$mUser->role_id] . "]";
        }
        $mRole = Roles::model()->findByPk($mUser->role_id);
        if ($mRole) {
            return $mUser->first_name . "<br>[$mRole->role_name]";
        }
        return $mUser->first_name;
    }

    /**
     * @Author: DungNT Mar 04, 2016
     */
    public static function GetNameWithLevelProvince($mUser)
    {
        if (is_null($mUser)) {
            return '';
        }
        $session = Yii::app()->session;
        if (isset($session['ROLE_NAME_USER'][$mUser->role_id])) {
            $province = "";
            if (isset($session['SESS_PROVINCE'][$mUser->province_id])) {
                $province = $session['SESS_PROVINCE'][$mUser->province_id];
            }

            return $mUser->first_name . " [" . $session['ROLE_NAME_USER'][$mUser->role_id] . "] - $province";
        }
        $mRole = Roles::model()->findByPk($mUser->role_id);
        if ($mRole) {
            return $mUser->first_name . "<br>[$mRole->role_name]";
        }
        return $mUser->first_name;
    }

    /**
     * @Author: DungNT Mar 04, 2016
     */
    public static function initSessionProvince()
    {
        $session = Yii::app()->session;
        if (!isset($session['SESS_PROVINCE'])) {
            $session['SESS_PROVINCE'] = GasProvince::getArrAll();
        }
    }

    /**
     * @Author: DungNT Feb 25, 2015
     * @Todo: add DungNT TO TEST SEND MAIL in some new function cron
     */
    public static function AddEmailAnhDung(&$aModelUserMail)
    {
        $model = Users::model()->findByPk(142134); // // Anh Dũng NB
        if (is_array($aModelUserMail)) {
            $aModelUserMail[] = $model;
        } else {
            $aModelUserMail = array($model);
        }
    }

    /**
     * @Author: DungNT Dec 12, 2014 -- add Oct 09, 2015
     * @Todo: gen unique session id in any table
     * @param: $className: name of model, string ex Users
     * @param: $fieldName: field name of model need check
     * @return: md5 string
     */
    public static function generateSessionIdByModel($className, $fieldName)
    {
        $session_id = md5(time() . StringHelper::getRandomString(16));
        $model_ = call_user_func(array($className, 'model'));
        $count = $model_->count("$fieldName='$session_id'");
        if ($count > 0) {
            $session_id = MyFunctionCustom::generateSessionIdByModel($className, $fieldName);
            return $session_id;
        } else {
            return $session_id;
        }
    }

    /**
     * @Author: DungNT Jul 24, 2015
     * @Todo: catch error of api
     * @Param: $ex
     */
    public static function ApiCatchError($ex, $objController)
    {
        $result = ApiModule::$defaultResponse;
        $result['message'] = "" . $ex->getMessage();
        $info = "API ERROR --- " . $result['message'];
        if(!in_array($ex->getCode(), SpjError::$aNotWriteLog)){
            Logger::WriteLog($info);
        }
        ApiModule::sendResponse($result, $objController);
    }


    /**
     * @Author: DungNT Nov 26, 2014
     * to copy from model to other model in one table
     * @param: model $mFrom
     * @param: model $mTo
     * @param: array $aFieldNotCopy: array('id','something_else'...)
     * @return: model $mTo
     */
    public static function copyFromToTable($mFrom, &$mTo, $aFieldNotCopy = array())
    {
        foreach ($mFrom->getAttributes() as $field_name => $field_value) {
            if (count($aFieldNotCopy)) {
                if (!in_array($field_name, $aFieldNotCopy) && $mTo->hasAttribute($field_name))
                    $mTo->$field_name = $mFrom->$field_name;
            } else {
                $mTo->$field_name = $mFrom->$field_name;
            }
        }
        return $mTo;
    }

    public static function escapeValues($value)
    {
        return str_replace("'", "''", $value);
    }

    /**
     * @Author: TRUNG June 03 2016
     * Đọc một số thành chữ
     * @param $so
     * @return string
     */
    public static function doc1so($so)
    {
        $arr_chuhangdonvi = array('không', 'một', 'hai', 'ba', 'bốn', 'năm', 'sáu', 'bảy', 'tám', 'chín');
        $result = $arr_chuhangdonvi[$so];
        return $result;
    }

    /**
     * @Author: TRUNG June 03 2016
     *Đọc 2 số thành chữ
     * @param $so
     * @return string
     */
    public static function doc2so($so)
    {
        $arr_chubinhthuong = array('không', 'một', 'hai', 'ba', 'bốn', 'năm', 'sáu', 'bảy', 'tám', 'chín');
        $arr_chuhangdonvi = array('mươi', 'mốt', 'hai', 'ba', 'bốn', 'lăm', 'sáu', 'bảy', 'tám', 'chín');
        $arr_chuhangchuc = array('', 'mười', 'hai mươi', 'ba mươi', 'bốn mươi', 'năm mươi', 'sáu mươi', 'bảy mươi', 'tám mươi', 'chín mươi');
        $resualt = '';
        $sohangchuc = substr($so, 0, 1);
        $sohangdonvi = substr($so, 1, 1);
        $resualt .= $arr_chuhangchuc[$sohangchuc];
        if ($sohangchuc == 1 && $sohangdonvi == 1)
            $resualt .= ' ' . $arr_chubinhthuong[$sohangdonvi];
        elseif ($sohangchuc == 1 && $sohangdonvi > 1)
            $resualt .= ' ' . $arr_chuhangdonvi[$sohangdonvi];
        elseif ($sohangchuc > 1 && $sohangdonvi > 0)
            $resualt .= ' ' . $arr_chuhangdonvi[$sohangdonvi];

        return $resualt;
    }

    /**
     * @Author: TRUNG June 03 2016
     *Đọc 3 số thành chữ
     * @param $so
     * @return string
     */
    public static function doc3so($so)
    {
        $resualt = '';
        $arr_chubinhthuong = array('không', 'một', 'hai', 'ba', 'bốn', 'năm', 'sáu', 'bảy', 'tám', 'chín');
        $sohangtram = substr($so, 0, 1);
        $sohangchuc = substr($so, 1, 1);
        $sohangdonvi = substr($so, 2, 1);
        $resualt = $arr_chubinhthuong[$sohangtram] . ' trăm';
        if ($sohangchuc == 0 && $sohangdonvi != 0)
            $resualt .= ' linh ' . $arr_chubinhthuong[$sohangdonvi];
        elseif ($sohangchuc != 0)
            $resualt .= ' ' . MyFormat::doc2so($sohangchuc . $sohangdonvi);
        return $resualt;
    }

    public function doc($so)
    {
        $result = '';
        $arr_So = array('ty' => '',
            'trieu' => '',
            'nghin' => '',
            'tram' => '');
        $sochuso = strlen($so);
        for ($i = $sochuso - 1; $i >= 0; $i--) {

            if ($sochuso - $i <= 3) {
                $arr_So['tram'] = substr($so, $i, 1) . $arr_So['tram'];
            } elseif ($sochuso - $i > 3 && $sochuso - $i <= 6) {
                $arr_So['nghin'] = substr($so, $i, 1) . $arr_So['nghin'];
            } elseif ($sochuso - $i > 6 && $sochuso - $i <= 9) {
                $arr_So['trieu'] = substr($so, $i, 1) . $arr_So['trieu'];
            } else {
                $arr_So['ty'] = substr($so, $i, 1) . $arr_So['ty'];
            }
        }
        if ($arr_So['ty'] > 0)
            $result .= MyFormat::doc($arr_So['ty']) . ' tỷ';
        if ($arr_So['trieu'] > 0) {
            if ($arr_So['trieu'] >= 100 || $arr_So['ty'] > 0)
                $result .= ' ' . MyFormat::doc3so($arr_So['trieu']) . ' triệu';
            elseif ($arr_So['trieu'] >= 10)
                $result .= ' ' . MyFormat::doc2so($arr_So['trieu']) . ' triệu';
            else $result .= ' ' . MyFormat::doc1so($arr_So['trieu']) . ' triệu';
        }
        if ($arr_So['nghin'] > 0) {
            if ($arr_So['nghin'] >= 100 || $arr_So['trieu'] > 0)
                $result .= ' ' . MyFormat::doc3so($arr_So['nghin']) . ' nghìn';
            elseif ($arr_So['nghin'] >= 10)
                $result .= ' ' . MyFormat::doc2so($arr_So['nghin']) . ' nghìn';
            else $result .= ' ' . MyFormat::doc1so($arr_So['nghin']) . ' nghìn';
        }
        if ($arr_So['tram'] > 0) {
            if ($arr_So['tram'] >= 100 || $arr_So['nghin'] > 0)
                $result .= ' ' . MyFormat::doc3so($arr_So['tram']);
            elseif ($arr_So['tram'] >= 10)
                $result .= ' ' . MyFormat::doc2so($arr_So['tram']);
            else $result .= ' ' . MyFormat::doc1so($arr_So['tram']);
        }

        return $result;
    }

    /**
     * @Author: TRUNG June 14 2016
     *Đọc số thành chữ
     * @param $so
     * @return string
     */
    public static function numberToString($so)
    {
        if ($so > 99999999999999){
            return $so;
        }
        $result = '';
        $arr_So = array('ty' => '',
            'trieu' => '',
            'nghin' => '',
            'tram' => '');
        $sochuso = strlen($so);
        for ($i = $sochuso - 1; $i >= 0; $i--) {

            if ($sochuso - $i <= 3) {
                $arr_So['tram'] = substr($so, $i, 1) . $arr_So['tram'];
            } elseif ($sochuso - $i > 3 && $sochuso - $i <= 6) {
                $arr_So['nghin'] = substr($so, $i, 1) . $arr_So['nghin'];
            } elseif ($sochuso - $i > 6 && $sochuso - $i <= 9) {
                $arr_So['trieu'] = substr($so, $i, 1) . $arr_So['trieu'];
            } else {
                $arr_So['ty'] = substr($so, $i, 1) . $arr_So['ty'];
            }
        }
        if ($arr_So['ty'] > 0)
            $result .= MyFormat::doc($arr_So['ty']) . ' tỷ';
        if ($arr_So['trieu'] > 0) {
            if ($arr_So['trieu'] >= 100 || $arr_So['ty'] > 0)
                $result .= ' ' . MyFormat::doc3so($arr_So['trieu']) . ' triệu';
            elseif ($arr_So['trieu'] >= 10)
                $result .= ' ' . MyFormat::doc2so($arr_So['trieu']) . ' triệu';
            else $result .= ' ' . MyFormat::doc1so($arr_So['trieu']) . ' triệu';
        }
        if ($arr_So['nghin'] > 0) {
            if ($arr_So['nghin'] >= 100 || $arr_So['trieu'] > 0)
                $result .= ' ' . MyFormat::doc3so($arr_So['nghin']) . ' nghìn';
            elseif ($arr_So['nghin'] >= 10)
                $result .= ' ' . MyFormat::doc2so($arr_So['nghin']) . ' nghìn';
            else $result .= ' ' . MyFormat::doc1so($arr_So['nghin']) . ' nghìn';
        }
        if ($arr_So['tram'] > 0) {
            if ($arr_So['tram'] >= 100 || $arr_So['nghin'] > 0)
                $result .= ' ' . MyFormat::doc3so($arr_So['tram']);
            elseif ($arr_So['tram'] >= 10)
                $result .= ' ' . MyFormat::doc2so($arr_So['tram']);
            else $result .= ' ' . MyFormat::doc1so($arr_So['tram']);
        }

        return $result;
    }
    
    /**
     * @Author: DungNT Jul 23, 2016
     * @Todo: replace , from string number 123,456
     */
    public static function removeComma($number) {
        return str_replace(",", "", $number);
    }
    public static function removeCommaAndPoint($number) {
        $number = str_replace(".", "", $number);
        return str_replace(",", "", $number);
    }
    
    /**
     * @Author: DungNT Aug 11, 2016
     * @Todo: get duplicate value on array
     */
    public static function getArrayDuplicateValue($arr) {
        // find duplicate value on array
        $dups = array();
        foreach(array_count_values($arr) as $val => $c)
            if($c > 1) $dups[] = $val;
        
        echo '<pre>';
        print_r($dups);
        echo '</pre>';
        die;
    }
    
    public static function getRoleName($role_id) {
        $role_name = "";
        if(isset(Yii::app()->session)){
            $session    = Yii::app()->session;
            if(isset($session['ROLE_NAME_USER'])){
                $role_name  = $session['ROLE_NAME_USER'][$role_id];
            }
        }
        $mAppCache = new AppCache();
        $cacheRole = $mAppCache->getListdata('Roles', AppCache::LISTDATA_ROLE, 'role_name');
        if(isset($cacheRole[$role_id])){// add Now 29, 2016
            return $cacheRole[$role_id];
        }
        
        if(empty($role_name)){
            $role_name = Roles::model()->findByPk($role_id)->role_name;
        }
        return $role_name;
    }
    
    /**
     * @Author: DungNT Dec 01, 2016
     * @Todo: $json: array to endcode JSON_UNESCAPED_UNICODE
     */
    public static function jsonEncode($array) {
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }
    /**
     * @Author: DungNT Jun 11, 2017
     * @Todo: đổi 10000 thành 1 tỉ https://stackoverflow.com/questions/4371059/shorten-long-numbers-to-k-m-b
     */
    public static function shortenLongNumber($n) {
        if ($n < 1000000) {
            // Anything less than a million
            $n_format = number_format($n);
        } else if ($n < 1000000000) {
            // Anything less than a billion
            $n_format = number_format($n / 1000000, 3) . 'M';
        } else {
            // At least a billion
            $n_format = number_format($n / 1000000000) . ' B';
        }
        return $n_format;
    }
    
    /** @Author: DungNT Mar 27, 2018
     *  @Todo: chuyển agent id to province id
     **/
    public static function convertAgentToProvinceId($agent_id) {
        $mAppCache = new AppCache();
        $aAgent = $mAppCache->getAgent();
        return isset($aAgent[$agent_id]) ? $aAgent[$agent_id]['province_id'] : 0;
    }
    /** @Author: DungNT Apr 27, 2018
     *  @Todo: get List model agent của list province id
     **/
    public static function getAgentOfProvince($aProvince) {
        if(count($aProvince) < 1){
            return [];
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.role_id='.ROLE_AGENT);
        $criteria->addCondition('t.status=' . STATUS_ACTIVE);
        $sParamsIn = implode(',', $aProvince);// Sep 23, 2016 be careful For Big bug
        $criteria->addCondition("t.province_id IN ($sParamsIn)");
        $models = Users::model()->findAll($criteria);
        $aRes = [];
        foreach($models as $item){
            $aRes[$item->id] = $item;
        }
        return $aRes;
    }
    
    /** @Author: DungNT Dec 07, 2018
     *  @Todo: check leng of string valid of not
     **/
    public function isValidLength($string, $lengthAllow) {
        $ok = true;
        $sLen = MyFunctionCustom::remove_vietnamese_accents($string);
        if(strlen($sLen) > $lengthAllow ){
            $ok = false;
        }
        return $ok;
    }

    /** @Author: DungNT May 27, 2019
     *  @Todo: remove all white space
     **/
    public static function removeAllWhiteSpace($str) {
        return preg_replace('/\s+/', '', $str);
    }
    
    /** @Author: DungNT Jun 07, 2019
     *  @Todo: plus month 
     *  @param: $dateBegin: Y-m-d
     *  @param: $monthCount number month plus
     *  @return: array first day of each month
     * Array
        (
            [0] => 2019-06-01
            [1] => 2019-07-01
            [2] => 2019-08-01
        )
     **/
    public function plusMonth($dateBegin, $monthCount) {
        $aRes = [];
        $aMonth[]   = $dateBegin;
        $cMonth     = $dateBegin;
        for($i = 1; $i < $monthCount; $i++):
            $cMonth = MyFormat::modifyDays($cMonth, 1, '+', 'month');
            $aMonth[] = $cMonth;
        endfor;
        return $aMonth;
    }

}
