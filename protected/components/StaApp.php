<?php
class StaApp
{
    /**
     * @Author: DungNT Apr 21, 2017
     * @Todo: view report tồn kho trên appp
     * $model is model GasStoreCard $mStorecard
     */
    public static function calcStoreMovement($mStorecard, $needMore=[]){
    try{
        $aRes = [];
        $date_from  = $mStorecard->date_from_ymd;
        $date_to    = $mStorecard->date_to_ymd;
        $agent_id   = $mStorecard->agent_id;
        // 2. Lấy tồn đầu kỳ cho mỗi vật tư nhập ban đầu bởi đại lý
        $aRes['OPENING_BALANCE_YEAR_BEFORE'] = MyFunctionCustom::getOpeningBalanceYearBefore($agent_id);
//        $aRes['OPENING_IMPORT'] = MyFunctionCustom::calcTotalExportImportFixQuery($agent_id, $date_from, TYPE_STORE_CARD_IMPORT);
//        $aRes['OPENING_EXPORT'] = MyFunctionCustom::calcTotalExportImportFixQuery($agent_id, $date_from, TYPE_STORE_CARD_EXPORT);
        MyFunctionCustom::calcTotalExportImportFixQueryV1($aRes, $agent_id, $date_from);
        // XONG ĐOẠN CHO calcOpeningStockOnly        
//        // 6. Tính tổng nhập trong khoảng ngày $date_from, $date_to cho từng cấp con 
//        $aRes['IMPORT'] = MyFunctionCustom::calcTotalExportImportInRangeDate($agent_id, '', $date_from, $date_to, TYPE_STORE_CARD_IMPORT);
//        // 7. Tính tổng xuất trong khoảng ngày $date_from, $date_to cho từng cấp con
//        $aRes['EXPORT'] = MyFunctionCustom::calcTotalExportImportInRangeDate($agent_id, '', $date_from, $date_to, TYPE_STORE_CARD_EXPORT);
//        
        MyFunctionCustom::calcTotalExportImportInRangeDateV1($aRes, $agent_id, '', $date_from, $date_to);
        
        $needMore = array('GetAll'=>1);
        $aRes['aMaterials'] = GasMaterials::getArrayModelParentAndListSubId($needMore);
        return StaAppFormat::calcStoreMovement($mStorecard, $aRes);
        
    }catch (Exception $e){
        MyFormat::catchAllException($e);
    }
    }
    
    /**
     * @Author: DungNT Apr 24, 2017
     * @Todo: view report bán hàng hộ GĐ trên appp
     * $model is model Selll $mSell StaApp::hgd
     */
    public static function hgd($mSell){
    try{
        $aRes = $mSell->ReportAgent();
        return StaAppFormat::hgd($mSell, $aRes);
    }catch (Exception $e){
        MyFormat::catchAllException($e);
    }
    }
}
