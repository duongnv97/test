<?php

/* Jun 21, 2017 */

class Sta3
{
    public $user_id = 0, $date_from = 0, $date_to = 0, $source_app = '', $type = '';

    /**
     * Query sell list between date range
     *
     * @author : Trung June 21, 2017
     *
     * @param int $user_id
     * @param string $date_from
     * @param string $date_to
     * @param boolean $is_group_date is group date or not
     * @param $type
     * @return Sell
     */
    public function querySellListByDate($user_id, $type, $is_group_date = true)
    {
        if ($type == Call::QUEUE_BO_MOI) {
            return [];
        }
        $mAppCache      = new AppCache();
        $criteria       = new CDbCriteria ();
        $statusPaid     = Sell::STATUS_PAID;// Anh Dung fix Sep1817 , không tính đơn hàng hủy mà bị giá cao
        $statusCancel   = Sell::STATUS_CANCEL;
        $sCancelType    = Transaction::CANCEL_SUPPORT_AGENT_BUSY;
        $c              = "t.status=$statusPaid OR (t.status=$statusCancel AND t.high_price<>1 AND t.status_cancel NOT IN ($sCancelType) ) AND t.uid_login<>".GasConst::UID_ADMIN;
//        $criteria->addCondition('t.call_id > 0 AND t.status IN (' . Sell::STATUS_PAID . ',' . Sell::STATUS_CANCEL . ')');
//        $criteria->addCondition('t.call_id > 0 AND t.status IN (' . Sell::STATUS_PAID . ',' . Sell::STATUS_CANCEL . ')');
//        $criteria->addCondition('t.status IN (' . Sell::STATUS_PAID . ',' . Sell::STATUS_CANCEL . ')');
        $criteria->addCondition($c);
        if (!empty ($user_id)) {
            $criteria->addCondition('t.uid_login = ' . $user_id);
        }
        if ($type == Call::QUEUE_BO_MOI) {
            $aIdDieuPhoiBoMoi = $mAppCache->getArrayIdRole(ROLE_DIEU_PHOI);
            $sParamsIn = implode(',', $aIdDieuPhoiBoMoi);
            $criteria->addCondition('t.uid_login IN (' . $sParamsIn . ')');
        } elseif ($type == Call::QUEUE_HGD) {
            $aIdDieuPhoiBoMoi = $mAppCache->getArrayIdRole(ROLE_DIEU_PHOI);
            $aIdCallCenter = $mAppCache->getArrayIdRole(ROLE_CALL_CENTER);
            $aIdCallCenter = array_merge($aIdCallCenter, $aIdDieuPhoiBoMoi);
            $sParamsIn = implode(',', $aIdCallCenter);
            $criteria->addCondition('t.uid_login IN (' . $sParamsIn . ')');
        }
        DateHelper::searchBetween($this->date_from, $this->date_to, 'created_date_only_bigint', $criteria, false);
        $criteria->select = "count(t.id) as id, t.uid_login, t.status, t.status_cancel" . ($is_group_date ? ', t.created_date_only' : '');
        $criteria->group = "t.uid_login, t.status, t.status_cancel" . ($is_group_date ? ', t.created_date_only' : '');

        return Sell::model()->findAll($criteria);
    }

    /**
     * Query sell list between date range
     *
     * @author : Trung June 21, 2017
     *
     * @param unknown $user_id
     * @param unknown $date_from
     * @param unknown $date_to
     * @param $type
     * @param bool $isGroupHourOnly
     * @return Sell[]
     */
    public static function querySellListInHour($user_id, $date_from, $date_to, $type, $isGroupHourOnly = false)
    {
        $criteria = new CDbCriteria ();
        $mAppCache = new AppCache();

        $criteria->addCondition('t.status=' . Sell::STATUS_PAID);
        if (!empty ($user_id)) {
            $criteria->addCondition('t.uid_login = ' . $user_id);
        }
        if ($type == Call::QUEUE_BO_MOI) {
            $aIdDieuPhoiBoMoi = $mAppCache->getArrayIdRole(ROLE_DIEU_PHOI);
            $sParamsIn = implode(',', $aIdDieuPhoiBoMoi);
            $criteria->addCondition('t.uid_login IN (' . $sParamsIn . ')');
        } elseif ($type == Call::QUEUE_HGD) {
            $aIdDieuPhoiBoMoi = $mAppCache->getArrayIdRole(ROLE_DIEU_PHOI);
            $aIdCallCenter = $mAppCache->getArrayIdRole(ROLE_CALL_CENTER);
            $aIdCallCenter = array_merge($aIdCallCenter, $aIdDieuPhoiBoMoi);
            
            $sParamsIn = implode(',', $aIdCallCenter);
            $criteria->addCondition('t.uid_login IN (' . $sParamsIn . ')');
        }
        $criteria->addBetweenCondition("t.created_date_only", $date_from, $date_to);
        $criteria->select = "count(t.id) as id, t.uid_login, t.created_date_only, hour(created_date) as hour";
        $criteria->group = 'hour(created_date)' . ($isGroupHourOnly ? '' : ', t.created_date_only');

        return Sell::model()->findAll($criteria);
    }

    /**
     * Query call recall by user/date
     *
     * @author : Trung June 21, 2017
     *
     * @param unknown $user_id
     * @param unknown $date_from
     * @param unknown $date_to
     * @return unknown
     */
    public function queryForCallReport($model)
    {
        $criteria = new CDbCriteria ();
        $mCall = new Call();
        $sParamsInExtBoMoi  = implode(',', $mCall->getListExtBoMoi());
        if ($model->type == Call::QUEUE_BO_MOI) {
            $criteria->addCondition('t.destination_number IN ('.$sParamsInExtBoMoi.')');
        } elseif ($model->type == Call::QUEUE_HGD) {
            $criteria->addCondition('t.destination_number NOT IN ('.$sParamsInExtBoMoi.')');
        }
        $criteria->compare('user_id', $model->user_id);
        $criteria->compare('direction', Call::DIRECTION_INBOUND);
//        $criteria->addCondition('t.call_status IN (' . Call::STATUS_HANGUP . ',' . Call::STATUS_MISS_CALL . ')'); // Aut0217 A.Dung fix remove Miss Call
        $criteria->addCondition('t.call_status IN (' . Call::STATUS_HANGUP .')');
//        $criteria->addBetweenCondition("t.created_date_only", $this->date_from, $this->date_to);
        DateHelper::searchBetween($this->date_from, $this->date_to, 'created_date_bigint', $criteria, true);
        $criteria->addCondition('failed_status<>' . GasConst::SELL_CALL_TELESALE);// Jan1019 bỏ telesale ra khỏi tổng cuộc gọi
        
        $criteria->select = "count(t.id) as id, t.user_id, t.created_date_only";
        $criteria->group = "t.user_id, t.created_date_only";

        return Call::model()->findAll($criteria);
    }

    /**
     * Query call recall by user/date
     *
     * @author : Trung June 21, 2017
     *
     * @param unknown $user_id
     * @param unknown $date_from
     * @param unknown $date_to
     * @param bool $isGroupHourOnly
     * @return Call[]
     */
    public static function queryForCallReportInHour($model, $date_from, $date_to, $isGroupHourOnly = false)
    {
        $criteria = new CDbCriteria ();

        $mAppCache = new AppCache();
        $aIdDieuPhoiBoMoi = $mAppCache->getArrayIdRole(ROLE_DIEU_PHOI);
        $sParamsIn = implode(',', $aIdDieuPhoiBoMoi);
        $mCall = new Call();
        $sParamsInExtBoMoi  = implode(',', $mCall->getListExtBoMoi());
        if ($model->type == Call::QUEUE_BO_MOI) {
            $criteria->addCondition('t.user_id IN (' . $sParamsIn . ') AND t.destination_number IN ('.$sParamsInExtBoMoi.')');
        } elseif ($model->type == Call::QUEUE_HGD) {
            $criteria->addCondition('t.user_id NOT IN (' . $sParamsIn . ') AND t.destination_number NOT IN ('.$sParamsInExtBoMoi.')');
        }
        $criteria->compare('user_id', $model->user_id);
        $criteria->compare('direction', Call::DIRECTION_INBOUND);
        $criteria->addCondition('t.call_status IN (' . Call::STATUS_HANGUP . ',' . Call::STATUS_MISS_CALL . ')');
        $criteria->addBetweenCondition("t.created_date_only", $date_from, $date_to);
        $criteria->select = "count(t.id) as id, t.created_date_only, hour(created_date) as hour, t.call_status";
        $criteria->group = 't.call_status, hour(created_date)' . ($isGroupHourOnly ? '' : ', t.created_date_only');

        return Call::model()->findAll($criteria);
    }

    public static function queryForCallReportByFailedStatus($model, $date_from, $date_to)
    {
        $criteria = new CDbCriteria ();
        $mCall = new Call();
        $sParamsInExtBoMoi  = implode(',', $mCall->getListExtBoMoi());
        if ($model->type == Call::QUEUE_BO_MOI) {
//            $criteria->addCondition('t.user_id IN (' . $sParamsIn . ')');
            $criteria->addCondition('t.destination_number IN ('.$sParamsInExtBoMoi.')');
        } elseif ($model->type == Call::QUEUE_HGD) {
//            $criteria->addCondition('t.user_id NOT IN (' . $sParamsIn . ')');
            $criteria->addCondition('t.destination_number NOT IN ('.$sParamsInExtBoMoi.')');
        }
        $criteria->compare('user_id', $model->user_id);
        $criteria->compare('direction', Call::DIRECTION_INBOUND);
        $criteria->compare('failed_status', $model->failed_status);
        $criteria->addCondition('t.call_status IN (' . Call::STATUS_HANGUP . ')');
//        $criteria->addCondition('t.failed_status <> 0');
        $criteria->addCondition('t.sell_id = 0');
        $criteria->addBetweenCondition("t.created_date_only", $date_from, $date_to);
        $criteria->select = "count(t.id) as id, t.user_id, t.failed_status";
        $criteria->group = 't.user_id, t.failed_status';

        return Call::model()->findAll($criteria);
    }

    /**
     * query call report data by date and user id
     *
     * @author : Trung June 21, 2017
     */
    public function queryCallReportByDate($model)
    {
        $this->date_from    = $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
        $this->date_to      = $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);
        $this->user_id      = $model->user_id;
        $this->type         = $model->type;
        $this->source_app   = $model->source_app;

        $aRes = [];
        // 1. Query call record by user/date
        $models = $this->queryForCallReport($model);
        $this->callReportFormatCall($models, $aRes);
        
        // 2. Query sell record by user/date
        $models = $this->querySellListByDate($model->user_id, $model->type);
        $this->callReportFormatSell($models, $aRes);
        
        // 3. Query AppOrder bò mối record by user/date
        $this->queryOrderBoMoiListByDate($aRes);

        $mAppCache = new AppCache();
        $aRes ['LIST_CALL_CENTER_USER'] = ($mAppCache->getListdataUserByRole(ROLE_CALL_CENTER) + $mAppCache->getListdataUserByRole(ROLE_DIEU_PHOI));
        $aRes ['LIST_CALL_CENTER_USER'][2] = 'GN Trả Thẻ';
        $aRes ['DATE_ARRAY'] = MyFormat::getArrayDay($date_from, $date_to);
        arsort($aRes ['OUTPUT_SUM']);
        arsort($aRes ['OUTPUT_SUM_SALE']);
        return $aRes;
    }
    
    
    /** @Author: ANH DUNG Now 04, 2017
     *  @Todo: format data CallIn 
     **/
    public function callReportFormatCall($models, &$aRes) {
        $aRes ['OUTPUT_SUM'] = $aRes ['OUTPUT'] = [];
        foreach ($models as $item) {
            $aRes ['OUTPUT'] [$item->user_id] [$item->created_date_only] = $item->id;
            if (isset ($aRes ['OUTPUT_SUM'] [$item->user_id])) {
                $aRes ['OUTPUT_SUM'] [$item->user_id] += $item->id * 1;
            } else {
                $aRes ['OUTPUT_SUM'] [$item->user_id] = $item->id * 1;
            }
        }
    }
    
    /** @Author: ANH DUNG Now 04, 2017
     *  @Todo: format sell data
     **/
    public function callReportFormatSell($models, &$aRes) {
        $aRes ['OUTPUT_SUM_SALE'] = $aRes ['OUTPUT_SALE'] = [];
        foreach ($models as $item) {
            if(empty($item->status)){
                continue;
            }
            $status_id = $item->status;
            /** @var Sell $item */
            if (isset($aRes['OUTPUT_SALE'][$item->uid_login][$item->created_date_only][$status_id])) {
                $aRes['OUTPUT_SALE'][$item->uid_login][$item->created_date_only][$status_id] += $item->id;
            } else {
                $aRes['OUTPUT_SALE'][$item->uid_login][$item->created_date_only][$status_id] = $item->id;
            }
            // Count total
            if (isset($aRes['OUTPUT_SALE'][$item->uid_login][$item->created_date_only][0])) {
                $aRes['OUTPUT_SALE'][$item->uid_login][$item->created_date_only][0] += $item->id;
            } else {
                $aRes['OUTPUT_SALE'][$item->uid_login][$item->created_date_only][0] = $item->id;
            }
            // Count by status type
            if (isset ($aRes ['OUTPUT_SUM_SALE'][$item->uid_login][$status_id])) {
                $aRes['OUTPUT_SUM_SALE'][$item->uid_login][$status_id] += $item->id;
            } else {
                $aRes['OUTPUT_SUM_SALE'][$item->uid_login][$status_id] = $item->id;
            }
            // Count total
            if (isset ($aRes ['OUTPUT_SUM_SALE'] [$item->uid_login][0])) {
                $aRes['OUTPUT_SUM_SALE'][$item->uid_login][0] += $item->id;
            } else {
                $aRes['OUTPUT_SUM_SALE'][$item->uid_login][0] = $item->id;
            }
            // Prevent missing user
            if (!isset($aRes ['OUTPUT_SUM'][$item->uid_login])) {
                $aRes ['OUTPUT_SUM'][$item->uid_login] = 0;
            }
        }
        
    }

    /**
     * query call report data by date and user id and group by hour
     *
     * @author : Trung June 22, 2017
     */
    public static function queryCallReportInHour(&$model)
    {
        $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
        $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);

        $aRes = [];
        $aRes ['SUMMARIZE'] = [];

//      Get list call in hour by user/call_status/hour
        $models = Sta3::queryForCallReportInHour($model, $date_from, $date_to);
        foreach ($models as $item) {
            // Add hangup and miss call to report
            if ($item->call_status == Call::STATUS_HANGUP || $item->call_status == Call::STATUS_MISS_CALL) {
                $aRes ['SUMMARIZE'] [$item->call_status] [$item->created_date_only . '_' . $item->hour] = $item->id * 1;
            }
        }
        // Query sell record by user/call_status/hour
        $models = Sta3::querySellListInHour($model->user_id, $date_from, $date_to, $model->type);
        foreach ($models as $item) {
            /** @var Sell $item */
            $aRes ['SUMMARIZE'] [0] [$item->created_date_only . '_' . $item->hour] = $item->id * 1;
        }

        $aRes ['DATE_ARRAY'] = MyFormat::getArrayDay($date_from, $date_to);
        return $aRes;
    }

    /**
     * query call report data by date and user id and group by hour
     *
     * @author : Trung June 22, 2017
     */
    public static function queryCallReportByHour(&$model)
    {
        $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
        $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);

        $aRes = [];
        $aRes ['SUMMARIZE'] = [];

//      Get list call in hour by user/call_status/hour
        $models = Sta3::queryForCallReportInHour($model, $date_from, $date_to, true);
        foreach ($models as $item) {
            // Add hangup and miss call to report
            if ($item->call_status == Call::STATUS_HANGUP || $item->call_status == Call::STATUS_MISS_CALL) {
                $aRes ['SUMMARIZE'] [$item->call_status] [$item->hour] = $item->id * 1;
            }
        }
        // Query sell record by user/call_status/hour
        $models = Sta3::querySellListInHour($model->user_id, $date_from, $date_to, $model->type, true);
        foreach ($models as $item) {
            /** @var Sell $item */
            $aRes ['SUMMARIZE'] [0] [$item->hour] = $item->id * 1;
        }

        return $aRes;
    }

    /**
     * Get user name from id in list
     * @Author Trung June 22, 2017
     *
     * @param $list_users get from AppCache::getListdataUserByRole()
     * @param $user_id
     * @return string username
     */
    public static function getCallUserNameFromID($list_users, $user_id)
    {
        return $user_id > 0 ? isset($list_users[$user_id]) ?
            $list_users[$user_id] : $user_id : 'Chưa xác định';
    }

    /**
     * Query data for failed status report
     * @Author Trung July 8, 2017
     *
     * @param $model Call Model for query
     * @return array username
     */
    public static function queryCallReportByFailedStatus(&$model)
    {
        $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
        $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);

        // Query call record by user/failed status
        $models = Sta3::queryForCallReportByFailedStatus($model, $date_from, $date_to);

        $aRes = [];
        $aRes ['OUTPUT_SUM'] = [];
        $aRes ['OUTPUT_SUM_TYPE'] = [];
        $aRes ['OUTPUT'] = [];
        $mAppCache = new AppCache();
        foreach ($models as $item) {
            /** @var Call $item */
//            unset($aStatusFailed[GasConst::SELL_COMPLETE]);
            if(empty($item->failed_status) || $item->failed_status == GasConst::SELL_COMPLETE){
                continue ;
            }
            $aRes ['OUTPUT'][$item->failed_status][$item->user_id] = $item->id;
            if (isset ($aRes['OUTPUT_SUM'][$item->user_id])) {
                $aRes ['OUTPUT_SUM'] [$item->user_id] += $item->id * 1;
            } else {
                $aRes ['OUTPUT_SUM'] [$item->user_id] = $item->id * 1;
            }
            if (isset ($aRes['OUTPUT_SUM_TYPE'][$item->failed_status])) {
                $aRes ['OUTPUT_SUM_TYPE'][$item->failed_status] += $item->id * 1;
            } else {
                $aRes ['OUTPUT_SUM_TYPE'][$item->failed_status] = $item->id * 1;
            }
        }

        $aRes ['LIST_CALL_CENTER_USER'] = ($mAppCache->getListdataUserByRole(ROLE_CALL_CENTER) + $mAppCache->getListdataUserByRole(ROLE_DIEU_PHOI));
        arsort($aRes ['OUTPUT_SUM']);
        return $aRes;
    }

    /**
     * Query data for sell status report
     * @Author Trung July 11, 2017
     *
     * @param $model Call Model for query
     * @return array
     */
    public function queryCallReportBySellStatus(&$model)
    {
//        $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
//        $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);
        $this->date_from    = $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
        $this->date_to      = $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);

        // Query sell record by user/status
        $models = $this->querySellListByDate($model->user_id, $model->type, false);
        $aRes['OUTPUT_SALE'] = $aRes ['OUTPUT_SUM_SALE'] = [];
        foreach ($models as $item) {
            $status_id = $item->status;
            if ($status_id == Sell::STATUS_CANCEL) {
                $status_id = Sell::STATUS_CANCEL . '_' . $item->status_cancel;
            }
            /** @var Sell $item */
            if (isset($aRes['OUTPUT_SALE'][$status_id][$item->uid_login])) {
                $aRes['OUTPUT_SALE'][$status_id][$item->uid_login] += $item->id;
            } else {
                $aRes['OUTPUT_SALE'][$status_id][$item->uid_login] = $item->id;
            }
            // Count total
            if (isset($aRes['OUTPUT_SALE'][0][$item->uid_login])) {
                $aRes['OUTPUT_SALE'][0][$item->uid_login] += $item->id;
            } else {
                $aRes['OUTPUT_SALE'][0][$item->uid_login] = $item->id;
            }

            // Count by status type
            if (isset ($aRes ['OUTPUT_SUM_SALE'][$status_id])) {
                $aRes['OUTPUT_SUM_SALE'][$status_id] += $item->id;
            } else {
                $aRes['OUTPUT_SUM_SALE'][$status_id] = $item->id;
            }
            // Count total
            if (isset ($aRes ['OUTPUT_SUM_SALE'] [$item->uid_login][0])) {
                $aRes['OUTPUT_SUM_SALE'][0] += $item->id;
            } else {
                $aRes['OUTPUT_SUM_SALE'][0] = $item->id;
            }
        }

        $mAppCache = new AppCache();
        $aRes ['LIST_CALL_CENTER_USER'] = ($mAppCache->getListdataUserByRole(ROLE_CALL_CENTER) + $mAppCache->getListdataUserByRole(ROLE_DIEU_PHOI));
        $aRes['CANCEL_STATUS_LIST'] = Transaction::model()->getArrayStatusCancel();
        arsort($aRes ['OUTPUT_SALE']);
        return $aRes;
    }
    
    /** @Author: ANH DUNG Now 04, 2017
     *  @Todo: get list AppOrder in range date
     **/
    public function queryOrderBoMoiListByDate(&$aRes)
    {
        $mAppCache = new AppCache();
        $aUidCallCenter = $mAppCache->getListdataUserByRole(ROLE_CALL_CENTER) + $mAppCache->getListdataUserByRole(ROLE_DIEU_PHOI);
        if ($this->type == Call::QUEUE_HGD) {
            return ;
        }
        $criteria       = new CDbCriteria ();
//        $criteria->addCondition('t.status='.GasAppOrder::STATUS_COMPPLETE);
        if (!empty ($this->user_id)) {
            $criteria->addCondition('t.uid_login = ' . $this->user_id . ' OR t.uid_confirm = ' . $this->user_id);
        }
        if ($this->source_app == GasConst::VALUE_NO) {
            $criteria->addCondition('t.uid_login_role <> ' . ROLE_CUSTOMER);
        }elseif ($this->source_app == GasConst::VALUE_YES) {
            $criteria->addCondition('t.uid_login_role = ' . ROLE_CUSTOMER);
        }
        
        $sParamsIn = implode(',', UsersExtend::getAgentNotRunApp());
        $criteria->addCondition("t.agent_id NOT IN ($sParamsIn)");
        
//        $criteria->addBetweenCondition("t.date_delivery", $this->date_from, $this->date_to);
        DateHelper::searchBetween($this->date_from, $this->date_to, 'date_delivery_bigint', $criteria, false);
//        $criteria->select   = 'count( DISTINCT t.created_date ) as id, t.uid_login_role, t.uid_confirm, t.uid_login, t.date_delivery';
        $criteria->select   = 'count( DISTINCT CONCAT(t.customer_id, t.created_date) ) as id, t.uid_login_role, t.uid_confirm, t.uid_login, t.date_delivery, t.status';
//        https://stackoverflow.com/questions/1471250/counting-distinct-over-multiple-columns
        $criteria->group    = 't.created_date, t.customer_id';
//        $criteria->group    = 't.status, t.uid_login, t.date_delivery'; group theo kieu nay bi sai

        $models = GasAppOrder::model()->findAll($criteria);

        foreach ($models as $item) {
            $uid  = $item->uid_login;
            if($item->uid_login_role == ROLE_CUSTOMER){
                $uid  = $item->uid_confirm;
            }
            if(! array_key_exists ( $uid , $aUidCallCenter )){
                continue ;
            }
            $status_id = Sell::STATUS_PAID;// lấy biến này để Merge cùng với data của Sell
            if($item->status != GasAppOrder::STATUS_COMPPLETE){
                $status_id = Sell::STATUS_CANCEL;
//                echo $uid." -- ".$item->status . " -- $item->id <br>";
            }
            
            /** @var Sell $item */
            if (isset($aRes['OUTPUT_SALE'][$uid][$item->date_delivery][$status_id])) {
                $aRes['OUTPUT_SALE'][$uid][$item->date_delivery][$status_id] += $item->id;
            } else {
                $aRes['OUTPUT_SALE'][$uid][$item->date_delivery][$status_id] = $item->id;
            }
            // Count total
            if (isset($aRes['OUTPUT_SALE'][$uid][$item->date_delivery][0])) {
                $aRes['OUTPUT_SALE'][$uid][$item->date_delivery][0] += $item->id;
            } else {
                $aRes['OUTPUT_SALE'][$uid][$item->date_delivery][0] = $item->id;
            }
            // Count by status type
            if (isset ($aRes ['OUTPUT_SUM_SALE'][$uid][$status_id])) {
                $aRes['OUTPUT_SUM_SALE'][$uid][$status_id] += $item->id;
            } else {
                $aRes['OUTPUT_SUM_SALE'][$uid][$status_id] = $item->id;
            }
            // Count total
            if (isset ($aRes ['OUTPUT_SUM_SALE'][$uid][0])) {
                $aRes['OUTPUT_SUM_SALE'][$uid][0] += $item->id;
            } else {
                $aRes['OUTPUT_SUM_SALE'][$uid][0] = $item->id;
            }
            // Prevent missing user
            if (!isset($aRes ['OUTPUT_SUM'][$uid])) {
                $aRes ['OUTPUT_SUM'][$uid] = 0;
            }
        }
    }
    /** @Author: Pham Thanh Nghia Nov 30, 2018
     *  @Todo: Báo cáo đơn bảo trì bò mối và hộ GĐ gasUphold/ReportSum
     *  @Param: $model : GasUphold
     **/
    public function gasUpholdReportSum($model){
        $aUphold = $this->reportGasUphold($model); // bò
        
        $aUpholdHGD = $this->reportGasUpholdHGD($model); // HGD
        $result = [
            'aUphold' => $aUphold,
            'aUpholdHGD' => $aUpholdHGD,
        ];
        Yii::app()->session['dataUpholdReportSum'] = $result;
        return $result;
    }
    
    public function reportGasUphold($model){
        $criteria=new CDbCriteria;
        $date_from = $date_to = ''; $roleCustomer = ROLE_CUSTOMER;
        $aRes = $aSumRow = $aSumCol = [];
        $toTalSum = 0;
        if(!empty($model->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
        }
        if(!empty($model->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);
        }
        if(!empty($date_from)){
            $criteria->addCondition("DATE(t.created_date) >='$date_from'");
        }
        if(!empty($date_to)){
            $criteria->addCondition("DATE(t.created_date) <='$date_to'");
        }
        $criteria->addCondition("t.role_id<>$roleCustomer "); // AND t.status=" . GasUphold::STATUS_COMPLETE
//        $aDay = MyFormat::getArrayDay($date_from, $date_to);
        
        if($model->type_new == UpholdHgd::REPORT_AGENT){
            $criteria->select = "t.agent_id,DATE(t.created_date) as created_date, count(id) as id";
            $criteria->group = "t.agent_id, DATE(t.created_date) ";
            $model = GasUphold::model()->findAll($criteria); // bò
            foreach ($model as $value) {
                if(!isset($aRes[$value->agent_id][$value->created_date])){
                    $aRes[$value->agent_id][$value->created_date] = $value->id;
                }else{
                    $aRes[$value->agent_id][$value->created_date] += $value->id;
                }
                if(!isset($aSumRow[$value->agent_id])){
                    $aSumRow[$value->agent_id] = $value->id;
                }else{
                    $aSumRow[$value->agent_id] += $value->id;
                }
                if(!isset($aSumCol[$value->created_date])){
                    $aSumCol[$value->created_date] = $value->id;
                }else{
                    $aSumCol[$value->created_date] += $value->id;
                }
                $toTalSum+=$value->id;
            }
            
        }else{ // uid_login
            $aIdUser = Users::getArrIdUserByRole([ROLE_CALL_CENTER, ROLE_DIEU_PHOI]);
            $criteria->addInCondition("t.uid_login", $aIdUser);
            $criteria->select = "t.uid_login ,DATE(t.created_date) as created_date, count(id) as id";
            $criteria->group = "t.uid_login, DATE(t.created_date) ";
            $model = GasUphold::model()->findAll($criteria);
            foreach ($model as $value) {
                if(!isset($aRes[$value->uid_login][$value->created_date])){
                    $aRes[$value->uid_login][$value->created_date] = $value->id;
                }else{
                    $aRes[$value->uid_login][$value->created_date] += $value->id;
                }
                if(!isset($aSumRow[$value->uid_login])){
                    $aSumRow[$value->uid_login] = $value->id;
                }else{
                    $aSumRow[$value->uid_login] += $value->id;
                }
                if(!isset($aSumCol[$value->created_date])){
                    $aSumCol[$value->created_date] = $value->id;
                }else{
                    $aSumCol[$value->created_date] += $value->id;
                }
                $toTalSum+=$value->id;
            }
        } // end if
        $aDay = MyFormat::getArrayDay($date_from, $date_to);
        $result = [
            'data' => $aRes,
            'toTalSum'  => $toTalSum,
            'aDay' => $aDay,
            'aSumCol' => $aSumCol,
            'aSumRow' => $aSumRow,
        ];
        return $result;
    }
    // UpholdHgd
    public function reportGasUpholdHGD($model){
        $criteria=new CDbCriteria;
        $date_from = $date_to = '';
        $aRes = $aSumRow = $aSumCol = [];
        $toTalSum = 0;
        if(!empty($model->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
        }
        if(!empty($model->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);
        }
        if(!empty($date_from)){
            $criteria->addCondition("DATE(t.created_date) >='$date_from'");
        }
        if(!empty($date_to)){
            $criteria->addCondition("DATE(t.created_date) <='$date_to'");
        }
//        $criteria->compare('t.status',UpholdHgd::STATUS_COMPLETE); lay het don hang
//        $aDay = MyFormat::getArrayDay($date_from, $date_to);
        
        if($model->type_new == UpholdHgd::REPORT_AGENT){
            $criteria->select = "t.agent_id,DATE(t.created_date) as created_date, count(id) as id";
            $criteria->group = "t.agent_id, DATE(t.created_date) ";
            $model = UpholdHgd::model()->findAll($criteria);
            foreach ($model as $value) {
                if(!isset($aRes[$value->agent_id][$value->created_date])){
                    $aRes[$value->agent_id][$value->created_date] = $value->id;
                }else{
                    $aRes[$value->agent_id][$value->created_date] += $value->id;
                }
                if(!isset($aSumRow[$value->agent_id])){
                    $aSumRow[$value->agent_id] = $value->id;
                }else{
                    $aSumRow[$value->agent_id] += $value->id;
                }
                if(!isset($aSumCol[$value->created_date])){
                    $aSumCol[$value->created_date] = $value->id;
                }else{
                    $aSumCol[$value->created_date] += $value->id;
                }
                $toTalSum+=$value->id;
            }
            
        }else{ // uid_login
            $aIdUser = Users::getArrIdUserByRole([ROLE_CALL_CENTER, ROLE_DIEU_PHOI]);
            $criteria->addInCondition("t.uid_login", $aIdUser);
            $criteria->select = "t.uid_login ,DATE(t.created_date) as created_date, count(id) as id";
            $criteria->group = "t.uid_login, DATE(t.created_date) ";
            $model = UpholdHgd::model()->findAll($criteria);
            foreach ($model as $value) {
                if(!isset($aRes[$value->uid_login][$value->created_date])){
                    $aRes[$value->uid_login][$value->created_date] = $value->id;
                }else{
                    $aRes[$value->uid_login][$value->created_date] += $value->id;
                }
                if(!isset($aSumRow[$value->uid_login])){
                    $aSumRow[$value->uid_login] = $value->id;
                }else{
                    $aSumRow[$value->uid_login] += $value->id;
                }
                if(!isset($aSumCol[$value->created_date])){
                    $aSumCol[$value->created_date] = $value->id;
                }else{
                    $aSumCol[$value->created_date] += $value->id;
                }
                $toTalSum+=$value->id;
            }
        } // end if
        
        $result = [
            'data' => $aRes,
            'toTalSum'  => $toTalSum,
//            'aDay' => $aDay,
            'aSumCol' => $aSumCol,
            'aSumRow' => $aSumRow,
        ];
        
//        $_SESSION['dataReportForCallCenter'] = $result;
        return $result;
    }
    
    /** @Author: DuongNV Dec 6,2018
     *  @Todo: Báo cáo khách hàng giao khó ReportCustomer/reportHardDelivery
     *  @Param: $model để get date_from, date_to
     **/
    public function reportHardDelivery($model) {
        $res           = [];
        $date_from     = MyFormat::dateConverDmyToYmd($model->date_from, '-');
        $date_to       = MyFormat::dateConverDmyToYmd($model->date_to, '-');
        $criteria      = new CDbCriteria;
//        $aHardCustomer = $this->getHardDeliveryCustomer();
        $aHardCustomer = [];
        $aSupportPrice = $this->getArrSupportHardDelivery($date_from, $date_to, $aHardCustomer);
        $criteria->addBetweenCondition('t.date_delivery', $date_from, $date_to);
        $criteria->addInCondition("t.materials_type_id", array_keys($this->arrBinh([12, 45, 50])));
        $criteria->addInCondition('t.customer_id', $aHardCustomer);
        $models = GasStoreCardDetail::model()->findAll($criteria);
        if(empty($models)) return [];
        $aCustomer = [];
        $aEmployee = [];
        $aSale     = [];
        
        foreach ($models as $value) {
            $aCustomer[$value->customer_id] = $value->customer_id;
        }
        
//        $aSupportPrice = $this->getArrSupportHardDelivery($date_from, $date_to, $aCustomer);
        foreach ($models as $value) {
            $id_binh       = $value->materials_type_id;
            if($id_binh == MATERIAL_TYPE_BINHBO_50){ // Bình 45 và 50 tính chung, thành cột bình 45
                $id_binh = MATERIAL_TYPE_BINHBO_45;
            }
            $support_price = isset($aSupportPrice[$value->customer_id][$id_binh]) ? $aSupportPrice[$value->customer_id][$id_binh] : 0;
            $total_money   = $value->qty * $support_price;
            $idPVKHorTaiXe = $value->employee_maintain_id;
//            $aCustomer[$value->customer_id] = $value->customer_id;
            $aSale[$value->sale_id]         = $value->sale_id;
            /*
             * Nếu có employee_maintain_id thì tiền = 100%, ngược lại sẽ chia đôi cho tài xế và phụ xe
             */
            if(empty($value->employee_maintain_id)){
                $idPVKHorTaiXe  = $value->driver_id_1;
                $money          = empty($value->phu_xe_1) ? $total_money : $total_money/2;
                $aEmployee[$value->driver_id_1] = $value->driver_id_1;
//                $aEmployee[$value->phu_xe_1]    = $value->phu_xe_1;
                // Thành tiền bảng ds nhân viên
                $this->setValue($res['emp_money'][$value->driver_id_1], $money);
//                $this->setValue($res['emp_money'][$value->phu_xe_1], $money);
                
                // DuongNV May 27,19 Nếu ko có phụ xe thì tài xế nhận 100%, ngược lại thì chia đôi
                if( !empty($value->phu_xe_1) ){
                    $aEmployee[$value->phu_xe_1] = $value->phu_xe_1;
                    $this->setValue($res['emp_money'][$value->phu_xe_1], $money);
                }
            } else {
                $aEmployee[$value->employee_maintain_id] = $value->employee_maintain_id;
                // Thành tiền bảng ds nhân viên
                $this->setValue($res['emp_money'][$value->employee_maintain_id], $total_money);
            }
            // Số lượng bình
            $this->setValue($res['output'][$value->customer_id][$value->sale_id][$id_binh], $value->qty);
            // Thành tiền
            $this->setValue($res['output'][$value->customer_id][$value->sale_id]['amount'], $total_money);
            // Tổng
            if(isset($res['sum'])){
                $res['sum'] += $total_money;
            } else {
                $res['sum']  = $total_money;
            }
        }
        // Đơn giá mỗi khách hàng
        $res['price']    = $this->getPriceByListUser($aCustomer, $date_from);
        // Object user
        $res['customer'] = empty($aCustomer) ? [] : Users::getArrObjectUserByRole("", $aCustomer);
        $res['sale']     = empty($aSale)     ? [] : Users::getArrObjectUserByRole("", $aSale);
        $res['employee'] = empty($aEmployee) ? [] : Users::getArrObjectUserByRole("", $aEmployee);
        $res['part']     = $this->getArrayPart($aEmployee); // Bộ phận, vd kho phước tân
        $res['binh']     = $this->arrBinh([12, 45]);
        // Sort theo bộ phận tăng dần
        if(!empty($res['employee'])){
            uasort($res['employee'], function($a, $b){
                return $a->parent_id > $b->parent_id;
            });
        }
        $_SESSION['data-excel-hard'] = $res;
        return $res;
    }
    
    /** @Author: DuongNV Dec 6,2018
     *  @Todo: get array part (bộ phận) by array user id
     **/
    public function getArrayPart($aEmployeeId) {
        $aEmployee = Users::getArrObjectUserByRole("", $aEmployeeId);
        $aPart     = [];
        foreach ($aEmployee as $mUser) {
            $aPart[$mUser->parent_id] = $mUser->parent_id;
        }
        return Users::getArrObjectUserByRole("", $aPart);
    }
    
    /** @Author: DuongNV Dec 6,2018
     *  @Todo: get array id hard delivery customer
     **/
    public function getHardDeliveryCustomer() {
        $criteria = new CDbCriteria;
        $criteria->compare('t.first_char', Users::GROUP_GIAO_KHO);
        $models = Users::model()->findAll($criteria);
        return CHtml::listData($models, 'id', 'id');
    }
    
    /** @Author: DuongNV Dec 6,2018
     *  @Todo: get array bình by array number
     *  @param $aNumber danh sách bình, vd bình 12, bình 45 => $aNumber = [12, 45]
     **/
    public function arrBinh($aNumber = []) {
        $aRes  = [];
        $aBinh = [
            MATERIAL_TYPE_BINHBO_50 => "Bình 50",
            MATERIAL_TYPE_BINHBO_45 => "Bình 45",
            MATERIAL_TYPE_BINH_12   => "Bình 12",
            MATERIAL_TYPE_BINH_6    => "Bình 6",
            MATERIAL_TYPE_BINH_4    => "Bình 4",
        ];
        $aBinhNumber = CmsFormatter::$MATERIAL_VALUE_KG;
        if(empty($aNumber)) return $aBinh;
        foreach ($aNumber as $number) {
            $key = array_search($number, $aBinhNumber);
            if(isset($aBinh[$key])){
                $aRes[$key] = $aBinh[$key];
            }
        }
        return $aRes;
    }
    
    /** @Author: DuongNV 12 Dec, 2018
     *  @Todo: get price by list user 
     *  @param: $dateSearch Y-m-d
     **/
    public function getPriceByListUser($aUser, $dateSearch) {
        if(empty($aUser)) return [];
        $tempDate = explode('-', $dateSearch);
        $criteria = new CDbCriteria;
        $criteria->addInCondition('t.customer_id', $aUser);
        $criteria->compare('t.c_month', $tempDate[1]*1);
        $criteria->compare('t.c_year', $tempDate[0]);
        $models   = UsersPrice::model()->findAll($criteria);
        $res      = [];
        foreach ($models as $value) {
//            $res[$value->customer_id] = $value->price_small;
            $res[$value->customer_id] = [
                'price12' => $value->price_small,
                'price45' => $value->price,
            ];
        }
        return $res;
    }
    
    /** @Author: DuongNV 12 Dec, 2018
     *  @Todo: get array money hỗ trợ cho nhân viên giao hàng khó
     **/
    public function getArrSupportHardDelivery($from, $to, &$aCustomer = []) {
        $criteria = new CDbCriteria;
        $criteria->compare('t.type', PriceSupport::TYPE_HARD_DELIVERY);
        $criteria->compare('t.status', STATUS_ACTIVE);
        $criteria->addBetweenCondition('t.date_apply', $from, $to);

//        if(!empty($aCustomer)){
//            $criteria->addInCondition('t.customer_id', $aCustomer);// không cần thiết sử dụng Condition này
//        }
        $models = PriceSupport::model()->findAll($criteria);
        $res    = [];
        foreach ($models as $value) {
            $res[$value->customer_id][MATERIAL_TYPE_BINH_12]   = $value->price_12;
            $res[$value->customer_id][MATERIAL_TYPE_BINHBO_45] = $value->price_45;
            $aCustomer[$value->customer_id]                    = $value->customer_id;
        }
        return $res;
    }
    
    /** @Author: DuongNV 12 Dec, 2018
     *  @Todo: set value to variable if isset
     **/
    public function setValue(&$var, $value) {
        if(isset($var)){
            $var += $value;
        } else {
            $var  = $value;
        }
    }
    
    /** @Author: Nguyen Khanh Toan Dec 14, 2018
     *  @Todo: Báo cáo đơn hàng bảo trì bò mối và hộ GĐ gasUphold/ReportSum
     *  @Param: $model : GasUphold
     **/
    public function gasUpholdReportMaintenance($model){
        $aUphold = $this->reportGasUpholdMaintenance($model); // bò
        $aUpholdHGD = $this->reportGasUpholdHGDMaintenance($model); // HGD
        
        $result = [
            'aUphold' => $aUphold,
            'aUpholdHGD' => $aUpholdHGD,
        ];
        $_SESSION['dataReportMaintenance'] = $result;
        return $result;
    }
    //Khanh Toan Dec 14 GasUphold
    public function reportGasUpholdMaintenance($model){
        $criteria=new CDbCriteria;
        $date_from = $date_to = ''; 
        $roleCustomer = ROLE_CUSTOMER;
        $aRes = [];
        if(!empty($model->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
        }
        if(!empty($model->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);
        }
        if(!empty($date_from)){
            $criteria->addCondition("DATE(t.created_date) >='$date_from'");
        }
        if(!empty($date_to)){
            $criteria->addCondition("DATE(t.created_date) <='$date_to'");
        }
        $criteria->addCondition("t.role_id<>$roleCustomer");
         // uid_login
//        $aIdUser = Users::getArrIdUserByRole(ROLE_CALL_CENTER);
        $aIdUser = Users::getArrIdUserByRole([ROLE_CALL_CENTER, ROLE_DIEU_PHOI]);
        $criteria->addInCondition("t.uid_login", $aIdUser);
        $criteria->select = "t.uid_login , count(id) as id, sum(if(t.status = ".GasUphold::STATUS_COMPLETE.",1,0)) as status";
        $criteria->group = "t.uid_login ";
        $mGasUpholds = GasUphold::model()->findAll($criteria);
        foreach ($mGasUpholds as $mGasUphold) {
            if(!isset($aRes[$mGasUphold->uid_login]['SUM'])){
                $aRes['OUTPUT'][$mGasUphold->uid_login]['SUM'] = $mGasUphold->id;
            }else{
                $aRes['OUTPUT'][$mGasUphold->uid_login]['SUM'] += $mGasUphold->id;
            }
            if(!isset($aRes[$mGasUphold->uid_login]['COMPLETE'])){
                $aRes['OUTPUT'][$mGasUphold->uid_login]['COMPLETE'] = $mGasUphold->status;
            }else{
                $aRes['OUTPUT'][$mGasUphold->uid_login]['COMPLETE'] += $mGasUphold->status;
            }
            if(!isset($aRes['SUMALL'])){
                $aRes['SUMALL'] = $mGasUphold->id;
            }else{
                $aRes['SUMALL'] += $mGasUphold->id;
            }
            if(!isset($aRes['SUM_COMPLETE'])){
                $aRes['SUM_COMPLETE'] = $mGasUphold->status;
            }else{
                $aRes['SUM_COMPLETE'] += $mGasUphold->status;
            }
        } 
        return $aRes;
    }
    //Khanh Toan Dec 14 UpholdHgd
    public function reportGasUpholdHGDMaintenance($model){
        $criteria=new CDbCriteria;
        $date_from = $date_to = ''; 
        $roleCustomer = ROLE_CUSTOMER;
        $aRes = [];
        if(!empty($model->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
        }
        if(!empty($model->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);
        }
        if(!empty($date_from)){
            $criteria->addCondition("DATE(t.created_date) >='$date_from'");
        }
        if(!empty($date_to)){
            $criteria->addCondition("DATE(t.created_date) <='$date_to'");
        }
         // uid_login
        $aIdUser = Users::getArrIdUserByRole([ROLE_CALL_CENTER, ROLE_DIEU_PHOI]);
//        $criteria->addInCondition("t.uid_login", $aIdUser);
        $criteria->select = "t.uid_login, count(id) as id, sum(if(t.status = ".UpholdHgd::STATUS_COMPLETE.",1,0)) as status";
        $criteria->group = "t.uid_login";
        $mUpholdHgds = UpholdHgd::model()->findAll($criteria);
        foreach ($mUpholdHgds as $mUpholdHgd) {
            if(!isset($aRes[$mUpholdHgd->uid_login]['SUM'])){
                $aRes['OUTPUT'][$mUpholdHgd->uid_login]['SUM'] = $mUpholdHgd->id;
            }else{
                $aRes['OUTPUT'][$mUpholdHgd->uid_login]['SUM'] += $mUpholdHgd->id;
            }
            if(!isset($aRes[$mUpholdHgd->uid_login]['COMPLETE'])){
                $aRes['OUTPUT'][$mUpholdHgd->uid_login]['COMPLETE'] = $mUpholdHgd->status;
            }else{
                $aRes['OUTPUT'][$mUpholdHgd->uid_login]['COMPLETE'] += $mUpholdHgd->status;
            }
            if(!isset($aRes['SUMALL'])){
                $aRes['SUMALL'] = $mUpholdHgd->id;
            }else{
                $aRes['SUMALL'] += $mUpholdHgd->id;
            }
            if(!isset($aRes['SUM_COMPLETE'])){
                $aRes['SUM_COMPLETE'] = $mUpholdHgd->status;
            }else{
                $aRes['SUM_COMPLETE'] += $mUpholdHgd->status;
            }
        } 
        return $aRes;
    }
    
}
