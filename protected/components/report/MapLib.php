<?php
/* Feb2818 some function map - location */
class MapLib {
    
    public $earthRadius = 6371000; // m
    public $maxRange    = 8000; // m
    public $username = '';
    
    /** get array agent test, không check show on app Gas24h
     **/
    public static function getAgentHideLocation() {
       return [
            768411,// Công Ty CP Dầu Khí Miền Nam 
            1315, // Cửa hàng 5
        ];
    }

    /** @Author: ANH DUNG Feb 28, 2018
     *  @Todo: thu nhỏ lại tập hợp điểm thỏa mãn, những điểm nào quá $maxRange thì không đưa vào tính toán
     * limit agent
     * @param: $aLocationAgent array agent_id => location + radius agent
     * @param: $userLatitude $userLongitude current location user
     * @return: mảng info agent nằm trong $maxRange: array agent_id => location + radius agent
     **/
    public function miniatureRadius($aLocationAgent, $userLatitude, $userLongitude) {
        $aRes = [];
        if(empty($userLatitude) || empty($userLongitude)){
            return [];
        }
        $maxRadiusRange = $this->maxRange / ($this->earthRadius * M_PI) * 180;
        $currentMinRadiusRange = $maxRadiusRange;

        // Filter agent in range
        foreach ($aLocationAgent as $agent_id => $aInfo) {
            if(empty($aInfo['location'])){
                continue ;
            }
            $temp = explode(',', $aInfo['location']);
            if(!isset($temp[1])){
                continue ;
            }
            $currentAgentLat    = $temp[0];
            $currentAgentLong   = $temp[1];
            $distanceX = abs($userLatitude - $currentAgentLat);
            $distanceY = abs($userLongitude - $currentAgentLong);
            if ($distanceX < $maxRadiusRange && $distanceY < $maxRadiusRange) {
                $distanceXY = sqrt(pow($distanceX, 2) + pow($distanceY, 2));
                if ($currentMinRadiusRange >= $distanceXY) {
                    $currentMinRadiusRange = $distanceXY;
                    $aRes[$agent_id] = $aInfo;
                }
            }
        }
        
        return $aRes;
    }
    
    /**
     * Calculates the great-circle distance between two points, with
     * the Haversine formula.
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param float $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [m] (same as earthRadius)
     */
    function haversineGreatCircleDistance(
        $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }
    
    
    /** @Author: ANH DUNG Feb 28, 2018
     *  @Todo: tìm đại lý gần nhất của KH
     * tính khoảng cách từ customer đến các đại lý
     * sau đó lấy đại lý gân user nhất so sánh bán kính có hợp lệ không
     *  @param: $q is params post of user
     *  @return: agent_id
     **/
    public function gas24hGetAgentNearest($latitude, $longitude) {
        if(empty($latitude) || empty($longitude)){
            return 0;
        }
        $mGasRemain = new GasRemain();

        $mAppCache      = new AppCache();
        $aLocationAgent = $mAppCache->getGas24hAgentRadius();
        $aAgentNearest  = $this->miniatureRadius($aLocationAgent, $latitude, $longitude);
        $aDistanceToAgent      = [];
        foreach($aAgentNearest as $agent_id => $aInfo){
            if(array_key_exists($agent_id, $mGasRemain->getArrayAgentCar())){
                continue;// Apr0118 tạm bỏ đại lý xe tải ra khỏi list của AppGas24h
            }
            $temp = explode(',', $aInfo['location']);
            // tính khoảng cách từ customer đến các đại lý
            $aDistanceToAgent[$agent_id] = $this->haversineGreatCircleDistance($latitude, $longitude, $temp[0], $temp[1]);
        }
        
        if(count($aDistanceToAgent) == 0){
            if(Yii::app()->params['EnableChangeExt'] == 'yes'):
//                Logger::WriteLog("Username: {$this->username} Location $latitude,$longitude");
            endif;
            return 0;
        }
        asort($aDistanceToAgent);// tăng dần asort low to high
        $agentNearest       = 0;
        $agentDistanceMin   = 0;
        foreach($aDistanceToAgent as $agent_id => $agentDistance){
            $agentNearest       = $agent_id;
            $agentDistanceMin   = $agentDistance;
            break;
        }
        $this->debugAgent($aAgentNearest, $aDistanceToAgent, $agentNearest);
        if($agentNearest != 0 && $agentDistanceMin < $aAgentNearest[$agentNearest]['radius']){
            return $agentNearest;// nếu khoảng cách ngắn nhất nhỏ hơn bán kính đại lý thì thỏa mãn
        }
        
        return 0;
    }
    
    /** @Author: ANH DUNG Feb 19, 2018
     *  @Todo: for debug only
     **/
    public function debugAgent($aAgentNearest, $aDistanceToAgent, $agentNearest) {
        if(Yii::app()->params['EnableChangeExt'] == 'no'):
            return ;
        endif;
        $mAppCache = new AppCache();
        $aAgent = $mAppCache->getAgentListdata(); $log = '';
        foreach($aAgentNearest as $agent_id => $aInfo){
            $name = isset($aAgent[$agent_id]) ? $aAgent[$agent_id] : $agent_id;
            $name = MyFunctionCustom::remove_vietnamese_accents($name);
            $log .= "Username: {$this->username} - $agent_id $name - radius - ". $aInfo['radius'].' Khoang cach: '.$aDistanceToAgent[$agent_id]." SELECT AGENT: $agentNearest<br>";
        }
//        Logger::WriteLog($log);

    }
    
    
}
