<?php
/** Class for function Readonly*/
class FuncReadonly
{
    public $agent_id = 0;
    /** @Author: ANH DUNG Jun 15, 2018
     *  @Todo: get các chương trình KM, đưa vào hỗ trợ cho tổng đài xem
     */
    public function getListPromotion() {
        return [
                112 => [// Đại lý Thủ Đức 1 
                    'expiry_date'   => '2018-09-14',
                    'msg'           => "1. Lấy gas có quà là 2 lít dầu hoặc 1 cái đế gas
                                       <br>2. Không quà giảm 50k",
                ],
                1210050 => [// Đại Lý Tp Bến Tre
                    'expiry_date'   => '2018-12-31',
                    'msg'           => "1. Lấy gas có quà gồm 1 chai dầu ăn cộng nửa kg đường, 5 cái chén, 2 cái tô, 3 cái ly, dây gas.
                                       <br>2. Không quà giảm 50k",
                ],
                1525846 => [// Đại Lý Sơn Đông - Bến Tre
                    'expiry_date'   => '2018-12-31',
                    'msg'           => "1. Lấy gas có quà gồm 1 chai dầu ăn cộng nửa kg đường, 5 cái chén, 2 cái tô, 3 cái ly, dây gas.
                                       <br>2. Không quà giảm 50k",
                ],
                107 => [// Đại lý Gò Vấp 1
                    'expiry_date'   => '2018-10-31',
                    'msg'           => "1. Quà 2 lít dầu or 1 đế gas
                                       <br>2. Không quà giảm 50k",
                ],
                148404 => [// Cửa hàng Vũng Liêm 
                    'expiry_date'   => '2018-12-31',
                    'msg'           => "1. Lấy gas có quà gồm 1 chai dầu ăn cộng nửa kg đường, 5 cái chén, 2 cái tô, 3 cái ly, dây gas.
                                       <br>2. Không quà giảm 50k",
                ],
                100 => [// Đại lý Quận 2
                    'expiry_date'   => '2018-12-13',
                    'msg'           => "1. Quà 2 lít dầu
                                       <br>2. Không quà giảm 50k",
                ],
                105 => [// Đại lý Quận 9
                    'expiry_date'   => '2019-01-18',
                    'msg'           => "1. Gọi điện tặng 2 lít dầu or 2kg đường or 1lít dầu và 1kg đường
                                       <br>2. Không quà giảm 50k",
                ],
        ];
    }
    
    /** @Author: ANH DUNG Jun 15, 2018
     *  @Todo: get các chương trình KM, đưa vào hỗ trợ cho tổng đài xem
     */
    public function getAgentPromotion() {
        $res = ''; $today = date('Y-m-d');
        if(array_key_exists($this->agent_id, $this->getListPromotion())){
            $aData = $this->getListPromotion();
            $dateExp = MyFormat::dateConverYmdToDmy($aData[$this->agent_id]['expiry_date']);
            if(MyFormat::compareTwoDate($aData[$this->agent_id]['expiry_date'], $today)){
                $res = "Chương trình khuyến mãi hiện tại của đại lý: Ngày hết hạn $dateExp<br>".$aData[$this->agent_id]['msg'];
            }
        }
        return $res;
    }
    
    
}
