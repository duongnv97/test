<?php
/** Feb1419 Class for function scan something to update */
class CronScan
{
    /** @Author: DungNT Feb 14, 2019 - 5 phut scan 1 lan
     *  @Todo: 1 ngày 1 lần 4h30 Quét NV không giao Gas trong ngày mà còn tồn quỹ trên 10k, nghĩa là không phát sinh thu, chi thì xử lý 
     * Scan trừ tiền tính lỗi âm quỹ tự động cho nvpv 20% sô tiền âm quỹ, 
     * nhắc nhở cho gs, ktkv (lần 1) sau 8 tiếng sẽ quét lần 2. Nếu vẫn âm quỹ sẽ tính lỗi luôn gs, ktkv 20% trên tổng số tiền âm quỹ
     * @flow: get record trong 2 ngày để check lần 1 và 2
        1. Lần 1 PVKH Save vào table gas_employee_problems, trc khi save check xem ngày hôm đó đã phạt NV chưa, nếu rồi thì thôi
        1.1. notify phone + email cho các GS và KTKV

        2. Lần 2 sau 8h sẽ quét nếu vẫn âm quỹ thì tính lỗi cho GS và KTKV 20% trên tổng số tiền âm
        get những record bị phạt lần 1 ra + 8h nếu lớn hơn time hiện tại thì phạt GS + KTKV > check nếu rồi thì thôi ko insert nữa
     *  3. notify to user -- done
     **/
    public function scanCheatCashbook() {
        $from = time();
        $mEmployeeProblems  =new EmployeeProblems();
        $mMonitorUpdate     =new MonitorUpdate();
        $aAgentNotReport    = $mMonitorUpdate->getAgentNotReport();
        $needMore           = ['ArrAgentIdNotFind' => $aAgentNotReport];
        // tính tồn quỹ của all employee
        $mCashBookDetail    =new GasCashBookDetail();
        $mCashBookDetail->date_from         = date('d-m-Y');
        $mCashBookDetail->date_to           = $mCashBookDetail->date_from;
//        $mMonitorUpdate->handleErrorBankTransferAddException($mCashBookDetail);
        $mCashBookDetail->isInventoryAlert  = true;
        $mCashBookDetail->getInventoryCashAllEmployee($needMore);
        
        /*** Scan2 sau 8h sẽ quét nếu vẫn âm quỹ thì tính lỗi cho GS và KTKV 20% trên tổng số tiền âm 
         * phai dua Scan2 run trc de tranh ham scanNegativeCashbookRecheckData()
         */
        $mEmployeeProblems->handleNegativeCashbookScan2($mCashBookDetail);
        $this->scanNegativeCashbookRecheckData($mCashBookDetail);
        if(count($mCashBookDetail->aDataNegativeCashbook) < 1){
            return ;
        }
        $this->scanNegativeCashbookSendEmail($mCashBookDetail);
        if($this->isOnlyAlert()){
            return ;
        }
        /*** Scan1 save errors + nhắc nhở cho gs, ktkv (lần 1) ***/
        $mEmployeeProblems->handleNegativeCashbook($mCashBookDetail);
        
        $to = time(); $second = $to-$from;
        Logger::WriteLog(' ***************** scanNegativeCashbook done in: '.($second).'  Second  <=> '.($second/60).' Minutes');
    }
    
    /** @Author: DungNT Jan 19, 2019
     *  @Todo: 13h chiểu gửi cảnh báo âm quỹ cho KTKV và giám sát
     **/
    public function isOnlyAlert() {
        $cHours = date('H');
        $isAlert = false;
        if($cHours == 12 ){// Dec2918 khung giờ chỉ alert, không tính lỗi
            $isAlert = true;
        }
        return $isAlert;
    }
    
    /** @Author: DungNT Feb 15, 2019
     *  @Todo: send email alert
     **/
    public function scanNegativeCashbookSendEmail($mCashBookDetail) {
        $mHtmlFormat = new HtmlFormat();
        $mHtmlFormat->alertOnly = true;
        $mHtmlFormat->emailNegativeCashbook($mCashBookDetail);
    }
    
    /** @Author: DungNT Feb 14, 2019
     *  @Todo: remove uid đã phạt lần 1 loại ra không phạt nữa
     **/
    public function scanNegativeCashbookRecheckData(&$mCashBookDetail) {
        $mEmployeeProblems =new EmployeeProblems();
        $aUidRemove = [];
        /*** Scan1 get list uid đã phạt lần 1 loại ra không phạt nữa ***/
        $aWasScan = $mEmployeeProblems->getListNegativeCashbookWasScan();
        // 1. check and remove in $mCashBookDetail->aDataNegativeCashbook
        foreach($mCashBookDetail->aDataNegativeCashbook as $employee_id => $aInfo):
            if(isset($aWasScan[$employee_id][$aInfo['agent_id']])):
                unset($mCashBookDetail->aDataNegativeCashbook[$employee_id]);
                $aUidRemove[$employee_id] = $employee_id;
            endif;
        endforeach;
        // 2. check and remove in $mCashBookDetail->aDataNegativeCashbookSort
        foreach($mCashBookDetail->aDataNegativeCashbookSort as $province_id => $aInfoUid):
            foreach($aInfoUid as $employee_id => $closingBalance):
                if(in_array($employee_id, $aUidRemove)):
                    unset($mCashBookDetail->aDataNegativeCashbookSort[$province_id][$employee_id]);
                endif;
            endforeach;
        endforeach;
    }
    
}
