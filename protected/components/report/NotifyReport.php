<?php

class NotifyReport
{
    const TAB_TYPE_GENERAL  = 1;
    const TAB_TYPE_DETAIL   = 2;
    const TAB_TYPE_SMS      = 3;
    
    /** @Author: ANH DUNG Now 24, 2018
     *  @Todo: get array type same data report
     **/
    public static function getTypeSameData() {
        return [self::TAB_TYPE_GENERAL, self::TAB_TYPE_SMS];
    }
    
    /** @Author: DuongNV Nov 15,18
     *  @Todo: get data report 
     *  @Param: $type = {TAB_TYPE_GENERAL , TAB_TYPE_DETAIL}
     **/
    public function getDataReport($date_from, $date_to, $mGasScheduleSmsHistory) {
        $type = isset($_GET['type']) ? $_GET['type'] : NotifyReport::TAB_TYPE_GENERAL;
        $aRes = [];
        $criteria = new CDbCriteria;
        DateHelper::searchBetween($date_from, $date_to, "created_date_bigint", $criteria, true);
        $this->getDataReportMapCondition($type, $criteria, $mGasScheduleSmsHistory);
        $mSmsHistory = GasScheduleSmsHistory::model()->findAll($criteria);
        $aTypeSameData = NotifyReport::getTypeSameData();
        
        foreach ($mSmsHistory as $item) {
            $money = GasScheduleSmsHistory::getPrice($item->network)*$item->id;
            if(in_array($type, $aTypeSameData) ){
                if(isset($aRes['data'][$item->role_id][$item->created_date])){
                    $aRes['data'][$item->role_id][$item->created_date] += $money;
                } else {
                    $aRes['data'][$item->role_id][$item->created_date] = $money;
                }
                // Sum tab general
                if(isset($aRes['sum'][$item->role_id])){
                    $aRes['sum'][$item->role_id] += $money;
                } else {
                    $aRes['sum'][$item->role_id] = $money;
                }
                // Sum row tab general
                if(isset($aRes['sum_row'][$item->created_date])){
                    $aRes['sum_row'][$item->created_date] += $money;
                } else {
                    $aRes['sum_row'][$item->created_date] = $money;
                }
                // Array month
                $aRes['date_month'] = $this->getArrayMonth($date_from, $date_to);
            } elseif($type == self::TAB_TYPE_DETAIL){
                if(isset($aRes['data'][$item->uid_login][$item->created_date])){
                    $aRes['data'][$item->uid_login][$item->created_date] += $money;
                } else {
                    $aRes['data'][$item->uid_login][$item->created_date] = $money;
                }
                // Sum tab detail - sử dụng chung cho tính lương - trừ tiền SMS của telesale
                if(isset($aRes['sum'][$item->uid_login])){
                    $aRes['sum'][$item->uid_login] += $money;
                } else {
                    $aRes['sum'][$item->uid_login] = $money;
                }
                // Sum by date
                if(isset($aRes['sum_date'][$item->created_date])){
                    $aRes['sum_date'][$item->created_date] += $money;
                } else {
                    $aRes['sum_date'][$item->created_date] = $money;
                }
                
                $aRes['date_month'] = MyFormat::getArrayDay($date_from, $date_to);
            }
        }
        // sort desc
        if(!empty($aRes['data'])){
            uasort($aRes['data'], function($a, $b){
                $sumA = array_sum($a);
                $sumB = array_sum($b);
                if($sumA == $sumB) return 0;
                return ($sumA > $sumB) ? -1 : 1;
            });
        }
        if(in_array($type, $aTypeSameData) ){
            $_SESSION['data-excel-general'] = $aRes;
        } else {
            $_SESSION['data-excel-detail']  = $aRes;
        }
        return $aRes;
    }
    
    /** @Author: ANH DUNG Now 24, 2018
     *  @Todo: map some condtion
     **/
    public function getDataReportMapCondition($type, &$criteria, $mGasScheduleSmsHistory) {
        if($type == self::TAB_TYPE_GENERAL){
            $criteria->select = 'count(t.id) as id, t.role_id, t.network, MONTH(t.created_date) as created_date, YEAR(t.created_date) as created_date_bigint';
            $criteria->group = 't.role_id, MONTH(t.created_date), YEAR(t.created_date), t.network';
        } elseif($type == self::TAB_TYPE_DETAIL){
            if($mGasScheduleSmsHistory->role_id != ROLE_TELESALE){
                $criteria->compare('t.role_id', $mGasScheduleSmsHistory->role_id);
            }else{
                $criteria->compare('t.type', GasScheduleSms::TYPE_TELESALE_CODE);
            }
            $criteria->select = 'count(t.id) as id, t.uid_login, t.network, DATE(t.created_date) as created_date';
            $criteria->group = 't.uid_login, t.network, DATE(t.created_date)';
        } elseif($type == self::TAB_TYPE_SMS){
            $criteria->select = 'count(t.id) as id, t.type as role_id, t.network, MONTH(t.created_date) as created_date, YEAR(t.created_date) as created_date_bigint';
            $criteria->group = 't.type, t.network, MONTH(t.created_date), YEAR(t.created_date)';
        }
    }
    
    /** @Author: DuongNV Nov 5,18
     *  @Todo: get array month between two date
     *  @Param: $from, $to: Y-m-d
     **/
    public function getArrayMonth($from, $to){
        $start    = (new DateTime($from))->modify('first day of this month');
        $end      = (new DateTime($to))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period   = new DatePeriod($start, $interval, $end); // Array month between 2 dates
        $aMonth   = [];
        foreach ($period as $dt){
            $aMonth[] = $dt->format("n"); // Month one degit (1,2,..10,11,12)
        }
        return $aMonth;
    }
    
    /** @Author: ANH DUNG Now 25, 2018 - sử dụng ReportSalary
     *  @Todo: get chi phí của bộ phận telesale trong 1 tháng để đưa vào trừ lương
     *  @param: $date_from format Y-m-d
     *  @param: $date_to Y-m-d
     **/
    public function getSmsTelesale($date_from, $date_to) {
        $_GET['type'] = NotifyReport::TAB_TYPE_DETAIL;
        $mGasScheduleSmsHistory = new GasScheduleSmsHistory();
        $mGasScheduleSmsHistory->role_id = ROLE_TELESALE;
        $aData = $this->getDataReport($date_from, $date_to, $mGasScheduleSmsHistory);
        // $aData['sum'] is array(user_id => total_money) trong 1 tháng
        return $aData['sum'];
    }
    

}