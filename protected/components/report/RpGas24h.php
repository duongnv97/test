<?php
/* Apr 07, 2018 */

class RpGas24h
{
    const TYPE_SEARCH_CUSTOMER = 1;
    const TYPE_SEARCH_EMPLOYEE = 2;
    const TYPE_SEARCH_SORT_1 = 1; // tổng nhập mã
    const TYPE_SEARCH_SORT_2 = 2; // mua hàng
    const TYPE_SEARCH_SORT_3 = 3; // Số tiền còn lại
    const TYPE_SEARCH_SORT_4 = 4; // Tổng tiền đã mua
    public $mUser = null, $user_id = 0, $date_from = 0, $date_to = 0, $source_app = '', $type = '';
    public $date_from_ymd, $date_to_ymd, $aEmployee = [];
    /** @Author: ANH DUNG Apr 07, 2018
     *  @Todo: report số lg user cài đặt, nhập mã KM theo đại lý
     * 1. tính SL cài đặt theo khoảng ngày, group theo agent
     * 2. Tính SL đã nhập mã
     * 3. Tính SL đã đặt hàng
     **/
    public function setupByAgent() {
        $aRes = [];
        $aRes['sumActiveUser']      = 0;
        $aRes['sumInputPromotion']  = 0;
        $aRes['sumOrders']          = 0;
        $this->date_from  = MyFormat::dateDmyToYmdForAllIndexSearch($this->mUser->date_from);
        $this->date_to    = MyFormat::dateDmyToYmdForAllIndexSearch($this->mUser->date_to);
        $this->gas24hGetActiveUserByAgent($aRes);
        $this->gas24hGetPromotion($aRes);
        $this->gas24hGetOrder($aRes);
        return $aRes;
    }
    
    // 1. tính SL cài đặt theo khoảng ngày, group theo agent
    public function gas24hGetActiveUserByAgent(&$aRes) {
        $criteria = new CDbCriteria();
        $criteria->addCondition($this->gas24hConditionActive('t'));
        $criteria->select   = 'count(t.id) as id, t.area_code_id';
        $criteria->group    = 't.area_code_id';
//        $criteria->order    = 't.area_code_id';
        $models = Users::model()->findAll($criteria);
        foreach($models as $item){
            $aRes['aActiveUser'][$item->area_code_id]   = $item->id;
            $aRes['sumActiveUser']                      += $item->id;
        }
        if(isset($aRes['aActiveUser'])){
            arsort($aRes['aActiveUser']);
        }
    }
    
    /** @Author: ANH DUNG Apr 19, 2018
     *  @Todo: get condition active user in range date
     **/
    public function gas24hConditionActive($alias) {
        $c = "$alias.role_id=".ROLE_CUSTOMER." AND $alias.type=".CUSTOMER_TYPE_STORE_CARD." AND $alias.is_maintain=".UsersExtend::STORE_CARD_HGD_APP;
        $c .= " AND DATE($alias.created_date) BETWEEN '$this->date_from' AND '$this->date_to' ";
        if(is_array($this->mUser->province_id)){
            $sParamsIn = implode(',',  $this->mUser->province_id);
            $c .= " AND $alias.province_id IN ($sParamsIn)";
        }
        return $c;
    }
    
    /** @Author: ANH DUNG Apr 19, 2018
     *  @Todo: get những user id kích hoạt trong khoảng ngày đưa vào IN condition
     **/
    public function gas24hLimitUser(&$criteria, $fieldName) {
        $alias = 'SubQ';
        $subQuery = "SELECT $alias.id FROM gas_users as $alias WHERE ".$this->gas24hConditionActive($alias);
        $criteria->addCondition("t.$fieldName IN($subQuery)");
    }

    // 2. sử dụng KM 1 bình + 5. sử dụng mã giới thiệu (100k)
    public function gas24hGetPromotion(&$aRes) {
        $criteria = new CDbCriteria();
//        $criteria->addCondition('t.status_use='.  AppPromotionUser::STATUS_COMPLETE. ' AND t.date_apply IS NOT NULL');
        $this->gas24hLimitUser($criteria, 'user_id');
        $criteria->select   = 'count(t.id) as id, t.agent_id';
        $criteria->group    = 't.agent_id';
        $models = AppPromotionUser::model()->findAll($criteria);
        foreach($models as $item){
            $aRes['InputPromotion'][$item->agent_id] = $item->id;
            $aRes['sumInputPromotion']               += $item->id;
        }
    }
    // 3. Tính SL đã đặt hàng
    public function gas24hGetOrder(&$aRes) {
        $criteria = new CDbCriteria();
        $this->gas24hLimitUser($criteria, 'customer_id');
        $criteria->addCondition('t.status='.Sell::STATUS_PAID);
        $criteria->addCondition('t.materials_type_id=' . GasMaterialsType::MATERIAL_BINH_12KG);
        $criteria->addBetweenCondition('t.created_date_only', $this->date_from, $this->date_to); 
        $criteria->select   = 'count(t.id) as id, t.agent_id';
        $criteria->group    = 't.agent_id';
        $models = SellDetail::model()->findAll($criteria);
        foreach($models as $item){
            $aRes['Orders'][$item->agent_id] = $item->id;
            $aRes['sumOrders']               += $item->id;
        }
    }
    
    
    /** @Author: ANH DUNG Jun 16, 2018
     *  @Todo: get all ref  customer of user
     * @param: $this->aEmployee is array id => name
     **/
    public function getReferralOfUser(){
        $criteria = new CDbCriteria();
//        $criteria->addCondition("DATE_FORMAT(t.created_date,'%Y-%m-%d') >=  '". $this->date_from_ymd."'");
//        $criteria->addCondition("DATE_FORMAT(t.created_date,'%Y-%m-%d') <=  '".$this->date_to_ymd."'");
        $criteria->addBetweenCondition('DATE(t.created_date)', $this->date_from_ymd, $this->date_to_ymd); 
        if(!empty($this->user_id)){
            $criteria->addCondition('t.ref_id=' . $this->user_id);
        }
        if(count($this->aEmployee)){
            $criteria->addCondition("t.ref_id IN (". implode(',', array_keys($this->aEmployee)).")");
        }
        return GasReferralTracking::model()->findAll($criteria);
    }
    
    /** @Author: NamNH 08/2018
    *  @Todo: get array search type
    *  @Param:
    **/
    public function getTypeSearch(){
        return [
            self::TYPE_SEARCH_CUSTOMER => 'Khách hàng',
            self::TYPE_SEARCH_EMPLOYEE => 'Nhân viên',
        ];
        
    }
    
    /** @Author: NamNH 08/2018
    *  @Todo: get array search type
    *  @Param:
    **/
    public function getTopList(){
        return [
            10 => 10,
            50 => 50,
            100 => 100,
            200 => 200,
        ];
        
    }
    
    /** @Author: NamNH month/2018
     *  @Todo: get report top
     *  @Param:
     **/
    public function getReportTop(){
        $aData                      = [];
        $tblUser                    = Users::model()->tableName();
        $tblPromotion               = AppPromotion::model()->tableName();
        $criteria                   = new CDbCriteria;
        $criteria->select           = 'p.code_no as status, t.ref_id, u.first_name as created_date,count(DISTINCT t.invited_id) as invited_id, sum(if(t.date_order IS NULL,0,1)) as id';
        $criteria->limit            = !empty($this->mUser->parent_id) ? $this->mUser->parent_id : 50;
        $criteria->order            = 'invited_id DESC';
        $criteria->join             = ' JOIN '.$tblUser.' as u ON u.id = t.ref_id';
        $criteria->join             .= ' JOIN '.$tblPromotion.' as p ON p.owner_id = t.ref_id';
        $criteria->group            = 't.ref_id, u.first_name, p.code_no';
        if(empty($this->mUser->type)){
            $this->mUser->type = self::TYPE_SEARCH_CUSTOMER;
        }
        switch ($this->mUser->type){
            case self::TYPE_SEARCH_CUSTOMER:
                $criteria->compare('u.role_id', ROLE_CUSTOMER);
                break;
            case self::TYPE_SEARCH_EMPLOYEE:
                $criteria->addCondition('u.role_id !='. ROLE_CUSTOMER);
                break;
        }
        $aReportReferralTracking    = ReportReferralTracking::model()->findAll($criteria);
        foreach ($aReportReferralTracking as $key => $mReportReferralTracking) {
            $aData[$mReportReferralTracking->ref_id] = [
                'first_name'    => $mReportReferralTracking->created_date,
                'code_no'       => $mReportReferralTracking->status,
                'countRef'      => $mReportReferralTracking->invited_id,
                'countConfirm'  => $mReportReferralTracking->id,
            ];
        }
        return $aData;
    }
    
    /** @Author: NamNH month/2018
     *  @Todo: get report top
     *  @Param:
     **/
    public function getReportTop2(){
        $aData                      = [];
        $criteria                   = new CDbCriteria;
        $criteria->select           = 't.ref_id,count(DISTINCT t.invited_id) as invited_id, sum(if(t.date_order IS NULL,0,1)) as id';
        $criteria->limit            = !empty($this->mUser->parent_id) ? $this->mUser->parent_id : 50;
        $criteria->order            = 'invited_id DESC';
        $criteria->group            = 't.ref_id';
        if(empty($this->mUser->type)){
            $this->mUser->type = self::TYPE_SEARCH_CUSTOMER;
        }
        switch ($this->mUser->type){
            case self::TYPE_SEARCH_CUSTOMER:
                $criteria->compare('t.ref_role_id', ROLE_CUSTOMER);
                break;
            case self::TYPE_SEARCH_EMPLOYEE:
                $criteria->addCondition('t.ref_role_id !='. ROLE_CUSTOMER);
                break;
        }
        $aReportReferralTracking    = ReportReferralTracking::model()->findAll($criteria);
        $aIdUser        = CHtml::listData($aReportReferralTracking, 'ref_id', 'ref_id');
        $aFirstName     = $this->getFirstName($aIdUser);
        $aCodeNo        = $this->getCodeNo($aIdUser);
        $aAmount        = $this->getAmount($aIdUser);
        $aSellAmount    = $this->getSellAmount($aIdUser);
        foreach ($aReportReferralTracking as $key => $mReportReferralTracking) {
            $aData[$mReportReferralTracking->ref_id] = [
                'first_name'        => !empty($aFirstName[$mReportReferralTracking->ref_id]) ? $aFirstName[$mReportReferralTracking->ref_id] : '',
                'code_no'           => !empty($aCodeNo[$mReportReferralTracking->ref_id]) ? $aCodeNo[$mReportReferralTracking->ref_id] : '',
                'countRef'          => $mReportReferralTracking->invited_id,
                'countConfirm'      => $mReportReferralTracking->id,
                'amount'            => !empty($aAmount[$mReportReferralTracking->ref_id]) ? $aAmount[$mReportReferralTracking->ref_id] : '',
                'sell_amount'       => !empty($aSellAmount[$mReportReferralTracking->ref_id]) ? $aSellAmount[$mReportReferralTracking->ref_id] : '',
            ];
        }
//        sort amount
        if(!empty($this->mUser->agent_id_search)){
            $aSort = $this->getArryFieldSort();
            $field_sort = !empty($aSort[$this->mUser->agent_id_search]) ? $aSort[$this->mUser->agent_id_search] : '';
            uksort($aData, function($key1,$key2) use($aData,$field_sort){
                return $aData[$key1][$field_sort] < $aData[$key2][$field_sort];
            });
        }else{
            uksort($aData, function($key1,$key2) use($aData){
                return $aData[$key1]['amount'] < $aData[$key2]['amount'];
            });
        }
        
        return $aData;
    }
    
    /** @Author: NamNH 08/2018
     *  @Todo: get data code no
     *  @Param:
     **/
    public function getCodeNo($aIdUsers){
        $aData                          = [];
        if(!empty($aIdUsers)){
            $criteria                   = new CDbCriteria;
            $sParamsIn = implode(',',  $aIdUsers);
            $criteria->addCondition("t.owner_id IN ($sParamsIn)");
            $models = AppPromotion::model()->findAll($criteria);
            $aData = CHtml::listData($models, 'owner_id', 'code_no');
        }
        return $aData;
        
    }
    
    /** @Author: NamNH 08/2018
     *  @Todo: get data code no
     *  @Param:
     **/
    public function getFirstName($aIdUsers){
        $aData                          = [];
        if(!empty($aIdUsers)){
            $criteria                   = new CDbCriteria;
            $sParamsIn = implode(',',  $aIdUsers);
            $criteria->addCondition("t.id IN ($sParamsIn)");
            $models = Users::model()->findAll($criteria);
            $aData = CHtml::listData($models, 'id', 'first_name');
        }
        return $aData;
        
    }
    
    /** @Author: NamNH 08/2018
     *  @Todo: get amount of users
     *  @Param:
     **/
    public function getSellAmount($aIdUsers){
        $aData                          = [];
        if(!empty($aIdUsers)){
            $criteria                   = new CDbCriteria;
            $criteria->select    = 't.customer_id, SUM(t.price) AS price_root';
            $criteria->compare('t.materials_type_id', GasMaterialsType::MATERIAL_BINH_12KG);
            $criteria->addCondition('t.source = ' . Sell::SOURCE_APP.' AND t.status = ' . Sell::STATUS_PAID);
            $criteria->addCondition("t.price = t.promotion_amount AND promotion_amount > 0");
            $sParamsIn = implode(',',  $aIdUsers);
            $criteria->addCondition("t.customer_id IN ($sParamsIn)");
            $criteria->group    = 't.customer_id';
            $models = SellDetail::model()->findAll($criteria);
            $aData = CHtml::listData($models, 'customer_id', 'price_root');
        }
        return $aData;
        
    }
    
    /** @Author: NamNH 08/2018
     *  @Todo: get sell amount of user
     **/
    public function getAmount($aIdUsers){
        $aData                          = [];
        if(!empty($aIdUsers)){
            $criteria                   = new CDbCriteria;
            $criteria->compare('t.promotion_type', AppPromotion::TYPE_REFERENCE_USER);
            $sParamsIn = implode(',',  $aIdUsers);
            $criteria->addCondition("t.user_id IN ($sParamsIn)");
            $models = AppPromotionUser::model()->findAll($criteria);
            $aData = CHtml::listData($models, 'user_id', 'promotion_amount');
        }
        return $aData;
        
    }
    
    /** @Author: NamNH 08/2018
     *  @Todo: get array sort in view
     **/
    public function getSort(){
        return [
            self::TYPE_SEARCH_SORT_1 => 'Tổng nhập mã',
            self::TYPE_SEARCH_SORT_2 => 'Mua hàng',
            self::TYPE_SEARCH_SORT_3 => 'Số tiền còn lại',
            self::TYPE_SEARCH_SORT_4 => 'Tổng tiền đã mua',
        ];
    }
    
    /** @Author: NamNH 08/2018
     *  @Todo: get array sort in view
     **/
    public function getArryFieldSort(){
        return [
            self::TYPE_SEARCH_SORT_1 => 'countRef',
            self::TYPE_SEARCH_SORT_2 => 'countConfirm',
            self::TYPE_SEARCH_SORT_3 => 'amount',
            self::TYPE_SEARCH_SORT_4 => 'sell_amount',
        ];
    }
}
