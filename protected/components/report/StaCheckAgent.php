<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class StaCheckAgent{
    /**
     * @Author: PHAM THANH NGHIA Jan 4, 2019
     * @Todo: lấy dữ liệu chia làm 2 phần 
     * Phân 1 : Kho == store
     * Phần 2 : Quĩ 
     * @param : $agent_id, date_from data_to
     */
    public function getDataCheckAgent($agent_id){
        $aRes = []; 
        $data_current = date('Y-m-d');
        $aRes = $this->existDataCheckAgent($agent_id,$data_current);
        if(empty($aRes)){
            $aStore = $this->getDataStore($agent_id,$data_current); // Phần 1
            $aCashBook = $this->getDataCashBook($agent_id,$data_current); // Phần 2
            $aRes['STORE'] = $aStore;
            $aRes['CASHBOOK'] = $aCashBook;
        }
        return $aRes;
    }
    /**
     * @Author: PHAM THANH NGHIA Jan 4, 2019
     * @Todo: lấy dữ liệu kho dựa vào admin/gasstorecard/view_store_movement_summary
     */
    public function getDataStore($agent_id,$data_current){
        $aRes = [];
        $model = new GasStoreCard();
        $model->agent_id    = $agent_id;
        $model->date_from_ymd  = $data_current;
        $model->date_to_ymd    = $data_current;
        $data = Sta2::calcStoreMovement($model);
        
        $aMaterials = $data['aMaterials'];
        $sum12OpeningStock = 0;
        $sum12Import = 0;
        $sum12Export = 0;
        $sum12Balance = 0;
        $sum12Show = true;

        $OPENING_BALANCE_YEAR_BEFORE = $data['OPENING_BALANCE_YEAR_BEFORE'];
        $OPENING_IMPORT = isset($data['OPENING_IMPORT']) ? $data['OPENING_IMPORT'] : [];
        $OPENING_EXPORT = isset($data['OPENING_EXPORT']) ? $data['OPENING_EXPORT'] : [];
        $IMPORT = isset($data['IMPORT']) ? $data['IMPORT'] : [];
        $EXPORT = isset($data['EXPORT']) ? $data['EXPORT'] : [];
        
        foreach($aMaterials as $key=>$obj):
            $objParent = $obj['parent_obj'];
            $objSubModel = $obj['sub_arr_model']; // is array $key=>$mMaterial sub
            $parentBegin = 0;
            $parentImport = 0;
            $parentExport = 0;
            $parentEnd = 0;
            //
            foreach($objSubModel as $key=>$mMaterial):
                // tồn đầu
                $OpeningYear = isset($OPENING_BALANCE_YEAR_BEFORE[$mMaterial->id])?$OPENING_BALANCE_YEAR_BEFORE[$mMaterial->id]:0;
                $OpeningYearImport = isset($OPENING_IMPORT[$mMaterial->id])?$OPENING_IMPORT[$mMaterial->id]:0;
                $OpeningYearExport = isset($OPENING_EXPORT[$mMaterial->id])?$OPENING_EXPORT[$mMaterial->id]:0;
                $OpeningStock = $OpeningYear+$OpeningYearImport-$OpeningYearExport;
                // Nhập xuất kho
                $totalImport = isset($IMPORT[$mMaterial->id])?$IMPORT[$mMaterial->id]:0;
                $totalExport = isset($EXPORT[$mMaterial->id])?$EXPORT[$mMaterial->id]:0;
                if($OpeningStock==0 && $totalImport==0 && $totalExport==0)
                    continue;
                // Tồn cuối $remain
                $remain = $OpeningStock+$totalImport-$totalExport;

                $parentBegin += $OpeningStock;
                $parentImport += $totalImport;
                $parentExport += $totalExport;
                $parentEnd += $remain;

                if($obj['parent_obj']->materials_type_id==MATERIAL_TYPE_BINH_12){
                    $sum12OpeningStock += $OpeningStock;
                    $sum12Import += $totalImport;
                    $sum12Export += $totalExport;
                    $sum12Balance += $remain;
                }
                $item['materials_type_id'] = $obj['parent_obj']->materials_type_id;
                $item['type'] = $obj['parent_obj']->name;
//                $item['materials_no']   =$mMaterial->materials_no; // mã vật tư
//                $item['name']           =$mMaterial->name; // tên vật tư
                $item['material_id'] = $mMaterial->id;
                $item['open']       =$OpeningStock; // Tồn đầu
                $item['import']     =$totalImport; // Nhập kho
                $item['export']     =$totalExport; // xuất kho
                $item['end']        =$remain; // Tồn cuối
                $item['current']    = '';
                $item['difference'] = '';
                $aRes[] = $item;
            endforeach;
            
        endforeach;
//        // tỉnh tổng gas 12
//        $item['type'] = 'Tổng gas 12';
////        $item['materials_no']   = '';
////        $item['name']           = '';
//        $item['material_id'] = '';
//        $item['open']       =$sum12OpeningStock; // Tồn đầu
//        $item['import']     =$sum12Import; // Nhập kho
//        $item['export']     =$sum12Export; // xuất kho
//        $item['end']        =$sum12Balance; // Tồn cuối
//        $item['current']    = '';
//        $item['difference'] = '';
//        $aRes[] = $item;
        
        return $aRes;
    }
    
    public function getDataCashBook($agent_id,$data_current){
        $aRes = $item = [];
        $model = new GasCashBookDetail();
        $model->date_from_ymd    = $data_current;
        $model->date_to_ymd      = $data_current;
        $model->release_date     = $data_current;
        $model->agent_id         = $agent_id;
        $model->calcMultiAgent($aRes, GasCashBookDetail::P_OPENING_BALANCE);
        $model->calcMultiAgent($aRes, GasCashBookDetail::P_INCURRED);
        
        if(isset($aRes[GasCashBookDetail::P_OPENING_BALANCE])){ 
            $ARR_OPENING  = $aRes[GasCashBookDetail::P_OPENING_BALANCE];
            $INCURRED   = isset($aRes[GasCashBookDetail::P_INCURRED]) ? $aRes[GasCashBookDetail::P_INCURRED] : [];
            $sumEnd = 0;
            
            $aInfo          = isset($ARR_OPENING[$agent_id]) ? $ARR_OPENING[$agent_id] : ['revenue'=>0, 'cost'=>0];
            $aInfo['cost']  = isset($aInfo['cost']) ? $aInfo['cost'] : 0;
            $open           = $aInfo['revenue'] - $aInfo['cost'];
            $amountRevenue  = isset($INCURRED[$agent_id]['revenue']) ? $INCURRED[$agent_id]['revenue'] : 0;
            $amountCost     = isset($INCURRED[$agent_id]['cost']) ? $INCURRED[$agent_id]['cost'] : 0;
            $end            = $open + $amountRevenue - $amountCost;
            $sumEnd         += $end;
            
            $item['open']       =$open; // Tồn đầu
            $item['import']     =$amountRevenue; // Nhập kho
            $item['export']     =$amountCost; // xuất kho
            $item['end']        =$end; // Tồn cuối
            $item['current']    = '';
            $item['difference'] = '';
        }
        return $item;
    }
    /**
     * @Author: PHAM THANH NGHIA 2019
     * @Todo: kiểm tra đã lấy dữ liệu theo đại lý của ngày hôm đó rồi
     */
    public function existDataCheckAgent($agent_id,$data_current){
        $criteria = new CDbCriteria();
        $criteria->compare('t.agent_id',$agent_id);
        $criteria->addCondition("DATE(t.created_date) ='$data_current'");
        $model = CheckAgent::model()->find($criteria);
        if ($model == null) {
            return [];
        } else {
            return $model->getJsonToData();
        }
        
    }
        
        
}
