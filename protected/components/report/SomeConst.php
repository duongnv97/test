<?php
/** Class for function define some const*/
class SomeConst
{
    /** @Author: ANH DUNG Oct 25, 2017
     *  @Todo: get agent của giám sát - Xuat excel cuoc goi callcenter
     */
    public static function getAgentOfMonitor() {
        return [
            // Nguyễn Văn Đợt - 217782
            103 => 217782,    // Đại lý Quận 8.1
            945418 => 217782, // Đại Lý Quận 4.2
            658920 => 217782, // Đại lý Bình Thạnh 2
            768408 => 217782, // Đại Lý Bình Thạnh 3
            106 => 217782, // Đại lý Bình Thạnh 1
            868845 => 217782, // Đại Lý Gò Vấp 2
            113 => 217782, // Đại lý Quận 3
            104 => 217782, // Đại lý Quận 8.2
            102 => 217782,    // Đại lý Quận 7
            
            // Kiều Duy Lâm - 257122
            110 => 257122, //Đại lý Thủ Dầu Một
            357031 => 257122,// Đại lý Tân Phước Khánh 
            115 => 257122, // Đại lý Dĩ An
            1210 => 257122, // Đại Lý Thuận Giao
            375037 => 257122, // Đại lý Thái Hòa
            1457 => 257122, // Đại lý Đồng An
            109 => 257122, // Đại lý Lái Thiêu
            268833 => 257122, //  Đại Lý Phú Hòa
            119 => 257122,// Đại lý Tân Định
            111 => 257122, // - Đại lý Bến Cát 
            118 => 257122, // Đại lý An Thạnh
            
            // Lê Thanh Liêm - 893613
            30753, //- Cửa hàng Cần Thơ 1 - Đại Lý
            171449, // - Cửa hàng Trà Vinh - Đại Lý
            138544, // - Cửa hàng Cần Thơ 2 - Đại Lý
            197436, // Cửa Hàng Phú Thuận
            30751, // Cửa hàng Vĩnh Long 1 - Đại Lý
            652695, // - Cửa Hàng Bình Minh - Đại Lý
            1048571,// Đại Lý Cà Mau Petro
            148404, // - Cửa hàng Vũng Liêm - Đại Lý
            262526, // - Cửa Hàng Ô Môn - Đại Lý
            1079730,// Đại Lý Bạc Liêu
            
            // Vương Thanh Hà - 866992
             
            // Huỳnh Gia Huy - 981743
             
            // Nguyễn Nhật Trường - 435846
            
            
            // Hà Văn Trung - 766819
             
             
            // Trần Tuấn Anh - 863291
             
             
            // Võ Tấn Huy - 863284
             
             
            // Trần Trung Hiếu - 303
            
            
            242369, // Đại lý quận 10
            768409, // Đại Lý Thủ Đức 2
            126,    // Đại lý Tân Phú
            108,    // Đại lý Hóc Môn
            122,// Đại lý Bình Tân
            27740,// Đại lý Tân Bình 2 Jun13
            120,// Đại lý Tân Sơn
            211393,// Đại Lý Quận 6
            27740, // Đại lý Tân Bình 2
            1052166,// Đại Lý An Nhơn
            
            1054994,// Đại Lý Lê Duẩn
            // Đồng Nai **********
            123,// Đại lý Trảng Dài
            114,// Đại lý Long Bình Tân
            116, // Đại lý Bình Đa
            121,// Đại lý Ngã Ba Trị An
            507991,// Đại lý Đông Hòa
            392141, //  Đại lý Long Thành
            // Bình Dương **************
//            1311, // Cửa hàng 1
//            1313, // Cửa hàng 3
            1086796, // Đại Lý Nguyễn Văn Cừ - Gia Lai 
//            268835, // Cửa hàng Chư Sê
            1120456,// Đại Lý Hai Bà Trưng Kon Tum 
//            28941,// Cửa hàng Thi Sách
            1070036,// Đại Lý Tam Bình
            // Aug2217
            
        ];
    }
    
}
