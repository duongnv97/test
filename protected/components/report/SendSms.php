<?php
/** Class for function SMS */
class SendSms
{
    public $sale_id;
    /**
     * @Author: ANH DUNG Mar 30, 2018
     * @Todo: gửi thông báo KM đến KH hộ GĐ
    /* 1. xử lý tách cron những item gửi nhiều ra 1 cron khác, những item gửi ít thì vẫn chạy cron cũ
     * @param: $title: nội dung thông báo
     * @title Run 1: Gas 24h thong bao: Ke tu ngay 1/7/2017 tat ca cac dau so dien thoai chung toi se thuc hien thay doi sang tong dai moi 19001521\nXin cam on quy khach hang
     * 1. insert to sms
     * 2. insert to table tracking sms promotion
     * 3. update daily model UsersSms field: customer_id_app  find all KH app đc tạo trong ngày, find với só phone, nếu có thì update sang
     * 4. update daily model UsersSms field: last_purchase lấy các đơn hàng app trong ngày update lại với customer_id = customer_id_app
     */
    public function promotionHgd() {
        $from = time();
        try {
            ini_set('memory_limit','1500M');
            /** 1. get all record thỏa mãn 1 số điều kiện */
            $title = 'Ca Thang Tu, vo tu nhan qua.Nhap ma Gas24h de 100k bay veo vao vi.Tai app lien tay tai android:http://bit.ly/Gas24h_android va IOS:http://bit.ly/Gas24hh_iOS';

            // 1. get all record thỏa mãn đk
            $mUsers                     = new Users();
            $mUsers->province_id        = GasProvince::TP_HCM;
            $mUsers->date_from          = '01-01-2018';
            $mUsers->date_to            = '28-02-2018';
            
            $criteria = new CDbCriteria();
            $sta2 = new Sta2();
            $sta2->reportSmsHgdGetCondition($criteria, $mUsers);
            $criteria->select = 't.user_id, t.phone, t.network, role_id, t.is_maintain, province_id, district_id';
            $criteria->group = 't.user_id';
            $aModelUserPhone = UsersPhone::model()->findAll($criteria);
//            $aModelUserPhone = $this->getDataDevTest();

            // 2. save tracking để thống kê chiến dịch sms
            $this->saveTrackingSms($aModelUserPhone);
            // 3. do insert to sms
            $mGasScheduleSms = new GasScheduleSms();
            $mGasScheduleSms->doInsertMultiAnnounce($aModelUserPhone, $title);
            // free memmory - How can I clear the memory while running a long PHP script? tried unset() ==> http://stackoverflow.com/questions/10544779/how-can-i-clear-the-memory-while-running-a-long-php-script-tried-unset
            $aModelPhone = null;
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
        $to = time();
        $second = $to-$from;
        $info = "smsPromotion HGD done in: $second  Second  <=> ".($second/60)." Minutes";
        Logger::WriteLog($info);
        SendEmail::bugToDev($info);
    }
    
    /** @Author: ANH DUNG Mar 30, 2018
     *  @Todo: only dev test
     **/
    public function getDataDevTest() {
        $aUserId = [1068981, 1054530];
        $criteria = new CDbCriteria();
        $criteria->addInCondition('t.user_id', $aUserId );
        return UsersPhone::model()->findAll($criteria);
    }
    
    /** @Author: ANH DUNG Mar 30, 2018
     *  @Todo: insert to table tracking sms promotion
     * 1. xử lý khi KH app mới trong 1 ngày tạo, thì vào table UsersSms check map trùng số phone thì update customer_id_app
     * 2. xu ly KH App lay hang thi cung vao table nay update theo so phone???
     **/
    public function saveTrackingSms($aModelUserPhone) {
        $aRowInsert = $aCheckDuplicate = [];
        $smsType = UsersSms::TYPE_1_CODE_GAS24H;
        foreach($aModelUserPhone as $mPhoneCustomer){
            if(in_array($mPhoneCustomer->phone, $aCheckDuplicate)){continue;}
            $aCheckDuplicate[$mPhoneCustomer->phone] = $mPhoneCustomer->phone;
            $aRowInsert[] = "('$smsType',
                '$mPhoneCustomer->user_id',
                '$mPhoneCustomer->phone',
                '$mPhoneCustomer->role_id',
                '$mPhoneCustomer->is_maintain',
                '$mPhoneCustomer->province_id',
                '$mPhoneCustomer->district_id'
            )";
        }
        $tableName = UsersSms::model()->tableName();
        $sql = "insert into $tableName (sms_type,
                        user_id,
                        phone,
                        role_id,
                        is_maintain,
                        province_id,
                        district_id
                        ) values ".implode(',', $aRowInsert);
        if(count($aRowInsert)>0){
            Yii::app()->db->createCommand($sql)->execute();
        }
    }
    
    
    /** @Author: ANH DUNG Mar 30, 2018
     *  @Todo: tracking lại dữ liệu khi chạy SMS marketing với sms_type = 1, số KH đã cài app, số KH đã đặt hàng
     * @note: view function promotionHgd
        3. update daily model UsersSms field: customer_id_app  find all KH app đc tạo trong ngày, find với só phone, nếu có thì update sang
     *  4. update daily model UsersSms field: last_purchase lấy các đơn hàng app trong ngày update lại với customer_id = customer_id_app
     **/
    public function cronUpdateTrackingSms() {
        $from = time();
        $date = date('Y-m-d');
        $this->trackingInstallApp($date);
        $this->trackingMakeOrder($date);
        $to = time();
        $second = $to-$from;
        $info = '!Important cronUpdateTrackingSms done in: '.($second).'  Second  <=> '.($second/60).' Minutes';
        Logger::WriteLog($info);
    }
    
    /** @Author: ANH DUNG Mar 30, 2018
     *  @Todo: 3. update daily model UsersSms field: customer_id_app  find all KH app đc tạo trong ngày, find với só phone, nếu có thì update sang
     **/
    public function trackingInstallApp($date) {
        $criteria = new CDbCriteria();
        $criteria->addCondition("DATE(t.created_date)='$date'");
        $criteria->compare('t.role_id', ROLE_CUSTOMER);
        $criteria->compare('t.type', CUSTOMER_TYPE_STORE_CARD);
        $criteria->compare('t.is_maintain', UsersExtend::STORE_CARD_HGD_APP);
        $models = Users::model()->findAll($criteria);
        $total = 0;
        foreach($models as $item){
            $criteria = new CDbCriteria();
            $criteria->compare('t.phone', UsersPhone::removeLeftZero($item->username));
            $criteria->compare('t.sms_type', UsersSms::TYPE_1_CODE_GAS24H);
            $criteria->addCondition('t.customer_id_app = 0');
            $model = UsersSms::model()->find($criteria);
            if($model){
                $model->customer_id_app = $item->id;
                $model->update(['customer_id_app']);
                $total++;
            }
            $model = null;
        }
        $models = null;
    }
    
    /** @Author: ANH DUNG Mar 30, 2018
     *  @Todo: 4. update daily model UsersSms field: last_purchase lấy các đơn hàng app trong ngày update lại với customer_id = customer_id_app
     * lúc này để xác định KH app đã đặt hàng thành công chưa, update vào tracking để theo dõi
     **/
    public function trackingMakeOrder($date) {
        $criteria = new CDbCriteria();
        $criteria->compare('t.created_date_only', $date);
        $criteria->compare('t.source', Sell::SOURCE_APP);
        $criteria->compare('t.status', Sell::STATUS_PAID);
        $models = Sell::model()->findAll($criteria);
        if(count($models) == 0){
            return ;
        }
        $aCustomerId = CHtml::listData($models, 'customer_id', 'customer_id');
        
        $criteria = new CDbCriteria();
        $sParamsIn = implode(',',  $aCustomerId);
        $criteria->addCondition("customer_id_app IN ($sParamsIn)");
        $aUpdate = array('last_purchase' => $date);
        UsersSms::model()->updateAll($aUpdate, $criteria);
    }
    
    /** @Author: DungNT Apr 19, 2019
     *  @Todo: Lấy số báo giá cho KH - gửi thông báo bất kỳ đến KH bò mối, 
     *  Hiện tại gửi thông báo nghỉ việc vào số báo giá của KH đang lấy hàng
     * 1/ Apr1919 Sale Phuong nghỉ việc 17/04/2019
     * CTy CP Dau Khi Mien Nam tran trong thong bao den Quy khach hang:  Quyet dinh cho thoi viec nhan vien Nguyen Duy Phuong ke tu ngay 17/04/2019. Moi chi tiet lien he tong dai Gas24h: 19001521 hoac lien he truc tiep: Mr. Duc (0965 303 963); Mr. Vinh (0919 893 089) ;Ms. Ha (0933 037 660)
     * 2/ Jul0419 Sale Chu Văn Tuấn đã nghỉ việc 30/06/2019
     * 
     **/
    public function announceAnything() {
        // $this->sale_id            = 1619638; // Nguyễn Duy Phương KD
        $this->sale_id            = 1911247; // Chu Văn Tuấn
        
        $from = time();
        $title = "CTy CP Dau Khi Mien Nam tran trong thong bao den Quy khach hang:"
                . "\nNhan vien Chu Van Tuan nghi viec tu ngay 30/06/2019."
                . "\nMoi chi tiet lien he tong dai Gas24h: 19001521 hoac lien he truc tiep: Mr. Duc (0909 875 420); Ms. Ha (0933 037 660)";
        $aModelPhone                = $this->getRecordBoMoi();
        echo '<pre>';
        print_r(count($aModelPhone));
        echo '</pre>';
        die;
        $mGasScheduleSms            = new GasScheduleSms();
        $mGasScheduleSms->time_send = date('Y-m-d H:i:s');
        $mGasScheduleSms->doInsertMultiAnnounce($aModelPhone, $title);
        // free memmory - How can I clear the memory while running a long PHP script? tried unset() ==> http://stackoverflow.com/questions/10544779/how-can-i-clear-the-memory-while-running-a-long-php-script-tried-unset        
        $to = time();
        $second = $to-$from;
        $info = "announceAnything done in: $second  Second  <=> ".($second/60)." Minutes";
        Logger::WriteLog($info);
        echo $info;die;
    }
    
    /**
     * @Author: ANH DUNG Feb 26, 2017
     * @Todo: get all record phone bò mối còn lấy hàng. Để gửi SMS báo giá hay KM gì đó
     */
    public function getRecordBoMoi() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.type=' . UsersPhoneExt2::TYPE_PRICE_BO_MOI);
        $criteria->addCondition('t.user_id IN ('.$this->getSqlCustomerConLayHang().')');
        return UsersPhoneExt2::model()->findAll($criteria);
    }
    // https://stackoverflow.com/questions/12666502/mysql-in-clause-max-number-of-arguments
    public function getSqlCustomerConLayHang() {
        $roleCustomer       = ROLE_CUSTOMER;
        $statusLayHang      = Users::CON_LAY_HANG;
        $sParamsIn          = implode(',', CmsFormatter::$aTypeIdBoMoi);
//        $addLimitBoMoi = "SubQ.province_id IN ($sParamsInProvince) AND SubQ.is_maintain IN ($sParamsIn) AND SubQ.channel_id=$statusLayHang";
        $addLimitBoMoi = "SubQ.sale_id = {$this->sale_id} AND SubQ.is_maintain IN ($sParamsIn) AND SubQ.channel_id=$statusLayHang";
        return "SELECT SubQ.id FROM `gas_users` as SubQ WHERE SubQ.role_id=$roleCustomer AND $addLimitBoMoi";
    }
    
}
