<?php
/** Class for function define some const*/
class SpjError
{
    const SPJE001 = 'SPJE001';// không ghi log với mã lỗi này
    const SELL001 = 101;// không ghi log với mã lỗi này
    
    /** @Author: DungNT Jul 18, 2019
     *  @Todo: list errors not write log
     **/
    public static $aNotWriteLog = [
        SpjError::SELL001,
    ];
}
