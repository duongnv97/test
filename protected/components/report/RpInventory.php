<?php
/* Dec 25, 2018 -- move các function tính toán tồn kho sản lượng Sale vào model này */

class RpInventory
{
    public $notGetWarehouse = 0;
    /**
     * @Author: ANH DUNG Nov 06, 2016 => Dec2518 
     * @Todo: tỉnh tổng nhập hay xuất theo từng đại lý, từng vật tư
     * ở đây sẽ tính cho all agent từ ngày cắt data By Year đến ngày $date_delivery
     */
    public function calcAgentExportImport($date_delivery){
        $aRes = [];
        $criteria = new CDbCriteria();
        // nếu không chọn đại lý có thể sẽ lấy toàn hệ thống, khi đó sẽ ko tính KHO nhập xuất
        if($this->notGetWarehouse){
            $sParamsIn = implode(',', GasCheck::getAgentNotGentAuto());
            $criteria->addCondition("t.user_id_create NOT IN ($sParamsIn)");
        }
        
//        if(is_array($aMaterialTypeId)){
//            $sParamsIn = implode(',', $aMaterialTypeId);
//            $criteria->addCondition("t.materials_type_id IN ($sParamsIn)");
//        }
//        Aug1317 Mô tả bug: nếu loại VT bị chuyển giữa nhiều loại qua lại thì khi mình limit theo loại VT sẽ ra bc tồn kho sai 
//        VD: VAN0003 Van điều áp gia đình Namilux Xám +> bị đưa sang loại VT 17, mà hiện tại loại VT của nó là 3 nên bị sai BC

         // ở đây là thống kê tồn thực tế tại thời điểm nên sẽ lấy <= khác với Báo Cáo Nhập Xuất Tồn chỉ lấy < 
//        $criteria->addCondition("t.date_delivery <= '$date_delivery'");
        $date_from = MyFunctionCustom::GetYearForReport()."-01-01";
//        $criteria->addBetweenCondition('t.date_delivery', $date_from, $date_delivery);
        DateHelper::searchBetween($date_from, $date_delivery, 'date_delivery_bigint', $criteria);
        // chỗ này sửa: vì câu trên t.date_delivery <  sẽ lấy all data, không đúng với kế hoạch cut data 
//        $criteria->addCondition("year(t.date_delivery) >= ". MyFunctionCustom::GetYearForReport() ); // Fix change Jan 02, 2014
        $criteria->select   = 't.type_store_card, t.user_id_create, t.materials_id, t.materials_type_id, sum(qty) as qty';
        $criteria->group    = 't.type_store_card, t.user_id_create, t.materials_id';
        $models = GasStoreCardDetail::model()->findAll($criteria);
        foreach($models as $item){
            $aRes[$item->user_id_create][$item->materials_type_id][$item->materials_id][$item->type_store_card] = $item->qty;
        }
        // sử dụng for theo agent id và material id
        $models = null;
        return $aRes;
     }
     
     
    
}
