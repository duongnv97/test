<?php
/* Dec 14, 2017 - handle with solr*/

class ExtSolr
{   
    public $nameComponent       ='solrUser';
    public $nameClass           ='CSolrComponent';
//    public $host                = '128.199.148.191';
    public $host                = '128.199.205.12';
    public $port                = 8983;
    public $indexPath           = '/solr/gas_users';
    
    public $sQuery = '', $sConditionSame = '';
    
    /** @Author: ANH DUNG Dec 14, 2017
     *  @Todo: get field to add or update, sync
     * id,first_name,province_id,district_id,ward_id,sale_id,code_bussiness,code_account,role_id,status,phone,area_code_id,parent_id,is_maintain,type,created_by,address,address_vi
     * ,,,,,,,,,,,,,,created_by,address,address_vi
     **/
    public function getFields(){
        return [
            'id',
            'first_name',
            'province_id',
            'district_id',
            'ward_id',
            'sale_id',
            'code_bussiness',
            'code_account',
            'role_id',
            'status',
            'phone',
            'area_code_id',
            'parent_id',
            'is_maintain',
            'type',
            'created_by',
            'address',
            'address_vi',
        ];
    }
    
    /** @Author: ANH DUNG Dec 14, 2017
     *  @Todo: format number trc khi insert to solr
     **/
    public function getFieldsNumberOnly(){
        return [
            'province_id',
            'district_id',
            'ward_id',
            'sale_id',
            'status',
            'area_code_id',
            'parent_id',
            'is_maintain',
            'type',
            'created_by',
        ];
    }

    public function getNameComponent(){
         return $this->nameComponent;
    }
    public function getNameClass(){
         return $this->nameClass;
    }
    public function getHost(){
         return $this->host;
    }
    public function getPort(){
         return $this->port;
    }
    public function getIndexPath(){
         return $this->indexPath;
    }
    public function setNameComponent($value){
         $this->nameComponent = $value;
    }
    public function setNameClass($value){
        $this->nameClass = $value;
    }
    public function setHost($value){
        $this->host = $value;
    }
    public function setPort($value){
       $this->port = $value;
    }
    public function setIndexPath($value){
       $this->indexPath = $value;
    }
    
    public function searchByKeyword(){
        $result = Yii::app()->{$this->nameComponent}->get($this->sQuery, 0, 30);
        return $result;
    }
    /** To add or update many documents
     *  Yii::app()->userSearch->updateMany(array('1'=>array('id'=>1,
                                        'name'=>'tom',
                                        'age'=> 25),
                             '2'=>array('id'=>2,
                                        'name'=>'pitt')
                     ));
     * @param: array $aData 
     */
    public function addMany($aData){
        Yii::app()->{$this->nameComponent}->updateMany($aData);
    }
    
    /** @Author: ANH DUNG Dec 14, 2017
     *  @Todo: remove by array key
     **/
    public function deleteByKey($aId){
        Yii::app()->{$this->nameComponent}->rmByIds($aId);
    }
    
    /** @Author: ANH DUNG Dec 16, 2017
     *  @Todo: build string query by key value
     **/
    public function addParams($key, $value){
        $this->sQuery .= " && $key:$value";
    }
    
    /** @Author: ANH DUNG Dec 16, 2017
     *  @Todo: build string query by key value
     * @param: $value format 'value OR value 2 OR value3'
     **/
    public function addInCondition($key, $value){
        $this->sQuery .= " && $key:($value)";
    }
    /** @Author: ANH DUNG Dec 16, 2017
     *  @Todo: build string query by key value
     * @param: $value format 'value OR value 2 OR value3'
     **/
    public function addNotInCondition($key, $aValue){
        foreach($aValue as $value){
            $this->sQuery .= " && -$key:$value";
        }
    }
    
}
