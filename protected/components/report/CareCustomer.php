<?php
/** Class for function relate customer bo moi, HGD  */
class CareCustomer
{
    /** @Author: ANH DUNG Now 05, 2018
     *  @Todo: Bùi Đức Hoàn Em cho quét các khách hàng KH40 : 45 ngày giúp anh.
     **/
    public function alertCustomerStopOrder() {
        try {
            $limitDate      = 45; $criteria   =new CDbCriteria;
            $mUsersExtend   = new UsersExtend();
            $criteria->addCondition('first_char=' . Users::GROUP_KH40);
            $aCustomer      = $mUsersExtend->getCustomerStopOrder($limitDate, $criteria);
            $mHtmlFormat        = new HtmlFormat();
            $mHtmlFormat->html = '<p><b>Danh sách khách hàng KH40 không lấy gas 45 ngày</b></p><br><br>';
            $mHtmlFormat->getContentBoMoiNotSetPrice($aCustomer);
            $bodyEmail      = $mHtmlFormat->html;
            $needMore['title']      = '[Cảnh báo 45 ngày] Danh sách KH40 không lấy gas 45 ngày - '.date('d/m/Y');
//            $needMore['list_mail']  = ['dungnt@spj.vn']; $needMore['SendNow'] = 1; // Jan1818 function này chỉ để Dev Test
            $needMore['list_mail'] = ['dungnt@spj.vn', 'kinhdoanh@spj.vn'];
            SendEmail::bugToDev($bodyEmail, $needMore);
            
            Logger::WriteLog(count($aCustomer). " - KH alertCustomerStopOrder KH40 stop order 45 ngay");
            $this->alertCustomerStopOrder7Days();
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /** @Author: ANH DUNG Dec 30, 2018
     *  @Todo: alert KH không lấy hàng 7 ngày cho KH khu vực Anh Hiếu 
     **/
    public function alertCustomerStopOrder7Days() {
        $limitDate      = 7; $criteria   =new CDbCriteria;
        $mUsersExtend   = new UsersExtend();
        $aSaleId = [GasLeave::UID_CHIEF_MONITOR, 865752, 471328];
        $sParamsIn = implode(',', $aSaleId);
        $criteria->addCondition("t.sale_id IN ($sParamsIn)");
        $aCustomer      = $mUsersExtend->getCustomerStopOrder($limitDate, $criteria);
        $mHtmlFormat        = new HtmlFormat();
        $mHtmlFormat->html = "<p><b>Danh sách khách hàng  không lấy gas $limitDate ngày</b></p><br><br>";
        $mHtmlFormat->getContentBoMoiNotSetPrice($aCustomer);
        $bodyEmail      = $mHtmlFormat->html;
        $needMore['title']      = "[Cảnh báo $limitDate ngày] Danh sách không lấy gas $limitDate ngày - ".date('d/m/Y');
//            $needMore['list_mail']  = ['dungnt@spj.vn']; $needMore['SendNow'] = 1; // Jan1818 function này chỉ để Dev Test
        $needMore['list_mail'] = ['dungnt@spj.vn'];
        SendEmail::bugToDev($bodyEmail, $needMore);

        Logger::WriteLog(count($aCustomer). " - KH alertCustomerStopOrder stop order $limitDate ngay ********* NEW *************");
    }
    
    /** @Author: LOCNV May 21, 2019
     *  @Todo: khoi tao cac gia tri cho inventoryCustomer
     *  @Param: model GasStoreCard
     *  @param model $mInventoryCustomer
     * LOC001
     **/
    public function initDebit($model, &$mInventoryCustomer) {
        $mInventoryCustomer->date_from    = $model->date_from;
        $mInventoryCustomer->date_to      = $model->date_to;
//        if(count($mInventoryCustomer->aCustomerIdLimitVo)){// DungNT Fix Jan1619
//            $aCustomerId = array_merge($aCustomerId, $mInventoryCustomer->aCustomerIdLimitVo);
//        }
        $mInventoryCustomer->province_id        = '';
        $mInventoryCustomer->price_code         = '';
        $mInventoryCustomer->aCustomerIdLimitVo = [];
    }
    
    /** @Author: LOCNV May 21, 2019
     *  @Todo: Tinh cong no vo
     *  @Param: $model model GasStoreCard
     * LOC001
     **/
    public function calcDebitVo($model) {
        $mInventoryCustomer = new InventoryCustomer();
        $this->initDebit($model, $mInventoryCustomer);
        $aData = $mInventoryCustomer->reportDebitVo();
        //tinh cong no vo
        $aCustomer          = isset($aData['CUSTOMER_MODEL']) ? $aData['CUSTOMER_MODEL'] : [];
        $aMaterials         = isset($aData['MATERIALS_MODEL']) ? $aData['MATERIALS_MODEL'] : [];
        $BEGIN_VO           = isset($aData['BEGIN_VO']) ? $aData['BEGIN_VO'] : [];
        $OUTPUT_BEFORE      = isset($aData['OUTPUT_BEFORE']) ? $aData['OUTPUT_BEFORE'] : [];
        $OUTPUT_IN_PERIOD   = isset($aData['OUTPUT_IN_PERIOD']) ? $aData['OUTPUT_IN_PERIOD'] : [];
        $aIdVo              = isset($aData['ID_VO']) ? $aData['ID_VO'] : [];
        $aCongNoVo          = [];


        foreach ($aCustomer as $mCustomer){
        $customer_id = $mCustomer->id;
        
        $agentName   = isset($listdataAgent[$mCustomer->area_code_id]) ? $listdataAgent[$mCustomer->area_code_id] : '';

        $sum_begin = $sum_import = $sum_export = $sum_end = 0;
        $sum_begin_nho = $sum_import_nho = $sum_export_nho = $sum_end_nho = 0;
        $sum_begin_lon = $sum_import_lon = $sum_export_lon = $sum_end_lon = 0;
            
            //for tinh cong no vo nho
        foreach ($aIdVo[$customer_id]['MATERIALS_VO_NHO'] as $materials_id => $qty){
            $materials_no = $name  = $materials_id;
            if(isset($aMaterials[$materials_id])){
                $materials_no = $aMaterials[$materials_id]['materials_no'];
                $name = $aMaterials[$materials_id]['name'];
            }
            $qty_begin          = isset($BEGIN_VO[$customer_id][$materials_id]) ? $BEGIN_VO[$customer_id][$materials_id] : 0;
            $qty_import_before  = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
            $qty_export_before  = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;
                
            $qty_import_in_period = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
            $qty_export_in_period = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;
                
            $begin      = $qty_begin + $qty_export_before - $qty_import_before;
            $end        = $begin + $qty_export_in_period - $qty_import_in_period;
            $sum_begin      += $begin;
            $sum_import     += $qty_import_in_period;
            $sum_export     += $qty_export_in_period;
            $sum_end        += $end;
//                tính toán vỏ nhỏ
            $sum_begin_nho  += $begin;
            $sum_import_nho += $qty_import_in_period;
            $sum_export_nho += $qty_export_in_period;
            $sum_end_nho    += $end;
            
            if($begin == 0 && $qty_import_in_period == 0 && $qty_export_in_period == 0 && $end == 0){
                continue;
            }
                
        }
            //for tinh cong no vo lon
        foreach ($aIdVo[$customer_id]['MATERIALS_VO_LON'] as $materials_id => $qty){
            $materials_no = $name  = $materials_id;
            if(isset($aMaterials[$materials_id])){
                $materials_no = $aMaterials[$materials_id]['materials_no'];
                $name = $aMaterials[$materials_id]['name'];
            }
            $qty_begin          = isset($BEGIN_VO[$customer_id][$materials_id]) ? $BEGIN_VO[$customer_id][$materials_id] : 0;
            $qty_import_before  = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
            $qty_export_before  = isset($OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_BEFORE[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;
                
            $qty_import_in_period = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_IMPORT][$customer_id][$materials_id] : 0;
            $qty_export_in_period = isset($OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id]) ? $OUTPUT_IN_PERIOD[TYPE_STORE_CARD_EXPORT][$customer_id][$materials_id] : 0;
                
            $begin      = $qty_begin + $qty_export_before - $qty_import_before;
            $end        = $begin + $qty_export_in_period - $qty_import_in_period;
            $sum_begin      += $begin;
            $sum_import     += $qty_import_in_period;
            $sum_export     += $qty_export_in_period;
            $sum_end        += $end;
//                Tính toán vỏ lớn
            $sum_begin_lon  += $begin;
            $sum_import_lon += $qty_import_in_period;
            $sum_export_lon += $qty_export_in_period;
            $sum_end_lon    += $end;
            
            if($begin == 0 && $qty_import_in_period == 0 && $qty_export_in_period == 0 && $end == 0){
                continue;
            }
                
        }
        $aCongNoVo[$customer_id]['vo_nho'] = $sum_end_nho;
        $aCongNoVo[$customer_id]['vo_lon'] = $sum_end_lon;
        $aCongNoVo[$customer_id]['tong'] = $sum_end_lon + $sum_end_nho;
    }
        return $aCongNoVo;   
    }
    
    /** @Author: LOCNV May 21, 2019
     *  @Todo: tinh cong no tien (no cuoi ky)
     *  @Param: object $model model GasStoreCard
     * LOC001
     **/
    public function calcDebitCash($model) {
        $mInventoryCustomer = new InventoryCustomer();
        $mAppCache          = new AppCache();
        $this->initDebit($model, $mInventoryCustomer);
        $aData = $mInventoryCustomer->reportDebitCash();
        $aCongNoTien = [];
        
        $aCustomer              = isset($aData['CUSTOMER_MODEL']) ? $aData['CUSTOMER_MODEL'] : [];
        $BEGIN_CASH             = isset($aData['BEGIN_CASH']) ? $aData['BEGIN_CASH'] : [];
        $DATA_OPENING_DEBIT     = isset($aData['DATA_OPENING_DEBIT']) ? $aData['DATA_OPENING_DEBIT'] : [];
        $DATA_IN_PERIOD_DEBIT   = isset($aData['DATA_IN_PERIOD_DEBIT']) ? $aData['DATA_IN_PERIOD_DEBIT'] : [];
        $DATA_OPENING_CREDIT    = isset($aData['DATA_OPENING_CREDIT']) ? $aData['DATA_OPENING_CREDIT'] : [];
        $DATA_IN_PERIOD_CREDIT  = isset($aData['DATA_IN_PERIOD_CREDIT']) ? $aData['DATA_IN_PERIOD_CREDIT'] : [];
        $listdataAgent          = $mAppCache->getAgentListdata(); $i = 1;
        $listdataSale           = $mAppCache->getListdataUserByRole(ROLE_SALE);
        $listdataSale1          = $mAppCache->getListdataUserByRole(ROLE_MONITORING_MARKET_DEVELOPMENT);
        $listdataSale2          = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MAINTAIN);
        $listdataSale           = $listdataSale + $listdataSale1 + $listdataSale2;
        $index = 1;
        $sumOpeningDebit = $sumOpeningCredit = $sumIncurredDebit = $sumIncurredCredit = $sumClosingDebit = $sumClosingCredit = 0;
        
        foreach ($aCustomer as $customer_id => $mCustomer) {
            if(!$mCustomer->allowAccessAgent($mCustomer->area_code_id)){
                continue;// Add check Dec2518
            }
            $agentName  = isset($listdataAgent[$mCustomer->area_code_id]) ? $listdataAgent[$mCustomer->area_code_id] : '';
            $saleName   = isset($listdataSale[$mCustomer->sale_id]) ? $listdataSale[$mCustomer->sale_id] : $mCustomer->sale_id;
            $customer_id = $mCustomer->id;
            $closingDebit1 = $closingCredit1 = 0;
            $closingDebit2 = $closingCredit2 = 0;
            
            $openingDebit1          = isset($BEGIN_CASH[$customer_id]['debit']) ? $BEGIN_CASH[$customer_id]['debit'] : 0;
            $openingCredit1         = isset($BEGIN_CASH[$customer_id]['credit']) ? $BEGIN_CASH[$customer_id]['credit'] : 0;
            
            // vì tiền của KH có thu tiền mặt và chi gas dư, nên ở đây phải trừ đi gas dư
            $incurredDebit1     = isset($DATA_OPENING_DEBIT[$customer_id]) ? $DATA_OPENING_DEBIT[$customer_id] : 0;// phải thu KH
//            $incurredCredit1    = $model->calcDebitCustomer($DATA_OPENING_CREDIT, $customer_id);// tính thu trong kỳ không được trừ gas dư
            $incurredCredit1    = isset($DATA_OPENING_CREDIT[$customer_id][GasMasterLookup::MASTER_TYPE_THU]) ? $DATA_OPENING_CREDIT[$customer_id][GasMasterLookup::MASTER_TYPE_THU] : 0;// Jul0317 đã thu - không trừ gas dư
            // 1. nợ và có đầu kỳ
            $mInventoryCustomer->calcClosingDebitCredit($openingDebit1, $openingCredit1, $incurredDebit1, $incurredCredit1, $closingDebit1, $closingCredit1);
            // 2. phát sinh nợ và có trong kỳ
            $incurredDebit2     = isset($DATA_IN_PERIOD_DEBIT[$customer_id]) ? $DATA_IN_PERIOD_DEBIT[$customer_id] : 0;
//            $incurredCredit2    = $model->calcDebitCustomer($DATA_IN_PERIOD_CREDIT, $customer_id);
            $incurredCredit2    = isset($DATA_IN_PERIOD_CREDIT[$customer_id][GasMasterLookup::MASTER_TYPE_THU]) ? $DATA_IN_PERIOD_CREDIT[$customer_id][GasMasterLookup::MASTER_TYPE_THU] : 0;
            /* 3. nợ và có cuối kỳ
             * công thức: nợ + nợ - có - có = A
             * Nếu A > 0 thì A nằm bên nợ cuối kỳ
             * Nếu A < 0 thì A nằm bên có cuối kỳ
             */
            $mInventoryCustomer->calcClosingDebitCredit($closingDebit1, $closingCredit1, $incurredDebit2, $incurredCredit2, $closingDebit2, $closingCredit2);
            // Jul1218 nếu nhỏ hơn 1k thì round = 0 - Fix Thúc 
            $closingDebit1          = $closingDebit1 > InventoryCustomer::AMOUNT_ROUND_DOWN ? $closingDebit1 : 0;
            $closingCredit1         = $closingCredit1 > InventoryCustomer::AMOUNT_ROUND_DOWN ? $closingCredit1 : 0;
            $closingDebit2          = $closingDebit2 > InventoryCustomer::AMOUNT_ROUND_DOWN ? $closingDebit2 : 0;
            $closingCredit2         = $closingCredit2 > InventoryCustomer::AMOUNT_ROUND_DOWN ? $closingCredit2 : 0;
            // Jul1218 nếu nhỏ hơn 1k thì round = 0 - Fix Thúc 
            
            if( $mInventoryCustomer->view_closing_debit && 
                ( $closingCredit2 > 0 || ((empty($closingCredit2) && empty($closingDebit2))))
            ){
                continue;
            }
            if($mInventoryCustomer->view_closing_credit && 
                ($closingDebit2 > 0 || ((empty($closingCredit2) && empty($closingDebit2)))) ){
                continue;
            }
            
            $aCongNoTien[$customer_id] = $closingDebit2;
        }
        return $aCongNoTien;
    }
    
    /** @Author: LOCNV May 21, 2019
     *  @Todo: ham get criteria de lay danh sach san luong
     *  @Param: $date_from, $date_to, $model, $criteria, $is_compare, $aCustomer
     * LOC001
     **/
    public function reportCustomerProductionGetCriteria($date_from, $date_to, $model, &$criteria, $is_compare = false, $aCustomer = []) {
        $select = 'sum(t.qty) as qty, t.customer_id, t.materials_type_id, t.sale_id';
        $group_by = 't.customer_id, t.materials_type_id';
        if ($is_compare == false) {
            $criteria->addBetweenCondition('t.date_delivery', $date_from, $date_to);
            if ($model->customer_id != '') {
                $criteria->addCondition("t.customer_id = '$model->customer_id'");
            }
        }
        else {
            $date_from_3_month = MyFormat::modifyDays($date_from, 3, '-', 'month');
            $date_to_3_month = MyFormat::modifyDays($date_from, 1 , '-');
            $criteria->addBetweenCondition('t.date_delivery', $date_from_3_month, $date_to_3_month);
            $criteria->addInCondition('t.customer_id', $aCustomer);
            $select = 'sum(t.qty) as qty, t.customer_id, t.materials_type_id, t.sale_id, t.date_delivery';
            $group_by = 't.customer_id, t.materials_type_id, YEAR(t.date_delivery), MONTH(t.date_delivery)';
        }
        $criteria->compare('t.sale_id', $model->ext_sale_id);
//        DateHelper::searchBetween($date_from, $date_to, 'date_delivery_bigint', $criteria, false);
        $criteria->addInCondition("t.materials_type_id",CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT);
//        $criteria->select = 'sum(if(t.materials_type_id = '.MATERIAL_TYPE_BINHBO_50.',t.qty * 50,if(t.materials_type_id = '.MATERIAL_TYPE_BINHBO_45.',t.qty * 45,if(t.materials_type_id = '.MATERIAL_TYPE_BINH_12.',t.qty * 12,if(t.materials_type_id = '.MATERIAL_TYPE_BINH_6.',t.qty * 6,if(t.materials_type_id = '.GasMaterialsType::MATERIAL_BINH_4KG.',t.qty * 4,0)))))) as qtyKg, sum(t.qty) as qty, '
//                . 't.customer_id, t.materials_type_id, t.sale_id';
        $aWith = [];
        if(!empty($model->channel_id)){
            $criteria->compare('customer.channel_id', $model->channel_id);
            $aWith[] = 'customer';
        }
        if(count($aWith)){
            $criteria->with     = $aWith;
            $criteria->together = true;
        }
        
        $criteria->select = $select;
        $criteria->addCondition("t.type_customer = ".STORE_CARD_KH_BINH_BO." OR t.type_customer = ".STORE_CARD_KH_MOI);
        $criteria->group = $group_by;
        
    }
    
    /** @Author: LOCNV May 14, 2019
     *  @Todo:
     *  @Param: $model model GasStoreCard
     * LOC001
     **/
    public function reportCustomerProduction($model) {
        $aCustomer = $aSale = $aGasStoreCardDetail = $aCongNoVo = $res = $aSanLuongGanNhat = [];
        //chuyen thanh dinh dang Ymd de search trong csdl
        $date_from = MyFormat::dateConverDmyToYmd($model->date_from, '-');
        $date_to = MyFormat::dateConverDmyToYmd($model->date_to, '-');
        
        //truy van trong gas store card detail
        $criteria = new CDbCriteria();
        $this->reportCustomerProductionGetCriteria($date_from, $date_to, $model, $criteria);
        $aModels = GasStoreCardDetail::model()->findAll($criteria);

        //kiem tra va tra ve rong
        if (empty($aModels))
            return [];
        
        foreach ($aModels as $key => $value) {
            $aCustomer[$value->customer_id]     = $value->customer_id;
            $aSale[$value->sale_id]             = $value->sale_id;
            $qtyKg = $value->qty*CmsFormatter::$MATERIAL_VALUE_KG[$value->materials_type_id];
            
            //loc theo san luong qtykg_from -> to
//            if ($qtyKg >= $model->qtykg_from && $qtyKg <= $model->qtykg_to) {
//                if(!isset($aGasStoreCardDetail[$value->customer_id])){
//                    $aGasStoreCardDetail[$value->customer_id] = $qtyKg;
//                }else{
//                    $aGasStoreCardDetail[$value->customer_id] += $qtyKg;                
//                }
//            }
            if(!isset($aGasStoreCardDetail[$value->customer_id])) {
                $aGasStoreCardDetail[$value->customer_id] = $qtyKg;
            } else {
                $aGasStoreCardDetail[$value->customer_id] += $qtyKg;                
            }
        }
        
        // tinh san luong gan nhat trong 3 thang
        $criteria2 = new CDbCriteria();
        $this->reportCustomerProductionGetCriteria($date_from, $date_to, $model, $criteria2, true, $aCustomer);
        $aModels2 = GasStoreCardDetail::model()->findAll($criteria2);
        if (empty($aModels2))
            return [];
        foreach ($aModels2 as $key => $value) {
            $qtyKg = $value->qty*CmsFormatter::$MATERIAL_VALUE_KG[$value->materials_type_id];            
            //loc theo san luong qtykg_from -> to
//            if ($qtyKg >= $model->qtykg_from && $qtyKg <= $model->qtykg_to) {
//                if(!isset($aSanLuongGanNhat[$value->customer_id][$value->date_delivery])){
//                    $aSanLuongGanNhat[$value->customer_id][$value->date_delivery] = $qtyKg;
//                }else{
//                    if (isset($aSanLuongGanNhat[$value->customer_id]))
//                        $aSanLuongGanNhat[$value->customer_id][$value->date_delivery] += $qtyKg;                
//                }
//            }
            $date_delivery = date_format(date_create($value->date_delivery), 'm-Y');
            if(!isset($aSanLuongGanNhat[$value->customer_id][$date_delivery])) {
                    $aSanLuongGanNhat[$value->customer_id][$date_delivery] = $qtyKg;
            } else {
                if (isset($aSanLuongGanNhat[$value->customer_id]))
                    $aSanLuongGanNhat[$value->customer_id][$date_delivery] += $qtyKg;                
            }
        }
        
        //goi ham tinh cong no vo
        $aCongNoVo = $this->calcDebitVo($model);
        if (empty($aCongNoVo))
            $aCongNoVo = [];
        
        //goi ham tinh cong no tien
        $aCongNoTien = $this->calcDebitCash($model);
        if (empty($aCongNoTien))
            $aCongNoTien = [];
        
        //tra ve cac du lieu
        $res['aModelCustomer']          = empty($aCustomer) ? [] : Users::getArrObjectUserByRole("", $aCustomer);
        $res['aModelSale']              = empty($aSale)     ? [] : Users::getArrObjectUserByRole("", $aSale);
        $res['aGasStoreCardDetail']     = $aGasStoreCardDetail;
        $res['aCongNoVo']               = $aCongNoVo;
        $res['aCongNoTien']             = $aCongNoTien;
        $res['aSanLuongGanNhat']        = $aSanLuongGanNhat;
        return $res;        
    }
}
