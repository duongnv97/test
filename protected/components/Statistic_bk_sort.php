<?php
class Statistic_bk_sort
{
    public static function Statistic_maintain(){
        // 1. get arr id agent
        // 2. user relation (agentCustomerCount) of Model User to count customer of agent limit by created date
        // 3. query table maintain each agent count  DA BAO TRI	CUOC GOI TOT	CUOC GOI XAU limit by created date
        // 4. query table maintain SELL  each agent count  BAN HANG SAU BT	CUOC GOI TOT	CUOC GOI XAU limit by created date
        // one query then group by agent id. hi hi

        $aIdAgent = Users::getArrIdUserByRole(); // default is role agent
        $date_from = '';
        $date_to = '';
        $aResSta = array();
        if(!empty($_POST['GasMaintain']['date_from']))
                $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($_POST['GasMaintain']['date_from']);
        if(!empty($_POST['GasMaintain']['date_to']))
                $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($_POST['GasMaintain']['date_to']);
        $aResSta['DATE_FROM'] = $_POST['GasMaintain']['date_from'];
        $aResSta['DATE_TO'] = $_POST['GasMaintain']['date_to'];
        // 1 get count customer of agent
        $aResSta['AGENT']['AgentCustomer'] = Statistic::getAgentCustomerCount($aIdAgent, $date_from, $date_to);
        $aResSta['AGENT']['CountIsMaintain'] = Statistic::getAgentMaintainCount('GasMaintain', $date_from, $date_to);
        $aResSta['AGENT']['CountIsMaintainCallGood'] = Statistic::getAgentMaintainCount('GasMaintain', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_GOOD);
        $aResSta['AGENT']['CountIsMaintainCallBad'] = Statistic::getAgentMaintainCount('GasMaintain', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_BAD);

        $aResSta['AGENT']['CountSellMaintain'] = Statistic::getAgentMaintainCount('GasMaintainSell', $date_from, $date_to);
        $aResSta['AGENT']['CountSellMaintainCallGood'] = Statistic::getAgentMaintainCount('GasMaintainSell', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_GOOD);
        $aResSta['AGENT']['CountSellMaintainCallBad'] = Statistic::getAgentMaintainCount('GasMaintainSell', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_BAD);

        // 2. get count by NV PHUC VU KHACH HANG
        $aResSta['MAINTAIN_EMPLOYEE']['CountIsMaintain'] = Statistic::getMaintainCountOfEmployee('GasMaintain', $date_from, $date_to);
        $aResSta['MAINTAIN_EMPLOYEE']['CountIsMaintainCallGood'] = Statistic::getMaintainCountOfEmployee('GasMaintain', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_GOOD);
        $aResSta['MAINTAIN_EMPLOYEE']['CountIsMaintainCallBad'] = Statistic::getMaintainCountOfEmployee('GasMaintain', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_BAD);
        $aResSta['MAINTAIN_EMPLOYEE']['CountSellMaintain'] = Statistic::getMaintainCountOfEmployee('GasMaintainSell', $date_from, $date_to);
        $aResSta['MAINTAIN_EMPLOYEE']['CountSellMaintainCallGood'] = Statistic::getMaintainCountOfEmployee('GasMaintainSell', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_GOOD);
        $aResSta['MAINTAIN_EMPLOYEE']['CountSellMaintainCallBad'] = Statistic::getMaintainCountOfEmployee('GasMaintainSell', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_BAD);


        //public static function getMaintainCountOfEmployee($name_model, $date_from, $date_to, $status='', $field_select='maintain_employee_id', $type=TYPE_MAINTAIN)
        // 3. get count by NV KE TOAN BAN HANG
        $aResSta['MAINTAIN_EMPLOYEE_ACCOUNTING']['CountIsMaintain'] = Statistic::getMaintainCountOfEmployee('GasMaintain', $date_from, $date_to, '', 'accounting_employee_id');
        $aResSta['MAINTAIN_EMPLOYEE_ACCOUNTING']['CountIsMaintainCallGood'] = Statistic::getMaintainCountOfEmployee('GasMaintain', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_GOOD, 'accounting_employee_id');
        $aResSta['MAINTAIN_EMPLOYEE_ACCOUNTING']['CountIsMaintainCallBad'] = Statistic::getMaintainCountOfEmployee('GasMaintain', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_BAD, 'accounting_employee_id');
        $aResSta['MAINTAIN_EMPLOYEE_ACCOUNTING']['CountSellMaintain'] = Statistic::getMaintainCountOfEmployee('GasMaintainSell', $date_from, $date_to, '', 'accounting_employee_id');
        $aResSta['MAINTAIN_EMPLOYEE_ACCOUNTING']['CountSellMaintainCallGood'] = Statistic::getMaintainCountOfEmployee('GasMaintainSell', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_GOOD, 'accounting_employee_id');
        $aResSta['MAINTAIN_EMPLOYEE_ACCOUNTING']['CountSellMaintainCallBad'] = Statistic::getMaintainCountOfEmployee('GasMaintainSell', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_BAD, 'accounting_employee_id');

        $_SESSION['data-excel'] = $aResSta;
        return $aResSta;	
  }

    public static function Statistic_market_development(){
            // 1. get arr id agent
            // 2. user relation (agentCustomerCount) of Model User to count customer of agent limit by created date
            // 3. query table maintain each agent count  DA BAO TRI	CUOC GOI TOT	CUOC GOI XAU limit by created date
            // 4. query table maintain SELL  each agent count  BAN HANG SAU BT	CUOC GOI TOT	CUOC GOI XAU limit by created date
            // one query then group by agent id. hi hi

            $aIdAgent = Users::getArrIdUserByRole(); // default is role agent
            $date_from = '';
            $date_to = '';
            $aResSta = array();
            if(!empty($_POST['GasMaintain']['date_from']))
                    $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($_POST['GasMaintain']['date_from']);
            if(!empty($_POST['GasMaintain']['date_to']))
                    $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($_POST['GasMaintain']['date_to']);
            $aResSta['DATE_FROM'] = $_POST['GasMaintain']['date_from'];
            $aResSta['DATE_TO'] = $_POST['GasMaintain']['date_to'];
            // 2. get count by NV PHUC VU KHACH HANG
            $aResSta['MAINTAIN_EMPLOYEE']['CountIsMaintain'] = Statistic::getMaintainCountOfEmployee('GasMaintain', $date_from, $date_to, '', 'maintain_employee_id', TYPE_MARKET_DEVELOPMENT);
            $aResSta['MAINTAIN_EMPLOYEE']['CountIsMaintainCallGood'] = Statistic::getMaintainCountOfEmployee('GasMaintain', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_GOOD, 'maintain_employee_id', TYPE_MARKET_DEVELOPMENT);
            $aResSta['MAINTAIN_EMPLOYEE']['CountIsMaintainCallBad'] = Statistic::getMaintainCountOfEmployee('GasMaintain', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_BAD, 'maintain_employee_id', TYPE_MARKET_DEVELOPMENT);
            $aResSta['AGENT'] = array();
            $aResSta['MAINTAIN_EMPLOYEE_ACCOUNTING'] = array();
            $_SESSION['data-excel'] = $aResSta;
            return $aResSta;	
  }

      public static function getAgentCustomerCount($aIdAgent, $date_from, $date_to, $type=CUSTOMER_TYPE_MAINTAIN){
                    $aRes = array();
                    $total = 0;

                    foreach($aIdAgent as $agent_id){
                            $list_customer_id = GasMaintain::getCustomerOfAgent($agent_id);
                            $criteria = new CDBcriteria();			
                            $criteria->addInCondition('t.id', $list_customer_id);
                            if(!empty($date_from) && empty($date_to))
                                    $criteria->addCondition("t.created_date>='$date_from'");
                            if(empty($date_from) && !empty($date_to))
                                    $criteria->addCondition("t.created_date<='$date_to'");
                            if(!empty($date_from) && !empty($date_to))
                                    $criteria->addBetweenCondition("t.created_date",$date_from,$date_to);	
                            $criteria->compare('t.type', $type);
                            if(isset($_POST['GasMaintain']['province_id']))
                                $criteria->compare('t.province_id', $_POST['GasMaintain']['province_id']);
                            $aRes[$agent_id] =  Users::model()->count($criteria);			
                            $total += $aRes[$agent_id];
                    }
                    if($_POST['GasMaintain']['sort_by']=='asc'){ // tăng dần asort low to high							
                            asort($aRes);						
                    }else{ // high to low
                            arsort($aRes);
                    }
                    //echo '<pre>'; print_r($aRes);echo '</pre>'; die;
                    $aRes['total_count'] = $total;
                    return $aRes;	

                    /* $criteria = new CDBcriteria();
                    $criteria->addInCondition('t.id', $aIdAgent);
                    $criteria->with = array('agentCustomerCount');
                    $criteria->together = true;
                    if(!empty($date_from) && empty($date_to))
                            $criteria->addCondition("agentCustomerCount.created_date>=$date_from");
                    if(empty($date_from) && !empty($date_to))
                            $criteria->addCondition("agentCustomerCount.created_date<=$date_to");
                    if(!empty($date_from) && !empty($date_to))
                            $criteria->addBetweenCondition('agentCustomerCount.created_date',$date_from,$date_to);
                    $mAgentCustomer = Users::model()->findAll($criteria);
                    $aRes = array();
                    if(count($mAgentCustomer)>0)
                    foreach($mAgentCustomer as $item){
                            $aRes[$item->id] = $item->agentCustomerCount;
                    }
                    return $aRes;			 */
      }
	  	  
    /**
     * @Author: ANH DUNG 11-11-2013
     * @Todo: thống kê đếm số bảo trì OR PTTT 
     * theo tỉnh, đại lý,  khoảng thời gian vs trạng thái
     * @Param: model $name_model is model GasMaintain or GasMaintainsell
     * @Param: date $date_from is date from: 2013-11-15
     * @Param: date $date_to is date to: 2013-12-15
     * @Param: array $status is array status
     * @Param: string $field_select is agent_id
     * @Param: number $type is 1: bảo trì, 2: PTTT
     * @Return: array[agent_id]=>123
     */     
    public static function getAgentMaintainCount($name_model, $date_from, $date_to, $status='', $field_select='agent_id', $type=TYPE_MAINTAIN){
        $criteria=new CDbCriteria;
        $total = 0;
        $date_select = 'maintain_date';
        if($name_model=='GasMaintainSell')
                $date_select = 'date_sell';

        if(!empty($date_from) && empty($date_to))
                $criteria->addCondition("t.$date_select>='$date_from'");
        if(empty($date_from) && !empty($date_to))
                $criteria->addCondition("t.$date_select<='$date_to'");
        if(!empty($date_from) && !empty($date_to))
                $criteria->addBetweenCondition("t.$date_select",$date_from,$date_to); 	
        if(is_array($status))
                $criteria->addInCondition('t.status', $status);
        $criteria->compare('t.type', $type);
        if(isset($_POST['GasMaintain']['province_id']))
            $criteria->compare('t.province_id', $_POST['GasMaintain']['province_id']);

        $criteria->select = "$field_select, count(id) as count_row";
        $criteria->group = "$field_select";		
        $model_ = call_user_func(array($name_model, 'model'));
        $mRes = $model_->findAll($criteria);
        $aRes = array();
        if(count($mRes)>0)
        foreach($mRes as $item){
                $aRes[$item->$field_select] = $item->count_row;
                $total += $aRes[$item->$field_select];
        }
        $aRes['total_count'] = $total;
        return $aRes;			
    }
          
    /**
     * @Author: ANH DUNG 11-19-2013
     * @Todo: thống kê đếm số bảo trì OR PTTT theo nhân viên bảo trì hay kế toán đại lý,
     * theo tỉnh, khoảng thời gian vs trạng thái
     * @Param: model $name_model is model GasMaintain or GasMaintainsell
     * @Param: date $date_from is date from: 2013-11-15
     * @Param: date $date_to is date to: 2013-12-15
     * @Param: array $status is array status
     * @Param: string $field_select is maintain_employee_id OR accounting_employee_id
     * @Param: number $type is 1: bảo trì, 2: PTTT
     * @Return: array[agent_id][maintain_employee_id OR accounting_employee_id]=>123
     */     
    public static function getMaintainCountOfEmployee($name_model, $date_from, $date_to, $status='', $field_select='maintain_employee_id', $type=TYPE_MAINTAIN){
        $criteria=new CDbCriteria;
        $total = 0;
              $date_select = 'maintain_date';
              if($name_model=='GasMaintainSell')
                      $date_select = 'date_sell';

        if(!empty($date_from) && empty($date_to))
                $criteria->addCondition("t.$date_select>='$date_from'");
        if(empty($date_from) && !empty($date_to))
                $criteria->addCondition("t.$date_select<='$date_to'");
        if(!empty($date_from) && !empty($date_to))
                $criteria->addBetweenCondition("t.$date_select",$date_from,$date_to); 	
        if(is_array($status))
                $criteria->addInCondition('t.status', $status);
        $criteria->select = "$field_select, agent_id, monitoring_id, count(id) as count_row";
                        if($type==TYPE_MAINTAIN)
                              $criteria->group = "$field_select, agent_id";
                      else
                              $criteria->group = "$field_select";
        $criteria->compare('t.type', $type);

        if(isset($_POST['GasMaintain']['province_id']))
          $criteria->compare('t.province_id', $_POST['GasMaintain']['province_id']);

        $model_ = call_user_func(array($name_model, 'model'));
        $mRes = $model_->findAll($criteria);
        $aRes = array();
        if(count($mRes)>0)
        foreach($mRes as $item){
            if($type==TYPE_MAINTAIN)
                $aRes[$item->agent_id][$item->$field_select] = $item->count_row;
            else
                $aRes[$item->monitoring_id][$item->$field_select] = $item->count_row;
                $total += $item->count_row;
        }
        $aRes['total_count'] = $total;
        return $aRes;			
    }
	  
    /**
     * @Author: ANH DUNG 11-19-2013
     * @Todo: thống kê đếm số bảo trì OR PTTT theo từng tháng,
     * theo tỉnh, khoảng thời gian vs trạng thái
     * @Param: model $model is model GasMaintain
     * @Param: string $field_name is maintain_date OR date_sell
     * @Param: model $name_model is model GasMaintain or GasMaintainsell
     * @Param: number $type is 1: bảo trì, 2: PTTT
     * @Return: array[total_month][2013][6][agent_id]=>123
     */ 
    public static function Statistic_maintain_by_month($model, $field_name='maintain_date', $name_model='GasMaintain', $type=TYPE_MAINTAIN){
        $model->statistic_month = is_array($model->statistic_month)?$model->statistic_month:ActiveRecord::getArrMonthSearch();
        $aRes = array();
        foreach($model->statistic_month as $month){
            // Lấy số ngày trong tháng
            $days_in_month = cal_days_in_month(0, $month, $model->statistic_year) ;

            for($i=1;$i<=$days_in_month; $i++){
                if($i<10)
                    $i='0'.$i;
               $date = $model->statistic_year.'-'.$month.'-'.$i;

               $criteria = new CDBcriteria();
               $criteria->compare("t.$field_name",$date);
               $criteria->compare("t.type", $type);
               if(isset($_POST['GasMaintain']['province_id']))
                 $criteria->compare('t.province_id', $_POST['GasMaintain']['province_id']);

               $criteria->select = "agent_id, count(id) as count_row";
               $criteria->group = "agent_id";		
               $model_ = call_user_func(array($name_model, 'model'));
               $mRes = $model_->findAll($criteria);

               if(count($mRes)>0){
                   if(!isset($aRes['month'][$month]))
                       $aRes['month'][$month] = 1;
                   $aRes['days'][$month][] = $i;
                   foreach($mRes as $item){
                       $aRes[$model->statistic_year][$month][$i][$item->agent_id] = $item->count_row;
                       if(!isset($aRes['total_month'][$model->statistic_year][$month][$item->agent_id]))
                           $aRes['total_month'][$model->statistic_year][$month][$item->agent_id] = $item->count_row;
                       else
                           $aRes['total_month'][$model->statistic_year][$month][$item->agent_id] += $item->count_row;
                   }
               }

            }

           //if($month==10){
           //echo '<pre>'; print_r($aRes['total_month'][$model->statistic_year][$month]);echo '</pre>';
           //}
           if(isset($aRes['total_month'][$model->statistic_year][$month]) && is_array($aRes['total_month'][$model->statistic_year][$month])){
               if($_POST['GasMaintain']['sort_by']=='asc'){ // tăng dần asort low to high	
                   asort($aRes['total_month'][$model->statistic_year][$month]);
               }else{ // high to low
                   arsort($aRes['total_month'][$model->statistic_year][$month]);
               }
           }
        }
        $_SESSION['data-excel'] = $aRes;
        return $aRes;             
    } 
		 
	/**
     * @Author: ANH DUNG 11-22-2013
     * @Todo: thống kê đếm số bảo trì OR PTTT theo từng tháng,
     * theo tỉnh, khoảng thời gian vs trạng thái
     * @Param: model $model is model GasMaintain
     * @Param: string $field_name is maintain_date OR date_sell
     * @Param: model $name_model is model GasMaintain or GasMaintainsell
     * @Param: number $type is 1: bảo trì, 2: PTTT
     * @Return: array[total_month][2013][6][agent_id]=>123
     */ 
    public static function Statistic_market_development_by_month($model, $field_name='maintain_date', $name_model='GasMaintain', $type=TYPE_MARKET_DEVELOPMENT){
        $model->statistic_month = is_array($model->statistic_month)?$model->statistic_month:ActiveRecord::getArrMonthSearch();
        $model->monitoring_id = is_array($model->monitoring_id)?$model->monitoring_id:array();
        $aRes = array();
        foreach($model->statistic_month as $month){
            // Lấy số ngày trong tháng
            $days_in_month = cal_days_in_month(0, $month, $model->statistic_year) ;

            for($i=1;$i<=$days_in_month; $i++){
                if($i<10)
                    $i='0'.$i;
               $date = $model->statistic_year.'-'.$month.'-'.$i;

               $criteria = new CDBcriteria();
               $criteria->compare("t.$field_name",$date);
               $criteria->compare('t.type', $type);
               if(isset($_POST['GasMaintain']['province_id']))
                 $criteria->compare('t.province_id', $_POST['GasMaintain']['province_id']);
               if(count($model->monitoring_id)>0)
                   $criteria->addInCondition('t.monitoring_id', $model->monitoring_id);
               if(!empty($model->agent_id))
                   $criteria->compare('t.agent_id', $model->agent_id);

               $criteria->select = "maintain_employee_id, count(id) as count_row, monitoring_id";
               $criteria->group = "maintain_employee_id";		
               $model_ = call_user_func(array($name_model, 'model'));
               $mRes = $model_->findAll($criteria);

               if(count($mRes)>0){
                   if(!isset($aRes['month'][$month]))
                       $aRes['month'][$month] = 1;
                   $aRes['days'][$month][] = $i;
                   foreach($mRes as $item){
                       //if($item->count_row>0){
                           $aRes['market_dev'][$model->statistic_year][$month][$i][$item->monitoring_id][$item->maintain_employee_id] = $item->count_row;
                           if(!isset($aRes['total_month'][$model->statistic_year][$month][$item->monitoring_id][$item->maintain_employee_id]))
                                   $aRes['total_month'][$model->statistic_year][$month][$item->monitoring_id][$item->maintain_employee_id] = $item->count_row;
                           else
                                   $aRes['total_month'][$model->statistic_year][$month][$item->monitoring_id][$item->maintain_employee_id] += $item->count_row;
                           if(!isset($aRes['total_monitor'][$model->statistic_year][$month][$item->monitoring_id]))
                                   $aRes['total_monitor'][$model->statistic_year][$month][$item->monitoring_id] = $item->count_row;
                           else
                                   $aRes['total_monitor'][$model->statistic_year][$month][$item->monitoring_id] += $item->count_row;
                           if($item->count_row>0){
                               if(!isset($aRes['rowspan'][$model->statistic_year][$month][$item->monitoring_id]))
                                   $aRes['rowspan'][$model->statistic_year][$month][$item->monitoring_id] = 1;
                               else
                                   $aRes['rowspan'][$model->statistic_year][$month][$item->monitoring_id] += 1;
                           }
                       //}
                   }

               }// end if(count($mRes)>0){	

               // ++++++ to get bán hàng của PTTT ++++++++ ///

               $criteria = new CDBcriteria();
               $criteria->compare("t.date_sell",$date);
               $criteria->compare('t.type', $type);
               if(isset($_POST['GasMaintain']['province_id']))
                 $criteria->compare('t.province_id', $_POST['GasMaintain']['province_id']);
               if(!empty($model->agent_id))
                   $criteria->compare('t.agent_id', $model->agent_id);
               $criteria->select = "maintain_employee_id, count(id) as count_row, monitoring_id";
               $criteria->group = "maintain_employee_id";		
               $mRes = GasMaintainSell::model()->findAll($criteria);

               if(count($mRes)>0){
                   if(!isset($aRes['month'][$month]))
                       $aRes['month'][$month] = 1;
                   if(!isset($aRes['days'][$month]))
                       $aRes['days'][$month] = array();
                   if(!in_array($i , $aRes['days'][$month]))
                       $aRes['days'][$month][] = $i;
                   foreach($mRes as $item){
//                            if($item->count_row>0){
                           $aRes['market_dev_sell'][$model->statistic_year][$month][$i][$item->monitoring_id][$item->maintain_employee_id] = $item->count_row;

/*                          if(!isset($aRes['total_month_sell'][$model->statistic_year][$month][$item->monitoring_id][$item->maintain_employee_id]))
                                   $aRes['total_month_sell'][$model->statistic_year][$month][$item->monitoring_id][$item->maintain_employee_id] = $item->count_row;
                           else
                               $aRes['total_month_sell'][$model->statistic_year][$month][$item->monitoring_id][$item->maintain_employee_id] += $item->count_row;
                           */
                               // 05-12-2013 fix for employee Võ Hữu Hòa 114/2	

                           // đoạn này fix cho, nếu trong tháng nhân viên không có 1 PTTT nào, mà có bán hàng
                           if(!isset($aRes['total_month'][$model->statistic_year][$month][$item->monitoring_id][$item->maintain_employee_id]))
                               $aRes['total_month'][$model->statistic_year][$month][$item->monitoring_id][$item->maintain_employee_id] = 0;
                           
                           if(!isset($aRes['total_monitor'][$model->statistic_year][$month][$item->monitoring_id]))
                                   $aRes['total_monitor'][$model->statistic_year][$month][$item->monitoring_id] = 0;
                           
                           // đoạn này fix cho, nếu trong tháng nhân viên không có 1 PTTT nào, mà có bán hàng

                           if(!isset($aRes['total_month_sell'][$model->statistic_year][$month][$item->monitoring_id][$item->maintain_employee_id]))
                                   $aRes['total_month_sell'][$model->statistic_year][$month][$item->monitoring_id][$item->maintain_employee_id] = $item->count_row;
                           else
                                   $aRes['total_month_sell'][$model->statistic_year][$month][$item->monitoring_id][$item->maintain_employee_id] += $item->count_row;

                           if(!isset($aRes['total_monitor_sell'][$model->statistic_year][$month][$item->monitoring_id]))
                                   $aRes['total_monitor_sell'][$model->statistic_year][$month][$item->monitoring_id] = $item->count_row;
                           else
                                   $aRes['total_monitor_sell'][$model->statistic_year][$month][$item->monitoring_id] += $item->count_row;
//                            }	
                   }
               } // end if(count($mRes)>0){					
               // ++++++ to get bán hàng của PTTT ++++++++ 7,10,6///

            }// end for  for($i=1;$i<=$days_in_month; $i++){      

           if(0){// debug
                   //if($month==11)
               echo '<pre>';
               print_r($aRes['market_dev_sell']);die;
               print_r($aRes['total_month'][$model->statistic_year][$month]);
               asort($aRes['total_month'][$model->statistic_year][$month]);
               print_r($aRes['total_month'][$model->statistic_year][$month]);

               arsort($aRes['total_month'][$model->statistic_year][$month]);
               print_r($aRes['total_month'][$model->statistic_year][$month]);
               echo '</pre>';
               die;
           }		

           // Xử lý sắp xếp mảng của giám sát vs nhân viên PTTT theo thứ tự
           // $aRes['total_month'][$model->statistic_year][$month] is list array[monitor_id]=>array[employee_id]=>total
           if(isset($aRes['total_month'][$model->statistic_year][$month]) && is_array($aRes['total_month'][$model->statistic_year][$month]) && count($aRes['total_month'][$model->statistic_year][$month])>0)
           {
                   //echo '<pre>';
                   //print_r($aRes['total_month'][$model->statistic_year][$month]);echo '</pre>';
                   foreach($aRes['total_month'][$model->statistic_year][$month] as $key=>$item){
                       if($model->sort_by=='asc'){ // tăng dần asort low to high							
                               asort($item);						
                       }else{ // high to low
                               arsort($item);
                       }
                       $aRes['total_month'][$model->statistic_year][$month][$key] = $item;
                   }
                   // sort for total of monitor
                   if(isset($aRes['total_monitor'][$model->statistic_year][$month])){
                        if($model->sort_by=='asc'){ // tăng dần asort low to high	
                                asort($aRes['total_monitor'][$model->statistic_year][$month]);						
                        }else{ // high to low
                                arsort($aRes['total_monitor'][$model->statistic_year][$month]);
                        }
                    }
                   // sort for total of monitor
                   //echo '<pre>';
                   //print_r($aRes['total_month'][$model->statistic_year][$month]);echo '</pre>'; die;
                   //print_r($aRes['total_monitor'][$model->statistic_year][$month]);echo '</pre>'; die;
           }

           // end Xử lý sắp xếp mảng của giám sát vs nhân viên PTTT theo thứ tự

        } // end for foreach($model->statistic_month as $month){
        $_SESSION['data-excel'] = $aRes;
                    //var_dump($aRes['total_month']);
        return $aRes;             
    }
    
    /**
     * @Author: ANH DUNG 02-05-2014
     * @Todo: thống kê doanh thu - sản lượng của từng đại lý theo tháng,  năm
     * @Return: 1 tab đầu tổng của năm, tiếp theo là từng tháng
     */    
    public static function RevenueOutput($model){
        // 1. TỔNG SẢN LƯỢNG GAS BÌNH 12KG THÁNG 12 NĂM 2013
        // 2. TỔNG SẢN LƯỢNG GAS 12KG NĂM 2013
        // 3. TỔNG SẢN LƯỢNG GAS BÌNH BÒ THÁNG 11 NĂM 2013
        // 4. TỔNG SẢN LƯỢNG GAS BÌNH BÒ ĐẠI LÝ NĂM 2013
        // 5. TỔNG DOANH THU ĐẠI LÝ THÁNG 12 NĂM 2013
        // 6. TỔNG DOANH THU ĐẠI LÝ NĂM 2013
        // 7. TỔNG ĐẠI LÝ NỘP CÔNG TY THÁNG 12 NĂM 2013
        // 8. TỔNG ĐẠI LÝ NỘP CÔNG TY NĂM 2013
        $aRes = array();
        $aRes['OUTPUT'] = Statistic::getTotalOutput($model);
//        $aRes['REVENUE'] = Statistic::getTotalOutput($model); // LATER
        
        $_SESSION['data-excel'] = $aRes;
        return $aRes;
    }
    
    /**
     * @Author: ANH DUNG 02-07-2014
     * @Todo: tính tổng sản lượng của các loại gas
     * @param: $model 
     * @Return: array
     */        
    public static function getTotalOutput($model){
        $aRes = array();
        // lặp 2 loại bình thống kê là 12 và 45kg
        foreach( CmsFormatter::$MATERIAL_TYPE_OUTPUT_STATISTIC as $type_output){
            // 1. get id những vật tư có loại là Gas 12 Kg OR 45kg
            $aMaterialId = GasMaterials::getArrayIdByMaterialTypeId($type_output['list_id']);        
            foreach($model->statistic_month as $month){
                Statistic::getOutputByMonthYear($model, $month, $aMaterialId, $aRes, $type_output);
            }// end foreach($model->statistic_month as $month){            
        }
        return $aRes;
    }
	 
    /**
     * @Author: ANH DUNG 02-07-2014
     * @Todo: tính tổng sản lượng của từng loại gas, xếp theo ngày, tính theo loại vật tư
     * @param: $aMaterial array id vật tư
     * @param: $month     
     * @param: $model
     * @param: $aRes: is final array return
     * @param: $type_output is  array(
            'code'=>'12KG',
            'name'=>'BÌNH 12KG',
            'list_id'=>array(4), // hiện tại chỉ có 1, sẽ dc tách ra nhiều sau này
        ), 
     * @Return: not return because it in $aRes
     */        
    public static function getOutputByMonthYear($model, $month, $aMaterialId, &$aRes, $type_output){        
        // Lấy số ngày trong tháng
        $days_in_month = cal_days_in_month(0, $month, $model->statistic_year) ;
        $aAgent = Users::getArrObjectUserByRoleHaveOrder(ROLE_AGENT, 'id');// $aAgent dạng: array[id agent]=>model agent
        $aAgentSortMonth = array();
        $aAgentSortYear = array();
        for($i=1;$i<=$days_in_month; $i++){
            if($i<10)
                $i='0'.$i;
            $date = $model->statistic_year.'-'.$month.'-'.$i;
            $criteria = new CDBcriteria();
            $criteria->compare("t.date_delivery",$date);
            $criteria->addInCondition("t.type_in_out", CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC); // xuất bán, bán bộ bình, thế chân
            $criteria->addInCondition("t.materials_id", $aMaterialId);
            $criteria->addInCondition("t.user_id_create", $model->agent_id);
            $criteria->select = "t.user_id_create, sum(qty) as qty";
            $criteria->group = "t.user_id_create";
            $mRes = GasStoreCardDetail::model()->findAll($criteria);
            if(count($mRes)>0){
                
                if(!isset($aRes['month'][$month]))
                    $aRes['month'][$month] = 1; // biến $month dùng để foreach nên chỉ cần gán =1 là dc
                $aRes[$type_output['code']]['days'][$month][] = $i;
                foreach($mRes as $item){
                    //  for tab each month
                    // one cell
                    $aRes[$type_output['code']][$month][$i][$item->user_id_create] = $item->qty;
                    // like : $aRes[12KG]['total_day'][08][agent 102]
                    //sum_row -- không dùng kiểu này vì không sort dc theo tỉnh, dùng kiểu bên dưới
//                    if(!isset($aRes[$type_output['code']]['sum_row_total_month'][$month][$item->user_id_create]))
//                        $aRes[$type_output['code']]['sum_row_total_month'][$month][$item->user_id_create] = $item->qty;
//                    else
//                        $aRes[$type_output['code']]['sum_row_total_month'][$month][$item->user_id_create] += $item->qty;
                    if(!isset($aAgentSortMonth[$aAgent[$item->user_id_create]->province_id][$item->user_id_create]))
                        $aAgentSortMonth[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] = $item->qty;
                    else
                        $aAgentSortMonth[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] += $item->qty;
                    
                    if(!isset($aRes[$type_output['code']]['sum_col_total_month_all_agent'][$month]))
                        $aRes[$type_output['code']]['sum_col_total_month_all_agent'][$month] = $item->qty;
                    else
                        $aRes[$type_output['code']]['sum_col_total_month_all_agent'][$month] += $item->qty;
                    // need sum column...here, nên sum luôn column ở đây, xuống view khỏi tạo biến để sum nữa
                    // sum column one day for all agent
                    if(!isset($aRes[$type_output['code']]['sum_col'][$month][$i]))
                        $aRes[$type_output['code']]['sum_col'][$month][$i] = $item->qty;
                    else
                        $aRes[$type_output['code']]['sum_col'][$month][$i] += $item->qty;                    
                    
                    //  for tab each month
                    
                    //  for tab sum year
                    // one cell
                    if(!isset($aRes[$type_output['code']]['total_year'][$month][$item->user_id_create]))
                        $aRes[$type_output['code']]['total_year'][$month][$item->user_id_create] = $item->qty;
                    else
                        $aRes[$type_output['code']]['total_year'][$month][$item->user_id_create] += $item->qty;
                    
                    //sum_row
//                    if(!isset($aRes[$type_output['code']]['total_year']['sum_row'][$item->user_id_create]))
//                        $aRes[$type_output['code']]['total_year']['sum_row'][$item->user_id_create] = $item->qty;
//                    else
//                        $aRes[$type_output['code']]['total_year']['sum_row'][$item->user_id_create] += $item->qty;
                    if(!isset($aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create]))
                        $aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] = $item->qty;
                    else
                        $aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] += $item->qty;

                    // need sum column...here, nên sum luôn column ở đây, xuống view khỏi tạo biến để sum nữa
                    // sum column one day for all agent
                    if(!isset($aRes[$type_output['code']]['total_year']['sum_col'][$month]))
                        $aRes[$type_output['code']]['total_year']['sum_col'][$month] = $item->qty;
                    else
                        $aRes[$type_output['code']]['total_year']['sum_col'][$month] += $item->qty;           
                    
                    if(!isset($aRes[$type_output['code']]['sum_col_total_year_all_agent']))
                        $aRes[$type_output['code']]['sum_col_total_year_all_agent'] = $item->qty;
                    else
                        $aRes[$type_output['code']]['sum_col_total_year_all_agent'] += $item->qty;
                    
                    //  for tab sum year
                }
                   
            }
                        
        } // end  for($i=1;$i<=$days_in_month; $i++){
        
        // sort
//        if(isset($aRes[$type_output['code']]['sum_row_total_month'][$month]) && is_array($aRes[$type_output['code']]['sum_row_total_month'][$month])){
//            arsort($aRes[$type_output['code']]['sum_row_total_month'][$month]);// giảm dần high to low
//        }     
//        if(isset($aRes[$type_output['code']]['total_year']['sum_row']) && is_array($aRes[$type_output['code']]['total_year']['sum_row'])){
//            arsort($aRes[$type_output['code']]['total_year']['sum_row']);// giảm dần high to low
//        }  
        // sort each month by tỉnh
        if(count($aAgentSortMonth)){
            foreach($aAgentSortMonth as $province_id=>$arrayAgentTotalMonth){
                arsort($arrayAgentTotalMonth);
                if(!isset($aRes[$type_output['code']]['sum_row_total_month'][$month]))
                    $aRes[$type_output['code']]['sum_row_total_month'][$month] = $arrayAgentTotalMonth;
                else
                    $aRes[$type_output['code']]['sum_row_total_month'][$month] += $arrayAgentTotalMonth;
            }
        }
        
        // sort year by tỉnh
        if(count($aAgentSortYear)){
            foreach($aAgentSortYear as $province_id=>$arrayAgentTotalMonth){
                arsort($arrayAgentTotalMonth);
                if(!isset($aRes[$type_output['code']]['total_year']['sum_row']))
                    $aRes[$type_output['code']]['total_year']['sum_row'] = $arrayAgentTotalMonth;
                else
                    $aRes[$type_output['code']]['total_year']['sum_row'] += $arrayAgentTotalMonth;
            }
        }
        // sort
    }
	 
	  
}
?>