<?php
class ExportExcel
{
     // Nguyen Dung 08-29-2013
      public static function export_agent_revenue($dataExport,$dataAgent){
      try{
                Yii::import('application.extensions.vendors.PHPExcel',true);
		$objPHPExcel = new PHPExcel();
		 // Set properties
		 $objPHPExcel->getProperties()->setCreator("NguyenDung")
                            ->setLastModifiedBy("NguyenDung")
                            ->setTitle('Report Monthly Revenue')
                            ->setSubject("Office 2007 XLSX Document")
                            ->setDescription("Report Monthly Revenue")
                            ->setKeywords("office 2007 openxml php")
                            ->setCategory("GAS");
		// step 0: Tạo ra các sheet
                $i=0;
                if(count($dataExport)>0){
                     $objPHPExcel->removeSheetByIndex(0); // may be need
                    foreach($dataExport as $year=>$aMonth){
                        $objPHPExcel->createSheet();
                        $objPHPExcel->setActiveSheetIndex($i);
                        // set default font and font-size
                        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
                        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
                        $objPHPExcel->getActiveSheet()->setTitle('NĂM '.$year);
                        $objPHPExcel->getActiveSheet()->setCellValue("A1", 'BÁO CÁO DOANH THU THEO THÁNG CỦA NĂM '.$year);
                        $objPHPExcel->getActiveSheet()->mergeCells("A1:D1");
                        
                        $objPHPExcel->getActiveSheet()->setCellValue("A3", 'Đại Lý');
                        $keyMonth=1;
                        if(count($aMonth)>0){
                            foreach ($aMonth as $month=>$aObj){
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+1).'3', $month);
                                $keyMonth++;
                            }
                        }
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+1).'3', 'Grand Total');
                        $objPHPExcel->getActiveSheet()
                                    ->getStyle('A3:'.MyFunctionCustom::columnName($keyMonth+1).'3')
                                    ->getFont()
                                    ->setBold(true);                        
                        $objPHPExcel->getActiveSheet()->getStyle('B3:'.MyFunctionCustom::columnName($keyMonth+1).'3')
                                    ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);                        
                        
                        $j=0; // is index of $dataAgent
                        $index = 1;
                        $rowNum = 4;
                        $sumCol = array();                     
                        foreach($dataAgent as $kAgent=>$itemAgent){
                            $sumRow=0;
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).($j+$rowNum), $itemAgent->first_name);
                            $keyMonth=1;
                            if(count($aMonth)>0){
                                foreach ($aMonth as $month=>$aObj){
                                    if(isset($dataExport[$year][$month][$itemAgent->id])){
                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+1).($j+$rowNum), $dataExport[$year][$month][$itemAgent->id]);
                                        $sumRow+=$dataExport[$year][$month][$itemAgent->id];
                                        if(isset($sumCol[$month]))
                                            $sumCol[$month]+=$dataExport[$year][$month][$itemAgent->id];
                                        else 
                                            $sumCol[$month]=$dataExport[$year][$month][$itemAgent->id];
                                    }
                                    $keyMonth++;
                                }
                            }
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum), $sumRow);
							$objPHPExcel->getActiveSheet()
                                    ->getStyle(MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum))
                                    ->getFont()
                                    ->setBold(true);  
                            $j++;
                        }
                        // begin write sum col  
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).($j+$rowNum), 'Grand Total');
                        $keyMonth=1;
                        $sumAll=0;
                        if(count($aMonth)>0){
                            foreach ($aMonth as $month=>$aObj){
                                if(isset($sumCol[$month])){
                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum), $sumCol[$month]);
                                    $sumAll+=$sumCol[$month];
                                }
                                $keyMonth++;
                            }
                        }                        
                        
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum), $sumAll);
                        
                        $objPHPExcel->getActiveSheet()
                                        ->getStyle(MyFunctionCustom::columnName($index).($j+$rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum))
                                        ->getFont()
                                        ->setBold(true);		
									
                        $objPHPExcel->getActiveSheet()
                        ->getStyle(MyFunctionCustom::columnName($index+1).($rowNum).':'.MyFunctionCustom::columnName($index+1+$keyMonth).($j+$rowNum) )->getNumberFormat()
                        ->setFormatCode('#,##0');               
                        $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).'3:'.MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum) )
                                                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			
                     $i++;   
                    } // ********** end for 1 sheet for($i=0;$i<count($model->costs_month);$i++){
                }    
                
		 //save file 
		 $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		 //$objWriter->save('MyExcel.xslx');

		 for($level=ob_get_level();$level>0;--$level)
		 {
			 @ob_end_clean();
		 }
		 header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		 header('Pragma: public');
		 header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		 header('Content-Disposition: attachment; filename="'.'Report Monthly Revenue-'.date('d-m-Y').'.'.'xlsx'.'"');
		 header('Cache-Control: max-age=0');				
		 $objWriter->save('php://output');			
		 Yii::app()->end();	
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }                       
      }
	  
    public static function export_agent_price_root($dataExport,$dataAgent){
    try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
         // Set properties
         $objPHPExcel->getProperties()->setCreator("NguyenDung")
                    ->setLastModifiedBy("NguyenDung")
                    ->setTitle('Report Monthly Price Root')
                    ->setSubject("Office 2007 XLSX Document")
                    ->setDescription("Report Monthly Price Root")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("GAS");
        // step 0: Tạo ra các sheet
        $i=0;
        if(count($dataExport)>0){
             $objPHPExcel->removeSheetByIndex(0); // may be need
            foreach($dataExport as $year=>$aMonth){
                $objPHPExcel->createSheet();
                $objPHPExcel->setActiveSheetIndex($i);
                // set default font and font-size
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
                $objPHPExcel->getActiveSheet()->setTitle('NĂM '.$year);
                $objPHPExcel->getActiveSheet()->setCellValue("A1", 'BÁO CÁO GIÁ VỐN THEO THÁNG CỦA NĂM '.$year);
                $objPHPExcel->getActiveSheet()->mergeCells("A1:D1");

                $objPHPExcel->getActiveSheet()->setCellValue("A3", 'Đại Lý');
                $keyMonth=1;
                if(count($aMonth)>0){
                    foreach ($aMonth as $month=>$aObj){
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+1).'3', $month);
                        $keyMonth++;
                    }
                }
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+1).'3', 'Grand Total');
                $objPHPExcel->getActiveSheet()
                            ->getStyle('A3:'.MyFunctionCustom::columnName($keyMonth+1).'3')
                            ->getFont()
                            ->setBold(true);                        
                $objPHPExcel->getActiveSheet()->getStyle('B3:'.MyFunctionCustom::columnName($keyMonth+1).'3')
                            ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);                        

                $j=0; // is index of $dataAgent
                $index = 1;
                $rowNum = 4;
                $sumCol = array();                     
                foreach($dataAgent as $kAgent=>$itemAgent){
                    $sumRow=0;
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).($j+$rowNum), $itemAgent->first_name);
                    $keyMonth=1;
                    if(count($aMonth)>0){
                        foreach ($aMonth as $month=>$aObj){
                            if(isset($dataExport[$year][$month][$itemAgent->id])){
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+1).($j+$rowNum), $dataExport[$year][$month][$itemAgent->id]);
                                $sumRow+=$dataExport[$year][$month][$itemAgent->id];
                                if(isset($sumCol[$month]))
                                    $sumCol[$month]+=$dataExport[$year][$month][$itemAgent->id];
                                else 
                                    $sumCol[$month]=$dataExport[$year][$month][$itemAgent->id];
                            }
                            $keyMonth++;
                        }
                    }
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum), $sumRow);
                    $j++;
                }
                // begin write sum col  
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).($j+$rowNum), 'Grand Total');
                $keyMonth=1;
                $sumAll=0;
                if(count($aMonth)>0){
                    foreach ($aMonth as $month=>$aObj){
                        if(isset($sumCol[$month])){
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum), $sumCol[$month]);
                            $sumAll+=$sumCol[$month];
                        }
                        $keyMonth++;
                    }
                }                        

                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum), $sumAll);

                $objPHPExcel->getActiveSheet()
                                ->getStyle(MyFunctionCustom::columnName($index).($j+$rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum))
                                ->getFont()
                                ->setBold(true);		

                $objPHPExcel->getActiveSheet()
                ->getStyle(MyFunctionCustom::columnName($index+1).($rowNum).':'.MyFunctionCustom::columnName($index+1+$keyMonth).($j+$rowNum) )->getNumberFormat()
                ->setFormatCode('#,##0');               
                $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).'3:'.MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum) )
                                                ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

             $i++;   
            } // ********** end for 1 sheet for($i=0;$i<count($model->costs_month);$i++){
        }    

         //save file 
         $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
         //$objWriter->save('MyExcel.xslx');

         for($level=ob_get_level();$level>0;--$level)
         {
                 @ob_end_clean();
         }
         header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
         header('Pragma: public');
         header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment; filename="'.'Report Monthly Price Root-'.date('d-m-Y').'.'.'xlsx'.'"');
         header('Cache-Control: max-age=0');				
         $objWriter->save('php://output');			
         Yii::app()->end();         
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }               
    }
	  
    public static function export_agent_cost($dataExport,$dataAgent){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
         // Set properties
         $objPHPExcel->getProperties()->setCreator("NguyenDung")
                    ->setLastModifiedBy("NguyenDung")
                    ->setTitle('Report Monthly Cost')
                    ->setSubject("Office 2007 XLSX Document")
                    ->setDescription("Report Monthly Cost")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("GAS");
        // step 0: Tạo ra các sheet
        $i=0;
        if(count($dataExport)>0){
             $objPHPExcel->removeSheetByIndex(0); // may be need
            foreach($dataExport as $year=>$aMonth){
                $objPHPExcel->createSheet();
                $objPHPExcel->setActiveSheetIndex($i);
                // set default font and font-size
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
                $objPHPExcel->getActiveSheet()->setTitle('NĂM '.$year);
                $objPHPExcel->getActiveSheet()->setCellValue("A1", 'BÁO CÁO CHI PHÍ NĂM THEO ĐẠI LÝ CỦA NĂM '.$year);
                $objPHPExcel->getActiveSheet()->mergeCells("A1:D1");

                $objPHPExcel->getActiveSheet()->setCellValue("A3", 'Đại Lý');
                $keyMonth=1;
                if(count($aMonth)>0){
                    foreach ($aMonth as $month=>$aObj){
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+1).'3', $month);
                        $keyMonth++;
                    }
                }
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+1).'3', 'Grand Total');
                $objPHPExcel->getActiveSheet()
                            ->getStyle('A3:'.MyFunctionCustom::columnName($keyMonth+1).'3')
                            ->getFont()
                            ->setBold(true);                        
                $objPHPExcel->getActiveSheet()->getStyle('B3:'.MyFunctionCustom::columnName($keyMonth+1).'3')
                            ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);                        

                $j=0; // is index of $dataAgent
                $index = 1;
                $rowNum = 4;
                $sumCol = array();                     
                foreach($dataAgent as $kAgent=>$itemAgent){
                    $sumRow=0;
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).($j+$rowNum), $itemAgent->first_name);
                    $keyMonth=1;
                    if(count($aMonth)>0){
                        foreach ($aMonth as $month=>$aObj){
                            if(isset($dataExport[$year][$month][$itemAgent->id])){
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+1).($j+$rowNum), $dataExport[$year][$month][$itemAgent->id]);
                                $sumRow+=$dataExport[$year][$month][$itemAgent->id];
                                if(isset($sumCol[$month]))
                                    $sumCol[$month]+=$dataExport[$year][$month][$itemAgent->id];
                                else 
                                    $sumCol[$month]=$dataExport[$year][$month][$itemAgent->id];
                            }
                            $keyMonth++;
                        }
                    }
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum), $sumRow);
                                                $objPHPExcel->getActiveSheet()
                            ->getStyle(MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum))
                            ->getFont()
                            ->setBold(true);    
                    $j++;
                }
                // begin write sum col  
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).($j+$rowNum), 'Grand Total');
                $keyMonth=1;
                $sumAll=0;
                if(count($aMonth)>0){
                    foreach ($aMonth as $month=>$aObj){
                        if(isset($sumCol[$month])){
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum), $sumCol[$month]);
                            $sumAll+=$sumCol[$month];
                        }
                        $keyMonth++;
                    }
                }                        

                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum), $sumAll);

                $objPHPExcel->getActiveSheet()
                                ->getStyle(MyFunctionCustom::columnName($index).($j+$rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum))
                                ->getFont()
                                ->setBold(true);		

                $objPHPExcel->getActiveSheet()
                ->getStyle(MyFunctionCustom::columnName($index+1).($rowNum).':'.MyFunctionCustom::columnName($index+1+$keyMonth).($j+$rowNum) )->getNumberFormat()
                ->setFormatCode('#,##0');               
                $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).'3:'.MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum) )
                                                ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

             $i++;   
            } // ********** end for 1 sheet for($i=0;$i<count($model->costs_month);$i++){
        }    

         //save file 
         $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
         //$objWriter->save('MyExcel.xslx');

         for($level=ob_get_level();$level>0;--$level)
         {
                 @ob_end_clean();
         }
         header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
         header('Pragma: public');
         header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment; filename="'.'Report Monthly Cost-'.date('d-m-Y').'.'.'xlsx'.'"');
         header('Cache-Control: max-age=0');				
         $objWriter->save('php://output');			
         Yii::app()->end();		          
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }               
      }
	  	  	  
    public static function export_cost_sum_total($dataExport,$aCost){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
         // Set properties
         $objPHPExcel->getProperties()->setCreator("NguyenDung")
                    ->setLastModifiedBy("NguyenDung")
                    ->setTitle('Report Cost Sum Total')
                    ->setSubject("Office 2007 XLSX Document")
                    ->setDescription("Report Cost Sum Total")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("GAS");
        // step 0: Tạo ra các sheet
        $i=0;
        if(count($dataExport)>0){
             $objPHPExcel->removeSheetByIndex(0); // may be need
            foreach($dataExport as $year=>$aMonth){
                $objPHPExcel->createSheet();
                $objPHPExcel->setActiveSheetIndex($i);
                // set default font and font-size
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
                $objPHPExcel->getActiveSheet()->setTitle('NĂM '.$year);
                $objPHPExcel->getActiveSheet()->setCellValue("A1", 'BÁO CÁO TỔNG CHI PHÍ CỦA NĂM '.$year);
                $objPHPExcel->getActiveSheet()->mergeCells("A1:D1");

                $objPHPExcel->getActiveSheet()->setCellValue("A3", 'Chi Phí');
                $keyMonth=1;
                                        // begin render header month
                if(count($aMonth)>0){
                    foreach ($aMonth as $month=>$aObj){
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+1).'3', $month);
                        $keyMonth++;
                    }
                }
                                        // begin render Grand Total
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+1).'3', 'Grand Total');
                $objPHPExcel->getActiveSheet()
                            ->getStyle('A3:'.MyFunctionCustom::columnName($keyMonth+1).'3')
                            ->getFont()
                            ->setBold(true);                        
                $objPHPExcel->getActiveSheet()->getStyle('B3:'.MyFunctionCustom::columnName($keyMonth+1).'3')
                            ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);                        

                $j=0; // is index of $aCost
                $index = 1;
                $rowNum = 4;
                $sumCol = array();
                                        // begin render body data					
                foreach($aCost as $kCostId=>$objCost){
                    $sumRow=0;
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).($j+$rowNum), $objCost->name);
                    $keyMonth=1;
                                                $flagAddRow=false;
                    if(count($aMonth)>0){
                        foreach ($aMonth as $month=>$aObj){
                            if(isset($dataExport[$year][$month][$objCost->id])){
                                                                        $flagAddRow=true;
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+1).($j+$rowNum), $dataExport[$year][$month][$objCost->id]);
                                $sumRow+=$dataExport[$year][$month][$objCost->id];
                                if(isset($sumCol[$month]))
                                    $sumCol[$month]+=$dataExport[$year][$month][$objCost->id];
                                else 
                                    $sumCol[$month]=$dataExport[$year][$month][$objCost->id];
                            }
                            $keyMonth++;
                        }
                    }
                                                if($flagAddRow){
                                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum), $sumRow);
                                                        $objPHPExcel->getActiveSheet()
                            ->getStyle(MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum))
                            ->getFont()
                            ->setBold(true);    
                                                        $j++;
                                                }

                }
                // begin write sum col  
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).($j+$rowNum), 'Grand Total');
                $keyMonth=1;
                $sumAll=0;
                if(count($aMonth)>0){
                    foreach ($aMonth as $month=>$aObj){
                        if(isset($sumCol[$month])){
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum), $sumCol[$month]);
                            $sumAll+=$sumCol[$month];
                        }
                        $keyMonth++;
                    }
                }                        

                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum), $sumAll);

                $objPHPExcel->getActiveSheet()
                                ->getStyle(MyFunctionCustom::columnName($index).($j+$rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum))
                                ->getFont()
                                ->setBold(true);		

                $objPHPExcel->getActiveSheet()
                ->getStyle(MyFunctionCustom::columnName($index+1).($rowNum).':'.MyFunctionCustom::columnName($index+1+$keyMonth).($j+$rowNum) )->getNumberFormat()
                ->setFormatCode('#,##0');               
                $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).'3:'.MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum) )
                                                ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

             $i++;   
            } // ********** end for 1 sheet for($i=0;$i<count($model->costs_month);$i++){
        }    

         //save file 
         $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
         //$objWriter->save('MyExcel.xslx');

         for($level=ob_get_level();$level>0;--$level)
         {
                 @ob_end_clean();
         }
         header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
         header('Pragma: public');
         header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment; filename="'.'Report Cost Sum Total-'.date('d-m-Y').'.'.'xlsx'.'"');
         header('Cache-Control: max-age=0');				
         $objWriter->save('php://output');			
         Yii::app()->end();	
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }               
      }
	  	  
	   
    public static function export_agent_gross_profit($dataExportRevenue, $dataExportPriceRoot, $dataAgent){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
         // Set properties
         $objPHPExcel->getProperties()->setCreator("NguyenDung")
                    ->setLastModifiedBy("NguyenDung")
                    ->setTitle('Report Monthly Gross Profit')
                    ->setSubject("Office 2007 XLSX Document")
                    ->setDescription("Report Monthly Gross Profit")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("GAS");
        // step 0: Tạo ra các sheet
        $i=0;
        if(count($dataExportRevenue)>0){
             $objPHPExcel->removeSheetByIndex(0); // may be need
            foreach($dataExportRevenue as $year=>$aMonth){
                $objPHPExcel->createSheet();
                $objPHPExcel->setActiveSheetIndex($i);
                // set default font and font-size
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
                $objPHPExcel->getActiveSheet()->setTitle('NĂM '.$year);
                $objPHPExcel->getActiveSheet()->setCellValue("A1", 'BÁO CÁO LÃI GỘP THEO THÁNG CỦA NĂM '.$year);
                $objPHPExcel->getActiveSheet()->mergeCells("A1:D1");

                $objPHPExcel->getActiveSheet()->setCellValue("A3", 'Đại Lý');
                $keyMonth=1;
                if(count($aMonth)>0){
                    foreach ($aMonth as $month=>$aObj){
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+1).'3', $month);
                        $keyMonth++;
                    }
                }
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+1).'3', 'Grand Total');
                $objPHPExcel->getActiveSheet()
                            ->getStyle('A3:'.MyFunctionCustom::columnName($keyMonth+1).'3')
                            ->getFont()
                            ->setBold(true);                        
                $objPHPExcel->getActiveSheet()->getStyle('B3:'.MyFunctionCustom::columnName($keyMonth+1).'3')
                            ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);                        

                $j=0; // is index of $dataAgent
                $index = 1;
                $rowNum = 4;
                $sumCol = array();                     
                foreach($dataAgent as $kAgent=>$itemAgent){
                    $sumRow=0;
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).($j+$rowNum), $itemAgent->first_name);
                    $keyMonth=1;
                    if(count($aMonth)>0){
                        foreach ($aMonth as $month=>$aObj){
                                                                $Revenue = isset($dataExportRevenue[$year][$month][$itemAgent->id])?$dataExportRevenue[$year][$month][$itemAgent->id]:0;
                                                                $PriceRoot = isset($dataExportPriceRoot[$year][$month][$itemAgent->id])?$dataExportPriceRoot[$year][$month][$itemAgent->id]:0;
                            $cellValue = $Revenue-$PriceRoot;
                                                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+1).($j+$rowNum), $cellValue);
                                                                $sumRow+=$cellValue;
                                                                if(isset($sumCol[$month]))
                                                                        $sumCol[$month]+=$cellValue;
                                                                else 
                                                                        $sumCol[$month]=$cellValue;

                            $keyMonth++;
                        }
                    }
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum), $sumRow);
                    $j++;
                }
                // begin write sum col  
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).($j+$rowNum), 'Grand Total');
                $keyMonth=1;
                $sumAll=0;
                if(count($aMonth)>0){
                    foreach ($aMonth as $month=>$aObj){
                        if(isset($sumCol[$month])){
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum), $sumCol[$month]);
                            $sumAll+=$sumCol[$month];
                        }
                        $keyMonth++;
                    }
                }                        

                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum), $sumAll);

                $objPHPExcel->getActiveSheet()
                                ->getStyle(MyFunctionCustom::columnName($index).($j+$rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum))
                                ->getFont()
                                ->setBold(true);		

                $objPHPExcel->getActiveSheet()
                ->getStyle(MyFunctionCustom::columnName($index+1).($rowNum).':'.MyFunctionCustom::columnName($index+1+$keyMonth).($j+$rowNum) )->getNumberFormat()
                ->setFormatCode('#,##0');               
                $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).'3:'.MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum) )
                                                ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

             $i++;   
            } // ********** end for 1 sheet for($i=0;$i<count($model->costs_month);$i++){
        }    

         //save file 
         $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
         //$objWriter->save('MyExcel.xslx');

         for($level=ob_get_level();$level>0;--$level)
         {
                 @ob_end_clean();
         }
         header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
         header('Pragma: public');
         header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment; filename="'.'Report Monthly Gross Profit-'.date('d-m-Y').'.'.'xlsx'.'"');
         header('Cache-Control: max-age=0');				
         $objWriter->save('php://output');			
         Yii::app()->end();	
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }              
      }
	  	  
	  
    public static function export_agent_net_profit($dataExportRevenue, $dataExportPriceRoot, $dataExportCost, $dataAgent){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
         // Set properties
         $objPHPExcel->getProperties()->setCreator("NguyenDung")
                    ->setLastModifiedBy("NguyenDung")
                    ->setTitle('Report Monthly Net Profit')
                    ->setSubject("Office 2007 XLSX Document")
                    ->setDescription("Report Monthly Net Profit")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("GAS");
        // step 0: Tạo ra các sheet
        $i=0;
        if(count($dataExportRevenue)>0){
             $objPHPExcel->removeSheetByIndex(0); // may be need
            foreach($dataExportRevenue as $year=>$aMonth){
                $objPHPExcel->createSheet();
                $objPHPExcel->setActiveSheetIndex($i);
                //  
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
                $objPHPExcel->getActiveSheet()->setTitle('NĂM '.$year);
                $objPHPExcel->getActiveSheet()->setCellValue("A1", 'BÁO CÁO LỢI NHUẬN THUẦN THEO THÁNG CỦA NĂM '.$year);
                $objPHPExcel->getActiveSheet()->mergeCells("A1:D1");

                $objPHPExcel->getActiveSheet()->setCellValue("A3", 'Đại Lý');
                $keyMonth=1;
                if(count($aMonth)>0){
                    foreach ($aMonth as $month=>$aObj){
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+1).'3', $month);
                        $keyMonth++;
                    }
                }
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+1).'3', 'Grand Total');
                $objPHPExcel->getActiveSheet()
                            ->getStyle('A3:'.MyFunctionCustom::columnName($keyMonth+1).'3')
                            ->getFont()
                            ->setBold(true);                        
                $objPHPExcel->getActiveSheet()->getStyle('B3:'.MyFunctionCustom::columnName($keyMonth+1).'3')
                            ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);                        

                $j=0; // is index of $dataAgent
                $index = 1;
                $rowNum = 4;
                $sumCol = array();      

                foreach($dataAgent as $kAgent=>$itemAgent){
                    $sumRow=0;
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).($j+$rowNum), $itemAgent->first_name);
                    $keyMonth=1;
                    if(count($aMonth)>0){
                        foreach ($aMonth as $month=>$aObj){
                                                                $Revenue = isset($dataExportRevenue[$year][$month][$itemAgent->id])?$dataExportRevenue[$year][$month][$itemAgent->id]:0;
                                                                $PriceRoot = isset($dataExportPriceRoot[$year][$month][$itemAgent->id])?$dataExportPriceRoot[$year][$month][$itemAgent->id]:0;
                                                                $Cost = isset($dataExportCost[$year][$month][$itemAgent->id])?$dataExportCost[$year][$month][$itemAgent->id]:0;                                    
                            $cellValue = $Revenue-$PriceRoot-$Cost;									
                                                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+1).($j+$rowNum), $cellValue);
                                                                $sumRow+=$cellValue;
                                                                if(isset($sumCol[$month]))
                                                                        $sumCol[$month]+=$cellValue;
                                                                else 
                                                                        $sumCol[$month]=$cellValue;

                            $keyMonth++;
                        }
                    }
                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum), $sumRow);
                    $j++;
                }

                // begin write sum col  
                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).($j+$rowNum), 'Grand Total');
                $keyMonth=1;
                $sumAll=0;
                if(count($aMonth)>0){
                    foreach ($aMonth as $month=>$aObj){
                        if(isset($sumCol[$month])){
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum), $sumCol[$month]);
                            $sumAll+=$sumCol[$month];
                        }
                        $keyMonth++;
                    }
                }                        

                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum), $sumAll);

                $objPHPExcel->getActiveSheet()
                                ->getStyle(MyFunctionCustom::columnName($index).($j+$rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum))
                                ->getFont()
                                ->setBold(true);		

                $objPHPExcel->getActiveSheet()
                ->getStyle(MyFunctionCustom::columnName($index+1).($rowNum).':'.MyFunctionCustom::columnName($index+1+$keyMonth).($j+$rowNum) )->getNumberFormat()
                ->setFormatCode('#,##0');               
                $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($index).'3:'.MyFunctionCustom::columnName($index+$keyMonth).($j+$rowNum) )
                                                ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

             $i++;   
            } // ********** end for 1 sheet for($i=0;$i<count($model->costs_month);$i++){
        }    

         //save file 
         $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
         //$objWriter->save('MyExcel.xslx');

         for($level=ob_get_level();$level>0;--$level)
         {
                 @ob_end_clean();
         }
         header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
         header('Pragma: public');
         header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment; filename="'.'Report Monthly Net Profit-'.date('d-m-Y').'.'.'xlsx'.'"');
         header('Cache-Control: max-age=0');				
         $objWriter->save('php://output');			
         Yii::app()->end();		          
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }          
      }
	  	  	  	  
    public static function export_agent_revenue_by_material($dataExport,$dataAgent, $dataAllMaterial){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
         // Set properties
         $objPHPExcel->getProperties()->setCreator("NguyenDung")
                    ->setLastModifiedBy("NguyenDung")
                    ->setTitle('Report Monthly Revenue')
                    ->setSubject("Office 2007 XLSX Document")
                    ->setDescription("Report Monthly Revenue")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("GAS");
        // step 0: Tạo ra các sheet
        $i=0;
        if(count($dataExport)>0){
             $objPHPExcel->removeSheetByIndex(0); // may be need
//                    for($i;$i<count($dataExport);$i++){
            foreach($dataExport as $year=>$aAgentId){
                $objPHPExcel->createSheet();
                $objPHPExcel->setActiveSheetIndex($i);
                // set default font and font-size
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
                $objPHPExcel->getActiveSheet()->setTitle('NĂM '.$year);
                $objPHPExcel->getActiveSheet()->setCellValue("A1", 'BÁO CÁO DOANH THU ĐẠI LÝ THEO SẢN PHẨM NĂM '.$year);
                $objPHPExcel->getActiveSheet()->mergeCells("A1:D1");

                $objPHPExcel->getActiveSheet()->setCellValue("A3", 'Đại Lý');
                $objPHPExcel->getActiveSheet()->setCellValue("B3", 'Sản Phẩm');
                $key_agent_id=0;
                $rowNum = 3;
                $index = 1;

                foreach($aAgentId as $agent_id=>$month_obj){		
                    if(isset($dataExport[$year][$agent_id]['arrMaterialId'])){
                            // render heaer of one agent
                            $keyMonth=1;
                            $sumCol = array();
                            foreach($dataExport[$year][$agent_id]['month'] as $month){
                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, 'T'.$month);											
                                    $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($keyMonth+2).$rowNum)
                                    ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                                    $objPHPExcel->getActiveSheet()
                                                    ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
                                                    ->getFont()
                                                    ->setBold(true);								
                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+3).$rowNum, 'Grand Total');
                                    $keyMonth++;
                            }
                            $rowNum++;
                            // end render heaer of one agent
                            $cellBeginAgent=MyFunctionCustom::columnName($index+2).$rowNum;
                            $cellBeginMergeAgent=MyFunctionCustom::columnName($index).$rowNum;
                            $cellBeginBorderAgent=MyFunctionCustom::columnName($index).($rowNum-1);

                                foreach($dataExport[$year][$agent_id]['arrMaterialId'] as $keyMaterial=>$material_id){
                                        $keyMonth=1;
                                        $sumRow = 0;
                                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).$rowNum, $dataAllMaterial[$material_id]->name);


                                                foreach($dataExport[$year][$agent_id]['month'] as $month){									
                                                        if($keyMaterial==0){
                                                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).$rowNum, $dataAgent[$agent_id]->first_name);
                                                        }

                                                        if(isset($dataExport[$year][$agent_id]['month_obj'][$month][$material_id])){
                                                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, 
                                                                        $dataExport[$year][$agent_id]['month_obj'][$month][$material_id]['parent_total']); 
                                                                $sumRow+=	$dataExport[$year][$agent_id]['month_obj'][$month][$material_id]['parent_total'];												
                                                                if(isset($sumCol[$month]))
                                                                        $sumCol[$month]+=$dataExport[$year][$agent_id]['month_obj'][$month][$material_id]['parent_total'];			
                                                                else
                                                                        $sumCol[$month]=$dataExport[$year][$agent_id]['month_obj'][$month][$material_id]['parent_total'];


                                                        }
                                                        $objPHPExcel->getActiveSheet()
                                                                ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
                                                                ->getFont()
                                                                ->setBold(true);										
                                                        $keyMonth++;
                                                } // end  foreach($dataExport[$year][$itemAgent->id] as $aMonth){
                                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumRow);
                                                $rowNum++;
                                                // for detail of one parent id
                                                $key_of_month_out=0;
                                                        foreach($dataExport[$year][$agent_id]['month'] as $month){
                                                                if(isset($dataExport[$year][$agent_id]['month_obj'][$month][$material_id])){
                                                                        if($key_of_month_out!=0)
                                                                            $rowNum-=(count($dataExport[$year][$agent_id]['month_obj'][$month][$material_id]['sub_obj']));                                                                            
                                                                        foreach($dataExport[$year][$agent_id]['month_obj'][$month][$material_id]['sub_obj'] as $keySub=>$subObj){
                                                                                $keyMonth2=1;
                                                                                foreach($dataExport[$year][$agent_id]['month'] as $month2){	
                                                                                        if($subObj->sell_month==$month2){
                                                                                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).$rowNum, $dataAllMaterial[$subObj->materials_id]->name);
                                                                                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth2+2).$rowNum, $subObj->total_sell);																							
                                                                                        }
                                                                                        $keyMonth2++;
                                                                                }
                                                                                $rowNum++;	
                                                                        }
                                                                }
                                                                $key_of_month_out++;
                                                        }
                                                // end for detail of one parent id

                                } // end foreach($dataExport[$year][$agent_id]['arrMaterialId'] as $keyMaterial=>$material_id){	

                                $objPHPExcel->getActiveSheet()
                                    ->mergeCells($cellBeginMergeAgent.':'.MyFunctionCustom::columnName($index).($rowNum-1));
                                $objPHPExcel->getActiveSheet()->getStyle($cellBeginMergeAgent.':'.MyFunctionCustom::columnName($index).($rowNum))
                                    ->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


                            // total col
                            $keyMonth=1;
                            $sumOneAgent=0;
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth).$rowNum,  'Grand Total');
                            foreach($dataExport[$year][$agent_id]['month'] as $month){
                                    if(isset($sumCol[$month])){
                                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumCol[$month]);
                                            $sumOneAgent+=$sumCol[$month];	
                                    }
                                    $objPHPExcel->getActiveSheet()
                                                    ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
                                                    ->getFont()
                                                    ->setBold(true);											
                                    $keyMonth++;
                            }
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumOneAgent);

                            // end total col
                             $objPHPExcel->getActiveSheet()
                                    ->getStyle($cellBeginAgent.':'.MyFunctionCustom::columnName($keyMonth+2).$rowNum)->getNumberFormat()
                                    ->setFormatCode('#,##0'); 


                            $objPHPExcel->getActiveSheet()->getStyle($cellBeginBorderAgent.':'.MyFunctionCustom::columnName($keyMonth+2).$rowNum )
                                    ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);								

                            $rowNum++;	
                        }
                    $key_agent_id++;

                } // end foreach($dataAgent as $kAgent=>$itemAgent){



             $i++;   // for createSheet(
            } // ********** end for 1 sheet for($i=0;$i<count($model->costs_month);$i++){
        }    

         //save file 
         $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
         //$objWriter->save('MyExcel.xslx');

         for($level=ob_get_level();$level>0;--$level)
         {
                 @ob_end_clean();
         }
         header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
         header('Pragma: public');
         header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment; filename="'.'Report Monthly Revenue-'.date('d-m-Y').'.'.'xlsx'.'"');
         header('Cache-Control: max-age=0');				
         $objWriter->save('php://output');			
         Yii::app()->end();		          
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }              
      }
    
	  
    public static function export_agent_price_root_by_material($dataExport,$dataAgent, $dataAllMaterial){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
         // Set properties
         $objPHPExcel->getProperties()->setCreator("NguyenDung")
                    ->setLastModifiedBy("NguyenDung")
                    ->setTitle('Report Monthly Price Root')
                    ->setSubject("Office 2007 XLSX Document")
                    ->setDescription("Report Monthly Price Root")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("GAS");
        // step 0: Tạo ra các sheet
        $i=0;
        if(count($dataExport)>0){
             $objPHPExcel->removeSheetByIndex(0); // may be need
//                    for($i;$i<count($dataExport);$i++){
            foreach($dataExport as $year=>$aAgentId){
                $objPHPExcel->createSheet();
                $objPHPExcel->setActiveSheetIndex($i);
                // set default font and font-size
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
                $objPHPExcel->getActiveSheet()->setTitle('NĂM '.$year);
                $objPHPExcel->getActiveSheet()->setCellValue("A1", 'BÁO CÁO GIÁ VỐN ĐẠI LÝ THEO SẢN PHẨM NĂM '.$year);
                $objPHPExcel->getActiveSheet()->mergeCells("A1:D1");

                $objPHPExcel->getActiveSheet()->setCellValue("A3", 'Đại Lý');
                $objPHPExcel->getActiveSheet()->setCellValue("B3", 'Sản Phẩm');
                $key_agent_id=0;
                $rowNum = 3;
                $index = 1;

                foreach($aAgentId as $agent_id=>$month_obj){		
                    if(isset($dataExport[$year][$agent_id]['arrMaterialId'])){
                            // render heaer of one agent
                            $keyMonth=1;
                            $sumCol = array();
                            foreach($dataExport[$year][$agent_id]['month'] as $month){
                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, 'T'.$month);											
                                    $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($keyMonth+2).$rowNum)
                                    ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                                    $objPHPExcel->getActiveSheet()
                                                    ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
                                                    ->getFont()
                                                    ->setBold(true);								
                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+3).$rowNum, 'Grand Total');
                                    $keyMonth++;
                            }
                            $rowNum++;
                            // end render heaer of one agent
                            $cellBeginAgent=MyFunctionCustom::columnName($index+2).$rowNum;
                            $cellBeginMergeAgent=MyFunctionCustom::columnName($index).$rowNum;
                            $cellBeginBorderAgent=MyFunctionCustom::columnName($index).($rowNum-1);

                                foreach($dataExport[$year][$agent_id]['arrMaterialId'] as $keyMaterial=>$material_id){
                                        $keyMonth=1;
                                        $sumRow = 0;
                                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).$rowNum, $dataAllMaterial[$material_id]->name);


                                                foreach($dataExport[$year][$agent_id]['month'] as $month){									
                                                        if($keyMaterial==0){
                                                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).$rowNum, $dataAgent[$agent_id]->first_name);
                                                        }

                                                        if(isset($dataExport[$year][$agent_id]['month_obj'][$month][$material_id])){
                                                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, 
                                                                        $dataExport[$year][$agent_id]['month_obj'][$month][$material_id]['parent_total']); 
                                                                $sumRow+=	$dataExport[$year][$agent_id]['month_obj'][$month][$material_id]['parent_total'];												
                                                                if(isset($sumCol[$month]))
                                                                        $sumCol[$month]+=$dataExport[$year][$agent_id]['month_obj'][$month][$material_id]['parent_total'];			
                                                                else
                                                                        $sumCol[$month]=$dataExport[$year][$agent_id]['month_obj'][$month][$material_id]['parent_total'];


                                                        }
                                                        $objPHPExcel->getActiveSheet()
                                                                ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
                                                                ->getFont()
                                                                ->setBold(true);										
                                                        $keyMonth++;
                                                } // end  foreach($dataExport[$year][$itemAgent->id] as $aMonth){
                                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumRow);
                                                $rowNum++;
                                                // for detail of one parent id
                                                        $key_of_month_out=0;
                                                        foreach($dataExport[$year][$agent_id]['month'] as $month){	
                                                                if(isset($dataExport[$year][$agent_id]['month_obj'][$month][$material_id])){
                                                                        if($key_of_month_out!=0)
                                                                            $rowNum-=(count($dataExport[$year][$agent_id]['month_obj'][$month][$material_id]['sub_obj']));
                                                                        foreach($dataExport[$year][$agent_id]['month_obj'][$month][$material_id]['sub_obj'] as $keySub=>$subObj){
                                                                                $keyMonth2=1;
                                                                                foreach($dataExport[$year][$agent_id]['month'] as $month2){	
                                                                                        if($subObj->sell_month==$month2){
                                                                                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).$rowNum, $dataAllMaterial[$subObj->materials_id]->name);
                                                                                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth2+2).$rowNum, $subObj->total_root);																							
                                                                                        }
                                                                                        $keyMonth2++;
                                                                                }
                                                                                $rowNum++;	
                                                                        }
                                                                }
                                                        $key_of_month_out++;
                                                        }
                                                // end for detail of one parent id

                                } // end foreach($dataExport[$year][$agent_id]['arrMaterialId'] as $keyMaterial=>$material_id){	

                                $objPHPExcel->getActiveSheet()
                                    ->mergeCells($cellBeginMergeAgent.':'.MyFunctionCustom::columnName($index).($rowNum-1));
                                $objPHPExcel->getActiveSheet()->getStyle($cellBeginMergeAgent.':'.MyFunctionCustom::columnName($index).($rowNum))
                                    ->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


                            // total col
                            $keyMonth=1;
                            $sumOneAgent=0;
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth).$rowNum,  'Grand Total');
                            foreach($dataExport[$year][$agent_id]['month'] as $month){
                                    if(isset($sumCol[$month])){
                                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumCol[$month]);
                                            $sumOneAgent+=$sumCol[$month];	
                                    }
                                    $objPHPExcel->getActiveSheet()
                                                    ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
                                                    ->getFont()
                                                    ->setBold(true);											
                                    $keyMonth++;
                            }
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumOneAgent);

                            // end total col
                             $objPHPExcel->getActiveSheet()
                                    ->getStyle($cellBeginAgent.':'.MyFunctionCustom::columnName($keyMonth+2).$rowNum)->getNumberFormat()
                                    ->setFormatCode('#,##0'); 


                            $objPHPExcel->getActiveSheet()->getStyle($cellBeginBorderAgent.':'.MyFunctionCustom::columnName($keyMonth+2).$rowNum )
                                    ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);								

                            $rowNum++;	
                        }
                    $key_agent_id++;

                } // end foreach($dataAgent as $kAgent=>$itemAgent){



             $i++;   // for createSheet(
            } // ********** end for 1 sheet for($i=0;$i<count($model->costs_month);$i++){
        }    

         //save file 
         $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
         //$objWriter->save('MyExcel.xslx');

         for($level=ob_get_level();$level>0;--$level)
         {
                 @ob_end_clean();
         }
         header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
         header('Pragma: public');
         header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment; filename="'.'Report Monthly Price Root-'.date('d-m-Y').'.'.'xlsx'.'"');
         header('Cache-Control: max-age=0');				
         $objWriter->save('php://output');			
         Yii::app()->end();
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }              
      }
	  
	  
    public static function export_agent_by_costs_2($dataExport,$dataAgent, $aCost){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
         // Set properties
         $objPHPExcel->getProperties()->setCreator("NguyenDung")
                    ->setLastModifiedBy("NguyenDung")
                    ->setTitle('Report Monthly Agent Cost 2')
                    ->setSubject("Office 2007 XLSX Document")
                    ->setDescription("Report Monthly Agent Cost 2")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("GAS");
        // step 0: Tạo ra các sheet
        $i=0;
        if(count($dataExport)>0){
             $objPHPExcel->removeSheetByIndex(0); // may be need
//                    for($i;$i<count($dataExport);$i++){
            foreach($dataExport as $year=>$aAgentId){
                $objPHPExcel->createSheet();
                $objPHPExcel->setActiveSheetIndex($i);
                // set default font and font-size
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
                $objPHPExcel->getActiveSheet()->setTitle('NĂM '.$year);
                $objPHPExcel->getActiveSheet()->setCellValue("A1", 'BÁO CÁO CHI PHÍ ĐẠI LÝ NĂM '.$year);
                $objPHPExcel->getActiveSheet()->mergeCells("A1:D1");

                $objPHPExcel->getActiveSheet()->setCellValue("A3", 'Đại Lý');
                $objPHPExcel->getActiveSheet()->setCellValue("B3", 'Chi Phí');
                $key_agent_id=0;
                $rowNum = 3;
                $index = 1;

                foreach($aAgentId as $agent_id=>$month_){		

                            // render heaer of one agent
                            $keyMonth=1;
                            $sumCol = array();
                            foreach($dataExport[$year][$agent_id]['month'] as $month){
                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, 'T'.$month);											
                                    $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($keyMonth+2).$rowNum)
                                    ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                                    $objPHPExcel->getActiveSheet()
                                                    ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
                                                    ->getFont()
                                                    ->setBold(true);								
                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+3).$rowNum, 'Grand Total');
                                    $keyMonth++;
                            }
                            $rowNum++;
                            // end render heaer of one agent
                                                                // write name of agent
                                                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).$rowNum, $dataAgent[$agent_id]->first_name);

                            $cellBeginAgent=MyFunctionCustom::columnName($index+2).$rowNum;
                            $cellBeginMergeAgent=MyFunctionCustom::columnName($index).$rowNum;
                            $cellBeginBorderAgent=MyFunctionCustom::columnName($index).($rowNum-1);
                                                                        // begin render detail costs one agent
                                foreach($aCost as $cost_id=>$cost_obj){
                                                                                $keyMonth=1;
                                                                                $sumRow = 0;
                                                                                $flagAddRow=false;

                                                                                foreach($dataExport[$year][$agent_id]['month'] as $month){									
                                                                                                if(isset($dataExport[$year][$agent_id][$month][$cost_id])){
                                                                                                        $flagAddRow=true;
                                                                                                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, 
                                                                                                                                $dataExport[$year][$agent_id][$month][$cost_id]); 
                                                                                                                $sumRow+=	$dataExport[$year][$agent_id][$month][$cost_id];												
                                                                                                                if(isset($sumCol[$month]))
                                                                                                                                $sumCol[$month]+=$dataExport[$year][$agent_id][$month][$cost_id];			
                                                                                                                else
                                                                                                                                $sumCol[$month]=$dataExport[$year][$agent_id][$month][$cost_id];
                                                                                                }

                                                                                                $keyMonth++;
                                                                                } // end  foreach($dataExport[$year][$itemAgent->id] as $aMonth){
                                                                                if($flagAddRow){
                                                                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).($rowNum), $cost_obj->name);
                                                                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumRow);
                                                                                        // bold for sum row
                                                                                        $objPHPExcel->getActiveSheet()															
                                                                                                                ->getStyle(MyFunctionCustom::columnName($keyMonth+2).$rowNum)
                                                                                                                ->getFont()
                                                                                                                ->setBold(true);		 												
                                                                                        $rowNum++;

                                                                                }

                                } // end foreach($aCost as $cost_id=>$cost_obj){
                                                                        // begin render detail costs one agent

                                $objPHPExcel->getActiveSheet()
                                    ->mergeCells($cellBeginMergeAgent.':'.MyFunctionCustom::columnName($index).($rowNum-1));
                                $objPHPExcel->getActiveSheet()->getStyle($cellBeginMergeAgent.':'.MyFunctionCustom::columnName($index).($rowNum))
                                    ->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


                            // total col
                            $keyMonth=1;
                            $sumOneAgent=0;
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth).$rowNum,  'Grand Total');
                            foreach($dataExport[$year][$agent_id]['month'] as $month){
                                    if(isset($sumCol[$month])){
                                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumCol[$month]);
                                            $sumOneAgent+=$sumCol[$month];	
                                    }
                                    $objPHPExcel->getActiveSheet()
                                                    ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
                                                    ->getFont()
                                                    ->setBold(true);											
                                    $keyMonth++;
                            }
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumOneAgent);

                            // end total col
                             $objPHPExcel->getActiveSheet()
                                    ->getStyle($cellBeginAgent.':'.MyFunctionCustom::columnName($keyMonth+2).$rowNum)->getNumberFormat()
                                    ->setFormatCode('#,##0'); 


                            $objPHPExcel->getActiveSheet()->getStyle($cellBeginBorderAgent.':'.MyFunctionCustom::columnName($keyMonth+2).$rowNum )
                                    ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);								

                            $rowNum++;	

                    $key_agent_id++;

                } // end foreach($dataAgent as $kAgent=>$itemAgent){

             $i++;   // for createSheet(
            } // ********** end for 1 sheet for($i=0;$i<count($model->costs_month);$i++){
        }    

         //save file 
         $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
         //$objWriter->save('MyExcel.xslx');

         for($level=ob_get_level();$level>0;--$level)
         {
                 @ob_end_clean();
         }
         header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
         header('Pragma: public');
         header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment; filename="'.'Report Monthly Agent Cost 2-'.date('d-m-Y').'.'.'xlsx'.'"');
         header('Cache-Control: max-age=0');				
         $objWriter->save('php://output');			
         Yii::app()->end();         
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }              
      }
	  
    public static function export_agent_by_costs_3($dataExport,$dataAgent, $aCost){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
         // Set properties
         $objPHPExcel->getProperties()->setCreator("NguyenDung")
                    ->setLastModifiedBy("NguyenDung")
                    ->setTitle('Report Monthly Agent Cost 3')
                    ->setSubject("Office 2007 XLSX Document")
                    ->setDescription("Report Monthly Agent Cost 3")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("GAS");
        // step 0: Tạo ra các sheet
        $i=0;
        if(count($dataExport)>0){
             $objPHPExcel->removeSheetByIndex(0); // may be need
//                    for($i;$i<count($dataExport);$i++){
            foreach($dataExport as $year=>$aCostId){
                $objPHPExcel->createSheet();
                $objPHPExcel->setActiveSheetIndex($i);
                // set default font and font-size
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
                $objPHPExcel->getActiveSheet()->setTitle('NĂM '.$year);
                $objPHPExcel->getActiveSheet()->setCellValue("A1", 'BÁO CÁO CHI PHÍ ĐẠI LÝ NĂM '.$year);
                $objPHPExcel->getActiveSheet()->mergeCells("A1:D1");

                $objPHPExcel->getActiveSheet()->setCellValue("A3", 'Chi Phí');
                $objPHPExcel->getActiveSheet()->setCellValue("B3", 'Đại Lý');
                $key_agent_id=0;
                $rowNum = 3;
                $index = 1;

                foreach($aCostId as $cost_id=>$month_){		

                            // render heaer of one agent
                            $keyMonth=1;
                            $sumCol = array();
                            foreach($dataExport[$year][$cost_id]['month'] as $month){
                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, 'T'.$month);											
                                    $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($keyMonth+2).$rowNum)
                                    ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                                    $objPHPExcel->getActiveSheet()
                                                    ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
                                                    ->getFont()
                                                    ->setBold(true);								
                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+3).$rowNum, 'Grand Total');
                                    $keyMonth++;
                            }
                            $rowNum++;
                            // end render heaer of one agent
                                                                // write name of agent
                                                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).$rowNum, $aCost[$cost_id]->name);

                            $cellBeginAgent=MyFunctionCustom::columnName($index+2).$rowNum;
                            $cellBeginMergeAgent=MyFunctionCustom::columnName($index).$rowNum;
                            $cellBeginBorderAgent=MyFunctionCustom::columnName($index).($rowNum-1);
                                                                $flagMergeCell=false;
                                                                        // begin render detail costs one agent
                                foreach($dataAgent as $agent_id=>$agent_obj){
                                                                                $keyMonth=1;
                                                                                $sumRow = 0;
                                                                                $flagAddRow=false;

                                                                                foreach($dataExport[$year][$cost_id]['month'] as $month){									
                                                                                                if(isset($dataExport[$year][$cost_id][$month][$agent_id])){
                                                                                                        $flagAddRow=true;
                                                                                                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, 
                                                                                                                                $dataExport[$year][$cost_id][$month][$agent_id]); 
                                                                                                                $sumRow+=	$dataExport[$year][$cost_id][$month][$agent_id];												
                                                                                                                if(isset($sumCol[$month]))
                                                                                                                                $sumCol[$month]+=$dataExport[$year][$cost_id][$month][$agent_id];			
                                                                                                                else
                                                                                                                                $sumCol[$month]=$dataExport[$year][$cost_id][$month][$agent_id];
                                                                                                }

                                                                                                $keyMonth++;
                                                                                } // end  foreach($dataExport[$year][$itemAgent->id] as $aMonth){
                                                                                if($flagAddRow){
                                                                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).($rowNum), $agent_obj->first_name);
                                                                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumRow);
                                                                                        // bold for sum row
                                                                                        $objPHPExcel->getActiveSheet()															
                                                                                                                ->getStyle(MyFunctionCustom::columnName($keyMonth+2).$rowNum)
                                                                                                                ->getFont()
                                                                                                                ->setBold(true);		 												
                                                                                        $rowNum++;
                                                                                        $flagMergeCell=true;
                                                                                }

                                } // end foreach($aCost as $cost_id=>$cost_obj){
                                                                        // begin render detail costs one agent
                                                                        if($flagMergeCell){
                                                                                $objPHPExcel->getActiveSheet()
                                                                                        ->mergeCells($cellBeginMergeAgent.':'.MyFunctionCustom::columnName($index).($rowNum-1));
                                                                                $objPHPExcel->getActiveSheet()->getStyle($cellBeginMergeAgent.':'.MyFunctionCustom::columnName($index).($rowNum))
                                                                                        ->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                                                                        }

                            // total col
                            $keyMonth=1;
                            $sumOneAgent=0;
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth).$rowNum,  'Grand Total');
                            foreach($dataExport[$year][$cost_id]['month'] as $month){
                                    if(isset($sumCol[$month])){
                                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumCol[$month]);
                                            $sumOneAgent+=$sumCol[$month];	
                                    }
                                    $objPHPExcel->getActiveSheet()
                                                    ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
                                                    ->getFont()
                                                    ->setBold(true);											
                                    $keyMonth++;
                            }
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumOneAgent);

                            // end total col
                             $objPHPExcel->getActiveSheet()
                                    ->getStyle($cellBeginAgent.':'.MyFunctionCustom::columnName($keyMonth+2).$rowNum)->getNumberFormat()
                                    ->setFormatCode('#,##0'); 


                            $objPHPExcel->getActiveSheet()->getStyle($cellBeginBorderAgent.':'.MyFunctionCustom::columnName($keyMonth+2).$rowNum )
                                    ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);								

                            $rowNum++;	

                    $key_agent_id++;

                } // end foreach($dataAgent as $kAgent=>$itemAgent){

             $i++;   // for createSheet(
            } // ********** end for 1 sheet for($i=0;$i<count($model->costs_month);$i++){
        }    

         //save file 
         $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
         //$objWriter->save('MyExcel.xslx');

         for($level=ob_get_level();$level>0;--$level)
         {
                 @ob_end_clean();
         }
         header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
         header('Pragma: public');
         header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment; filename="'.'Report Monthly Agent Cost 3-'.date('d-m-Y').'.'.'xlsx'.'"');
         header('Cache-Control: max-age=0');				
         $objWriter->save('php://output');			
         Yii::app()->end();	
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }     
      }
	  
    public static function export_agent_output_by_material($dataExport,$dataAgent, $dataAllMaterial){
    try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
         // Set properties
         $objPHPExcel->getProperties()->setCreator("NguyenDung")
                    ->setLastModifiedBy("NguyenDung")
                    ->setTitle('Report Monthly Output')
                    ->setSubject("Office 2007 XLSX Document")
                    ->setDescription("Report Monthly Output")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("GAS");
        // step 0: Tạo ra các sheet
        $i=0;
        if(count($dataExport)>0){
             $objPHPExcel->removeSheetByIndex(0); // may be need
//                    for($i;$i<count($dataExport);$i++){
            foreach($dataExport as $year=>$aAgentId){
                $objPHPExcel->createSheet();
                $objPHPExcel->setActiveSheetIndex($i);
                // set default font and font-size
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
                $objPHPExcel->getActiveSheet()->setTitle('NĂM '.$year);
                $objPHPExcel->getActiveSheet()->setCellValue("A1", 'BÁO CÁO SẢN LƯỢNG ĐẠI LÝ THEO SẢN PHẨM NĂM '.$year);
                $objPHPExcel->getActiveSheet()->mergeCells("A1:D1");

                $objPHPExcel->getActiveSheet()->setCellValue("A3", 'Đại Lý');
                $objPHPExcel->getActiveSheet()->setCellValue("B3", 'Sản Phẩm');
                $key_agent_id=0;
                $rowNum = 3;
                $index = 1;

                foreach($aAgentId as $agent_id=>$month_obj){		
                    if(isset($dataExport[$year][$agent_id]['arrMaterialId'])){
                            // render heaer of one agent
                            $keyMonth=1;
                            $sumCol = array();
                            foreach($dataExport[$year][$agent_id]['month'] as $month){
                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, 'T'.$month);											
                                    $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($keyMonth+2).$rowNum)
                                    ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                                    $objPHPExcel->getActiveSheet()
                                                    ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
                                                    ->getFont()
                                                    ->setBold(true);								
                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+3).$rowNum, 'Grand Total');
                                    $keyMonth++;
                            }
                            $rowNum++;
                            // end render heaer of one agent
                            $cellBeginAgent=MyFunctionCustom::columnName($index+2).$rowNum;
                            $cellBeginMergeAgent=MyFunctionCustom::columnName($index).$rowNum;
                            $cellBeginBorderAgent=MyFunctionCustom::columnName($index).($rowNum-1);

                                foreach($dataExport[$year][$agent_id]['arrMaterialId'] as $keyMaterial=>$material_id){
                                        $keyMonth=1;
                                        $sumRow = 0;
                                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).$rowNum, $dataAllMaterial[$material_id]->name);


                                                foreach($dataExport[$year][$agent_id]['month'] as $month){									
                                                        if($keyMaterial==0){
                                                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).$rowNum, $dataAgent[$agent_id]->first_name);
                                                        }

                                                        if(isset($dataExport[$year][$agent_id]['month_obj'][$month][$material_id])){
                                                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, 
                                                                        $dataExport[$year][$agent_id]['month_obj'][$month][$material_id]['parent_total']); 
                                                                $sumRow+=	$dataExport[$year][$agent_id]['month_obj'][$month][$material_id]['parent_total'];												
                                                                if(isset($sumCol[$month]))
                                                                        $sumCol[$month]+=$dataExport[$year][$agent_id]['month_obj'][$month][$material_id]['parent_total'];			
                                                                else
                                                                        $sumCol[$month]=$dataExport[$year][$agent_id]['month_obj'][$month][$material_id]['parent_total'];


                                                        }
                                                        $objPHPExcel->getActiveSheet()
                                                                ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
                                                                ->getFont()
                                                                ->setBold(true);										
                                                        $keyMonth++;
                                                } // end  foreach($dataExport[$year][$itemAgent->id] as $aMonth){
                                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumRow);
                                                $rowNum++;
                                                // for detail of one parent id
                                                    $key_of_month_out=0;
                                                        foreach($dataExport[$year][$agent_id]['month'] as $month){	
                                                                if(isset($dataExport[$year][$agent_id]['month_obj'][$month][$material_id])){
                                                                        if($key_of_month_out!=0)
                                                                            $rowNum-=(count($dataExport[$year][$agent_id]['month_obj'][$month][$material_id]['sub_obj']));
                                                                        foreach($dataExport[$year][$agent_id]['month_obj'][$month][$material_id]['sub_obj'] as $keySub=>$subObj){
                                                                                $keyMonth2=1;
                                                                                foreach($dataExport[$year][$agent_id]['month'] as $month2){	
                                                                                    if($subObj->sell_month==$month2){
                                                                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).$rowNum, $dataAllMaterial[$subObj->materials_id]->name);
                                                                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth2+2).$rowNum, $subObj->qty);																							
                                                                                    }
                                                                                    $keyMonth2++;
                                                                                }
                                                                                $rowNum++;	
                                                                        }
                                                                }
                                                        $key_of_month_out++;
                                                        }
                                                // end for detail of one parent id

                                } // end foreach($dataExport[$year][$agent_id]['arrMaterialId'] as $keyMaterial=>$material_id){	

                                $objPHPExcel->getActiveSheet()
                                    ->mergeCells($cellBeginMergeAgent.':'.MyFunctionCustom::columnName($index).($rowNum-1));
                                $objPHPExcel->getActiveSheet()->getStyle($cellBeginMergeAgent.':'.MyFunctionCustom::columnName($index).($rowNum))
                                    ->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


                            // total col
                            $keyMonth=1;
                            $sumOneAgent=0;
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth).$rowNum,  'Grand Total');
                            foreach($dataExport[$year][$agent_id]['month'] as $month){
                                    if(isset($sumCol[$month])){
                                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumCol[$month]);
                                            $sumOneAgent+=$sumCol[$month];	
                                    }
                                    $objPHPExcel->getActiveSheet()
                                                    ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
                                                    ->getFont()
                                                    ->setBold(true);											
                                    $keyMonth++;
                            }
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumOneAgent);

                            // end total col
                             $objPHPExcel->getActiveSheet()
                                    ->getStyle($cellBeginAgent.':'.MyFunctionCustom::columnName($keyMonth+2).$rowNum)->getNumberFormat()
                                    ->setFormatCode('#,##0'); 


                            $objPHPExcel->getActiveSheet()->getStyle($cellBeginBorderAgent.':'.MyFunctionCustom::columnName($keyMonth+2).$rowNum )
                                    ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);								

                            $rowNum++;	
                        }
                    $key_agent_id++;

                } // end foreach($dataAgent as $kAgent=>$itemAgent){



             $i++;   // for createSheet(
            } // ********** end for 1 sheet for($i=0;$i<count($model->costs_month);$i++){
        }    

         //save file 
         $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
         //$objWriter->save('MyExcel.xslx');

         for($level=ob_get_level();$level>0;--$level)
         {
                 @ob_end_clean();
         }
         header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
         header('Pragma: public');
         header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment; filename="'.'Report Monthly Output-'.date('d-m-Y').'.'.'xlsx'.'"');
         header('Cache-Control: max-age=0');				
         $objWriter->save('php://output');			
         Yii::app()->end();
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }              
      }
    
	  
    public static function export_agent_gross_profit_by_material($dataExportRevenue, $dataExportPriceRoot, $dataAgent, $dataAllMaterial){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
         // Set properties
         $objPHPExcel->getProperties()->setCreator("NguyenDung")
                    ->setLastModifiedBy("NguyenDung")
                    ->setTitle('Report Monthly Gross Profit')
                    ->setSubject("Office 2007 XLSX Document")
                    ->setDescription("Report Monthly Gross Profit")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("GAS");
        // step 0: Tạo ra các sheet
        $i=0;
        if(count($dataExportRevenue)>0){
             $objPHPExcel->removeSheetByIndex(0); // may be need
//                    for($i;$i<count($dataExportRevenue);$i++){
            foreach($dataExportRevenue as $year=>$aAgentId){
                $objPHPExcel->createSheet();
                $objPHPExcel->setActiveSheetIndex($i);
                // set default font and font-size
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
                $objPHPExcel->getActiveSheet()->setTitle('NĂM '.$year);
                $objPHPExcel->getActiveSheet()->setCellValue("A1", 'BÁO CÁO LỢI NHUẬN GỘP ĐẠI LÝ THEO SẢN PHẨM NĂM '.$year);
                $objPHPExcel->getActiveSheet()->mergeCells("A1:D1");

                $objPHPExcel->getActiveSheet()->setCellValue("A3", 'Đại Lý');
                $objPHPExcel->getActiveSheet()->setCellValue("B3", 'Sản Phẩm');
                $key_agent_id=0;
                $rowNum = 3;
                $index = 1;

                foreach($aAgentId as $agent_id=>$month_obj){		
                    if(isset($dataExportRevenue[$year][$agent_id]['arrMaterialId'])){
                            // render heaer of one agent
                            $keyMonth=1;
                            $sumCol = array();
                            foreach($dataExportRevenue[$year][$agent_id]['month'] as $month){
                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, 'T'.$month);											
                                    $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($keyMonth+2).$rowNum)
                                    ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                                    $objPHPExcel->getActiveSheet()
                                                    ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
                                                    ->getFont()
                                                    ->setBold(true);								
                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+3).$rowNum, 'Grand Total');
                                    $keyMonth++;
                            }
                            $rowNum++;
                            // end render heaer of one agent
                            $cellBeginAgent=MyFunctionCustom::columnName($index+2).$rowNum;
                            $cellBeginMergeAgent=MyFunctionCustom::columnName($index).$rowNum;
                            $cellBeginBorderAgent=MyFunctionCustom::columnName($index).($rowNum-1);
                                // begin render of one parent material
                                foreach($dataExportRevenue[$year][$agent_id]['arrMaterialId'] as $keyMaterial=>$material_id){
                                        $keyMonth=1;
                                        $sumRow = 0;
                                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).$rowNum, $dataAllMaterial[$material_id]->name);


                                                foreach($dataExportRevenue[$year][$agent_id]['month'] as $month){									
                                                        if($keyMaterial==0){
                                                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).$rowNum, $dataAgent[$agent_id]->first_name);
                                                        }
                                                        $Revenue_ = isset($dataExportRevenue[$year][$agent_id]['month_obj'][$month][$material_id]['parent_total'])?$dataExportRevenue[$year][$agent_id]['month_obj'][$month][$material_id]['parent_total']:0;
                                                        $PriceRoot_ = isset($dataExportPriceRoot[$year][$agent_id]['month_obj'][$month][$material_id]['parent_total'])?$dataExportPriceRoot[$year][$agent_id]['month_obj'][$month][$material_id]['parent_total']:0;
                                                        $cellValue = $Revenue_-$PriceRoot_;

                                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, 
                                                                $cellValue); 
                                                        $sumRow+=$cellValue;
                                                        if(isset($sumCol[$month]))
                                                                $sumCol[$month]+=$cellValue;
                                                        else
                                                                $sumCol[$month]=$cellValue;

                                                        $objPHPExcel->getActiveSheet()
                                                                ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
                                                                ->getFont()
                                                                ->setBold(true);										
                                                        $keyMonth++;
                                                } // end  foreach($dataExportRevenue[$year][$itemAgent->id] as $aMonth){
                                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumRow);
                                                $rowNum++;
                                                // for detail of one parent id
                                                foreach($dataExportRevenue[$year][$agent_id]['month'] as $month){	
                                                    if(isset($dataExportRevenue[$year][$agent_id]['month_obj'][$month][$material_id])){
                                                            foreach($dataExportRevenue[$year][$agent_id]['month_obj'][$month][$material_id]['sub_obj'] as $keySub=>$subObj){
                                                                    $keyMonth2=1;
                                                                    foreach($dataExportRevenue[$year][$agent_id]['month'] as $month2){	
                                                                            if($subObj->sell_month==$month2){
                                                                                    $PriceRoot_1 = isset($dataExportPriceRoot[$year][$agent_id]['month_obj'][$month][$material_id]['sub_obj'][$keySub]->total_root)?$dataExportPriceRoot[$year][$agent_id]['month_obj'][$month][$material_id]['sub_obj'][$keySub]->total_root:0;
                                                                                    $cellValue_1 = $subObj->total_sell - $PriceRoot_1;
                                                                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).$rowNum, $dataAllMaterial[$subObj->materials_id]->name);
                                                                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth2+2).$rowNum, $cellValue_1);	
                                                                            }
                                                                            $keyMonth2++;
                                                                    }
                                                                    $rowNum++;	
                                                            }
                                                    }
                                                }
                                                // end for detail of one parent id

                                } // end foreach($dataExportRevenue[$year][$agent_id]['arrMaterialId'] as $keyMaterial=>$material_id){	
                                // end render of one parent material    
                                $objPHPExcel->getActiveSheet()
                                    ->mergeCells($cellBeginMergeAgent.':'.MyFunctionCustom::columnName($index).($rowNum-1));
                                $objPHPExcel->getActiveSheet()->getStyle($cellBeginMergeAgent.':'.MyFunctionCustom::columnName($index).($rowNum))
                                    ->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


                            // total col
                            $keyMonth=1;
                            $sumOneAgent=0;
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth).$rowNum,  'Grand Total');
                            foreach($dataExportRevenue[$year][$agent_id]['month'] as $month){
                                    if(isset($sumCol[$month])){
                                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumCol[$month]);
                                            $sumOneAgent+=$sumCol[$month];	
                                    }
                                    $objPHPExcel->getActiveSheet()
                                                    ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
                                                    ->getFont()
                                                    ->setBold(true);											
                                    $keyMonth++;
                            }
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumOneAgent);

                            // end total col
                             $objPHPExcel->getActiveSheet()
                                    ->getStyle($cellBeginAgent.':'.MyFunctionCustom::columnName($keyMonth+2).$rowNum)->getNumberFormat()
                                    ->setFormatCode('#,##0'); 


                            $objPHPExcel->getActiveSheet()->getStyle($cellBeginBorderAgent.':'.MyFunctionCustom::columnName($keyMonth+2).$rowNum )
                                    ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);								

                            $rowNum++;	
                        }
                    $key_agent_id++;

                } // end foreach($dataAgent as $kAgent=>$itemAgent){



             $i++;   // for createSheet(
            } // ********** end for 1 sheet for($i=0;$i<count($model->costs_month);$i++){
        }    

         //save file 
         $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
         //$objWriter->save('MyExcel.xslx');

         for($level=ob_get_level();$level>0;--$level)
         {
                 @ob_end_clean();
         }
         header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
         header('Pragma: public');
         header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment; filename="'.'Report Monthly Gross Profit-'.date('d-m-Y').'.'.'xlsx'.'"');
         header('Cache-Control: max-age=0');				
         $objWriter->save('php://output');			
         Yii::app()->end();
         }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }     
      }
    
      
      
    public static function export_revenue_material_by_agent($dataExport,$dataAgent, $dataAllMaterial, $model){
    try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
         // Set properties
         $objPHPExcel->getProperties()->setCreator("NguyenDung")
                    ->setLastModifiedBy("NguyenDung")
                    ->setTitle('Report Product Revenue')
                    ->setSubject("Office 2007 XLSX Document")
                    ->setDescription("Report Product Revenue")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("GAS");
        // step 0: Tạo ra các sheet
        $i=0;
        if(count($dataExport)>0){
             $objPHPExcel->removeSheetByIndex(0); // may be need
//                    for($i;$i<count($dataExport);$i++){
            foreach($dataExport as $year=>$aMaterialId){
                $objPHPExcel->createSheet();
                $objPHPExcel->setActiveSheetIndex($i);
                // set default font and font-size
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
                $objPHPExcel->getActiveSheet()->setTitle('NĂM '.$year);
                $material_sub_name = $model->materials_sub_id?$dataAllMaterial[$model->materials_sub_id]->name.' ':'';
                $objPHPExcel->getActiveSheet()->setCellValue("A1", 'BÁO CÁO DOANH THU SẢN PHẨM '.$material_sub_name.' NĂM '.$year);
                $objPHPExcel->getActiveSheet()->mergeCells("A1:D1");

                $objPHPExcel->getActiveSheet()->setCellValue("A3", 'Sản Phẩm');
                $objPHPExcel->getActiveSheet()->setCellValue("B3", 'Đại Lý');
                $rowNum = 3;
                $index = 1;

                foreach($aMaterialId as $material_id=>$month_obj){		
                    if(isset($dataExport[$year][$material_id]['arrAgentId']) && count($dataExport[$year][$material_id]['arrAgentId'])>0){
                            // render heaer of one agent
                            $keyMonth=1;
                            $sumCol = array();
                            $key_agent_id=0;
                            foreach($dataExport[$year][$material_id]['month'] as $month){
                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, 'T'.$month);											
                                    $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($keyMonth+2).$rowNum)
                                    ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                                    $objPHPExcel->getActiveSheet()
                                                    ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
                                                    ->getFont()
                                                    ->setBold(true);								
                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+3).$rowNum, 'Grand Total');
                                    $keyMonth++;
                            }
                            $rowNum++;
                            // end render heaer of one agent
                            $cellBeginAgent=MyFunctionCustom::columnName($index+2).$rowNum;
                            $cellBeginMergeAgent=MyFunctionCustom::columnName($index).$rowNum;
                            $cellBeginBorderAgent=MyFunctionCustom::columnName($index).($rowNum-1);

                                foreach($dataAgent as $agent_id=>$obj_agent){
                                    if(in_array($agent_id, $dataExport[$year][$material_id]['arrAgentId'])){
                                        $keyMonth=1;
                                        $sumRow = 0;
                                        if($key_agent_id==0){
                                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).$rowNum, $dataAllMaterial[$material_id]->name);
                                        }
                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).$rowNum, $obj_agent->first_name);

                                        foreach($dataExport[$year][$material_id]['month'] as $month){									
                                            if(isset($dataExport[$year][$material_id]['month_obj'][$month][$agent_id])){
                                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, 
                                                            $dataExport[$year][$material_id]['month_obj'][$month][$agent_id]); 
                                                    $sumRow+=	$dataExport[$year][$material_id]['month_obj'][$month][$agent_id];												
                                                    if(isset($sumCol[$month]))
                                                            $sumCol[$month]+=$dataExport[$year][$material_id]['month_obj'][$month][$agent_id];			
                                                    else
                                                            $sumCol[$month]=$dataExport[$year][$material_id]['month_obj'][$month][$agent_id];


                                            }
//                                                    $objPHPExcel->getActiveSheet()
//                                                            ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
//                                                            ->getFont()
//                                                            ->setBold(true);										
                                            $keyMonth++;
                                        } // end  foreach($dataExport[$year][$itemAgent->id] as $aMonth){
                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumRow);
                                        $rowNum++;
                                        $key_agent_id++;
                                    } // end if in_array()
                                } // end foreach($dataExport[$year][$agent_id]['arrMaterialId'] as $keyMaterial=>$material_id){	

                                $objPHPExcel->getActiveSheet()
                                    ->mergeCells($cellBeginMergeAgent.':'.MyFunctionCustom::columnName($index).($rowNum-1));
                                $objPHPExcel->getActiveSheet()->getStyle($cellBeginMergeAgent.':'.MyFunctionCustom::columnName($index).($rowNum))
                                    ->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


                            // total col
                            $keyMonth=1;
                            $sumOneMaterial=0;
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth).$rowNum,  'Grand Total');
                            foreach($dataExport[$year][$material_id]['month'] as $month){
                                    if(isset($sumCol[$month])){
                                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumCol[$month]);
                                            $sumOneMaterial+=$sumCol[$month];	
                                    }
                                    $objPHPExcel->getActiveSheet()
                                                    ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
                                                    ->getFont()
                                                    ->setBold(true);											
                                    $keyMonth++;
                            }
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumOneMaterial);

                            // end total col
                             $objPHPExcel->getActiveSheet()
                                    ->getStyle($cellBeginAgent.':'.MyFunctionCustom::columnName($keyMonth+2).$rowNum)->getNumberFormat()
                                    ->setFormatCode('#,##0'); 


                            $objPHPExcel->getActiveSheet()->getStyle($cellBeginBorderAgent.':'.MyFunctionCustom::columnName($keyMonth+2).$rowNum )
                                    ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);								

                            $rowNum++;	
                        }
                    $key_agent_id++;

                } // end foreach($dataAgent as $kAgent=>$itemAgent){



             $i++;   // for createSheet(
            } // ********** end for 1 sheet for($i=0;$i<count($model->costs_month);$i++){
        }    

         //save file 
         $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
         //$objWriter->save('MyExcel.xslx');

         for($level=ob_get_level();$level>0;--$level)
         {
                 @ob_end_clean();
         }
         header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
         header('Pragma: public');
         header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment; filename="'.'Report Product Revenue-'.date('d-m-Y').'.'.'xlsx'.'"');
         header('Cache-Control: max-age=0');				
         $objWriter->save('php://output');			
         Yii::app()->end();		          
         }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }     
      }
      
    public static function export_price_root_material_by_agent($dataExport,$dataAgent, $dataAllMaterial, $model){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
         // Set properties
         $objPHPExcel->getProperties()->setCreator("NguyenDung")
                    ->setLastModifiedBy("NguyenDung")
                    ->setTitle('Report Product Price Root')
                    ->setSubject("Office 2007 XLSX Document")
                    ->setDescription("Report Product Price Root")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("GAS");
        // step 0: Tạo ra các sheet
        $i=0;
        if(count($dataExport)>0){
             $objPHPExcel->removeSheetByIndex(0); // may be need
//                    for($i;$i<count($dataExport);$i++){
            foreach($dataExport as $year=>$aMaterialId){
                $objPHPExcel->createSheet();
                $objPHPExcel->setActiveSheetIndex($i);
                // set default font and font-size
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
                $objPHPExcel->getActiveSheet()->setTitle('NĂM '.$year);
                $material_sub_name = $model->materials_sub_id?$dataAllMaterial[$model->materials_sub_id]->name.' ':'';
                $objPHPExcel->getActiveSheet()->setCellValue("A1", 'BÁO CÁO GIÁ VỐN SẢN PHẨM '.$material_sub_name.' NĂM '.$year);
                $objPHPExcel->getActiveSheet()->mergeCells("A1:D1");

                $objPHPExcel->getActiveSheet()->setCellValue("A3", 'Sản Phẩm');
                $objPHPExcel->getActiveSheet()->setCellValue("B3", 'Đại Lý');
                $rowNum = 3;
                $index = 1;

                foreach($aMaterialId as $material_id=>$month_obj){		
                    if(isset($dataExport[$year][$material_id]['arrAgentId']) && count($dataExport[$year][$material_id]['arrAgentId'])>0){
                            // render heaer of one agent
                            $keyMonth=1;
                            $sumCol = array();
                            $key_agent_id=0;
                            foreach($dataExport[$year][$material_id]['month'] as $month){
                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, 'T'.$month);											
                                    $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($keyMonth+2).$rowNum)
                                    ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                                    $objPHPExcel->getActiveSheet()
                                                    ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
                                                    ->getFont()
                                                    ->setBold(true);								
                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+3).$rowNum, 'Grand Total');
                                    $keyMonth++;
                            }
                            $rowNum++;
                            // end render heaer of one agent
                            $cellBeginAgent=MyFunctionCustom::columnName($index+2).$rowNum;
                            $cellBeginMergeAgent=MyFunctionCustom::columnName($index).$rowNum;
                            $cellBeginBorderAgent=MyFunctionCustom::columnName($index).($rowNum-1);

                                foreach($dataAgent as $agent_id=>$obj_agent){
                                    if(in_array($agent_id, $dataExport[$year][$material_id]['arrAgentId'])){
                                        $keyMonth=1;
                                        $sumRow = 0;
                                        if($key_agent_id==0){
                                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).$rowNum, $dataAllMaterial[$material_id]->name);
                                        }
                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).$rowNum, $obj_agent->first_name);

                                        foreach($dataExport[$year][$material_id]['month'] as $month){									
                                            if(isset($dataExport[$year][$material_id]['month_obj'][$month][$agent_id])){
                                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, 
                                                            $dataExport[$year][$material_id]['month_obj'][$month][$agent_id]); 
                                                    $sumRow+=	$dataExport[$year][$material_id]['month_obj'][$month][$agent_id];												
                                                    if(isset($sumCol[$month]))
                                                            $sumCol[$month]+=$dataExport[$year][$material_id]['month_obj'][$month][$agent_id];			
                                                    else
                                                            $sumCol[$month]=$dataExport[$year][$material_id]['month_obj'][$month][$agent_id];


                                            }
//                                                    $objPHPExcel->getActiveSheet()
//                                                            ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
//                                                            ->getFont()
//                                                            ->setBold(true);										
                                            $keyMonth++;
                                        } // end  foreach($dataExport[$year][$itemAgent->id] as $aMonth){
                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumRow);
                                        $rowNum++;
                                        $key_agent_id++;
                                    } // end if in_array()
                                } // end foreach($dataExport[$year][$agent_id]['arrMaterialId'] as $keyMaterial=>$material_id){	

                                $objPHPExcel->getActiveSheet()
                                    ->mergeCells($cellBeginMergeAgent.':'.MyFunctionCustom::columnName($index).($rowNum-1));
                                $objPHPExcel->getActiveSheet()->getStyle($cellBeginMergeAgent.':'.MyFunctionCustom::columnName($index).($rowNum))
                                    ->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


                            // total col
                            $keyMonth=1;
                            $sumOneMaterial=0;
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth).$rowNum,  'Grand Total');
                            foreach($dataExport[$year][$material_id]['month'] as $month){
                                    if(isset($sumCol[$month])){
                                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumCol[$month]);
                                            $sumOneMaterial+=$sumCol[$month];	
                                    }
                                    $objPHPExcel->getActiveSheet()
                                                    ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
                                                    ->getFont()
                                                    ->setBold(true);											
                                    $keyMonth++;
                            }
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumOneMaterial);

                            // end total col
                             $objPHPExcel->getActiveSheet()
                                    ->getStyle($cellBeginAgent.':'.MyFunctionCustom::columnName($keyMonth+2).$rowNum)->getNumberFormat()
                                    ->setFormatCode('#,##0'); 


                            $objPHPExcel->getActiveSheet()->getStyle($cellBeginBorderAgent.':'.MyFunctionCustom::columnName($keyMonth+2).$rowNum )
                                    ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);								

                            $rowNum++;	
                        }
                    $key_agent_id++;

                } // end foreach($dataAgent as $kAgent=>$itemAgent){



             $i++;   // for createSheet(
            } // ********** end for 1 sheet for($i=0;$i<count($model->costs_month);$i++){
        }    

         //save file 
         $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
         //$objWriter->save('MyExcel.xslx');

         for($level=ob_get_level();$level>0;--$level)
         {
                 @ob_end_clean();
         }
         header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
         header('Pragma: public');
         header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment; filename="'.'Report Product Price Root-'.date('d-m-Y').'.'.'xlsx'.'"');
         header('Cache-Control: max-age=0');				
         $objWriter->save('php://output');			
         Yii::app()->end();		
         }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }     
      }
    
      
    public static function export_output_material_by_agent($dataExport,$dataAgent, $dataAllMaterial, $model){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
         // Set properties
         $objPHPExcel->getProperties()->setCreator("NguyenDung")
                    ->setLastModifiedBy("NguyenDung")
                    ->setTitle('Report Product Output')
                    ->setSubject("Office 2007 XLSX Document")
                    ->setDescription("Report Product Output")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("GAS");
        // step 0: Tạo ra các sheet
        $i=0;
        if(count($dataExport)>0){
             $objPHPExcel->removeSheetByIndex(0); // may be need
//                    for($i;$i<count($dataExport);$i++){
            foreach($dataExport as $year=>$aMaterialId){
                $objPHPExcel->createSheet();
                $objPHPExcel->setActiveSheetIndex($i);
                // set default font and font-size
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
                $objPHPExcel->getActiveSheet()->setTitle('NĂM '.$year);
                $material_sub_name = $model->materials_sub_id?$dataAllMaterial[$model->materials_sub_id]->name.' ':'';
                $objPHPExcel->getActiveSheet()->setCellValue("A1", 'BÁO CÁO SẢN LƯỢNG SẢN PHẨM '.$material_sub_name.' NĂM '.$year);
                $objPHPExcel->getActiveSheet()->mergeCells("A1:D1");

                $objPHPExcel->getActiveSheet()->setCellValue("A3", 'Sản Phẩm');
                $objPHPExcel->getActiveSheet()->setCellValue("B3", 'Đại Lý');
                $rowNum = 3;
                $index = 1;

                foreach($aMaterialId as $material_id=>$month_obj){		
                    if(isset($dataExport[$year][$material_id]['arrAgentId']) && count($dataExport[$year][$material_id]['arrAgentId'])>0){
                            // render heaer of one agent
                            $keyMonth=1;
                            $sumCol = array();
                            $key_agent_id=0;
                            foreach($dataExport[$year][$material_id]['month'] as $month){
                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, 'T'.$month);											
                                    $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($keyMonth+2).$rowNum)
                                    ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                                    $objPHPExcel->getActiveSheet()
                                                    ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
                                                    ->getFont()
                                                    ->setBold(true);								
                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+3).$rowNum, 'Grand Total');
                                    $keyMonth++;
                            }
                            $rowNum++;
                            // end render heaer of one agent
                            $cellBeginAgent=MyFunctionCustom::columnName($index+2).$rowNum;
                            $cellBeginMergeAgent=MyFunctionCustom::columnName($index).$rowNum;
                            $cellBeginBorderAgent=MyFunctionCustom::columnName($index).($rowNum-1);

                                foreach($dataAgent as $agent_id=>$obj_agent){
                                    if(in_array($agent_id, $dataExport[$year][$material_id]['arrAgentId'])){
                                        $keyMonth=1;
                                        $sumRow = 0;
                                        if($key_agent_id==0){
                                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).$rowNum, $dataAllMaterial[$material_id]->name);
                                        }
                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).$rowNum, $obj_agent->first_name);

                                        foreach($dataExport[$year][$material_id]['month'] as $month){									
                                            if(isset($dataExport[$year][$material_id]['month_obj'][$month][$agent_id])){
                                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, 
                                                            $dataExport[$year][$material_id]['month_obj'][$month][$agent_id]); 
                                                    $sumRow+=	$dataExport[$year][$material_id]['month_obj'][$month][$agent_id];												
                                                    if(isset($sumCol[$month]))
                                                            $sumCol[$month]+=$dataExport[$year][$material_id]['month_obj'][$month][$agent_id];			
                                                    else
                                                            $sumCol[$month]=$dataExport[$year][$material_id]['month_obj'][$month][$agent_id];


                                            }
//                                                    $objPHPExcel->getActiveSheet()
//                                                            ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
//                                                            ->getFont()
//                                                            ->setBold(true);										
                                            $keyMonth++;
                                        } // end  foreach($dataExport[$year][$itemAgent->id] as $aMonth){
                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumRow);
                                        $rowNum++;
                                        $key_agent_id++;
                                    } // end if in_array()
                                } // end foreach($dataExport[$year][$agent_id]['arrMaterialId'] as $keyMaterial=>$material_id){	

                                $objPHPExcel->getActiveSheet()
                                    ->mergeCells($cellBeginMergeAgent.':'.MyFunctionCustom::columnName($index).($rowNum-1));
                                $objPHPExcel->getActiveSheet()->getStyle($cellBeginMergeAgent.':'.MyFunctionCustom::columnName($index).($rowNum))
                                    ->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


                            // total col
                            $keyMonth=1;
                            $sumOneMaterial=0;
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth).$rowNum,  'Grand Total');
                            foreach($dataExport[$year][$material_id]['month'] as $month){
                                    if(isset($sumCol[$month])){
                                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumCol[$month]);
                                            $sumOneMaterial+=$sumCol[$month];	
                                    }
                                    $objPHPExcel->getActiveSheet()
                                                    ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
                                                    ->getFont()
                                                    ->setBold(true);											
                                    $keyMonth++;
                            }
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumOneMaterial);

                            // end total col
                             $objPHPExcel->getActiveSheet()
                                    ->getStyle($cellBeginAgent.':'.MyFunctionCustom::columnName($keyMonth+2).$rowNum)->getNumberFormat()
                                    ->setFormatCode('#,##0'); 


                            $objPHPExcel->getActiveSheet()->getStyle($cellBeginBorderAgent.':'.MyFunctionCustom::columnName($keyMonth+2).$rowNum )
                                    ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);								

                            $rowNum++;	
                        }
                    $key_agent_id++;

                } // end foreach($dataAgent as $kAgent=>$itemAgent){



             $i++;   // for createSheet(
            } // ********** end for 1 sheet for($i=0;$i<count($model->costs_month);$i++){
        }    

         //save file 
         $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
         //$objWriter->save('MyExcel.xslx');

         for($level=ob_get_level();$level>0;--$level)
         {
                 @ob_end_clean();
         }
         header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
         header('Pragma: public');
         header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment; filename="'.'Report Product Output-'.date('d-m-Y').'.'.'xlsx'.'"');
         header('Cache-Control: max-age=0');				
         $objWriter->save('php://output');			
         Yii::app()->end();		          
         }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }     
      }
    
      
    public static function export_gross_profit_material_by_agent($dataExportRevenue, $dataExportPriceRoot, $dataAgent, $dataAllMaterial, $model){
        try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
         // Set properties
         $objPHPExcel->getProperties()->setCreator("NguyenDung")
                    ->setLastModifiedBy("NguyenDung")
                    ->setTitle('Report Product Gross Profit')
                    ->setSubject("Office 2007 XLSX Document")
                    ->setDescription("Report Product Gross Profit")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("GAS");
        // step 0: Tạo ra các sheet
        $i=0;
        if(count($dataExportRevenue)>0){
             $objPHPExcel->removeSheetByIndex(0); // may be need
//                    for($i;$i<count($dataExportRevenue);$i++){
            foreach($dataExportRevenue as $year=>$aMaterialId){
                $objPHPExcel->createSheet();
                $objPHPExcel->setActiveSheetIndex($i);
                // set default font and font-size
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
                $objPHPExcel->getActiveSheet()->setTitle('NĂM '.$year);
                $material_sub_name = $model->materials_sub_id?$dataAllMaterial[$model->materials_sub_id]->name.' ':'';
                $objPHPExcel->getActiveSheet()->setCellValue("A1", 'BÁO CÁO LỢI NHUẬN GỘP SẢN PHẨM '.$material_sub_name.' NĂM '.$year);
                $objPHPExcel->getActiveSheet()->mergeCells("A1:D1");

                $objPHPExcel->getActiveSheet()->setCellValue("A3", 'Sản Phẩm');
                $objPHPExcel->getActiveSheet()->setCellValue("B3", 'Đại Lý');
                $rowNum = 3;
                $index = 1;

                foreach($aMaterialId as $material_id=>$month_obj){		
                    if(isset($dataExportRevenue[$year][$material_id]['arrAgentId']) && count($dataExportRevenue[$year][$material_id]['arrAgentId'])>0){
                            // render heaer of one agent
                            $keyMonth=1;
                            $sumCol = array();
                            $key_agent_id=0;
                            foreach($dataExportRevenue[$year][$material_id]['month'] as $month){
                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, 'T'.$month);											
                                    $objPHPExcel->getActiveSheet()->getStyle(MyFunctionCustom::columnName($keyMonth+2).$rowNum)
                                    ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                                    $objPHPExcel->getActiveSheet()
                                                    ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
                                                    ->getFont()
                                                    ->setBold(true);								
                                    $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+3).$rowNum, 'Grand Total');
                                    $keyMonth++;
                            }
                            $rowNum++;
                            // end render heaer of one agent
                            $cellBeginAgent=MyFunctionCustom::columnName($index+2).$rowNum;
                            $cellBeginMergeAgent=MyFunctionCustom::columnName($index).$rowNum;
                            $cellBeginBorderAgent=MyFunctionCustom::columnName($index).($rowNum-1);

                                foreach($dataAgent as $agent_id=>$obj_agent){
                                    if(in_array($agent_id, $dataExportRevenue[$year][$material_id]['arrAgentId'])){
                                        $keyMonth=1;
                                        $sumRow = 0;
                                        if($key_agent_id==0){
                                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index).$rowNum, $dataAllMaterial[$material_id]->name);
                                        }
                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index+1).$rowNum, $obj_agent->first_name);

                                        foreach($dataExportRevenue[$year][$material_id]['month'] as $month){
                                            $Revenue = isset($dataExportRevenue[$year][$material_id]['month_obj'][$month][$agent_id])?$dataExportRevenue[$year][$material_id]['month_obj'][$month][$agent_id]:0;
                                            $PriceRoot = isset($dataExportPriceRoot[$year][$material_id]['month_obj'][$month][$agent_id])?$dataExportPriceRoot[$year][$material_id]['month_obj'][$month][$agent_id]:0;
                                            $cellValue = $Revenue-$PriceRoot;

                                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, 
                                                    $cellValue); 
                                            $sumRow+=$cellValue;
                                            if(isset($sumCol[$month]))
                                                    $sumCol[$month]+=$cellValue;
                                            else
                                                    $sumCol[$month]=$cellValue;

//                                                    $objPHPExcel->getActiveSheet()
//                                                            ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
//                                                            ->getFont()
//                                                            ->setBold(true);										
                                            $keyMonth++;
                                        } // end  foreach($dataExportRevenue[$year][$itemAgent->id] as $aMonth){
                                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumRow);
                                        $rowNum++;
                                        $key_agent_id++;
                                    } // end if in_array()
                                } // end foreach($dataExportRevenue[$year][$agent_id]['arrMaterialId'] as $keyMaterial=>$material_id){	

                                $objPHPExcel->getActiveSheet()
                                    ->mergeCells($cellBeginMergeAgent.':'.MyFunctionCustom::columnName($index).($rowNum-1));
                                $objPHPExcel->getActiveSheet()->getStyle($cellBeginMergeAgent.':'.MyFunctionCustom::columnName($index).($rowNum))
                                    ->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


                            // total col
                            $keyMonth=1;
                            $sumOneMaterial=0;
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth).$rowNum,  'Grand Total');
                            foreach($dataExportRevenue[$year][$material_id]['month'] as $month){
                                    if(isset($sumCol[$month])){
                                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumCol[$month]);
                                            $sumOneMaterial+=$sumCol[$month];	
                                    }
                                    $objPHPExcel->getActiveSheet()
                                                    ->getStyle(MyFunctionCustom::columnName($index).($rowNum).':'.MyFunctionCustom::columnName($index+$keyMonth+2).($rowNum))
                                                    ->getFont()
                                                    ->setBold(true);											
                                    $keyMonth++;
                            }
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($keyMonth+2).$rowNum, $sumOneMaterial);

                            // end total col
                             $objPHPExcel->getActiveSheet()
                                    ->getStyle($cellBeginAgent.':'.MyFunctionCustom::columnName($keyMonth+2).$rowNum)->getNumberFormat()
                                    ->setFormatCode('#,##0'); 


                            $objPHPExcel->getActiveSheet()->getStyle($cellBeginBorderAgent.':'.MyFunctionCustom::columnName($keyMonth+2).$rowNum )
                                    ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);								

                            $rowNum++;	
                        }
                    $key_agent_id++;

                } // end foreach($dataAgent as $kAgent=>$itemAgent){



             $i++;   // for createSheet(
            } // ********** end for 1 sheet for($i=0;$i<count($model->costs_month);$i++){
        }    

         //save file 
         $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
         //$objWriter->save('MyExcel.xslx');

         for($level=ob_get_level();$level>0;--$level)
         {
                 @ob_end_clean();
         }
         header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
         header('Pragma: public');
         header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment; filename="'.'Report Product Gross Profit-'.date('d-m-Y').'.'.'xlsx'.'"');
         header('Cache-Control: max-age=0');				
         $objWriter->save('php://output');			
         Yii::app()->end();		          
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }                  
    }
    
    // Anh Dung Feb 19, 2016
    public static function OutputAgent($model, $aData){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                        ->setLastModifiedBy("NguyenDung")
                                        ->setTitle('OutputAgent')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("OutputAgent")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1;
        $i=1;
        $cmsFormatter = new CmsFormatter();
        $aType = GasStoreCard::$DAILY_INTERNAL_TYPE_CUSTOMER;
        $aTypeText = $aType[$model->ext_is_maintain];
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);            
        $objPHPExcel->getActiveSheet()->setTitle('Báo cáo sản lượng đại lý'); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "Báo cáo sản lượng $aTypeText từ $model->date_from đến $model->date_to");
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);    			
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $row++;
        $index=1;
        $beginBorder = $row;
        if($model->ext_is_maintain == CUSTOMER_HO_GIA_DINH){
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'SN');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại Lý');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã Số');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Thương Hiệu');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Số Lượng');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Giá Bán');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Doanh Thu');
        }else{
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'SN');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Đại Lý');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Khách Hàng');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã Số');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Thương Hiệu');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Số Lượng (Bình)');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Số Lượng (KG)');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Gas Dư (KG)');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Trừ Gas Dư (KG)');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Giá Bán');
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Doanh Thu');
        }
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        self::OutputAgentBody($model, $aData, $objPHPExcel, $row, $i);

        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);

        $row--;		
//        $index--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        if($model->ext_is_maintain == CUSTOMER_HO_GIA_DINH){
            $objPHPExcel->getActiveSheet()->getStyle("E$beginBorder:E".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
            $objPHPExcel->getActiveSheet()
                                    ->getStyle("E$beginBorder:G".$row)->getNumberFormat()
                                    ->setFormatCode('#,##0'); 
        }else{
            $objPHPExcel->getActiveSheet()
                                    ->getStyle("F$beginBorder:K".$row)->getNumberFormat()
                                    ->setFormatCode('#,##0'); 
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
//            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        }
        
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.'OutputAgent'.'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }     
    }
    /**
     * @Author: ANH DUNG Feb 19, 2016
     * @Todo: handle body of excel
     */
    public static function OutputAgentBody($model, $aData, &$objPHPExcel, &$row, &$i) {
        $cmsFormatter = new CmsFormatter();
        $OUTPUT = isset($aData['OUTPUT'])?$aData['OUTPUT']:array();
        $REMAIN = isset($aData['REMAIN'])?$aData['REMAIN']:array();
//        echo '<pre>';
//        print_r($REMAIN);
//        echo '</pre>';
//        die;
        $AGENT_MODEL = $aData['ROLE_AGENT_MODEL'];
        $CUSTOMER_MODEL = isset($aData['CUSTOMER_MODEL'])?$aData['CUSTOMER_MODEL']:array();
        $aModelMaterial = GasMaterials::getArrayIdByMaterialTypeId(CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT, array("ListIdModel"=>1));
        $aCheckWriteRemain = array();
        $PRICE_SETUP = $aData['PRICE_SETUP'];
        $ZONE_PROVINCE = GasOrders::getTargetZoneProvinceId();
        
        if($model->ext_is_maintain == CUSTOMER_HO_GIA_DINH){
            foreach($AGENT_MODEL as $province_id=> $aModelAgent):
                foreach($ZONE_PROVINCE as $z_id=>$aProvince){
                    $key_provice = array_search ($province_id, $aProvince);// array_search Returns the key for needle if it is found in the array, FALSE otherwise.
                    if($key_provice !== FALSE){
                        $zone_id = $z_id;
                    }
                }
                foreach($aModelAgent as $mAgent):
                    $priceHgd       = MyFunctionCustom::getHgdPriceSetup($PRICE_SETUP, $mAgent->id);
                    $sumAgent = 0;
                    $sumAgentAmount = 0;
                    $i=1;
                    foreach($OUTPUT[$mAgent->id] as $materials_id => $qty):
                        $materialName = "";// Jun 12, 2016
                        $materialCode = "";// Jun 12, 2016
                        if(isset($aModelMaterial[$materials_id])){
                            $materialName = $aModelMaterial[$materials_id]->getName();
                            $materialCode = $aModelMaterial[$materials_id]->materials_no;
                        }
                        
                        // Jun 12, 2016
                        $price = isset($priceHgd[$materials_id]) ? $priceHgd[$materials_id] : 0;
                        $amount = $price * $qty;
                        $sumAgentAmount += $amount;
                            
                        $index=1;
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $i);
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $mAgent->first_name);
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $materialCode);
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $materialName);
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $qty);
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $price);
                        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $amount);
                        $sumAgent += $qty;
                        $row++;
                        $i++;
                    endforeach;// end $OUTPUT[$mAgent->id]
                    $objPHPExcel->getActiveSheet()
                        ->getStyle(MyFunctionCustom::columnName(1).($row).':'.MyFunctionCustom::columnName(15).($row))
                        ->getFont()
                        ->setBold(true);
//                    $objPHPExcel->getActiveSheet()->mergeCells("A$row:C$row");
                    $objPHPExcel->getActiveSheet()->setCellValue("D$row", "Tổng cộng");
                    $objPHPExcel->getActiveSheet()->setCellValue("E$row", $sumAgent);
                    $objPHPExcel->getActiveSheet()->setCellValue("G$row", $sumAgentAmount);
                    $row++;
                    $i++;
                endforeach;// end foreach($aModelAgent
            endforeach;// end foreach($AGENT_MODEL
        }else{// for BO + MOI
            foreach($AGENT_MODEL as $province_id=> $aModelAgent):
                foreach($ZONE_PROVINCE as $z_id=>$aProvince){
                    $key_provice = array_search ($province_id, $aProvince);// array_search Returns the key for needle if it is found in the array, FALSE otherwise.
                    if($key_provice !== FALSE){
                        $zone_id = $z_id;
                    }
                }
                foreach($aModelAgent as $mAgent):
                    $sumAgent = 0;
                    $sumAgentAmount = 0;
                    $sumAgentKg = 0;
                    $sumRemainKg = 0;
                    $sumTotalAfterRemainKg = 0;
                    $i=1;
                    foreach($OUTPUT[$mAgent->id] as $customer_id=>$aMaterialsTypeId):
                        foreach($aMaterialsTypeId as $materials_type_id => $aMaterialsId):
                            foreach($aMaterialsId as $materials_id => $qty):
                                $materialName = "";// Jun 12, 2016
                                $materialCode = "";// Jun 12, 2016
                                if(isset($aModelMaterial[$materials_id])){
                                    $materialName = $aModelMaterial[$materials_id]->getName();
                                    $materialCode = $aModelMaterial[$materials_id]->materials_no;
                                }
                                // Jun 12, 2016
                                $price = isset($PRICE_SETUP[$materials_id][$zone_id]) ? $PRICE_SETUP[$materials_id][$zone_id] : 0;
                                $amount = $price * $qty;
                                $sumAgentAmount += $amount;
                                
                                $needWriteRemain = false;
                                $index=1;
                                $Qty2Kg = $qty*(isset(CmsFormatter::$MATERIAL_VALUE_KG[$materials_type_id]) ? CmsFormatter::$MATERIAL_VALUE_KG[$materials_type_id] : 1);
                                $remainKg = isset($REMAIN[$mAgent->id][$customer_id]) ? $REMAIN[$mAgent->id][$customer_id]: 0;
                                $totalAfterRemain = $Qty2Kg;
                                if($remainKg>0 && !isset($aCheckWriteRemain[$mAgent->id][$customer_id]) ){
                                    $aCheckWriteRemain[$mAgent->id][$customer_id] = $customer_id;
                                    $totalAfterRemain = $totalAfterRemain - $remainKg;
                                    $sumRemainKg += $remainKg;
                                    
                                    $needWriteRemain = true;
                                }
                                if(!$needWriteRemain){
                                    $remainKg = "";
                                }
                                $sumTotalAfterRemainKg += $totalAfterRemain;
                                
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $i);
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $mAgent->first_name);
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $CUSTOMER_MODEL[$customer_id]->first_name);
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $materialCode);
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $materialName);
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $qty);
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $Qty2Kg);
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $remainKg);
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $totalAfterRemain);
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $price);
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $amount);
                                
                                $sumAgent += $qty;
                                $sumAgentKg += $Qty2Kg;
                                $row++;
                                $i++;
                            endforeach;// end foreach $aMaterialsId
                        endforeach;// end $aMaterialsTypeId
                    endforeach;// end $OUTPUT[$mAgent->id]
                    $objPHPExcel->getActiveSheet()
                        ->getStyle(MyFunctionCustom::columnName(1).($row).':'.MyFunctionCustom::columnName(15).($row))
                        ->getFont()
                        ->setBold(true);
//                    $objPHPExcel->getActiveSheet()->mergeCells("A$row:D$row");
                    $objPHPExcel->getActiveSheet()->setCellValue("E$row", "Tổng cộng");
                    $objPHPExcel->getActiveSheet()->setCellValue("F$row", $sumAgent);
                    $objPHPExcel->getActiveSheet()->setCellValue("G$row", $sumAgentKg);
                    $objPHPExcel->getActiveSheet()->setCellValue("H$row", $sumRemainKg);
                    $objPHPExcel->getActiveSheet()->setCellValue("I$row", $sumTotalAfterRemainKg);
                    $objPHPExcel->getActiveSheet()->setCellValue("K$row", $sumAgentAmount);
                    $row++;
                    $i++;
                endforeach;// end foreach($aModelAgent
            endforeach;// end foreach($AGENT_MODEL
        }
    }
    
    // Anh Dung Jul 29, 2016
    public static function OutputCar($model, $aData){
      try{
        Yii::import('application.extensions.vendors.PHPExcel',true);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("NguyenDung")
                                        ->setLastModifiedBy("NguyenDung")
                                        ->setTitle('OutputCar')
                                        ->setSubject("Office 2007 XLSX Document")
                                        ->setDescription("OutputCar")
                                        ->setKeywords("office 2007 openxml php")
                                        ->setCategory("Gas");
        $row=1;
        $i=1;
        $cmsFormatter = new CmsFormatter();
        // 1.sheet 1 Đại Lý
        $objPHPExcel->setActiveSheetIndex(0);		
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12); 
        $objPHPExcel->getActiveSheet()->setTitle('Báo cáo sản lượng xe tải '); 
        $objPHPExcel->getActiveSheet()->setCellValue("A$row", "Báo cáo sản lượng từ $model->date_from đến $model->date_to");
        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                            ->setBold(true);    			
        $objPHPExcel->getActiveSheet()->mergeCells("A$row:H$row");
        $row++;
        $index=1;
        $beginBorder = $row;
        
//        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'SN');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Ngày');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Số Xe');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Nhân Viên');
        foreach(GasMaterialsType::$ARR_GAS_VO as $materials_type => $typeName){
            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $typeName);
        }
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Mã Thẻ Kho');
        $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", 'Tỉnh');
        $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
        $index--;

        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A$row:".MyFunctionCustom::columnName($index).$row)->getFont()
                            ->setBold(true);    	
        $row++;
        self::OutputCarBody($model, $aData, $objPHPExcel, $row, $i);
        $objPHPExcel->getActiveSheet()->getStyle("B$beginBorder:N".$row)
            ->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);

        $row--;
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("D$beginBorder:K".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objPHPExcel->getActiveSheet()
                                ->getStyle("D$beginBorder:K".$row)->getNumberFormat()
                                ->setFormatCode('#,##0'); 
        $objPHPExcel->getActiveSheet()->getStyle("A$beginBorder:".MyFunctionCustom::columnName($index).$row)
                        ->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        //save file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        for($level=ob_get_level();$level>0;--$level)
        {
                @ob_end_clean();
        }
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-type: '.'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.'OutputCar'.'.'.'xlsx'.'"');

        header('Cache-Control: max-age=0');				
        $objWriter->save('php://output');			
        Yii::app()->end();
        }catch (Exception $e){
            MyFormat::catchAllException($e);
        }
    }
    
    /**
     * @Author: ANH DUNG Jul 29, 2016
     * @Todo: handle body of excel OutputCar
     */
    public static function OutputCarBody($model, $aData, &$objPHPExcel, &$row, &$i) {
        $OUTPUT_CAR     = isset($aData['OUTPUT_CAR'])?$aData['OUTPUT_CAR']:array();
        $STORECARD_NO   = isset($aData['STORECARD_NO'])?$aData['STORECARD_NO']:array();
        $MODEL_USER     = isset($aData['MODEL_USER'])?$aData['MODEL_USER']:array();
        $PROVINCE_ID    = isset($aData['PROVINCE_ID'])?$aData['PROVINCE_ID']:array();
        $mAppCache      = new AppCache();
        $aProvice       = $mAppCache->getListdata('GasProvince', AppCache::LISTDATA_PROVINCE);
        $aDays = MyFormat::getArrayDay($model->date_from_ymd, $model->date_to_ymd);
        foreach($model->getArrRoadRoute() as $roadRoute => $roadRouteText){
            $objPHPExcel->getActiveSheet()->setCellValue("A$row", $roadRouteText);
            $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()
                                ->setBold(true);
            $objPHPExcel->getActiveSheet()->mergeCells("A$row:C$row");
            $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
            $row++;
            foreach($aDays as $day){
                if(!isset($OUTPUT_CAR[$roadRoute][$day])){
                    continue ;
                }
                $dayVn = MyFormat::dateConverYmdToDmy($day);
                foreach($OUTPUT_CAR[$roadRoute][$day] as $car_id => $aCarInfo){
                    $carName    = $MODEL_USER[$car_id]->getFullName();
                    foreach($aCarInfo as $storecardId => $aQtyEm){
                        $key = 0;
                        foreach($aQtyEm['employee'] as $uid){
                            $em = $MODEL_USER[$uid]->getFullName();
                            $provinceName = '';
                            if(isset($PROVINCE_ID[$storecardId])){
                                $provinceName = isset($aProvice[$PROVINCE_ID[$storecardId]]) ? $aProvice[$PROVINCE_ID[$storecardId]] : '';
                            }
                            $index=1;
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $dayVn);
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $carName);
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $em);
                            foreach(GasMaterialsType::$ARR_GAS_VO as $materials_type => $typeName){
                                $qty = isset($aQtyEm['qty'][$materials_type]) ? $aQtyEm['qty'][$materials_type] : "";
                                if($key != 0 ){
                                    $qty = "";
                                }
                                $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $qty);
                            }
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $STORECARD_NO[$storecardId]);
                            $objPHPExcel->getActiveSheet()->setCellValue(MyFunctionCustom::columnName($index++)."$row", $provinceName);
                            $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(30);
                            $row++;
                            $key++;
                        }
                    }
                }
            }// end foreach($aDays as $day){
        }// end foreach($model->getArrRoadRoute()
        
    }
    
    
}
?>