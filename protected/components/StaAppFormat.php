<?php // Apr 22, 2017 sử dụng để format dữ liệu đẩy xuống app
class StaAppFormat
{
    public $sumTypeQty = 0, $sumTypeAmount = 0, $sumTypeDiscount = 0, $sumTypeBuVo = 0, $sumTypePromotionAmount = 0,  $sumTypeGasRemainAmount = 0, $sumTypeRevenue = 0;
    public $agent_id = 0, $materials_type_id = 0;
    /**
     * @Author: ANH DUNG Apr 22, 2017
     * @Todo: format view report tồn kho trên appp
     * $model is model GasStoreCard
     */
    public static function calcStoreMovement($model, $aRes){
    try{
        /* exampe data
         * record => [rows] => [
         *  name, begin, in, out, end, data => array(name, begin, in, out, end, data)
         * ]
         */
        $appData = [];$appData['rows'] = [];
        $aMaterials     = $aRes['aMaterials'];
        $OPENING_BALANCE_YEAR_BEFORE = $aRes['OPENING_BALANCE_YEAR_BEFORE'];
        $OPENING_IMPORT = $aRes['OPENING_IMPORT'];
        $OPENING_EXPORT = $aRes['OPENING_EXPORT'];
        $IMPORT         = isset($aRes['IMPORT']) ? $aRes['IMPORT'] : [];
        $EXPORT         = isset($aRes['EXPORT']) ? $aRes['EXPORT'] : [];
        
        foreach($aMaterials as $key=>$obj):
            $objParent      = $obj['parent_obj'];
            $objSubModel    = $obj['sub_arr_model']; // is array $key=>$mMaterial sub
            $parentBegin    = $parentImport = $parentExport = $parentEnd = 0;
            $aDataSub       = [];
            foreach($objSubModel as $key=>$mMaterial):
                // tồn đầu
                $OpeningYear        = isset($OPENING_BALANCE_YEAR_BEFORE[$mMaterial->id])?$OPENING_BALANCE_YEAR_BEFORE[$mMaterial->id]:0;
                $OpeningYearImport  = isset($OPENING_IMPORT[$mMaterial->id])?$OPENING_IMPORT[$mMaterial->id]:0;
                $OpeningYearExport  = isset($OPENING_EXPORT[$mMaterial->id])?$OPENING_EXPORT[$mMaterial->id]:0;
                $OpeningStock       = $OpeningYear+$OpeningYearImport-$OpeningYearExport;
                // Nhập xuất kho
                $totalImport = isset($IMPORT[$mMaterial->id])?$IMPORT[$mMaterial->id]:0;
                $totalExport = isset($EXPORT[$mMaterial->id])?$EXPORT[$mMaterial->id]:0;
                if($OpeningStock==0 && $totalImport==0 && $totalExport==0)
                    continue;
                // Tồn cuối $remain
                $remain = $OpeningStock+$totalImport-$totalExport;

                $parentBegin    += $OpeningStock;
                $parentImport   += $totalImport;
                $parentExport   += $totalExport;
                $parentEnd      += $remain;

                $temp           = [];
                $temp['name']   = $mMaterial->materials_no.' - '.$mMaterial->name;
                $temp['begin']  = ActiveRecord::formatCurrency($OpeningStock);
                $temp['in']     = ActiveRecord::formatCurrency($totalImport);
                $temp['out']    = ActiveRecord::formatCurrency($totalExport);
                $temp['end']    = ActiveRecord::formatCurrency($remain);
                $temp['data']   = [];
                $aDataSub[]     = $temp;
            endforeach;// end foreach($objSubModel
            
            if($parentBegin==0&&$parentImport==0&&$parentExport==0){
                continue ;
            }
            $temp           = [];
            $temp['name']   = $obj['parent_obj']->name;
            $temp['begin']  = ActiveRecord::formatCurrency($parentBegin);
            $temp['in']     = ActiveRecord::formatCurrency($parentImport);
            $temp['out']    = ActiveRecord::formatCurrency($parentExport);
            $temp['end']    = ActiveRecord::formatCurrency($parentEnd);
            $temp['data']   = $aDataSub;
            if(in_array($obj['parent_obj']->materials_type_id, GasMaterialsType::$SETUP_PRICE_GAS_12) ){
                array_unshift($appData['rows'] , $temp);// đưa gas lên đầu report
            }else{
                $appData['rows'][] = $temp;
            }
        endforeach;// end foreach($aMaterials
        return $appData;
    }catch (Exception $e){
        MyFormat::catchAllException($e);
    }
    }
    
    /**
     * @Author: ANH DUNG Apr 24, 2017
     * @Todo: format view report hgd in range date
     * $mSell is model Sell $mSell
     * /* exampe data
         * record => [rows] => [
         *  name, qty, price, amount, discount, bu_vo, revenue => array(name, qty, price, amount, discount, bu_vo, revenue
         * ]
     */
    public static function hgd(&$mSell, $aData){
    try{
    $sumQty = $sumAmount = $sumDiscount = $sumRevenue = $sumBuVo = $sumPromotionAmount = $sumGasRemainAmount = 0;
    $sumGas = $sumVo = $sumKm = $sumByType = 0;
//    $aMaterialType = GasMaterialsType::getAllItem();
    $mAppCache      = new AppCache();
    $aMaterialType = $mAppCache->getListdata('GasMaterialsType', AppCache::LISTDATA_MATERIAL_TYPE);

    $aOrderType     = $mSell->getArrayOrderType();
    unset($aOrderType[Sell::ORDER_TYPE_NORMAL]);
    $OUTPUT_OTHER   = isset($aData['OUTPUT_OTHER']) ? $aData['OUTPUT_OTHER'] : [];
    $aSumOrderType  = $appData = []; $appData['rows'] = [];
    $aMaterial      = $mAppCache->getListdata('GasMaterials', AppCache::LISTDATA_MATERIAL);
    // name, qty, price, amount, discount, bu_vo, revenue
    if(!isset($aData['OUTPUT'])){
        return ['sum_order_type'=> null, 'total_revenue'=> null, 'sum_order_type' => null, 'rows'=>null];
    }
    foreach ($aData['OUTPUT'] as $agent_id => $aInfo1):
        foreach ($aInfo1 as $materials_type_id => $aInfo2):
            $sumTypeQty = $sumTypeAmount= $sumTypeDiscount = $sumTypeRevenue = $sumTypeQty = $sumTypeBuVo = $sumTypePromotionAmount = $sumTypeGasRemainAmount = 0;
            $aDataSub = [];
            foreach ($aInfo2 as $materials_id => $aInfo3):
                $amount_bu_vo       = $aInfo3['amount_bu_vo'] > 0 ? ActiveRecord::formatCurrency($aInfo3['amount_bu_vo']) : '';
                $promotionAmount    = $aInfo3['promotion_amount'] > 0 ? ActiveRecord::formatCurrency($aInfo3['promotion_amount']) : '';
                $gasRemainAmount    = $aInfo3['gas_remain_amount'] > 0 ? ActiveRecord::formatCurrency($aInfo3['gas_remain_amount']) : '';
                $qty            = $aInfo3['qty'] > 0 ? ActiveRecord::formatCurrency($aInfo3['qty']) : '';
                $price          = $aInfo3['price'] > 0 ? ActiveRecord::formatCurrency($aInfo3['price']) : '';
                $amount         = $aInfo3['amount'] > 0 ? ActiveRecord::formatCurrencyRound($aInfo3['amount']) : '';
                $qty_discount   = $aInfo3['qty_discount'] > 0 ? ActiveRecord::formatCurrency($aInfo3['qty_discount']) : '';
                $discountAmount = $aInfo3['qty_discount'] * $mSell->getPromotionDiscount();
                $discountAmount += $aInfo3['amount_discount'] > 9 ? $aInfo3['amount_discount'] : 0;// Sep 04, 2016 thêm cho phần sửa CK
                $discountAmount = round($discountAmount, -3);
                // http://stackoverflow.com/questions/5150001/how-to-round-to-nearest-thousand
                $revenue            = $aInfo3['amount'] + $aInfo3['amount_bu_vo']- $discountAmount - $aInfo3['promotion_amount'] - $aInfo3['gas_remain_amount'];
                $sumQty             += $aInfo3['qty'];
                $sumAmount          += $aInfo3['amount'];
                $sumDiscount        += $discountAmount;
                $sumRevenue         += $revenue;
                $sumBuVo            += $aInfo3['amount_bu_vo'];
                $sumPromotionAmount += $aInfo3['promotion_amount'];
                $sumGasRemainAmount += $aInfo3['gas_remain_amount'];

                $sumTypeQty         += $aInfo3['qty'];
                $sumTypeAmount      += $aInfo3['amount'];
                $sumTypeDiscount    += $discountAmount;
                $sumTypeRevenue     += $revenue;
                $sumTypeBuVo        += $aInfo3['amount_bu_vo'];
                $sumTypePromotionAmount         += $aInfo3['promotion_amount'];
                $sumTypeGasRemainAmount         += $aInfo3['gas_remain_amount'];

//                $revenue        = $revenue > 0 ? ActiveRecord::formatCurrencyRound($revenue) : '' ;// Close on Mar1518 show số âm trừ gas dư lên app GN
                $revenue        = ActiveRecord::formatCurrencyRound($revenue);
                $discountAmount = $discountAmount > 0 ? ActiveRecord::formatCurrencyRound($discountAmount) : '' ;

                if(in_array($materials_type_id, GasMaterialsType::$ARR_WINDOW_VO)){
                    $sumVo += $aInfo3['qty'];
                }elseif(in_array($materials_type_id, GasMaterialsType::$ARR_WINDOW_PROMOTION)){
                    $sumKm += $aInfo3['qty'];
                }else{ // for Gas and other material val, day, vt khac
                    $sumGas += $aInfo3['qty'];
                }
                
                $temp                   = [];
                $temp['name']           = isset($aMaterial[$materials_id]) ?  $aMaterial[$materials_id] : '';
                $temp['qty']            = $qty;
                $temp['price']          = $price;
                $temp['amount']         = $amount;
                $temp['discount']       = $discountAmount;
                $temp['bu_vo']              = $amount_bu_vo;
                $temp['gas_remain_amount']  = $gasRemainAmount;
                $temp['revenue']        = $revenue;
                $temp['data']           = [];
                $aDataSub[]             = $temp;
                foreach ($aOrderType as $order_type => $order_type_text):
                    $order_type_amount =  '';
                    if(isset($OUTPUT_OTHER[$agent_id][$materials_id][$order_type])){
                        $order_type_amount =  ActiveRecord::formatCurrencyRound($OUTPUT_OTHER[$agent_id][$materials_id][$order_type]);
                        if(!isset($aSumOrderType[$agent_id][$order_type])){
                            $aSumOrderType[$agent_id][$order_type]     = $OUTPUT_OTHER[$agent_id][$materials_id][$order_type]*1;
                        }else{
                            $aSumOrderType[$agent_id][$order_type]     += $OUTPUT_OTHER[$agent_id][$materials_id][$order_type]*1;
                        }
                    }
                endforeach;// end foreach ($aOrderType 
            endforeach;// end foreach ($aInfo2

            /* begin sum material type
             *  Mar1518 xử lý chia 2 row với sum Type vỏ 12kg, vì có gas dư của app hgd
             */
//$ = 0, $ = 0, $ = 0, $ = 0, $ = 0,  $ = 0;
            $mStaApp = new StaAppFormat();
            $mStaApp->agent_id                  = $agent_id;
            $mStaApp->materials_type_id         = $materials_type_id;
            $mStaApp->sumTypeQty                = $sumTypeQty;
            $mStaApp->sumTypeAmount             = $sumTypeAmount;
            $mStaApp->sumTypeDiscount           = $sumTypeDiscount;
            $mStaApp->sumTypeBuVo               = $sumTypeBuVo;
            $mStaApp->sumTypePromotionAmount    = $sumTypePromotionAmount;
            $mStaApp->sumTypeGasRemainAmount    = $sumTypeGasRemainAmount;
            $mStaApp->sumTypeRevenue            = $sumTypeRevenue;
            $typeVo12 = GasCashBookDetail::VO_12_BU;// Mar1518 xử lý cho vỏ 12 có thu bù vỏ + chi gas dư
            
            if($materials_type_id == GasMaterialsType::MATERIAL_VO_12):
                $mStaApp->hgdRowSumMaterialType($mSell, $appData, $aMaterialType, $aDataSub, GasCashBookDetail::VO_12_BU);
                $mStaApp->hgdRowSumMaterialType($mSell, $appData, $aMaterialType, $aDataSub, GasCashBookDetail::VO_12_GAS_REMAIN);
            else:
                $mStaApp->hgdRowSumMaterialType($mSell, $appData, $aMaterialType, $aDataSub, $typeVo12);
            endif;
            // end  sum material type
            
        endforeach;// end foreach ($aInfo1
    endforeach;// end foreach ($aData['OUTPUT']
    // begin row sum all
    StaAppFormat::hgdRowSumAll($mSell, $appData, $sumQty, $sumAmount, $sumDiscount, $sumBuVo, $sumPromotionAmount, $sumRevenue);
    
    $appData['sum_order_type'] = [];
    $sumByType = StaAppFormat::hgdRowByType($mSell, $appData, $aSumOrderType, $aOrderType);
    
    $appData['total_revenue'] = ActiveRecord::formatCurrencyRound($sumRevenue+$sumByType);
    return $appData;
    
    }catch (Exception $e){
        MyFormat::catchAllException($e);
    }
    }
    
    /** @Author: ANH DUNG May 10, 2017
     *  @Todo: add row by type: row loại bán hàng */
    public static function hgdRowByType(&$mSell, &$appData, $aSumOrderType, $aOrderType) {
        // begin row 
        $sumByType = 0;
        foreach($aSumOrderType as $agent_id => $aSumOrderTypeData):
            foreach ($aOrderType as $order_type => $order_type_text):
                $amount = 0;
                if(isset($aSumOrderTypeData[$order_type])){
                    $amount = $aSumOrderTypeData[$order_type];
                }
                if(empty($amount)) continue;

                if($order_type == Sell::ORDER_TYPE_THU_VO){
                    $sumByType -= $amount;
                }else{
                    $sumByType += $amount;
                }

                $temp               = [];
                $temp['name']       = $order_type_text;
                $temp['qty']        = '';
                $temp['price']      = '';
                $temp['amount']     = '';
                $temp['discount']   = '';
                $temp['bu_vo']      = '';
                $temp['revenue']    = ActiveRecord::formatCurrencyRound($amount);
                $temp['data']       = [];
                $appData['sum_order_type'][] = $temp;

                if($mSell->getDataInsert){// xử lý build array để insert cashbook detail, tách riêng với bc quỹ trên app GN
                    $mCashBookDetail = new GasCashBookDetail();
                    $mCashBookDetail->typeItem              = GasCashBookDetail::TYPE_ITEM_SELL_TYPE;
                    $mCashBookDetail->agent_id              = $agent_id;
                    $mCashBookDetail->release_date          = $mSell->date_from_ymd;
                    $mCashBookDetail->employee_id           = $mSell->employee_maintain_id;
                    $mCashBookDetail->qty                   = 0;
                    $mCashBookDetail->amount                = $amount;
                    $mCashBookDetail->materials_type_id     = $order_type;
                    $mCashBookDetail->materials_type_name   = $temp['name'];
                    
//                    $sInsert = GasCashBookDetail::makeRecordHgd($agent_id, $mSell->date_from_ymd, $order_type, $temp['name'], 0, $amount, $mSell->employee_maintain_id, GasCashBookDetail::TYPE_ITEM_SELL_TYPE);
                    $sInsert = $mCashBookDetail->makeRecordHgd();
                    if(!empty($sInsert)){
                        $mSell->aRowInsert[] = $sInsert;
                    }
                }
            endforeach;// end foreach ($aOrderType 
        endforeach;// end foreach($aSumOrderType
        
        
        return $sumByType;
    }
    
    public static function hgdRowSumAll(&$mSell, &$appData, $sumQty, $sumAmount, $sumDiscount, $sumBuVo, $sumPromotionAmount, $sumRevenue) {
        $temp               = [];
        $temp['name']       = 'Sum all';
        $temp['qty']        = ActiveRecord::formatCurrency($sumQty);
        $temp['price']      = '';
        $temp['amount']     = ActiveRecord::formatCurrencyRound($sumAmount);
        $temp['discount']   = $sumDiscount > 0 ? ActiveRecord::formatCurrencyRound($sumDiscount) : '';
        // Sep2217 có thể chỗ bu_vo và promotion_amount sử dụng khi sinh row ở cashbook chứ ko trả xuống app report
        $temp['bu_vo']                  = $sumBuVo > 0 ? ActiveRecord::formatCurrencyRound($sumBuVo) : '';
        $temp['promotion_amount']       = $sumPromotionAmount > 0 ? ActiveRecord::formatCurrency($sumPromotionAmount):'';
        $temp['revenue']    = ActiveRecord::formatCurrencyRound($sumRevenue);
        $temp['data']       = [];
        $appData['sum_all'][] = $temp;
        // begin row sum all
    }
//    ($mSell, $appData, $aMaterialType, $aDataSub)
    public function hgdRowSumMaterialType(&$mSell, &$appData, $aMaterialType, $aDataSub, $typeVo12) {
        $temp               = [];// one row on app
        $temp['name']       = $aMaterialType[$this->materials_type_id];
        $temp['qty']        = ActiveRecord::formatCurrency($this->sumTypeQty);
        $temp['price']      = '';
        $temp['amount']     = $this->sumTypeAmount > 0 ? ActiveRecord::formatCurrencyRound($this->sumTypeAmount) : '';
        $temp['discount']   = $this->sumTypeDiscount > 0 ? ActiveRecord::formatCurrency($this->sumTypeDiscount):'';
        // Sep2217 có thể chỗ bu_vo và promotion_amount sử dụng khi sinh row ở cashbook chứ ko trả xuống app report
        $temp['bu_vo']      = $this->sumTypeBuVo > 0 ? ActiveRecord::formatCurrency($this->sumTypeBuVo):'';
        $temp['promotion_amount']      = $this->sumTypePromotionAmount > 0 ? ActiveRecord::formatCurrency($this->sumTypePromotionAmount):'';
//        $temp['revenue']    = $sumTypeRevenue > 0 ? ActiveRecord::formatCurrencyRound($sumTypeRevenue):'';
        $temp['revenue']    = ActiveRecord::formatCurrencyRound($this->sumTypeRevenue);// Close on Mar1518 show số âm trừ gas dư lên app GN
        $temp['data']       = $aDataSub;
        if($typeVo12 != GasCashBookDetail::VO_12_GAS_REMAIN){
            if(in_array($this->materials_type_id, GasMaterialsType::$SETUP_PRICE_GAS_12) ){
                array_unshift($appData['rows'], $temp);
            }else{// đưa gas lên đầu report
                $appData['rows'][] = $temp;
            }
        }
        
        if($mSell->getDataInsert){
            $mCashBookDetail = new GasCashBookDetail();
            $mCashBookDetail->typeItem              = GasCashBookDetail::TYPE_ITEM_NORMAL;
            $mCashBookDetail->agent_id              = $this->agent_id;
            $mCashBookDetail->release_date          = $mSell->date_from_ymd;
            $mCashBookDetail->employee_id           = $mSell->employee_maintain_id;
            $mCashBookDetail->qty                   = $this->sumTypeQty;
            $mCashBookDetail->amount                = $this->sumTypeRevenue;
            $mCashBookDetail->materials_type_id     = $this->materials_type_id;
            $mCashBookDetail->materials_type_name   = $temp['name'];
            if($this->materials_type_id == GasMaterialsType::MATERIAL_VO_12 && $typeVo12 == GasCashBookDetail::VO_12_BU){
                $mCashBookDetail->amount            = $this->sumTypeBuVo;
            }elseif($this->materials_type_id == GasMaterialsType::MATERIAL_VO_12 && $typeVo12 == GasCashBookDetail::VO_12_GAS_REMAIN){
                $mCashBookDetail->amount            = $this->sumTypeGasRemainAmount;
                $mCashBookDetail->typeItem          = GasCashBookDetail::TYPE_ITEM_GAS_REMAIN;
            }
            
//            $sInsert = GasCashBookDetail::makeRecordHgd($this->agent_id, $mSell->date_from_ymd, 
// $materials_type_id, $temp['name'], $sumTypeQty, $sumTypeRevenue, $mSell->employee_maintain_id, GasCashBookDetail::TYPE_ITEM_NORMAL);
            $sInsert = $mCashBookDetail->makeRecordHgd();
            if(!empty($sInsert)){
                $mSell->aRowInsert[] = $sInsert;
            }
        }
    }
    
    /**
     * @Author: ANH DUNG May 11, 2017
     * @Todo: format view report hgd to date
     * $mSell is model Sell $mCashbookDetail
     * /* exampe data
         * record => [rows] => [
         *  name, customer_name, qty, in, out, end
         * ]
     */
    public static function cashbook($mCashbookDetail){
    try{
        $mAppCache              = new AppCache();
        $aEmployee              = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MAINTAIN);
        $aCashbookType          = $mAppCache->getModelTypeLookup(GasMasterLookup::TYPE_CASHBOOK);
        $openingBalance         = $mCashbookDetail->getOpeningBalanceOfAgent();
        $aModelCashBookDetail   = $mCashbookDetail->getDaily();
        $appData = []; $appData['rows'] = [];
        $appData['opening_balance'] = 'Dư đầu kỳ: '.ActiveRecord::formatCurrency($openingBalance);
//        $appData['opening_balance'] = 'Dư đầu kỳ: 15,000,000';
        foreach($aModelCashBookDetail as $key=>$item):
            $employeeName = isset($aEmployee[$item->employee_id]) ? $aEmployee[$item->employee_id] : $item->name_employee;
            $amount = $item->amount > 0 ? ActiveRecord::formatCurrency($item->amount) : '';
            $in_text = $out_text = '';
            if($item->type==MASTER_TYPE_REVENUE){ // thu
                $openingBalance += $item->amount;
                $in_text = $amount;
            }elseif($item->type==MASTER_TYPE_COST){ // chi
                $openingBalance -= $item->amount;
                $out_text = $amount;
            }
            $typeName = isset($aCashbookType[$item->master_lookup_id]) ? $aCashbookType[$item->master_lookup_id]['name'] : '';
            
            $temp                   = [];
            $temp['name']           = $typeName;
            $temp['customer_name']  = $item->getCustomer('first_name');
            $temp['qty']            = $item->qty > 0 ? $item->qty : '';
            $temp['in']             = $in_text;
            $temp['out']            = $out_text;
            $temp['end']            = ActiveRecord::formatCurrency($openingBalance);
            $appData['rows'][]      = $temp;
        endforeach;
        $appData['ending_balance'] = 'Quỹ tiền mặt: '.ActiveRecord::formatCurrency($openingBalance);
        return $appData; 
    }catch (Exception $e){
        MyFormat::catchAllException($e);
    }
    }
    
}
