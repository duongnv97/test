<?php
/** Aug 13, 2016 Class for communication with other platform */
class GateApi
{
    /**
     * @Author: ANH DUNG Aug 13, 2016
     * @Todo: get json string tọa độ google map của agent
     */
    public function getLocationAgent($needMore = array()) {
        $mGasRemain     = new GasRemain();
        $mAppCache      = new AppCache();
        if(isset($needMore['listAgent'])){
            $aAgent = $needMore['listAgent'];
        }else{
            $company_id = isset($_GET['Users']['parent_id'])?$_GET['Users']['parent_id']:0;
            $aAgent = Users::getArrObjectUserByRole(ROLE_AGENT, array(), array('parent_id'=>$company_id));
        }
        
        $index = 1; $aDataChart = $aDataApp = []; $company_gas24h = 930409;
        $aAgentRadius2Km = [
            1086796,// Đại Lý Nguyễn Văn Cừ
            1054994,// Đại Lý Lê Duẩn
        ];
        $aAgentRadiusHide = MapLib::getAgentHideLocation();
        $aProvince2km   = [GasProvince::TINH_BINH_DUONG, GasProvince::TINH_DONG_NAI];
        $gasBrand       = GasProfile::getListMaterialForMap();
        $aProvince      = $mAppCache->getListdata('GasProvince', AppCache::LISTDATA_PROVINCE);
        
        foreach($aAgent as $mAgent){
            $tempApp = [];
            $radius = $mAgent->price_other*1;// in meters
//            if($mAgent->province_id == GasProvince::TP_HCM || in_array($mAgent->id, $aAgentRadius2Km)
//                || in_array($mAgent->province_id, $aProvince2km)
//            ){
//                $radius = Yii::app()->params['distanceAgentApp']*1;// in meters
//            }// close on Mar1718 không load cứng radius nữa, mà load theo từng agent
            
            if(in_array($mAgent->id, $aAgentRadiusHide) ){
                $radius = 10;// in meters
            }
            
            if(trim($mAgent->slug) != ""){
                $tmp = explode(",", $mAgent->slug);
                if(count($tmp) == 2){
                    $brand = isset($gasBrand[$mAgent->id]) ? $gasBrand[$mAgent->id]['BrandName']:"";
                    $contentString = "<div id='content'>";
                    $contentString .= "<h1 id='firstHeading' class='firstHeading'>{$mAgent->getFullName()}</h1>";
                        $contentString .= "<div id='bodyContent'>";
                            $contentString .= "<p><b>Chủ cơ sở:</b> {$mAgent->getCreatedBy()}</p>";
                            $contentString .= "<p><b>Địa chỉ:</b> {$mAgent->getAddress()}</p>";
                            $contentString .= "<p><b>Phone:</b> {$mAgent->getPhone()}</p>";
                            $contentString .= "<p><b>Web:</b> <a href='http://daukhimiennam.com' target='_blank'>www.daukhimiennam.com</a></p>";
                            $contentString .= "<p><b>Thương hiệu:</b> $brand</p>";
                        $contentString .= "</div>";
                    $contentString .= "</div>";
                    $company_id = $mAgent->parent_id;
                    if(isset($needMore['GetLogo24h'])){
                        $company_id = $company_gas24h;
                    }
                    $source = "/upload/company_logo_map/map_logo_$company_id.png";
                    if (!file_exists(Yii::getPathOfAlias("webroot") . $source)){
                        $source = "/upload/company_logo_map/map_logo_default.png";
                    }
                    if(array_key_exists($mAgent->id, $mGasRemain->getArrayAgentCar())){
                        $source = "/upload/company_logo_map/icon_car.png";
                    }

                    $icon = Yii::app()->createAbsoluteUrl("/").$source;
                    $aDataChart[]   = array($contentString, $tmp[0], $tmp[1], $index++, $icon, $radius, $mAgent->id, $mAgent->getFullName());
                
                    $tempApp['name']        = $mAgent->getFullName();
                    $tempApp['address']     = $mAgent->getAddress();
                    $tempApp['lat']         = (double)$tmp[0];
                    $tempApp['long']        = (double)$tmp[1];
                    $tempApp['radius']      = $radius;
                    $tempApp['province_id']     = $mAgent->province_id;
                    $tempApp['province_name']   = isset($aProvince[$mAgent->province_id]) ? $aProvince[$mAgent->province_id] : '';
                    $aDataApp[] = $tempApp;
                }
            }
        }
        if(isset($needMore['GetArray'])){
            return $aDataChart;
        }
        if(isset($needMore['GetAppMap'])){
            return $aDataApp;
        }
        return json_encode($aDataChart); 
    }
   

}
