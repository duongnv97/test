<?php
/** @Author: LocNV Jul 9, 2019
 *  @Todo: Gas file picture list
 *  @Param: 
 **/
class UploadMultiFileWidget extends CWidget
{
    public $model = null, $title_col_file = '', $field_name = '';
    public $relations = '', $onlyView = false, $type_gas_file = '';

    public function run()
    {        
        $this->render(
                'UploadMultiFile/uploadMultiFile',
                array(
                    'model'             => $this->model,
                    'relations'         => $this->relations,
                    'title_col_file'    => $this->title_col_file,
                    'field_name'        => $this->field_name,
                    'type_gas_file'     => $this->type_gas_file,
                    'model_name'        => get_class($this->model)
                    )
                );
    }
}

