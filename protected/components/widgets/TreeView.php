<?php
class TreeView extends CWidget
{
    const VIEW_TREE     = 1;
    const VIEW_TABLE    = 2;
    
    public $model       = null, $aData=[];
    public $typeView    = 1;
    public function run()
    {   
        if($this->typeView == TreeView::VIEW_TREE){
            $this->render('TreeView/TreeView', array('model'=>$this->model));
        }elseif($this->typeView == TreeView::VIEW_TABLE){
            $this->render('TreeView/TableView', array('model'=>$this->model, 'aData' => $this->aData));
        }
    }
}