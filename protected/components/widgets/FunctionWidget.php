<?php
/** @Author: HOANG NAM 16/04/2018
 *  @Todo: function hr widget
 *  @Param: 
 **/
class FunctionWidget extends CWidget
{
    public $relation,$role_id,$model,$field_name;
    
    public function run()
    {        
        $this->render('function/_form_function', array('model'=>$this->model,'field_name'=>$this->field_name,'relation'=>$this->relation,'role_id'=>$this->role_id));
    }
}