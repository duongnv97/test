<?php
/** @Author: NVDuong Jun 19, 2018
 *  @Todo: tabs widget hr
 *  @Param:
 **/
class TabWidget extends CWidget
{
    public $ajax_url, $params_url, $aTab, $dataName, $aDataTab;
    public $title, $aView, $ajax, $class, $tabId, $dataValue, $aNeedMore;
    
    public function run()
    {        
        $this->render('Tab/tab', array(
            'ajax_url'=>$this->ajax_url,
            'params_url'=>$this->params_url,
            'aTab'=>$this->aTab,
            'aDataTab'=>$this->aDataTab,
            'dataName'=>$this->dataName,
            'dataValue'=>$this->dataValue,
            'title'=>$this->title,
            'aView'=>$this->aView,
            'ajax'=>$this->ajax,
            'class'=>$this->class,
            'tabId'=>$this->tabId,
            'aNeedMore'=>$this->aNeedMore,  
        ));
    }
}