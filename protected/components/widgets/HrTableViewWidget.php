<?php
/** @Author: DuongNV Mar 29, 2019
 *  @Todo: Xem bảng lương
 *  @Param: 
 **/
class HrTableViewWidget extends CWidget
{
    public $data, $fixedHeader = false, $enableScript = true, $sumRowBottom = true;
    
    public function run()
    {        
        $this->render('HrTableViewWidget/index', array(
            'data'          => $this->data,
            'fixedHeader'   => $this->fixedHeader, // set true if tính lương (Bảng lương cho thủ quỹ chi)
            'enableScript'  => $this->enableScript, // Nếu render nhiều bảng thì set false, cho đoạn script vào view chính
            'sumRowBottom'  => $this->sumRowBottom, // Hàng tổng dưới
        ));
    }
}