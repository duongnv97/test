<?php
/** @Author: DuongNV Mar 29, 2019
 *  @Todo: Xem bảng lương để xếp duyệt
 *  @Param: 
 **/
class HrReviewWidget extends CWidget
{
    public $model;
    
    public function run()
    {        
        $mApprove   = new HrApprovedReports();
        if( $mApprove->isTreasurer() ){ // Nếu là thủ quỹ sẽ có giao diện riêng
            $this->render('HrReviewWidget/indexForTreasurer', array(
                'model' => $this->model,
            ));
        } else {
            $this->render('HrReviewWidget/index', array(
                'model' => $this->model,
            ));
        }
    }
}