<?php
/** @Author: Pham Thanh Nghia Dec 7 ,2018
 *  @Todo:  1 dòng thống kê số đơn bảo trì của 1 NV đang login 
 @ex BT Bò mối: 5 Mới, 12 chưa HT
     BT Hộ GĐ: 3 Mới, 16 chưa HT
 **/
class ReportEmployeeUpholdWidget extends CWidget
{
    public $needMore = []; // type == [string , table]
    public $uId ,$date;
//    'uId'=> '572967',
    public function run()
    {   

        $this->render('reportEmployeeUphold/index', 
                array(

                    'needMore' => $this->needMore,
                )
        );
    }
    
    public function getDataForGasUphold(&$new,&$complete){
        $criteria=new CDbCriteria;
        if(!empty($this->uId)){
            $criteria->addCondition("t.uid_login =". $this->uId);
        }else{
            $this->uId = MyFormat::getCurrentUid();
            $criteria->addCondition("t.uid_login=".$this->uId);
        }
        if(!empty($this->date)){
            $month = date("m",strtotime($this->date));
            $year  = date("Y",strtotime($this->date));
            $criteria->addCondition("MONTH(t.created_date) ='$month'");
            $criteria->addCondition("YEAR(t.created_date) ='$year'");
        }else{
            $month = date('m');
            $year  = date('Y');
            $criteria->addCondition("MONTH(t.created_date) ='$month'");
            $criteria->addCondition("YEAR(t.created_date) ='$year'");
        }
//        $criteria->addInCondition("t.status", [GasUphold::STATUS_NEW,GasUphold::STATUS_COMPLETE]);
        $criteria->select = "t.status , count(id) as id";
        $criteria->group = "t.status ";
        $model = GasUphold::model()->findAll($criteria); // bò
        foreach ($model as $value) {
           if($value->status == GasUphold::STATUS_NEW){
               $new = $value->id;
           }
           if($value->status == GasUphold::STATUS_HANLDE){
               $complete = $value->id;
           }
        }
    }
    public function getDataForUpholdHgd(&$hgdNew,&$hgdComplete){
        $criteria=new CDbCriteria;
        if(!empty($this->uId)){
            $criteria->addCondition("t.uid_login =". $this->uId);
        }else{
            $this->uId = MyFormat::getCurrentUid();
            $criteria->addCondition("t.uid_login=".$this->uId);
        }
        if(!empty($this->date)){
            $month = date("m",strtotime($this->date));
            $year  = date("Y",strtotime($this->date));
            $criteria->addCondition("MONTH(t.created_date_only) ='$month'");
            $criteria->addCondition("YEAR(t.created_date_only) ='$year'");
//            $criteria->addCondition("t.created_date_only ='$this->date'"); 
        }else{
            $month = date('m');
            $year  = date('Y');
            $criteria->addCondition("MONTH(t.created_date_only) ='$month'");
            $criteria->addCondition("YEAR(t.created_date_only) ='$year'");
        }
//        $criteria->addInCondition("t.status", [UpholdHgd::STATUS_NEW,UpholdHgd::STATUS_COMPLETE]);
        $criteria->select = "t.status , count(id) as id";
        $criteria->group = "t.status ";
        $model = UpholdHgd::model()->findAll($criteria); // 
        foreach ($model as $value) {
           if($value->status == UpholdHgd::STATUS_NEW){
               $hgdNew = $value->id;
           }
           if($value->status == UpholdHgd::STATUS_CONFIRM){
               $hgdComplete = $value->id;
           }
        }
    }
}