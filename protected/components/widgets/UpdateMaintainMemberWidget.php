<?php

class UpdateMaintainMemberWidget extends CWidget{
    public $model;
    public $type;
    public function run(){
        $this->render('UpdateMaintainMember/updateDetail', [
            'model' =>$this->model, 
            'type'  =>$this->type
        ]);
    }
}
