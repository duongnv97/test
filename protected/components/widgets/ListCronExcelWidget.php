<?php
/** @Author: DuongNV Jan 16, 2019
 *  @Todo: quản lý file cron excel của báo cáo
 **/
class ListCronExcelWidget extends CWidget
{
    public $viewByYear      = false; // xem những file mới nhất, if true -> Xem theo 2 năm gần nhất
    public $limit           = 10; // for case ($viewByYear = false)
    public $model;
    public $nameDir = '';// DungNT Feb1019 fix for one model have multi index export excel ex: GasAppOrder, GasAppOrderDaily
    
    public function run()
    {        
        $this->render('ListCronExcel/view', array(
            'model'       => $this->model,
            'viewByYear'  => $this->viewByYear,
            'limit'       => $this->limit,
            'nameDir'     => $this->nameDir,
        ));
    }
}