<?php
/** @Author: DuongNV Aug0519
 *  @Todo: Xem CNCN
 *  @Param: 
 **/
class DebtsWidget extends CWidget
{
    public $model, $aData; // model gasDebts, $aData
    public $uid; // if is set => render search without field user_id
    
    public function run()
    {        
        $this->render('Debts/index', array(
            'model' => $this->model,
            'aData' => $this->aData,
            'uid' => $this->uid,
        ));
    }
}