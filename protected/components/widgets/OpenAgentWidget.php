<?php

/**
 * Description of OpenAgent
 *
 * @author nhh
 */
class OpenAgentWidget extends CWidget {

    public $needMore = [];
    public $mIssue, $dataProvider;

    public function run() {
        $this->render('OpenAgent/Open', array('mIssue' => $this->mIssue, 'dataProvider' => $this->dataProvider, 'needMore' => $this->needMore));
    }

}
