<?php
class EmployeeProjectWidget extends CWidget
{
    public $needMore = [];
    public $mIssue, $dataProvider;
    public function run()
    {        
        $this->render('EmployeeProject/EmployeeProject', 
                array(
                    'mIssue'=> $this->mIssue, 
                    'dataProvider'=> $this->dataProvider, 
                    'needMore' =>  $this->needMore
                )
        );
    }
}