<?php
class CustomerChangeInfoWidget extends CWidget
{
    public $needMore = [];
    public $customer_id = 0;
    public function run()
    {        
        $this->render('user/ChangeInfo', array('customer_id' => $this->customer_id, 'needMore' =>  $this->needMore));
    }
}