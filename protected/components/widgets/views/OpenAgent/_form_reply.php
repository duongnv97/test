<div class="in collapse" style="height: auto;">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
//	'id'=>'new_ticket_form',
        'action' => Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/reply', array('id' => $data->id,'view_type'=> GasIssueTickets::VIEW_TYPE_OPEN_AGENT)),
        'htmlOptions' => array('class' => 'simple_form new_reply', 'enctype' => 'multipart/form-data'),
        'clientOptions' => array(
//            'validateOnSubmit' => true,
//            'afterValidate' => 'js:function(form, attribute, data, hasError){ không nên dùng hàm này nếu ko return true thì ko submit dc  }'
        ),
    ));
    ?>

    <fieldset>
        <h3> Giai đoạn <?php echo $data->stage_current; ?></h3>
        <?php
        include '_form_renewal_date.php';
        ?>
        <!-- Mar2818 tổng G sát -> NV xử lý level 1-->
        <?php // if($ModelCreate->canViewApiListChiefMonitor()): ?>
        <?php ?>
        <?php // include '_form_reply_chief_monitor.php';  ?>
        <?php // endif;  ?>

        <!--- Mar2818 Giám sát => pháp lý level 2-->
        <?php if ($ModelCreate->canViewApiListMonitorAgent()): ?>
            <?php // if(1): ?>
            <?php include '_form_reply_monitor_agent.php'; ?>
        <?php endif; ?>
        <!--- Mar2818 accounting_id => level 3-->
        <?php if ($ModelCreate->canViewApiListAccounting()): ?>
            <?php // if(1): ?>
            <?php include '_form_reply_accounting.php'; ?>
        <?php endif; ?>

        <?php if ($ModelCreate->canWriteReply()): ?>
            <?php echo $form->textArea($ModelCreate, 'message', array('class' => '', 'cols' => 40, 'placeholder' => 'Nội dung trả lời ...')); ?>
            <div class="errorMessage l_padding_20 display_none gasErrorMsg" style="">Chưa nhập nội dung trả lời.</div>
        <?php endif; ?>

        <?php include 'index_file.php'; ?>
       
        <br>
        <input name="GasCurl" type="hidden" value="<?php echo GasCheck::getCurl(); ?>">
        <input name="GasIssueTickets[stage_current]" type="hidden" value="<?php echo $data->stage_current; ?>">
        <!--<input type="submit" value="Submit Reply" name="commit" class="button">-->            
        <input data-id='<?php echo $data->id; ?>' id='submit_ajax' type="button" value="Submit Reply" name="commit" class="button submit_ajax" style='background-color: #7dc36b;color: #fff;display: block;font-size: 16px;font-weight: 300;line-height: 16px;margin: 0 auto;text-transform: uppercase;width: 700px;'>            

    </fieldset>

    <?php $this->endWidget(); ?>    
</div><!-- <div id="new_ticket"  -->

<script type="text/javaScript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.form.js"></script>