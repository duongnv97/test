<?php $ModelCreate = new GasIssueTickets(); ?>
<h4>Post Reply</h4>
<div class="styled-form">
    <?php
    if ($data->stage_current <= GasIssueTickets::STAGE_TOTAL_OPEN_AGENT) {
        if ($data->process_status == 3 && $data->stage_current != 3 ) {
            if($data->showCreateProcessNext()){
            $link = Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/reply', array('id' => $data->id,'form_process_next'=>1));
            echo '<a class="button create_next w-200" href="' . $link . '" style="padding:5px;text-align: center;background-color: #3a8bce;color: #fff;font-size: 16px;font-weight: 300;line-height: 16px;margin: 0 auto;text-transform: uppercase;display:block">Quy trình tiếp theo</a>';
            }
        } else {
            include '_form_reply.php';
        }
    }
    ?>
</div>
