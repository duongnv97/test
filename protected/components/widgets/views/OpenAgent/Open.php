<?php 
    $idCListView = isset($needMore['idCListView']) ? $needMore['idCListView'] : 0;
    $ajaxUpdate = false;
    if($idCListView == GasTickets::STATUS_CLOSE){
        $ajaxUpdate = true;
    }
   
?>
<?php  // NGUYEN DUNG
    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => 'OpenAgent/_item_clist_view',
        'id' => 'item_clist_close'.$idCListView,
        'ajaxUpdate' => $ajaxUpdate, 
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox(); fnUpdateAjax(); }',
        'itemsCssClass'=>'',
        'pagerCssClass'=>'pager',
        'pager'=> array(
            'maxButtonCount' => 10,
            'class'=>'CLinkPager',
            'header'=> false,
            'footer'=> false,
            'id'=>'id-of-pager-ul'.$idCListView,
        ),
        'summaryText' => true,
        'summaryText'=>'Showing <strong>{start} - {end}</strong> of {count} results',
        'template'=>'{summary}{pager}<ul class="tickets clearfix">{items}</ul>{pager}{summary}',
    ));
?>