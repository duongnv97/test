<?php $cmsFormater = new CmsFormatter();
    $uidLogin = Yii::app()->user->id;
    $mTicket = $data->rIssueTickets;
    $mCustomer = $mTicket->rCustomer;
    
//    $NameCustomer = '';
//    if($mCustomer){
//        $NameCustomer = $mCustomer->first_name ;
//    }
//    $color_red = "";
//    if($mTicket->expired){
//        $color_red = "color_red";
//    }
//    $mUser = Users::model()->findByPk($uidLogin);

?>
<?php 
if($data->stage_status == 3){
    echo "<p style='
    text-align: center;
    font-weight: 700;
    color: #999999;
    text-transform: uppercase;
' >Hoàn thành giai đoạn $data->stage_process</p>";
}
?>
<ul class="replies">
    <li class="received reply margin_0">
        <div class="message">
            <?php if($index == 0 && $mTicket->status == GasTickets::STATUS_CLOSE ):?>
                <?php 
                    echo "<b>- Close Issue bởi: </b>".GasTickets::ShowNameReply($mTicket->rCloseUserId)." - ".$cmsFormater->formatDateTime($mTicket->close_date)."<br>";
                ?>
            <?php endif;?>
            <?php // if($data->stage_status == 3 && $data->stage_process == 1): ?>
            <?php
//                echo "<b>- Vị trí: </b> <a href='https://www.google.com/maps/place/".trim($mTicket->getGoogleMap())."' target='_blank'>{$mTicket->getGoogleMap()}</a>";
              ?>
            <?php // endif;?>

            <?php if(!empty($data->employee_problems_id)): ?>
                <?php echo '<br><b>'.$data->getProblemOnGrid().'</b>'; ?>
            <?php endif; ?>
            
            <?php if(!empty($data->move_to_uid)): ?>
                    <?php echo '<br><b>'.$data->getMoveToUid().'</b><br>'; ?>
            <?php endif; ?>
            <?php echo $data->getRenewalDateOnGrid();?><br>
            <?php echo nl2br($data->message);?><br>
            
            <?php echo $cmsFormater->formatIssueFile($data); ?>
            
            <div class="clr"></div>
            <?php // echo $data->renderButtonPunish();?>
            <!--- for delete one detail button -->
            <?php // if(GasCheck::canDeleteData($mTicket) && GasCheck::isAllowAccess('gasIssueTickets', 'DeleteOneDetail')):?>
            <?php if(GasCheck::isAllowAccess('gasIssueTickets', 'DeleteOneDetail')):?>
                <div class="clr"></div>
                <?php echo $data->renderButtonDelete();?>
                <div class="clr"></div>
            <?php endif;?> 
            <!--- for delete one detail button -->
            <div class="clr"></div>
            <span class="posted_on">Ngày gửi <?php echo $cmsFormater->formatDateTime($data->created_date); ?></span>
        </div>
        <div class="author">
            <!--<img width="90" height="90" src="https://secure.gravatar.com/avatar/c2da17c95e66c07894e30584b34a9921?default=identicon&amp;secure=true&amp;size=90" alt="Gravatar">-->
            <span class="name item_b">
                <?php // echo $data->uid_post == GasConst::UID_ADMIN ? 'Nguyễn Tiến Dũng<br>[ IT ]' : GasTickets::ShowNameReplyAtDetailTicket($data->rUidPost, array('mDetailTicket'=>$data)); ?>
                <?php echo $data->getInfoUserPost();?>
            </span>
        </div>
    </li>
</ul>