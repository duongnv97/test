<h3 class='title-info'><?php echo $title; ?></h3>
<?php
//$user model user
//$relations relations to pictures
//$title title h3
//$field_name name row
?>
<?php 
if(!$onlyView): ?>
 <div class="row">
    <?php
            if($user->getError('file_name')): ?>
             <label>&nbsp</label>
             <div class="errorMessage"><?php echo $user->getError('file_name') ?></div>
            <?php
            endif;
    ?>
    <label>&nbsp</label>
    <div>
        <a href="javascript:void(0);" class="text_under_none item_b" style="line-height:25px" onclick="fnBuildRow();">
            <img style="float: left;margin-right:8px;" src="<?php echo Yii::app()->theme->baseUrl;?>/images/add.png"> 
            Thêm Dòng ( Phím tắt F8 )
        </a>
        
    </div>
</div>
<?php endif;?>
<div class="clr"></div>

<div class="row">
    <label>&nbsp</label>
    <table class="materials_table hm_table" style="width:80%;">
        <thead>
            <tr>
                <th class="item_c" style="width:10%;">#</th>
                <th class="item_code item_c" style="width:40%;">File Scan ( cho phép định dạng <?php echo GasProfileDetail::$AllowFile;?>)</th>
                <th class="item_code item_c" style="width:40%;"><?php echo EmployeesImages::model()->getAttributeLabel('type') ?></th>
                <?php 
                if(!$onlyView): ?>
                    <th class="item_unit last item_c" style="width:10%;">Xóa</th>
                <?php endif;?>
            </tr>
        </thead>
        <tbody>
            <?php
//            Create row table
            $index = 0;
            if(isset($user->$relations)):
                $aPicture = $user->$relations;
                $index = count($aPicture);
                foreach ($aPicture as $keyPicture => $mEmployeeImage) {?>
                <tr class="item_picture">
                    <input <?php echo 'value="'.$mEmployeeImage->id.'"';?> <?php echo 'name="'.$field_name.'[old][id][]"';?> type="hidden">
                    <td class="item_c order_no">
                                <?php echo $keyPicture+1; ?>
                    </td>
                    <td class="item_c">
                        <?php if(!empty($mEmployeeImage->file_name)): ?>
                        <p>
                            <a rel="group1" class="gallery" href="<?php echo ImageProcessing::bindImageByModel($mEmployeeImage,'','',array('size'=>'size2'));?>"> 
                                <img width="100" height="70" src="<?php echo ImageProcessing::bindImageByModel($mEmployeeImage,'','',array('size'=>'size1'));?>">
                            </a>
                        </p>
                        <?php else:?>
                        <input <?php echo 'name="'.$field_name.'[old][image]"';?> type="hidden">
                        <input accept="image/*" <?php echo 'name="'.$field_name.'[old][image][]"';?> type="file">
                        <?php endif;?>
                    </td>
                    
                    <td class="item_c">
                        <?php if($onlyView):
                            echo isset($fakeEmployeeImages->getListType()[$mEmployeeImage->type]) ? $fakeEmployeeImages->getListType()[$mEmployeeImage->type] : '';
                        else: ?>
                        <?php
                           echo $fakeEmployeeImages->getDropdownType($field_name,'old',$mEmployeeImage->type);
                        ?>
                        <?php endif;?>
                    </td>
                    <?php if(!$onlyView && $mEmployeeImage->canUpdate()): ?>
                    <td class="item_c">
                        <span class="remove_icon_only"></span>
                    </td>
                    <?php endif;?>
                </tr>
                <?php }
            endif;
            ?>
        </tbody>
    </table>
</div> 
<script>
    
    $(document).ready(function(){
        $(".gallery").colorbox({iframe:true,innerHeight:'900', innerWidth: '1000',close: "<span title='close'>close</span>"});
    });
    
    $index = <?php echo $index; ?>;
    $(document).keydown(function(e) {
        if(e.which == 119) {
            fnBuildRow();
        }        
    });
    function fnBuildRow(){
        $current = $('.materials_table').find('.order_no').length;
        if($current<<?php echo EmployeesImages::$max_upload; ?>)
        {
            $strNewRow = '<tr class="item_picture">';
            $strNewRow += '<td class="item_c order_no">';
            $strNewRow += ++$current;
            $strNewRow += '</td>';
            $strNewRow += '<td class="item_c">';
            $strNewRow += '<input <?php echo 'name="'.$field_name.'[new][image][]"';?> type="hidden">';
            $strNewRow += '<input accept="image/*" <?php echo 'name="'.$field_name.'[new][image][]"';?> type="file">';
            $strNewRow += '</td>';
            $strNewRow += '<td class="item_c">';
            $strNewRow += '<?php
                echo $fakeEmployeeImages->getDropdownType($field_name,'new',null);
            ?>';
            $strNewRow += '</td>';
            $strNewRow += '<td class="item_c">';
            $strNewRow += '<span class="remove_icon_only"></span>';
            $strNewRow += '</td>';
            $strNewRow += '</tr>';
            $('.materials_table').find('tbody').append($strNewRow);
            $index++;
        }
    }
</script>
