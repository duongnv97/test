<?php 
$mMonitorUpdate = new MonitorUpdate();
$mMonitorUpdate->user_id    = $customer_id;
$mMonitorUpdate->type       = [MonitorUpdate::TYPE_CHANGE_INFO_CUSTOMER, MonitorUpdate::TYPE_CHANGE_INFO_CUSTOMER_HGD];
$models                     = $mMonitorUpdate->getByType();
$listdataCallCenter = CacheSession::getListdataByRole([ROLE_CALL_CENTER, ROLE_DIEU_PHOI]);
$models = is_array($models) ? $models : [];
?>
<table class="tb hm_table f_size_15">
    <thead>
        <tr>
            <th class="item_c" colspan="4">Lịch sử sửa thông tin</th>
        </tr>
        <tr>
            <th class="w-100">Nhân viên</th>
            <th class="w-150">Ngày sửa</th>
            <th class="w-300">Thông tin cũ</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($models as $key=>$item): ?>
    <?php 
        $nameUser = isset($listdataCallCenter[$item->agent_id]) ? $listdataCallCenter[$item->agent_id] : $item->agent_id;
        $json = json_decode($item->json, true);
        $sInfo = '<b>'.$json['first_name']." - ".$json['phone']. '</b><br>' . $json['address'];
    ?>
        <tr>
            <td><?php echo $nameUser?></td>
            <td><?php echo $item->last_update."<br>MonitorUpdate id: $item->id";?></td>
            <td><?php echo $sInfo;?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />