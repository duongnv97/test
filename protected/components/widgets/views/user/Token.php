<?php 
$mToken = new UsersTokens();
$mToken->user_id = $user_id;
$aToken = $mToken->getAllOfUid();
$mMonitorUpdate = new MonitorUpdate();
$mMonitorUpdate->user_id    = $user_id;
$mMonitorUpdate->type       = MonitorUpdate::TYPE_CHANGE_PASS;
$totalChangePass            = $mMonitorUpdate->countRecordByType();
?>
<table class="tb hm_table f_size_15">
    <thead>
        <tr>
            <th class="item_c" colspan="4">All Token. Total Required Change Pass: <?php echo $totalChangePass;?></th>
        </tr>
        <tr>
            <th class="w-20">#</th>
            <th class="w-100">Type</th>
            <th class="w-300">Token</th>
            <th class="w-150">Creaated Date</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($aToken as $key=>$item): ?>
        <tr>
            <td><?php echo $key+1;?></td>
            <td class="item_c item_b"><?php echo $item->getType();?></td>
            <td><?php echo $item->token;?></td>
            <td><?php echo $item->getCreatedDate('d/m/Y H:i:s');?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />