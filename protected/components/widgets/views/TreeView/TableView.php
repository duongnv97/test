<?php 
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
?>
<?php if(!empty($aData)): ?>
<?php
$i = 1;
$sum_all_code  = empty($aData['SUM_COL_CODE']) ? 0 : array_sum($aData['SUM_COL_CODE']);
$sum_all_order = empty($aData['SUM_COL_ORDER']) ? 0 : array_sum($aData['SUM_COL_ORDER']);
$sum_all_text  = $sum_all_code . '/' . $sum_all_order;
$mAppCache     = new AppCache();
$aAgent        = $mAppCache->getAgentListdata();
?>
<b>Họ Tên: <?php echo empty($aData['DATA_REF']) ? '' : $aData['DATA_REF']->first_name; ?></b>
<br>Chỉ số: App / BQV
<div class="grid-view display_none">
    <table class="items hm_table tableReport freezetablecolumns" style='width:100%;' id="report_table">
        <thead>
            <tr>
                <th class='item_c w-20'>#</th>
                <th class='item_c w-50'>Mã</th>
                <th class='item_c w-150'>Tên</th>
                <th class='item_c w-100'>Đại lý</th>
                <th class='item_c w-100'>Chức vụ</th>
                <th class='item_c w-40'>Tổng</th>
                <?php foreach ($aData['DATA_DATE'] as $d){ ?>
                <th class='item_c w-30'><?php echo substr($d, -2); ?></th>
                <?php } ?>
            </tr>
        </thead>
        <tbody>
            <tr class="h_20">
                <td colspan="5" class="item_r item_b">Tổng</td>
                <td class="item_b item_c"><?php echo $sum_all_text; ?></td>
                <?php foreach ($aData['DATA_DATE'] as $d) : ?>
                <?php 
                $sum_row_code  = isset($aData['SUM_ROW_CODE'][$d]) ? $aData['SUM_ROW_CODE'][$d] : 0;
                $sum_row_order = isset($aData['SUM_ROW_ORDER'][$d]) ? $aData['SUM_ROW_ORDER'][$d] : 0;
                $sum_row = ($sum_row_code == 0 && $sum_row_order == 0) ? '' : $sum_row_code . '/' . $sum_row_order;
                ?>
                <td class="item_b item_c"><?php echo $sum_row; ?></td>
                <?php endforeach; ?>
            </tr>
            <?php foreach ($aData['DATA_INVITED'] as $invited_id => $mUser) : ?>
            <tr class="h_20">
                <td><?php echo $i++; ?></td>
                <td><?php echo $mUser->code_account; // Lưu code_no vào biến code_account ?></td>
                <td><?php echo $mUser->first_name; ?></td>
                <td><?php echo isset($aAgent[$mUser->parent_id]) ? $aAgent[$mUser->parent_id] : '' ; ?></td>
                <td><?php echo $mUser->getRoleName(); ?></td>
                <?php 
                $sum_code  = isset($aData['SUM_COL_CODE'][$invited_id]) ? $aData['SUM_COL_CODE'][$invited_id] : 0;
                $sum_order = isset($aData['SUM_COL_ORDER'][$invited_id]) ? $aData['SUM_COL_ORDER'][$invited_id] : 0;
                $sum_col = ($sum_code == 0 && $sum_order == 0) ? '' : $sum_code . '/' . $sum_order;
                ?>
                <td class="item_b item_c"><?php echo $sum_col; ?></td>
                <?php foreach ($aData['DATA_DATE'] as $d) : ?>
                <?php 
                $inputCode = isset($aData['DATA_INPUT_CODE'][$invited_id][$d]) ? $aData['DATA_INPUT_CODE'][$invited_id][$d] : 0;
                $order     = isset($aData['DATA_ORDER'][$invited_id][$d]) ? $aData['DATA_ORDER'][$invited_id][$d] : 0;
                $text = ($inputCode == 0 && $order == 0) ? '' : $inputCode . '/' . $order;
                ?>
                <td class="item_c"><?php echo $text; ?></td>
                <?php endforeach; ?>
            </tr>
            <?php endforeach; ?>
        </thead>
    </table>
</div>
<?php endif; ?>

<script type="text/javascript"
        src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>
<script type="text/javascript">
    fnAddClassOddEven('items');
    $('.freezetablecolumns').each(function () {
        var id_table = $(this).attr('id');
        $('#' + id_table).freezeTableColumns({
            width:       1100,   // required - bt 1100
            height:      400,   // required
            numFrozen: 6,     // optional
            frozenWidth: 530,   // optional
            clearWidths: true  // optional
        });
    });
    $(window).load(function () {
        var index = 1;
        $('.freezetablecolumns').each(function () {
            if (index == 1)
                $(this).closest('div.grid-view').show();
            index++;
        });
        fnAddClassOddEven('items');
    });
</script>