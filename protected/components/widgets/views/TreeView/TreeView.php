<?php 
//get parameter
//model -> model ReportReferralTracking
//$aData = $model->getTreeReferralTrackingStepByStep();
//$dataView =  $model->getHtmlReportTree($aData);
//$htmlTree = $dataView['tree'];
//$type = $model->type;
//$table = array(
//    'show' =>$model->showTable,
//    'htmlTable' => $dataView['table'],
//);

$htmlTree   = $model->htmlTree;
$type       = $model->type;
$table = array(
    'show'      => $model->showTable,
    'htmlTable' => $model->htmlTable,
);

?>

<?php
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
switch ($type){
    case 'horizontal':
        Yii::app()->clientScript->registerCss('mycss', '
            .clt, .clt ul, .clt li {
                position: relative;
            }
            .clt ul {
                list-style: none;
                padding-left: 20px;
            }
            .clt li::before, .clt li::after {
                content: "";
                position: absolute;
                left: -12px;
            }
            .clt li::before {
                border-top: 1px solid #000;
                top: 9px;
                width: 18px;
                height: 0;
            }
            .clt li::after {
                border-left: 1px solid #000;
                height: 100%;
                width: 0px;
                top: -5px;
            }
            .clt ul > li:last-child::after {
                height: 15px;
            }
            .clt a{
                border: 1px solid black;
                display: inline-block;
                padding: 5px;
                margin: 5px;
                cursor: pointer;
            }
            .clt .sum-tree{
                border: none;
                position: relative;
                left: -20px;
                padding-left: 0;
                margin-left: 0;
            }
            .clt li a:hover, .clt li a:hover+ul li a {
                background: #c8e4f8; color: #000; border: 1px solid #94a0b4;
            }
            .clt li a:hover+ul li::after,
            .clt li a:hover+ul li::before,
            .clt li a:hover+ul::before,
            .clt li a:hover+ul ul::before{
                border-color:  #94a0b4;
            }
        ');
        break;
    case 'vertical':
        Yii::app()->clientScript->registerCss('mycss', '
            .clt {
                overflow: scroll;
                overflow-y: hidden;
            }
            .clt ul {
                display: flex;
                padding-top: 20px; position: relative;

                transition: all 0.5s;
                -webkit-transition: all 0.5s;
                -moz-transition: all 0.5s;
            }

            .clt li {
                float: left; text-align: center;
                list-style-type: none;
                position: relative;
                padding: 20px 5px 0 5px;

                transition: all 0.5s;
                -webkit-transition: all 0.5s;
                -moz-transition: all 0.5s;
            }

            .clt li::before, .clt li::after{
                content: "";
                position: absolute; top: 0; right: 50%;
                border-top: 1px solid #ccc;
                width: 50%; height: 20px;
            }
            .clt li::after{
                right: auto; left: 50%;
                border-left: 1px solid #ccc;
            }

            .clt li:only-child::after, .clt li:only-child::before {
                display: none;
            }

            .clt li:only-child{ padding-top: 0;}
            .clt li:first-child::before, .clt li:last-child::after{
                border: 0 none;
            }

            .clt li:last-child::before{
                border-right: 1px solid #ccc;
                border-radius: 0 5px 0 0;
                -webkit-border-radius: 0 5px 0 0;
                -moz-border-radius: 0 5px 0 0;
            }
            .clt li:first-child::after{
                border-radius: 5px 0 0 0;
                -webkit-border-radius: 5px 0 0 0;
                -moz-border-radius: 5px 0 0 0;
            }


            .clt ul ul::before{
                content: "";
                position: absolute; top: 0; left: 50%;
                border-left: 1px solid #ccc;
                width: 0; height: 20px;
            }

            .clt li a{
                border: 1px solid #ccc;
                padding: 5px 10px;
                text-decoration: none;
                color: #666;
                font-family: arial, verdana, tahoma;
                font-size: 11px;
                display: inline-block;
                cursor: pointer;
                border-radius: 5px;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;

                transition: all 0.5s;
                -webkit-transition: all 0.5s;
                -moz-transition: all 0.5s;
            }


            .clt li a:hover, .clt li a:hover+ul li a {
                background: #c8e4f8; color: #000; border: 1px solid #94a0b4;
            }
            /
            .clt li a:hover+ul li::after,
            .clt li a:hover+ul li::before,
            .clt li a:hover+ul::before,
            .clt li a:hover+ul ul::before{
                border-color:  #94a0b4;
            }
        ');
        break;    
}
?>
<div class="table">
<?php
if(isset($table['htmlTable']) && $table['show'] && is_array($table['htmlTable'])){?>
    <h2 style="margin-bottom: 0;">Nhập mã cấp <?php echo $model->tableLevel; ?></h2>
    <div class="grid-view">
        <table class="items hm_table">
            <thead>
                <tr  class="h_10">
                    <th class="w-20 ">#</th>
                    <th class="w-150 ">Tên</th>
                    <th class="w-100 ">SDT</th>
                    <th class="w-100 ">Nhập mã giới thiệu</th>
                    <th class="w-100 ">Đã mua hàng</th>
                </tr>
            </thead>
            <tbody>
            <?php 
            foreach ($table['htmlTable'] as $key => $value) { 
                $index = 1;
                ?>
                <tr>
                    <td class="item_c item_b" colspan="5"><?php echo isset($value['name']) ?  $value['name'] : '';?></td>
                </tr>
                <?php foreach ($value['value'] as $valueView) { ?>
                <tr>
                    <td><?php echo $index++; ?></td>
                    <td><?php echo isset($valueView['name']) ?  $valueView['name'] : 'Không rõ'; ?></td>
                    <td><?php echo isset($valueView['phone']) ?  $valueView['phone'] : 'Không rõ'; ?></td>
                    <td class="item_c"><?php echo isset($valueView['countChild']) && $valueView['countChild'] > 0 ?  $valueView['countChild'] : ''; ?></td>
                    <td class="item_c"><?php echo isset($valueView['countConfirm']) && $valueView['countConfirm'] > 0 ?  $valueView['countConfirm'] : ''; ?></td>
                </tr>         
                <?php } ?>
            <?php }?>
            </tbody>
        </table>
    </div>
<?php } ?>
</div>

<div class="clt">
  <?php echo $htmlTree; ?>
</div>
