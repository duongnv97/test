<?php
//$model: model name
//$relations: relations to gas file
//$title_col_file: title in column input file before file type
//$field_name: row name
//$type_gas_file: type gas file
?>
<div class="row">
    <?php if($model->getError('file_name')): ?>
        <div class="errorMessage"><?php echo $model->getError('file_name') ?></div>
    <?php endif;?>
</div>
<div class="clr"></div>
<div class="row tb_file" style="" >
        <div>
            <a href="javascript:void(0);" class="text_under_none item_b" style="line-height:25px" onclick="fnBuildRowFile(this);">
                <img style="float: left;margin-right:8px;" src="<?php echo Yii::app()->theme->baseUrl;?>/images/add.png"> 
                Thêm Dòng
            </a>
        </div>
        <table class="materials_table hm_table">
            <thead>
                <tr>
                    <th class="item_c">#</th>
                    <th class="item_code item_c">Upload chứng từ file đính kèm. Cho phép <?php echo GasFile::$AllowFile;?></th>
                    <th class="item_c">Xóa</th>
                </tr>
            </thead>
        <tbody>
            <?php
//            Create row table
            $index = 0;
            if(isset($model->$relations)):
                $aPicture = $model->$relations;
                $aModelFile = $model->$relations;
//                $index = count($aPicture);?>
            <?php foreach($aModelFile as $mFile):?>
                <tr class="materials_row">
                    <td class="item_c order_no"></td>
                    <td class="item_l w-400">
                        <a rel="group1" class="gallery" href="<?php echo ImageProcessing::bindImageByModel($mFile,'','',array('size'=>'size2'));?>"> 
                                <img width="100" height="70" src="<?php echo ImageProcessing::bindImageByModel($mFile,'','',array('size'=>'size1'));?>">
                            </a>
                    </td>
                    <td class="item_c last">
                        <input type="checkbox" name="delete_file[]" value="<?php echo $mFile->id;?>">
                    </td>
                </tr>
            <?php endforeach;?>
            <?php for($i = 1; $i <= (GasSettle::MAX_SETTLE_ITEMS); $i++): ?>
            <?php 
                $display = "";
                if($i > GasFile::IMAGE_MAX_UPLOAD_SHOW)
                    $display = "display_none";
            ?>
                <tr class="materials_row <?php echo $display;?>">
                    <td class="item_c order_no"><?php echo $i;?></td>
                    <td class="item_l w-400">
                        <input id="<?php echo 'yt' . $model_name . '_' . $field_name ?>" type="hidden" value="" name="<?php echo $model_name .'[' . $field_name . '][]' ?>" />
                        <input class="input_file" accept="image/*" name="<?php echo $model_name .'[' . $field_name . '][]' ?>" id="<?php echo $model_name . '_' . $field_name ?>" type="file" />
                    </td>
                    <td class="item_c last"><span class="remove_icon_only"></span></td>
                </tr>
            <?php endfor;?>
            <?php endif; ?>
        </tbody>
    </table>
</div>
<script>
    $(document).ready(function(){
        fnBindRemoveIcon();
    });
    $(window).load(function () { // không dùng dc cho popup
        $(".gallery").colorbox({iframe:true,innerHeight:'1100', innerWidth: '1000',close: "<span title='close'>close</span>"});
    });
    function fnBuildRowFile(this_){
        $(this_).closest('.tb_file').find('.materials_table').find('tr:visible:last').next('tr').show();
    }
</script>
