<div class="wide form">

<?php 
$form=$this->beginWidget('CActiveForm', array(
	'action'=> strtok(GasCheck::getCurl(), '?'),
//	'action'=> ['/admin/gasDebts/reportDebtsDetail'],
	'method'=>'get',
)); 
?>

    <div class="row more_col">
        <div class="col1">
            <?php echo $form->labelEx($model, 'date_from'); ?>
            <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'date_from',
                'options' => array(
                    'showAnim' => 'fold',
                    'dateFormat' => MyFormat::$dateFormatSearch,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly' => true,
                ),
                'htmlOptions' => array(
                    'class' => 'w-16',
                    'style' => 'height:20px;',
                    'readonly' => 'readonly',
                ),
            ));
            ?>  
        </div>

        <div class="col2">
            <?php echo $form->labelEx($model, 'date_to'); ?>
            <?php 
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'date_to',
                'options' => array(
                    'showAnim' => 'fold',
                    'dateFormat' => MyFormat::$dateFormatSearch,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'button',
                    'buttonImage' => Yii::app()->theme->baseUrl . '/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly' => true,
                ),
                'htmlOptions' => array(
                    'class' => 'w-16',
                    'style' => 'height:20px;',
                    'readonly' => 'readonly',
                ),
            ));
            ?>  
        </div>
    </div>
    
    <?php if( empty($this->uid) ): ?>
    <div class="row">
        <?php echo $form->labelEx($model, 'user_id', ['label' => 'Tên nhân viên']); ?>
        <?php echo $form->hiddenField($model, 'user_id'); ?>
        <?php
        // Widget auto complete search user
        $aData = array(
            'model'             => $model,
            'field_customer_id' => 'user_id',
            'url'               => Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login', ['PaySalary' => 1]),
            'name_relation_user'        => 'rUser',
            'field_autocomplete_name'   => 'autocomplete_user',
            'ClassAdd'                  => 'w-500',
            'placeholder'       => 'Nhập mã hoặc tên nhân viên'
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'    => $aData)
                );
        ?>
    </div>
    <?php endif; ?>
        
    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Xem thống kê',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->