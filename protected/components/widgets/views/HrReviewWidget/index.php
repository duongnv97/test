<?php 
$css    = Yii::app()->theme->baseUrl . '/js/bootstrap-multiselect-master/css/bootstrap-3.1.1.min.css';
$js     = Yii::app()->theme->baseUrl . '/js/bootstrap-multiselect-master/js/bootstrap-3.1.1.min.js';
$cssHr  = Yii::app()->theme->baseUrl . '/admin/css/hr.css';
Yii::app()->clientScript->registerCssFile($cssHr);
Yii::app()->clientScript->registerCssFile($css);
?>
<script src="<?php echo $js ?>"></script>

<?php 
$data       = $model->data;
$aHeader    = $data[HrSalaryReports::KEY_HEADER];
$cRole = MyFormat::getCurrentRoleId();
//if ($cRole == ROLE_ADMIN) {
//    echo '<pre>';
//    print_r($data);
//    echo '</pre>';
//}
$sumLCB = 0; // sum lương cơ bản (tháng trc)
$sumLSL = 0; // sum lương sản lượng (tháng trc)
$sumTTN = 0; // sum tổng thu nhập (tháng trc)
$sumTG  = 0; // sum tăng (giảm)
$sumL   = 0; // sum lương
// Sum theo BP
$sumPosLCB = 0; // sum lương cơ bản (tháng trc)
$sumPosLSL = 0; // sum lương sản lượng (tháng trc)
$sumPosTTN = 0; // sum tổng thu nhập (tháng trc)
$sumPosTG  = 0; // sum tăng (giảm)
$sumPosL   = 0; // sum lương
unset($data[HrSalaryReports::KEY_HEADER]);

$aPosition  = UsersProfile::model()->getArrWorkRoom();
$sumAll     = 0;
?>
<div class="container">
    <p>Click từng bộ phận để xem chi tiết.</p>
    <table class="table table-striped table-bordered table-sticky">
        <thead>
        <?php 
        foreach ($aHeader as $key => $item) {
            echo '<tr>';
            foreach ($item as $index => $value) {
                if($key == 0){
                    $attr = ($index == 'prmo') ? 'colspan="3"' // tháng trc
                                          : ($index == 'cumo' ? 'colspan="2"' : 'rowspan="2"'); // tháng này
                } else {
                    $attr = 'class="sticky-under"';
                }
                echo "<th $attr>$value</th>";
            }
            echo '</tr>';
        }
        ?>
        </thead>
        <tbody>
        <?php $i = 1; ?>
        <?php foreach ($data as $pos => $aDataPosition): ?>
            <?php 
            // Sum theo BP
            $sumPosLCB = 0; // sum lương cơ bản (tháng trc)
            $sumPosLSL = 0; // sum lương sản lượng (tháng trc)
            $sumPosTTN = 0; // sum tổng thu nhập (tháng trc)
            $sumPosTG  = 0; // sum tăng (giảm)
            $sumPosL   = 0; // sum lương
            ?>
            <tr class="info" data-toggle="collapse" data-target=".position-div-<?php echo $pos; ?>" style="cursor: pointer">
                <td><?php echo $i++; ?></td>
                <td colspan="13" class="item_b">
                    <?php echo isset($aPosition[$pos]) ? $aPosition[$pos] : '' ?>
                </td>
            </tr>
            <?php foreach ($aDataPosition as $aDataRow): ?>
                <?php $class = empty($aDataRow['enjo']) ? '' : 'danger'; ?>
                <tr class="<?php echo $class; ?>">
                    <td>
                        <div class="position-group collapse position-div-<?php echo $pos; ?>">
                            <?php echo $i++; ?>
                        </div>
                    </td>
                    <?php foreach ($aDataRow as $k => $value): ?>   
                    <?php 
                        switch ($k) {
                            case 'lcb':
                                $sumPosLCB += $value;
                                $sumLCB += $value;
                                break;
                            case 'lsl':
                                $sumPosLSL += $value;
                                $sumLSL += $value;
                                break;
                            case 'ttn':
                                $sumPosTTN += $value;
                                $sumTTN += $value;
                                break;
                            case 'tg':
                                $sumPosTG += $value;
                                $sumTG += $value;
                                break;
                            case 'l':
                                $sumPosL += $value;
                                $sumL += $value;
                                break;

                            default:
                                break;
                        }
                    ?>
                    <td>
                        <div class="position-group collapse position-div-<?php echo $pos; ?>">
                            <?php echo is_int($value) ? ActiveRecord::formatCurrency($value) : $value; ?>
                        </div>
                    </td>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
                    
            <!--Sum theo bp-->
            <tr style="font-size:14px;">
                <td colspan="8" class="item_b item_r">
                    <div class="position-group collapse position-div-<?php echo $pos; ?>">
                        Tổng
                    </div>
                </td>
                <td class="item_b">
                    <div class="position-group collapse position-div-<?php echo $pos; ?>">
                        <?php echo ActiveRecord::formatCurrency($sumPosLCB); ?>
                    </div>
                </td>
                <td class="item_b">
                    <div class="position-group collapse position-div-<?php echo $pos; ?>">
                        <?php echo ActiveRecord::formatCurrency($sumPosLSL); ?>
                    </div>
                </td>
                <td class="item_b">
                    <div class="position-group collapse position-div-<?php echo $pos; ?>">
                        <?php echo ActiveRecord::formatCurrency($sumPosTTN); ?>
                    </div>
                </td>
                <td class="item_b">
                    <div class="position-group collapse position-div-<?php echo $pos; ?>">
                        <?php echo ActiveRecord::formatCurrency($sumPosTG); ?>
                    </div>
                </td>
                <td class="item_b">
                    <div class="position-group collapse position-div-<?php echo $pos; ?>">
                        <?php echo ActiveRecord::formatCurrency($sumPosL); ?>
                    </div>
                </td>
                <td></td>
            </tr><!-- END Sum theo bp-->
        <?php endforeach; ?>
            <!--Sum all-->
            <tr style="font-size:15px;">
                <td colspan="8" class="item_b item_r">
                    Tổng các bộ phận
                </td>
                <td class="item_b">
                    <?php echo ActiveRecord::formatCurrency($sumLCB); ?>
                </td>
                <td class="item_b">
                    <?php echo ActiveRecord::formatCurrency($sumLSL); ?>
                </td>
                <td class="item_b">
                    <?php echo ActiveRecord::formatCurrency($sumTTN); ?>
                </td>
                <td class="item_b">
                    <?php echo ActiveRecord::formatCurrency($sumTG); ?>
                </td>
                <td class="item_b">
                    <?php echo ActiveRecord::formatCurrency($sumL); ?>
                </td>
                <td></td>
            </tr><!-- END Sum all-->
        </tbody>
    </table>
    <!--<h4 class="item_r"><b>Tổng cộng: <?= ActiveRecord::formatCurrency($sumAll) ?></b></h4>-->
</div>

<script>
$('.position-group.collapse').each(function(){
    if(!$(this).hasClass('in')){
        $(this).closest('tr').hide();
    }
});
$('.collapse').on('show.bs.collapse', function () {
    $(this).closest('tr').show();
});
$('.collapse').on('hide.bs.collapse', function () {
    $(this).closest('tr').hide('fast');
});
</script>
