<?php 
$css    = Yii::app()->theme->baseUrl . '/js/bootstrap-multiselect-master/css/bootstrap-3.1.1.min.css';
$js     = Yii::app()->theme->baseUrl . '/js/bootstrap-multiselect-master/js/bootstrap-3.1.1.min.js';
$cssHr  = Yii::app()->theme->baseUrl . '/admin/css/hr.css';
Yii::app()->clientScript->registerCssFile($cssHr);
Yii::app()->clientScript->registerCssFile($css);
?>
<script src="<?php echo $js ?>"></script>

<?php 
$header     = $model->data[HrSalaryReports::KEY_HEADER];
$data       = $model->data;
unset($data[HrSalaryReports::KEY_HEADER]);
$aPosition  = UsersProfile::model()->getArrWorkRoom();
$sumAll     = 0;
//$title      = "Bảng lương của các bộ phận từ ngày "
//                .MyFormat::dateConverYmdToDmy($model->start_date)
//                ." đến ngày "
//                .MyFormat::dateConverYmdToDmy($model->end_date);
$title      = "Bảng lương kê lương kỳ 2 T".date('n/Y', strtotime($model->start_date));
?>
<div class="container">
    <h2>CÁC THÔNG TIN CẦN ĐỂ GIAO DỊCH VIETTELPAY VÀ NGÂN HÀNG</h2>
    <h4><?php echo $title; ?></h4>
    <table class="table table-striped table-bordered table-sticky">
        <thead>
            <tr>
                <?php foreach ($header as $aUserData): ?>
                    <th><?php echo $aUserData; ?></th>
                <?php endforeach; ?>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $uid => $aUserData): ?>
            <tr>
                <?php foreach ($aUserData as $key => $value): ?>
                <td>
                    <?php echo (is_int($value) && $key > 1) ? ActiveRecord::formatCurrency($value) : $value; ?>
                </td>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>