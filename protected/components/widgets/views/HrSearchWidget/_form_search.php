        
<?php 
$method = isset($method) ? $method : 'get';
$url    = Yii::app()->createUrl('admin/HrWorkSchedule/WorkScheduleUI');
$action = isset($action) ? $action : $url;
$is_view_all = isset($is_view_all) ? $is_view_all : TRUE;
// Keep value when submit
if(isset($_GET['HrWorkSchedule'])) {
    $model->search_province = isset($_GET['HrWorkSchedule']['search_province']) ? $_GET['HrWorkSchedule']['search_province'] : "";
    $model->search_position = isset($_GET['HrWorkSchedule']['search_position']) ? $_GET['HrWorkSchedule']['search_position'] : "";
    $model->employee_id = isset($_GET['HrWorkSchedule']['employee_id']) ? $_GET['HrWorkSchedule']['employee_id'] : "";
}
?>
<div class="wide form" style="padding: 0;">
    <div class="row" style="margin: 0">
        <?php $form=$this->beginWidget('CActiveForm', array(
                'action' => $action,
                'method' => $method,
                'id'     => 'form-hr-search',
        )); ?>
            <?php if($is_view_all): ?>
                <div class="row">
                    <?php echo $form->labelEx($model, 'datePlanFrom',['label'=>'Kế hoạch làm việc từ ngày']); ?>
                     <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'datePlanFrom',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
                        //                            'minDate'=> '0',
                        //                            'maxDate'=> '0',
                //                        'minDate'   => date('01-m-Y'),
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'style'=>'height:20px;',
                                'readOnly' => true,
                //                        'value'=> isset($_POST['HrWorkSchedule']['datePlanFrom']) ? $_POST['HrWorkSchedule']['datePlanFrom'] : (isset($_GET['datePlanFrom']) ? $_GET['datePlanFrom'] : date('01-m-Y')),
                            ),
                        ));
                        ?>  
                    <?php echo $form->error($model,'datePlanFrom'); ?>
                </div>

                <div class="row">
                    <?php echo $form->labelEx($model, 'datePlanTo',['label'=>'Đến ngày']); ?>
                     <?php 
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,        
                            'attribute'=>'datePlanTo',
                            'options'=>array(
                                'showAnim'=>'fold',
                                'dateFormat'=> MyFormat::$dateFormatSearch,
                        //                            'minDate'=> '0',
                        //                            'maxDate'=> '0',
                //                        'minDate'   => date('01-m-Y'),
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showOn' => 'button',
                                'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                                'buttonImageOnly'=> true,    
                            ),        
                            'htmlOptions'=>array(
                                'class'=>'w-16',
                                'style'=>'height:20px;',
                                'readOnly' => true,
                //                        'value'=> isset($_POST['HrWorkSchedule']['datePlanTo']) ? $_POST['HrWorkSchedule']['datePlanTo'] : (isset($_GET['datePlanTo']) ? $_GET['datePlanTo'] : date('t-m-Y')),
                            ),
                        ));
                        ?>  
                    <?php echo $form->error($model,'datePlanTo'); ?>
                </div>
            <?php endif; ?>    

            <div class="row">
                <?php echo $form->labelEx($model,'search_province', ['label'=>'Tỉnh']); ?>
                <div class="fix-label">
                    <?php
                       $this->widget('ext.multiselect.JMultiSelect',array(
                             'model'=>$model,
                             'attribute'=>'search_province',
                             'data'=> GasProvince::getArrAll(),
                             // additional javascript options for the MultiSelect plugin
                            'options'=>array('selectedList' => 30,),
                             // additional style
                             'htmlOptions'=>array('class' => 'w-500'),
                       ));    
                   ?>
                </div>
            </div>
        
            <div class="row">
                <?php $mProfile = new UsersProfile(); ?>
                <?php echo $form->labelEx($mProfile,'position_work'); ?>
                <div class="fix-label">
                    <?php
                       $this->widget('ext.multiselect.JMultiSelect',array(
                             'model'=>$model,
                             'attribute'=>'search_position',
                             'data'=> $mProfile->getArrWorkRoom(),
                             // additional javascript options for the MultiSelect plugin
                            'options'=>array('selectedList' => 30,),
                             // additional style
                             'htmlOptions'=>array('class' => 'w-500'),
                       ));    
                   ?>
                </div>
            </div>
        
            <div class="row">
                <?php echo $form->labelEx($model,'employee_id'); ?>
                <?php echo $form->hiddenField($model,'employee_id', array('class'=>'')); ?>
                <?php
                    // 1. limit search kh của sale
//                    $aRole = array_keys(HrSalaryReports::model()->getArrayRoleSalary());
//                    $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(",", $aRole)));
                    $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_login', ['PaySalary' => 1]);
                    // widget auto complete search user customer and supplier
                    $aData = array(
                        'model'=>$model,
                        'field_customer_id'=>'employee_id',
                        'url'=> $url,
                        'name_relation_user'=>'rEmployee',
                        'ClassAdd' => 'w-400',
//                        'field_autocomplete_name' => 'autocomplete_name_2',
                        'placeholder'=>'Nhập mã NV hoặc tên',
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aData));
                ?>
            </div>
        
            <div class="row">
                <?php echo $form->labelEx($model,'manager_name'); ?>
                <?php echo $form->hiddenField($model,'manager_name', array('class'=>'')); ?>
                <?php
                    // 1. limit search kh của sale
                    $sRole = implode(',', [ROLE_ACCOUNTING_ZONE, ROLE_MONITORING_MARKET_DEVELOPMENT]);
                    $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', ['PaySalary'=>1, 'role'=>$sRole]);
                    // widget auto complete search user customer and supplier
                    $aData = array(
                        'model'                     =>$model,
                        'field_customer_id'         =>'manager_name',
                        'url'                       => $url,
                        'name_relation_user'        =>'rManager',
                        'ClassAdd'                  => 'w-400',
                        'field_autocomplete_name'   => 'autocomplete_name_2',
                        'placeholder'               =>'Nhập mã NV hoặc tên',
                    );
                    $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                        array('data'=>$aData));
                ?>
            </div>
        
            <?php if($is_view_all): ?>
                <div class="row">
                   <?php echo $form->labelEx($model,'is_view_sum', ['label'=>'Có cột tổng']); ?>
                   <?php echo $form->checkbox($model, 'is_view_sum', ['checked' => true]); ?>
                </div>
            <?php endif; ?>
        
            <div class="row buttons">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'label'=>'Search',
                    'type'=>'info', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                    'size'=>'small', // null, 'large', 'small' or 'mini'
                    'htmlOptions' => array(
                            'style' => 'margin-left: 50px; margin-bottom: 20px;',
                            'name' => 'search_user',
                            'value' => 1
                        ),
                )); ?>	
            </div>

        <?php $this->endWidget(); ?>
    </div>
</div>