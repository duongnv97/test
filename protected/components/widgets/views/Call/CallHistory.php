<?php   
Yii::app()->clientScript->registerScript('search1', "
$('.searchCallHistory').click(function(){
	$('.search-form-call-w').toggle();
	return false;
});
$('.search-form-call-w form').submit(function(){
	$.fn.yiiGridView.update('borrow-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});
");
Yii::app()->clientScript->registerScript('ajaxupdate', "
$('#gas-app-order-grid a.ajaxupdate').live('click', function() {
    $.fn.yiiGridView.update('gas-app-order-grid', {
        type: 'POST',
        url: $(this).attr('href'),
        success: function() {
            $.fn.yiiGridView.update('gas-app-order-grid');
        }
    });
    return false;
});
");

?>
<h1>Danh sách cuộc gọi Call Center</h1>
<?php echo CHtml::link('Tìm Kiếm Nâng Cao','javascript:;',array('class'=>'searchCallHistory')); ?>
<div class="search-form search-form-call-w ">
<?php include '_search.php';?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'borrow-grid',
	'dataProvider'=>$mCall->search(),
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox();}',
        'template'=>'{pager}{summary}{items}{pager}{summary}',
//    'itemsCssClass' => 'items custom_here',
//    'htmlOptions'=> array('class'=>'grid-view custom_here'),
        'pager' => array(
            'maxButtonCount'=>  CmsFormatter::$PAGE_MAX_BUTTON,
        ),
        'enableSorting' => false,
	'columns'=>array(
        array(
            'header' => 'S/N',
            'type' => 'raw',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'headerHtmlOptions' => array('width' => '10px','style' => 'text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'name'=>'direction',
//            'value'=>'$data->getDirection()."<br>".$data->getStartTime()."<br>".$data->call_uuid',
            'value'=>'$data->getDirection(true)."<br>".$data->getStartTime()',
            'type'=>'raw',
        ),
        array(
            'name'=>'caller_number',
            'type'=>'raw',
            'value'=>'$data->getCallNumberView()',
        ),
        array(
            'name'=>'destination_number',
            'type'=>'raw',
            'value'=>'$data->getDestinationNumberView()',
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'header'=>'Giây gọi',
            'type'=>'raw',
            'value'=>'$data->getBillDuration()',
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'name'=>'user_id',
            'type'=>'raw',
            'value'=>'$data->getUser()',
        ),
        array(
            'name'=>'agent_id',
            'type'=>'raw',
            'value'=>'$data->getAgentGrid()',
        ),
        array(
            'name'=>'customer_id',
            'type'=>'raw',
            'value'=>'"<b>".$data->getCustomer()."</b><br>".$data->getCustomer("address")',
        ),
        array(
            'header'=>'Tạo bán hàng',
            'value'=>'$data->getUrlMakeSell()',
            'type'=>'raw',
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'header'=>'Status',
            'value'=>'$data->getCallStatus()',
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'header'=>'Trạng thái',
            'type'=>'raw',
            'value'=>'$data->getFailedStatus()',
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'header'=>'Ghi chú',
            'type'=>'raw',
            'value'=>'$data->getFailedNote()',
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'header'=>'Audit',
            'type'=>'raw',
            'value'=>'$data->getAudit()',
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'header'=>'Nghe Lại',
            'type'=>'raw',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'value'=>'$data->getUrlPlayAudio()',
        ),
        array(
            'name'=> 'created_date',
            'type'=> 'html',
            'value'=> '$data->getCreatedDate()."<br>(".$data->id.")<br>". $data->call_uuid',
            'htmlOptions' => array('style' => 'width:50px;'),
        ),

	),
)); ?>

<script>
$(document).ready(function() {
    fnUpdateColorbox();
    bindChangeAgent();
});

function fnUpdateColorbox(){
    fnShowhighLightTr();
    fixTargetBlank();
//    $(".createDetail").colorbox({iframe:true,innerHeight:'550', innerWidth: '850',close: "<span title='close'>close</span>"});
    $('.PlayAudioPbxIcon').click(function(){
        var td = $(this).closest('td');
        td.find('.PlayAudioPbxDiv').toggle();
    });
    $(".AjaxMakeOrderBoMoi").colorbox({iframe:true,innerHeight:'1100', innerWidth: '700',close: "<span title='close'>close</span>"});
    $(".AjaxCreateCustomerHgd").colorbox({iframe:true,innerHeight:'1100', innerWidth: '700',close: "<span title='close'>close</span>"});
}


function bindChangeAgent(){
    $('body').on('click', '.ShowChangeAgent', function(){
        $(this).hide();
        var td = $(this).closest('td');
        td.find('.WrapChangeAgentDropdown').show();
    });
    $('body').on('click', '.CancelChangeAgent', function(){
        var td = $(this).closest('td');
        td.find('.WrapChangeAgentDropdown').hide();
        td.find('.ShowChangeAgent').show();
    });
    $('body').on('click', '.SaveChangeAgent', function(){
        var td = $(this).closest('td');
        var tr = $(this).closest('tr');
        var agent_id    = td.find('.SelectAgentId').val();
        var status      = td.find('.ChangeAgentValue').val();
        var note        = td.find('.ChangeAgentNote').val();
        var score       = td.find('.ChangeValueScore').val();
        
//        if($.trim(status) == ''){
//            alert("Chưa chọn phân loại");
//            return false;
//        }
//        if($.trim(agent_id) == ''){
//            alert("Chưa chọn đại lý");
//            return false;
//        }
        
        var url_        = $(this).attr('next');
        var typeText    = td.find('.ChangeAgentValue').find('option:selected').text();
        var agentName   = td.find('.SelectAgentId').find('option:selected').text();
        console.log(agentName);
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        $.ajax({
            url:url_,
            type: 'post',
//            dataType: 'json',
            data: {type_call_test:status, note:note, agent_id:agent_id, score:score },
            success: function(data){
                td.find('.TypeCallTestText').text(typeText);
                td.find('.WrapChangeAgentDropdown').hide();
                td.find('.ShowChangeAgent').show();
                
                tr.find('.AgentGridName').text(agentName);
                tr.find('.CallNoteText').text(note);
                $.unblockUI();
            }
        });
    });
}

</script>
<!--
<audio controls>
  <source src="horse.ogg" type="audio/ogg">
  <source src="https://apps.worldfone.vn/externalcrm/playback2.php?calluuid=1491879194.2751&secrect=1923d10f88daae995b1f461c04f15165&version=3" type="audio/mpeg">
Your browser does not support the audio element.
</audio>
-->