<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=> GasCheck::getCurl(),
	'method'=>'get',
)); 
$cRole = MyFormat::getCurrentRoleId();
?>

    <div class="row more_col">
        <div class="col1">
            <?php echo $form->dropDownList($mCall,'direction', $mCall->getArrayDirection(),array('class'=>'w-200', 'empty'=>'Select')); ?>
	</div>
        <div class="col1">
            <?php echo $form->dropDownList($mCall,'call_status', $mCall->getArrayCallStatus(),array('class'=>'w-200', 'empty'=>'Chọn trạng thái')); ?>
	</div>
        <div class="col2">
            <?php echo $form->textField($mCall,'caller_number',array('class'=>'w-170','placeholder'=>$mCall->getAttributeLabel('caller_number'))); ?>
        </div>
        <div class="col3">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $form->textField($mCall,'destination_number',array('class'=>'w-170','placeholder'=>$mCall->getAttributeLabel('destination_number'))); ?>
        </div>
    </div>
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->dropDownList($mCall,'type', $mCall->getArrayTypeCall(),array('class'=>'w-200', 'empty'=>'Select')); ?>
	</div>
        <div class="col2">
            <?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
            $this->widget('CJuiDateTimePicker',array(
                'model'=>$mCall, //Model object
                'attribute'=>'date_from', //attribute name
                'mode'=>'datetime', //use "time","date" or "datetime" (default)
                'language'=>'en-GB',
                'options'=>array(
                    'showAnim'=>'fold',
                    'showButtonPanel'=>true,
                    'autoSize'=>true,
                    'dateFormat'=>'dd-mm-yy',
                    'timeFormat'=>'hh:mm:ss',
                    'width'=>'120',
                    'separator'=>' ',
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,
                    'changeMonth' => true,
                    'changeYear' => true,
                    //                'regional' => 'en-GB'
                ),
                'htmlOptions' => array(
                    'style' => 'width:180px;',
                    'placeholder' => 'Thời gian từ',
                ),
            ));
            ?>
        </div>
        <div class="col3">
            <?php Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
            $this->widget('CJuiDateTimePicker',array(
                'model'=>$mCall, //Model object
                'attribute'=>'date_to', //attribute name
                'mode'=>'datetime', //use "time","date" or "datetime" (default)
                'language'=>'en-GB',
                'options'=>array(
                    'showAnim'=>'fold',
                    'showButtonPanel'=>true,
                    'autoSize'=>true,
                    'dateFormat'=>'dd-mm-yy',
                    'timeFormat'=>'hh:mm:ss',
                    'width'=>'120',
                    'separator'=>' ',
                    'showOn' => 'button',
                    'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                    'buttonImageOnly'=> true,
                    'changeMonth' => true,
                    'changeYear' => true,
                    //                'regional' => 'en-GB'
                ),
                'htmlOptions' => array(
                    'style' => 'width:180px;',
                    'placeholder' => 'Đến',
                ),
            ));
            ?>
        </div>
    </div>
    
    <div class="row more_col">
        <div class="col1">
            <?php echo $form->dropDownList($mCall,'failed_status', GasConst::getSellReason(true),array('class'=>'w-200', 'empty'=>'Loại cuộc gọi')); ?>
	</div>
    </div>
    
    <?php // if($cRole == ROLE_CALL_CENTER): ?>
    <?php if(0): ?>
    <div class="row">
    <?php echo $form->labelEx($mCall,'customer_id',['label'=>'KH hộ GĐ']); ?>
    <?php echo $form->hiddenField($mCall,'customer_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/searchHgd');
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$mCall,
                'field_customer_id'=>'customer_id',
                'url'=> $url,
                'name_relation_user'=>'rCustomer',
                'ClassAdd' => 'w-400',
//                'fnSelectCustomer'=>'fnSelectCustomer',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
//            ?>
    </div>

    <?php else: ?>
    <div class="row">
    <?php echo $form->labelEx($mCall,'customer_id',['label'=>'KH Bò Mối']); ?>
    <?php echo $form->hiddenField($mCall,'customer_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_user_by_code');
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$mCall,
                'field_customer_id'=>'customer_id',
                'url'=> $url,
                'name_relation_user'=>'rCustomer',
                'ClassAdd' => 'w-400',
//                'fnSelectCustomer'=>'fnSelectCustomer',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
    </div>
    <?php endif; ?>
    
    <div class="row">
    <?php echo $form->labelEx($mCall,'customer_hgd_id',['label'=>'KH hộ GĐ']); ?>
    <?php echo $form->hiddenField($mCall,'customer_hgd_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/searchHgd');
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$mCall,
                'field_customer_id'=>'customer_hgd_id',
                'url'=> $url,
                'name_relation_user'=>'rCustomerHgd',
                'field_autocomplete_name' => 'autocomplete_name_3',
                'ClassAdd' => 'w-400',
//                'fnSelectCustomer'=>'fnSelectCustomer',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
//            ?>
    </div>
    
    <div class="row">
    <?php echo $form->labelEx($mCall,'user_id'); ?>
    <?php echo $form->hiddenField($mCall,'user_id', array('class'=>'')); ?>
    <?php
        // 1. limit search kh của sale
        $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> implode(',', Call::getArrayRoleExt()).','.ROLE_TELESALE ));
        // widget auto complete search user customer and supplier
        $aData = array(
            'model'=>$mCall,
            'field_customer_id'=>'user_id',
            'url'=> $url,
            'name_relation_user'=>'rUser',
            'ClassAdd' => 'w-400',
            'field_autocomplete_name' => 'autocomplete_name_1',
            'placeholder'=>'Nhập mã NV hoặc tên',
        );
        $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
            array('data'=>$aData));
        ?>
    </div>
    
    <div class="row">
    <?php echo $form->labelEx($mCall,'agent_id'); ?>
    <?php echo $form->hiddenField($mCall,'agent_id', array('class'=>'')); ?>
    <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', array('role'=> ROLE_AGENT));
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$mCall,
                'field_customer_id'=>'agent_id',
                'url'=> $url,
                'name_relation_user'=>'rAgent',
                'ClassAdd' => 'w-400',
                'field_autocomplete_name' => 'autocomplete_name_2',
                'placeholder'=>'Nhập mã hoặc tên đại lý',
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        <?php echo $form->error($mCall,'agent_id'); ?>
    </div>

    <div class="row buttons" style="padding-left: 0;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->