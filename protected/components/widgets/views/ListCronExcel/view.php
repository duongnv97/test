<?php 
$mCronExcel = new CronExcel();
if(!$mCronExcel->canDownload()) return false;
$mCronExcel->nameDir = $nameDir; // DungNT Feb1019
?>

<?php 
Yii::app()->clientScript->registerScript('widgetListExcelFile', "
$('.list-button').click(function(){
	$('.list-file').toggle();
	return false;
});
$('.table-link').on('click', function(){
    $(this).siblings().removeClass('active');
    $(this).addClass('active');
    var year = $(this).data('year');
    $('.table-content').hide();
    $('.table-'+year).show();
});
");
?>
<br>
<?php echo CHtml::link('File Excel hệ thống hàng tháng','#',array('class'=>'list-button')); ?>
<div class="list-file display_none" style="">
    <?php 
    if($viewByYear){
        include 'view_by_year.php'; 
    } else {
        include 'view_by_newest.php'; 
    }
    ?>
</div>
<br><br>