<!--Danh sách các năm-->
<?php for($i=1; $i>=0; $i--): ?>
    <?php $year = date("Y") - $i; ?>
    <div class="table-link" data-year="<?php echo $year; ?>" style="color:white; background:#0080FF; margin:5px 3px 0 0; padding:2px; border-radius:5px; display:inline-block; cursor: pointer;">
        <?php echo $year; ?>
    </div>
<?php endfor; ?>

<!--Table danh sách cách file excel của năm đó-->
<?php for($i=1; $i>=0; $i--): ?>
    <?php $year = date("Y") - $i; ?>
    <div class="display_none table-content table-<?php echo $year; ?>" style="margin: 5px 0">
        <table border="1" style="border-collapse: collapse;">
            <thead>
                <tr style="background: #f1f1f1; text-align: center;">
                    <th style="padding:5px;">#</th>
                    <th style="padding:5px;">Tên file</th>
                    <th style="padding:5px;">Ngày tạo</th>
                    <th style="padding:5px;">Kích thước</th>
                </tr>
            </thead>
            <tbody>
            <?php 
            $model_name     = get_class($model);
            $pathUpload     = $mCronExcel->folderUpload."/$year/$model_name";
            $aFileTemp      = $model->multiFileGetArrayFile($pathUpload);
            $aFile          = array_diff($aFileTemp, array('..', '.'));
            $aFileNDate     = [];
            foreach ($aFile as $file_name){
                $fsize = filesize($pathUpload."/$file_name");
                if ($fsize >= 1073741824){
                    $fsize = number_format($bytes / 1073741824, 2) . ' GB';
                }elseif ($fsize >= 1048576){
                    $fsize = number_format($fsize / 1048576, 2) . ' MB';
                } else {
                    $fsize = number_format($fsize / 1024, 2) . ' KB';
                }
                $aFileNDate[] = [
                    'file_name'     => $file_name,
                    'download_link' => Yii::app()->baseUrl."/$pathUpload"."/$file_name",
                    'created_date'  => date('d/m/Y H:i:s', filemtime($pathUpload."/$file_name")),
                    'created_date_bigint' => filemtime($pathUpload."/$file_name"),
                    'file_size'     => $fsize,
                ];
            }
            uasort($aFileNDate, function($a, $b){
                if($a['created_date_bigint'] == $b['created_date_bigint']){
                    return $a['file_name'] < $b['file_name'];
                }
                return $a['created_date_bigint'] < $b['created_date_bigint'];
            });
            $no             = 1;
            ?>
            <?php foreach ($aFileNDate as $detail): ?>
                <tr>
                    <td style="padding:5px;">
                        <?php echo $no++; ?>
                    </td>
                    <td style="padding:5px;">
                        <?php echo CHtml::link($detail['file_name'], $detail['download_link']); ?>
                    </td>
                    <td style="padding:5px;">
                        <?php echo $detail['created_date']; ?>
                    </td>
                    <td style="padding:5px;">
                        <?php echo $detail['file_size']; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php endfor; ?>