<div id="tabs-4">
    <?php if(isset($aHistoryUphold['YEAR_MONTH'])) :?>
    <?php
        $YEAR_MONTH     = $aHistoryUphold['YEAR_MONTH'];
        $OUTPUT         = $aHistoryUphold['OUTPUT'];
    ?>
    <div class="support_statistic_ouput">
        <table class="hm_table detail-view" style="width:auto;">
            <thead>
                <tr class="odd">
                    <th class="item_c">Tháng</th>
                    <?php foreach( $YEAR_MONTH as $year=>$aMonth):?>
                        <?php foreach( $aMonth as $month => $not_use_value):?>
                        <th class="w-50 item_c"><?php echo $month."/$year";?></th>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($OUTPUT as $type=>$reportInfo): ?>
                <?php   $mUphold = new GasUphold();
                        $mUphold->type = $type;
                ?>
                <tr class="odd">
                    <td><?php echo $mUphold->getTypeText(); ?></td>
                    <?php foreach( $YEAR_MONTH as $year=>$aMonth):?>
                        <?php foreach( $aMonth as $month => $not_use_value):?>
                        <?php 
                            $val = isset($reportInfo[$year][$month]) ? $reportInfo[$year][$month] : "";
                        ?>
                        <td class="w-50 item_c"><?php echo $val;?></td>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div><!--end upport_statistic_ouput-->
    <?php else: ?>
        Không có dữ liệu
    <?php endif; ?>
    
</div>