<?php 
$date_to    = date("Y-m-d");
$month_statistic = 6;
$date_from  = MyFormat::modifyDays($date_to, $month_statistic, '-', 'month');
$mUphold = new GasUphold();
$mUphold->customer_id   = $customer_id;
$mUphold->date_from     = $date_from;
$mUphold->date_to       = $date_to;
$aHistoryUphold         = $mUphold->getHistoryCustomerByMonth();

// support customer
$aHistory = GasSupportCustomer::GetHistorySupport($customer_id, array());
$count_history = count($aHistory);

?>
<div id="tabs" class="grid-view">
    <ul>
        <li>
            <a class="tab_1" href="#tabs-1">Giá bán</a>
        </li>
        <li>
            <a class="tab_2" href="#tabs-2">Đầu tư</a>
        </li>
        <li>
            <a class="tab_3" href="#tabs-3">Sản lượng</a>
        </li>
        <li>
            <a class="tab_4" href="#tabs-4">Bảo trì</a>
        </li>
    </ul>

    <?php include 'ReportTab1.php';?>
    <?php include 'ReportTab2.php';?>
    <?php include 'ReportTab3.php';?>
    <?php include 'ReportTab4.php';?>
</div>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/admin/css/jquery-ui-1.8.18.custom.css" type=text/css rel=stylesheet>
<script>
    $(document).ready(function(){
        $( "#tabs" ).tabs();
        fnUpdateSumSupport();
    });
    

</script>