<div id="tabs-1">
    <?php $aPriceHistory = UsersPrice::getHistoryPriceCustomer($customer_id);?>
    
    <table class="hm_table detail-view w-700 " style="">
        <thead>
            <tr class="odd">
                <th class="item_c">Tháng</th>
                <?php foreach( $aPriceHistory['month'] as $month):?>
                    <th class="w-50 item_c"><?php echo $month;?></th>
                <?php endforeach; ?>
            </tr>
        </thead>
        <tbody>
            <tr class="even">
                <td class="w-100">Bình bò</td>
                <?php foreach( $aPriceHistory['month'] as $month):?>
                    <?php 
                        $price = isset($aPriceHistory['BB'][$month]) ? $aPriceHistory['BB'][$month] : '';
                    ?>
                <td class="item_r h_30"><?php echo $price; ?></td>
                <?php endforeach; ?>
            </tr>
            <tr class="odd">
                <td>Bình 12</td>
                <?php foreach( $aPriceHistory['month'] as $month):?>
                    <?php 
                        $price = isset($aPriceHistory['B12'][$month]) ? $aPriceHistory['B12'][$month] : '';
                    ?>
                    <td class=" item_r h_30"><?php echo $price; ?></td>
                <?php endforeach; ?>
            </tr>
        </tbody>
    </table>
</div>