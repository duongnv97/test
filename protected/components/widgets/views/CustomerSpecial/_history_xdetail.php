<?php $this->widget('application.components.widgets.XDetailView', array(
    'data' => $item,
    'attributes' => array(
        'group11'=>array(
            'ItemColumns' => 3,
            'attributes' => array(
                    array(
                        'label' => 'NV Kinh Doanh',
                        'type' => 'html',
                        'value' => $item->getSale(),
                    ),
                    array(
                        'name' => 'status',
                        'value' => GasSupportCustomer::GetStatusSupport($item),
                    ),
//                    'date_request:date',
                    
                    'code_no',
                    array(
                        'name' => 'customer_id',
                        'type' => 'html',
                        'value' => Users::GetInfoField($item->rCustomer, "first_name")."<br>". Users::GetInfoField($item->rCustomer, "address"),
                    ),
                    array(
                        'name' => 'type_customer',
                        'value'=>$item->type_customer?CmsFormatter::$CUSTOMER_BO_MOI[$item->type_customer]:"",
                    ),
                    array(
                        'name' => 'note',
                        'type' => 'html',
                        'value' => nl2br($item->note),
                    ),
                    array(
                        'name' => 'note_technical',
                        'type' => 'html',
                        'value' => nl2br($item->note_technical),
                    ),
                    array(
                        'name' => 'sale_approved',
                        'type' => 'html',
                        'value' => $item->getSaleApproved(),
                    ),
                    array(
                        'name' => 'note_update_real_time',
                        'type' => 'html',
                        'value' => $item->getNotUpdateRealTime(),
                    ),
            ),
        ),
        
        
        'group12'=>array(
            'ItemColumns' => 1,
            'attributes' => array(
                    array(
                        'name' => 'item_name',
                        'type' => 'html',
                        'value' => $item->formatGasSupportCustomerItemName(),
                    ),
            ),
        ),
        
        'group13'=>array(
            'ItemColumns' => 3,
            'attributes' => array(
                    array(
                        'name' => 'deloy_by',
                        'type' => 'html',
                        'value' => $item->getDeloyHtml(),
                    ),
                    array(
                        'name' => 'contact_person',
                    ),
                    array(
                        'name' => 'contact_tel',
                    ),
                    array(
                        'name' => 'time_doing',
                        'value' => $item->getTimeDoing(),
                    ),
                    array(
                        'name' => 'price_qty',
                    ),
                    array(
                        'name' => 'file_design',
                        'type' => 'html',
                        'value' => $item->renderListFile(),
                    ),
                    array(
                        'name' => 'time_doing_real',
                        'value' => $item->getTimeDoingReal(),
                    ),
                    array(
                        'name' => 'uid_login',
                        'type' => 'NameAndRole',
                        'value' => $item->rUidLogin,
                    ),
                    'created_date:datetime',
            ),
        ),
        
        'group2'=>array(
            'ItemColumns' => 3,
            'itemCssClass' => array('even xdetail_c2'),
            'attributes' => array(
                    array(
                        'name' => 'approved_uid_level_1',
                        'type' => 'NameAndRole',
                        'value' => $item->rApprovedUidLevel1,
                        
                    ),
                    array(
                        'name' => 'approved_date_level_1',
                        'type' => 'datetime',
                        'htmlOptions' => array('class' => 'w-100')
                    ),
                    array(
                        'name' => 'approved_note_level_1',
                        'type' => 'html',
                        'value' => nl2br($item->approved_note_level_1),
                    ),
            ),
        ),
        
        'group3'=>array(
            'ItemColumns' => 3,
            'itemCssClass' => array('odd xdetail_c2'),
            'attributes' => array(
                    array(
                        'name' => 'approved_uid_level_2',
                        'type' => 'NameAndRole',
                        'value' => $item->rApprovedUidLevel2,
                        'htmlOptions' => array('class' => 'w-200')
                    ),
                    array(
                        'name' => 'approved_date_level_2',
                        'type' => 'datetime',
                        'htmlOptions' => array('class' => 'w-100')
                    ),
                    array(
                        'name' => 'approved_note_level_2',
                        'type' => 'html',
                        'value' => nl2br($item->approved_note_level_2),
                    ),
            ),
        ),
        
        'group4'=>array(
            'ItemColumns' => 3,
            'itemCssClass' => array('odd xdetail_c2'),
            'attributes' => array(
                    array(
                        'name' => 'approved_uid_level_3',
                        'type' => 'NameAndRole',
                        'value' => $item->rApprovedUidLevel3,
                    ),
                    'approved_date_level_3:datetime',
                    array(
                        'name' => 'approved_note_level_3',
                        'type' => 'html',
                        'value' => nl2br($item->approved_note_level_3),
                    ),
            ),
        ),
        
        
        
    ),
)); ?>