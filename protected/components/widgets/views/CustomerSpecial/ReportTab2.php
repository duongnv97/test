<div id="tabs-2">
    <?php if($count_history): ?>
        <p>Tổng giá trị đầu tư: <span class="AmountSupportSum item_b"></span></p>
        <?php foreach($aHistory as $item):?>
        <div class="ClickShowHideWrap">
            <h1><a class="ClickShowHide" href="javascript:void(0)"> Đề xuất lần <?php echo $count_history--; ?>: </a> &nbsp;&nbsp;<span class="AmountSupportShow"></span></h1>
            <div class="ClickShowHideItem display_none">
                <?php include '_history_xdetail.php';?>
            </div>
        </div>
        <?php endforeach;?>
    <?php endif;?>
    
</div>
<script>
    function fnUpdateSumSupport(){
        var sumAll = 0;
        $('.AmountSupport').each(function(){
            sumAll += $(this).text() * 1;
            var div = $(this).closest('.ClickShowHideWrap');
            div.find('.AmountSupportShow').text(commaSeparateNumber($(this).text()));
        });
        $('.AmountSupportSum').text(commaSeparateNumber(sumAll));
    }
</script>