<div id="tabs-3">
    <div class="form">
        <?php
        $aData = array(
            'customer_id'=> $customer_id,
            'month_statistic'=> $month_statistic,
        );
        ?>    
        <div class="row">
            <span class="item_b">Sản lượng khách hàng trong <?php echo $month_statistic; ?> tháng gần đây</span>
        </div>
        <div class="row">
            <?php
            $this->widget('ext.GasStatistic.GasStatistic',
                array('data'=>$aData));
            ?>
        </div>
    </div>
    
</div>