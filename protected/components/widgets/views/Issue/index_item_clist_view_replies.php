
<?php  // KHANH TOAN
    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $data->searchReplies(),
        'itemView' => 'Issue/_item_clist_view_replies_detail',
        'ajaxUpdate'=>true, 
//        'afterAjaxUpdate'=>'function(id, data){ BindClickView(); }',
        'itemsCssClass'=>'',
        'pagerCssClass'=>'pager',
        'pager'=> array(
            'maxButtonCount' => 10,
            'class'=>'CLinkPager',
            'header'=> false,
            'footer'=> false,
        ),
        'summaryText' => true,
        'summaryText'=>'Showing <strong>{start} - {end}</strong> of {count} results',
        'template'=>'{summary}{pager}<ul class="tickets clearfix">{items}</ul>{pager}{summary}',
    ));
?>
