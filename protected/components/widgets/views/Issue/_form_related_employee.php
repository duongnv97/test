<hr>
<div class="row ">
    <input type="text" class="UserHandle UserRelatedAutocomplete" placeholder="Chọn nhân viên liên quan" style="width: 550px; float: left;">
    <span class="remove_row_item" onclick="fnRemoveNameHand(this);"></span>
    <div class="clr"></div>
    
    <table class="related_employee_table materials_table hm_table" style="width: 100%">
        <thead>
            <tr>
                <th>#</th>
                <th>Nhân viên</th>
                <th>Xóa</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            $aRelated = $data->getRelatedEmployee();
            if(!empty($aRelated)):
                $aRelatedObj = Users::model()->getArrObjectUserByRole('', $aRelated);
                $aRole = Roles::model()->loadItems();
                $no = 1;
                foreach ($aRelatedObj as $user):
                ?>
                <tr>
                    <td class="item_c order_no"><?php echo $no++; ?></td>
                    <td> 
                        <?php echo $user->first_name . " - " . $aRole[$user->role_id]; ?>
                        <input type="hidden" name="GasIssueTickets[related_employee][]" value="<?php echo $user->id; ?>">
                    </td>
                    <td class="item_c last"><span class="remove_icon_only"></span></td>
                </tr>
                <?php
                    endforeach;
                endif;
                ?>
        </tbody>
    </table>
</div>

<hr>

<script>
    $(function(){
        var urlAutocomplete = '<?php echo $ModelCreate->getUrlSearchEmployee();?>';
        fnBindRelatedAutocomplete(urlAutocomplete);
    });
    
    function fnSelectRelated(ui, tbl){
        fnRefreshOrderNumber();
        var numAvailable = tbl.find('tbody tr').length + 1;
        var tr = '<tr>' +
                    '<td class="item_c order_no">'+ numAvailable +'</td>' +
                    '<td>' 
                        + ui.item.label + 
                        '<input type="hidden" name="GasIssueTickets[related_employee][]" value='+ui.item.id+'>' +
                    '</td>' +
                    '<td class="item_c last"><span class="remove_icon_only"></span></td>' +
                '</tr>';
        tbl.append(tr);
    }
    
    function fnClearData(classField){
        $(classField).removeAttr("readonly");
        $(classField).val("");
    }
    
    function fnBindRelatedAutocomplete(urlAutocomplete){
        $('.UserRelatedAutocomplete').each(function(){
            fnBindRAutocomplete($(this), urlAutocomplete);
        });
    }

    // to do bind autocompelte for input
    // @param objInput : is obj input ex  $('.customer_autocomplete')
    function fnBindRAutocomplete(objInput, urlAutocomplete){
        var parent_div = objInput.closest('.new_reply');
        objInput.autocomplete({
            source: urlAutocomplete,
            minLength: 2,
            close: function( event, ui ) { 
                fnClearData('.UserRelatedAutocomplete');
                //$( "#GasStoreCard_materials_name" ).val(''); 
            },
            search: function( event, ui ) { 
                    objInput.addClass('grid-view-loading-gas');
            },
            response: function( event, ui ) { 
                objInput.removeClass('grid-view-loading-gas');
                var json = $.map(ui, function (value, key) { return value; });
                if(json.length<1){
                    var error = '<div class=\'errorMessage clr autocomplete_name_text\'>Không tìm thấy dữ liệu.</div>';
                    if(parent_div.find('.autocomplete_name_text').size()<1){
                        parent_div.find('.remove_row_item').after(error);
                    }
                    else
                        parent_div.find('.autocomplete_name_text').show();
                }                    

            },
            select: function( event, ui ) {
                objInput.attr('readonly',true);
                parent_div.find('.autocomplete_name_text').remove();
                var tbl = $(this).siblings('table.related_employee_table');
                fnSelectRelated(ui, tbl);
            }
        });
    }
</script>