<?php 
    $idCListView = isset($needMore['idCListView']) ? $needMore['idCListView'] : 0;
    /* DungNT Oct0219 close - cho ajax hết list Open - nếu cần fix sẽ open lại sau
    $ajaxUpdate = false;
    if($idCListView == GasTickets::STATUS_CLOSE){
        $ajaxUpdate = true;
    }*/
    $ajaxUpdate = true;// is default of CListView
?>

<!--DuongNV Aug3019 lọc những ticket của uid login (có [Xử lý] đằng trc)-->
<?php echo CHtml::link('Tìm kiếm nâng cao','#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
    <?php include '_search.php'; ?>
</div>

<?php  // NGUYEN DUNG
    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => 'Issue/_item_clist_view',
        'id' => 'item_clist_close'.$idCListView,
        'ajaxUpdate' => $ajaxUpdate, 
        'afterAjaxUpdate'=>'function(id, data){ fnUpdateColorbox(); fnUpdateAjax(); }',
        'itemsCssClass'=>'',
        'pagerCssClass'=>'pager',
        'pager'=> array(
            'maxButtonCount' => 10,
            'class'=>'CLinkPager',
            'header'=> false,
            'footer'=> false,
            'id'=>'id-of-pager-ul'.$idCListView,
        ),
        'summaryText' => true,
        'summaryText'=>'Showing <strong>{start} - {end}</strong> of {count} results',
        'template'=>'{summary}{pager}<ul class="tickets clearfix">{items}</ul>{pager}{summary}',
    ));
?>