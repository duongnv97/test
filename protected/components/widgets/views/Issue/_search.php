<div class="wide form">

<?php 
// Nếu để GasCheck::getCurl() thì $_GET trên url sẽ dài ra -> error url too long
$actionUrl = empty(Yii::app()->request->getPathInfo()) ? Yii::app()->createAbsoluteUrl("/admin/site/index") : '/'.Yii::app()->request->getPathInfo();
$form=$this->beginWidget('CActiveForm', array(
//	'action'=> GasCheck::getCurl(),
	'action'=> $actionUrl,
	'method'=>'get',
)); ?>

    <div class="row">
        <?php 
        $showMineOnly   = empty($_GET['GasIssueTickets']['showMineOnly']) ? false : true;
        $htmlOptions    = $showMineOnly ? ['value'=>1, 'checked'=>'checked'] : ['value'=>1];
        ?>
        <?php echo $form->label($mIssue,'showMineOnly',array('label'=>'Chỉ hiện cv cần xử lý')); ?>
        <?php echo $form->checkBox($mIssue,'showMineOnly',$htmlOptions); ?>
    </div>
    
    <div class="row buttons" style="padding-left: 159px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Search',
            'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
        )); ?>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->