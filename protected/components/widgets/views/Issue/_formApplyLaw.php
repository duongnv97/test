<?php // if($ModelCreate->canApplyLaw()): Feb2119 Close move check one by one ?>
<?php if(1): ?>
<?php 
    $idCheckbox     = 'CheckboxApplyLaw'.$data->id;
    $idCheckbox1    = 'CheckboxApplyAccounting'.$data->id;
    $idCheckbox2    = 'CheckboxApplyMonitor'.$data->id;
    $idCheckbox3    = 'CheckboxApplyPunish'.$data->id;
    $idCheckbox4    = 'CheckboxApplyQuickAction'.$data->id;
    $idCheckbox5    = 'CheckboxUpdateStopOrder'.$data->id;
    $mEmployeeProblems = new EmployeeProblems();
    $lb = 'Phạt nhân viên trả lời '. ActiveRecord::formatCurrency($mEmployeeProblems->getArrayMoneyType()[EmployeeProblems::PROBLEM_DIRECTOR_PUNISH]);
    $urlUpdateStopOrder = Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/index', ['UpdateStopOrder' => $data->id]);
    $urlUpdateMeeting   = Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/index', ['UpdateMeeting' => $data->id]);
    
    $alertTextStopOrder     = 'Bạn chắc chắn chuyển trạng thái KH sang không lấy hàng?';
    $alertTextMeeting       = 'Bạn chắc chắn muốn chuyển sự việc sang Meeting?';
    if($data->status_app == GasIssueTickets::STATUS_8_QUICK_ACTION){
        $alertTextMeeting   = 'Bạn chắc chắn muốn gỡ bỏ Meeting?';
    }
?>
<table class="BoxReplyApplyLaw">
    <tr>
        <td>
            <?php if($ModelCreate->canApplyLaw()): ?>
            <div class="row  item_l ">
                <!--<a class="btn_cancel ApplyLaw" alert_text="Chắc chắn muốn chuyển sự việc sang phòng pháp lý?" next="<?php // echo Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/reply', array('id'=>$data->id, 'ApplyLaw'=>1)); ?>" href='javascript:;'>Chuyển pháp lý</a>-->
                <?php echo $form->checkBox($ModelCreate,'applyLaw',array('class' => 'CheckLikeRadio', 'id'=> $idCheckbox)); ?>
                <?php echo $form->labelEx($ModelCreate,'applyLaw',array('class'=>'checkbox_one_label', 'for'=> $idCheckbox)); ?>
                <br><br>
            </div>
            <?php endif; ?>
            <?php if($ModelCreate->canApplyLaw()): ?>
            <div class="row  item_l ">
                <!--<a class="btn_cancel ApplyLaw" alert_text="Chắc chắn muốn chuyển sự việc sang phòng pháp lý?" next="<?php // echo Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/reply', array('id'=>$data->id, 'ApplyLaw'=>1)); ?>" href='javascript:;'>Chuyển pháp lý</a>-->
                <?php echo $form->checkBox($ModelCreate,'applyAccounting',array('class' => 'CheckLikeRadio', 'id'=> $idCheckbox1)); ?>
                <?php echo $form->labelEx($ModelCreate,'applyAccounting',array('class'=>'checkbox_one_label', 'for'=> $idCheckbox1)); ?>
                <br><br>
            </div>
            <?php endif; ?>
            <?php if($data->canApplyMeeting()): ?>
            <div class="row  item_l ">
                <!--<a class="btn_cancel ApplyLaw" alert_text="Chắc chắn muốn chuyển sự việc sang phòng pháp lý?" next="<?php // echo Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/reply', array('id'=>$data->id, 'ApplyLaw'=>1)); ?>" href='javascript:;'>Chuyển pháp lý</a>-->
                <?php echo $form->checkBox($ModelCreate,'applyQuickAction',array('class' => 'CheckLikeRadio ChkUpdateStopOrder', 'id'=> $idCheckbox4, 'next' => $urlUpdateMeeting, 'alertText'=> $alertTextMeeting)); ?>
                <b><?php echo $form->labelEx($ModelCreate,'applyQuickAction',array('class'=>'checkbox_one_label', 'for'=> $idCheckbox4, 'label'=> $data->getAttributeLabel('applyQuickAction'))); ?></b>
                <br><br>
            </div>
            <?php endif; ?>
        </td>
        <td>
            <?php if($ModelCreate->canApplyLaw()): ?>
            <div class="row  item_l ">
                <!--<a class="btn_cancel ApplyLaw" alert_text="Chắc chắn muốn chuyển sự việc sang phòng pháp lý?" next="<?php // echo Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/reply', array('id'=>$data->id, 'applyMonitor'=>1)); ?>" href='javascript:;'>Chuyển pháp lý</a>-->
                <?php echo $form->checkBox($ModelCreate,'applyMonitor',array('class' => 'CheckLikeRadio', 'id'=> $idCheckbox2)); ?>
                <?php echo $form->labelEx($ModelCreate,'applyMonitor',array('class'=>'checkbox_one_label', 'for'=> $idCheckbox2)); ?>
                <br><br>
            </div>
            <?php endif; ?>
            <?php if($ModelCreate->canApplyLaw()): ?>
            
            <!--DuongNV Sep1819 thay bang button GD phat 50k-->
            <!--<div class="row  item_l ">-->
                <!--<a class="btn_cancel ApplyLaw" alert_text="Chắc chắn muốn chuyển sự việc sang phòng pháp lý?" next="<?php // echo Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/reply', array('id'=>$data->id, 'applyMonitor'=>1)); ?>" href='javascript:;'>Chuyển pháp lý</a>-->
                <?php // echo $form->checkBox($ModelCreate,'applyPunish',array('class' => 'CheckLikeRadio', 'id'=> $idCheckbox3)); ?>
                <?php // echo $form->labelEx($ModelCreate,'applyPunish',array('label'=>$lb, 'class'=>'checkbox_one_label', 'for'=> $idCheckbox3)); ?>
                <!--<br><br>-->
            <!--</div>-->
            <?php endif; ?>
            <?php if($ModelCreate->canApplyLaw()): ?>
            <div class="row  item_l ">
                <!--<a class="btn_cancel ApplyLaw" alert_text="Chắc chắn muốn chuyển sự việc sang phòng pháp lý?" next="<?php // echo Yii::app()->createAbsoluteUrl('admin/gasIssueTickets/reply', array('id'=>$data->id, 'applyMonitor'=>1)); ?>" href='javascript:;'>Chuyển pháp lý</a>-->
                <?php echo $form->checkBox($ModelCreate,'updateStopOrder',array('class' => 'CheckLikeRadio ChkUpdateStopOrder', 'id'=> $idCheckbox5, 'next' => $urlUpdateStopOrder, 'alertText'=> $alertTextStopOrder)); ?>
                <?php echo $form->labelEx($ModelCreate,'updateStopOrder',array('class'=>'checkbox_one_label', 'for'=> $idCheckbox5)); ?>
                <br><br>
            </div>
            <?php endif; ?>
        </td>
    </tr>
    
</table>
<?php endif; ?>
