<?php
$aMaintain  = array();
$title      = '';
$id_search  = 'agent_id';
$paramsATCL = [];
switch ($type) {
    case GasOneMany::TAB_TYPE_DRIVER:
        $role       = implode(',', [ROLE_DRIVER, ROLE_MANAGING_DIRECTOR]);
        $paramsATCL = ['role' => $role, 'PaySalary' => 1];
        $title      = 'Lái xe';
        $aManyID    = GasOneMany::getArrOfManyId($model->id, ONE_AGENT_DRIVER);
        $aMaintain  = Users::getArrayModelByArrayId($aManyID);
        break;
    case GasOneMany::TAB_TYPE_TRUCK:
        $role       = implode(',', [ROLE_CAR]);
        $paramsATCL = ['role' => $role];
        $title      = 'Xe tải';
        $aManyID    = GasOneMany::getArrOfManyId($model->id, ONE_AGENT_CAR);
        $aMaintain  = Users::getArrayModelByArrayId($aManyID);  
        break;
    
    case GasOneMany::TAB_TYPE_PHU_XE:
        $title      = 'Phụ xe';
        $paramsATCL = ['role' => $role, 'PaySalary' => 1];
        $role       = implode(',', [ROLE_PHU_XE, ROLE_DRIVER, ROLE_CRAFT_WAREHOUSE]);
        $aManyID    = GasOneMany::getArrOfManyId($model->id, ONE_AGENT_PHU_XE);
        $aMaintain  = Users::getArrayModelByArrayId($aManyID);
        break;
    
    case GasOneMany::TAB_TYPE_AGENT:
        $role       = ROLE_AGENT;
        $paramsATCL = ['role' => $role, 'GetAllAgent' => 1];
        $id_search  = 'maintain_agent_id';
        $title      = 'Đại lý';
        if($model->maintain_agent_id && is_array($model->maintain_agent_id)){
            $aMaintain = Users::getArrayModelByArrayId($model->maintain_agent_id);
        }
        break;

    default:
        break;
}
$model->maintain_agent_id = null;
$index = 1;

$userID     = empty($_GET['id']) ? 0 : $_GET['id'];
$linkDriver = Yii::app()->createAbsoluteUrl('admin/gasmember/updateDetail',['id' => $userID, 'type'=> GasOneMany::TAB_TYPE_DRIVER]);
$linkCar    = Yii::app()->createAbsoluteUrl('admin/gasmember/updateDetail',['id' => $userID, 'type'=> GasOneMany::TAB_TYPE_TRUCK]);
$linkPhuXe  = Yii::app()->createAbsoluteUrl('admin/gasmember/updateDetail',['id' => $userID, 'type'=> GasOneMany::TAB_TYPE_PHU_XE]);
$linkAgent  = Yii::app()->createAbsoluteUrl('admin/gasmember/updateDetail',['id' => $userID, 'type'=> GasOneMany::TAB_TYPE_AGENT]);
?>
<h2>Thông tin:</h2>
<div>
    <ul>
        <li><?php echo '<b>Tên:</b> '.$model->getFullName().'<br>'; ?></li>
        <li><?php echo '<b>Chức vụ:</b> '.$model->getRoleName().'<br>'; ?></li>
        <li><?php echo '<b>Ngày vào làm:</b> '.$model->getDateBeginJob().'<br>'; ?> </li>
        <li><?php echo '<b>Lương:</b> '.$model->getPaySalary(); ?></li>
    </ul>
</div>

<div class="form">
    <h1>Cập nhật 
        <a class='btn_cancel f_size_15 <?php echo $type == GasOneMany::TAB_TYPE_DRIVER ? 'active' : ''; ?>' id='tabDriver' href="<?php echo $linkDriver ?>" style="text-decoration:none;">Tài xế</a>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <a class='btn_cancel f_size_15 <?php echo $type == GasOneMany::TAB_TYPE_TRUCK ? 'active' : ''; ?>' id='tabCar' href="<?php echo $linkCar ?>" style="text-decoration:none;">Xe tải</a>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <a class='btn_cancel f_size_15 <?php echo $type == GasOneMany::TAB_TYPE_PHU_XE ? 'active' : ''; ?>' id='tabPhuXe' href="<?php echo $linkPhuXe ?>" style="text-decoration:none;">Phụ xe</a>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <a class='btn_cancel f_size_15 <?php echo $type == GasOneMany::TAB_TYPE_AGENT ? 'active' : ''; ?>' id='tabAgent' href="<?php echo $linkAgent ?>" style="text-decoration:none;">Đại lý theo dõi</a>
    </h1>
</div>
<div class="form">
    
    <?php echo MyFormat::BindNotifyMsg();?>
    
    <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'users-form',
            'enableAjaxValidation'=>false,
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>
    
    <div class="row ">
        <?php echo $form->label($model,$title); ?>
        <?php echo $form->hiddenField($model,$id_search, array('class'=>'')); ?>
        <?php
            // 1. limit search kh của sale
            $url = Yii::app()->createAbsoluteUrl('admin/ajax/search_by_role', $paramsATCL);
            // widget auto complete search user customer and supplier
            $aData = array(
                'model'=>$model,
                'field_customer_id'=> $type == GasOneMany::TAB_TYPE_AGENT ? 'maintain_agent_id' : 'agent_id',
                'url'=> $url,
                'name_relation_user'=>'CustomerMaintain',
                'ClassAdd' => 'w-300',
                'field_autocomplete_name' => 'autocomplete_name_2',
                'placeholder'=>'Nhập mã hoặc tên '.$title,
                'fnSelectCustomerV2' => "fnDeloyBySelectAgent",
                'doSomethingOnClose' => "doSomethingOnCloseMaintain",
            );
            $this->widget('ext.SpaAutocompleteCustomer.SpaAutocompleteCustomer',
                array('data'=>$aData));
            ?>
        
        <!--autocomplete ko can error-->
        <?php // echo $form->error($model,$id_search   ); ?>
    </div>
    
    <div class='row'>
        <label>&nbsp</label>
        <table class="materials_table hm_table tb_deloy_by w-500">
            <thead>
                <tr>
                    <th class="item_c w-20">#</th>
                    <th class="item_code item_c w-300"><?php echo $title; ?></th>
                    <th class="item_unit last item_c">Xóa</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($aMaintain as $key => $mUser): ?>
                <tr>
                    <input name=<?php echo ($type == GasOneMany::TAB_TYPE_AGENT) ? "Users[maintain_agent_id][]" : "Users[agent_id][]"?>  value="<?php echo $key; ?>" type="hidden">
                    <td class="item_c order_no"><?php echo $index++; ?></td>
                    <td class="uid_maintain_<?php echo $mUser->id?>"><?php echo $mUser->getNameWithRole(). "  ---  ".$mUser->id;?></td>
                    <td class="itemtyec last"><span class="remove_icon_only"></span></td>
                </tr>
                <?php endforeach;?>
            </tbody>

        </table>
    </div>
    
    <div class='row buttons' style="padding-left: 141px;">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'label' => 'Save',
        'type'=>'null', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        //'htmlOptions' => array('style' => 'margin-bottom: 10px; float: right;'),
    )); ?>
    </div>
    <?php $this->endWidget() ?>
</div>
<br>
<script>
    $(document).ready(function(){
        fnBindRemoveIcon();
        
    });
    var type = <?php echo $type; ?>;
    switch(type){
        case <?php echo GasOneMany::TAB_TYPE_DRIVER ?>:
            $("a").removeClass('active');
            $("#tabDriver").addClass('active');
            break;
        case <?php echo GasOneMany::TAB_TYPE_TRUCK ?>:
            $("a").removeClass('active');
            $("#tabCar").addClass('active');
            break;
        case <?php echo GasOneMany::TAB_TYPE_PHU_XE ?>:
            $("a").removeClass('active');
            $("#tabPhuXe").addClass('active');
            break;
        case <?php echo GasOneMany::TAB_TYPE_AGENT ?>:
            $("a").removeClass('active');
            $("#tabAgent").addClass('active');
            break;
        default: break;
    }
    /**
    * @Author: ANH DUNG Jun 23, 2015
    * @Todo: function này dc gọi từ ext của autocomplete
    * @Param: $model
    */
    function fnDeloyBySelectAgent(ui, idField, idFieldCustomer){
        if(idField == '#Users_autocomplete_name_1'){
            var ClassCheck = "uid_"+ui.item.id;
            var td_name = '<td class="'+ClassCheck+'">'+ui.item.name_role+'</td>';
            var td_remove = '<td class="item_c last"><span class="remove_icon_only"></span></td>';
            var input = '<input name="Users[agent_id_search][]"  value="'+ui.item.id+'" type="hidden">';
            var tr = '<tr><td class="item_c order_no"></td>'+td_name+td_remove+input+'</tr>';
            if($('.tb_deloy_by_1 tbody').find('.'+ClassCheck).size() < 1 ) {
                $('.tb_deloy_by_1 tbody').append(tr);
            }
        }else{
            var ClassCheck = "uid_maintain_"+ui.item.id;
            var td_name = '<td class="'+ClassCheck+'">'+ui.item.name_role+'</td>';
            var td_remove = '<td class="item_c last"><span class="remove_icon_only"></span></td>';
            var input = '<input name="Users[<?php echo $id_search ?>][]"  value="'+ui.item.id+'" type="hidden">';
            var tr = '<tr><td class="item_c order_no"></td>'+td_name+td_remove+input+'</tr>';
            if($('.tb_deloy_by tbody').find('.'+ClassCheck).size() < 1 ) {
                $('.tb_deloy_by tbody').append(tr);
            }
        }
        fnRefreshOrderNumber();
    }
    /**
    * @Author: ANH DUNG Dec 28, 2016
    * @Todo: function này dc gọi từ ext của autocomplete, action close auto
    */
    function doSomethingOnClose(ui, idField, idFieldCustomer){
        var row = $(idField).closest('.row');
        row.find('.remove_row_item').trigger('click');
    }

    function doSomethingOnCloseMaintain(ui, idField, idFieldCustomer){
        var row = $(idField).closest('.row');
        row.find('.remove_row_item').trigger('click');
    }
</script>