<?php
$aTabsContainer = array();
$mRoles = new HrSalaryReports();
if (!isset($dataName))
    $dataName = 'role';
if (!isset($ajax)) {
    $ajax = true;
    $aView = '';
}
$className = isset($class) ? $class : 'tabLink';
$tabId = isset($tabId) ? $tabId : 'tabs';

/* Default tab */
if (empty($aTab)) {
    $aTab = $mRoles->getArrayRoleSalary();
}

/* Set properties for tab */
$i = 0;
foreach ($aTab as $key => $role) {
    $i++;
    $aTabsContainer[$i]['id'] = $tabId . $className . $i;
    $aTabsContainer[$i]['label'] = $role;
    $aTabsContainer[$i]['content'] = empty($aView) ? '' : is_array($aView) ? $aView[$i] : $aView;
    if(empty($aDataTab)){
        $aTabsContainer[$i]['linkOptions'] = array('class' => $className, 'data-' . $dataName => $key);
    } else {
        $aTabsContainer[$i]['linkOptions'] = array('class' => $className, 'data-' . $dataName => $aDataTab[$i - 1]);
    }
    if ($i == 1) {
        $aTabsContainer[$i]['active'] = true;
    }
}
?>
    <?php echo $title; ?>
<div class="row" style="margin: 0">
    <?php
    $this->widget('bootstrap.widgets.TbTabs', array(
        'id' => $tabId,
        'type' => 'tabs',
        'tabs' => $aTabsContainer,
    ));
    ?>	
</div>
<div id='data'></div>

<!--import bootstrap-->
<?php
$Url = Yii::app()->theme->baseUrl . '/js/bootstrap-multiselect-master/js/bootstrap-3.1.1.min.js';
Yii::app()->getClientScript()->registerScriptFile($Url);
?>

<?php if (!empty($ajax)) { ?>
    <script>
        /*Check if element is empty*/
        function isEmptyOf<?php echo $tabId ?>(el) {
            return !$.trim(el.html())
        }

        /* get data by ajax, not request if requested  */
        /* params: event, url controller to call ajax, data-*, classname */
        function getDataAjaxOf<?php echo $tabId ?>(e, url, dataName, className, tabId) {
            var data = $(e.target).data(dataName);/* get data of click elm */
            var tabLink = '';
            if (typeof data === 'undefined') {
                data = '<?php echo empty($dataValue) ? '' : $dataValue; ?>';/* get data truyen vao */
                if (data != '') {
                    tabLink = $('#' + tabId + '>ul>li>a[data-' + dataName + '=' + data + ']').attr('href');
                    $('#' + tabId + '>ul>li.active').removeClass('active');
                    $('#' + tabId + '>ul>li>a[data-' + dataName + '=' + data + ']').parent('li').addClass('active');
                    $('#' + tabId + ">div.tab-content>div.tab-pane").removeClass('active in');
                    $("#" + tabId + " " + tabLink).addClass('active in');
                }
            } else {
                tabLink = String($(e.target).attr('href'));/* get href(id of container elm) of append element */
            }

            if (tabLink == '') {
                tabLink = $('#' + tabId + ' .active>.' + className).attr('href');
            }

            tabLink = tabLink.replace('#', '');/* get id of container element */

            /* check if element not empty(ajax requested), then dont request ajax */
            if (!isEmptyOf<?php echo $tabId ?>($("#" + tabId + " #" + tabLink))) {
                return 0;
            }

            /* default data if init */
            var defaultData = $('.' + className).eq(0).data('<?php echo $dataName ?>');
            if (data === '')
                data = defaultData;
            
            $.ajax({
                'url': url + data,
                'cache': false,
                'data': {
                    aNeedMore: '<?php echo empty($aNeedMore) ? '' : json_encode($aNeedMore); ?>'
                },
                'success': function (html) {
                    $("#" + tabId + " #" + tabLink).html(html);
                },
            });
        }
        $(function (e) {

            var dataName = '<?php echo $dataName ?>';
            var className = '<?php echo $className ?>';
            var tabId = '<?php echo $tabId ?>';
            var url = '<?php echo Yii::app()->createAbsoluteUrl($ajax_url, empty($params_url) ? [] : $params_url) . '/' . $dataName . '/' ?>';



            getDataAjaxOf<?php echo $tabId ?>(e, url, dataName, className, tabId);

            $(document).on('click', '#' + tabId + ' .' + className, function (e) {

                getDataAjaxOf<?php echo $tabId ?>(e, url, dataName, className, tabId);

            });
        })

    </script>
<?php } ?>

<?php
Yii::app()->clientScript->registerScript("style", "
$('head').append('<link rel=stylesheet href=" . Yii::app()->theme->baseUrl . "/js/bootstrap-multiselect-master/css/bootstrap-3.1.1.min.css>');
    $('head').append(
            '<style>'+
            '.active{background: white!important}'+
            'a." . $className . "{font-size: 11px; background: #5ba0c9; color: white; margin: 1px!important;}'+
            'li:not(.active) a." . $className . ":hover{background: #2a6496}'+
            '.nav-tabs>li.active>a, .nav-tabs>li.active>a:hover, .nav-tabs>li.active>a:focus{background: white}'+
            '</style>'
    );
");
?>