<?php
/**
 * list variables
 * $data
 * $fixedHeader
 */
// Sort header report
$mHrSalaryReport    = new HrSalaryReports();
if($fixedHeader){
    $aPair          = $mHrSalaryReport->getArrayHeaderReport($data);
}
if(empty($data)) return;
$aUid               = array_keys($data);
unset($aUid[0]); // remove [0] => 'header'
$aUserProfile       = $mHrSalaryReport->getArrayProfile($aUid);
$aHeader            = empty($data[HrSalaryReports::KEY_HEADER]) ? [] : $data[HrSalaryReports::KEY_HEADER];
// Text align left, center
$aAlignLeft         = [
    HrSalaryReports::KEY_NHANVIEN,
    HrSalaryReports::KEY_PROVINCE,
    HrSalaryReports::KEY_MONITOR,
    HrSalaryReports::KEY_POS_ROOM,
    HrSalaryReports::KEY_POSITION,
    HrSalaryReports::KEY_NOTE,
];
$aAlignCenter       = [
    HrSalaryReports::KEY_CAN_PAID,
];
$aFixColumn         = [
    HrSalaryReports::KEY_NHANVIEN,
    HrSalaryReports::KEY_POS_ROOM,
    HrSalaryReports::KEY_MONITOR,
    HrSalaryReports::KEY_POSITION,
    HrSalaryReports::KEY_PROVINCE,
];
?>
<!--<div class="tbl-container" style="width:915px;">-->
<div class="tbl-container">
    <table class="reportTable hm_table salary_table" border="1" style="border-collapse: collapse; width: 100%; overflow: scroll;">
        <?php
        $aWeekendDate   = [];
        $i              = 1;
        $aIndexPercent  = [];
        $aHeaderKey     = [];
        $aSumRow        = [];
        ?>
        <thead>
            <tr>
                <th class="fix_col">#</th>
                <?php 
                $aPair = isset($aPair) ? $aPair : $aHeader;
                foreach ($aPair as $key => $cell) {
                    $aHeaderKey[]           = $key;
                    $cssClassWeekend = '';
                    if (strpos($cell, "T7") !== false || strpos($cell, "CN") !== false) {
                        $aWeekendDate[]     = $key;
                        $cssClassWeekend    = 'weekend';
                    }
                    if (strpos($cell, "(%)") !== false) {
                        $aIndexPercent[]    = $key;
                    }
                    $removeIcon             = '<i class="remove-icon glyphicon glyphicon-remove"></i>';
                    if($key == HrSalaryReports::KEY_NHANVIEN){
                        $removeIcon         = '';
                    }
                    $cssHide                = in_array($key, $aFixColumn) ? 'fix_col' : '';
                    echo '<th class="' . $cssClassWeekend . $cssHide.  '" date-col-id="'.$key.'">'
                            . $removeIcon
                            . $cell 
                        . '</th>';
                } ?>
            </tr>
        </thead>
        <tbody>
            <?php 
            foreach ($data as $row_key => $row) {
                if($row_key == -1 || $row_key == HrSalaryReports::KEY_HEADER) continue;
//                $isLeaveJob             = !empty($aUserProfile[$row_key]['leave_date']) && $aUserProfile[$row_key]['leave_date'] <= date('Y-m-d'); // DuongNV Sep0619 close có ngày nghỉ việc là highlight hết
                $isLeaveJob             = !empty($aUserProfile[$row_key]['leave_date']);
                $classTr                = $isLeaveJob ? 'class="user-leave-job"' : '';
                echo "<tr {$classTr}>";
                echo '<td class="text-center fix_col">' . $i++ . '</td>';
                foreach ($aHeaderKey as $h_key) {
                    $cssClassWeekend    = in_array($h_key, $aAlignLeft) ? ' item_l ' : ' item_r ';
                    $cssClassWeekend    = in_array($h_key, $aAlignCenter) ? 'item_c' : $cssClassWeekend;
                    if (in_array($h_key, $aWeekendDate)) {
                        $cssClassWeekend .= 'weekend';
                    }
                    $cell = isset($row[$h_key]) ? $row[$h_key] : 0;
                    if (in_array($h_key, $aIndexPercent)) {
                        $cell           = number_format((float)$cell, 1, '.', ''); // làm tròn 1 chữ số, vd 12.345 -> 12.3
                    }
                    $cssHide            = in_array($h_key, $aFixColumn) ? 'fix_col' : '';
                    // Nếu lớn hơn 1000 thì làm tròn
                    $cellText           = (abs($cell) >= 1000) ? ActiveRecord::formatCurrencyRound($cell) : $cell;
                    if( is_numeric($cell) ){
                        $aSumRow[$h_key]    = empty($aSumRow[$h_key]) ? $cell : ($aSumRow[$h_key]+$cell);
                    }
                    echo '<td class="' . $cssClassWeekend . $cssHide.  '" date-col-id="'.$h_key.'">' . $cellText . '</td>';
                }
                echo '</tr>';
            }
            ?>
            
            <?php if( $this->sumRowBottom ): ?>
            <tr class="sum_row">
                <td class="fix_col"></td>
                <?php foreach ($aPair as $key => $name) : ?>
                    <?php 
                    $money = empty($aSumRow[$key]) ? '' : $aSumRow[$key];
                    if( (abs($money) >= 1000) ){
                        $money = ActiveRecord::formatCurrencyRound($money);
                    }
                    $class = in_array($key, $aFixColumn) ? 'item_r item_b fix_col' : 'item_r item_b';
                    ?>
                <td class="<?php echo $class; ?>"><?php echo $money; ?></td>
                <?php endforeach; ?>
            </tr>
            <?php endif; ?>
        </tbody>;
    </table>
</div>

<?php if($enableScript): ?>
<script>
    $(function() {
//        $('.salary_table thead').append($('.sum_row').removeClass('display_none').clone());
        $('.reportTable tbody').scroll(function(e) { //detect a scroll event on the tbody
            var left = $(this).scrollLeft();
            $('.reportTable thead').css("left", -left); //fix the thead relative to the body scrolling
            $('.reportTable thead th.fix_col').css("left", left);
            $('.reportTable tbody td.fix_col').css("left", left);
        });
    });
</script>
<?php endif; ?>