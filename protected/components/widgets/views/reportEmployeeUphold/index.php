<?php
//$mReport  = new ReportEmployeeUpholdWidget();
if(isset($_GET['dateReport'])){
    $this->date = MyFormat::dateConverDmyToYmd($_GET['dateReport'], '-');
    
}else{
    $this->date = date('Y-m-d');
}
$new = $complete = $hgdNew = $hgdComplete = 0;
//$mReport->uId = 710307; //test 22-12-2015
//$mReport->uId = MyFormat::getCurrentUid();
$this->getDataForGasUphold($new,$complete);
$this->getDataForUpholdHgd($hgdNew,$hgdComplete);
?>

<?php if(isset($needMore['type']) && $needMore['type'] == 'string'): ?>
<?php 
    $mGasUphold = new GasUphold();
    $mGasUphold->date_to = date('d-m-Y');
    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model'=>$mGasUphold,        
        'attribute'=>'date_to',
        'options'=>array(
            'showAnim'=>'fold',
            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
            'maxDate'=> '0',
            'changeMonth' => true,
            'changeYear' => true,
            'showOn' => 'button',
            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
            'buttonImageOnly'=> true,                                
        ),        
        'htmlOptions'=>array(
            'class'=>'w-100 date_from',
            'size'=>'16',
            'style'=>'float:left;',                               
        ),
    ));
?>
<div id="content-report">
<p>BT Bò mối :  <b><?php echo $new; ?></b> (Mới)  &emsp;  <b><?php echo $complete; ?></b>(Chưa HT)   &emsp;   BT Hộ GĐ : <b><?php echo $hgdNew; ?></b> (Mới) &emsp; <b><?php echo $hgdComplete; ?></b> (Chưa HT) </p>
</div>
<?php else:  ?>
<div class="view_uphold">
    <table class="tb hm_table">
        <thead>
        <tr style="">
            <th class="w-150 ">
                <div class="col2">
                <?php 
                    $mGasUphold = new GasUphold();
                    $mGasUphold->date_to = date('d-m-Y');
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$mGasUphold,        
                        'attribute'=>'date_to',
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=> MyFormat::$dateFormatSearch,
//                            'minDate'=> '0',
                            'maxDate'=> '0',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showOn' => 'button',
                            'buttonImage'=> Yii::app()->theme->baseUrl.'/admin/images/icon_calendar_r.gif',
                            'buttonImageOnly'=> true,                                
                        ),        
                        'htmlOptions'=>array(
                            'class'=>'w-100 date_from',
                            'size'=>'16',
                            'style'=>'float:left;',                               
                        ),
                    ));
                ?>
                </div>
            </th>
            <th class="w-100 ">Mới</th>
            <th class="w-100 ">Chưa HT</th>
        </tr>
        </thead>
        <tbody id="content-report">
        <tr>
            <th class="item_c">BT Bò mối</th>
            <td class=" item_c"><?php echo $new; ?></td>
            <td class=" item_c"><?php echo $complete; ?></td>
        </tr>
        <tr>
            <th class="item_c">BT Hộ GĐ</th>
            <td class=" item_c"><?php echo $hgdNew; ?></td>
            <td class=" item_c"><?php echo $hgdComplete; ?></td>
        </tr>
        </tbody>
    </table>
</div>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<?php endif; ?>
<script>
$(document).ready(function(){
    $('#GasUphold_date_to').change(function() {
        var strUrl = window.location.href;
        var dateReport = $('#GasUphold_date_to').val();
//        console.log($('#GasUphold_date_to').val());
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        $.ajax({
            url: strUrl,
            type: 'GET',
            cache: false,
            data: {'dateReport' : dateReport},
            success: function(data){
//                console.log(data);
                var html = $(data).find("#content-report").html();
//                console.log(html);
                $("#content-report").html(html);
                $.unblockUI();
            },
            error: function (){
                alert('Có lỗi xảy ra');
            }
        });
    });
    
});    
</script>
