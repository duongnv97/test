<?php 
    $cRole = MyFormat::getCurrentRoleId();
    // Dec 03, 2016 hiện tại mới mở socket cho Admin + Giao nhận và KTBH
    $mSocketNotify = new GasSocketNotify();
?>
<?php if(in_array($cRole, GasSocketNotify::getRoleAllowSocket())): ?>
    <?php
    $mUser = Users::model()->findByPk(MyFormat::getCurrentUid());
    $token = $mUser->verify_code;
    $agentParam = '';
    if(in_array($cRole, GasSocketNotify::getRoleUpdateNotify())){
        $agent_id   = $mUser->parent_id;
        $agentParam = '&agent_id='.$agent_id;
    }
    $mSocketNotify = new GasSocketNotify();
    $url   = "ws://io.huongminhgroup.com:8004/user_id=$mUser->id&huongminh_token=$token".$agentParam;
//    $url   = "ws://localhost:8004/user_id=$mUser->id&huongminh_token=$token".$agentParam;
    ?>
    <span class="display_none SocketUrlHandleNotify" next="<?php echo Yii::app()->createAbsoluteUrl('admin/ajaxSocket/handleNotify');?>" ></span>
    <span class="display_none SocketUrlDieuPhoiConfirm" next="<?php echo $mSocketNotify->getUrlConfirm();?>" ></span>
    <span class="display_none UrlIconNewNotify" ><?php echo $mSocketNotify->getStatusIcon(); ?></span>

    <?php if(in_array($cRole, $mSocketNotify->getArrRolePlayAudio())): // fix Jul1418  ?>
    <audio id="AudioPlayer" preload="auto" >
      <source src="http://io.huongminhgroup.com/upload/sound/iphone_sound_sms.wav" /> 
      <!--fallback if no mp3 support in browser--> 
    </audio>
    <audio id="AudioPlayerDieuPhoi" preload="auto" loop >
      <source src="http://io.huongminhgroup.com/upload/sound/iphone_6_original_mp3cut.net.wav" /> 
    </audio>

    <audio id="AudioPlayerNotify" preload="auto">
      <source src="http://io.huongminhgroup.com/upload/sound/FacebookNotificationSound.wav" /> 
      <!--fallback if no mp3 support in browser--> 
    </audio>
    <?php endif; ?>
    
    
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/huongminh_socket.js"></script>
    <script>
        var globalWebSocket;
        $(function () {
            transConfirmRead();
            orderConfirmRead();
    //        document.cookie = '';
            globalWebSocket = wsStart('<?php echo $url;?>');
        });

        $(window).on('beforeunload', function(){
            globalWebSocket.close();
    //        console.log(globalWebSocket.readyState);
        });

    </script>

<?php endif; // end if(in_array($cRole, GasSocketNotify::getRoleUpdateNotify()) ?>