<?php 
$gridCss = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview/styles.css';
Yii::app()->getClientScript()->registerCssFile($gridCss);
$data = $dataProvider->getData();
$cUid = MyFormat::getCurrentUid();
$tbHeigh = 200;
if(in_array($cUid, $mIssue->getListUidBigPageSize())){
    $tbHeigh = 600;
}

$c =1; $minDate = $maxDate = date('d-m-Y');
foreach ($data as $key => $value) {
    if($c == 1){
        $minDate = MyFormat::dateConverYmdToDmy($value->created_date, 'd-m-Y'); //
        $maxDate = $value->getFinishDay('d-m-Y');
        $c =0;
    }
    if(strtotime(MyFormat::dateConverYmdToDmy($value->created_date, 'd-m-Y')) < strtotime($minDate) ){
        $minDate = MyFormat::dateConverYmdToDmy($value->created_date, 'd-m-Y');
    }
    if(strtotime($value->getFinishDay('d-m-Y')) > strtotime($maxDate) ){
        $maxDate = $value->getFinishDay('d-m-Y');
    }
}
$aDay = MyFormat::getArrayDay(MyFormat::dateConverDmyToYmd($minDate,'-'), MyFormat::dateConverDmyToYmd($maxDate,'-'));
$now_date = strtotime(date('Y-m-d'));
?>
<style>
    .weekend {
        background: #cecece;
    }
    .day-yellow{
        background: #ffe895;
    }
    .day-blue {
        background:#87b9f7;
    }
    .day-red {
        background:#ff6c6c;
    }
    .day-now-blue {
        background:#939cbd;
    }
    .day-blue-red {
        background:#bf1c1c;
    }
</style>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scroll/jquery.freezetablecolumns.1.1.js"></script>
<div class="grid-view display_none">
    <table id="freezetablecolumns" class="items hm_table freezetablecolumns">
        <thead>
        <tr style="">
            <th class="w-20 ">STT<br>&nbsp;</th>
            <th class="w-250 ">Danh sách công việc</th>
            <th class="w-60 ">Start</th>
            <th class="w-60 ">Finish</th>
            <th class="w-60 ">End</th>
            <?php foreach ($aDay as $key => $value): 
                $value = strtotime($value); ?>
            <th class='w-20'>
                <?php echo date('D', $value); ?><br>
                <?php  echo date('d', $value); ?>
            </th>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
            <?php foreach ($data as $key => $item): 
            $start_date = strtotime(MyFormat::dateConverYmdToDmy($item->created_date, 'd-m-Y'));
            $finish_date = strtotime($item->getFinishDay('d-m-Y'));    
            $close_date = strtotime($item->getCloseDate('d-m-Y'));
            ?>
                <tr>
                    <td class="item_c"><?php echo $key+1; ?></td>
                    <td class="item_l"><?php echo $item->title; ?>
                            <?php // echo $item->getCloseDate('d-m-Y'); ?>
                    </td>
                    <td class="item_c"><?php echo MyFormat::dateConverYmdToDmy($item->created_date, 'd/m/Y'); ?></td>
                    <td class="item_c"><?php echo $item->getFinishDay('d/m/Y'); ?></td>
                    <td class="item_c"><?php echo $item->getCloseDate('d/m/Y'); ?></td>
                    
                    <?php foreach ($aDay as $key => $value): 
                    $value = strtotime($value); 
                    $day = date('D', $value);
                    $css_class = '';
                    if($start_date <= $value && $value <= $finish_date ) $css_class = 'day-blue ';
//                    if($close_date == $value) $css_class ='day-red ';
                    if($value == $now_date) $css_class ='day-yellow ';
                    if($value == $now_date && $start_date <= $value && $value <= $finish_date ) $css_class ='day-now-blue';
//                    if($close_date == $value && $start_date <= $value && $value <= $finish_date ) $css_class ='day-blue-red';
                    ?>
                        <td class='w-10 <?php echo $css_class; ?>'>
                            &nbsp;
                        </td>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print-store-card.css" />
<script type="text/javascript">
    var widthInner = window.innerWidth;
    console.log(widthInner);
    fnAddClassOddEven('items');
    $('.freezetablecolumns').each(function () {
        var id_table = $(this).attr('id');
        if(id_table == 'freezetablecolumns'){
            $('#' + id_table).freezeTableColumns({
                width:      widthInner*0.9,<?php //echo Users::WIDTH_freezetable;?>   // required - bt 1100
                height:      <?php echo $tbHeigh;?>,   // required
                numFrozen: 5,     // optional
                frozenWidth: 300,   // optional
                clearWidths: true  // optional
            });
        }
        
    });
    $(window).load(function () {
        var index = 1;
        $('.freezetablecolumns').each(function () {
            if (index == 1)
                $(this).closest('div.grid-view').show();
            index++;
        });
        fnAddClassOddEven('items');
    });
    
</script>