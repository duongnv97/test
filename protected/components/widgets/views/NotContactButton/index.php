
<?php 
$url = Yii::app()->createAbsoluteUrl("admin/ajax/createNotContact");
?>
<div class="form">
    <a href="<?php echo $url; ?>" class="btn_cancel f_size_14 not_contact_btn text_under_none">Không liên lạc được Sale, GĐKV</a>
</div>
<script>
    $('.not_contact_btn').colorbox({
        iframe: true,
        overlayClose: true, escKey: true,
        innerHeight: '550',
        innerWidth: '1000', close: '<span title=close>close</span>',
        onClosed: function () { // update view when close colorbox
            
        }
    });
</script>