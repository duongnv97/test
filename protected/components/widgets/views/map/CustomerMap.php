<?php 
    $mCustomer = Users::model()->findByPk($customer_id);
?>
<h3 class='title-info'>Tọa độ vị trí Khách hàng: <?php echo $mCustomer->slug;?></h3>
<div class="clr"></div>
<?php if(!empty($mCustomer->slug)): ?>
<?php
$tmp    = explode(",", $mCustomer->slug);
$info   = "<b>{$mCustomer->first_name}</b><br> {$mCustomer->address}";
$aDataChart     = [];
$aDataChart[]   = [$info, $tmp[0], $tmp[1], 0];
$js_array       = json_encode($aDataChart);
// echo "var javascript_array = ". $js_array . ";\n";
$idMap = 'map'.$mCustomer->id;
?>
<!--http://stackoverflow.com/questions/3059044/google-maps-js-api-v3-simple-multiple-marker-example
https://developers.google.com/maps/documentation/javascript/markers#animated
https://developers.google.com/maps/documentation/javascript/examples/infowindow-simple-max
-->
<!--<script src="http://maps.google.com/maps/api/js?key=AIzaSyCfsbfnHgTh4ODoXLXFfEFhHskDhnRVtjQ" type="text/javascript"></script>-->
<div id="<?php echo $idMap;?>" style="width: 100%; height: 400px;"></div>
<script type="text/javascript">
    var locations = <?php echo $js_array;?>;
    var map = new google.maps.Map(document.getElementById('<?php echo $idMap;?>'), {
      zoom: 18,
      center: new google.maps.LatLng(<?php echo $tmp[0];?>, <?php echo $tmp[1];?>),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;
//    var image = 'http://daukhi.huongminhgroup.com/apple-touch-icon.png';

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(locations[i][1], locations[i][2]),
          map: map
  //        icon: image
        });

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            infowindow.setContent(locations[i][0]);
            infowindow.open(map, marker);
          }
        })(marker, i));
      
        var infowindow2 = new google.maps.InfoWindow({
              content: locations[i][0],
        });
        infowindow2.open(map, marker);
    }
  </script>
<?php endif; ?>