<?php if($mTransactionHistory && $mTransactionHistory->source == Sell::SOURCE_APP): ?>
<?php 
$googleAdd      = $mTransactionHistory->address;
$temp           = explode(',', $mTransactionHistory->google_map);
$mapLatitude    = $temp[0];
$mapLongitude   = $temp[1];

$index = 1;
$source = '/themes/gas/new-icon/status_busy.png';
$icon = Yii::app()->createAbsoluteUrl("/").$source;
//$aDataChart[]   = array('xxxx xx', $tmp[0], $tmp[1], $index++, $icon);
$aDataChart[]   = array($googleAdd, $mapLatitude, $mapLongitude, $index++, $icon);

$js_array_employee = json_encode($aDataChart);
$idMap      = str_replace('.', '', $mapLatitude);
$infowindow = 'infowindow_content'.$idMap;
?>
<div id="<?php echo $idMap;?>" style="width: 100%; height: 400px;"></div>
<div id="<?php echo $infowindow;?>">
  <span id="place-name"  class="title"></span><br>
  <span id="place-address"></span>
</div>
<?php
/** for detail =>  https://developers.google.com/maps/documentation/javascript/examples/places-autocomplete
 *  https://developers.google.com/places/web-service/usage?hl=en_US
 */
    $mGateApi = new GateApi();
    $js_array = $mGateApi->getLocationAgent(['GetLogo24h'=>1]);
?>
<?php if($loadJavaScript): ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfsbfnHgTh4ODoXLXFfEFhHskDhnRVtjQ&libraries=places&callback=initMap"
        async defer></script>
    <?php endif; ?>
    <script>
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
      var <?php echo $infowindow;?>;
      function initMap() {
        var locations       = <?php echo $js_array;?>;
        var locationsUser   = <?php echo $js_array_employee;?>;
          
        <?php echo $infowindow;?> = new google.maps.Map(document.getElementById('<?php echo $idMap;?>'), {
          center: {lat: <?php echo $mapLatitude;?>, lng: <?php echo $mapLongitude;?>},
          zoom: 12
        });

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('<?php echo $infowindow;?>');
        infowindow.setContent(infowindowContent);
        
        // Apr 24, 2017 for agent spj icon
        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
              position: new google.maps.LatLng(locations[i][1], locations[i][2]),
              map: <?php echo $infowindow;?>,
              label: locations[i][7], // agent name
              icon: locations[i][4]
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
              return function() {
                infowindow.setContent(locations[i][0]);
                infowindow.open(<?php echo $infowindow;?>, marker);
              }
            })(marker, i));
            
            // Add the circle for this city to the map.
          var cityCircle = new google.maps.Circle({
            strokeColor: '#81D4FA',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#B3E5FC',
            fillOpacity: 0.35,
            map: <?php echo $infowindow;?>,
            center: new google.maps.LatLng(locations[i][1], locations[i][2]),
            radius: locations[i][5] // in meters
          });
        }
        // locations[i][6] // is agent id
        // Apr 24, 2017 for agent spj icon
        
        // May 19, 2017 for user GN location
//        for (i = 0; i < locationsUser.length; i++) {
//            marker = new google.maps.Marker({
//              position: new google.maps.LatLng(locationsUser[i][1], locationsUser[i][2]),
//              map: map,
//              icon: locationsUser[i][4]
//            });
//
//            google.maps.event.addListener(marker, 'click', (function(marker, i) {
//              return function() {
//                infowindow.setContent(locationsUser[i][0]);
//                infowindow.open(map, marker);
//              }
//            })(marker, i));
//            
//            var infowindow2 = new google.maps.InfoWindow({
//                content: locationsUser[i][0],
//            });
//            infowindow2.open(map, marker);
//        }
        // May 19, 2017 for user GN location
//        var marker = new google.maps.Marker({
//          map: map,
//          anchorPoint: new google.maps.Point(0, -29)
//        });
      }
    </script>

<?php endif; ?>