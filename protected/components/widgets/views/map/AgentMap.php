<?php
$mGateApi = new GateApi();
$js_array = $mGateApi->getLocationAgent($needMore); 
// echo "var javascript_array = ". $js_array . ";\n";

?>
<!--http://stackoverflow.com/questions/3059044/google-maps-js-api-v3-simple-multiple-marker-example
https://developers.google.com/maps/documentation/javascript/markers#animated
https://developers.google.com/maps/documentation/javascript/examples/infowindow-simple-max
-->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyCfsbfnHgTh4ODoXLXFfEFhHskDhnRVtjQ" type="text/javascript"></script>
<div id="map" style="width: 80%; height: 600px;"></div>
<script type="text/javascript">
    var locations       = <?php echo $js_array;?>;
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 7,
      center: new google.maps.LatLng(12.663024, 108.173691),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;
//    var image = 'http://daukhi.huongminhgroup.com/apple-touch-icon.png';

    for (i = 0; i < locations.length; i++) {
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map,
        label: locations[i][7], // agent name
        icon: locations[i][4]
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
      
        // Add the circle for this city to the map.
        var cityCircle = new google.maps.Circle({
          strokeColor: '#81D4FA',
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: '#B3E5FC',
          fillOpacity: 0.35,
          map: map,
          center: new google.maps.LatLng(locations[i][1], locations[i][2]),
          radius: locations[i][5] // in meters
        });
      
    }
  </script>