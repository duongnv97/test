<?php
class MapOnePointWidget extends CWidget
{
    public $title       = '';
    public $latitude    = 0;
    public $longitude   = 0;
    public $needMore    = [];
    public function run()
    {        
        if(!empty($this->latitude) && !empty($this->longitude)){
            $this->render('map/OnePoint', array('title'=> $this->title, 'latitude'=> $this->latitude, 'longitude'=> $this->longitude,'needMore' =>  $this->needMore));
        }
    }
}