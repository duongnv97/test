<?php
/** @Author: HOANG NAM 23/01/2018
 *  @Todo: Employee picture list
 *  @Code: NAM005
 *  @Param: 
 **/
class EmployeePictureWidget extends CWidget
{
    public $user = null,$title='',$field_name ='',$relations='',$onlyView = false;
    public $fakeEmployeeImages;

    public function run()
    {        
        if(empty($this->fakeEmployeeImages)){
           $this->fakeEmployeeImages = new EmployeesImages();
        }
        $this->render('user/employeePictures', array('fakeEmployeeImages'=>$this->fakeEmployeeImages,'onlyView'=>$this->onlyView,'user' => $this->user,'relations'=>$this->relations,'title'=>$this->title,'field_name'=>$this->field_name));
    }
}