<?php
class IssueWidget extends CWidget
{
    public $needMore = [];
    public $mIssue, $dataProvider;
    public function run()
    {        
        $this->render('Issue/Open', array('mIssue'=> $this->mIssue, 'dataProvider'=> $this->dataProvider, 'needMore' =>  $this->needMore));
    }
}