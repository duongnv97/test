<?php
class MapCustomerBookWidget extends CWidget
{
    public $mTransactionHistory = null;
    public $loadJavaScript      = true;
    public $needMore    = [];
    public function run()
    {        
        $this->render('map/MapCustomerBook', array('mTransactionHistory'=> $this->mTransactionHistory,'loadJavaScript' => $this->loadJavaScript, 'needMore' =>  $this->needMore));
    }
}