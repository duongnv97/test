<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DomainConst
 *
 * @author NguyenPT
 */
class DomainConst {
    //-----------------------------------------------------
    // Domain constants
    //-----------------------------------------------------
    /** String value "0" */
    const NUMBER_ZERO_VALUE                             = '0';
    /** String value "1" */
    const NUMBER_ONE_VALUE                              = '1';
    /** String value "2" */
    const NUMBER_TWO_VALUE                              = '2';
    /** Constant of Status: Active */
    const DEFAULT_STATUS_ACTIVE                         = DomainConst::NUMBER_ONE_VALUE;
    /** Constant of Status: Inactive */
    const DEFAULT_STATUS_INACTIVE                       = DomainConst::NUMBER_ZERO_VALUE;
    /** Constant of Gender: Male */
    const GENDER_MALE                                   = DomainConst::NUMBER_ONE_VALUE;
    /** Constant of Gender: Female */
    const GENDER_FEMALE                                 = DomainConst::NUMBER_TWO_VALUE;
    /** Constant of Gender: Other */
    const GENDER_OTHER                                  = DomainConst::NUMBER_ZERO_VALUE;
    /** Constant of Access status: Allow */
    const DEFAULT_ACCESS_ALLOW                          = DomainConst::NUMBER_ONE_VALUE;  
    /** Constant of Access status: Deny */
    const DEFAULT_ACCESS_DENY                           = DomainConst::NUMBER_ZERO_VALUE;
    /** Constant of Checkbox status: CHECKED */
    const CHECKBOX_STATUS_CHECKED                       = DomainConst::NUMBER_ONE_VALUE;
    /** Constant of Checkbox status: UNCHECKED */
    const CHECKBOX_STATUS_UNCHECKED                     = DomainConst::NUMBER_ZERO_VALUE;
    /** Default value of parent id: 0 */
    const DEFAULT_PARENT_VALUE                          = DomainConst::NUMBER_ZERO_VALUE;
    /** API Response status: Failed */
    const API_RESPONSE_STATUS_FAILED                    = 0;
    /** API Response status: Success */
    const API_RESPONSE_STATUS_SUCCESS                   = 1;
    /** API Response code: Bad request */
    const API_RESPONSE_CODE_BAD_REQUEST                 = 400;
    /** API Response code: Success */
    const API_RESPONSE_CODE_SUCCESS                     = 200;
    /** API Response code: Unauthorized */
    const API_RESPONSE_CODE_UNAUTHORIZED                = 401;
    
    /** Platform flag */
    const PLATFORM_IOS                                  = 0;
    const PLATFORM_ANDROID                              = 1;
    const PLATFORM_WINDOW                               = 2;
    const PLATFORM_WEB                                  = 3;
    
    /** Constant of splitter */
    const SPLITTER_TYPE_1                               = ', ';
    const SPLITTER_TYPE_2                               = ',';
    /** Date format */
    const DATE_FORMAT_1                                 = 'Y-m-d H:i:s';
    /** Date format */
    const DATE_FORMAT_2                                 = 'dd/mm/yy';
    /** Date format */
    const DATE_FORMAT_2_1                                 = 'mm/yy';
    /** Date format */
    const DATE_FORMAT_3                                 = 'd/m/Y';
    /** Date format */
    const DATE_FORMAT_4                                 = 'Y-m-d';
    /** Date format */
    const DATE_FORMAT_5                                 = 'd \t\h\g m\, Y';
    /** Date format */
    const DATE_FORMAT_6                                 = 'Y/m/d';
    /** Date format */
    const DATE_FORMAT_7                                 = 'Y/m/d H:i:s';
    /** Date format */
    const DATE_FORMAT_8                                 = 'H:i\, d \t\h\g m\, Y';
    /** Date format */
    const DATE_FORMAT_9                                 = 'YmdHis';
    /** Date format */
    const DATE_FORMAT_10                                = 'Ymd';
    /** Date format */
    const DATE_FORMAT_11                                = 'd/m/Y H:i';
    /** Date format */
    const DATE_FORMAT_12                                = 'm/Y';
    /** Date format */
    const DEFAULT_TIMEZONE                              = 'Asia/Ho_Chi_Minh';
    /** Default IP address */
    const DEFAULT_IP_ADDRESS                            = '192.168.1.1';
    /** Minimum length of password */
    const PASSW_LENGTH_MIN                              = 6;
    /** Maximum length of password */
    const PASSW_LENGTH_MAX                              = 50;
    /** Prefix of store card id string */
    const STORE_CARD_ID_PREFIX                          = 'TK';
    /** Prefix of order id string */
    const ORDER_ID_PREFIX                               = 'ĐH';
    /** Prefix of customer id string */
    const CUSTOMER_ID_PREFIX                            = 'KH';
    /** Prefix of medical record id string */
    const MEDICAL_RECORD_ID_PREFIX                      = 'HS';
    
    
    //-----------------------------------------------------
    // Keys setting 
    //-----------------------------------------------------
    /** Token */
    const KEY_TOKEN                         = 'token';
    /** Keyword */
    const KEY_KEYWORD                       = 'keyword';
    /** Q */
    const KEY_ROOT_REQUEST                  = 'q';
    /** Notify Id */
    const KEY_NOTIFY_ID                     = 'notify_id';
    /** Type */
    const KEY_TYPE                          = 'type';
    /** Object Id */
    const KEY_OBJECT_ID                     = 'obj_id';
    /** Issue Id */
    const KEY_ISSUE_ID                      = 'issue_id';
    /** Customer Id */
    const KEY_CUSTOMER_ID                   = 'customer_id';
    /** Customer Name */
    const KEY_CUSTOMER_NAME                 = 'customer_name';
    /** Customer Address */
    const KEY_CUSTOMER_ADDRESS              = 'customer_address';
    /** Customer Agent */
    const KEY_CUSTOMER_AGENT                = 'customer_agent';
    /** Customer Agent */
    const KEY_CUSTOMER_AGENT_DELIVERY       = 'customer_delivery_agent';
    /** Customer Agent */
    const KEY_CUSTOMER_AGENT_DELIVERY_ID    = 'customer_delivery_agent_id';
    /** Customer Phone */
    const KEY_CUSTOMER_PHONE                = 'customer_phone';
    /** Customer contact */
    const KEY_CUSTOMER_CONTACT              = 'customer_contact';
    /** Title */
    const KEY_TITLE                         = 'title';
    /** Message */
    const KEY_MESSAGE                       = 'message';
    /** Problem */
    const KEY_PROBLEM                       = 'problem';
    /** Page */
    const KEY_PAGE                          = 'page';
    /** Chief monitor id */
    const KEY_CHIEF_MONITOR_ID              = 'chief_monitor_id';
    /** Monitor agent id */
    const KEY_MONITOR_AGENT_ID              = 'monitor_agent_id';
    /** Accounting id */
    const KEY_ACCOUNTING_ID                 = 'accounting_id';
    /** Username */
    const KEY_USERNAME                      = 'username';
    /** Password */
    const KEY_PASSWORD                      = 'password';
    /** GCM device token */
    const KEY_GCM_DEVICE_TOKEN              = 'gcm_device_token';
    /** APNS device token */
    const KEY_APNS_DEVICE_TOKEN             = 'apns_device_token';
    /** News Id */
    const KEY_NEWS_ID                       = 'news_id';
    /** Device phone */
    const KEY_DEVICE_PHONE                  = 'device_phone';
    /** Id */
    const KEY_ID                            = 'id';
    /** Note Customer */
    const KEY_NOTE_CUSTOMER                 = 'note_customer';
    /** qty_12 */
    const KEY_QTY_12                        = 'qty_12';
    /** qty_50 */
    const KEY_QTY_50                        = 'qty_50';
    /** qty_12_list */
    const KEY_QTY_12_LIST                   = 'qty_12_list';
    /** qty_50_list */
    const KEY_QTY_50_LIST                   = 'qty_50_list';
    /** Phone */
    const KEY_PHONE                         = 'phone';
    /** Password */
    const KEY_PASSWORD_CONFIRM              = 'password_confirm';
    /** First name */
    const KEY_FIRST_NAME                    = 'first_name';
    /** Province Id */
    const KEY_PROVINCE_ID                   = 'province_id';
    /** District Id */
    const KEY_DISTRICT_ID                   = 'district_id';
    /** Ward Id */
    const KEY_WARD_ID                       = 'ward_id';
    /** Street id */
    const KEY_STREET_ID                     = 'street_id';
    /** Street */
    const KEY_STREET                        = 'street';
    /** House Number */
    const KEY_HOUSE_NUMBER                  = 'house_numbers';
    /** Sign up code */
    const KEY_SIGN_UP_CODE                  = 'signup_code';
    /** Employee Id */
    const KEY_EMPLOYEE_ID                   = 'employee_id';
    /** Employee Name */
    const KEY_EMPLOYEE_NAME                 = 'employee_name';
    /** Employee phone */
    const KEY_EMPLOYEE_PHONE                = 'employee_phone';
    /** Employee code */
    const KEY_EMPLOYEE_CODE                 = 'employee_code';
    /** Employee image */
    const KEY_EMPLOYEE_IMG                  = 'employee_image';
    /** Uphold type */
    const KEY_UPHOLD_TYPE                   = 'type_uphold';
    /** Uphold type */
    const KEY_UPHOLD_TYPE_IDX               = 'uphold_type';
    /** Content */
    const KEY_CONTENT                       = 'content';
    /** Contact person */
    const KEY_CONTACT_PERSON                = 'contact_person';
    /** Contact telephone number */
    const KEY_CONTACT_TEL                   = 'contact_tel';
    /** Status */
    const KEY_STATUS                        = 'status';
    /** Status number */
    const KEY_STATUS_NUMBER                 = 'status_number';
    /** Uphold Id */
    const KEY_UPHOLD_ID                     = 'uphold_id';
    /** Hours handle */
    const KEY_HOURS_HANDLE                  = 'hours_handle';
    /** Contact phone */
    const KEY_CONTACT_PHONE                 = 'contact_phone';
    /** Contact */
    const KEY_CONTACT                       = 'contact';
    /** Contact note */
    const KEY_CONTACT_NOTE                  = 'contact_note';
    /** Note */
    const KEY_NOTE                          = 'note';
    /** Report wrong */
    const KEY_REPORT_WRONG                  = 'report_wrong';
    /** Note internal */
    const KEY_NOTE_INTERNAL                 = 'note_internal';
    /** Reply Id */
    const KEY_REPLY_ID                      = 'reply_id';
    /** Old password */
    const KEY_OLD_PASSWORD                  = 'old_password';
    /** New password */
    const KEY_NEW_PASSWORD                  = 'new_password';
    /** New password confirm */
    const KEY_NEW_PASSWORD_CONFIRM          = 'new_password_confirm';
    /** Code */
    const KEY_CODE                          = 'code';
    /** Record */
    const KEY_RECORD                        = 'record';
    /** Notify count text */
    const KEY_NOTIFY_COUNT_TEXT             = 'NotifyCountText';
    /** Issue create */
    const KEY_ISSUE_CREATE                  = 'issue_create';
    /** Total page */
    const KEY_TOTAL_PAGE                    = 'total_page';
    /** Total quantity */
    const KEY_TOTAL_QTY                     = 'total_qty';
    /** Promotion amount */
    const KEY_PROMOTION_AMOUNT              = 'promotion_amount';
    /** Discount amount */
    const KEY_DISCOUNT_AMOUNT               = 'discount_amount';
    /** Total */
    const KEY_TOTAL                         = 'total';
    /** LIST_CHIEF_MONITOR */
    const KEY_LIST_CHIEF_MONITOR            = 'LIST_CHIEF_MONITOR';
    /** LIST_MONITOR_AGENT */
    const KEY_LIST_MONITOR_AGENT            = 'LIST_MONITOR_AGENT';
    /** LIST_ACCOUNTING */
    const KEY_LIST_ACCOUNTING               = 'LIST_ACCOUNTING';
    /** Model issue */
    const KEY_MODEL_ISSUE                   = 'model_issue';
    /** Menu */
    const KEY_MENU                          = 'menu';
    /** Data uphold */
    const KEY_DATA_UPHOLD                   = 'data_uphold';
    /** Maximum upload size */
    const KEY_MAX_UPLOAD                    = 'max_upload';
    /** Data issue */
    const KEY_DATA_ISSUE                    = 'data_issue';
    /** Role id */
    const KEY_ROLE_ID                       = 'role_id';
    /** User id */
    const KEY_USER_ID                       = 'user_id';
    /** User name */
    const KEY_USER_NAME                     = 'user_name';
    /** owner_id */
    const KEY_OWNER_ID                      = 'owner_id';
    /** owner_name */
    const KEY_OWNER_NAME                    = 'owner_name';
    /** User information */
    const KEY_USER_INFO                     = 'user_info';
    /** Check menu */
    const KEY_CHECK_MENU                    = 'check_menu';
    /** Province list */
    const KEY_PROVINCE_LIST                 = 'province_list';
    /** District list */
    const KEY_DISTRICT_LIST                 = 'district_list';
    /** Ward list */
    const KEY_WARD_LIST                     = 'ward_list';
    /** Detail id */
    const KEY_DETAIL_ID                     = 'detail_id';
    /** Model uphold */
    const KEY_MODEL_UPHOLD                  = 'model_uphold';
    /** Address */
    const KEY_ADDRESS                       = 'address';
    /** Address */
    const KEY_GOOGLE_ADDRESS                = 'google_address';
    /** Image avatar */
    const KEY_IMG_AVATAR                    = 'image_avatar';
    /** Notify type */
    const KEY_NOTIFY_TYPE                   = 'notify_type';
    /** Request type */
    const KEY_REQUEST_TYPE                  = 'request_by';
    /** Request type */
    const KEY_RATING_STATUS                 = 'rating_status';
    /** Request type */
    const KEY_RATING_TYPE                   = 'rating_type';
    /** Request type */
    const KEY_RATING_NOTE                   = 'rating_note';
    /** Other information */
    const KEY_OTHER_INFO                    = 'OtherInfo';
    /** Uphold last id */
    const KEY_UPHOLD_ID_LASTEST             = 'uphold_id_lastest';
    /** Need change pass */
    const KEY_NEED_CHANGE_PASS              = 'need_change_pass';
    /** Need update app */
    const KEY_NEED_UPDATE_APP               = 'need_update_app';
    /** Detail reply id */
    const KEY_DETAIL_REPLY_ID               = 'detail_reply_id';
    /** Uphold create */
    const KEY_UPHOLD_CREATE                 = 'uphold_create';
    /** Role name */
    const KEY_ROLE_NAME                     = 'role_name';
    /** List streets */
    const KEY_LIST_STREET                   = 'list_street';
    /** List agents */
    const KEY_LIST_AGENT                    = 'list_agent';
    /** List hgd type */
    const KEY_LIST_HGD_TYPE                 = 'list_hgd_type';
    /** List hgd_invest */
    const KEY_LIST_HGD_INVEST               = 'list_hgd_invest';
    /** Name */
    const KEY_NAME                          = 'name';
    /** Gender */
    const KEY_GENDER                        = 'gender';
    /** Data */
    const KEY_DATA                          = 'data';
    /** Key transaction id */
    const KEY_TRANSACTION_ID                = 'transaction_id';
    /** Key transaction key */
    const KEY_TRANSACTION_KEY               = 'transaction_key';
    /** Key transaction type */
    const KEY_TRANSACTION_TYPE              = 'transaction_type';
    /** Grand total */
    const KEY_GRAND_TOTAL                   = 'grand_total';
    /** Setting key: is login */
    const KEY_SETTING_IS_LOGGING            = 'gasservice.isLogin';
    /** Setting key: user token */
    const KEY_SETTING_USER_TOKEN            = 'gasservice.user.token';
    /** Setting key: orderVipDescription */
    const KEY_SETTING_ORDER_VIP_DESCRIPTION = 'gasservice.order.orderVipDescription';
    /** Setting key: training mode */
    const KEY_SETTING_TRAINING_MODE         = 'gasservice.trainningMode';
    /** Setting key: Transaction id */
    const KEY_SETTING_TRANSACTION_ID        = 'gasservice.transaction.id';
    /** Setting key: Transaction key */
    const KEY_SETTING_TRANSACTION_KEY       = 'gasservice.transaction.key';
    /** Setting key: Temp token */
    const KEY_SETTING_TEMP_TOKEN            = 'gasservice.temp.token';
    /** Setting key: Debug color */
    const KEY_SETTING_DEBUG_COLOR           = 'gasservice.debug.color';
    /** Setting key: Debug toast */
    const KEY_SETTING_DEBUG_TOAST           = 'gasservice.debug.toast';
    /** Setting key: Debug zoom */
    const KEY_SETTING_DEBUG_ZOOM            = 'gasservice.debug.zoom';
    /** Setting key: Debug Is Gas service */
    const KEY_SETTING_DEBUG_IS_GAS_SERVICE  = 'gasservice.debug.isGasService';
    /** Setting key: Debug Is Show number picker */
    const KEY_SETTING_DEBUG_IS_SHOW_NUM_PICKER  = 'gasservice.debug.isShowNumPicker';
    /** Setting key: Debug Is Show number picker */
    const KEY_SETTING_DEBUG_IS_SHOW_TOP_ICON    = 'gasservice.debug.isShowTopIcon';
    /** Setting key: Debug Flag use material_name or material_name_short */
    const KEY_SETTING_DEBUG_IS_USE_MATERIAL_NAME_SHORT    = 'gasservice.debug.isUseMaterialNameShort';
    /** Setting key: List gas information */
    const KEY_LIST_GAS_INFORMATION          = 'gasservice.listGasInfo';
    /** Setting key: Role id */
    const KEY_SETTING_ROLE_ID               = 'gasservice.roleId';
    /** Setting key: Material type id */
    const KEY_MATERIALS_TYPE_ID             = 'materials_type_id';
    /** Setting key: Material id */
    const KEY_MATERIALS_ID                  = 'materials_id';
    /** Setting key: Material name */
    const KEY_MATERIALS_NAME                = 'materials_name';
    /** Setting key: Material no */
    const KEY_MATERIALS_NO                  = 'materials_no';
    /** Setting key: Material short name */
    const KEY_MATERIALS_NAME_SHORT          = 'materials_name_short';
    /** Setting key: Material price */
    const KEY_MATERIALS_PRICE               = 'material_price';
    /** Setting key: Price */
    const KEY_PRICE                         = 'price';
    /** Setting key: Quantity */
    const KEY_QUANTITY                      = 'qty';
    /** Setting key: Real quantity */
    const KEY_QUANTITY_REAL                 = 'qty_real';
    /** Setting key: Serial */
    const KEY_SERI                          = 'seri';
    /** Setting key: Serial */
    const KEY_SERIAL                        = 'serial';
    /** Setting key: KEY_KG_EMPTY */
    const KEY_KG_EMPTY                      = 'kg_empty';
    /** Setting key: KEY_KG_HAS_GAS */
    const KEY_KG_HAS_GAS                    = 'kg_has_gas';
    /** Setting key: Amount */
    const KEY_AMOUNT                        = 'amount';
    /** Setting key: Material image */
    const KEY_MATERIAL_IMAGE                = 'material_image';
    /** Setting key: Email */
    const KEY_EMAIL                         = 'email';
    /** Setting key: Agent */
    const KEY_AGENT                         = 'agent';
    /** Setting key: Agent id */
    const KEY_AGENT_ID                      = 'agent_id';
    /** Setting key: Agent name */
    const KEY_AGENT_NAME                    = 'agent_name';
    /** Setting key: Agent phone */
    const KEY_AGENT_PHONE                   = 'agent_phone';
    /** Setting key: Agent cell phone */
    const KEY_AGENT_CELL_PHONE              = 'agent_cell_phone';
    /** Setting key: Agent phone support */
    const KEY_AGENT_PHONE_SUPPORT           = 'agent_phone_support';
    /** Setting key: Agent address */
    const KEY_AGENT_ADDRESS                 = 'agent_address';
    /** Setting key: Agent latitude */
    const KEY_AGENT_LAT                     = 'agent_latitude';
    /** Setting key: Agent longitude */
    const KEY_AGENT_LONG                    = 'agent_longitude';
    /** Setting key: Agent list */
    const KEY_AGENT_LIST                    = 'agent_list';
    /** Setting key: Information of agent */
    const KEY_INFO_AGENT                    = 'info_agent';
    /** Setting key: Information of gas */
    const KEY_INFO_GAS                      = 'info_gas';
    /** Setting key: Information of price */
    const KEY_INFO_PRICE                    = 'info_price';
    /** Setting key: Information of promotion */
    const KEY_INFO_PROMOTION                = 'info_promotion';
    /** Setting key: Distance 1 */
    const KEY_DISTANCE_1                    = 'distance_1';
    /** Setting key: Distance 2 */
    const KEY_DISTANCE_2                    = 'distance_2';
    /** Setting key: Allow update */
    const KEY_ALLOW_UPDATE                  = 'allow_update';
    /** Setting key: Order type */
    const KEY_ORDER_TYPE                    = 'order_type';
    /** Setting key: Type amount */
    const KEY_TYPE_AMOUNT                   = 'type_amount';
    /** Setting key: Amount discount */
    const KEY_AMOUNT_DISCOUNT               = 'amount_discount';
    /** Setting key: Discount type */
    const KEY_DISCOUNT_TYPE                 = 'discount_type';
    /** Setting key: Status cancel */
    const KEY_STATUS_CANCEL                 = 'status_cancel';
    /** Setting key: Order detail */
    const KEY_ORDER_DETAIL                  = 'order_detail';
    /** Setting key: Customer info */
    const KEY_CUSTOMER_INFO                 = 'customer_info';
    /** Setting key: Boss name */
    const KEY_BOSS_NAME                     = 'boss_name';
    /** Setting key: Boss phone */
    const KEY_BOSS_PHONE                    = 'boss_phone';
    /** Setting key: Manager name */
    const KEY_MANAGER_NAME                  = 'manage_name';
    /** Setting key: Manager phone */
    const KEY_MANAGER_PHONE                 = 'manage_phone';
    /** Setting key: Technical name */
    const KEY_TECHNICAL_NAME                = 'technical_name';
    /** Setting key: Technical phone */
    const KEY_TECHNICAL_PHONE               = 'technical_phone';
    
    /** Key total record */
    const KEY_TOTAL_RECORD                  = 'total_record';
    /** Code no */
    const KEY_CODE_NO                       = 'code_no';
    /** Level type */
    const KEY_LEVEL_TYPE                    = 'level_type';
    /** Created date */
    const KEY_CREATED_DATE                  = 'created_date';
    /** Created byte */
    const KEY_CREATED_BY                    = 'created_by';
    /** Sale name */
    const KEY_SALE_NAME                     = 'sale_name';
    /** Sale phone */
    const KEY_SALE_PHONE                    = 'sale_phone';
    /** Sale type */
    const KEY_SALE_TYPE                     = 'sale_type';
    /** Schedule month */
    const KEY_SCHEDULE_MONTH                = 'schedule_month';
    /** Last reply message */
    const KEY_LAST_REPLY_MESSAGE            = 'last_reply_message';
    /** Schedule Type */
    const KEY_SCHEDULE_TYPE                 = 'schedule_type';
    /** Uid login */
    const KEY_UID_LOGIN                     = 'uid_login';
    /** Date time handle */
    const KEY_DATE_TIME_HANDLE              = 'date_time_handle';
    /** Reply item */
    const KEY_REPLY_ITEM                    = 'reply_item';
    /** Image thumb */
    const KEY_IMG_THUMB                     = 'thumb';
    /** Image large */
    const KEY_IMG_LARGE                     = 'large';
    /** Image list */
    const KEY_IMAGES                        = 'images';
    /** Image */
    const KEY_IMAGE                         = 'image';
    /** Latitude */
    const KEY_LATITUDE                      = 'latitude';
    /** Version code */
    const KEY_VERSION_CODE                  = 'version_code';
    /** Longitude */
    const KEY_LONGITUDE                     = 'longitude';
    /** Uphold list */
    const KEY_UPHOLD_LIST                   = 'uphold_list';
    /** Uphold rating */
    const KEY_UPHOLD_RATING                 = 'uphold_rating';
    /** Version code */
    const KEY_APP_VERSION_CODE              = 'app_version_code';
    /** Confirm code */
    const KEY_CONFIRM_CODE                  = 'confirm_code';
    /** show_nhan_giao_hang */
    const KEY_SHOW_NHAN_GH                  = 'show_nhan_giao_hang';
    /** show_huy_giao_hang */
    const KEY_SHOW_HUY_GH                   = 'show_huy_giao_hang';
    /** Transaction History Id */
    const KEY_TRANSACTION_HISTORY_ID        = 'transaction_history_id';
    /** Call center uphold */
    const KEY_CALL_CENTER_UPHOLD            = 'call_center_uphold';
    /** Hotline */
    const KEY_HOTLINE                       = 'hotline';
    /** Flag gas 24h */
    const KEY_FLAG_GAS_24H                  = 'app_type';
    /** Key text of order type */
    const KEY_ORDER_TYPE_TEXT               = 'order_type_text';
    /** Key amount of order type */
    const KEY_ORDER_TYPE_AMOUNT             = 'order_type_amount';
    /** Key amount of bu vo */
    const KEY_AMOUNT_BU_VO                  = 'amount_bu_vo';
    /** Key expiration date */
    const KEY_EXPIRY_DATE                   = 'expiry_date';
    /** Key b50 kg */
    const KEY_B50                           = 'b50';
    /** Key b45 kg */
    const KEY_B45                           = 'b45';
    /** Key b12 kg */
    const KEY_B12                           = 'b12';
    /** Key b6 kg */
    const KEY_B6                            = 'b6';
    /** Key Unit */
    const KEY_UNIT                          = 'unit';
    /** Key Delivery date */
    const KEY_DATE_DELIVERY                 = 'date_delivery';
    /** Key Total gas */
    const KEY_TOTAL_GAS                     = 'total_gas';
    /** Key Total gas du */
    const KEY_TOTAL_GAS_DU                  = 'total_gas_du';
    /** Key note of employee */
    const KEY_NOTE_EMPLOYEE                 = 'note_employee';
    /** Key Name of gas */
    const KEY_NAME_GAS                      = 'name_gas';
    /** Key name of driver */
    const KEY_NAME_DRIVER                   = 'name_driver';
    /** Key name of car */
    const KEY_NAME_CAR                      = 'name_car';
    /** Key name of maintain employee */
    const KEY_NAME_EMPLOYEE_MAINTAIN        = 'name_employee_maintain';
    /** Key Cylinder information */
    const KEY_INFO_CYLINDER                 = 'info_vo';
    /** Key Order id */
    const KEY_ORDER_ID                      = 'order_id';
    /** Key Buying */
    const KEY_BUYING                        = 'buying';
    /** Key Platform */
    const KEY_PLATFORM                      = 'platform';
    /** Key Date from */
    const KEY_DATE_FROM                     = 'date_from';
    /** Key Date to */
    const KEY_DATE_TO                       = 'date_to';
    /** Key Customer Family Brand*/
    const KEY_CUSTOMER_FAMILY_BRAND         = 'hgd_thuong_hieu';
    /** List Customer family type */
    const KEY_HGD_TYPE                      = 'hgd_type';
    /** List Customer family type */
    const KEY_HGD_TYPE_ID                   = 'hgd_type_id';
    /** List Customer family type */
    const KEY_HGD_TIME_USE                  = 'hgd_time_use';
    /** List Customer family type */
    const KEY_HGD_DOI_THU                   = 'hgd_doi_thu';
    /** Key customer type */
    const KEY_CUSTOMER_TYPE                 = 'customer_type';
    /** Key latitude longitude */
    const KEY_LONG_LAT                      = 'latitude_longitude';
    /** Key list_hgd_invest_text */
    const KEY_HGD_INVEST_TEXT               = 'list_hgd_invest_text';
    /** Key Can update flag */
    const KEY_CAN_UPDATE_FLAG               = 'can_update';
    /** Key full name */
    const KEY_FULL_NAME                     = 'full_name';
    /** Key Model record */
    const KEY_MODEL_RECORD                  = 'model_record';
    /** Key Report */
    const KEY_HGD_REPORT                    = 'hgd_short_report';
    /** Key Material cylinder */
    const KEY_MATERIAL_VO                   = 'material_vo';
    /** Key Material gas */
    const KEY_MATERIAL_GAS                  = 'material_gas';
    /** Key the other material */
    const KEY_MATERIAL_HGD                  = 'material_hgd';
    /** Key Cancel order reasons */
    const KEY_ORDER_STATUS_CANCEL           = 'order_status_cancel';
    /** Key List order type */
    const KEY_ORDER_LIST_TYPE               = 'order_list_type';
    /** Key List order discount type */
    const KEY_ORDER_LIST_DISCOUNT_TYPE      = 'order_list_discount_type';
    /** Key List order discount type */
    const KEY_STORECARD_STATUS_CANCEL       = 'storecard_status_cancel';
    /** Key Action type */
    const KEY_ACTION_TYPE                   = 'action_type';
    /** Key Change type */
    const KEY_CHANGE_TYPE                   = 'change_type';
    /** Key App order id */
    const KEY_APP_ORDER_ID                  = 'app_order_id';
    /** Key Type of store card */
    const KEY_TYPE_IN_OUT                   = 'type_in_out';
    /** Key List Types of store card */
    const KEY_LIST_TYPE_IN_OUT              = 'list_type_in_out';
    /** Key List all materials */
    const KEY_LIST_ALL_MATERIAl             = 'list_all_material';
    /** Key List all materials */
    const KEY_LIST_CASHBOOK_MATER_LOOKUP    = 'list_cashbook_master_lookup';
    /** Key Support type */
    const KEY_SUPPORT_ID                    = 'support_id';
    /** Key Support type */
    const KEY_SUPPORT_TEXT                  = 'support_text';
    /** Key favorite limit */
    const KEY_LIMIT_FAVORITE                = 'limit_favorite';
    /** Key Support type list */
    const KEY_LIST_SUPPORT_EMPLOYEE         = 'list_support_employee';
    /** Key show button complete flag */
    const KEY_SHOW_BUTTON_COMPLETE          = 'show_button_complete';
    /** Key show button save flag */
    const KEY_SHOW_BUTTON_SAVE              = 'show_button_save';
    /** Key setting */
    const KEY_TOTAL_GAS_DU_KG               = 'total_gas_du_kg';
    /** Key setting */
    const KEY_SHOW_THU_TIEN                 = 'show_thu_tien';
    /** Key setting */
    const KEY_SHOW_CHI_GAS_DU               = 'show_chi_gas_du';
    /** Key setting */
    const KEY_SHOW_BUTTON_DEBIT             = 'show_button_debit';
    /** Key setting */
    const KEY_PAY_DIRECT                    = 'pay_direct';
    /** Key setting */
    const KEY_SHOW_BUTTON_CANCEL            = 'show_button_cancel';
    /** Key setting */
    const KEY_LIST_ID_IMAGE                 = 'list_id_image';
    /** Key lookup type */
    const KEY_LOOKUP_TYPE                   = 'lookup_type';
    /** Key master lookup id */
    const KEY_MASTER_LOOKUP_ID              = 'master_lookup_id';
    /** Key master lookup text */
    const KEY_MASTER_LOOKUP_TEXT            = 'master_lookup_text';
    /** Key date input */
    const KEY_DATE_INPUT                    = 'date_input';
    /** Key lookup type text */
    const KEY_LOOKUP_TYPE_TEXT              = 'lookup_type_text';
    /** Key list images */
    const KEY_LIST_IMAGE                    = 'list_image';
    /** Key begin quantity */
    const KEY_BEGIN                         = 'begin';
    /** Key in quantity */
    const KEY_IN                            = 'in';
    /** Key out quantity */
    const KEY_OUT                           = 'out';
    /** Key end quantity */
    const KEY_END                           = 'end';
    /** Key rows */
    const KEY_ROWS                          = 'rows';
    /** Key allow_update_storecard_hgd */
    const KEY_ALLOW_UPDATE_STORECARD_HGD    = 'allow_update_storecard_hgd';
    /** Key next_time_update_storecard_hgd */
    const KEY_NEXT_UPDATE_STORECARD_HGD     = 'next_time_update_storecard_hgd';
    /** Setting key: discount */
    const KEY_DISCOUNT                      = 'discount';
    /** Key amount of bu vo */
    const KEY_BU_VO                         = 'bu_vo';
    /** Key amount of revenue */
    const KEY_REVENUE                       = 'revenue';
    /** Key sum all */
    const KEY_SUM_ALL                       = 'sum_all';
    /** Key sum order type */
    const KEY_SUM_ORDER_TYPE                = 'sum_order_type';
    /** Key total revenue */
    const KEY_TOTAL_REVENUE                 = 'total_revenue';
    /** Key opening balance */
    const KEY_OPENING_BALANCE               = 'opening_balance';
    /** Key ending balance */
    const KEY_ENDING_BALANCE                = 'ending_balance';
    /** Balance */
    const KEY_BALANCE                       = 'balance';
    /** Key report_inventory */
    const KEY_REPORT_INVENTORY              = 'report_inventory';
    /** Key report_hgd */
    const KEY_REPORT_ORDER_FAMILY           = 'report_hgd';
    /** Key report_cashbook */
    const KEY_REPORT_CASHBOOK               = 'report_cashbook';
    /** Key note of create */
    const KEY_NOTE_CREATE                   = 'note_create';
    /** show_confirm */
    const KEY_SHOW_CONFIRM                  = 'show_confirm';
    /** show_cancel */
    const KEY_SHOW_CANCEL                   = 'show_cancel';
    /** send_to_id */
    const KEY_SEND_TO_ID                    = 'send_to_id';
    /** name_user_reply */
    const KEY_NAME_USER_REPLY               = 'name_user_reply';
    /** position */
    const KEY_POSITION                      = 'position';
    /** name_user_to */
    const KEY_NAME_USER_TO                  = 'name_user_to';
    /** time_reply */
    const KEY_TIME_REPLY                    = 'time_reply';
    /** can_close */
    const KEY_CAN_CLOSE                     = 'can_close';
    /** can_reply */
    const KEY_CAN_REPLY                     = 'can_reply';
    /** list_reply */
    const KEY_LIST_REPLY                    = 'list_reply';
    /** time_send */
    const KEY_TIME_SEND                     = 'time_send';
    /** created_date_on_history */
    const KEY_CREATED_DATE_ON_HISTORY       = 'created_date_on_history';
    /** isIncomming */
    const KEY_IS_INCOMMING                  = 'isIncomming';
    /** isIncommingName */
    const KEY_IS_INCOMMING_NAME             = 'isIncommingName';
    /** account_id */
    const KEY_ACCOUNT_ID                    = 'account_id';
    /** account_name */
    const KEY_ACCOUNT_NAME                  = 'account_name';
    /** action_date */
    const KEY_ACTION_DATE                   = 'action_date';
    /** description */
    const KEY_DESCRIPTION                   = 'description';
    /** Flag successUpdate */
    const KEY_SUCCESS_UPDATE                = 'successUpdate';
    /** Alias */
    const KEY_ALIAS                         = 'alias';
    /** Child Actions */
    const KEY_CHILD_ACTIONS                 = 'childActions';
    /** Childen key */
    const KEY_CHILDREN                      = 'children';
    /** Actions */
    const KEY_ACTIONS                       = 'actions';
    /** expression */
    const KEY_EXPRESSION                    = 'expression';
    /** Allow */
    const KEY_ALLOW                         = 'allow';
    /** Deny */
    const KEY_DENY                          = 'deny';
    /** JSON_FIELD */
    const KEY_JSON_FIELD                    = 'JSON_FIELD';
    /** json */
    const KEY_JSON                          = 'json';
    /** home */
    const KEY_HOME                          = 'home';
    /** account */
    const KEY_ACCOUNT                       = 'account';
    /** customer_list */
    const KEY_CUSTOMER_LIST                 = 'customer_list';
    /** city_id */
    const KEY_CITY_ID                       = 'city_id';
    
    /**----- Message content -----*/
    const CONTENT00003  = 'Mã';
    
    const CONTENT00010  = 'Ngày tạo';
    const CONTENT00011  = 'Chi tiết';
    
    const CONTENT00026  = 'Trạng thái';
    const CONTENT00027  = 'Active';
    const CONTENT00028  = 'Inactive';
    const CONTENT00029  = 'Nam';
    const CONTENT00030  = 'Nữ';
    const CONTENT00031  = 'Khác';
    const CONTENT00032  = 'Allow';
    const CONTENT00033  = 'Deny';
    const CONTENT00034  = '#';
    const CONTENT00035  = 'Cập nhật thành công';
    
    const CONTENT00042  = 'Tên';
    
    const CONTENT00054  = 'Người tạo';
    
    const CONTENT00062  = 'Mô tả';
    
    const CONTENT00081  = '<p class="note">Dòng có dấu <span class="required">*</span> là bắt buộc.</p>';
    
    const CONTENT00139  = 'Ngày bắt đầu';
    const CONTENT00140  = 'Ngày kết thúc';
    
    const CONTENT00377  = 'Lưu';
    
    const CONTENT10000  = 'Công thức';
    const CONTENT10001  = 'Tên công thức';
    const CONTENT10002  = 'Hoạt động';
    const CONTENT10003  = 'Không hoạt động';
    const CONTENT10004  = 'Nhóm người dùng';
    const CONTENT10005  = 'Công thức';
    const CONTENT10006  = 'Loại';
    const CONTENT10007  = 'Loại công thức';
    const CONTENT10008  = 'Loại bảng lương';
    const CONTENT10009  = 'Người xét duyệt';
    const CONTENT10010  = 'Ngày xét duyệt';
    const CONTENT10011  = 'Ý kiến';
    const CONTENT10012  = 'Báo cáo lương';
    const CONTENT10013  = 'Tên hàm';
    const CONTENT10014  = 'Tên Tham số';
    const CONTENT10015  = 'Danh sách công thức';
    const CONTENT10016  = 'Cài đặt công thức';
    const CONTENT10017  = 'Xem công thức';
    const CONTENT10018  = 'Tạo mới công thức';
    const CONTENT10019  = 'Công thức tính theo từng ngày';
    const CONTENT10020  = 'Công thức tính theo khoảng thời gian';
    const CONTENT10021  = 'Tính theo ngày';
}
