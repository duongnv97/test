<?php
class UpdateSql1
{
    /**
     * @Author: ANH DUNG Now282017
     * @Todo: add tự động KM 1 Bình miễn phí cho một số user
     * UpdateSql1::autoAddPromotionGas24h();
     */
    public static function autoAddPromotionGas24h(){
        $from = time();
        /* 1. get all User need add
         * 2. call function auto add promotion
         */
        $criteria = new CDbCriteria();
        $criteria->compare('t.role_id', ROLE_CUSTOMER);
        $criteria->compare('t.type', CUSTOMER_TYPE_STORE_CARD);
        $criteria->compare('t.is_maintain', UsersExtend::STORE_CARD_HGD_APP);
        $criteria->addCondition('t.id <= 1277108');
        $models = Users::model()->findAll($criteria);
        foreach($models as $mUser){
            $mAppSignup = new AppSignup();
            $mAppSignup->autoAddPromotion($mUser, AppPromotion::PID_FREE_ONE);
        }
        
        $to = time();
        $second = $to-$from;
        echo count($models).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;  	
    }
	  
    
    /** @Author: ANH DUNG Dec 02, 2017
     *  @Todo: cập nhật chữ hoa cho mã giới thiệu
     **/
    public static function uppercaseCodePromotion() {
        
        $criteria = new CDbCriteria();
        $criteria->compare('t.type', AppPromotion::TYPE_REFERENCE_USER);
        $criteria->compare('t.id<73');
        $models = AppPromotion::model()->findAll($criteria);
        foreach($models as $item){
            $item->code_no = strtoupper($item->code_no);
            $item->update(['code_no']);
        }
        die;
    }
    
    
    /** @Author: ANH DUNG Dec 20, 2017
     *  @Todo: cập nhật type_customer cho BC công nợ tiền tổng quát admin/inventoryCustomer/reportDebitCash 
     **/
    public static function updateTypeCustomerReportDebitCash() {
        $customer_id = 758526;
        $mCustomer = Users::model()->findByPk($customer_id);
        if(!$mCustomer || $mCustomer->role_id != ROLE_CUSTOMER){
            die('Not customer');
        }
        $tableAppOrder          = GasAppOrder::model()->tableName();
        $tableAppOrderDetail    = GasAppOrderCDetailReal::model()->tableName();
        $tableEmployeeCashbook  = EmployeeCashbook::model()->tableName();
        
        $sql1 = "UPDATE `$tableAppOrder` SET `type_customer` = '$mCustomer->is_maintain' WHERE customer_id=$customer_id";
        $sql2 = "UPDATE `$tableAppOrderDetail` SET `type_customer` = '$mCustomer->is_maintain' WHERE customer_id=$customer_id";
        $sql3 = "UPDATE `$tableEmployeeCashbook` SET `type_customer` = '$mCustomer->is_maintain' WHERE customer_id=$customer_id";
        
        Yii::app()->db->createCommand($sql1)->execute();
        Yii::app()->db->createCommand($sql2)->execute();
        Yii::app()->db->createCommand($sql3)->execute();
        
        die('run ok');
    }
    
    /** @Author: ANH DUNG Jan 07, 2018
     *  @Todo: cap nhat sang ben solr cua KH app
     **/
    public static function fixUpdateCustomerAppSolr() {
//        die;
        $criteria = new CDbCriteria();
//        $criteria->compare("role_id", ROLE_AGENT);
        $criteria->compare("role_id", ROLE_CUSTOMER);
        $criteria->compare("type", CUSTOMER_TYPE_STORE_CARD);
//        $criteria->compare('t.channel_id', Users::CON_LAY_HANG);
//        $criteria->compare("sale_id", 868857);
//        $criteria->compare("province_id", GasProvince::TINH_VUNG_TAU);
//        $criteria->compare("is_maintain", UsersExtend::STORE_CARD_HGD_APP);
        $criteria->addInCondition( 't.is_maintain', CmsFormatter::$aTypeIdMakeUsername);
        $models = Users::model()->findAll($criteria);
        foreach($models as $mUser){
            $mUser->solrAdd();
        }
        die;
    }
    
    /** @Author: ANH DUNG Jan 19, 2018
     *  @Todo: fix date_apply KM gas24h, cập nhật ngày sử dụng KM của gas24h
     **/
    public static function gas24hUpdatePromotion() {
        $count = 0;
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.status_use='.  AppPromotionUser::STATUS_COMPLETE);
        $models = AppPromotionUser::model()->findAll($criteria);
        foreach($models as $item){
            $criteria = new CDbCriteria();
            $criteria->addCondition('t.app_promotion_user_id='.  $item->id);
            $mSell = Sell::model()->find($criteria);
            if($mSell){
                $count++;
                $item->date_apply = $mSell->created_date_only;
                $item->update(['date_apply']);
            }
        }
        echo '<pre>';
        print_r($count);
        echo '</pre>';
        die;
    }
    
    /** @Author: ANH DUNG Mar 10, 2018
     *  @Todo: update role ownewr of AppPromotion
     **/
    public static function updateRoleOwner() {
        $criteria = new CDbCriteria();
        $criteria->compare('type', AppPromotion::TYPE_REFERENCE_USER);
        $models = AppPromotion::model()->findAll($criteria);
        foreach($models as $item){
            $mUser = Users::model()->findByPk($item->owner_id);
            if($mUser){
                $item->owner_role_id = $mUser->role_id;
                $item->update(['owner_role_id']);
            }
        }
        die('ok run');
    }
    
    /** @Author: ANH DUNG Mar 29, 2018
     *  @Todo: update lại time complete của đơn hàng bò mối Tháng 3
     **/
    public static function updateTimeCompleteAppOrder() {
        die; // last run -> 1474 done in: 4  Second
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->compare("t.status", GasAppOrder::STATUS_COMPPLETE);
        $criteria->addCondition("t.date_delivery >= '2018-03-01'");
        $criteria->addCondition("t.complete_time IS NULL");
        $criteria->limit = 5000;
        $models = GasAppOrder::model()->findAll($criteria);
        $count = 0;
        foreach($models as $item){
            $mEventComplete = new TransactionEvent();
            $mEventComplete->transaction_history_id = $item->id;
            $mEventComplete->type                   = TransactionEvent::TYPE_BO_MOI;
            $mEventComplete =  $mEventComplete->getOrderComplete();
            if($mEventComplete){
                $item->complete_time = $mEventComplete->created_date;
                $item->update(['complete_time']);
                $count++;
            }
        }
        $to = time();
        $second = $to-$from;
        echo $count.' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;
        
    }

    /** @Author: ANH DUNG Apr 07, 2018
     *  @Todo: Cập nhật đại lý của user nhập app
     **/
    public static function fixAgentPromotion() {
        $models = AppPromotionUser::model()->findAll();
        foreach($models as $item){
            $item->update(['agent_id', 'province_id', 'district_id']);
        }
        echo 'done';die;
        
    }    
    /** @Author: ANH DUNG Apr 10, 2018
     *  @Todo: Cập nhật is_maintain của user Token
     **/
    public static function fixUsersTokensIsMaintain() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.type<>' . UsersTokens::LOGIN_WEB );
        $models = UsersTokens::model()->findAll($criteria);
        foreach($models as $item){
            $mUser = $item->rUser;
            if($mUser){
                $item->is_maintain = $mUser->is_maintain;
                $item->update(['is_maintain']);
            }
        }
        echo count($models). ' done';die;
    }
    
    /** @Author: ANH DUNG May 03, 2018
     *  @Todo: cập nhật sale id của KH App nhập mã Ref Telesale
     **/
    public static function updateSaleIdTelesale() {
        ini_set('memory_limit','1500M');
        $mAppPromotionUser = new AppPromotionUser();
        $mAppPromotionUser->getListMarketing();
        
        $mAppPromotion  = new AppPromotion();
        
        $criteria = new CDbCriteria();
        $criteria->addInCondition('t.ref_id', $mAppPromotion->getListUidPttt());
//        $criteria->compare('t.ref_id', 1014257);
        $criteria->compare('DATE(t.created_date)', '2018-08-11');
        $models = ReferralTracking::model()->findAll($criteria);
        echo '<pre>';
        print_r(count($models));
        echo '</pre>';
        die;
        foreach($models as $k => $item){
            $mCustomer = Users::model()->findByPk($item->invited_id);
            $mCustomer->sale_id = $item->ref_id;
            $mCustomer->update(['sale_id']);
            // update table sell
            $criteria = new CDbCriteria();
            $criteria->compare( 'customer_id', $item->invited_id);
            $aUpdate = array('sale_id' => $item->ref_id);
            Sell::model()->updateAll($aUpdate, $criteria);
            SellDetail::model()->updateAll($aUpdate, $criteria);
        }
        die(' ok done');
    }
    
    
    /** @Author: ANH DUNG May 28, 2018
     *  @Todo: cập nhật date_order cho cac ma da confirm Telesale
     **/
    public static function updateDateOrderTelesale() {
        ini_set('memory_limit','1500M');
        $mAppCache      = new AppCache();
        $idCallCenter   = $mAppCache->getListdataUserByRole(ROLE_CALL_CENTER);
        $idCallPvkh     = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MAINTAIN);
        $idCallCenter   = array_keys($idCallCenter);
        $idCallPvkh     = array_keys($idCallPvkh);
        $aId            = array_merge($idCallCenter, $idCallPvkh);
        
        $criteria = new CDbCriteria();
        $criteria->compare('t.status', 1);
        $criteria->addCondition('t.date_order IS NULL');
        $criteria->addCondition("DATE(t.created_date) >= '2018-04-01'");
        $criteria->addCondition("t.ref_id IN (". implode(',', $aId).")");
        $models = ReferralTracking::model()->findAll($criteria);
        echo '<pre>';
        print_r(count($models));
        echo '</pre>';
        die;
        foreach($models as $k => $item){
            // update table sell
            $mSell = new Sell();
            $mSell->customer_id = $item->invited_id;
            $mSell = $mSell->getOrderLastestOfCustomer();
            if(!empty($mSell)){
                $item->date_order = $mSell->created_date_only;
                $item->update(['date_order']);
            }
        }
        die(' ok done');
    }

    /** @Author: ANH DUNG May 28, 2018
     *  @Todo: fix update first order của đơn app Telesale và PVKH
     **/
    public static function updateFirstOrderTelesale() {
        ini_set('memory_limit','1500M');
        $mAppCache      = new AppCache();
        $idCallCenter   = $mAppCache->getListdataUserByRole(ROLE_CALL_CENTER);
        $idCallPvkh     = $mAppCache->getListdataUserByRole(ROLE_EMPLOYEE_MAINTAIN);
        $idCallCenter   = array_keys($idCallCenter);
        $idCallPvkh     = array_keys($idCallPvkh);
        $aId            = array_merge($idCallCenter, $idCallPvkh);
        $aId[] = 936982;// Phạm Thị Hà
        
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.status=' . Sell::STATUS_PAID);
        $criteria->addCondition("t.created_date_only >= '2018-10-17'");
        $criteria->addCondition("t.sale_id > 0 AND first_order=0");
        $models = Sell::model()->findAll($criteria);
//        echo '<pre>';
//        print_r(count($models));
//        echo '</pre>';
//        die;
        $count = 0;
        foreach($models as $item){
            $item->setFirstOrder();
//            $item->first_order = Sell::IS_FIRST_ORDER;
            if($item->first_order){
                $count++;
            }
            $item->update(['first_order']);
        }
        echo '<pre>';
        print_r($count);
        echo '</pre>';
        die;
    }
    
    /** @Author: ANH DUNG Jun 30, 2018
     *  @Todo: Anh Đức YC chuyển KH mối CTY sang  loại KH bò, còn KH mối của sale vẫn giữ loại KH cũ
     * chuyển loại KH + sale_id 
     * Done Jun3018 2847 done in: 429 Second <=> 7.15 Minutes
     **/
    public function changeCustomerMoiSangBo() {
        set_time_limit(72000); ini_set('memory_limit','1500M');
//        $sale_id = 863284;// Võ Tấn Huy
        $sale_id = 1207466;// Đặng Hoàng Tuấn
        $from = time();
        $criteria = new CDbCriteria();
        $criteria->compare("t.role_id", ROLE_CUSTOMER);
        $criteria->compare('t.is_maintain', STORE_CARD_KH_MOI);
        $criteria->addCondition("t.sale_id=$sale_id");
        $models = Users::model()->findAll($criteria);
//        echo '<pre>';
//        print_r(count($models));
//        echo '</pre>';
//        die;
        
        foreach($models as $mUser){
            $mUser->is_maintain     = STORE_CARD_KH_BINH_BO;// Khách Hàng Bình Bò
//            $mUser->sale_id         = 49184;// Khách Hàng Công Ty - Bò
            $mUser->update(['sale_id', 'is_maintain']);
            $mUser->solrAdd();
            CronUpdate::updateTypeCustomerReportDebitCash($mUser->id);
        }
        
        $to = time();
        $second = $to-$from;
        echo count($models).' done in: '.($second).'  Second  <=> '.($second/60).' Minutes';die;
    }
    
    /** @Author: ANH DUNG Jun 27, 2018
     *  @Todo: update một số user đc tạo trong 1 ngày nhung khong dc dong bo sang Solr
     **/
    public static function updateUserSorl() {
        set_time_limit(72000); ini_set('memory_limit','1500M');
        $criteria = new CDbCriteria();
        $criteria->addCondition("DATE(t.created_date)='2018-05-29'");
        $models = Users::model()->findAll($criteria);
//        $count = Users::model()->count($criteria);
//        echo '<pre>';
////        print_r(count($models));
//        print_r($count);
//        echo '</pre>';
//        die;
        foreach($models as $mUser){
            $mUser->solrAdd();
        }
        die(count($models). ' -- ok 1');
    }
    
    
    /** @Author: ANH DUNG Aug 24, 2018
     *  @Todo: fix update date_order  at ReferralTracking
     **/
    public function fixDateOrderReferralTracking() {
        $count = 0;
        $criteria = new CDbCriteria();
        $criteria->addCondition("status=1 AND date_order is null");
        $models = ReferralTracking::model()->findAll($criteria);
        foreach($models as $item){
            $criteria = new CDbCriteria();
            $criteria->addCondition('t.customer_id=' . $item->invited_id);
            $criteria->addCondition('t.status=' . Sell::STATUS_PAID);
            $criteria->addCondition('t.source=' . Sell::SOURCE_APP);
            $criteria->limit = 1;
            $criteria->order = 't.id ASC';
            $mSell = Sell::model()->find($criteria);
            if($mSell){
                $item->date_order = $mSell->created_date_only;
                $item->update(['date_order']);
                $count++;
            }
        }
        echo '<pre>';
        print_r($count);
        echo '</pre>';
        die;
    }
    
    
    /** @Author: ANH DUNG Oct 11, 2018
     *  @Todo: fix chuyển đầu số 11 -> 10 số
     * 1. define các đầu số chuyển
     * 2. run check 
     * 3. xử lý riêng table user với các table khác 
     * 4. sau khi scan het loai KH bo moi + HGD thi scan dk sau: 6185 total
     * SELECT * FROM `gas_users` WHERE role_id =4 AND TYPE IN ( 1, 2 )

     * 
     * 
     * http://vietnamnet.vn/vn/cong-nghe/vien-thong/lich-chuyen-doi-sim-11-thanh-10-so-cua-viettel-mobifone-vinaphone-475945.html
    1. sử dụng biến login_attemp để scan 
    - table Users: update phone, address_vi
    - run KH bò mối riêng, sau đến KH loại KH hộ GĐ 
    - run change username của KH app Gas24h, kết hợp với change đấu số của `gas_app_login_lock` để báo KH đổi đầu số khi login
    - table UsersPhoneExt2, gas_users_phone_log, `gas_app_login_lock` 
    2. chú ý khi sửa username của KH app Gas24h - ghi ra các phần bị ảnh hưởng: login phải alert ntn?
    3. Đồng bộ server solr khoảng 4h sáng 
     * UPDATE  `gas`.`gas_users` SET  `login_attemp` =  '0'
     * 
     **/
    public static function changePhone11To10() {
        $from = time();
        ini_set('memory_limit','2000M');
        $aPrefix = UpdateSql1::getArrayPrefixChange();
        $limit = 2000; $aPhoneChange = []; $totalChange = $totalRun = 0;
        $aIdTest = [1211579, 1211578];
        $criteria = new CDbCriteria();
        // 1. xử lý với NV nội bộ -- done 2320 total,
//        $criteria->addCondition("t.role_id<>".ROLE_CUSTOMER);
//        $criteria->addInCondition("t.id", $aIdTest);

        // 2. xử ly với KH bò mối, hộ GĐ 
//        $criteria->compare('t.role_id', ROLE_CUSTOMER);
//        $aTypeCustomer = CmsFormatter::$aTypeIdMakeUsername;
        $aTypeCustomer = CmsFormatter::$aTypeIdHgd;
//        $criteria->addInCondition( 't.is_maintain', $aTypeCustomer); // Aug 19, 2014 chỉ update KH bò mối thôi, vì cái 
        $criteria->addCondition("t.login_attemp=0 OR login_attemp IS NULL ");
        $criteria->limit = $limit;
        $models = Users::model()->findAll($criteria);
        foreach($models as $mCustomer){
//            if(empty($mCustomer->phone)){
//                continue;
//            } phải run scan 100% các record để biết chỉ quét 1 lần
            $aPhone = explode('-', $mCustomer->phone);
            $aPhoneNew = []; $needUpdate = false;
            foreach($aPhone as $phone){
                if($phone < 9000000){
                    $needUpdate = true;
                    continue;
                }
                // check 4 số đầu của phone
                $prefix_code = substr($phone, 0, 4);
                if(isset($aPrefix[$prefix_code])){
                    $charRemove = strlen($prefix_code);
                    $newPhone = $aPrefix[$prefix_code].substr($phone, $charRemove);
                    
                    $aPhoneChange[] = " Customer $mCustomer->id: ".$phone.' -- '.$newPhone;
                    $aPhoneNew[$newPhone]    = $newPhone;
                    $needUpdate = true;
                }else{
                    $aPhoneNew[$phone]  = $phone;
                }
            }
            
            $mCustomer->login_attemp = 1;
            $mCustomer->phone = implode('-', $aPhoneNew);
            $mCustomer->update();
            $totalRun++;
            if($needUpdate){
                $mCustomer->solrAdd();
                UsersPhone::savePhone($mCustomer);
                $totalChange++ ;
            }
        }
           
        if($totalChange < 1){
            return ;
        }
        $to = time();
        $second = $to-$from;
        $info = $totalChange." - Run $totalRun Customer Change done in:+ ".($second).'  Second  <=> '.($second/60).' Minutes '.date('Y-m-d H:i:s');
//        $info .= json_encode($aPhoneChange);
        Logger::WriteLog($info);
    }
    
    /* 1. Change user name + username_open trong applogin log
     * 2. chan khong cho login bang username 016
     * 
     */
    public static function changeUsernameKhApp() {
        $from = time();
        ini_set('memory_limit','3000M');
        $aPrefix = UpdateSql1::getArrayPrefixChange();
        $limit = 5000; $aPhoneChange = []; $totalChange = $totalRun = 0;
        $aIdTest = [1211579, 1211578];
        $criteria = new CDbCriteria();
        // 2. xử ly với KH bò mối, hộ GĐ 
        $criteria->compare('t.role_id', ROLE_CUSTOMER);
        $criteria->compare( 't.is_maintain', UsersExtend::STORE_CARD_HGD_APP); // Aug 19, 2014 chỉ update KH bò mối thôi, vì cái 
        $criteria->addCondition("DATE(t.created_date) > '2018-10-09'");
        $criteria->limit = $limit;
        $models = Users::model()->findAll($criteria);
        foreach($models as $mCustomer){
            $needUpdate = false;
            $prefix_code = substr($mCustomer->username, 0, 4);
            if(isset($aPrefix[$prefix_code])){
                $charRemove = strlen($prefix_code);
                $newUsername = $aPrefix[$prefix_code].substr($mCustomer->username, $charRemove);

                $aPhoneChange[] = " Customer $mCustomer->id: ".$mCustomer->username.' -- '.$newUsername;
                $mCustomer->username    = $newUsername;
                $totalChange++ ;
                $needUpdate = true;
            }
//            $mCustomer->login_attemp = 2;
            if($needUpdate){
                $mCustomer->solrAdd();
                $mCustomer->update();
            }
            $totalRun++;
        }
        if($totalChange < 1){
            echo 'totalChange <  1 -- ';
            return ;
        }
        $to = time();
        $second = $to-$from;
        $info = $totalChange." - Run $totalRun changeUsernameKhApp Change done in:+ ".($second).'  Second  <=> '.($second/60).' Minutes '.date('Y-m-d H:i:s');
//        $info .= json_encode($aPhoneChange);
        Logger::WriteLog($info);
    }
    
    /* 1. Change user name + username_open trong applogin log
     * 2. chan khong cho login bang username 016
     */
    public static function changeUsernameOpenApp() { 
        $from = time();
        ini_set('memory_limit','3000M');
        $aPrefix = UpdateSql1::getArrayPrefixChange();
        $limit = 5000; $aPhoneChange = []; $totalChange = $totalRun = 0;
        $aIdTest = [1211579, 1211578];
        $criteria = new CDbCriteria();
        // 2. xử ly với KH bò mối, hộ GĐ 
//        $criteria->addCondition("t.flag_fix_update=0");
        $criteria->addCondition("DATE( last_update ) > '2018-10-09'");
        
        $criteria->limit = $limit;
        $models = AppLoginLock::model()->findAll($criteria);
        foreach($models as $mCustomer){
            $fieldUpdate = $mCustomer->username_open;
            $prefix_code = substr($fieldUpdate, 0, 4);
            if(isset($aPrefix[$prefix_code])){
                $charRemove = strlen($prefix_code);
                $newUsername = $aPrefix[$prefix_code].substr($fieldUpdate, $charRemove);

                $aPhoneChange[] = " Customer $mCustomer->id: ".$fieldUpdate.' -- '.$newUsername;
                $mCustomer->username_open    = $newUsername;
                $totalChange++ ;
            }
            
//            $mCustomer->flag_fix_update = 1;
//            $mCustomer->update();
            $totalRun++;
        }
        if($totalChange < 1){
            return ;
        }
        $to = time();
        $second = $to-$from;
        $info = $totalChange." - Run $totalRun changeUsernameKhApp Change done in:+ ".($second).'  Second  <=> '.($second/60).' Minutes '.date('Y-m-d H:i:s');
//        $info .= json_encode($aPhoneChange);
        Logger::WriteLog($info);
    }
    
    
    /* 1. Change phone ở 2 table gas_users_phone_2, gas_users_phone_log
     * chi lay 3 so dau de check
     * Phone format: 169360996, 938346765, 1289908771
     */
    public static function changePhoneOtherTable() {
        $from = time();
        ini_set('memory_limit','3000M');
        $aPrefix = UpdateSql1::getArrayPrefixChangeNotZero();
        $limit = 5000; $aPhoneChange = []; $totalChange = $totalRun = 0;
        $criteria = new CDbCriteria();
        // 2. xử ly với KH bò mối, hộ GĐ 
        $criteria->addCondition("t.flag_fix_update=0");
        $criteria->limit = $limit;
//        $models = UsersPhoneExt2::model()->findAll($criteria);
//        $models = UsersPhoneLog::model()->findAll($criteria);
//        $models = CustomerDraft::model()->findAll($criteria);
        foreach($models as $mCustomer){
            $fieldUpdate = $mCustomer->phone;
            $prefix_code = substr($fieldUpdate, 0, 3);
            if(isset($aPrefix[$prefix_code])){
                $charRemove = strlen($prefix_code);
                $newUsername = $aPrefix[$prefix_code].substr($fieldUpdate, $charRemove);

                $aPhoneChange[] = " Customer $mCustomer->id: ".$fieldUpdate.' -- '.$newUsername;
                $mCustomer->phone    = $newUsername;
                $totalChange++ ;
            }
            
            $mCustomer->flag_fix_update = 1;
            $mCustomer->update();
            $totalRun++;
        }
        if($totalChange < 1){
            return ;
        }
        $to = time();
        $second = $to-$from;
        $info = $totalChange." - Run $totalRun changePhoneOtherTable Change done in:+ ".($second).'  Second  <=> '.($second/60).' Minutes '.date('Y-m-d H:i:s');
        $info .= json_encode($aPhoneChange);
        Logger::WriteLog($info);
    }
    
    
    /** @Author: ANH DUNG Oct 11, 2018
     *  @Todo: get các đầu số chuyển 
     **/
    public static function getArrayPrefixChange() {
        return [
            // Viettel
            '0169' => '039',
            '0168' => '038',
            '0167' => '037',
            '0166' => '036',
            '0165' => '035',
            '0164' => '034',
            '0163' => '033',
            '0162' => '032',
            
            // MobiFone
            '0120' => '070',
            '0121' => '079',
            '0122' => '077',
            '0126' => '076',
            '0128' => '078',
            
            // VinaPhone
            '0123' => '083',
            '0124' => '084',
            '0125' => '085',
            '0127' => '081',
            '0129' => '082',
            
            //Gmobile
            '0199' => '059',
            
             // Vietnamobile
            '0186' => '056',
            '0188' => '058',
        ];
    }
    
    /** @Author: ANH DUNG Oct 11, 2018
     *  @Todo: get các đầu số chuyển 
     **/
    public static function getArrayPrefixChangeNotZero() {
        return [
            // Viettel
            '169' => '39',
            '168' => '38',
            '167' => '37',
            '166' => '36',
            '165' => '35',
            '164' => '34',
            '163' => '33',
            '162' => '32',
            
            // MobiFone
            '120' => '70',
            '121' => '79',
            '122' => '77',
            '126' => '76',
            '128' => '78',
            
            // VinaPhone
            '123' => '83',
            '124' => '84',
            '125' => '85',
            '127' => '81',
            '129' => '82',
            
            //Gmobile
            '199' => '59',
            
             // Vietnamobile
            '186' => '56',
            '188' => '58',
        ];
    }
    
    
    /** @Author: ANH DUNG Oct 19, 2018
     *  @Todo: update phone sang table gas_users_phone
     * do phone không lưu sang table UsersPhone
     **/
    public static function fixPhoneNotUpdateToUserPhone() {
        $from = time();
        ini_set('memory_limit','3000M');
        $aPrefix = UpdateSql1::getArrayPrefixChange();
        $limit = 5000; $aPhoneChange = []; $totalChange = $totalRun = 0;
        $criteria = new CDbCriteria();
        // 2. xử ly với KH bò mối, hộ GĐ 
        $criteria->compare('t.role_id', ROLE_CUSTOMER);
        $criteria->addCondition("t.login_attemp=0 OR login_attemp IS NULL");
        $criteria->addInCondition( 't.is_maintain', CmsFormatter::$aTypeIdHgd); // Aug 19, 2014 chỉ update KH bò mối thôi, vì cái 
        $criteria->addCondition("t.phone <> '' AND t.id NOT IN (SELECT user_id FROM gas_users_phone b)");
        $criteria->limit = $limit;
        $models = Users::model()->findAll($criteria);
//        $count = Users::model()->count($criteria);
        foreach($models as $mCustomer){
            UsersPhone::savePhone($mCustomer);
            $totalChange++ ;
            $mCustomer->login_attemp = 1;
            $mCustomer->update();
        }
        
        $to = time();
        $second = $to-$from;
        $info = $totalChange." - Run $totalRun fixPhoneNotUpdateToUserPhone Change done in:+ ".($second).'  Second  <=> '.($second/60).' Minutes '.date('Y-m-d H:i:s');
//        $info .= json_encode($aPhoneChange);
        Logger::WriteLog($info);
        
    }
    
    /** @Author: ANH DUNG Now 01, 2018
     *  @Todo: check lại bình tính sai của telesale 
     **/
    public static function recheckBqvTelesale() {
//        $date_from_ymd  = '2018-10-01';
//        $date_to_ymd    = '2018-10-31';
        
        $date_from_ymd  = '2019-03-01';
        $date_to_ymd    = '2019-03-31';
        $mAppCache      = new AppCache();
        // 1. check BQV tính 2 lần 
        $aTelesale      = $mAppCache->getListdataUserByRole(ROLE_TELESALE);
//        $criteria = new CDbCriteria();
//        $criteria->addCondition("t.sale_id IN (". implode(',', array_keys($aTelesale)).")");
//        $criteria->addCondition('t.first_order='.Sell::IS_FIRST_ORDER);
//        $criteria->addCondition('t.status = '.Sell::STATUS_PAID);
//        DateHelper::searchBetween($date_from_ymd, $date_to_ymd, 'created_date_only_bigint', $criteria, false);
//        $criteria->order = 't.sale_id';
////        $criteria->limit = 1000; 
//        $index = 1;
//        $models = Sell::model()->findAll($criteria);
//        foreach($models as $mSell){
//            $date_to    = MyFormat::modifyDays($mSell->created_date_only, 1, '-', 'day');
//            $date_from  = MyFormat::modifyDays($mSell->created_date_only, 5, '-', 'month');
//
//            $criteria = new CDbCriteria();
//            DateHelper::searchBetween($date_from, $date_to, 'created_date_only_bigint', $criteria, false);
//            $criteria->addCondition("t.customer_id=$mSell->customer_id AND t.id<>$mSell->id" );
////            $criteria->addCondition("t.customer_id=$mSell->customer_id AND t.first_order=1 AND t.sale_id>0" );
//            $criteria->addCondition('t.status = '.Sell::STATUS_PAID);
//            $criteria->order = 't.id DESC';
//            
//            $aSellOrder = Sell::model()->findAll($criteria);
//            if(count($aSellOrder) > 0){
//                $mSellUpdate = $aSellOrder[0];
//                $telesaleName = isset($aTelesale[$mSellUpdate->sale_id]) ? $aTelesale[$mSellUpdate->sale_id] : '';
//                $telesaleName = MyFunctionCustom::remove_vietnamese_accents($telesaleName);
////                echo $index++ . ". Mã đơn trùng: ". $mSellUpdate->customer_id . ' id KH' .$mSellUpdate->customer_id. ' -- date = '.$mSellUpdate->created_date_only."  - $total <br>";
//                echo $index++ . ". $telesaleName - ". $mSellUpdate->code_no . ' Phone DH 0' .$mSellUpdate->phone. ' -- date = '.$mSellUpdate->created_date_only."  - $total <br>";
//                $mSellUpdate->first_order = 0;
////                $mSellUpdate->update(['first_order']);
//            }
//        }
//        echo '<pre>';
//        print_r(count($models));
//        echo '</pre>';
//        die;

        // 2. BQV của PTTT+ CCS ( Cũ là PVKH ) bị tính nhầm cho Telesale
        $mOneMany               = new GasOneMany();
        $aCcsTeam               = $mOneMany->getManyIdByTypeOrOneId(GasOneMany::TYPE_MONITOR_CCS);
        $mAppPromotion = new AppPromotion();
        $aGdkv = $mAppPromotion->getArrayPartnerFixCode();
        $criteria = new CDbCriteria();
//        $criteria->addCondition("t.owner_id_root IN (". implode(',', array_keys($aGdkv)).")");
        $criteria->addCondition("t.owner_id IN (". implode(',', array_keys($aCcsTeam)).")");
        $models = AppPromotion::model()->findAll($criteria);
        $aCodeGdkv  = CHtml::listData($models, 'id', 'id');
        $aIdPvkh    = CHtml::listData($models, 'id', 'owner_id');

        $criteria = new CDbCriteria();
        $criteria->addCondition("t.promotion_id IN (". implode(',', $aCodeGdkv).")");
        $criteria->addCondition('t.first_order='.Sell::IS_FIRST_ORDER);
        $criteria->addCondition('t.status = '.Sell::STATUS_PAID);
        DateHelper::searchBetween($date_from_ymd, $date_to_ymd, 'created_date_only_bigint', $criteria, false);
        $criteria->order = 't.sale_id';
        $index = 1;
        $models = Sell::model()->findAll($criteria);
        foreach($models as $mSell){
            $mSellUpdate = $mSell;
            if($mSell->sale_id == 0 || array_key_exists($mSell->sale_id, $aTelesale)){
                $telesaleName = isset($aTelesale[$mSellUpdate->sale_id]) ? $aTelesale[$mSellUpdate->sale_id] : '';
                $telesaleName = MyFunctionCustom::remove_vietnamese_accents($telesaleName);
//                echo $index++ . ". $mSell->id  - $mSell->promotion_id <br>";
                echo $index++ . ". $telesaleName - ". $mSellUpdate->code_no . ' Phone DH 0' .$mSellUpdate->phone. ' -- date = '.$mSellUpdate->created_date_only."  - $total <br>";
                $mSellUpdate->sale_id = isset($aIdPvkh[$mSell->promotion_id]) ? $aIdPvkh[$mSell->promotion_id] : $mSell->sale_id;
//                $mSellUpdate->update(['sale_id']);
                $criteria = new CDbCriteria();
                $criteria->compare( 'sell_id', $mSellUpdate->id);
                $aUpdate = array('sale_id' => $mSellUpdate->sale_id);
                SellDetail::model()->updateAll($aUpdate, $criteria);
            }
        }
        echo '<pre>';
        print_r(count($models));
        echo '</pre>';
        die;
    }
    
    /** @Author: DungNT Mar 08, 2019
     *  @Todo: tính lại BQV của PTTT mà tính cho telesale
     * @param: $dateRun Y-m-d
     **/
    public static function updateBugBqvCcsToTelesale($dateRun) {
        // function move to Model CronUpdate
    }
    
    /** @Author: DungNT Feb 1, 2019
     *  @Todo: fix BQV telesale KH nhập mã những ko update đc sale_id bên model User => bên Sell ko dc tính 
     **/
    public static function updateBugBqvTelesale() {
//        $date_from_ymd  = '2019-01-01';
//        $date_to_ymd    = '2019-01-31';
        $date_from_ymd  = '2019-02-01';
        $date_to_ymd    = '2019-02-28';
        
        // 1. check BQV tính 2 lần 
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.sale_id=0 AND t.promotion_type=" . AppPromotion::TYPE_FIRST_USER);
        $criteria->addCondition('t.first_order='.Sell::IS_FIRST_ORDER);
        $criteria->addCondition('t.status = '.Sell::STATUS_PAID);
        DateHelper::searchBetween($date_from_ymd, $date_to_ymd, 'created_date_only_bigint', $criteria, false);
        $models = Sell::model()->findAll($criteria);
        $count = 0;
        foreach($models as $mSell){
            $mAppPromotion = AppPromotion::model()->findByPk($mSell->promotion_id);
            $mOwnerPromotion = Users::model()->findByPk($mAppPromotion->owner_id);
            if($mOwnerPromotion->role_id == ROLE_TELESALE){
                $count++;
                echo $mSell->code_no . ' -- ';
                $mSell->sale_id = $mAppPromotion->owner_id;
                $mSell->update(['sale_id']);
                
                $criteria = new CDbCriteria();
                $criteria->compare( 'sell_id', $mSell->id);
                $aUpdate = array('sale_id' => $mSell->sale_id);
                SellDetail::model()->updateAll($aUpdate, $criteria);
            }
        }
        echo '<pre>';
        print_r($count);
        echo '</pre>';
        die;
    }
    
        
    /** @Author: ANH DUNG Now 02, 2018
     *  @Todo: update phạt ticket tháng 10
     **/
    public static function ticketFixFlowPunish() {
        echo '<pre>';
        print_r($_POST);
        echo '</pre>';
        die;
        $from = time();
//        $date_from_ymd  = '2018-10-01';// -- done Now0218
//        $date_to_ymd    = '2018-10-31';
        $date_from_ymd  = '2018-11-01';// done Now0218
        $date_to_ymd    = '2018-11-30';
        
        $criteria = new CDbCriteria();
        $criteria->with = ['rTicketError'];
        $criteria->addCondition('rTicketError.type='.MonitorUpdate::TYPE_FIX_TICKET);
        $criteria->addBetweenCondition('DATE(rTicketError.last_update)', $date_from_ymd, $date_to_ymd);
        $models = GasTickets::model()->findAll($criteria);
        foreach($models as $item){
            $item->doFlowPunish();
        }
        $to = time();
        $second = $to-$from;
        $info = count($models)." - Run  ticketFixFlowPunish Change done in:+ ".($second).'  Second  <=> '.($second/60).' Minutes '.date('Y-m-d H:i:s');
        echo $info;
        Logger::WriteLog($info);
        die();
    }
    
    /** @Author: ANH DUNG Now 25, 2018
     *  @Todo: update phạt lỗi chưa nộp ngân hàng tháng 10 + T11
     **/
    public static function errorBankTransferFlowPunish() {
        $from = time();
//        $date_from_ymd  = '2018-10-01';// -- done Now0218
//        $date_to_ymd    = '2018-10-31';
        $date_from_ymd  = '2018-11-01';// done Now0218
        $date_to_ymd    = '2018-11-30';
        
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.type='.MonitorUpdate::TYPE_ERROR_BANK_TRANSFER_16H);
        $criteria->addBetweenCondition('DATE(last_update)', $date_from_ymd, $date_to_ymd);
        $models = MonitorUpdate::model()->findAll($criteria);
        foreach($models as $item){
            $item->saveErrorsBankTransferMoveToSupport();
        }
        $to = time();
        $second = $to-$from;
        $info = count($models)." - Run  ticketFixFlowPunish Change done in:+ ".($second).'  Second  <=> '.($second/60).' Minutes '.date('Y-m-d H:i:s');
        echo $info;
        Logger::WriteLog($info);
        die();
    }
    
    /** @Author: ANH DUNG Now 17, 2018
     *  @Todo: update sale_id pvkh với những table Sell, update back sale_id to model customer
     * 1. A = findAll promotion_id ứng với mỗi mã của GĐKV
     * 2. foreach A rồi find all promotion_id bên sell với sale_id = 0
     * 3. Update sale_id = owner_id cho table Sell
     * 4. Update sale_id = owner_id cho Customer đó
     **/
    public static function fixSaleIdOfSell() {
        $mAppPromotion = new AppPromotion();
        foreach($mAppPromotion->getArrayPartnerGdkv() as $owner_id):
            $aSubOwner      = $mAppPromotion->getByOwnerIdRoot($owner_id);
            $aIdPromotion   = CHtml::listData($aSubOwner, 'id', 'code_no');
//            $aIdPvkh    = CHtml::listData($aSubOwner, 'owner_id', 'code_no');
            foreach($aSubOwner as $mPromotion):
                $criteria = new CDbCriteria();
                $criteria->compare("t.promotion_id", $mPromotion->id);
//                $criteria->addCondition("t.sale_id = 0");// 1. for fix sale_id=0
                $criteria->addCondition("t.sale_id > 0 AND t.sale_id <> ".$mPromotion->owner_id);// for fix telesale ghi đè của PVKH
                $aSellFix = Sell::model()->findAll($criteria);
                if(count($aSellFix) < 1){
                    continue ;
                }
                echo " <br> $mPromotion->code_no --- ". count($aSellFix). ' need fix ';
                $totalUpdate = 0;
                foreach($aSellFix as $mSell):
//                    $mSell->sale_id = $mPromotion->owner_id;
//                    $mSell->update(['sale_id']);
//                    $mCustomer = $mSell->rCustomer;
//                    if($mCustomer){
//                        $mCustomer->sale_id = $mPromotion->owner_id;
//                        $mCustomer->update(['sale_id']);
//                        $mCustomer->solrAdd();
//                        $totalUpdate++;
//                    }
                endforeach;
                echo " +++ totalUpdate = $totalUpdate";
            endforeach;
        endforeach;
        
        
        die('ok1');
    }
    
    
    /** @Author: ANH DUNG Now 23, 2018
     *  @Todo: run sql fix lại code_account cho NV công ty
     *  CDbCommand::execute() failed: SQLSTATE[HY000]: General error: 5 database is locked. The SQL statement executed was: DELETE FROM 'YiiSession' WHERE expire<:expire. in /var/www/clients/client1/web2/web/protected/components/GasCheck.php (667) in /var/www/clients/client1/web2/web/protected/modules/admin/AdminModule.php (14) in /var/www/clients/client1/web2/web/index.php (26)
     **/
    public static function fixUserCodeAccount() {
        $from = time();
        $mRoles = new Roles();
        $aRoleLock = array_merge(Roles::getRestrict(), $mRoles->getRoleNotSendEmail());
        $criteria = new CDbCriteria();
        $criteria->addNotInCondition("t.role_id", $aRoleLock);
//        $criteria->addNotInCondition("t.role_id", [ROLE_CUSTOMER, ROLE_AGENT]);
        $criteria->addCondition("t.code_account=''");
//        $criteria->addCondition("t.code_account<>''");
        $criteria->limit = 100;
        $models = Users::model()->findAll($criteria);
        echo '<pre>';
        print_r(count($models));
        echo '</pre>';
        foreach($models as $mUser){
//            $mUser->code_account = '';
            $mUser->code_account    = MyFunctionCustom::getNextIdForEmployee('Users', 'HM', 6);
            $mUser->update(['code_account']);
            echo $mUser->code_account.' -- ';
        }
        $to = time();
        $second = $to-$from;
        $info = count($models)." - Run  fixUserCodeAccount Change done in:+ ".($second).'  Second  <=> '.($second/60).' Minutes '.date('Y-m-d H:i:s');
        echo $info;
        Logger::WriteLog($info);
        die('ok 11');
        
    }
    
    /** @Author: ANH DUNG Dec 01, 2018
     *  @Todo: fix score call review
     **/
    public static function fixScoreCallReview() {
        $index = 1;
        $criteria = new CDbCriteria();
        $criteria->compare("status_check_call", CallReview::TESTED);
        $criteria->addCondition("score>0");
        $dateFrom   = '2018-11-01';
        $dateTo     = '2018-11-30';
//        $criteria->addCondition("created_date_bigint > $dateCheck");
        DateHelper::searchBetween($dateFrom, $dateTo, 'created_date_bigint', $criteria, true);
//        $criteria->limit = 10;
        $models = Call::model()->findAll($criteria);
        echo '<pre>';
        print_r(count($models));
        echo '</pre>';
//        die;
        foreach($models as $mCall){
            $mCallReview = new CallReview();
            $mCall->score                   = $mCallReview->calculateScoreByCallId($mCall->id);
//            echo ($index++). " - caller_number = $mCall->caller_number -- New Score $mCall->score <br>";
            $mCall->update();
        }
        die;
    }
    
    /** @Author: ANH DUNG Dec 27, 2018
     *  @Todo: fix auto approve leave old
     **/
    public static function autoApproveLeave() {
        $dateFrom   = '2016-01-01';
        $dateTo     = '2018-12-25';
        
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition("DATE(t.created_date)", $dateFrom, $dateTo);
        $criteria->compare('t.status', GasLeave::STA_NEW);
        $models = GasLeave::model()->findAll($criteria);
        echo '<pre>';
        print_r(count($models));
        echo '</pre>';
//        die;
        foreach($models as $mLeave){
            $mLeave->scenario = 'update_status';
            $mLeave->status = GasLeave::STA_APPROVED_BY_MANAGE;
            $mLeave->manage_approved_uid = GasConst::UID_ADMIN;
            $mLeave->manage_approved_date = date("Y-m-d H:i:s");
            $mLeave->manage_approved_status = $mLeave->status;
            $mLeave->update();
        }
        
        die('ok1');
    }
    
    /** @Author: ANH DUNG Dec 27, 2018
     *  @Todo: fix auto approve leave old
     **/
    public static function autoAddLeaveDetail() {
        $dateFrom   = '2018-10-01';
        $dateTo     = '2018-12-30';
        
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition("DATE(t.created_date)", $dateFrom, $dateTo);
        $criteria->addInCondition('t.status', [GasLeave::STA_APPROVED_BY_DIRECTOR, GasLeave::STA_APPROVED_BY_MANAGE]);
        $models = GasLeave::model()->findAll($criteria);
        echo '<pre>';
        print_r(count($models));
        echo '</pre>';
//        die;
        foreach($models as $mLeave){
            $mLeave->saveDetail();
        }
        
        die('ok1');
    }
    
    /** @Author: DungNT May 04, 2019
     *  @Todo: fix delete record ReferralTracking của mã KM hết hạn
     **/
    public static function fixDeleteRecordReferralTracking() {
        $from = time();
        $date_from  = '2019-01-01';
        $date_to    = '2019-05-30';
        $criteria = new CDbCriteria();
        $criteria->compare("t.status_use", AppPromotionUser::STATUS_EXPIRED);
        $criteria->compare('t.promotion_type', AppPromotion::TYPE_FIRST_USER);
        DateHelper::searchBetween($date_from, $date_to, 'created_date_bigint', $criteria, true);
        $models = AppPromotionUser::model()->findAll($criteria);
        foreach($models as $item){
            $item->removeExpiredRecordReferral();
        }
        $to = time();
        $second = $to-$from;
        $info = count($models)." - Run  fixDeleteRecordReferralTracking done in:+ ".($second).'  Second  <=> '.($second/60).' Minutes '.date('Y-m-d H:i:s');
        Logger::WriteLog($info);
        echo $info;die;
    }
    
    /** @Author: DungNT Jun 02, 2019
     *  @Todo: xóa 1 số phone trong table gas_users_phone_2
     * do thêm bị trùng hoặc do ái đó thêm vào 
     **/
    public static function fixRemovePhoneExt2() {
        die('need review phone remove');
        $from = time();
        $aPhoneRemove = ['0765147686'];// Jun219 Fix Trâm Sale Admin Cũ
//        $aPhoneRemove = ['0384331552'];
        $aTypePhone = [UsersPhoneExt2::TYPE_PRICE_BO_MOI, UsersPhoneExt2::TYPE_BAOTRI_BO_MOI, UsersPhoneExt2::TYPE_PAY_DEBIT];
//        $aTypePhone = [UsersPhoneExt2::TYPE_PRICE_BO_MOI];
        $criteria = new CDbCriteria();
        $criteria->addInCondition("t.phone", $aPhoneRemove);
        $criteria->addInCondition("t.type", $aTypePhone);
        $models = UsersPhoneExt2::model()->findAll($criteria);
        $aIdCustomer = CHtml::listData($models, 'user_id', 'user_id');
        
        $sParamsIn = implode(',',  $aIdCustomer);
        $criteria = new CDbCriteria();
        $criteria->addCondition("t.id IN ($sParamsIn)");
        $aCustomer = Users::model()->findAll($criteria);
        foreach($aCustomer as $mCustomer){
            $mCustomer->loadExtPhone2();
            
            // 1. Loại Báo giá 
            $newPhone = [];
            $atemp = explode('-', $mCustomer->phone_ext2);
            foreach ($atemp as $phone){
                if(!in_array($phone, $aPhoneRemove)){
                    $newPhone[] = $phone;
                }
            }
            $newPhone = implode('-', $newPhone);
            $mCustomer->phone_ext2 = $newPhone;
//            echo "Báo giá id = $mCustomer->id <pre>";
//            print_r("Old: $mCustomer->phone_ext2 <br>");
//            print_r("New: $newPhone <br>");
//            echo '</pre>';
            
            // 2. Loại Bảo trì
            $newPhone = [];
            $atemp = explode('-', $mCustomer->phone_ext3);
            foreach ($atemp as $phone){
                if(!in_array($phone, $aPhoneRemove)){
                    $newPhone[] = $phone;
                }
            }
            $newPhone = implode('-', $newPhone);
            $mCustomer->phone_ext3 = $newPhone;
//            echo 'Bảo trì <pre>';
//            print_r("Old: $mCustomer->phone_ext3 <br>");
//            print_r("New: $newPhone <br>");
//            echo '</pre>';

            // 3. Loại công nợ 
            $newPhone = [];
            $atemp = explode('-', $mCustomer->phone_ext4);
            foreach ($atemp as $phone){
                if(!in_array($phone, $aPhoneRemove)){
                    $newPhone[] = $phone;
                }
            }
            $newPhone = implode('-', $newPhone);
            $mCustomer->phone_ext4 = $newPhone;
//            echo 'công nợ <pre>';
//            print_r("Old: $mCustomer->phone_ext4 <br>");
//            print_r("New: $newPhone <br>");
//            echo '</pre>';
            $mCustomer->saveExtPhone2();
        }
        
        $to = time();
        $second = $to-$from;
        $info = count($aCustomer)." - Run  fixRemovePhoneExt2 done in:+ ".($second).'  Second  <=> '.($second/60).' Minutes '.date('Y-m-d H:i:s');
        Logger::WriteLog($info);
        echo $info;die;
    }
    
    /** @Author: DungNT Aug 28, 2019
     *  @Todo: change url in table cms from http://spj.daukhimiennam.com -> http://noibo.daukhimiennam.com
     **/
    public static function changeUrlCms() {
        $models = Cms::model()->findAll();
        foreach($models as $mCms){
            $mCms->cms_content = str_replace('http://spj.', 'http://noibo.', $mCms->cms_content);
            $mCms->update(['cms_content']);
        }
        
    }
    
}
?>
