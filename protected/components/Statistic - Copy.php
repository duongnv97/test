<?php
class StatisticCopy1
{
    public static function Statistic_maintain(){
    try{
        // 1. get arr id agent
        // 2. user relation (agentCustomerCount) of Model User to count customer of agent limit by created date
        // 3. query table maintain each agent count  DA BAO TRI	CUOC GOI TOT	CUOC GOI XAU limit by created date
        // 4. query table maintain SELL  each agent count  BAN HANG SAU BT	CUOC GOI TOT	CUOC GOI XAU limit by created date
        // one query then group by agent id. hi hi

        $aIdAgent = Users::getArrIdUserByRole(); // default is role agent
        $date_from = '';
        $date_to = '';
        $aResSta = array();
        if(!empty($_POST['GasMaintain']['date_from']))
                $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($_POST['GasMaintain']['date_from']);
        if(!empty($_POST['GasMaintain']['date_to']))
                $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($_POST['GasMaintain']['date_to']);
        $aResSta['DATE_FROM'] = $_POST['GasMaintain']['date_from'];
        $aResSta['DATE_TO'] = $_POST['GasMaintain']['date_to'];
        // 1 get count customer of agent
        $aResSta['AGENT']['AgentCustomer'] = Statistic::getAgentCustomerCount($aIdAgent, $date_from, $date_to);
        $aResSta['AGENT']['CountIsMaintain'] = Statistic::getAgentMaintainCount('GasMaintain', $date_from, $date_to);
        $aResSta['AGENT']['CountIsMaintainCallGood'] = Statistic::getAgentMaintainCount('GasMaintain', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_GOOD);
        $aResSta['AGENT']['CountIsMaintainCallBad'] = Statistic::getAgentMaintainCount('GasMaintain', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_BAD);

        $aResSta['AGENT']['CountSellMaintain'] = Statistic::getAgentMaintainCount('GasMaintainSell', $date_from, $date_to);
        $aResSta['AGENT']['CountSellMaintainCallGood'] = Statistic::getAgentMaintainCount('GasMaintainSell', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_GOOD);
        $aResSta['AGENT']['CountSellMaintainCallBad'] = Statistic::getAgentMaintainCount('GasMaintainSell', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_BAD);

        // 2. get count by NV PHUC VU KHACH HANG
        $aResSta['MAINTAIN_EMPLOYEE']['CountIsMaintain'] = Statistic::getMaintainCountOfEmployee('GasMaintain', $date_from, $date_to);
        $aResSta['MAINTAIN_EMPLOYEE']['CountIsMaintainCallGood'] = Statistic::getMaintainCountOfEmployee('GasMaintain', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_GOOD);
        $aResSta['MAINTAIN_EMPLOYEE']['CountIsMaintainCallBad'] = Statistic::getMaintainCountOfEmployee('GasMaintain', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_BAD);
        $aResSta['MAINTAIN_EMPLOYEE']['CountSellMaintain'] = Statistic::getMaintainCountOfEmployee('GasMaintainSell', $date_from, $date_to);
        $aResSta['MAINTAIN_EMPLOYEE']['CountSellMaintainCallGood'] = Statistic::getMaintainCountOfEmployee('GasMaintainSell', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_GOOD);
        $aResSta['MAINTAIN_EMPLOYEE']['CountSellMaintainCallBad'] = Statistic::getMaintainCountOfEmployee('GasMaintainSell', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_BAD);


        //public static function getMaintainCountOfEmployee($name_model, $date_from, $date_to, $status='', $field_select='maintain_employee_id', $type=TYPE_MAINTAIN)
        // 3. get count by NV KE TOAN BAN HANG
        $aResSta['MAINTAIN_EMPLOYEE_ACCOUNTING']['CountIsMaintain'] = Statistic::getMaintainCountOfEmployee('GasMaintain', $date_from, $date_to, '', 'accounting_employee_id');
        $aResSta['MAINTAIN_EMPLOYEE_ACCOUNTING']['CountIsMaintainCallGood'] = Statistic::getMaintainCountOfEmployee('GasMaintain', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_GOOD, 'accounting_employee_id');
        $aResSta['MAINTAIN_EMPLOYEE_ACCOUNTING']['CountIsMaintainCallBad'] = Statistic::getMaintainCountOfEmployee('GasMaintain', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_BAD, 'accounting_employee_id');
        $aResSta['MAINTAIN_EMPLOYEE_ACCOUNTING']['CountSellMaintain'] = Statistic::getMaintainCountOfEmployee('GasMaintainSell', $date_from, $date_to, '', 'accounting_employee_id');
        $aResSta['MAINTAIN_EMPLOYEE_ACCOUNTING']['CountSellMaintainCallGood'] = Statistic::getMaintainCountOfEmployee('GasMaintainSell', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_GOOD, 'accounting_employee_id');
        $aResSta['MAINTAIN_EMPLOYEE_ACCOUNTING']['CountSellMaintainCallBad'] = Statistic::getMaintainCountOfEmployee('GasMaintainSell', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_BAD, 'accounting_employee_id');

        $_SESSION['data-excel'] = $aResSta;
        return $aResSta;
        
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }              
  }

    public static function Statistic_market_development(){
    try{
            // 1. get arr id agent
            // 2. user relation (agentCustomerCount) of Model User to count customer of agent limit by created date
            // 3. query table maintain each agent count  DA BAO TRI	CUOC GOI TOT	CUOC GOI XAU limit by created date
            // 4. query table maintain SELL  each agent count  BAN HANG SAU BT	CUOC GOI TOT	CUOC GOI XAU limit by created date
            // one query then group by agent id. hi hi

            $aIdAgent = Users::getArrIdUserByRole(); // default is role agent
            $date_from = '';
            $date_to = '';
            $aResSta = array();
            if(!empty($_POST['GasMaintain']['date_from']))
                    $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($_POST['GasMaintain']['date_from']);
            if(!empty($_POST['GasMaintain']['date_to']))
                    $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($_POST['GasMaintain']['date_to']);
            $aResSta['DATE_FROM'] = $_POST['GasMaintain']['date_from'];
            $aResSta['DATE_TO'] = $_POST['GasMaintain']['date_to'];
            // 2. get count by NV PHUC VU KHACH HANG
            $aResSta['MAINTAIN_EMPLOYEE']['CountIsMaintain'] = Statistic::getMaintainCountOfEmployee('GasMaintain', $date_from, $date_to, '', 'maintain_employee_id', TYPE_MARKET_DEVELOPMENT);
            $aResSta['MAINTAIN_EMPLOYEE']['CountIsMaintainCallGood'] = Statistic::getMaintainCountOfEmployee('GasMaintain', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_GOOD, 'maintain_employee_id', TYPE_MARKET_DEVELOPMENT);
            $aResSta['MAINTAIN_EMPLOYEE']['CountIsMaintainCallBad'] = Statistic::getMaintainCountOfEmployee('GasMaintain', $date_from, $date_to, CmsFormatter::$STATUS_MAINTAIN_BAD, 'maintain_employee_id', TYPE_MARKET_DEVELOPMENT);
            $aResSta['AGENT'] = array();
            $aResSta['MAINTAIN_EMPLOYEE_ACCOUNTING'] = array();
            $_SESSION['data-excel'] = $aResSta;
            return $aResSta;
            
    }catch (Exception $e)
    {
        MyFormat::catchAllException($e);
    }                 
  }

      public static function getAgentCustomerCount($aIdAgent, $date_from, $date_to, $type=CUSTOMER_TYPE_MAINTAIN){
            $aRes = array();
            $total = 0;

            foreach($aIdAgent as $agent_id){
                    $list_customer_id = GasMaintain::getCustomerOfAgent($agent_id);
                    $criteria = new CDBcriteria();			
                    $criteria->addInCondition('t.id', $list_customer_id);
                    if(!empty($date_from) && empty($date_to))
                            $criteria->addCondition("t.created_date>='$date_from'");
                    if(empty($date_from) && !empty($date_to))
                            $criteria->addCondition("t.created_date<='$date_to'");
                    if(!empty($date_from) && !empty($date_to))
                            $criteria->addBetweenCondition("t.created_date",$date_from,$date_to);	
                    $criteria->compare('t.type', $type);
                    if(isset($_POST['GasMaintain']['province_id']))
                        $criteria->compare('t.province_id', $_POST['GasMaintain']['province_id']);
                    $aRes[$agent_id] =  Users::model()->count($criteria);			
                    $total += $aRes[$agent_id];
            }
            if($_POST['GasMaintain']['sort_by']=='asc'){ // tăng dần asort low to high							
                    asort($aRes);						
            }else{ // high to low
                    arsort($aRes);
            }
            //echo '<pre>'; print_r($aRes);echo '</pre>'; die;
            $aRes['total_count'] = $total;
            return $aRes;	

            /* $criteria = new CDBcriteria();
            $criteria->addInCondition('t.id', $aIdAgent);
            $criteria->with = array('agentCustomerCount');
            $criteria->together = true;
            if(!empty($date_from) && empty($date_to))
                    $criteria->addCondition("agentCustomerCount.created_date>=$date_from");
            if(empty($date_from) && !empty($date_to))
                    $criteria->addCondition("agentCustomerCount.created_date<=$date_to");
            if(!empty($date_from) && !empty($date_to))
                    $criteria->addBetweenCondition('agentCustomerCount.created_date',$date_from,$date_to);
            $mAgentCustomer = Users::model()->findAll($criteria);
            $aRes = array();
            if(count($mAgentCustomer)>0)
            foreach($mAgentCustomer as $item){
                    $aRes[$item->id] = $item->agentCustomerCount;
            }
            return $aRes; */
      }
	  	  
    /**
     * @Author: ANH DUNG 11-11-2013
     * @Todo: thống kê đếm số bảo trì OR PTTT 
     * theo tỉnh, đại lý,  khoảng thời gian vs trạng thái
     * @Param: model $name_model is model GasMaintain or GasMaintainsell
     * @Param: date $date_from is date from: 2013-11-15
     * @Param: date $date_to is date to: 2013-12-15
     * @Param: array $status is array status
     * @Param: string $field_select is agent_id
     * @Param: number $type is 1: bảo trì, 2: PTTT
     * @Return: array[agent_id]=>123
     */     
    public static function getAgentMaintainCount($name_model, $date_from, $date_to, $status='', $field_select='agent_id', $type=TYPE_MAINTAIN){
        $criteria=new CDbCriteria;
        $total = 0;
        $date_select = 'maintain_date';
        if($name_model=='GasMaintainSell')
                $date_select = 'date_sell';

        if(!empty($date_from) && empty($date_to))
                $criteria->addCondition("t.$date_select>='$date_from'");
        if(empty($date_from) && !empty($date_to))
                $criteria->addCondition("t.$date_select<='$date_to'");
        if(!empty($date_from) && !empty($date_to))
                $criteria->addBetweenCondition("t.$date_select",$date_from,$date_to); 	
        if(is_array($status))
                $criteria->addInCondition('t.status', $status);
        $criteria->compare('t.type', $type);
        if(isset($_POST['GasMaintain']['province_id']))
            $criteria->compare('t.province_id', $_POST['GasMaintain']['province_id']);

        $criteria->select = "$field_select, count(id) as count_row";
        $criteria->group = "$field_select";		
        $model_ = call_user_func(array($name_model, 'model'));
        $mRes = $model_->findAll($criteria);
        $aRes = array();
        if(count($mRes)>0)
        foreach($mRes as $item){
                $aRes[$item->$field_select] = $item->count_row;
                $total += $aRes[$item->$field_select];
        }
        $aRes['total_count'] = $total;
        return $aRes;			
    }
          
    /**
     * @Author: ANH DUNG 11-19-2013
     * @Todo: thống kê đếm số bảo trì OR PTTT theo nhân viên bảo trì hay kế toán đại lý,
     * theo tỉnh, khoảng thời gian vs trạng thái
     * @Param: model $name_model is model GasMaintain or GasMaintainsell
     * @Param: date $date_from is date from: 2013-11-15
     * @Param: date $date_to is date to: 2013-12-15
     * @Param: array $status is array status
     * @Param: string $field_select is maintain_employee_id OR accounting_employee_id
     * @Param: number $type is 1: bảo trì, 2: PTTT
     * @Return: array[agent_id][maintain_employee_id OR accounting_employee_id]=>123
     */     
    public static function getMaintainCountOfEmployee($name_model, $date_from, $date_to, $status='', $field_select='maintain_employee_id', $type=TYPE_MAINTAIN){
        $criteria=new CDbCriteria;
        $total = 0;
              $date_select = 'maintain_date';
              if($name_model=='GasMaintainSell')
                      $date_select = 'date_sell';

        if(!empty($date_from) && empty($date_to))
                $criteria->addCondition("t.$date_select>='$date_from'");
        if(empty($date_from) && !empty($date_to))
                $criteria->addCondition("t.$date_select<='$date_to'");
        if(!empty($date_from) && !empty($date_to))
                $criteria->addBetweenCondition("t.$date_select",$date_from,$date_to); 	
        if(is_array($status))
                $criteria->addInCondition('t.status', $status);
        $criteria->select = "$field_select, agent_id, monitoring_id, count(id) as count_row";
                        if($type==TYPE_MAINTAIN)
                              $criteria->group = "$field_select, agent_id";
                      else
                              $criteria->group = "$field_select";
        $criteria->compare('t.type', $type);

        if(isset($_POST['GasMaintain']['province_id']))
          $criteria->compare('t.province_id', $_POST['GasMaintain']['province_id']);

        $model_ = call_user_func(array($name_model, 'model'));
        $mRes = $model_->findAll($criteria);
        $aRes = array();
        if(count($mRes)>0)
        foreach($mRes as $item){
            if($type==TYPE_MAINTAIN)
                $aRes[$item->agent_id][$item->$field_select] = $item->count_row;
            else
                $aRes[$item->monitoring_id][$item->$field_select] = $item->count_row;
                $total += $item->count_row;
        }
        $aRes['total_count'] = $total;
        return $aRes;			
    }
	  
    /**
     * @Author: ANH DUNG 11-19-2013
     * @Todo: thống kê đếm số bảo trì OR PTTT theo từng tháng,
     * theo tỉnh, khoảng thời gian vs trạng thái
     * @Param: model $model is model GasMaintain
     * @Param: string $field_name is maintain_date OR date_sell
     * @Param: model $name_model is model GasMaintain or GasMaintainsell
     * @Param: number $type is 1: bảo trì, 2: PTTT
     * @Return: array[total_month][2013][6][agent_id]=>123
     */ 
    public static function Statistic_maintain_by_month($model, $field_name='maintain_date', $name_model='GasMaintain', $type=TYPE_MAINTAIN){
    try{
        $model->statistic_month = is_array($model->statistic_month)?$model->statistic_month:ActiveRecord::getArrMonthSearch();
        $aRes = array();
        foreach($model->statistic_month as $month){
            // Lấy số ngày trong tháng
            $days_in_month = cal_days_in_month(0, $month, $model->statistic_year) ;

            for($i=1;$i<=$days_in_month; $i++){
                if($i<10)
                    $i='0'.$i;
               $date = $model->statistic_year.'-'.$month.'-'.$i;

               $criteria = new CDBcriteria();
               $criteria->compare("t.$field_name",$date);
               $criteria->compare("t.type", $type);
               if(isset($_POST['GasMaintain']['province_id']))
                 $criteria->compare('t.province_id', $_POST['GasMaintain']['province_id']);

               $criteria->select = "agent_id, count(id) as count_row";
               $criteria->group = "agent_id";		
               $model_ = call_user_func(array($name_model, 'model'));
               $mRes = $model_->findAll($criteria);

               if(count($mRes)>0){
                   if(!isset($aRes['month'][$month]))
                       $aRes['month'][$month] = 1;
                   $aRes['days'][$month][] = $i;
                   foreach($mRes as $item){
                       $aRes[$model->statistic_year][$month][$i][$item->agent_id] = $item->count_row;
                       if(!isset($aRes['total_month'][$model->statistic_year][$month][$item->agent_id]))
                           $aRes['total_month'][$model->statistic_year][$month][$item->agent_id] = $item->count_row;
                       else
                           $aRes['total_month'][$model->statistic_year][$month][$item->agent_id] += $item->count_row;
                   }
               }

            }

           //if($month==10){
           //echo '<pre>'; print_r($aRes['total_month'][$model->statistic_year][$month]);echo '</pre>';
           //}
           if(isset($aRes['total_month'][$model->statistic_year][$month]) && is_array($aRes['total_month'][$model->statistic_year][$month])){
               if($_POST['GasMaintain']['sort_by']=='asc'){ // tăng dần asort low to high	
                   asort($aRes['total_month'][$model->statistic_year][$month]);
               }else{ // high to low
                   arsort($aRes['total_month'][$model->statistic_year][$month]);
               }
           }
        }
        $_SESSION['data-excel'] = $aRes;
        return $aRes;
        
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }              
    } 
		 
	/**
     * @Author: ANH DUNG 11-22-2013
     * @Todo: thống kê đếm số bảo trì OR PTTT theo từng tháng,
     * theo tỉnh, khoảng thời gian vs trạng thái
     * @Param: model $model is model GasMaintain
     * @Param: string $field_name is maintain_date OR date_sell
     * @Param: model $name_model is model GasMaintain or GasMaintainsell
     * @Param: number $type is 1: bảo trì, 2: PTTT
     * @Return: array[total_month][2013][6][agent_id]=>123
     */ 
    public static function Statistic_market_development_by_month($model, $field_name='maintain_date', $name_model='GasMaintain', $type=TYPE_MARKET_DEVELOPMENT){
        try{
        $model->statistic_month = is_array($model->statistic_month)?$model->statistic_month:ActiveRecord::getArrMonthSearch();
        $model->monitoring_id = is_array($model->monitoring_id)?$model->monitoring_id:array();
        $aRes = array();
        foreach($model->statistic_month as $month){
            // Lấy số ngày trong tháng
            $days_in_month = cal_days_in_month(0, $month, $model->statistic_year) ;

            for($i=1;$i<=$days_in_month; $i++){
                if($i<10)
                    $i='0'.$i;
               $date = $model->statistic_year.'-'.$month.'-'.$i;

               $criteria = new CDBcriteria();
               $criteria->compare("t.$field_name",$date);
               $criteria->compare('t.type', $type);
               if(isset($_POST['GasMaintain']['province_id']))
                 $criteria->compare('t.province_id', $_POST['GasMaintain']['province_id']);
               if(count($model->monitoring_id)>0)
                   $criteria->addInCondition('t.monitoring_id', $model->monitoring_id);
               if(!empty($model->agent_id))
                   $criteria->compare('t.agent_id', $model->agent_id);

               $criteria->select = "maintain_employee_id, count(id) as count_row, monitoring_id";
               $criteria->group = "maintain_employee_id";		
               $model_ = call_user_func(array($name_model, 'model'));
               $mRes = $model_->findAll($criteria);

               if(count($mRes)>0){
                   if(!isset($aRes['month'][$month]))
                       $aRes['month'][$month] = 1;
                   $aRes['days'][$month][] = $i;
                   foreach($mRes as $item){
                       //if($item->count_row>0){
                           $aRes['market_dev'][$model->statistic_year][$month][$i][$item->monitoring_id][$item->maintain_employee_id] = $item->count_row;
                           if(!isset($aRes['total_month'][$model->statistic_year][$month][$item->monitoring_id][$item->maintain_employee_id]))
                                   $aRes['total_month'][$model->statistic_year][$month][$item->monitoring_id][$item->maintain_employee_id] = $item->count_row;
                           else
                                   $aRes['total_month'][$model->statistic_year][$month][$item->monitoring_id][$item->maintain_employee_id] += $item->count_row;
                           if(!isset($aRes['total_monitor'][$model->statistic_year][$month][$item->monitoring_id]))
                                   $aRes['total_monitor'][$model->statistic_year][$month][$item->monitoring_id] = $item->count_row;
                           else
                                   $aRes['total_monitor'][$model->statistic_year][$month][$item->monitoring_id] += $item->count_row;
                           if($item->count_row>0){
                               if(!isset($aRes['rowspan'][$model->statistic_year][$month][$item->monitoring_id]))
                                   $aRes['rowspan'][$model->statistic_year][$month][$item->monitoring_id] = 1;
                               else
                                   $aRes['rowspan'][$model->statistic_year][$month][$item->monitoring_id] += 1;
                           }
                       //}
                   }

               }// end if(count($mRes)>0){	

               // ++++++ to get bán hàng của PTTT ++++++++ ///

               $criteria = new CDBcriteria();
               $criteria->compare("t.date_sell",$date);
               $criteria->compare('t.type', $type);
               if(isset($_POST['GasMaintain']['province_id']))
                 $criteria->compare('t.province_id', $_POST['GasMaintain']['province_id']);
               if(!empty($model->agent_id))
                   $criteria->compare('t.agent_id', $model->agent_id);
               $criteria->select = "maintain_employee_id, count(id) as count_row, monitoring_id";
               $criteria->group = "maintain_employee_id";		
               $mRes = GasMaintainSell::model()->findAll($criteria);

               if(count($mRes)>0){
                   if(!isset($aRes['month'][$month]))
                       $aRes['month'][$month] = 1;
                   if(!isset($aRes['days'][$month]))
                       $aRes['days'][$month] = array();
                   if(!in_array($i , $aRes['days'][$month]))
                       $aRes['days'][$month][] = $i;
                   foreach($mRes as $item){
//                            if($item->count_row>0){
                           $aRes['market_dev_sell'][$model->statistic_year][$month][$i][$item->monitoring_id][$item->maintain_employee_id] = $item->count_row;

/*                          if(!isset($aRes['total_month_sell'][$model->statistic_year][$month][$item->monitoring_id][$item->maintain_employee_id]))
                                   $aRes['total_month_sell'][$model->statistic_year][$month][$item->monitoring_id][$item->maintain_employee_id] = $item->count_row;
                           else
                               $aRes['total_month_sell'][$model->statistic_year][$month][$item->monitoring_id][$item->maintain_employee_id] += $item->count_row;
                           */
                               // 05-12-2013 fix for employee Võ Hữu Hòa 114/2	

                           // đoạn này fix cho, nếu trong tháng nhân viên không có 1 PTTT nào, mà có bán hàng
                           if(!isset($aRes['total_month'][$model->statistic_year][$month][$item->monitoring_id][$item->maintain_employee_id]))
                               $aRes['total_month'][$model->statistic_year][$month][$item->monitoring_id][$item->maintain_employee_id] = 0;
                           
                           if(!isset($aRes['total_monitor'][$model->statistic_year][$month][$item->monitoring_id]))
                                   $aRes['total_monitor'][$model->statistic_year][$month][$item->monitoring_id] = 0;
                           
                           // đoạn này fix cho, nếu trong tháng nhân viên không có 1 PTTT nào, mà có bán hàng

                           if(!isset($aRes['total_month_sell'][$model->statistic_year][$month][$item->monitoring_id][$item->maintain_employee_id]))
                                   $aRes['total_month_sell'][$model->statistic_year][$month][$item->monitoring_id][$item->maintain_employee_id] = $item->count_row;
                           else
                                   $aRes['total_month_sell'][$model->statistic_year][$month][$item->monitoring_id][$item->maintain_employee_id] += $item->count_row;

                           if(!isset($aRes['total_monitor_sell'][$model->statistic_year][$month][$item->monitoring_id]))
                                   $aRes['total_monitor_sell'][$model->statistic_year][$month][$item->monitoring_id] = $item->count_row;
                           else
                                   $aRes['total_monitor_sell'][$model->statistic_year][$month][$item->monitoring_id] += $item->count_row;
//                            }	
                   }
               } // end if(count($mRes)>0){					
               // ++++++ to get bán hàng của PTTT ++++++++ 7,10,6///

            }// end for  for($i=1;$i<=$days_in_month; $i++){      

           if(0){// debug
                   //if($month==11)
               echo '<pre>';
               print_r($aRes['market_dev_sell']);die;
               print_r($aRes['total_month'][$model->statistic_year][$month]);
               asort($aRes['total_month'][$model->statistic_year][$month]);
               print_r($aRes['total_month'][$model->statistic_year][$month]);

               arsort($aRes['total_month'][$model->statistic_year][$month]);
               print_r($aRes['total_month'][$model->statistic_year][$month]);
               echo '</pre>';
               die;
           }		

           // Xử lý sắp xếp mảng của giám sát vs nhân viên PTTT theo thứ tự
           // $aRes['total_month'][$model->statistic_year][$month] is list array[monitor_id]=>array[employee_id]=>total
           if(isset($aRes['total_month'][$model->statistic_year][$month]) && is_array($aRes['total_month'][$model->statistic_year][$month]) && count($aRes['total_month'][$model->statistic_year][$month])>0)
           {
                   //echo '<pre>';
                   //print_r($aRes['total_month'][$model->statistic_year][$month]);echo '</pre>';
                   foreach($aRes['total_month'][$model->statistic_year][$month] as $key=>$item){
                       if($model->sort_by=='asc'){ // tăng dần asort low to high							
                               asort($item);						
                       }else{ // high to low
                               arsort($item);
                       }
                       $aRes['total_month'][$model->statistic_year][$month][$key] = $item;
                   }
                   // sort for total of monitor
                   if(isset($aRes['total_monitor'][$model->statistic_year][$month])){
                        if($model->sort_by=='asc'){ // tăng dần asort low to high	
                                asort($aRes['total_monitor'][$model->statistic_year][$month]);						
                        }else{ // high to low
                                arsort($aRes['total_monitor'][$model->statistic_year][$month]);
                        }
                    }
                   // sort for total of monitor
                   //echo '<pre>';
                   //print_r($aRes['total_month'][$model->statistic_year][$month]);echo '</pre>'; die;
                   //print_r($aRes['total_monitor'][$model->statistic_year][$month]);echo '</pre>'; die;
           }

           // end Xử lý sắp xếp mảng của giám sát vs nhân viên PTTT theo thứ tự

        } // end for foreach($model->statistic_month as $month){
        $_SESSION['data-excel'] = $aRes;
                    //var_dump($aRes['total_month']);
        return $aRes;
        
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }                     
    }
    
    /**
     * @Author: ANH DUNG 02-05-2014
     * @Todo: thống kê doanh thu - sản lượng của từng đại lý theo tháng,  năm
     * @Return: 1 tab đầu tổng của năm, tiếp theo là từng tháng
     */    
    public static function RevenueOutput($model){
    try{
        // 1. TỔNG SẢN LƯỢNG GAS BÌNH 12KG THÁNG 12 NĂM 2013
        // 2. TỔNG SẢN LƯỢNG GAS 12KG NĂM 2013
        // 3. TỔNG SẢN LƯỢNG GAS BÌNH BÒ THÁNG 11 NĂM 2013
        // 4. TỔNG SẢN LƯỢNG GAS BÌNH BÒ ĐẠI LÝ NĂM 2013
        // 5. TỔNG DOANH THU ĐẠI LÝ THÁNG 12 NĂM 2013
        // 6. TỔNG DOANH THU ĐẠI LÝ NĂM 2013
        // 7. TỔNG ĐẠI LÝ NỘP CÔNG TY THÁNG 12 NĂM 2013
        // 8. TỔNG ĐẠI LÝ NỘP CÔNG TY NĂM 2013
        $aRes = array();
        // chỗ ROLE_AGENT không phải đổi qua ROLE_SUB_USER_AGENT. Xác nhận Apr 13, 2014
        $aAgent = Users::getArrObjectUserByRoleHaveOrder(ROLE_AGENT, 'id');// $aAgent dạng: array[id agent]=>model agent
        //
//        $aRes['OUTPUT'] = Statistic::getTotalOutput($model, $aAgent, $aRes);
        Statistic::getTotalOutput($model, $aAgent, $aRes);
//        $aRes['MONEY_BANK'] = Statistic::getMoneyBankRevenue($model, $aAgent, 'MONEY_BANK');
//        $aRes['TOTAL_REVENUE'] = Statistic::getMoneyBankRevenue($model, $aAgent, 'TOTAL_REVENUE');
        Statistic::getMoneyBankRevenue($model, $aAgent, 'MONEY_BANK', $aRes);
        Statistic::getMoneyBankRevenue($model, $aAgent, 'TOTAL_REVENUE', $aRes);
        
        $_SESSION['data-excel'] = $aRes;
        return $aRes;

        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }                      
    }
    
    /**
     * @Author: ANH DUNG 02-07-2014
     * @Todo: tính tổng sản lượng của các loại gas
     * @param: $model 
     * @Return: array
     */        
    public static function getTotalOutput($model, $aAgent, &$aRes){
        // lặp 2 loại bình thống kê là 12 và 45kg
        foreach( CmsFormatter::$MATERIAL_TYPE_OUTPUT_STATISTIC as $type_output){
            // 1. get id những vật tư có loại là Gas 12 Kg OR 45kg
            $aMaterialId = GasMaterials::getArrayIdByMaterialTypeId($type_output['list_id']);        
            Statistic::getOutputByMonthYear($model, $model->statistic_month, $aMaterialId, $aRes, $type_output, $aAgent);
            if($type_output['code']=='12KG'){
                // lấy chi tiết sản lượng cho từng loại của B12, hộ gd, bò, mối
                if(empty($model->statistic_month)){
                    Statistic::getOutputDetail12KgByYear($model, $model->statistic_month, $aMaterialId, $aRes, GasStoreCard::$TYPE_CUSTOMER[CUSTOMER_HO_GIA_DINH], $aAgent);
                    Statistic::getOutputDetail12KgByYear($model, $model->statistic_month, $aMaterialId, $aRes, GasStoreCard::$TYPE_CUSTOMER[STORE_CARD_KH_BINH_BO], $aAgent);
                    Statistic::getOutputDetail12KgByYear($model, $model->statistic_month, $aMaterialId, $aRes, GasStoreCard::$TYPE_CUSTOMER[STORE_CARD_KH_MOI], $aAgent);                    
                }else{
                    Statistic::getOutputByMonthYearDetail12Kg($model, $model->statistic_month, $aMaterialId, $aRes, GasStoreCard::$TYPE_CUSTOMER[CUSTOMER_HO_GIA_DINH], $aAgent);
                    Statistic::getOutputByMonthYearDetail12Kg($model, $model->statistic_month, $aMaterialId, $aRes, GasStoreCard::$TYPE_CUSTOMER[STORE_CARD_KH_BINH_BO], $aAgent);
                    Statistic::getOutputByMonthYearDetail12Kg($model, $model->statistic_month, $aMaterialId, $aRes, GasStoreCard::$TYPE_CUSTOMER[STORE_CARD_KH_MOI], $aAgent);
                }
            }            
        }
    }
	 
    /**
     * @Author: ANH DUNG 02-09-2014
     * @Todo: tính tổng tiền đại lý nộp ngân hàng và tổng doanh thu của cty
     * @param: $model 
     * @Return: array
     */        
    public static function getMoneyBankRevenue($model, $aAgent, $TYPE, &$aRes){        
        Statistic::getMoneyBankAndTotalRevenue($model, $model->statistic_month, $aRes, $aAgent, $TYPE);
    }
	 
    /**
     * @Author: ANH DUNG 02-07-2014
     * @Todo: tính tổng sản lượng của từng loại gas, xếp theo ngày, tính theo loại vật tư
     * @param: $aMaterial array id vật tư
     * @param: $month     
     * @param: $model
     * @param: $aRes: is final array return
     * @param: $type_output is  array(
            'code'=>'12KG',
            'name'=>'BÌNH 12KG',
            'list_id'=>array(4), // Loại Vật Tư Gas 12 Kg, hiện tại chỉ có 1, sẽ dc tách ra nhiều sau này
        ), 
     * @Return: not return because it in $aRes
     */        
    public static function getOutputByMonthYear($model, $month, $aMaterialId, &$aRes, $type_output, $aAgent){        
        self::getOutputByYear($model, $month, $aMaterialId, $aRes, $type_output, $aAgent);
        if(empty($model->statistic_month)) return;
        // Lấy số ngày trong tháng
        $days_in_month = cal_days_in_month(0, $month, $model->statistic_year) ;        
        $aAgentSortMonth = array();
        $aAgentSortYear = array();
        for($i=1;$i<=$days_in_month; $i++){
            if($i<10)
                $i='0'.$i;
            $date = $model->statistic_year.'-'.$month.'-'.$i;
            $criteria = new CDBcriteria();
            $criteria->compare("t.date_delivery",$date);
            $criteria->addInCondition("t.type_in_out", CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC); // xuất bán, bán bộ bình, thế chân
            $criteria->addInCondition("t.materials_id", $aMaterialId);
            $criteria->addInCondition("t.user_id_create", $model->agent_id);
            $criteria->select = "t.user_id_create, sum(qty) as qty";
            $criteria->group = "t.user_id_create";
            $mRes = GasStoreCardDetail::model()->findAll($criteria);
            if(count($mRes)>0){
                
                if(!isset($aRes['month'][$month]))
                    $aRes['month'][$month] = 1; // biến $month dùng để foreach nên chỉ cần gán =1 là dc
                $aRes['OUTPUT'][$type_output['code']]['days'][$month][] = $i;
                foreach($mRes as $item){
                    //  for tab each month
                    // one cell
                    $aRes['OUTPUT'][$type_output['code']][$month][$i][$item->user_id_create] = $item->qty;
                    // like : $aRes['OUTPUT'][12KG]['total_day'][08][agent 102]
                    //sum_row -- dùng kiểu này vì sort dc theo tỉnh
                    if(!isset($aAgentSortMonth[$aAgent[$item->user_id_create]->province_id][$item->user_id_create]))
                        $aAgentSortMonth[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] = $item->qty;
                    else
                        $aAgentSortMonth[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] += $item->qty;
                    
                    if(!isset($aRes['OUTPUT'][$type_output['code']]['sum_col_total_month_all_agent'][$month]))
                        $aRes['OUTPUT'][$type_output['code']]['sum_col_total_month_all_agent'][$month] = $item->qty;
                    else
                        $aRes['OUTPUT'][$type_output['code']]['sum_col_total_month_all_agent'][$month] += $item->qty;
                    // need sum column...here, nên sum luôn column ở đây, xuống view khỏi tạo biến để sum nữa
                    // sum column one day for all agent
                    if(!isset($aRes['OUTPUT'][$type_output['code']]['sum_col'][$month][$i]))
                        $aRes['OUTPUT'][$type_output['code']]['sum_col'][$month][$i] = $item->qty;
                    else
                        $aRes['OUTPUT'][$type_output['code']]['sum_col'][$month][$i] += $item->qty;                                        
                    //  for tab each month
                    
                    //  for tab sum year
                    // one cell
                    if(!isset($aRes['OUTPUT'][$type_output['code']]['total_year'][$month][$item->user_id_create]))
                        $aRes['OUTPUT'][$type_output['code']]['total_year'][$month][$item->user_id_create] = $item->qty;
                    else
                        $aRes['OUTPUT'][$type_output['code']]['total_year'][$month][$item->user_id_create] += $item->qty;
                    
                    //sum_row
                    if(!isset($aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create]))
                        $aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] = $item->qty;
                    else
                        $aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] += $item->qty;

                    // need sum column...here, nên sum luôn column ở đây, xuống view khỏi tạo biến để sum nữa
                    // sum column one day for all agent
                    if(!isset($aRes['OUTPUT'][$type_output['code']]['total_year']['sum_col'][$month]))
                        $aRes['OUTPUT'][$type_output['code']]['total_year']['sum_col'][$month] = $item->qty;
                    else
                        $aRes['OUTPUT'][$type_output['code']]['total_year']['sum_col'][$month] += $item->qty;           
                    
                    if(!isset($aRes['OUTPUT'][$type_output['code']]['sum_col_total_year_all_agent']))
                        $aRes['OUTPUT'][$type_output['code']]['sum_col_total_year_all_agent'] = $item->qty;
                    else
                        $aRes['OUTPUT'][$type_output['code']]['sum_col_total_year_all_agent'] += $item->qty;                    
                    //  for tab sum year
                }                   
            }
                        
        } // end  for($i=1;$i<=$days_in_month; $i++){
        
        // sort each month by tỉnh
        if(count($aAgentSortMonth)){
            foreach($aAgentSortMonth as $province_id=>$arrayAgentTotalMonth){
                arsort($arrayAgentTotalMonth);
                if(!isset($aRes['OUTPUT'][$type_output['code']]['sum_row_total_month'][$month]))
                    $aRes['OUTPUT'][$type_output['code']]['sum_row_total_month'][$month] = $arrayAgentTotalMonth;
                else
                    $aRes['OUTPUT'][$type_output['code']]['sum_row_total_month'][$month] += $arrayAgentTotalMonth;
            }
        }
        
        // sort year by tỉnh
//        if(count($aAgentSortYear)){
//            foreach($aAgentSortYear as $province_id=>$arrayAgentTotalMonth){
//                arsort($arrayAgentTotalMonth);
//                if(!isset($aRes['OUTPUT'][$type_output['code']]['total_year']['sum_row']))
//                    $aRes['OUTPUT'][$type_output['code']]['total_year']['sum_row'] = $arrayAgentTotalMonth;
//                else
//                    $aRes['OUTPUT'][$type_output['code']]['total_year']['sum_row'] += $arrayAgentTotalMonth;
//            }
//        }
        // sort
    }
    
    
    // Tính riêng cho năm
    public static function getOutputByYear($model, $month, $aMaterialId, &$aRes, $type_output, $aAgent){        
        if(!empty($model->statistic_month)) return;
        // Lấy số ngày trong tháng
//        $days_in_month = cal_days_in_month(0, $month, $model->statistic_year) ;        
        $aAgentSortMonth = array();
        $aAgentSortYear = array();
        for($i=1;$i<=12; $i++){
            if($i<10)
                $i='0'.$i;
            $month = $i;
            $criteria = new CDBcriteria();
            $criteria->compare('month(t.date_delivery)', $month);
            $criteria->compare('year(t.date_delivery)', $model->statistic_year);
            $criteria->addInCondition("t.type_in_out", CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC); // xuất bán, bán bộ bình, thế chân
            $criteria->addInCondition("t.materials_id", $aMaterialId);
            $criteria->addInCondition("t.user_id_create", $model->agent_id);
            $criteria->select = "t.user_id_create, sum(qty) as qty";
            $criteria->group = "t.user_id_create";
            $mRes = GasStoreCardDetail::model()->findAll($criteria);
            if(count($mRes)>0){
                
                if(!isset($aRes['month'][$month]))
                    $aRes['month'][$month] = 1; // biến $month dùng để foreach nên chỉ cần gán =1 là dc
                foreach($mRes as $item){
                    //  for tab sum year
                    // one cell
                    if(!isset($aRes['OUTPUT'][$type_output['code']]['total_year'][$month][$item->user_id_create]))
                        $aRes['OUTPUT'][$type_output['code']]['total_year'][$month][$item->user_id_create] = $item->qty;
                    else
                        $aRes['OUTPUT'][$type_output['code']]['total_year'][$month][$item->user_id_create] += $item->qty;
                    
                    //sum_row
                    if(!isset($aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create]))
                        $aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] = $item->qty;
                    else
                        $aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] += $item->qty;

                    // need sum column...here, nên sum luôn column ở đây, xuống view khỏi tạo biến để sum nữa
                    // sum column one day for all agent
                    if(!isset($aRes['OUTPUT'][$type_output['code']]['total_year']['sum_col'][$month]))
                        $aRes['OUTPUT'][$type_output['code']]['total_year']['sum_col'][$month] = $item->qty;
                    else
                        $aRes['OUTPUT'][$type_output['code']]['total_year']['sum_col'][$month] += $item->qty;           
                    
                    if(!isset($aRes['OUTPUT'][$type_output['code']]['sum_col_total_year_all_agent']))
                        $aRes['OUTPUT'][$type_output['code']]['sum_col_total_year_all_agent'] = $item->qty;
                    else
                        $aRes['OUTPUT'][$type_output['code']]['sum_col_total_year_all_agent'] += $item->qty;                    
                    //  for tab sum year
                }                   
            }
                        
        } // end  for($i=1;$i<=$days_in_month; $i++){
        
        // sort each month by tỉnh
        if(count($aAgentSortMonth)){
            foreach($aAgentSortMonth as $province_id=>$arrayAgentTotalMonth){
                arsort($arrayAgentTotalMonth);
                if(!isset($aRes['OUTPUT'][$type_output['code']]['sum_row_total_month'][$month]))
                    $aRes['OUTPUT'][$type_output['code']]['sum_row_total_month'][$month] = $arrayAgentTotalMonth;
                else
                    $aRes['OUTPUT'][$type_output['code']]['sum_row_total_month'][$month] += $arrayAgentTotalMonth;
            }
        }
        
        // sort year by tỉnh
        if(count($aAgentSortYear)){
            foreach($aAgentSortYear as $province_id=>$arrayAgentTotalMonth){
                arsort($arrayAgentTotalMonth);
                if(!isset($aRes['OUTPUT'][$type_output['code']]['total_year']['sum_row']))
                    $aRes['OUTPUT'][$type_output['code']]['total_year']['sum_row'] = $arrayAgentTotalMonth;
                else
                    $aRes['OUTPUT'][$type_output['code']]['total_year']['sum_row'] += $arrayAgentTotalMonth;
            }
        }
        // sort
        return;
    }
    
    public static function getOutputByMonthYearDetail12Kg($model, $month, $aMaterialId, &$aRes, $type_output, $aAgent){        
        
        if(empty($model->statistic_month)) return;
        // Lấy số ngày trong tháng
        $days_in_month = cal_days_in_month(0, $month, $model->statistic_year) ;        
        $aAgentSortMonth = array();
        $aAgentSortYear = array();
        $type_customer = array_search($type_output, GasStoreCard::$TYPE_CUSTOMER) ;
        
        for($i=1;$i<=$days_in_month; $i++){
            if($i<10)
                $i='0'.$i;
            $date = $model->statistic_year.'-'.$month.'-'.$i;
            $criteria = new CDBcriteria();
            $criteria->compare("t.date_delivery",$date);
            $criteria->compare("t.type_customer",$type_customer);
            
            $criteria->addInCondition("t.type_in_out", CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC); // xuất bán, bán bộ bình, thế chân
            $criteria->addInCondition("t.materials_id", $aMaterialId);
            $criteria->addInCondition("t.user_id_create", $model->agent_id);
            $criteria->select = "t.user_id_create, sum(qty) as qty";
            $criteria->group = "t.user_id_create";
            $mRes = GasStoreCardDetail::model()->findAll($criteria);
            if(count($mRes)>0){
                
                if(!isset($aRes['month'][$month]))
                    $aRes['month'][$month] = 1; // biến $month dùng để foreach nên chỉ cần gán =1 là dc
                $aRes['DETAIL_12KG'][$type_output]['days'][$month][] = $i;
                foreach($mRes as $item){
                    //  for tab each month
                    // one cell
                    $aRes['DETAIL_12KG'][$type_output][$month][$i][$item->user_id_create] = $item->qty;
                    // like : $aRes['DETAIL_12KG'][12KG]['total_day'][08][agent 102]
                    //sum_row -- dùng kiểu này vì sort dc theo tỉnh
                    if(!isset($aAgentSortMonth[$aAgent[$item->user_id_create]->province_id][$item->user_id_create]))
                        $aAgentSortMonth[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] = $item->qty;
                    else
                        $aAgentSortMonth[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] += $item->qty;
                    
                    if(!isset($aRes['DETAIL_12KG'][$type_output]['sum_col_total_month_all_agent'][$month]))
                        $aRes['DETAIL_12KG'][$type_output]['sum_col_total_month_all_agent'][$month] = $item->qty;
                    else
                        $aRes['DETAIL_12KG'][$type_output]['sum_col_total_month_all_agent'][$month] += $item->qty;
                    // need sum column...here, nên sum luôn column ở đây, xuống view khỏi tạo biến để sum nữa
                    // sum column one day for all agent
                    if(!isset($aRes['DETAIL_12KG'][$type_output]['sum_col'][$month][$i]))
                        $aRes['DETAIL_12KG'][$type_output]['sum_col'][$month][$i] = $item->qty;
                    else
                        $aRes['DETAIL_12KG'][$type_output]['sum_col'][$month][$i] += $item->qty;                                        
                    //  for tab each month
                    
                    //  for tab sum year
                    // one cell
                    if(!isset($aRes['DETAIL_12KG'][$type_output]['total_year'][$month][$item->user_id_create]))
                        $aRes['DETAIL_12KG'][$type_output]['total_year'][$month][$item->user_id_create] = $item->qty;
                    else
                        $aRes['DETAIL_12KG'][$type_output]['total_year'][$month][$item->user_id_create] += $item->qty;
                    
                    //sum_row
                    if(!isset($aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create]))
                        $aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] = $item->qty;
                    else
                        $aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] += $item->qty;

                    // need sum column...here, nên sum luôn column ở đây, xuống view khỏi tạo biến để sum nữa
                    // sum column one day for all agent
                    if(!isset($aRes['DETAIL_12KG'][$type_output]['total_year']['sum_col'][$month]))
                        $aRes['DETAIL_12KG'][$type_output]['total_year']['sum_col'][$month] = $item->qty;
                    else
                        $aRes['DETAIL_12KG'][$type_output]['total_year']['sum_col'][$month] += $item->qty;           
                    
                    if(!isset($aRes['DETAIL_12KG'][$type_output]['sum_col_total_year_all_agent']))
                        $aRes['DETAIL_12KG'][$type_output]['sum_col_total_year_all_agent'] = $item->qty;
                    else
                        $aRes['DETAIL_12KG'][$type_output]['sum_col_total_year_all_agent'] += $item->qty;                    
                    //  for tab sum year
                }                   
            }
                        
        } // end  for($i=1;$i<=$days_in_month; $i++){
        
        // sort each month by tỉnh
        if(count($aAgentSortMonth)){
            foreach($aAgentSortMonth as $province_id=>$arrayAgentTotalMonth){
                arsort($arrayAgentTotalMonth);
                if(!isset($aRes['DETAIL_12KG'][$type_output]['sum_row_total_month'][$month]))
                    $aRes['DETAIL_12KG'][$type_output]['sum_row_total_month'][$month] = $arrayAgentTotalMonth;
                else
                    $aRes['DETAIL_12KG'][$type_output]['sum_row_total_month'][$month] += $arrayAgentTotalMonth;
            }
        }
        
        // sort year by tỉnh
//        if(count($aAgentSortYear)){
//            foreach($aAgentSortYear as $province_id=>$arrayAgentTotalMonth){
//                arsort($arrayAgentTotalMonth);
//                if(!isset($aRes['DETAIL_12KG'][$type_output]['total_year']['sum_row']))
//                    $aRes['DETAIL_12KG'][$type_output]['total_year']['sum_row'] = $arrayAgentTotalMonth;
//                else
//                    $aRes['DETAIL_12KG'][$type_output]['total_year']['sum_row'] += $arrayAgentTotalMonth;
//            }
//        }
        // sort
    }
	 
    // lấy chi tiết bình 12 theo năm
    public static function getOutputDetail12KgByYear($model, $month, $aMaterialId, &$aRes, $type_output, $aAgent){        
        if(!empty($model->statistic_month)) return;
        // Lấy số ngày trong tháng
        $aAgentSortMonth = array();
        $aAgentSortYear = array();
        $type_customer = array_search($type_output, GasStoreCard::$TYPE_CUSTOMER) ;
        
        for($i=1;$i<=12; $i++){
            if($i<10)
                $i='0'.$i;
            $month = $i;
            $criteria = new CDBcriteria();
            $criteria->compare('month(t.date_delivery)', $month);
            $criteria->compare('year(t.date_delivery)', $model->statistic_year);
            $criteria->compare("t.type_customer",$type_customer);
            
            $criteria->addInCondition("t.type_in_out", CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC); // xuất bán, bán bộ bình, thế chân
            $criteria->addInCondition("t.materials_id", $aMaterialId);
            $criteria->addInCondition("t.user_id_create", $model->agent_id);
            $criteria->select = "t.user_id_create, sum(qty) as qty";
            $criteria->group = "t.user_id_create";
            $mRes = GasStoreCardDetail::model()->findAll($criteria);
            if(count($mRes)>0){
                if(!isset($aRes['month'][$month]))
                    $aRes['month'][$month] = 1; // biến $month dùng để foreach nên chỉ cần gán =1 là dc
                foreach($mRes as $item){
                    
                    //  for tab sum year
                    // one cell
                    if(!isset($aRes['DETAIL_12KG'][$type_output]['total_year'][$month][$item->user_id_create]))
                        $aRes['DETAIL_12KG'][$type_output]['total_year'][$month][$item->user_id_create] = $item->qty;
                    else
                        $aRes['DETAIL_12KG'][$type_output]['total_year'][$month][$item->user_id_create] += $item->qty;
                    
                    //sum_row
                    if(!isset($aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create]))
                        $aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] = $item->qty;
                    else
                        $aAgentSortYear[$aAgent[$item->user_id_create]->province_id][$item->user_id_create] += $item->qty;

                    // need sum column...here, nên sum luôn column ở đây, xuống view khỏi tạo biến để sum nữa
                    // sum column one day for all agent
                    if(!isset($aRes['DETAIL_12KG'][$type_output]['total_year']['sum_col'][$month]))
                        $aRes['DETAIL_12KG'][$type_output]['total_year']['sum_col'][$month] = $item->qty;
                    else
                        $aRes['DETAIL_12KG'][$type_output]['total_year']['sum_col'][$month] += $item->qty;           
                    
                    if(!isset($aRes['DETAIL_12KG'][$type_output]['sum_col_total_year_all_agent']))
                        $aRes['DETAIL_12KG'][$type_output]['sum_col_total_year_all_agent'] = $item->qty;
                    else
                        $aRes['DETAIL_12KG'][$type_output]['sum_col_total_year_all_agent'] += $item->qty;                    
                    //  for tab sum year
                }                   
            }
                        
        } // end  for($i=1;$i<=$days_in_month; $i++){
        
        
        // sort year by tỉnh
        if(count($aAgentSortYear)){
            foreach($aAgentSortYear as $province_id=>$arrayAgentTotalMonth){
                arsort($arrayAgentTotalMonth);
                if(!isset($aRes['DETAIL_12KG'][$type_output]['total_year']['sum_row']))
                    $aRes['DETAIL_12KG'][$type_output]['total_year']['sum_row'] = $arrayAgentTotalMonth;
                else
                    $aRes['DETAIL_12KG'][$type_output]['total_year']['sum_row'] += $arrayAgentTotalMonth;
            }
        }
        // sort
    }
	 	  
    /**
     * @Author: ANH DUNG 02-07-2014
     * @Todo: tính tổng tiền thu về từ đại lý nộp ngân hàng và tính tổng doanh thu từ đại lý -- gộp chung lại, gọi 2 lần
     * @param: $model
     * @param: $month     
     * @param: $aRes: is final array return
     * @param: $TYPE: MONEY_BANK or TOTAL_REVENUE
     * @Return: not return because it in &$aRes MONEY_BANK TOTAL_REVENUE
     */        
    public static function getMoneyBankAndTotalRevenue($model, $month, &$aRes, $aAgent, $TYPE='MONEY_BANK'){
        
        if(empty($model->statistic_month)) return;
        // Lấy số ngày trong tháng
        $days_in_month = cal_days_in_month(0, $month, $model->statistic_year) ;        
        $aAgentSortMonth = array();
        $aAgentSortYear = array();
        for($i=1;$i<=$days_in_month; $i++){
            if($i<10)
                $i='0'.$i;
            $date = $model->statistic_year.'-'.$month.'-'.$i;
            $criteria = new CDBcriteria();
            $criteria->compare("t.release_date",$date);
            if($TYPE=='MONEY_BANK'){// chi nộp ngân hàng
                $criteria->addInCondition("t.master_lookup_id", CmsFormatter::$TYPE_MONEY_FROM_BANK); 
            }else{// TỔNG DOANH THU ĐẠI LÝ - trừ kh bình bò ra
                $criteria->addInCondition("t.master_lookup_id", CmsFormatter::$REVENUE_ITEM); 
                $criteria->addNotInCondition("t.type_customer", CmsFormatter::$REVENUE_ITEM_EXCEPT_CUSTOMER); 
            }
            $criteria->addInCondition("t.agent_id", $model->agent_id);
            $criteria->select = "t.agent_id, sum(amount) as amount";
            $criteria->group = "t.agent_id";
            $mRes = GasCashBookDetail::model()->findAll($criteria);
            if(count($mRes)>0){
                
                if(!isset($aRes['month'][$month]))
                    $aRes['month'][$month] = 1; // biến $month dùng để foreach nên chỉ cần gán =1 là dc
                $aRes[$TYPE]['days'][$month][] = $i;
                foreach($mRes as $item){
                    //  for tab each month
                    // one cell
                    $aRes[$TYPE][$month][$i][$item->agent_id] = $item->amount;
                    // like : $aRes[12KG]['total_day'][08][agent 102]
                    //sum_row -- dùng kiểu này vì sort dc theo tỉnh
                    if(!isset($aAgentSortMonth[$aAgent[$item->agent_id]->province_id][$item->agent_id]))
                        $aAgentSortMonth[$aAgent[$item->agent_id]->province_id][$item->agent_id] = $item->amount;
                    else
                        $aAgentSortMonth[$aAgent[$item->agent_id]->province_id][$item->agent_id] += $item->amount;
                    
                    if(!isset($aRes[$TYPE]['sum_col_total_month_all_agent'][$month]))
                        $aRes[$TYPE]['sum_col_total_month_all_agent'][$month] = $item->amount;
                    else
                        $aRes[$TYPE]['sum_col_total_month_all_agent'][$month] += $item->amount;
                    // need sum column...here, nên sum luôn column ở đây, xuống view khỏi tạo biến để sum nữa
                    // sum column one day for all agent
                    if(!isset($aRes[$TYPE]['sum_col'][$month][$i]))
                        $aRes[$TYPE]['sum_col'][$month][$i] = $item->amount;
                    else
                        $aRes[$TYPE]['sum_col'][$month][$i] += $item->amount;                    
                    
                    //  for tab each month
                    
                    //  for tab sum year
                    // one cell
                    if(!isset($aRes[$TYPE]['total_year'][$month][$item->agent_id]))
                        $aRes[$TYPE]['total_year'][$month][$item->agent_id] = $item->amount;
                    else
                        $aRes[$TYPE]['total_year'][$month][$item->agent_id] += $item->amount;
                    
                    //sum_row
                    if(!isset($aAgentSortYear[$aAgent[$item->agent_id]->province_id][$item->agent_id]))
                        $aAgentSortYear[$aAgent[$item->agent_id]->province_id][$item->agent_id] = $item->amount;
                    else
                        $aAgentSortYear[$aAgent[$item->agent_id]->province_id][$item->agent_id] += $item->amount;

                    // need sum column...here, nên sum luôn column ở đây, xuống view khỏi tạo biến để sum nữa
                    // sum column one day for all agent
                    if(!isset($aRes[$TYPE]['total_year']['sum_col'][$month]))
                        $aRes[$TYPE]['total_year']['sum_col'][$month] = $item->amount;
                    else
                        $aRes[$TYPE]['total_year']['sum_col'][$month] += $item->amount;           
                    
                    if(!isset($aRes[$TYPE]['sum_col_total_year_all_agent']))
                        $aRes[$TYPE]['sum_col_total_year_all_agent'] = $item->amount;
                    else
                        $aRes[$TYPE]['sum_col_total_year_all_agent'] += $item->amount;
                    
                    //  for tab sum year
                }                   
            }
                        
        } // end  for($i=1;$i<=$days_in_month; $i++){
        
        // sort each month by tỉnh
        if(count($aAgentSortMonth)){
            foreach($aAgentSortMonth as $province_id=>$arrayAgentTotalMonth){
                arsort($arrayAgentTotalMonth);
                if(!isset($aRes[$TYPE]['sum_row_total_month'][$month]))
                    $aRes[$TYPE]['sum_row_total_month'][$month] = $arrayAgentTotalMonth;
                else
                    $aRes[$TYPE]['sum_row_total_month'][$month] += $arrayAgentTotalMonth;
            }
        }
        
        // sort year by tỉnh
        if(count($aAgentSortYear)){
            foreach($aAgentSortYear as $province_id=>$arrayAgentTotalMonth){
                arsort($arrayAgentTotalMonth);
                if(!isset($aRes[$TYPE]['total_year']['sum_row']))
                    $aRes[$TYPE]['total_year']['sum_row'] = $arrayAgentTotalMonth;
                else
                    $aRes[$TYPE]['total_year']['sum_row'] += $arrayAgentTotalMonth;
            }
        }
        // sort
    }
	 
    /**
     * @Author: ANH DUNG Feb 22, 2014
     * @Todo: thống kê daily sản lượng KH bình bò, mối
     * @param: $model
     * @Return: $aRes: is final array return
     */       
    public static function Output_daily($model){
        try{
        /* 1. findAll customer id của Loại KH bò hoặc mối
         * 2. nếu có sale thì findAll luôn customer id của sale
         * 3. trong truy vấn sẽ ưu tiên cho ngày hơn là chọn tháng và năm
         * 4. lấy id các loại bình từ loại vật tư
         */
        $aRes = array();
        $aRes['customer_id'] = array();
        $aRes['store_card_id'] = array();
        $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
        $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);
        // 1. findAll customer id của Loại KH bò hoặc mối
        $customerIdByBinhBoMoi = Users::getCustomerBinhBoMoi($model->ext_is_maintain);
        // 2. nếu có sale thì findAll luôn customer id của sale
        $customerIdBySale = Users::getCustomerBySale($model->ext_sale_id);        
        // 4. lấy id các loại bình từ loại vật tư
        foreach( CmsFormatter::$MATERIAL_TYPE_BINHBO_12 as $material_type_id){
            // $material_type_id là loại vật tư MATERIAL_TYPE_BINHBO_50,// Gas Bình Bò 50 Kg ....
            // get array id những vật tư có loại là $material_type_id: Gas 12 Kg OR 45kg OR 50
            $aMaterialId = GasMaterials::getArrayIdByMaterialTypeId($material_type_id);     
            
            $criteria = new CDBcriteria();
            // kiểm tra xem là nhập vỏ hay xuất bán
            $GET_ID_STORECARD = 0; // biến này để xuống dưới kiểm tra xem có lấy id store card không?
            $type_in_out = array(STORE_CARD_TYPE_5); // nhập vỏ
            if(in_array($material_type_id, CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT)){
                $GET_ID_STORECARD = 1;
                $type_in_out = CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC; // xuất bán
            }
            $criteria->addInCondition("t.type_in_out", $type_in_out ); // nhập vỏ
//            $type_in_out = STORE_CARD_TYPE_5; // nhập vỏ
//            if(in_array($material_type_id, CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT)){
//                $type_in_out = STORE_CARD_TYPE_3; // xuất bán
//            }
//            $criteria->compare("t.type_in_out", $type_in_out ); // nhập vỏ
            
            // kiểm tra xem là nhập vỏ hay xuất bán
            
            if(count($aMaterialId)){
                $criteria->addInCondition("t.materials_id", $aMaterialId);
            }
            
            $criteria->addInCondition("t.user_id_create", $model->agent_id);
            if(count($customerIdByBinhBoMoi)){
                $criteria->addInCondition("t.customer_id", $customerIdByBinhBoMoi);
            }
            if(count($customerIdBySale)){
                $criteria->addInCondition("t.customer_id", $customerIdBySale);
            }
            
            if(!empty($model->customer_id)){
                $criteria->compare("t.customer_id", $model->customer_id );
            }
            
            if(!empty($model->collection_customer)){
                if($model->collection_customer==  GasStoreCard::DA_THU_TIEN){
                    $criteria->compare("t.collection_customer_done", 1);
                }elseif($model->collection_customer==GasStoreCard::CHUA_THU_TIEN){
                    $criteria->addCondition('t.collection_customer_done<1 OR t.collection_customer_done IS NULL');
                }
            }             
            
            // 3. trong truy vấn sẽ ưu tiên cho ngày hơn là chọn tháng và năm            
            $criteria->addBetweenCondition("t.date_delivery",$date_from,$date_to);             
            // tạm close lại vì sẽ search từ ngày .. đến ngày - May 22, 2014
//            if(!empty($model->date_delivery)){
//                $date_delivery = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_delivery);
//                $criteria->compare('t.date_delivery', $date_delivery);
//            }            
//            else{ 
//                if(!empty($model->statistic_month))
//                    $criteria->compare('month(t.date_delivery)', $model->statistic_month);
//                if(!empty($model->statistic_year))
//                    $criteria->compare('year(t.date_delivery)', $model->statistic_year);
//            }

            $criteria->select = "t.user_id_create,t.date_delivery,t.customer_id,"
                    . "t.store_card_id, sum(qty) as qty, sum(collection_customer) as collection_customer, "
                    . "t.collection_customer_date, sum(receivables_customer) as receivables_customer";
            $criteria->group = "t.date_delivery, t.customer_id,t.user_id_create"; 
//            $criteria->group = "t.date_delivery, t.customer_id"; 
            $criteria->order = "t.date_delivery ASC";
            $mRes = GasStoreCardDetail::model()->findAll($criteria);     
            // đoạn này viết mà không rõ kết quả?
            if(count($mRes)){
                foreach($mRes as $item){
                    // $aRes['customer_id'][$item->customer_id] là những Kh duy nhất của list này
                    $aRes['customer_id'][$item->customer_id] = $item->customer_id; // dùng để show info cua customer chăng, findAll 1 lần để lấy all model của user?
                    $aRes['store_card_id'][$item->store_card_id] = $item->store_card_id; // dùng để lấy người giao hàng sau này, findAll với Incondition để lấy thông tin người giao hàng
                    $aRes['data'][$item->date_delivery][$item->user_id_create][$item->customer_id][$material_type_id] = $item;                                        
                    $aRes['agent'][$item->customer_id] = $item->user_id_create;
                    $aRes['agent_customer_unique'][$item->user_id_create][$item->customer_id] = $item->user_id_create;
                    if($GET_ID_STORECARD){
                        // May 07, 2014 chỗ này mới lấy model người giao của xuất bán, chưa lấy người giao của nhập vỏ
                        $aRes['store_card'][$item->user_id_create][$item->customer_id] = $item->store_card_id;
                    }
                    
                    // for thu tiền KH, phải tách ra vì cái obj  $item kia nó gom nhóm theo $material_type_id
                    if(!isset($aRes['collection_customer'][$item->date_delivery][$item->user_id_create][$item->customer_id])){
                        $aRes['collection_customer'][$item->date_delivery][$item->user_id_create][$item->customer_id] = $item->collection_customer;
                    }else{
                        $aRes['collection_customer'][$item->date_delivery][$item->user_id_create][$item->customer_id] += $item->collection_customer;
                    }
                    
                    if(!isset($aRes['receivables_customer'][$item->date_delivery][$item->user_id_create][$item->customer_id])){
                        $aRes['receivables_customer'][$item->date_delivery][$item->user_id_create][$item->customer_id] = $item->receivables_customer;
                    }else{
                        $aRes['receivables_customer'][$item->date_delivery][$item->user_id_create][$item->customer_id] += $item->receivables_customer;
                    }
                }
            }
        
        } // end oreach( CmsFormatter::$MATERIAL_TYPE_BINHBO_12
        
        // Lấy in fo của customer vs storecard
        $aRes['customer_obj'] = array();
        $aRes['store_card_obj'] = array();
       
        if(count($aRes['customer_id'])){
            $aRes['customer_obj'] = Users::getArrObjectUserByRole('', $aRes['customer_id']);
        }
        if(count($aRes['store_card_id'])){
            $aRes['store_card_obj'] = GasStoreCard::getArrObjectByArrId($aRes['store_card_id']);
        }
        // lấy gas dư
        self::getGasRemain($model, $aRes, $date_from, $date_to);
        // lấy gas dư
        $aRes['ARR_DAYS'] = MyFormat::getArrayDay($date_from, $date_to);
        
        // Lấy in fo của customer vs storecard
        $_SESSION['data-excel'] = $aRes;
        
        return $aRes;
        
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }           
    }
    
    /**
     * @Author: ANH DUNG Feb 28, 2014
     * @Todo: lấy gas dư của KH trong ngày đó hoặc tháng
     * @param: $model
     * @Return: $aRes: is final array return
     */      
    public static function getGasRemain($model, &$aRes, $date_from, $date_to){
        $criteria = new CDBcriteria();
        if(count($aRes['customer_id'])){
            $criteria->addInCondition('t.customer_id', $aRes['customer_id']);
        }
        
        // 3. trong truy vấn sẽ ưu tiên cho ngày hơn là chọn tháng và năm
        $criteria->addBetweenCondition("t.date_input",$date_from,$date_to);             
            // tạm close lại vì sẽ search từ ngày .. đến ngày - May 22, 2014        
//        if(!empty($model->date_delivery)){
//            $date_delivery = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_delivery);
//            $criteria->compare('t.date_input', $date_delivery);
//        }else{
//            if(!empty($model->statistic_month))
//                $criteria->compare('month(t.date_input)', $model->statistic_month);
//            if(!empty($model->statistic_year))
//                $criteria->compare('year(t.date_input)', $model->statistic_year);
//        }        

        $criteria->select = "t.date_input,t.customer_id,t.agent_id, "
                . "sum(amount_gas) as amount_gas, sum(amount) as amount";
        $criteria->group = "t.date_input, t.customer_id, t.agent_id"; 
        $criteria->order = "t.date_input ASC";
        
        $mRes = GasRemain::model()->findAll($criteria);
        if(count($mRes)){
            foreach($mRes as $item){
                $aRes['GasRemain'][$item->date_input][$item->agent_id][$item->customer_id] = $item;
//                echo $item->date_input." - $item->customer_id - $item->agent_id ** debug";
            }        
        } 
    }
    
    /**
     * @Author: ANH DUNG Mar 26, 2014
     * @Todo: thống kê target của sale vs agent
     * @param: $model
     * @Return: $aRes: is final array return
     */       
    public static function Target($model, $needMore=array()){
    try{
        /* 1. lấy mảng user cần tìm gồm agent_id vs sale_id là biến $model->agent_id
         * 2. find target dc thiết lập cho 1 năm, rồi dùng công thức tính ra cho 
         * mỗi tháng là bao nhiêu? đưa ra mảng [user_id][type=bò/mối][month] = target
         * 3. tính toán sản lượng thực tế của từng user:
         * dựa vào http://daukhimiennam.com/admin/gasreports/output_daily: qty
         * 4. tính gas gư 
         * 5. kết quả sẽ đưa ra 2 table ?
         */
        // 1. lấy mảng user cần tìm gồm agent_id vs sale_id là biến $model->agent_id
        $AgentAndSaleId = array();
        $aRes = array();
        if( !isset($needMore['agent_view_sale_moi']) && ( Yii::app()->user->role_id==ROLE_SUB_USER_AGENT || Yii::app()->user->role_id==ROLE_SALE) ){
            // chỗ này là cho phép cả sale vs agent xem báo cáo
            // nhưng mà model có sẵn attribute là agent_id nên sẽ dùng chung cho cả sale_id vs agent_id
            $AgentAndSaleId = array(Yii::app()->user->id);
            if(Yii::app()->user->role_id==ROLE_SUB_USER_AGENT){
                $aRes['ROLE_AGENT'] = array(MyFormat::getAgentId());
                $AgentAndSaleId = array(MyFormat::getAgentId());
            }elseif(Yii::app()->user->role_id==ROLE_SALE){
                $aRes['ROLE_SALE'] = $AgentAndSaleId;
            }
            $aRes['ROLE_SALE_MOI_ID'] = array(Yii::app()->user->id);// May 11, 2014 khai báo tạm chưa xử lý gì
            // vì khi nào sale login xem target thì mới xử lý chỗ này, chỉ dành cho sale mối, sale bò thì ở bên trên có rồi
            
        }else{
            $agent_id = Users::getArrIdUserByArrayRole(array(ROLE_AGENT));
//            $sale_id = Users::getArrIdUserByArrayRole(array(ROLE_SALE), 
//                    array('gender'=>Users::SALE_BO)); // ngày 07-30-2014 đóng đoạn này lại, vì cần lấy luôn cho sale mối chuyên viên
            $sale_id = Users::getArrIdUserByArrayRole(array(ROLE_SALE), 
                    array('gender'=> Users::SALE_BO ));
            $sale_id_chuyen_vien_moi = Users::getArrIdUserByArrayRole(array(ROLE_SALE),
                    array('gender'=> array(Users::SALE_MOI_CHUYEN_VIEN) ));
            $aRes['ROLE_AGENT'] = $agent_id;
            $aRes['ROLE_SALE'] = $sale_id;
            $aRes['ROLE_SALE_MOI_CHUYEN_VIEN'] = $sale_id_chuyen_vien_moi;
                        
            $aRes['ROLE_SALE_MOI_ID'] = Users::getArrIdUserByArrayRole(array(ROLE_SALE), 
                    array('gender'=>Users::SALE_MOI));
            // chỗ này array_merge là dùng cho sale bò + đại lý vì có target setup, sale mối không có nên không merge array
            $AgentAndSaleId = array_merge($agent_id, $sale_id, $sale_id_chuyen_vien_moi);
        }
        
        GasTargetMonthly::getTargetEachMonth($AgentAndSaleId, $model->statistic_year, $model->statistic_month,$aRes);

        // target cho sale bò
        if(Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT){
            // nếu là đại lý login thì ko tính target của sale bò, sẽ thừa và gây chậm khi load page
            // khi đại lý login sẽ chạy thống kê này, do đó nên đóng lại
            Statistic::getOutputDailyForTarget($model, 'sale_id',$aRes);
            Statistic::getOutputDailyForTarget($model, 'sale_id',$aRes, array('ROLE_SALE_MOI_CHUYEN_VIEN'=>1));
        }
        
        Statistic::getOutputDailyForTarget($model, 'user_id_create',$aRes, array('gender'=>Users::SALE_BO));// target đại lý
        Statistic::getOutputDailyForTarget($model, 'user_id_create',$aRes, array('gender'=>Users::SALE_MOI));// target đại lý /* Fix Aug 05, 2014 */
        
        Statistic::getOutputDailyForTarget($model, GasTargetMonthly::HGD_REAL ,$aRes);
        
        Statistic::getOutputDailyForTargetBinhBoCuaKhMoi($model, 'user_id_create',$aRes);
        // target cho sale mối
        if(Yii::app()->user->role_id!=ROLE_SUB_USER_AGENT || isset($needMore['agent_view_sale_moi'])){
            // nếu là đại lý login thì ko tính target của sale mối, sẽ thừa và gây chậm khi load page
            Statistic::getOutputDailySaleMoi($model, $aRes);            
        }
        // Lấy info model của SALE VS AGENT
        
//            $aRes['ROLE_SALE_MODEL'] = Users::getArrObjectUserByRole('', $aRes['ROLE_SALE']);// cách cũ bỏ
        $aRes['ROLE_SALE_MODEL'] = Users::getArrObjecByRoleGroupByProvince(ROLE_SALE, array('gender'=>Users::SALE_BO));
        $aRes['ROLE_SALE_MODEL_MOI_CHUYEN_VIEN'] = Users::getArrObjecByRoleGroupByProvince(ROLE_SALE, array('gender'=>Users::SALE_MOI_CHUYEN_VIEN));
        if(isset($aRes['SALE_MOI_UID'])){            
            $aRes['ROLE_SALE_MODEL_MOI'] = Users::getArrObjecByRoleGroupByProvince(
                        ROLE_SALE, array('gender'=>Users::SALE_MOI,
                                'only_id_model'=>1,
                                'aUid'=>$aRes['SALE_MOI_UID'] ));
        }
        
        if(isset($aRes['ROLE_AGENT']) && count($aRes['ROLE_AGENT'])){
            $aRes['ROLE_AGENT_MODEL'] = Users::getArrObjecByRoleGroupByProvince(ROLE_AGENT, array('gender'=>Users::IS_AGENT));
        }     
        
        // sort cho sale mối
        if(isset($aRes['SALE_MOI_SORT_TOTAL']) && count($aRes['SALE_MOI_SORT_TOTAL'])){
            arsort($aRes['SALE_MOI_SORT_TOTAL']);
        }        
        
        $_SESSION['data-excel'] = $aRes;
        return $aRes;    
        
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }                 
    }
    
    /**
     * @Author: ANH DUNG Mar 26, 2014
     * @Todo: thống kê daily sản lượng KH bình bò, mối theo từng đại lý vs từng sale, theo 1 tháng submit từ dưới lên
     * @param: $model
     * @Return: $aRes: is final array return
     */       
    public static function getOutputDailyForTarget($model, $fieldName, &$aRes, $needMore=array()){
        /* 1. findAll and group by sale id, type customer id là Loại KH bò hoặc mối của từng sale hoặc từng đại lý
         * sau đó tính toán sum sản lượng cho từng sale 
         * 2. tương tự cho user_id_create là agent id
         * 3. lấy id các loại bình từ loại vật tư
         */        
        //  1/ tính toán lượng bình bán thực tế
        foreach( CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT as $material_type_id){
            // Nếu là tính target cho KH hộ GD bình 12
            if($fieldName==GasTargetMonthly::HGD_REAL){
                if($material_type_id!=MATERIAL_TYPE_BINH_12)
                    continue;
            }
            if(isset($needMore['ROLE_SALE_MOI_CHUYEN_VIEN'])){
                if($material_type_id!=MATERIAL_TYPE_BINH_12)
                    continue;
            }
            // Nếu là tính target cho KH hộ GD bình 12
            // $material_type_id là loại vật tư MATERIAL_TYPE_BINHBO_50,// Gas Bình Bò 50 Kg ....
            // $aMaterialId <=> get array id những vật tư có loại là $material_type_id: Gas 12 Kg OR 45kg OR 50
            $aMaterialId = GasMaterials::getArrayIdByMaterialTypeId($material_type_id);

            $criteria = new CDBcriteria();
            $criteria->addInCondition("t.type_in_out", CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC); // xuất bán, bán bộ bình, thế chân
//            if($fieldName==GasTargetMonthly::HGD_REAL){
//            }else{                
//                $criteria->compare("t.type_in_out", STORE_CARD_TYPE_3 );// xuất bán, dùng cho KH Bình bò mối,
//            }
            
            if(count($aMaterialId)){
                $criteria->addInCondition("t.materials_id", $aMaterialId);
            }
            
            if($fieldName=="sale_id" && isset($aRes['ROLE_SALE'])){
                // giới hạn chỉ lấy những id của sale bò, tăng perform
                $ROLE_SALE_ID = $aRes['ROLE_SALE'];
                if(isset($needMore['ROLE_SALE_MOI_CHUYEN_VIEN']) && isset($aRes['ROLE_SALE_MOI_CHUYEN_VIEN']) ){
                    $ROLE_SALE_ID = $aRes['ROLE_SALE_MOI_CHUYEN_VIEN'];
                }
//                $criteria->addInCondition("t.sale_id", $aRes['ROLE_SALE']);
                $criteria->addInCondition("t.sale_id", $ROLE_SALE_ID);
            }

            // ưu tiên thống kê theo ngày
            if(!empty($model->date_delivery)){
//                $criteria->compare('t.date_delivery', MyFormat::dateDmyToYmdForAllIndexSearch($model->date_delivery));
                // May 19, 2014 fix
                $t_date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_delivery);
                $tmpDate = explode('-', $t_date_to);
                if(count($tmpDate)!=3){
                    $tmpDate[0] = date('Y');
                    $tmpDate[1] = date('m');
                }
                $t_date_from = "$tmpDate[0]-$tmpDate[1]-01" ;
                $criteria->addBetweenCondition("t.date_delivery",$t_date_from,$t_date_to);                
            }else{
                $criteria->compare('month(t.date_delivery)', $model->statistic_month);
                $criteria->compare('year(t.date_delivery)', $model->statistic_year);
            }
            $criteria->compare("t.warehouse_new", Users::IS_AGENT_ONLY);
            
            if($fieldName==GasTargetMonthly::HGD_REAL){
                $criteria->compare("t.type_customer", CUSTOMER_HO_GIA_DINH);
                $criteria->select = "sum(qty) as qty, t.user_id_create";
                $criteria->group = "t.user_id_create";
                $mRes = GasStoreCardDetail::model()->findAll($criteria);                 
                if(count($mRes)){
                    foreach($mRes as $item){
                        $aRes[GasTargetMonthly::HGD_REAL][$item->user_id_create] = $item->qty*CmsFormatter::$MATERIAL_VALUE_KG[$material_type_id]; 
//                        if(!isset($aRes[GasTargetMonthly::HGD_REAL][$item->$fieldName][$item->type_customer])){
//                            $aRes[GasTargetMonthly::HGD_REAL][$item->$fieldName][$item->type_customer] = $item->qty*CmsFormatter::$MATERIAL_VALUE_KG[$material_type_id]; 
//                        }else{
//                            $aRes['OUTPUT'][$item->$fieldName][$item->type_customer] += $item->qty*CmsFormatter::$MATERIAL_VALUE_KG[$material_type_id]; 
//                        }
                        // nhân luôn với số KG của loại bình đó
                    }
                }
            }else{
                // begin else if($fieldName==GasTargetMonthly::TAR
                $criteria->addCondition("t.$fieldName<>''");
                $criteria->addInCondition("t.type_customer", CmsFormatter::$aTypeIdBoMoi);
                
                /* Fix Aug 05, 2014 */
                if($fieldName=='user_id_create' && isset($needMore['gender'])){// Aug 05, 2014 chỗ này sửa làm 2 lần find cho 1 cho bò, 1 cho mối
                // vì thống kê target của đại lý không tính của sale chuyên viên và pttt.
                    $criteria->compare('t.type_customer', $needMore['gender']);
                    if($needMore['gender']==Users::SALE_MOI){
                        // lấy mảng sale không đưa vào thống kê của đại lý
                        $aUserSaleNotIn = Users::getArrIdUserByArrayRole(array(ROLE_SALE), array(Users::SALE_MOI_CHUYEN_VIEN,Users::SALE_MOI_PTTT));
                        $criteria->addNotInCondition('t.sale_id', $aUserSaleNotIn);
                    }
                 }
                 /* Fix Aug 05, 2014 */
                 
                $criteria->select = "sum(qty) as qty, t.type_customer, t.$fieldName";
                $criteria->group = "t.$fieldName, t.type_customer"; 
                                
                $mRes = GasStoreCardDetail::model()->findAll($criteria);                 
                if(count($mRes)){
                    foreach($mRes as $item){                                                
                        if(!isset($aRes['OUTPUT'][$item->$fieldName][$item->type_customer])){
                            $aRes['OUTPUT'][$item->$fieldName][$item->type_customer] = $item->qty*CmsFormatter::$MATERIAL_VALUE_KG[$material_type_id]; 
                        }else{
                            $aRes['OUTPUT'][$item->$fieldName][$item->type_customer] += $item->qty*CmsFormatter::$MATERIAL_VALUE_KG[$material_type_id]; 
                        }
                        // nhân luôn với số KG của loại bình đó
                    }
                }
                // end else if($fieldName==GasTargetMonthly::TAR                
            }     

        } // end foreach( CmsFormatter::$MATERIAL_TYPE_BINHBO_12          

        // nếu là gas hộ gd thì ko có gas dư
        if($fieldName==GasTargetMonthly::HGD_REAL){
            return;
        }
        
        if(isset($needMore['ROLE_SALE_MOI_CHUYEN_VIEN'])){
            return; /// nếu là tính cho sale mối chuyên viên thì ko tính gas dư tiếp nữa
        }
        //  2/ tính toán lượng gas dư ***********
        if($fieldName=='user_id_create'){
           $fieldName='agent_id' ; // vì bên bảng gas_gas_remain lưu là agent_id còn table storecard là user_id_create
           if(isset($needMore['gender']) && $needMore['gender'] == Users::SALE_MOI )
                return;
        }
        
        $criteria = new CDBcriteria();
        // ưu tiên thống kê theo ngày
        if(!empty($model->date_delivery)){
//            $criteria->compare('t.date_input', MyFormat::dateDmyToYmdForAllIndexSearch($model->date_delivery));
            $t_date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_delivery);
            $tmpDate = explode('-', $t_date_to);
            if(count($tmpDate)!=3){
                $tmpDate[0] = date('Y');
                $tmpDate[1] = date('m');
            }

            $t_date_from = "$tmpDate[0]-$tmpDate[1]-01" ;
            $criteria->addBetweenCondition("t.date_input",$t_date_from,$t_date_to);            
        }else{
            $criteria->compare('month(t.date_input)', $model->statistic_month);
            $criteria->compare('year(t.date_input)', $model->statistic_year);
        }
        $criteria->addCondition("t.customer_id<>0");
        if($fieldName=="sale_id" && isset($aRes['ROLE_SALE'])){
            $criteria->addInCondition("t.sale_id", $aRes['ROLE_SALE']);
        }        
        $criteria->select = "sum(amount_gas) as amount_gas, t.type_customer, t.$fieldName";
//        $criteria->group = "t.type_customer, t.$fieldName"; // cũ, không hiểu sao lại group by ngược với cái bên trên
        $criteria->group = "t.$fieldName, t.type_customer"; 
        $mRes = GasRemain::model()->findAll($criteria);
        if(count($mRes)){
            foreach($mRes as $item){
                if(!isset($aRes['REMAIN'][$item->$fieldName][$item->type_customer])){
                    $aRes['REMAIN'][$item->$fieldName][$item->type_customer] = $item->amount_gas;
                }else{
                    $aRes['REMAIN'][$item->$fieldName][$item->type_customer] += $item->amount_gas;
                }
//                            $sumOutputRemain = $item->amount_gas;
            }
        }
                    
        //  end 2/ tính toán lượng gas dư *************
        // trừ gas dư ra sản lượng thực tế, kiểu làm cũ => bỏ, sẽ tính toán trừ khi render ở view 
//     $aRes['REAL_OUTPUT'][$uid][$type] = $sumOutput50_45_12-$sumOutputRemain;
    }   
    
    /**
     * @Author: ANH DUNG May 10, 2014
     * @Todo: thống kê daily sản lượng KH bình mối, theo từng sale, theo 1 tháng submit từ dưới lên
     * @param: $model
     * @Return: $aRes: is final array return
     */       
    public static function getOutputDailySaleMoi($model, &$aRes){        
        $aMaterialId = GasMaterials::getArrayIdByMaterialTypeId(MATERIAL_TYPE_BINH_12);
        $criteria = new CDBcriteria();
        $criteria->addInCondition("t.type_in_out", CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC); // xuất bán, bán bộ bình, thế chân
        if(count($aMaterialId)){
            $criteria->addInCondition("t.materials_id", $aMaterialId);
        }
        // ưu tiên thống kê theo ngày
        if(!empty($model->date_delivery)){
//            $criteria->compare('t.date_delivery', MyFormat::dateDmyToYmdForAllIndexSearch($model->date_delivery));
            $t_date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_delivery);
            $tmpDate = explode('-', $t_date_to);
            if(count($tmpDate)!=3){
                $tmpDate[0] = date('Y');
                $tmpDate[1] = date('m');
            }            
            $t_date_from = "$tmpDate[0]-$tmpDate[1]-01" ;
            $criteria->addBetweenCondition("t.date_delivery",$t_date_from,$t_date_to);
        }else{
            $criteria->compare('month(t.date_delivery)', $model->statistic_month);
            $criteria->compare('year(t.date_delivery)', $model->statistic_year);
        }
//        $criteria->addCondition("t.sale_id<>0 AND t.sale_id IS NOT NULL ");
        if(isset($aRes['ROLE_SALE_MOI_ID']))
            $criteria->addInCondition("t.sale_id", $aRes['ROLE_SALE_MOI_ID']);

        $criteria->select = "sum(qty) as qty, t.date_delivery, t.sale_id";
        $criteria->group = "t.date_delivery, t.sale_id";
        $mRes = GasStoreCardDetail::model()->findAll($criteria);                 
        if(count($mRes)){            
            foreach($mRes as $item){
                $aRes['SALE_MOI'][$item->date_delivery][$item->sale_id] = $item->qty; 
                if(!isset($aRes['SALE_MOI_SORT_TOTAL'][$item->sale_id])){
                    $aRes['SALE_MOI_SORT_TOTAL'][$item->sale_id] = $item->qty;
                }else {
                    $aRes['SALE_MOI_SORT_TOTAL'][$item->sale_id] += $item->qty;
                }
                // ĐỂ LẤY MẢNG MODEL CỦA SALE, LÀM SORT NÊN MỚI PHẢI LẤY KIỂU NÀY
                $aRes['SALE_MOI_UID'][$item->sale_id] = $item->sale_id;
            }
        }        
        
    }
    
    /**
     * @Author: ANH DUNG May 16, 2014
     * @Todo: thống kê daily sản lượng KH bình mối lấy bình bò, cả gas dư nữa
     * theo thay đổi May 16, 2014, vì KH mối có thể lấy bình bò, nên tách riêng thống kê b12 riêng vs bình bò
     * @param: $model
     * @Return: $aRes: is final array return
     */       
    public static function getOutputDailyForTargetBinhBoCuaKhMoi($model, $fieldName, &$aRes){
        /* 1. findAll and group by sale id, type customer id là Loại KH bò hoặc mối của từng sale hoặc từng đại lý
         * sau đó tính toán sum sản lượng cho từng sale 
         * 2. tương tự cho user_id_create là agent id
         * 3. lấy id các loại bình từ loại vật tư
         */        
        //  1/ tính toán lượng bình bán thực tế
        foreach( CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT as $material_type_id){
            // Nếu là type bình 12 thì ko tính, còn lại tính hết cho bình 50, 45, 6
            if($material_type_id==MATERIAL_TYPE_BINH_12)
                continue;
            // $material_type_id là loại vật tư MATERIAL_TYPE_BINHBO_50,// Gas Bình Bò 50 Kg ....
            // $aMaterialId <=> get array id những vật tư có loại là $material_type_id: Gas 12 Kg OR 45kg OR 50
            $aMaterialId = GasMaterials::getArrayIdByMaterialTypeId($material_type_id);

            $criteria = new CDBcriteria();
            $criteria->addInCondition("t.type_in_out", CmsFormatter::$TYPE_IN_OUT_OUTPUT_STATISTIC); // xuất bán, bán bộ bình, thế chân           
            if(count($aMaterialId)){
                $criteria->addInCondition("t.materials_id", $aMaterialId);
            }
            
            // ưu tiên thống kê theo ngày
            if(!empty($model->date_delivery)){
//                $criteria->compare('t.date_delivery', MyFormat::dateDmyToYmdForAllIndexSearch($model->date_delivery));
                $t_date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_delivery);
                $tmpDate = explode('-', $t_date_to);
                if(count($tmpDate)!=3){
                    $tmpDate[0] = date('Y');
                    $tmpDate[1] = date('m');
                }                
                $t_date_from = "$tmpDate[0]-$tmpDate[1]-01" ;
                $criteria->addBetweenCondition("t.date_delivery",$t_date_from,$t_date_to);                
            }else{
                $criteria->compare('month(t.date_delivery)', $model->statistic_month);
                $criteria->compare('year(t.date_delivery)', $model->statistic_year);
            }
         
            $criteria->addCondition("t.$fieldName<>''");
//            $criteria->addCondition("t.type_customer<>0");
            $criteria->compare("t.type_customer", STORE_CARD_KH_MOI);
            $criteria->compare("t.warehouse_new", Users::IS_AGENT_ONLY);
            $criteria->select = "sum(qty) as qty, t.type_customer, t.$fieldName";
            $criteria->group = "t.$fieldName, t.type_customer";         

            $mRes = GasStoreCardDetail::model()->findAll($criteria);                 
            if(count($mRes)){
                foreach($mRes as $item){
                    if(!isset($aRes['BINH_BO_KH_MOI'][$item->$fieldName])){
                        $aRes['BINH_BO_KH_MOI'][$item->$fieldName][$item->type_customer] = $item->qty*CmsFormatter::$MATERIAL_VALUE_KG[$material_type_id]; 
                    }else{
                        $aRes['BINH_BO_KH_MOI'][$item->$fieldName][$item->type_customer] += $item->qty*CmsFormatter::$MATERIAL_VALUE_KG[$material_type_id]; 
                    }
                    // nhân luôn với số KG của loại bình đó
                }
            }
            // end else if($fieldName==GasTargetMonthly::TAR                

        } // end foreach( CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT          
    }   
        
    
    /**
     * @Author: ANH DUNG Apr 04, 2014
     * @Todo: Thống kê Xác định bình quay về của PTTT
     * @Param: $model model GasMaintainSell chứa điều kiện search
     */
    public static function DetermineGasGoBack($model){
        try{
        $aRes = array();
        $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
        $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);
        
        self::getTotalGoBack($model , $aRes, 'TOTAL_ALL', $date_from, $date_to);
        self::getTotalGoBack($model , $aRes, 'TOTAL_USING_GAS_HM', $date_from, $date_to);
        self::getTotalGoBack($model , $aRes, 'TOTAL_QTY_BIGGER_2', $date_from, $date_to);
        self::getTotalGoBack($model , $aRes, 'TOTAL_QTY_BIGGER_2_COUNT_CUSTOMER', $date_from, $date_to);
        return $aRes;
        
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }             
    }

    /** belong to DetermineGasGoBack
     * @Todo: lấy tổng bình quay về của NV PTTT theo đại lý
     * @param: $NAME_VAR: tên biến toàn cục cho từng loại thống kê
     */    
    public static function getTotalGoBack($model , &$aRes, $NAME_VAR, $date_from, $date_to){
        $criteria=new CDbCriteria;
        $criteria->compare('t.type', TYPE_MARKET_DEVELOPMENT);
        if($model->maintain_employee_id)
            $criteria->compare('t.maintain_employee_id',$model->maintain_employee_id);
        if($model->agent_id)
            $criteria->compare('t.agent_id', $model->agent_id);
        
        if(!empty($date_from) && !empty($date_to))
            $criteria->addBetweenCondition("t.date_sell",$date_from,$date_to);                 
        elseif(!empty($date_from) && empty($date_to))
            $criteria->addCondition("t.date_sell>='$date_from'");
        elseif(empty($date_from) && !empty($date_to))
            $criteria->addCondition("t.date_sell<='$date_to'");
        
        if($NAME_VAR=='TOTAL_USING_GAS_HM'){
            $criteria->compare('t.using_gas_huongminh', 1);
        }        
        $criteria->addCondition('t.maintain_employee_id<>""');
                
        $criteria->select = "t.agent_id, t.maintain_employee_id,"
                    . " sum(quantity_sell) as quantity_sell";
        $criteria->group = "t.maintain_employee_id, t.agent_id"; 
        if($NAME_VAR=='TOTAL_ALL'){
            $criteria->order = "quantity_sell DESC"; 
        }
        
        if($NAME_VAR=='TOTAL_QTY_BIGGER_2'){
            $criteria->compare('t.using_gas_huongminh', 0);
            $criteria->group = "t.maintain_employee_id, t.agent_id, t.customer_id"; 
            $criteria->having = "quantity_sell>1"; 
        } 
        
        if($NAME_VAR=='TOTAL_QTY_BIGGER_2_COUNT_CUSTOMER'){
            $criteria->compare('t.using_gas_huongminh', 0);
            $criteria->select = "t.agent_id, t.maintain_employee_id, t.customer_id,"
                    . " sum(quantity_sell) as quantity_sell";            
            $criteria->group = "t.maintain_employee_id, t.agent_id, t.customer_id"; 
            $criteria->having = "quantity_sell>1"; 
        }  
        
        // có thể sử dụng chung hàm này cho thống kê tiếp
        
        $mRes = GasMaintainSell::model()->findAll($criteria);
        if(count($mRes)){
            foreach($mRes as $item){
                if($NAME_VAR=='TOTAL_QTY_BIGGER_2_COUNT_CUSTOMER'){
                    if(!isset($aRes[$NAME_VAR][$item->maintain_employee_id][$item->agent_id])){
                        $aRes[$NAME_VAR][$item->maintain_employee_id][$item->agent_id]=1;
                    }
                    else{
                        $aRes[$NAME_VAR][$item->maintain_employee_id][$item->agent_id]+=1;
                    }
                    $aRes['CUSTOMER_ID_BIGGER_2_COUNT'][] = $item->customer_id; // mảng này để find 1 lần tất cả model của customer
                    $aRes['ARR_CUSTOMER_ID'][$item->maintain_employee_id][$item->agent_id][] = $item->customer_id;// mảng này để hiện thông tin của KH cho từng dòng
                }else{
                    if(!isset($aRes[$NAME_VAR][$item->maintain_employee_id][$item->agent_id]))
                        $aRes[$NAME_VAR][$item->maintain_employee_id][$item->agent_id] = $item->quantity_sell;
                    else
                        $aRes[$NAME_VAR][$item->maintain_employee_id][$item->agent_id] += $item->quantity_sell;
                }
                
            }
        }        
    }
    
    /**
     * @Author: ANH DUNG May 21, 2014
     * @Todo: bc daily nội bộ - admin/gasreports/DailyInternal
     * @Param: $model model GasStoreCard chứa điều kiện thống kê
     */
    public static function DailyInternal($model){
        try{
        $aRes = array();
        $criteria = new CDBcriteria();
        // ưu tiên thống kê theo ngày
        $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_from);
        $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);
        $criteria->addBetweenCondition("t.date_delivery",$date_from,$date_to);
        if(!empty($model->ext_type_customer)){
            if($model->ext_type_customer==CUSTOMER_HO_GIA_DINH){
                $criteria->addCondition('( t.type_customer='.CUSTOMER_HO_GIA_DINH.' ) AND ( t.customer_id=0  OR t.customer_id IS NULL )');                
            }elseif($model->ext_type_customer==CUSTOMER_OTHER){
                $criteria->addCondition('( t.type_customer=0  OR t.type_customer IS NULL ) AND ( t.customer_id<>0)');
            }else{
                $criteria->compare('t.type_customer', $model->ext_type_customer);
            }
        }
        
        if($model->ext_type_gas_vo ==  GasStoreCard::TYPE_VO){// thống kê nhập xuất vỏ
            $criteria->addInCondition('t.materials_type_id', CmsFormatter::$MATERIAL_TYPE_BINHBO_INPUT);
        }elseif($model->ext_type_gas_vo ==  GasStoreCard::TYPE_GAS){// thống kê nhập xuất gas
            $criteria->addInCondition('t.materials_type_id', CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT);
        }elseif($model->ext_type_gas_vo ==  GasStoreCard::TYPE_NOT_GAS_VO){ // is self::TYPE_NOT_GAS_VO
            $aNotIn = array_merge(CmsFormatter::$MATERIAL_TYPE_BINHBO_INPUT, CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT);
            $criteria->addNotInCondition('t.materials_type_id', $aNotIn);
        }
        
        if(is_array($model->type_in_out) && count($model->type_in_out)){
            $criteria->addInCondition('t.type_in_out', $model->type_in_out);
        }
        
        $criteria->compare('t.user_id_create', $model->agent_id);
        if(!empty($model->ext_materials_id)){
            $criteria->compare('t.materials_id', $model->ext_materials_id);
        }
        if(!empty($model->ext_materials_type_id)){
            $criteria->compare('t.materials_type_id', $model->ext_materials_type_id);
        }
        if(!empty($model->customer_id)){
            $criteria->compare('t.customer_id', $model->customer_id);
        }
        
        $criteria->select = "sum(qty) as qty, t.customer_id, t.type_store_card, "
            . "t.type_in_out, t.date_delivery,t.materials_id, t.materials_type_id, t.user_id_create,t.delivery_person";
        $criteria->group = "t.date_delivery, t.customer_id, t.materials_id, t.type_store_card";         
        $criteria->order = 't.date_delivery ASC';
        
        $mRes = GasStoreCardDetail::model()->findAll($criteria);                 
        if(count($mRes)){
            foreach($mRes as $item){
                $aRes['DATA'][$item->date_delivery][$item->customer_id][$item->materials_id][$item->type_store_card] = $item; 
                // vì đã sum qty ở trên rồi nên sẽ không phải cộng dồn nữa, mà gắn thẳng $item model vào luôn
                $aRes['CUSTOMER_ID'][$item->customer_id] = $item->customer_id;
            }
        }
        
        $aRes['CUSTOMER_MODEL'] = array();
        if(isset($aRes['CUSTOMER_ID']) && count($aRes['CUSTOMER_ID'])){
            $aRes['CUSTOMER_MODEL'] = Users::getArrObjectUserByRole('', $aRes['CUSTOMER_ID']);
        }        
        $aRes['MATERIAL_MODEL'] = GasMaterials::getArrAllObjMaterial();
        $aRes['ARR_DAYS'] = MyFormat::getArrayDay($date_from, $date_to);
        return $aRes;
        
        }catch (Exception $e)
        {
            Yii::log("Statistic::AgentNXT(); Uid: " .Yii::app()->user->id. "Exception ".  $e->getMessage(), 'error');
            $code = 404;
            if(isset($e->statusCode))
                $code=$e->statusCode;
            if($e->getCode())
                $code=$e->getCode();
            throw new CHttpException($code, $e->getMessage());
        }         
        
    }
    
    /**
     * @Author: ANH DUNG Jun 08, 2014
     * @Todo: báo cáo nhập xuất tồn của đại lý, index sau khi login
     */
    public static function AgentNXT(){
    try{
        $aRes = array();
        $date = date('Y-m-d');
        if(isset($_GET['date_nxt'])){
            $date = MyFormat::dateDmyToYmdForAllIndexSearch($_GET['date_nxt']);
        }
            
        $agent_id = MyFormat::getAgentId();
        $list_agent_id = Users::getArrIdAgentNotWarehouse();
        
        // 1. ************** Bình gas ****************
        // 1.1 get all material id nhập gas gom theo đại lý
        // 1.2 get all material id xuất gas gom theo loại KH: hộ gd, bò, mối, đại lý khác, khác
        self::AgentNXTGetDetail($aRes, 'GAS_INPUT', TYPE_STORE_CARD_IMPORT, $date, $agent_id, $list_agent_id);
        self::AgentNXTGetDetail($aRes, 'GAS_OUTPUT', TYPE_STORE_CARD_EXPORT, $date, $agent_id, $list_agent_id);
        // 2. ************** Vỏ Bình gas **************
        // 2.1 get all material id vỏ  gom theo loại KH: hộ gd, bò, mối, đại lý khác, khác
        // 2.2 get all material id vỏ gom theo đại lý
        self::AgentNXTGetDetail($aRes, 'VO_INPUT', TYPE_STORE_CARD_IMPORT, $date, $agent_id, $list_agent_id);
        self::AgentNXTGetDetail($aRes, 'VO_OUTPUT', TYPE_STORE_CARD_EXPORT, $date, $agent_id, $list_agent_id);
        // 3. ************** Vật tư & quà tặng **********
        // 3.1 get all material id vật tư & quà tặng gom theo type_in_out loại nhập xuất
        // 3.2 get all material id vật tư & quà tặng gom theo type_in_out loại nhập xuất
        self::AgentNXTGetDetail($aRes, 'VT_GIFT_INPUT', TYPE_STORE_CARD_IMPORT, $date, $agent_id, $list_agent_id);
        self::AgentNXTGetDetail($aRes, 'VT_GIFT_OUTPUT', TYPE_STORE_CARD_EXPORT, $date, $agent_id, $list_agent_id);
         
        // 4. ************** tính tồn đầu kỳ cho các material id ở trên **************
        $allMaterialId = GasMaterials::getArrayIdByMaterialTypeId('', array('order'=>'materials_type_id ASC'));
        $aRes['OPENING_BALANCE_YEAR_BEFORE'] = MyFunctionCustom::getOpeningBalanceArrMaterialYearBefore($agent_id, $allMaterialId);
        
        $aRes['UNIQUE_MATERIAL_ID'] = $allMaterialId;
        $date_from = date('Y')."-01-01"; // hỗ trợ xem trong năm
        if($date_from!=$date){
            $date_to = MyFormat::modifyDays($date, 1, '-'); // nên trừ đi 1 ngày
            // 4.1 Tính dư đầu kỳ cho mảng vật tư
            // 4.2 Tính tổng nhập trong khoảng ngày $date_from, $date_to cho mảng vật tư
            $aRes['IMPORT_IN_YEAR'] = MyFunctionCustom::calcTotalExportImportInRangeDate($agent_id, $aRes['UNIQUE_MATERIAL_ID'], $date_from, $date_to, TYPE_STORE_CARD_IMPORT);
            // 4.3 Tính tổng xuất trong khoảng ngày $date_from, $date_to cho mảng vật tư
            $aRes['EXPORT_IN_YEAR'] = MyFunctionCustom::calcTotalExportImportInRangeDate($agent_id, $aRes['UNIQUE_MATERIAL_ID'], $date_from, $date_to, TYPE_STORE_CARD_EXPORT);
        }else{ // xử lý cho ngày đầu năm mới: vd 01-01-2015
            // chỗ này không cần else nữa vì dưới view đã kiểm tra isset($AGENT_NXT['IMPORT_IN_YEAR'])?$AGENT_NXT['IMPORT_IN_YEAR']:array();
        }
        
        // 5 ************** lấy object model các material id ở trên **************
        $aRes['MATERIAL_MODEL'] = GasMaterials::getArrayModel($allMaterialId);
        $aRes['MATERIAL_GAS_FOREACH'] = GasMaterials::getArrayIdByMaterialTypeId(CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT, array('order'=>'materials_type_id ASC'));
        $aRes['MATERIAL_VO_FOREACH'] = GasMaterials::getArrayIdByMaterialTypeId(CmsFormatter::$MATERIAL_TYPE_BINHBO_INPUT, array('order'=>'materials_type_id ASC'));
        $aRes['MATERIAL_VT_GIFT_FOREACH'] = GasMaterials::getArrayIdByMaterialTypeId(
                    '', array('order'=>'materials_type_id ASC',
                        'NOT_IN_1'=>CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT,
                        'NOT_IN_2'=>CmsFormatter::$MATERIAL_TYPE_BINHBO_INPUT,
                        ));
        
        $aRes['WAREHOUSE_MODEL'] = Users::getArrObjectUserByRole('', CmsFormatter::$LIST_WAREHOUSE_ID);
        return $aRes;
        
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }             
    }
    
    /**
     * @Author: ANH DUNG Jun 15, 2014
     * @Todo: display full name of user
     * @Param: $aRes FINAL RETURN
     * @Param: $NAME_VAR: GAS_INPUT, GAS_OUTPUT, VO_INPUT, VO_OUTPUT, VT_GIFT_INPUT, VT_GIFT_OUTPUT
     * @Param: $type_store_card 1: Nhập, 2: xuất
     * @Param: $date 2014-08-06
     * @Param: $agent_id is current agent statistic
     * @Param: $list_agent_id array(id=>id)
     */
    public static function AgentNXTGetDetail(&$aRes, $NAME_VAR, $type_store_card, $date, $agent_id, $list_agent_id){
        $criteria=new CDbCriteria;
        $criteria->compare('t.type_store_card', $type_store_card);
        $criteria->compare('t.user_id_create', $agent_id);
        $criteria->compare('t.date_delivery', $date);
                
        
        if($NAME_VAR=='GAS_INPUT'){ // nhập  gas
            $criteria->addInCondition('t.materials_type_id', CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT);
            $criteria->select = "t.customer_id, t.materials_type_id, t.materials_id,"
                    . " sum(qty) as qty";
            $criteria->group = "t.customer_id, t.materials_type_id, t.materials_id";
            $mRes = GasStoreCardDetail::model()->findAll($criteria);
            foreach ($mRes as $item){
                $aRes[$NAME_VAR][$item->customer_id][$item->materials_id]=$item->qty;
                $aRes['GAS_FOREACH'][$item->materials_type_id][$item->materials_id] = $item->materials_id;
                $aRes['UNIQUE_MATERIAL_ID'][$item->materials_id] = $item->materials_id;
                // UNIQUE_MATERIAL_ID dùng để tính tồn đầu và lấy list model
                // nếu không nằm trong mấy kho kia thì sẽ cộng vào Nhập Đại lý khác và KHÁC
                if(!in_array($item->customer_id, CmsFormatter::$LIST_WAREHOUSE_ID)){
                    if(in_array($item->customer_id, $list_agent_id)){ // đại lý khác
                        if(!isset($aRes[$NAME_VAR]['OTHER_AGENT'][$item->materials_id]))
                            $aRes[$NAME_VAR]['OTHER_AGENT'][$item->materials_id] = $item->qty;
                        else
                            $aRes[$NAME_VAR]['OTHER_AGENT'][$item->materials_id] += $item->qty;
                    }else{ // đối tượng khác có thể là KH 
                        if(!isset($aRes[$NAME_VAR]['OTHER_OBJ'][$item->materials_id]))
                            $aRes[$NAME_VAR]['OTHER_OBJ'][$item->materials_id] = $item->qty;
                        else
                            $aRes[$NAME_VAR]['OTHER_OBJ'][$item->materials_id] += $item->qty;                        
                    }
                }
            }
        }

        if($NAME_VAR=='GAS_OUTPUT'){ //  xuất gas
            $criteria->addInCondition('t.materials_type_id', CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT);
            $criteria->select = "t.type_customer, t.materials_type_id, t.materials_id,"
                    . " sum(qty) as qty";
            $criteria->group = "t.type_customer, t.materials_type_id, t.materials_id";
            $mRes = GasStoreCardDetail::model()->findAll($criteria);
            foreach ($mRes as $item){
                $aRes[$NAME_VAR][$item->type_customer][$item->materials_id]=$item->qty;
                $aRes['GAS_FOREACH'][$item->materials_type_id][$item->materials_id] = $item->materials_id;
                $aRes['UNIQUE_MATERIAL_ID'][$item->materials_id] = $item->materials_id;
            }
        }elseif($NAME_VAR=='VO_INPUT'){ // nhập  Vỏ
            $criteria->addInCondition('t.materials_type_id', CmsFormatter::$MATERIAL_TYPE_BINHBO_INPUT);
            $criteria->select = "t.type_customer, t.materials_type_id, t.materials_id,"
                    . " sum(qty) as qty";
            $criteria->group = "t.type_customer, t.materials_type_id, t.materials_id";
            $mRes = GasStoreCardDetail::model()->findAll($criteria);
            foreach ($mRes as $item){
                $aRes[$NAME_VAR][$item->type_customer][$item->materials_id]=$item->qty;
                $aRes['VO_FOREACH'][$item->materials_type_id][$item->materials_id] = $item->materials_id;
                $aRes['UNIQUE_MATERIAL_ID'][$item->materials_id] = $item->materials_id;
            }
        }elseif($NAME_VAR=='VO_OUTPUT'){ // XUẤT  Vỏ
            $criteria->addInCondition('t.materials_type_id', CmsFormatter::$MATERIAL_TYPE_BINHBO_INPUT);
            $criteria->select = "t.customer_id, t.materials_type_id, t.materials_id,"
                    . " sum(qty) as qty";
            $criteria->group = "t.customer_id, t.materials_type_id, t.materials_id";
            $mRes = GasStoreCardDetail::model()->findAll($criteria);
            foreach ($mRes as $item){
                $aRes[$NAME_VAR][$item->customer_id][$item->materials_id]=$item->qty;
                $aRes['VO_FOREACH'][$item->materials_type_id][$item->materials_id] = $item->materials_id;
                $aRes['UNIQUE_MATERIAL_ID'][$item->materials_id] = $item->materials_id;
                // nếu không nằm trong mấy kho kia thì sẽ cộng vào Nhập Đại lý khác và KHÁC
                if(!in_array($item->customer_id, CmsFormatter::$LIST_WAREHOUSE_ID)){
                     // đối tượng khác có thể là KH 
                    if(!isset($aRes[$NAME_VAR]['OTHER_OBJ'][$item->materials_id]))
                        $aRes[$NAME_VAR]['OTHER_OBJ'][$item->materials_id] = $item->qty;
                    else
                        $aRes[$NAME_VAR]['OTHER_OBJ'][$item->materials_id] += $item->qty;                    
                }                
            }
        }elseif($NAME_VAR=='VT_GIFT_INPUT'){ // nhập  Vật tư & quà tặng
            $criteria->addNotInCondition('t.materials_type_id', CmsFormatter::$MATERIAL_TYPE_BINHBO_INPUT);
            $criteria->addNotInCondition('t.materials_type_id', CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT);
            $criteria->addInCondition('t.type_in_out', CmsFormatter::$NXT_VT_GIFT_INPUT);
            $criteria->select = "t.type_in_out, t.materials_type_id, t.materials_id,"
                    . " sum(qty) as qty";
            $criteria->group = "t.type_in_out, t.materials_type_id, t.materials_id";
            $mRes = GasStoreCardDetail::model()->findAll($criteria);
            foreach ($mRes as $item){
                $aRes[$NAME_VAR][$item->type_in_out][$item->materials_id]=$item->qty;
                $aRes['VT_GIFT_FOREACH'][$item->materials_type_id][$item->materials_id] = $item->materials_id;
                $aRes['UNIQUE_MATERIAL_ID'][$item->materials_id] = $item->materials_id;
            }
        }elseif($NAME_VAR=='VT_GIFT_OUTPUT'){ // xuất  Vật tư & quà tặng
            $criteria->addNotInCondition('t.materials_type_id', CmsFormatter::$MATERIAL_TYPE_BINHBO_INPUT);
            $criteria->addNotInCondition('t.materials_type_id', CmsFormatter::$MATERIAL_TYPE_BINHBO_OUTPUT);
            $criteria->addInCondition('t.type_in_out', CmsFormatter::$NXT_VT_GIFT_OUTPUT);
            $criteria->select = "t.type_in_out, t.materials_type_id, t.materials_id,"
                    . " sum(qty) as qty";
            $criteria->group = "t.type_in_out, t.materials_type_id, t.materials_id";
            $mRes = GasStoreCardDetail::model()->findAll($criteria);
            foreach ($mRes as $item){
                $aRes[$NAME_VAR][$item->type_in_out][$item->materials_id]=$item->qty;
                $aRes['VT_GIFT_FOREACH'][$item->materials_type_id][$item->materials_id] = $item->materials_id;
                $aRes['UNIQUE_MATERIAL_ID'][$item->materials_id] = $item->materials_id;
            }
        }
        
        
    }
    
    /**
     * @Author: ANH DUNG Jun 28, 2014
     * @Todo: tính tồn kho toàn hệ thống của từng vật tư theo từng đại lý
     * @Param: $model model store card chứa thông tin điều kiện search
     */
    public static function Inventory($model){
        try{
        // 1. tính tồn đầu cho tất cả các loại vật tư
        // 2. tính tổng nhập, xuất của các loại vật tư theo từng đại lý
        /* 1. */
        $aRes['OPENING_BALANCE_YEAR_BEFORE'] = MyFunctionCustom::getOpeningBalanceGroupByAgent($model->agent_id, '', $model->ext_materials_type_id);
        $date_from = date('Y-').'01-01';// từ đầu năm, hiện tại hỗ trợ xem ngày trong năm, không cho chọn ngày của năm trước
        $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($model->date_to);
        /* 2. */
        $aRes['IMPORT'] = MyFunctionCustom::sumTotalExportImportGroupByAgent($model->agent_id, $date_from, $date_to, TYPE_STORE_CARD_IMPORT);
        $aRes['EXPORT'] = MyFunctionCustom::sumTotalExportImportGroupByAgent($model->agent_id, $date_from, $date_to, TYPE_STORE_CARD_EXPORT);
        $aRes['MODEL_MATERIAL_TYPE'] = GasMaterials::getArrayModelMaterialType($model->ext_materials_type_id);        
        return $aRes;
        
        }catch (Exception $e)
        {
            MyFormat::catchAllException($e);
        }
    }
    
}
