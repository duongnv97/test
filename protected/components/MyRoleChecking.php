<?php

/**
 * Created by PhpStorm.
 * User: commitflame
 * Date: 7/18/2016
 * Time: 10:10 AM
 */
class MyRoleChecking
{
    public static $cRole;

    /**
     * @Author: TRUNG Jul 18 2016
     *
     * @return bool
     */
    public static function isRoleAdmin()
    {
        if (!isset(MyRoleChecking::$cRole)) {
            $cRole = Yii::app()->user->role_id;
        }

        return $cRole == ROLE_ADMIN;
    }

    /**
     * @Author: TRUNG Jul 18 2016
     *
     * @return bool
     */
    public static function isRoleDieuPhoi()
    {
        if (!isset(MyRoleChecking::$cRole)) {
            $cRole = Yii::app()->user->role_id;
        }

        return $cRole == ROLE_DIEU_PHOI;
    }
}