<?php
Yii::import('zii.widgets.CPortlet');
class CalendarJudoing extends CPortlet
{
    private $_assetsUrl;

    public $fullname;

    public $events;
    public $value;

    public function init()
    {
        parent::init();
    }

    public function renderContent()
    {
        if(Yii::app()->user->isClinic){
            $events = Jobs::model()->findAll(array('condition'=>'clinic_id ='.Yii::app()->user->id,'order'=>'t.from ASC'));
        } else {
            $events = Jobs::model()->findAll(array('order'=>'t.from ASC'));
        }
        $this->render('calendar',array('events'=>$events));
    }

    public function getAssetsUrl()
    {
        if ($this->_assetsUrl === null)
            $this->_assetsUrl = Yii::app()->getAssetManager()->publish(
                Yii::getPathOfAlias('ext.mywidget.assets'), true, -1, true );
        return $this->_assetsUrl;
    }
}