
<style type="text/css">
	div.day-number	 { 
	background:#999; 
	position:absolute; 
	z-index:2; 
	top:-5px; 
	right:-25px; 
	padding:5px; 
	color:#fff; 
	font-weight:bold; 
	width:20px; 
	text-align:center; 
}
td.calendar-day, td.calendar-day-np { 
	width:120px; 
	padding:5px 25px 5px 5px; 
	border-bottom:1px solid #999; 
	border-right:1px solid #999; 
}

</style>

<?php
/* draws a calendar */
function draw_calendar($month,$year,$events = array()){

	/* draw table */
	$calendar = '<table cellpadding="0" cellspacing="0" class="calendar">';

	/* table headings */
	$headings = array('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
	$calendar.= '<tr><td class="title">'.implode('</td><td class="title">',$headings).'</td></tr>';

	/* days and weeks vars now ... */
	$running_day = date('w',mktime(0,0,0,$month,1,$year));
	$days_in_month = date('t',mktime(0,0,0,$month,1,$year));
	$days_in_this_week = 1;
	$day_counter = 0;
	$dates_array = array();

	/* row for week one */
	$calendar.= '<tr class="calendar-row">';

	/* print "blank" days until the first of the current week */
	for($x = 0; $x < $running_day; $x++):
		$calendar.= '<td>&nbsp;</td>';
		$days_in_this_week++;
	endfor;

	/* keep going with days.... */
	for($list_day = 1; $list_day <= $days_in_month; $list_day++):
		$calendar.= '<td><div class="column">';
			/* add in the day number */
            if(Yii::app()->user->isClinic){
                $list_date = date ('Y-m-d',strtotime($year .'-'. $month .'-'. $list_day));
                if($list_date < date('Y-m-d')){
                    $calendar.= '<div class="even">'.$list_day.'</div>';
                } else {
                    $addJobLink = Yii::app()->createAbsoluteUrl("/member/jobs/clinicAddJobs/jobDate/" . strtotime(date("Y-m-d", strtotime($year .'-'. $month .'-'. $list_day)))) ;
                    $calendar.= '<div class="even">'.$list_day.'<a href="'.$addJobLink.'"><img src="' . Yii::app()->theme->baseUrl . '/images/add.gif" width="12" height="12" alt="add" class="add_btn" /></a></div>';
                }
            } else {
                $calendar.= '<div class="even">'.$list_day.'</div>';
            }
			
			$event_day = date ('Y-m-d',strtotime($year.'-'.$month.'-'.$list_day));

				foreach($events as $event) {
                    $start_event_date = date ('Y-m-d',strtotime($event['from']));
                    $start_event_hour = date ('H:i',strtotime($event['from']));
                    $end_event_hour = date ('H:i',strtotime($event['to']));
                    if($start_event_date == $event_day){
                        if(Yii::app()->user->isClinic){
                            if($event['to']< date('Y-m-d H:i:s')){
                                $calendar.= '<div class="blocked"><a href="'.Yii::app()->createAbsoluteUrl('/member/jobs/viewJob/id/'.$event['id']).'">'.$start_event_hour.' - '.$end_event_hour. '</a></div>';
                            }else {
                                if($event['urgent'] == 1 && (int)$event["doctor_id"] == 0){
                                    $calendar.= '<div class="urgent"><a href="'.Yii::app()->createAbsoluteUrl('/member/jobs/viewJob/id/'.$event['id']).'">'.$start_event_hour.' - '.$end_event_hour. '</a></div>';
                                }elseif ((int)$event["doctor_id"] == 0){
                                    $calendar.= '<div class="available"><a href="'.Yii::app()->createAbsoluteUrl('/member/jobs/viewJob/id/'.$event['id']).'">'.$start_event_hour.' - '.$end_event_hour. '</a></div>';
                                } else{
                                    $calendar.= '<div class="blocked"><a href="'.Yii::app()->createAbsoluteUrl('/member/jobs/viewJob/id/'.$event['id']).'">'.$start_event_hour.' - '.$end_event_hour. '</a></div>';
                                }
                            }
                        }
                        if(Yii::app()->user->isDoctor){
                            if($event['to'] < date('Y-m-d H:i:s')){
                                $calendar.= '<div class="blocked">'.$start_event_hour.' - '.$end_event_hour. '</a></div>';
                            }
                            else    
                            {
                            	if($event['urgent'] == 1 && (int)$event["doctor_id"] == 0){
                                	$calendar.= '<div class="urgent"><a href="'.Yii::app()->createAbsoluteUrl('/member/jobs/bookJob/id/'.$event['id']).'">'.$start_event_hour.' - '.$end_event_hour. '</a></div>';
                            	}elseif((int)$event["doctor_id"] == 0){
                                	$calendar.= '<div class="available"><a href="'.Yii::app()->createAbsoluteUrl('/member/jobs/bookJob/id/'.$event['id']).'">'.$start_event_hour.' - '.$end_event_hour. '</a></div>';
                            	}
                               	else
                               	{
                               		$calendar.= '<div class="blocked">'.$start_event_hour.' - '.$end_event_hour. '</a></div>';
                               	}	
                            }
                        }
                    } else {
                        $calendar.= str_repeat('',2);
                    }
				}


		$calendar.= '</div></td>';
		if($running_day == 6):
			$calendar.= '</tr>';
			if(($day_counter+1) != $days_in_month):
				$calendar.= '<tr>';
			endif;
			$running_day = -1;
			$days_in_this_week = 0;
		endif;
		$days_in_this_week++; $running_day++; $day_counter++;
	endfor;

	/* finish the rest of the days in the week */
	if($days_in_this_week < 8):
		for($x = 1; $x <= (8 - $days_in_this_week); $x++):
			$calendar.= '<td>&nbsp;</td>';
		endfor;
	endif;

	/* final row */
	$calendar.= '</tr>';
	

	/* end the table */
	$calendar.= '</table>';

	/** DEBUG **/
	$calendar = str_replace('</td>','</td>'."\n",$calendar);
	$calendar = str_replace('</tr>','</tr>'."\n",$calendar);
	
	/* all done, return result */
	return $calendar;
}

function random_number() {
	srand(time());
	return (rand() % 7);
}

/* date settings */
if(isset($_GET['month'])) $month = (int)$_GET['month'];
else $month = date('m');

if(isset($_GET['year'])) $year = (int)$_GET['year'];
else $year = date('y');


/* "next month" control */
$next_month_link = '<a href="?month='.($month != 12 ? $month + 1 : 1).'&year='.($month != 12 ? $year : $year + 1).'" class="right"><img src="'.Yii::app()->theme->baseUrl.'/images/next_mth.gif" width="18" height="23" alt="prev" class="left" border="0"/></a>';

/* "previous month" control */
$previous_month_link = '<a href="?month='.($month != 1 ? $month - 1 : 12).'&year='.($month != 1 ? $year : $year - 1).'" class="left"><img src="'.Yii::app()->theme->baseUrl.'/images/prev_mth.gif" width="18" height="23" alt="prev" class="left" border="0"/></a>';


/* bringing the controls together */
$controls = '<div class="month_pick"><form method="get"><h1>'.$previous_month_link.''.date('F',mktime(0,0,0,$month,1,$year)).' '.$year.''.$next_month_link.'</h1> </form></div>';

/* get all events for the given month */

echo '<div class="table_wrap">'.$controls.'</div>';
echo '<div style="clear:both;"></div>';
echo draw_calendar($month,$year,$events);
echo '<br /><br />';
?>