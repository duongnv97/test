<div class="WrapAutocompleteStreet float_l">
<?php 
if(!isset($data['field_autocomplete_name']))
    $data['field_autocomplete_name'] = 'autocomplete_material_name';
$data['class_name'] = get_class($data['model']);
$idField = "#".$data['class_name']."_".$data['field_autocomplete_name'];

if(!isset($data['field_street_id']))
    $data['field_street_id'] = 'street_id';
$idFieldStreet = "#".$data['class_name']."_".$data['field_street_id'];

if(!isset($data['width']))
    $data['width'] = '360px;';
$readonly='';
$value='';
if(($data['model']->{$data['NameRelation']})){
    $value = $data['model']->{$data['NameRelation']}->name;
    $readonly=1;
}
$ClassAdd = '';
if(isset($data['ClassAdd']))
    $ClassAdd = $data['ClassAdd'];

?> 

<?php 
$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
        'attribute' => $data['field_autocomplete_name'],
        'model'     => $data['model'],
        'sourceUrl' => $data['url'],
        'options'=>array(
                'minLength'=>MIN_LENGTH_AUTOCOMPLETE,
                'multiple'=> true,
                'search'=>"js:function( event, ui ) {
                        $('$idField').addClass('grid-view-loading-gas');
                        } ",
                'response'=>"js:function( event, ui ) {
                            var parent_div = $('$idField').closest('div.WrapAutocompleteStreet');
                            var json = $.map(ui, function (value, key) { return value; });
                            if(json.length<1){
                                var error = '<div class=\'errorMessage clr autocomplete_name_text\'>Không tìm thấy dữ liệu.</div>';
                                if(parent_div.find('.autocomplete_name_text').size()<1)
                                    parent_div.find('.add_new_item').after(error);
                                else
                                    parent_div.find('.autocomplete_name_text').show();
                                parent_div.find('.remove_row_item').hide();
                            }

                        $('$idField').removeClass('grid-view-loading-gas');
                        } ",
                'select'=>"js:function(event, ui) {
                        var parent_div = $('$idField').closest('div.WrapAutocompleteStreet');
                        $('$idFieldStreet').val(ui.item.id);
                        var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveNameStreetWidget(this, \"$idField\", \"$idFieldStreet\")\'></span>';
                        $('$idField').parent('div').find('.remove_row_item').remove();
                        $('$idField').attr('readonly',true).after(remove_div);
                        parent_div.find('.autocomplete_name_text').hide();
                }",
        ),
        'htmlOptions' => array(
            'class'         => 'float_l autocomplete_name_error '.$ClassAdd,
            'maxlength'     => 45,
            'value'         => $value,
            'readonly'      => $readonly,
            'placeholder'   => 'Nhập tên đường không dấu',
        ),
)); 
?>
  
</div>

<div class="clr"></div>
<script>
function fnRemoveNameStreetWidget(this_, idField, idFieldStreet){
    $(this_).closest('div.WrapAutocompleteStreet').find('input').attr("readonly",false); 
    $(idField).val("");
    $(idFieldStreet).val("");
    fnAfterRemoveStreetWidget(this_, idField, idFieldStreet);
}
function fnAfterRemoveStreetWidget(this_, idField, idFieldStreet){}

$(document).ready(function(){
    var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveNameStreetWidget(this, \"<?php echo $idField;?>\", \"<?php echo $idFieldStreet;?>\")\'></span>';
    $('<?php echo $idField;?>').parent('div').find('.remove_row_item').remove();
    $('<?php echo $idField;?>').after(remove_div);
    
});


</script>
