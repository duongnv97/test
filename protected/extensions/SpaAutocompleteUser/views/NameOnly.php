<?php
$ClassAddDivWrap = '';
if(isset($data['ClassAddDivWrap']))
    $ClassAddDivWrap= $data['ClassAddDivWrap'];
?>
<div class="unique_wrap_autocomplete <?php echo $ClassAddDivWrap;?>">
<?php 
$data['url']                        = isset($data['url']) ? $data['url'] : Yii::app()->createAbsoluteUrl('admin/ajax/search_all_customer_storecard');
$data['field_autocomplete_name']    = isset($data['field_autocomplete_name']) ? $data['field_autocomplete_name'] : 'autocomplete_name';
$data['field_customer_id']          = isset($data['field_customer_id']) ? $data['field_customer_id'] : 'customer_id';

$data['class_name'] = get_class($data['model']);
$idFieldNameOnly    = $data['class_name'].'_'.$data['field_autocomplete_name'];
$idField            = "#$idFieldNameOnly";

if(isset($data['InputSearchIdCustom'])){
    $idField            = '#'.$data['InputSearchIdCustom'];
    $idFieldNameOnly    =  $data['InputSearchIdCustom'];
}

$idFieldCustomerID = '#'.$data['class_name']."_".$data['field_customer_id'];
if(isset($data['InputHideAutoId'])){
    $idFieldCustomerID =  '#'.$data['InputHideAutoId'];
}

$ClassAdd = '';
if(isset($data['ClassAdd']))
    $ClassAdd = $data['ClassAdd'];
$placeholder = 'Nhập mã KH/NCC, Số ĐT. Tối thiểu '.MIN_LENGTH_AUTOCOMPLETE." ký tự";
if(isset($data['placeholder']))
    $placeholder = $data['placeholder'];

$ShowTableInfo = 1;// Sep 23, 2015
if(isset($data['ShowTableInfo'])){
    $ShowTableInfo = $data['ShowTableInfo'];
}
$CallFnSelectCustomer   = 0;// Apr 20, 2016
$doSomethingOnClose     = 0;// Dec 28, 2016

$CallFnSelectCustomerV2 = 0;
if(isset($data['fnSelectCustomer'])):
    $CallFnSelectCustomer = 1;
else:
    $data['fnSelectCustomer'] = 1;
endif;

if(isset($data['fnSelectCustomerV2'])):
    $CallFnSelectCustomerV2 = 1;
endif;

if(isset($data['doSomethingOnClose'])):
    $doSomethingOnClose = 1;
endif;

$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
        'attribute'=>$data['field_autocomplete_name'],
        'model'=>$data['model'],
        'sourceUrl'=>$data['url'],
        'options'=>array(
                'minLength'=>MIN_LENGTH_AUTOCOMPLETE,
                'multiple'=> true,
        'search'=>"js:function( event, ui ) {
                $('$idField').addClass('grid-view-loading-gas');
                } ",
        'close'=>"js:function( event, ui ) {
                    if($doSomethingOnClose){
                        nameOnlyOnClose(ui, '$idField', '$idFieldCustomerID');
                    }
            } ",
        'response'=>"js:function( event, ui ) {
                var json = $.map(ui, function (value, key) { return value; });
                var parent_div = $('$idField').closest('div.unique_wrap_autocomplete');
                if(json.length<1){
                    var error = '<div class=\'errorMessage clr autocomplete_name_text\'>Không tìm thấy dữ liệu.</div>';
                    if(parent_div.find('.autocomplete_name_text').size()<1)
                        parent_div.find('.autocomplete_name_error').after(error);
                    else
                        parent_div.find('.autocomplete_name_error').parent('div').find('.autocomplete_name_text').show();
                    parent_div.find('.autocomplete_name_error').parent('div').find('.remove_row_item').hide();
                }
                $('$idField').removeClass('grid-view-loading-gas');
            } ",

            'select'=>"js:function(event, ui) {
                    var parent_div = $('$idField').closest('div.unique_wrap_autocomplete');
                    $('$idFieldCustomerID').val(ui.item.id);
                    var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveName(this, \"$idField\", \"$idFieldCustomerID\")\'></span>';                    
                    $('$idField').parent('div').find('.remove_row_item').remove();
                    $('$idField').attr('readonly',true).after(remove_div);
                    
                    parent_div.find('.autocomplete_name_error').parent('div').find('.autocomplete_name_text').hide();
                    if($CallFnSelectCustomer){
                        {$data['fnSelectCustomer']}(ui.item.id, '$idField', '$idFieldCustomerID');
                    }
                    if($CallFnSelectCustomerV2){
                        nameOnlyAfterSelect(ui, '$idField', '$idFieldCustomerID');
                    }

            }",
        ),
        'htmlOptions'=>array(
            'class'=>"autocomplete_name_error $ClassAdd" ,
            'size'=>30,
            'maxlength'=>45,
            'style'=>'float:left;',
            'placeholder'=>$placeholder,
            'id'=> $idFieldNameOnly,
        ),
)); 

$session=Yii::app()->session;
$mUserRelation = $data['model']->{$data['name_relation_user']};
?>
<?php if($mUserRelation):?>
    <?php 
    $show_role_name = '';
    if(isset($data['show_role_name'])){
        $show_role_name = " - ".$session['ROLE_NAME_USER'][ $mUserRelation->role_id ];
    }
    $info_name = $mUserRelation->first_name . $show_role_name;
    if( $mUserRelation->role_id == ROLE_SALE ){
        $info_name = MyFormat::BuildNameSaleSystem( $mUserRelation );
    }
    ?>
    <script>
        $(function(){
//            var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveName(this, \"<?php echo $idField;?>\", \"<?php echo $idFieldCustomerID;?>\")\'></span>';
//            $('<?php echo $idField;?>').val('<?php echo $info_name;?>').attr('readonly',true).after(remove_div);
            $('<?php echo $idField;?>').val('<?php echo $info_name;?>').attr('readonly',true);
        });
        
    </script>
<?php endif; ?>
</div>
<script>
    $(function(){
        var remove_div = '<span class=\'remove_row_item\' onclick=\'fnRemoveName(this, \"<?php echo $idField;?>\", \"<?php echo $idFieldCustomerID;?>\")\'></span>';
        $('<?php echo $idField;?>').after(remove_div);
    });
    
    function fnRemoveName(this_, idField, idFieldCustomer){
        $(this_).closest('div.unique_wrap_autocomplete').find(idField).attr("readonly",false);
        $(idField).val("");
        $(idFieldCustomer).val("");
        fnAfterRemove(this_, idField, idFieldCustomer);
    }

    function fnAfterRemove(this_, idField, idFieldCustomer){}
    
    /* Mar 24, 2014 Dùng để gọi 1 số hàm bên ngoài truyền vào
     * một số view cần custom cái đó nên gọi,
     * sử dụng cho after select
     */
    <?php if(isset($data['fnSelectCustomer'])): // Apr 20, 2016 ?>
    function fnCallSomeFunctionAfterSelect(user_id, idField, idFieldCustomer){        
        <?php echo $data['fnSelectCustomer'];?>(user_id, idField, idFieldCustomer);
    }
    <?php endif;?>
    
    /** Sep 23, 2015 Dùng để gọi 1 số hàm bên ngoài truyền vào
     * một số view cần custom cái đó nên gọi,
     * @note: sử dụng cho after select
     * hàm này chuyển cả object ui vào, chứ không phải là bó gọn trong user_id
     */
    <?php if(isset($data['fnSelectCustomerV2'])): ?>
    function nameOnlyAfterSelect(ui, idField, idFieldCustomer){
        <?php echo $data['fnSelectCustomerV2'];?>(ui, idField, idFieldCustomer);
    }
    <?php endif;?>
    
    <?php if(isset($data['nameOnlyOnClose'])): ?>
    function doSomethingOnClose(ui, idField, idFieldCustomer){
        <?php echo $data['doSomethingOnClose'];?>(ui, idField, idFieldCustomer);
    }
    <?php endif;?>
    
    
</script>
