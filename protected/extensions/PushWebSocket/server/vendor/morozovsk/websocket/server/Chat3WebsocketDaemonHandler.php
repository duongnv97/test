<?php

namespace morozovsk\websocket\server;

class Chat3WebsocketDaemonHandler extends \morozovsk\websocket\Daemon
{
    public $connectionList = [];
    public $super_user = null;
    const SUPER_ADMIN_CODE = '@super_admin@'; // Lệnh để nâng quyền admin
    const SUPER_LIST_USER = '1'; // Lệnh để nâng quyền admin
    const SUPER_LIST_TOKEN = '2'; // Lệnh để nâng quyền admin

    // Sử dụng cho verify code
    const key = "ds3Dk3pidsfcPd";

    protected function onOpen($connectionId, $info)
    {//Call when connecting to a new client
        $message = 'User mới #' . $connectionId . ' : ' . var_export($info, true) . ' ' . stream_socket_get_name($this->clients[$connectionId], true);

        // Gửi đến super user
        if ($this->super_user) {
            $this->sendToClient($this->super_user, $message);
        }

        $info['GET'];//or use $info['Cookie'] for use PHPSESSID or $info['X-Real-IP'] if you use proxy-server like nginx
        parse_str(substr($info['GET'], 1), $_GET);//parse get-query

        if (isset($_GET['verify_code']) && isset($_GET['huongminh_token'])) {
            $verifyCode = self::decodeVerifyCode($_GET['verify_code']);

            if (self::verifyValidCode($_GET['huongminh_token'], $verifyCode)) {
                // Chỉ thêm những user được xác nhận
                $this->connectionList[$connectionId] = $_GET['huongminh_token'];
            } else {
                // User truyen khong du verify code
            }
        } else {
            // User truyen params khong hop le
        }
    }

    protected function onClose($connectionId)
    {//It is called when the connection is closed with an existing client
        unset($this->connectionList[$connectionId]);

        // Xóa super user
        if (isset($this->super_user) && $this->super_user == $connectionId) {
            unset($this->super_user);
        }
    }

    protected function onMessage($connectionId, $data, $type)
    {// Called when a message is received from the client
        if (!strlen($data)) {
            return;
        }
        // Xoa bo ky tu html va php
        $data = strip_tags($data);

        // Thêm người dùng admin
        if ($data == self::SUPER_ADMIN_CODE) {
            $this->super_user = $connectionId;
            $message = 'Bạn đã đăng ký thàng công super admin';
            $this->sendToClient($this->super_user, $message);
            return;
        }

        if ($connectionId == $this->super_user) {
            $message = '';
            // Super admin gửi message
            if ($data == self::SUPER_LIST_USER) {
                $message = "Số lượng người sử dụng hiện tại là: " . count($this->connectionList);
            } elseif ($data == self::SUPER_LIST_TOKEN) {
                $message = "Danh sách token: " . json_encode($this->connectionList);
            }
            $this->sendToClient($this->super_user, $message);
        } else {
            // User gửi message
            if ($this->super_user) {
                $message = 'SuperUser: User gửi message #' . $connectionId . ' : ' . $data;
                $this->sendToClient($this->super_user, $message);
            }
        }
    }

    protected function onServiceMessage($connectionId, $data)
    {
        // Gửi thông tin cho super user
        if (isset($this->super_user)) {
            $this->sendToClient($this->super_user, 'Phát hiện server gửi: ' . $data);
        }
        $data = json_decode($data);

        $message = '';

        if (isset($data->user_token)) {
            // Phát hiện tìm theo huongminh_token

            foreach ($this->connectionList as $currentConnectionId => $userToken) {
                if ($data->user_token == $userToken) {
                    if (isset($data->code)) {
                        // Server dang push code, truyền code về client
                        $message = 'C:' . ($data->code * 1);
                    }
                    $this->sendToClient($currentConnectionId, $message);
                }
            }
        }

        if (isset($this->super_user)) {
            $this->sendToClient($this->super_user, 'Message gửi là: ' . $message);
        }
    }

    /**
     * @Author: TRUNG Jul 25 2016
     * Decode mã xác nhận để đảm bảo code được gửi từ server hướng minh
     * @param $verifyCode
     * @return string
     */
    private function decodeVerifyCode($verifyCode)
    {
        $uuid = $this->decrypt($verifyCode, self::key);
        return $uuid;
    }

    /**
     * Returns an encrypted & utf8-encoded
     */
    function encrypt($pure_string, $encryption_key)
    {
        $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $encryption_key, utf8_encode($pure_string), MCRYPT_MODE_ECB, $iv);
        $encrypted_string = base64_encode($encrypted_string);
        return $encrypted_string;
    }

    /**
     * Returns decrypted original string
     */
    function decrypt($encrypted_string, $encryption_key)
    {
        $encrypted_string = base64_decode($encrypted_string);
        $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, $encryption_key, $encrypted_string, MCRYPT_MODE_ECB, $iv);
        return $decrypted_string;
    }

    /**
     * @Author: TRUNG Jul 25 2016
     * Từ mã xác nhận đã được giải mã. Kiểm tra code có được thỏa mãn
     * @param $token
     * @param $verifyCode
     */
    private function verifyValidCode($token, $verifyCode)
    {
        try {
            if (!empty($token) && strpos($token . ":", $verifyCode) == 0) {
                date_default_timezone_set('Asia/Ho_Chi_Minh');
                $dateObj = \DateTime::createFromFormat("Y-m-d h-i-sa", substr($verifyCode, strlen($token . ":")));
                if ($dateObj) {
                    if ($dateObj->getTimestamp() + 60 > time()) { // Cho phép trong vòng 1 phút
                        var_dump(true);
                        return true;
                    }
                }
            }
        } catch (\Exception $ex) {
        }
        var_dump(false);
        return false;
    }
}