<?php

class PushWebSocket
{
    const LOCAL_SOCKET_ADDRESS = 'tcp://127.0.0.1:8010'; // 'tcp://127.0.0.1:8010';
    private static $ERROR_CODE_SOCKET_FAILED = -9999;
    private static $ERROR_MESSAGE_SOCKET_FAILED = 'Unable to connect to web socket';
    private static $instance = null;

    // Sử dụng cho verify code
    const key = "ds3Dk3pidsfcPd";

    private static function getWebSocket()
    {
        if (self::$instance == null) {
            try {
                self::$instance = @stream_socket_client(PushWebSocket::LOCAL_SOCKET_ADDRESS,
                    PushWebSocket::$ERROR_CODE_SOCKET_FAILED, PushWebSocket::$ERROR_MESSAGE_SOCKET_FAILED);//connect to the web server socket
            } catch (Exception $ex) {
                // Khong handle
            }
        }
        return self::$instance;
    }

    public static function pushCodeToClientToken($jsonData)
    {
        $instance = self::getWebSocket();
        try {
            if ($instance) {
                fwrite($instance, json_encode($jsonData) . "\n");//send a message
            }
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * @Author: TRUNG Jul 25 2016
     * Tạo verify code
     * @param $userModel Users
     * @return string string
     */
    public static function generateVerifyCode($userModel)
    {
        if ($userModel && $userModel->id) {

            $textToEncrypt = $userModel->id . ":" . date("Y-m-d h-i-sa");

            $uuid = self::encrypt($textToEncrypt, self::key);

            return $uuid;
        }
        return '';
    }

    /**
     * Returns an encrypted & utf8-encoded => base64
     */
    public static function encrypt($pure_string, $encryption_key)
    {
        $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $encryption_key, utf8_encode($pure_string), MCRYPT_MODE_ECB, $iv);
        $encrypted_string = base64_encode($encrypted_string);
        return $encrypted_string;
    }
}