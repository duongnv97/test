<style type="text/css">
	div.day-number	 { 
	background:#999; 
	position:absolute; 
	z-index:2; 
	top:-5px; 
	right:-25px; 
	padding:5px; 
	color:#fff; 
	font-weight:bold; 
	width:20px; 
	text-align:center; 
}
td.calendar-day, td.calendar-day-np { 
	width:120px; 
	padding:5px 25px 5px 5px; 
	border-bottom:1px solid #999; 
	border-right:1px solid #999; 
}

</style>

<?php
/* draws a calendar */
function draw_calendar(){
    $week_number = date('W');
    $year = date('Y');
    $week_number = (($week_number >= 1) AND ($week_number <= 52))?($week_number):(1);

    if($week_number < 10){
    $week_number = "0".$week_number;
    }
    $headings = array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
    for($day=1; $day<=count($headings); $day++)
    {
        $arr[] = date('d M Y', strtotime($year."W".$week_number.$day));
    }
    /* draw table */
    $calendar = '<table class="search-result">';      
    /* table headings */
    $calendar.= '<thead id="header_stick"><tr><th class="first">&nbsp;</th>';
    for($i=0;$i<7;$i++){
        $calendar.='<th><strong>'.$headings[$i].'</strong>'.$arr[$i].'</th>'; 
    }
    $calendar.='</tr></thead>';

    /* row for week one */
    $calendar.= '<tbody>';
        $calendar .='<tr>
                <td class="first">
                <div class="doctor-profile">
                    <img src="images/img-doctor-2.jpg" alt="image" class="image" />
                    <div class="info">
                        <dl>
                            <dt class="name">
                                <a href="doctor-profile.html" class="tool">Dr Nancy Tan</a>
                                <div class="tooltip">
                                    <p class="name-tool"><strong>Dr Nancy Tan</strong></p>
                                    <table width="100%">
                                        <tr class="odd">
                                                <td class="first">Qualifications:</td>
                                            <td><strong>Sed ut perspiciatis</strong></td>
                                        </tr>
                                        <tr>
                                                <td class="first">Languages spoken:</td>
                                            <td><strong>English</strong></td>
                                        </tr>
                                        <tr class="odd">
                                                <td class="first">Education:</td>
                                            <td><strong>Nemo enim ipsam</strong></td>
                                        </tr>
                                    </table>
                                    <p class="link"><a href="#">Read more</a></p>
                                </div>
                            </dt>
                                <dd class="type-4">Cardiologist</dd>
                                <dd>3 Mount Elizabeth Singapore 228510</dd>
                        </dl>
                    </div>
                </div>
            </td>
                <td colspan="7" class="all">
                <a href="booking-details.html">Appointments on request</a>
            </td>
            </tr>';
    $calendar .='</tbody></table>';
    return $calendar;
}
echo draw_calendar();
?>