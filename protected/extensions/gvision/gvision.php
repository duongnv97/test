<?php
/** @Author: ANH DUNG Dec 02, 2018
 *  @Todo: apply Google Client Libraries
 *  Sử dụng để xác định biển số xe từ 1 image chụp ở trạm cân
 * @note: https://github.com/Art4/json-api-client/issues/28
 * /var/www/html/gas/protected/extensions/gvision/vendor/google/protobuf/php/src/Google/Protobuf/Internal/Message.php
 * Line 1157 Change function parseFromJsonStream($input) vì khi parse json bị error
 * @From: $array = json_decode($input->getData(), true, 512, JSON_BIGINT_AS_STRING);
 * @To  : $array = json_decode($input->getData(), true, 512);
 * 
 **/
Yii::import('zii.widgets.CPortlet');
# includes the autoloader for libraries installed with composer
require __DIR__ . '/vendor/autoload.php';

# imports the Google Cloud client library
use Google\Cloud\Vision\V1\ImageAnnotatorClient;

## fix GOOGLE_APPLICATION_CREDENTIALS https://stackoverflow.com/questions/35418798/google-cloud-api-application-default-credentials
putenv('GOOGLE_APPLICATION_CREDENTIALS='.__DIR__.'/vendor/Gas24h-vision-key.json'); //your path to file of cred

class gvision
{
    public $path='', $aTextFound = [], $textSelect = '';// url to file
    public $typeDetection = 1; // default is Text Detection

    public function getContent(){
        switch ($this->typeDetection) {
            case TruckScales::GVISION_TEXT_DETECTION:
                $this->getTextDetection();
                break;

            default:
                $this->getTextDetection();
                break;
        }
    }
    
    /** @Author: ANH DUNG Dec 02, 2018
     *  @Todo: Text Detection performs Optical Character Recognition. 
     * It detects and extracts text within an image with support for a broad range of languages. It also features automatic language identification.
     * @guide: https://cloud.google.com/vision/docs/detecting-text#vision-text-detection-php
     **/
    public function getTextDetection(){
//        $this->path = 'http://dev.spj.vn/x1.jpg';
//        $this->path = 'http://dev.spj.vn/x2.jpg';
        $imageAnnotator = new ImageAnnotatorClient();

        # annotate the image
        $image = file_get_contents($this->path);
        $response = $imageAnnotator->textDetection($image);
        $texts = $response->getTextAnnotations();
//        printf('%d texts found:' . PHP_EOL, count($texts));
        foreach ($texts as $text) {
            $this->aTextFound[] = $text->getDescription();
            $this->textSelect   = $text->getDescription();
//            print($text->getDescription() . PHP_EOL);

            # get bounds
            $vertices = $text->getBoundingPoly()->getVertices();
            $bounds = [];
            
            foreach ($vertices as $vertex) {
                $bounds[] = sprintf('(%d,%d)', $vertex->getX(), $vertex->getY());
            }
//            print('Bounds: ' . join(', ',$bounds) . PHP_EOL);
        }
        $imageAnnotator->close();
    }
            
    
}