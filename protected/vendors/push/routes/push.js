var express = require('express');
var router = express.Router();


// https://firebase.google.com/docs/cloud-messaging/admin/legacy-fcm
// https://firebase.google.com/docs/cloud-messaging/admin/send-messages
var admin = require("firebase-admin");

var serviceAccount = require("../config/gas-android-firebase.json");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://gas-android.firebaseio.com"
});

// This registration token comes from the client FCM SDKs.
var registrationToken = 'dlL6IBsm5Eg:APA91bHXCJ0gFRWZPfkyuEVEfc-_Ohix2P54z4bkunOAeBujYdIQrUFg_vzIRcif_flxNforC8MHJO25aH8_vm_4gxeIw_Q0FiVqcOslx0m9x20snUeyDhE5dJCwEASy3qAc-rgCeQgOEXR7NC_T8qO29D0IKjQdRA';

// See the "Defining the message payload" section below for details
// on how to define a message payload.
var payload = {
    data: {
        notify_type: '6',
        reply_id: '2',
        id: '' + Math.floor(Math.random() * 100000),
        type: 'TYPE_ORDER_BO_MOI',
        message: "Test data",
        notify_id: '2262'
    }
};

/* GET users listing. */
router.get('/', function (req, res, next) {
    // Send a message to the device corresponding to the provided registration token.
    admin.messaging().sendToDevice([registrationToken], payload)
        .then(function (response) {
            // See the MessagingDevicesResponse reference documentation for
            // the contents of response.
            console.log('Successfully sent message:', response);
            res.send('OK');
        })
        .catch(function (error) {
            console.log('Error sending message:', error);
            res.send(JSON.stringify(error));
        });
});

router.post('/', function (req, res, next) {
    if (req.body && Array.isArray(req.body.tokens) && typeof req.body.data === 'object') {
        admin.messaging().sendToDevice(req.body.tokens, {data: req.body.data})
            .then(function (response) {
                // See the MessagingDevicesResponse reference documentation for the contents of response.
                res.send('OK');
            })
            .catch(function (error) {
                console.log('Error:', error);
                res.send("Error");
            });
    } else {
        res.send('Please send right data!');
        console.log('Please send right data!');
    }
});

module.exports = router;
