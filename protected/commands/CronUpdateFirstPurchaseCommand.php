<?php
class CronUpdateFirstPurchaseCommand extends CConsoleCommand
{
    public function run($arg)
    {// đang chạy 2 lần 1 ngày 0 0,12 * * *
        try {
        // chú ý bug khi viết Cron: Uid: Exception Property "CConsoleApplication.session" is not defined.
        Logger::WriteLog('cronRemoveTokenNotUse CronUpdateFirstPurchaseCommand run: ');
        UsersTokens::cronRemoveTokenNotUse();// Apr 25, 2016 xóa những token của user app ko sử dụng
        Logger::WriteLog('cronRemoveNotifyStorecard CronUpdateFirstPurchaseCommand');
        GasSocketNotify::cronRemoveNotifyStorecard();
        Logger::WriteLog('CronUpdateFirstPurchase CronUpdateFirstPurchaseCommand');
        GasBussinessContract::CronUpdateFirstPurchase();// Close on Dec 19, 2015
        /*
         * Dec 19, 2015 định return ở hàm kia, nhưng không hiểu tại sao nó lại văng lỗi ra: PHP Error[2]: include(CDbcriteria.php): failed to open stream:
         * resolve bug: lỗi hoa thường: CDbcriteria <=> CDbCriteria
         */
//        UpdateSql::UpdateLastInputRemainStorecard();// Add Apr 10, 2015 - Remove jan1818 không cần nữa
        GasScheduleNotify::moveNotifyToHistory(); // Dec 22, 2015
//        UpdateSql::UpdateLastPurchase();// Close on Apr 25, 2016 vì thấy không cần thiết lắm. Jan 25, 2016, run update ngày lấy hàng cuối cùng của những KH bị null trong cái last_purchase, không rõ lý do vì sao NULL
        $mLog = new Logger();
        $mLog->deleteLog();
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}