<?php
class NotifyRoadShowCommand extends CConsoleCommand
{
    /* for update and genera data 
     * run at 09h every day # 05 8 * * *   root  /usr/bin/php /var/www/spj.daukhimiennam.com/web/cron.php NotifyRoadShow &> /dev/null
     *  
     */
    public function run($arg) 
    {
        try {
            ini_set('memory_limit','3500M');
            $today                  = date('Y-m-d');
            $mGasEventMarket        = new GasEventMarket(); 
            $mGasEventMarket->cronRemindRoadShow($today,1);// Thực hiện gửi mail tự động roadshow
            $mGasEventMarket->cronRemindRoadShow($today,5);// Thực hiện gửi mail tự động roadshow
            
//            Thực hiện cron notify closingPersinalDebit
            $mClosingPersonalDebit = new ClosingPersonalDebit();
            $mClosingPersonalDebit->gasAppOrderDaily($today);
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
}