<?php
class ToExcelCommand extends CConsoleCommand
{
    //01 20 * * *   root  /usr/bin/php /var/www/spj.daukhimiennam.com/web/cron.php ToExcel &> /dev/null
    public function run($arg) 
    {
        try { // Cron export excel 2 thang lien tiep 
            $from = time();
            $mCronExcel     = new CronExcel();
            $date_from      = MyFormat::modifyDays(date('Y-m-01'), $mCronExcel->numberMonthRun, '-', 'month', 'Y-m-01');// tru di 2 thang, lay ngay DAU thang 
//            $date_from      = '2019-04-01';
            $mCronExcel->cronExcelEmployeeCashbook($date_from);
            $mCronExcel->cronExcelGasRemain($date_from);
//            // $mCronExcel->cronExcelGasAppOrder($date_from);
            $mCronExcel->cronExcelGasAppOrderDaily($date_from);
            $mCronExcel->cronExcelSell($date_from);
//            $mCronExcel->cronExcelGasStoreCard($date_from);// Add Aug1219 -> move to ToExcel1
//            $mCronExcel->cronExcelCustomerHgdApp(); need set ini_set('memory_limit','4500M'); alloc more memory
            
            $mGasScheduleSms = new GasScheduleSms();
            $mGasScheduleSms->alertAnything('ToExcel 08h '.date('d/m').' done at: '.date('Y-m-d H:i:s'), GasConst::$arrAdminUserReal);
            $to = time(); $second = $to-$from;
            Logger::WriteLog("___ExcelCron__All___ ToExcel done in: ".($second).'  Second  <=> '.($second/60).' Minutes done at: '.date('Y-m-d H:i:s'));
        }catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}