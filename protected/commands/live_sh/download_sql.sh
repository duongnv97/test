# Dec 03, 2014 download from url daukhi to huongminhgroup.com
FDATE=`date +%F`
C_YEAR=`date +"%Y"`
BACKUPDIR="/mnt/volume-android-huongminhgroup/backup/db"
if [ ! -d $BACKUPDIR/$C_YEAR/ ] ; then
        mkdir $BACKUPDIR/$C_YEAR/
fi

## wget http://spj.daukhimiennam.com/gas_sql/c1gas35-$FDATE.tar.bz2 -O /var/www/anhdunggas/c1gas35-$FDATE.tar.bz2
## wget http://spj.daukhimiennam.com/2014-12-04.backup_site.zip -O /var/www/anhdunggas/2014-12-04.backup_site.zip
# Oct0617 save backup db to block store
wget http://spj.daukhimiennam.com/gas_sql/c1gas35-$FDATE.tar.bz2 -O /mnt/volume-android-huongminhgroup/backup/db/$C_YEAR/c1gas35-$FDATE.tar.bz2

## Jun 18, 2015 download backup phap ly+van ban - 00:00 chu nhat hang tuan 
wget http://spj.daukhimiennam.com/gas_sql/$FDATE.backup_profile.zip -O /var/www/anhdunggas/$FDATE.backup_profile.zip
wget http://spj.daukhimiennam.com/gas_sql/$FDATE.backup_text_scan.zip -O /var/www/anhdunggas/$FDATE.backup_text_scan.zip

# Dec 03, 2014 remove file after 20 day
##find /var/www/anhdunggas -mtime +20 -print0 | xargs -0 rm -rf
# Jun 18, 2015 Recursively remove files those size is less than 1MB
find $BACKUPDIR/$C_YEAR -type f -size -1M -delete

# Dec 03, 2014 download from url daukhi to huongminhgroup.com
# How to stop clamav?
/etc/init.d/clamav-daemon stop
/etc/init.d/amavis stop
/etc/init.d/spamassassin stop
service postfix stop
