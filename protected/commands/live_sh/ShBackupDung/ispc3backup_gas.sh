#!/bin/bash
#
# ISPConfig3 back up script based on bak-res script by go0ogl3 gabi@eurosistems.ro
#
# Copyright (c) Ioannis Sannos ( http://www.isopensource.com )
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# The above copyright notice and this permission notice shall be included in
# all copies of the script.
#
# description: A backup script for sites and databases on ISPConfig 3 servers
# Add this script in cron tab in order to be executed once per day.
# Example (04:30 at night every day):
# 30 04 * * * /backup/ispc3backup.sh &> /dev/null
#
# author: Ioannis Sannos
# date: 2010-03-06 13:45:10
# The state of development is "It works for me"!
# So don't blame me if anything bad will happen to you or to your computer 
# if you use this script.
#

# ANH DUNG May 10, 2014
## Start user editable variables
CLIENTSDIR="/var/www/clients" 		# directory where ICPConfig 3 clients folders are located
BACKUPDIR="/var/www/spj.daukhimiennam.com/private/backup_gas"		# backup directory
DBUSER="c1gas35"						 # database user
DBPASS="adP1assHPWA0rd456"				# database password
TAR=`which tar`						# name and location of tar
ARG="-cjpSPf"		#sparse			# tar arguments P = removed / 
tmpdir="/var/www/spj.daukhimiennam.com/tmp"				# temp dir for database dump and other stuff
## End user editable variables

########### make needed directories if not exist #############
if [ ! -d $BACKUPDIR/ ] ; then
	exit 0
fi
if [ ! -d $tmpdir/ ] ; then
	mkdir $tmpdir/
fi
if [ ! -d $BACKUPDIR/db/ ] ; then
	mkdir $BACKUPDIR/db/
fi
if [ ! -d $BACKUPDIR/db/daily/ ] ; then
	mkdir $BACKUPDIR/db/daily/
fi
if [ ! -d $BACKUPDIR/db/weekly/ ] ; then
	mkdir $BACKUPDIR/db/weekly/
fi
if [ ! -d $BACKUPDIR/webs/ ] ; then
	mkdir $BACKUPDIR/webs/
fi
if [ ! -d $BACKUPDIR/webs/daily/ ] ; then
	mkdir $BACKUPDIR/webs/daily/
fi
if [ ! -d $BACKUPDIR/webs/weekly/ ] ; then
	mkdir $BACKUPDIR/webs/weekly/
fi

FDATE=`date +%F`		# Full Date, YYYY-MM-DD, year sorted, eg. 2009-11-21
WDAY=`date +%w`			#Day of week (0 for sunday)

########### backup database #############

# check and fix any errors found
mysqlcheck -u$DBUSER -p$DBPASS --all-databases --optimize --auto-repair --silent 2>&1
# Starting database dumps
for i in `mysql -u$DBUSER -p$DBPASS -Bse 'show databases'`; do
	`mysqldump -u$DBUSER -p$DBPASS $i --allow-keywords --comments=false --add-drop-table > $tmpdir/db-$i-$FDATE.sql`
	
	# ANH DUNG check if is table information_schema we not need backup this table
	if [ $i = "information_schema" ] ; then
		rm -rf $tmpdir/db-$i-$FDATE.sql
		continue
	fi
	# check if is table information_schema
	
	# Daily backup and Weekly backup on sundays
	if [ $WDAY = "0" ] ; then
	# if [ true ] ; then
		if [ -f $BACKUPDIR/db/weekly/$i.tar.bz2 ] ; then
			rm -rf $BACKUPDIR/db/weekly/$i.tar.bz2
		fi
		if [ -f $BACKUPDIR/db/daily/$i.tar.bz2 ] ; then
			## Jul 09, 2014 cp $BACKUPDIR/db/daily/$i.tar.bz2 $BACKUPDIR/db/weekly/$i.tar.bz2
			rm -rf $BACKUPDIR/db/daily/$i.tar.bz2
		fi
		$TAR $ARG $BACKUPDIR/db/daily/$i-$FDATE.tar.bz2 -C $tmpdir db-$i-$FDATE.sql
		# daily ANH DUNG ADD alway have a file with name c1gas.tar.bz2 not include date
## May 07, 2017 close this, ko hieu dung de lam gi		$TAR $ARG $BACKUPDIR/db/daily/$i.tar.bz2 -C $tmpdir db-$i-$FDATE.sql

		##if [ ! -f $BACKUPDIR/db/weekly/$i-$FDATE.tar.bz2 ] ; then
			## cp $BACKUPDIR/db/daily/$i.tar.bz2 $BACKUPDIR/db/weekly/$i.tar.bz2
			# weekly ANH DUNG ADD alway have a file with name c1gas.tar.bz2 not include date
			## cp $BACKUPDIR/db/daily/$i.tar.bz2 $BACKUPDIR/db/weekly/$i-$FDATE.tar.bz2
		##fi
	else
		if [ -f $BACKUPDIR/db/daily/$i.tar.bz2 ] ; then
			rm -rf $BACKUPDIR/db/daily/$i.tar.bz2
		fi
		$TAR $ARG $BACKUPDIR/db/daily/$i-$FDATE.tar.bz2 -C $tmpdir db-$i-$FDATE.sql
		# daily ANH DUNG ADD alway have a file with name c1gas.tar.bz2 not include date
		## Jul 09, 2014  $TAR $ARG $BACKUPDIR/db/daily/$i.tar.bz2 -C $tmpdir db-$i-$FDATE.sql
	fi
	rm -rf $tmpdir/db-$i-$FDATE.sql
	
done

# Dec 03, 2014 copy to dir can download from url        
cp $BACKUPDIR/db/daily/c1gas35-$FDATE.tar.bz2 /var/www/spj.daukhimiennam.com/web/gas_sql/c1gas35-$FDATE.tar.bz2
# Feb 16, 2017 khong copy nua, de luon ben nay
# Dec 03, 2014 copy to dir can download from url

# all done
# Remove older backups (> 60 days),
# unless you want to run out of drive space
find $BACKUPDIR/db/daily -mtime +2 -print0 | xargs -0 rm -rf
        # Dec 03, 2014 remove file after 5 day
find /var/www/spj.daukhimiennam.com/web/gas_sql -mtime +2 -print0 | xargs -0 rm -rf
## Dec0618 remove dir runtime for test Bug YiiSession
##Sep0119 move to backup_some_upload.sh  => rm -rf /var/www/clients/client1/web2/web/protected/runtime/*
## change Aug2719 cd /var/www/spj.daukhimiennam.com/web/protected/runtime

exit 0
