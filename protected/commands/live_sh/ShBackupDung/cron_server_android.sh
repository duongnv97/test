====================    Aug 28, 2019    =========================
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

# m h dom mon dow user	command
17 *	* * *	root    cd / && run-parts --report /etc/cron.hourly
25 6	* * *	root	test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.daily )
47 6	* * 7	root	test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.weekly )
52 6	1 * *	root	test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.monthly )
#
# May 09, 2014 for  2h 35 run my backup daily
#####35 2 * * * root /var/www/daukhimiennam.com/web/protected/commands/ispc3backup_gas.sh &> /dev/null
#####25 01 * * *   root  /usr/bin/php /var/www/daukhimiennam.com/web/cron.php Resetpass
###0 9,15 * * *   root /usr/bin/php /var/www/daukhimiennam.com/web/cron.php Resetpass
#####0 */3 * * *   root  /usr/bin/php /var/www/daukhimiennam.com/web/cron.php CronUpdateFirstPurchase
#####0 0 1,15  * *   root  /usr/bin/php /var/www/daukhimiennam.com/web/cron.php CronCleanTrackLogin

# Sep 09, 2014 run scan Malware
05 01 * * *   root  maldet -a /var/www/clients/ &> /dev/null

## test 1 phut * * * * *   root  /usr/bin/php /var/www/daukhimiennam.com/web/cron.php Resetpass
# download sql from lvie VPS every 5h AM
0 6 *  * * root /var/www/android.huongminhgroup.com/web/protected/commands/download_sql.sh &> /dev/null
## 1 phut chay 1 lan Dec 02, 2015 App Send notify to User - Open when test Sep 01, 2016
### */1 * * * *   root  /usr/bin/php /var/www/android.huongminhgroup.com/web/cron.php SendScheduleNotify &> /dev/null
## 3h chay build notify
## 0 */3 * * *   root  /usr/bin/php  /var/www/android.huongminhgroup.com/web/cron.php UpholdBuildNotify
## */5 * * * *   root /var/www/android.huongminhgroup.com/web/protected/commands/live_sh/rebootvps.sh &> /dev/null


### not use  35 2 * * * root /var/www/bantoi.huongminhgroup.com/web/protected/commands/ispc3backup_bantoi.sh &> /dev/null
*/1 * * * * root /usr/bin/php /var/www/io.huongminhgroup.com/web/cron.php SocketStart &> /dev/null
*/5 * * * * root /usr/bin/php /var/www/io.huongminhgroup.com/web/cron.php SocketCheck &> /dev/null

### Apr 20,17 run de remove event CallCenter
2 */1 * * *   root  /usr/bin/php /var/www/io.huongminhgroup.com/web/cron.php CronUpdateFirstPurchase &> /dev/null
* * * * *   root  /usr/bin/php /var/www/android.huongminhgroup.com/web/cron.php SendScheduleNotify &> /dev/null
*/5 * * * * root /var/www/android.huongminhgroup.com/web/protected/commands/stop_service.sh &> /dev/null