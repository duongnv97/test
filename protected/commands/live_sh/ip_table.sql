/**
    ANH DUNG ADD Jun 08, 2018
    backup config iptable some server
*/
/* 
Install
sudo apt-get update 
sudo apt-get install iptables-persistent
Allow specific IP on port:

Add line in file  "vi /etc/iptables/rules.v4" . Add before "COMMIT"
-A INPUT -p tcp --dport 8984 -s 210.245.35.96 -j ACCEPT 
-A INPUT -p tcp --dport 8984 -j DROP

Reload service: ip table iptable
For ubuntu 14:
service iptables-persistent reload
For ubuntu 16:
service netfilter-persistent reload

ref: https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-using-iptables-on-ubuntu-14-04
http://dev-notes.eu/2016/08/persistent-iptables-rules-in-ubuntu-16-04-xenial-xerus/

# Solr Live 128.199.205.12 Generated by iptables-save v1.6.0 on Wed Dec 20 09:13:33 2017
*filter
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]

# Solr config
-A INPUT -p tcp --dport 8983 -s 210.245.35.96 -j ACCEPT
-A INPUT -p tcp --dport 8983 -s 128.199.130.14 -j ACCEPT
-A INPUT -p tcp --dport 8983 -s 128.199.166.98 -j ACCEPT
-A INPUT -p tcp --dport 8983 -j DROP

# SSH config
-A INPUT -p tcp --dport 3435 -s 210.245.35.96 -j ACCEPT
-A INPUT -p tcp --dport 3435 -s 128.199.130.14 -j ACCEPT
-A INPUT -p tcp --dport 3435 -s 128.199.166.98 -j ACCEPT
-A INPUT -p tcp --dport 3435 -s 116.109.211.176 -j ACCEPT
-A INPUT -p tcp --dport 3435 -j DROP
COMMIT
# Completed on Wed Dec 20 09:13:33 2017

# Daukhimiennam.com live 128.199.130.14 Generated by iptables-save v1.4.21 on Tue Jan  2 21:27:22 2018
*filter
:INPUT ACCEPT [2977:650607]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [2943:2287481]
# SSH config Jan0117
-A INPUT -p tcp --dport 3435 -s 210.245.35.96 -j ACCEPT
-A INPUT -p tcp --dport 3435 -s 128.199.130.14 -j ACCEPT
-A INPUT -p tcp --dport 3435 -s 128.199.166.98 -j ACCEPT
-A INPUT -p tcp --dport 3435 -s 116.109.211.176 -j ACCEPT
-A INPUT -p tcp --dport 3435 -j DROP
# Some other config Jan0117
COMMIT
# Completed on Tue Jan  2 21:27:22 2018





*/

