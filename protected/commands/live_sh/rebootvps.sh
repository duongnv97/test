#!/bin/bash
arg=$(head -n 1 /tmp/rebootvps)
if [ "$arg" == "doreboot" ]; then
    FDATE=`date +%Y-%m-%d:%H:%M:%S`
    >/tmp/rebootvps
    ## FILES=/var/www/android.huongminhgroup.com/web/protected/commands/live_sh/rebootlog
    FILES=/var/www/spj.daukhimiennam.com/web/protected/commands/live_sh/rebootlog
    for f in $FILES ; do
        echo "$FDATE Rebooting" >> $f
    done
  echo "Rebooting"
  echo 'password' | sudo -S reboot
fi