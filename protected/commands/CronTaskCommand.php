<?php
class CronTaskCommand extends CConsoleCommand
{
    // every minutes  * * * * *   root  /usr/bin/php /var/www/spj.daukhimiennam.com/web/cron.php CronTask &> /dev/null
    public function run($arg) 
    {
        try { // Apr 11, 2018
            $model = new CronTask();
            $model->runSyncCache();
            $model->run();
        }catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}