<?php
class SocketStartCommand extends CConsoleCommand
{
    // every minutes  * * * * *   root  /usr/bin/php /var/www/spj.daukhimiennam.com/web/cron.php SendScheduleNotify
    public function run($arg) 
    {
        try { // Oct 02, 2016
            GasSocketNotify::cronRemoveNotifyCall();
            Yii::import('application.commands.*');
            $command = new PushServerCommand("test", "test");
            
            $cHours = date("H");
            if($cHours == 3 ) {// 3h sáng sẽ restart lại socket 1 lần
                $command->run(array('stop'));
            }else{
//                $command->run(array('stop'));
                $command->run(array('start'));
            }

//            $command->run(array('restart'));
        }catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}