<?php // Add Aug1219
class ToExcel1Command extends CConsoleCommand
{
    //01 20 * * *   root  /usr/bin/php /var/www/spj.daukhimiennam.com/web/cron.php ToExcel &> /dev/null
    public function run($arg) 
    {
        try { // Cron export excel 2 thang lien tiep 
            $from = time();
            $mCronExcel     = new CronExcel();
            $date_from      = MyFormat::modifyDays(date('Y-m-01'), $mCronExcel->numberMonthRun, '-', 'month', 'Y-m-01');// tru di 2 thang, lay ngay DAU thang 
//            $date_from      = '2019-04-01';
            $mCronExcel->cronExcelGasStoreCard($date_from);
            
            $mGasScheduleSms = new GasScheduleSms();// đoạn này không cần gửi SMS theo dõi
            $mGasScheduleSms->alertAnything('ToExcel1 08h '.date('d/m').' done at: '.date('Y-m-d H:i:s'), GasConst::$arrAdminUserReal);
            $to = time(); $second = $to-$from;
            Logger::WriteLog("___ExcelCron__All___ ToExcel1Command done in: ".($second).'  Second  <=> '.($second/60).' Minutes done at: '.date('Y-m-d H:i:s'));
        }catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}