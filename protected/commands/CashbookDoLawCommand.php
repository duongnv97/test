<?php
class CashbookDoLawCommand extends CConsoleCommand
{
    // every minutes  * * * * *   root  /usr/bin/php /var/www/spj.daukhimiennam.com/web/cron.php SendScheduleNotify
// 28 16 * * *   root  /usr/bin/php /var/www/spj.daukhimiennam.com/web/cron.php CashbookDoLaw &> /dev/null
    public function run($arg) 
    {
        try { // Dec 02, 2015
            $from   = time();
            $cHours = date('H');
            $mMonitorUpdate = new MonitorUpdate();
            if($cHours < 12 || $cHours > 16){// Dec2918 khung giờ chỉ alert, không tính lỗi
                $mMonitorUpdate->alertOnly = true;
            }
            $mMonitorUpdate->handleErrorBankTransfer();
            $to = time();
            $second = $to-$from;
            $info = "Cron CashbookDoLawCommand  done in {$second}  Second  <=> ".($second/60).' Minutes';
            Logger::WriteLog($info);
        }catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}