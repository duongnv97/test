<?php
class NotifyAndroidCommand extends CConsoleCommand
{
    // every minutes  * * * * *   root  /usr/bin/php /var/www/spj.daukhimiennam.com/web/cron.php NotifyAndroid &> /dev/null
    public function run($arg) 
    {
        try { // Apr 11, 2018
            $mScheduleNotify = new GasScheduleNotify();
            $mScheduleNotify->cronBigQty(UsersTokens::PLATFORM_ANDROID);
        }catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}