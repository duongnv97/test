<?php
class Scan5MinCommand extends CConsoleCommand
{
    // 30 16,23 * * *  root  /usr/bin/php /var/www/spj.daukhimiennam.com/web/cron.php Scan5Min &> /dev/null
    public function run($arg) 
    {
        try {
            // update new to here
            $now            = date('Y-m-d H:i:s');
            $minutesAdd     = 5;
            $nextTime       = MyFormat::addDays($now, $minutesAdd, "+", 'minutes', 'Y-m-d H:i:s');
            Yii::app()->setting->setDbItem('enable_delete', 'no');
            Yii::app()->setting->setDbItem('time5MUpdateSetting', $nextTime);
            
        }catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}