<?php
class UpholdBuildNotifyCommand extends CConsoleCommand
{
    public function run($arg) 
    {
        try { // Dec 02, 2015
            GasUphold::NotifyDinhKyBuildList();
            Logger::WriteLog('Cron UpholdBuildNotifyCommand run: CronCleanNotifyHistory');
            GasScheduleNotifyHistory::CronCleanNotifyHistory();
        }catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}