<?php

class PushServerCommand extends CConsoleCommand
{
    protected $max = 10;
    protected $index = 0;
    protected $data = array();
    protected $config = array(
        'class' => 'morozovsk\websocket\WebsocketDaemonHandler',
//        'pid' => 'pushserver.pid',// tạo file chứa process id. To find PID on Ubuntu => ps ax | grep php
        'pid' => '/tmp/websocket_chat.pid',//  nên tạo ra ở thư mục tmp
        'websocket' => 'tcp://0.0.0.0:8004',
        'localsocket' => 'tcp://127.0.0.1:8010',
        //'master' => 'tcp://127.0.0.1:8020',
        //'eventDriver' => 'event'
    );

    public function actionIndex()
    {
        die("Unknown parameter (start|stop|restart)\r\n");
    }

    public function actionStart()
    {
        require_once __DIR__ . '/PushServer/autoload.php';

        $WebsocketServer = new morozovsk\websocket\Server($this->config);
        call_user_func(array($WebsocketServer, 'start'));
    }

    public function actionStop()
    {
        require_once __DIR__ . '/PushServer/autoload.php';

        $WebsocketServer = new morozovsk\websocket\Server($this->config);
        call_user_func(array($WebsocketServer, 'stop'));
    }

    public function actionRestart()
    {
        require_once __DIR__ . '/PushServer/autoload.php';

        $WebsocketServer = new morozovsk\websocket\Server($this->config);
        call_user_func(array($WebsocketServer, 'restart'));
    }

    public function actionCheck()
    {
        $this->checkNotification();
    }

    public function actionSend($userId, $code, $message)
    {
        if (PushWebSocket::pushCodeToClientToken([])) {
            echo "Sent";
        }
    }

    public function actionAddMessage($userId, $code, $message)
    {
        $model = new GasSocketNotify();
        $model->user_id = $userId;
        $model->code = $code;
        $model->status = GasSocketNotify::STATUS_UNCOMPLETED;
        $model->message = $message;
        if ($model->save(false)) {
            echo "Saved";
        } else {
            var_dump($model->getErrors());
        }
    }

    public static function checkNotification()
    {
        $localsocket = 'tcp://127.0.0.1:8010';

        $instance = stream_socket_client($localsocket, $errno, $errstr);//connect to the web server socket
        PushWebSocket::pushCodeToClientToken(['action' => 'checkNotification']);

//        echo "Websocket service loop is triggered! \n";
    }
}