<?php
class RunAnythingCommand extends CConsoleCommand
{
    public function run($arg) 
    {// * * * * *   root  /usr/bin/php /var/www/spj.daukhimiennam.com/web/cron.php RunAnything &> /dev/null
        try {
            // chú ý bug khi viết Cron: Uid: Exception Property "CConsoleApplication.session" is not defined.
            
//            Logger::WriteLog('Cron CallMoveHistoryCommand run');
//            CronUpdate::updateUserPhoneNetwork();
//            $_POST['CronExcelDataBuvo'] = AppCache::getBuVo();// use for global param in cron have foreach
            
//            $mCareCustomer = new CareCustomer();
//            $mCareCustomer->alertCustomerStopOrder();die('ok1');
            
//            $mIssueTickets = new GasIssueTickets();
//            $mIssueTickets->moveCustomerStopOrderToIssueTickets();
//            $mScheduleNotify = new GasScheduleNotify();
//            $mScheduleNotify->cronBigQty(UsersTokens::PLATFORM_ANDROID);
            
//            UpdateSql1::changePhone11To10();
//            UpdateSql1::changeUsernameKhApp();
//            UpdateSql1::changePhoneOtherTable();
//            $mSetupTeam = new SetupTeam();
//            $mSetupTeam->date_from = '2018-10-01';
//            $mSetupTeam->date_to = '2018-10-30';
//            $mSetupTeam->cronCopyNewMonth();
//            UpdateSql1::ticketFixFlowPunish();
            
//            $mEmployeeProblems =new EmployeeProblems();
//            $mEmployeeProblems->unsetAttributes();
//            $mEmployeeProblems->date_from  = '01-10-2018';
//            $mEmployeeProblems->date_to    = '31-10-2018';
//            $mEmployeeProblems->cronMoveToSalary(); 
//            SellReportQuantity::autoGenData();
//            CronUpdate::removeCustomerOffOrderUnderNpp();
//            UpdateSql1::cronRemoveInactiveUserOfAgent();
//            $mCronScan = new CronScan();
//            $mCronScan->scanCheatCashbook();
//            $this->fixCron1Cron2();
            
//            $today = '2019-08-23';
//            $mGasStoreCardApi = new GasStoreCardApi();
//            $mGasStoreCardApi->cronAutoGenHgd($today);// Add Sep1617 chạy toàn hệ thống 1 lần trong ngày
            
//            $mHrWorkSchedule        = new HrWorkSchedule();
//            $mHrWorkSchedule->cronCustomSchedule(); // Tự động tạo lịch hằng ngày cho 1 số bp
//            ini_set('memory_limit','4500M'); $mCronExcel = new CronExcel();
//            $mCronExcel->cronExcelCustomerHgdApp();
           
            
            
        }catch (Exception $exc){
            // /usr/bin/php /var/www/spj.daukhimiennam.com/web/cron.php RunAnything
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    
    /** @Author: ANH DUNG Jan 06, 2019
     *  @Todo: fix lại cron 1,2 không run hết, phải run lại
     **/
    public function fixCron1Cron2() {
        $today = '2019-01-05';
        $mSellReport            = new SellReport();
        $mIssueTickets          = new GasIssueTickets();
        $mTransactionHistory    = new TransactionHistory();
        $mStaView               = new StaView();
        $mPromotionUser         = new AppPromotionUser();
        $mSetupTeam             = new SetupTeam();


        $mStaView->deleteDuplicate($today);
        $mPromotionUser->cronSetExpired();
        $mTransactionHistory = new TransactionHistory();
        $mTransactionHistory->cronDeleteDraft();

        $mSetupTeam->cronCopyNewMonth();

        $mEmployeeProblems =new EmployeeProblems();
        $mEmployeeProblems->unsetAttributes();
        $mEmployeeProblems->cronMoveToSalary();

        $mSellReportQuantity = new SellReportQuantity();
        $mSellReportQuantity->insertTable($today);
        CronUpdate::removeCustomerOffOrderUnderNpp();
        UpdateSql1::cronRemoveInactiveUserOfAgent();
        $mTargetDaily = new TargetDaily();// Jan0319
        $mTargetDaily->addByDate($today);

        $mEmployeeProblems->updateEmptyAgent($today);

        $mGasScheduleSms = new GasScheduleSms();
        $mGasScheduleSms->alertAnything('Cron1 23h '.date('d/m').' done at: '.date('Y-m-d H:i:s'));

        echo ' --- ok run ---  ';
    }
}