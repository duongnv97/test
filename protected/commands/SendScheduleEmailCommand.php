<?php
class SendScheduleEmailCommand extends CConsoleCommand
{
    public function run($arg)
    {
//        return ;// only for test
        try {
            SendScheduleEmailCommand::doRun();
        }catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Jul 14, 2016
     * @Todo: để gọi hàm send từ 1 function khác
     */
    public static function doRun() {
         /* chay luon ca RunCronSendResetPass trong hàm này
         * vi ham do se chi send khoang luc 2h đêm và send khoảng 30 phút là hết mail=>sẽ không ảnh hưởng
         * cron này sẽ chạy every 10 phút
         */
//        echo Yii::app()->createAbsoluteUrl("admin/gasText/text_view", array('id'=>12));die;
//        Yii::log("Uid: xxxxxx Exception ", 'error');die;
        self::ChangeMail();
        $CountSend = 0; // đếm xem đã gửi dc bao nhiêu email rồi
        $CountSend1 = 0; // đếm xem đã gửi dc bao nhiêu email rồi
        $CountSend2 = 0; // đếm xem đã gửi dc bao nhiêu email rồi
        $CountSend3 = 0; // đếm xem đã gửi dc bao nhiêu email rồi
        $MaxSend = GasScheduleEmail::MAX_SEND;
        GasScheduleEmail::RunCronSendResetPass();
        // Sep 17, 2015 Xử lý cho send văn bản = mail thứ 2 nkhuongminh1@gmail.com
//        Yii::app()->setting->setDbItem('smtpUsername', GasScheduleEmail::MAIL_SECOND);
//        Yii::app()->setting->setDbItem('smtpUsername', GasScheduleEmail::MAIL_PRIMARY);
        // Sep 18, 2015 chưa chạy dc , vì nó đã set smtpUsername khi chạy file index rồi :d
        
        GasScheduleEmail::RunCronSendNotifySupportCustomer( $CountSend1, GasScheduleEmail::MAIL_SUPPORT_CUSTOMER_CHANGE_STATUS, 'NotifySupportCustomer hỗ trợ KH' );
//        SendScheduleEmailCommand::CheckMaxSend($CountSend, $MaxSend);// Sep0417 khong can check cai nay nua
        GasScheduleEmail::RunCronSendTextAlert( $CountSend );
        
        // Jul 10, 2016 Close all bên dưới, vì mới change xử lý gửi hết trong 1 lần find
    }
    
    public static function CheckMaxSend($CountSend, $MaxSend){
        if($CountSend >= $MaxSend){
            die;
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 24, 2015
     * @Todo: đổi email send các loại văn bản + phản ánh sự việc
     * cho email reset pass riêng 1 cái nkhuongminh1@gmail.com
     */
    public static function ChangeMail() {
        return ; // Sep 24, 2016 đổi sang send = no-reply.spj.vn không cần phải change kiểu này nữa
//        Yii::app()->setting->setDbItem('smtpUsername', GasScheduleEmail::MAIL_SECOND); // only for test
//        return ;// only for test
        $cHours = date('H');
        if($cHours > 3 && $cHours < 22 ) { // Từ 3h sáng đến (cả ngày) tối sẽ dùng mail nkhuongminh@gmail.com để send 500 email
            Yii::app()->setting->setDbItem('smtpUsername', Yii::app()->setting->getItem('email_cron_primary'));
//            Yii::app()->setting->setDbItem('smtpUsername', GasScheduleEmail::MAIL_PRIMARY);
//            Yii::app()->setting->setDbItem('smtpUsername', GasScheduleEmail::MAIL_THIRD); // Oct 23, 2015
        }elseif($cHours > 22 ){// Từ 1h->3h sáng sẽ dùng mail nkhuongminh1@gmail.com để send reset pass
            Yii::app()->setting->setDbItem('smtpUsername', Yii::app()->setting->getItem('email_cron_reset_pass'));
//            Yii::app()->setting->setDbItem('smtpUsername', GasScheduleEmail::MAIL_SECOND);
        }
    }
}