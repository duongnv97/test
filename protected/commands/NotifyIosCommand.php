<?php
class NotifyIosCommand extends CConsoleCommand
{
    // every minutes  * * * * *   root  /usr/bin/php /var/www/spj.daukhimiennam.com/web/cron.php NotifyIos &> /dev/null
    public function run($arg) 
    {
        try { // Apr 11, 2018
            $mScheduleNotify = new GasScheduleNotify();
            $mScheduleNotify->cronBigQty(UsersTokens::PLATFORM_IOS);
        }catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}