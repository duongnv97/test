<?php

namespace morozovsk\websocket;

use ApiModule;

class WebsocketDaemonHandler extends \morozovsk\websocket\Daemon
{
    public $connectionList = [];
    public $userList = [];
    public $super_user = null;
    const SUPER_ADMIN_CODE = '@super_admin@'; // Lệnh để nâng quyền admin
    const SUPER_LIST_USER = '1'; // Lệnh để nâng quyền admin
    const SUPER_LIST_TOKEN = '2'; // Lệnh để nâng quyền admin
    private $disableVerify = false;

    // Sử dụng cho verify code
    const key = "ds3Dk3pidsfcPd";

    // Used for check debug log or not
    private $DEBUG = true;
    // Used for lock action notification check
    private $lockActionNotification = false;

    protected function onOpen($connectionId, $info)
    {  //Call when connecting to a new client
        try{
        $message = 'New user #' . $connectionId . ' : ' . var_export($info, true) . ' ' . stream_socket_get_name($this->clients[$connectionId], true);
        $this->logConsoleDebug($message . "\n");

        // Gửi đến super user
        if (isset($this->super_user) && $this->super_user) {
            $this->sendToClient($this->super_user, $message);
        }

        $info['GET'];//or use $info['Cookie'] for use PHPSESSID or $info['X-Real-IP'] if you use proxy-server like nginx
        parse_str(substr($info['GET'], 1), $_GET);//parse get-query

        if ($this->disableVerify && isset($_GET['user_id'])) {
            // Chỉ thêm những user được xác nhận
            $this->connectionList[$connectionId] = $_GET['user_id'];
            $this->buildUserList($connectionId, $_GET['user_id']);
//            $this->userList[$_GET['user_id']] = $connectionId;
            return;
        } else if (isset($_GET['huongminh_token']) && isset($_GET['user_id'])) {
            $criteria = new \CDbCriteria;
            $criteria->compare('user_id', $_GET['user_id']);
            $criteria->compare('token', $_GET['huongminh_token']);
            $userToken = \UsersTokens::model()->find($criteria);
            if ($userToken) {
                // Chỉ thêm những user được xác nhận
                $user_id = $_GET['user_id'];
                if(isset($_GET['agent_id']) && !empty($_GET['agent_id'])){
                    $user_id = $_GET['agent_id'];
                }
                
                $this->connectionList[$connectionId] = $user_id;
                $this->buildUserList($connectionId, $user_id);
//                $this->userList[$_GET['user_id']] = $connectionId;
                return;
            }
            $aInfo = array('connection_id'=>$connectionId);
//            $this->sendToClient($connectionId, json_encode($aInfo));
        } else if (isset($_GET['SpjOnlineUser'])) {// Dec 04,2 016 xử lý get user online
            $message = json_encode($this->connectionList, JSON_UNESCAPED_UNICODE);
            $this->sendToClient($connectionId, $message);
            return ;
        }
        
        // User truyen params khong hop le
        $this->sendToClient($connectionId, "User truyen params khong hop le");
        //$this->close($connectionId);
        }catch (Exception $exc){
            \GasCheck::CatchAllExeptiong($exc);
        }
    }
    
    /**
     * @Author: ANH DUNG Oct 03, 2016
     * @Todo: 1 user có nhiều $connectionId, để gửi notify to multiuser
     */
    public function buildUserList($connectionId, $user_id) {
        $this->userList[$user_id][$connectionId] = $connectionId;
    }

    protected function onClose($connectionId)
    {//It is called when the connection is closed with an existing client
        if (isset($this->connectionList[$connectionId])) {
            $userId = $this->connectionList[$connectionId];

            // Remove both connection list and user list
            unset($this->connectionList[$connectionId]);
            if(isset($this->userList[$userId])){
                unset($this->userList[$userId][$connectionId]);
                if(count($this->userList[$userId]) < 1){
                    unset($this->userList[$userId]);
                }
            }
        }

        // Xóa super user
        if (isset($this->super_user) && $this->super_user == $connectionId) {
            unset($this->super_user);
        }
    }

    protected function onMessage($connectionId, $data, $type)
    {// Called when a message is received from the client
        try{
        if (!strlen($data)) {
            return;
        }
        // Xoa bo ky tu html va php
        $data = strip_tags($data);

        // Thêm người dùng admin
        if ($data == self::SUPER_ADMIN_CODE) {
            $this->super_user = $connectionId;

            $message = 'Bạn đã đăng ký thàng công super admin';
            $this->sendToClient($this->super_user, $message);
            return;
        }

        if ($connectionId == $this->super_user) {
            $message = '';
            // Super admin gửi message
            if ($data == self::SUPER_LIST_USER) {
                $message = "Số lượng người sử dụng hiện tại là: " . count($this->connectionList);
            } elseif ($data == self::SUPER_LIST_TOKEN) {
                $message = "Danh sách token: " . json_encode($this->connectionList) . '\n';
                $message .= "Danh sách user: " . json_encode($this->userList);
            }
            $this->sendToClient($this->super_user, $message);
        } else {
            // User gửi message
            if ($this->super_user) {
                $message = 'SuperUser: User gửi message #' . $connectionId . ' : ' . $data;
                $this->sendToClient($this->super_user, $message);
            }
        }
        }catch (Exception $exc){
            \GasCheck::CatchAllExeptiong($exc);
        }
    }

    protected function onServiceMessage($connectionId, $data)
    {
        try{
        // Gửi thông tin cho super user
        if (isset($this->super_user)) {
            $this->sendToClient($this->super_user, 'Phát hiện server gửi: '.date("Y-m-d H:i:s")."  " . $data);
        }
        $data = json_decode($data);

        if (isset($data->action)) {
            // Check action
            if ($data->action == 'checkNotification') {
                $this->checkNotification();
            } else if ($data->action == 'toggleDebugMode') {
                $this->toggleDebugMode();
            }
        }
        }catch (Exception $exc){
            \GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * @Author: TRUNG Jul 25 2016
     * Decode mã xác nhận để đảm bảo code được gửi từ server hướng minh
     * @param $verifyCode
     * @return string
     */
    private function decodeVerifyCode($verifyCode)
    {
        $uuid = $this->decrypt($verifyCode, self::key);
        return $uuid;
    }

    /**
     * Returns an encrypted & utf8-encoded
     */
    function encrypt($pure_string, $encryption_key)
    {
        $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $encryption_key, utf8_encode($pure_string), MCRYPT_MODE_ECB, $iv);
        $encrypted_string = base64_encode($encrypted_string);
        return $encrypted_string;
    }

    /**
     * Returns decrypted original string
     */
    function decrypt($encrypted_string, $encryption_key)
    {
        $encrypted_string = base64_decode($encrypted_string);
        $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, $encryption_key, $encrypted_string, MCRYPT_MODE_ECB, $iv);
        return $decrypted_string;
    }

    /**
     * @Author: TRUNG Jul 25 2016
     * Từ mã xác nhận đã được giải mã. Kiểm tra code có được thỏa mãn
     * @param $token
     * @param $verifyCode
     */
    private function verifyValidCode($token, $verifyCode)
    {
        try {
            if (!empty($token) && strpos($token . ":", $verifyCode) == 0) {
                date_default_timezone_set('Asia/Ho_Chi_Minh');
                $dateObj = \DateTime::createFromFormat("Y-m-d h-i-sa", substr($verifyCode, strlen($token . ":")));
                if ($dateObj) {
                    if ($dateObj->getTimestamp() + 60 > time()) { // Cho phép trong vòng 1 phút
                        var_dump(true);
                        return true;
                    }
                }
            }
        } catch (\Exception $ex) {
        }
        return false;
    }

    /**
     * Check notification forever
     */
    private function checkNotification()
    {
        try{
        if ($this->lockActionNotification == false) {
            $this->lockActionNotification = true;
            
            $this->logConsoleDebug("Loop: Check notification \n");
            $listUncompleted = \GasSocketNotify::getUncompletedNotify(array_keys($this->userList));
            $aCodeDelete = [\GasSocketNotify::CODE_ALERT_CALL_CENTER, \GasSocketNotify::CODE_CALL_EVENT, \GasSocketNotify::CODE_CALL_EVENT_MONITOR, \GasSocketNotify::CODE_APP_ORDER_NEW, \GasSocketNotify::CODE_APP_ORDER_CONFIRM];
            /** @var \GasSocketNotify $item */
            foreach ($listUncompleted as $item) {
                try {
                    $data = ApiModule::$defaultSuccessResponse;
                    $json = json_decode($item->json, true);
                    $data['record']['code']             = $item->code;
                    $data['record']['obj_id']           = $item->obj_id;
                    $data['record']['id']               = $item->remote_id;
                    $data['record']['server_io_notify_id']     = $item->id;
                    $data['record']['msg']              = $item->message;
                    $data['record']['created_date']     = $item->getCreatedDate();
                    $data['record']['agent_name']       = isset($json['agent_name']) ? $json['agent_name'] : '';
                    $data['record']['call_uuid']        = isset($json['call_uuid']) ? $json['call_uuid'] : '';
                    $data['record']['parent_call_uuid'] = isset($json['parent_call_uuid']) ? $json['parent_call_uuid'] : '';
                    $data['record']['child_call_uuid']  = isset($json['child_call_uuid']) ? $json['child_call_uuid'] : '';
                    $data['record']['call_temp_id']     = isset($json['call_temp_id']) ? $json['call_temp_id'] : '';
                    $data['record']['call_status']      = isset($json['call_status']) ? $json['call_status'] : '';
                    $data['record']['destination_number'] = isset($json['destination_number']) ? $json['destination_number'] : '';

                    $result = $this->findAndSendToClient($item->user_id, $data);// Close on Mar 13, 2017 Move xuống dưới, cho cập nhật xong rồi send xuống client
                    if ($result && in_array($item->code, $aCodeDelete)) {// Close on Now 29, 2016
                        $item->delete(); // Jun 14, 2017 chắc sẽ delete luôn, không cần update nữa,
//                        \GasSocketNotify::markSentNotify($item->id);// Close on Jun 14, 2017 không cần update nữa, xóa luôn ( vì chưa làm được phần send luôn, ko cần save vào db nữa)
//                        $this->logConsoleDebug("Check notification sent notify id: {$item->id}\n");
                    }
//                    $result = $this->findAndSendToClient($item->user_id, $data); nếu move xuống dưới thì bị error Undefined variable: result
                    
                } catch (\Exception $ex) {
                    echo 'Error when check notification' . $ex->getMessage();
                }
            }

            $this->lockActionNotification = false;
        }
        }catch (Exception $exc){
            \GasCheck::CatchAllExeptiong($exc);
        }
    }

    /** Log debug data to console
     * @param $string
     */
    private function logConsoleDebug($string)
    {
        if ($this->DEBUG) {
            echo $string;
        }
    }

    /**
     * @param $userId
     * @param $jsonData
     * @return string
     */
    protected function findAndSendToClient($userId, $jsonData)
    {
        if(!isset($this->userList[$userId])){
            return false;
        }
        $ok = false;
        foreach($this->userList[$userId] as $currentConnectionId){
            // Server dang push code, truyền code về client
            $sendMessage = json_encode($jsonData);
            $this->sendToClient($currentConnectionId, $sendMessage);

            if (isset($this->super_user)) {
                $this->sendToClient($this->super_user, "ConnectID: $currentConnectionId TimeSent: ".date("Y-m-d H:i:s")." Message gửi là: $sendMessage");
            }
            $ok = true;
        }
        return $ok;
    }

    /**
     * On/Off debug mode
     */
    private function toggleDebugMode()
    {
        if ($this->DEBUG) {
            $this->logConsoleDebug("Debug mode will be off!");
        }
        $this->DEBUG = !$this->DEBUG;

        if ($this->DEBUG) {
            $this->logConsoleDebug("Debug mode is on!");
        }
    }
}