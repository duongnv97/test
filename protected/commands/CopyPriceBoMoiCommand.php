<?php
class CopyPriceBoMoiCommand extends CConsoleCommand
{
    public function run($arg) 
    {
        try { // Cron chạy vào 3h sáng ngày 01 hàng tháng
            $mAppCache          = new AppCache();
            $mUsersPrice        = new UsersPrice();
            $mUsersPriceOther   = new UsersPriceOther();
            
            $currentMonth       = date('m');
            $currentYear        = date('Y');
            
            $mUsersPrice->copyPriceGByMonthYear(date('m'), date('Y'));
            $mAppCache->setPriceHgd(AppCache::PRICE_HGD);// Mar0718, đưa vào đây để đảm bảo ngày đầu tháng luôn cập nhật giá mới của HGD trong cache
            $mUsersPriceOther->cronCopyOldPrice($currentMonth, $currentYear); // cron copy gia con, chay vao 3h ngay dau thang
//            $mUsersExtend = new UsersExtend();// Apr 01, 2017 chuyển sang báo giá lúc 11h trưa để anh Hiếu nhận khoán sửa giá
//            $mUsersExtend->makeQuotes();// nguy hiểm ko muốn chạy tự động, chắc phải chạy = tay quá, vì nó sẽ send SMS luôn
        }catch (Exception $exc)
        {
            GasCheck::CatchAllExeptiong($exc);
        }
    }
}