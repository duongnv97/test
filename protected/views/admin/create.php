<?php
$this->breadcrumbs=array(
	'Admins'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Admin', 'url'=>array('Admin/')),
	array('label'=>'Create Admin', 'url'=>'', 'active'=>true),
);
?>

<h1>Create Admin</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>