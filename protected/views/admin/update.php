<?php
$this->breadcrumbs=array(
	'Admins'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Admin', 'url'=>array('Admin/')),
	array('label'=>'View Admin', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Create Admin', 'url'=>array('create')),
	array('label'=>'Update Admin', 'url'=>'', 'active'=>true),
);
?>

<h1>Update Admin <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>