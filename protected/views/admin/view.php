<?php
$this->breadcrumbs=array(
	'Admins'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Admin', 'url'=>array('Admin/')),
	array('label'=>'View Admin', 'url'=>'', 'active'=>true),
	array('label'=>'Create Admin', 'url'=>array('create')),
	array('label'=>'Update Admin', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Admin', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>View Admin #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'username',
		'email',
		'password',
		'fullname',
		'mric',
		'mcr',
		'contact_number',
		'contact_name',
		'address',
		'post_code',
		'zone',
		'created_date',
		'application_id',
		'role_id',
		'last_login',
		'last_login_ip',
		'status',
		'deleted',
	),
)); ?>
