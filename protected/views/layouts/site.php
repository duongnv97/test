<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
     <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <link rel="shortcut icon" type="image/ico" href="<?php echo Yii::app()->theme->baseUrl; ?>/favicon.ico" />
    <link rel="apple-touch-icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/images/favicon.png" />
     
<!--<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/slide/css/nivo-style.css" type="text/css" />-->
<!--<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/slide/css/nivo-slider.css" type="text/css" />-->
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/cssmenuvertical3/cssmenuvertical.css" rel="stylesheet" type="text/css" media="screen" /> 
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css" type="text/css" />
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/mobile_admin.css" />

<?php Yii::app()->clientScript->registerCoreScript('jquery'); //jQuery JavaScript Library v1.8.3 ?>
<!--<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/slide/js/jquery.nivo.slider.pack.js"></script>-->
</head>

<body>
    <div id="page">
        <header id="branding" role="banner">
            <div id="wrapper" style="display: none;">
                
            </div>
        </header>
        <div id="main">
            <!--<section id="primary">-->
                <div id="content" role="main">
                    <?php echo $content; ?>
                </div>                
            <!--</section>-->
        </div>
    </div>
</body>

</html>

<script type="text/javascript">
        $(document).ready(function() {

        });
</script>