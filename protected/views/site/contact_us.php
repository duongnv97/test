<section>
        <h1 class="title"><strong>Contact Us</strong></h1>
    <div class="content document">
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'contact-us-form',
            'enableClientValidation' => true,
            'enableAjaxValidation' => false,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
            'htmlOptions'=>array(
                'class'=>'jNice booking contact',
            ),
        )); ?>
            <div class="contact-form">
                <div class="member">
                        <div class="row">
                        <label for="name"><strong>Your name</strong></label>
                        <?php echo $form->textField($model,'name', array('maxlength'=>50, 'class'=>'text-1')); ?>
                        <?php echo $form->error($model,'name'); ?>
                    </div>
                        <div class="row">
                        <label for="email"><strong>Your email</strong></label>
                        <?php echo $form->textField($model,'email', array('maxlength'=>50, 'class'=>'text-1')); ?>
                        <?php echo $form->error($model,'email'); ?>
                    </div>
                </div><!-- //member -->
                <div class="row">
                    <label for="message"><strong>Message</strong></label>
                    <?php echo $form->textArea($model,'message', array('class'=>'text-3 text-message', 'cols'=>10,'rows'=>5)); ?>
                    <?php echo $form->error($model,'message'); ?>
                </div>
                <div class="button">
                        <button value="Submit" class="" type="submit" name="" id=""><span><span><span>Submit</span></span></span></button>
                </div>
            </div>
    		<div class="map" style="float:left;padding-left:40px;">
    			<?php
                    $search_result_cms = Cms::model()->findByAttributes(array('id'=>4));
                    echo $search_result_cms->cms_content;
                ?>
    		</div>
            <?php if($success !=''):?>
                <div class="text text_emergencies">
                    <?php echo $success; ?>
                </div>
            <?php endif; ?>
            <div class="clear"></div>                            
        <?php $this->endWidget(); ?>
    </div>
</section>