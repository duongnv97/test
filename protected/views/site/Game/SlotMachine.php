<?php 
    $date       = date('Y-m-d'); $mSignup = new AppSignup();
    $aLogin     = $mSignup->getFirstLoginByDate($date);
    $countItem  = isset($aLogin['UID']) ? count($aLogin['UID']) : 0;// số item li - tương đương với bao nhiêu số phone
    $str        = ''; $index = 1;
    $aData      = isset($aLogin['DATA']) ? $aLogin['DATA'] : [];
    foreach($aData as $uid => $aInfo):
        if(is_null($aInfo)){
            continue;
        }
        $index ++ ;
        $phone = UsersPhone::formatPhoneView($aInfo->phone);
        $str .= "<li class='number'><p>$phone</p></li>";
    endforeach;
?>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/GameApp/spj_game.css" rel="stylesheet" /> 
<div>
    <div class="logo">
        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/css/GameApp/logo_ver_en.png" alt=""/>
    </div>
    <div class="wrap-body">
        <div class="wrap-box1">
             <div class="item-box2">
                 <h4 style="margin-top: -64px;margin-bottom: 30px;">Quay số may mắn <?php echo $countItem;?> số điện thoại App</h4>
                 <div id="viewbox">
                     <div class="wrapper" id="slotmachine">
                        <ul>
                            <?php echo $str;?>
                        </ul>
                     </div>
                     <a href="javascript:;" class="show-button btn-start" id="start">start</a>
                 </div>
               <!--<div class="number"><p>0987 568 698</p><button class="btn-start">Random</button></div>-->
             </div>
         </div>
    </div>
</div>    
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.easing.1.3.js"></script>
<script type="text/javascript">
	$( ".show-button" ).click(function() {
	  $( ".show-button" ).toggle();
	});
	$(document).ready(function () {

    // Clone the first element and append it to the end of the list
    var list = $('#slotmachine>ul:first');
    var firstItem = list.find('li:first');
    firstItem.clone().appendTo(list);
    var listHeight = <?php echo $countItem;?> * 200;

    function moveTo(val) {
        val = -val % listHeight;
        if (val > 0) val -= listHeight;
        $('#slotmachine').css({
            top: val
        });
    }

    function spin(count) {
        $('#slotmachine').stop().animate({
            top: -listHeight
        }, 2000, 'linear', function () {
            if (count == 0) {
                var slot = Math.floor(Math.random() * 6),
                    top = -slot * 200,
                    time =  2000 * slot / 6;
                console.log(count, slot, top, time)
                $(this).css({
                    top: 0
                }).animate({
                    top: top
                },time, 'easeOutQuad')
            } else {
                $(this).css({
                    top: 0
                })
                spin(count - 1)
            };
        });
    }
    $('#start').click(function () {
        $('#slotmachine').css({
            top: 0
        }) 
        spin(3)
        // 10 cai khoang 22 second tùy theo chiều dài số đt nữa
    });

    $('#moveTo').click(function () {
        moveTo($('#pos').val());
    });
});
</script>

