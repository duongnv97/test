<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>
<link href="spj_game.css" rel="stylesheet" /> 
<body>
    <div class="logo">
        <img src="logo_ver_en.png" alt=""/>
    </div>
    <div class="box-start">
        
        <div class="stopwatch"></div>
        <ul class="results"></ul>
        <button class="btn-start show-button over-button"  onClick="stopwatch.start();">Start</button>
    </div>
    </div>
   <div class="wrap-box-wrap">
    <div class="wrap-box1">
	    <div class="item-box1">
        	<h4>Cài đặt App</h4>
            <div class="info-box1">
           	 <div class="step"><span>Bước 1</span> Tải App</div>
             <div class="step-style2"><span>B1</span> Tải App</div>
             <p>- Tải app <strong style="color:red">Gas24h</strong> trên <strong>AppStore</strong> hoặc <strong>Google Play</strong> ( từ khóa:gas service, daukhimiennam, dat gas ...) hoặc</p>
			<p>- Tải bằng QR Code</p>
            <div class="step"><span>Bước 2</span>Đăng ký</div>
            <p>- Dùng số điện thoại để đăng ký tài khoản</p>
            <p>- Nhập mã  Pin từ tin nhắn SMS để hoàn tất</p>
             <div class="step"><span>Bước 3</span>Đặt gas</div>
             <p>- Đặt gas thành công để nhận phần thưởng</p>
            </div>
            <div class="number"><nav class="controls">
            <a href="#" class="button" onClick="stopwatch.start();">Start</a>
            <!--<a href="#" class="button" onClick="stopwatch.lap();">Lap</a>-->
            <a href="#" class="button" onClick="stopwatch.stop();">Stop</a>
            <a href="#" class="button" onClick="stopwatch.restart();">Restart</a>
            <!--<a href="#" class="button" onClick="stopwatch.clear();">Clear Laps</a>-->
        
    </div>
    	</div>
         <div class="item-box1 mid-box">
         <h4 style="text-align:center">số người tải thành công</h4>
         <div class="number-sc">
          598
         </div>
         <div class="loading">
             
             <img src="loading5.gif" alt="">
         </div>
         </div>
         <div class="item-box1">
         <h4>kết quả</h4>
         	<ol>
            <li>
                <p class="phone">0987 568 698 <span>04:25:23</span></p>
            </li>
             <li>
                <p class="phone">0987 568 698 <span>04:25:23</span></p>
            </li>
             <li>
                <p class="phone">0987 568 698 <span>04:25:23</span></p>
            </li>
             <li>
                <p class="phone">0987 568 698 <span>04:25:23</span></p>
            </li>
             <li>
                <p class="phone">0987 568 698 <span>04:25:23</span></p>
            </li>
             <li>
                <p class="phone">0987 568 698 <span>04:25:23</span></p>
            </li>
             <li>
                <p class="phone">0987 568 698 <span>04:25:23</span></p>
            </li>
             <li>
                <p class="phone">0987 568 698 <span>04:25:23</span></p>
            </li>
             <li>
                <p class="phone">0987 568 698 <span>04:25:23</span></p>
            </li>
             <li>
                <p class="phone">0987 568 698 <span>04:25:23</span></p>
            </li>
            
            </ol>
         </div>
    </div>
   </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript">
        class Stopwatch {
    constructor(display, results) {
        this.running = false;
        this.display = display;
        this.results = results;
        this.laps = [];
        this.reset();
        this.print(this.times);
    }
    
    reset() {
        this.times = [ 0, 0, 0 ];
    }
    
    start() {
        if (!this.time) this.time = performance.now();
        if (!this.running) {
            this.running = true;
            requestAnimationFrame(this.step.bind(this));
        }
    }
    
    lap() {
        let times = this.times;
        let li = document.createElement('li');
        li.innerText = this.format(times);
        this.results.appendChild(li);
    }
    
    stop() {
        this.running = false;
        this.time = null;
    }

    restart() {
        if (!this.time) this.time = performance.now();
        if (!this.running) {
            this.running = true;
            requestAnimationFrame(this.step.bind(this));
        }
        this.reset();
    }
    
    clear() {
        clearChildren(this.results);
    }
    
    step(timestamp) {
        if (!this.running) return;
        this.calculate(timestamp);
        this.time = timestamp;
        this.print();
        requestAnimationFrame(this.step.bind(this));
    }
    
    calculate(timestamp) {
        var diff = timestamp - this.time;
        // Hundredths of a second are 100 ms
        this.times[2] += diff / 10;
        // Seconds are 100 hundredths of a second
        if (this.times[2] >= 100) {
            this.times[1] += 1;
            this.times[2] -= 100;
        }
        // Minutes are 60 seconds
        if (this.times[1] >= 60) {
            this.times[0] += 1;
            this.times[1] -= 60;
        }
    }
    
    print() {
        this.display.innerText = this.format(this.times);
    }
    
    format(times) {
        return `\
${pad0(times[0], 2)}:\
${pad0(times[1], 2)}:\
${pad0(Math.floor(times[2]), 2)}`;
    }
}

function pad0(value, count) {
    var result = value.toString();
    for (; result.length < count; --count)
        result = '0' + result;
    return result;
}

function clearChildren(node) {
    while (node.lastChild)
        node.removeChild(node.lastChild);
}

let stopwatch = new Stopwatch(
    document.querySelector('.stopwatch'),
    document.querySelector('.results'));
        $( ".show-button" ).click(function() {
          $( ".show-button" ).toggle();
        });
    </script>
</body>
</html>