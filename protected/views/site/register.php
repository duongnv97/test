<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/passwordStrengthMeter.js"></script>
<div class="form">
<section>
<h1 class="title"><strong>User Register</strong></h1>
    <div class="content" >
        <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'register-form',
                //'enableAjaxValidation'=>false,
                //'enableClientValidation'=>true,
                'htmlOptions'=>array('class'=>'jNice booking'),
                'clientOptions' => array(
                        /*
                        'validateOnSubmit' => true,
                        'validateOnChange' => true,
                        'validateOnType' => true,
                        //'beforeValidateAttribute'=>'js:function(form, attribute){alert("working");}'
                        //'beforeValidateAttribute'=>'js:function(form, attribute){ alert(1)}',
                        //'afterValidateAttribute' => 'js:function(form, attribute, data, hasError){ alert("workss"); }'
                         * 
                         */
                ),
            
        )); ?>
        
                <div style="float: left; margin-right: 40px;width: 230px;">
                    <?php echo CHtml::image(Yii::app()->theme->baseUrl.'/images/img-login.png');?>
                </div>    
            
            <div class="group-3" style="float: left;">
                <div class="not-member" style="display: block;">
                        <p class="note"><strong>Join as a member</strong></p>
                        <?php echo $form->errorSummary($model); ?>
                    <div class="row">
                        <label><strong>Full name</strong></label>
                        <input type="text" class="text-5" maxlength="100" name="Users[first_name]" id="Users_first_name" value="<?php echo !empty($model->first_name)? $model->first_name.' '.$model->last_name:'';?>" />
                        <div class="ajaxValidateSuccess"></div>
                        <div class="ajaxValidateError"></div>
                        <div class="errorMessage" style="display:none">Full name is required</div>
                    </div>
                    <div class="row">
                        <label><strong>Username</strong></label>
                        <input type="text" class="text-5" maxlength="100" name="Users[username]" id="Users_username" value="<?php echo !empty($model->first_name)? $model->username:'';?>" />
                        <div class="ajaxValidateSuccess"></div>
                        <div class="ajaxValidateError"></div>
                        <div class="errorMessage" style="display:none">Username is required</div>
                    </div>
                    <div class="row" style="display: none;">    
                        <label><strong>Gender</strong></label>
                        <div class="number">
                        <select name="Users[gender]">
                            <option <?php if($model->gender=='MALE') echo 'selected="selected"';?> value="MALE">Male</option>
                            <option <?php if($model->gender=='FEMALE') echo 'selected="selected"';?> value="FEMALE">Female</option>
                        </select>
                        </div>   
                    </div>   
                    <div class="row">
                        <label for="email"><strong>My Email</strong></label>        
                        <input type="text" class="text-5" maxlength="100" name="Users[email]" id="Users_email" value="<?php echo $model->email;?>"/>
                        <div class="ajaxValidateSuccess"></div>
                        <div class="ajaxValidateError"></div>
                        <div class="errorMessage" style="display:none">Email is required</div>
                    </div>
                    <div class="row">
                        <label for="confirm_email"><strong>Confirm my email</strong></label>
                        <input type="text" class="text-5" maxlength="100" name="Users[confirm_email]" id="Users_confirm_email" value="<?php echo $model->email;?>"/>
                        <div class="ajaxValidateSuccess"></div>
                        <div class="ajaxValidateError"></div>
                        <div class="errorMessage" style="display:none">Confirm email is required</div>
                    </div>
                        <div class="text sub-text-1 email_verify_code_hide" style="border: none; margin-bottom: 12px;">
                        <p>We need to contact you to confirm this registration.</p>
                        <p>A SMS with an authentication code will be sent to you.</p>
                        <p>You have to key this authentication code into the next screen page to confirm this registration.</p>
                    </div>
                        
                    <div class="row resize_slt_jnice">
                        <label><strong>My mobile phone number</strong></label>
                        <div class="number">
                            <?php echo AreaCode::sltAreaCode('Users_code_phone', ActiveRecord::getDefaultAreaCode(),true); ?>
                        </div>
                         <input type="text" class="text-2 only_number" maxlength="20" name="Users[phone]" id="Users_phone" value="<?php echo $number_phone;?>"/>
                        <div class="ajaxValidateSuccess"></div>
                        <div class="ajaxValidateError"></div>
                        <div class="errorMessage" style="display:none;margin-left: 190px;"></div>
                    </div>
                        
                    <div class="row sms_confirm_phone_number_hide">
                        <label><strong>Confirm my mobile phone number</strong></label>
                        <div class="number">
                            <?php echo AreaCode::sltAreaCode('Users_code_phone_verify', ActiveRecord::getDefaultAreaCode(),true); ?>
                        </div>
                        <input type="text" class="text-2 only_number" maxlength="100" name="Users[phone_verify]" id="Users_phone_verify" value="<?php echo $number_phone;?>"/>
                        <div class="ajaxValidateSuccess"></div>
                        <div class="ajaxValidateError"></div>
                        <div class="errorMessage" style="display:none;margin-left: 100px;">Retype mobile phone number</div>
                    </div>
                        
                        <div class="row">
                        <label for="reg_password"><strong>Password <em>( minimum 6 characters )</em></strong></label>
                        <?php //echo $form->passwordField($model,'password_hash',array('class'=>'text-5')); ?>
                        <input type="password" class="text-5" maxlength="<?php echo ActiveRecord::getMaxLengthPassword();?>" name="Users[password_hash]" id="Users_password_hash" />
                        <div class="ajaxValidateSuccess"></div>
                        <div class="ajaxValidateError"></div>
                        <div style="color:green;float: left;width: 100%;" id='result'></div> 
                        <div class="errorMessage" style="display:none;">Password is required</div>
                        <div id="passwordhelp_pop" style="display: none;" class="helppop">
                            <div class="helpctr" id="passwordhelp"><p>Strong passwords contain 7-16 characters, do not include common words or names, and combine uppercase letters, lowercase letters, numbers, and symbols.</p></div>
                        </div>
                    </div>
                        <div class="row">
                        <label for="reg_confirm_password"><strong>Confirm my password</strong></label>
                        <input type="password" id="Users_confirm_password" class="text-5" name="Users[confirm_password]" maxlength="<?php echo ActiveRecord::getMaxLengthPassword();?>" />
                        <div class="ajaxValidateSuccess"></div>
                        <div class="ajaxValidateError"></div>
                        <div class="errorMessage" style="display:none;">Retype password</div>
                    </div>

                </div>
                <div class="term-content document">
                    <?php $model = Cms::model()->findAll('slug="'.ActiveRecord::getSlugTermsOfUse().'" ORDER BY created_date ASC'); 
                        if(count($model)>0)
                            echo $model[0]->cms_content;
                    ?>
                </div>
                
                <div class="row check_terms">
                    <div style="padding-top: 10px;">
                        <input type="checkbox" id="accept" name="Users[accept]" /><label for="accept">I have read and accept 65doctor
                            <a href="<?php echo Yii::app()->createAbsoluteUrl('member/site/view_cms_slug/slug/'.ActiveRecord::getSlugTermsOfUse());?>" target="_blank">Terms of Use</a>.</label>
                    </div>
                </div>
                <div class="button">
                    <input type="submit" value="Register" id="submit-reg" />
                </div>
            </div>
          <div class="clear"></div>         
          
        <?php $this->endWidget(); ?>
        </div>
</section>
</div>

<style>

div.form div.success input,
div.form div.success textarea,
div.form div.success select,
div.form input.success,
div.form textarea.success,
div.form select.success
{
	background: #E6EFC2;
	border-color: #C6D880;
}


div.form .errorSummary
{
	border: 2px solid #C00;
	padding: 7px 7px 12px 7px;
	margin: 0 0 20px 0;
	background: #FEE;
	font-size: 0.9em;
}
div.form .errorMessage
{
	color: red;
	font-size: 0.9em;
}
div.form .errorSummary p
{
	margin: 0;
	padding: 5px;
}
div.form .errorSummary ul
{
	margin: 0;
	padding: 0 0 0 20px;
}
    
</style>

<script type="text/javascript">
$(document).ready(function(){
    
    $('#Users_password_hash').keyup(function(){$('#result').html(passwordStrength($('#Users_password_hash').val(),$('#Users_first_name').val()))})
//    $('.resize_slt_jnice').find('.jNiceSelectWrapper').css({ 'width': 235 });
//    $('.resize_slt_jnice').find('.jNiceSelectText').css({ 'width': 230 });
//    $('.resize_slt_jnice').find('.jNiceSelectWrapper').find('ul').css({ 'width': 230 });
//    $('.resize_slt_jnice').find('.jNiceSelectWrapper').find('ul').find('li').css({ 'width': 230 });
//    $('.resize_slt_jnice').find('.number').css({ 'width': 240 });

        validateNumber();
//        if($('#reg_prefer_email').is(':checked')){
//            $('.email_verify_code_hide').show();
//        }
//        if($('#reg_sms').is(':checked')){
//            $('.sms_confirm_phone_number_hide').show();
//        }


    
    
    $('#submit-reg').unbind("click").click(function(){
        var checkE=true;
        $('.errorMessage').each(function(){
            if($(this).css('display')=='block')
                checkE=false; 
        });

        $('#register-form').unbind("submit").bind('submit',function(){
            if(myValidateForm() && checkE)
                return true;
            return false;
        });
    });
    
//    $('#reg_prefer_email').unbind('click').bind('click', function(){
//        $('.email_verify_code_hide').show();
//        $('.sms_confirm_phone_number_hide').hide();
//        $('.sms_confirm_phone_number_hide').find('.errorMessage').hide();
//    });
//    $('#reg_sms').unbind('click').bind('click', function(){
//        $('.email_verify_code_hide').hide();
//        $('.sms_confirm_phone_number_hide').show();
//    });
    
    
    $('#Users_confirm_email').blur(function(){
        var msg_require = 'Confirm email is required';
        var msg_confirm = 'Confirm email don\'t match';
        if($.trim($(this).val())==''){
            $(this).parent('div').find('.errorMessage').show().text(msg_require);
            $(this).parent('div').find('.ajaxValidateError').show();
            return false;
        }else{
            if($(this).val()!= $('#Users_email').val() ){
                $(this).parent('div').find('.ajaxValidateSuccess').hide();
                $(this).parent('div').find('.errorMessage').show().text(msg_confirm);
                $(this).parent('div').find('.ajaxValidateError').show();
                return false;
            }else{
                $(this).parent('div').find('.errorMessage').hide();
                $(this).parent('div').find('.ajaxValidateError').hide();
                $(this).parent('div').find('.ajaxValidateSuccess').show();
            }
            return true
        }
        
    });
    
    $('#Users_phone_verify').blur(function(){
            if($(this).val().length==0){
                $(this).parent('div').find('.errorMessage').show().text('Retype mobile phone number');
                $(this).parent('div').find('.ajaxValidateError').show();
                $(this).parent('div').find('.ajaxValidateSuccess').hide();
                return false;
            }else{
                if(($(this).val()+ $('#Users_code_phone_verify').val()) != ( $('#Users_phone').val()+$('#Users_code_phone').val() ) ){
                    $(this).parent('div').find('.errorMessage').show().text('Retype mobile phone number');
                    $(this).parent('div').find('.ajaxValidateError').show();
                    $(this).parent('div').find('.ajaxValidateSuccess').hide();
                    return false;
                }else{
                    $(this).parent('div').find('.errorMessage').hide();
                    $(this).parent('div').find('.ajaxValidateError').hide();
                    $(this).parent('div').find('.ajaxValidateSuccess').show();
                    return true
                }
            }
        
    });
    
    $('#Users_password_hash').blur(function(){
        if($(this).val().length==0){
            $(this).parent('div').find('.errorMessage').show().text('Password is required');
            $(this).parent('div').find('.ajaxValidateError').show();
            return false;
        }
        if($(this).val().length<6){
            $(this).parent('div').find('.errorMessage').show().text('Password at least 6 characters');
            $(this).parent('div').find('.ajaxValidateError').show();
            return false;
        }
        $(this).parent('div').find('.errorMessage').hide();
        $(this).parent('div').find('.ajaxValidateError').hide();
        $(this).parent('div').find('.ajaxValidateSuccess').show();
        $('#Users_confirm_password').trigger('blur');
        
    });
            
    $('#Users_confirm_password').blur(function(){
        var passw = $('#Users_password_hash').val();
        var confirmPassw = $(this).val();
        if(passw!=confirmPassw || $('#Users_password_hash').val().length==0 ){
            $(this).parent('div').find('.errorMessage').show().text('These password don\'t match');
            $(this).parent('div').find('.ajaxValidateError').show();
            $(this).parent('div').find('.ajaxValidateSuccess').hide();
            return false;
        }
        $(this).parent('div').find('.errorMessage').hide();
        $(this).parent('div').find('.ajaxValidateError').hide();
        $(this).parent('div').find('.ajaxValidateSuccess').show();
        return true
    });        

    $('#Users_first_name').blur(function(){
        if($(this).val().length==0 || $.trim($(this).val())==''){
            $(this).parent('div').find('.errorMessage').show().text('Name is required');
            $(this).parent('div').find('.ajaxValidateError').show();
            $(this).parent('div').find('.ajaxValidateSuccess').hide();
            return false;
        }else{
            $(this).parent('div').find('.errorMessage').hide();
            $(this).parent('div').find('.ajaxValidateError').hide();
            $(this).parent('div').find('.ajaxValidateSuccess').show();
            return true
        }
    });        
    
    
    $('#Users_email,#Users_confirm_email,#Users_phone, #Users_phone_verify,#Users_password_hash,#Users_confirm_password,#Users_first_name').keydown(function(){
        $(this).parent('div').find('.errorMessage').hide();
        $(this).parent('div').find('.ajaxValidateSuccess').hide();
        $(this).parent('div').find('.ajaxValidateError').hide();
    });
    
    
    /*+++++ begin Mail validate +++++*/    
    $('#Users_email').blur(function(){
        var email = $(this).val();
        if($.trim($(this).val())=='') { 
            $(this).parent('div').find('.errorMessage').show();
            $(this).parent('div').find('.ajaxValidateError').show();
            return false;}
        $('#Users_confirm_email').trigger('blur');
        //var css = $(this).parent('div').find('.errorMessage').css('display');
        // begin ajax	
        var url_ = "<?php echo Yii::app()->createAbsoluteUrl('member/site/ajax_check_mail');?>";
            $.ajax({
                    url: url_,
                    data: "email="+email,
                    type: "post",
                    dataType: "json",
                    async: false,
                    success: function(data){
                            if(data['success']){
                                $('#Users_email').parent('div').find('.ajaxValidateSuccess').show();
                                $('#Users_email').parent('div').find('.errorMessage').hide();
                                $('#Users_email').parent('div').find('.ajaxValidateError').hide();
                                }else{
                                    $('#Users_email').parent('div').find('.errorMessage').show().text(data['msg']);
                                    $('#Users_email').parent('div').find('.ajaxValidateError').show();
                                    $('#Users_email').parent('div').find('.ajaxValidateSuccess').hide();
                            }
                    }
            });
        // end ajax
    });

    /*+++++ end  Mail validate +++++*/    
    /*+++++ begin phone validate +++++*/    
    //$('#Users_phone,#Users_code_phone').blur(function(){
    $('#Users_phone,#Users_code_phone').blur(function(){
        var phone = $.trim($('#Users_phone').val());
        var area_code_id = $('#Users_code_phone').val();


        if(phone=='') {
            $('#Users_phone').parent('div').find('.errorMessage').show().text('The mobile phone number is required');
            $('#Users_phone').parent('div').find('.ajaxValidateError').show();
            $('#Users_phone').parent('div').find('.ajaxValidateSuccess').hide();
            return false};
        //var css = $(this).parent('div').find('.errorMessage').css('display');
        // begin ajax	
        var url_ = "<?php echo Yii::app()->createAbsoluteUrl('member/site/ajax_check_phone');?>";
            $.ajax({
                    url: url_,
                    data: {phone:phone,area_code_id:area_code_id},
                    type: "post",
                    dataType: "json",
                    async: false,
                    success: function(data){
                            if(data['success']){
                                $('#Users_phone').parent('div').find('.ajaxValidateSuccess').show();
                                $('#Users_phone').parent('div').find('.errorMessage').hide();
                                $('#Users_phone').parent('div').find('.ajaxValidateError').hide();
                                }else{
                                $('#Users_phone').parent('div').find('.errorMessage').show().text(data['msg']);
                                $('#Users_phone').parent('div').find('.ajaxValidateError').show();
                                $('#Users_phone').parent('div').find('.ajaxValidateSuccess').hide();
                            }

                    }
            });
        // end ajax
        $('#Users_phone_verify').trigger('blur');
    });

    /*+++++ end  phone validate +++++*/    
    
});

    function myValidateForm(){
        var res = true;
        if($('#Users_first_name').val().length==0 || $.trim($('#Users_first_name').val())==''){
            $('#Users_first_name').parent('div').find('.errorMessage').show().text('Name is required');
            $('#Users_first_name').parent('div').find('.ajaxValidateError').show();
            res = false;
        }
        if($.trim($('#Users_confirm_email').val())==''){
            $('#Users_confirm_email').parent('div').find('.errorMessage').show().text('Confirm email is required');
            $('#Users_confirm_email').parent('div').find('.ajaxValidateError').show();
            res = false;
        }
        
        if($.trim($('#Users_phone_verify').val())==''){
            $('#Users_phone_verify').parent('div').find('.errorMessage').show().text('Retype mobile phone number');
            $('#Users_phone_verify').parent('div').find('.ajaxValidateError').show();
            res = false;
        }
    
        if($('#Users_confirm_password').val().length==0){
            $('#Users_confirm_password').parent('div').find('.errorMessage').show().text('Retype password');
            $('#Users_confirm_password').parent('div').find('.ajaxValidateError').show();
            res = false;
        }
        if($.trim($('#Users_email').val())==''){
            $('#Users_email').parent('div').find('.errorMessage').show().text('Email is required');
            $('#Users_email').parent('div').find('.ajaxValidateError').show();
            res = false;
        }
        if($.trim($('#Users_phone').val())==''){
            $('#Users_phone').parent('div').find('.errorMessage').show().text('The mobile phone number is required');
            $('#Users_phone').parent('div').find('.ajaxValidateError').show();
            res = false;
        }
        if(!$('#accept').attr('checked')){
            $('.check_terms').css("border", "1px solid red");
            res = false;
        }else
            $('.check_terms').css("border", "none");
        
        return res;
    }

    function checkEmailClient() {
        var email = $('#Users_email').val();
        if(email.length==0) return false;
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(email)) {
            //alert('Please provide a valid email address');            
            return false;
        }
        return true;
    }


    function validateNumber(){
		$(".only_number").each(function(){
			$(this).unbind("keydown");
			$(this).bind("keydown",function(event){
			    if( !(event.keyCode == 8                                // backspace
			        || event.keyCode == 46                              // delete
			        || event.keyCode == 9							// tab
			        || event.keyCode == 190							// dáº¥u cháº¥m (point) 
			        || (event.keyCode >= 35 && event.keyCode <= 40)     // arrow keys/home/end
			        || (event.keyCode >= 48 && event.keyCode <= 57)     // numbers on keyboard
			        || (event.keyCode >= 96 && event.keyCode <= 105))   // number on keypad
			        ) {
			            event.preventDefault();     // Prevent character input
			    	}
			});
		});
    }

    function fnUserPhoneChange(this_){
        $(this_).parent('div').parent('div').parent('div').find('.errorMessage').hide();
        $(this_).parent('div').parent('div').parent('div').find('.ajaxValidateError').hide();
        $(this_).parent('div').parent('div').parent('div').find('.ajaxValidateSuccess').hide();
    
        var phone = $.trim($('#Users_phone').val());
        var area_code_id = $('#Users_code_phone').val();
        
        if($('#Users_phone').val().length==0) return false;
        // begin ajax	
        var url_ = "<?php echo Yii::app()->createAbsoluteUrl('member/site/ajax_check_phone');?>";
            $.ajax({
                    url: url_,
                    data: {phone:phone,area_code_id:area_code_id},
                    type: "post",
                    dataType: "json",
                    async: false,
                    success: function(data){
                            if(data['success']){
                                $('#Users_phone').parent('div').find('.ajaxValidateSuccess').show();
                                }else{
                                $('#Users_phone').parent('div').find('.errorMessage').show().text(data['msg']);
                                $('#Users_phone').parent('div').find('.ajaxValidateError').show();
                            }
                            $('#Users_phone_verify').trigger('blur');
                    }
            });
        // end ajax
    }
    
    function fnUserPhoneConfirmChange(this_){
            $(this_).parent('div').parent('div').parent('div').find('.errorMessage').hide();
            $(this_).parent('div').parent('div').parent('div').find('.ajaxValidateError').hide();
            $(this_).parent('div').parent('div').parent('div').find('.ajaxValidateSuccess').hide();

            if($('#Users_phone').val().length==0) return false;

            var phone_no = $('#Users_phone').val();
            var phone_code = $('#Users_code_phone').val();

            var phone_no_verify = $('#Users_phone_verify').val();
            var phone_code_verify = $('#Users_code_phone_verify').val();

            if(phone_no!=phone_no_verify){
                $(this_).parent('div').parent('div').parent('div').find('.errorMessage').show().text('Retype mobile phone number');
                $(this_).parent('div').parent('div').parent('div').find('.ajaxValidateError').show();
                return false;
            }   

            if(phone_code!=phone_code_verify){
                $(this_).parent('div').parent('div').parent('div').find('.errorMessage').show().text('Retype mobile phone number');
                $(this_).parent('div').parent('div').parent('div').find('.ajaxValidateError').show();
                return false;
            }
            $(this_).parent('div').parent('div').parent('div').find('.ajaxValidateSuccess').show();    
    }

</script>
