<link href="/themes/gas/admin/css/screen.css" rel="stylesheet" type="text/css" media="screen"/>
<link rel="stylesheet" href="/themes/gas/admin/css/huongminh.css"/>
<script type="text/javascript" src="/themes/gas/js/huongminh.js"></script>
<?php
$dataProvider   = $model->search();
$needMore       = array("listAgent"=>$dataProvider->data);
$mGateApi       = new GateApi();
$js_array       = $mGateApi->getLocationAgent($needMore);
?>
<?php $this->widget('AgentMapWidget', array('needMore' => $needMore)); ?>
<style>
    #page {
        max-width: 100%;
        max-height: 100%;
    }
    #map {
        width: 100% !important;
    }
    #main {
        width: 100% !important;
    }
    #bodyContent {
        text-align: left;
    }
    #content {
        padding: 0;
    }
</style>
