<div class="join-tempt wrapper clearfix">				
    <div class="box-1 form register-form fixie success" style="width: 100%;">
        <p>You have registered successfully.</p>
        <p>Now you can <?php echo CHtml::link('login',Yii::app()->createAbsoluteUrl('site/login'));?> to your account</p>        
    </div>
</div>

<style>
.wrapper .register-form p {
    color: #8B8B8B;
    font-size: 19px;
    padding-bottom: 10px;
}
    
</style>