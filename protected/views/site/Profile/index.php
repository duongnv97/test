<head>
    <title>Công ty CP Dầu Khí Miền Nam</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <meta charset="UTF-8">
    <meta name="theme-color" content="#e21118">
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style-profile.css" />
    
</head>
<div class="gridProfile">
    <div class="logo">
        <!--<img src="image/logo.png" alt="" />-->
        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo-profile.png" alt="" />
    </div>
<div class="gridProfile__item">
    <div class="w-image">
        <!--<div class="image"><img src="image/avatar.jpeg" alt="" /></div>-->
        <!--<div class="image"><img src="<?php //echo Yii::app()->theme->baseUrl; ?>/images/avatar.jpeg" alt="" /></div>-->
        
        <?php if(!empty($model->mUsersRef->image_sign)): ?>
        <a class="gallery" href="<?php echo ImageProcessing::bindImageByModel($model->mUsersRef,'','',array('size'=>'size2'));?>">
            <img src="<?php echo ImageProcessing::bindImageByModel($model->mUsersRef,'','',array('size'=>'size1'));?>">
        </a>
        <?php endif;?>
        
    </div>

    <div class="info">
        <h4> <?php echo $model->first_name; ?></h4>
        <h5></h5>
        <p><strong>MNV: <?php echo $model->code_account; ?></strong></p>
    </div>
</div>
</div>

