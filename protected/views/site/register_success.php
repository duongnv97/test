<div class="join-tempt wrapper clearfix">				
    <div class="box-1 form register-form fixie success" style="width: 100%;">
        <?php $aError = $model->getErrors(); if(isset($aError['email'])): ?>
            <p>Can not send email to: <?php echo $model->email;?></p>
        <?php else:?>
        <p>Thank you for your registered with billionpoll.com</p>
        <p>You can login to your account on Billionpoll when you received our confirmed email</p>        
        <p><a href="<?php echo Yii::app()->baseUrl; ?>"> Back to homepage</a></p>
        <?php endif; ?>
    </div>
</div>

<style>
.wrapper .register-form p {
    color: #8B8B8B;
    font-size: 19px;
    padding-bottom: 10px;
}
    
</style>