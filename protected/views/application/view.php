<?php
$this->breadcrumbs=array(
	'Applications'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Application', 'url'=>array('Application/')),
	array('label'=>'View Application', 'url'=>'', 'active'=>true),
	array('label'=>'Create Application', 'url'=>array('create')),
	array('label'=>'Update Application', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Application', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>View Application #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'front_end',
		'back_end',
	),
)); ?>
