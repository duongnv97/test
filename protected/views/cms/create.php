<?php
$this->breadcrumbs=array(
	'Cms'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Cms', 'url'=>array('Cms/')),
	array('label'=>'Create Cms', 'url'=>'', 'active'=>true),
);
?>

<h1>Create Cms</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>