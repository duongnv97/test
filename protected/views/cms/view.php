<?php
$this->breadcrumbs=array(
	'Cms'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Cms', 'url'=>array('Cms/')),
	array('label'=>'View Cms', 'url'=>'', 'active'=>true),
	array('label'=>'Create Cms', 'url'=>array('create')),
	array('label'=>'Update Cms', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Cms', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>View Cms #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'content',
		'created_date',
		'display_order',
		'show_in_menu',
		'position',
		'creator_id',
		'published',
		'short_content',
		'link',
		'meta_keywords',
		'meta_desc',
		'url',
	),
)); ?>
