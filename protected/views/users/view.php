<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Users', 'url'=>array('Users/')),
	array('label'=>'View Users', 'url'=>'', 'active'=>true),
	array('label'=>'Create Users', 'url'=>array('create')),
	array('label'=>'Update Users', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Users', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>View Users #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'username',
		'email',
		'password',
		
		'nric',
		'mcr',
		'contact_number',
		'contact_name',
		'address',
		'post_code',
		'zone',
		'created_date',
		'application_id',
		'role_id',
		'last_login',
		'last_login_ip',
		'status',
		'deleted',
	),
)); ?>
