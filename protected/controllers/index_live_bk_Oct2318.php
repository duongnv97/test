<?php
try {
	error_reporting(0); // tat loi
	// error_reporting(E_ALL & ~ E_NOTICE); // khong ro cai nay tat loi gi, cu de vay da
	date_default_timezone_set('Asia/Ho_Chi_Minh');
	// change the following paths if necessary
	define('DS', DIRECTORY_SEPARATOR);
	define('PS', PATH_SEPARATOR);
	define('ROOT', dirname(__FILE__));
	define('YII_PROTECTED_DIR',  ROOT . DS . 'protected');
	define('YII_THEMES_DIR',  ROOT . DS . 'themes');
	define('YII_UPLOAD_DIR',  ROOT . DS . 'upload');
	define('YII_UPLOAD_FOLDER', 'upload');

	$yii=dirname(__FILE__).'/yii-framework-1.1.13/yii.php';
	$config=dirname(__FILE__).'/protected/config/main.php';

	// remove the following lines when in production mode
	define('YII_DEBUG', true);
	// specify how many levels of call stack should be shown in each log message
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

	require_once($yii);
	Yii::createWebApplication($config);
	SettingForm::applySettings();//override settings by values from database
	Yii::app()->run();

} catch (Exception $exc) { // ANH DUNG Dec 26, 2014
    if(strpos($exc->getMessage(), "Can't connect to local MySQL server") ){
	echo "Server can not connect. Vui long cho 5 phut, neu sau 5 phut khong vao duoc WEB ban lien he voi Nguyen Tien Dung: 01684 331 552 Hoac Nguyen Ngoc Kien: 0988 180 386";
	sleep(60); // for InnoDB: Starting crash recovery. mysql-wont-start-innodb-database-was-not-shut-down-normally
	$Handle = fopen("/tmp/rebootvps", 'w');// Aug 21,20216 xử lý reboot khi Sql crash
        fwrite($Handle, "doreboot");
        fclose($Handle);
        die;
    }else{
        throw new CHttpException(404, $exc->getMessage());
    }
}
