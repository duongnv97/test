<?php

class siteController extends Controller
{
    public $attempts = 2;
    public $counter;
    public function init() {
//        GasCheck::CheckIpOtherCountryV2FeOnlyDie();
        return parent::init();
    }
    
    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                    'class'=>'CCaptchaAction',
                    'backColor'=>0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                    'class'=>'CViewAction',
            ),
        );
    }
  
    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        $this->redirect('underConstruction');
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }
    
    public function actionUnderConstruction(){
echo '<h1>Server s1</h1>';
//        if(Yii::app()->params['server_maintenance']=='no')
//            Yii::app()->controller->redirect(Yii::app()->createAbsoluteUrl('admin/site/index'));
        $this->pageTitle = 'Hệ thống không truy cập';
        $this->render('underconstruction');
    }
     
    /** @Author: ANH DUNG Oct 19, 2017
     *  @Todo: short game for app
     */
    public function actionSpjGame(){
        $this->resetPhone();
         if(isset($_GET['SlotMachine'])){
             $this->pageTitle = 'Quay số may mắn';
            $this->render('Game/SlotMachine');
         }else{
             $this->pageTitle = 'Game App';
            $this->render('Game/App');
         }
    }

    /**
     * @Author: ANH DUNG Oct 19, 2017
     * @Todo: xóa những số phone đã Trúng giải
     */
    public function resetPhone() {
        if(isset($_GET['remove'])){
            $aPhoneRemove = ['01265147686', '0908118790'];
            $criteria = new CDbCriteria();
            $criteria->compare('type', AppSignup::TYPE_SIGNUP);
            $sParamsIn = implode(',', $aPhoneRemove);
            $criteria->addCondition("phone IN ($sParamsIn)");
            $aUpdate = array('type' => AppSignup::TYPE_FORGOT_PASS);
            AppSignup::model()->updateAll($aUpdate, $criteria);
            echo 'done'; die;
        }
    }
    /** @Author: Pham Thanh Nghia Oct 8, 2018
     *  @Todo: site/profile/code_account/HMDL002 
     **/
    public function actionProfile($code_account = null){
        $this->layout = "";
        if(is_null($code_account)){
            $this->render('underconstruction');
        }
        try{
            $this->pageTitle = "Profile";
            $criteria = new CDbCriteria;
            $criteria->compare('code_account', $code_account);
            $model = Users::model()->find($criteria);
            if($model){
                $model->LoadUsersRefImageSign();
                $this->render('Profile/index',array(
                    'model'=>$model, 
                ));
            }else{
                $this->pageTitle = 'Hệ thống không truy cập';
                $this->render('underconstruction');
            }
        }catch (Exception $exc){
            GasCheck::CatchAllExeptiong($exc);
        }
    }

    /**
     * @author LocNV Oct 04, 2019
     */
    public function actionAgent()
    {
        try {
            $this->pageTitle = "Quy hoạch";
            $model=new Users('search');
            $model->unsetAttributes();  // clear any default values
            $model->role_id = ROLE_AGENT;
            $this->render('Map/index', array(
                'model' => $model
            ));
        } catch (Exception $exc){
        GasCheck::CatchAllExeptiong($exc);
    }
    }
   
}
