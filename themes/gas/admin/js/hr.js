    /*
     * This js is apply to HR
     */
    
    function fnBindCreateCoefficientsColorbox(){
        $('.create a').colorbox({
            iframe: true,
            overlayClose: false, escKey: false,
            innerHeight: '400',
            innerWidth: '600', close: '<span title=close>close</span>',
            onClosed: function () { // update view when close colorbox
                var role = $('#tabs>ul>li.active>a').data('role');
                $.fn.yiiGridView.update('Coefficients-grid'+role);
            }
        });
    }
    
    function fnBindCreateParametersColorbox(){
        $(".create a").colorbox({
            iframe: true,
            overlayClose: false, escKey: false,
            innerHeight: '400',
            innerWidth: '600', close: "<span title='close'>close</span>",
            onClosed: function () { // update view when close colorbox
                var role = $('#tabs>ul>li.active>a').data('role');
                $.fn.yiiGridView.update('Parameters-grid'+role);
            }
        });
    }
    
    //DRAG n DROP
    //cài đặt cho phép drop
    function allowDrop(ev) {
        ev.preventDefault();
    }
    
    //kéo
    function drag(ev, element, field, toClass) {
        if ($(element).hasClass('alreadyIn')) {
            toClass = '';
        }
        ev.originalEvent.dataTransfer.setData("source", $(ev.target).text());//lấy id của phần tử đang drag và gán vào biến source
        ev.originalEvent.dataTransfer.setData("data_id", $(ev.target).data(field));//lấy id của phần tử đang drag và gán vào biến source
        ev.originalEvent.dataTransfer.setData("toClass", toClass);
    }
    
    //Work schedule
    function wsdrag(ev, element, field, toClass) {
        $(element).attr('id','dragging'); //element is $(this)
        $(element).parent().attr('id','dragging_container');
        ev.originalEvent.dataTransfer.setData("color_class",$(ev.target).attr('class'));
        ev.originalEvent.dataTransfer.setData("data_id",$(ev.target).data('shift_id')) ;
        ev.originalEvent.dataTransfer.setData("data_name",$(ev.target).data('shift_name')) ;
        ev.originalEvent.dataTransfer.setData("toClass",toClass) ;
        
    }
    
    //thả
    function drop(ev, element, toClass, format, autoIncrease) {
        var ts = $(element).children("span").length + 1;
        ev.preventDefault();
        var elmData = ev.originalEvent.dataTransfer.getData('source');//lấy giá trị biến source và gán vào biến elm_id
        var dropAreaClass = ev.originalEvent.dataTransfer.getData('toClass');
        var data_id = ev.originalEvent.dataTransfer.getData('data_id');
        if (dropAreaClass !== toClass)
            return;
        if (elmData === "")
            return;
        $selector = 'span[data-id=' + data_id + ']';
        if ($(element).find($selector).length <= 0) {
            currentTrDrop = $(element).data('current');
            $(element).append("<span class='btnDefault' data-id=" + data_id + ">" + format + ts + ": " + elmData + "<input class='display_none' name='HrFunctions[function][" + currentTrDrop + "][" + format + "][]' value ='" + data_id + "'></span>");
        }

        if (!elmData) {
            return
        }
        $(".dropErr").text("");
    }
    
    // Workschedule drop
    function wsdrop(ev, element, toClass, format, autoIncrease) {
        var ts = $(element).children("span").length + 1;
        ev.preventDefault();
        var color_class = ev.originalEvent.dataTransfer.getData('color_class');//lấy giá trị biến source và gán vào biến elm_id
        var dropAreaClass = ev.originalEvent.dataTransfer.getData('toClass');
        var data_shift_id = ev.originalEvent.dataTransfer.getData('data_id');
        var data_shift_name = ev.originalEvent.dataTransfer.getData('data_name');
        var data_date = $(ev.target).data('date');
        var data_employee = $(ev.target).data('employee');
        //swap if a drag b
        if($('#dragging').hasClass('alreadyIn')){ // #dragging is element which being dragging
            $('#dragging').remove();
            $(element).children().clone().appendTo('#dragging_container'); // #dragging is element contain #dragging
            var oldShift = $('#dragging_container').children('.shift_cell').data('shift_id');
            var oldDate = $('#dragging_container').data('date');
            var oldEmp = $('#dragging_container').data('employee');
            $('#dragging_container').children('.shift_cell').attr('data-date',oldDate);
            $('#dragging_container').children('.shift_cell').attr('data-employee',oldEmp);
            var aOldData = [oldShift, oldDate, oldEmp];
            var oldData = JSON.stringify(aOldData);
            $('#dragging_container').find("input[name='HrWorkSchedule[data][]']").val(oldData).removeClass('unmodify');
        } else {
            $('#dragging').removeAttr('id');
        }
        $('#dragging_container').removeAttr('id');
        
        var pos = color_class.search("shift_color");
        var color_class = color_class.substring(pos, pos+14);

//        if(dropAreaClass !== toClass) return;
        if(color_class === "")return;
        $selector = 'span[data-id='+data_shift_id+']';
        if($(element).find($selector).length <= 0){
            $(element).empty();
            currentTrDrop = $(element).data('current');
            var aData = [data_shift_id, data_date, data_employee];
            var data = JSON.stringify(aData);
            var html_epd = "<div class='shift_container shift_cell "+ color_class +
                            "' data-shift_id="+ data_shift_id +" data-shift_name='"+data_shift_name+"' data-date="+data_date+" data-employee="+data_employee+">"+data_shift_name+
                            "<input type='hidden' name='HrWorkSchedule[data][]' value ='"+data+"'>"+
                            "</div>";
            $(element).append(html_epd);
        }

        if(!color_class){return}
        $(".dropErr").text("");
    }
    
    //drag phần từ trong ô ra ngoài thì xóa phần tử đó (ko áp dụng cho ô input)
    function dropOutsideToDelete(){
        $(document).on("dragend",".alreadyIn" , function(event){
            //var mouse = (typeof event === 'undefined') ?   MouseEvent : event;
            $parent = $(this).parent();
            if(event.originalEvent.dataTransfer.dropEffect === 'none'){
                $('#dragging_container').removeAttr('id');
                $(this).remove();
            }
            $i = 1;
            $parent.find('.alreadyIn').each(function(){
                $(this).html($(this).html().replace(/\d+/,$i++));
            });
        });
    }
    
    function initDrag(fromClass, toClass){
        $("." + fromClass).addClass("dragItem");
        $(document).on("mousedown", ".alreadyIn, ." + fromClass, function () {
            $("." + fromClass).attr("draggable", "true");
            $(".alreadyIn").attr("draggable", "true");
        })
        //kéo phần từ qua vùng cho phép drop thì hiện dấu cho phép drop
        $(document).on("dragover", "." + toClass, function (event) {
            allowDrop(event);
        });
    }
    
    /*allowDrag("fromClass", "toClass", "field", "data format", "auto increase")
    * field: "id", "class", "text" (default), "data-*" (vd: "data-value")
    * data format là ký tự dc thêm vào đầu đoạn text của phần tử drag
    * auto inscrease = true(default) or false    
    * function drag từ fromClass đến toClass và tên trường (id, class, text, data-*)
    */
    function allowDrag(fromClass, toClass, field = "id", format = "", autoIncrease = "false") {
        initDrag(fromClass, toClass);
        $(document).on("dragstart", ".alreadyIn, ." + fromClass, function (event) {
            drag(event, this, field, toClass);
        });
        $(document).on("drop", "." + toClass, function (event) {
            if ($(this).hasClass(toClass)) {
                $("." + toClass).addClass("dragTarget");
                drop(event, this, toClass, format, autoIncrease);
                $(this).children().addClass("alreadyIn"); //dấu hiệu thêm class alreadyIn để phân biệt cái nào đã được drop vào rồi
            }
        });
        dropOutsideToDelete();
    }
    
    //Workschedule 
    function wsallowDrag(fromClass, toClass, field = "id", format="", autoIncrease="false"){
        initDrag(fromClass, toClass);
        $(document).on("dragstart",".alreadyIn, ."+fromClass,function(event){
            wsdrag(event, this, field, toClass);
        });
        $(document).on("drop","."+toClass,function(event){
            if($(this).hasClass(toClass)){
                $("."+toClass).addClass("dragTarget");
                wsdrop(event, this, toClass, format, autoIncrease);
                $(this).children().addClass("alreadyIn"); //dấu hiệu thêm class alreadyIn để phân biệt cái nào đã được drop vào rồi
            }
        });
        //drag phần từ trong ô ra ngoài thì xóa phần tử đó (ko áp dụng cho ô input)
        dropOutsideToDelete();
    }//--END DRAG n DROP
    
    function removeRow(){
        $('.remove_icon_only').off('click').on('click',function(){
            if(confirm('Bạn chắc chắn muốn xóa?')){
                $(this).closest('tr').remove();
            }
        });
    }
    
    function scrollTable(){
        $(function() {
          $('#tbl_work_schedule tbody').scroll(function(e) { //detect a scroll event on the tbody
            var left = $("#tbl_work_schedule tbody").scrollLeft();
            $('#tbl_work_schedule thead').css("left", -left); //fix the thead relative to the body scrolling
//            $('#tbl_work_schedule thead th:nth-child(1)').css("left", left); //fix the first cell of the header
//            $('#tbl_work_schedule thead th:nth-child(2)').css("left", left);
//            $('#tbl_work_schedule thead th:nth-child(3)').css("left", left);
            $('#tbl_work_schedule thead th.fix-col').css("left", left);
            
//            $('#tbl_work_schedule tbody td:nth-child(1)').css("left", left); //fix the first column of tdbody
//            $('#tbl_work_schedule tbody td:nth-child(2)').css("left", left);
//            $('#tbl_work_schedule tbody td:nth-child(3)').css("left", left);
            $('#tbl_work_schedule tbody td.fix-col').css("left", left);
          });
        });
    }
    
    /*HrSalaryReport*/
    function fnBinClickCheckbox(){
        $('#check-all-role-btn').on('click', function(){
            var roleCheckbox = $('.role-checkbox');
            var userCheckbox = $('.user-checkbox');
            roleCheckbox.prop("checked", !roleCheckbox.prop("checked"));
            userCheckbox.prop("checked", !userCheckbox.prop("checked"));
        })
        $('.role_container').on('click',function(e){
            var slt = $(this).siblings('.user-container');
            if(e.target.nodeName == "INPUT"){ //check all user in role
                var crStt = $(e.target).prop("checked");
                var checkBoxes = $(this).siblings('.user-container').find("input.user-checkbox");
                if(crStt){ //if check
                    checkBoxes.prop("checked", crStt);
                } else { //not check
                    checkBoxes.prop("checked", crStt);
                }

                return 0;
            }
            $(this).toggleClass('anm');
                slt.toggle(300);
                return 0;
        })
        //Auto check role when user in role is checked
        $('.user-checkbox').on('click',function(e){
            var crStt = $(this).prop('checked');
            var role_ctn    = $(this).parents('.user-container').siblings('.role_container');
            var user_ctn    = $(this).parents('.user-container');
            var countCheck  = 0;
            // find if one user is checked, role is indeterminate
            for( var i = 0; i < user_ctn.children('.user-name').length; i++){
                if(user_ctn.children('.user-name').eq(i).children('.user-checkbox').prop('checked')){
                    countCheck++;
                    role_ctn.find('input.role-checkbox').prop('indeterminate', true);
                }
            }
            //if all user is checked, role check
            if(countCheck == user_ctn.children('.user-name').length){
                role_ctn.find('input.role-checkbox').prop('indeterminate', false);
                role_ctn.find('input.role-checkbox').prop('checked', true);
            }
            //if no user is checked, role uncheck
            if(countCheck == 0){
                role_ctn.find('input.role-checkbox').prop('indeterminate', false);
                role_ctn.find('input.role-checkbox').prop('checked', false);
            }
        });
    }
    
    /**
     * make an ajax request
     * @param {type} url ajax url in controller
     * @param {type} objData data to send
     * @param {type} elm element to assign
     * @returns html
     */
    function ajaxRequest(url, objData, elm, afterSuccess = false, func = '', sParams = ''){
        $.blockUI();
        $.ajax({
            'url': url,
            'data': objData,
            'success':function(html){
                elm.html(html);
                if(afterSuccess){
                    if(sParams == ''){
                        func();
                    } else {
                        func(sParams);
                    }
                }
                $.unblockUI();
            },
        });
    }
    
    function disableBtn(){
        if($('.rp-empty-item-ctn').length == 0){
            $('.calTotalSalaryBtn').removeAttr('disabled');
        } else {
            $('.calTotalSalaryBtn').attr('disabled', 'disabled');
        }
    }
    
    /**
     * @todo toggle a column
     * @params: index index of column, start at 0
     * @params: tableElement table selector have column to hide
     */
    function toggleColumn(index, tableElement){
        var header = tableElement.find('thead tr th');
        var body   = tableElement.find('tbody tr');
        for(var i = 0; i < body.length; i++){ // toggle body column with index
            body.eq(i).find('td').eq(index).toggle();
        }
        header.eq(index).toggle(); // toggle header with index
        
        if(tableElement.find('tr').outerWidth() < 1000){
            for(var i = 0; i < header.length; i++){
                var firstColWid = tableElement.find('tbody tr td').eq(i).outerWidth();
                tableElement.find('thead tr th').eq(i).css('width', firstColWid);
            }
        }
    }
    
    function resizeTable(tableContainer){
//        var tableElement = $('table.reportTable');
        var tableElement = tableContainer.find('table.reportTable');
        var header       = tableElement.find('thead tr th');
        if(tableElement.find('tr').outerWidth() < 1000){
            for(var i = 0; i < header.length; i++){
                var firstColWid = tableElement.find('tbody tr td').eq(i).outerWidth();
                tableElement.find('thead tr th').eq(i).css('width', firstColWid);
            }
        }
    }
    
    function fnBindShowHideTableColumn(){
       //@todo hide a column
       $(document).on('click', '.remove-icon', function(){
           var index         = $(this).closest('th').index();
           var table         = $(this).closest('.reportTable');
           toggleColumn(index, table);
           /* Show panel on above table when click hide icon */
           var listHideItem  = table.siblings('.listHideItem');
           var header        = table.find('thead tr th');
           var contentHeader = header.eq(index).text();
           listHideItem.prepend('<div class="reshow-icon" data-index=' + index + '>' + contentHeader + '</div>');
       });
        //@todo show a column
       $(document).on('click', '.reshow-icon', function(){
           var index   = $(this).data('index');
           var table   = $(this).closest('.listHideItem').siblings('.reportTable');
           toggleColumn(index, table);
           $(this).remove();
       });
    }
    
        /*
     * scroll đến tháng hiện tại ở tab đã chốt
     */
//    $(document).on('click', '.final-tab', function(){
//        var d = new Date();
//        var currentMonth = d.getMonth();
//        var slt = $($(this).attr('href')).find('.month.list-report');
//        setTimeout(function() {
//            if(!slt.find('li').hasClass('final-active')){
//                slt.find('li[data-month="'+currentMonth+'"]').click();
//                slt.animate({scrollTop:(currentMonth)*44+15}, 300, 'swing');
//            }
//        }, 300);
//    });
    
    function fnBindLoadReportType(url){
        $('.tabLink').on('click', function(e){
            var type = $(e.target).data('type');
            var idx  = $(this).parent().index() + 1;
            var elm  = $('#tabs>div.tab-content>#tabstabLink'+idx);
            if($.trim($($(e.target).attr('href')).html()) != ''){
                return 0;
            }
            var objData = {
                'type': type
            };
            ajaxRequest(url, objData, elm);
            
        });
    }
    
    function fnBinhLoadReportContent(url){
        $(document).on('click', '.ajaxBtn', function(e){
            $(this).siblings(".ajaxBtn.active").removeClass('active');
            $(this).addClass('active');
            var type        = $('#tabs>ul>li.active>a.tabLink').data('type');
            var reportElm   = $(e.target).closest('li.ajaxBtn');
            var action      = reportElm.data('action');
            var id          = reportElm.data('id');
            var code_no     = reportElm.data('code_no');
            var elm         = $(this).closest('.tab-acting.tab-pane.fade.in.active').find('.reportTblContent');
            var divListBtn  = $(this).closest('div.left-menu').find('.gr-btn');
            var objData = {
                'type'      : type,
                'action'    : action,
                'id'        : id,
                'code_no'   : code_no,
            };
//            ajaxRequest(url, objData, elm, true, resizeTable, elm);
//            get list button after load content
            $.blockUI();
            $.ajax({
                'url': url,
                'data': objData,
                'success':function(html){
                    var res = JSON.parse(html);
                    if(typeof res['report'] != 'undefined'){ // load report content
                        elm.html(res['report']);
                    }
                    if(typeof res['button'] != 'undefined'){ // load list button when click report
                        divListBtn.html(res['button']);
                    }
                    resizeTable(elm);
                    $.unblockUI();
                },
            }); //end get list
            
        });
    }
    
    /*
    * get list table final when month clicked
    */
    function fnBindLoadListReport(url){
        $(document).on('click', '.getListTableBtn', function(){
            $(this).siblings().removeClass('final-active');
            $(this).addClass('final-active');
            var pos = $(this).closest('.list-report').siblings('.gr-btn').find('.role-select').val();
            var objData = {
                'month': $(this).data('month'),
                'year': $(this).data('year'),
                'type': $(this).data('type'),
                'pos': pos,
            };
            var elm = $(this).closest('.left-menu').siblings('.reportTblContent');
            ajaxRequest(url, objData, elm, true, disableBtn);
        });
    }
    
    /*
    * get list table final when month clicked
    */
    function fnBindSearchReport(url){
        $(document).on('submit', '#search-final-report', function(e){
            e.preventDefault();
//            var objData = {
//                'month': monthElm.data('month'),
//                'year': monthElm.data('year'),
//                'pos': pos,
//            };
            var objData = $(this).serializeArray();
        
            var elm = $(this).closest('.search-container').siblings('.reportTblContent');
            ajaxRequest(url, objData, elm);
        });
    }
    
    /*
    * get list table final when change role
    */
    function fbBindLoadReportOnChangeRole(url){
        $(document).on('change', '.role-select', function(){
            var pos         = $(this).val();
            var monthElm    = $(this).closest('.gr-btn').siblings('.list-report').find('li.final-active');
            if(typeof monthElm.data('month') == 'undefined'){
                return;
            }
            var objData = {
                'month': monthElm.data('month'),
                'year': monthElm.data('year'),
                'type': monthElm.data('type'),
                'pos': pos,
            };
            var elm = $(this).closest('.left-menu').siblings('.reportTblContent');
            ajaxRequest(url, objData, elm, true, disableBtn);
        });
    }
    
    /**
    * Xoá bảng lương tab đang tính
    */
    function fnBindDeleteReport(urDelete, UrlTypeReport){
        jQuery(document).on('click','.deleteReportBtn',function() {
            //get id of report to delete
            var id = $(this).closest('.left-menu').find('ul.list-report li.calculatingBtn.active').data('id');
            if(typeof id == 'undefined') return false;
            if(!confirm('Are you sure you want to delete this item?')) return false;
            $.ajax({
                type: 'POST',
                url: urDelete +'/id/'+id,
                data: {
                    'same_code_no': 1
                },
                success: function(data) {
                    //after delete
                    var type = $('#tabs>ul>li.active>a').data('type');
                    var idx = $('#tabs>ul>li.active').index()+1;
                    var elm = $('#tabs>div.tab-content>#tabstabLink'+idx);
                    var objData = {
                        'type': type
                    };
                    ajaxRequest(UrlTypeReport, objData, elm);
                }
            });
        });
    }
    
    function fnBindLoadMoreMonth(url){
        $(document).on('click', '.loadMoreMonthBtn', function(){
            var crYear  = $(this).next('.separateYear').data('year');
            var type    = $(this).data('type');
            var objData = { 
                'crYear' : crYear,
                'type' : type,
            };
            var elm     = $(this);
            $.ajax({
                'url': url,
                'data': objData,
                'success':function(html){
                    elm.after(html);
                },
            });
        });
    }
    
    // Tính tổng thu nhập
    function fnBindCalculateFinalSalary(url){
        $(document).on('click', '.calTotalSalaryBtn', function(){
            var elm             = $(this).closest('.reportTblContent');
            var selected        = $(this).siblings('.selected');
//            var listReportId    = '';
//            for(var i = 0; i < selected.length; i++){
//                listReportId += selected.eq(i).data('id') + ',';
//            }
//            if(listReportId == ''){
//                alert('Vui lòng chọn bảng trước khi tính!');
//                return false;
//            }
//            var objData = {
                
//                'listReportId': listReportId
//                };
            var objData = $('#search-final-report').serialize();
            var lastResponseLength = false;
            $.blockUI();
            $('.blockUI.blockMsg.blockPage').append('<div id="progress-bar-ui"></div>')
            $.ajax({
                'url': url,
                'data': objData,
                'success':function(html){
                    $.unblockUI();
                    if(elm.children().eq(0).hasClass('error-rp')){
                        elm.children('.error-rp').empty();
                    } else {
                        elm.prepend('<div class="error-rp"></div>');
                    }
                    elm.children('.error-rp').html(html);
                    $('.pbcontainer').remove();
                },
                xhrFields: { // For progress bar
                    // ref: https://gist.github.com/sohelrana820/63f029d3aa12936afbc50eb785c496c0
                    // Getting on progress streaming response
                    onprogress: function(e)
                    {
                        var progressResponse;
                        var response = e.currentTarget.response;
                        if(lastResponseLength === false)
                        {
                            progressResponse = response;
                            lastResponseLength = response.length;
                        }
                        else
                        {
                            progressResponse = response.substring(lastResponseLength);
                            lastResponseLength = response.length;
                        }
                        $('#progress-bar-ui').html(progressResponse);
                    }
                }
            });
        });
    }
    
    //show and hide OK, Cancel btn
    function toggleOkCancelBtn(){
        $(document).on('click', '.approvedBtn, .calculatingBtn', function(e){
            var id = $(this).data('id');
            if($(this).hasClass('approvedBtn')){ //approvedBtn
                var stt         = $(this).data('status');
                var sttApproved = '3';
                var okBtn       = $(this).closest('.left-menu').find('.okBtn');
                var cancelBtn   = $(this).closest('.left-menu').find('.cancelBtn');
                /*
                 * if status is approve, ok btn will show, cancel btn will be disable
                 */
                okBtn.removeClass('disabled');
                cancelBtn.removeClass('disabled');
                cancelBtn.attr('data-id', id); //assign report id to button
                okBtn.attr('data-id', id); //assign report id to button
                if(stt == sttApproved){
                    cancelBtn.addClass('disabled');
                } else {
                    okBtn.addClass('disabled');
                }
            }
        });
    }
    
    /**
    * Switch status giữa stt_final và approved
    */
    function fnBindSwitchStatus(url, urlListReport = ''){
        $(document).on('click', '.changeSttBtn', function(e){
            var sttApproved = '1'; // Đang tính
            var sttFinal    = '4'; // Đã chốt
            var typeBtn     = $(this).data('type-btn');
            var selectedElm = $(this).closest('.left-menu').find('li.active');
            var btn         = $(this);
            var id          = selectedElm.data('id');
            var code_no     = selectedElm.data('code_no');
            var elm         = $(this).closest('.left-menu').siblings('.reportTblContent');
            var funcType    = $('#tabs').find('ul>li.active>a').data('type');
            if(typeof id == 'undefined'){
                alert('Vui lòng chọn bảng lương!');
                return false;
            }
            if(typeof code_no == 'undefined'){
                code_no = '';
            }
            if($(this).hasClass('moveToApprovedTab')){
                moveToApprovedTab(url, urlListReport, elm, id, 3, selectedElm, funcType, btn, code_no)
                return;
            }
            if($(this).hasClass('moveToCalculatingTab')){
                moveToCalculatingTab(url, urlListReport, elm, id, 1, selectedElm, funcType, btn, code_no)
                return;
            }
            if(typeBtn == sttApproved){ // Cancel btn
                toStatus(url, urlListReport, elm, id, sttApproved, selectedElm, funcType, btn, code_no);
//                selectedElm.attr('style', 'background: #dbcb7f!important');
            } else {    // Ok btn
                toStatus(url, urlListReport, elm, id, sttFinal, selectedElm, funcType, btn, code_no);
//                selectedElm.attr('style', 'background: #a2ffba!important');
            }
        });
    }
    
    /*
     * Chuyển bảng lương đang tính sang tab đã duyệt
     * @param {type} url            HrSalaryReportsController -> actionChangeStatus
     * @param {type} urlListReport  HrSalaryReportsController -> actionGetHtmlListReport
     * @param {type} elm            element chứa bảng lương
     * @param {type} id             id bảng lương
     * @param {type} stt            stt đã duyệt 
     * @param {type} selectedElm    Bảng lương đang chọn (cột bên trái)
     * @param {type} funcType       Chấm công, sản lượng, ...
     * @param {type} btn            Nút ok, cancel tab đã duyệt
     */
    function moveToApprovedTab(url, urlListReport, elm, id, stt, selectedElm, funcType, btn, code_no){
        var rpCtn = selectedElm.closest('ul.list-report');
        var objData = {
            'type' : funcType,
        };
        var lst_rp_refresh = btn.closest('.tab-content').find('[id^=approved] ul.list-report');
        $.ajax({
            'url': url,
            'data': {
                'id'     : id,
                'code_no': code_no,
                'status' : stt
            },
            'success':function(html){
                if(!html.trim()){ // If error
                    alert('Đã tồn tại một bảng lương tương tự!');
                } else { // If change ok
                    rpCtn.empty();
                    elm.empty();
                    
                    // Refresh list report tab đang tính
                    objData.status = '1';
                    ajaxRequest(urlListReport, objData, rpCtn, true, removeActive, rpCtn);
                    
                    // Refresh list report tab đã duyệt
                    objData.status = '3,4';
                    ajaxRequest(urlListReport, objData, lst_rp_refresh, true, removeActive, lst_rp_refresh);
                }
            }
        });
    }
    
    /*
     * Chuyển bảng lương đã duyệt sang tab đang tính
     * @param {type} url            HrSalaryReportsController -> actionChangeStatus
     * @param {type} urlListReport  HrSalaryReportsController -> actionGetHtmlListReport
     * @param {type} elm            element chứa bảng lương
     * @param {type} id             id bảng lương
     * @param {type} stt            stt đang tính
     * @param {type} selectedElm    Bảng lương đang chọn (cột bên trái)
     * @param {type} funcType       Chấm công, sản lượng, ...
     * @param {type} btn            Nút ok, cancel tab đã duyệt
     */
    function moveToCalculatingTab(url, urlListReport, elm, id, stt, selectedElm, funcType, btn, code_no){
        var rpCtn = selectedElm.closest('ul.list-report');
        var objData = {
            'type' : funcType,
        };
        var lst_rp_refresh = btn.closest('.tab-content').find('[id^=calculating] ul.list-report');
        $.ajax({
            'url': url,
            'data': {
                'id'     : id,
                'code_no': code_no,
                'status' : stt
            },
            'success':function(html){
                rpCtn.empty();
                elm.empty();
                
                // Refresh list report tab đã duyệt
                objData.status = '3,4';
                ajaxRequest(urlListReport, objData, rpCtn, true, removeActive, rpCtn);
                
                // Refresh list report tab đang tính
                objData.status = '1';
                ajaxRequest(urlListReport, objData, lst_rp_refresh, true, removeActive, lst_rp_refresh);
            }
        });
    }
    
    
    /**
     * Change status approved salary reports
     * @param {stt}  report status
     * @param {elm}  element to change text between Da duyet & Da chot
     * @param {selectedElm}  selected element (report in list)
     * @param {btn}  btn click (Ok or Cancel)
     * @returns none
     */
    function toStatus(url, urlListReport, elm, id, stt, selectedElm, funcType, btn, code_no){
        var rpCtn = selectedElm.closest('ul.list-report');
        var objData = {
            'type'  : funcType,
            'status': '3,4'
        };
        $.ajax({
            'url': url,
            'data': {
                'id'     : id,
                'code_no': code_no,
                'status' : stt
            },
            'success':function(html){
                if(!html.trim()){ // If error
                    alert('Đã tồn tại một bảng lương tương tự!');
                } else { // If change ok
//                    location.reload();
                    rpCtn.empty();
                    elm.empty();
                    btn.addClass('disabled');
                    ajaxRequest(urlListReport, objData, rpCtn, true, removeActive, rpCtn);
                }
            },
        });
    }
    
    function removeActive(rpCtn){
        rpCtn.find('li.active').removeClass('active');
    }
    
    
    function fnBindGotoMissReport(){
        $(document).on('click', '.miss-table', function(){
            var type = $(this).data('type');
            $('.tabLink[data-type="'+type+'"]').click();
        });
    }
    
    // Di duyển element lên trên(-1) hay xuống dưới (1)
    function moveElement(dir, elm){
        if(dir < 0){ // Go up
            elm.insertBefore(elm.prev());
        } else { // Go down
            elm.insertAfter(elm.next());
        }
    }
    
    /**
     * @Author: DuongNV Sep 06, 2018
     * @Todo: Move up or down tr in table  (admin/hrFunctions/setting -> create)
     */
    function fnBindMoveRow(){
        $(document).on('click', '.go-up, .go-down', function(){
            var elm = $(this).closest('tr');
            if($(this).hasClass('go-up')){
                moveElement(-1, elm);
            } else {
                moveElement(1, elm);
            }
        })
    }