/**
 * Created by commitflame on 7/25/2016.
 */

// @Author: Trung Jul 25 2016 : Bắt đầu xử lý cho web socket
// Xin quyền notification
document.addEventListener('DOMContentLoaded', function () {
    if (!Notification) {
        alert('Desktop notifications not available in your browser. Try Chromium.');
        return;
    }

    if (Notification.permission !== "granted")
        Notification.requestPermission();
});

function notifyMe($title, $message, $link) {
    if (Notification.permission !== "granted")
        Notification.requestPermission();
    else {
        var notification = new Notification($title, {
            icon: '/YiiCore/themes/gas/favicon.ico',
            body: $message,
        });

        notification.onclick = function () {
            window.open($link);
        };
    }
}

function webSocketStart() {
    ws = new WebSocket('ws://127.0.0.1:8004/type=chat&huongminh_token=' + $webSocketToken  + '&verify_code=' + $webSocketVerifyCode);
    ws.onopen = function () {
        onWebSocketOpen();
    };
    ws.onclose = function () {
        onWebSocketClose();
    };
    ws.onmessage = function (evt) {
        onWebSocketReceiveMessage(evt);
    };
}

$webSocketToken = '';
$webSocketVerifyCode = '';

// Interface
function onWebSocketOpen() {
    console.log('Đăng nhập socket thành công');
};
function onWebSocketClose() {
    console.log('Đăng nhập socket thất bại');
    //setTimeout(webSocketStart, 5000);
};
function onWebSocketReceiveMessage(evt) {
    console.log(evt.data);
};
// @Author: Trung Jul 25 2016 : Kết thúc xữ lý cho web socket