/**
 * @Author: DungNT Sep 01, 2018
 * @Todo: move all js issue to new file js
 */

function fnInitIssueEvent(BLOCK_UI_COLOR, urlAutocomplete, urlAjaxUpdate) {
    BindClickView();
    BindClickReply(BLOCK_UI_COLOR);
//        BindClickClose(BLOCK_UI_COLOR);
    BindPickTicket(BLOCK_UI_COLOR);
    BindTicketSubmit(BLOCK_UI_COLOR);
    fnRefreshOrderNumber();
    fnBindRemoveIcon();
    fnUpdateColorbox();
    fnBindAllAutocomplete(urlAutocomplete);
    bindEvent();
    fnUpdateAjax(urlAjaxUpdate);
}


function fnBuildRow(this_){
    $(this_).closest('.tb_file').find('.materials_table').find('tr:visible:last').next('tr').show();
}

function fnUpdateColorbox(){
    $(".gallery").colorbox({iframe:true,innerHeight:'1500', innerWidth: '1050',close: "<span title='close'>close</span>"});
    fnUpdateCurrentPage();
}

function fnUpdateCurrentPage(){
    var cPage = $('#id-of-pager-ul').find('.selected').find('a').text();
    var input ="<input type='hidden' name='cPage' value='"+cPage+"' >";
    $('.form_reopen').append(input);
}

/**
* @Author: DungNT Dec 15, 2015
* @Todo: có thể custom function này, vì cái này viết chung cho
* @Param: 
*/
function BindClickReply(BLOCK_UI_COLOR){
    $('.new_reply').find('input:submit').live('click',function(){
        var ok =  true;
        var form = $(this).closest('form');
        var message = form.find('textarea').val();
        if( form.find('.gasErrorMsg').size()>0 && $.trim(message)==''){
            form.find('.errorMessage').show();
            ok = false;
        }else{
            form.find('.errorMessage').hide();
        }

        // Add Dec 15, 2015
//            if(form.find('.chief_monitor_id').size()>0 && form.find('.chief_monitor_id').val() == "" ){
//                form.find('.errorMessageChiefMonitor').show();
//                ok = false;
//            }else{
//                form.find('.errorMessageChiefMonitor').hide();
//            }

        // Add Dec 15, 2015
        if(form.find('.monitor_agent_id').size()>0 && form.find('.monitor_agent_id').val() == "" ){
            form.find('.errorMessageMonitorAgent').show();
            ok = false;
        }else{
            form.find('.errorMessageMonitorAgent').hide();
        }

        // Add Dec 24, 2015
        if(form.find('.accounting_id').size()>0 && form.find('.accounting_id').val() == "" ){
            form.find('.errorMessageAccounting').show();
            ok = false;
        }else{
            form.find('.errorMessageAccounting').hide();
        }

        if(!ok){
            return ok;
        }
        $.blockUI({ overlayCSS: { backgroundColor: BLOCK_UI_COLOR } }); 
        return true;
    }); 
}


function fnBindAllAutocomplete(urlAutocomplete){
    $('.UserAutocomplete').each(function(){
        fnBindAutocomplete($(this), urlAutocomplete);
    });
}

// to do bind autocompelte for input
// @param objInput : is obj input ex  $('.customer_autocomplete')
function fnBindAutocomplete(objInput, urlAutocomplete){
    var parent_div = objInput.closest('.new_reply');

    objInput.autocomplete({
        source: urlAutocomplete,
        minLength: 2,
        close: function( event, ui ) { 
            //$( "#GasStoreCard_materials_name" ).val(''); 
        },
        search: function( event, ui ) { 
                objInput.addClass('grid-view-loading-gas');
        },
        response: function( event, ui ) { 
            objInput.removeClass('grid-view-loading-gas');
            var json = $.map(ui, function (value, key) { return value; });
            if(json.length<1){
                var error = '<div class=\'errorMessage clr autocomplete_name_text\'>Không tìm thấy dữ liệu.</div>';
                if(parent_div.find('.autocomplete_name_text').size()<1){
                    parent_div.find('.remove_row_item').after(error);
                }
                else
                    parent_div.find('.autocomplete_name_text').show();
            }                    

        },
        select: function( event, ui ) {
            objInput.attr('readonly',true);
            parent_div.find('.autocomplete_name_text').remove();
            parent_div.find('.chief_monitor_id').val(ui.item.id);
        }
    });
}

function fnRemoveNameHand(this_){
    var parent_div = $(this_).closest('.new_reply');
    parent_div.find('.UserAutocomplete').attr("readonly",false);
    parent_div.find('.UserAutocomplete').val("");
    parent_div.find('.chief_monitor_id').val("");
    parent_div.find('.autocomplete_name_text').remove();
}

function bindEvent(){
    $('body').on('click', '.ApplyLaw', function(){
        var msg = $(this).attr('alert_text');
        var url_ = $(this).attr('next');
        if(!confirm(msg)){
            return ;
        }
        $.blockUI({ overlayCSS: { backgroundColor: 'fff' } });
        $(this).closest('li.ticket').remove();
        $.ajax({
            url: url_,
            success: function(data){
                $.unblockUI();
            }
        });

    });

    $('body').on('click', '.CheckLikeRadio', function(){
        if($(this).is(':checked')){
            var wrapTable = $(this).closest('.BoxReplyApplyLaw');
            var currentId = $(this).attr('id');
            wrapTable.find('input:checkbox').each(function(){
                var checkboxId = $(this).attr('id');
                if(checkboxId != currentId){
                    $(this).attr('checked', false);
                }
            });
        }
    });
    
    $('body').on('click', '.ChkUpdateStopOrder', function(){
        var parentLi    = $(this).closest('li.ticket');
        if(confirm($(this).attr('alertText'))){
            $.blockUI({ overlayCSS: { backgroundColor: 'fff' } });
            var url_ = $(this).attr('next');
            var BoxReplyApplyLaw = $(this).closest('.BoxReplyApplyLaw');
            $.ajax({
                url: url_,
                type: 'post',
                dataType: 'json',
                success: function(data){
                    $.unblockUI();
//                    var flashMsg = "<div class='flash notice'><a data-dismiss='alert' class='close' href='javascript:void(0)'>×</a>"++"</div>";
//                    BoxReplyApplyLaw.before(data['msg']);
                    parentLi.before(data['msg']);
                    parentLi.remove();
                }
            });
        }
    });

}

function fnUpdateAjax(urlAjaxUpdate){
//            $('.submit_ajax').off('click').on('click',function(){
        $('body').on('click', '.submit_ajax', function(){
        $element        = this;
        parent_form     = $(this).closest('form');
        var url_        = urlAjaxUpdate + '/id/' + $(this).attr("data-id");
        var parentLi    = $(this).closest('li.ticket');
        parent_form.ajaxSubmit({
            type: 'post',
            dataType: 'json',
            url: url_,
            beforeSend:function(data){
                $.blockUI({
    //                        message: '', 
    //                        overlayCSS:  { backgroundColor: '#fff' }
               });
            },
            success: function(data)
            {
    //                    parent_form.find('input:file').val('');
                if(data['success']){
                    parentLi.before(data['msg']);
                    parentLi.remove();
                }else{
                    $($element).before(data['msg']);
                }
                $('.flash a.close').click(function(){ $(this).closest('div.flash').remove(); });
                $.unblockUI();
            }
        });// end parent_form.ajaxSubmit({
    });
}

// DuongNV Oct011019 ajax nut phat 50k
function fnSubmitAjax(jsClass){
    $(document).on('click', '.'+jsClass, function(e){
        e.preventDefault();
        var msg  = $(this).attr('alert_text');
        var url_        = $(this).data("url");
        if(!confirm(msg) || typeof(url_) === 'undefined'){
            return false;
        }
        $element        = this;
        parent_form     = $(this).closest('form');
        var currentInfo = $(this).closest('.message').find('div.js-punish-info');
        var currentBtn  = $(this);
        parent_form.ajaxSubmit({
            type: 'post',
            dataType: 'json',
            url: url_,
            beforeSend:function(data){
                $.blockUI({
               });
            },
            success: function(data)
            {
                if(data['success']){
                    currentInfo.before(data['msg']);
                    currentInfo.html(data['new']);
                    currentBtn.after('<b style="color:red;">Đã phạt, reload lại trang để phạt tiếp!</b>')
                    currentBtn.remove();
                }else{
                    $($element).before(data['msg']);
                }
                $('.flash a.close').click(function(){ $(this).closest('div.flash').remove(); });
                $.unblockUI();
            }
        });// end parent_form.ajaxSubmit({
    });
}
