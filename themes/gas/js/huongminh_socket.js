/**
 * @Author: DungNT Now 27, 2016
 * @Todo: something with web socket
 */
function wsStart(url) {
    ws = new WebSocket(url);
    ws.onopen = function () {
//        console.log('System: the connection open');
    };
    ws.onclose = function () {
//        $("#chat").append("<p>System: the connection is closed, trying to reconnect</p>");
        console.log('System: the connection is closed, trying to reconnect');
//        var timeOut = getRandomInt(5000, 60000);// 5s->60second
        var timeOut = getRandomInt(5000, 10000);// 5s->10second
        setTimeout(function() { wsStart(url); }, timeOut);
    };
    ws.onmessage = function (evt) {
        handleReceiveNotify(evt);
    };
    return ws;
}

/**
* Returns a random integer between min (inclusive) and max (inclusive)
* Using Math.round() will give you a non-uniform distribution!
*/
function getRandomInt(min, max) {
   return Math.floor(Math.random() * (max - min + 1)) + min;
}

function isValidJson(json){
    try{
       var json = JSON.parse(json);
    }catch(e){
        return ;
        alert('Không thể kết nối đến máy chủ CallCenter. Vui lòng thoát ra đăng nhập lại.\nNếu chưa được gọi cho Dũng 01684 331 552');
        $('#content').html('<div class="form"><h1>Không thể kết nối đến máy chủ, đăng nhập lại để sử dụng chức năng này</h1></div>');
        console.log('Invalid Json: '+json);
        return false;
    }
    return true;
}

/**
 * @Author: DungNT Now 29, 2016
 * @Todo: handle receive notify
 */
function handleReceiveNotify(evt) {
    if(!isValidJson(evt.data)){
        return ;
    }
    obj = $.parseJSON(evt.data);
    if(typeof obj !='object'){
        return ;
      // It is JSON
    }
    
    if(!obj.hasOwnProperty("status") || !obj.hasOwnProperty("record")){
        return ;
    }
    var record = obj.record;
    if(!record.hasOwnProperty("code")){
        return ;
    }
//    console.log(obj);
    var code = record.code*1;
    switch(code) {
        case 3:
            notifyAppTransactionNew(record); // Dec 04, 2016 Tạm close cái này lại, khi nào demo xong thì mở ra
            break;
        case 1:
        case 2:
        case 4:
        case 6:
        case 7:
            notifyDieuPhoiOrder(record);
            break;
        case 5: // Call Event South Telecom VOIP
            callEvent(record);
            break;
        case 8: // Call Event Monitor South Telecom VOIP
            callMonitor(record);
            break;
        case 9: // Dec0417 Hide row when one user click make Sell for App Gas24h
            confirmAppGas24h(record);
            break;
        case 10: // Sep1418 Alert any thing to user CallCenter
            alertCallCenter(record);
            break;

        default:
            return ;
    }

}
/**
 * @Author: DungNT Now 29, 2016
 * @Todo: play audio
 */
function audioPlay(id_audio) {
    var foo = document.getElementById(id_audio);
    foo.play();
}
function audioPause(id_audio) {
    var foo = document.getElementById(id_audio);
    foo.pause();  //just bind play/pause to some onclick events on your page
}
/**
 * @Author: DungNT Now 30, 2016
 * @Todo: xử lý notify đơn hàng tạo từ app, bật sound + update back io + show record new
 */
function notifyAppTransactionNew(record){
    /* 1. update back server io là client đã nhận dc message
     * 2. handle cho từng action
     */
    var code        = record.code*1;
    var obj_id      = record.obj_id*1;
    var remote_id   = record.id*1;
    var server_io_notify_id   = record.server_io_notify_id*1;
    var url_        = $('.SocketUrlHandleNotify').attr('next');
    $.ajax({
        url: url_,
        data: {code:code, obj_id:obj_id, remote_id:remote_id, server_io_notify_id:server_io_notify_id},
        type: 'post',
        dataType: 'json',
        success:function(data){
//            console.log(data);
            if(data['success']){
                if($('#transaction-grid tbody').find('.'+data['ClassRowSpan']).size() == 0){
                    $('#transaction-grid tbody').append(data['html']);
                    notifyMe('Đặt hàng App Gas24h', 'Khách hàng đặt hàng App Gas24h');
                    audioPlay('AudioPlayer');
                }
//                $('#transaction-grid tbody').prepend(data['html']);
            }else{
                console.log('not data');
            }
        }
    });
}

function transConfirmRead(){
    $('body').on('click', '.TransConfirmRead', function(){
        audioPause('AudioPlayer');
        $(this).find('img').remove();
        var url_ = $(this).attr('next');
        $.ajax({
            url: url_,
        });
    });
}

/**
 * @Author: DungNT Now 30, 2016
 * @Todo: xử lý notify đơn hàng điều phối
 */
function notifyDieuPhoiOrder(record){
    audioPlay('AudioPlayerDieuPhoi');
    var code            = record.code*1;
    var obj_id          = record.obj_id*1;
    var remote_id       = record.id*1;
    var server_io_notify_id   = record.server_io_notify_id*1;
    var url_            = $('.SocketUrlHandleNotify').attr('next');
    var DieuPhoiRow     = 'DieuPhoiRow'+remote_id;
    notifyDieuPhoiAddRow(record, DieuPhoiRow);
    if(code == 4){// CODE_SELL_ALERT_KTBH
        notifyMe('Chưa xác nhận đơn hàng', record.agent_name);
    }else if(code == 6 || code == 7){
//        notifyMe('Khách hàng đặt APP',record.msg);
        alert('Khách hàng bò mối đặt APP: ' + record.msg);
    }
    
    $.ajax({
        url: url_,
        data: {code:code, obj_id:obj_id, remote_id:remote_id, server_io_notify_id:server_io_notify_id},
        type: 'post',
        dataType: 'json',
        success:function(data){}
    });
}

/* build tr and add vào row đầu tiên */
function notifyDieuPhoiAddRow(record, DieuPhoiRow){
    var url             = $('.SocketUrlDieuPhoiConfirm').attr('next');
    var remote_id       = record.id*1;
    url_                = updateQueryStringParameter(url, 'id', remote_id);
    var urlIcon         = $('.UrlIconNewNotify').clone();
    urlIcon.find('a').attr('next', url_);
    var code            = record.code*1;
    var classWrap       = 'BoxSystemNotify'+code;
    
    var html = '<tr>';
    html += '<td><span class="'+DieuPhoiRow+' order_no">1</span></td>';
    html += '<td>'+urlIcon.html()+'</td>';
    html += '<td>'+record.agent_name+'</td>';
    html += '<td>'+record.msg+'</td>';
    html += '<td class="item_c">'+record.created_date+'</td>';
    html += '</tr>';
    
    var classRow = $('.'+classWrap+' tbody').find('.'+DieuPhoiRow);
    
    if(classRow.size() == 0){
        $('.'+classWrap+' tbody').prepend(html);
        fnRefreshOrderNumber();
        return true;
    }// vẫn phải add row vào để đại lý tắt chuông trong trường hợp bị send lỗi
    
    if(classRow.size() > 0){
        // xử lý trường hợp đã có row sãn khi đại lý load page lần đầu, sẽ không insert row mà insert icon để user click xác nhận lại
        // vd khi user click xác nhận thì socket die, do đó Server IO sẽ tiếp tục gửi,
        var tr = classRow.closest('tr');
        tr.find('td').eq(1).html(urlIcon.html());
    }
    return false;
}

function orderConfirmRead(){
    $('body').on('click', '.OrderConfirmRead', function(){
        audioPause('AudioPlayerDieuPhoi');
        $(this).find('img').remove();
        var url_ = $(this).attr('next');
        $.ajax({
            url: url_,
        });
    });
}

/**
 * @Author: DungNT Mar 13, 2017
 * @Todo: xử lý notify Số điện thoại gọi đến
 */
function callEvent(record){
//    audioPlay('AudioPlayer');
    /* 1. không cần update back server io là client đã nhận dc message
     * 2. xử lý set số phone gọi đến và background
     * 3. ajax get info customer + history lấy hàng ở event gọi đến
     */
    callEventUpdatePopup(record);
    
}

/** @Author: DungNT Mar 13, 2017 
 * @Todo: Cập nhật popup thông tin phone gọi đến
 */
function callEventUpdatePopup(record) {
    var call_status     = record.call_status*1;// 2:Dialing, 3:DialAnswer, 4:HangUp
//    var phone_number    = '0'+record.msg;
    var phone_number    = record.msg;
    var call_temp_id    = record.call_temp_id;
//    console.log(record);
    switch (call_status){
        case 2: // Dialing
            var call_uuid           = record.call_uuid;
            var parent_call_uuid    = record.parent_call_uuid;
            WrapOneTab = callEventFindLineHanlde(record, call_uuid);
            if(!WrapOneTab){// if is null
                break;
            }
            var bgClass = 'bg-green';
            callEventSetNewPhone(WrapOneTab, phone_number);
            updateUrlCreateCustomer(WrapOneTab, phone_number);
            callEventSetNewBackground(WrapOneTab, bgClass);
            callEventSetCallStatusLine(WrapOneTab, record, call_status, call_uuid);
            callEventSetActiveTab(WrapOneTab, record);
            callEventSetActiveTabBackground(WrapOneTab, bgClass);
            getCustomerByPhone(WrapOneTab, phone_number, call_temp_id);
//            notifyMe('Cuộc gọi đến', phone_number);// Close on Sep2817
            updateUrlCreateSell(WrapOneTab, 0);// Jun 15, 2017 cho update ở đây trước, sau đó overide sau
            break;
        case 3: // DialAnswer
            var call_uuid       = record.call_uuid;
            var child_call_uuid = record.child_call_uuid;
            WrapOneTab = callEventFindCurrentCall(record, child_call_uuid);
            if(!WrapOneTab){// if is null
                break;
            }
            var bgClass = 'bg-green';
            var customer_id = WrapOneTab.find('.customer_id').val();
            callEventSetNewBackground(WrapOneTab, bgClass);
//            updateUrlCreateSell(WrapOneTab, customer_id);// không change ở đây nữa Apr 13, 2017
            callEventSetCallStatusLine(WrapOneTab, record, call_status, child_call_uuid);
            break;
        case 4: // HangUp 
            var call_uuid           = record.call_uuid;
            WrapOneTab = callEventFindCurrentCall(record, call_uuid);
            if(!WrapOneTab){// if is null
                break;
            }
            var bgClass = 'bg-blue';
            callEventSetNewPhone(WrapOneTab, phone_number+' - Xong');
            callEventSetNewBackground(WrapOneTab, bgClass);
            callEventSetCallStatusLine(WrapOneTab, record, call_status);
            callEventSetActiveTab(WrapOneTab, record);
            callEventSetActiveTabBackground(WrapOneTab, bgClass);
            callEventHangUpResetVal(WrapOneTab, record, call_uuid);
            break;
        
        case 7: // MissCall
            var call_uuid = record.call_uuid;
            WrapOneTab = callEventFindCurrentCall(record, call_uuid);
            if(!WrapOneTab){// if is null
                break;
            }
            var bgClass = 'bg-red';
            
            callEventSetNewPhone(WrapOneTab, phone_number+' - Miss Call');
            callEventSetNewBackground(WrapOneTab, bgClass);
            callEventSetActiveTabBackground(WrapOneTab, bgClass);
            
            break;

        default: 
            break;
    }
    
    // continue active tab handle call - nếu tô background lên dc trên tab thì tốt
    
}

/** @Author: DungNT Mar 13, 2017
 * @Todo: tìm line phù hợp để hiện thị cuộc gọi
 */
function callEventFindLineHanlde(record, call_uuid) {
    if($('.'+call_uuid).size() > 0 ){// xử lý trường hợp có 2 event của cùng 1 call cùng gửi xuống 
        return null;
    }
    
    var WrapOneTab = callEventFindLineHanldeScan(0);// line free
    if(!WrapOneTab){// trường hợp ko có line nào free thì tìm những line đã cúp máy để put vào
        WrapOneTab = callEventFindLineHanldeScan(4);// hangup
    }
    if(!WrapOneTab){//  if is null trường hợp ko có line nào free thì tìm những line miss call
        WrapOneTab = callEventFindLineHanldeScan(7);// Chưa rõ status của Miss Call(7)
    }
    return WrapOneTab;
}
// belong to callEventFindLineHanlde
function callEventFindLineHanldeScan(call_status) {
    var res = null;
    $('.CallStatusLine').each(function(){
        var WrapOneTab = $(this).closest('.WrapOneTab');
        if($(this).val() == call_status ){
            res = WrapOneTab;
            return false;
        }
    });
    return res;
}

/**
 * @Author: DungNT Mar 13, 2017
 * @Todo: find current call when have event DialAnswer, HangUp
 */
function callEventFindCurrentCall(record, call_uuid) {
    var res = null;
    $('.call_uuid').each(function(){
        var WrapOneTab = $(this).closest('.WrapOneTab');
        if($(this).val() == call_uuid){
            res = WrapOneTab;
            return false;
        }
        if(WrapOneTab.find('.parent_call_uuid').val() == call_uuid && WrapOneTab.find('.CallStatusLine').val() == 3){
            res = WrapOneTab;
            return false;
        }
    });
    return res;
}

function callEventSetCallStatusLine(WrapOneTab, record, call_status, call_uuid){
    WrapOneTab.find('.CallStatusLine').val(call_status);
    WrapOneTab.find('.CallCurrentPhone').val(record.msg);
    WrapOneTab.find('.call_uuid').val(call_uuid);
    WrapOneTab.find('.call_uuid').addClass(call_uuid);
    if(call_status == 2){//Dialing
        WrapOneTab.find('.parent_call_uuid').val(record.parent_call_uuid);
    }
    WrapOneTab.find('.OrderBoMoiDate').val($('.TodayText').val());
    
}
function callEventSetActiveTab(WrapOneTab, record){
    var currentTab = WrapOneTab.find('.CallCurrentTab').val();
    $( "#tabs" ).tabs({ active: currentTab });
    // set phone number for line title
    var phone_number    = record.msg;
    var lastFive = phone_number.substr(phone_number.length - 6); // => "Tabs1"
    $('.CallLiTab'+currentTab).find('a').text(lastFive);
}
function callEventSetActiveTabBackground(WrapOneTab, class_name){
    var currentTab = WrapOneTab.find('.CallCurrentTab').val();
    var li = $( ".CallUlTab" ).find('li').eq(currentTab);
    li.removeClass('bg-green bg-blue bg-red');
    li.addClass(class_name);
}

/** @Author: DungNT Mar 13, 2017
 * @Todo: set số phone gọi đến vào popup
 */
function callEventSetNewPhone(WrapOneTab, phone_number) {
    WrapOneTab.find('.CallPhoneNumber').text(phone_number);
}
/** @Author: DungNT Mar 13, 2017
 * @Todo: set backgound số phone gọi đến vào popup
 */
function callEventSetNewBackground(WrapOneTab, class_name) {
    WrapOneTab.find('.CallPhoneNumber').removeClass('bg-green bg-blue bg-red');
    WrapOneTab.find('.CallPhoneNumber').addClass(class_name);
}
function callEventHangUpResetVal(WrapOneTab, record, call_uuid){
//    WrapOneTab.find('.CallStatusLine').val(0);
    WrapOneTab.find('.call_uuid').val(call_uuid);// khi reset như này thì không update được trạng thái cuộc gọi
    WrapOneTab.find('.call_uuid').attr('class', 'call_uuid');
}

/**
 * @Author: DungNT Mar 14, 2017
 * @Todo: Khi có sự kiện Dialling thì get info customer by phone_number or customer_id: name + phone + add + history
 * 1. xử lý popup thông tin customer nếu có nhiều customer tìm thấy với 1 số đt
 * 2. xư lý nếu có 1 customer tìm thấy với số đt đó
 * 3. cập nhật user_id đang login vào Call Temp
 */
function getCustomerByPhone(WrapOneTab, phone_number, call_temp_id) {
    var url_            = WrapOneTab.find('.CallGetCustomerByPhoneUrl').val();
    var CallCurrentTab  = WrapOneTab.find('.CallCurrentTab').val();
    $.blockUI({ overlayCSS: { backgroundColor: '#fff' } });
    $.ajax({
        url: url_,
        data: {phone_number:phone_number, CallCurrentTab: CallCurrentTab, call_temp_id: call_temp_id},
        type: 'post',
        dataType: 'json',
        success:function(data){
            freeLineBoMoi(WrapOneTab);// free line trước khi map thông tin KH mới vào
            $.unblockUI();
            if(data['success'] && data['total_record'] > 0){
                $('#BoxSelectCustomer').html(data['msg']);
                if(data['total_record'] > 1){
                    popupSelectCustomer();
                }else{// có 1 KH tìm thấy
                    $('.RowCustomerName:first').trigger('click');
                }
            }
        }
    });
}

/**
 * @Author: DungNT Mar 15, 2017
 * @Todo: get history customer mua hàng
 */
function getCustomerHistory(WrapOneTab, customer_id) {
    var url_            = WrapOneTab.find('.CallGetCustomerHistoryUrl').val();
    var CallCurrentTab  = WrapOneTab.find('.CallCurrentTab').val();
    $.blockUI({ overlayCSS: { backgroundColor: '#fff' } });
    $.ajax({
        url: url_,
        data: {customer_id:customer_id, CallCurrentTab: CallCurrentTab},
        type: 'post',
        dataType: 'json',
        success:function(data){
            $.unblockUI();
            var agent_id    = data['customer_info']['customer_delivery_agent_id'];
            var agent_name  = data['customer_info']['customer_delivery_agent'];
            if(data['IsBoMoi']){
                setCustomerInfo(data, WrapOneTab, CallCurrentTab);
                WrapOneTab.find('.BoxCustomerHistory').html(data['msg']);
            }else{
                WrapOneTab.find('.BoxCustomerHistory').html(data['msg']);
            }
            setAgentUpdateCall(WrapOneTab, agent_id, agent_name);
        }
    });
}

/**
 * @Author: DungNT May 22, 2017
 * @Todo: xử lý history bò mối set customer info at response
 */
function setCustomerInfo(data, WrapOneTab, CallCurrentTab) {
    WrapOneTab.find('.OrderBoMoiCustomerView').show();
//    WrapOneTab.find('.OrderBoMoiCustomerInfo').show();
    var agent_id    = data['customer_info']['customer_delivery_agent_id'];
    var agent_name  = data['customer_info']['customer_delivery_agent'];
    // 1. set agent info
    WrapOneTab.find('.OrderBoMoiAgentId').val(agent_id);
    WrapOneTab.find('.OrderBoMoiAgentName').val(agent_name);
    if($.trim(data['customer_info']['customer_delivery_agent']) != ''){
        WrapOneTab.find('.OrderBoMoiAgentName').attr('readonly', 1);
    }
    
    // 2. set box customer info
    WrapOneTab.find('.OrderBoMoiCustomerInfo').find('.customer_agent_name').text(data['customer_info']['customer_agent']);
    WrapOneTab.find('.OrderBoMoiCustomerInfo').find('.customer_agent_name_delivery').text(data['customer_info']['customer_delivery_agent']);
    WrapOneTab.find('.OrderBoMoiCustomerInfo').find('.customer_contact').text(data['customer_info']['contact']);
    WrapOneTab.find('.OrderBoMoiCustomerInfo').find('.customer_type').text(data['customer_info']['customer_type']);
    WrapOneTab.find('.OrderBoMoiCustomerInfo').find('.customer_note').text(data['customer_info']['contact_note']);
    WrapOneTab.find('.OrderBoMoiCustomerInfo').find('.customer_sale').text(data['customer_info']['sale_name'] + ' - ' + data['customer_info']['sale_phone']);
    // 3. set agent info to call status
}

/** @Author: DungNT Sep 12, 2017
 *  @Todo: set info agent update status call
 */
function setAgentUpdateCall(WrapOneTab, agent_id, agent_name) {
    WrapOneTab.find('.UpdateAgentId ').val(agent_id);
    WrapOneTab.find('.UpdateAgentName ').val(agent_name).attr('readonly', 1);
}

/**
 * @Author: DungNT Mar 16, 2017
 * @Todo: ajax update customer id cho Call khi có customer dc select
 */
function callUpdateCustomerId(WrapOneTab, customer_id) {
    var url_        = WrapOneTab.find('.CallUpdateCustomerIdUrl').val();
    var call_uuid   = WrapOneTab.find('.call_uuid').val();
    $.ajax({
        url: url_,
        data: {customer_id:customer_id, call_uuid: call_uuid},
        type: 'post',
        dataType: 'json',
        success:function(data){}
    });
}

/**
 * @Author: DungNT Mar 15, 2017
 * @Todo: cập nhật customer_id vào url Sell
 */
function updateUrlCreateSell(WrapOneTab, customer_id) {
    if(WrapOneTab.find('.CallCreateSellUrl').size() < 1){
        return ;
    }
    
    var call_uuid       = WrapOneTab.find('.parent_call_uuid').val();
    var next            = WrapOneTab.find('.CallCreateSellUrl').attr('href');
    var phone_number    = WrapOneTab.find('.CallCurrentPhone').val();
    
    next = updateQueryStringParameter(next, 'customer_id', customer_id);
    next = updateQueryStringParameter(next, 'call_uuid', call_uuid);
    next = updateQueryStringParameter(next, 'phone_number', phone_number);
    WrapOneTab.find('.CallCreateSellUrl').attr('href', next);
        
    // Apr 20, 2017 update url create uphold HGĐ
    var next = WrapOneTab.find('.CallCreateUpholdUrl').attr('href');
    next = updateQueryStringParameter(next, 'customer_id', customer_id);
    next = updateQueryStringParameter(next, 'call_uuid', call_uuid);
    next = updateQueryStringParameter(next, 'phone_number', phone_number);
    WrapOneTab.find('.CallCreateUpholdUrl').attr('href', next);
    
    // Jun 24, 2017 update url create uphold Bo Moi
    var next = WrapOneTab.find('.CallCreateUpholdBoMoiUrl').attr('href');
    next = updateQueryStringParameter(next, 'customer_id', customer_id);
    next = updateQueryStringParameter(next, 'call_uuid', call_uuid);
    next = updateQueryStringParameter(next, 'phone_number', phone_number);
    WrapOneTab.find('.CallCreateUpholdBoMoiUrl').attr('href', next);
}
function updateUrlCreateCustomer(WrapOneTab, phone_number) {
    var CallCurrentTab  = WrapOneTab.find('.CallCurrentTab').val();
    var next = WrapOneTab.find('.BtnCreateCustomer').attr('href');
    next = updateQueryStringParameter(next, 'phone_number', phone_number);
    next = updateQueryStringParameter(next, 'CallCurrentTab', CallCurrentTab);
    WrapOneTab.find('.BtnCreateCustomer').attr('href', next);
}

/**
 * @Author: DungNT Mar 14, 2017
 * @Todo: bind event for some Class control
 */
function callBindEventElement() {
    $('.ControlCallBoxUpdateFailed').click(function(){
        $('.CallBoxUpdateFailed').toggle();
    });
    
    /** Cập nhật thông tin KH được select trên popup trả về khi find theo số phone */
    $('body').on('click', '.RowCustomerName', function(){
        var li = $(this).closest('li');
        var CallCurrentTab  = $(this).attr('CallCurrentTab');
        var customer_id     = $(this).attr('customer_id');
        var WrapOneTab      = $('.CallCurrentTab'+CallCurrentTab).closest('.WrapOneTab');
        setInfoCustomer(WrapOneTab, customer_id, $(this).text(), li.find('.RowCustomerPhone').html(), li.find('.RowCustomerAddress').html());
        WrapOneTab.find('.BtnUpdatePhone').hide();
        callUpdateCustomerId(WrapOneTab, customer_id);
        $.colorbox.close();
    });
    
    /* Apr 06, 2017 click free line */
    $('body').on('click', '.BtnFreeLine', function(){
//        if(!confirm('Bạn chắc chắn muốn Free Line?')){
//            return ;
//        }
        var WrapOneTab = $(this).closest('.WrapOneTab');
        freeLine(WrapOneTab);
    });
    
}

/**
 * @Author: DungNT Apr 06, 2017
 * @Todo: reset tab
 */
function freeLine(WrapOneTab) {
    var CallCurrentTab = WrapOneTab.find('.CallCurrentTab').val()*1;
    callEventHangUpResetVal(WrapOneTab, 0, 0);
    
    var record = {
        msg : '',
        parent_call_uuid : ''
    };
    var call_status = 0;
    var call_uuid   = '';
    
    var bgClass     = '';
    callEventSetNewPhone(WrapOneTab, '');
    callEventSetNewBackground(WrapOneTab, bgClass);
    callEventSetCallStatusLine(WrapOneTab, record, call_status, call_uuid);
    callEventSetActiveTabBackground(WrapOneTab, bgClass);
    $('.CallLiTab'+CallCurrentTab).find('a').text('Line '+ (CallCurrentTab+1));
    
    var linkCreateSell = WrapOneTab.find('.CallCreateSellUrl').attr('NormalLink');
    WrapOneTab.find('.CallCreateSellUrl').attr('href', linkCreateSell);
    
    var linkUpholdHgd = WrapOneTab.find('.CallCreateUpholdUrl').attr('NormalLink');
    WrapOneTab.find('.CallCreateUpholdUrl').attr('href', linkUpholdHgd);
    
    var linkUpholdBoMoi = WrapOneTab.find('.CallCreateUpholdBoMoiUrl').attr('NormalLink');
    WrapOneTab.find('.CallCreateUpholdBoMoiUrl').attr('href', linkUpholdBoMoi);
    
    freeLineBoMoi(WrapOneTab);
    resetBoxUpdateStatusCall(WrapOneTab);
}

function resetBoxUpdateStatusCall(WrapOneTab) {// Sep1217
    setAgentUpdateCall(WrapOneTab, 0, '');
    WrapOneTab.find('.UpdateAgentName ').attr('readonly', false);
    WrapOneTab.find('.CallFailedNote ').val('');
}

function setInfoCustomer(WrapOneTab, customer_id, customer_name, customer_phone, customer_address){
    WrapOneTab.find('.customer_id').val(customer_id);
    WrapOneTab.find('.CustomerInfoName').text(customer_name);
    WrapOneTab.find('.CustomerInfoPhone').html(customer_phone);
    WrapOneTab.find('.CustomerInfoAddress').html(customer_address);
    getCustomerHistory(WrapOneTab, customer_id);
    updateUrlCreateSell(WrapOneTab, customer_id);
    setEmptyBoMoiQty(WrapOneTab);
}

/**
 * @Author: DungNT MaY 23, 2017
 * @Todo: reset tab Bò Mối
 */
function freeLineBoMoi(WrapOneTab){
    setEmptyBoMoiQty(WrapOneTab);
    WrapOneTab.find('.WrapOrderType').find('input:radio:first').attr('checked', true);
    WrapOneTab.find('.OrderBoMoiAgentName').val('');
    WrapOneTab.find('.OrderBoMoiAgentName').val('');
    WrapOneTab.find('.OrderBoMoiAgentId').val(0);
    WrapOneTab.find('.OrderBoMoiDieuXeId').val(0);
    WrapOneTab.find('.OrderBoMoiNote').val('');
    WrapOneTab.find('.OrderBoMoiPayDirect').attr('checked', false);
//    WrapOneTab.find('.OrderBoMoiCustomerInfo').hide();
    WrapOneTab.find('.BoxRenderNotify').html('');
    freeLineBoMoiCleanTableCustomer(WrapOneTab);
}
function freeLineBoMoiCleanTableCustomer(WrapOneTab){
    WrapOneTab.find('.customer_agent_name').html('');
    WrapOneTab.find('.customer_agent_name_delivery').html('');
    WrapOneTab.find('.customer_contact').html('');
    WrapOneTab.find('.customer_type').html('');
    WrapOneTab.find('.customer_note').html('');
    WrapOneTab.find('.customer_sale').html('');
    
    WrapOneTab.find('.CustomerInfoName').text('');
    WrapOneTab.find('.CustomerInfoPhone').text('');
    WrapOneTab.find('.CustomerInfoAddress').text('');
    WrapOneTab.find('.BoxCustomerHistory').html('');
}

function setEmptyBoMoiQty(WrapOneTab){
    setEmptyBoMoiQtyOnly(WrapOneTab);
    WrapOneTab.find('.DisplayError').html('');
    WrapOneTab.find('.OrderBoMoiPayDirect').attr('checked', false);
    WrapOneTab.find('.DivDeliveryTimer').hide();
    WrapOneTab.find('.OrderBoMoiDeliveryTimer').val('');
}

function setEmptyBoMoiQtyOnly(WrapOneTab){
    WrapOneTab.find('.CallB50').val(0);
    WrapOneTab.find('.CallB45').val(0);
    WrapOneTab.find('.CallB12').val(0);
    WrapOneTab.find('.CallB6').val(0);
    WrapOneTab.find('.JsChkDeliveryTimer').attr('checked', false);
    WrapOneTab.find('.TbBoMoiOtherItem tbody').html('');
}

/**
 * @Author: DungNT Mar 15, 2017
 * @Todo: xử lý button cập nhật số ĐT
 */
function handleUpdatePhone(){
    $('.BtnUpdatePhone').click(function(){
        WrapOneTab = $(this).closest('.WrapOneTab');
//        alert(WrapOneTab.find('.CallCurrentTab').val());
        var customer_id     = WrapOneTab.find('.customer_id').val();
//        var phone_number    = '0'+WrapOneTab.find('.CallCurrentPhone').val();
        var phone_number    = WrapOneTab.find('.CallCurrentPhone').val();
        var customer_name   = WrapOneTab.find('.CustomerInfoName').text();
        var customer_phone  = WrapOneTab.find('.CustomerInfoPhone').text();
        var customer_address = WrapOneTab.find('.CustomerInfoAddress').text();
        var msg = 'Bạn chắc chắn cập nhật số: '+phone_number;
        msg += '\nCho khách hàng: '+ customer_name;
        msg += '\nĐT: '+ customer_phone;
        msg += '\nĐ/C: '+ customer_address;
        if(confirm(msg)){
            $.blockUI({ overlayCSS: { backgroundColor: '#fff' } });
            $.ajax({
                url: $(this).attr('next'),
                data: {customer_id:customer_id, phone: phone_number},
                type: 'post',
                dataType: 'json',
                success:function(data){
                    $.unblockUI();
                    WrapOneTab.find('.CustomerInfoPhone').text(data['phone']);
                    alert(data['msg']);
                }
            });
        }
    });
}

/**
 * @Author: DungNT Mar 16, 2017
 * @Todo: bind event khi click cập nhật trạng thái cuộc gọi không thành công
 */
function handleCallFailedUpdate() {
    $('.CallFailedBtnUpdate').click(function(){
        WrapOneTab = $(this).closest('.WrapOneTab');
        var call_uuid   = WrapOneTab.find('.parent_call_uuid').val();
        var agent_id    = WrapOneTab.find('.UpdateAgentId').val();
        var note        = WrapOneTab.find('.CallFailedNote').val();

        if($.trim(call_uuid) == '' || call_uuid==0){
            alert('Không có cuộc gọi đến');
            return ;
        }
        if($.trim(agent_id) == ''){
            alert('Chưa chọn đại lý');
            return ;
        }
        var status  = 0;
        
        WrapOneTab.find('.CallFailedStatus').each(function(){
            if($(this).is(':checked')){
                status  = $(this).val();
            }
        });
        if(status==0){
            alert('Chưa chọn lý do cuộc gọi'); return ;
        }

        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } });
        $.ajax({
            url: $(this).attr('next'),
            data: {call_uuid:call_uuid, status: status, note: note, agent_id: agent_id},
            type: 'post',
            dataType: 'json',
            success:function(data){
                $.unblockUI();
                WrapOneTab.find('.BoxRenderNotify').html(data['msg']);
//                alert(data['msg']);
                // Call function Free Line
            }
        });
        
    });
}

/* May 21, 2017 for Bo Moi action */
function bindEventBoMoi(){
    $('.SpjCallUp').click(function(){
        var WrapChangeQty   = $(this).closest('.WrapChangeQty');
        var NameQty         = WrapChangeQty.attr('NameQty');
        var input           = WrapChangeQty.find('.'+NameQty);
        var cVal            = input.val()*1;
        cVal += 1;
//        console.log(cVal);
        input.val(cVal);
    });
    $('.SpjCallDown').click(function(){
        var WrapChangeQty   = $(this).closest('.WrapChangeQty');
        var NameQty         = WrapChangeQty.attr('NameQty');
        var input           = WrapChangeQty.find('.'+NameQty);
        var cVal            = input.val()*1;
        if(cVal == 0 ){ return; }
        cVal -= 1;
        input.val(cVal);
    });
}

// to do bind autocompelte for input
// @param objInput : is obj input ex  $('.customer_autocomplete')
function fnBindAutocompleteAgent(objInput, urlSource){
    var parent_div = objInput.closest('.WrapAgentSearch');
    objInput.autocomplete({
        source: urlSource,
        minLength: '2',
        close: function( event, ui ) { 
            //$( "#GasStoreCard_materials_name" ).val(''); 
        },
        search: function( event, ui ) { 
            objInput.addClass('grid-view-loading-gas');
        },
        response: function( event, ui ) { 
            objInput.removeClass('grid-view-loading-gas');
            var json = $.map(ui, function (value, key) { return value; });
            if(json.length<1){
                var error = '<div class=\'errorMessage clr autocomplete_name_text\'>Không tìm thấy dữ liệu.</div>';
                if(parent_div.find('.autocomplete_name_text').size()<1){
                    parent_div.find('.remove_row_item').after(error);
                }
                else
                    parent_div.find('.autocomplete_name_text').show();
            }
        },
        select: function( event, ui ) {
            objInput.attr('readonly',true);
            parent_div.find('.autocomplete_name_text').hide();
            parent_div.find('.OrderBoMoiAgentId').val(ui.item.id);
        }
    });
}

function fnRemoveNameAgent(this_){
    var parent_div = $(this_).closest('.WrapAgentSearch');
    parent_div.find('.OrderBoMoiAgentName').val("").attr("readonly",false);
    parent_div.find('.OrderBoMoiAgentId').val(0);
}
function fnOrderBoMoiType(){
    $('.OrderBoMoiType').click(function(){
        var WrapOneTab = $(this).closest('.WrapOneTab');
        var cVal = $(this).val()*1;
        WrapOneTab.find('.WrapAgentSearch, .WrapDieuXe').hide();
        if(cVal == 1 || cVal == 3){// Giao ngay + thu vỏ
            WrapOneTab.find('.WrapAgentSearch').show();
        }else{// xe tải giao
            WrapOneTab.find('.WrapDieuXe').show();
        }
    });
}

/**
* @Author: DungNT May 21, 2017
* @Todo: xử lý validate và submit order
*/
function fnOrderBoMoiSubmit(){
    $('.OrderBoMoiSubmit').click(function(){
        var WrapOneTab      = $(this).closest('.WrapOneTab');
        var call_uuid       = WrapOneTab.find('.parent_call_uuid').val();
        var customer_id     = WrapOneTab.find('.customer_id').val();
        var phone_number    = WrapOneTab.find('.CallCurrentPhone').val();
        var customer_name   = WrapOneTab.find('.CustomerInfoName').text();
        var customer_phone  = WrapOneTab.find('.CustomerInfoPhone').text();
        var customer_address = WrapOneTab.find('.CustomerInfoAddress').text();
        var agent_name      = WrapOneTab.find('.OrderBoMoiAgentName').val();
        var dieuxe_name     = WrapOneTab.find('.OrderBoMoiDieuXeId').find('option:selected').text();
        
        var CallB50     = WrapOneTab.find('.CallB50').val()*1;
        var CallB45     = WrapOneTab.find('.CallB45').val()*1;
        var CallB12     = WrapOneTab.find('.CallB12').val()*1;
        var CallB6      = WrapOneTab.find('.CallB6').val()*1;
        
        var agent_id        = WrapOneTab.find('.OrderBoMoiAgentId').val()*1;
        var dieuxe_id       = WrapOneTab.find('.OrderBoMoiDieuXeId').val()*1;
        var note            = WrapOneTab.find('.OrderBoMoiNote').val();
        var date_delivery   = WrapOneTab.find('.OrderBoMoiDate').val();
        var pay_direct      = WrapOneTab.find('.OrderBoMoiPayDirect').is(':checked') ? 1 : 0;
        var delivery_timer  = WrapOneTab.find('.OrderBoMoiDeliveryTimer').val();
        if(!WrapOneTab.find('.JsChkDeliveryTimer').is(':checked')){
            delivery_timer='';
        }

        var order_type      = 0;
        var order_type_label = ''; var nameAgent = '';
        WrapOneTab.find('.WrapOrderType').find('input:radio').each(function(){
            if($(this).is(':checked')){
                order_type          = $(this).val();
                order_type_label    = $(this).closest('.item').find('label').text();
                return false;
            }
        });
        var  bo_moi_other_item = handleBoMoiOtherItem(WrapOneTab);

        // client validate
        var msgError = 'Không thể tạo đơn hàng.';
        var noteClient = '';
        if(order_type == 0){// validate order_type
            alert(msgError+' Chưa chọn loại đơn hàng.');
            return ;
        }
        if( (order_type == 1 || order_type == 3)){// Giao ngay + thu vỏ
            if(agent_id < 1){
                alert(msgError+' Chưa chọn đại lý');
                return ;
            }
            nameAgent = agent_name;
        }else if(order_type == 2){// xe tải giao, chưa chọn
            if(dieuxe_id < 1){
                alert(msgError+' Chưa chọn điều xe');
                return ;
            }
            nameAgent = dieuxe_name;
        }
        if(customer_id < 1){
            alert(msgError+' Chưa chọn khách hàng');
            return ;
        }
        if(CallB50 < 1 && CallB45 < 1 && CallB12 < 1 && CallB6 < 1 && WrapOneTab.find('.other_materials_id').size() < 1 ){
            alert('Không thể tạo đơn hàng. Chưa nhập số lượng');
            return ;
        }
        // client validate
        // build string msg
        var sQty = buildStringQty(CallB50, CallB45, CallB12, CallB6);
        
        var msg = 'Xác nhận đơn hàng: '+ order_type_label + ' - '+date_delivery;
            msg += '\n\n '+ sQty;
            msg += '\n\n '+ nameAgent;
            msg += '\n\n'+ customer_name;
//        if(!confirm(msg)){// bỏ, nếu cần thì open lại
//            return ;
//        }
        if( (order_type == 2)){// xe tải giao
            noteClient =  note;
        }else{
            noteClient = sQty + ' - ĐT: '+phone_number+' '+note;
        }

        WrapOneTab.block({  overlayCSS:  { backgroundColor: '#fff' } }); 
        $.ajax({
            url: $('.UrlBoMoiSubmit').val(),
            data: {customer_id:customer_id, agent_id: agent_id, user_id_executive: dieuxe_id, note: noteClient, date_delivery: date_delivery,
                call_uuid: call_uuid, b50: CallB50, b45: CallB45, b12: CallB12, b6: CallB6, phone_number: phone_number, order_type: order_type, pay_direct: pay_direct,
                delivery_timer: delivery_timer, bo_moi_other_item: bo_moi_other_item
            },
            type: 'post',
            dataType: 'json',
            success:function(data){
                WrapOneTab.find('.DisplayError').html(data['html']);
                if(data['success']){
                    setEmptyBoMoiQtyOnly(WrapOneTab);
                }else{
                    alert(data['msg']);
                }
                WrapOneTab.unblock();
                // Call function Free Line
            }
        });
        
    });
}

/**
 * @Author: DungNT Sep 02, 2019
 * @Todo: handle params BoMoiOtherItem: bep van day
 * @return: build string param id - qty: 106-1,107-6,109-8,
 */
function handleBoMoiOtherItem(WrapOneTab) {
    var index       = 0;
    var sOtherItem   = '';
    WrapOneTab.find('.other_materials_id').each(function(){
        var id          = $(this).val();
        var qty         = WrapOneTab.find('.other_materials_qty').eq(index++).val();
        var itemFormat  = id + '-' + qty;
        sOtherItem += (itemFormat+',');
    });
    return sOtherItem;
}


/**
* @Author: DungNT May 21, 2017
* @Todo: build string qty book
*/
function buildStringQty(CallB50, CallB45, CallB12, CallB6){
    var aQty = [];
    if(CallB50 > 0){
        aQty.push(CallB50+' bình 50 kg');
    }
    if(CallB45 > 0){
        aQty.push(CallB45+' bình 45 kg');
    }
    if(CallB12 > 0){
        aQty.push(CallB12+' bình 12 kg');
    }
    if(CallB6 > 0){
        aQty.push(CallB6+' bình 6 kg');
    }
    return aQty.join(', ');
}
    
    
function bindChangeAgent(){
    $('body').on('click', '.ShowChangeAgent', function(){
        $(this).hide();
        var td = $(this).closest('td');
        td.find('.WrapChangeAgentDropdown').show();
    });
    $('body').on('click', '.CancelChangeAgent', function(){
        var td = $(this).closest('td');
        td.find('.WrapChangeAgentDropdown').hide();
        td.find('.ShowChangeAgent').show();
    });
    $('body').on('click', '.SaveChangeAgent', function(){
        var td = $(this).closest('td');
        var status = td.find('.ChangeAgentValue').val();
        if($.trim(status) == ''){
            alert("Chưa chọn lý do không lấy gas của khách hàng");
            return false;
        }
        var url_ = $(this).attr('next');
        $.ajax({
            url:url_,
            type: 'post',
//            dataType: 'json',
            data: {status:status},
            success: function(data){
                td.closest('tr').remove();
            }
        });
    });
/****** Jun 14, 2017 for call mornitor ************/
}

/** @Author: DungNT Jun 14, 2017 
 * @Todo: Cập nhật thông tin phone gọi đến lên monitor NV
 */
function callMonitor(record) {
    var call_status     = record.call_status*1;// 2:Dialing, 3:DialAnswer, 4:HangUp
//    var phone_number    = '0'+record.msg;
    var phone_number        = record.msg;
    var call_temp_id        = record.call_temp_id;
    var WrapCallMonitorTb   = $('.CallMonitorTb');
    var call_time           = record.created_date;
    var destination_number  = record.destination_number;
    var bgClass = 'bg-blue';
//    console.log(record);
    switch (call_status){
        case 1: // Start
                bgClass = 'bg-green';
                var call_uuid = record.call_uuid;
            break;
        case 2: // Dialing
                var call_uuid           = record.call_uuid;
                var parent_call_uuid    = record.parent_call_uuid;
                call_uuid = parent_call_uuid;
            break;
        case 3: // DialAnswer
                bgClass = 'bg-answer';
                var call_uuid       = record.call_uuid;
//                var child_call_uuid = record.child_call_uuid;// is call_uuid of Dialing
//                call_uuid = child_call_uuid;
            break;
        case 4: // HangUp 
                var call_uuid           = record.call_uuid;// is call_uuid of Start
            break;
        case 7: // MissCall
                bgClass = 'bg-red';
                var call_uuid = record.call_uuid;
            break;
        default:
            console.log('1 not catch event status: '+call_status);
            return ;
            break;
    }
    call_uuid = call_uuid.replace('.', '-');// bỏ dấu . trong chuỗi để class hợp lệ  
    call_time = call_time.split(" ");// tach lay time
    call_time = call_time[1];
    
    var test = '  child_call_uuid: '+record.child_call_uuid +' parent_call_uuid: '+record.parent_call_uuid;
    if(phone_number == '01684331552'){
//        console.log(' call_uuid: '+call_uuid + ' phone_number: '+phone_number + ' bgClass: '+ bgClass + ' call_status: '+ call_status + ' destination_number: '+destination_number + test);
    }
    /* Tìm record có sẵn change color, text status, destination_number
     * nếu không co thì add new
     */
    if(call_status == 1){// Start
        callMonitorAdd(WrapCallMonitorTb, call_uuid, phone_number, bgClass, call_time, call_status, destination_number);
    }else{
        callMonitorUpdate(WrapCallMonitorTb, call_uuid, phone_number, bgClass, call_time, call_status, destination_number);
    }
    callMonitorClean(WrapCallMonitorTb);
    
}
/** @Author: DungNT Jun 14, 2017
 * @Todo: add new row to tb monitor 
 */
function callMonitorAdd(WrapCallMonitorTb, call_uuid, phone_number, bgClass, call_time, call_status, destination_number) {
   var html = '' ;
    html += '<tr>';
        html += '<td class="item_c order_no"></td>';
        html += '<td class="'+call_uuid+'">'+phone_number+'</td>';
        html += '<td class="item_c">'+call_time+'</td>';
        html += '<td class="item_c">';
            html += '<div class="'+bgClass+' color_call"></div>';
        html += '</td>';
        html += '<td class="item_c call_status_text">Gọi đến chờ</td>';
        html += '<td class="item_c destination_number">205</td>';
    html += '</tr>';
    WrapCallMonitorTb.find('tbody').prepend(html);
    fnRefreshOrderNumber();    
}

/** @Author: DungNT Jun 14, 2017
 * @Todo: add new row to tb monitor 
 */
function callMonitorUpdate(WrapCallMonitorTb, call_uuid, phone_number, bgClass, call_time, call_status, destination_number) {
    var tr = WrapCallMonitorTb.find('.'+call_uuid).closest('tr');
    if(!tr){
        console.log('Monitor is null tr');
        return ;
    }
    if(call_status == 4){
        tr.find('.color_call').attr('class', 'color_call');
    }else{
        tr.find('.color_call').attr('class', bgClass+ ' color_call');
    }
    
    var call_status_text = '';
    switch (call_status){
        case 2: // Dialing
                call_status_text = 'Dialing';
            break;
        case 3: // DialAnswer
                call_status_text = 'Trả lời';
            break;
        case 4: // HangUp 
                call_status_text = 'Xong';
            break;
        case 7: // MissCall
                call_status_text = 'Miss Call';
            break;
        default: 
            console.log('2 not catch event status: '+call_status);
            return ;
            break;
    }
    
    tr.find('.call_status_text').text(call_status_text);
    if((destination_number*1) > 0){
        tr.find('.destination_number').text(destination_number);
    }
}
/** @Author: DungNT Jun 14, 2017
 * @Todo: remove row if over limit row
 */
function callMonitorClean(WrapCallMonitorTb){
    var countRow = WrapCallMonitorTb.find('tbody tr').size();
    if(countRow > 30) {
         WrapCallMonitorTb.find('tbody tr:last').remove();
    }
}

/****** Jun 14, 2017 for call mornitor ************/

/** @Author: DungNT Dec 04, 2017
 *  @Todo: event Remove row notify Order App Gas24h when one user has confirm
 */
function confirmAppGas24h(record){
    var obj_id          = record.obj_id*1;
    var ClassRowSpan    = 'row_trans_' + obj_id;
    console.log(ClassRowSpan);
    $('#transaction-grid tbody').find('.'+ClassRowSpan).closest('tr').remove();
}
/** @Author: DungNT Sep 14, 2018 
 *  @Todo:  Alert any thing to user CallCenter
 */
function alertCallCenter(record){
    var flashMsg = "<div class='flash notice'><a data-dismiss='alert' class='close' href='javascript:void(0)'>×</a>"+record.msg+"</div>";
    $('.SocketBoxRight').prepend(flashMsg);
    alert(record.msg);
}

