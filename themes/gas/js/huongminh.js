$(function() {
    fnBindTrHmtable();
    fnBindFreezetablecolumns();
    // fix for menu DungNT 11-22-2013
    var z_index=200;
    $('#navmenu > li').each(function(){
        $(this).css({'z-index':z_index});
        z_index--;
    });
    // fix for menu DungNT 11-22-2013
    $('.cancel_iframe').click(function(){
        parent.$.fn.colorbox.close();
    });
    bindEventForHelpNumber();
    fnBindCloseFlash();
    fnBindClickShowHide();
    fnRowQtyPriceAmount();
    fnRowQtySum();
    fnSltSupportPercent();
    fnSltPercentDraft();
    fnPreventPaste(); // Dec 16, 2015
    fnBindClickToCall(); // Sep 06, 2018
    fnBtnAjaxOnly(); // Sep 06, 2018
    fnCopyToClipboard();
}); 


function fnPreventPaste(){
    $('.PreventPaste').bind("paste",function(e) {
        e.preventDefault();
    });
}

$(window).scroll(function() {
    if ($(this).scrollTop()>0)
     {
//        $('.backtotop').fadeOut();
        $('.backtotop').show();
     }
    else
     {
         $('.backtotop').hide();
//      $('.backtotop').fadeIn();
     }
 });    

function fnBindCloseFlash(){
    $('body').on('click', '.flash a.close', function(){
        $(this).closest('div.flash').hide();
    });
//  Sep1418 Close $('.flash a.close').click(function(){ $(this).closest('div.flash').hide(); });
}

function showMsgWarning(msg){
     $('.inline_content_warning').html(msg);
     $.colorbox({width:"600px",height:'250px', inline:true, href:"#inline_content_warning"});
}        

function detecting_browsers_by_ducktyping(){
    var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
        // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
    var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
    var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
        // At least Safari 3+: "[object HTMLElementConstructor]"
    var isChrome = !!window.chrome && !isOpera;              // Chrome 1+
    var isIE = /*@cc_on!@*/false || !!document.documentMode;   // At least IE6

    if(isFirefox)
        return 'firefox';
    else if(isChrome)
        return 'chrome';
    else if(isSafari)
        return 'safari';
    else if(isOpera)
        return 'opera';
    else if(isIE)
        return 'ie';
}

// format number: 200,000
function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
}    

// PHP 'afterValidate'=>'js:function(form, data, hasError) {return fnAddCheckSubmit();}'
function fnAddCheckSubmit(){
    if(confirm('Bạn chắc chắn muốn lưu dữ liệu?'))
            return true;
    return false;
}	

function bindEventForHelpNumber(){
        $(".number_only").on('keyup blur change', function() {
        if($.isNumeric($(this).val()) && $.trim($(this).val())!=''){
            $(this).next('div').show().html(commaSeparateNumber($(this).val()*1));
        }
        else
            $(this).next('div').show().html('');
    });    
}

/** 02-13-2014 DungNT
 * to do: overlay screen when user click submit
 * @param {string} BLOCK_UI_COLOR : color code
 */

function fnBindEventSubmitClick(BLOCK_UI_COLOR){
    $('.form').find('button:submit').click(function(){
        fnRunBlockUi(BLOCK_UI_COLOR);
    });
}

function fnRunBlockUi(BLOCK_UI_COLOR){// Oct 14, 2015
    $.blockUI({ overlayCSS: { backgroundColor: BLOCK_UI_COLOR } }); 
}

/** Anh Dung Feb 26, 2014
 * @returns {undefined}
 */
function fnBindRemoveIcon(){
    $('.remove_icon_only').live('click',function(){
        if(confirm('Bạn chắc chắn muốn xóa?')){
            fnBeforeRemoveIcon($(this));
            $(this).closest('tr').remove();
            fnRefreshOrderNumber();
            fnAfterRemoveIcon();
        }
    });
}

function fnBeforeRemoveIcon(this_){} // không để làm gì cả, sẽ overide trong từng file cụ thể

function fnAfterRemoveIcon(){} // không để làm gì cả, sẽ overide trong từng file cụ thể

/** Feb 26, 2014
 * @returns {undefined}
 */
function fnRefreshOrderNumber(){
    validateNumber();
    $('.materials_table').each(function(){
        var index = 1;
        $(this).find('.order_no').each(function(){
            $(this).text(index++);
        });
    });
}    

/** Anh Dung Jul 06, 2016
 */
function fnShowhighLightTr(){
    $('.high_light_tr').each(function(){
        var tr_parent = $(this).closest('tr');    
        tr_parent.css({'background':'#F9966B'});
    });
    
    $('.high_light_td').each(function(){
        var td_parent = $(this).closest('td');    
        td_parent.css({'background':'#8BC34A'});
    });
    $('.high_light_td_red').each(function(){
        var td_parent = $(this).closest('td');    
        td_parent.css({'background':'#F9966B'});
    });
    
    $('.high_light_tr1').each(function(){
        var tr_parent = $(this).closest('tr');    
        tr_parent.css({'background':'#8BC34A'});
    });
}

/** Apr 09, 2014 to do bind event for icon remove material */
function fnBindRemoveMaterial(){
    $('.remove_material_js').live('click',function(){
        if(confirm('Bạn chắc chắn muốn xóa?')){
            var _parent = $(this).closest('.col_material');
            _parent.find('.other_material_id_hide').val('');
            _parent.find('.material_text').text('');
            _parent.find('.material_autocomplete').attr('readonly',false).val('');
        }
    });
}

/** Apr 09, 2014 to do bind event for icon remove material 
 * @param {string} ClassHideId tên class của input ẩn ex: other_material_id_hide
 * @param {string} ClassInputAutocomplete tên class của input autocomplete: ex: material_autocomplete
 * 
 * */
function fnBindRemoveMaterialFix(ClassHideId, ClassInputAutocomplete, ClassNeedEmptyText){
    ClassNeedEmptyText = typeof ClassNeedEmptyText !== 'undefined' ? ClassNeedEmptyText : 'material_text';
    $('.remove_material_js').live('click',function(){
        if(confirm('Bạn chắc chắn muốn xóa?')){
            var _parent = $(this).closest('.col_material');
            _parent.find('.'+ClassHideId).val('');
            _parent.find('.'+ClassNeedEmptyText).text('');
            _parent.find('.'+ClassInputAutocomplete).attr('readonly',false).val('');
        }
    });
}


function validateNumber(){     
//    console.log('Bind event input number');
    $(".number_only_v1").each(function(){
            $(this).unbind("keydown");
            $(this).bind("keydown",function(event){
                if( !(event.keyCode == 8                                // backspace
                    || event.keyCode == 46                              // delete
                    || event.keyCode == 110                              // dấu chám bên bàn phím nhỏ
                    || event.keyCode == 9							// tab
                    || event.keyCode == 190							// dấu chấm (point) 
                    || (event.keyCode >= 35 && event.keyCode <= 40)     // arrow keys/home/end
                    || (event.keyCode >= 48 && event.keyCode <= 57)     // numbers on keyboard
                    || (event.keyCode >= 96 && event.keyCode <= 105))   // number on keypad
                    ) {
                        event.preventDefault();     // Prevent character input
                    }
            });
    });

    return;
    $(".number_only").each(function(){
            $(this).unbind("keydown");
            $(this).bind("keydown",function(event){
                if( !(event.keyCode == 8                                // backspace
                    || event.keyCode == 46                              // delete
                    || event.keyCode == 9							// tab
                    || event.keyCode == 190							// dấu chấm (point) 
                    || (event.keyCode >= 35 && event.keyCode <= 40)     // arrow keys/home/end
                    || (event.keyCode >= 48 && event.keyCode <= 57)     // numbers on keyboard
                    || (event.keyCode >= 96 && event.keyCode <= 105))   // number on keypad
                    ) {
                        event.preventDefault();     // Prevent character input
                    }
            });
    });
}        

function validatePhoneNumber(){
    $(".phone_number_only").each(function(){
            $(this).unbind("keydown");
            $(this).bind("keydown",function(event){
             //alert(event.keyCode)
                if( !(event.keyCode == 8                                // backspace
                    || event.keyCode == 46                              // delete
                    || event.keyCode == 9							// tab
//                    || event.keyCode == 190							// dấu chấm (point) 
//                    || event.keyCode == 32							// dấu cách (space)
                    || event.keyCode == 189							// dấu gạch ngang (-) for Chrome + Safari
                    || event.keyCode == 173							// dấu gạch ngang (-) for Firefox
                    || event.keyCode == 109							// dấu gạch ngang bên bàn phím nhỏ (-)
                    || (event.keyCode >= 35 && event.keyCode <= 40)     // arrow keys/home/end
                    || (event.keyCode >= 48 && event.keyCode <= 57)     // numbers on keyboard
                    || (event.keyCode >= 96 && event.keyCode <= 105))   // number on keypad
                    ) {
                        event.preventDefault();     // Prevent character input
                    }
            });
    });
}

function fixTargetBlank() {
	$('.button-column a').attr('target', '_blank');
        $('.remove_html_only').html('');
//	$('.control-nav').find('.btn a').attr('target', '_blank');
}

function fnBindTrHmtable(){
    $('.hm_table tr').live('click',function()
    {
        $('.hm_table tr').removeClass('selected');
        $(this).addClass('selected');
    });    
}

// May 11, 2014 bind event select for tr of freezetablecolumns
function fnBindFreezetablecolumns(){
    
    $('.freezetablecolumns tr').live('click',function()
    {
        var div_grid_view = $(this).closest('div.grid-view');
        div_grid_view.find('tr.selected').removeClass('selected');
        var tr_index = $(this).index();
        div_grid_view.find('.freezetablecolumns').each(function(){
            $(this).find('tbody tr').eq(tr_index).addClass('selected');
        });
    });    
    
    $('.freezetablecolumns_only_tr tr').live('click',function()
    {
        var div_grid_view = $(this).closest('div.grid-view');
        div_grid_view.find('tr.selected').removeClass('selected');
        var tr_index = $(this).index();
        div_grid_view.find('.freezetablecolumns_only_tr').each(function(){
            $(this).find('tbody tr').eq(tr_index).addClass('selected');
        });
    });      
    
    /* có thể dùng kiểu này cũng dc, vì nó click hết cho các đại lý khi xem nhiều tab 
    $('.freezetablecolumns tr').live('click',function()
    {
        $('.freezetablecolumns tr').removeClass('selected');
        var tr_index = $(this).index();
        $('.freezetablecolumns').each(function(){
            $(this).find('tbody tr').eq(tr_index).addClass('selected');
        });
    });    
    
    $('.freezetablecolumns_only_tr tr').live('click',function()
    {
        $('.freezetablecolumns_only_tr tr').removeClass('selected');
        var tr_index = $(this).index();
        $('.freezetablecolumns_only_tr').each(function(){
            $(this).find('tbody tr').eq(tr_index).addClass('selected');
        });
    });    
    */
    
}

function fnAddClassOddEven(className){
    $('.'+className).each(function(){    
        $(this).find("tbody > tr:odd").addClass("odd");
        $(this).find("tbody > tr:even").addClass("even");
    })
}

function BindClickClose(BLOCK_UI_COLOR){ // Dec 15, 2014
    $('.btn_closed_tickets').live('click',function(){
        var alert_text = $(this).attr('alert_text');
        if(confirm(alert_text)){
            $.blockUI({ overlayCSS: { backgroundColor: BLOCK_UI_COLOR } }); 
            return true;
        }
        return false;
    }); 
    $('.OnlyRunLinkAlertText').live('click',function(){
        var alert_text = $(this).attr('alert_text');
        if(confirm(alert_text)){
            return true;
        }
        return false;
    }); 
}

/**
 * @Author: DungNT Sep 11, 2015
 */
function fnBindClickShowHide() {
    $('.ClickShowHide').live('click',function(){
        var wrap_div = $(this).closest('.ClickShowHideWrap');
        wrap_div.find('.ClickShowHideItem').toggle();
    });
}

/**
 * @Author: DungNT Jun 19, 2015
 * @Todo: dùng cho các table format qty*price = amount
 */
function fnRowQtyPriceAmount() {
//    $('.items_qty, .items_price').live('keyup',function()
    $('.items_qty, .items_price').live('change',function()
    {
        // 1. for amount
        var wrap = $(this).closest('.RowQtyPriceAmount');
        var qty = wrap.find('.items_qty').val();
        var price = wrap.find('.items_price').val();
        var amount = qty*price*1;
        amount = fnConvertFloatNumber(amount);
        wrap.find('.items_amount').attr('data_gas', amount);
        wrap.find('.items_amount').text(commaSeparateNumber(amount));
        
        // 2. for grand total
        var items_grand_total = 0;
        $('.items_qty').each(function(){
            var wrap = $(this).closest('.RowQtyPriceAmount');
            var qty = wrap.find('.items_qty').val();
            var price = wrap.find('.items_price').val();
            var amount = qty*price*1;
            items_grand_total += amount;
        });
        
        items_grand_total = fnConvertFloatNumber(items_grand_total);
        var wrap_table = $(this).closest('.WrapRowQtyPriceAmount');
        wrap_table.find('.items_grand_total').text(commaSeparateNumber(items_grand_total));
    });
}

/**
 * @Author: DungNT Now 06, 2015
 * @Todo: dùng cho các table format sum qty
 */
function fnRowQtySum() {
//    $('.items_qty, .items_price').live('keyup',function()
    $('.items_qty').live('change',function()
    {
        // 1. for sum qty
        var qty_sum = 0;
        $('.items_qty').each(function(){
            var wrap = $(this).closest('.RowQtySum');
            var qty = wrap.find('.items_qty').val()*1;
            qty_sum += qty;
        });
        qty_sum = fnConvertFloatNumber(qty_sum);
        var wrap_table = $(this).closest('.WrapRowQtySum');
        wrap_table.find('.items_qty_sum').text(commaSeparateNumber(qty_sum));
    });
}

/**
 * @Author: DungNT Jul 30, 2016
 * @Todo: dùng cho các format qty*price = amount ở cùng 1 row trên table
 * @pram: string class: items_qty
 * @pram: string class: items_price
 * @pram: string class: items_amount = qty * price
 * @pram: string class: items_grand_total to sum
 * @pram: string class: RowWrapQtyPriceAmount class wrap 1 block qty*price
 * @pram: string class: WrapTable class table wrap để tính items_grand_total
 */
function handleQtyPriceAmount(items_qty, items_price, items_amount, class_grand_total, data_amount_calc, RowWrapQtyPriceAmount, WrapTable, ClassTotalFinal) {
//    $('.items_qty, .items_price').live('keyup',function()
    $('.'+items_qty+', .'+items_price).live('change',function()
    {
        // 1. for amount
        var wrap = $(this).closest('.'+RowWrapQtyPriceAmount);
        var qty = wrap.find('.'+items_qty).val();
        var price = wrap.find('.'+items_price).val();
        var amount = qty*price*1;
        amount = fnConvertFloatNumber(amount);
        wrap.find('.'+items_amount).attr(data_amount_calc, amount);
        wrap.find('.'+items_amount).text(commaSeparateNumber(amount));
        
        // 2. for grand total
        var items_grand_total = 0;
        $('.'+items_qty).each(function(){
            var wrap = $(this).closest('.'+RowWrapQtyPriceAmount);
            var qty = wrap.find('.'+items_qty).val();
            var price = wrap.find('.'+items_price).val();
            var amount = qty*price*1;
            items_grand_total += amount;
        });
        
        items_grand_total = fnConvertFloatNumber(items_grand_total);
        var wrap_table = $(this).closest('.'+WrapTable);
        wrap_table.find('.'+class_grand_total).text(commaSeparateNumber(items_grand_total));
        if($('.'+ClassTotalFinal).size() > 0){// Xử lý cho form
            $('.'+ClassTotalFinal).val(items_grand_total);
            $('.'+ClassTotalFinal).trigger('change');
        }
    });
}

function fnRowSumAnyField(class_item, class_sum) {
    // 1. for sum qty
    var qty_sum = 0;
    $(class_item).each(function(){
        var qty = $(this).val()*1;
        qty_sum += qty;
    });
    qty_sum = fnConvertFloatNumber(qty_sum);
    $(class_sum).text(commaSeparateNumber(qty_sum));
}

/**
 * @Author: DungNT Jun 16, 2015
 * @Todo: convert to number have comma
 * @Param: $model
 */
function fnConvertFloatNumber(number) {
    var res = parseFloat(number);
    res = Math.round(res * 100) / 100;
    if(isNaN(res)){
        res = 0;
    }
    return res;
}

/**
 * @Author: DungNT Jun 19, 2015
 * @Todo: something
 */
function fnSltSupportPercent() {
    $('.support_percent').change(function(){
        var percent = $(this).val();
        var wrap = $(this).closest('.RowQtyPriceAmount');
        var amount_span = wrap.find('.items_amount');
        var amount = amount_span.attr('data_gas');
        var amount_after_discount = amount * (100 - percent)/100;
        amount_after_discount = fnConvertFloatNumber(amount_after_discount);
        wrap.find('.amount_final').html(commaSeparateNumber(amount_after_discount));
    });
}

/**
 * @Author: DungNT Jun 19, 2015
 * @Todo: something
 */
function fnSltPercentDraft() {
    $('.percent_draft').change(function(){
        var percent = $(this).val();
        $('.support_percent').val(percent);
        $('.support_percent').trigger('change');
    });
}

/** Mar 11, 2014 -  DungNT - COPY FROM MINDEDGE
 * Dùng để cập nhật url tiếp theo khi change select
 * @param {string} classOfSelect class of tag select
 * @param {string} requestUri 
 */
function fnUpdateNextUrl(classOfSelect, requestUri, attributeAdd){
    $(classOfSelect).find('option').each(function(){        
        $(this).attr('next',updateQueryStringParameter(requestUri, attributeAdd, $(this).val()));
    });  
    // reg event
    $(classOfSelect).change(function(){
        window.location= $('option:selected', this).attr('next');
    });
}

/**
* @param {String} uri
* @param {String} key : is name new attribute
* @param {String} value : is value of new attribute
* @returns {String}             */
// http://stackoverflow.com/questions/5999118/add-or-update-query-string-parameter
function updateQueryStringParameter(uri, key, value) {
  var re = new RegExp("([?|&])" + key + "=.*?(&|$)", "i");
  separator = uri.indexOf('?') !== -1 ? "&" : "?";
  if (uri.match(re)) {
    return uri.replace(re, '$1' + key + "=" + value + '$2');
  }
  else {
    return uri + separator + key + "=" + value;
  }
}  

function fnBindFixLabelRequired(){
    $('.fix_custom_label_required').each(function(){
        var label = $(this).find('label:first');
        if(label.find('span').size()<1){
            label.append('<span class="required"> *</span>');
        }
    });
}

/**
 * @Author: DungNT Jan 31, 2016
 */
function fnBindScheduleType(){
    $('.schedule_type').change(function(){
        var wrap_div = $(this).closest('.wrap_schedule_type');
        wrap_div.find('.schedule_type_same_hide').hide();
        wrap_div.find('.schedule_type_'+$(this).val()).show();
        if($(this).val() == 2){
            wrap_div.find('.schedule_in_month').val(1);
        }else{
            wrap_div.find('.schedule_in_month').val("");
        }
    });
}

/**
 * @Author: DungNT Jan 31, 2016
 */
function fnBindChangeProvince(BLOCK_UI_COLOR, url_){
    $('#Users_province_id').change(function(){
        var province_id = $(this).val();        
        $.blockUI({ overlayCSS: { backgroundColor: BLOCK_UI_COLOR } }); 
        $.ajax({
            url: url_,
            data: {ajax:1,province_id:province_id},
            type: "get",
            dataType:'json',
            success: function(data){
                $('#Users_district_id').html(data['html_district']);
                $('#Users_storehouse_id').html(data['html_store']);
                $.unblockUI();
            }
        });
    });
}

/**
 * @Author: DungNT Jun 05, 2016
 * https://github.com/autoNumeric/autoNumeric
 */
function fnInitInputCurrency() {
    $(".ad_fix_currency").each(function(){
        $(this).autoNumeric('init', {lZero:"deny", aPad: false} ); 
    });
    $(".ad_fix_currency_float").each(function(){
        $(this).autoNumeric('init', {lZero:"deny", aPad: false} );
    });
}
/**
 * @Author: DungNT Jul 21, 2016
 * @Todo: handle change price code
 * @Param: usersPrice/update create
 */
function gSelectChange(price_other){
    $('.gSelect').change(function(){
        var tr = $(this).closest('tr.cash_book_row');
        var v = $(this).val();
        if(v == price_other){
            tr.find('.gPrice').show();
        }else{
            tr.find('.gPrice').val('').hide();
//            tr.find('.gPrice').closest('td').find('.help_number').text('');
        }
    });
    $('.gSelectSmall').change(function(){
        var tr = $(this).closest('tr.cash_book_row');
        var v = $(this).val();
        if(v == price_other){
            tr.find('.gPriceSmall').show();
        }else{
            tr.find('.gPriceSmall').val('').hide();
//            tr.find('.gPriceSmall').closest('td').find('.help_number').text('');
        }
    });
    
    $('.gSelect4Kg').change(function(){
        var tr = $(this).closest('tr.cash_book_row');
        var v = $(this).val();
        if(v == price_other){
            tr.find('.gPrice4Kg').show();
        }else{
            tr.find('.gPrice4Kg').val('').hide();
        }
    });
    $('.gSelect6Kg').change(function(){
        var tr = $(this).closest('tr.cash_book_row');
        var v = $(this).val();
        if(v == price_other){
            tr.find('.gPrice6Kg').show();
        }else{
            tr.find('.gPrice6Kg').val('').hide();
        }
    });
    
}

/**
* @Author: DungNT Now 08, 2016
* @Todo: bindRadioEvent
*/
function bindRadioEvent(){
    $('.GasCheckboxList').each(function(){
        $(this).find('input:radio').click(function(){
            var li = $(this).closest('li');
            var WrapRadio = $(this).closest('div.GasCheckboxList');
            fnMakeRadioCss(li, $(this), WrapRadio);
        });
        
        $(this).find('input:radio').each(function(){
            var li = $(this).closest('li');
            if($(this).is(":checked")){
                var WrapRadio = $(this).closest('div.GasCheckboxList');
                fnMakeRadioCss(li, $(this), WrapRadio);
            }
        });
    });
}
    
/**
* @Author: DungNT Feb 17, 2016
* @Todo: something
* @Param: li element wrap checkbox
* @Param: element object checkbox
* @Param: WrapRadio object div wrap li
*/
function fnMakeRadioCss(li, element, WrapRadio){
    WrapRadio.find('li').css({"background-color":'transparent'});
    li.css({"background-color":'#4CAF50'});
    return ;
    if(element.is(":checked")){
        li.css({"background-color":'#4CAF50'});
    }else{
        li.css({"background-color":'note'});
    }
}

function removeCommaInString(str){
    return str.replace(/,/g , '');
}
function bindEventAddVo(){// inventoryCustomer/create
    $('body').on('click', '.AddVo', function(){
        var tdBig      = $(this).closest('.ClassTdVo');
        var tdBlockVo = tdBig.find('.BlockVo:first').closest('td');
        var BlockVo = tdBig.find('.BlockVo:first').clone();
        BlockVo.find('.BlockVoRemove').show();
        BlockVo.find('select').val('');
        BlockVo.find('input').val('');
        BlockVo.find('input').attr('readonly', false);
        bindMaterialAutocomplete(BlockVo.find('.material_autocomplete'));
        tdBlockVo.append(BlockVo);

    });
    $('body').on('click', '.BlockVoRemove', function(){
        $(this).closest('.BlockVo ').remove();

    });
    
    $('body').on('click', '.RemoveMaterialJs', function(){
        var parent_div = $(this).closest('.BlockVo');
        parent_div.find('.materials_id_vo').val('');
        parent_div.find('.material_autocomplete').attr('readonly',false).val('');
    });   
}


/**  @Author: DungNT Jul 03, 2018. bind sự kiện chọn load quận huyện khi chọn tỉnh 
 *  @param: string idInputProvince ex: #GasUphold_province_id
 *  @param: string idInputDistrict ex: #GasUphold_district_id
 *  @param: string urlRequest ex: GasUphold_district_id
 *  @Call: fnSelectProvinceDistrict('#CustomerDraft_province_id', '#CustomerDraft_district_id' , "<?php echo Yii::app()->createAbsoluteUrl('admin/ajax/get_slt_district');?>");
 * */
function fnSelectProvinceDistrict(idInputProvince, idInputDistrict, urlRequest ){
    $('body').on('change', idInputProvince, function(){
        var province_id = $(this).val();        
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        $.ajax({
            url: urlRequest,
            data: {ajax:1,province_id:province_id}, 
            type: "get",
            dataType:'json',
            success: function(data){
                $(idInputDistrict).html(data['html_district']);                
                $.unblockUI();
            }
        });
    });
}
/**  @Author: DungNT Sep 06, 2018. bind sự kiện click to call * */
function fnBindClickToCall(){
    $('body').on('click', '.SpjClickToCall', function(){
        var urlServer      = $(this).attr('next');
        var urlMakeCall    = $(this).attr('urlMakeCall');
        $.blockUI({ overlayCSS: { backgroundColor: '#fff' } }); 
        $.ajax({
            url: urlServer,
            data: {urlMakeCall:urlMakeCall},
            type: 'post',
            dataType:'json',
            success: function(data){
                $.unblockUI();
                if(!data['success']){
                    alert(data['msg'])
                }
            }
        });
    });
    
    $('body').on('mousedown', '.SpjClickToCall', function(ev){
        if(ev.which == 3){
            copyTextToClipboard($(this).text());
        }
    });
    
}

// Sep0918 Handle copy text to clipboard
function fallbackCopyTextToClipboard(text) {
  var textArea = document.createElement("textarea");
  textArea.value = text;
  document.body.appendChild(textArea);
//  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    console.log('Fallback: Copying text command was ' + msg);
  } catch (err) {
    console.error('Fallback: Oops, unable to copy', err);
  }

  document.body.removeChild(textArea);
}
function copyTextToClipboard(text) {
  if (!navigator.clipboard) {
    fallbackCopyTextToClipboard(text);
    return;
  }
  
//  navigator.clipboard.writeText(text).then(function() {
//    console.log('Async: Copying to clipboard was successful!');
//  }, function(err) {
//    console.error('Async: Could not copy text: ', err);
//  });
  
}

/**
 * @Author: DungNT Sep 20, 2018
 * @Todo: xử lý request one url by ajax for delete item
 */
function fnBtnAjaxOnly() {
    $('body').on('click', '.BtnAjaxDeleteItem', function(){
        var alert_text = $(this).attr('alert_text');
        if(confirm(alert_text)){
            $.blockUI({ overlayCSS: { backgroundColor: 'fff' } });
            var url_ = $(this).attr('AjaxUrl');
            var WrapBtnAjaxDeleteItem = $(this).closest('.WrapBtnAjaxDeleteItem');
            $.ajax({
                url: url_,
                type: 'post',
                dataType: 'json',
                success: function(data){
                    $.unblockUI();
                    var flashMsg = "<div class='flash notice'><a data-dismiss='alert' class='close' href='javascript:void(0)'>×</a>"+data['msg']+"</div>";
                    WrapBtnAjaxDeleteItem.before(flashMsg);
                    WrapBtnAjaxDeleteItem.remove();
                }
            });
        }
        return false;
    });
}
    
/**
 * @Author: DungNT Oct 21, 2018
 * @Todo: bind event click to copy
 */
function fnCopyToClipboard() {
    $('.copyToClipboard').click(function(){
        var element_name = $(this).attr('data-class');
        copyToClipboard($('.'+element_name));
    });
}
    
function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
}