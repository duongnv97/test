<?php

class GasReceipts extends BaseSpj {

    const TYPE_FOR_EMPLOYEE = 1;
    const TYPE_FOR_CAR      = 2;
    
    const MAXLENGTH_NUMBER  = 6;

    public $autocomplete_name, $autocomplete_name_1, $create_for, $created_date_only;
    public $aUsed, $ArrayNotUsed = array();
    public $message, $number, $mAssignNumber, $mRemoveNumber, $pageSize;

    public static function getArrayStatus() {
        return [
            STATUS_ACTIVE => 'Hoạt động',
            STATUS_INACTIVE => 'Không hoạt động',
        ];
    }

    public static function getArrayCreateFor() {
        return [
            self::TYPE_FOR_EMPLOYEE => 'Nhân viên PVKH',
            self::TYPE_FOR_CAR => 'Xe tải',
        ];
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return '{{_receipts}}';
    }

    public function rules() {
        return array(
            array('object_id, number_from, number_to', 'required', 'on' => 'create_receipts, update_receipts'),
            array('name, object_id, role_id, province_id, agent_id,  status, number_from, number_to, json, created_by, create_for, date_from, date_to, pageSize', 'safe'),
            array('number_from', 'numerical', 'integerOnly' => true, 'min' => 0, 'max' => 100000, 'tooBig' => 'Nhập giá trị nhỏ hơn 100 000', 'tooSmall' => 'Số chứng từ không tồn tại số âm'),
            array('number_to', 'numerical', 'integerOnly' => true, 'min' => 0, 'max' => 100000, 'tooBig' => 'Nhập giá trị nhỏ hơn 100 000', 'tooSmall' => 'Số chứng từ không tồn tại số âm'),
            array('number_from', 'compare', 'compareAttribute' => 'number_to', 'operator' => '<=', 'message' => 'Số chứng từ bắt đầu nhỏ hơn or bằng số kết thúc'),
//            array('number, object_id', 'validateNumber', 'on'=>'assign_number'),
        );
    }

    public function relations() {
        return array(
            'rUsers' => array(self::BELONGS_TO, 'Users', 'object_id'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rCreatedBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
        );
    }

    public function attributeLabels() {
        return array(
            'name' => 'Tên',
            'object_id' => 'NV hoặc xe tải',
            'role_id' => 'Chức vụ',
            'province_id' => 'Tỉnh',
            'agent_id' => 'Đại lý',
            'number' => 'Số biên bản giao nhận',
            'status' => 'Trạng thái',
            'number_from' => 'Từ',
            'number_to' => 'Đến',
            'created_by' => 'Người tạo'
        );
    }

    public function search() {
        $criteria = new CDbCriteria();
        $criteria->compare('t.id', $this->id);
//        $criteria->addSearchCondition('name', $this->name);
        $criteria->compare('t.object_id', $this->object_id);
        $criteria->compare('t.number_from', $this->number_from);
        $criteria->compare('t.number_to', $this->number_to);
        if (!empty($this->province_id)) {
            $criteria->compare('t.province_id', $this->province_id);
        }
        if (!empty($this->agent_id)) {
            $criteria->compare('t.agent_id', $this->agent_id);
        }
        $criteria->compare('t.status', $this->status);
        if (!empty($this->date_from)) {
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
//            $criteria->addCondition("t.created_date_only>='$date_from'");
//            DateHelper::searchGreater($date_from, 'created_date', $criteria);
            $criteria->addCondition("DATE(t.created_date) >= '$date_from'");
        }
        if (!empty($this->date_to)) {
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
//            $criteria->addCondition("t.created_date_only<='$date_to'");
//            DateHelper::searchSmaller($date_to, 'created_date', $criteria);
            $criteria->addCondition("DATE(t.created_date) <= '$date_to'");
        }
        $criteria->order = 'created_date DESC';
        $pageSize = empty($this->pageSize) ? 20 : $this->pageSize;
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => $pageSize,
            ),
        ));
    }

    /** @Author: TRONG NHAN Aug 5, 2019
     *  @Todo: loading data before save
     *  @Param: param
     * */
    public function handleBeforeSave() {
        $this->number_from = MyFormat::removeComma($this->number_from);
        $this->number_to   = MyFormat::removeComma($this->number_to);
        if ($this->isNewRecord) {
            $this->created_by = MyFormat::getCurrentUid();
        }
        if (!empty($this->rUsers)) {
            $mUsers             = $this->rUsers;
            $this->role_id      = $mUsers->role_id;
            $this->province_id  = $mUsers->province_id;
            $this->agent_id     = $mUsers->parent_id;
        }
    }

    public function getName() {
        $res    = '';
//        $res    = $this->name;
        if ( $this->canSetExpired() ) {
            $link       = Yii::app()->createAbsoluteUrl('admin/gasReceipts/CheckExpired', array('id' => $this->id,));
            $actionText = '<span >Set hết hạn</span>';
            $res       .= '<br><a class="setExpired" href="'. $link . '" target="" >' . $actionText . '</a> ';
        }
        return $res;
    }

    public function getNameView() {
        return $this->name;
    }

    public function getObjectName() {
        return empty($this->rUsers) ? '' : $this->rUsers->getNameWithRole();
    }

    public function getProvince() {
        return empty($this->rUsers) ? '' : $this->rUsers->getProvince();
    }

    public function getAgent() {
        return empty($this->rAgent) ? '' : $this->rAgent->getFullName();
    }

    public function getStatus() {
        $aStatus = GasReceipts::getArrayStatus();
        return empty($aStatus[$this->status]) ? '' : $aStatus[$this->status];
    }

    public function getNumber() {
//        return ActiveRecord::formatCurrency($this->number_from) . ' - ' . ActiveRecord::formatCurrency($this->number_to);
        return $this->number_from . ' - ' . $this->number_to;
    }

    /** @Author: TRONG NHAN Aug 8, 2019
     *  @Todo: Sắp xếp mảng tăng dần
     *  @Param: param
     * */
    public function sortAscArr($arr) {
        if (count($arr) <= 1) {
            return $arr;
        } else {
            $aPivot = $arr[0];
            $aLeft = array();
            $aRight = array();
            for ($i = 1; $i < count($arr); $i++) {
                if ($arr[$i] < $aPivot) {
                    $aLeft[] = $arr[$i];
                } else {
                    $aRight[] = $arr[$i];
                }
            }
            return array_merge($this->sortAscArr($aLeft), array($aPivot), $this->sortAscArr($aRight));
        }
    }

    /** @Author: TRONG NHAN Aug 7, 2019
     *  @Todo: todo
     *  @Param: param
     * */
    public function getStrNumberUsed() {
        $sNumberUsed = '';
        if (!empty($this->json)) {
            $aUsed = json_decode($this->json, true);
            if (is_array($aUsed['number_used'])):
//                $aSort= $this->sortAscArr($aUsed['number_used']);
//                $this->divideArr($aSort, $sNumberUsed);
                $this->divideArr($aUsed['number_used'], $sNumberUsed);
//            elseif(is_array($aUsed)):
//                foreach ($aUsed as $value){
//                    $sNumberUsed .= $value . ' ';
//                }
            endif;
        }
        return $sNumberUsed;
    }

    /** @Author: TRONG NHAN Aug 6, 2019
     *  @Todo: todo
     *  @Param: param
     *  @ex {"number_used":[201,202,203,903,904]}
     * */
    public function findArrNumberNotUsed() {
        $aNumber = array();
        if (!empty($this->number_from) && !empty($this->number_to) && !empty($this->json)) {
            $aUsed = json_decode($this->json, true);
            if (is_array($aUsed['number_used'])) {
//                $aSort = $this->sortAscArr($aUsed['number_used']);
                $aSort = $aUsed['number_used'];
                sort($aSort);
                for ($x = $this->number_from; $x <= $this->number_to; $x++) {
                    if (!in_array($x, $aSort)) {
                        $aNumber[] = $x;
                    }
                }
            }
//            else if (is_array($aUsed)){
//                for ($x = $this->number_from ; $x <= $this->number_to; $x++) {
//                        if (!in_array($x, $aUsed)) {
//                            $aNumber[] = $x;
//                        }
//                    }
//            } 
        }
        return $aNumber;
    }

    /** @Author: TRONG NHAN Aug 6, 2019
     *  @Todo:Trả 
     *  @Param: param
     * */
    public function getStrNumberNotUsed() {
        $aNotUsed = $this->findArrNumberNotUsed();
        $sNumberNotUsed = '';
        if (is_array($aNotUsed)) {
            $this->divideArr($aNotUsed, $sNumberNotUsed);
        }
        return $sNumberNotUsed;
    }

    /** @Author: NamLA Aug 8, 2019
     *  @Todo: tách mảng thành các khoảng
     *  @Param: Input: array, output; string
     * */
    public function divideArr($arr, &$str) {
        sort($arr);
        foreach ($arr as $key => $value) {
            if ($key == 0) {
                $str .= $arr[$key];
            } else {
                if ($key == 1 && $arr[$key] - $arr[$key - 1] != 1) {
                    $str .= ', ' . $arr[$key];
                } else {
                    if ($arr[$key] - $arr[$key - 1] == 1) {
                        if ($key == sizeof($arr) - 1) {
                            $str .= '-' . $arr[$key];
                        }
                    } else {
                        if ($key > 1 && $arr[$key - 1] - $arr[$key - 2] == 1) {
                            $str .= '-' . $arr[$key - 1];
                        }
                        $str .= ', ' . $arr[$key];
                    }
                }
            }
        }
    }

    /** @Author: TRONG NHAN Aug 7, 2019
     *  @Todo: todo
     *  @Param: param
     * */
    public function getCreatedBy() {
        return empty($this->rCreatedBy) ? '' : $this->rCreatedBy->getFullName();
    }

    /** @Author: NamLA Aug 6, 2019
     *  @Todo: 
     *      -chỉ người tạo mới được update 
     *      -ko update được sau 1 ngày tạo 
     *      -status INACTIVE ko update duoc
     *  @Param: 
     * */
    public function canUpdate() {
        $cUid = MyFormat::getCurrentUid();
        return $cUid == $this->created_by && $this->status == STATUS_ACTIVE;
    }
    
    /** @Author: DuongNV
     *  @Todo:
     **/
    public function canSetExpired() {
        $cUid   = MyFormat::getCurrentUid();
        return $cUid == $this->created_by && $this->status == STATUS_ACTIVE;
    }

    
    /** @Author: NhanDT Aug 13, 2019
     *  @Todo: validate số biên bản đã được sử dụng chưa
     *  @Param: $this->object_id id xe hoac nhan vien pvkh, $this->number number of user id
     *  @ex {"number_used":[201,202,203,903,904]}
     * */
    public function validateNumber() {
        $user_id        = $this->object_id;
        $number         = (int) $this->number;
        $mUser          = Users::model()->findByPk($user_id);
        $aRoleMain      = [ROLE_EMPLOYEE_MAINTAIN, ROLE_CAR];
        if( empty($mUser) ) {
            $this->message = 'Nhân viên không tồn tại';
            $this->addError('object_id', $this->message);
            return false;
        }
        if( empty($number) ) {
            $this->message = 'Số biên bản không thể trống';
            $this->addError('number', $this->message);
            return false;
        }
        $role           = $mUser->role_id;
        if (in_array($role, $aRoleMain)){ // NV PVKH or xe tai
            $aReceipts  = $this->getDataSameCond(['object_id'=>$user_id]);
            if( empty($aReceipts) ) {
                $this->message = 'Nhân viên chưa được cấp số biên bản trên hệ thống';
                $this->addError('object_id', $this->message);
                return false;
            }
            if( count($aReceipts) > 2 ){
                $this->message = 'Nhân viên được cấp quá 2 dãy số biên bản, liên hệ KTKV để hỗ trợ';
                $this->addError('object_id', $this->message);
                return false;
            }
            return $this->checkNumber($aReceipts);
        } else if ($role == ROLE_EMPLOYEE_MARKET_DEVELOPMENT){ // NV PTTT chỉ chạy hộ cho đại lý, nên sẽ dc nhập số của all agent
            $aReceipts  = $this->getDataSameCond(['agent_id'=>$mUser->parent_id]);
            return $this->checkNumber($aReceipts);
        } else {
            $this->message = 'Chức vụ phải là PVKH, Xe tải hoặc PTTT';
            $this->addError('object_id', $this->message);
            return false;
        }
        return true;
    }
    
    /** @Author: DuongNV AUg1919
     *  @Todo: get aData same cond
     *  @params: $aReceipts array model receipts 
     **/
    public function getDataSameCond($aCond) {
        $criteria   = new CDbCriteria;
        if( !empty($aCond['object_id']) ){
            $criteria->compare('t.object_id', $aCond['object_id']);
        }
        if( !empty($aCond['agent_id']) ){
            $criteria->compare('t.agent_id', $aCond['agent_id']);
        }
        $criteria->compare('t.status', STATUS_ACTIVE);
        return GasReceipts::model()->findAll($criteria);
    }
    
    /** @Author: DuongNV AUg1919
     *  @Todo: validate number
     *  @params: $aReceipts array model receipts 
     **/
    public function checkNumber($aReceipts) {
        $outOfRange     = 0;
        foreach ($aReceipts as $item) {
            $aDataJson  = empty($item->json) ? [] : json_decode($item->json, true);
            $aUsed      = empty($aDataJson['number_used']) ? [] : $aDataJson['number_used'];
            // Out of range
            if( $this->number < $item->number_from || $this->number > $item->number_to ){
                $outOfRange++;
            } else {
                $this->mAssignNumber = $item;
            }
            // Is used
            if( in_array($this->number, $aUsed) ){
                $this->mRemoveNumber = $item;
                $this->mAssignNumber = null;
                $this->message       = 'Số biên bản đã được sử dụng';
                $this->addError('number', $this->message);
                return false;
            }
        }
        if($outOfRange >= count($aReceipts)){
            $this->mAssignNumber = null;
            $this->message  = 'Số biên bản không nằm trong khoảng đã cấp';
            $this->addError('number', $this->message);
            return false;
        }
        return true;
    }
    
    /** @Author: TRONG NHAN Aug 14,2019
     *  @Todo: gán số biên bản cho nhân viên, nếu gán xong mà full thì set inactive luôn
     *  @Param: $this->object_id id xe hoac nhan vien pvkh, $this->number number of user id
     *  @usage: Nếu cần validate thì uncomment trên rules
     *  $model->object_id   = 171;
        $model->number      = 11235;
        $model->assignNumber();
     *  get message after assign: $model->message
     *  get model being assign: $model->mAssignNumber
     **/
    public function assignNumber() {
        $canAssign          = $this->validateNumber();
        if( $canAssign && !empty($this->mAssignNumber) ){
            $item           = $this->mAssignNumber;
            $aDataJson      = empty($item->json) ? [] : json_decode($item->json, true);
            $aUsed          = empty($aDataJson['number_used']) ? [] : $aDataJson['number_used'];
            $aUsed[]        = $this->number;
            $aDataJson['number_used'] = $aUsed;
            $item->json     = json_encode($aDataJson);
            $countTotal     = $item->number_to-$item->number_from;
            if($countTotal <= count($aUsed)){ // set inactive if full
                $item->status = STATUS_INACTIVE;
            }
            $item->save();
            $this->message  = 'Gán thành công';
            return true;
        }
        return $canAssign;
    }
    
    /** @Author: DuongNV Aug2019
     *  @Todo: remove số biên bản giao nhận, dùng để chỉnh sửa số biên bản khi nhập sai
     *  @Param: 
     *  $model->object_id   = 171;
        $model->number      = 11235;
        $model->removeNumber();
     **/
    public function removeNumber() {
        $this->validateNumber();
        if( !empty($this->mRemoveNumber) ){
            $model          = $this->mRemoveNumber;
            $aDataJson      = empty($model->json) ? [] : json_decode($model->json, true);
            $aUsed          = empty($aDataJson['number_used']) ? [] : $aDataJson['number_used'];
            $key            = array_search($this->number, $aUsed);
            if( $key === FALSE || empty($aUsed[$key]) ) return false;
            unset($aUsed[$key]);
            $aDataJson['number_used'] = array_values($aUsed);
            $model->json    = json_encode($aDataJson);
            $model->save();
            $this->message  = 'Xóa thành công';
            return true;
        }
        $this->message      = 'Số chưa được gán cho nhân viên nào';
        return false;
    }

}
