<?php

/**
 * This is the model class for table "{{_gas_district}}".
 *
 * The followings are the available columns in table '{{_gas_district}}':
 * @property integer $id
 * @property integer $province_id
 * @property string $name
 * @property string $short_name
 * @property integer $status
 */
class GasDistrict extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasDistrict the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_district}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('province_id, status', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>150),
            array('short_name', 'length', 'max'=>100),
            array('id, province_id, name, short_name, status', 'safe'),
            array('province_id, name', 'required','on'=>'create,update'),
            array('short_name+province_id', 'application.extensions.uniqueMultiColumnValidator'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'province' => array(self::BELONGS_TO, 'GasProvince', 'province_id'),
            'user' => array(self::BELONGS_TO, 'Users', 'user_id_create'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                'id' => 'ID',
                'province_id' => 'Tỉnh/TP',
                'name' => 'Tên Quận/Huyện',
                'name_vi' => 'Tên không dấu',
                'status' => 'Status',
                'user_id_create' => 'Người Tạo',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.id',$this->id);
        $criteria->compare('t.province_id',$this->province_id);
        $criteria->compare('t.name',$this->name,true);
        $criteria->compare('t.short_name',$this->short_name,true);
        $criteria->compare('t.status',$this->status);
        $aWith=array();
        $aWith[] = 'province';  
        $criteria->together = true;
        $criteria->with = $aWith;
        $sort = new CSort();
        $sort->attributes = array(
//                    'name'=>'short_name',
            'province_id'=>array(
                    'asc'=>'province.name',
                    'desc'=>'province.name DESC',
            ),         
            'name'=>array(
                    'asc'=>'t.short_name',
                    'desc'=>'t.short_name DESC',
            ),         

        );    
        $sort->defaultOrder = 't.id DESC';                 
        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=> 50,
                ),
                'sort' => $sort,
        ));
    }

    public static function getArrAll($province_id='')
    {
        $criteria = new CDbCriteria;
        if(!empty($province_id)){
            $criteria->addCondition('t.province_id='.$province_id);
            $criteria->addCondition('t.status='.STATUS_ACTIVE);
        }
        else {
            return array();
        }
        return  CHtml::listData(self::model()->findAll($criteria),'id','name');
    }
    
    /**
     * @Author: ANH DUNG Jan 31, 2016
     * @Todo: fix above function
     */
    public static function getArrAllFix($province_id='')
    {
        $criteria = new CDbCriteria;
        if(!empty($province_id)){
            $criteria->addCondition('t.province_id='.$province_id);
            $criteria->addCondition('t.status='.STATUS_ACTIVE);
        }
        return  CHtml::listData(self::model()->findAll($criteria),'id','name');
    }            

    public function beforeSave() {
        $this->name = trim($this->name);
        $this->short_name = strtolower(MyFunctionCustom::remove_vietnamese_accents($this->name));
        if($this->isNewRecord){
            $this->user_id_create = Yii::app()->user->id;
        }            
        return parent::beforeSave();
    }

    public function beforeValidate() {
        $this->name = trim($this->name);
        $this->short_name = strtolower(MyFunctionCustom::remove_vietnamese_accents($this->name));
        return parent::beforeValidate();
    }

    public function behaviors()
    {
        return array(
            'sluggable' => array(
                    'class' => 'application.extensions.mintao-yii-behavior-sluggable.SluggableBehavior',
                    'columns' => array('short_name'),
                    'unique' => true,
                    'update' => true,
            ), 
        );
    }
    
    /**
     * @Author: ANH DUNG Jun 24, 2016
     * @Todo: get data and group by province
     */
    public static function apiGetData() {
        $aWard = GasWard::apiGetData();
        $criteria = new CDbCriteria();
        $criteria->compare("t.status", STATUS_ACTIVE);
        $models = self::model()->findAll($criteria);
        $aRes = array();
        foreach($models as $item){
            $aTemp = array(
                "id" => $item->id,
                "name" => $item->name,
                "data" => isset($aWard[$item->id]) ? $aWard[$item->id]:array(),
            );
            $aRes[$item->province_id][] = $aTemp;
        }
        return $aRes;
    }
        

        
}