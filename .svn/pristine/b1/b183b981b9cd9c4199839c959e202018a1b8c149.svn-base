<?php

/**
 * This is the model class for table "{{_check_agent}}".
 *
 * The followings are the available columns in table '{{_check_agent}}':
 * @property string $id
 * @property string $agent_id
 * @property string $uid_login
 * @property string $created_date
 * @property string $created_date_bigint
 * @property string $note
 * @property string $json
 */
class CheckAgent extends BaseSpj
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CheckAgent the static model class
     */
    public $autocomplete_name, $autocomplete_name_1, $file_image, $ktkvkt, $sl_ktkvkt;
//    public $date_from,$date_to;
    public $store_card_type;
    public $province_id;
    public $area_id;
    
    // CmsFormatter::$STORE_CARD_ALL_TYPE
    // DuongNV Aug0119 điều chỉnh khi kiểm kho đại lý
    public static $aStoreCardModify = [
        STORE_CARD_TYPE_6  => 'Nhập khác',
        STORE_CARD_TYPE_7  => 'Xuất khác',
        STORE_CARD_TYPE_24 => 'Xuất kho treo CN trừ lương',
    ];
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_check_agent}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            array('agent_id', 'required', 'on' => 'create, update'),
            ['id, agent_id, uid_login, created_date, created_date_bigint, note, json,date_from,date_to', 'safe'],
            ['file_image, store_card_type', 'safe'],
            ['province_id', 'safe'],
            ['area_id', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations(){
        return array(
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rFile' => array(self::HAS_MANY, 'GasFile', 'belong_id',
                'on'=>'rFile.type='.  GasFile::TYPE_CHECK_AGENT,
                'order'=>'rFile.id ASC',
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'agent_id' => 'Đại Lý',
            'uid_login' => 'Người tạo',
            'created_date' => 'Ngày tạo',
            'created_date_bigint' => 'Created Date Bigint',
            'note' => 'Ghi chú',
            'json' => 'Chi tiết',
            'date_from' => 'Từ ngày',
            'date_to' => 'Đến ngày',
            'province_id' => 'Tỉnh',
            'area_id' => 'Khu vực',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $date_from = $date_to = '';
	$criteria->compare('t.agent_id',$this->agent_id);
	$criteria->compare('t.uid_login',$this->uid_login);
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
            DateHelper::searchGreater($date_from, 'created_date_bigint', $criteria);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
            DateHelper::searchSmaller($date_to, 'created_date_bigint', $criteria, true);
        }
        //search multi provinces
        if (!empty($this->province_id)) {
            $aAgentId = $this->getAgentOfProvince($this->province_id);
            if (!empty($aAgentId)) {
                $aInAgentId = implode(',', $aAgentId);
            } else {
                $aInAgentId = -1;
            }
            $criteria->addCondition("t.agent_id IN ($aInAgentId)");
        }
        $criteria->order = 't.id DESC';
        $_SESSION['data-excel-check-agent-index'] = new CActiveDataProvider($this, array(
            'pagination' => false,
            'criteria' => $criteria,
        ));
        $_SESSION['model-excel-check-agent-index'] = $this;
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    public function getAgent() {
        $aAgent = $this->rAgent;
        return isset($aAgent->first_name) ? $aAgent->first_name : "";
    }
    
    public function getUidLogin() {
        $mUser = $this->rUidLogin;
        if($mUser){
            return $mUser->first_name;
        }
        return '';
    }
    
    public function addAttributes($post){
        $this->note = empty($post['note']) ? '' : $post['note'];
        $this->json = json_encode(array(
            'STORE' => isset($post['STORE']) ? $post['STORE'] : '',
            'CASHBOOK' => isset($post['CASHBOOK']) ? $post['CASHBOOK'] : '',
        ));
    } 
    protected function beforeSave() {
        if($this->isNewRecord){
            $this->uid_login = Yii::app()->user->id;
        }
        return parent::beforeSave();
    }    
    public function canUpdate(){
        $created_date   = date("Y-m-d", strtotime($this->created_date));
        $dayAllow       = date('Y-m-d');// chỉ cho phép nhân viên CallCenter cập nhật trong ngày
        $dayAllow       = MyFormat::modifyDays($dayAllow, 1, '-');// cho phép update trong ngày
        $dateCond       = MyFormat::compareTwoDate($created_date,$dayAllow);
        $exceptCond     = (MyFormat::getCurrentRoleId() == ROLE_ADMIN);
        $userCond       = (MyFormat::getCurrentUid() == $this->uid_login);
        return ($dateCond && $userCond) || $exceptCond;
    }
    
    public function existData(){
        $data_current   = date('Y-m-d');
        $criteria       = new CDbCriteria();
        $criteria->compare('t.agent_id', $this->agent_id);
        $cUid           = MyFormat::getCurrentUid();
        $criteria->compare('t.uid_login', $cUid);
        $criteria->addCondition("DATE(t.created_date) ='$data_current'");
        $model          = CheckAgent::model()->find($criteria);
        if ($model == null) {
            return null;
        } else {
            return $model;
        }
    }
    public function updateModel(){
        $data_current = date('Y-m-d');
        $criteria = new CDbCriteria();
        $criteria->compare('t.agent_id',$this->agent_id);
        $criteria->addCondition("DATE(t.created_date) ='$data_current'");
        $model = CheckAgent::model()->find($criteria);
        if ($model != null) {
            $model->json = $this->json;
            $model->note = $this->note;
            $model->update();
        }
        return $model;
    }
    
    public function getJsonToData(){
        return json_decode($this->json,true);
    }
    
    /** @Author: LocNV Jul 13, 2019
     *  @Todo:
     *  @Param:
     **/
    public function getImages() {
        $res = '';
        $cmsFormater = new CmsFormatter();
        $res .= $cmsFormater->formatBreakTaskDailyFile($this, array('width'=>30, 'height'=>30));
        return $res;
    }
    
    /** @Author: LocNV Jul  , 2019
     *  @Todo:
     *  @Param:
     **/
    public function reportKiemKho() {
        $res = $res1            = [];
        $criteria               = new CDbCriteria();

        //search by area and multi province
        $aProvinceByZone = [];

        if(!empty($this->area_id)){
            $aProvinceByZone = GasOrders::getTargetZoneProvinceId()[$this->area_id];
        }
        $aProvinceByZone;

        if (!empty($aProvinceByZone)) {
            $aAgentId = $this->getAgentOfProvince($aProvinceByZone);
            if (!empty($aAgentId)) {
                $aInAgentId = implode(',', $aAgentId);
            } else {
                $aInAgentId = -1;
            }
            $criteria->addCondition("t.agent_id IN ($aInAgentId)");
        }
        
        if (!empty($this->agent_id)) {
            $criteria->compare('t.agent_id',$this->agent_id);    
        }
        
        if (!empty($this->uid_login)) {
            $criteria->compare('t.uid_login',$this->uid_login);  
        }
	
        if(empty($this->date_from) && empty($this->date_to) ){
            return [];
        }
//        
        $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        DateHelper::searchGreater($date_from, 'created_date_bigint', $criteria);
        DateHelper::searchSmaller($date_to, 'created_date_bigint', $criteria, true);
        
        $criteria->select   = 't.uid_login, t.id, count(distinct(t.agent_id)) as ktkvkt, count(t.agent_id) as sl_ktkvkt';
        $criteria->group    = 't.uid_login';
        $criteria->order    = 't.id DESC';
        
        $listdata           = [];
        $mSetupTeam         = new SetupTeam();
        $mSetupTeam->date_from      = $date_from;
        $mSetupTeam->date_to        = $date_to;
        $aType              = [SetupTeam::TYPE_KTKV_AGENT];
        $aData              = $mSetupTeam->getByType($aType); //ds cac dai ly ma uktkv quan ly

        $aCheckAgent        = CheckAgent::model()->findAll($criteria);

        foreach ($aCheckAgent as $value) {
            $res1[$value->uid_login] = isset($aData[$value->uid_login]) ? count($aData[$value->uid_login]) : 0;
        }
        
        foreach ($aCheckAgent as $value) {
            $item = $value->uid_login;
//            $res[$item]['ktkv_id']      = $value->uid_login;
            $res[$item]['ktkv_name']    = isset($value->rUidLogin)? $value->rUidLogin->first_name : "";
//            $res[$item]['ktkvql']       = isset($aData[$value->uid_login]) ? count($aData[$value->uid_login]) : $i++;
            $res[$item]['ktkvkt']       = $value->ktkvkt;
            $res[$item]['sl_ktkvkt']    = $value->sl_ktkvkt;
        }
        
        arsort($res1);
        
        $data['aUID_LOGIN'] = $res1;
        $data['aDATA'] = $res;

        //for excel
        $_SESSION['data-excel-check-agent-report'] = $data;
        $_SESSION['model-excel-check-agent-report'] = $this;

        return $data;
    }

    /**
     * @Author: LocNv Sep 12, 2019
     * @Todo: get list agent by array province
     * @param array $aProvince list of provinces
     * @return array
     */
    public function getAgentOfProvince($aProvince) {
        if(!is_array($aProvince)){
            return [];
        }
        $aRes = [];
        $mAppCache = new AppCache();
        $aAgent = $mAppCache->getAgent();
        foreach($aAgent as $item){
            if(in_array($item['province_id'], $aProvince)){
                $aRes[] = $item['id'];
            }
        }
        return $aRes;
    }
    
}