<?php

/**
 * This is the model class for table "{{_gas_profile}}".
 *
 * The followings are the available columns in table '{{_gas_profile}}':
 * @property string $id
 * @property string $code_no
 * @property string $agent_id
 * @property string $uid_login
 * @property string $name
 * @property string $name_vi
 * @property string $date_expired
 * @property string $created_date
 */
class GasProfile extends BaseSpj
{
    public $nextAlertExpired, $daysCheck, $aModelDetail; // QL Hồ Sơ Pháp Lý
    public $mDetail; // dùng để lưu aIdNotIn cho phần update xóa record detail đi
    public $MAX_ID, $date_from, $date_to, $autocomplete_name;
    public $company_id, $province_id;
    public static $max_upload = 35;
    
    const ALERT_TYPE_NEW        = 0;
    const ALERT_TYPE_60         = 1;// 60 ngay het han
    const ALERT_TYPE_30         = 2;
    const ALERT_TYPE_EXPIRED    = 3;
    const ALERT_DONE            = 4;
    const ALERT_TYPE_ONE_YEAR   = 5;// 1 năm hết hạn
    
    // Dec 02, 2014 loại gom nhóm hồ sơ pháp lý
    const P_TYPE_1 = 1;
    const P_TYPE_2 = 2;
    const P_TYPE_3 = 3;
    const P_TYPE_4 = 4;
    const P_TYPE_5 = 5;
    const P_TYPE_6 = 6;
    const P_TYPE_7 = 7;
    const P_TYPE_8 = 8;
    const P_TYPE_9 = 9;
    const P_TYPE_10 = 10;
    const P_TYPE_11 = 11;
    const P_TYPE_12 = 12;
    const P_TYPE_13 = 13;
    const P_TYPE_14 = 14;
    const P_TYPE_15 = 15;
    const P_TYPE_16 = 16;
    
    public static $ARR_P_TYPE = array(
        GasProfile::P_TYPE_1 => 'Giấy Đăng Ký Kinh Doanh',
        GasProfile::P_TYPE_2 => 'Giấy Chứng Nhận Đủ Điều Kiện Kinh Doanh Khí Dầu Mỏ Hóa Lỏng',
        GasProfile::P_TYPE_3 => 'Giấy Chứng Nhận Đủ Điều Kiện Về Phòng Cháy Và Chữa Cháy',
        GasProfile::P_TYPE_4 => 'Giấy Chứng Nhận Thẩm Duyệt Phòng Cháy Chữa Cháy',
        GasProfile::P_TYPE_5 => 'Biên Bản Nghiệm Thu Hoàn Thành Công Trình',
        GasProfile::P_TYPE_6 => 'Công Văn Nghiệm Thu Phòng Cháy Chữa Cháy',
        GasProfile::P_TYPE_7 => 'Giấy An Ninh Trật Tự',
        GasProfile::P_TYPE_8 => 'Giấy Chứng Nhận Đăng Ký Thuế',
        GasProfile::P_TYPE_9 => 'Hợp Đồng Cung Cấp LPG',
        GasProfile::P_TYPE_10 => 'Bảo Hiểm Cháy Nổ',
        GasProfile::P_TYPE_11 => 'Hợp Đồng Ủy Quyền',
        GasProfile::P_TYPE_12 => 'Hợp Đồng Thuê Nhà',
        GasProfile::P_TYPE_13 => 'Bản Vẽ Thẩm Duyệt Phòng Cháy Chữa Cháy',
        GasProfile::P_TYPE_14 => 'Giấy CNĐĐK LÀM THƯƠNG NHÂN PHÂN PHỐI,',
        GasProfile::P_TYPE_15 => 'Giấy CNĐĐK LÀM TỔNG ĐẠI LÝ',
        GasProfile::P_TYPE_16 => 'Hồ sơ tài sản',
    );
    
    public function getArrayAlertType() {
        return [
            GasProfile::ALERT_TYPE_60           => 60,// days
            GasProfile::ALERT_TYPE_30           => 30,
            GasProfile::ALERT_TYPE_EXPIRED      => 0,
            GasProfile::ALERT_TYPE_ONE_YEAR     => 365 // days
        ];
    }
    public function getAlertDays() {
        $aType = $this->getArrayAlertType();
        return isset($aType[$this->alert_expired]) ? $aType[$this->alert_expired] : 0;
    }
    
    // Dec 02, 2014 loại gom nhóm hồ sơ pháp lý
    
    public static $ROLE_VIEW_EXPIRY = array(
        ROLE_ADMIN,ROLE_RECEPTION,ROLE_EMPLOYEE_OF_LEGAL, ROLE_MONITORING, ROLE_AUDIT, ROLE_ACCOUNTING, ROLE_ACCOUNTING_ZONE
    );
    
    public static $ROLE_ACCOUNTING_VIEW = array(
        GasLeave::DUNG_NTT, // Nguyễn Thị Thùy Dung
        GasLeave::PHUONG_PTK, // Phan Thị Kim Phương
    );

    
    // lấy số ngày cho phép đại lý cập nhật QL Hồ Sơ Pháp Lý
    public static function getDayAllowUpdate(){
        return Yii::app()->params['days_update_profile_scan'];
    }
    
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_profile}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('agent_id, name', 'required', 'on'=>'create,update'),
            array('note,order_number, list_materials_id,id, code_no, agent_id, uid_login, name, name_vi, date_expired, created_date', 'safe'),
            array('company_id, province_id, update_by, update_time, alert_expired, status, type,date_from,date_to', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rProfileDetail' => array(self::HAS_MANY, 'GasProfileDetail', 'profile_id',
                'order'=>'rProfileDetail.order_number ASC',
            ),
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'code_no' => 'Mã Số',
            'agent_id' => 'Đại Lý',
            'uid_login' => 'Người Tạo',
            'name' => 'Tên Hồ Sơ',
            'name_vi' => 'Slug 1',
            'date_expired' => 'Ngày Hết Hạn',
            'created_date' => 'Ngày Tạo',
            'date_from' => 'Hết Hạn Từ Ngày',
            'date_to' => 'Hết Hạn Đến Ngày',
            'order_number' => 'Số Thứ Tự',
            'list_materials_id' => 'Thương Hiệu Gas',
            'note' => 'Ghi Chú',
            'type' => 'Loại Giấy',
            'status' => 'Trạng thái',
            'update_by' => 'Người cập nhật',
            'update_time' => 'Ngày cập nhật',
            'company_id' => 'Công ty',
            'province_id' => 'Tỉnh',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $cRole = Yii::app()->user->role_id;
        $cUid = Yii::app()->user->id;
        $criteria=new CDbCriteria;
        $criteria->compare('t.code_no',$this->code_no,true);
        $criteria->compare('t.agent_id',$this->agent_id);
        $criteria->compare('t.name_vi',$this->name, true);
        $criteria->compare('t.status',$this->status);
        $criteria->compare('t.type',$this->type);
        if( $cRole==ROLE_SUB_USER_AGENT ){
            $criteria->compare('t.agent_id', MyFormat::getAgentId() );
        }elseif($cUid == GasLeave::UID_VIEW_ALL_TAY_NGUYEN_GIA_LAI){
        }else{
            if(!empty($this->agent_id)){
                $criteria->compare('t.agent_id', $this->agent_id);
            }
            GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');
        }

        // Dec 26, 2014, phân quyền define('ROLE_ACCOUNTING', 33);// Nhân Viên Kế Toán xem pháp lý
        // 2 người 129311  Nguyễn Thị Thùy Dung, 300 Phan Thị Kim Phương
        // Oct 22, 2015 đóng lại, cho phép NV kt xem hết theo đại lý giới hạn
//        if( $cRole == ROLE_ACCOUNTING ){
//            if(in_array( $cUid, GasProfile::$ROLE_ACCOUNTING_VIEW )){
//                // nothing to do
//            }else{
//                $criteria->compare('t.agent_id', -1);// xử lý không cho load row nào nếu không là user allow
//            }
//        }
        // Dec 26, 2014

        if(!empty($this->created_date)){
            $this->created_date = MyFormat::dateDmyToYmdForAllIndexSearch($this->created_date);
            $criteria->compare('t.created_date',$this->created_date,true);
        }

        $date_from = '';
        $date_to = '';
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        }
        if(!empty($date_from) && empty($date_to))
                $criteria->addCondition("t.date_expired>='$date_from'");
        if(empty($date_from) && !empty($date_to))
                $criteria->addCondition("t.date_expired<='$date_to'");
        if(!empty($date_from) && !empty($date_to))
                $criteria->addBetweenCondition("t.date_expired",$date_from,$date_to);      
        
        //dearch province
        if (!empty($this->province_id)) {
            $aAgentId = $this->getAgentOfProvince($this->province_id);
            if (!empty($aAgentId)) {
            $aInAgentId = implode(',', $aAgentId);
            } else {
                $aInAgentId = -1;
            }
            $criteria->addCondition("t.agent_id IN ($aInAgentId)");
        }
        
        //search company
        if (!empty($this->company_id)) {
            $aAgentId = $this->getAgentOfCompany($this->company_id);
            if (!empty($aAgentId)) {
            $aInAgentId = implode(',', $aAgentId);
            } else {
                $aInAgentId = -1;
            }
            $criteria->addCondition("t.agent_id IN ($aInAgentId)");
        }
        
        $sort = new CSort();

        $sort->attributes = array(
            'code_no'=>'code_no',
            'agent_id'=>'agent_id',
            'date_expired'=>'date_expired',
            'type'=>'type',
            'name'=>array(
                'asc' => 't.name_vi',
                'desc' => 't.name_vi desc',
            ),
            'created_date'=>'created_date',
        );
        $sort->defaultOrder = 't.agent_id ASC, t.order_number ASC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort' => $sort,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    /**
     * @Author: ANH DUNG Dec 01, 2014
     * @Todo: số ngày cảnh báo hết hạn hồ sơ pháp lý dc seting trong setting form $profile_day_alert_expiry
     */
    public function searchExpiry()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.code_no',$this->code_no,true);
        $criteria->compare('t.agent_id',$this->agent_id);
        $criteria->compare('t.uid_login',$this->uid_login);
        $criteria->compare('t.name_vi',$this->name,true);
        $criteria->compare('t.status',$this->status);
        $criteria->compare('t.type',$this->type);
        if(Yii::app()->user->role_id==ROLE_SUB_USER_AGENT){
            $criteria->compare('t.agent_id', MyFormat::getAgentId() );
        }else{
            if(!empty($this->agent_id)){
                $criteria->compare('t.agent_id', $this->agent_id);
            }
            GasAgentCustomer::addInConditionAgent($criteria, 't.agent_id');
        }
        
        if(!empty($this->created_date)){
            $this->created_date = MyFormat::dateDmyToYmdForAllIndexSearch($this->created_date);
            $criteria->compare('t.created_date',$this->created_date,true);
        }
        
        //dearch province
        if (!empty($this->province_id)) {
            $aAgentId = $this->getAgentOfProvince($this->province_id);
            if (!empty($aAgentId)) {
            $aInAgentId = implode(',', $aAgentId);
            } else {
                $aInAgentId = -1;
            }
            $criteria->addCondition("t.agent_id IN ($aInAgentId)");
        }
        
        //search company
        if (!empty($this->company_id)) {
            $aAgentId = $this->getAgentOfCompany($this->company_id);
            if (!empty($aAgentId)) {
            $aInAgentId = implode(',', $aAgentId);
            } else {
                $aInAgentId = -1;
            }
            $criteria->addCondition("t.agent_id IN ($aInAgentId)");
        }

        $date_from = '';
        $date_to = '';
        if(!empty($this->date_from)){
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
        }
        if(!empty($this->date_to)){
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
        }
        if(!empty($date_from) && empty($date_to))
                $criteria->addCondition("t.date_expired>='$date_from'");
        if(empty($date_from) && !empty($date_to))
                $criteria->addCondition("t.date_expired<='$date_to'");
        if(!empty($date_from) && !empty($date_to))
                $criteria->addBetweenCondition("t.date_expired",$date_from,$date_to);
        $days_expiry_search = Yii::app()->params['profile_day_alert_expiry'];
        $criteria->addCondition(" date_expired BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL $days_expiry_search DAY) ");
//            $criteria->addCondition("DATE_ADD(date_expired, INTERVAL YEAR(CURDATE())-YEAR(date_expired) YEAR) 
//            BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL $days_expiry_search DAY) ");
        $criteria->order = 'month(date_expired) ASC, day(date_expired) ASC';

        $sort = new CSort();
        $sort->attributes = array(
            'code_no'=>'code_no',
            'agent_id'=>'agent_id',
            'date_expired'=>'date_expired',
            'type'=>'type',
            'name'=>array(
                'asc' => 'name_vi',
                'desc' => 'name_vi desc',
            ),
            'created_date'=>'created_date',
        );    
        $sort->defaultOrder = 't.agent_id ASC, t.order_number ASC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort' => $sort,
            'pagination'=>array(
//                    'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                'pageSize'=> 20,
            ),
        ));
    }    
    
    protected function beforeSave() {
        $this->name = trim($this->name);
        $this->name_vi = strtolower(MyFunctionCustom::remove_vietnamese_accents($this->name));
        
        if(strpos($this->date_expired, '/')){
            $this->date_expired = MyFormat::dateConverDmyToYmd($this->date_expired);
            MyFormat::isValidDate($this->date_expired);
        }
        if($this->isNewRecord){
            $this->uid_login = Yii::app()->user->id;
            $this->code_no = MyFunctionCustom::getNextId('GasProfile', 'P'.date('y'), LENGTH_TICKET,'code_no');
        }
        
        if(isset($_POST['GasProfile']['list_materials_id']) && is_array($_POST['GasProfile']['list_materials_id']))
            $this->list_materials_id = implode(',', $this->list_materials_id);
        else
            $this->list_materials_id = '';
        return parent::beforeSave();
    }
    
    protected function beforeDelete() {
        GasProfileDetail::delete_by_profile_id($this->id);
        return parent::beforeDelete();
    } 
    
    
    /**
     * @Author: ANH DUNG Dec 03, 2014
     * @Todo: get type of profile
     * @Param: $model
     */
    public static function GetTypeProfile($model) {
        $res = '';
        if(isset( GasProfile::$ARR_P_TYPE[$model->type])){
            $res = GasProfile::$ARR_P_TYPE[$model->type];
        }
        return $res;
    }
    
    public static function GetArrayTypeProfile() {
        return GasProfile::$ARR_P_TYPE;
    }
    
    /**
     * @Author: ANH DUNG Sep 19, 2016
     */
    public function getAgent() {
        $mUser = $this->rAgent;
        if($mUser){
            return $mUser->getFullName();
        }
        return '';
    }
    
    public function getName() {
        return $this->name;
    }
    public function getNote() {
        return $this->note;
    }
    
    /**
     * @Author: ANH DUNG Sep 25, 2016
     */
    public function getUrlView() {
        return Yii::app()->createAbsoluteUrl("admin/gasProfile/view", array('id'=>$this->id));
    }
    
    /**
     * @Author: ANH DUNG Dec 03, 2014
     * @Todo: report agent profile, báo cáo đại lý đã có những loại hs pháp lý nào
     */
    public function report() {
        $aRes = array();
        $criteria = new CDbCriteria();
        if(!empty($this->agent_id)){
            $criteria->addCondition("t.agent_id = $this->agent_id");
        }
        $criteria->select = "t.agent_id, t.type";
        $models = self::model()->findAll($criteria);
        foreach( $models as $item ){
            $aRes[$item->agent_id][$item->type] = 1;
        }
        return $aRes;
    }
    public function reportGetModelAgent() {
        $criteria = new CDbCriteria();
        if(!empty($this->company_id)):
            $criteria->addCondition("t.parent_id = $this->company_id");
        endif;
        if(is_array($this->province_id)):
            $sParamsIn = implode(',', $this->province_id);
            $criteria->addCondition("t.province_id IN ($sParamsIn)");
        endif;
        if(!empty($this->agent_id)){
            $criteria->addCondition("t.id = $this->agent_id");
        }
        $criteria->addCondition('t.role_id='.ROLE_AGENT);
        $criteria->order = 't.id';
        $models = Users::model()->findAll($criteria);
        $aRes = [];
        foreach($models as $item){
            $aRes[$item->id] = $item;
        }
        return $aRes;
    }
    
    /**
     * @Author: ANH DUNG Jul 02, 2016
     * @Todo: list_materials_id
     */
    public static function getListMaterialForMap() {
        $criteria = new CDbCriteria();
        $criteria->compare("t.type", GasProfile::P_TYPE_2);
        $models = self::model()->findAll($criteria);
        $aRes = array();
        $aMaterialId = array();

        foreach($models as $model){
            if($model->list_materials_id != ''){
                $tmp = explode(",", $model->list_materials_id);
                $aMaterialId = array_merge($aMaterialId, $tmp);
                $aRes[$model->agent_id]['id'] = $tmp;
            }
        }
        
        $aModelMaterial = GasMaterials::getArrayModel($aMaterialId);
        foreach($aRes as $agent_id=>$arr){
            $tempName = array();
            foreach($aRes[$agent_id]['id'] as $key => $material_id){
                $tempName[$material_id] = $aModelMaterial[$material_id]->getName();
            }
            $aRes[$agent_id]['ArrayBrand'] = $tempName;
            $aRes[$agent_id]['BrandName'] = implode(", ", $tempName);
        }
        return $aRes;
    }
    
    /**
     * @Author: ANH DUNG Dec 13, 2016
     */
    public function canUpdate() {
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, GasProfile::getDayAllowUpdate(), '-');
        $session=Yii::app()->session;
        
        $cRole          = MyFormat::getCurrentRoleId();
        $cUid           = MyFormat::getCurrentUid();
        $aRoleAllow     = [ROLE_ACCOUNTING, ROLE_SALE];
        $aRoleCheck1    = [ROLE_ACCOUNTING_ZONE];
        $aUidAllow1     = [GasConst::UID_THUAN_NH];
        if($cUid == GasConst::UID_HIEP_TV){
            return true;// Feb0218 mở cho anh Hiệp cập nhật hết
        }
        
        if($cUid == GasLeave::UID_VIEW_ALL_TAY_NGUYEN_GIA_LAI){
            return in_array($this->agent_id, $session['LIST_AGENT_OF_USER']);
        }
//        elseif(in_array($cRole, $aRoleAllow) && $cUid != GasConst::UID_VINH_NH){
//            if(($this->uid_login != $cUid)){
//                return false;
//            }
//            return MyFormat::compareTwoDate($this->created_date, $dayAllow);
//        }// Close Mar2619
        if(in_array($cRole, $aRoleCheck1)){
            return in_array($cUid, $aUidAllow1);// Mar2619 cho phép Thuận KTKV sửa hết giấy tờ các đại lý quản lý
        }

        if(isset($session['LIST_AGENT_OF_USER']) && count($session['LIST_AGENT_OF_USER'])){
            return in_array($this->agent_id, $session['LIST_AGENT_OF_USER']);
        }
        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }
    
    public function getStatus() {
        $aStatus = ActiveRecord::getUserStatus();
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : '';
    }
    
    /** Dec3017 HOANG NAM
     * @CODE: NAM001
     *  1. Lấy danh sách profile hết hạn
     *  2. group by theo vung`
     *  3. email
     */
    public function doAlertExpired(){
        $aProvinceOfZone = [];
        $aZoneProvinceId =  GasOrders::getZoneNewProvinceId();
        foreach ($aZoneProvinceId as $zone_id => $aProvince) {
            foreach($aProvince as $province_id){
                $aProvinceOfZone[$province_id] = $zone_id;
            }
        }
        $aProfileExpired = $this->getListExpiredData();
        $mAppCache = new AppCache();
        $aAgent = $mAppCache->getAgent();
        $aRes = [];
        foreach ($aProfileExpired as $mProfile) {
            // DuongNV Sep2319 những đại lý inactive sẽ không cảnh báo
            $mAgent = isset($aAgent[$mProfile->agent_id]) ? $aAgent[$mProfile->agent_id] : '';
            if(empty($mAgent) || $mAgent->status == STATUS_INACTIVE){
                continue;
            }
            
            if(isset($aAgent[$mProfile->agent_id])){
                $province_id = $aAgent[$mProfile->agent_id]['province_id'];
                $zone_id = isset($aProvinceOfZone[$province_id]) ? $aProvinceOfZone[$province_id] : $province_id;
                $aRes[$zone_id][$province_id][] = clone $mProfile;
//                $mProfile->nextAlertExpired = $this->nextAlertExpired;
//                $mProfile->updateNextAlert();
            }
        }
        if(count($aRes)){
            $mHtml      =new HtmlFormat();
            $content    = $mHtml->renderEmailProfileExpired($aRes);
            $this->alertExpiredDoSendMail($content);
        }
    }
    
    /** @Author: ANH DUNG Jan 03, 2018
     * @Todo: send mail alert cho pháp lý các khu vực hồ sơ hết hạn
     */
    public function alertExpiredDoSendMail($content) {
        $needMore['title']      = "Cảnh báo pháp lý hết hạn {$this->daysCheck} ngày";
//        $needMore['list_mail']  = ['dungnt@spj.vn'];
        $needMore['list_mail'] = ['dungnt@spj.vn', 'phaply@spj.vn', 'thuymdx@spj.vn'];
        SendEmail::bugToDev($content, $needMore);
    }
    
    /** @Author: ANH DUNG Jan 03, 2018
     *  @Todo: get model và set type alert cho lần tiếp theo của model
     * không cần update biến alert_expired vì ở đây cứ đúng ngày hết hạn thì find ra để cảnh báo.
     * Sang ngày sau thì record đó sẽ không nằm trong danh sách nữa
     **/
    public function getListExpiredData(){
        $this->daysCheck        = $this->getAlertDays();
        $this->nextAlertExpired = GasProfile::ALERT_TYPE_NEW;
        $alertTypeQuery         = GasProfile::ALERT_TYPE_NEW;
        switch ($this->alert_expired) {
            case GasProfile::ALERT_TYPE_ONE_YEAR:
                $this->nextAlertExpired = GasProfile::ALERT_TYPE_60;
                break;
            case GasProfile::ALERT_TYPE_60:
                $this->nextAlertExpired = GasProfile::ALERT_TYPE_30;
                break;
            case GasProfile::ALERT_TYPE_30:
                $this->nextAlertExpired = GasProfile::ALERT_TYPE_EXPIRED;
                $alertTypeQuery         = GasProfile::ALERT_TYPE_60;
                break;
            case GasProfile::ALERT_TYPE_EXPIRED:
                $this->nextAlertExpired = GasProfile::ALERT_DONE;
                $alertTypeQuery         = GasProfile::ALERT_TYPE_30;
                break;
        }
        $criteria       =new CDbCriteria;
        $dateExpired    = MyFormat::modifyDays(date('Y-m-d'), $this->daysCheck);
        $criteria->compare('t.date_expired', $dateExpired);
        $criteria->order = 't.date_expired ASC';
        if($this->alert_expired == GasProfile::ALERT_TYPE_ONE_YEAR){
            $criteria->compare('t.type', GasProfile::P_TYPE_12);// hop dong thue nha
        }
//        $criteria->compare('t.alert_expired', $alertTypeQuery);
        return GasProfile::model()->findAll($criteria);
    }
    
    /** @Author: ANH DUNG Jan 03, 2018
     *  @Todo: set next alert of record, để xác nhận đã gửi cảnh báo của record này
     **/
    public function updateNextAlert() {
        $this->alert_expired = $this->nextAlertExpired;
        $this->update(['alert_expired']);
    }
    
    /** @Author: ANH DUNG Jan 03, 2018
     *  @Todo: tìm những hồ sơ hết hạn trong 60 và 30 ngày email cảnh bảo cho bộ phận pháp lý
     * 1. cảnh báo 60 ngày hết hạn
     * 2. cảnh báo 30 ngày hết hạn
     * 3. cảnh báo 0 ngày hết hạn
     * Miền tây: Vinh
        Tây Nguyên: Tuyết
        Bình Định: Dũng thủ kho
        Vũng Tàu: đạt 
        Đông Nam bộ Tp HCM + đn + bd + tây Ninh: Chị Hạnh 

     **/
    public function alertExpired() {
        $mProfile   =new GasProfile();
        $mProfile->alert_expired = GasProfile::ALERT_TYPE_ONE_YEAR;
        $mProfile->doAlertExpired();
        
        $mProfile   =new GasProfile();
        $mProfile->alert_expired = GasProfile::ALERT_TYPE_60;
        $mProfile->doAlertExpired();
        
        $mProfile   =new GasProfile();
        $mProfile->alert_expired = GasProfile::ALERT_TYPE_30;
        $mProfile->doAlertExpired();
        
        $mProfile   =new GasProfile();
        $mProfile->alert_expired = GasProfile::ALERT_TYPE_EXPIRED;
        $mProfile->doAlertExpired();
    }
    
    public function getUpdateTime() {
        if(empty($this->update_time)){
            return ;
        }
        return MyFormat::dateConverYmdToDmy($this->update_time, 'd/m/Y H:i');
    }
    
    /** @Author: ANH DUNG Jul 28, 2017
     * @Todo: get list agent by array province
     */
    public function getAgentOfProvince($aProvince) {
        if(!is_array($aProvince)){
            return [];
        }
        $aRes = [];
        $mAppCache = new AppCache();
        $aAgent = $mAppCache->getAgent();
        foreach($aAgent as $item){
            if(in_array($item['province_id'], $aProvince)){
                $aRes[] = $item['id'];
            }
        }
        return $aRes;
    }
    
    /** @Author: LocNV Aug 07, 2019
     *  @Todo: get list agent by company id (parent_id)
     *  @Param:
     **/
    public function getAgentOfCompany($companyId) {
        $aRes = [];
        $mAppCache = new AppCache();
        $aAgent = $mAppCache->getAgent();
        foreach($aAgent as $item){
            if ($item['parent_id'] == $companyId) {
                $aRes[] = $item['id'];
            }
        }

        return $aRes;
    }

    /** @Author: DuongNV
     *  @Todo:
     *  @Param:
     **/
    public function getUrlCreateHomeContract($agent_id) {
        $homeContractModel = HomeContract::model()->findByAttributes(array('agent_id' => $agent_id));
        $str = '';
        if ($homeContractModel) {
            $url = Yii::app()->createAbsoluteUrl('/admin/homeContract/update/id/' . $homeContractModel->id, array('ajax' => 1)); //'http://localhost/lab1/admin/homeContract/update/id/'.$homeContractModel->id;
            $str .= '<label>&nbsp;</label>';
            $str .= '<div class="add_new_item float_l" style="padding-left: 0;">';
            $str .= '<a class="IframeHomeContract" style="padding-left: 0;"';
            $str .= 'href="' . $url . '">';
            $str .= 'Cập Nhật Hợp Đồng Thuê Nhà</a>';
            $str .='</div>';
        } else {
            $url = Yii::app()->createAbsoluteUrl('/admin/homeContract/create' , array('ajax' => 1,'agent_id'=>$agent_id)); //'http://localhost/lab1/admin/homeContract/update/id/'.$homeContractModel->id;
            $str .= '<label>&nbsp;</label>';
            $str .= '<div class="add_new_item float_l" style="padding-left: 0;">';
            $str .= '<a class="IframeHomeContract" style="padding-left: 0;"';
            $str .= 'href="' . $url . '">';
            $str .= 'Tạo Mới Hợp Đồng Thuê Nhà</a>';
            $str .='</div>';
        }
        return $str;
    }
    
}