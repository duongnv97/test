<?php

/**
 * This is the model class for table "{{_spin_numbers}}".
 *
 * The followings are the available columns in table '{{_spin_numbers}}':
 * @property string $id
 * @property string $user_id
 * @property string $number
 * @property integer $status
 * @property string $created_date
 * @property string $expired_date
 */
class SpinNumbers extends BaseSpj
{
    
    public $autocomplete_name, $autocomplete_name2, $rank, $role_id;
    public $limit_assign, $user_assign, $phone, $is_test = true;
    public $mAppUserLogin; // api
    
    const NUMBER_MIN        = 10000000;
    const NUMBER_MAX        = 99999999;
    
    const LIMIT_AMOUNT_NUMBER   = 1000; // giới hạn số mỗi lần tạo (web)
    const LIMIT_SHORTAGE_NUMBER = 2000; // Nếu kho số chưa gán <= 2000 mới tạo thêm
    const LIMIT_NUMBER_AVAILABLE= 5000; // Số lượng số tối đa (cron)
    const LIMIT_TYPE_CODE       = 1;
    const LIMIT_TYPE_APP        = 2;
    const LIMIT_ONE_ORDER_ADMIN = 1; // 1 vé / 1 user
    
    const STATUS_NEW        = 1; // mới tạo, chưa gán user
    const STATUS_NORMAL     = 2;
    const STATUS_WINNING    = 3;
    
    const SOURCE_TYPE_APP   = 1;  // dat hang qua app gas24h
    const SOURCE_TYPE_CODE  = 2;  // Nhap ma khuyen mai, cai nay ko su dung nua
    const SOURCE_TYPE_REF   = 3;  // Gthieu tai app, chia sẻ app
    const SOURCE_TYPE_FIRST = 4;  // tai app lan dau
    const SOURCE_TYPE_TEST  = 5;  // test
    const SOURCE_TYPE_CALL  = 6;  // Dat hang qua tong dai
    
    public static function getArrayStatus() {
        return [
            self::STATUS_NEW        => 'Mới',
            self::STATUS_NORMAL     => 'Chưa quay thưởng',
            self::STATUS_WINNING    => 'Đã trúng thưởng',
        ];
    }
    
    public static function getArraySource() {
        return [
            self::SOURCE_TYPE_APP   => 'Mua hàng',
            self::SOURCE_TYPE_CODE  => 'Cài app qua mã giới thiệu',
            self::SOURCE_TYPE_REF   => 'Chia sẻ cài app',
            self::SOURCE_TYPE_FIRST => 'Tải app lần đầu',
            self::SOURCE_TYPE_TEST  => 'Test',
        ];
    }
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return SpinNumbers the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_spin_numbers}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('status', 'numerical', 'integerOnly'=>true),
            array('user_id', 'length', 'max'=>11),
            array('number', 'length', 'max'=>8),
            array('created_date, expired_date, date_from, date_to, role_id, limit_assign, user_assign', 'safe'),
            array('id, user_id, number, status, created_date, expired_date, source, source_detail', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rUsers'=> array(self::BELONGS_TO, 'Users', 'user_id'),
            'rEmployee'=> array(self::BELONGS_TO, 'Users', 'user_assign'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                'id'            => 'ID',
                'user_id'       => 'Khách hàng',
                'number'        => 'Số quay thưởng',
                'source'        => 'Nguồn',
                'source_detail' => 'Mã đơn hàng/Mã giới thiệu',
                'status'        => 'Trạng thái',
                'created_date'  => 'Ngày tạo',
                'expired_date'  => 'Ngày hết hạn',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        if($this->status != self::STATUS_NEW){
            $criteria->addCondition('t.user_id !=0');
        }
        $criteria->compare('t.user_id',$this->user_id);
        $criteria->compare('t.number',$this->number, true);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('t.source', $this->source);
        if(!empty($this->source_detail)){
            $criteria->addSearchCondition('t.source_detail', $this->source_detail);
        }
        if (!empty($this->date_from)) {
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_from);
            $criteria->addCondition("DATE(expired_date) >= '$date_from'");
        }
        if (!empty($this->date_to)) {
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->date_to);
            $criteria->addCondition("DATE(expired_date) <= '$date_to'");
        }
        $criteria->order = 't.id DESC';
        $criteria->order = 't.created_date DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> 50,
            ),
        ));
    }
    
    /** @Author: NhanDT Aug 15, 2019
     *  @Todo: create random number; 
     *  @Param: $max = 100 nếu hết đột ngột or $max = 5000 - tổng của row trống 
     **/
    public function autoGenNumber($max) {
        if($max > SpinNumbers::LIMIT_NUMBER_AVAILABLE){
            return false;
        }
        $from = time();
        for($i = 0; $i < $max; $i++){
            $number = $this->generateNumber();
            $this->saveNewNumber($number);
        }
        
        $to     = time();
        $second = $to-$from;
        $info   = $max.' new Code done in: '.($second).'  Second  <=> '.($second/60).' Minutes';
        return true;
//        Logger::WriteLog($info);
    }
    
    /** @Author: NhanDT AUG 15, 2019
     * @return: unique number in table SpinNumbers
     * @to_do: sinh tự động số có 8 chữ số
     */    
    public function generateNumber(){
        $number         = rand(self::NUMBER_MIN, self::NUMBER_MAX);
        $countNumber    = SpinNumbers::model()->count("`number`=$number");
        if($countNumber){ // check trùng nhau
            $number     = $this->generateNumber();
            return $number;
        }else{
            return $number;
        }
    }
    
    /** @Author: DuongNV Aug3019
     * @return: unique number in table SpinNumbers
     * @to_do: sinh tự động số có 8 chữ số
     * @des: Lấy hết những số có sẵn, random number not in array
     */    
    public function generateNumberV2(){
        // Lấy hết những number hiện tại
        $aModelNumber   = SpinNumbers::model()->findAll();
        $aNumber        = [];
        foreach ($aModelNumber as $value) {
            $aNumber[$value->number] = $value->number;
        }
        // lấy random, ktra nếu có rồi thì next, đến khi chưa có thì return
        $number         = rand(self::NUMBER_MIN, self::NUMBER_MAX);
        while( !empty($aNumber[$number]) ){
            $number     = rand(self::NUMBER_MIN, self::NUMBER_MAX);
        }
        return $number;
    }
    
    /** @Author: NhanDT Aug 14, 2019
     *  @Todo: save one record to table SpinNumbers  
     *  @Param: param
     **/
    public function saveNewNumber($nunmber) {
        $model = new SpinNumbers();
        $model->number = $nunmber;
        $model->status = self::STATUS_NEW;
        $model->save();
        $model = null;
    }

    /** @Author: NhanDT Aug 15, 2019
     *  @Todo: create rows with condition rows<2000 at 11:00pm; or 100 rows when hết đột xuất
     *  @Param: param
     **/
    public function createRowsCondition() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.user_id <= 0');
        $count = SpinNumbers::model()->count($criteria);
        $model = new SpinNumbers();
        if($count <= 1){ // hết đột ngột, đảm bảo mỗi hóa đơn => return 2 số quay thưởng
            $model->autoGenNumber(100);
        }
    }
    
    /** @Author: DuongNV Sep1419
     *  @Todo: cron auto gen number if < limit
     *  @Param:
     **/
    public function cronAutoGenNumber() {
        $criteria   = new CDbCriteria();
        $criteria->addCondition('t.user_id <= 0');
        $count      = SpinNumbers::model()->count($criteria);
        $model      = new SpinNumbers();
        if($count <= SpinNumbers::LIMIT_SHORTAGE_NUMBER){ // neu thieu so thi cron tao them so
            $cQty = SpinNumbers::LIMIT_NUMBER_AVAILABLE - $count;
            $model->autoGenNumber($cQty);
            // send log
            $this->sendCronLog('cronAutoGenNumber', $cQty);
        }
    }
    
    /** @Author: DuongNV Sep1419
     *  @Todo: cron delete expired number
     *  @Param:
     **/
    public function cronAutoDeleteNumber($today) {
        $criteria   = new CDbCriteria();
        $criteria->addCondition("t.expired_date <= '$today'");
        $numberRows = SpinNumbers::model()->deleteAll($criteria);
        // send log
        $this->sendCronLog('cronAutoDeleteNumber', $numberRows);
    }
    
    public function sendCronLog($functionName, $numRows = '', $isSendMail = true, $isWriteLog = true){
        // Send log
        $needMore['title']      = $functionName;
        $needMore['list_mail']  = ['duongnv@spj.vn'];
        $strLog                 = '******* '.$functionName.' done at '.date('d-m-Y H:i:s').' | '.$numRows.' rows affected!';
        if($isSendMail){
            SendEmail::bugToDev($strLog, $needMore);
        }
        if($isWriteLog){
            Logger::WriteLog($strLog);
        }
    }

    /** @Author: NhanDT Aug 14, 2019
     *  @Todo: save after user has one order; provice 2 nunmber/order
     *  @Param: param
     **/
    public function saveUserNumber($user_id, $source, $source_detail='') {
//        if(empty($user_id) || !$this->canContinue($user_id)) {
//            return;
//        }
        if(empty($user_id)) { // mo thu test 1 thang
            return;
        }
        $criteria   = new CDbCriteria();
        $criteria->addCondition('user_id = 0 OR user_id is null');
        $source     = empty($source) ? self::SOURCE_TYPE_TEST : $source;
        switch ($source) {
            case self::SOURCE_TYPE_APP:
                $criteria->limit = self::LIMIT_TYPE_APP;
                break;
            // chia se app, nhap ma, cai app lan dau
            case self::SOURCE_TYPE_CALL:
            case self::SOURCE_TYPE_CODE:
            case self::SOURCE_TYPE_REF:
            case self::SOURCE_TYPE_FIRST:
                $criteria->limit = self::LIMIT_TYPE_CODE;
                break;
            case self::SOURCE_TYPE_TEST:
                $criteria->limit = self::LIMIT_ONE_ORDER_ADMIN;
                break;

            default:
                break;
        }
        $aUpdate = array(
            'user_id'       => $user_id,
            'source'        => $source, 
            'source_detail' => $source_detail,
            'status'        => self::STATUS_NORMAL,
            'created_date'  => date('Y-m-d H:i'), 
            'expired_date'  => date('Y-m-d', strtotime('+2 months'))
        ); 
        SpinNumbers::model()->updateAll($aUpdate, $criteria);
        $this->notifyUser($user_id);
    }
    
    /** @Author: DuongNV Sep1919
     *  @Todo: send notify when have new number
     **/
    public function notifyUser($user_id, $type = GasScheduleNotify::GAS24H_NEW_LUCKY_NUMBER){
        if(!$this->canContinue($user_id, $type)){
//            Logger::WriteLog('cant notify: '.$this->phone.' - '.$user_id);
            return false;
        }
        $this->sendCronLog('notify number ok ???', 0, 1, 0);
        return;
//        Logger::WriteLog('notify ok');
        $sendNow    = true; // test, live run se chuyen sang false
        $strTitle   = 'Chúc mừng bạn đã nhận được mã số may mắn từ Gas24h!';
        if($type == GasScheduleNotify::GAS24H_NEW_LUCKY_NUMBER){
            $this->runInsertNotify(GasScheduleNotify::GAS24H_REMAIN, $user_id, $strTitle, $sendNow); // test
//            $this->runInsertNotify(GasScheduleNotify::GAS24H_NEW_LUCKY_NUMBER, $user_id, $strTitle, $sendNow);
        }else{
            $strTitle = 'Chúc mừng bạn đã TRÚNG GIẢI từ chương trình quay số may mắn Gas24h!';
            $this->runInsertNotify(GasScheduleNotify::GAS24H_REMAIN, $user_id, $strTitle, $sendNow); // test
//            $this->runInsertNotify(GasScheduleNotify::GAS24H_NUMBER_WINNING, $user_id, $strTitle, $sendNow);
        }
    }
    
    /** @Author: DuongNV Sep1919
     *  @Todo: insert notify
     **/
    public function runInsertNotify($type, $uid, $title, $sendNow = true) {
        $json_var = [];
        if( !empty($uid) ){
            $mScheduleNotify = GasScheduleNotify::InsertRecord($uid, $type, '', '', $title, $json_var); 
            if($sendNow && !empty($mScheduleNotify)){
                $mScheduleNotify->sendImmediateForSomeUser();
            }
        }
    }
    
    /** @Author: DuongNV Sep2119
     *  @Todo: neu dang test thi chi ap dung cho 1 vai user
     **/
    public function canContinue($user_id, $type = GasScheduleNotify::GAS24H_NEW_LUCKY_NUMBER) {
        if($this->is_test){
            if($type == GasScheduleNotify::GAS24H_NUMBER_WINNING){ // neu notify winning thi lay sdt dang nhap cua user (username)
                $mUser = Users::model()->findByPk($user_id);
                $this->phone = empty($mUser) ? '' : $mUser->username;
            }
            if(empty($this->phone) || !in_array($this->phone, $this->getArrayPhoneDemo()) ){
                return false;
            }
        }
        return true;
    }

    /** @Author: NhanDT Aug 14, 2019
     *  @Todo: + return arr các số quay thưởng tại thời điểm hiện tại; 
     *         + conditions: số còn hạn và các số trước ngày quay 1 ngày
     *  @Param: param
     **/
    public function arrNumberSpin(){
        $arr        = array();
        $criteria   = new CDbCriteria();
        $criteria->compare('status', self::STATUS_NORMAL);
        if($this->is_test){
            $tblUsersName   = Users::tableName();
            $criteria->join = "LEFT JOIN $tblUsersName u on t.user_id = u.id";
            $aPhoneTest     = $this->getArrayPhoneDemo();
            $sParamsIn      = implode(',', $aPhoneTest);
            if(!empty($sParamsIn)){
                $criteria->addCondition("u.username IN ($sParamsIn)");
            }
        } else {
            $criteria->addCondition("created_date <'".date('Y-m-d')."'");
            $criteria->addCondition("expired_date >='".date('Y-m-d')."'"); 
        }
        $mNumberAc  = self::model()->findAll($criteria);    
        foreach ($mNumberAc as $value) {
            $arr[]  = $value->number; 
        }
        return $arr;
    }
    
    
    /** @Author: NhanDT Aug 15, 2019
     *  @Todo: trả về số lượng khách chưa quay thưởng tại thời điểm hiện tại (thời điểm quay số trúng thưởng)
     *  @Param:
     **/
    public function getQtyUsersUnexpired() {
        $criteria = new CDbCriteria();
        $criteria->compare('t.status',self::STATUS_NORMAL);
        $cRole  = MyFormat::getCurrentRoleId();
        if($cRole != ROLE_ADMIN){
           $criteria->addCondition("expired_date >='".date('Y-m-d')."'");
           $criteria->addCondition("created_date <'".date('Y-m-d')."'");
        }
        return self::model()->count($criteria);
    }

    /** @Author: NhanDT Aug 15, 2019
     *  @Todo: todo
     *  @Param: param
     **/
    public function getUser() {
        
        $str ='';
        if(!empty($this->rUsers)){
            $str.='<b>Tên KH: </b>'.$this->rUsers->getFullName().'<br>';
            if (!empty($this->rUsers->phone)){           
                
                $str.='<b>SĐT: </b>'.$this->rUsers->getPhone().'<br>';
            }
            if (!empty($this->rUsers->address)){
                $str.='<b>Địa chỉ: </b>'.$this->rUsers->address.'<br>';
            }
        } else return '';
        return $str;
    }

    public function getSource() {
        $aSource = self::getArraySource();
        return empty($aSource[$this->source]) ? '' : $aSource[$this->source];
    }

    public function getSourceDetail() {
        return empty($this->source_detail)?'':$this->source_detail;
    }
    
    public function getNumber() {
        return empty($this->number)?'':$this->number;
    }
    
    public function getCreatedDateSpin() {
        return empty($this->created_date)?'':MyFormat::dateConverYmdToDmy($this->created_date);
    }

    public function getExpiredDate() {
        return empty($this->expired_date)?'':MyFormat::dateConverYmdToDmy($this->expired_date);
    }
    
    public function getStatus() {
        $aStatus = SpinNumbers::getArrayStatus();
        return empty($aStatus[$this->status]) ? '': $aStatus[$this->status];
    }
    
    /** @Author: NhanDT Aug 16,2019
     *  @Todo: 
     *  @Param: param
     **/
    public function checkExpired() {
        $criteria = new CDbCriteria();
        $criteria->compare('expired_date', date('Y-m-d', strtotime('-1 day')));
        $aUpdate = array('status' => 0);
        SpinNumbers::model()->updateAll($aUpdate, $criteria);
    }
    
    /** @Author: NhanDT Aug 16, 2019
     *  @Todo: change status from active (1) to winning (2)
     *  @Param: param
     **/
    public function updateUserWinning($number) {
        $criteria   = new CDbCriteria();
        $criteria->compare('number', $number);
//        $aUpdate    = array('status' => self::STATUS_WINNING);
//        SpinNumbers::model()->updateAll($aUpdate, $criteria);
        $model      = SpinNumbers::model()->find($criteria);
        
        if(!empty($model)){
            $model->status = self::STATUS_WINNING;
            $model->update();
            $this->notifyUser($model->user_id, GasScheduleNotify::GAS24H_NUMBER_WINNING);
        }
    }
        
    /** @Author: NhanDT Aug 20, 2019
     *  @Todo: random lucky number
     *  @Param: param
     **/
    public function randomLuckyNumber() {
        $aLucky = $this->arrNumberSpin();
        if (!is_array($aLucky) || empty($aLucky))
            return;
        return $aLucky[array_rand($aLucky)];
    }
    
    /** @Author: NhanDT Aug 21,2019
     *  @Todo: find user ứng với Number
     *  @Param: param
     **/
    public function findUser($number) {
        $criteria = new CDbCriteria();
        $criteria->compare('number', $number);
        $mNum = self::model()->find($criteria);
        return empty($mNum->user_id)?'':$mNum->user_id;
    }

    /** @Author: DuongNV Aug2119
     *  @Todo: get list number of user API
     **/
    public function apiGetListNumber(&$result, $q) {
        $result['record'] = [];
        $mUser = $this->mAppUserLogin;
        if( !empty($mUser) ){
            $mSpinNumber            = new SpinNumbers();
            $mSpinNumber->user_id   = $mUser->id;
            $result['record']       = $mSpinNumber->getListUserNumber();
        }
    }

    /** @Author: DuongNV Aug2119
     *  @Todo: get list number of user API
     *  @Params: $this->user_id
     **/
    public function getListUserNumber() {
        $result             = [];
        if( empty($this->user_id) ) return $result;
        $cDate              = date('Y-m-d');
        $criteria           = new CDbCriteria;
        $tblDetail          = SpinCampaignDetails::tableName();
        $criteria->select   = "t.*, dt.rank";
        $criteria->join     = " LEFT JOIN $tblDetail dt ON dt.user_id = t.user_id AND dt.number = t.number";
        $criteria->compare('t.user_id', $this->user_id);
        $criteria->addCondition("t.expired_date >= '$cDate'");
        $criteria->order    = 't.expired_date DESC';
        
        $models             = SpinNumbers::model()->findAll($criteria);
        $aRank              = SpinCampaignDetails::getArrayRank();
        foreach ($models as $item) {
            $aNewData = [
                'status_id'     => $item->status,
                'status_text'   => $item->getStatus(),
                'rank_id'       => $item->rank,
                'rank_text'     => empty($aRank[$item->rank]) ? '' : $aRank[$item->rank],
                'number'        => $item->number,
                'expired_date'  => date('d/m', strtotime($item->expired_date)),
            ];
            $result[] = $aNewData;
        }
        return $result;
    }
    
    /** @Author: DuongNV Aug2119
     *  @Todo: get number history of user API
     **/
    public function apiGetNumberHistory(&$result, $q) {
        $result['record']           = [];
        $mUser                      = $this->mAppUserLogin;
        if( !empty($mUser) ){
            if( empty($mUser->id) ) return $result;
            $criteria               = new CDbCriteria;
            $criteria->compare('t.user_id', $mUser->id);
            $criteria->order = 't.created_date DESC';
            
            $mCampaign              = new SpinCampaigns();
            $dataProvider           = $mCampaign->apiGetDataPrivider($criteria, $this, $q->page);
            $models                 = $dataProvider->data;
            $CPagination            = $dataProvider->pagination;
            $result['total_record'] = $CPagination->itemCount;
            $result['total_page']   = $CPagination->pageCount;
            foreach ($models as $item) {
                $aNewData = [
                    'source_id'     => $item->source,
                    'source_text'   => $item->getSource(),
                    'source_detail' => $item->getSourceDetail(),
                    'number'        => $item->number,
                    'created_date'  => MyFormat::dateConverYmdToDmy($item->created_date),
                ];
                $result['record'][] = $aNewData;
            }
        }
    }
    /** @Author: NamLA Sep2119
     *  @Todo: chuyển trạng thái của số cũ thành Normal == 1
     **/
    public function changeStatusNumber($number){
        $criteria = new CDbCriteria();
        $criteria->compare('number', $number);
        if(!empty($number)){
//            $aUpdate = ['status'=> SpinNumbers::STATUS_NORMAL];
//            SpinNumbers::model()->updateAll($aUpdate,$criteria);
            $model = SpinNumbers::model()->find($criteria);
            $model->status = SpinNumbers::STATUS_NORMAL;
            $model->update();
//            $this->notifyUser($model->user_id);
        }
    }
    
    /** @Author: DuongNV Sep1219
     *  @Todo: check show button for admin
     **/
    public function canAutogen() {
        $cRole = MyFormat::getCurrentRoleId();
        $aAllow = [ROLE_ADMIN];
        return in_array($cRole, $aAllow);
    }
    
    /** @Author: DuongNV Sep1219
     *  @Todo: get new number (chưa gán cho ai)
     *  @Param:
     **/
    public function getArrayNewNumber($limit = '') {
        $criteria = new CDbCriteria();
        $criteria->compare('t.status', SpinNumbers::STATUS_NEW);
        $criteria->addCondition('t.user_id = 0 OR t.user_id is null');
        if(!empty($limit)){
            $criteria->limit    = $limit;
        }
        return SpinNumbers::model()->findAll($criteria);
    }
    
    /** @Author: DuongNV Sep1219
     *  @Todo: assign number to multi user
     *  @Param:user_id array uid
     **/
    public function assignMultiUser($limitUser = '') {
        if(empty($this->user_id) && !is_array($this->user_id)){
            return false;
        }
        $aUid       = array_values($this->user_id); //reset key
        $i          = 0;
        $countUid   = count($aUid);
        $limitUser  = empty($limitUser) ? $countUid : $limitUser;
        $aNewNumber = $this->getArrayNewNumber($limitUser);
        if(empty($aNewNumber)){
            $numGen = $limitUser > self::LIMIT_AMOUNT_NUMBER ? self::LIMIT_AMOUNT_NUMBER: $limitUser;
            $this->autoGenNumber($numGen);
        }
        $aNewNumber = $this->getArrayNewNumber($limitUser);
        foreach ($aNewNumber as $mSpinNumbers) {
            if( !empty($aUid[$i]) ){
                $mSpinNumbers->user_id          = $aUid[$i++];
                $mSpinNumbers->source           = SpinNumbers::SOURCE_TYPE_TEST;
                $mSpinNumbers->source_detail    = '#4dm1ncut3';
                $mSpinNumbers->status           = SpinNumbers::STATUS_NORMAL;
                $mSpinNumbers->expired_date     = MyFormat::modifyDays(date('Y-m-d'), 60);
                $mSpinNumbers->update();
            }
            if( $i == $countUid ){ // check đề phòng, cũng ko cần lắm
                break;
            }
        }
        return $i;
    }
    
    /** @Author: DuongNV Sep1919
     *  @Todo: list user phone demo quay so
     **/
    public function getArrayPhoneDemo() {
        return [
            '0935714733', // Duong IT
            '0981404392', // Hao IT
            '0988538360', // c Phuong TD
            '0384223842', // a Quan
            '0789908771', // c Ven
            '0904122722', // a Dot
            '0389945321', // a Trung
            '0352676870', // telesale
            '0979905539',
            '0365420905',
            '0393825693',
            '0393825693',
            '0338061606',
            '0911404481',
            '0788040533',
            '0327323388',
            '0985163698',
            '0789908771',
            '0977779742',
            '0369514524',
            '0395145244',
        ];
    }
    
    /** @Author: DuongNV Sep1919
     *  @Todo: list user id demo quay so
     **/
    public function getArrayUserIDDemo() {
        return [];
        $aUidDemo = [
            1740110, // Duong IT server test
            1298082, // KH0999888779 server test
            2175451, // KH Duong IT server live
            2298298, // KH Hao IT server live
            1278434, // chi Phuong
            1730585, // Nam IT
            1384960, // a Trung IT
            1283711, // a Dot
        ];
        $aPhoneDemo             = $this->getArrayPhoneDemo();
        
        $criteria               = new CDbCriteria();
        $criteria->compare("t.role_id", ROLE_CUSTOMER);
        $sParamsIn              = implode(',', $aPhoneDemo);
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.username IN ($sParamsIn)");
        }
        $aUsers                 = Users::model()->findAll($criteria);
        foreach ($aUsers as $mUsers) {
            $aUidDemo[]  = $mUsers->id;
        }
        return array_unique($aUidDemo);
    }
    
    
}
