<?php

/**
 * This is the model class for table "{{_hr_plan}}".
 *
 * The followings are the available columns in table '{{_hr_plan}}':
 * @property string id
 * @property string code_no
 * @property string name
 * @property DateTime plan_from
 * @property DateTime plan_to
 * @property integer department_id
 * @property integer company_id
 * @property integer qty
 * @property string description
 * @property integer status
 * @property integer approved_1
 * @property DateTime approved_1_date
 * @property integer created_by
 * @property DateTime created_date
 *
 * @property Users rApproved
 * @property Users rCreatedBy
 * @property ArrayObject(HrInterViewPlan) rCandidates List candidates
 */
class HrPlan extends CActiveRecord
{
    public $COUNT_DATE = 7; // Số ngày cách nhau tối thiểu giữa ngày bắt đầu và ngày kết thúc
    
    const STA_NEW = 1;
    const STA_APPROVED = 2;
    const STA_NEED_UPDATE = 3;
    const STA_REJECT = 4;
    public $history_note;
    public $autocomplete_name;
    public $autocomplete_name_v1;
    public static $LIST_STATUS_TEXT = array(
        HrPlan::STA_NEW => "Chờ Duyệt",
        HrPlan::STA_APPROVED => "Quản lý đã duyệt",
        HrPlan::STA_NEED_UPDATE => "Yêu cầu cập nhật",
        HrPlan::STA_REJECT => "Từ chối",
    );

    public static $LIST_STATUS_APPROVE_TEXT = array(
        HrPlan::STA_APPROVED => "Quản lý đã duyệt",
        HrPlan::STA_NEED_UPDATE => "Yêu cầu cập nhật",
        HrPlan::STA_REJECT => "Từ chối",
    );

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasLeave the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_hr_plan}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('name, plan_from, plan_to, department_id, company_id, qty, approved_1', 'required', 'on' => 'create'),
            array('description', 'length', 'max' => 500),
            array('status', 'required', 'on' => 'update_status'),
            array('department_id, company_id, approved_1', 'NonZeroId', 'on' => 'create'),
            array('qty', 'numerical', 'min' => 1),
            array('qty', 'numerical', 'max' => 1000),
            array('name, plan_from, plan_to, department_id, company_id, qty, approved_1, status, note', 'safe'),
            array('status', 'required', 'on' => 'updateStatus'),
            array('plan_from', 'CheckDateFrom', 'on' => 'create,update'),
//            array('plan_to', 'CheckDateTo', 'on' => 'create,update','date_from'=>'plan_from','count_date'=>'COUNT_DATE'),
        );
    }

    public function NonZeroId($attribute, $params)
    {
        if ($this->$attribute <= 0) {
            $this->addError($attribute, $attribute + 'can not blank!');
            $this->$attribute = '';
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rApproved' => array(self::BELONGS_TO, 'Users', 'approved_1'),
            'rCreatedBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'rCandidates' => array(self::HAS_MANY, 'HrInterviewPlan', 'plan_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'code_no' => 'Mã kế hoạch',
            'name' => 'Tên',
            'company_id' => 'Công ty',
            'department_id' => 'Phòng ban',
            'qty' => 'Số lượng',
            'plan_from' => 'Từ ngày',
            'plan_to' => 'Đến ngày',
            'description' => 'Mô tả',
            'status' => 'Trạng thái',
            'approved_1' => 'Người duyệt',
            'approved_1_date' => 'Ngày duyệt',
            'created_by' => 'Người tạo',
            'created_date' => 'Ngày tạo',
            'note'    =>  'Ghi chú'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id, true);
        $criteria->compare('t.code_no', $this->code_no);
        $criteria->compare('t.company_id', $this->company_id);
        $criteria->compare('t.department_id', $this->department_id);
        $criteria->compare('t.name', $this->name);
        $criteria->compare('t.qty', $this->qty);
        $criteria->compare('t.description', $this->description);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('t.approved_1', $this->approved_1);
        $criteria->compare('t.approved_1_date', $this->approved_1_date);
        $criteria->compare('t.created_by', $this->created_by, true);
        $criteria->compare('t.created_date', $this->created_date, true);

        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();

        if ($cRole != ROLE_ADMIN) {
            $criteria->addCondition("t.created_by = $cUid OR t.approved_1 = $cUid ");
        }

        if (!empty($this->plan_from)) {
            $date_from = MyFormat::dateDmyToYmdForAllIndexSearch($this->plan_from);
            $criteria->addCondition("t.plan_from >= '$date_from'");
        }
        if (!empty($this->plan_to)) {
            $date_to = MyFormat::dateDmyToYmdForAllIndexSearch($this->plan_to);
            $criteria->addCondition("t.plan_to <= '$date_to'");
        }

        $sort = new CSort();
        $sort->attributes = array(
            'code_no' => 'code_no',
            'plan_from' => 'plan_from',
            'plan_to' => 'plan_to',
            'status' => 'status',
        );
        $sort->defaultOrder = 't.id DESC';

//        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ),
            'sort' => $sort,
        ));
    }

    public function getOwnerName()
    {
        return $this->rCreatedBy->first_name;
    }

    /**
     * Return department name
     */
    public function getDepartment()
    {
        $mAppCache = new AppCache();
        $listCompany = $mAppCache->getListdataUserByRole(ROLE_DEPARTMENT);
        if (isset($listCompany[$this->department_id])) {
            return $listCompany[$this->department_id];
        } else {
            return '';
        }
    }

    /**
     * Return company name
     */
    public function getCompany()
    {
        $mAppCache = new AppCache();
        $listCompany = $mAppCache->getListdataUserByRole(ROLE_COMPANY);
        if (isset($listCompany[$this->company_id])) {
            return $listCompany[$this->company_id];
        } else {
            return '';
        }
    }

    /** Get list of companies
     * @return array list company
     */
    public static function getCompanies()
    {
        $mAppCache = new AppCache();
        return $mAppCache->getListdataUserByRole(ROLE_COMPANY);
    }

    /** Get approve person for confirm plan
     * @return string
     */
    public function getApproveName()
    {
        return $this->rApproved ? $this->rApproved->first_name : '';
    }

    /** Get list department
     * @return array array of department
     */
    public static function getDepartments()
    {
        $aModelDepartments = Users::model()->findAllByAttributes(array('role_id'=>ROLE_DEPARTMENT));
        $aDep = [];
        foreach ($aModelDepartments as $item) {
            $aDep[$item->id] = $item->first_name;
        }

        $mAppCache = new AppCache();
        return $mAppCache->getListdataUserByRole(ROLE_DEPARTMENT);
    }

    /** Get approve person for confirm plan
     * @return array
     */
    public static function getApprovePersons()
    {
        $aModelUser = GasOneManyBig::getArrayModelUser(GasOneManyBig::TYPE_APPROVE_HR_PLAN, GasOneManyBig::TYPE_APPROVE_HR_PLAN);
        $aRes = array();
        foreach ($aModelUser as $item) {
            $aRes[$item->id] = $item->getNameWithRole();
        }
        return $aRes;
    }

    /** Check user can update or not
     * @return bool
     */
    public function canUpdate()
    {
        return Yii::app()->user->id == $this->created_by &&
        in_array($this->status, array(
            HrPlan::STA_NEW,
            HrPlan::STA_NEED_UPDATE
        ));
    }

    public function canUpdateStatus()
    {
        return Yii::app()->user->id == $this->approved_1 &&
        in_array($this->status, array(
            HrPlan::STA_NEW,
        ));
    }

    /** Return current status
     * @return string
     */
    public function getStatusName()
    {
        return isset(HrPlan::$LIST_STATUS_TEXT[$this->status]) ? HrPlan::$LIST_STATUS_TEXT[$this->status] : 'Error';
    }

    /** Return current status as html
     * @return string html code
     */
    public function getStatusNameHtml()
    {
        $result = isset(HrPlan::$LIST_STATUS_TEXT[$this->status]) ? HrPlan::$LIST_STATUS_TEXT[$this->status] : 'Error';
        if ($this->status == HrPlan::STA_APPROVED) {
            if (count($this->rCandidates) < $this->qty) {
                $result .= '<br/><a href="' . Yii::app()->createUrl("admin/hrInterviewPlan/create", array("plan_id" => $this->id)) . '">Thêm ứng viên</a>';
            }
            $result .= '<br/><a href="' . Yii::app()->createUrl("admin/hrInterviewPlan/index", array("plan_id" => $this->id)) . '">Danh sách ứng viên</a>';
        }
        return $result;
    }

    /** Get current candidate vs total
     * @return string html code
     */
    public function getQuantityStatus()
    {
        if ($this->status == HrPlan::STA_APPROVED) {
            return count($this->rCandidates) . '/' . $this->qty;
        } else {
            return $this->qty;
        }
    }

    //-----------------------------------------------------
    // Parent override methods
    //-----------------------------------------------------
    /** Check validate
     * @return bool
     */
    protected function beforeValidate()
    {
        return parent::beforeValidate();
    }

    /**
     * Override before save method
     * @return Parent result
     */
    public function beforeSave()
    {
        if (strpos($this->plan_from, '/')) {
            $this->plan_from = MyFormat::dateConverDmyToYmd($this->plan_from);
            MyFormat::isValidDate($this->plan_from);
        }
        if (strpos($this->plan_to, '/')) {
            $this->plan_to = MyFormat::dateConverDmyToYmd($this->plan_to);
            MyFormat::isValidDate($this->plan_to);
        }

        $userId = isset(Yii::app()->user) ? Yii::app()->user->id : '';
        if ($this->isNewRecord) {
            $this->status = HrPlan::STA_NEW;
            $this->code_no = $this->buildCodeNo();
            $this->created_by = Yii::app()->user->id;
        }
        return parent::beforeSave();
    }

    //-----------------------------------------------------
    // Ultilities
    //-----------------------------------------------------

    public function generateViewLink()
    {
        if ($this->status == HrPlan::STA_APPROVED) {
            return Yii::app()->createUrl("hrPlan/view", array("id" => $this->id));
        } else {
            Yii::app()->createUrl("hrInterviewPlan/create", array("plan_id" => $this->id));
        }
    }

    /**
     * Generate code name for HR plan
     */
    public function buildCodeNo()
    {
        $prefix_code = 'HRP' . date('y');
        return $prefix_code . ActiveRecord::randString(6, GasConst::CODE_CHAR);
    }
    /** @Author: HOANG NAM day/month/2018
     *  @Todo: Get list HrPlan by Array status
     *  @Param: 
     **/
    public static function getListHrPlan($mHrInterviewPlan,$aStatus=array(), $available = true)
    {
        $aHrPlan=[];
        $result = [];
        if(empty($aStatus)) {
            $aHrPlan = HrPlan::model()->findAll();
        } else {
            $criteria=new CDbCriteria();
            HrPlan::limitFieldName($criteria, implode(',', $aStatus), 'status');
            $aHrPlan = HrPlan::model()->findAll($criteria);
        }
        /** @var HrPlan $value */
        foreach ($aHrPlan as $mHrPlan) {
            if( $available && $mHrPlan->qty <= count($mHrPlan->rCandidates) && $mHrInterviewPlan->plan_id != $mHrPlan->id) {
                continue;
            }
            $result[$mHrPlan->id] = $mHrPlan->code_no . ' - ' . $mHrPlan->name;
        }
        return $result;
    }
    /** @Author: HOANG NAM 14/03/2018
     *  @Todo: limit field name in string
     *  @Param: 
     **/
    public static function limitFieldName(&$criteria, $sParamsIn, $fieldName) {
        if(!empty($sParamsIn)){
            $criteria->addCondition("t.$fieldName IN ($sParamsIn)");
        }
    }
    /** @Author: HOANG NAM 19/03/2018
     *  @Todo: Check date when create and update hr plan
     *  @Param: 
     **/
    public function CheckDateFrom($attribute, $params){
        $today = date("d/m/Y");
        if($this->$attribute < $today) {
            $this->addError($attribute, 'Ngày bắt đầu phải lớn hơn ngày hiện tại!');
        }
    }
    public function CheckDateTo($attribute, $params){
        $fieldDateFrom = $params['date_from'];
        $fieldCount = $params['count_date'];
        $dateFrom = isset($this->$fieldDateFrom) ? $this->$fieldDateFrom : date("d/m/Y");
        $countDate = isset($this->$fieldCount) ? $this->$fieldCount : 0;
        $dateTo = $this->$attribute;
        if($dateTo - $dateFrom < $countDate) {
            $this->addError($attribute, "Ngày kết thúc phải lớn hơn ngày bắt đầu $countDate ngày!");
        }
    }
    /** @Author: HOANG NAM 19/03/2018
     *  @Todo: handle format data update
     *  @Param: 
     **/
    public function handleUpdate(){
        $this->plan_from = MyFormat::dateConverYmdToDmy($this->plan_from, 'd/m/Y');
        $this->plan_to = MyFormat::dateConverYmdToDmy($this->plan_to, 'd/m/Y');
    }
    /** @Author: HOANG NAM 19/03/2018
     *  @Todo: handle before update
     *  @Param: 
     **/
    public function handleViewStatus(){
        $this->history_note = $this->note;
        $this->note        = "";
    }
    public function handleBeforeUpdateStatus(){
        if(!empty($_POST['HrPlan']['note'])) {
            $mUser = Users::model()->findByPk(MyFormat::getCurrentUid());
            $nameUser = !empty($mUser) ? $mUser->first_name :'';
            $this->note = '['.date('Ymd'). '-' . $nameUser  . ']' . $this->note;
        }
        $this->history_note = isset($_POST['HrPlan']['history_note']) ? $_POST['HrPlan']['history_note'] : '';
        $this->note = $this->history_note.'\n'.$this->note;
    }
    /** @Author: HOANG NAM 19/03/2018
     *  @Todo: get view history
     *  @Param: 
     **/
    public function getHistoryNote(){
        return !empty($this->history_note) ? $this->history_note : str_replace('\n','<br>',$this->note);
    }
}