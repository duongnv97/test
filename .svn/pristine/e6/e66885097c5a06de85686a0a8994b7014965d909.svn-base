<?php

/**
 * This is the model class for table "{{_truck_plan}}".
 *
 * The followings are the available columns in table '{{_truck_plan}}':
 * @property string $auto_done
 * @property string $code_no
 * @property integer $type
 * @property integer $status 
 * @property string $plan_date
 * @property string $plan_date_bigint
 * @property string $supplier_id
 * @property string $supplier_warehouse_id
 * @property integer $qty_plan
 * @property integer $qty_real
 * @property integer $supplier_price
 * @property integer $transport_price
 * @property string $driver_id
 * @property string $car_id
 * @property integer $gas_remain
 * @property string $created_by
 * @property string $created_date
 * @property string $created_date_bigint
 * @property string $driver_note
 * @property string $price_usd_ton
 * @property string $exchange_rate
 */
class TruckPlan extends BaseSpj
{
    CONST TYPE_BUY_INTERNAL             = 1; // Loại Mua kho nội bộ
    CONST TYPE_SELL_CUSTOMER            = 2; // BAN CHO KH
    CONST TYPE_SHIPPING                 = 3; // Vận chuyển thuê
    CONST TYPE_GOODS_EXCHANGE_          = 4; // Đổi hàng
    CONST TYPE_GOODS_BORROW             = 5; // Mượn hàng
    CONST TYPE_GOODS_RETURN             = 6; // Trả hàng
    
    CONST TYPE_ORDER_IMPORT             = 1; // nhập hàng từ NCC
    CONST TYPE_ORDER_EXPORT             = 2; // xuất hàng cho KH, kho nội bộ
    
    CONST STT_NEW                       = 1;
    CONST STT_CONFIRM                   = 2;
    CONST STT_DONE                      = 3;
    CONST STT_CANCEL                    = 4;
    CONST STT_COMPLETE_ORDER_SUPPLIER   = 5;
    
    
    CONST AUTO_DONE_NORMAL      = 1;// đơn thường, tài xế hoàn thành
    CONST AUTO_DONE_BY_USER     = 2; // đơn điều phối tự hoàn thành, ko điều xuống tài xế
    
    public static $aType = [
        self::TYPE_BUY_INTERNAL     => 'Mua về kho nội bộ',
        self::TYPE_SELL_CUSTOMER    => 'Bán cho KH',
        self::TYPE_SHIPPING         => 'Vận chuyển thuê',
        self::TYPE_GOODS_EXCHANGE_  => 'Đổi hàng',
        self::TYPE_GOODS_BORROW     => 'Mượn hàng',
        self::TYPE_GOODS_RETURN     => 'Trả hàng',
    ];
    
    public static $aStatus = [
        TruckPlan::STT_NEW                       => 'Mới',
        TruckPlan::STT_CONFIRM                   => 'Đã xác nhận',
        TruckPlan::STT_COMPLETE_ORDER_SUPPLIER   => 'Đã nhận hàng',
        TruckPlan::STT_DONE                      => 'Hoàn thành',
        TruckPlan::STT_CANCEL                    => 'Hủy',
    ];
    
    public static $aStatusDetail = [
        TruckPlan::STT_NEW          => 'Mới',
        TruckPlan::STT_CONFIRM      => 'Đã xác nhận',
        TruckPlan::STT_DONE         => 'Hoàn thành',
        TruckPlan::STT_CANCEL       => 'Hủy',
    ];
    
    public $customer_id, $autocomplete_name, $autocomplete_name2, $autocomplete_name3, $autocomplete_name4, $MAX_ID;
    public $aIdDetailNotDelete = [], $plan_date_from, $plan_date_to;
    
    public function getAppStatusNew() {
        return [
            TruckPlan::STT_NEW,
            TruckPlan::STT_CONFIRM,
        ];
    }
    
    public function getArrayAutoDone() {
        return [
            TruckPlan::AUTO_DONE_NORMAL     => 'Đơn thường',
            TruckPlan::AUTO_DONE_BY_USER    => 'Điếu phối tự hoàn thành',
        ];
    }
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return TruckPlan the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_truck_plan}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            array('type, plan_date, supplier_id, supplier_warehouse_id, qty_plan', 'required', 'on' => 'create, update'),
            ['price_usd_ton, exchange_rate, customer_id, plan_date_from, plan_date_to, auto_done, transport_id, transport_price_gas_remain, sj_shipping_price, transport_price_gas_remain, created_by_note, id, code_no, type, status, plan_date, plan_date_bigint, supplier_id, supplier_warehouse_id, qty_plan, qty_real, supplier_price, transport_price, driver_id, car_id, gas_remain, created_by, created_date, created_date_bigint, driver_note', 'safe'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'rDriver' => array(self::BELONGS_TO, 'Users', 'driver_id'),
            'rCar' => array(self::BELONGS_TO, 'Users', 'car_id'),
            'rDetail' => array(self::HAS_MANY, 'TruckPlanDetail', 'truck_plan_id'),
            'rSupplier' => array(self::BELONGS_TO, 'Users', 'supplier_id'),
            'rWarehouse' => array(self::BELONGS_TO, 'Users', 'supplier_warehouse_id'),
            'rCreatedBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rDetailImport' => array(self::HAS_ONE, 'TruckPlanDetail', 'truck_plan_id',
                'on'=>'rDetailImport.type_order='. TruckPlan::TYPE_ORDER_IMPORT,
            ),
            'rDetailExport' => array(self::HAS_MANY, 'TruckPlanDetail', 'truck_plan_id',
                'on'=>'rDetailExport.type_order='. TruckPlan::TYPE_ORDER_EXPORT,
                'order'=>'rDetailExport.id DESC',
            ),
            
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code_no' => 'Mã code',
            'type' => 'Loại',
            'status' => 'Trạng thái',
            'plan_date' => 'Ngày tạo lệnh',
            'plan_date_bigint' => 'Plan Date Bigint',
            'supplier_id' => 'Nhà cung cấp',
            'supplier_warehouse_id' => 'Kho NCC',
            'qty_plan' => 'Số lượng dự kiến',
            'qty_real' => 'Số lượng thực tế',
            'supplier_price' => 'Giá NCC / kg',
            'transport_price' => 'Giá thuê vận chuyển / kg',
            'driver_id' => 'Tài xế',
            'car_id' => 'Xe',
            'gas_remain' => 'Gas dư',
            'created_by' => 'Nguời tạo',
            'created_date' => 'Ngày tạo',
            'driver_note' => 'Tài xế ghi chú',
            'created_by_note' => 'Ghi chú khi lấy hàng',
            'transport_id' => 'Đơn vị vận chuyển',
            'transport_price_gas_remain' => 'Giá Gas chênh lệch',
            'sj_shipping_price' => 'Giá vận chuyển thuê / kg',
            'sj_shipping_price_gas_remain' => 'Giá Gas chênh lệch',
            'customer_id' => 'Khách hàng',
            'price_usd_ton' => 'Giá mua USD/tấn',
            'exchange_rate' => 'Tỷ giá mua',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        if (!empty($this->code_no)) {
            $criteria->compare('t.code_no',$this->code_no);
        }
        if (!empty($this->type)) {
            $criteria->compare('t.type',$this->type);
        }
        if (!empty($this->status)) {
            $criteria->compare('t.status',$this->status);
        }
        if (!empty($this->supplier_id)) {
            $criteria->compare('t.supplier_id',$this->supplier_id);
        }
        if (!empty($this->supplier_warehouse_id)) {
            $criteria->compare('t.supplier_warehouse_id',$this->supplier_warehouse_id);
        }
        if( !empty($this->plan_date_from) ){
            DateHelper::searchGreater($this->plan_date_from, 'plan_date_bigint', $criteria);
        }
        if( !empty($this->plan_date_to) ){
            DateHelper::searchSmaller($this->plan_date_to, 'plan_date_bigint', $criteria);
        }
        if( !empty($this->customer_id) ){
            $criteria->compare('rDetailExport.customer_id', $this->customer_id);
            $criteria->with = ['rDetailExport'];
            $criteria->together = true;
        }
        
        if (!empty($this->driver_id)) {
            $criteria->compare('t.driver_id',$this->driver_id);
        }
        if (!empty($this->car_id)) {
            $criteria->compare('t.car_id',$this->car_id);
        }
        $criteria->order = 't.id DESC';

        //LocNV Sep 25, 2019 session data export excel
        $_SESSION['data-excel-truck-plan'] = new CActiveDataProvider($this, array(
            'pagination' => false,
            'criteria' => $criteria,
        ));
        $_SESSION['data-model-truck-plan'] = $this;

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    public function getStatusAppNumber() {
        /* 1: New
         * 2: confirm
         * 3: complete Import
         * 4: cancel 
         * 5: done all
         * 
         */
        return $this->status;
    }
    public function getCodeNo() {
        return $this->code_no;
    }
    
    public function getType(){
        $aType = TruckPlan::$aType;
        return isset($aType[$this->type]) ? $aType[$this->type] : '';
    }
    
    public function getStatus(){
        $aStatus = TruckPlan::$aStatus;
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : '';
    }
    
    public function getPlanDate(){
        return MyFormat::dateConverYmdToDmy($this->plan_date);
    }
    
    public function getSupplier(){
        return isset($this->rSupplier) ? $this->rSupplier->getFullName() : '';
    }
    
    public function getSupplierWarehouse(){
        return isset($this->rWarehouse) ? $this->rWarehouse->getFullName() : '';
    }
    
    public function getDriver($field_name = 'first_name'){
        return isset($this->rDriver) ? $this->rDriver->{$field_name} : '';
    }
    
    public function getCar(){
        return isset($this->rCar) ? $this->rCar->getFullName() : '';
    }
    
    public function getCreatedByNote(){
        return $this->created_by_note;
    }
    
    public function getCreatedBy(){
        return isset($this->rCreatedBy) ? $this->rCreatedBy->getFullName() : '';
    }
    public function getQtyPlan($format = false){
        if($format){
            return ActiveRecord::formatCurrency($this->qty_plan);
        }
        return $this->qty_plan;
    }
    public function getQtyReal($format = false){
        if($format){
            return ActiveRecord::formatCurrency($this->qty_real);
        }
        return $this->qty_real;
    }
    public function getGasRemain($format = false){
        if($format){
            return ActiveRecord::formatCurrency($this->gas_remain);
        }
        return $this->gas_remain;
    }
    public function getGasRemainAmount($format = false){
        if($format){
            return ActiveRecord::formatCurrency($this->amount_gas_remain);
        }
        return $this->amount_gas_remain;
    }
    
    /** @Author: DungNT Jul 05, 2019
     *  @Todo: show auto done + status
     * nếu là đơn tự hoàn thành thì không show trạng thái nữa, khi tạo là hoàn thành luôn
     **/
    public function showAutoDone() {
        $ok = false;
        if($this->isNewRecord || (!$this->isNewRecord && $this->auto_done == TruckPlan::AUTO_DONE_NORMAL) ){
            $ok = true;
        }
        return $ok;
    }
    public function getAutoDone() {
        $aData = $this->getArrayAutoDone();
        return isset($aData[$this->auto_done]) ? $aData[$this->auto_done] : '';
    }
    
    protected function beforeSave() {
        return parent::beforeSave();
    }
    
    protected function beforeDelete() {
        $this->deleteDetail();
        return parent::beforeDelete();
    }
    
    /** @Author: DuongNV May 15,19
     *  @Todo: get array supplier
     **/
    public function getArraySupplier() {
        $criteria   = new CDbCriteria;
        $criteria->compare('t.role_id', ROLE_CUSTOMER);
        $criteria->compare('t.is_maintain', UsersExtend::STORE_CARD_SUPPLIER);
        $models     = Users::model()->findAll($criteria);
        return CHtml::listData($models, 'id', 'first_name');
    }
    
    /** @Author: DuongNV May 15,19
     *  @Todo: get array supplier warehouse
     **/
    public function getArraySupplierWarehouse() {
        $criteria   = new CDbCriteria;
        $criteria->compare('t.role_id', ROLE_CUSTOMER);
        $criteria->compare('t.is_maintain', UsersExtend::STORE_CARD_SUPPLIER_WAREHOUSE);
//        $criteria->compare('t.parent_id', $this->supplier_id);// DungNT Close Jun0919 -- load all Warehouse
        $models     = Users::model()->findAll($criteria);
        return CHtml::listData($models, 'id', 'first_name');
    }
    
    /** @Author: DuongNV Jun 09,19
     *  @Todo: get array transport
     **/
    public function getArrayTransport() {
        $criteria   = new CDbCriteria;
        $criteria->compare('t.role_id', ROLE_CUSTOMER);
        $criteria->compare('t.is_maintain', UsersExtend::STORE_CARD_TRANSPORT);
        $models     = Users::model()->findAll($criteria);
        return CHtml::listData($models, 'id', 'first_name');
    }
    
    /** @Author: DungNT May 25, 2019
     *  @Todo: save one row nhận hàng (import)
     *  detail chia làm 2 loại: import (nhận hàng) và export (giao hàng)
     **/
    public function saveDetailImport() {
        $isNew = true;
        $mDetail                            = new TruckPlanDetail();
        if(!empty($this->rDetailImport)){
            $mDetail    = $this->rDetailImport;
            $isNew      = false;
        }
        $mDetail->truck_plan_id             = $this->id;
        $mDetail->type_order                = TruckPlan::TYPE_ORDER_IMPORT;
        $mDetail->type                      = $this->type;
        $mDetail->plan_date                 = $this->plan_date;
        $mDetail->supplier_id               = $this->supplier_id;
        $mDetail->supplier_warehouse_id     = $this->supplier_warehouse_id;
        $mDetail->supplier_price            = $this->supplier_price;
        $mDetail->price_usd_ton             = $this->price_usd_ton;
        $mDetail->exchange_rate             = $this->exchange_rate;
        $mDetail->driver_id                 = $this->driver_id;
        $mDetail->car_id                    = $this->car_id;
        $mDetail->customer_id               = 0;
        $mDetail->customer_role_id          = 0;
        $mDetail->customer_warehouse_id     = 0;
        $mDetail->customer_warehouse_other  = '';
        $mDetail->customer_price            = $this->supplier_price;
        $mDetail->qty_plan                  = $this->qty_plan;
//        if($isNew || $this->auto_done == TruckPlan::AUTO_DONE_BY_USER){
        if(1){// Aug1919 luôn update từ web, ko cần check trường hợp - Cuối tháng Chiến mới sửa dữ liệu để ra báo cáo
            $mDetail->qty_real                  = $this->qty_real;
            $mDetail->status                    = $this->status;
        }
        $mDetail->created_by_note           = $this->created_by_note;
        $mDetail->driver_note               = '';
        $mDetail->cancel_note               = '';
        $mDetail->save();
        $mDetail->titleNotify = "{$this->getPlanDate()} Gas bồn nhận: {$this->getSupplierWarehouse()}";
        $mDetail->notifyEmployee();
        $this->aIdDetailNotDelete[$mDetail->id] = $mDetail->id;
    }
    
    /** @Author: DuongNV May 17,19
     *  @Todo: save list detail
     **/
    public function saveDetailExport($aPostValue) {
        $this->saveDetailImport();
        if( empty($aPostValue['customer_id']) ) { return false;}
        if( !$this->isNewRecord ){
            $aDetail                            = $this->getArrayDetailExport();
        }
        foreach ($aPostValue['customer_id'] as $key => $customer_id) {
            $mDetail                            = new TruckPlanDetail();
            $isNew                              = true;
            // Update exists detail ( not delete all and create new all)
            if( isset($aPostValue['id'][$key]) && isset($aDetail[$aPostValue['id'][$key]]) && !$this->isNewRecord ){
                $mDetail                        = $aDetail[$aPostValue['id'][$key]];
                $isNew                          = false;
//                $aNotDelete[]                   = $aPostValue['id'][$key];
            }
            $mDetail->customer_id               = $customer_id;
            $mCustomer = $mDetail->rCustomer;
            $mDetail->truck_plan_id             = $this->id;
            $mDetail->type_order                = TruckPlan::TYPE_ORDER_EXPORT;
            $mDetail->type                      = $this->type;
            $mDetail->plan_date                 = $this->plan_date;
            $mDetail->supplier_id               = $this->supplier_id;
            $mDetail->supplier_warehouse_id     = $this->supplier_warehouse_id;
            $mDetail->supplier_price            = $this->supplier_price;
            $mDetail->driver_id                 = $this->driver_id;
            $mDetail->car_id                    = $this->car_id;
            $mDetail->customer_role_id          = $mCustomer->role_id;
            $mDetail->customer_warehouse_id     = 0;
            $mDetail->customer_warehouse_other  = $aPostValue['customer_warehouse_other'][$key];
            $mDetail->customer_price            = $aPostValue['customer_price'][$key];
            $mDetail->qty_plan                  = $aPostValue['qty_plan'][$key];
            $mDetail->qty_real                  = $aPostValue['qty_real'][$key];
            $mDetail->status                    = $aPostValue['status'][$key];
            $mDetail->created_by_note           = $aPostValue['created_by_note'][$key];
            $mDetail->price_usd_ton             = $aPostValue['price_usd_ton'][$key];// DungNT Sep2119
            $mDetail->exchange_rate             = $aPostValue['exchange_rate'][$key];
            $mDetail->driver_note               = '';
            $mDetail->cancel_note               = '';
            $mDetail->trimData();
            if($this->auto_done == TruckPlan::AUTO_DONE_BY_USER){// overide auto done
                $mDetail->status                = $this->status;
            }

            if($isNew){
                $mDetail->save();
            } else {
                $mDetail->update();
            }
            $mDetail->notifyEmployee();
            $this->aIdDetailNotDelete[$mDetail->id] = $mDetail->id;
        }
        // Delete detail
        $this->deleteDetail();
        $this->webUpdateDetail();
//        if( !$this->isNewRecord ){
//            $aDelete = array_diff(array_keys($aDetail), $aNotDelete);
//            $criteria = new CDbCriteria;
//            $criteria->addInCondition('id',$aDelete);
//            TruckPlanDetail::model()->deleteAll($criteria);
//        }
    }
    
    /** @Author: DungNT May 27, 2019
     *  @Todo: delete some detail
     **/
    public function deleteDetail() {
//        $aDelete = array_diff(array_keys($aDetail), $aNotDelete);
        $criteria = new CDbCriteria;
        if(count($this->aIdDetailNotDelete)){
            $criteria->addNotInCondition('id', $this->aIdDetailNotDelete);
        }
        $criteria->compare('truck_plan_id', $this->id);
        $models = TruckPlanDetail::model()->findAll($criteria);
        Users::deleteArrModel($models);
    }
    
    /** @Author: DungNT Jul 30, 2019
     *  @Todo: web: do some action update detail when doing from web
     **/
    public function webUpdateDetail() {
        $aDetailExport = $this->getArrayDetailExport();
        $aStatusUpdate = [TruckPlan::STT_DONE, TruckPlan::STT_CANCEL];
        $mAppUserLogin = Users::model()->findByPk(MyFormat::getCurrentUid());
        foreach($aDetailExport as $mDetail){
            if(in_array($mDetail->status, $aStatusUpdate)){
                $mDetail->mAppUserLogin = $mAppUserLogin;
                $mDetail->doSameAction();// DungNT Jul3019
            }
        }
    }
    
    /** @Author: DungNT May 27, 2019
     *  @Todo: get array detail export
     **/
    public function getArrayDetailExport() {
        $mDetailSearch                  = new TruckPlanDetail();
        $mDetailSearch->truck_plan_id   = $this->id;
        return $mDetailSearch->getListDetail(TruckPlan::TYPE_ORDER_EXPORT);
    }
    
    public function generateCodeNo(){
        if(empty($this->code_no)){
            $this->code_no = MyFunctionCustom::getNextId('TruckPlan', 'TA'.date('y'), LENGTH_TICKET, 'code_no');
        }
    }
    
    /** @Author: DungNT May 24, 2019
     *  @Todo: init some data when create
     **/
    public function initDataCreate() {
        $this->unsetAttributes();
        $this->plan_date    = date('d/m/Y');
        $this->status       = TruckPlan::STT_NEW;
    }
    
     /** @Author: DungNT May 24, 2019
     *  @Todo: format some data before save to database
     **/
    public function formatDataSave() {
        $this->generateCodeNo();
        if(strpos($this->plan_date, '/')){
            $this->plan_date = MyFormat::dateConverDmyToYmd($this->plan_date);
            MyFormat::isValidDate($this->plan_date);
        }
        $this->plan_date_bigint = strtotime($this->plan_date);
        if(empty($this->created_by)){
            $this->created_by       = MyFormat::getCurrentUid();
        }
        $this->qty_plan         = MyFormat::removeComma($this->qty_plan);
        $this->qty_real         = MyFormat::removeComma($this->qty_real);
        $this->supplier_price   = MyFormat::removeComma($this->supplier_price);
        $this->transport_price  = MyFormat::removeComma($this->transport_price);
        $this->price_usd_ton    = MyFormat::removeComma($this->price_usd_ton);
        $this->exchange_rate    = MyFormat::removeComma($this->exchange_rate);
        if($this->isNewRecord && $this->auto_done == TruckPlan::AUTO_DONE_BY_USER){
            $this->status = TruckPlan::STT_DONE;
        }
        
    }
    /** @Author: DungNT May 24, 2019
     *  @Todo: format some data before render to view 
     **/
    public function formatDataBeforeRenderUpdate() {
        $this->plan_date = MyFormat::dateConverYmdToDmy($this->plan_date, 'd/m/Y');
    }
    
    public function validateWebCreate() {
        if( empty($_POST['TruckPlanDetail']['customer_id']) ) { 
            $this->addError('type', 'Chưa chọn khách giao hàng');
            return ;
        }
        $models = Users::getArrObjectUserByArrUid($_POST['TruckPlanDetail']['customer_id']);
        foreach($models as $mUser){
            if($mUser->role_id == ROLE_CUSTOMER && $mUser->is_maintain != UsersExtend::STORE_CARD_GAS_TANK){
                $this->addError('customer_id', "Khách hàng {$mUser->first_name} không phải là khách hàng gas bồn");
                return ;
            }
        }
    }
    
    /** @Author: DungNT Jun 20, 2019
     *  @Todo: get all file of each detail
     **/
    public function getAllFile() {
        $res = $this->getImgImport();$imgExport = '';
        foreach($this->rDetailExport as $mDetail){
            $imgExport .= $mDetail->getViewImg();
        }
        if(!empty($imgExport)){
            $res .= "<br>Ảnh Giao  $imgExport";
        }
        return $res;
    }
    
    /** @Author: DungNT Jun 20, 2019
     *  @Todo: get file import
     **/
    public function getImgImport() {
        $res = '';
        if($this->rDetailImport){
            $res .= 'Ảnh Nhận '.$this->rDetailImport->getViewImg();
        }
        return $res;
    }
    
    /** @Author: DungNT Jun 22,19
     *  @Todo: Chỉ người tạo dc update và update sau 5 ngày kể từ ngày tạo
     **/
    public function canUpdate() {
        $cRole      = MyFormat::getCurrentRoleId();
        $cUid       = MyFormat::getCurrentUid();
        if($cRole == ROLE_ADMIN){
            return true;
        }
        $dayCanUpdate   = 35;// Khoảng thời gian có thể update kể từ ngày tạo - 1 thang de Chien lam bao cao
        $cDate          = date('Y-m-d');
        $expDate        = MyFormat::modifyDays($this->created_date, $dayCanUpdate);
        if($cUid == $this->created_by && MyFormat::compareTwoDate($expDate, $cDate)){
            return true;
        }
        return false;
    }

    /** @Author: DungNT Jun 22, 2019
     *  @Todo: get detail on list
     **/
    public function getHtmlShortTableDetail() {
        $sumQty = 0;
        $str = "<table  cellpadding='0' cellspacing='0'> ";
        $str .= "<tr>";
//        $str .= "<td>Mã đơn</td>";
        $str .= "<td>Nơi giao</td>";
        $str .= "<td>SL giao</td>";
        $str .= "<td>Trạng thái</td>";
//        $str .= "<td>Lái xe note</td>";
        $str .= "<td>TG Hoàn thành</td>";
        $str .= "</tr>";
        foreach ($this->getArrayDetailExport() as $mDetail):
            $str .= "<tr>";
//                    $str .= "<td  style='border:1px solid #000;'>{$mDetail->getCodeNo()}</td>";
                    $str .= "<td  style='border:1px solid #000;'>{$mDetail->getCustomer()} {$mDetail->getCodeNo()}</td>";
                    $str .= "<td  style='border:1px solid #000; text-align: right;'>{$mDetail->getQtyReal(true)}</td>";
                    $str .= "<td  style='border:1px solid #000; text-align: center;'>{$mDetail->getStatus()}</td>";
//                    $str .= "<td  style='border:1px solid #000;'>{$mDetail->getDriverNote()}</td>";
                    $str .= "<td  style='border:1px solid #000;'>{$mDetail->getCompleteTime()} {$mDetail->getDriverNote()}</td>";
            $str .= "</tr>";
            $sumQty += $mDetail->getQtyReal(false);
        endforeach;
        $str .=     "<tr class='item_b item_c'>";
        $str .=         "<td style='border:1px solid #000;'>Tổng</td>";
        $str .=         "<td style='border:1px solid #000;'>" . ActiveRecord::formatCurrency($sumQty) . "</td>";
        $str .=         "<td colspan='2' style='border:1px solid #000;'></td>";
        $str .=     "</tr>";
        $str .= "</table>";
        $str = "<p style='display:inline-block;'>$str</p>";
        return $str;
    }
    
}