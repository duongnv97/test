<?php

/*
 * This is the model class for table "{{_contract_list_management}}".
 * 
 * 
 */

class ContractListManagement extends BaseSpj {
    
    const TYPE_SELL     = 1;
    const TYPE_BUY      = 2;
    
    public $autocomplete_name;
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_contract_list_management}}';
    }
    
    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            array('province_id,contract_number,customer_name,contract_value,sign_date', 'required','on' => 'create, update'),
            array('type, status,id, contract_number, sign_date,customer_id, description, contract_value, bank_account_number,bank, bank_branch,province_id,created_date,created_by', 'safe'),
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rUser' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'rProvince' => array(self::BELONGS_TO, 'GasProvince', 'province_id'),
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'created_by'),
        ];
    }
    
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contract_number' => 'Số hợp đồng',
            'customer_id' => 'Tên khách hàng',
            'customer_name' => 'Tên khách hàng',
            'sign_date' => 'Ngày ký',
            'description' => 'Diễn giải',
            'contract_value' => 'Giá trị hợp đồng',
            'bank_account_number' => 'Số tài khoản',
            'bank' => 'Ngân hàng',
            'bank_branch' => 'Chi nhánh',
            'province_id' => 'Tỉnh', 
            'created_by' => 'Người tạo', 
            'created_date' => 'Ngày tạo', 
            'status' => 'Trạng thái', 
            'type' => 'Loại', 
        ];
    }
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        if (!empty($this->contract_number)) {
            $criteria->compare('t.contract_number', $this->contract_number);
        }
        if (!empty($this->status)) {
            $criteria->compare('t.status', $this->status);
        }
        if(!empty($this->customer_id)){
            $criteria->compare('t.customer_id', $this->customer_id);
        }
        if(!empty($this->type)){
            $criteria->compare('t.type', $this->type);
        }
        if(is_array($this->province_id) && count($this->province_id)){
            $sParamsIn = implode(',', $this->province_id);
            $criteria->addCondition("t.province_id IN ($sParamsIn)");
        }
        if(!empty($this->sign_date)){
            $signDate = MyFormat::dateConverDmyToYmd($this->sign_date,'-');
            $criteria->compare('t.sign_date', $signDate);
        }
        $sort = new CSort();

        $sort->attributes = array(
            'contract_number' => 'contract_number',
            'customer_id' => 'customer_id',
            'province_id' => 'province_id',
        );

        $sort->defaultOrder = 't.id desc';
//        $criteria->compare('can_access',$this->can_access,true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => $sort,
            'pagination' => array(
                //'pageSize' => $this->pageSize,
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            )
        ));
    }
    
    public function getArrayType(){
        return [
            self::TYPE_BUY  => 'HĐ mua',
            self::TYPE_SELL => 'HĐ bán',
        ];
    }
    
    public function getContractNumber(){
        return $this->contract_number;
    }
    
    public function getContractValue(){
        return ActiveRecord::formatCurrency($this->contract_value);
    }
    
    public function getDescription(){
        return $this->description;
    }
    
    public function getSignDate($fomat = 'd/m/Y'){
        return MyFormat::dateConverYmdToDmy($this->sign_date, $fomat);
    }
    
    public function getCustomerName(){
        $mCustomer = $this->rCustomer;
        if(!empty($mCustomer)){
            return $mCustomer->getFullName();
        }
        return '';
    }
    
    public function getCustomerNameText(){
        return $this->customer_name;
    }

    public function getProvince() {
        $mProvince = $this->rProvince;
        return $mProvince->name;
    }
    
    public function getUidLogin() {
        $mUser = $this->rUidLogin;
        if(!empty($mUser)){
            return $mUser->getFullName();
        }
        return '';
    }
    
    public function formatDataBeforeSave() {
        if (strpos($this->sign_date, '/')) {
            $this->sign_date = MyFormat::dateConverDmyToYmd($this->sign_date);
            MyFormat::isValidDate($this->sign_date);
        }
        $this->created_by   = MyFormat::getCurrentUid();
        $this->contract_value   = str_replace(',', '', $this->contract_value);
    }
    
    public function getArrayUserCanupdateAll(){
        return [
            182302,//Nguyễn Thị Ngân - Kế Toán Trưởng
        ];
    }
    /** @Author: NamNH Sep2519
    *  @Todo:check canupdate
    **/
    public function canUpdate(){
        $cRole = MyFormat::getCurrentRoleId();
        $cUid = MyFormat::getCurrentUid();
        $aUidApply = $this->getArrayUserCanupdateAll();
        if ($cRole == ROLE_ADMIN || in_array($cUid, $aUidApply)) {
            return true;
        }
        if ($cUid == $this->created_by) {
            return true;
        }
        return false;
    }

    /** @Author: NamNH Oct0119
    *  @Todo:get all active contract
    **/
    public function getListContract()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('t.status', STATUS_ACTIVE);
        $acontractListManagement = ContractListManagement::model()->findAll($criteria);
        foreach ($acontractListManagement as $key => $mContractListManagement) {
            $aRes[$mContractListManagement->id] = $mContractListManagement->contract_number . ' - '. $mContractListManagement->customer_name;
        }
        return $aRes;
    }

    public function formatBeforeUpdate(){
        $this->sign_date = MyFormat::dateConverYmdToDmy($this->sign_date);
    }
    
    public function getStatus(){
        $aStatus  = ActiveRecord::getUserStatus();
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : '';
    }
    
    public function getType(){
        $aType  = $this->getArrayType();
        return isset($aType[$this->type]) ? $aType[$this->type] : '';
    }
    
    public function getTotalCompleteMoney($format = true){
        $result = 0;
        if(empty($this->id)){
            return $result;
        }
        $criteria   = new CDbCriteria;
        $criteria->compare('t.cashier_confirm', GasSettle::CASHIER_CONFIRM_YES);
        $criteria->select   = 'sum(t.amount) as amount,t.id';
        $criteria->compare('t.contract_id', $this->id);
        $criteria->group    = 't.id';
        $mSettle    = GasSettle::model()->find($criteria);
        if(!empty($mSettle)){
            $result += $mSettle->amount;
        }
        return $format ? ActiveRecord::formatCurrency($result) : $result;
    }
}
