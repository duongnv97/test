<?php

/**
 * This is the model class for table "{{_sell_detail}}".
 *
 * The followings are the available columns in table '{{_sell_detail}}':
 * @property string $id
 * @property string $sell_id
 * @property string $customer_id
 * @property integer $type_customer
 * @property string $sale_id
 * @property string $agent_id
 * @property string $uid_login
 * @property string $employee_maintain_id
 * @property integer $materials_type_id
 * @property integer $materials_id
 * @property string $qty
 * @property string $price
 * @property string $amount
 * @property string $seri
 * @property string $created_date_only
 */
class SellDetail extends CActiveRecord
{
    public $date_month, $date_year;
    
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_sell_detail}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('materials_type_id, materials_id', 'required', 'on'=>'WindowCreate'),
            array('qty','numerical','min'=>0, 'on'=>'WindowCreate'),
            array('materials_parent_id, id, sell_id, customer_id, type_customer, sale_id, agent_id, uid_login, employee_maintain_id, materials_type_id, materials_id, qty, price, amount, seri, created_date_only', 'safe'),
            array('amount_bu_vo, type_amount, amount_discount', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rAgent' => array(self::BELONGS_TO, 'Users', 'agent_id'),
            'rMaterial' => array(self::BELONGS_TO, 'GasMaterials', 'materials_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'customer_id' => 'Customer',
            'type_customer' => 'Type Customer',
            'sale_id' => 'Sale',
            'agent_id' => 'Agent',
            'uid_login' => 'Uid Login',
            'employee_maintain_id' => 'Employee Maintain',
            'materials_type_id' => 'Materials Type',
            'materials_id' => 'Materials',
            'qty' => 'Qty',
            'price' => 'Price',
            'amount' => 'Amount',
            'seri' => 'Seri',
            'created_date_only' => 'Created Date Only',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.customer_id',$this->customer_id,true);
        $criteria->compare('t.type_customer',$this->type_customer);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
 
    protected function beforeSave() {
        $this->seri = trim($this->seri);
        return parent::beforeSave();
    }
    
    /**
    * @Author: ANH DUNG Feb 23 2018
    * @Todo: get all record by sell id
     */        
    public static function getByRootId($root_id){
        if(empty($root_id)){
            return [];
        }
        $criteria = new CDbCriteria();
        $criteria->compare('sell_id', $root_id);
        return self::model()->findAll($criteria);
        // Với những model không xử lý beforedelete thì không phải delete từng item nữa, mà xóa hết luôn DeleteAll
        // Sep 16, 2016 không cần thiết phải xóa từng detail nữa,
    }
    
    /**
    * @Author: ANH DUNG Sep 16, 2016
    * @Todo: get delete all store card detail by store_card_id
    * @Param: $root_id
     */        
    public static function deleteByRootId($root_id){
        if(empty($root_id)){
            return ;
        }
        $criteria = new CDbCriteria();
        $criteria->compare('sell_id', $root_id);
        self::model()->deleteAll($criteria);
        // Với những model không xử lý beforedelete thì không phải delete từng item nữa, mà xóa hết luôn DeleteAll
        // Sep 16, 2016 không cần thiết phải xóa từng detail nữa,
    }
    
    /**
     * @Author: ANH DUNG Oct 25, 2016
     * @Todo: get discount amount for one detail
     */
    public function getDiscountAmount() {
        if($this->amount_discount > 1){
            $amount_discount    = $this->amount_discount;
        }else{
            $amount_discount    = $this->qty_discount > 0 ? ($this->qty_discount * Sell::model()->getPromotionDiscount()) : 0;
        }
        return $amount_discount;
    }
    
    /** @Author: ANH DUNG Jul 13, 2017
     * @Todo: tính giá bán val dây khi giao nhận bấm lưu trên app, chỉ tính lại giá val dây, ko có gas 12kg
     */
    public function calcPriceVanDay($priceHgd) {
//        if($this->materials_id == GasMaterials::ID_DAY_INOX){
//            return ;// Aug0217 dây inox không can thiệp giá từ app giao nhận
//        }// Aug3118 luôn tính lại giá  cho all vật tư
        $this->calcPriceVanDayV1($priceHgd);
    }

    /** @Author: ANH DUNG Aug 03, 2017
     * @Todo: tách hàm tính riêng giá val dây, không có VT dây inox khuyến mãi
     */
    public function calcPriceVanDayV1($priceHgd) {
        if( $this->materials_type_id != GasMaterialsType::MATERIAL_BINH_12KG && in_array($this->materials_type_id, GasMaterialsType::$ARR_VT_HGD)){
            if($this->amount > 0 ){// có thể bỏ khi anh Hoàn setup xong giá val dây bếp
                return ;
            }
            $this->price    = isset($priceHgd[$this->materials_id]) ? $priceHgd[$this->materials_id] : 0;
            $this->amount   = $this->qty * $this->price;
        }
    }
    
    /** @Author: ANH DUNG Aug 15, 2018
     *  @Todo: list agent not limit gas remain back
     * Chú hướng dẫn tài xế khi giao nhận gas đại lý Quận 6 nhớ để riêng bình gas cân gas dư và cân lại trước khi mang về xưởng ,nếu tài xế ko cân mà khi về đến xưởng thiếu tài xế chịu trách nhiệm ạ.
     **/
    public function getListAgentFullRemain() {
        return [
            211393      => Sell::REMAIN_ALLOW_6, // Đại Lý Quận 6 - end 2019-04-01
            1284630     => Sell::REMAIN_ALLOW_6, // Sep1318 Đại Lý Bửu Long Close Oct1618
            242369      => Sell::REMAIN_ALLOW_6, // Sep1818 Đại Lý Quận 10
            126 => Sell::REMAIN_ALLOW_6, // Đại lý Tân Phú 
            758528      => Sell::REMAIN_ALLOW_6,// Đại Lý Uyên Hưng - Oct1718
            123         => Sell::REMAIN_ALLOW_6, // Đại lý Trảng Dài -- END 2019-04-15
            109         => Sell::REMAIN_ALLOW_6, // Đại lý Lái Thiêu
            111         => Sell::REMAIN_ALLOW_6, // Đại lý Bến Cát

            // End chuong trinhf -> Dec3118
            262526      => Sell::REMAIN_ALLOW,// Cửa Hàng Ô Môn - ko co ket thuc, lam mai
            1317820     => Sell::REMAIN_ALLOW,//  Đại Lý Trà Nóc 
            30753       => Sell::REMAIN_ALLOW, // Cửa hàng Cần Thơ 1
            138544      => Sell::REMAIN_ALLOW, // Cửa hàng Cần Thơ 2
            171449      => Sell::REMAIN_ALLOW, // Cửa hàng Trà Vinh
            
            110 => Sell::REMAIN_ALLOW_6, // Đại lý Thủ Dầu Một - Oct1818
            971240 => Sell::REMAIN_ALLOW_6,// Đại Lý Phú Mỹ -  kết thúc chương trình 30/10/2019.
//            268833 => Sell::REMAIN_ALLOW_6,// Đại Lý Phú Hòa 
            
//            106             => Sell::REMAIN_ALLOW_6,// Đại lý Bình Thạnh 1 - end Jan3119
            658920          => Sell::REMAIN_ALLOW_6,// Đại lý Bình Thạnh 2 - end May0919
            112             => Sell::REMAIN_ALLOW_6,// Đại lý Thủ Đức 1 - end 2019-03-31
            
            118             => Sell::REMAIN_ALLOW_6,// Đại lý An Thạnh - Now1218
//            100             => Sell::REMAIN_ALLOW_6,// Đại lý Quận 2 - Now1318
            116             => Sell::REMAIN_ALLOW_6,// Đại lý Bình Đa- Now2218
            117             => Sell::REMAIN_ALLOW_6,// Đại lý Thống Nhất- Now2318
            114             => Sell::REMAIN_ALLOW_6,// Đại lý Long Bình Tân- Now2318
            357031          => Sell::REMAIN_ALLOW_6,// Đại lý Tân Phước Khánh - Now2318
            1317722         => Sell::REMAIN_ALLOW_6,// Đại Lý Long Khánh - Đồng Nai - Now2318   01.04.2019 đến 31.05.2019 , cân giới hạn : 6kg 
            
//            768409          => Sell::REMAIN_ALLOW_6, // Đại Lý Thủ Đức 2 - Now2918
            1372582         => Sell::REMAIN_ALLOW_6, // Đại Lý Quận 2.2 - Now2918
            
            919016          => Sell::REMAIN_ALLOW_6, // Đại Lý Lê Phụng Hiểu - Dec0118
            952187          => Sell::REMAIN_ALLOW_6, // Đại Lý Bến Nôm - Dec0118
            1011084         => Sell::REMAIN_ALLOW_6, // Đại Lý Trần Phú - Dec0118
            2030779         => Sell::REMAIN_ALLOW_6, // Đại Lý Biển Đông 1 - Vũng Tàu
            
            1161320 => Sell::REMAIN_ALLOW, // Đại Lý Nguyễn Trãi - Phường 9 Cà Mau - Dec0118
            1048571 => Sell::REMAIN_ALLOW, // Đại Lý Cà Mau Petro - Phường 8 - Dec0118
            1079730 => Sell::REMAIN_ALLOW, // Đại Lý Bạc Liêu - Aug0319
//            CodePartner::AGENT_THANH_SON => Sell::REMAIN_ALLOW_6,// end 2019-01-31
//            939720          => Sell::REMAIN_ALLOW_7, // Đại Lý Bình Chuẩn
            
            105             => Sell::REMAIN_ALLOW_6, // Đại Lý Quan 9 -- end 2019-01-10
            1058061         => Sell::REMAIN_ALLOW_6, // Đại Lý Thủ Đức 3  -- end 2019-04-22
            
            // Anh Đợt
//            108             => Sell::REMAIN_ALLOW_6, // Đại lý Hóc Môn - start Jan0218 - end 2019-02-02
            235             => Sell::REMAIN_ALLOW_6, // Đại lý Tân Bình 1 - end 2019-02-02
            107             => Sell::REMAIN_ALLOW_6, // Đại lý Gò Vấp 1 -- end 2019-03-31
            27740           => Sell::REMAIN_ALLOW_6, // Đại lý Tân Bình 2 -- Lan TB1
            768408          => Sell::REMAIN_ALLOW_6, //  Đại Lý Bình Thạnh 3 -- Lan TB1
            103             => Sell::REMAIN_ALLOW_6, // Đại lý Quận 8.1 End 2019-03-06
            122             => Sell::REMAIN_ALLOW_6, // Đại lý Bình Tân End 2019-03-06
            1599181         => Sell::REMAIN_ALLOW_6, // Đại Lý Bình Tân 2

            1120439         => Sell::REMAIN_ALLOW, // Đại Lý Sóc Trăng -- end 2019-02-28
            1317767         => Sell::REMAIN_ALLOW, // Đại Lý Châu Đốc -- end 2019-02-03
            1317794         => Sell::REMAIN_ALLOW, // Đại Lý Núi Sam -- end 2019-02-03
            475830          => Sell::REMAIN_ALLOW, // Cửa Hàng Long Xuyên 1 -- end 2019-03-20
            596026          => Sell::REMAIN_ALLOW, // Cửa Hàng Long Xuyên 2 -- end 2019-03-20
            30751           => Sell::REMAIN_ALLOW, // Cửa hàng Vĩnh Long 1-- end 2019-02-03
//            197436          => Sell::REMAIN_ALLOW_6, // Cửa Hàng Phú Thuận -- end 2019-02-03
//            652695          => Sell::REMAIN_ALLOW_6, // Cửa Hàng Bình Minh  -- end 2019-02-03

//            104             => Sell::REMAIN_ALLOW_6, // Đại lý Quận 8.2 -- end 2019-02-03
            100             => Sell::REMAIN_ALLOW_6, // Đại lý Quận 2 - A Hieu -- end 2019-01-24
            1210050         => Sell::REMAIN_ALLOW, // Đại Lý Tp Bến Tre -- end 2019-04-30
            375037          => Sell::REMAIN_ALLOW_7, // Đại lý Thái Hòa - start 2019-01-12 - end sau 4 thang
            115             => Sell::REMAIN_ALLOW_6, // Đại lý Dĩ An - start 2019-03-01 - end sau 4 thang - end 2019-07-01
//            1486944         => Sell::REMAIN_ALLOW, // Thanh Minh - Long An - start 2019-03-22 -  end sau 20 ngày  end 2019-04-10
//            1317841         => Sell::REMAIN_ALLOW_6, // Đại Lý Cao Lãnh - Đồng Tháp- start 2019-03-22 - end sau 20 ngày  end 2019-04-10
            868845          => Sell::REMAIN_ALLOW_6, // Đại Lý Gò Vấp 2- start 2019-04-01 -- 
            1966690         => Sell::REMAIN_ALLOW, // Đại Lý Mỹ Tho 1 - start 2019-04-09 -- 
            2039377         => Sell::REMAIN_ALLOW, // Đại Lý Mỹ Tho 2 - Tiền Giang
            392141          => Sell::REMAIN_ALLOW_6, // Đại lý Long Thành - start 2019-04-09 -- 
            367242          => Sell::REMAIN_ALLOW_6, // Đại lý Hố Nai -  chương trình từ ngày 16.4.2019 - 15.6.2019, cân giới hạng 6kg
            137638          => Sell::REMAIN_ALLOW_6, // Đại lý Xưởng Phước Tân -  chương trình từ ngày 16.4.2019 - 15.6.2019, cân giới hạng 6kg
            120             => Sell::REMAIN_ALLOW_6, // - Đại lý Tân Sơn-  chương trình từ ngày 03-05-2019, cân giới hạn 6kg
            1210            => Sell::REMAIN_ALLOW_6, // Đại Lý Thuận Giao-  cân gas dư dưới 6kg đến hết ngày 15/09/2019
            2009121         => Sell::REMAIN_ALLOW_6, // ĐLLK K8 Thành Thành Công - Đồng Nai  end 2019-05-10
            2009125         => Sell::REMAIN_ALLOW_6, // ĐLLK Thành Thành Công - Đồng Nai
            1364588         => Sell::REMAIN_ALLOW_6, // Đại Lý Chơn Thành - Bình Phước 
            2076752         => Sell::REMAIN_ALLOW, // ĐLLK Thiện Phương Bù Đăng - Bình Phước 
            113             => Sell::REMAIN_ALLOW_6, // Đại lý Quận 3
            2076820         => Sell::REMAIN_ALLOW_6, // ĐLLK Gas24h Bình An
            2076814         => Sell::REMAIN_ALLOW_6, // ĐLLK Gạo An Giang - Gas 24h Tân Uyên - cân gas dư đến hết ngày 30/09/2019
            314408          => Sell::REMAIN_ALLOW_6, // Đại lý Bình Thung
            1773595         => Sell::REMAIN_ALLOW_6, // Thanh Sơn 1 - Đồng Nai 
            
            2068340         => Sell::REMAIN_ALLOW, // Đại Lý Lê Chánh
            
            1356862         => Sell::REMAIN_ALLOW_6, // Đại Lý Phú Vinh - Đồng Nai 
            1317722         => Sell::REMAIN_ALLOW_6, // Đại Lý Long Khánh - Đồng Nai
            121             => Sell::REMAIN_ALLOW_6, //  Đại lý Ngã Ba Trị An - Đại Lý
            863146          => Sell::REMAIN_ALLOW, // Cửa Hàng Chợ Mới 2 
            1755139         => Sell::REMAIN_ALLOW_6, // ĐLLK Quang Đại - Cẩm Mỹ 
            572832         => Sell::REMAIN_ALLOW_6, // Đại lý Bình Đường - cân gas dư đến hết ngày 30/09/2019
            1176354        => Sell::REMAIN_ALLOW, //  Đại Lý Châu Thành - An Giang
            1524018        => Sell::REMAIN_ALLOW_6, // Đại Lý Thới Hòa - Bình Dương
            2152168         => Sell::REMAIN_ALLOW_6, // Đại Lý Tân Phú 2
            119             => Sell::REMAIN_ALLOW_6, //  Đại lý Tân Định
            101             => Sell::REMAIN_ALLOW_6, // Đại lý Quận 4
            102             => Sell::REMAIN_ALLOW_6, // Đại lý Quận 7

        ];
    }

    /** @Author: ANH DUNG Oct 08, 2018
     *  @Todo: list agent allow cân gas dư gọi điện 
     **/
    public function getListAgentTakeRemainFromCall() {
        return [
//            1284630, // Sep1318 Đại Lý Bửu Long
//            758528,// Đại Lý Uyên Hưng - Oct1718
//            123, // Đại lý Trảng Dài -- end 2019-02-03 -- close Feb2819
            
//            262526,// Cửa Hàng Ô Môn - Oct1718
//            1317820,//  Đại Lý Trà Nóc
//            30753, // Cửa hàng Cần Thơ 1
//            138544, // Cửa hàng Cần Thơ 2
            
//            118,// Đại lý An Thạnh - Now1218
//            939720, // Đại Lý Bình Chuẩn -- end 2019-01-12
//            115, // Đại lý Dĩ An -- end 2019-05-01
//            392141, // Đại lý Long Thành
        ];
    }

    /** @Author: ANH DUNG Mar 11, 2018
     *  @Todo: đưa 1 số validate dữ liệu vào model này
     **/
    public function validateData($mSell) {
        if($this->gas_remain > 0 && $mSell->is_timer != Forecast::TYPE_TIMER){ //NamNH Sep 03, 2019
            throw new Exception('Ngừng chương trình cân gas dư bắt nóng, chỉ cho phép cân gas dư đối với các khách hàng đặt gas hẹn giờ qua App');
        }
        if($this->kg_empty > 0 && $this->kg_empty < 10){
            throw new Exception('Trọng lượng vỏ phải trên 10kg, vui lòng kiểm tra lại vỏ');
        }
        if($this->gas_remain < 0 ||  $this->gas_remain > 12){
            throw new Exception('Trọng lượng vỏ phải nhỏ hơn trọng lượng cân, cho phép Gas dư tối đa '.Sell::REMAIN_ALLOW.' kg');
        }
        // Now0818 xu ly can Full Gas Du cho Don hen gio
        $aAgentGasRemain    = $this->getListAgentFullRemain();
        $maxGasRemainAllow  = isset($aAgentGasRemain[$this->agent_id]) ? $aAgentGasRemain[$this->agent_id] : Sell::REMAIN_ALLOW;
//        if($mSell->is_timer != Forecast::TYPE_TIMER && !in_array($this->agent_id, $this->getListAgentFullRemain()) && $this->gas_remain > Sell::REMAIN_ALLOW){
        if($mSell->is_timer != Forecast::TYPE_TIMER && !array_key_exists($this->agent_id, $this->getListAgentFullRemain()) && $this->gas_remain > Sell::REMAIN_ALLOW){
            throw new Exception('Trọng lượng vỏ phải nhỏ hơn trọng lượng cân, cho phép Gas dư tối đa '.Sell::REMAIN_ALLOW.' kg');
        }
        if($mSell->is_timer != Forecast::TYPE_TIMER && array_key_exists($this->agent_id, $this->getListAgentFullRemain()) && $this->gas_remain > $maxGasRemainAllow){
            throw new Exception('Trọng lượng vỏ phải nhỏ hơn trọng lượng cân, cho phép Gas dư tối đa '.$maxGasRemainAllow.' kg');
        }

        if($this->gas_remain > 0 && (empty($this->seri) || ($this->seri*1) < 1)){
            throw new Exception('Bạn chưa nhập Seri bình');
        }
//        if($this->gas_remain > 0 && $this->gas_remain < 1){// Mar2219 check ko cho can gas du duoi 1kg
//            throw new Exception('Không cho phép cân Gas dư dưới 1kg, vui lòng kiểm tra lại');
//        } DungNT May2519 Close theo y kien GD
        // Oct0818 cho phép Cân gas dư của Đơn App và Call
        if(in_array($this->agent_id, $this->getListAgentTakeRemainFromCall())){
            return ;
        }
        if($this->gas_remain > 0 && $mSell->source != Sell::SOURCE_APP){
            throw new Exception('Cân gas dư HGĐ chỉ áp dụng cho đơn hàng app. Bạn hãy kiểm tra lại');
        }
        $aProvinceAllow = [GasProvince::TP_HCM, GasProvince::TINH_DONG_NAI, 
            GasProvince::TINH_VUNG_TAU, GasProvince::TINH_CA_MAU, GasProvince::TINH_BAC_LIEU,
        ];
        
        if($mSell->is_timer != Forecast::TYPE_TIMER && $this->gas_remain > 0 && !in_array($mSell->province_id, $aProvinceAllow) && !array_key_exists($this->agent_id, $this->getListAgentFullRemain()) ){
            throw new Exception('Cân gas dư HGĐ chỉ áp dụng cho đơn hàng app tại TP HCM. Bạn không thể nhập gas dư');
        }
    }
    
    /** @Author: DuongNV Sep 07, 2018
     *  @Todo: Get agent name by id
     *  @Param: $id
     **/
    public function getAgentById($id) {
        $mUsers = Users::model()->findByPk($id);
        if(!empty($mUsers && $mUsers->role_id == ROLE_AGENT)){
            return $mUsers->first_name;
        }
        return '';
    }
}