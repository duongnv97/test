<?php
/** @Author: HOANG NAM 26/03/2018
 *  @Todo: 
 *  @Param: 
 **/
/**
 * This is the model class for table "{{_gas_uphold_plan}}".
 *
 * The followings are the available columns in table '{{_gas_uphold_plan}}':
 * @property string $id
 * @property string $start_date
 * @property integer $end_date
 * @property string $employee_id
 * @property string $customer_id
 * @property integer $uphold_id
 * @property integer $uphold_reply_id 
 * @property string $created_date
 */
class GasUpholdPlan extends BaseSpj
{
    public $viewExpired = '', $count_day_update = 5, $autocomplete_employee, $autocomplete_customer, $mCustomer = null;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GasUpholdPlan the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_gas_uphold_plan}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('employee_id,end_date,start_date', 'required','on'=>'create,update'),
            array('viewExpired, id, created_by, start_date, end_date, employee_id, customer_id, uphold_id, uphold_reply_id, created_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'rEmployee' => array(self::BELONGS_TO, 'Users', 'employee_id'),
            'rCustomer' => array(self::BELONGS_TO, 'Users', 'customer_id'),
            'rUphold' => array(self::BELONGS_TO, 'GasUphold', 'uphold_id'),
            'rUpholdReply' => array(self::BELONGS_TO, 'GasUpholdReply', 'uphold_reply_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'start_date' => 'Từ ngày',
            'end_date' => 'Đến ngày',
            'employee_id' => 'Nhân viên',
            'customer_id' => 'Khách hàng',
            'uphold_id' => 'Uphold',
            'uphold_reply_id' => 'Uphold Reply',
            'created_date' => 'Ngày tạo',
            'viewExpired' => 'Xem quá hạn',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria=new CDbCriteria;
        if(!empty($this->employee_id)){
            $criteria->addCondition("t.employee_id = ".$this->employee_id);
        }
        if(!empty($this->customer_id)){
            $criteria->addCondition("t.customer_id = ".$this->customer_id);
        }
        if(!empty($this->start_date)){
            $criteria->addCondition("t.end_date >= '".MyFormat::dateConverDmyToYmd($this->start_date,'-')."'");
        }
        if(!empty($this->end_date)){
            $criteria->addCondition("t.end_date <= '". MyFormat::dateConverDmyToYmd($this->end_date,'-')."'");
        }
        if(!empty($this->viewExpired)){
            $today = date('Y-m-d');
//            $today = MyFormat::modifyDays($today, 4);
            $criteria->addCondition("t.uphold_reply_id=0 AND t.end_date < '$today'");
        }
        $criteria->addCondition("t.uphold_reply_id=0");
        if(!empty($this->uphold_id)){
            $mUphold = new GasUphold();
            $mUphold = $mUphold->findByCodeNo($this->uphold_id);
            if($mUphold){
                $criteria->addCondition("t.customer_id = $mUphold->customer_id");
            }
        }
        
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
        ));
    }
    
    public function beforeSave(){
        // Apr0518 Anh Dũng -> không nên can thiệp ở đây, vì có nhiều chỗ save model, chứ ko riêng gì action create, update trên web
        return parent::beforeSave();
    }
    
    public function  formatDataBeforeSave(){
        $this->start_date   = MyFormat::dateConverDmyToYmd($this->start_date, '-');
        $this->end_date     = MyFormat::dateConverDmyToYmd($this->end_date, '-');
        if($this->scenario !== 'update') {
            $this->created_date = date('Y-m-d H:i:s');
        }
        if(empty($this->created_by)){
            $this->created_by       = MyFormat::getCurrentUid();
        }
    }
    
    public function  formatDataAfterSelect(){
        $this->start_date   = MyFormat::dateConverYmdToDmy($this->start_date,'d-m-Y');
        $this->end_date     = MyFormat::dateConverYmdToDmy($this->end_date,'d-m-Y');
        if($this->scenario !== 'update') {
            $this->created_date = date('Y-m-d H:i:s');
        }
    }
    
    /** @Author: HOANG NAM 04/04/2018
     *  @Todo: validate multi
     *  @Param: 
     **/
    public function validateMulti(){
        if(!isset($_POST['GasUpholdPlan']['customer'])) {
            $this->addError('customer_id','Phải chọn ít nhất 1 khách hàng.');
        }
    }
    /** @Author: HOANG NAM 26/03/2018
     *  @Todo: save multi value
     *  @Param: 
     **/
    public function saveMulti() {
        $aRowInsert= array();
        foreach ($_POST['GasUpholdPlan']['customer'] as $key => $idCustomer) {
            $aRowInsert[]="('". MyFormat::dateConverDmyToYmd($_POST['GasUpholdPlan']['start_date'],'-')."',
                '". MyFormat::dateConverDmyToYmd($_POST['GasUpholdPlan']['end_date'],'-') ."',
                '". $_POST['GasUpholdPlan']['employee_id'] ."',
                '".$idCustomer."',
                '".MyFormat::getCurrentUid()."'
                )";
        }
        $tableName = GasUpholdPlan::model()->tableName();;
        $sql = "insert into $tableName (start_date,end_date,employee_id,customer_id,created_by) values ";
        $sql .= implode(',', $aRowInsert);
        if(count($aRowInsert)>0){
            Yii::app()->db->createCommand($sql)->execute();
        }
    }
    
    public function getStartDate(){
        return MyFormat::dateConverYmdToDmy($this->start_date, 'd/m/Y');
    }
    
    public function getEndDate(){
//        if(empty($this->uphold_reply_id ) && date('Y-m-d') > $this->end_date){
        if($this->isExpired()){
            return "<span class='high_light_tr'>".MyFormat::dateConverYmdToDmy($this->end_date, 'd/m/Y')."</span>";
        }
        return MyFormat::dateConverYmdToDmy($this->end_date, 'd/m/Y');
    }
    
    public function getEmployee(){
        return !empty($this->rEmployee) ? $this->rEmployee->first_name : '';

    }
    
    public function getCustomer($field_name='') {
        if($this->mCustomer){
            $mUser = $this->mCustomer;
        }else{
            $mUser = $this->rCustomer;
        }
        if($mUser){
            if(is_null($this->mCustomer)){
                $this->mCustomer = $mUser;
            }
            if($field_name != ''){
                return $mUser->$field_name;
            }
//            return "<b>".$mUser->code_bussiness."-".$mUser->first_name."</b><br>".$mUser->address."<br><b>Phone: </b>".$mUser->phone;
            return $mUser->code_bussiness.'-'.$mUser->first_name;
        }
        return '';
    }

    public function getReply() {
        $res = '';
        $mUpholdReply = $this->rUpholdReply;
        if($mUpholdReply && $mUpholdReply->status == GasUphold::STATUS_COMPLETE){
            $res = $mUpholdReply->getCreatedDate();
        }
        return $res;
    }
    /** @Author: ANH DUNG Apr 05, 2018
     *  @Todo: kiểm tra row đã quá hạn chưa
     *  @return: true if expired, false if not expired
     **/
    public function isExpired() {
        $res = false;
//        if(empty($this->uphold_reply_id) && MyFormat::compareTwoDate(date('Y-m-d'), $this->end_date)){
        if(empty($this->uphold_reply_id) && date('Y-m-d') > $this->end_date){
            $res = true;
        }
        return $res;
    }
    
    public function canUpdate() {
        $cRole  = MyFormat::getCurrentRoleId();
            if($cRole == ROLE_ADMIN){
            return true;
        }
        $cUid   = MyFormat::getCurrentUid();
        if ($cUid != $this->created_by || MyFormat::modifyDays($this->created_date,5,'+','day') > date('Y-m-d')) {
            return false;
        }
        return true;
    }
    
    /** @Author: ANH DUNG Apr 05, 2018
     *  @Todo: get record schedule plan of customer
     *  @Param: $this->start_date: 2018-04-25
     **/
    public function getRecordByDate() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.customer_id=' . $this->customer_id );
        $criteria->addCondition("t.start_date <= '".$this->start_date."'");
        $criteria->addCondition("t.end_date >= '". $this->start_date."'");
        return self::model()->find($criteria);
    }
    
    /** @Author: ANH DUNG May 18, 2018
     *  @Todo: get plan chưa done, luôn load xuống app giao nhận
     **/
    public function getPlanNotComplete() {
        $criteria = new CDbCriteria();
        if(!empty($this->employee_id)){
            $criteria->addCondition('t.employee_id=' . $this->employee_id );
        }
        if(!empty($this->customer_id)){
            $criteria->addCondition('t.customer_id=' . $this->customer_id );
        }
        $criteria->addCondition('t.uphold_id = 0 ');
        return self::model()->findAll($criteria);
    }
    /** @Author: ANH DUNG May 18, 2018
     *  @Todo: get plan chưa done, luôn load xuống app giao nhận
     **/
    public function getPlanLastestOfCustomer() {
        if(empty($this->customer_id)){
            return null;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.customer_id=' . $this->customer_id );
//        $criteria->addCondition('t.uphold_id = 0 ');
        $criteria->order = "t.id DESC";
        $criteria->limit = 1;
        return self::model()->find($criteria);
    }
    
    /** @Author: ANH DUNG Jan 15, 2019
     *  @Todo: Scan customer Khong Lay Hang => xoa lich bao tri di
     **/
    public function cronRemoveCustomerStopOrder() {
        $from = time();
        $date_from  = date('Y-m-01');
        $date_to    = date('Y-m-d');
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition('t.start_date', $date_from, $date_to);
        $criteria->addCondition('t.uphold_reply_id = 0');
        $models = GasUpholdPlan::model()->findAll($criteria);
        $count = 0;
        foreach($models as $mPlan):
            $mCustomer = Users::model()->findByPk($mPlan->customer_id);
            if($mCustomer && $mCustomer->channel_id == Users::KHONG_LAY_HANG){
                $mPlan->delete();
                $count++;
            }
        endforeach;
        
        $to = time();
        $second = $to-$from;
        $info = $count.' ***************** Remove GasUpholdPlan Record cronRemoveCustomerStopOrder done in: '.($second).'  Second  <=> '.($second/60).' Minutes';
        Logger::WriteLog($info);
        
    }
    
}