<?php

/**
 * This is the model class for table "{{_leave_holidays_type}}".
 *
 * The followings are the available columns in table '{{_leave_holidays_type}}':
 * @property string $id
 * @property string $name
 * @property integer $factor
 * @property string $description
 * @property integer $status
 */
class HrHolidayTypes extends BaseSpj
{
    public $status_active = 1;
    public $status_in_active = 2;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return LeaveHolidaysType the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_hr_holiday_types}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('name, factor, status', 'required','on'=>'create,update'),
            array('status', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>255),
            array('id, name, factor, description, status', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                'id' => 'ID',
                'name' => 'Tên',
                'factor' => 'Hệ số',
                'description' => 'Mô tả',
                'status' => 'Trạng thái',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        if(!empty($this->name)) {
            $criteria->addCondition('t.name like "%'.$this->name.'%"');
        }
        if(!empty($this->factor)) {
            $criteria->addCondition('t.factor = '.$this->factor);
        }
        if(!empty($this->status)) {
            $criteria->addCondition('t.status = '.$this->status);
        }
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    /** @Author: HOANG NAM 30/03/2018
     *  @Todo: get
     *  @Param: 
     **/
    public function getName(){
        return $this->name;
    }
    public function getFactor(){
        return $this->factor;
    }
    public function getDescription(){
        return $this->description;
    }
    public function getArrayStatus(){
        return array(
            $this->status_active => 'Hoạt động',
            $this->status_in_active => 'Không hoạt động',
        );
    }
    public function getStatus(){
        $aStatus = $this->getArrayStatus();
        return isset($aStatus[$this->status]) ? $aStatus[$this->status] : '';
    }
    /** @Author: HOANG NAM 02/04/2018
     *  @Todo: get array type
     *  @Param: 
     **/
    public function getArrayType(){
        $result = array();
        $criteria=new CDbCriteria;
        $criteria->addCondition('t.status = '.$this->status_active);
        $aLeaveHolidaysType = HrHolidayTypes::model()->findAll($criteria);
        foreach ($aLeaveHolidaysType as $key => $mLeaveHolidays) {
            $result[$mLeaveHolidays->id] = $mLeaveHolidays->name;
        }
        return $result;
    }
    
    /** @Author: DuongNV 21/12/2018
     *  @Todo:
     **/
    public function getArrayFactor() {
        $criteria = new CDbCriteria;
        $criteria->compare('t.status', $this->status_active);
        $models = self::model()->findAll($criteria);
        $res    = [];
        foreach ($models as $value) {
            $res[$value->id] = $value->factor;
        }
        return $res;
    }
}