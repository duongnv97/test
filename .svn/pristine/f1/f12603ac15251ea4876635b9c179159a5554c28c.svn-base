<?php

/**
 * This is the model class for table "{{_gas_profile_detail}}".
 *
 * The followings are the available columns in table '{{_gas_profile_detail}}':
 * @property string $id
 * @property string $profile_id
 * @property string $date_expired
 * @property string $file_name
 */
class GasProfileDetail extends CActiveRecord
{
    public static $AllowFile = 'jpg,jpeg,png';
    public static $aSize = array(
        'size1' => array('width' => 128, 'height' => 96), // small size, dùng ở form update văn bản
        'size2' => array('width' => 1536, 'height' => 1300), // size view, dùng ở xem văn bản
    );
    
    public $aIdNotIn;
    
    public static $pathUpload = 'upload/profile';

    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return '{{_gas_profile_detail}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(            
            array('aIdNotIn,id, profile_id, date_expired, file_name', 'safe'),
            array('file_name', 'file','on'=>'UploadFileScan',
                'allowEmpty'=>true,
                'types'=> self::$AllowFile,
                'wrongType'=> "Chỉ cho phép định dạng file ".self::$AllowFile." .",
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'profile_id' => 'Profile',
            'date_expired' => 'Date Expired',
            'file_name' => 'File Name',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.profile_id',$this->profile_id,true);
        $criteria->compare('t.date_expired',$this->date_expired,true);
        $criteria->compare('t.file_name',$this->file_name,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }

    public function defaultScope()
    {
        return array(
                //'condition'=>'',
        );
    }
        
    /**
     * @Author: ANH DUNG Sep 06, 2014
     * @Todo:validate file submit
     * @Param: $model model GasProfile
     */
    public static function validateFile($model, $needMore=array()){
        $model->aModelDetail = array();
        $ok=false;
        if(isset($_POST['GasProfileDetail']['file_name'])  && count($_POST['GasProfileDetail']['file_name'])){
            foreach($_POST['GasProfileDetail']['file_name'] as $key=>$item){
                $mFile = new GasProfileDetail('UploadFileScan');
                $mFile->file_name  = CUploadedFile::getInstance($model->mDetail,'file_name['.$key.']');
                if(!is_null($mFile->file_name)){                    
                    $ok=true;
                    MyFormat::IsImageFile($_FILES['GasProfileDetail']['tmp_name']['file_name'][$key]);
                }
                $mFile->validate();
                if($mFile->hasErrors()){
                    $model->addError('mDetail', "File Error: ".$mFile->getError('file_name'));
                    $model->aModelDetail[] = $mFile;
                    $ok=false;
                    break;
                }
                if(!is_null($mFile->file_name))
                    $model->aModelDetail[] = $mFile;
            }
        }
        if(!$ok && isset($needMore['create'])){
            $model->addError('mDetail', "Bạn Chưa Chọn File Scan Upload ");
        }
    }
    
    /**
     * @Author: ANH DUNG Sep 06, 2014
     * @Todo: saveFileScanDetail
     * @Param: $model model GasProfile
     */
    public static function saveFileScanDetail($model){
        try {            
            set_time_limit(7200);
            $model->aModelDetail = array();
            $NewRecord = GasProfile::model()->findByPk($model->id);
            $model->created_date = $NewRecord->created_date;
            if(isset($_POST['GasProfileDetail']['file_name'])  && count($_POST['GasProfileDetail']['file_name'])){
                foreach($_POST['GasProfileDetail']['file_name'] as $key=>$item){
                    $mFile = new GasProfileDetail('UploadFileScan');
                    $mFile->profile_id = $model->id;
                    $mFile->date_expired = MyFormat::GetYearByDate($model->created_date, array( 'format'=>'Y-m-d'));
                    $mFile->order_number = isset($_POST['GasProfileDetail']['order_number'][$key])?$_POST['GasProfileDetail']['order_number'][$key]:1;
                    $mFile->file_name  = CUploadedFile::getInstance($model->mDetail,'file_name['.$key.']');
                    if(!is_null($mFile->file_name)){
                        $mFile->file_name  = self::saveFile($model, $mFile, 'file_name', $key);
                        $mFile->save();
                        self::resizeImage($model, $mFile, 'file_name');
                    }
                }
            }
        } catch (Exception $exc) {
            throw new Exception("Có Lỗi Khi Upload File: ". $exc->getMessage());
        }
    }    
    
    /**
     * @Author: ANH DUNG Sep 06, 2014
     * To do: save file 
     * @param: $model GasProfileDetail
     * @param: $count 1,2,3
     * @return: name of image upload/transactions/property_document
     */
    public static function  saveFile($mGasFileScan, $model, $fieldName, $count)
    {        
        if(is_null($model->$fieldName)) return '';
        $year = MyFormat::GetYearByDate($mGasFileScan->created_date);        
        $pathUpload = self::$pathUpload."/$year";
        $ext = $model->$fieldName->getExtensionName();
        $fileName = date('Y-m-d').time();
        $fileName = $fileName."-".ActiveRecord::randString().$count.'.'.$ext;
        $imageProcessing = new ImageProcessing();
        $imageProcessing->createDirectoryByPath($pathUpload);
        $model->$fieldName->saveAs($pathUpload.'/'.$fileName);
        return $fileName;
    }
    
    /*
     * @Author: ANH DUNG Sep 06, 2014
     * To do: resize file scan
     * @param: $mGasFileScan model GasFileScan
     * @param: $model model GasProfileDetail
     * @param: $fieldName 
     */
    public static function resizeImage($mGasFileScan, $model, $fieldName) {
        $year = MyFormat::GetYearByDate($mGasFileScan->created_date);
        $pathUpload = self::$pathUpload."/$year";
        $ImageHelper = new ImageHelper();     
        $ImageHelper->folder = '/'.$pathUpload;
        $ImageHelper->file = $model->$fieldName;
        $ImageHelper->aRGB = array(0, 0, 0);//full black background
        $ImageHelper->thumbs = self::$aSize;
//        $ImageHelper->createFullImage = true;
        $ImageHelper->createThumbs();
        $ImageHelper->deleteFile($ImageHelper->folder . '/' . $model->$fieldName);        
    }    
       
    /*
     * @Author: ANH DUNG May 18, 2014
     * To do: delete file scan
     * @param: $model model user
     * @param: $fieldName is avatar, agent_company_logo
     * @param: $aSize
     */
    public static function RemoveFileOnly($pk, $fieldName) {
        $modelRemove = self::model()->findByPk($pk);
        if (is_null($modelRemove) || empty($modelRemove->$fieldName))
            return;
        $year = MyFormat::GetYearByDate($modelRemove->date_expired);
        $pathUpload = self::$pathUpload."/$year";
        $ImageHelper = new ImageHelper();     
        $ImageHelper->folder = '/'.$pathUpload;
        $ImageHelper->deleteFile($ImageHelper->folder . '/' . $modelRemove->$fieldName);
        foreach ( self::$aSize as $key => $value) {
            $ImageHelper->deleteFile($ImageHelper->folder . '/' . $key . '/' . $modelRemove->$fieldName);
        }
    }
    
    protected function beforeDelete() {
        self::RemoveFileOnly($this->id, 'file_name');
        return parent::beforeDelete();
    }
    
    /**
     * @Author: ANH DUNG Sep 06, 2014
     * @Todo: delete detail by $profile_id
     * @Param: $profile_id
     */
    public static function delete_by_profile_id($profile_id){
        $criteria = new CDbCriteria();
        $criteria->compare("t.profile_id", $profile_id);
        $models = self::model()->findAll($criteria);
        Users::deleteArrModel($models);        
    }
        
    
    /**
     * @Author: ANH DUNG May 18, 2014
     * @Todo: delete record by file_scan_id and array id not in
     * @Param: $file_scan_id 
     * @Param: $aIdNotIn 
     */
    public static function deleteByNotInId($profile_id, $aIdNotIn){
        $criteria = new CDbCriteria();
        $criteria->compare("t.profile_id", $profile_id);
        $criteria->addNotInCondition("t.id", $aIdNotIn);
        $models = self::model()->findAll($criteria);
        Users::deleteArrModel($models);
    }
    
    /**
     * @Author: ANH DUNG Aug 31, 2014
     * @Todo: update order number of file
     * @param: $model model GasProfile
     */
    public static function UpdateOrderNumberFile($model){
        $sql='';
        $tableName = self::model()->tableName();
        if(isset($_POST['GasProfileDetail']['aIdNotIn'])  && count($_POST['GasProfileDetail']['aIdNotIn'])){
            foreach($_POST['GasProfileDetail']['aIdNotIn'] as $key=>$pk){
                $order_number = isset($_POST['GasProfileDetail']['order_number'][$key])?$_POST['GasProfileDetail']['order_number'][$key]:1;
                if($pk){
                    $sql .= "UPDATE $tableName SET "
                        . " `order_number`=\"$order_number\" "
                        . "WHERE `id`=$pk ;";
                }
            }
        }
        if(trim($sql)!='')
            Yii::app()->db->createCommand($sql)->execute();
    }    
    
}