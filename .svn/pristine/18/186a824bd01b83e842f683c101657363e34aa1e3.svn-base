<?php

/**
 * This is the model class for table "{{_mortgage}}".
 *
 * The followings are the available columns in table '{{_mortgage}}':
 * @property integer $id
 * @property string $company_id
 * @property string $bank_id
 * @property string $asset_id
 * @property string $asset_amount
 * @property string $uid_login
 * @property integer $status
 */
class Mortgage extends BaseSpj
{
    const TYPE_STK   = 1;// STK
    const TYPE_BDS   = 2;// BDS    
    const TYPE_TS    = 3;// TS
    
    public $autocomplete_name, $MAX_ID;
    
    /** @Author: KhueNM 
     *  @Todo:
     *  @Param:
     **/
    public function getType() {
        return array(
            Mortgage::TYPE_STK  => 'STK',
            Mortgage::TYPE_BDS  => 'BĐS',
            Mortgage::TYPE_TS   => 'TS'
        );
    }
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Mortgage the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_mortgage}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('borrow_ration, bank_credit, bank_ratio, bank_credit_type, company_id, bank_id, asset_id, asset_amount', 'required', 'on'=>'create, update'),
            array('asset_group, borrow_ration, bank_credit, bank_ratio, bank_credit_type, code_no, id, company_id, bank_id, asset_id, asset_amount, uid_login, status', 'safe'),
            array('type,note,borrow_time,interest_rate,limit_date,limit_payment_guarantee', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'rAsset' => array(self::BELONGS_TO, 'Users', 'asset_id'),
            'rCompany' => array(self::BELONGS_TO, 'Users', 'company_id'),
            'rBank' => array(self::BELONGS_TO, 'Users', 'bank_id'),
            'rUidLogin' => array(self::BELONGS_TO, 'Users', 'uid_login'),
            
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                'id' => 'ID',
                'company_id' => 'Công ty',
                'bank_id' => 'Ngân hàng',
                'asset_id' => 'Tài sản',
                'asset_amount' => 'Định giá tài sản',
                'uid_login' => 'Uid Login',
                'status' => 'Trạng thái',
                'bank_credit' => 'Hạn mức',
                'bank_credit_type' => 'Loại hạn mức',
                'bank_ratio' => 'Hệ số tín chấp(%)',
                'borrow_ration' => 'Hệ số cho vay',
                'code_no' => 'Mã số',
                'created_date' => 'Ngày tạo',
                'asset_group' => 'Nhóm TS',
                'type' => 'Mục',
                'note' => 'Ghi chú',
                'borrow_time'=> 'Thời gian vay(tháng)',
                'interest_rate' => 'Lãi suất năm(%)',
                'limit_date'=> 'Ngày cấp hạn mức',
                'limit_payment_guarantee'=> 'Hạn mức BLTT'
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('t.id',$this->id);
        $criteria->compare('t.company_id',$this->company_id);
        $criteria->compare('t.bank_id',$this->bank_id);
        $criteria->compare('t.bank_credit_type',$this->bank_credit_type);
        $criteria->compare('t.asset_id',$this->asset_id);
        $criteria->compare('t.asset_amount',$this->asset_amount);
        $criteria->compare('t.uid_login',$this->uid_login);
        $criteria->compare('t.status',$this->status);
        $criteria->compare('t.asset_group',$this->asset_group);
        $criteria->order = 't.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
            ),
        ));
    }
    
    protected function beforeSave() {
        if($this->isNewRecord){
            $this->code_no = MyFunctionCustom::getNextId('Mortgage', 'M'.date('y'), 7, 'code_no');
        }
        return parent::beforeSave();
    }
            
    public function canUpdate() {
        return true;// Aug0819 chị Ngân phân quyền cho chị Quỳnh, Hùng theo dõi cập nhật hết, nên ai đc quyền thì cho phép sửa
        $cRole  = MyFormat::getCurrentRoleId();
        $cUid   = MyFormat::getCurrentUid();
        if($cRole == ROLE_ADMIN){
            return true;
        }
        if($cUid != $this->uid_login){
            return false;
        }
        $dayAllow = date('Y-m-d');
        $dayAllow = MyFormat::modifyDays($dayAllow, 10, '-');
        return MyFormat::compareTwoDate($this->created_date, $dayAllow);
    }
    
    public function getListdataByRole($role_id) {
        $mAppCache = new AppCache();
        return $mAppCache->getListdataUserByRole($role_id);
    }
    public function getArrayCreditType() {
        $mBorrow = new Borrow();
        return $mBorrow->getArrayType();
    }
    protected function beforeValidate() {
        $this->asset_amount = MyFormat::removeComma($this->asset_amount);
        $this->bank_credit = MyFormat::removeComma($this->bank_credit);
        $mAsset = Users::model()->findByPk($this->asset_id);
        if($mAsset){
            $this->asset_group = $mAsset->code_account;
        }
        return parent::beforeValidate();
    }
    public function getAsset($field_name='first_name') {
        $mUser = $this->rAsset;
        if($mUser){
            if($field_name=='reason_leave'){
                return $mUser->getUserRefField("reason_leave");
            }
            return $mUser->$field_name;
        }
        return '';
    }
    public function getCompany() {
        $mUser = $this->rCompany;
        if($mUser){
            return $mUser->getFullName();
        }
        return "";
    }
    public function getBank() {
        $mUser = $this->rBank;
        if($mUser){
            return $mUser->getFullName();
        }
        return "";
    }
    public function getUidLogin() {
        $mUser = $this->rUidLogin;
        if($mUser){
            return $mUser->getFullName();
        }
        return "";
    }
    public function getAssetAmount($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->asset_amount);
        }
        return $this->asset_amount;
    }
        
    /** @Author: KhueNM Aug , 2019
     *  @Todo: format data save - gasSourceReport
     *  @Param:
     * */
    public function formatDataBeforeSave() {
        if (strpos($this->limit_date, '/')) {
            $this->limit_date = MyFormat::dateConverDmyToYmd($this->limit_date);
            MyFormat::isValidDate($this->limit_date);
        }
    }
    
    /** @Author: KhueNM 
     **/
    public function getNote() {
        return $this->note;
    }
    
    /** @Author: KhueNM 
     *  @Todo:
     **/
    public function getLimitDate($fomat = 'd/m/Y'){
        return MyFormat::dateConverYmdToDmy($this->limit_date, $fomat);
    }
    
    /** @Author: KhueNM 
     *  @Todo:
     **/
    public function getLimitPaymentGuarantee(){
        return ActiveRecord::formatCurrency($this->limit_payment_guarantee);
    }
    
    /** @Author: KhueNM 
     *  @Todo:
     **/
    public function getBorrowTime($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->borrow_time) . ' tháng';
        }
        return ActiveRecord::formatCurrency($this->borrow_time);
    }
    
    /** @Author: KhueNM 
     *  @Todo:
     **/
    public function getInterestRate(){
        return ActiveRecord::formatCurrency($this->interest_rate) . ' %';
    }
    
    /** @Author: KhueNM 
     *  @Todo:
     *  @Param:
     **/
    public function showType(){
        $aType = $this->getType();
        return empty($aType[$this->type]) ? '' : $aType[$this->type];
    }
    

    public function getBankCredit($format = false) {
        if($format){
            return ActiveRecord::formatCurrency($this->bank_credit);
        }
        return $this->bank_credit;
    }
    public function getBankCreditShort() {
        return MyFormat::shortenLongNumber($this->bank_credit);
    }
    public function getBankRatio() {
        return ActiveRecord::formatCurrency($this->bank_ratio) . ' %';
    }
    public function getBorrowRation() {
        return ActiveRecord::formatCurrency($this->borrow_ration) . ' %';
    }
    public function getAssetGroup() {
        if(empty($this->asset_group)){
            return '';
        }
            
        return $this->asset_group;
    }
    public function getBankCreditType() {
        $aType = $this->getArrayCreditType();
        return isset($aType[$this->bank_credit_type]) ?  $aType[$this->bank_credit_type] : '';
    }
    public function initData() {
        $mAsset = Users::model()->findByPk($this->asset_id);
        if($mAsset){
            $this->asset_amount = $mAsset->last_name;
        }
    }

    /** @Author: ANH DUNG Jun 11, 2017
     * @Todo: run report
     */
    public function report() {
        $criteria=new CDbCriteria;
        $this->mapCondition($criteria);
        $models = Mortgage::model()->findAll($criteria);
        return $models;
    }
    
    /** belong report */
    public function mapCondition(&$criteria) {
        $criteria->compare('t.company_id',$this->company_id);
        $criteria->compare('t.bank_id',$this->bank_id);
        $criteria->compare('t.bank_credit_type',$this->bank_credit_type);
        $criteria->compare('t.asset_id',$this->asset_id);
        $criteria->compare('t.type',$this->type);
        $criteria->compare('t.note',$this->note, true);
        $criteria->compare('t.limit_date',$this->limit_date);
        $criteria->compare('t.borrow_time',$this->borrow_time);
        $criteria->compare('t.interest_rate',$this->interest_rate);
        $criteria->compare('t.limit_payment_guarantee',$this->limit_payment_guarantee);
        $criteria->compare('t.status', STATUS_ACTIVE);
        $criteria->order = 't.company_id, t.bank_id';
//        $criteria->group = 't.asset_group';
        $criteria->group = 't.asset_id, t.company_id, t.bank_id';
//        $criteria->select = 'sum(t.asset_amount) as asset_amount, company_id, bank_id, bank_credit, bank_ratio, borrow_ration, bank_credit_type, asset_group, asset_id'; // old db
        $criteria->select = 'sum(t.asset_amount) as asset_amount, company_id, bank_id, bank_credit, bank_ratio, borrow_ration, bank_credit_type, asset_group, asset_id,limit_payment_guarantee,type,note,limit_date,borrow_time,interest_rate';
    }
    
    /**
     * @Author: ANH DUNG Jun 11, 2017
     * @Todo: foreach để tính lại rowspan
     */
    public function calcRowSpan($models) {
        $aRes = [];
        foreach($models as $model){
            if(!isset($aRes['COMPANY'][$model->company_id])){
                $aRes['COMPANY'][$model->company_id] = 1;
            }else{
                $aRes['COMPANY'][$model->company_id] += 1;
            }
            
            if(!isset($aRes['BANK'][$model->company_id][$model->bank_id])){
                $aRes['BANK'][$model->company_id][$model->bank_id] = 1;
            }else{
                $aRes['BANK'][$model->company_id][$model->bank_id] += 1;
            }
            if(!isset($aRes['CREDIT_TYPE'][$model->company_id][$model->bank_id][$model->bank_credit_type])){
                $aRes['CREDIT_TYPE'][$model->company_id][$model->bank_id][$model->bank_credit_type] = 1;
            }else{
                $aRes['CREDIT_TYPE'][$model->company_id][$model->bank_id][$model->bank_credit_type] += 1;
            }
            
            $aRes['ASSETS_SUM'][$model->company_id][$model->bank_id][$model->asset_id]['sum'] = $model->asset_amount;
//            if(!isset($aRes['ASSETS_SUM'][$model->company_id][$model->bank_id][$model->asset_id]['sum'])){
//            }else{
//                $aRes['ASSETS_SUM'][$model->company_id][$model->bank_id][$model->asset_id]['sum'] += $model->asset_amount;
//            }
            $aRes['ASSETS_SUM'][$model->company_id][$model->bank_id][$model->asset_id]['data'] = $model;
            
            
            if(!isset($aRes['ASSETS'][$model->company_id][$model->bank_id][$model->asset_id])){
                $aRes['ASSETS'][$model->company_id][$model->bank_id][$model->asset_id] = 1;
            }else{
                $aRes['ASSETS'][$model->company_id][$model->bank_id][$model->asset_id] += 1;
            }

            $moneyWillReceive = ($model->asset_amount * $model->borrow_ration)/$model->bank_ratio;
            if(!isset($aRes['ASSETS_GIAI_NGAN_SUM'][$model->company_id][$model->bank_id][$model->asset_id])){
                $aRes['ASSETS_GIAI_NGAN_SUM'][$model->company_id][$model->bank_id][$model->asset_id] = $moneyWillReceive;
            }else{
                $aRes['ASSETS_GIAI_NGAN_SUM'][$model->company_id][$model->bank_id][$model->asset_id] += $moneyWillReceive;
            }
            
        }
        $session = Yii::app()->session;
        $session['mortgage-excel-report'] = $this;
        return $aRes;
    }
    
    
}