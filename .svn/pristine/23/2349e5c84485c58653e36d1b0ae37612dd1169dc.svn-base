<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CommonProcess
 *
 * @author NguyenPT
 */
class CommonProcess {
    /**
     * Gnerate code no value
     * @param String $prefix Prefix
     * @param Int $length Length of return value
     * @return string
     */
    public static function generateCodeNo($prefix, $length = 6) {
        $prefixCode = $prefix.date('y');
        $retVal = $prefixCode . ActiveRecord::randString($length, GasConst::CODE_CHAR);
        return $retVal;
    }
    
    /**
     * Get list interview result score
     * @return String
     */
    public static function getInterviewScore() {
        $retVal = array();
        for ($index = 0; $index < 11; $index++) {
            $retVal[$index] = $index . '';
        }
        return $retVal;
    }
    
    //-----------------------------------------------------
    // ++ Date time process
    //-----------------------------------------------------
    /**
     * Get value of current date time
     * @param String $format Date time format
     * @return Date time string (default is DATE_FORMAT_1 - 'Y-m-d H:i:s')
     */
    public static function getCurrentDateTime($format = DomainConst::DATE_FORMAT_1) {
        date_default_timezone_set(DomainConst::DEFAULT_TIMEZONE);
        return date($format);
    }
    
    /**
     * Convert a datetime value to another format for save to my sql
     * @param String $datetime Input date timme string
     * @return String Date time string after format
     */
    public static function convertDateTimeToMySqlFormat($datetime, $format) {
        $retVal = CommonProcess::convertDateTime($datetime , $format, DomainConst::DATE_FORMAT_1);
        return $retVal;
    }
    
    /**
     * Get current date time string with format for save to my sql
     * @return String Date time string
     */
    public static function getCurrentDateTimeWithMySqlFormat() {
        return CommonProcess::getCurrentDateTime();
    }
    
    /**
     * Convert date time with format
     * @param type $datetime
     * @param type $format
     * @return type
     */
    public static function convertDateTimeWithFormat($datetime, $format = DomainConst::DATE_FORMAT_3) {
        $time = '';
        $datetimeFormat = DateTime::createFromFormat(DomainConst::DATE_FORMAT_1, $datetime);
        if ($datetimeFormat) {
            $time = $datetimeFormat->format($format);
        }
        
        return $time;
    }
    
    /**
     * Convert date time
     * @param String $datetime Date time value to convert
     * @param String $fromFormat Convert from this format
     * @param String $toFormat Convert to this format
     * @return String Date time value after convert
     */
    public static function convertDateTime($datetime, $fromFormat, $toFormat) {
        $converter = DateTime::createFromFormat($fromFormat, $datetime);
        if ($converter) {
            return $converter->format($toFormat);
        }
        return '';
    }
    
    /**
     * Get tomorrow day
     * @param String $format Date time format
     * @return Date time string (default is DATE_FORMAT_1 - 'Y-m-d H:i:s')
     */
    public static function getNextDateTime($format = DomainConst::DATE_FORMAT_1) {
        date_default_timezone_set(DomainConst::DEFAULT_TIMEZONE);
        return date($format, strtotime('+1 day'));
    }
    
    /**
     * Get first day of current month
     * @param String $format Date time format
     * @return Date time string (default is DATE_FORMAT_1 - 'Y-m-d H:i:s')
     */
    public static function getFirstDateOfCurrentMonth($format = DomainConst::DATE_FORMAT_1) {
        date_default_timezone_set(DomainConst::DEFAULT_TIMEZONE);
        $formatFirst = str_replace("d", "01", $format);
        return date($formatFirst);
    }
    
    /**
     * Get last day of current month
     * @param String $format Date time format
     * @return Date time string (default is DATE_FORMAT_1 - 'Y-m-d H:i:s')
     */
    public static function getLastDateOfCurrentMonth($format = DomainConst::DATE_FORMAT_1) {
        date_default_timezone_set(DomainConst::DEFAULT_TIMEZONE);
        $formatFirst = str_replace("d", "t", $format);
        return date($formatFirst);
    }
    
    /**
     * Get first date of month
     * @param String $date Date time value (format is DATE_FORMAT_4 - 'Y-m-d')
     * @return Date time string (default is DATE_FORMAT_4 - 'Y-m-d')
     */
    public static function getFirstDateOfMonth($date) {
        return date('Y-m-01', strtotime($date));
    }
    
    /**
     * Get last date of month
     * @param String $date Date time value (format is DATE_FORMAT_4 - 'Y-m-d')
     * @return Date time string (default is DATE_FORMAT_4 - 'Y-m-d')
     */
    public static function getLastDateOfMonth($date) {
        return date('Y-m-t', strtotime($date));
    }
    
    /**
     * Get date period
     * @param Sring $from Date from (format is DATE_FORMAT_4 - 'Y-m-d')
     * @param Sring $to Date to (format is DATE_FORMAT_4 - 'Y-m-d')
     */
    public static function getDatePeriod($from, $to) {
        $begin = new DateTime($from);
        $end = (new DateTime($to))->modify('+1 day');
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);
        return $period;
    }
    
    /**
     * Get month period
     * @param Sring $from Date from (format is DATE_FORMAT_4 - 'Y-m-d')
     * @param Sring $to Date to (format is DATE_FORMAT_4 - 'Y-m-d')
     */
    public static function getMonthPeriod($from, $to) {
        $begin = new DateTime($from);
        $end = (new DateTime($to))->modify('+1 day');
        $interval = DateInterval::createFromDateString('first day of next month');
        $period = new DatePeriod($begin, $interval, $end);
        return $period;
    }
    
    /**
     * Get previous month
     * @param String $format Date time format
     * @return Date time string (default is DATE_FORMAT_1 - 'Y-m-d H:i:s')
     */
    public static function getPreviousMonth($format = DomainConst::DATE_FORMAT_1) {
        date_default_timezone_set(DomainConst::DEFAULT_TIMEZONE);
        return date($format, strtotime('-1 month'));
    }
    
    /**
     * Get next month
     * @param String $format Date time format
     * @param Int $monthCount Count of month
     * @return Date time string (default is DATE_FORMAT_1 - 'Y-m-d H:i:s')
     */
    public static function getNextMonth($format = DomainConst::DATE_FORMAT_1, $monthCount = 1) {
        $date = self::getFirstDateOfCurrentMonth();
        date_default_timezone_set(DomainConst::DEFAULT_TIMEZONE);
        return date($format, strtotime('+' . $monthCount . ' month', strtotime($date)));
    }
    
    /**
     * Get next month of a date
     * @param String $date Date value (format: Y-m-d)
     * @return type
     */
    public static function getNextMonthOfDate($date, $format = DomainConst::DATE_FORMAT_1, $monthCount = 1) {
        $firstDate = self::getFirstDateOfMonth($date);
        date_default_timezone_set(DomainConst::DEFAULT_TIMEZONE);
        return date($format, strtotime('+' . $monthCount . ' month', strtotime($firstDate)));
    }
    //-----------------------------------------------------
    // -- Date time process
    //-----------------------------------------------------

    public static function dumpVariable($variable) {
        echo "<pre>";
        var_dump($variable);
        echo "<pre>";
        die;
    }
    
    public static function dumpArrVariable($arrVariable) {
        echo '<pre>';
        echo print_r($arrVariable);
        echo '</pre>';
        die;
    }
    
    /**
     * Echo test string
     * @param String $message Message
     * @param String $data Data
     */
    public static function echoTest($message, $data) {
        if (is_array($data)) {
            $sData = '[' . implode(", ", $data) . ']';
            echo '<b>' . $message . '</b>' . '<i>' . $sData . '</i>';
        } else {
            echo '<b>' . $message . '</b>' . '<i>' . $data . '</i>';
        }
        echo '<br/>';
    }
    
    /**
     * Echo test string
     * @param String $message Message
     * @param String $data Data
     */
    public static function echoArrayKeyValue($message, $data) {
        if (is_array($data)) {
            $aData = array();
            foreach ($data as $key => $value) {
                if (is_array($value)) {
                    
                } else {
                    $aData[] = '(' . $key . ' - ' . $value . ')';
                }
            }
            $sData = '[' . implode(", ", $aData) . ']';
            echo '<b>' . $message . '</b>' . '<i>' . $sData . '</i>';
            echo '<br/>';
        }
    }
    
    //-----------------------------------------------------
    // ++ Handle json string
    //-----------------------------------------------------
    public static function json_encode_unicode($data) {
        if (defined('JSON_UNESCAPED_UNICODE')) {
            return json_encode($data, JSON_UNESCAPED_UNICODE);
        }
        return preg_replace_callback('/(?<!\\\\)\\\\u([0-9a-f]{4})/i',
            function ($m) {
                $d = pack("H*", $m[1]);
                $r = mb_convert_encoding($d, "UTF8", "UTF-16BE");
                return $r!=="?" && $r!=="" ? $r : $m[0];
            }, json_encode($data)
        );
    }
}
