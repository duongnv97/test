CREATE TABLE IF NOT EXISTS `gas_referral_tracking` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `invited_id` int(11) unsigned NOT NULL COMMENT 'nguoi dc gioi thieu',
  `ref_id` int(11) unsigned NOT NULL COMMENT 'nguoi gioi thieu',
  `status` tinyint(1) unsigned NOT NULL COMMENT '0: new, 1: confirm',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `invited_id` (`invited_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;


ALTER TABLE `gas_app_promotion` ADD `owner_id` int(11) DEFAULT NULL COMMENT 'id of owner';
ALTER TABLE  `gas_app_promotion` CHANGE  `owner_id`  `owner_id` INT( 11 ) UNSIGNED NULL DEFAULT NULL COMMENT  'id of owner';
