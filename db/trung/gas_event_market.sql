
CREATE TABLE `gas_event_market` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Tiêu đề',
  `employees` text CHARACTER SET utf8 NOT NULL COMMENT 'Json danh sách nhân viên',
  `agents` text CHARACTER SET utf8 NOT NULL COMMENT 'Json danh sách đại lý',
  `start_date` datetime NOT NULL COMMENT 'Ngày bắt đầu',
  `end_date` datetime NOT NULL COMMENT 'Ngày kết thúc',
  `note` text CHARACTER SET utf8 COMMENT 'Ghi chú',
  `status` tinyint(3) UNSIGNED NOT NULL COMMENT '1: hoạt động, 0. Không hoạt động',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `gas_event_market`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `status` (`status`);

ALTER TABLE `gas_event_market`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

