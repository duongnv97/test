<?php

    function updateAccount($username1, $updateAccount1, &$no)
    {
        $api_resourse = 'user/updateAccount';
        $action_url = API_ENDPOINT.$api_resourse;
        $method = 'POST';
        $q = json_encode($updateAccount1);
        echo '<h1>'.$no++.'. user/updateAccount '.$username1.'</h1><br>Request: '.$method.': '.$action_url.'<br/><pre>';
        print_r($q);
        echo '</pre>';
        $res = request($q, $action_url, $method);
    }
    function setNewPasscode($username1, $token1, &$no)
    {
        $api_resourse = 'user/setNewPasscode';
        $action_url = API_ENDPOINT.$api_resourse;
        $method = 'POST';
        $q = '{"username":"'.$username1.'","token":"'.$token1.'","passcode":"1234"}';
        echo '<h1>'.$no++.'. '.$api_resourse.' '.$username1.'</h1><br>Request: '.$method.': '.$action_url.'<br/><pre>';
        print_r($q);
        echo '</pre>';
        $res = request($q, $action_url, $method);
    }
    function changePasscode($username1, $token1, $res, &$no)
    {
        $api_resourse = 'user/changePasscode';
        $action_url = API_ENDPOINT.$api_resourse;
        $method = 'POST';
        $q = '{"username":"'.$username1.'","token":"'.$token1.'","old_passcode":"'.$res['record']['passcode'].'","new_passcode":"1234"}';
        echo '<h1>'.$no++.'. '.$api_resourse.' '.$username1.'</h1><br>Request: '.$method.': '.$action_url.'<br/><pre>';
        print_r($q);
        echo '</pre>';
        $res = request($q, $action_url, $method);
    }
    function loginFirstTime($username1, &$no)
    {
        $api_resourse = 'user/loginFirstTime';
        $action_url = API_ENDPOINT.$api_resourse;
        $method = 'POST';
        $q = '{"username":"'.$username1.'"}';
        echo '<h1>'.$no++.'. '.$api_resourse.' '.$username1.'</h1><br>Request: '.$method.': '.$action_url.'<br/><pre>';
        print_r($q);
        echo '</pre>';
        $res = request($q, $action_url, $method);
    }
    function verifyMobileNumber($username1, $verification_code1, &$no)
    {
        $api_resourse = 'user/verifyMobileNumber';
        $action_url = API_ENDPOINT.$api_resourse;
        $method = 'POST';
        $q = '{"username":"'.$username1.'","verification_code":"'.$verification_code1.'"}';
        echo '<h1>'.$no++.'. user/verifyMobileNumber '.$username1.'</h1><br>Request: '.$method.': '.$action_url.'<br/><pre>';
        print_r($q);
        echo '</pre>';
        $res = request($q, $action_url, $method);
    }
    function details($username1, $token1, &$no)
    {
        $api_resourse = 'user/details';
        $action_url = API_ENDPOINT.$api_resourse;
        $method = 'POST';
        $q = '{"username":"'.$username1.'","token":"'.$token1.'"}';
        echo '<h1>'.$no++.'. '.$api_resourse.' '.$username1.'</h1><br>Request: '.$method.': '.$action_url.'<br/><pre>';
        print_r($q);
        echo '</pre>';
        $res = request($q, $action_url, $method);
        return $res;
    }
    function balance($username2, $token2, &$no)
    {
        $api_resourse = 'user/balance';
        $action_url = API_ENDPOINT.$api_resourse;
        $method = 'POST';
        $q = '{"username":"'.$username2.'","token":"'.$token2.'"}';
        echo '<h1>'.$no++.'. user/balance '.$username2.'</h1><br>Request: '.$method.': '.$action_url.'<br/><pre>';
        print_r($q);
        echo '</pre>';
        $res = request($q, $action_url, $method);
    }
    function transactionList($username2, $token2, &$no)
    {
        $api_resourse = 'transaction/list';
        $action_url = API_ENDPOINT.$api_resourse;
        $method = 'POST';
        $q = '{"username":"'.$username2.'","token":"'.$token2.'"}';
        echo '<h1>'.$no++.'. '.$api_resourse.' '.$username2.'</h1><br>Request: '.$method.': '.$action_url.'<br/><pre>';
        print_r($q);
        echo '</pre>';
        $res = request($q, $action_url, $method);
        echo '<pre>';
        print_r($res);
        echo '</pre>';
    }
    function request($q, $action_url, $method)
    {
        $q = json_decode($q, true);
        $q["debug"] = 1;
        $q = json_encode($q);
        try
        {
        //    $avatar = file_get_contents('http://c1.f3.img.vnecdn.net/2015/05/25/legenda-thailand-ingin-bantu-t-6786-3036-1432523464_490x294.jpg');
        //    $avatar = base64_encode(file_get_contents('G:/ecoin/upload/xxx.jpg'));
            $paramSend = array('q'=>$q);
        //    $paramSend = array('q'=>'{"username":"841689700979","token":"7891a56222059e54f76b4075a46e1e9d","name":"Bao","dob":"1987-10-11","email":"quocbao1087@gmail.com","nric":"290893135","avatar":"'.$avatar.'"}');
            $request = new OAuthRequester($action_url, $method, $paramSend);
            $result = $request->doRequest();

            echo 'Response: <pre>';
//            print_r(json_decode($result['body'], true));
            print_r($result['body']);
            echo '</pre>';
            return json_decode($result['body'], true);
        }
        catch(OAuthException2 $e)
        {
            echo '<pre>';
            print_r($e);
            echo '</pre>';
            exit;
        }
    }

?>
