<?php ob_start(); ?>
<!DOCTYPE html>
<html lang="en">
<?php include_once('inc/head.php'); ?>

  <body>

    <?php include_once('inc/header.php'); ?>

    <div class="container-fluid">
      <div class="row-fluid">
        <?php include_once('inc/sidebar.php'); ?>
        
        <div class="span9">
            <div class="page-header">
            <h1>Gas API Test Only From PHP Request</h1>
        </div>

            <div class="navbar">
              <div class="navbar-inner">
                  <form class="form-horizontal" method="post">
                      <fieldset>
                          <legend>Content to send</legend>
                         
                          <div class="control-group">
                          <label class="control-label" for="method">Method</label>
                          <div class="controls">
                              <select name="method" class="span3">
                                  <option value="POST" <?php if(isset($_POST['method']) && $_POST['method'] =='POST')  echo 'selected="selected"'; ?>>POST</option>
                                  <option value="GET"<?php if(isset($_POST['method']) && $_POST['method'] =='GET')  echo 'selected="selected"'; ?>>GET</option>
                                  <option value="PUT"<?php if(isset($_POST['method']) && $_POST['method'] =='PUT')  echo 'selected="selected"'; ?>>PUT</option>
                                  <option value="DELETE"<?php if(isset($_POST['method']) && $_POST['method'] =='DELETE')  echo 'selected="selected"'; ?>>DELETE</option>
                              </select>
                          </div>
                      </div>  
                          
                        <div class="control-group">
                          <label class="control-label" for="api_resourse">Api resourse</label>
                          <div class="controls">
                              <input name="api_resourse" id="api_resourse"  value="<?php if(isset($_POST['api_resourse'])) echo ($_POST['api_resourse']);?>" type="text" class="span10" placeholder="<?php echo 'user/loginFirstTime'; ?>">
                          </div>
                      </div>  
                          
                      <div class="control-group">
                          <label class="control-label" for="q">q=</label>
                          <div class="controls">
                              <textarea rows="5" name="q" id="q" class="span10" placeholder='{"token":"3ba8bfa6221c671fe682f965491fd86c","b50":"1", "b45":"0", "b12":"2", "b6":"0", "note_customer":""}'><?php if(isset($_POST['q'])) echo $_POST['q']; else echo ''; ?></textarea>
                          </div>
                      </div>    
                     
                      <div class="control-group">
                          <div class="controls">
                              <button class="btn" type="submit">Submit</button>
                          </div>
                      </div>
                      </fieldset>
                </form>
              </div>
            </div>
            
            <h2>Result</h2>
            <div class="well">
                <?php require_once('code/_twoleggedTest.php'); ?>
            </div>
        </div>
        
        
        