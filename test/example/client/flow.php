<?php
/*
 * bb
 */
include_once "../../library/OAuthStore.php";
include_once "../../library/OAuthRequester.php";
include_once "code/_flow.php";

// Test of the OAuthStore2Leg 

$key = 'admin'; // fill with your public key 
$secret = '123456'; // fill with your secret key
//define('API_ENDPOINT', 'http://code.local/ecoin/api/');
define('API_ENDPOINT', 'http://verzview.com/verzecoin/demo/api/');

$options = array('consumer_key' => $key, 'consumer_secret' => $secret);
$oinstance = OAuthStore::instance("2Leg", $options);

//fixtures
//$username1 = '841689700979';
//$username2 = '84993958309';
//$username3 = '84902244581';//merchant
//$verification_code1 = '289651';
//$verification_code2 = '172015';
//$verification_code3 = '891867';
//$token1 = '85631d9f0849aa90575b2e10263c6a23';
//$token2 = '7677453737f83c3330fa4b9369ff9788';
//$token3 = '96ea2e9b3c9390d517a42535ef66d76f';

//verzview
$username1 = '841689700979';
$username2 = '84993958309';
$username3 = '84902244581';//merchant
$verification_code1 = '';
$verification_code2 = '';
$verification_code3 = '';
$token1 = '';
$token2 = '';
$token3 = '';

$updateAccount1 = array("username"=>$username1,
                        "token"=>$token1,
                        "name"=>"Bao Receiver ".$username1,
                        "nric"=>"000".$username1,
                        "dob"=>"1987-11-10",
                        "email"=>"receiver1087@gmail.com",
                    );

$updateAccount2 = array("username"=>$username2,
                        "token"=>$token2,
                        "name"=>"Bao Sender ".$username2,
                        "nric"=>"000".$username2,
                        "dob"=>"1987-11-10",
                        "email"=>"sender1087@yahoo.com",
                    );
$topupAmount1 = 10;
$topupAmount2 = 10;
        
$max_payment_limit1 = 100;
$max_payment_limit2 = 100;
$max_payment_limit_without_passcode1 = 2;
$max_payment_limit_without_passcode2 = 2;

$toTestUpdateAccount = true;
$toTestSetting = true;
$toTestTopUp = true;

$toTestReceiveRequest = true;
$receiveRequestAmount = 3;      
$toTestAcceptRequest = true;
$txn_id_acceptRequest = '';
$qrCodeReceiveRequest = '';

$toTestPayToNumber = true;
$toTestPayToMerchant = true;
$staff_id = 1;
$payToAmount = 2;

$no = 1;
if($verification_code1== '' || $verification_code2 == '' || $verification_code3 == '')
{
    loginFirstTime($username1, $no);
    loginFirstTime($username2, $no);
    loginFirstTime($username3, $no);
    exit;//fill verification code to fixtures
}

if($token1 =='' || $token2 == '' || $token3 == '')
{
    verifyMobileNumber($username1, $verification_code1, $no);
    verifyMobileNumber($username2, $verification_code2, $no);
    verifyMobileNumber($username3, $verification_code3, $no);
    exit;//fill token to fixtures
}

if($toTestUpdateAccount){
    updateAccount($username1, $updateAccount1, $no);
    updateAccount($username2, $updateAccount2, $no);
    
    $res = details($username1, $token1, $no);
    if($res['record']['passcode'] == '')
    {
        setNewPasscode($username1, $token1, $no);
    }
    else {
        changePasscode($username1, $token1, $res, $no);
    }
    
    $res = details($username2, $token2, $no);
    if($res['record']['passcode'] == '')
    {
        setNewPasscode($username2, $token2, $no);
    }else {
        changePasscode($username2, $token2, $res, $no);
    }
}

if($toTestSetting){
    //SETTINGS    
    $api_resourse = 'user/setMaxPaymentLimit';
    $action_url = API_ENDPOINT.$api_resourse;
    $method = 'POST';
    $q = '{"username":"'.$username2.'","token":"'.$token2.'","max_payment_limit":"'.$max_payment_limit2.'"}';
    echo '<h1>'.$no++.'. user/setMaxPaymentLimit '.$username2.'</h1><br>Request: '.$method.': '.$action_url.'<br/><pre>';
    print_r($q);
    echo '</pre>';
    $res = request($q, $action_url, $method);
    
    $api_resourse = 'user/setMaxPaymentLimitWithoutPasscode';
    $action_url = API_ENDPOINT.$api_resourse;
    $method = 'POST';
    $q = '{"username":"'.$username2.'","token":"'.$token2.'","max_payment_limit_without_passcode":"'.$max_payment_limit_without_passcode2.'"}';
    echo '<h1>'.$no++.'. user/setMaxPaymentLimitWithoutPasscode '.$username2.'</h1><br>Request: '.$method.': '.$action_url.'<br/><pre>';
    print_r($q);
    echo '</pre>';
    $res = request($q, $action_url, $method);
}

if($toTestTopUp){
    //TOP UP
    $api_resourse = 'transaction/topup';
    $action_url = API_ENDPOINT.$api_resourse;
    $method = 'POST';
    $q = '{"username":"'.$username2.'","token":"'.$token2.'","amount":"'.$topupAmount2.'"}';
    echo '<h1>'.$no++.'. transaction/topup '.$username2.'</h1><br>Request: '.$method.': '.$action_url.'<br/><pre>';
    print_r($q);
    echo '</pre>';
    $res = request($q, $action_url, $method);
}

if($toTestReceiveRequest){//request accept
    $api_resourse = 'transaction/receiveRequest';
    $action_url = API_ENDPOINT.$api_resourse;
    $method = 'POST';
    $q = '{"username":"'.$username1.'","token":"'.$token1.'","sender_username":"'.$username2.'","sender_name":"SRECEIVE REQUEST ACCEPT","amount":"'.$receiveRequestAmount.'","remarks":"need '.$receiveRequestAmount.'"}';
    echo '<h1>'.$no++.'. transaction/receiveRequest '.$username2.'</h1><br>Request: '.$method.': '.$action_url.'<br/><pre>';
    print_r($q);
    echo '</pre>';
    $res = request($q, $action_url, $method);
    $txn_id_acceptRequest = $res['record']['txn_id'];
    
    balance($username1, $token1, $no);
    balance($username2, $token2, $no);
}

if($toTestAcceptRequest){
    $api_resourse = 'transaction/acceptRequest';
    $action_url = API_ENDPOINT.$api_resourse;
    $method = 'POST';
    $q = '{"username":"'.$username2.'","token":"'.$token2.'","txn_id":"'.$txn_id_acceptRequest.'","passcode":"1234"}';
    echo '<h1>'.$no++.'. '.$api_resourse.' '.$username2.'</h1><br>Request: '.$method.': '.$action_url.'<br/><pre>';
    print_r($q);
    echo '</pre>';
    $res = request($q, $action_url, $method);
    
    balance($username1, $token1, $no);
    balance($username2, $token2, $no);
}

if($toTestReceiveRequest){//request scan
    $api_resourse = 'transaction/receiveRequest';
    $action_url = API_ENDPOINT.$api_resourse;
    $method = 'POST';
    $q = '{"username":"'.$username1.'","token":"'.$token1.'","sender_username":"'.$username2.'","sender_name":"RECEIVE REQUEST SCAN","amount":"'.$receiveRequestAmount.'","remarks":"need '.$receiveRequestAmount.'"}';
    echo '<h1>'.$no++.'. '.$api_resourse.' '.$username2.'</h1><br>Request: '.$method.': '.$action_url.'<br/><pre>';
    print_r($q);
    echo '</pre>';
    $res = request($q, $action_url, $method);
    $qrCodeReceiveRequest = $res['qr_code_test'];
    
    $api_resourse = 'transaction/checkQRCode';
    $action_url = API_ENDPOINT.$api_resourse;
    $method = 'POST';
    $q = '{"username":"'.$username2.'","token":"'.$token2.'","qr_code":"'.$qrCodeReceiveRequest.'","passcode":"1234","confirm":"1"}';
    echo '<h1>'.$no++.'. '.$api_resourse.' '.$username2.'</h1><br>Request: '.$method.': '.$action_url.'<br/><pre>';
    print_r($q);
    echo '</pre>';
    $res = request($q, $action_url, $method);
    echo '<pre>';
    print_r($res);
    echo '</pre>';
    
    balance($username1, $token1, $no);
    balance($username2, $token2, $no);
}

if($toTestPayToNumber){
    $api_resourse = 'transaction/pay';
    $action_url = API_ENDPOINT.$api_resourse;
    $method = 'POST';
    $q = '{"username":"'.$username2.'","token":"'.$token2.'","receiver_username":"'.$username1.'","receiver_name":"PAY TO NUMBER '.$username1.'","amount":"'.$payToAmount.'","remarks":"my remark payto","passcode":"1234"}';
    echo '<h1>'.$no++.'. '.$api_resourse.' '.$username2.'</h1><br>Request: '.$method.': '.$action_url.'<br/><pre>';
    print_r($q);
    echo '</pre>';
    $res = request($q, $action_url, $method);
    
    balance($username1, $token1, $no);
    balance($username2, $token2, $no);
}

if($toTestPayToMerchant){
    balance($username3, $token3, $no);
    
    $api_resourse = 'transaction/pay';
    $action_url = API_ENDPOINT.$api_resourse;
    $method = 'POST';
    $q = '{"username":"'.$username2.'","token":"'.$token2.'","receiver_username":"'.$username3.'","receiver_name":"PAY TO MERCHANT '.$username3.'","staff_id":"'.$staff_id.'","amount":"'.$payToAmount.'","remarks":"my remark payto merchant","passcode":"1234"}';
    echo '<h1>'.$no++.'. '.$api_resourse.' '.$username2.'</h1><br>Request: '.$method.': '.$action_url.'<br/><pre>';
    print_r($q);
    echo '</pre>';
    $res = request($q, $action_url, $method);
    
    balance($username2, $token2, $no);
    balance($username3, $token3, $no);
}

    transactionList($username1, $token1, $no);
    transactionList($username2, $token2, $no);
    transactionList($username3, $token3, $no);

    
exit;

?>
