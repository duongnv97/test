<?php 
    $code = isset($_GET['code']) ? $_GET['code'] : '';
?>
<div class="maincontent">
    <div class="container">
       <div class="tbl-full">
           <div class="tbl-cell">
               <div class="box-slider">
                   <div  class="inner-slider">
                       <div id="slider">
                               <div><img src="img/img1.jpg" alt=""></div>
                               <div><img src="img/img2.jpg" alt=""></div>
                               <div><img src="img/img3.jpg" alt=""></div>
                               <div><img src="img/img4.jpg" alt=""></div>
                               <div><img src="img/img5.jpg" alt=""></div>
                           </div>
                   </div>

       </div>
       <div class="box-content">
           <div class="box1">
               <h4>mã giới thiệu</h4>
               <div class="box1-info">
                   <?php echo $code;?>
               </div>
           </div>
           <div class="box2">
               <h4>Hướng dẫn</h4>
               <p>Bước 1: Chọn mục <b>"Khuyến mãi"</b> trên thanh tùy chọn</p>
               <p>Bước 2: Chọn chức năng <b>"Sử dụng mã"</b> </p>
               <p>Bước 3: Nhập mã khuyến mãi</p>
               <div class="box-download">
                   <a href="https://itunes.apple.com/us/app/gas24h/id1189016286?mt=8"><img src="img/app-store.png" alt=""></a>
                   <a href="https://play.google.com/store/apps/details?id=gas.dung.com.gas24h&hl=en"><img src="img/google-play.png" alt=""></a>
               </div>
           </div>
       </div>
           </div>
       </div>
   </div>
</div>